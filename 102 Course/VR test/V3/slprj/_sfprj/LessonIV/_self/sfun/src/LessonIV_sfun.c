/* Include files */

#include "LessonIV_sfun.h"
#include "LessonIV_sfun_debug_macros.h"
#include "c4_LessonIV.h"
#include "c7_LessonIV.h"
#include "c23_LessonIV.h"
#include "c32_LessonIV.h"
#include "c43_LessonIV.h"
#include "c53_LessonIV.h"

/* Type Definitions */

/* Named Constants */

/* Variable Declarations */

/* Variable Definitions */
uint32_T _LessonIVMachineNumber_;

/* Function Declarations */

/* Function Definitions */
void LessonIV_initializer(void)
{
}

void LessonIV_terminator(void)
{
}

/* SFunction Glue Code */
unsigned int sf_LessonIV_method_dispatcher(SimStruct *simstructPtr, unsigned int
  chartFileNumber, const char* specsCksum, int_T method, void *data)
{
  if (chartFileNumber==4) {
    c4_LessonIV_method_dispatcher(simstructPtr, method, data);
    return 1;
  }

  if (chartFileNumber==7) {
    c7_LessonIV_method_dispatcher(simstructPtr, method, data);
    return 1;
  }

  if (chartFileNumber==23) {
    c23_LessonIV_method_dispatcher(simstructPtr, method, data);
    return 1;
  }

  if (chartFileNumber==32) {
    c32_LessonIV_method_dispatcher(simstructPtr, method, data);
    return 1;
  }

  if (chartFileNumber==43) {
    c43_LessonIV_method_dispatcher(simstructPtr, method, data);
    return 1;
  }

  if (chartFileNumber==53) {
    c53_LessonIV_method_dispatcher(simstructPtr, method, data);
    return 1;
  }

  return 0;
}

extern void sf_LessonIV_uses_exported_functions(int nlhs, mxArray * plhs[], int
  nrhs, const mxArray * prhs[])
{
  plhs[0] = mxCreateLogicalScalar(0);
}

unsigned int sf_LessonIV_process_check_sum_call( int nlhs, mxArray * plhs[], int
  nrhs, const mxArray * prhs[] )
{

#ifdef MATLAB_MEX_FILE

  char commandName[20];
  if (nrhs<1 || !mxIsChar(prhs[0]) )
    return 0;

  /* Possible call to get the checksum */
  mxGetString(prhs[0], commandName,sizeof(commandName)/sizeof(char));
  commandName[(sizeof(commandName)/sizeof(char)-1)] = '\0';
  if (strcmp(commandName,"sf_get_check_sum"))
    return 0;
  plhs[0] = mxCreateDoubleMatrix( 1,4,mxREAL);
  if (nrhs>1 && mxIsChar(prhs[1])) {
    mxGetString(prhs[1], commandName,sizeof(commandName)/sizeof(char));
    commandName[(sizeof(commandName)/sizeof(char)-1)] = '\0';
    if (!strcmp(commandName,"machine")) {
      ((real_T *)mxGetPr((plhs[0])))[0] = (real_T)(4174807114U);
      ((real_T *)mxGetPr((plhs[0])))[1] = (real_T)(1819290195U);
      ((real_T *)mxGetPr((plhs[0])))[2] = (real_T)(3183789453U);
      ((real_T *)mxGetPr((plhs[0])))[3] = (real_T)(1999643636U);
    } else if (!strcmp(commandName,"exportedFcn")) {
      ((real_T *)mxGetPr((plhs[0])))[0] = (real_T)(0U);
      ((real_T *)mxGetPr((plhs[0])))[1] = (real_T)(0U);
      ((real_T *)mxGetPr((plhs[0])))[2] = (real_T)(0U);
      ((real_T *)mxGetPr((plhs[0])))[3] = (real_T)(0U);
    } else if (nrhs==3 && !strcmp(commandName,"chart")) {
      unsigned int chartFileNumber;
      chartFileNumber = (unsigned int)mxGetScalar(prhs[2]);
      switch (chartFileNumber) {
       case 4:
        {
          extern void sf_c4_LessonIV_get_check_sum(mxArray *plhs[]);
          sf_c4_LessonIV_get_check_sum(plhs);
          break;
        }

       case 7:
        {
          extern void sf_c7_LessonIV_get_check_sum(mxArray *plhs[]);
          sf_c7_LessonIV_get_check_sum(plhs);
          break;
        }

       case 23:
        {
          extern void sf_c23_LessonIV_get_check_sum(mxArray *plhs[]);
          sf_c23_LessonIV_get_check_sum(plhs);
          break;
        }

       case 32:
        {
          extern void sf_c32_LessonIV_get_check_sum(mxArray *plhs[]);
          sf_c32_LessonIV_get_check_sum(plhs);
          break;
        }

       case 43:
        {
          extern void sf_c43_LessonIV_get_check_sum(mxArray *plhs[]);
          sf_c43_LessonIV_get_check_sum(plhs);
          break;
        }

       case 53:
        {
          extern void sf_c53_LessonIV_get_check_sum(mxArray *plhs[]);
          sf_c53_LessonIV_get_check_sum(plhs);
          break;
        }

       default:
        ((real_T *)mxGetPr((plhs[0])))[0] = (real_T)(0.0);
        ((real_T *)mxGetPr((plhs[0])))[1] = (real_T)(0.0);
        ((real_T *)mxGetPr((plhs[0])))[2] = (real_T)(0.0);
        ((real_T *)mxGetPr((plhs[0])))[3] = (real_T)(0.0);
      }
    } else if (!strcmp(commandName,"target")) {
      ((real_T *)mxGetPr((plhs[0])))[0] = (real_T)(3267489760U);
      ((real_T *)mxGetPr((plhs[0])))[1] = (real_T)(3330274261U);
      ((real_T *)mxGetPr((plhs[0])))[2] = (real_T)(2714568270U);
      ((real_T *)mxGetPr((plhs[0])))[3] = (real_T)(344490432U);
    } else {
      return 0;
    }
  } else {
    ((real_T *)mxGetPr((plhs[0])))[0] = (real_T)(844232944U);
    ((real_T *)mxGetPr((plhs[0])))[1] = (real_T)(3336744916U);
    ((real_T *)mxGetPr((plhs[0])))[2] = (real_T)(1039531352U);
    ((real_T *)mxGetPr((plhs[0])))[3] = (real_T)(2597316912U);
  }

  return 1;

#else

  return 0;

#endif

}

unsigned int sf_LessonIV_autoinheritance_info( int nlhs, mxArray * plhs[], int
  nrhs, const mxArray * prhs[] )
{

#ifdef MATLAB_MEX_FILE

  char commandName[32];
  char aiChksum[64];
  if (nrhs<3 || !mxIsChar(prhs[0]) )
    return 0;

  /* Possible call to get the autoinheritance_info */
  mxGetString(prhs[0], commandName,sizeof(commandName)/sizeof(char));
  commandName[(sizeof(commandName)/sizeof(char)-1)] = '\0';
  if (strcmp(commandName,"get_autoinheritance_info"))
    return 0;
  mxGetString(prhs[2], aiChksum,sizeof(aiChksum)/sizeof(char));
  aiChksum[(sizeof(aiChksum)/sizeof(char)-1)] = '\0';

  {
    unsigned int chartFileNumber;
    chartFileNumber = (unsigned int)mxGetScalar(prhs[1]);
    switch (chartFileNumber) {
     case 4:
      {
        if (strcmp(aiChksum, "IsrQhqnsMKN8DLVwiLUWNB") == 0) {
          extern mxArray *sf_c4_LessonIV_get_autoinheritance_info(void);
          plhs[0] = sf_c4_LessonIV_get_autoinheritance_info();
          break;
        }

        plhs[0] = mxCreateDoubleMatrix(0,0,mxREAL);
        break;
      }

     case 7:
      {
        if (strcmp(aiChksum, "VJeQUUJTCELLRJR78QQIGF") == 0) {
          extern mxArray *sf_c7_LessonIV_get_autoinheritance_info(void);
          plhs[0] = sf_c7_LessonIV_get_autoinheritance_info();
          break;
        }

        plhs[0] = mxCreateDoubleMatrix(0,0,mxREAL);
        break;
      }

     case 23:
      {
        if (strcmp(aiChksum, "VJeQUUJTCELLRJR78QQIGF") == 0) {
          extern mxArray *sf_c23_LessonIV_get_autoinheritance_info(void);
          plhs[0] = sf_c23_LessonIV_get_autoinheritance_info();
          break;
        }

        plhs[0] = mxCreateDoubleMatrix(0,0,mxREAL);
        break;
      }

     case 32:
      {
        if (strcmp(aiChksum, "VJeQUUJTCELLRJR78QQIGF") == 0) {
          extern mxArray *sf_c32_LessonIV_get_autoinheritance_info(void);
          plhs[0] = sf_c32_LessonIV_get_autoinheritance_info();
          break;
        }

        plhs[0] = mxCreateDoubleMatrix(0,0,mxREAL);
        break;
      }

     case 43:
      {
        if (strcmp(aiChksum, "VJeQUUJTCELLRJR78QQIGF") == 0) {
          extern mxArray *sf_c43_LessonIV_get_autoinheritance_info(void);
          plhs[0] = sf_c43_LessonIV_get_autoinheritance_info();
          break;
        }

        plhs[0] = mxCreateDoubleMatrix(0,0,mxREAL);
        break;
      }

     case 53:
      {
        if (strcmp(aiChksum, "VJeQUUJTCELLRJR78QQIGF") == 0) {
          extern mxArray *sf_c53_LessonIV_get_autoinheritance_info(void);
          plhs[0] = sf_c53_LessonIV_get_autoinheritance_info();
          break;
        }

        plhs[0] = mxCreateDoubleMatrix(0,0,mxREAL);
        break;
      }

     default:
      plhs[0] = mxCreateDoubleMatrix(0,0,mxREAL);
    }
  }

  return 1;

#else

  return 0;

#endif

}

unsigned int sf_LessonIV_get_eml_resolved_functions_info( int nlhs, mxArray *
  plhs[], int nrhs, const mxArray * prhs[] )
{

#ifdef MATLAB_MEX_FILE

  char commandName[64];
  if (nrhs<2 || !mxIsChar(prhs[0]))
    return 0;

  /* Possible call to get the get_eml_resolved_functions_info */
  mxGetString(prhs[0], commandName,sizeof(commandName)/sizeof(char));
  commandName[(sizeof(commandName)/sizeof(char)-1)] = '\0';
  if (strcmp(commandName,"get_eml_resolved_functions_info"))
    return 0;

  {
    unsigned int chartFileNumber;
    chartFileNumber = (unsigned int)mxGetScalar(prhs[1]);
    switch (chartFileNumber) {
     case 4:
      {
        extern const mxArray *sf_c4_LessonIV_get_eml_resolved_functions_info
          (void);
        mxArray *persistentMxArray = (mxArray *)
          sf_c4_LessonIV_get_eml_resolved_functions_info();
        plhs[0] = mxDuplicateArray(persistentMxArray);
        mxDestroyArray(persistentMxArray);
        break;
      }

     case 7:
      {
        extern const mxArray *sf_c7_LessonIV_get_eml_resolved_functions_info
          (void);
        mxArray *persistentMxArray = (mxArray *)
          sf_c7_LessonIV_get_eml_resolved_functions_info();
        plhs[0] = mxDuplicateArray(persistentMxArray);
        mxDestroyArray(persistentMxArray);
        break;
      }

     case 23:
      {
        extern const mxArray *sf_c23_LessonIV_get_eml_resolved_functions_info
          (void);
        mxArray *persistentMxArray = (mxArray *)
          sf_c23_LessonIV_get_eml_resolved_functions_info();
        plhs[0] = mxDuplicateArray(persistentMxArray);
        mxDestroyArray(persistentMxArray);
        break;
      }

     case 32:
      {
        extern const mxArray *sf_c32_LessonIV_get_eml_resolved_functions_info
          (void);
        mxArray *persistentMxArray = (mxArray *)
          sf_c32_LessonIV_get_eml_resolved_functions_info();
        plhs[0] = mxDuplicateArray(persistentMxArray);
        mxDestroyArray(persistentMxArray);
        break;
      }

     case 43:
      {
        extern const mxArray *sf_c43_LessonIV_get_eml_resolved_functions_info
          (void);
        mxArray *persistentMxArray = (mxArray *)
          sf_c43_LessonIV_get_eml_resolved_functions_info();
        plhs[0] = mxDuplicateArray(persistentMxArray);
        mxDestroyArray(persistentMxArray);
        break;
      }

     case 53:
      {
        extern const mxArray *sf_c53_LessonIV_get_eml_resolved_functions_info
          (void);
        mxArray *persistentMxArray = (mxArray *)
          sf_c53_LessonIV_get_eml_resolved_functions_info();
        plhs[0] = mxDuplicateArray(persistentMxArray);
        mxDestroyArray(persistentMxArray);
        break;
      }

     default:
      plhs[0] = mxCreateDoubleMatrix(0,0,mxREAL);
    }
  }

  return 1;

#else

  return 0;

#endif

}

unsigned int sf_LessonIV_third_party_uses_info( int nlhs, mxArray * plhs[], int
  nrhs, const mxArray * prhs[] )
{
  char commandName[64];
  char tpChksum[64];
  if (nrhs<3 || !mxIsChar(prhs[0]))
    return 0;

  /* Possible call to get the third_party_uses_info */
  mxGetString(prhs[0], commandName,sizeof(commandName)/sizeof(char));
  commandName[(sizeof(commandName)/sizeof(char)-1)] = '\0';
  mxGetString(prhs[2], tpChksum,sizeof(tpChksum)/sizeof(char));
  tpChksum[(sizeof(tpChksum)/sizeof(char)-1)] = '\0';
  if (strcmp(commandName,"get_third_party_uses_info"))
    return 0;

  {
    unsigned int chartFileNumber;
    chartFileNumber = (unsigned int)mxGetScalar(prhs[1]);
    switch (chartFileNumber) {
     case 4:
      {
        if (strcmp(tpChksum, "sSxMd1pgAWxKSAJmIx2Q9pE") == 0) {
          extern mxArray *sf_c4_LessonIV_third_party_uses_info(void);
          plhs[0] = sf_c4_LessonIV_third_party_uses_info();
          break;
        }
      }

     case 7:
      {
        if (strcmp(tpChksum, "sc3SGRGa1hsgKRF4Te44fRG") == 0) {
          extern mxArray *sf_c7_LessonIV_third_party_uses_info(void);
          plhs[0] = sf_c7_LessonIV_third_party_uses_info();
          break;
        }
      }

     case 23:
      {
        if (strcmp(tpChksum, "sc3SGRGa1hsgKRF4Te44fRG") == 0) {
          extern mxArray *sf_c23_LessonIV_third_party_uses_info(void);
          plhs[0] = sf_c23_LessonIV_third_party_uses_info();
          break;
        }
      }

     case 32:
      {
        if (strcmp(tpChksum, "sc3SGRGa1hsgKRF4Te44fRG") == 0) {
          extern mxArray *sf_c32_LessonIV_third_party_uses_info(void);
          plhs[0] = sf_c32_LessonIV_third_party_uses_info();
          break;
        }
      }

     case 43:
      {
        if (strcmp(tpChksum, "sc3SGRGa1hsgKRF4Te44fRG") == 0) {
          extern mxArray *sf_c43_LessonIV_third_party_uses_info(void);
          plhs[0] = sf_c43_LessonIV_third_party_uses_info();
          break;
        }
      }

     case 53:
      {
        if (strcmp(tpChksum, "sc3SGRGa1hsgKRF4Te44fRG") == 0) {
          extern mxArray *sf_c53_LessonIV_third_party_uses_info(void);
          plhs[0] = sf_c53_LessonIV_third_party_uses_info();
          break;
        }
      }

     default:
      plhs[0] = mxCreateDoubleMatrix(0,0,mxREAL);
    }
  }

  return 1;
}

unsigned int sf_LessonIV_jit_fallback_info( int nlhs, mxArray * plhs[], int nrhs,
  const mxArray * prhs[] )
{
  char commandName[64];
  char tpChksum[64];
  if (nrhs<3 || !mxIsChar(prhs[0]))
    return 0;

  /* Possible call to get the jit_fallback_info */
  mxGetString(prhs[0], commandName,sizeof(commandName)/sizeof(char));
  commandName[(sizeof(commandName)/sizeof(char)-1)] = '\0';
  mxGetString(prhs[2], tpChksum,sizeof(tpChksum)/sizeof(char));
  tpChksum[(sizeof(tpChksum)/sizeof(char)-1)] = '\0';
  if (strcmp(commandName,"get_jit_fallback_info"))
    return 0;

  {
    unsigned int chartFileNumber;
    chartFileNumber = (unsigned int)mxGetScalar(prhs[1]);
    switch (chartFileNumber) {
     case 4:
      {
        if (strcmp(tpChksum, "sSxMd1pgAWxKSAJmIx2Q9pE") == 0) {
          extern mxArray *sf_c4_LessonIV_jit_fallback_info(void);
          plhs[0] = sf_c4_LessonIV_jit_fallback_info();
          break;
        }
      }

     case 7:
      {
        if (strcmp(tpChksum, "sc3SGRGa1hsgKRF4Te44fRG") == 0) {
          extern mxArray *sf_c7_LessonIV_jit_fallback_info(void);
          plhs[0] = sf_c7_LessonIV_jit_fallback_info();
          break;
        }
      }

     case 23:
      {
        if (strcmp(tpChksum, "sc3SGRGa1hsgKRF4Te44fRG") == 0) {
          extern mxArray *sf_c23_LessonIV_jit_fallback_info(void);
          plhs[0] = sf_c23_LessonIV_jit_fallback_info();
          break;
        }
      }

     case 32:
      {
        if (strcmp(tpChksum, "sc3SGRGa1hsgKRF4Te44fRG") == 0) {
          extern mxArray *sf_c32_LessonIV_jit_fallback_info(void);
          plhs[0] = sf_c32_LessonIV_jit_fallback_info();
          break;
        }
      }

     case 43:
      {
        if (strcmp(tpChksum, "sc3SGRGa1hsgKRF4Te44fRG") == 0) {
          extern mxArray *sf_c43_LessonIV_jit_fallback_info(void);
          plhs[0] = sf_c43_LessonIV_jit_fallback_info();
          break;
        }
      }

     case 53:
      {
        if (strcmp(tpChksum, "sc3SGRGa1hsgKRF4Te44fRG") == 0) {
          extern mxArray *sf_c53_LessonIV_jit_fallback_info(void);
          plhs[0] = sf_c53_LessonIV_jit_fallback_info();
          break;
        }
      }

     default:
      plhs[0] = mxCreateDoubleMatrix(0,0,mxREAL);
    }
  }

  return 1;
}

unsigned int sf_LessonIV_updateBuildInfo_args_info( int nlhs, mxArray * plhs[],
  int nrhs, const mxArray * prhs[] )
{
  char commandName[64];
  char tpChksum[64];
  if (nrhs<3 || !mxIsChar(prhs[0]))
    return 0;

  /* Possible call to get the updateBuildInfo_args_info */
  mxGetString(prhs[0], commandName,sizeof(commandName)/sizeof(char));
  commandName[(sizeof(commandName)/sizeof(char)-1)] = '\0';
  mxGetString(prhs[2], tpChksum,sizeof(tpChksum)/sizeof(char));
  tpChksum[(sizeof(tpChksum)/sizeof(char)-1)] = '\0';
  if (strcmp(commandName,"get_updateBuildInfo_args_info"))
    return 0;

  {
    unsigned int chartFileNumber;
    chartFileNumber = (unsigned int)mxGetScalar(prhs[1]);
    switch (chartFileNumber) {
     case 4:
      {
        if (strcmp(tpChksum, "sSxMd1pgAWxKSAJmIx2Q9pE") == 0) {
          extern mxArray *sf_c4_LessonIV_updateBuildInfo_args_info(void);
          plhs[0] = sf_c4_LessonIV_updateBuildInfo_args_info();
          break;
        }
      }

     case 7:
      {
        if (strcmp(tpChksum, "sc3SGRGa1hsgKRF4Te44fRG") == 0) {
          extern mxArray *sf_c7_LessonIV_updateBuildInfo_args_info(void);
          plhs[0] = sf_c7_LessonIV_updateBuildInfo_args_info();
          break;
        }
      }

     case 23:
      {
        if (strcmp(tpChksum, "sc3SGRGa1hsgKRF4Te44fRG") == 0) {
          extern mxArray *sf_c23_LessonIV_updateBuildInfo_args_info(void);
          plhs[0] = sf_c23_LessonIV_updateBuildInfo_args_info();
          break;
        }
      }

     case 32:
      {
        if (strcmp(tpChksum, "sc3SGRGa1hsgKRF4Te44fRG") == 0) {
          extern mxArray *sf_c32_LessonIV_updateBuildInfo_args_info(void);
          plhs[0] = sf_c32_LessonIV_updateBuildInfo_args_info();
          break;
        }
      }

     case 43:
      {
        if (strcmp(tpChksum, "sc3SGRGa1hsgKRF4Te44fRG") == 0) {
          extern mxArray *sf_c43_LessonIV_updateBuildInfo_args_info(void);
          plhs[0] = sf_c43_LessonIV_updateBuildInfo_args_info();
          break;
        }
      }

     case 53:
      {
        if (strcmp(tpChksum, "sc3SGRGa1hsgKRF4Te44fRG") == 0) {
          extern mxArray *sf_c53_LessonIV_updateBuildInfo_args_info(void);
          plhs[0] = sf_c53_LessonIV_updateBuildInfo_args_info();
          break;
        }
      }

     default:
      plhs[0] = mxCreateDoubleMatrix(0,0,mxREAL);
    }
  }

  return 1;
}

void sf_LessonIV_get_post_codegen_info( int nlhs, mxArray * plhs[], int nrhs,
  const mxArray * prhs[] )
{
  unsigned int chartFileNumber = (unsigned int) mxGetScalar(prhs[0]);
  char tpChksum[64];
  mxGetString(prhs[1], tpChksum,sizeof(tpChksum)/sizeof(char));
  tpChksum[(sizeof(tpChksum)/sizeof(char)-1)] = '\0';
  switch (chartFileNumber) {
   case 4:
    {
      if (strcmp(tpChksum, "sSxMd1pgAWxKSAJmIx2Q9pE") == 0) {
        extern mxArray *sf_c4_LessonIV_get_post_codegen_info(void);
        plhs[0] = sf_c4_LessonIV_get_post_codegen_info();
        return;
      }
    }
    break;

   case 7:
    {
      if (strcmp(tpChksum, "sc3SGRGa1hsgKRF4Te44fRG") == 0) {
        extern mxArray *sf_c7_LessonIV_get_post_codegen_info(void);
        plhs[0] = sf_c7_LessonIV_get_post_codegen_info();
        return;
      }
    }
    break;

   case 23:
    {
      if (strcmp(tpChksum, "sc3SGRGa1hsgKRF4Te44fRG") == 0) {
        extern mxArray *sf_c23_LessonIV_get_post_codegen_info(void);
        plhs[0] = sf_c23_LessonIV_get_post_codegen_info();
        return;
      }
    }
    break;

   case 32:
    {
      if (strcmp(tpChksum, "sc3SGRGa1hsgKRF4Te44fRG") == 0) {
        extern mxArray *sf_c32_LessonIV_get_post_codegen_info(void);
        plhs[0] = sf_c32_LessonIV_get_post_codegen_info();
        return;
      }
    }
    break;

   case 43:
    {
      if (strcmp(tpChksum, "sc3SGRGa1hsgKRF4Te44fRG") == 0) {
        extern mxArray *sf_c43_LessonIV_get_post_codegen_info(void);
        plhs[0] = sf_c43_LessonIV_get_post_codegen_info();
        return;
      }
    }
    break;

   case 53:
    {
      if (strcmp(tpChksum, "sc3SGRGa1hsgKRF4Te44fRG") == 0) {
        extern mxArray *sf_c53_LessonIV_get_post_codegen_info(void);
        plhs[0] = sf_c53_LessonIV_get_post_codegen_info();
        return;
      }
    }
    break;

   default:
    break;
  }

  plhs[0] = mxCreateDoubleMatrix(0,0,mxREAL);
}

void LessonIV_debug_initialize(struct SfDebugInstanceStruct* debugInstance)
{
  _LessonIVMachineNumber_ = sf_debug_initialize_machine(debugInstance,"LessonIV",
    "sfun",0,45,0,0,0);
  sf_debug_set_machine_event_thresholds(debugInstance,_LessonIVMachineNumber_,0,
    0);
  sf_debug_set_machine_data_thresholds(debugInstance,_LessonIVMachineNumber_,0);
}

void LessonIV_register_exported_symbols(SimStruct* S)
{
}

static mxArray* sRtwOptimizationInfoStruct= NULL;
mxArray* load_LessonIV_optimization_info(void)
{
  if (sRtwOptimizationInfoStruct==NULL) {
    sRtwOptimizationInfoStruct = sf_load_rtw_optimization_info("LessonIV",
      "LessonIV");
    mexMakeArrayPersistent(sRtwOptimizationInfoStruct);
  }

  return(sRtwOptimizationInfoStruct);
}

void unload_LessonIV_optimization_info(void)
{
  if (sRtwOptimizationInfoStruct!=NULL) {
    mxDestroyArray(sRtwOptimizationInfoStruct);
    sRtwOptimizationInfoStruct = NULL;
  }
}

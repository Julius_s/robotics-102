/* Include files */

#include <stddef.h>
#include "blas.h"
#include "LessonIV_sfun.h"
#include "c4_LessonIV.h"
#include <string.h>
#define CHARTINSTANCE_CHARTNUMBER      (chartInstance->chartNumber)
#define CHARTINSTANCE_INSTANCENUMBER   (chartInstance->instanceNumber)
#include "LessonIV_sfun_debug_macros.h"
#define _SF_MEX_LISTEN_FOR_CTRL_C(S)   sf_mex_listen_for_ctrl_c_with_debugger(S, sfGlobalDebugInstanceStruct);

static void chart_debug_initialization(SimStruct *S, unsigned int
  fullDebuggerInitialization);
static void chart_debug_initialize_data_addresses(SimStruct *S);
static const mxArray* sf_opaque_get_hover_data_for_msg(void *chartInstance,
  int32_T msgSSID);

/* Type Definitions */

/* Named Constants */
#define CALL_EVENT                     (-1)

/* Variable Declarations */

/* Variable Definitions */
static real_T _sfTime_;
static const char * c4_debug_family_names[7] = { "ii", "nargin", "nargout",
  "players", "gameOn", "ball", "infoToTransmit" };

/* Function Declarations */
static void initialize_c4_LessonIV(SFc4_LessonIVInstanceStruct *chartInstance);
static void initialize_params_c4_LessonIV(SFc4_LessonIVInstanceStruct
  *chartInstance);
static void enable_c4_LessonIV(SFc4_LessonIVInstanceStruct *chartInstance);
static void disable_c4_LessonIV(SFc4_LessonIVInstanceStruct *chartInstance);
static void c4_update_debugger_state_c4_LessonIV(SFc4_LessonIVInstanceStruct
  *chartInstance);
static const mxArray *get_sim_state_c4_LessonIV(SFc4_LessonIVInstanceStruct
  *chartInstance);
static void set_sim_state_c4_LessonIV(SFc4_LessonIVInstanceStruct *chartInstance,
  const mxArray *c4_st);
static void finalize_c4_LessonIV(SFc4_LessonIVInstanceStruct *chartInstance);
static void sf_gateway_c4_LessonIV(SFc4_LessonIVInstanceStruct *chartInstance);
static void mdl_start_c4_LessonIV(SFc4_LessonIVInstanceStruct *chartInstance);
static void initSimStructsc4_LessonIV(SFc4_LessonIVInstanceStruct *chartInstance);
static void init_script_number_translation(uint32_T c4_machineNumber, uint32_T
  c4_chartNumber, uint32_T c4_instanceNumber);
static const mxArray *c4_sf_marshallOut(void *chartInstanceVoid, uint8_T
  c4_inData_data[], int32_T c4_inData_sizes[2]);
static void c4_emlrt_marshallIn(SFc4_LessonIVInstanceStruct *chartInstance,
  const mxArray *c4_infoToTransmit, const char_T *c4_identifier, uint8_T
  c4_y_data[], int32_T c4_y_sizes[2]);
static void c4_b_emlrt_marshallIn(SFc4_LessonIVInstanceStruct *chartInstance,
  const mxArray *c4_u, const emlrtMsgIdentifier *c4_parentId, uint8_T c4_y_data[],
  int32_T c4_y_sizes[2]);
static void c4_sf_marshallIn(void *chartInstanceVoid, const mxArray
  *c4_mxArrayInData, const char_T *c4_varName, uint8_T c4_outData_data[],
  int32_T c4_outData_sizes[2]);
static const mxArray *c4_b_sf_marshallOut(void *chartInstanceVoid, void
  *c4_inData);
static const mxArray *c4_c_sf_marshallOut(void *chartInstanceVoid, void
  *c4_inData);
static const mxArray *c4_d_sf_marshallOut(void *chartInstanceVoid, void
  *c4_inData);
static const mxArray *c4_e_sf_marshallOut(void *chartInstanceVoid, void
  *c4_inData);
static real_T c4_c_emlrt_marshallIn(SFc4_LessonIVInstanceStruct *chartInstance,
  const mxArray *c4_u, const emlrtMsgIdentifier *c4_parentId);
static void c4_b_sf_marshallIn(void *chartInstanceVoid, const mxArray
  *c4_mxArrayInData, const char_T *c4_varName, void *c4_outData);
static uint8_T c4_typecast(SFc4_LessonIVInstanceStruct *chartInstance, int8_T
  c4_x);
static const mxArray *c4_f_sf_marshallOut(void *chartInstanceVoid, void
  *c4_inData);
static int32_T c4_d_emlrt_marshallIn(SFc4_LessonIVInstanceStruct *chartInstance,
  const mxArray *c4_u, const emlrtMsgIdentifier *c4_parentId);
static void c4_c_sf_marshallIn(void *chartInstanceVoid, const mxArray
  *c4_mxArrayInData, const char_T *c4_varName, void *c4_outData);
static const mxArray *c4_players_bus_io(void *chartInstanceVoid, void *c4_pData);
static const mxArray *c4_ball_bus_io(void *chartInstanceVoid, void *c4_pData);
static uint8_T c4_e_emlrt_marshallIn(SFc4_LessonIVInstanceStruct *chartInstance,
  const mxArray *c4_b_is_active_c4_LessonIV, const char_T *c4_identifier);
static uint8_T c4_f_emlrt_marshallIn(SFc4_LessonIVInstanceStruct *chartInstance,
  const mxArray *c4_u, const emlrtMsgIdentifier *c4_parentId);
static void init_dsm_address_info(SFc4_LessonIVInstanceStruct *chartInstance);
static void init_simulink_io_address(SFc4_LessonIVInstanceStruct *chartInstance);

/* Function Definitions */
static void initialize_c4_LessonIV(SFc4_LessonIVInstanceStruct *chartInstance)
{
  if (sf_is_first_init_cond(chartInstance->S)) {
    initSimStructsc4_LessonIV(chartInstance);
    chart_debug_initialize_data_addresses(chartInstance->S);
  }

  chartInstance->c4_sfEvent = CALL_EVENT;
  _sfTime_ = sf_get_time(chartInstance->S);
  chartInstance->c4_is_active_c4_LessonIV = 0U;
}

static void initialize_params_c4_LessonIV(SFc4_LessonIVInstanceStruct
  *chartInstance)
{
  (void)chartInstance;
}

static void enable_c4_LessonIV(SFc4_LessonIVInstanceStruct *chartInstance)
{
  _sfTime_ = sf_get_time(chartInstance->S);
}

static void disable_c4_LessonIV(SFc4_LessonIVInstanceStruct *chartInstance)
{
  _sfTime_ = sf_get_time(chartInstance->S);
}

static void c4_update_debugger_state_c4_LessonIV(SFc4_LessonIVInstanceStruct
  *chartInstance)
{
  (void)chartInstance;
}

static const mxArray *get_sim_state_c4_LessonIV(SFc4_LessonIVInstanceStruct
  *chartInstance)
{
  const mxArray *c4_st;
  const mxArray *c4_y = NULL;
  const mxArray *c4_b_y = NULL;
  uint8_T c4_hoistedGlobal;
  uint8_T c4_u;
  const mxArray *c4_c_y = NULL;
  c4_st = NULL;
  c4_st = NULL;
  c4_y = NULL;
  sf_mex_assign(&c4_y, sf_mex_createcellmatrix(2, 1), false);
  c4_b_y = NULL;
  sf_mex_assign(&c4_b_y, sf_mex_create("y",
    *chartInstance->c4_infoToTransmit_data, 3, 0U, 1U, 0U, 2,
    (*chartInstance->c4_infoToTransmit_sizes)[0],
    (*chartInstance->c4_infoToTransmit_sizes)[1]), false);
  sf_mex_setcell(c4_y, 0, c4_b_y);
  c4_hoistedGlobal = chartInstance->c4_is_active_c4_LessonIV;
  c4_u = c4_hoistedGlobal;
  c4_c_y = NULL;
  sf_mex_assign(&c4_c_y, sf_mex_create("y", &c4_u, 3, 0U, 0U, 0U, 0), false);
  sf_mex_setcell(c4_y, 1, c4_c_y);
  sf_mex_assign(&c4_st, c4_y, false);
  return c4_st;
}

static void set_sim_state_c4_LessonIV(SFc4_LessonIVInstanceStruct *chartInstance,
  const mxArray *c4_st)
{
  const mxArray *c4_u;
  int32_T c4_tmp_sizes[2];
  uint8_T c4_tmp_data[64];
  int32_T c4_i0;
  int32_T c4_i1;
  int32_T c4_loop_ub;
  int32_T c4_i2;
  chartInstance->c4_doneDoubleBufferReInit = true;
  c4_u = sf_mex_dup(c4_st);
  c4_emlrt_marshallIn(chartInstance, sf_mex_dup(sf_mex_getcell("infoToTransmit",
    c4_u, 0)), "infoToTransmit", c4_tmp_data, c4_tmp_sizes);
  ssSetCurrentOutputPortDimensions_wrapper(chartInstance->S, 1, 0, 1);
  ssSetCurrentOutputPortDimensions_wrapper(chartInstance->S, 1, 1, c4_tmp_sizes
    [1]);
  c4_i0 = (*chartInstance->c4_infoToTransmit_sizes)[0];
  c4_i1 = (*chartInstance->c4_infoToTransmit_sizes)[1];
  c4_loop_ub = c4_tmp_sizes[0] * c4_tmp_sizes[1] - 1;
  for (c4_i2 = 0; c4_i2 <= c4_loop_ub; c4_i2++) {
    (*chartInstance->c4_infoToTransmit_data)[c4_i2] = c4_tmp_data[c4_i2];
  }

  chartInstance->c4_is_active_c4_LessonIV = c4_e_emlrt_marshallIn(chartInstance,
    sf_mex_dup(sf_mex_getcell("is_active_c4_LessonIV", c4_u, 1)),
    "is_active_c4_LessonIV");
  sf_mex_destroy(&c4_u);
  c4_update_debugger_state_c4_LessonIV(chartInstance);
  sf_mex_destroy(&c4_st);
}

static void finalize_c4_LessonIV(SFc4_LessonIVInstanceStruct *chartInstance)
{
  (void)chartInstance;
}

static void sf_gateway_c4_LessonIV(SFc4_LessonIVInstanceStruct *chartInstance)
{
  uint8_T c4_hoistedGlobal;
  int32_T c4_i3;
  c4_Player c4_b_players[6];
  uint8_T c4_b_gameOn;
  c4_Ball c4_b_ball;
  uint32_T c4_debug_family_var_map[7];
  real_T c4_ii;
  real_T c4_nargin = 3.0;
  real_T c4_nargout = 1.0;
  int32_T c4_b_infoToTransmit_sizes[2];
  uint8_T c4_b_infoToTransmit_data[64];
  int32_T c4_infoToTransmit;
  int32_T c4_b_infoToTransmit;
  int32_T c4_i4;
  static uint8_T c4_uv0[4] = { 104U, 101U, 97U, 100U };

  uint8_T c4_u0;
  uint8_T c4_u1;
  int32_T c4_tmp_sizes[2];
  int32_T c4_loop_ub;
  int32_T c4_i5;
  uint8_T c4_tmp_data[7];
  int32_T c4_c_infoToTransmit;
  int32_T c4_d_infoToTransmit;
  int32_T c4_i6;
  int32_T c4_b_ii;
  int16_T c4_x;
  uint8_T c4_y[2];
  uint8_T c4_u2;
  uint8_T c4_u3;
  int32_T c4_b_tmp_sizes[2];
  int32_T c4_b_loop_ub;
  int32_T c4_i7;
  uint8_T c4_b_tmp_data[68];
  int32_T c4_i8;
  int32_T c4_e_infoToTransmit;
  int32_T c4_f_infoToTransmit;
  int32_T c4_c_loop_ub;
  int32_T c4_i9;
  int32_T c4_i10;
  int32_T c4_i11;
  int32_T c4_d_loop_ub;
  int32_T c4_i12;
  int32_T c4_e_loop_ub;
  int32_T c4_i13;
  _SFD_SYMBOL_SCOPE_PUSH(0U, 0U);
  _sfTime_ = sf_get_time(chartInstance->S);
  _SFD_CC_CALL(CHART_ENTER_SFUNCTION_TAG, 3U, chartInstance->c4_sfEvent);
  _SFD_DATA_RANGE_CHECK((real_T)*chartInstance->c4_gameOn, 1U, 1U, 0U,
                        chartInstance->c4_sfEvent, false);
  chartInstance->c4_sfEvent = CALL_EVENT;
  _SFD_CC_CALL(CHART_ENTER_DURING_FUNCTION_TAG, 3U, chartInstance->c4_sfEvent);
  c4_hoistedGlobal = *chartInstance->c4_gameOn;
  for (c4_i3 = 0; c4_i3 < 6; c4_i3++) {
    c4_b_players[c4_i3].x = *(int8_T *)&((char_T *)(c4_Player *)&((char_T *)
      chartInstance->c4_players)[8 * c4_i3])[0];
    c4_b_players[c4_i3].y = *(int8_T *)&((char_T *)(c4_Player *)&((char_T *)
      chartInstance->c4_players)[8 * c4_i3])[1];
    c4_b_players[c4_i3].orientation = *(int16_T *)&((char_T *)(c4_Player *)
      &((char_T *)chartInstance->c4_players)[8 * c4_i3])[2];
    c4_b_players[c4_i3].color = *(uint8_T *)&((char_T *)(c4_Player *)&((char_T *)
      chartInstance->c4_players)[8 * c4_i3])[4];
    c4_b_players[c4_i3].position = *(uint8_T *)&((char_T *)(c4_Player *)
      &((char_T *)chartInstance->c4_players)[8 * c4_i3])[5];
    c4_b_players[c4_i3].valid = *(uint8_T *)&((char_T *)(c4_Player *)&((char_T *)
      chartInstance->c4_players)[8 * c4_i3])[6];
  }

  c4_b_gameOn = c4_hoistedGlobal;
  c4_b_ball.x = *(int8_T *)&((char_T *)chartInstance->c4_ball)[0];
  c4_b_ball.y = *(int8_T *)&((char_T *)chartInstance->c4_ball)[1];
  c4_b_ball.valid = *(uint8_T *)&((char_T *)chartInstance->c4_ball)[2];
  _SFD_SYMBOL_SCOPE_PUSH_EML(0U, 7U, 7U, c4_debug_family_names,
    c4_debug_family_var_map);
  _SFD_SYMBOL_SCOPE_ADD_EML_IMPORTABLE(&c4_ii, 0U, c4_e_sf_marshallOut,
    c4_b_sf_marshallIn);
  _SFD_SYMBOL_SCOPE_ADD_EML_IMPORTABLE(&c4_nargin, 1U, c4_e_sf_marshallOut,
    c4_b_sf_marshallIn);
  _SFD_SYMBOL_SCOPE_ADD_EML_IMPORTABLE(&c4_nargout, 2U, c4_e_sf_marshallOut,
    c4_b_sf_marshallIn);
  _SFD_SYMBOL_SCOPE_ADD_EML(c4_b_players, 3U, c4_d_sf_marshallOut);
  _SFD_SYMBOL_SCOPE_ADD_EML(&c4_b_gameOn, 4U, c4_c_sf_marshallOut);
  _SFD_SYMBOL_SCOPE_ADD_EML(&c4_b_ball, 5U, c4_b_sf_marshallOut);
  _SFD_SYMBOL_SCOPE_ADD_EML_DYN_IMPORTABLE(c4_b_infoToTransmit_data, (const
    int32_T *)&c4_b_infoToTransmit_sizes, NULL, 0, 6, (void *)c4_sf_marshallOut,
    (void *)c4_sf_marshallIn);
  CV_EML_FCN(0, 0);
  _SFD_EML_CALL(0U, chartInstance->c4_sfEvent, 2);
  _SFD_DIM_SIZE_GEQ_CHECK(64, 4, 2);
  c4_b_infoToTransmit_sizes[0] = 1;
  c4_b_infoToTransmit_sizes[1] = 4;
  c4_infoToTransmit = c4_b_infoToTransmit_sizes[0];
  c4_b_infoToTransmit = c4_b_infoToTransmit_sizes[1];
  for (c4_i4 = 0; c4_i4 < 4; c4_i4++) {
    c4_b_infoToTransmit_data[c4_i4] = c4_uv0[c4_i4];
  }

  _SFD_EML_CALL(0U, chartInstance->c4_sfEvent, 3);
  _SFD_EML_CALL(0U, chartInstance->c4_sfEvent, 4);
  c4_u0 = c4_typecast(chartInstance, c4_b_ball.x);
  c4_u1 = c4_typecast(chartInstance, c4_b_ball.y);
  c4_tmp_sizes[0] = 1;
  c4_tmp_sizes[1] = c4_b_infoToTransmit_sizes[1] + 3;
  c4_loop_ub = c4_b_infoToTransmit_sizes[1] - 1;
  for (c4_i5 = 0; c4_i5 <= c4_loop_ub; c4_i5++) {
    c4_tmp_data[c4_tmp_sizes[0] * c4_i5] =
      c4_b_infoToTransmit_data[c4_b_infoToTransmit_sizes[0] * c4_i5];
  }

  c4_tmp_data[c4_tmp_sizes[0] * c4_b_infoToTransmit_sizes[1]] = c4_b_gameOn;
  c4_tmp_data[c4_tmp_sizes[0] * (c4_b_infoToTransmit_sizes[1] + 1)] = c4_u0;
  c4_tmp_data[c4_tmp_sizes[0] * (c4_b_infoToTransmit_sizes[1] + 2)] = c4_u1;
  _SFD_DIM_SIZE_GEQ_CHECK(64, 7, 2);
  c4_b_infoToTransmit_sizes[0] = 1;
  c4_b_infoToTransmit_sizes[1] = 7;
  c4_c_infoToTransmit = c4_b_infoToTransmit_sizes[0];
  c4_d_infoToTransmit = c4_b_infoToTransmit_sizes[1];
  for (c4_i6 = 0; c4_i6 < 7; c4_i6++) {
    c4_b_infoToTransmit_data[c4_i6] = c4_tmp_data[c4_i6];
  }

  _SFD_EML_CALL(0U, chartInstance->c4_sfEvent, 9);
  c4_ii = 1.0;
  c4_b_ii = 0;
  while (c4_b_ii < 6) {
    c4_ii = 1.0 + (real_T)c4_b_ii;
    CV_EML_FOR(0, 1, 0, 1);
    _SFD_EML_CALL(0U, chartInstance->c4_sfEvent, 10);
    c4_x = c4_b_players[(int32_T)c4_ii - 1].orientation;
    memcpy(&c4_y[0], &c4_x, (size_t)2 * sizeof(uint8_T));
    c4_u2 = c4_typecast(chartInstance, c4_b_players[(int32_T)c4_ii - 1].x);
    c4_u3 = c4_typecast(chartInstance, c4_b_players[(int32_T)c4_ii - 1].y);
    c4_b_tmp_sizes[0] = 1;
    c4_b_tmp_sizes[1] = c4_b_infoToTransmit_sizes[1] + 4;
    c4_b_loop_ub = c4_b_infoToTransmit_sizes[1] - 1;
    for (c4_i7 = 0; c4_i7 <= c4_b_loop_ub; c4_i7++) {
      c4_b_tmp_data[c4_b_tmp_sizes[0] * c4_i7] =
        c4_b_infoToTransmit_data[c4_b_infoToTransmit_sizes[0] * c4_i7];
    }

    c4_b_tmp_data[c4_b_tmp_sizes[0] * c4_b_infoToTransmit_sizes[1]] = c4_u2;
    c4_b_tmp_data[c4_b_tmp_sizes[0] * (c4_b_infoToTransmit_sizes[1] + 1)] =
      c4_u3;
    for (c4_i8 = 0; c4_i8 < 2; c4_i8++) {
      c4_b_tmp_data[c4_b_tmp_sizes[0] * ((c4_i8 + c4_b_infoToTransmit_sizes[1])
        + 2)] = c4_y[c4_i8];
    }

    _SFD_DIM_SIZE_GEQ_CHECK(64, c4_b_tmp_sizes[1], 2);
    c4_b_infoToTransmit_sizes[0] = 1;
    c4_b_infoToTransmit_sizes[1] = c4_b_tmp_sizes[1];
    c4_e_infoToTransmit = c4_b_infoToTransmit_sizes[0];
    c4_f_infoToTransmit = c4_b_infoToTransmit_sizes[1];
    c4_c_loop_ub = c4_b_tmp_sizes[0] * c4_b_tmp_sizes[1] - 1;
    for (c4_i9 = 0; c4_i9 <= c4_c_loop_ub; c4_i9++) {
      c4_b_infoToTransmit_data[c4_i9] = c4_b_tmp_data[c4_i9];
    }

    c4_b_ii++;
    _SF_MEX_LISTEN_FOR_CTRL_C(chartInstance->S);
  }

  CV_EML_FOR(0, 1, 0, 0);
  _SFD_EML_CALL(0U, chartInstance->c4_sfEvent, -10);
  _SFD_SYMBOL_SCOPE_POP();
  ssSetCurrentOutputPortDimensions_wrapper(chartInstance->S, 1, 0, 1);
  ssSetCurrentOutputPortDimensions_wrapper(chartInstance->S, 1, 1,
    c4_b_infoToTransmit_sizes[1]);
  c4_i10 = (*chartInstance->c4_infoToTransmit_sizes)[0];
  c4_i11 = (*chartInstance->c4_infoToTransmit_sizes)[1];
  c4_d_loop_ub = c4_b_infoToTransmit_sizes[0] * c4_b_infoToTransmit_sizes[1] - 1;
  for (c4_i12 = 0; c4_i12 <= c4_d_loop_ub; c4_i12++) {
    (*chartInstance->c4_infoToTransmit_data)[c4_i12] =
      c4_b_infoToTransmit_data[c4_i12];
  }

  _SFD_CC_CALL(EXIT_OUT_OF_FUNCTION_TAG, 3U, chartInstance->c4_sfEvent);
  _SFD_SYMBOL_SCOPE_POP();
  _SFD_CHECK_FOR_STATE_INCONSISTENCY(_LessonIVMachineNumber_,
    chartInstance->chartNumber, chartInstance->instanceNumber);
  c4_e_loop_ub = (*chartInstance->c4_infoToTransmit_sizes)[0] *
    (*chartInstance->c4_infoToTransmit_sizes)[1] - 1;
  for (c4_i13 = 0; c4_i13 <= c4_e_loop_ub; c4_i13++) {
    _SFD_DATA_RANGE_CHECK((real_T)(*chartInstance->c4_infoToTransmit_data)
                          [c4_i13], 3U, 1U, 0U, chartInstance->c4_sfEvent, false);
  }
}

static void mdl_start_c4_LessonIV(SFc4_LessonIVInstanceStruct *chartInstance)
{
  (void)chartInstance;
}

static void initSimStructsc4_LessonIV(SFc4_LessonIVInstanceStruct *chartInstance)
{
  (void)chartInstance;
}

static void init_script_number_translation(uint32_T c4_machineNumber, uint32_T
  c4_chartNumber, uint32_T c4_instanceNumber)
{
  (void)c4_machineNumber;
  (void)c4_chartNumber;
  (void)c4_instanceNumber;
}

static const mxArray *c4_sf_marshallOut(void *chartInstanceVoid, uint8_T
  c4_inData_data[], int32_T c4_inData_sizes[2])
{
  const mxArray *c4_mxArrayOutData = NULL;
  int32_T c4_u_sizes[2];
  int32_T c4_u;
  int32_T c4_b_u;
  int32_T c4_loop_ub;
  int32_T c4_i14;
  uint8_T c4_u_data[64];
  const mxArray *c4_y = NULL;
  SFc4_LessonIVInstanceStruct *chartInstance;
  chartInstance = (SFc4_LessonIVInstanceStruct *)chartInstanceVoid;
  c4_mxArrayOutData = NULL;
  c4_u_sizes[0] = 1;
  c4_u_sizes[1] = c4_inData_sizes[1];
  c4_u = c4_u_sizes[0];
  c4_b_u = c4_u_sizes[1];
  c4_loop_ub = c4_inData_sizes[0] * c4_inData_sizes[1] - 1;
  for (c4_i14 = 0; c4_i14 <= c4_loop_ub; c4_i14++) {
    c4_u_data[c4_i14] = c4_inData_data[c4_i14];
  }

  c4_y = NULL;
  sf_mex_assign(&c4_y, sf_mex_create("y", c4_u_data, 3, 0U, 1U, 0U, 2,
    c4_u_sizes[0], c4_u_sizes[1]), false);
  sf_mex_assign(&c4_mxArrayOutData, c4_y, false);
  return c4_mxArrayOutData;
}

static void c4_emlrt_marshallIn(SFc4_LessonIVInstanceStruct *chartInstance,
  const mxArray *c4_infoToTransmit, const char_T *c4_identifier, uint8_T
  c4_y_data[], int32_T c4_y_sizes[2])
{
  emlrtMsgIdentifier c4_thisId;
  c4_thisId.fIdentifier = c4_identifier;
  c4_thisId.fParent = NULL;
  c4_thisId.bParentIsCell = false;
  c4_b_emlrt_marshallIn(chartInstance, sf_mex_dup(c4_infoToTransmit), &c4_thisId,
                        c4_y_data, c4_y_sizes);
  sf_mex_destroy(&c4_infoToTransmit);
}

static void c4_b_emlrt_marshallIn(SFc4_LessonIVInstanceStruct *chartInstance,
  const mxArray *c4_u, const emlrtMsgIdentifier *c4_parentId, uint8_T c4_y_data[],
  int32_T c4_y_sizes[2])
{
  int32_T c4_i15;
  uint32_T c4_uv1[2];
  int32_T c4_i16;
  static boolean_T c4_bv0[2] = { false, true };

  boolean_T c4_bv1[2];
  int32_T c4_tmp_sizes[2];
  uint8_T c4_tmp_data[64];
  int32_T c4_y;
  int32_T c4_b_y;
  int32_T c4_loop_ub;
  int32_T c4_i17;
  (void)chartInstance;
  for (c4_i15 = 0; c4_i15 < 2; c4_i15++) {
    c4_uv1[c4_i15] = 1U + 63U * (uint32_T)c4_i15;
  }

  for (c4_i16 = 0; c4_i16 < 2; c4_i16++) {
    c4_bv1[c4_i16] = c4_bv0[c4_i16];
  }

  sf_mex_import_vs(c4_parentId, sf_mex_dup(c4_u), c4_tmp_data, 1, 3, 0U, 1, 0U,
                   2, c4_bv1, c4_uv1, c4_tmp_sizes);
  c4_y_sizes[0] = 1;
  c4_y_sizes[1] = c4_tmp_sizes[1];
  c4_y = c4_y_sizes[0];
  c4_b_y = c4_y_sizes[1];
  c4_loop_ub = c4_tmp_sizes[0] * c4_tmp_sizes[1] - 1;
  for (c4_i17 = 0; c4_i17 <= c4_loop_ub; c4_i17++) {
    c4_y_data[c4_i17] = c4_tmp_data[c4_i17];
  }

  sf_mex_destroy(&c4_u);
}

static void c4_sf_marshallIn(void *chartInstanceVoid, const mxArray
  *c4_mxArrayInData, const char_T *c4_varName, uint8_T c4_outData_data[],
  int32_T c4_outData_sizes[2])
{
  const mxArray *c4_infoToTransmit;
  const char_T *c4_identifier;
  emlrtMsgIdentifier c4_thisId;
  int32_T c4_y_sizes[2];
  uint8_T c4_y_data[64];
  int32_T c4_loop_ub;
  int32_T c4_i18;
  SFc4_LessonIVInstanceStruct *chartInstance;
  chartInstance = (SFc4_LessonIVInstanceStruct *)chartInstanceVoid;
  c4_infoToTransmit = sf_mex_dup(c4_mxArrayInData);
  c4_identifier = c4_varName;
  c4_thisId.fIdentifier = c4_identifier;
  c4_thisId.fParent = NULL;
  c4_thisId.bParentIsCell = false;
  c4_b_emlrt_marshallIn(chartInstance, sf_mex_dup(c4_infoToTransmit), &c4_thisId,
                        c4_y_data, c4_y_sizes);
  sf_mex_destroy(&c4_infoToTransmit);
  c4_outData_sizes[0] = 1;
  c4_outData_sizes[1] = c4_y_sizes[1];
  c4_loop_ub = c4_y_sizes[1] - 1;
  for (c4_i18 = 0; c4_i18 <= c4_loop_ub; c4_i18++) {
    c4_outData_data[c4_outData_sizes[0] * c4_i18] = c4_y_data[c4_y_sizes[0] *
      c4_i18];
  }

  sf_mex_destroy(&c4_mxArrayInData);
}

static const mxArray *c4_b_sf_marshallOut(void *chartInstanceVoid, void
  *c4_inData)
{
  const mxArray *c4_mxArrayOutData;
  c4_Ball c4_u;
  const mxArray *c4_y = NULL;
  int8_T c4_b_u;
  const mxArray *c4_b_y = NULL;
  int8_T c4_c_u;
  const mxArray *c4_c_y = NULL;
  uint8_T c4_d_u;
  const mxArray *c4_d_y = NULL;
  SFc4_LessonIVInstanceStruct *chartInstance;
  chartInstance = (SFc4_LessonIVInstanceStruct *)chartInstanceVoid;
  c4_mxArrayOutData = NULL;
  c4_mxArrayOutData = NULL;
  c4_u = *(c4_Ball *)c4_inData;
  c4_y = NULL;
  sf_mex_assign(&c4_y, sf_mex_createstruct("structure", 2, 1, 1), false);
  c4_b_u = c4_u.x;
  c4_b_y = NULL;
  sf_mex_assign(&c4_b_y, sf_mex_create("y", &c4_b_u, 2, 0U, 0U, 0U, 0), false);
  sf_mex_addfield(c4_y, c4_b_y, "x", "x", 0);
  c4_c_u = c4_u.y;
  c4_c_y = NULL;
  sf_mex_assign(&c4_c_y, sf_mex_create("y", &c4_c_u, 2, 0U, 0U, 0U, 0), false);
  sf_mex_addfield(c4_y, c4_c_y, "y", "y", 0);
  c4_d_u = c4_u.valid;
  c4_d_y = NULL;
  sf_mex_assign(&c4_d_y, sf_mex_create("y", &c4_d_u, 3, 0U, 0U, 0U, 0), false);
  sf_mex_addfield(c4_y, c4_d_y, "valid", "valid", 0);
  sf_mex_assign(&c4_mxArrayOutData, c4_y, false);
  return c4_mxArrayOutData;
}

static const mxArray *c4_c_sf_marshallOut(void *chartInstanceVoid, void
  *c4_inData)
{
  const mxArray *c4_mxArrayOutData = NULL;
  uint8_T c4_u;
  const mxArray *c4_y = NULL;
  SFc4_LessonIVInstanceStruct *chartInstance;
  chartInstance = (SFc4_LessonIVInstanceStruct *)chartInstanceVoid;
  c4_mxArrayOutData = NULL;
  c4_u = *(uint8_T *)c4_inData;
  c4_y = NULL;
  sf_mex_assign(&c4_y, sf_mex_create("y", &c4_u, 3, 0U, 0U, 0U, 0), false);
  sf_mex_assign(&c4_mxArrayOutData, c4_y, false);
  return c4_mxArrayOutData;
}

static const mxArray *c4_d_sf_marshallOut(void *chartInstanceVoid, void
  *c4_inData)
{
  const mxArray *c4_mxArrayOutData;
  int32_T c4_i19;
  c4_Player c4_b_inData[6];
  int32_T c4_i20;
  c4_Player c4_u[6];
  const mxArray *c4_y = NULL;
  static int32_T c4_iv0[1] = { 6 };

  int32_T c4_iv1[1];
  int32_T c4_i21;
  const c4_Player *c4_r0;
  int8_T c4_b_u;
  const mxArray *c4_b_y = NULL;
  int8_T c4_c_u;
  const mxArray *c4_c_y = NULL;
  int16_T c4_d_u;
  const mxArray *c4_d_y = NULL;
  uint8_T c4_e_u;
  const mxArray *c4_e_y = NULL;
  uint8_T c4_f_u;
  const mxArray *c4_f_y = NULL;
  uint8_T c4_g_u;
  const mxArray *c4_g_y = NULL;
  SFc4_LessonIVInstanceStruct *chartInstance;
  chartInstance = (SFc4_LessonIVInstanceStruct *)chartInstanceVoid;
  c4_mxArrayOutData = NULL;
  c4_mxArrayOutData = NULL;
  for (c4_i19 = 0; c4_i19 < 6; c4_i19++) {
    c4_b_inData[c4_i19] = (*(c4_Player (*)[6])c4_inData)[c4_i19];
  }

  for (c4_i20 = 0; c4_i20 < 6; c4_i20++) {
    c4_u[c4_i20] = c4_b_inData[c4_i20];
  }

  c4_y = NULL;
  c4_iv1[0] = c4_iv0[0];
  sf_mex_assign(&c4_y, sf_mex_createstructarray("structure", 1, c4_iv1), false);
  for (c4_i21 = 0; c4_i21 < 6; c4_i21++) {
    c4_r0 = &c4_u[c4_i21];
    c4_b_u = c4_r0->x;
    c4_b_y = NULL;
    sf_mex_assign(&c4_b_y, sf_mex_create("y", &c4_b_u, 2, 0U, 0U, 0U, 0), false);
    sf_mex_addfield(c4_y, c4_b_y, "x", "x", c4_i21);
    c4_c_u = c4_r0->y;
    c4_c_y = NULL;
    sf_mex_assign(&c4_c_y, sf_mex_create("y", &c4_c_u, 2, 0U, 0U, 0U, 0), false);
    sf_mex_addfield(c4_y, c4_c_y, "y", "y", c4_i21);
    c4_d_u = c4_r0->orientation;
    c4_d_y = NULL;
    sf_mex_assign(&c4_d_y, sf_mex_create("y", &c4_d_u, 4, 0U, 0U, 0U, 0), false);
    sf_mex_addfield(c4_y, c4_d_y, "orientation", "orientation", c4_i21);
    c4_e_u = c4_r0->color;
    c4_e_y = NULL;
    sf_mex_assign(&c4_e_y, sf_mex_create("y", &c4_e_u, 3, 0U, 0U, 0U, 0), false);
    sf_mex_addfield(c4_y, c4_e_y, "color", "color", c4_i21);
    c4_f_u = c4_r0->position;
    c4_f_y = NULL;
    sf_mex_assign(&c4_f_y, sf_mex_create("y", &c4_f_u, 3, 0U, 0U, 0U, 0), false);
    sf_mex_addfield(c4_y, c4_f_y, "position", "position", c4_i21);
    c4_g_u = c4_r0->valid;
    c4_g_y = NULL;
    sf_mex_assign(&c4_g_y, sf_mex_create("y", &c4_g_u, 3, 0U, 0U, 0U, 0), false);
    sf_mex_addfield(c4_y, c4_g_y, "valid", "valid", c4_i21);
  }

  sf_mex_assign(&c4_mxArrayOutData, c4_y, false);
  return c4_mxArrayOutData;
}

static const mxArray *c4_e_sf_marshallOut(void *chartInstanceVoid, void
  *c4_inData)
{
  const mxArray *c4_mxArrayOutData = NULL;
  real_T c4_u;
  const mxArray *c4_y = NULL;
  SFc4_LessonIVInstanceStruct *chartInstance;
  chartInstance = (SFc4_LessonIVInstanceStruct *)chartInstanceVoid;
  c4_mxArrayOutData = NULL;
  c4_u = *(real_T *)c4_inData;
  c4_y = NULL;
  sf_mex_assign(&c4_y, sf_mex_create("y", &c4_u, 0, 0U, 0U, 0U, 0), false);
  sf_mex_assign(&c4_mxArrayOutData, c4_y, false);
  return c4_mxArrayOutData;
}

static real_T c4_c_emlrt_marshallIn(SFc4_LessonIVInstanceStruct *chartInstance,
  const mxArray *c4_u, const emlrtMsgIdentifier *c4_parentId)
{
  real_T c4_y;
  real_T c4_d0;
  (void)chartInstance;
  sf_mex_import(c4_parentId, sf_mex_dup(c4_u), &c4_d0, 1, 0, 0U, 0, 0U, 0);
  c4_y = c4_d0;
  sf_mex_destroy(&c4_u);
  return c4_y;
}

static void c4_b_sf_marshallIn(void *chartInstanceVoid, const mxArray
  *c4_mxArrayInData, const char_T *c4_varName, void *c4_outData)
{
  const mxArray *c4_nargout;
  const char_T *c4_identifier;
  emlrtMsgIdentifier c4_thisId;
  real_T c4_y;
  SFc4_LessonIVInstanceStruct *chartInstance;
  chartInstance = (SFc4_LessonIVInstanceStruct *)chartInstanceVoid;
  c4_nargout = sf_mex_dup(c4_mxArrayInData);
  c4_identifier = c4_varName;
  c4_thisId.fIdentifier = c4_identifier;
  c4_thisId.fParent = NULL;
  c4_thisId.bParentIsCell = false;
  c4_y = c4_c_emlrt_marshallIn(chartInstance, sf_mex_dup(c4_nargout), &c4_thisId);
  sf_mex_destroy(&c4_nargout);
  *(real_T *)c4_outData = c4_y;
  sf_mex_destroy(&c4_mxArrayInData);
}

const mxArray *sf_c4_LessonIV_get_eml_resolved_functions_info(void)
{
  const mxArray *c4_nameCaptureInfo = NULL;
  c4_nameCaptureInfo = NULL;
  sf_mex_assign(&c4_nameCaptureInfo, sf_mex_create("nameCaptureInfo", NULL, 0,
    0U, 1U, 0U, 2, 0, 1), false);
  return c4_nameCaptureInfo;
}

static uint8_T c4_typecast(SFc4_LessonIVInstanceStruct *chartInstance, int8_T
  c4_x)
{
  uint8_T c4_y;
  (void)chartInstance;
  memcpy(&c4_y, &c4_x, (size_t)1 * sizeof(uint8_T));
  return c4_y;
}

static const mxArray *c4_f_sf_marshallOut(void *chartInstanceVoid, void
  *c4_inData)
{
  const mxArray *c4_mxArrayOutData = NULL;
  int32_T c4_u;
  const mxArray *c4_y = NULL;
  SFc4_LessonIVInstanceStruct *chartInstance;
  chartInstance = (SFc4_LessonIVInstanceStruct *)chartInstanceVoid;
  c4_mxArrayOutData = NULL;
  c4_u = *(int32_T *)c4_inData;
  c4_y = NULL;
  sf_mex_assign(&c4_y, sf_mex_create("y", &c4_u, 6, 0U, 0U, 0U, 0), false);
  sf_mex_assign(&c4_mxArrayOutData, c4_y, false);
  return c4_mxArrayOutData;
}

static int32_T c4_d_emlrt_marshallIn(SFc4_LessonIVInstanceStruct *chartInstance,
  const mxArray *c4_u, const emlrtMsgIdentifier *c4_parentId)
{
  int32_T c4_y;
  int32_T c4_i22;
  (void)chartInstance;
  sf_mex_import(c4_parentId, sf_mex_dup(c4_u), &c4_i22, 1, 6, 0U, 0, 0U, 0);
  c4_y = c4_i22;
  sf_mex_destroy(&c4_u);
  return c4_y;
}

static void c4_c_sf_marshallIn(void *chartInstanceVoid, const mxArray
  *c4_mxArrayInData, const char_T *c4_varName, void *c4_outData)
{
  const mxArray *c4_b_sfEvent;
  const char_T *c4_identifier;
  emlrtMsgIdentifier c4_thisId;
  int32_T c4_y;
  SFc4_LessonIVInstanceStruct *chartInstance;
  chartInstance = (SFc4_LessonIVInstanceStruct *)chartInstanceVoid;
  c4_b_sfEvent = sf_mex_dup(c4_mxArrayInData);
  c4_identifier = c4_varName;
  c4_thisId.fIdentifier = c4_identifier;
  c4_thisId.fParent = NULL;
  c4_thisId.bParentIsCell = false;
  c4_y = c4_d_emlrt_marshallIn(chartInstance, sf_mex_dup(c4_b_sfEvent),
    &c4_thisId);
  sf_mex_destroy(&c4_b_sfEvent);
  *(int32_T *)c4_outData = c4_y;
  sf_mex_destroy(&c4_mxArrayInData);
}

static const mxArray *c4_players_bus_io(void *chartInstanceVoid, void *c4_pData)
{
  const mxArray *c4_mxVal = NULL;
  int32_T c4_i23;
  c4_Player c4_tmp[6];
  SFc4_LessonIVInstanceStruct *chartInstance;
  chartInstance = (SFc4_LessonIVInstanceStruct *)chartInstanceVoid;
  c4_mxVal = NULL;
  for (c4_i23 = 0; c4_i23 < 6; c4_i23++) {
    c4_tmp[c4_i23].x = *(int8_T *)&((char_T *)(c4_Player *)&((char_T *)
      (c4_Player (*)[6])c4_pData)[8 * c4_i23])[0];
    c4_tmp[c4_i23].y = *(int8_T *)&((char_T *)(c4_Player *)&((char_T *)
      (c4_Player (*)[6])c4_pData)[8 * c4_i23])[1];
    c4_tmp[c4_i23].orientation = *(int16_T *)&((char_T *)(c4_Player *)&((char_T *)
      (c4_Player (*)[6])c4_pData)[8 * c4_i23])[2];
    c4_tmp[c4_i23].color = *(uint8_T *)&((char_T *)(c4_Player *)&((char_T *)
      (c4_Player (*)[6])c4_pData)[8 * c4_i23])[4];
    c4_tmp[c4_i23].position = *(uint8_T *)&((char_T *)(c4_Player *)&((char_T *)
      (c4_Player (*)[6])c4_pData)[8 * c4_i23])[5];
    c4_tmp[c4_i23].valid = *(uint8_T *)&((char_T *)(c4_Player *)&((char_T *)
      (c4_Player (*)[6])c4_pData)[8 * c4_i23])[6];
  }

  sf_mex_assign(&c4_mxVal, c4_d_sf_marshallOut(chartInstance, c4_tmp), false);
  return c4_mxVal;
}

static const mxArray *c4_ball_bus_io(void *chartInstanceVoid, void *c4_pData)
{
  const mxArray *c4_mxVal = NULL;
  c4_Ball c4_tmp;
  SFc4_LessonIVInstanceStruct *chartInstance;
  chartInstance = (SFc4_LessonIVInstanceStruct *)chartInstanceVoid;
  c4_mxVal = NULL;
  c4_tmp.x = *(int8_T *)&((char_T *)(c4_Ball *)c4_pData)[0];
  c4_tmp.y = *(int8_T *)&((char_T *)(c4_Ball *)c4_pData)[1];
  c4_tmp.valid = *(uint8_T *)&((char_T *)(c4_Ball *)c4_pData)[2];
  sf_mex_assign(&c4_mxVal, c4_b_sf_marshallOut(chartInstance, &c4_tmp), false);
  return c4_mxVal;
}

static uint8_T c4_e_emlrt_marshallIn(SFc4_LessonIVInstanceStruct *chartInstance,
  const mxArray *c4_b_is_active_c4_LessonIV, const char_T *c4_identifier)
{
  uint8_T c4_y;
  emlrtMsgIdentifier c4_thisId;
  c4_thisId.fIdentifier = c4_identifier;
  c4_thisId.fParent = NULL;
  c4_thisId.bParentIsCell = false;
  c4_y = c4_f_emlrt_marshallIn(chartInstance, sf_mex_dup
    (c4_b_is_active_c4_LessonIV), &c4_thisId);
  sf_mex_destroy(&c4_b_is_active_c4_LessonIV);
  return c4_y;
}

static uint8_T c4_f_emlrt_marshallIn(SFc4_LessonIVInstanceStruct *chartInstance,
  const mxArray *c4_u, const emlrtMsgIdentifier *c4_parentId)
{
  uint8_T c4_y;
  uint8_T c4_u4;
  (void)chartInstance;
  sf_mex_import(c4_parentId, sf_mex_dup(c4_u), &c4_u4, 1, 3, 0U, 0, 0U, 0);
  c4_y = c4_u4;
  sf_mex_destroy(&c4_u);
  return c4_y;
}

static void init_dsm_address_info(SFc4_LessonIVInstanceStruct *chartInstance)
{
  (void)chartInstance;
}

static void init_simulink_io_address(SFc4_LessonIVInstanceStruct *chartInstance)
{
  chartInstance->c4_players = (c4_Player (*)[6])ssGetInputPortSignal_wrapper
    (chartInstance->S, 0);
  chartInstance->c4_gameOn = (uint8_T *)ssGetInputPortSignal_wrapper
    (chartInstance->S, 1);
  chartInstance->c4_infoToTransmit_data = (uint8_T (*)[64])
    ssGetOutputPortSignal_wrapper(chartInstance->S, 1);
  chartInstance->c4_infoToTransmit_sizes = (int32_T (*)[2])
    ssGetCurrentOutputPortDimensions_wrapper(chartInstance->S, 1);
  chartInstance->c4_ball = (c4_Ball *)ssGetInputPortSignal_wrapper
    (chartInstance->S, 2);
}

/* SFunction Glue Code */
#ifdef utFree
#undef utFree
#endif

#ifdef utMalloc
#undef utMalloc
#endif

#ifdef __cplusplus

extern "C" void *utMalloc(size_t size);
extern "C" void utFree(void*);

#else

extern void *utMalloc(size_t size);
extern void utFree(void*);

#endif

void sf_c4_LessonIV_get_check_sum(mxArray *plhs[])
{
  ((real_T *)mxGetPr((plhs[0])))[0] = (real_T)(3172550539U);
  ((real_T *)mxGetPr((plhs[0])))[1] = (real_T)(1338195478U);
  ((real_T *)mxGetPr((plhs[0])))[2] = (real_T)(2282080458U);
  ((real_T *)mxGetPr((plhs[0])))[3] = (real_T)(3671569536U);
}

mxArray* sf_c4_LessonIV_get_post_codegen_info(void);
mxArray *sf_c4_LessonIV_get_autoinheritance_info(void)
{
  const char *autoinheritanceFields[] = { "checksum", "inputs", "parameters",
    "outputs", "locals", "postCodegenInfo" };

  mxArray *mxAutoinheritanceInfo = mxCreateStructMatrix(1, 1, sizeof
    (autoinheritanceFields)/sizeof(autoinheritanceFields[0]),
    autoinheritanceFields);

  {
    mxArray *mxChecksum = mxCreateString("IsrQhqnsMKN8DLVwiLUWNB");
    mxSetField(mxAutoinheritanceInfo,0,"checksum",mxChecksum);
  }

  {
    const char *dataFields[] = { "size", "type", "complexity" };

    mxArray *mxData = mxCreateStructMatrix(1,3,3,dataFields);

    {
      mxArray *mxSize = mxCreateDoubleMatrix(1,1,mxREAL);
      double *pr = mxGetPr(mxSize);
      pr[0] = (double)(6);
      mxSetField(mxData,0,"size",mxSize);
    }

    {
      const char *typeFields[] = { "base", "fixpt", "isFixedPointType" };

      mxArray *mxType = mxCreateStructMatrix(1,1,sizeof(typeFields)/sizeof
        (typeFields[0]),typeFields);
      mxSetField(mxType,0,"base",mxCreateDoubleScalar(13));
      mxSetField(mxType,0,"fixpt",mxCreateDoubleMatrix(0,0,mxREAL));
      mxSetField(mxType,0,"isFixedPointType",mxCreateDoubleScalar(0));
      mxSetField(mxData,0,"type",mxType);
    }

    mxSetField(mxData,0,"complexity",mxCreateDoubleScalar(0));

    {
      mxArray *mxSize = mxCreateDoubleMatrix(1,0,mxREAL);
      double *pr = mxGetPr(mxSize);
      mxSetField(mxData,1,"size",mxSize);
    }

    {
      const char *typeFields[] = { "base", "fixpt", "isFixedPointType" };

      mxArray *mxType = mxCreateStructMatrix(1,1,sizeof(typeFields)/sizeof
        (typeFields[0]),typeFields);
      mxSetField(mxType,0,"base",mxCreateDoubleScalar(3));
      mxSetField(mxType,0,"fixpt",mxCreateDoubleMatrix(0,0,mxREAL));
      mxSetField(mxType,0,"isFixedPointType",mxCreateDoubleScalar(0));
      mxSetField(mxData,1,"type",mxType);
    }

    mxSetField(mxData,1,"complexity",mxCreateDoubleScalar(0));

    {
      mxArray *mxSize = mxCreateDoubleMatrix(1,0,mxREAL);
      double *pr = mxGetPr(mxSize);
      mxSetField(mxData,2,"size",mxSize);
    }

    {
      const char *typeFields[] = { "base", "fixpt", "isFixedPointType" };

      mxArray *mxType = mxCreateStructMatrix(1,1,sizeof(typeFields)/sizeof
        (typeFields[0]),typeFields);
      mxSetField(mxType,0,"base",mxCreateDoubleScalar(13));
      mxSetField(mxType,0,"fixpt",mxCreateDoubleMatrix(0,0,mxREAL));
      mxSetField(mxType,0,"isFixedPointType",mxCreateDoubleScalar(0));
      mxSetField(mxData,2,"type",mxType);
    }

    mxSetField(mxData,2,"complexity",mxCreateDoubleScalar(0));
    mxSetField(mxAutoinheritanceInfo,0,"inputs",mxData);
  }

  {
    mxSetField(mxAutoinheritanceInfo,0,"parameters",mxCreateDoubleMatrix(0,0,
                mxREAL));
  }

  {
    const char *dataFields[] = { "size", "type", "complexity" };

    mxArray *mxData = mxCreateStructMatrix(1,1,3,dataFields);

    {
      mxArray *mxSize = mxCreateDoubleMatrix(1,2,mxREAL);
      double *pr = mxGetPr(mxSize);
      pr[0] = (double)(1);
      pr[1] = (double)(64);
      mxSetField(mxData,0,"size",mxSize);
    }

    {
      const char *typeFields[] = { "base", "fixpt", "isFixedPointType" };

      mxArray *mxType = mxCreateStructMatrix(1,1,sizeof(typeFields)/sizeof
        (typeFields[0]),typeFields);
      mxSetField(mxType,0,"base",mxCreateDoubleScalar(3));
      mxSetField(mxType,0,"fixpt",mxCreateDoubleMatrix(0,0,mxREAL));
      mxSetField(mxType,0,"isFixedPointType",mxCreateDoubleScalar(0));
      mxSetField(mxData,0,"type",mxType);
    }

    mxSetField(mxData,0,"complexity",mxCreateDoubleScalar(0));
    mxSetField(mxAutoinheritanceInfo,0,"outputs",mxData);
  }

  {
    mxSetField(mxAutoinheritanceInfo,0,"locals",mxCreateDoubleMatrix(0,0,mxREAL));
  }

  {
    mxArray* mxPostCodegenInfo = sf_c4_LessonIV_get_post_codegen_info();
    mxSetField(mxAutoinheritanceInfo,0,"postCodegenInfo",mxPostCodegenInfo);
  }

  return(mxAutoinheritanceInfo);
}

mxArray *sf_c4_LessonIV_third_party_uses_info(void)
{
  mxArray * mxcell3p = mxCreateCellMatrix(1,0);
  return(mxcell3p);
}

mxArray *sf_c4_LessonIV_jit_fallback_info(void)
{
  const char *infoFields[] = { "fallbackType", "fallbackReason",
    "hiddenFallbackType", "hiddenFallbackReason", "incompatibleSymbol" };

  mxArray *mxInfo = mxCreateStructMatrix(1, 1, 5, infoFields);
  mxArray *fallbackType = mxCreateString("late");
  mxArray *fallbackReason = mxCreateString("ir_function_calls");
  mxArray *hiddenFallbackType = mxCreateString("");
  mxArray *hiddenFallbackReason = mxCreateString("");
  mxArray *incompatibleSymbol = mxCreateString("memcpy");
  mxSetField(mxInfo, 0, infoFields[0], fallbackType);
  mxSetField(mxInfo, 0, infoFields[1], fallbackReason);
  mxSetField(mxInfo, 0, infoFields[2], hiddenFallbackType);
  mxSetField(mxInfo, 0, infoFields[3], hiddenFallbackReason);
  mxSetField(mxInfo, 0, infoFields[4], incompatibleSymbol);
  return mxInfo;
}

mxArray *sf_c4_LessonIV_updateBuildInfo_args_info(void)
{
  mxArray *mxBIArgs = mxCreateCellMatrix(1,0);
  return mxBIArgs;
}

mxArray* sf_c4_LessonIV_get_post_codegen_info(void)
{
  const char* fieldNames[] = { "exportedFunctionsUsedByThisChart",
    "exportedFunctionsChecksum" };

  mwSize dims[2] = { 1, 1 };

  mxArray* mxPostCodegenInfo = mxCreateStructArray(2, dims, sizeof(fieldNames)/
    sizeof(fieldNames[0]), fieldNames);

  {
    mxArray* mxExportedFunctionsChecksum = mxCreateString("");
    mwSize exp_dims[2] = { 0, 1 };

    mxArray* mxExportedFunctionsUsedByThisChart = mxCreateCellArray(2, exp_dims);
    mxSetField(mxPostCodegenInfo, 0, "exportedFunctionsUsedByThisChart",
               mxExportedFunctionsUsedByThisChart);
    mxSetField(mxPostCodegenInfo, 0, "exportedFunctionsChecksum",
               mxExportedFunctionsChecksum);
  }

  return mxPostCodegenInfo;
}

static const mxArray *sf_get_sim_state_info_c4_LessonIV(void)
{
  const char *infoFields[] = { "chartChecksum", "varInfo" };

  mxArray *mxInfo = mxCreateStructMatrix(1, 1, 2, infoFields);
  const char *infoEncStr[] = {
    "100 S1x2'type','srcId','name','auxInfo'{{M[1],M[5],T\"infoToTransmit\",},{M[8],M[0],T\"is_active_c4_LessonIV\",}}"
  };

  mxArray *mxVarInfo = sf_mex_decode_encoded_mx_struct_array(infoEncStr, 2, 10);
  mxArray *mxChecksum = mxCreateDoubleMatrix(1, 4, mxREAL);
  sf_c4_LessonIV_get_check_sum(&mxChecksum);
  mxSetField(mxInfo, 0, infoFields[0], mxChecksum);
  mxSetField(mxInfo, 0, infoFields[1], mxVarInfo);
  return mxInfo;
}

static void chart_debug_initialization(SimStruct *S, unsigned int
  fullDebuggerInitialization)
{
  if (!sim_mode_is_rtw_gen(S)) {
    SFc4_LessonIVInstanceStruct *chartInstance = (SFc4_LessonIVInstanceStruct *)
      sf_get_chart_instance_ptr(S);
    if (ssIsFirstInitCond(S) && fullDebuggerInitialization==1) {
      /* do this only if simulation is starting */
      {
        unsigned int chartAlreadyPresent;
        chartAlreadyPresent = sf_debug_initialize_chart
          (sfGlobalDebugInstanceStruct,
           _LessonIVMachineNumber_,
           4,
           1,
           1,
           0,
           4,
           0,
           0,
           0,
           0,
           0,
           &(chartInstance->chartNumber),
           &(chartInstance->instanceNumber),
           (void *)S);

        /* Each instance must initialize its own list of scripts */
        init_script_number_translation(_LessonIVMachineNumber_,
          chartInstance->chartNumber,chartInstance->instanceNumber);
        if (chartAlreadyPresent==0) {
          /* this is the first instance */
          sf_debug_set_chart_disable_implicit_casting
            (sfGlobalDebugInstanceStruct,_LessonIVMachineNumber_,
             chartInstance->chartNumber,1);
          sf_debug_set_chart_event_thresholds(sfGlobalDebugInstanceStruct,
            _LessonIVMachineNumber_,
            chartInstance->chartNumber,
            0,
            0,
            0);
          _SFD_SET_DATA_PROPS(0,1,1,0,"players");
          _SFD_SET_DATA_PROPS(1,1,1,0,"gameOn");
          _SFD_SET_DATA_PROPS(2,1,1,0,"ball");
          _SFD_SET_DATA_PROPS(3,2,0,1,"infoToTransmit");
          _SFD_STATE_INFO(0,0,2);
          _SFD_CH_SUBSTATE_COUNT(0);
          _SFD_CH_SUBSTATE_DECOMP(0);
        }

        _SFD_CV_INIT_CHART(0,0,0,0);

        {
          _SFD_CV_INIT_STATE(0,0,0,0,0,0,NULL,NULL);
        }

        _SFD_CV_INIT_TRANS(0,0,NULL,NULL,0,NULL);

        /* Initialization of MATLAB Function Model Coverage */
        _SFD_CV_INIT_EML(0,1,1,0,0,0,0,0,1,0,0,0);
        _SFD_CV_INIT_EML_FCN(0,0,"eML_blk_kernel",0,-1,480);
        _SFD_CV_INIT_EML_FOR(0,1,0,244,255,450);

        {
          unsigned int dimVector[1];
          dimVector[0]= 6U;
          _SFD_SET_DATA_COMPILED_PROPS(0,SF_STRUCT,1,&(dimVector[0]),0,0,0,0.0,
            1.0,0,0,(MexFcnForType)c4_players_bus_io,(MexInFcnForType)NULL);
        }

        _SFD_SET_DATA_COMPILED_PROPS(1,SF_UINT8,0,NULL,0,0,0,0.0,1.0,0,0,
          (MexFcnForType)c4_c_sf_marshallOut,(MexInFcnForType)NULL);
        _SFD_SET_DATA_COMPILED_PROPS(2,SF_STRUCT,0,NULL,0,0,0,0.0,1.0,0,0,
          (MexFcnForType)c4_ball_bus_io,(MexInFcnForType)NULL);

        {
          unsigned int dimVector[2];
          dimVector[0]= 1U;
          dimVector[1]= 64U;
          _SFD_SET_DATA_COMPILED_PROPS(3,SF_UINT8,2,&(dimVector[0]),0,0,0,0.0,
            1.0,0,0,(MexFcnForType)c4_sf_marshallOut,(MexInFcnForType)
            c4_sf_marshallIn);
        }
      }
    } else {
      sf_debug_reset_current_state_configuration(sfGlobalDebugInstanceStruct,
        _LessonIVMachineNumber_,chartInstance->chartNumber,
        chartInstance->instanceNumber);
    }
  }
}

static void chart_debug_initialize_data_addresses(SimStruct *S)
{
  if (!sim_mode_is_rtw_gen(S)) {
    SFc4_LessonIVInstanceStruct *chartInstance = (SFc4_LessonIVInstanceStruct *)
      sf_get_chart_instance_ptr(S);
    if (ssIsFirstInitCond(S)) {
      /* do this only if simulation is starting and after we know the addresses of all data */
      {
        _SFD_SET_DATA_VALUE_PTR(0U, *chartInstance->c4_players);
        _SFD_SET_DATA_VALUE_PTR(1U, chartInstance->c4_gameOn);
        _SFD_SET_DATA_VALUE_PTR_VAR_DIM(3U,
          *chartInstance->c4_infoToTransmit_data, (void *)
          chartInstance->c4_infoToTransmit_sizes);
        _SFD_SET_DATA_VALUE_PTR(2U, chartInstance->c4_ball);
      }
    }
  }
}

static const char* sf_get_instance_specialization(void)
{
  return "sSxMd1pgAWxKSAJmIx2Q9pE";
}

static void sf_opaque_initialize_c4_LessonIV(void *chartInstanceVar)
{
  chart_debug_initialization(((SFc4_LessonIVInstanceStruct*) chartInstanceVar)
    ->S,0);
  initialize_params_c4_LessonIV((SFc4_LessonIVInstanceStruct*) chartInstanceVar);
  initialize_c4_LessonIV((SFc4_LessonIVInstanceStruct*) chartInstanceVar);
}

static void sf_opaque_enable_c4_LessonIV(void *chartInstanceVar)
{
  enable_c4_LessonIV((SFc4_LessonIVInstanceStruct*) chartInstanceVar);
}

static void sf_opaque_disable_c4_LessonIV(void *chartInstanceVar)
{
  disable_c4_LessonIV((SFc4_LessonIVInstanceStruct*) chartInstanceVar);
}

static void sf_opaque_gateway_c4_LessonIV(void *chartInstanceVar)
{
  sf_gateway_c4_LessonIV((SFc4_LessonIVInstanceStruct*) chartInstanceVar);
}

static const mxArray* sf_opaque_get_sim_state_c4_LessonIV(SimStruct* S)
{
  return get_sim_state_c4_LessonIV((SFc4_LessonIVInstanceStruct *)
    sf_get_chart_instance_ptr(S));     /* raw sim ctx */
}

static void sf_opaque_set_sim_state_c4_LessonIV(SimStruct* S, const mxArray *st)
{
  set_sim_state_c4_LessonIV((SFc4_LessonIVInstanceStruct*)
    sf_get_chart_instance_ptr(S), st);
}

static void sf_opaque_terminate_c4_LessonIV(void *chartInstanceVar)
{
  if (chartInstanceVar!=NULL) {
    SimStruct *S = ((SFc4_LessonIVInstanceStruct*) chartInstanceVar)->S;
    if (sim_mode_is_rtw_gen(S) || sim_mode_is_external(S)) {
      sf_clear_rtw_identifier(S);
      unload_LessonIV_optimization_info();
    }

    finalize_c4_LessonIV((SFc4_LessonIVInstanceStruct*) chartInstanceVar);
    utFree(chartInstanceVar);
    if (ssGetUserData(S)!= NULL) {
      sf_free_ChartRunTimeInfo(S);
    }

    ssSetUserData(S,NULL);
  }
}

static void sf_opaque_init_subchart_simstructs(void *chartInstanceVar)
{
  initSimStructsc4_LessonIV((SFc4_LessonIVInstanceStruct*) chartInstanceVar);
}

extern unsigned int sf_machine_global_initializer_called(void);
static void mdlProcessParameters_c4_LessonIV(SimStruct *S)
{
  int i;
  for (i=0;i<ssGetNumRunTimeParams(S);i++) {
    if (ssGetSFcnParamTunable(S,i)) {
      ssUpdateDlgParamAsRunTimeParam(S,i);
    }
  }

  if (sf_machine_global_initializer_called()) {
    initialize_params_c4_LessonIV((SFc4_LessonIVInstanceStruct*)
      sf_get_chart_instance_ptr(S));
  }
}

static void mdlSetWorkWidths_c4_LessonIV(SimStruct *S)
{
  ssMdlUpdateIsEmpty(S, 1);
  if (sim_mode_is_rtw_gen(S) || sim_mode_is_external(S)) {
    mxArray *infoStruct = load_LessonIV_optimization_info();
    int_T chartIsInlinable =
      (int_T)sf_is_chart_inlinable(sf_get_instance_specialization(),infoStruct,4);
    ssSetStateflowIsInlinable(S,chartIsInlinable);
    ssSetRTWCG(S,1);
    ssSetEnableFcnIsTrivial(S,1);
    ssSetDisableFcnIsTrivial(S,1);
    ssSetNotMultipleInlinable(S,sf_rtw_info_uint_prop
      (sf_get_instance_specialization(),infoStruct,4,
       "gatewayCannotBeInlinedMultipleTimes"));
    sf_set_chart_accesses_machine_info(S, sf_get_instance_specialization(),
      infoStruct, 4);
    sf_update_buildInfo(sf_get_instance_specialization(),infoStruct,4);
    if (chartIsInlinable) {
      ssSetInputPortOptimOpts(S, 0, SS_REUSABLE_AND_LOCAL);
      ssSetInputPortOptimOpts(S, 1, SS_REUSABLE_AND_LOCAL);
      ssSetInputPortOptimOpts(S, 2, SS_REUSABLE_AND_LOCAL);
      sf_mark_chart_expressionable_inputs(S,sf_get_instance_specialization(),
        infoStruct,4,3);
      sf_mark_chart_reusable_outputs(S,sf_get_instance_specialization(),
        infoStruct,4,1);
    }

    {
      unsigned int outPortIdx;
      for (outPortIdx=1; outPortIdx<=1; ++outPortIdx) {
        ssSetOutputPortOptimizeInIR(S, outPortIdx, 1U);
      }
    }

    {
      unsigned int inPortIdx;
      for (inPortIdx=0; inPortIdx < 3; ++inPortIdx) {
        ssSetInputPortOptimizeInIR(S, inPortIdx, 1U);
      }
    }

    sf_set_rtw_dwork_info(S,sf_get_instance_specialization(),infoStruct,4);
    ssSetHasSubFunctions(S,!(chartIsInlinable));
  } else {
  }

  ssSetOptions(S,ssGetOptions(S)|SS_OPTION_WORKS_WITH_CODE_REUSE);
  ssSetChecksum0(S,(2611893979U));
  ssSetChecksum1(S,(1418321650U));
  ssSetChecksum2(S,(3450362953U));
  ssSetChecksum3(S,(1335861676U));
  ssSetmdlDerivatives(S, NULL);
  ssSetExplicitFCSSCtrl(S,1);
  ssSetStateSemanticsClassicAndSynchronous(S, true);
  ssSupportsMultipleExecInstances(S,1);
}

static void mdlRTW_c4_LessonIV(SimStruct *S)
{
  if (sim_mode_is_rtw_gen(S)) {
    ssWriteRTWStrParam(S, "StateflowChartType", "Embedded MATLAB");
  }
}

static void mdlStart_c4_LessonIV(SimStruct *S)
{
  SFc4_LessonIVInstanceStruct *chartInstance;
  chartInstance = (SFc4_LessonIVInstanceStruct *)utMalloc(sizeof
    (SFc4_LessonIVInstanceStruct));
  if (chartInstance==NULL) {
    sf_mex_error_message("Could not allocate memory for chart instance.");
  }

  memset(chartInstance, 0, sizeof(SFc4_LessonIVInstanceStruct));
  chartInstance->chartInfo.chartInstance = chartInstance;
  chartInstance->chartInfo.isEMLChart = 1;
  chartInstance->chartInfo.chartInitialized = 0;
  chartInstance->chartInfo.sFunctionGateway = sf_opaque_gateway_c4_LessonIV;
  chartInstance->chartInfo.initializeChart = sf_opaque_initialize_c4_LessonIV;
  chartInstance->chartInfo.terminateChart = sf_opaque_terminate_c4_LessonIV;
  chartInstance->chartInfo.enableChart = sf_opaque_enable_c4_LessonIV;
  chartInstance->chartInfo.disableChart = sf_opaque_disable_c4_LessonIV;
  chartInstance->chartInfo.getSimState = sf_opaque_get_sim_state_c4_LessonIV;
  chartInstance->chartInfo.setSimState = sf_opaque_set_sim_state_c4_LessonIV;
  chartInstance->chartInfo.getSimStateInfo = sf_get_sim_state_info_c4_LessonIV;
  chartInstance->chartInfo.zeroCrossings = NULL;
  chartInstance->chartInfo.outputs = NULL;
  chartInstance->chartInfo.derivatives = NULL;
  chartInstance->chartInfo.mdlRTW = mdlRTW_c4_LessonIV;
  chartInstance->chartInfo.mdlStart = mdlStart_c4_LessonIV;
  chartInstance->chartInfo.mdlSetWorkWidths = mdlSetWorkWidths_c4_LessonIV;
  chartInstance->chartInfo.callGetHoverDataForMsg = NULL;
  chartInstance->chartInfo.extModeExec = NULL;
  chartInstance->chartInfo.restoreLastMajorStepConfiguration = NULL;
  chartInstance->chartInfo.restoreBeforeLastMajorStepConfiguration = NULL;
  chartInstance->chartInfo.storeCurrentConfiguration = NULL;
  chartInstance->chartInfo.callAtomicSubchartUserFcn = NULL;
  chartInstance->chartInfo.callAtomicSubchartAutoFcn = NULL;
  chartInstance->chartInfo.debugInstance = sfGlobalDebugInstanceStruct;
  chartInstance->S = S;
  sf_init_ChartRunTimeInfo(S, &(chartInstance->chartInfo), false, 0);
  init_dsm_address_info(chartInstance);
  init_simulink_io_address(chartInstance);
  if (!sim_mode_is_rtw_gen(S)) {
  }

  chart_debug_initialization(S,1);
  mdl_start_c4_LessonIV(chartInstance);
}

void c4_LessonIV_method_dispatcher(SimStruct *S, int_T method, void *data)
{
  switch (method) {
   case SS_CALL_MDL_START:
    mdlStart_c4_LessonIV(S);
    break;

   case SS_CALL_MDL_SET_WORK_WIDTHS:
    mdlSetWorkWidths_c4_LessonIV(S);
    break;

   case SS_CALL_MDL_PROCESS_PARAMETERS:
    mdlProcessParameters_c4_LessonIV(S);
    break;

   default:
    /* Unhandled method */
    sf_mex_error_message("Stateflow Internal Error:\n"
                         "Error calling c4_LessonIV_method_dispatcher.\n"
                         "Can't handle method %d.\n", method);
    break;
  }
}

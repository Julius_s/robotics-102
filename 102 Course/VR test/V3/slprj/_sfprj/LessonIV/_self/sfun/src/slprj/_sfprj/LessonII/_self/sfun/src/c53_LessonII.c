/* Include files */

#include <stddef.h>
#include "blas.h"
#include "LessonII_sfun.h"
#include "c53_LessonII.h"
#include <string.h>
#define CHARTINSTANCE_CHARTNUMBER      (chartInstance->chartNumber)
#define CHARTINSTANCE_INSTANCENUMBER   (chartInstance->instanceNumber)
#include "LessonII_sfun_debug_macros.h"
#define _SF_MEX_LISTEN_FOR_CTRL_C(S)   sf_mex_listen_for_ctrl_c_with_debugger(S, sfGlobalDebugInstanceStruct);

static void chart_debug_initialization(SimStruct *S, unsigned int
  fullDebuggerInitialization);
static void chart_debug_initialize_data_addresses(SimStruct *S);

/* Type Definitions */

/* Named Constants */
#define CALL_EVENT                     (-1)

/* Variable Declarations */

/* Variable Definitions */
static real_T _sfTime_;
static const char * c53_debug_family_names[10] = { "tempEmpty", "colors",
  "positions", "ii", "message", "nargin", "nargout", "Ball", "players", "gameOn"
};

/* Function Declarations */
static void initialize_c53_LessonII(SFc53_LessonIIInstanceStruct *chartInstance);
static void initialize_params_c53_LessonII(SFc53_LessonIIInstanceStruct
  *chartInstance);
static void enable_c53_LessonII(SFc53_LessonIIInstanceStruct *chartInstance);
static void disable_c53_LessonII(SFc53_LessonIIInstanceStruct *chartInstance);
static void c53_update_debugger_state_c53_LessonII(SFc53_LessonIIInstanceStruct *
  chartInstance);
static const mxArray *get_sim_state_c53_LessonII(SFc53_LessonIIInstanceStruct
  *chartInstance);
static void set_sim_state_c53_LessonII(SFc53_LessonIIInstanceStruct
  *chartInstance, const mxArray *c53_st);
static void finalize_c53_LessonII(SFc53_LessonIIInstanceStruct *chartInstance);
static void sf_gateway_c53_LessonII(SFc53_LessonIIInstanceStruct *chartInstance);
static void mdl_start_c53_LessonII(SFc53_LessonIIInstanceStruct *chartInstance);
static void initSimStructsc53_LessonII(SFc53_LessonIIInstanceStruct
  *chartInstance);
static void init_script_number_translation(uint32_T c53_machineNumber, uint32_T
  c53_chartNumber, uint32_T c53_instanceNumber);
static const mxArray *c53_sf_marshallOut(void *chartInstanceVoid, void
  *c53_inData);
static uint8_T c53_emlrt_marshallIn(SFc53_LessonIIInstanceStruct *chartInstance,
  const mxArray *c53_b_gameOn, const char_T *c53_identifier);
static uint8_T c53_b_emlrt_marshallIn(SFc53_LessonIIInstanceStruct
  *chartInstance, const mxArray *c53_u, const emlrtMsgIdentifier *c53_parentId);
static void c53_sf_marshallIn(void *chartInstanceVoid, const mxArray
  *c53_mxArrayInData, const char_T *c53_varName, void *c53_outData);
static const mxArray *c53_b_sf_marshallOut(void *chartInstanceVoid, void
  *c53_inData);
static void c53_c_emlrt_marshallIn(SFc53_LessonIIInstanceStruct *chartInstance,
  const mxArray *c53_b_players, const char_T *c53_identifier, c53_Player c53_y[6]);
static void c53_d_emlrt_marshallIn(SFc53_LessonIIInstanceStruct *chartInstance,
  const mxArray *c53_u, const emlrtMsgIdentifier *c53_parentId, c53_Player
  c53_y[6]);
static int8_T c53_e_emlrt_marshallIn(SFc53_LessonIIInstanceStruct *chartInstance,
  const mxArray *c53_u, const emlrtMsgIdentifier *c53_parentId);
static int16_T c53_f_emlrt_marshallIn(SFc53_LessonIIInstanceStruct
  *chartInstance, const mxArray *c53_u, const emlrtMsgIdentifier *c53_parentId);
static void c53_b_sf_marshallIn(void *chartInstanceVoid, const mxArray
  *c53_mxArrayInData, const char_T *c53_varName, void *c53_outData);
static const mxArray *c53_c_sf_marshallOut(void *chartInstanceVoid, void
  *c53_inData);
static c53_Ball c53_g_emlrt_marshallIn(SFc53_LessonIIInstanceStruct
  *chartInstance, const mxArray *c53_c_Ball, const char_T *c53_identifier);
static c53_Ball c53_h_emlrt_marshallIn(SFc53_LessonIIInstanceStruct
  *chartInstance, const mxArray *c53_u, const emlrtMsgIdentifier *c53_parentId);
static void c53_c_sf_marshallIn(void *chartInstanceVoid, const mxArray
  *c53_mxArrayInData, const char_T *c53_varName, void *c53_outData);
static const mxArray *c53_d_sf_marshallOut(void *chartInstanceVoid, void
  *c53_inData);
static const mxArray *c53_e_sf_marshallOut(void *chartInstanceVoid, void
  *c53_inData);
static real_T c53_i_emlrt_marshallIn(SFc53_LessonIIInstanceStruct *chartInstance,
  const mxArray *c53_u, const emlrtMsgIdentifier *c53_parentId);
static void c53_d_sf_marshallIn(void *chartInstanceVoid, const mxArray
  *c53_mxArrayInData, const char_T *c53_varName, void *c53_outData);
static const mxArray *c53_f_sf_marshallOut(void *chartInstanceVoid, void
  *c53_inData);
static void c53_j_emlrt_marshallIn(SFc53_LessonIIInstanceStruct *chartInstance,
  const mxArray *c53_u, const emlrtMsgIdentifier *c53_parentId, uint8_T c53_y[24]);
static void c53_e_sf_marshallIn(void *chartInstanceVoid, const mxArray
  *c53_mxArrayInData, const char_T *c53_varName, void *c53_outData);
static const mxArray *c53_g_sf_marshallOut(void *chartInstanceVoid, void
  *c53_inData);
static void c53_k_emlrt_marshallIn(SFc53_LessonIIInstanceStruct *chartInstance,
  const mxArray *c53_u, const emlrtMsgIdentifier *c53_parentId, uint8_T c53_y[26]);
static void c53_f_sf_marshallIn(void *chartInstanceVoid, const mxArray
  *c53_mxArrayInData, const char_T *c53_varName, void *c53_outData);
static const mxArray *c53_h_sf_marshallOut(void *chartInstanceVoid, void
  *c53_inData);
static void c53_l_emlrt_marshallIn(SFc53_LessonIIInstanceStruct *chartInstance,
  const mxArray *c53_u, const emlrtMsgIdentifier *c53_parentId, uint8_T c53_y[27]);
static void c53_g_sf_marshallIn(void *chartInstanceVoid, const mxArray
  *c53_mxArrayInData, const char_T *c53_varName, void *c53_outData);
static const mxArray *c53_i_sf_marshallOut(void *chartInstanceVoid, void
  *c53_inData);
static void c53_m_emlrt_marshallIn(SFc53_LessonIIInstanceStruct *chartInstance,
  const mxArray *c53_u, const emlrtMsgIdentifier *c53_parentId, uint8_T c53_y[6]);
static void c53_h_sf_marshallIn(void *chartInstanceVoid, const mxArray
  *c53_mxArrayInData, const char_T *c53_varName, void *c53_outData);
static const mxArray *c53_j_sf_marshallOut(void *chartInstanceVoid, void
  *c53_inData);
static c53_Player c53_n_emlrt_marshallIn(SFc53_LessonIIInstanceStruct
  *chartInstance, const mxArray *c53_u, const emlrtMsgIdentifier *c53_parentId);
static void c53_i_sf_marshallIn(void *chartInstanceVoid, const mxArray
  *c53_mxArrayInData, const char_T *c53_varName, void *c53_outData);
static int8_T c53_typecast(SFc53_LessonIIInstanceStruct *chartInstance, uint8_T
  c53_x);
static const mxArray *c53_k_sf_marshallOut(void *chartInstanceVoid, void
  *c53_inData);
static int32_T c53_o_emlrt_marshallIn(SFc53_LessonIIInstanceStruct
  *chartInstance, const mxArray *c53_u, const emlrtMsgIdentifier *c53_parentId);
static void c53_j_sf_marshallIn(void *chartInstanceVoid, const mxArray
  *c53_mxArrayInData, const char_T *c53_varName, void *c53_outData);
static const mxArray *c53_Ball_bus_io(void *chartInstanceVoid, void *c53_pData);
static const mxArray *c53_players_bus_io(void *chartInstanceVoid, void
  *c53_pData);
static void init_dsm_address_info(SFc53_LessonIIInstanceStruct *chartInstance);
static void init_simulink_io_address(SFc53_LessonIIInstanceStruct *chartInstance);

/* Function Definitions */
static void initialize_c53_LessonII(SFc53_LessonIIInstanceStruct *chartInstance)
{
  if (sf_is_first_init_cond(chartInstance->S)) {
    initSimStructsc53_LessonII(chartInstance);
    chart_debug_initialize_data_addresses(chartInstance->S);
  }

  chartInstance->c53_sfEvent = CALL_EVENT;
  _sfTime_ = sf_get_time(chartInstance->S);
  chartInstance->c53_is_active_c53_LessonII = 0U;
}

static void initialize_params_c53_LessonII(SFc53_LessonIIInstanceStruct
  *chartInstance)
{
  (void)chartInstance;
}

static void enable_c53_LessonII(SFc53_LessonIIInstanceStruct *chartInstance)
{
  _sfTime_ = sf_get_time(chartInstance->S);
}

static void disable_c53_LessonII(SFc53_LessonIIInstanceStruct *chartInstance)
{
  _sfTime_ = sf_get_time(chartInstance->S);
}

static void c53_update_debugger_state_c53_LessonII(SFc53_LessonIIInstanceStruct *
  chartInstance)
{
  (void)chartInstance;
}

static const mxArray *get_sim_state_c53_LessonII(SFc53_LessonIIInstanceStruct
  *chartInstance)
{
  const mxArray *c53_st;
  const mxArray *c53_y = NULL;
  const mxArray *c53_b_y = NULL;
  int8_T c53_u;
  const mxArray *c53_c_y = NULL;
  int8_T c53_b_u;
  const mxArray *c53_d_y = NULL;
  uint8_T c53_c_u;
  const mxArray *c53_e_y = NULL;
  uint8_T c53_hoistedGlobal;
  uint8_T c53_d_u;
  const mxArray *c53_f_y = NULL;
  int32_T c53_i0;
  c53_Player c53_e_u[6];
  const mxArray *c53_g_y = NULL;
  static int32_T c53_iv0[1] = { 6 };

  int32_T c53_iv1[1];
  int32_T c53_i1;
  const c53_Player *c53_r0;
  int8_T c53_f_u;
  const mxArray *c53_h_y = NULL;
  int8_T c53_g_u;
  const mxArray *c53_i_y = NULL;
  int16_T c53_h_u;
  const mxArray *c53_j_y = NULL;
  uint8_T c53_i_u;
  const mxArray *c53_k_y = NULL;
  uint8_T c53_j_u;
  const mxArray *c53_l_y = NULL;
  uint8_T c53_k_u;
  const mxArray *c53_m_y = NULL;
  uint8_T c53_b_hoistedGlobal;
  uint8_T c53_l_u;
  const mxArray *c53_n_y = NULL;
  c53_st = NULL;
  c53_st = NULL;
  c53_y = NULL;
  sf_mex_assign(&c53_y, sf_mex_createcellmatrix(4, 1), false);
  c53_b_y = NULL;
  sf_mex_assign(&c53_b_y, sf_mex_createstruct("structure", 2, 1, 1), false);
  c53_u = *(int8_T *)&((char_T *)chartInstance->c53_b_Ball)[0];
  c53_c_y = NULL;
  sf_mex_assign(&c53_c_y, sf_mex_create("y", &c53_u, 2, 0U, 0U, 0U, 0), false);
  sf_mex_addfield(c53_b_y, c53_c_y, "x", "x", 0);
  c53_b_u = *(int8_T *)&((char_T *)chartInstance->c53_b_Ball)[1];
  c53_d_y = NULL;
  sf_mex_assign(&c53_d_y, sf_mex_create("y", &c53_b_u, 2, 0U, 0U, 0U, 0), false);
  sf_mex_addfield(c53_b_y, c53_d_y, "y", "y", 0);
  c53_c_u = *(uint8_T *)&((char_T *)chartInstance->c53_b_Ball)[2];
  c53_e_y = NULL;
  sf_mex_assign(&c53_e_y, sf_mex_create("y", &c53_c_u, 3, 0U, 0U, 0U, 0), false);
  sf_mex_addfield(c53_b_y, c53_e_y, "valid", "valid", 0);
  sf_mex_setcell(c53_y, 0, c53_b_y);
  c53_hoistedGlobal = *chartInstance->c53_gameOn;
  c53_d_u = c53_hoistedGlobal;
  c53_f_y = NULL;
  sf_mex_assign(&c53_f_y, sf_mex_create("y", &c53_d_u, 3, 0U, 0U, 0U, 0), false);
  sf_mex_setcell(c53_y, 1, c53_f_y);
  for (c53_i0 = 0; c53_i0 < 6; c53_i0++) {
    c53_e_u[c53_i0].x = *(int8_T *)&((char_T *)(c53_Player *)&((char_T *)
      chartInstance->c53_players)[8 * c53_i0])[0];
    c53_e_u[c53_i0].y = *(int8_T *)&((char_T *)(c53_Player *)&((char_T *)
      chartInstance->c53_players)[8 * c53_i0])[1];
    c53_e_u[c53_i0].orientation = *(int16_T *)&((char_T *)(c53_Player *)
      &((char_T *)chartInstance->c53_players)[8 * c53_i0])[2];
    c53_e_u[c53_i0].color = *(uint8_T *)&((char_T *)(c53_Player *)&((char_T *)
      chartInstance->c53_players)[8 * c53_i0])[4];
    c53_e_u[c53_i0].position = *(uint8_T *)&((char_T *)(c53_Player *)&((char_T *)
      chartInstance->c53_players)[8 * c53_i0])[5];
    c53_e_u[c53_i0].valid = *(uint8_T *)&((char_T *)(c53_Player *)&((char_T *)
      chartInstance->c53_players)[8 * c53_i0])[6];
  }

  c53_g_y = NULL;
  c53_iv1[0] = c53_iv0[0];
  sf_mex_assign(&c53_g_y, sf_mex_createstructarray("structure", 1, c53_iv1),
                false);
  for (c53_i1 = 0; c53_i1 < 6; c53_i1++) {
    c53_r0 = &c53_e_u[c53_i1];
    c53_f_u = c53_r0->x;
    c53_h_y = NULL;
    sf_mex_assign(&c53_h_y, sf_mex_create("y", &c53_f_u, 2, 0U, 0U, 0U, 0),
                  false);
    sf_mex_addfield(c53_g_y, c53_h_y, "x", "x", c53_i1);
    c53_g_u = c53_r0->y;
    c53_i_y = NULL;
    sf_mex_assign(&c53_i_y, sf_mex_create("y", &c53_g_u, 2, 0U, 0U, 0U, 0),
                  false);
    sf_mex_addfield(c53_g_y, c53_i_y, "y", "y", c53_i1);
    c53_h_u = c53_r0->orientation;
    c53_j_y = NULL;
    sf_mex_assign(&c53_j_y, sf_mex_create("y", &c53_h_u, 4, 0U, 0U, 0U, 0),
                  false);
    sf_mex_addfield(c53_g_y, c53_j_y, "orientation", "orientation", c53_i1);
    c53_i_u = c53_r0->color;
    c53_k_y = NULL;
    sf_mex_assign(&c53_k_y, sf_mex_create("y", &c53_i_u, 3, 0U, 0U, 0U, 0),
                  false);
    sf_mex_addfield(c53_g_y, c53_k_y, "color", "color", c53_i1);
    c53_j_u = c53_r0->position;
    c53_l_y = NULL;
    sf_mex_assign(&c53_l_y, sf_mex_create("y", &c53_j_u, 3, 0U, 0U, 0U, 0),
                  false);
    sf_mex_addfield(c53_g_y, c53_l_y, "position", "position", c53_i1);
    c53_k_u = c53_r0->valid;
    c53_m_y = NULL;
    sf_mex_assign(&c53_m_y, sf_mex_create("y", &c53_k_u, 3, 0U, 0U, 0U, 0),
                  false);
    sf_mex_addfield(c53_g_y, c53_m_y, "valid", "valid", c53_i1);
  }

  sf_mex_setcell(c53_y, 2, c53_g_y);
  c53_b_hoistedGlobal = chartInstance->c53_is_active_c53_LessonII;
  c53_l_u = c53_b_hoistedGlobal;
  c53_n_y = NULL;
  sf_mex_assign(&c53_n_y, sf_mex_create("y", &c53_l_u, 3, 0U, 0U, 0U, 0), false);
  sf_mex_setcell(c53_y, 3, c53_n_y);
  sf_mex_assign(&c53_st, c53_y, false);
  return c53_st;
}

static void set_sim_state_c53_LessonII(SFc53_LessonIIInstanceStruct
  *chartInstance, const mxArray *c53_st)
{
  const mxArray *c53_u;
  c53_Ball c53_r1;
  c53_Player c53_rv0[6];
  int32_T c53_i2;
  chartInstance->c53_doneDoubleBufferReInit = true;
  c53_u = sf_mex_dup(c53_st);
  c53_r1 = c53_g_emlrt_marshallIn(chartInstance, sf_mex_dup(sf_mex_getcell(
    "Ball", c53_u, 0)), "Ball");
  *(int8_T *)&((char_T *)chartInstance->c53_b_Ball)[0] = c53_r1.x;
  *(int8_T *)&((char_T *)chartInstance->c53_b_Ball)[1] = c53_r1.y;
  *(uint8_T *)&((char_T *)chartInstance->c53_b_Ball)[2] = c53_r1.valid;
  *chartInstance->c53_gameOn = c53_emlrt_marshallIn(chartInstance, sf_mex_dup
    (sf_mex_getcell("gameOn", c53_u, 1)), "gameOn");
  c53_c_emlrt_marshallIn(chartInstance, sf_mex_dup(sf_mex_getcell("players",
    c53_u, 2)), "players", c53_rv0);
  for (c53_i2 = 0; c53_i2 < 6; c53_i2++) {
    *(int8_T *)&((char_T *)(c53_Player *)&((char_T *)chartInstance->c53_players)
                 [8 * c53_i2])[0] = c53_rv0[c53_i2].x;
    *(int8_T *)&((char_T *)(c53_Player *)&((char_T *)chartInstance->c53_players)
                 [8 * c53_i2])[1] = c53_rv0[c53_i2].y;
    *(int16_T *)&((char_T *)(c53_Player *)&((char_T *)chartInstance->c53_players)
                  [8 * c53_i2])[2] = c53_rv0[c53_i2].orientation;
    *(uint8_T *)&((char_T *)(c53_Player *)&((char_T *)chartInstance->c53_players)
                  [8 * c53_i2])[4] = c53_rv0[c53_i2].color;
    *(uint8_T *)&((char_T *)(c53_Player *)&((char_T *)chartInstance->c53_players)
                  [8 * c53_i2])[5] = c53_rv0[c53_i2].position;
    *(uint8_T *)&((char_T *)(c53_Player *)&((char_T *)chartInstance->c53_players)
                  [8 * c53_i2])[6] = c53_rv0[c53_i2].valid;
  }

  chartInstance->c53_is_active_c53_LessonII = c53_emlrt_marshallIn(chartInstance,
    sf_mex_dup(sf_mex_getcell("is_active_c53_LessonII", c53_u, 3)),
    "is_active_c53_LessonII");
  sf_mex_destroy(&c53_u);
  c53_update_debugger_state_c53_LessonII(chartInstance);
  sf_mex_destroy(&c53_st);
}

static void finalize_c53_LessonII(SFc53_LessonIIInstanceStruct *chartInstance)
{
  (void)chartInstance;
}

static void sf_gateway_c53_LessonII(SFc53_LessonIIInstanceStruct *chartInstance)
{
  int32_T c53_i3;
  int32_T c53_i4;
  uint8_T c53_b_message[31];
  uint32_T c53_debug_family_var_map[10];
  c53_Player c53_tempEmpty;
  uint8_T c53_colors[6];
  uint8_T c53_positions[6];
  real_T c53_ii;
  uint8_T c53_c_message[27];
  uint8_T c53_d_message[26];
  uint8_T c53_e_message[24];
  real_T c53_nargin = 1.0;
  real_T c53_nargout = 3.0;
  c53_Ball c53_c_Ball;
  c53_Player c53_b_players[6];
  uint8_T c53_b_gameOn;
  c53_Player c53_b_tempEmpty[6];
  int32_T c53_i5;
  int32_T c53_i6;
  int32_T c53_i7;
  int32_T c53_i8;
  int32_T c53_i9;
  static uint8_T c53_uv0[6] = { 103U, 103U, 103U, 98U, 98U, 98U };

  int32_T c53_i10;
  static uint8_T c53_uv1[6] = { 111U, 100U, 103U, 111U, 100U, 103U };

  int32_T c53_b_ii;
  real_T c53_c_ii;
  int32_T c53_i11;
  uint8_T c53_x[2];
  int16_T c53_y;
  int32_T c53_i12;
  _SFD_SYMBOL_SCOPE_PUSH(0U, 0U);
  _sfTime_ = sf_get_time(chartInstance->S);
  _SFD_CC_CALL(CHART_ENTER_SFUNCTION_TAG, 38U, chartInstance->c53_sfEvent);
  for (c53_i3 = 0; c53_i3 < 31; c53_i3++) {
    _SFD_DATA_RANGE_CHECK((real_T)(*chartInstance->c53_message)[c53_i3], 0U, 1U,
                          0U, chartInstance->c53_sfEvent, false);
  }

  chartInstance->c53_sfEvent = CALL_EVENT;
  _SFD_CC_CALL(CHART_ENTER_DURING_FUNCTION_TAG, 38U, chartInstance->c53_sfEvent);
  for (c53_i4 = 0; c53_i4 < 31; c53_i4++) {
    c53_b_message[c53_i4] = (*chartInstance->c53_message)[c53_i4];
  }

  _SFD_SYMBOL_SCOPE_PUSH_EML(0U, 10U, 13U, c53_debug_family_names,
    c53_debug_family_var_map);
  _SFD_SYMBOL_SCOPE_ADD_EML_IMPORTABLE(&c53_tempEmpty, 0U, c53_j_sf_marshallOut,
    c53_i_sf_marshallIn);
  _SFD_SYMBOL_SCOPE_ADD_EML_IMPORTABLE(c53_colors, 1U, c53_i_sf_marshallOut,
    c53_h_sf_marshallIn);
  _SFD_SYMBOL_SCOPE_ADD_EML_IMPORTABLE(c53_positions, 2U, c53_i_sf_marshallOut,
    c53_h_sf_marshallIn);
  _SFD_SYMBOL_SCOPE_ADD_EML_IMPORTABLE(&c53_ii, 3U, c53_e_sf_marshallOut,
    c53_d_sf_marshallIn);
  _SFD_SYMBOL_SCOPE_ADD_EML_IMPORTABLE(c53_c_message, MAX_uint32_T,
    c53_h_sf_marshallOut, c53_g_sf_marshallIn);
  _SFD_SYMBOL_SCOPE_ADD_EML_IMPORTABLE(c53_d_message, MAX_uint32_T,
    c53_g_sf_marshallOut, c53_f_sf_marshallIn);
  _SFD_SYMBOL_SCOPE_ADD_EML_IMPORTABLE(c53_e_message, MAX_uint32_T,
    c53_f_sf_marshallOut, c53_e_sf_marshallIn);
  _SFD_SYMBOL_SCOPE_ADD_EML_IMPORTABLE(&c53_nargin, 5U, c53_e_sf_marshallOut,
    c53_d_sf_marshallIn);
  _SFD_SYMBOL_SCOPE_ADD_EML_IMPORTABLE(&c53_nargout, 6U, c53_e_sf_marshallOut,
    c53_d_sf_marshallIn);
  _SFD_SYMBOL_SCOPE_ADD_EML(c53_b_message, 4U, c53_d_sf_marshallOut);
  _SFD_SYMBOL_SCOPE_ADD_EML_IMPORTABLE(&c53_c_Ball, 7U, c53_c_sf_marshallOut,
    c53_c_sf_marshallIn);
  _SFD_SYMBOL_SCOPE_ADD_EML_IMPORTABLE(c53_b_players, 8U, c53_b_sf_marshallOut,
    c53_b_sf_marshallIn);
  _SFD_SYMBOL_SCOPE_ADD_EML_IMPORTABLE(&c53_b_gameOn, 9U, c53_sf_marshallOut,
    c53_sf_marshallIn);
  CV_EML_FCN(0, 0);
  _SFD_EML_CALL(0U, chartInstance->c53_sfEvent, 3);
  c53_tempEmpty.x = 0;
  _SFD_EML_CALL(0U, chartInstance->c53_sfEvent, 4);
  c53_tempEmpty.y = 0;
  _SFD_EML_CALL(0U, chartInstance->c53_sfEvent, 5);
  c53_tempEmpty.orientation = 0;
  _SFD_EML_CALL(0U, chartInstance->c53_sfEvent, 6);
  c53_tempEmpty.color = 0U;
  _SFD_EML_CALL(0U, chartInstance->c53_sfEvent, 7);
  c53_tempEmpty.position = 0U;
  _SFD_EML_CALL(0U, chartInstance->c53_sfEvent, 8);
  c53_tempEmpty.valid = 0U;
  _SFD_EML_CALL(0U, chartInstance->c53_sfEvent, 9);
  c53_b_tempEmpty[0] = c53_tempEmpty;
  c53_b_tempEmpty[1] = c53_tempEmpty;
  c53_b_tempEmpty[2] = c53_tempEmpty;
  c53_b_tempEmpty[3] = c53_tempEmpty;
  c53_b_tempEmpty[4] = c53_tempEmpty;
  c53_b_tempEmpty[5] = c53_tempEmpty;
  for (c53_i5 = 0; c53_i5 < 6; c53_i5++) {
    c53_b_players[c53_i5] = c53_b_tempEmpty[c53_i5];
  }

  _SFD_EML_CALL(0U, chartInstance->c53_sfEvent, 11);
  for (c53_i6 = 0; c53_i6 < 27; c53_i6++) {
    c53_c_message[c53_i6] = c53_b_message[c53_i6 + 4];
  }

  _SFD_SYMBOL_SWITCH(4U, 4U);
  _SFD_EML_CALL(0U, chartInstance->c53_sfEvent, 12);
  c53_b_gameOn = c53_c_message[0];
  _SFD_EML_CALL(0U, chartInstance->c53_sfEvent, 13);
  for (c53_i7 = 0; c53_i7 < 26; c53_i7++) {
    c53_d_message[c53_i7] = c53_c_message[c53_i7 + 1];
  }

  _SFD_SYMBOL_SWITCH(4U, 5U);
  _SFD_EML_CALL(0U, chartInstance->c53_sfEvent, 14);
  c53_c_Ball.x = c53_typecast(chartInstance, c53_d_message[0]);
  _SFD_EML_CALL(0U, chartInstance->c53_sfEvent, 15);
  c53_c_Ball.y = c53_typecast(chartInstance, c53_d_message[1]);
  _SFD_EML_CALL(0U, chartInstance->c53_sfEvent, 16);
  c53_c_Ball.valid = 1U;
  _SFD_EML_CALL(0U, chartInstance->c53_sfEvent, 17);
  for (c53_i8 = 0; c53_i8 < 24; c53_i8++) {
    c53_e_message[c53_i8] = c53_d_message[c53_i8 + 2];
  }

  _SFD_SYMBOL_SWITCH(4U, 6U);
  _SFD_EML_CALL(0U, chartInstance->c53_sfEvent, 18);
  for (c53_i9 = 0; c53_i9 < 6; c53_i9++) {
    c53_colors[c53_i9] = c53_uv0[c53_i9];
  }

  _SFD_EML_CALL(0U, chartInstance->c53_sfEvent, 19);
  for (c53_i10 = 0; c53_i10 < 6; c53_i10++) {
    c53_positions[c53_i10] = c53_uv1[c53_i10];
  }

  _SFD_EML_CALL(0U, chartInstance->c53_sfEvent, 20);
  c53_ii = 1.0;
  c53_b_ii = 0;
  while (c53_b_ii < 6) {
    c53_ii = 1.0 + (real_T)c53_b_ii;
    CV_EML_FOR(0, 1, 0, 1);
    _SFD_EML_CALL(0U, chartInstance->c53_sfEvent, 21);
    c53_b_players[_SFD_EML_ARRAY_BOUNDS_CHECK("players", (int32_T)
      _SFD_INTEGER_CHECK("ii", c53_ii), 1, 6, 1, 0) - 1].x = c53_typecast
      (chartInstance, c53_e_message[_SFD_EML_ARRAY_BOUNDS_CHECK("message",
        (int32_T)_SFD_INTEGER_CHECK("(ii-1)*4+1", (c53_ii - 1.0) * 4.0 + 1.0), 1,
        24, 1, 0) - 1]);
    _SFD_EML_CALL(0U, chartInstance->c53_sfEvent, 22);
    c53_b_players[_SFD_EML_ARRAY_BOUNDS_CHECK("players", (int32_T)
      _SFD_INTEGER_CHECK("ii", c53_ii), 1, 6, 1, 0) - 1].y = c53_typecast
      (chartInstance, c53_e_message[_SFD_EML_ARRAY_BOUNDS_CHECK("message",
        (int32_T)_SFD_INTEGER_CHECK("(ii-1)*4+2", (c53_ii - 1.0) * 4.0 + 2.0), 1,
        24, 1, 0) - 1]);
    _SFD_EML_CALL(0U, chartInstance->c53_sfEvent, 23);
    c53_c_ii = (c53_ii - 1.0) * 4.0;
    for (c53_i11 = 0; c53_i11 < 2; c53_i11++) {
      c53_x[c53_i11] = c53_e_message[_SFD_EML_ARRAY_BOUNDS_CHECK("message",
        (int32_T)_SFD_INTEGER_CHECK("(ii-1)*4+3:(ii-1)*4+4", c53_c_ii + (3.0 +
        (real_T)c53_i11)), 1, 24, 1, 0) - 1];
    }

    memcpy(&c53_y, &c53_x[0], (size_t)1 * sizeof(int16_T));
    c53_b_players[_SFD_EML_ARRAY_BOUNDS_CHECK("players", (int32_T)
      _SFD_INTEGER_CHECK("ii", c53_ii), 1, 6, 1, 0) - 1].orientation = c53_y;
    _SFD_EML_CALL(0U, chartInstance->c53_sfEvent, 24);
    c53_b_players[_SFD_EML_ARRAY_BOUNDS_CHECK("players", (int32_T)
      _SFD_INTEGER_CHECK("ii", c53_ii), 1, 6, 1, 0) - 1].color =
      c53_colors[_SFD_EML_ARRAY_BOUNDS_CHECK("colors", (int32_T)
      _SFD_INTEGER_CHECK("ii", c53_ii), 1, 6, 1, 0) - 1];
    _SFD_EML_CALL(0U, chartInstance->c53_sfEvent, 25);
    c53_b_players[_SFD_EML_ARRAY_BOUNDS_CHECK("players", (int32_T)
      _SFD_INTEGER_CHECK("ii", c53_ii), 1, 6, 1, 0) - 1].position =
      c53_positions[_SFD_EML_ARRAY_BOUNDS_CHECK("positions", (int32_T)
      _SFD_INTEGER_CHECK("ii", c53_ii), 1, 6, 1, 0) - 1];
    _SFD_EML_CALL(0U, chartInstance->c53_sfEvent, 26);
    c53_b_players[_SFD_EML_ARRAY_BOUNDS_CHECK("players", (int32_T)
      _SFD_INTEGER_CHECK("ii", c53_ii), 1, 6, 1, 0) - 1].valid = 1U;
    c53_b_ii++;
    _SF_MEX_LISTEN_FOR_CTRL_C(chartInstance->S);
  }

  CV_EML_FOR(0, 1, 0, 0);
  _SFD_EML_CALL(0U, chartInstance->c53_sfEvent, -26);
  _SFD_SYMBOL_SCOPE_POP();
  *(int8_T *)&((char_T *)chartInstance->c53_b_Ball)[0] = c53_c_Ball.x;
  *(int8_T *)&((char_T *)chartInstance->c53_b_Ball)[1] = c53_c_Ball.y;
  *(uint8_T *)&((char_T *)chartInstance->c53_b_Ball)[2] = c53_c_Ball.valid;
  for (c53_i12 = 0; c53_i12 < 6; c53_i12++) {
    *(int8_T *)&((char_T *)(c53_Player *)&((char_T *)chartInstance->c53_players)
                 [8 * c53_i12])[0] = c53_b_players[c53_i12].x;
    *(int8_T *)&((char_T *)(c53_Player *)&((char_T *)chartInstance->c53_players)
                 [8 * c53_i12])[1] = c53_b_players[c53_i12].y;
    *(int16_T *)&((char_T *)(c53_Player *)&((char_T *)chartInstance->c53_players)
                  [8 * c53_i12])[2] = c53_b_players[c53_i12].orientation;
    *(uint8_T *)&((char_T *)(c53_Player *)&((char_T *)chartInstance->c53_players)
                  [8 * c53_i12])[4] = c53_b_players[c53_i12].color;
    *(uint8_T *)&((char_T *)(c53_Player *)&((char_T *)chartInstance->c53_players)
                  [8 * c53_i12])[5] = c53_b_players[c53_i12].position;
    *(uint8_T *)&((char_T *)(c53_Player *)&((char_T *)chartInstance->c53_players)
                  [8 * c53_i12])[6] = c53_b_players[c53_i12].valid;
  }

  *chartInstance->c53_gameOn = c53_b_gameOn;
  _SFD_CC_CALL(EXIT_OUT_OF_FUNCTION_TAG, 38U, chartInstance->c53_sfEvent);
  _SFD_SYMBOL_SCOPE_POP();
  _SFD_CHECK_FOR_STATE_INCONSISTENCY(_LessonIIMachineNumber_,
    chartInstance->chartNumber, chartInstance->instanceNumber);
  _SFD_DATA_RANGE_CHECK((real_T)*chartInstance->c53_gameOn, 3U, 1U, 0U,
                        chartInstance->c53_sfEvent, false);
}

static void mdl_start_c53_LessonII(SFc53_LessonIIInstanceStruct *chartInstance)
{
  (void)chartInstance;
}

static void initSimStructsc53_LessonII(SFc53_LessonIIInstanceStruct
  *chartInstance)
{
  (void)chartInstance;
}

static void init_script_number_translation(uint32_T c53_machineNumber, uint32_T
  c53_chartNumber, uint32_T c53_instanceNumber)
{
  (void)c53_machineNumber;
  (void)c53_chartNumber;
  (void)c53_instanceNumber;
}

static const mxArray *c53_sf_marshallOut(void *chartInstanceVoid, void
  *c53_inData)
{
  const mxArray *c53_mxArrayOutData = NULL;
  uint8_T c53_u;
  const mxArray *c53_y = NULL;
  SFc53_LessonIIInstanceStruct *chartInstance;
  chartInstance = (SFc53_LessonIIInstanceStruct *)chartInstanceVoid;
  c53_mxArrayOutData = NULL;
  c53_u = *(uint8_T *)c53_inData;
  c53_y = NULL;
  sf_mex_assign(&c53_y, sf_mex_create("y", &c53_u, 3, 0U, 0U, 0U, 0), false);
  sf_mex_assign(&c53_mxArrayOutData, c53_y, false);
  return c53_mxArrayOutData;
}

static uint8_T c53_emlrt_marshallIn(SFc53_LessonIIInstanceStruct *chartInstance,
  const mxArray *c53_b_gameOn, const char_T *c53_identifier)
{
  uint8_T c53_y;
  emlrtMsgIdentifier c53_thisId;
  c53_thisId.fIdentifier = c53_identifier;
  c53_thisId.fParent = NULL;
  c53_y = c53_b_emlrt_marshallIn(chartInstance, sf_mex_dup(c53_b_gameOn),
    &c53_thisId);
  sf_mex_destroy(&c53_b_gameOn);
  return c53_y;
}

static uint8_T c53_b_emlrt_marshallIn(SFc53_LessonIIInstanceStruct
  *chartInstance, const mxArray *c53_u, const emlrtMsgIdentifier *c53_parentId)
{
  uint8_T c53_y;
  uint8_T c53_u0;
  (void)chartInstance;
  sf_mex_import(c53_parentId, sf_mex_dup(c53_u), &c53_u0, 1, 3, 0U, 0, 0U, 0);
  c53_y = c53_u0;
  sf_mex_destroy(&c53_u);
  return c53_y;
}

static void c53_sf_marshallIn(void *chartInstanceVoid, const mxArray
  *c53_mxArrayInData, const char_T *c53_varName, void *c53_outData)
{
  const mxArray *c53_b_gameOn;
  const char_T *c53_identifier;
  emlrtMsgIdentifier c53_thisId;
  uint8_T c53_y;
  SFc53_LessonIIInstanceStruct *chartInstance;
  chartInstance = (SFc53_LessonIIInstanceStruct *)chartInstanceVoid;
  c53_b_gameOn = sf_mex_dup(c53_mxArrayInData);
  c53_identifier = c53_varName;
  c53_thisId.fIdentifier = c53_identifier;
  c53_thisId.fParent = NULL;
  c53_y = c53_b_emlrt_marshallIn(chartInstance, sf_mex_dup(c53_b_gameOn),
    &c53_thisId);
  sf_mex_destroy(&c53_b_gameOn);
  *(uint8_T *)c53_outData = c53_y;
  sf_mex_destroy(&c53_mxArrayInData);
}

static const mxArray *c53_b_sf_marshallOut(void *chartInstanceVoid, void
  *c53_inData)
{
  const mxArray *c53_mxArrayOutData;
  int32_T c53_i13;
  c53_Player c53_b_inData[6];
  int32_T c53_i14;
  c53_Player c53_u[6];
  const mxArray *c53_y = NULL;
  static int32_T c53_iv2[1] = { 6 };

  int32_T c53_iv3[1];
  int32_T c53_i15;
  const c53_Player *c53_r2;
  int8_T c53_b_u;
  const mxArray *c53_b_y = NULL;
  int8_T c53_c_u;
  const mxArray *c53_c_y = NULL;
  int16_T c53_d_u;
  const mxArray *c53_d_y = NULL;
  uint8_T c53_e_u;
  const mxArray *c53_e_y = NULL;
  uint8_T c53_f_u;
  const mxArray *c53_f_y = NULL;
  uint8_T c53_g_u;
  const mxArray *c53_g_y = NULL;
  SFc53_LessonIIInstanceStruct *chartInstance;
  chartInstance = (SFc53_LessonIIInstanceStruct *)chartInstanceVoid;
  c53_mxArrayOutData = NULL;
  c53_mxArrayOutData = NULL;
  for (c53_i13 = 0; c53_i13 < 6; c53_i13++) {
    c53_b_inData[c53_i13] = (*(c53_Player (*)[6])c53_inData)[c53_i13];
  }

  for (c53_i14 = 0; c53_i14 < 6; c53_i14++) {
    c53_u[c53_i14] = c53_b_inData[c53_i14];
  }

  c53_y = NULL;
  c53_iv3[0] = c53_iv2[0];
  sf_mex_assign(&c53_y, sf_mex_createstructarray("structure", 1, c53_iv3), false);
  for (c53_i15 = 0; c53_i15 < 6; c53_i15++) {
    c53_r2 = &c53_u[c53_i15];
    c53_b_u = c53_r2->x;
    c53_b_y = NULL;
    sf_mex_assign(&c53_b_y, sf_mex_create("y", &c53_b_u, 2, 0U, 0U, 0U, 0),
                  false);
    sf_mex_addfield(c53_y, c53_b_y, "x", "x", c53_i15);
    c53_c_u = c53_r2->y;
    c53_c_y = NULL;
    sf_mex_assign(&c53_c_y, sf_mex_create("y", &c53_c_u, 2, 0U, 0U, 0U, 0),
                  false);
    sf_mex_addfield(c53_y, c53_c_y, "y", "y", c53_i15);
    c53_d_u = c53_r2->orientation;
    c53_d_y = NULL;
    sf_mex_assign(&c53_d_y, sf_mex_create("y", &c53_d_u, 4, 0U, 0U, 0U, 0),
                  false);
    sf_mex_addfield(c53_y, c53_d_y, "orientation", "orientation", c53_i15);
    c53_e_u = c53_r2->color;
    c53_e_y = NULL;
    sf_mex_assign(&c53_e_y, sf_mex_create("y", &c53_e_u, 3, 0U, 0U, 0U, 0),
                  false);
    sf_mex_addfield(c53_y, c53_e_y, "color", "color", c53_i15);
    c53_f_u = c53_r2->position;
    c53_f_y = NULL;
    sf_mex_assign(&c53_f_y, sf_mex_create("y", &c53_f_u, 3, 0U, 0U, 0U, 0),
                  false);
    sf_mex_addfield(c53_y, c53_f_y, "position", "position", c53_i15);
    c53_g_u = c53_r2->valid;
    c53_g_y = NULL;
    sf_mex_assign(&c53_g_y, sf_mex_create("y", &c53_g_u, 3, 0U, 0U, 0U, 0),
                  false);
    sf_mex_addfield(c53_y, c53_g_y, "valid", "valid", c53_i15);
  }

  sf_mex_assign(&c53_mxArrayOutData, c53_y, false);
  return c53_mxArrayOutData;
}

static void c53_c_emlrt_marshallIn(SFc53_LessonIIInstanceStruct *chartInstance,
  const mxArray *c53_b_players, const char_T *c53_identifier, c53_Player c53_y[6])
{
  emlrtMsgIdentifier c53_thisId;
  c53_thisId.fIdentifier = c53_identifier;
  c53_thisId.fParent = NULL;
  c53_d_emlrt_marshallIn(chartInstance, sf_mex_dup(c53_b_players), &c53_thisId,
    c53_y);
  sf_mex_destroy(&c53_b_players);
}

static void c53_d_emlrt_marshallIn(SFc53_LessonIIInstanceStruct *chartInstance,
  const mxArray *c53_u, const emlrtMsgIdentifier *c53_parentId, c53_Player
  c53_y[6])
{
  static uint32_T c53_uv2[1] = { 6U };

  uint32_T c53_uv3[1];
  emlrtMsgIdentifier c53_thisId;
  static const char * c53_fieldNames[6] = { "x", "y", "orientation", "color",
    "position", "valid" };

  int32_T c53_i16;
  c53_uv3[0] = c53_uv2[0];
  c53_thisId.fParent = c53_parentId;
  sf_mex_check_struct(c53_parentId, c53_u, 6, c53_fieldNames, 1U, c53_uv3);
  for (c53_i16 = 0; c53_i16 < 6; c53_i16++) {
    c53_thisId.fIdentifier = "x";
    c53_y[c53_i16].x = c53_e_emlrt_marshallIn(chartInstance, sf_mex_dup
      (sf_mex_getfield(c53_u, "x", "x", c53_i16)), &c53_thisId);
    c53_thisId.fIdentifier = "y";
    c53_y[c53_i16].y = c53_e_emlrt_marshallIn(chartInstance, sf_mex_dup
      (sf_mex_getfield(c53_u, "y", "y", c53_i16)), &c53_thisId);
    c53_thisId.fIdentifier = "orientation";
    c53_y[c53_i16].orientation = c53_f_emlrt_marshallIn(chartInstance,
      sf_mex_dup(sf_mex_getfield(c53_u, "orientation", "orientation", c53_i16)),
      &c53_thisId);
    c53_thisId.fIdentifier = "color";
    c53_y[c53_i16].color = c53_b_emlrt_marshallIn(chartInstance, sf_mex_dup
      (sf_mex_getfield(c53_u, "color", "color", c53_i16)), &c53_thisId);
    c53_thisId.fIdentifier = "position";
    c53_y[c53_i16].position = c53_b_emlrt_marshallIn(chartInstance, sf_mex_dup
      (sf_mex_getfield(c53_u, "position", "position", c53_i16)), &c53_thisId);
    c53_thisId.fIdentifier = "valid";
    c53_y[c53_i16].valid = c53_b_emlrt_marshallIn(chartInstance, sf_mex_dup
      (sf_mex_getfield(c53_u, "valid", "valid", c53_i16)), &c53_thisId);
  }

  sf_mex_destroy(&c53_u);
}

static int8_T c53_e_emlrt_marshallIn(SFc53_LessonIIInstanceStruct *chartInstance,
  const mxArray *c53_u, const emlrtMsgIdentifier *c53_parentId)
{
  int8_T c53_y;
  int8_T c53_i17;
  (void)chartInstance;
  sf_mex_import(c53_parentId, sf_mex_dup(c53_u), &c53_i17, 1, 2, 0U, 0, 0U, 0);
  c53_y = c53_i17;
  sf_mex_destroy(&c53_u);
  return c53_y;
}

static int16_T c53_f_emlrt_marshallIn(SFc53_LessonIIInstanceStruct
  *chartInstance, const mxArray *c53_u, const emlrtMsgIdentifier *c53_parentId)
{
  int16_T c53_y;
  int16_T c53_i18;
  (void)chartInstance;
  sf_mex_import(c53_parentId, sf_mex_dup(c53_u), &c53_i18, 1, 4, 0U, 0, 0U, 0);
  c53_y = c53_i18;
  sf_mex_destroy(&c53_u);
  return c53_y;
}

static void c53_b_sf_marshallIn(void *chartInstanceVoid, const mxArray
  *c53_mxArrayInData, const char_T *c53_varName, void *c53_outData)
{
  const mxArray *c53_b_players;
  const char_T *c53_identifier;
  emlrtMsgIdentifier c53_thisId;
  c53_Player c53_y[6];
  int32_T c53_i19;
  SFc53_LessonIIInstanceStruct *chartInstance;
  chartInstance = (SFc53_LessonIIInstanceStruct *)chartInstanceVoid;
  c53_b_players = sf_mex_dup(c53_mxArrayInData);
  c53_identifier = c53_varName;
  c53_thisId.fIdentifier = c53_identifier;
  c53_thisId.fParent = NULL;
  c53_d_emlrt_marshallIn(chartInstance, sf_mex_dup(c53_b_players), &c53_thisId,
    c53_y);
  sf_mex_destroy(&c53_b_players);
  for (c53_i19 = 0; c53_i19 < 6; c53_i19++) {
    (*(c53_Player (*)[6])c53_outData)[c53_i19] = c53_y[c53_i19];
  }

  sf_mex_destroy(&c53_mxArrayInData);
}

static const mxArray *c53_c_sf_marshallOut(void *chartInstanceVoid, void
  *c53_inData)
{
  const mxArray *c53_mxArrayOutData;
  c53_Ball c53_u;
  const mxArray *c53_y = NULL;
  int8_T c53_b_u;
  const mxArray *c53_b_y = NULL;
  int8_T c53_c_u;
  const mxArray *c53_c_y = NULL;
  uint8_T c53_d_u;
  const mxArray *c53_d_y = NULL;
  SFc53_LessonIIInstanceStruct *chartInstance;
  chartInstance = (SFc53_LessonIIInstanceStruct *)chartInstanceVoid;
  c53_mxArrayOutData = NULL;
  c53_mxArrayOutData = NULL;
  c53_u = *(c53_Ball *)c53_inData;
  c53_y = NULL;
  sf_mex_assign(&c53_y, sf_mex_createstruct("structure", 2, 1, 1), false);
  c53_b_u = c53_u.x;
  c53_b_y = NULL;
  sf_mex_assign(&c53_b_y, sf_mex_create("y", &c53_b_u, 2, 0U, 0U, 0U, 0), false);
  sf_mex_addfield(c53_y, c53_b_y, "x", "x", 0);
  c53_c_u = c53_u.y;
  c53_c_y = NULL;
  sf_mex_assign(&c53_c_y, sf_mex_create("y", &c53_c_u, 2, 0U, 0U, 0U, 0), false);
  sf_mex_addfield(c53_y, c53_c_y, "y", "y", 0);
  c53_d_u = c53_u.valid;
  c53_d_y = NULL;
  sf_mex_assign(&c53_d_y, sf_mex_create("y", &c53_d_u, 3, 0U, 0U, 0U, 0), false);
  sf_mex_addfield(c53_y, c53_d_y, "valid", "valid", 0);
  sf_mex_assign(&c53_mxArrayOutData, c53_y, false);
  return c53_mxArrayOutData;
}

static c53_Ball c53_g_emlrt_marshallIn(SFc53_LessonIIInstanceStruct
  *chartInstance, const mxArray *c53_c_Ball, const char_T *c53_identifier)
{
  c53_Ball c53_y;
  emlrtMsgIdentifier c53_thisId;
  c53_thisId.fIdentifier = c53_identifier;
  c53_thisId.fParent = NULL;
  c53_y = c53_h_emlrt_marshallIn(chartInstance, sf_mex_dup(c53_c_Ball),
    &c53_thisId);
  sf_mex_destroy(&c53_c_Ball);
  return c53_y;
}

static c53_Ball c53_h_emlrt_marshallIn(SFc53_LessonIIInstanceStruct
  *chartInstance, const mxArray *c53_u, const emlrtMsgIdentifier *c53_parentId)
{
  c53_Ball c53_y;
  emlrtMsgIdentifier c53_thisId;
  static const char * c53_fieldNames[3] = { "x", "y", "valid" };

  c53_thisId.fParent = c53_parentId;
  sf_mex_check_struct(c53_parentId, c53_u, 3, c53_fieldNames, 0U, NULL);
  c53_thisId.fIdentifier = "x";
  c53_y.x = c53_e_emlrt_marshallIn(chartInstance, sf_mex_dup(sf_mex_getfield
    (c53_u, "x", "x", 0)), &c53_thisId);
  c53_thisId.fIdentifier = "y";
  c53_y.y = c53_e_emlrt_marshallIn(chartInstance, sf_mex_dup(sf_mex_getfield
    (c53_u, "y", "y", 0)), &c53_thisId);
  c53_thisId.fIdentifier = "valid";
  c53_y.valid = c53_b_emlrt_marshallIn(chartInstance, sf_mex_dup(sf_mex_getfield
    (c53_u, "valid", "valid", 0)), &c53_thisId);
  sf_mex_destroy(&c53_u);
  return c53_y;
}

static void c53_c_sf_marshallIn(void *chartInstanceVoid, const mxArray
  *c53_mxArrayInData, const char_T *c53_varName, void *c53_outData)
{
  const mxArray *c53_c_Ball;
  const char_T *c53_identifier;
  emlrtMsgIdentifier c53_thisId;
  c53_Ball c53_y;
  SFc53_LessonIIInstanceStruct *chartInstance;
  chartInstance = (SFc53_LessonIIInstanceStruct *)chartInstanceVoid;
  c53_c_Ball = sf_mex_dup(c53_mxArrayInData);
  c53_identifier = c53_varName;
  c53_thisId.fIdentifier = c53_identifier;
  c53_thisId.fParent = NULL;
  c53_y = c53_h_emlrt_marshallIn(chartInstance, sf_mex_dup(c53_c_Ball),
    &c53_thisId);
  sf_mex_destroy(&c53_c_Ball);
  *(c53_Ball *)c53_outData = c53_y;
  sf_mex_destroy(&c53_mxArrayInData);
}

static const mxArray *c53_d_sf_marshallOut(void *chartInstanceVoid, void
  *c53_inData)
{
  const mxArray *c53_mxArrayOutData = NULL;
  int32_T c53_i20;
  uint8_T c53_u[31];
  const mxArray *c53_y = NULL;
  SFc53_LessonIIInstanceStruct *chartInstance;
  chartInstance = (SFc53_LessonIIInstanceStruct *)chartInstanceVoid;
  c53_mxArrayOutData = NULL;
  for (c53_i20 = 0; c53_i20 < 31; c53_i20++) {
    c53_u[c53_i20] = (*(uint8_T (*)[31])c53_inData)[c53_i20];
  }

  c53_y = NULL;
  sf_mex_assign(&c53_y, sf_mex_create("y", c53_u, 3, 0U, 1U, 0U, 2, 1, 31),
                false);
  sf_mex_assign(&c53_mxArrayOutData, c53_y, false);
  return c53_mxArrayOutData;
}

static const mxArray *c53_e_sf_marshallOut(void *chartInstanceVoid, void
  *c53_inData)
{
  const mxArray *c53_mxArrayOutData = NULL;
  real_T c53_u;
  const mxArray *c53_y = NULL;
  SFc53_LessonIIInstanceStruct *chartInstance;
  chartInstance = (SFc53_LessonIIInstanceStruct *)chartInstanceVoid;
  c53_mxArrayOutData = NULL;
  c53_u = *(real_T *)c53_inData;
  c53_y = NULL;
  sf_mex_assign(&c53_y, sf_mex_create("y", &c53_u, 0, 0U, 0U, 0U, 0), false);
  sf_mex_assign(&c53_mxArrayOutData, c53_y, false);
  return c53_mxArrayOutData;
}

static real_T c53_i_emlrt_marshallIn(SFc53_LessonIIInstanceStruct *chartInstance,
  const mxArray *c53_u, const emlrtMsgIdentifier *c53_parentId)
{
  real_T c53_y;
  real_T c53_d0;
  (void)chartInstance;
  sf_mex_import(c53_parentId, sf_mex_dup(c53_u), &c53_d0, 1, 0, 0U, 0, 0U, 0);
  c53_y = c53_d0;
  sf_mex_destroy(&c53_u);
  return c53_y;
}

static void c53_d_sf_marshallIn(void *chartInstanceVoid, const mxArray
  *c53_mxArrayInData, const char_T *c53_varName, void *c53_outData)
{
  const mxArray *c53_nargout;
  const char_T *c53_identifier;
  emlrtMsgIdentifier c53_thisId;
  real_T c53_y;
  SFc53_LessonIIInstanceStruct *chartInstance;
  chartInstance = (SFc53_LessonIIInstanceStruct *)chartInstanceVoid;
  c53_nargout = sf_mex_dup(c53_mxArrayInData);
  c53_identifier = c53_varName;
  c53_thisId.fIdentifier = c53_identifier;
  c53_thisId.fParent = NULL;
  c53_y = c53_i_emlrt_marshallIn(chartInstance, sf_mex_dup(c53_nargout),
    &c53_thisId);
  sf_mex_destroy(&c53_nargout);
  *(real_T *)c53_outData = c53_y;
  sf_mex_destroy(&c53_mxArrayInData);
}

static const mxArray *c53_f_sf_marshallOut(void *chartInstanceVoid, void
  *c53_inData)
{
  const mxArray *c53_mxArrayOutData = NULL;
  int32_T c53_i21;
  uint8_T c53_u[24];
  const mxArray *c53_y = NULL;
  SFc53_LessonIIInstanceStruct *chartInstance;
  chartInstance = (SFc53_LessonIIInstanceStruct *)chartInstanceVoid;
  c53_mxArrayOutData = NULL;
  for (c53_i21 = 0; c53_i21 < 24; c53_i21++) {
    c53_u[c53_i21] = (*(uint8_T (*)[24])c53_inData)[c53_i21];
  }

  c53_y = NULL;
  sf_mex_assign(&c53_y, sf_mex_create("y", c53_u, 3, 0U, 1U, 0U, 2, 1, 24),
                false);
  sf_mex_assign(&c53_mxArrayOutData, c53_y, false);
  return c53_mxArrayOutData;
}

static void c53_j_emlrt_marshallIn(SFc53_LessonIIInstanceStruct *chartInstance,
  const mxArray *c53_u, const emlrtMsgIdentifier *c53_parentId, uint8_T c53_y[24])
{
  uint8_T c53_uv4[24];
  int32_T c53_i22;
  (void)chartInstance;
  sf_mex_import(c53_parentId, sf_mex_dup(c53_u), c53_uv4, 1, 3, 0U, 1, 0U, 2, 1,
                24);
  for (c53_i22 = 0; c53_i22 < 24; c53_i22++) {
    c53_y[c53_i22] = c53_uv4[c53_i22];
  }

  sf_mex_destroy(&c53_u);
}

static void c53_e_sf_marshallIn(void *chartInstanceVoid, const mxArray
  *c53_mxArrayInData, const char_T *c53_varName, void *c53_outData)
{
  const mxArray *c53_b_message;
  const char_T *c53_identifier;
  emlrtMsgIdentifier c53_thisId;
  uint8_T c53_y[24];
  int32_T c53_i23;
  SFc53_LessonIIInstanceStruct *chartInstance;
  chartInstance = (SFc53_LessonIIInstanceStruct *)chartInstanceVoid;
  c53_b_message = sf_mex_dup(c53_mxArrayInData);
  c53_identifier = c53_varName;
  c53_thisId.fIdentifier = c53_identifier;
  c53_thisId.fParent = NULL;
  c53_j_emlrt_marshallIn(chartInstance, sf_mex_dup(c53_b_message), &c53_thisId,
    c53_y);
  sf_mex_destroy(&c53_b_message);
  for (c53_i23 = 0; c53_i23 < 24; c53_i23++) {
    (*(uint8_T (*)[24])c53_outData)[c53_i23] = c53_y[c53_i23];
  }

  sf_mex_destroy(&c53_mxArrayInData);
}

static const mxArray *c53_g_sf_marshallOut(void *chartInstanceVoid, void
  *c53_inData)
{
  const mxArray *c53_mxArrayOutData = NULL;
  int32_T c53_i24;
  uint8_T c53_u[26];
  const mxArray *c53_y = NULL;
  SFc53_LessonIIInstanceStruct *chartInstance;
  chartInstance = (SFc53_LessonIIInstanceStruct *)chartInstanceVoid;
  c53_mxArrayOutData = NULL;
  for (c53_i24 = 0; c53_i24 < 26; c53_i24++) {
    c53_u[c53_i24] = (*(uint8_T (*)[26])c53_inData)[c53_i24];
  }

  c53_y = NULL;
  sf_mex_assign(&c53_y, sf_mex_create("y", c53_u, 3, 0U, 1U, 0U, 2, 1, 26),
                false);
  sf_mex_assign(&c53_mxArrayOutData, c53_y, false);
  return c53_mxArrayOutData;
}

static void c53_k_emlrt_marshallIn(SFc53_LessonIIInstanceStruct *chartInstance,
  const mxArray *c53_u, const emlrtMsgIdentifier *c53_parentId, uint8_T c53_y[26])
{
  uint8_T c53_uv5[26];
  int32_T c53_i25;
  (void)chartInstance;
  sf_mex_import(c53_parentId, sf_mex_dup(c53_u), c53_uv5, 1, 3, 0U, 1, 0U, 2, 1,
                26);
  for (c53_i25 = 0; c53_i25 < 26; c53_i25++) {
    c53_y[c53_i25] = c53_uv5[c53_i25];
  }

  sf_mex_destroy(&c53_u);
}

static void c53_f_sf_marshallIn(void *chartInstanceVoid, const mxArray
  *c53_mxArrayInData, const char_T *c53_varName, void *c53_outData)
{
  const mxArray *c53_b_message;
  const char_T *c53_identifier;
  emlrtMsgIdentifier c53_thisId;
  uint8_T c53_y[26];
  int32_T c53_i26;
  SFc53_LessonIIInstanceStruct *chartInstance;
  chartInstance = (SFc53_LessonIIInstanceStruct *)chartInstanceVoid;
  c53_b_message = sf_mex_dup(c53_mxArrayInData);
  c53_identifier = c53_varName;
  c53_thisId.fIdentifier = c53_identifier;
  c53_thisId.fParent = NULL;
  c53_k_emlrt_marshallIn(chartInstance, sf_mex_dup(c53_b_message), &c53_thisId,
    c53_y);
  sf_mex_destroy(&c53_b_message);
  for (c53_i26 = 0; c53_i26 < 26; c53_i26++) {
    (*(uint8_T (*)[26])c53_outData)[c53_i26] = c53_y[c53_i26];
  }

  sf_mex_destroy(&c53_mxArrayInData);
}

static const mxArray *c53_h_sf_marshallOut(void *chartInstanceVoid, void
  *c53_inData)
{
  const mxArray *c53_mxArrayOutData = NULL;
  int32_T c53_i27;
  uint8_T c53_u[27];
  const mxArray *c53_y = NULL;
  SFc53_LessonIIInstanceStruct *chartInstance;
  chartInstance = (SFc53_LessonIIInstanceStruct *)chartInstanceVoid;
  c53_mxArrayOutData = NULL;
  for (c53_i27 = 0; c53_i27 < 27; c53_i27++) {
    c53_u[c53_i27] = (*(uint8_T (*)[27])c53_inData)[c53_i27];
  }

  c53_y = NULL;
  sf_mex_assign(&c53_y, sf_mex_create("y", c53_u, 3, 0U, 1U, 0U, 2, 1, 27),
                false);
  sf_mex_assign(&c53_mxArrayOutData, c53_y, false);
  return c53_mxArrayOutData;
}

static void c53_l_emlrt_marshallIn(SFc53_LessonIIInstanceStruct *chartInstance,
  const mxArray *c53_u, const emlrtMsgIdentifier *c53_parentId, uint8_T c53_y[27])
{
  uint8_T c53_uv6[27];
  int32_T c53_i28;
  (void)chartInstance;
  sf_mex_import(c53_parentId, sf_mex_dup(c53_u), c53_uv6, 1, 3, 0U, 1, 0U, 2, 1,
                27);
  for (c53_i28 = 0; c53_i28 < 27; c53_i28++) {
    c53_y[c53_i28] = c53_uv6[c53_i28];
  }

  sf_mex_destroy(&c53_u);
}

static void c53_g_sf_marshallIn(void *chartInstanceVoid, const mxArray
  *c53_mxArrayInData, const char_T *c53_varName, void *c53_outData)
{
  const mxArray *c53_b_message;
  const char_T *c53_identifier;
  emlrtMsgIdentifier c53_thisId;
  uint8_T c53_y[27];
  int32_T c53_i29;
  SFc53_LessonIIInstanceStruct *chartInstance;
  chartInstance = (SFc53_LessonIIInstanceStruct *)chartInstanceVoid;
  c53_b_message = sf_mex_dup(c53_mxArrayInData);
  c53_identifier = c53_varName;
  c53_thisId.fIdentifier = c53_identifier;
  c53_thisId.fParent = NULL;
  c53_l_emlrt_marshallIn(chartInstance, sf_mex_dup(c53_b_message), &c53_thisId,
    c53_y);
  sf_mex_destroy(&c53_b_message);
  for (c53_i29 = 0; c53_i29 < 27; c53_i29++) {
    (*(uint8_T (*)[27])c53_outData)[c53_i29] = c53_y[c53_i29];
  }

  sf_mex_destroy(&c53_mxArrayInData);
}

static const mxArray *c53_i_sf_marshallOut(void *chartInstanceVoid, void
  *c53_inData)
{
  const mxArray *c53_mxArrayOutData = NULL;
  int32_T c53_i30;
  uint8_T c53_u[6];
  const mxArray *c53_y = NULL;
  SFc53_LessonIIInstanceStruct *chartInstance;
  chartInstance = (SFc53_LessonIIInstanceStruct *)chartInstanceVoid;
  c53_mxArrayOutData = NULL;
  for (c53_i30 = 0; c53_i30 < 6; c53_i30++) {
    c53_u[c53_i30] = (*(uint8_T (*)[6])c53_inData)[c53_i30];
  }

  c53_y = NULL;
  sf_mex_assign(&c53_y, sf_mex_create("y", c53_u, 3, 0U, 1U, 0U, 2, 1, 6), false);
  sf_mex_assign(&c53_mxArrayOutData, c53_y, false);
  return c53_mxArrayOutData;
}

static void c53_m_emlrt_marshallIn(SFc53_LessonIIInstanceStruct *chartInstance,
  const mxArray *c53_u, const emlrtMsgIdentifier *c53_parentId, uint8_T c53_y[6])
{
  uint8_T c53_uv7[6];
  int32_T c53_i31;
  (void)chartInstance;
  sf_mex_import(c53_parentId, sf_mex_dup(c53_u), c53_uv7, 1, 3, 0U, 1, 0U, 2, 1,
                6);
  for (c53_i31 = 0; c53_i31 < 6; c53_i31++) {
    c53_y[c53_i31] = c53_uv7[c53_i31];
  }

  sf_mex_destroy(&c53_u);
}

static void c53_h_sf_marshallIn(void *chartInstanceVoid, const mxArray
  *c53_mxArrayInData, const char_T *c53_varName, void *c53_outData)
{
  const mxArray *c53_positions;
  const char_T *c53_identifier;
  emlrtMsgIdentifier c53_thisId;
  uint8_T c53_y[6];
  int32_T c53_i32;
  SFc53_LessonIIInstanceStruct *chartInstance;
  chartInstance = (SFc53_LessonIIInstanceStruct *)chartInstanceVoid;
  c53_positions = sf_mex_dup(c53_mxArrayInData);
  c53_identifier = c53_varName;
  c53_thisId.fIdentifier = c53_identifier;
  c53_thisId.fParent = NULL;
  c53_m_emlrt_marshallIn(chartInstance, sf_mex_dup(c53_positions), &c53_thisId,
    c53_y);
  sf_mex_destroy(&c53_positions);
  for (c53_i32 = 0; c53_i32 < 6; c53_i32++) {
    (*(uint8_T (*)[6])c53_outData)[c53_i32] = c53_y[c53_i32];
  }

  sf_mex_destroy(&c53_mxArrayInData);
}

static const mxArray *c53_j_sf_marshallOut(void *chartInstanceVoid, void
  *c53_inData)
{
  const mxArray *c53_mxArrayOutData;
  c53_Player c53_u;
  const mxArray *c53_y = NULL;
  int8_T c53_b_u;
  const mxArray *c53_b_y = NULL;
  int8_T c53_c_u;
  const mxArray *c53_c_y = NULL;
  int16_T c53_d_u;
  const mxArray *c53_d_y = NULL;
  uint8_T c53_e_u;
  const mxArray *c53_e_y = NULL;
  uint8_T c53_f_u;
  const mxArray *c53_f_y = NULL;
  uint8_T c53_g_u;
  const mxArray *c53_g_y = NULL;
  SFc53_LessonIIInstanceStruct *chartInstance;
  chartInstance = (SFc53_LessonIIInstanceStruct *)chartInstanceVoid;
  c53_mxArrayOutData = NULL;
  c53_mxArrayOutData = NULL;
  c53_u = *(c53_Player *)c53_inData;
  c53_y = NULL;
  sf_mex_assign(&c53_y, sf_mex_createstruct("structure", 2, 1, 1), false);
  c53_b_u = c53_u.x;
  c53_b_y = NULL;
  sf_mex_assign(&c53_b_y, sf_mex_create("y", &c53_b_u, 2, 0U, 0U, 0U, 0), false);
  sf_mex_addfield(c53_y, c53_b_y, "x", "x", 0);
  c53_c_u = c53_u.y;
  c53_c_y = NULL;
  sf_mex_assign(&c53_c_y, sf_mex_create("y", &c53_c_u, 2, 0U, 0U, 0U, 0), false);
  sf_mex_addfield(c53_y, c53_c_y, "y", "y", 0);
  c53_d_u = c53_u.orientation;
  c53_d_y = NULL;
  sf_mex_assign(&c53_d_y, sf_mex_create("y", &c53_d_u, 4, 0U, 0U, 0U, 0), false);
  sf_mex_addfield(c53_y, c53_d_y, "orientation", "orientation", 0);
  c53_e_u = c53_u.color;
  c53_e_y = NULL;
  sf_mex_assign(&c53_e_y, sf_mex_create("y", &c53_e_u, 3, 0U, 0U, 0U, 0), false);
  sf_mex_addfield(c53_y, c53_e_y, "color", "color", 0);
  c53_f_u = c53_u.position;
  c53_f_y = NULL;
  sf_mex_assign(&c53_f_y, sf_mex_create("y", &c53_f_u, 3, 0U, 0U, 0U, 0), false);
  sf_mex_addfield(c53_y, c53_f_y, "position", "position", 0);
  c53_g_u = c53_u.valid;
  c53_g_y = NULL;
  sf_mex_assign(&c53_g_y, sf_mex_create("y", &c53_g_u, 3, 0U, 0U, 0U, 0), false);
  sf_mex_addfield(c53_y, c53_g_y, "valid", "valid", 0);
  sf_mex_assign(&c53_mxArrayOutData, c53_y, false);
  return c53_mxArrayOutData;
}

static c53_Player c53_n_emlrt_marshallIn(SFc53_LessonIIInstanceStruct
  *chartInstance, const mxArray *c53_u, const emlrtMsgIdentifier *c53_parentId)
{
  c53_Player c53_y;
  emlrtMsgIdentifier c53_thisId;
  static const char * c53_fieldNames[6] = { "x", "y", "orientation", "color",
    "position", "valid" };

  c53_thisId.fParent = c53_parentId;
  sf_mex_check_struct(c53_parentId, c53_u, 6, c53_fieldNames, 0U, NULL);
  c53_thisId.fIdentifier = "x";
  c53_y.x = c53_e_emlrt_marshallIn(chartInstance, sf_mex_dup(sf_mex_getfield
    (c53_u, "x", "x", 0)), &c53_thisId);
  c53_thisId.fIdentifier = "y";
  c53_y.y = c53_e_emlrt_marshallIn(chartInstance, sf_mex_dup(sf_mex_getfield
    (c53_u, "y", "y", 0)), &c53_thisId);
  c53_thisId.fIdentifier = "orientation";
  c53_y.orientation = c53_f_emlrt_marshallIn(chartInstance, sf_mex_dup
    (sf_mex_getfield(c53_u, "orientation", "orientation", 0)), &c53_thisId);
  c53_thisId.fIdentifier = "color";
  c53_y.color = c53_b_emlrt_marshallIn(chartInstance, sf_mex_dup(sf_mex_getfield
    (c53_u, "color", "color", 0)), &c53_thisId);
  c53_thisId.fIdentifier = "position";
  c53_y.position = c53_b_emlrt_marshallIn(chartInstance, sf_mex_dup
    (sf_mex_getfield(c53_u, "position", "position", 0)), &c53_thisId);
  c53_thisId.fIdentifier = "valid";
  c53_y.valid = c53_b_emlrt_marshallIn(chartInstance, sf_mex_dup(sf_mex_getfield
    (c53_u, "valid", "valid", 0)), &c53_thisId);
  sf_mex_destroy(&c53_u);
  return c53_y;
}

static void c53_i_sf_marshallIn(void *chartInstanceVoid, const mxArray
  *c53_mxArrayInData, const char_T *c53_varName, void *c53_outData)
{
  const mxArray *c53_tempEmpty;
  const char_T *c53_identifier;
  emlrtMsgIdentifier c53_thisId;
  c53_Player c53_y;
  SFc53_LessonIIInstanceStruct *chartInstance;
  chartInstance = (SFc53_LessonIIInstanceStruct *)chartInstanceVoid;
  c53_tempEmpty = sf_mex_dup(c53_mxArrayInData);
  c53_identifier = c53_varName;
  c53_thisId.fIdentifier = c53_identifier;
  c53_thisId.fParent = NULL;
  c53_y = c53_n_emlrt_marshallIn(chartInstance, sf_mex_dup(c53_tempEmpty),
    &c53_thisId);
  sf_mex_destroy(&c53_tempEmpty);
  *(c53_Player *)c53_outData = c53_y;
  sf_mex_destroy(&c53_mxArrayInData);
}

const mxArray *sf_c53_LessonII_get_eml_resolved_functions_info(void)
{
  const mxArray *c53_nameCaptureInfo = NULL;
  c53_nameCaptureInfo = NULL;
  sf_mex_assign(&c53_nameCaptureInfo, sf_mex_create("nameCaptureInfo", NULL, 0,
    0U, 1U, 0U, 2, 0, 1), false);
  return c53_nameCaptureInfo;
}

static int8_T c53_typecast(SFc53_LessonIIInstanceStruct *chartInstance, uint8_T
  c53_x)
{
  int8_T c53_y;
  (void)chartInstance;
  memcpy(&c53_y, &c53_x, (size_t)1 * sizeof(int8_T));
  return c53_y;
}

static const mxArray *c53_k_sf_marshallOut(void *chartInstanceVoid, void
  *c53_inData)
{
  const mxArray *c53_mxArrayOutData = NULL;
  int32_T c53_u;
  const mxArray *c53_y = NULL;
  SFc53_LessonIIInstanceStruct *chartInstance;
  chartInstance = (SFc53_LessonIIInstanceStruct *)chartInstanceVoid;
  c53_mxArrayOutData = NULL;
  c53_u = *(int32_T *)c53_inData;
  c53_y = NULL;
  sf_mex_assign(&c53_y, sf_mex_create("y", &c53_u, 6, 0U, 0U, 0U, 0), false);
  sf_mex_assign(&c53_mxArrayOutData, c53_y, false);
  return c53_mxArrayOutData;
}

static int32_T c53_o_emlrt_marshallIn(SFc53_LessonIIInstanceStruct
  *chartInstance, const mxArray *c53_u, const emlrtMsgIdentifier *c53_parentId)
{
  int32_T c53_y;
  int32_T c53_i33;
  (void)chartInstance;
  sf_mex_import(c53_parentId, sf_mex_dup(c53_u), &c53_i33, 1, 6, 0U, 0, 0U, 0);
  c53_y = c53_i33;
  sf_mex_destroy(&c53_u);
  return c53_y;
}

static void c53_j_sf_marshallIn(void *chartInstanceVoid, const mxArray
  *c53_mxArrayInData, const char_T *c53_varName, void *c53_outData)
{
  const mxArray *c53_b_sfEvent;
  const char_T *c53_identifier;
  emlrtMsgIdentifier c53_thisId;
  int32_T c53_y;
  SFc53_LessonIIInstanceStruct *chartInstance;
  chartInstance = (SFc53_LessonIIInstanceStruct *)chartInstanceVoid;
  c53_b_sfEvent = sf_mex_dup(c53_mxArrayInData);
  c53_identifier = c53_varName;
  c53_thisId.fIdentifier = c53_identifier;
  c53_thisId.fParent = NULL;
  c53_y = c53_o_emlrt_marshallIn(chartInstance, sf_mex_dup(c53_b_sfEvent),
    &c53_thisId);
  sf_mex_destroy(&c53_b_sfEvent);
  *(int32_T *)c53_outData = c53_y;
  sf_mex_destroy(&c53_mxArrayInData);
}

static const mxArray *c53_Ball_bus_io(void *chartInstanceVoid, void *c53_pData)
{
  const mxArray *c53_mxVal = NULL;
  c53_Ball c53_tmp;
  SFc53_LessonIIInstanceStruct *chartInstance;
  chartInstance = (SFc53_LessonIIInstanceStruct *)chartInstanceVoid;
  c53_mxVal = NULL;
  c53_tmp.x = *(int8_T *)&((char_T *)(c53_Ball *)c53_pData)[0];
  c53_tmp.y = *(int8_T *)&((char_T *)(c53_Ball *)c53_pData)[1];
  c53_tmp.valid = *(uint8_T *)&((char_T *)(c53_Ball *)c53_pData)[2];
  sf_mex_assign(&c53_mxVal, c53_c_sf_marshallOut(chartInstance, &c53_tmp), false);
  return c53_mxVal;
}

static const mxArray *c53_players_bus_io(void *chartInstanceVoid, void
  *c53_pData)
{
  const mxArray *c53_mxVal = NULL;
  int32_T c53_i34;
  c53_Player c53_tmp[6];
  SFc53_LessonIIInstanceStruct *chartInstance;
  chartInstance = (SFc53_LessonIIInstanceStruct *)chartInstanceVoid;
  c53_mxVal = NULL;
  for (c53_i34 = 0; c53_i34 < 6; c53_i34++) {
    c53_tmp[c53_i34].x = *(int8_T *)&((char_T *)(c53_Player *)&((char_T *)
      (c53_Player (*)[6])c53_pData)[8 * c53_i34])[0];
    c53_tmp[c53_i34].y = *(int8_T *)&((char_T *)(c53_Player *)&((char_T *)
      (c53_Player (*)[6])c53_pData)[8 * c53_i34])[1];
    c53_tmp[c53_i34].orientation = *(int16_T *)&((char_T *)(c53_Player *)
      &((char_T *)(c53_Player (*)[6])c53_pData)[8 * c53_i34])[2];
    c53_tmp[c53_i34].color = *(uint8_T *)&((char_T *)(c53_Player *)&((char_T *)
      (c53_Player (*)[6])c53_pData)[8 * c53_i34])[4];
    c53_tmp[c53_i34].position = *(uint8_T *)&((char_T *)(c53_Player *)&((char_T *)
      (c53_Player (*)[6])c53_pData)[8 * c53_i34])[5];
    c53_tmp[c53_i34].valid = *(uint8_T *)&((char_T *)(c53_Player *)&((char_T *)
      (c53_Player (*)[6])c53_pData)[8 * c53_i34])[6];
  }

  sf_mex_assign(&c53_mxVal, c53_b_sf_marshallOut(chartInstance, c53_tmp), false);
  return c53_mxVal;
}

static void init_dsm_address_info(SFc53_LessonIIInstanceStruct *chartInstance)
{
  (void)chartInstance;
}

static void init_simulink_io_address(SFc53_LessonIIInstanceStruct *chartInstance)
{
  chartInstance->c53_message = (uint8_T (*)[31])ssGetInputPortSignal_wrapper
    (chartInstance->S, 0);
  chartInstance->c53_b_Ball = (c53_Ball *)ssGetOutputPortSignal_wrapper
    (chartInstance->S, 1);
  chartInstance->c53_players = (c53_Player (*)[6])ssGetOutputPortSignal_wrapper
    (chartInstance->S, 2);
  chartInstance->c53_gameOn = (uint8_T *)ssGetOutputPortSignal_wrapper
    (chartInstance->S, 3);
}

/* SFunction Glue Code */
#ifdef utFree
#undef utFree
#endif

#ifdef utMalloc
#undef utMalloc
#endif

#ifdef __cplusplus

extern "C" void *utMalloc(size_t size);
extern "C" void utFree(void*);

#else

extern void *utMalloc(size_t size);
extern void utFree(void*);

#endif

void sf_c53_LessonII_get_check_sum(mxArray *plhs[])
{
  ((real_T *)mxGetPr((plhs[0])))[0] = (real_T)(496924623U);
  ((real_T *)mxGetPr((plhs[0])))[1] = (real_T)(214315009U);
  ((real_T *)mxGetPr((plhs[0])))[2] = (real_T)(1293669646U);
  ((real_T *)mxGetPr((plhs[0])))[3] = (real_T)(4138820961U);
}

mxArray* sf_c53_LessonII_get_post_codegen_info(void);
mxArray *sf_c53_LessonII_get_autoinheritance_info(void)
{
  const char *autoinheritanceFields[] = { "checksum", "inputs", "parameters",
    "outputs", "locals", "postCodegenInfo" };

  mxArray *mxAutoinheritanceInfo = mxCreateStructMatrix(1, 1, sizeof
    (autoinheritanceFields)/sizeof(autoinheritanceFields[0]),
    autoinheritanceFields);

  {
    mxArray *mxChecksum = mxCreateString("Jy2Iko6tMZGMZeDXqZSACD");
    mxSetField(mxAutoinheritanceInfo,0,"checksum",mxChecksum);
  }

  {
    const char *dataFields[] = { "size", "type", "complexity" };

    mxArray *mxData = mxCreateStructMatrix(1,1,3,dataFields);

    {
      mxArray *mxSize = mxCreateDoubleMatrix(1,2,mxREAL);
      double *pr = mxGetPr(mxSize);
      pr[0] = (double)(1);
      pr[1] = (double)(31);
      mxSetField(mxData,0,"size",mxSize);
    }

    {
      const char *typeFields[] = { "base", "fixpt", "isFixedPointType" };

      mxArray *mxType = mxCreateStructMatrix(1,1,sizeof(typeFields)/sizeof
        (typeFields[0]),typeFields);
      mxSetField(mxType,0,"base",mxCreateDoubleScalar(3));
      mxSetField(mxType,0,"fixpt",mxCreateDoubleMatrix(0,0,mxREAL));
      mxSetField(mxType,0,"isFixedPointType",mxCreateDoubleScalar(0));
      mxSetField(mxData,0,"type",mxType);
    }

    mxSetField(mxData,0,"complexity",mxCreateDoubleScalar(0));
    mxSetField(mxAutoinheritanceInfo,0,"inputs",mxData);
  }

  {
    mxSetField(mxAutoinheritanceInfo,0,"parameters",mxCreateDoubleMatrix(0,0,
                mxREAL));
  }

  {
    const char *dataFields[] = { "size", "type", "complexity" };

    mxArray *mxData = mxCreateStructMatrix(1,3,3,dataFields);

    {
      mxArray *mxSize = mxCreateDoubleMatrix(1,2,mxREAL);
      double *pr = mxGetPr(mxSize);
      pr[0] = (double)(1);
      pr[1] = (double)(1);
      mxSetField(mxData,0,"size",mxSize);
    }

    {
      const char *typeFields[] = { "base", "fixpt", "isFixedPointType" };

      mxArray *mxType = mxCreateStructMatrix(1,1,sizeof(typeFields)/sizeof
        (typeFields[0]),typeFields);
      mxSetField(mxType,0,"base",mxCreateDoubleScalar(13));
      mxSetField(mxType,0,"fixpt",mxCreateDoubleMatrix(0,0,mxREAL));
      mxSetField(mxType,0,"isFixedPointType",mxCreateDoubleScalar(0));
      mxSetField(mxData,0,"type",mxType);
    }

    mxSetField(mxData,0,"complexity",mxCreateDoubleScalar(0));

    {
      mxArray *mxSize = mxCreateDoubleMatrix(1,2,mxREAL);
      double *pr = mxGetPr(mxSize);
      pr[0] = (double)(6);
      pr[1] = (double)(1);
      mxSetField(mxData,1,"size",mxSize);
    }

    {
      const char *typeFields[] = { "base", "fixpt", "isFixedPointType" };

      mxArray *mxType = mxCreateStructMatrix(1,1,sizeof(typeFields)/sizeof
        (typeFields[0]),typeFields);
      mxSetField(mxType,0,"base",mxCreateDoubleScalar(13));
      mxSetField(mxType,0,"fixpt",mxCreateDoubleMatrix(0,0,mxREAL));
      mxSetField(mxType,0,"isFixedPointType",mxCreateDoubleScalar(0));
      mxSetField(mxData,1,"type",mxType);
    }

    mxSetField(mxData,1,"complexity",mxCreateDoubleScalar(0));

    {
      mxArray *mxSize = mxCreateDoubleMatrix(1,2,mxREAL);
      double *pr = mxGetPr(mxSize);
      pr[0] = (double)(1);
      pr[1] = (double)(1);
      mxSetField(mxData,2,"size",mxSize);
    }

    {
      const char *typeFields[] = { "base", "fixpt", "isFixedPointType" };

      mxArray *mxType = mxCreateStructMatrix(1,1,sizeof(typeFields)/sizeof
        (typeFields[0]),typeFields);
      mxSetField(mxType,0,"base",mxCreateDoubleScalar(3));
      mxSetField(mxType,0,"fixpt",mxCreateDoubleMatrix(0,0,mxREAL));
      mxSetField(mxType,0,"isFixedPointType",mxCreateDoubleScalar(0));
      mxSetField(mxData,2,"type",mxType);
    }

    mxSetField(mxData,2,"complexity",mxCreateDoubleScalar(0));
    mxSetField(mxAutoinheritanceInfo,0,"outputs",mxData);
  }

  {
    mxSetField(mxAutoinheritanceInfo,0,"locals",mxCreateDoubleMatrix(0,0,mxREAL));
  }

  {
    mxArray* mxPostCodegenInfo = sf_c53_LessonII_get_post_codegen_info();
    mxSetField(mxAutoinheritanceInfo,0,"postCodegenInfo",mxPostCodegenInfo);
  }

  return(mxAutoinheritanceInfo);
}

mxArray *sf_c53_LessonII_third_party_uses_info(void)
{
  mxArray * mxcell3p = mxCreateCellMatrix(1,0);
  return(mxcell3p);
}

mxArray *sf_c53_LessonII_jit_fallback_info(void)
{
  const char *infoFields[] = { "fallbackType", "fallbackReason",
    "hiddenFallbackType", "hiddenFallbackReason", "incompatibleSymbol" };

  mxArray *mxInfo = mxCreateStructMatrix(1, 1, 5, infoFields);
  mxArray *fallbackType = mxCreateString("late");
  mxArray *fallbackReason = mxCreateString("ir_function_calls");
  mxArray *hiddenFallbackType = mxCreateString("");
  mxArray *hiddenFallbackReason = mxCreateString("");
  mxArray *incompatibleSymbol = mxCreateString("memcpy");
  mxSetField(mxInfo, 0, infoFields[0], fallbackType);
  mxSetField(mxInfo, 0, infoFields[1], fallbackReason);
  mxSetField(mxInfo, 0, infoFields[2], hiddenFallbackType);
  mxSetField(mxInfo, 0, infoFields[3], hiddenFallbackReason);
  mxSetField(mxInfo, 0, infoFields[4], incompatibleSymbol);
  return mxInfo;
}

mxArray *sf_c53_LessonII_updateBuildInfo_args_info(void)
{
  mxArray *mxBIArgs = mxCreateCellMatrix(1,0);
  return mxBIArgs;
}

mxArray* sf_c53_LessonII_get_post_codegen_info(void)
{
  const char* fieldNames[] = { "exportedFunctionsUsedByThisChart",
    "exportedFunctionsChecksum" };

  mwSize dims[2] = { 1, 1 };

  mxArray* mxPostCodegenInfo = mxCreateStructArray(2, dims, sizeof(fieldNames)/
    sizeof(fieldNames[0]), fieldNames);

  {
    mxArray* mxExportedFunctionsChecksum = mxCreateString("");
    mwSize exp_dims[2] = { 0, 1 };

    mxArray* mxExportedFunctionsUsedByThisChart = mxCreateCellArray(2, exp_dims);
    mxSetField(mxPostCodegenInfo, 0, "exportedFunctionsUsedByThisChart",
               mxExportedFunctionsUsedByThisChart);
    mxSetField(mxPostCodegenInfo, 0, "exportedFunctionsChecksum",
               mxExportedFunctionsChecksum);
  }

  return mxPostCodegenInfo;
}

static const mxArray *sf_get_sim_state_info_c53_LessonII(void)
{
  const char *infoFields[] = { "chartChecksum", "varInfo" };

  mxArray *mxInfo = mxCreateStructMatrix(1, 1, 2, infoFields);
  const char *infoEncStr[] = {
    "100 S1x4'type','srcId','name','auxInfo'{{M[1],M[5],T\"Ball\",},{M[1],M[9],T\"gameOn\",},{M[1],M[8],T\"players\",},{M[8],M[0],T\"is_active_c53_LessonII\",}}"
  };

  mxArray *mxVarInfo = sf_mex_decode_encoded_mx_struct_array(infoEncStr, 4, 10);
  mxArray *mxChecksum = mxCreateDoubleMatrix(1, 4, mxREAL);
  sf_c53_LessonII_get_check_sum(&mxChecksum);
  mxSetField(mxInfo, 0, infoFields[0], mxChecksum);
  mxSetField(mxInfo, 0, infoFields[1], mxVarInfo);
  return mxInfo;
}

static void chart_debug_initialization(SimStruct *S, unsigned int
  fullDebuggerInitialization)
{
  if (!sim_mode_is_rtw_gen(S)) {
    SFc53_LessonIIInstanceStruct *chartInstance;
    ChartRunTimeInfo * crtInfo = (ChartRunTimeInfo *)(ssGetUserData(S));
    ChartInfoStruct * chartInfo = (ChartInfoStruct *)(crtInfo->instanceInfo);
    chartInstance = (SFc53_LessonIIInstanceStruct *) chartInfo->chartInstance;
    if (ssIsFirstInitCond(S) && fullDebuggerInitialization==1) {
      /* do this only if simulation is starting */
      {
        unsigned int chartAlreadyPresent;
        chartAlreadyPresent = sf_debug_initialize_chart
          (sfGlobalDebugInstanceStruct,
           _LessonIIMachineNumber_,
           53,
           1,
           1,
           0,
           4,
           0,
           0,
           0,
           0,
           0,
           &(chartInstance->chartNumber),
           &(chartInstance->instanceNumber),
           (void *)S);

        /* Each instance must initialize its own list of scripts */
        init_script_number_translation(_LessonIIMachineNumber_,
          chartInstance->chartNumber,chartInstance->instanceNumber);
        if (chartAlreadyPresent==0) {
          /* this is the first instance */
          sf_debug_set_chart_disable_implicit_casting
            (sfGlobalDebugInstanceStruct,_LessonIIMachineNumber_,
             chartInstance->chartNumber,1);
          sf_debug_set_chart_event_thresholds(sfGlobalDebugInstanceStruct,
            _LessonIIMachineNumber_,
            chartInstance->chartNumber,
            0,
            0,
            0);
          _SFD_SET_DATA_PROPS(0,1,1,0,"message");
          _SFD_SET_DATA_PROPS(1,2,0,1,"Ball");
          _SFD_SET_DATA_PROPS(2,2,0,1,"players");
          _SFD_SET_DATA_PROPS(3,2,0,1,"gameOn");
          _SFD_STATE_INFO(0,0,2);
          _SFD_CH_SUBSTATE_COUNT(0);
          _SFD_CH_SUBSTATE_DECOMP(0);
        }

        _SFD_CV_INIT_CHART(0,0,0,0);

        {
          _SFD_CV_INIT_STATE(0,0,0,0,0,0,NULL,NULL);
        }

        _SFD_CV_INIT_TRANS(0,0,NULL,NULL,0,NULL);

        /* Initialization of MATLAB Function Model Coverage */
        _SFD_CV_INIT_EML(0,1,1,0,0,0,0,0,1,0,0,0);
        _SFD_CV_INIT_EML_FCN(0,0,"eML_blk_kernel",0,-1,885);
        _SFD_CV_INIT_EML_FOR(0,1,0,573,584,885);

        {
          unsigned int dimVector[2];
          dimVector[0]= 1;
          dimVector[1]= 31;
          _SFD_SET_DATA_COMPILED_PROPS(0,SF_UINT8,2,&(dimVector[0]),0,0,0,0.0,
            1.0,0,0,(MexFcnForType)c53_d_sf_marshallOut,(MexInFcnForType)NULL);
        }

        _SFD_SET_DATA_COMPILED_PROPS(1,SF_STRUCT,0,NULL,0,0,0,0.0,1.0,0,0,
          (MexFcnForType)c53_Ball_bus_io,(MexInFcnForType)NULL);

        {
          unsigned int dimVector[1];
          dimVector[0]= 6;
          _SFD_SET_DATA_COMPILED_PROPS(2,SF_STRUCT,1,&(dimVector[0]),0,0,0,0.0,
            1.0,0,0,(MexFcnForType)c53_players_bus_io,(MexInFcnForType)NULL);
        }

        _SFD_SET_DATA_COMPILED_PROPS(3,SF_UINT8,0,NULL,0,0,0,0.0,1.0,0,0,
          (MexFcnForType)c53_sf_marshallOut,(MexInFcnForType)c53_sf_marshallIn);
      }
    } else {
      sf_debug_reset_current_state_configuration(sfGlobalDebugInstanceStruct,
        _LessonIIMachineNumber_,chartInstance->chartNumber,
        chartInstance->instanceNumber);
    }
  }
}

static void chart_debug_initialize_data_addresses(SimStruct *S)
{
  if (!sim_mode_is_rtw_gen(S)) {
    SFc53_LessonIIInstanceStruct *chartInstance;
    ChartRunTimeInfo * crtInfo = (ChartRunTimeInfo *)(ssGetUserData(S));
    ChartInfoStruct * chartInfo = (ChartInfoStruct *)(crtInfo->instanceInfo);
    chartInstance = (SFc53_LessonIIInstanceStruct *) chartInfo->chartInstance;
    if (ssIsFirstInitCond(S)) {
      /* do this only if simulation is starting and after we know the addresses of all data */
      {
        _SFD_SET_DATA_VALUE_PTR(0U, *chartInstance->c53_message);
        _SFD_SET_DATA_VALUE_PTR(1U, chartInstance->c53_b_Ball);
        _SFD_SET_DATA_VALUE_PTR(2U, *chartInstance->c53_players);
        _SFD_SET_DATA_VALUE_PTR(3U, chartInstance->c53_gameOn);
      }
    }
  }
}

static const char* sf_get_instance_specialization(void)
{
  return "sELumGX5fhAQC4ek2lInCkD";
}

static void sf_opaque_initialize_c53_LessonII(void *chartInstanceVar)
{
  chart_debug_initialization(((SFc53_LessonIIInstanceStruct*) chartInstanceVar
    )->S,0);
  initialize_params_c53_LessonII((SFc53_LessonIIInstanceStruct*)
    chartInstanceVar);
  initialize_c53_LessonII((SFc53_LessonIIInstanceStruct*) chartInstanceVar);
}

static void sf_opaque_enable_c53_LessonII(void *chartInstanceVar)
{
  enable_c53_LessonII((SFc53_LessonIIInstanceStruct*) chartInstanceVar);
}

static void sf_opaque_disable_c53_LessonII(void *chartInstanceVar)
{
  disable_c53_LessonII((SFc53_LessonIIInstanceStruct*) chartInstanceVar);
}

static void sf_opaque_gateway_c53_LessonII(void *chartInstanceVar)
{
  sf_gateway_c53_LessonII((SFc53_LessonIIInstanceStruct*) chartInstanceVar);
}

static const mxArray* sf_opaque_get_sim_state_c53_LessonII(SimStruct* S)
{
  ChartRunTimeInfo * crtInfo = (ChartRunTimeInfo *)(ssGetUserData(S));
  ChartInfoStruct * chartInfo = (ChartInfoStruct *)(crtInfo->instanceInfo);
  return get_sim_state_c53_LessonII((SFc53_LessonIIInstanceStruct*)
    chartInfo->chartInstance);         /* raw sim ctx */
}

static void sf_opaque_set_sim_state_c53_LessonII(SimStruct* S, const mxArray *st)
{
  ChartRunTimeInfo * crtInfo = (ChartRunTimeInfo *)(ssGetUserData(S));
  ChartInfoStruct * chartInfo = (ChartInfoStruct *)(crtInfo->instanceInfo);
  set_sim_state_c53_LessonII((SFc53_LessonIIInstanceStruct*)
    chartInfo->chartInstance, st);
}

static void sf_opaque_terminate_c53_LessonII(void *chartInstanceVar)
{
  if (chartInstanceVar!=NULL) {
    SimStruct *S = ((SFc53_LessonIIInstanceStruct*) chartInstanceVar)->S;
    ChartRunTimeInfo * crtInfo = (ChartRunTimeInfo *)(ssGetUserData(S));
    if (sim_mode_is_rtw_gen(S) || sim_mode_is_external(S)) {
      sf_clear_rtw_identifier(S);
      unload_LessonII_optimization_info();
    }

    finalize_c53_LessonII((SFc53_LessonIIInstanceStruct*) chartInstanceVar);
    utFree(chartInstanceVar);
    if (crtInfo != NULL) {
      utFree(crtInfo);
    }

    ssSetUserData(S,NULL);
  }
}

static void sf_opaque_init_subchart_simstructs(void *chartInstanceVar)
{
  initSimStructsc53_LessonII((SFc53_LessonIIInstanceStruct*) chartInstanceVar);
}

extern unsigned int sf_machine_global_initializer_called(void);
static void mdlProcessParameters_c53_LessonII(SimStruct *S)
{
  int i;
  for (i=0;i<ssGetNumRunTimeParams(S);i++) {
    if (ssGetSFcnParamTunable(S,i)) {
      ssUpdateDlgParamAsRunTimeParam(S,i);
    }
  }

  if (sf_machine_global_initializer_called()) {
    ChartRunTimeInfo * crtInfo = (ChartRunTimeInfo *)(ssGetUserData(S));
    ChartInfoStruct * chartInfo = (ChartInfoStruct *)(crtInfo->instanceInfo);
    initialize_params_c53_LessonII((SFc53_LessonIIInstanceStruct*)
      (chartInfo->chartInstance));
  }
}

static void mdlSetWorkWidths_c53_LessonII(SimStruct *S)
{
  ssMdlUpdateIsEmpty(S, 1);
  if (sim_mode_is_rtw_gen(S) || sim_mode_is_external(S)) {
    mxArray *infoStruct = load_LessonII_optimization_info();
    int_T chartIsInlinable =
      (int_T)sf_is_chart_inlinable(sf_get_instance_specialization(),infoStruct,
      53);
    ssSetStateflowIsInlinable(S,chartIsInlinable);
    ssSetRTWCG(S,1);
    ssSetEnableFcnIsTrivial(S,1);
    ssSetDisableFcnIsTrivial(S,1);
    ssSetNotMultipleInlinable(S,sf_rtw_info_uint_prop
      (sf_get_instance_specialization(),infoStruct,53,
       "gatewayCannotBeInlinedMultipleTimes"));
    sf_update_buildInfo(sf_get_instance_specialization(),infoStruct,53);
    if (chartIsInlinable) {
      ssSetInputPortOptimOpts(S, 0, SS_REUSABLE_AND_LOCAL);
      sf_mark_chart_expressionable_inputs(S,sf_get_instance_specialization(),
        infoStruct,53,1);
      sf_mark_chart_reusable_outputs(S,sf_get_instance_specialization(),
        infoStruct,53,3);
    }

    {
      unsigned int outPortIdx;
      for (outPortIdx=1; outPortIdx<=3; ++outPortIdx) {
        ssSetOutputPortOptimizeInIR(S, outPortIdx, 1U);
      }
    }

    {
      unsigned int inPortIdx;
      for (inPortIdx=0; inPortIdx < 1; ++inPortIdx) {
        ssSetInputPortOptimizeInIR(S, inPortIdx, 1U);
      }
    }

    sf_set_rtw_dwork_info(S,sf_get_instance_specialization(),infoStruct,53);
    ssSetHasSubFunctions(S,!(chartIsInlinable));
  } else {
  }

  ssSetOptions(S,ssGetOptions(S)|SS_OPTION_WORKS_WITH_CODE_REUSE);
  ssSetChecksum0(S,(2473014197U));
  ssSetChecksum1(S,(1036485692U));
  ssSetChecksum2(S,(2484679956U));
  ssSetChecksum3(S,(2229932412U));
  ssSetmdlDerivatives(S, NULL);
  ssSetExplicitFCSSCtrl(S,1);
  ssSupportsMultipleExecInstances(S,1);
}

static void mdlRTW_c53_LessonII(SimStruct *S)
{
  if (sim_mode_is_rtw_gen(S)) {
    ssWriteRTWStrParam(S, "StateflowChartType", "Embedded MATLAB");
  }
}

static void mdlStart_c53_LessonII(SimStruct *S)
{
  SFc53_LessonIIInstanceStruct *chartInstance;
  ChartRunTimeInfo * crtInfo = (ChartRunTimeInfo *)utMalloc(sizeof
    (ChartRunTimeInfo));
  chartInstance = (SFc53_LessonIIInstanceStruct *)utMalloc(sizeof
    (SFc53_LessonIIInstanceStruct));
  memset(chartInstance, 0, sizeof(SFc53_LessonIIInstanceStruct));
  if (chartInstance==NULL) {
    sf_mex_error_message("Could not allocate memory for chart instance.");
  }

  chartInstance->chartInfo.chartInstance = chartInstance;
  chartInstance->chartInfo.isEMLChart = 1;
  chartInstance->chartInfo.chartInitialized = 0;
  chartInstance->chartInfo.sFunctionGateway = sf_opaque_gateway_c53_LessonII;
  chartInstance->chartInfo.initializeChart = sf_opaque_initialize_c53_LessonII;
  chartInstance->chartInfo.terminateChart = sf_opaque_terminate_c53_LessonII;
  chartInstance->chartInfo.enableChart = sf_opaque_enable_c53_LessonII;
  chartInstance->chartInfo.disableChart = sf_opaque_disable_c53_LessonII;
  chartInstance->chartInfo.getSimState = sf_opaque_get_sim_state_c53_LessonII;
  chartInstance->chartInfo.setSimState = sf_opaque_set_sim_state_c53_LessonII;
  chartInstance->chartInfo.getSimStateInfo = sf_get_sim_state_info_c53_LessonII;
  chartInstance->chartInfo.zeroCrossings = NULL;
  chartInstance->chartInfo.outputs = NULL;
  chartInstance->chartInfo.derivatives = NULL;
  chartInstance->chartInfo.mdlRTW = mdlRTW_c53_LessonII;
  chartInstance->chartInfo.mdlStart = mdlStart_c53_LessonII;
  chartInstance->chartInfo.mdlSetWorkWidths = mdlSetWorkWidths_c53_LessonII;
  chartInstance->chartInfo.extModeExec = NULL;
  chartInstance->chartInfo.restoreLastMajorStepConfiguration = NULL;
  chartInstance->chartInfo.restoreBeforeLastMajorStepConfiguration = NULL;
  chartInstance->chartInfo.storeCurrentConfiguration = NULL;
  chartInstance->chartInfo.callAtomicSubchartUserFcn = NULL;
  chartInstance->chartInfo.callAtomicSubchartAutoFcn = NULL;
  chartInstance->chartInfo.debugInstance = sfGlobalDebugInstanceStruct;
  chartInstance->S = S;
  crtInfo->isEnhancedMooreMachine = 0;
  crtInfo->checksum = SF_RUNTIME_INFO_CHECKSUM;
  crtInfo->fCheckOverflow = sf_runtime_overflow_check_is_on(S);
  crtInfo->instanceInfo = (&(chartInstance->chartInfo));
  crtInfo->isJITEnabled = false;
  crtInfo->compiledInfo = NULL;
  ssSetUserData(S,(void *)(crtInfo));  /* register the chart instance with simstruct */
  init_dsm_address_info(chartInstance);
  init_simulink_io_address(chartInstance);
  if (!sim_mode_is_rtw_gen(S)) {
  }

  chart_debug_initialization(S,1);
}

void c53_LessonII_method_dispatcher(SimStruct *S, int_T method, void *data)
{
  switch (method) {
   case SS_CALL_MDL_START:
    mdlStart_c53_LessonII(S);
    break;

   case SS_CALL_MDL_SET_WORK_WIDTHS:
    mdlSetWorkWidths_c53_LessonII(S);
    break;

   case SS_CALL_MDL_PROCESS_PARAMETERS:
    mdlProcessParameters_c53_LessonII(S);
    break;

   default:
    /* Unhandled method */
    sf_mex_error_message("Stateflow Internal Error:\n"
                         "Error calling c53_LessonII_method_dispatcher.\n"
                         "Can't handle method %d.\n", method);
    break;
  }
}

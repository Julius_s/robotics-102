/* Include files */

#include "speedTest_sfun.h"
#include "speedTest_sfun_debug_macros.h"
#include "c1_speedTest.h"
#include "c2_speedTest.h"
#include "c3_speedTest.h"
#include "c4_speedTest.h"
#include "c7_speedTest.h"
#include "c8_speedTest.h"
#include "c9_speedTest.h"
#include "c10_speedTest.h"
#include "c12_speedTest.h"
#include "c13_speedTest.h"
#include "c20_speedTest.h"
#include "c21_speedTest.h"
#include "c22_speedTest.h"
#include "c30_speedTest.h"
#include "c34_speedTest.h"
#include "c60_speedTest.h"

/* Type Definitions */

/* Named Constants */

/* Variable Declarations */

/* Variable Definitions */
uint32_T _speedTestMachineNumber_;

/* Function Declarations */

/* Function Definitions */
void speedTest_initializer(void)
{
}

void speedTest_terminator(void)
{
}

/* SFunction Glue Code */
unsigned int sf_speedTest_method_dispatcher(SimStruct *simstructPtr, unsigned
  int chartFileNumber, const char* specsCksum, int_T method, void *data)
{
  if (chartFileNumber==1) {
    c1_speedTest_method_dispatcher(simstructPtr, method, data);
    return 1;
  }

  if (chartFileNumber==2) {
    c2_speedTest_method_dispatcher(simstructPtr, method, data);
    return 1;
  }

  if (chartFileNumber==3) {
    c3_speedTest_method_dispatcher(simstructPtr, method, data);
    return 1;
  }

  if (chartFileNumber==4) {
    c4_speedTest_method_dispatcher(simstructPtr, method, data);
    return 1;
  }

  if (chartFileNumber==7) {
    c7_speedTest_method_dispatcher(simstructPtr, method, data);
    return 1;
  }

  if (chartFileNumber==8) {
    c8_speedTest_method_dispatcher(simstructPtr, method, data);
    return 1;
  }

  if (chartFileNumber==9) {
    c9_speedTest_method_dispatcher(simstructPtr, method, data);
    return 1;
  }

  if (chartFileNumber==10) {
    c10_speedTest_method_dispatcher(simstructPtr, method, data);
    return 1;
  }

  if (chartFileNumber==12) {
    c12_speedTest_method_dispatcher(simstructPtr, method, data);
    return 1;
  }

  if (chartFileNumber==13) {
    c13_speedTest_method_dispatcher(simstructPtr, method, data);
    return 1;
  }

  if (chartFileNumber==20) {
    c20_speedTest_method_dispatcher(simstructPtr, method, data);
    return 1;
  }

  if (chartFileNumber==21) {
    c21_speedTest_method_dispatcher(simstructPtr, method, data);
    return 1;
  }

  if (chartFileNumber==22) {
    c22_speedTest_method_dispatcher(simstructPtr, method, data);
    return 1;
  }

  if (chartFileNumber==30) {
    c30_speedTest_method_dispatcher(simstructPtr, method, data);
    return 1;
  }

  if (chartFileNumber==34) {
    c34_speedTest_method_dispatcher(simstructPtr, method, data);
    return 1;
  }

  if (chartFileNumber==60) {
    c60_speedTest_method_dispatcher(simstructPtr, method, data);
    return 1;
  }

  return 0;
}

extern void sf_speedTest_uses_exported_functions(int nlhs, mxArray * plhs[], int
  nrhs, const mxArray * prhs[])
{
  plhs[0] = mxCreateLogicalScalar(0);
}

unsigned int sf_speedTest_process_check_sum_call( int nlhs, mxArray * plhs[],
  int nrhs, const mxArray * prhs[] )
{

#ifdef MATLAB_MEX_FILE

  char commandName[20];
  if (nrhs<1 || !mxIsChar(prhs[0]) )
    return 0;

  /* Possible call to get the checksum */
  mxGetString(prhs[0], commandName,sizeof(commandName)/sizeof(char));
  commandName[(sizeof(commandName)/sizeof(char)-1)] = '\0';
  if (strcmp(commandName,"sf_get_check_sum"))
    return 0;
  plhs[0] = mxCreateDoubleMatrix( 1,4,mxREAL);
  if (nrhs>1 && mxIsChar(prhs[1])) {
    mxGetString(prhs[1], commandName,sizeof(commandName)/sizeof(char));
    commandName[(sizeof(commandName)/sizeof(char)-1)] = '\0';
    if (!strcmp(commandName,"machine")) {
      ((real_T *)mxGetPr((plhs[0])))[0] = (real_T)(3482858187U);
      ((real_T *)mxGetPr((plhs[0])))[1] = (real_T)(4207822612U);
      ((real_T *)mxGetPr((plhs[0])))[2] = (real_T)(1017519107U);
      ((real_T *)mxGetPr((plhs[0])))[3] = (real_T)(3792190798U);
    } else if (!strcmp(commandName,"exportedFcn")) {
      ((real_T *)mxGetPr((plhs[0])))[0] = (real_T)(0U);
      ((real_T *)mxGetPr((plhs[0])))[1] = (real_T)(0U);
      ((real_T *)mxGetPr((plhs[0])))[2] = (real_T)(0U);
      ((real_T *)mxGetPr((plhs[0])))[3] = (real_T)(0U);
    } else if (!strcmp(commandName,"makefile")) {
      ((real_T *)mxGetPr((plhs[0])))[0] = (real_T)(1259406959U);
      ((real_T *)mxGetPr((plhs[0])))[1] = (real_T)(3733469414U);
      ((real_T *)mxGetPr((plhs[0])))[2] = (real_T)(196415341U);
      ((real_T *)mxGetPr((plhs[0])))[3] = (real_T)(389151619U);
    } else if (nrhs==3 && !strcmp(commandName,"chart")) {
      unsigned int chartFileNumber;
      chartFileNumber = (unsigned int)mxGetScalar(prhs[2]);
      switch (chartFileNumber) {
       case 1:
        {
          extern void sf_c1_speedTest_get_check_sum(mxArray *plhs[]);
          sf_c1_speedTest_get_check_sum(plhs);
          break;
        }

       case 2:
        {
          extern void sf_c2_speedTest_get_check_sum(mxArray *plhs[]);
          sf_c2_speedTest_get_check_sum(plhs);
          break;
        }

       case 3:
        {
          extern void sf_c3_speedTest_get_check_sum(mxArray *plhs[]);
          sf_c3_speedTest_get_check_sum(plhs);
          break;
        }

       case 4:
        {
          extern void sf_c4_speedTest_get_check_sum(mxArray *plhs[]);
          sf_c4_speedTest_get_check_sum(plhs);
          break;
        }

       case 7:
        {
          extern void sf_c7_speedTest_get_check_sum(mxArray *plhs[]);
          sf_c7_speedTest_get_check_sum(plhs);
          break;
        }

       case 8:
        {
          extern void sf_c8_speedTest_get_check_sum(mxArray *plhs[]);
          sf_c8_speedTest_get_check_sum(plhs);
          break;
        }

       case 9:
        {
          extern void sf_c9_speedTest_get_check_sum(mxArray *plhs[]);
          sf_c9_speedTest_get_check_sum(plhs);
          break;
        }

       case 10:
        {
          extern void sf_c10_speedTest_get_check_sum(mxArray *plhs[]);
          sf_c10_speedTest_get_check_sum(plhs);
          break;
        }

       case 12:
        {
          extern void sf_c12_speedTest_get_check_sum(mxArray *plhs[]);
          sf_c12_speedTest_get_check_sum(plhs);
          break;
        }

       case 13:
        {
          extern void sf_c13_speedTest_get_check_sum(mxArray *plhs[]);
          sf_c13_speedTest_get_check_sum(plhs);
          break;
        }

       case 20:
        {
          extern void sf_c20_speedTest_get_check_sum(mxArray *plhs[]);
          sf_c20_speedTest_get_check_sum(plhs);
          break;
        }

       case 21:
        {
          extern void sf_c21_speedTest_get_check_sum(mxArray *plhs[]);
          sf_c21_speedTest_get_check_sum(plhs);
          break;
        }

       case 22:
        {
          extern void sf_c22_speedTest_get_check_sum(mxArray *plhs[]);
          sf_c22_speedTest_get_check_sum(plhs);
          break;
        }

       case 30:
        {
          extern void sf_c30_speedTest_get_check_sum(mxArray *plhs[]);
          sf_c30_speedTest_get_check_sum(plhs);
          break;
        }

       case 34:
        {
          extern void sf_c34_speedTest_get_check_sum(mxArray *plhs[]);
          sf_c34_speedTest_get_check_sum(plhs);
          break;
        }

       case 60:
        {
          extern void sf_c60_speedTest_get_check_sum(mxArray *plhs[]);
          sf_c60_speedTest_get_check_sum(plhs);
          break;
        }

       default:
        ((real_T *)mxGetPr((plhs[0])))[0] = (real_T)(0.0);
        ((real_T *)mxGetPr((plhs[0])))[1] = (real_T)(0.0);
        ((real_T *)mxGetPr((plhs[0])))[2] = (real_T)(0.0);
        ((real_T *)mxGetPr((plhs[0])))[3] = (real_T)(0.0);
      }
    } else if (!strcmp(commandName,"target")) {
      ((real_T *)mxGetPr((plhs[0])))[0] = (real_T)(3061339410U);
      ((real_T *)mxGetPr((plhs[0])))[1] = (real_T)(1991824845U);
      ((real_T *)mxGetPr((plhs[0])))[2] = (real_T)(3599338742U);
      ((real_T *)mxGetPr((plhs[0])))[3] = (real_T)(2357874978U);
    } else {
      return 0;
    }
  } else {
    ((real_T *)mxGetPr((plhs[0])))[0] = (real_T)(1540291793U);
    ((real_T *)mxGetPr((plhs[0])))[1] = (real_T)(1427822239U);
    ((real_T *)mxGetPr((plhs[0])))[2] = (real_T)(1612184955U);
    ((real_T *)mxGetPr((plhs[0])))[3] = (real_T)(2661561013U);
  }

  return 1;

#else

  return 0;

#endif

}

unsigned int sf_speedTest_autoinheritance_info( int nlhs, mxArray * plhs[], int
  nrhs, const mxArray * prhs[] )
{

#ifdef MATLAB_MEX_FILE

  char commandName[32];
  char aiChksum[64];
  if (nrhs<3 || !mxIsChar(prhs[0]) )
    return 0;

  /* Possible call to get the autoinheritance_info */
  mxGetString(prhs[0], commandName,sizeof(commandName)/sizeof(char));
  commandName[(sizeof(commandName)/sizeof(char)-1)] = '\0';
  if (strcmp(commandName,"get_autoinheritance_info"))
    return 0;
  mxGetString(prhs[2], aiChksum,sizeof(aiChksum)/sizeof(char));
  aiChksum[(sizeof(aiChksum)/sizeof(char)-1)] = '\0';

  {
    unsigned int chartFileNumber;
    chartFileNumber = (unsigned int)mxGetScalar(prhs[1]);
    switch (chartFileNumber) {
     case 1:
      {
        if (strcmp(aiChksum, "y6Xs4WcS8WLRGoiPgg9VIE") == 0) {
          extern mxArray *sf_c1_speedTest_get_autoinheritance_info(void);
          plhs[0] = sf_c1_speedTest_get_autoinheritance_info();
          break;
        }

        plhs[0] = mxCreateDoubleMatrix(0,0,mxREAL);
        break;
      }

     case 2:
      {
        if (strcmp(aiChksum, "kHfYduJyIVyU5ctHVH1c5") == 0) {
          extern mxArray *sf_c2_speedTest_get_autoinheritance_info(void);
          plhs[0] = sf_c2_speedTest_get_autoinheritance_info();
          break;
        }

        plhs[0] = mxCreateDoubleMatrix(0,0,mxREAL);
        break;
      }

     case 3:
      {
        if (strcmp(aiChksum, "UdXCtx3vKeGTbSGWNIJsGB") == 0) {
          extern mxArray *sf_c3_speedTest_get_autoinheritance_info(void);
          plhs[0] = sf_c3_speedTest_get_autoinheritance_info();
          break;
        }

        plhs[0] = mxCreateDoubleMatrix(0,0,mxREAL);
        break;
      }

     case 4:
      {
        if (strcmp(aiChksum, "QbZsr6DW3D03akapgL3ytF") == 0) {
          extern mxArray *sf_c4_speedTest_get_autoinheritance_info(void);
          plhs[0] = sf_c4_speedTest_get_autoinheritance_info();
          break;
        }

        plhs[0] = mxCreateDoubleMatrix(0,0,mxREAL);
        break;
      }

     case 7:
      {
        if (strcmp(aiChksum, "YxuJ3VIU6BKAvA58gYIRPD") == 0) {
          extern mxArray *sf_c7_speedTest_get_autoinheritance_info(void);
          plhs[0] = sf_c7_speedTest_get_autoinheritance_info();
          break;
        }

        plhs[0] = mxCreateDoubleMatrix(0,0,mxREAL);
        break;
      }

     case 8:
      {
        if (strcmp(aiChksum, "XEXsRmqFscIqZkSUMcSdQF") == 0) {
          extern mxArray *sf_c8_speedTest_get_autoinheritance_info(void);
          plhs[0] = sf_c8_speedTest_get_autoinheritance_info();
          break;
        }

        plhs[0] = mxCreateDoubleMatrix(0,0,mxREAL);
        break;
      }

     case 9:
      {
        if (strcmp(aiChksum, "ArIrdR8KduvZC6FfJiWpp") == 0) {
          extern mxArray *sf_c9_speedTest_get_autoinheritance_info(void);
          plhs[0] = sf_c9_speedTest_get_autoinheritance_info();
          break;
        }

        plhs[0] = mxCreateDoubleMatrix(0,0,mxREAL);
        break;
      }

     case 10:
      {
        if (strcmp(aiChksum, "1BPu12ywslFvVuHCbA7wSB") == 0) {
          extern mxArray *sf_c10_speedTest_get_autoinheritance_info(void);
          plhs[0] = sf_c10_speedTest_get_autoinheritance_info();
          break;
        }

        plhs[0] = mxCreateDoubleMatrix(0,0,mxREAL);
        break;
      }

     case 12:
      {
        if (strcmp(aiChksum, "kp5o8oGyeKrNrmZ45YqG3D") == 0) {
          extern mxArray *sf_c12_speedTest_get_autoinheritance_info(void);
          plhs[0] = sf_c12_speedTest_get_autoinheritance_info();
          break;
        }

        plhs[0] = mxCreateDoubleMatrix(0,0,mxREAL);
        break;
      }

     case 13:
      {
        if (strcmp(aiChksum, "avMqIanoQY6XoECCReQu1D") == 0) {
          extern mxArray *sf_c13_speedTest_get_autoinheritance_info(void);
          plhs[0] = sf_c13_speedTest_get_autoinheritance_info();
          break;
        }

        plhs[0] = mxCreateDoubleMatrix(0,0,mxREAL);
        break;
      }

     case 20:
      {
        if (strcmp(aiChksum, "7ivw4cCNB5MBh41uVb3SPB") == 0) {
          extern mxArray *sf_c20_speedTest_get_autoinheritance_info(void);
          plhs[0] = sf_c20_speedTest_get_autoinheritance_info();
          break;
        }

        plhs[0] = mxCreateDoubleMatrix(0,0,mxREAL);
        break;
      }

     case 21:
      {
        if (strcmp(aiChksum, "aeIe7Vb6eDndSoISjztQnH") == 0) {
          extern mxArray *sf_c21_speedTest_get_autoinheritance_info(void);
          plhs[0] = sf_c21_speedTest_get_autoinheritance_info();
          break;
        }

        plhs[0] = mxCreateDoubleMatrix(0,0,mxREAL);
        break;
      }

     case 22:
      {
        if (strcmp(aiChksum, "xTDZbH0g8R2TeJ7jn9aNrF") == 0) {
          extern mxArray *sf_c22_speedTest_get_autoinheritance_info(void);
          plhs[0] = sf_c22_speedTest_get_autoinheritance_info();
          break;
        }

        plhs[0] = mxCreateDoubleMatrix(0,0,mxREAL);
        break;
      }

     case 30:
      {
        if (strcmp(aiChksum, "VqNKr9ToCKKeFTkP3bLInG") == 0) {
          extern mxArray *sf_c30_speedTest_get_autoinheritance_info(void);
          plhs[0] = sf_c30_speedTest_get_autoinheritance_info();
          break;
        }

        plhs[0] = mxCreateDoubleMatrix(0,0,mxREAL);
        break;
      }

     case 34:
      {
        if (strcmp(aiChksum, "2z7Fwh6WZoLlXSSoEUax0D") == 0) {
          extern mxArray *sf_c34_speedTest_get_autoinheritance_info(void);
          plhs[0] = sf_c34_speedTest_get_autoinheritance_info();
          break;
        }

        plhs[0] = mxCreateDoubleMatrix(0,0,mxREAL);
        break;
      }

     case 60:
      {
        if (strcmp(aiChksum, "piRjQqKoVYuQZLYYx7d1EC") == 0) {
          extern mxArray *sf_c60_speedTest_get_autoinheritance_info(void);
          plhs[0] = sf_c60_speedTest_get_autoinheritance_info();
          break;
        }

        plhs[0] = mxCreateDoubleMatrix(0,0,mxREAL);
        break;
      }

     default:
      plhs[0] = mxCreateDoubleMatrix(0,0,mxREAL);
    }
  }

  return 1;

#else

  return 0;

#endif

}

unsigned int sf_speedTest_get_eml_resolved_functions_info( int nlhs, mxArray *
  plhs[], int nrhs, const mxArray * prhs[] )
{

#ifdef MATLAB_MEX_FILE

  char commandName[64];
  if (nrhs<2 || !mxIsChar(prhs[0]))
    return 0;

  /* Possible call to get the get_eml_resolved_functions_info */
  mxGetString(prhs[0], commandName,sizeof(commandName)/sizeof(char));
  commandName[(sizeof(commandName)/sizeof(char)-1)] = '\0';
  if (strcmp(commandName,"get_eml_resolved_functions_info"))
    return 0;

  {
    unsigned int chartFileNumber;
    chartFileNumber = (unsigned int)mxGetScalar(prhs[1]);
    switch (chartFileNumber) {
     case 1:
      {
        extern const mxArray *sf_c1_speedTest_get_eml_resolved_functions_info
          (void);
        mxArray *persistentMxArray = (mxArray *)
          sf_c1_speedTest_get_eml_resolved_functions_info();
        plhs[0] = mxDuplicateArray(persistentMxArray);
        mxDestroyArray(persistentMxArray);
        break;
      }

     case 2:
      {
        extern const mxArray *sf_c2_speedTest_get_eml_resolved_functions_info
          (void);
        mxArray *persistentMxArray = (mxArray *)
          sf_c2_speedTest_get_eml_resolved_functions_info();
        plhs[0] = mxDuplicateArray(persistentMxArray);
        mxDestroyArray(persistentMxArray);
        break;
      }

     case 3:
      {
        extern const mxArray *sf_c3_speedTest_get_eml_resolved_functions_info
          (void);
        mxArray *persistentMxArray = (mxArray *)
          sf_c3_speedTest_get_eml_resolved_functions_info();
        plhs[0] = mxDuplicateArray(persistentMxArray);
        mxDestroyArray(persistentMxArray);
        break;
      }

     case 4:
      {
        extern const mxArray *sf_c4_speedTest_get_eml_resolved_functions_info
          (void);
        mxArray *persistentMxArray = (mxArray *)
          sf_c4_speedTest_get_eml_resolved_functions_info();
        plhs[0] = mxDuplicateArray(persistentMxArray);
        mxDestroyArray(persistentMxArray);
        break;
      }

     case 7:
      {
        extern const mxArray *sf_c7_speedTest_get_eml_resolved_functions_info
          (void);
        mxArray *persistentMxArray = (mxArray *)
          sf_c7_speedTest_get_eml_resolved_functions_info();
        plhs[0] = mxDuplicateArray(persistentMxArray);
        mxDestroyArray(persistentMxArray);
        break;
      }

     case 8:
      {
        extern const mxArray *sf_c8_speedTest_get_eml_resolved_functions_info
          (void);
        mxArray *persistentMxArray = (mxArray *)
          sf_c8_speedTest_get_eml_resolved_functions_info();
        plhs[0] = mxDuplicateArray(persistentMxArray);
        mxDestroyArray(persistentMxArray);
        break;
      }

     case 9:
      {
        extern const mxArray *sf_c9_speedTest_get_eml_resolved_functions_info
          (void);
        mxArray *persistentMxArray = (mxArray *)
          sf_c9_speedTest_get_eml_resolved_functions_info();
        plhs[0] = mxDuplicateArray(persistentMxArray);
        mxDestroyArray(persistentMxArray);
        break;
      }

     case 10:
      {
        extern const mxArray *sf_c10_speedTest_get_eml_resolved_functions_info
          (void);
        mxArray *persistentMxArray = (mxArray *)
          sf_c10_speedTest_get_eml_resolved_functions_info();
        plhs[0] = mxDuplicateArray(persistentMxArray);
        mxDestroyArray(persistentMxArray);
        break;
      }

     case 12:
      {
        extern const mxArray *sf_c12_speedTest_get_eml_resolved_functions_info
          (void);
        mxArray *persistentMxArray = (mxArray *)
          sf_c12_speedTest_get_eml_resolved_functions_info();
        plhs[0] = mxDuplicateArray(persistentMxArray);
        mxDestroyArray(persistentMxArray);
        break;
      }

     case 13:
      {
        extern const mxArray *sf_c13_speedTest_get_eml_resolved_functions_info
          (void);
        mxArray *persistentMxArray = (mxArray *)
          sf_c13_speedTest_get_eml_resolved_functions_info();
        plhs[0] = mxDuplicateArray(persistentMxArray);
        mxDestroyArray(persistentMxArray);
        break;
      }

     case 20:
      {
        extern const mxArray *sf_c20_speedTest_get_eml_resolved_functions_info
          (void);
        mxArray *persistentMxArray = (mxArray *)
          sf_c20_speedTest_get_eml_resolved_functions_info();
        plhs[0] = mxDuplicateArray(persistentMxArray);
        mxDestroyArray(persistentMxArray);
        break;
      }

     case 21:
      {
        extern const mxArray *sf_c21_speedTest_get_eml_resolved_functions_info
          (void);
        mxArray *persistentMxArray = (mxArray *)
          sf_c21_speedTest_get_eml_resolved_functions_info();
        plhs[0] = mxDuplicateArray(persistentMxArray);
        mxDestroyArray(persistentMxArray);
        break;
      }

     case 22:
      {
        extern const mxArray *sf_c22_speedTest_get_eml_resolved_functions_info
          (void);
        mxArray *persistentMxArray = (mxArray *)
          sf_c22_speedTest_get_eml_resolved_functions_info();
        plhs[0] = mxDuplicateArray(persistentMxArray);
        mxDestroyArray(persistentMxArray);
        break;
      }

     case 30:
      {
        extern const mxArray *sf_c30_speedTest_get_eml_resolved_functions_info
          (void);
        mxArray *persistentMxArray = (mxArray *)
          sf_c30_speedTest_get_eml_resolved_functions_info();
        plhs[0] = mxDuplicateArray(persistentMxArray);
        mxDestroyArray(persistentMxArray);
        break;
      }

     case 34:
      {
        extern const mxArray *sf_c34_speedTest_get_eml_resolved_functions_info
          (void);
        mxArray *persistentMxArray = (mxArray *)
          sf_c34_speedTest_get_eml_resolved_functions_info();
        plhs[0] = mxDuplicateArray(persistentMxArray);
        mxDestroyArray(persistentMxArray);
        break;
      }

     case 60:
      {
        extern const mxArray *sf_c60_speedTest_get_eml_resolved_functions_info
          (void);
        mxArray *persistentMxArray = (mxArray *)
          sf_c60_speedTest_get_eml_resolved_functions_info();
        plhs[0] = mxDuplicateArray(persistentMxArray);
        mxDestroyArray(persistentMxArray);
        break;
      }

     default:
      plhs[0] = mxCreateDoubleMatrix(0,0,mxREAL);
    }
  }

  return 1;

#else

  return 0;

#endif

}

unsigned int sf_speedTest_third_party_uses_info( int nlhs, mxArray * plhs[], int
  nrhs, const mxArray * prhs[] )
{
  char commandName[64];
  char tpChksum[64];
  if (nrhs<3 || !mxIsChar(prhs[0]))
    return 0;

  /* Possible call to get the third_party_uses_info */
  mxGetString(prhs[0], commandName,sizeof(commandName)/sizeof(char));
  commandName[(sizeof(commandName)/sizeof(char)-1)] = '\0';
  mxGetString(prhs[2], tpChksum,sizeof(tpChksum)/sizeof(char));
  tpChksum[(sizeof(tpChksum)/sizeof(char)-1)] = '\0';
  if (strcmp(commandName,"get_third_party_uses_info"))
    return 0;

  {
    unsigned int chartFileNumber;
    chartFileNumber = (unsigned int)mxGetScalar(prhs[1]);
    switch (chartFileNumber) {
     case 1:
      {
        if (strcmp(tpChksum, "77qs0O1aFQP9R5KVWKglD") == 0) {
          extern mxArray *sf_c1_speedTest_third_party_uses_info(void);
          plhs[0] = sf_c1_speedTest_third_party_uses_info();
          break;
        }
      }

     case 2:
      {
        if (strcmp(tpChksum, "BPbr6mnccR4iiqbbOejEIC") == 0) {
          extern mxArray *sf_c2_speedTest_third_party_uses_info(void);
          plhs[0] = sf_c2_speedTest_third_party_uses_info();
          break;
        }
      }

     case 3:
      {
        if (strcmp(tpChksum, "gblOQFxJdbh8vdZcLHybtE") == 0) {
          extern mxArray *sf_c3_speedTest_third_party_uses_info(void);
          plhs[0] = sf_c3_speedTest_third_party_uses_info();
          break;
        }
      }

     case 4:
      {
        if (strcmp(tpChksum, "9dC2ipLYw0brxEDvXnTWxC") == 0) {
          extern mxArray *sf_c4_speedTest_third_party_uses_info(void);
          plhs[0] = sf_c4_speedTest_third_party_uses_info();
          break;
        }
      }

     case 7:
      {
        if (strcmp(tpChksum, "gkxnlDDaCol3tTf1u27nYB") == 0) {
          extern mxArray *sf_c7_speedTest_third_party_uses_info(void);
          plhs[0] = sf_c7_speedTest_third_party_uses_info();
          break;
        }
      }

     case 8:
      {
        if (strcmp(tpChksum, "Jbiqze4DlRPbPmbIL0wS8F") == 0) {
          extern mxArray *sf_c8_speedTest_third_party_uses_info(void);
          plhs[0] = sf_c8_speedTest_third_party_uses_info();
          break;
        }
      }

     case 9:
      {
        if (strcmp(tpChksum, "SafIxCjmM9qEPS6BmID8AG") == 0) {
          extern mxArray *sf_c9_speedTest_third_party_uses_info(void);
          plhs[0] = sf_c9_speedTest_third_party_uses_info();
          break;
        }
      }

     case 10:
      {
        if (strcmp(tpChksum, "ykND2QY3AIDhFCaEB5eP5F") == 0) {
          extern mxArray *sf_c10_speedTest_third_party_uses_info(void);
          plhs[0] = sf_c10_speedTest_third_party_uses_info();
          break;
        }
      }

     case 12:
      {
        if (strcmp(tpChksum, "wt65AZ2hxioUIGw90mtxBB") == 0) {
          extern mxArray *sf_c12_speedTest_third_party_uses_info(void);
          plhs[0] = sf_c12_speedTest_third_party_uses_info();
          break;
        }
      }

     case 13:
      {
        if (strcmp(tpChksum, "ynY5ttWD0J2n5VW3gZiZPC") == 0) {
          extern mxArray *sf_c13_speedTest_third_party_uses_info(void);
          plhs[0] = sf_c13_speedTest_third_party_uses_info();
          break;
        }
      }

     case 20:
      {
        if (strcmp(tpChksum, "iLKkTZfgeUWbCHGkvxHUyF") == 0) {
          extern mxArray *sf_c20_speedTest_third_party_uses_info(void);
          plhs[0] = sf_c20_speedTest_third_party_uses_info();
          break;
        }
      }

     case 21:
      {
        if (strcmp(tpChksum, "mTPa5Pzbe3w9l3J7cRjFdB") == 0) {
          extern mxArray *sf_c21_speedTest_third_party_uses_info(void);
          plhs[0] = sf_c21_speedTest_third_party_uses_info();
          break;
        }
      }

     case 22:
      {
        if (strcmp(tpChksum, "Rlx5hbPfiijZHfd9WQUKiG") == 0) {
          extern mxArray *sf_c22_speedTest_third_party_uses_info(void);
          plhs[0] = sf_c22_speedTest_third_party_uses_info();
          break;
        }
      }

     case 30:
      {
        if (strcmp(tpChksum, "3eIjVhVEkbA6prTKjxYplG") == 0) {
          extern mxArray *sf_c30_speedTest_third_party_uses_info(void);
          plhs[0] = sf_c30_speedTest_third_party_uses_info();
          break;
        }
      }

     case 34:
      {
        if (strcmp(tpChksum, "xNYm6GNN8jSBExJBkcuC2B") == 0) {
          extern mxArray *sf_c34_speedTest_third_party_uses_info(void);
          plhs[0] = sf_c34_speedTest_third_party_uses_info();
          break;
        }
      }

     case 60:
      {
        if (strcmp(tpChksum, "H3SrOtX8Tlo0LVyUVlbGyE") == 0) {
          extern mxArray *sf_c60_speedTest_third_party_uses_info(void);
          plhs[0] = sf_c60_speedTest_third_party_uses_info();
          break;
        }
      }

     default:
      plhs[0] = mxCreateDoubleMatrix(0,0,mxREAL);
    }
  }

  return 1;
}

unsigned int sf_speedTest_jit_fallback_info( int nlhs, mxArray * plhs[], int
  nrhs, const mxArray * prhs[] )
{
  char commandName[64];
  char tpChksum[64];
  if (nrhs<3 || !mxIsChar(prhs[0]))
    return 0;

  /* Possible call to get the jit_fallback_info */
  mxGetString(prhs[0], commandName,sizeof(commandName)/sizeof(char));
  commandName[(sizeof(commandName)/sizeof(char)-1)] = '\0';
  mxGetString(prhs[2], tpChksum,sizeof(tpChksum)/sizeof(char));
  tpChksum[(sizeof(tpChksum)/sizeof(char)-1)] = '\0';
  if (strcmp(commandName,"get_jit_fallback_info"))
    return 0;

  {
    unsigned int chartFileNumber;
    chartFileNumber = (unsigned int)mxGetScalar(prhs[1]);
    switch (chartFileNumber) {
     case 1:
      {
        if (strcmp(tpChksum, "77qs0O1aFQP9R5KVWKglD") == 0) {
          extern mxArray *sf_c1_speedTest_jit_fallback_info(void);
          plhs[0] = sf_c1_speedTest_jit_fallback_info();
          break;
        }
      }

     case 2:
      {
        if (strcmp(tpChksum, "BPbr6mnccR4iiqbbOejEIC") == 0) {
          extern mxArray *sf_c2_speedTest_jit_fallback_info(void);
          plhs[0] = sf_c2_speedTest_jit_fallback_info();
          break;
        }
      }

     case 3:
      {
        if (strcmp(tpChksum, "gblOQFxJdbh8vdZcLHybtE") == 0) {
          extern mxArray *sf_c3_speedTest_jit_fallback_info(void);
          plhs[0] = sf_c3_speedTest_jit_fallback_info();
          break;
        }
      }

     case 4:
      {
        if (strcmp(tpChksum, "9dC2ipLYw0brxEDvXnTWxC") == 0) {
          extern mxArray *sf_c4_speedTest_jit_fallback_info(void);
          plhs[0] = sf_c4_speedTest_jit_fallback_info();
          break;
        }
      }

     case 7:
      {
        if (strcmp(tpChksum, "gkxnlDDaCol3tTf1u27nYB") == 0) {
          extern mxArray *sf_c7_speedTest_jit_fallback_info(void);
          plhs[0] = sf_c7_speedTest_jit_fallback_info();
          break;
        }
      }

     case 8:
      {
        if (strcmp(tpChksum, "Jbiqze4DlRPbPmbIL0wS8F") == 0) {
          extern mxArray *sf_c8_speedTest_jit_fallback_info(void);
          plhs[0] = sf_c8_speedTest_jit_fallback_info();
          break;
        }
      }

     case 9:
      {
        if (strcmp(tpChksum, "SafIxCjmM9qEPS6BmID8AG") == 0) {
          extern mxArray *sf_c9_speedTest_jit_fallback_info(void);
          plhs[0] = sf_c9_speedTest_jit_fallback_info();
          break;
        }
      }

     case 10:
      {
        if (strcmp(tpChksum, "ykND2QY3AIDhFCaEB5eP5F") == 0) {
          extern mxArray *sf_c10_speedTest_jit_fallback_info(void);
          plhs[0] = sf_c10_speedTest_jit_fallback_info();
          break;
        }
      }

     case 12:
      {
        if (strcmp(tpChksum, "wt65AZ2hxioUIGw90mtxBB") == 0) {
          extern mxArray *sf_c12_speedTest_jit_fallback_info(void);
          plhs[0] = sf_c12_speedTest_jit_fallback_info();
          break;
        }
      }

     case 13:
      {
        if (strcmp(tpChksum, "ynY5ttWD0J2n5VW3gZiZPC") == 0) {
          extern mxArray *sf_c13_speedTest_jit_fallback_info(void);
          plhs[0] = sf_c13_speedTest_jit_fallback_info();
          break;
        }
      }

     case 20:
      {
        if (strcmp(tpChksum, "iLKkTZfgeUWbCHGkvxHUyF") == 0) {
          extern mxArray *sf_c20_speedTest_jit_fallback_info(void);
          plhs[0] = sf_c20_speedTest_jit_fallback_info();
          break;
        }
      }

     case 21:
      {
        if (strcmp(tpChksum, "mTPa5Pzbe3w9l3J7cRjFdB") == 0) {
          extern mxArray *sf_c21_speedTest_jit_fallback_info(void);
          plhs[0] = sf_c21_speedTest_jit_fallback_info();
          break;
        }
      }

     case 22:
      {
        if (strcmp(tpChksum, "Rlx5hbPfiijZHfd9WQUKiG") == 0) {
          extern mxArray *sf_c22_speedTest_jit_fallback_info(void);
          plhs[0] = sf_c22_speedTest_jit_fallback_info();
          break;
        }
      }

     case 30:
      {
        if (strcmp(tpChksum, "3eIjVhVEkbA6prTKjxYplG") == 0) {
          extern mxArray *sf_c30_speedTest_jit_fallback_info(void);
          plhs[0] = sf_c30_speedTest_jit_fallback_info();
          break;
        }
      }

     case 34:
      {
        if (strcmp(tpChksum, "xNYm6GNN8jSBExJBkcuC2B") == 0) {
          extern mxArray *sf_c34_speedTest_jit_fallback_info(void);
          plhs[0] = sf_c34_speedTest_jit_fallback_info();
          break;
        }
      }

     case 60:
      {
        if (strcmp(tpChksum, "H3SrOtX8Tlo0LVyUVlbGyE") == 0) {
          extern mxArray *sf_c60_speedTest_jit_fallback_info(void);
          plhs[0] = sf_c60_speedTest_jit_fallback_info();
          break;
        }
      }

     default:
      plhs[0] = mxCreateDoubleMatrix(0,0,mxREAL);
    }
  }

  return 1;
}

unsigned int sf_speedTest_updateBuildInfo_args_info( int nlhs, mxArray * plhs[],
  int nrhs, const mxArray * prhs[] )
{
  char commandName[64];
  char tpChksum[64];
  if (nrhs<3 || !mxIsChar(prhs[0]))
    return 0;

  /* Possible call to get the updateBuildInfo_args_info */
  mxGetString(prhs[0], commandName,sizeof(commandName)/sizeof(char));
  commandName[(sizeof(commandName)/sizeof(char)-1)] = '\0';
  mxGetString(prhs[2], tpChksum,sizeof(tpChksum)/sizeof(char));
  tpChksum[(sizeof(tpChksum)/sizeof(char)-1)] = '\0';
  if (strcmp(commandName,"get_updateBuildInfo_args_info"))
    return 0;

  {
    unsigned int chartFileNumber;
    chartFileNumber = (unsigned int)mxGetScalar(prhs[1]);
    switch (chartFileNumber) {
     case 1:
      {
        if (strcmp(tpChksum, "77qs0O1aFQP9R5KVWKglD") == 0) {
          extern mxArray *sf_c1_speedTest_updateBuildInfo_args_info(void);
          plhs[0] = sf_c1_speedTest_updateBuildInfo_args_info();
          break;
        }
      }

     case 2:
      {
        if (strcmp(tpChksum, "BPbr6mnccR4iiqbbOejEIC") == 0) {
          extern mxArray *sf_c2_speedTest_updateBuildInfo_args_info(void);
          plhs[0] = sf_c2_speedTest_updateBuildInfo_args_info();
          break;
        }
      }

     case 3:
      {
        if (strcmp(tpChksum, "gblOQFxJdbh8vdZcLHybtE") == 0) {
          extern mxArray *sf_c3_speedTest_updateBuildInfo_args_info(void);
          plhs[0] = sf_c3_speedTest_updateBuildInfo_args_info();
          break;
        }
      }

     case 4:
      {
        if (strcmp(tpChksum, "9dC2ipLYw0brxEDvXnTWxC") == 0) {
          extern mxArray *sf_c4_speedTest_updateBuildInfo_args_info(void);
          plhs[0] = sf_c4_speedTest_updateBuildInfo_args_info();
          break;
        }
      }

     case 7:
      {
        if (strcmp(tpChksum, "gkxnlDDaCol3tTf1u27nYB") == 0) {
          extern mxArray *sf_c7_speedTest_updateBuildInfo_args_info(void);
          plhs[0] = sf_c7_speedTest_updateBuildInfo_args_info();
          break;
        }
      }

     case 8:
      {
        if (strcmp(tpChksum, "Jbiqze4DlRPbPmbIL0wS8F") == 0) {
          extern mxArray *sf_c8_speedTest_updateBuildInfo_args_info(void);
          plhs[0] = sf_c8_speedTest_updateBuildInfo_args_info();
          break;
        }
      }

     case 9:
      {
        if (strcmp(tpChksum, "SafIxCjmM9qEPS6BmID8AG") == 0) {
          extern mxArray *sf_c9_speedTest_updateBuildInfo_args_info(void);
          plhs[0] = sf_c9_speedTest_updateBuildInfo_args_info();
          break;
        }
      }

     case 10:
      {
        if (strcmp(tpChksum, "ykND2QY3AIDhFCaEB5eP5F") == 0) {
          extern mxArray *sf_c10_speedTest_updateBuildInfo_args_info(void);
          plhs[0] = sf_c10_speedTest_updateBuildInfo_args_info();
          break;
        }
      }

     case 12:
      {
        if (strcmp(tpChksum, "wt65AZ2hxioUIGw90mtxBB") == 0) {
          extern mxArray *sf_c12_speedTest_updateBuildInfo_args_info(void);
          plhs[0] = sf_c12_speedTest_updateBuildInfo_args_info();
          break;
        }
      }

     case 13:
      {
        if (strcmp(tpChksum, "ynY5ttWD0J2n5VW3gZiZPC") == 0) {
          extern mxArray *sf_c13_speedTest_updateBuildInfo_args_info(void);
          plhs[0] = sf_c13_speedTest_updateBuildInfo_args_info();
          break;
        }
      }

     case 20:
      {
        if (strcmp(tpChksum, "iLKkTZfgeUWbCHGkvxHUyF") == 0) {
          extern mxArray *sf_c20_speedTest_updateBuildInfo_args_info(void);
          plhs[0] = sf_c20_speedTest_updateBuildInfo_args_info();
          break;
        }
      }

     case 21:
      {
        if (strcmp(tpChksum, "mTPa5Pzbe3w9l3J7cRjFdB") == 0) {
          extern mxArray *sf_c21_speedTest_updateBuildInfo_args_info(void);
          plhs[0] = sf_c21_speedTest_updateBuildInfo_args_info();
          break;
        }
      }

     case 22:
      {
        if (strcmp(tpChksum, "Rlx5hbPfiijZHfd9WQUKiG") == 0) {
          extern mxArray *sf_c22_speedTest_updateBuildInfo_args_info(void);
          plhs[0] = sf_c22_speedTest_updateBuildInfo_args_info();
          break;
        }
      }

     case 30:
      {
        if (strcmp(tpChksum, "3eIjVhVEkbA6prTKjxYplG") == 0) {
          extern mxArray *sf_c30_speedTest_updateBuildInfo_args_info(void);
          plhs[0] = sf_c30_speedTest_updateBuildInfo_args_info();
          break;
        }
      }

     case 34:
      {
        if (strcmp(tpChksum, "xNYm6GNN8jSBExJBkcuC2B") == 0) {
          extern mxArray *sf_c34_speedTest_updateBuildInfo_args_info(void);
          plhs[0] = sf_c34_speedTest_updateBuildInfo_args_info();
          break;
        }
      }

     case 60:
      {
        if (strcmp(tpChksum, "H3SrOtX8Tlo0LVyUVlbGyE") == 0) {
          extern mxArray *sf_c60_speedTest_updateBuildInfo_args_info(void);
          plhs[0] = sf_c60_speedTest_updateBuildInfo_args_info();
          break;
        }
      }

     default:
      plhs[0] = mxCreateDoubleMatrix(0,0,mxREAL);
    }
  }

  return 1;
}

void sf_speedTest_get_post_codegen_info( int nlhs, mxArray * plhs[], int nrhs,
  const mxArray * prhs[] )
{
  unsigned int chartFileNumber = (unsigned int) mxGetScalar(prhs[0]);
  char tpChksum[64];
  mxGetString(prhs[1], tpChksum,sizeof(tpChksum)/sizeof(char));
  tpChksum[(sizeof(tpChksum)/sizeof(char)-1)] = '\0';
  switch (chartFileNumber) {
   case 1:
    {
      if (strcmp(tpChksum, "77qs0O1aFQP9R5KVWKglD") == 0) {
        extern mxArray *sf_c1_speedTest_get_post_codegen_info(void);
        plhs[0] = sf_c1_speedTest_get_post_codegen_info();
        return;
      }
    }
    break;

   case 2:
    {
      if (strcmp(tpChksum, "BPbr6mnccR4iiqbbOejEIC") == 0) {
        extern mxArray *sf_c2_speedTest_get_post_codegen_info(void);
        plhs[0] = sf_c2_speedTest_get_post_codegen_info();
        return;
      }
    }
    break;

   case 3:
    {
      if (strcmp(tpChksum, "gblOQFxJdbh8vdZcLHybtE") == 0) {
        extern mxArray *sf_c3_speedTest_get_post_codegen_info(void);
        plhs[0] = sf_c3_speedTest_get_post_codegen_info();
        return;
      }
    }
    break;

   case 4:
    {
      if (strcmp(tpChksum, "9dC2ipLYw0brxEDvXnTWxC") == 0) {
        extern mxArray *sf_c4_speedTest_get_post_codegen_info(void);
        plhs[0] = sf_c4_speedTest_get_post_codegen_info();
        return;
      }
    }
    break;

   case 7:
    {
      if (strcmp(tpChksum, "gkxnlDDaCol3tTf1u27nYB") == 0) {
        extern mxArray *sf_c7_speedTest_get_post_codegen_info(void);
        plhs[0] = sf_c7_speedTest_get_post_codegen_info();
        return;
      }
    }
    break;

   case 8:
    {
      if (strcmp(tpChksum, "Jbiqze4DlRPbPmbIL0wS8F") == 0) {
        extern mxArray *sf_c8_speedTest_get_post_codegen_info(void);
        plhs[0] = sf_c8_speedTest_get_post_codegen_info();
        return;
      }
    }
    break;

   case 9:
    {
      if (strcmp(tpChksum, "SafIxCjmM9qEPS6BmID8AG") == 0) {
        extern mxArray *sf_c9_speedTest_get_post_codegen_info(void);
        plhs[0] = sf_c9_speedTest_get_post_codegen_info();
        return;
      }
    }
    break;

   case 10:
    {
      if (strcmp(tpChksum, "ykND2QY3AIDhFCaEB5eP5F") == 0) {
        extern mxArray *sf_c10_speedTest_get_post_codegen_info(void);
        plhs[0] = sf_c10_speedTest_get_post_codegen_info();
        return;
      }
    }
    break;

   case 12:
    {
      if (strcmp(tpChksum, "wt65AZ2hxioUIGw90mtxBB") == 0) {
        extern mxArray *sf_c12_speedTest_get_post_codegen_info(void);
        plhs[0] = sf_c12_speedTest_get_post_codegen_info();
        return;
      }
    }
    break;

   case 13:
    {
      if (strcmp(tpChksum, "ynY5ttWD0J2n5VW3gZiZPC") == 0) {
        extern mxArray *sf_c13_speedTest_get_post_codegen_info(void);
        plhs[0] = sf_c13_speedTest_get_post_codegen_info();
        return;
      }
    }
    break;

   case 20:
    {
      if (strcmp(tpChksum, "iLKkTZfgeUWbCHGkvxHUyF") == 0) {
        extern mxArray *sf_c20_speedTest_get_post_codegen_info(void);
        plhs[0] = sf_c20_speedTest_get_post_codegen_info();
        return;
      }
    }
    break;

   case 21:
    {
      if (strcmp(tpChksum, "mTPa5Pzbe3w9l3J7cRjFdB") == 0) {
        extern mxArray *sf_c21_speedTest_get_post_codegen_info(void);
        plhs[0] = sf_c21_speedTest_get_post_codegen_info();
        return;
      }
    }
    break;

   case 22:
    {
      if (strcmp(tpChksum, "Rlx5hbPfiijZHfd9WQUKiG") == 0) {
        extern mxArray *sf_c22_speedTest_get_post_codegen_info(void);
        plhs[0] = sf_c22_speedTest_get_post_codegen_info();
        return;
      }
    }
    break;

   case 30:
    {
      if (strcmp(tpChksum, "3eIjVhVEkbA6prTKjxYplG") == 0) {
        extern mxArray *sf_c30_speedTest_get_post_codegen_info(void);
        plhs[0] = sf_c30_speedTest_get_post_codegen_info();
        return;
      }
    }
    break;

   case 34:
    {
      if (strcmp(tpChksum, "xNYm6GNN8jSBExJBkcuC2B") == 0) {
        extern mxArray *sf_c34_speedTest_get_post_codegen_info(void);
        plhs[0] = sf_c34_speedTest_get_post_codegen_info();
        return;
      }
    }
    break;

   case 60:
    {
      if (strcmp(tpChksum, "H3SrOtX8Tlo0LVyUVlbGyE") == 0) {
        extern mxArray *sf_c60_speedTest_get_post_codegen_info(void);
        plhs[0] = sf_c60_speedTest_get_post_codegen_info();
        return;
      }
    }
    break;

   default:
    break;
  }

  plhs[0] = mxCreateDoubleMatrix(0,0,mxREAL);
}

void speedTest_debug_initialize(struct SfDebugInstanceStruct* debugInstance)
{
  _speedTestMachineNumber_ = sf_debug_initialize_machine(debugInstance,
    "speedTest","sfun",0,16,0,0,0);
  sf_debug_set_machine_event_thresholds(debugInstance,_speedTestMachineNumber_,0,
    0);
  sf_debug_set_machine_data_thresholds(debugInstance,_speedTestMachineNumber_,0);
}

void speedTest_register_exported_symbols(SimStruct* S)
{
}

static mxArray* sRtwOptimizationInfoStruct= NULL;
mxArray* load_speedTest_optimization_info(void)
{
  if (sRtwOptimizationInfoStruct==NULL) {
    sRtwOptimizationInfoStruct = sf_load_rtw_optimization_info("speedTest",
      "speedTest");
    mexMakeArrayPersistent(sRtwOptimizationInfoStruct);
  }

  return(sRtwOptimizationInfoStruct);
}

void unload_speedTest_optimization_info(void)
{
  if (sRtwOptimizationInfoStruct!=NULL) {
    mxDestroyArray(sRtwOptimizationInfoStruct);
    sRtwOptimizationInfoStruct = NULL;
  }
}

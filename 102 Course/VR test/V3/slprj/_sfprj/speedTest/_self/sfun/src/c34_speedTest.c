/* Include files */

#include <stddef.h>
#include "blas.h"
#include "speedTest_sfun.h"
#include "c34_speedTest.h"
#define CHARTINSTANCE_CHARTNUMBER      (chartInstance->chartNumber)
#define CHARTINSTANCE_INSTANCENUMBER   (chartInstance->instanceNumber)
#include "speedTest_sfun_debug_macros.h"
#define _SF_MEX_LISTEN_FOR_CTRL_C(S)   sf_mex_listen_for_ctrl_c(sfGlobalDebugInstanceStruct,S);

/* Type Definitions */

/* Named Constants */
#define CALL_EVENT                     (-1)

/* Variable Declarations */

/* Variable Definitions */
static real_T _sfTime_;
static const char * c34_debug_family_names[4] = { "nargin", "nargout",
  "messageBuffer", "message" };

/* Function Declarations */
static void initialize_c34_speedTest(SFc34_speedTestInstanceStruct
  *chartInstance);
static void initialize_params_c34_speedTest(SFc34_speedTestInstanceStruct
  *chartInstance);
static void enable_c34_speedTest(SFc34_speedTestInstanceStruct *chartInstance);
static void disable_c34_speedTest(SFc34_speedTestInstanceStruct *chartInstance);
static void c34_update_debugger_state_c34_speedTest
  (SFc34_speedTestInstanceStruct *chartInstance);
static const mxArray *get_sim_state_c34_speedTest(SFc34_speedTestInstanceStruct *
  chartInstance);
static void set_sim_state_c34_speedTest(SFc34_speedTestInstanceStruct
  *chartInstance, const mxArray *c34_st);
static void finalize_c34_speedTest(SFc34_speedTestInstanceStruct *chartInstance);
static void sf_gateway_c34_speedTest(SFc34_speedTestInstanceStruct
  *chartInstance);
static void mdl_start_c34_speedTest(SFc34_speedTestInstanceStruct *chartInstance);
static void initSimStructsc34_speedTest(SFc34_speedTestInstanceStruct
  *chartInstance);
static void init_script_number_translation(uint32_T c34_machineNumber, uint32_T
  c34_chartNumber, uint32_T c34_instanceNumber);
static const mxArray *c34_sf_marshallOut(void *chartInstanceVoid, void
  *c34_inData);
static void c34_emlrt_marshallIn(SFc34_speedTestInstanceStruct *chartInstance,
  const mxArray *c34_b_message, const char_T *c34_identifier, uint8_T c34_y[31]);
static void c34_b_emlrt_marshallIn(SFc34_speedTestInstanceStruct *chartInstance,
  const mxArray *c34_u, const emlrtMsgIdentifier *c34_parentId, uint8_T c34_y[31]);
static void c34_sf_marshallIn(void *chartInstanceVoid, const mxArray
  *c34_mxArrayInData, const char_T *c34_varName, void *c34_outData);
static const mxArray *c34_b_sf_marshallOut(void *chartInstanceVoid, uint8_T
  c34_inData_data[], int32_T c34_inData_sizes[2]);
static const mxArray *c34_c_sf_marshallOut(void *chartInstanceVoid, void
  *c34_inData);
static real_T c34_c_emlrt_marshallIn(SFc34_speedTestInstanceStruct
  *chartInstance, const mxArray *c34_u, const emlrtMsgIdentifier *c34_parentId);
static void c34_b_sf_marshallIn(void *chartInstanceVoid, const mxArray
  *c34_mxArrayInData, const char_T *c34_varName, void *c34_outData);
static const mxArray *c34_d_sf_marshallOut(void *chartInstanceVoid, void
  *c34_inData);
static int32_T c34_d_emlrt_marshallIn(SFc34_speedTestInstanceStruct
  *chartInstance, const mxArray *c34_u, const emlrtMsgIdentifier *c34_parentId);
static void c34_c_sf_marshallIn(void *chartInstanceVoid, const mxArray
  *c34_mxArrayInData, const char_T *c34_varName, void *c34_outData);
static uint8_T c34_e_emlrt_marshallIn(SFc34_speedTestInstanceStruct
  *chartInstance, const mxArray *c34_b_is_active_c34_speedTest, const char_T
  *c34_identifier);
static uint8_T c34_f_emlrt_marshallIn(SFc34_speedTestInstanceStruct
  *chartInstance, const mxArray *c34_u, const emlrtMsgIdentifier *c34_parentId);
static void init_dsm_address_info(SFc34_speedTestInstanceStruct *chartInstance);
static void init_simulink_io_address(SFc34_speedTestInstanceStruct
  *chartInstance);

/* Function Definitions */
static void initialize_c34_speedTest(SFc34_speedTestInstanceStruct
  *chartInstance)
{
  chartInstance->c34_sfEvent = CALL_EVENT;
  _sfTime_ = sf_get_time(chartInstance->S);
  chartInstance->c34_is_active_c34_speedTest = 0U;
}

static void initialize_params_c34_speedTest(SFc34_speedTestInstanceStruct
  *chartInstance)
{
  (void)chartInstance;
}

static void enable_c34_speedTest(SFc34_speedTestInstanceStruct *chartInstance)
{
  _sfTime_ = sf_get_time(chartInstance->S);
}

static void disable_c34_speedTest(SFc34_speedTestInstanceStruct *chartInstance)
{
  _sfTime_ = sf_get_time(chartInstance->S);
}

static void c34_update_debugger_state_c34_speedTest
  (SFc34_speedTestInstanceStruct *chartInstance)
{
  (void)chartInstance;
}

static const mxArray *get_sim_state_c34_speedTest(SFc34_speedTestInstanceStruct *
  chartInstance)
{
  const mxArray *c34_st;
  const mxArray *c34_y = NULL;
  int32_T c34_i0;
  uint8_T c34_u[31];
  const mxArray *c34_b_y = NULL;
  uint8_T c34_hoistedGlobal;
  uint8_T c34_b_u;
  const mxArray *c34_c_y = NULL;
  c34_st = NULL;
  c34_st = NULL;
  c34_y = NULL;
  sf_mex_assign(&c34_y, sf_mex_createcellmatrix(2, 1), false);
  for (c34_i0 = 0; c34_i0 < 31; c34_i0++) {
    c34_u[c34_i0] = (*chartInstance->c34_message)[c34_i0];
  }

  c34_b_y = NULL;
  sf_mex_assign(&c34_b_y, sf_mex_create("y", c34_u, 3, 0U, 1U, 0U, 2, 1, 31),
                false);
  sf_mex_setcell(c34_y, 0, c34_b_y);
  c34_hoistedGlobal = chartInstance->c34_is_active_c34_speedTest;
  c34_b_u = c34_hoistedGlobal;
  c34_c_y = NULL;
  sf_mex_assign(&c34_c_y, sf_mex_create("y", &c34_b_u, 3, 0U, 0U, 0U, 0), false);
  sf_mex_setcell(c34_y, 1, c34_c_y);
  sf_mex_assign(&c34_st, c34_y, false);
  return c34_st;
}

static void set_sim_state_c34_speedTest(SFc34_speedTestInstanceStruct
  *chartInstance, const mxArray *c34_st)
{
  const mxArray *c34_u;
  uint8_T c34_uv0[31];
  int32_T c34_i1;
  chartInstance->c34_doneDoubleBufferReInit = true;
  c34_u = sf_mex_dup(c34_st);
  c34_emlrt_marshallIn(chartInstance, sf_mex_dup(sf_mex_getcell(c34_u, 0)),
                       "message", c34_uv0);
  for (c34_i1 = 0; c34_i1 < 31; c34_i1++) {
    (*chartInstance->c34_message)[c34_i1] = c34_uv0[c34_i1];
  }

  chartInstance->c34_is_active_c34_speedTest = c34_e_emlrt_marshallIn
    (chartInstance, sf_mex_dup(sf_mex_getcell(c34_u, 1)),
     "is_active_c34_speedTest");
  sf_mex_destroy(&c34_u);
  c34_update_debugger_state_c34_speedTest(chartInstance);
  sf_mex_destroy(&c34_st);
}

static void finalize_c34_speedTest(SFc34_speedTestInstanceStruct *chartInstance)
{
  (void)chartInstance;
}

static void sf_gateway_c34_speedTest(SFc34_speedTestInstanceStruct
  *chartInstance)
{
  int32_T c34_loop_ub;
  int32_T c34_i2;
  int32_T c34_b_messageBuffer_sizes[2];
  int32_T c34_messageBuffer;
  int32_T c34_b_messageBuffer;
  int32_T c34_b_loop_ub;
  int32_T c34_i3;
  uint8_T c34_b_messageBuffer_data[64];
  uint32_T c34_debug_family_var_map[4];
  real_T c34_nargin = 1.0;
  real_T c34_nargout = 1.0;
  uint8_T c34_b_message[31];
  int32_T c34_i4;
  int32_T c34_i5;
  int32_T c34_i6;
  _SFD_SYMBOL_SCOPE_PUSH(0U, 0U);
  _sfTime_ = sf_get_time(chartInstance->S);
  _SFD_CC_CALL(CHART_ENTER_SFUNCTION_TAG, 14U, chartInstance->c34_sfEvent);
  c34_loop_ub = (*chartInstance->c34_messageBuffer_sizes)[0] *
    (*chartInstance->c34_messageBuffer_sizes)[1] - 1;
  for (c34_i2 = 0; c34_i2 <= c34_loop_ub; c34_i2++) {
    _SFD_DATA_RANGE_CHECK((real_T)(*chartInstance->c34_messageBuffer_data)
                          [c34_i2], 0U);
  }

  chartInstance->c34_sfEvent = CALL_EVENT;
  _SFD_CC_CALL(CHART_ENTER_DURING_FUNCTION_TAG, 14U, chartInstance->c34_sfEvent);
  c34_b_messageBuffer_sizes[0] = 1;
  c34_b_messageBuffer_sizes[1] = (*chartInstance->c34_messageBuffer_sizes)[1];
  c34_messageBuffer = c34_b_messageBuffer_sizes[0];
  c34_b_messageBuffer = c34_b_messageBuffer_sizes[1];
  c34_b_loop_ub = (*chartInstance->c34_messageBuffer_sizes)[0] *
    (*chartInstance->c34_messageBuffer_sizes)[1] - 1;
  for (c34_i3 = 0; c34_i3 <= c34_b_loop_ub; c34_i3++) {
    c34_b_messageBuffer_data[c34_i3] = (*chartInstance->c34_messageBuffer_data)
      [c34_i3];
  }

  _SFD_SYMBOL_SCOPE_PUSH_EML(0U, 4U, 4U, c34_debug_family_names,
    c34_debug_family_var_map);
  _SFD_SYMBOL_SCOPE_ADD_EML_IMPORTABLE(&c34_nargin, 0U, c34_c_sf_marshallOut,
    c34_b_sf_marshallIn);
  _SFD_SYMBOL_SCOPE_ADD_EML_IMPORTABLE(&c34_nargout, 1U, c34_c_sf_marshallOut,
    c34_b_sf_marshallIn);
  _SFD_SYMBOL_SCOPE_ADD_EML_DYN(c34_b_messageBuffer_data, (const int32_T *)
    &c34_b_messageBuffer_sizes, NULL, 1, 2, (void *)c34_b_sf_marshallOut);
  _SFD_SYMBOL_SCOPE_ADD_EML_IMPORTABLE(c34_b_message, 3U, c34_sf_marshallOut,
    c34_sf_marshallIn);
  CV_EML_FCN(0, 0);
  _SFD_EML_CALL(0U, chartInstance->c34_sfEvent, 2);
  for (c34_i4 = 0; c34_i4 < 31; c34_i4++) {
    c34_b_message[c34_i4] = c34_b_messageBuffer_data[_SFD_EML_ARRAY_BOUNDS_CHECK
      ("messageBuffer", c34_i4 + 1, 1, c34_b_messageBuffer_sizes[1], 1, 0) - 1];
  }

  _SFD_EML_CALL(0U, chartInstance->c34_sfEvent, -2);
  _SFD_SYMBOL_SCOPE_POP();
  for (c34_i5 = 0; c34_i5 < 31; c34_i5++) {
    (*chartInstance->c34_message)[c34_i5] = c34_b_message[c34_i5];
  }

  _SFD_CC_CALL(EXIT_OUT_OF_FUNCTION_TAG, 14U, chartInstance->c34_sfEvent);
  _SFD_SYMBOL_SCOPE_POP();
  _SFD_CHECK_FOR_STATE_INCONSISTENCY(_speedTestMachineNumber_,
    chartInstance->chartNumber, chartInstance->instanceNumber);
  for (c34_i6 = 0; c34_i6 < 31; c34_i6++) {
    _SFD_DATA_RANGE_CHECK((real_T)(*chartInstance->c34_message)[c34_i6], 1U);
  }
}

static void mdl_start_c34_speedTest(SFc34_speedTestInstanceStruct *chartInstance)
{
  (void)chartInstance;
}

static void initSimStructsc34_speedTest(SFc34_speedTestInstanceStruct
  *chartInstance)
{
  (void)chartInstance;
}

static void init_script_number_translation(uint32_T c34_machineNumber, uint32_T
  c34_chartNumber, uint32_T c34_instanceNumber)
{
  (void)c34_machineNumber;
  (void)c34_chartNumber;
  (void)c34_instanceNumber;
}

static const mxArray *c34_sf_marshallOut(void *chartInstanceVoid, void
  *c34_inData)
{
  const mxArray *c34_mxArrayOutData = NULL;
  int32_T c34_i7;
  uint8_T c34_b_inData[31];
  int32_T c34_i8;
  uint8_T c34_u[31];
  const mxArray *c34_y = NULL;
  SFc34_speedTestInstanceStruct *chartInstance;
  chartInstance = (SFc34_speedTestInstanceStruct *)chartInstanceVoid;
  c34_mxArrayOutData = NULL;
  for (c34_i7 = 0; c34_i7 < 31; c34_i7++) {
    c34_b_inData[c34_i7] = (*(uint8_T (*)[31])c34_inData)[c34_i7];
  }

  for (c34_i8 = 0; c34_i8 < 31; c34_i8++) {
    c34_u[c34_i8] = c34_b_inData[c34_i8];
  }

  c34_y = NULL;
  sf_mex_assign(&c34_y, sf_mex_create("y", c34_u, 3, 0U, 1U, 0U, 2, 1, 31),
                false);
  sf_mex_assign(&c34_mxArrayOutData, c34_y, false);
  return c34_mxArrayOutData;
}

static void c34_emlrt_marshallIn(SFc34_speedTestInstanceStruct *chartInstance,
  const mxArray *c34_b_message, const char_T *c34_identifier, uint8_T c34_y[31])
{
  emlrtMsgIdentifier c34_thisId;
  c34_thisId.fIdentifier = c34_identifier;
  c34_thisId.fParent = NULL;
  c34_b_emlrt_marshallIn(chartInstance, sf_mex_dup(c34_b_message), &c34_thisId,
    c34_y);
  sf_mex_destroy(&c34_b_message);
}

static void c34_b_emlrt_marshallIn(SFc34_speedTestInstanceStruct *chartInstance,
  const mxArray *c34_u, const emlrtMsgIdentifier *c34_parentId, uint8_T c34_y[31])
{
  uint8_T c34_uv1[31];
  int32_T c34_i9;
  (void)chartInstance;
  sf_mex_import(c34_parentId, sf_mex_dup(c34_u), c34_uv1, 1, 3, 0U, 1, 0U, 2, 1,
                31);
  for (c34_i9 = 0; c34_i9 < 31; c34_i9++) {
    c34_y[c34_i9] = c34_uv1[c34_i9];
  }

  sf_mex_destroy(&c34_u);
}

static void c34_sf_marshallIn(void *chartInstanceVoid, const mxArray
  *c34_mxArrayInData, const char_T *c34_varName, void *c34_outData)
{
  const mxArray *c34_b_message;
  const char_T *c34_identifier;
  emlrtMsgIdentifier c34_thisId;
  uint8_T c34_y[31];
  int32_T c34_i10;
  SFc34_speedTestInstanceStruct *chartInstance;
  chartInstance = (SFc34_speedTestInstanceStruct *)chartInstanceVoid;
  c34_b_message = sf_mex_dup(c34_mxArrayInData);
  c34_identifier = c34_varName;
  c34_thisId.fIdentifier = c34_identifier;
  c34_thisId.fParent = NULL;
  c34_b_emlrt_marshallIn(chartInstance, sf_mex_dup(c34_b_message), &c34_thisId,
    c34_y);
  sf_mex_destroy(&c34_b_message);
  for (c34_i10 = 0; c34_i10 < 31; c34_i10++) {
    (*(uint8_T (*)[31])c34_outData)[c34_i10] = c34_y[c34_i10];
  }

  sf_mex_destroy(&c34_mxArrayInData);
}

static const mxArray *c34_b_sf_marshallOut(void *chartInstanceVoid, uint8_T
  c34_inData_data[], int32_T c34_inData_sizes[2])
{
  const mxArray *c34_mxArrayOutData = NULL;
  int32_T c34_u_sizes[2];
  int32_T c34_u;
  int32_T c34_b_u;
  int32_T c34_inData;
  int32_T c34_b_inData;
  int32_T c34_b_inData_sizes;
  int32_T c34_loop_ub;
  int32_T c34_i11;
  uint8_T c34_b_inData_data[64];
  int32_T c34_b_loop_ub;
  int32_T c34_i12;
  uint8_T c34_u_data[64];
  const mxArray *c34_y = NULL;
  SFc34_speedTestInstanceStruct *chartInstance;
  chartInstance = (SFc34_speedTestInstanceStruct *)chartInstanceVoid;
  c34_mxArrayOutData = NULL;
  c34_u_sizes[0] = 1;
  c34_u_sizes[1] = c34_inData_sizes[1];
  c34_u = c34_u_sizes[0];
  c34_b_u = c34_u_sizes[1];
  c34_inData = c34_inData_sizes[0];
  c34_b_inData = c34_inData_sizes[1];
  c34_b_inData_sizes = c34_inData * c34_b_inData;
  c34_loop_ub = c34_inData * c34_b_inData - 1;
  for (c34_i11 = 0; c34_i11 <= c34_loop_ub; c34_i11++) {
    c34_b_inData_data[c34_i11] = c34_inData_data[c34_i11];
  }

  c34_b_loop_ub = c34_b_inData_sizes - 1;
  for (c34_i12 = 0; c34_i12 <= c34_b_loop_ub; c34_i12++) {
    c34_u_data[c34_i12] = c34_b_inData_data[c34_i12];
  }

  c34_y = NULL;
  sf_mex_assign(&c34_y, sf_mex_create("y", c34_u_data, 3, 0U, 1U, 0U, 2,
    c34_u_sizes[0], c34_u_sizes[1]), false);
  sf_mex_assign(&c34_mxArrayOutData, c34_y, false);
  return c34_mxArrayOutData;
}

static const mxArray *c34_c_sf_marshallOut(void *chartInstanceVoid, void
  *c34_inData)
{
  const mxArray *c34_mxArrayOutData = NULL;
  real_T c34_u;
  const mxArray *c34_y = NULL;
  SFc34_speedTestInstanceStruct *chartInstance;
  chartInstance = (SFc34_speedTestInstanceStruct *)chartInstanceVoid;
  c34_mxArrayOutData = NULL;
  c34_u = *(real_T *)c34_inData;
  c34_y = NULL;
  sf_mex_assign(&c34_y, sf_mex_create("y", &c34_u, 0, 0U, 0U, 0U, 0), false);
  sf_mex_assign(&c34_mxArrayOutData, c34_y, false);
  return c34_mxArrayOutData;
}

static real_T c34_c_emlrt_marshallIn(SFc34_speedTestInstanceStruct
  *chartInstance, const mxArray *c34_u, const emlrtMsgIdentifier *c34_parentId)
{
  real_T c34_y;
  real_T c34_d0;
  (void)chartInstance;
  sf_mex_import(c34_parentId, sf_mex_dup(c34_u), &c34_d0, 1, 0, 0U, 0, 0U, 0);
  c34_y = c34_d0;
  sf_mex_destroy(&c34_u);
  return c34_y;
}

static void c34_b_sf_marshallIn(void *chartInstanceVoid, const mxArray
  *c34_mxArrayInData, const char_T *c34_varName, void *c34_outData)
{
  const mxArray *c34_nargout;
  const char_T *c34_identifier;
  emlrtMsgIdentifier c34_thisId;
  real_T c34_y;
  SFc34_speedTestInstanceStruct *chartInstance;
  chartInstance = (SFc34_speedTestInstanceStruct *)chartInstanceVoid;
  c34_nargout = sf_mex_dup(c34_mxArrayInData);
  c34_identifier = c34_varName;
  c34_thisId.fIdentifier = c34_identifier;
  c34_thisId.fParent = NULL;
  c34_y = c34_c_emlrt_marshallIn(chartInstance, sf_mex_dup(c34_nargout),
    &c34_thisId);
  sf_mex_destroy(&c34_nargout);
  *(real_T *)c34_outData = c34_y;
  sf_mex_destroy(&c34_mxArrayInData);
}

const mxArray *sf_c34_speedTest_get_eml_resolved_functions_info(void)
{
  const mxArray *c34_nameCaptureInfo = NULL;
  c34_nameCaptureInfo = NULL;
  sf_mex_assign(&c34_nameCaptureInfo, sf_mex_create("nameCaptureInfo", NULL, 0,
    0U, 1U, 0U, 2, 0, 1), false);
  return c34_nameCaptureInfo;
}

static const mxArray *c34_d_sf_marshallOut(void *chartInstanceVoid, void
  *c34_inData)
{
  const mxArray *c34_mxArrayOutData = NULL;
  int32_T c34_u;
  const mxArray *c34_y = NULL;
  SFc34_speedTestInstanceStruct *chartInstance;
  chartInstance = (SFc34_speedTestInstanceStruct *)chartInstanceVoid;
  c34_mxArrayOutData = NULL;
  c34_u = *(int32_T *)c34_inData;
  c34_y = NULL;
  sf_mex_assign(&c34_y, sf_mex_create("y", &c34_u, 6, 0U, 0U, 0U, 0), false);
  sf_mex_assign(&c34_mxArrayOutData, c34_y, false);
  return c34_mxArrayOutData;
}

static int32_T c34_d_emlrt_marshallIn(SFc34_speedTestInstanceStruct
  *chartInstance, const mxArray *c34_u, const emlrtMsgIdentifier *c34_parentId)
{
  int32_T c34_y;
  int32_T c34_i13;
  (void)chartInstance;
  sf_mex_import(c34_parentId, sf_mex_dup(c34_u), &c34_i13, 1, 6, 0U, 0, 0U, 0);
  c34_y = c34_i13;
  sf_mex_destroy(&c34_u);
  return c34_y;
}

static void c34_c_sf_marshallIn(void *chartInstanceVoid, const mxArray
  *c34_mxArrayInData, const char_T *c34_varName, void *c34_outData)
{
  const mxArray *c34_b_sfEvent;
  const char_T *c34_identifier;
  emlrtMsgIdentifier c34_thisId;
  int32_T c34_y;
  SFc34_speedTestInstanceStruct *chartInstance;
  chartInstance = (SFc34_speedTestInstanceStruct *)chartInstanceVoid;
  c34_b_sfEvent = sf_mex_dup(c34_mxArrayInData);
  c34_identifier = c34_varName;
  c34_thisId.fIdentifier = c34_identifier;
  c34_thisId.fParent = NULL;
  c34_y = c34_d_emlrt_marshallIn(chartInstance, sf_mex_dup(c34_b_sfEvent),
    &c34_thisId);
  sf_mex_destroy(&c34_b_sfEvent);
  *(int32_T *)c34_outData = c34_y;
  sf_mex_destroy(&c34_mxArrayInData);
}

static uint8_T c34_e_emlrt_marshallIn(SFc34_speedTestInstanceStruct
  *chartInstance, const mxArray *c34_b_is_active_c34_speedTest, const char_T
  *c34_identifier)
{
  uint8_T c34_y;
  emlrtMsgIdentifier c34_thisId;
  c34_thisId.fIdentifier = c34_identifier;
  c34_thisId.fParent = NULL;
  c34_y = c34_f_emlrt_marshallIn(chartInstance, sf_mex_dup
    (c34_b_is_active_c34_speedTest), &c34_thisId);
  sf_mex_destroy(&c34_b_is_active_c34_speedTest);
  return c34_y;
}

static uint8_T c34_f_emlrt_marshallIn(SFc34_speedTestInstanceStruct
  *chartInstance, const mxArray *c34_u, const emlrtMsgIdentifier *c34_parentId)
{
  uint8_T c34_y;
  uint8_T c34_u0;
  (void)chartInstance;
  sf_mex_import(c34_parentId, sf_mex_dup(c34_u), &c34_u0, 1, 3, 0U, 0, 0U, 0);
  c34_y = c34_u0;
  sf_mex_destroy(&c34_u);
  return c34_y;
}

static void init_dsm_address_info(SFc34_speedTestInstanceStruct *chartInstance)
{
  (void)chartInstance;
}

static void init_simulink_io_address(SFc34_speedTestInstanceStruct
  *chartInstance)
{
  chartInstance->c34_messageBuffer_data = (uint8_T (*)[64])
    ssGetInputPortSignal_wrapper(chartInstance->S, 0);
  chartInstance->c34_messageBuffer_sizes = (int32_T (*)[2])
    ssGetCurrentInputPortDimensions_wrapper(chartInstance->S, 0);
  sf_mex_size_one_check(((*chartInstance->c34_messageBuffer_sizes)[0U] == 0) &&
                        (!((*chartInstance->c34_messageBuffer_sizes)[1U] == 0)),
                        "messageBuffer");
  chartInstance->c34_message = (uint8_T (*)[31])ssGetOutputPortSignal_wrapper
    (chartInstance->S, 1);
}

/* SFunction Glue Code */
#ifdef utFree
#undef utFree
#endif

#ifdef utMalloc
#undef utMalloc
#endif

#ifdef __cplusplus

extern "C" void *utMalloc(size_t size);
extern "C" void utFree(void*);

#else

extern void *utMalloc(size_t size);
extern void utFree(void*);

#endif

void sf_c34_speedTest_get_check_sum(mxArray *plhs[])
{
  ((real_T *)mxGetPr((plhs[0])))[0] = (real_T)(1042695787U);
  ((real_T *)mxGetPr((plhs[0])))[1] = (real_T)(2659539410U);
  ((real_T *)mxGetPr((plhs[0])))[2] = (real_T)(1804896145U);
  ((real_T *)mxGetPr((plhs[0])))[3] = (real_T)(1827938802U);
}

mxArray* sf_c34_speedTest_get_post_codegen_info(void);
mxArray *sf_c34_speedTest_get_autoinheritance_info(void)
{
  const char *autoinheritanceFields[] = { "checksum", "inputs", "parameters",
    "outputs", "locals", "postCodegenInfo" };

  mxArray *mxAutoinheritanceInfo = mxCreateStructMatrix(1, 1, sizeof
    (autoinheritanceFields)/sizeof(autoinheritanceFields[0]),
    autoinheritanceFields);

  {
    mxArray *mxChecksum = mxCreateString("2z7Fwh6WZoLlXSSoEUax0D");
    mxSetField(mxAutoinheritanceInfo,0,"checksum",mxChecksum);
  }

  {
    const char *dataFields[] = { "size", "type", "complexity" };

    mxArray *mxData = mxCreateStructMatrix(1,1,3,dataFields);

    {
      mxArray *mxSize = mxCreateDoubleMatrix(1,2,mxREAL);
      double *pr = mxGetPr(mxSize);
      pr[0] = (double)(1);
      pr[1] = (double)(64);
      mxSetField(mxData,0,"size",mxSize);
    }

    {
      const char *typeFields[] = { "base", "fixpt" };

      mxArray *mxType = mxCreateStructMatrix(1,1,2,typeFields);
      mxSetField(mxType,0,"base",mxCreateDoubleScalar(3));
      mxSetField(mxType,0,"fixpt",mxCreateDoubleMatrix(0,0,mxREAL));
      mxSetField(mxData,0,"type",mxType);
    }

    mxSetField(mxData,0,"complexity",mxCreateDoubleScalar(0));
    mxSetField(mxAutoinheritanceInfo,0,"inputs",mxData);
  }

  {
    mxSetField(mxAutoinheritanceInfo,0,"parameters",mxCreateDoubleMatrix(0,0,
                mxREAL));
  }

  {
    const char *dataFields[] = { "size", "type", "complexity" };

    mxArray *mxData = mxCreateStructMatrix(1,1,3,dataFields);

    {
      mxArray *mxSize = mxCreateDoubleMatrix(1,2,mxREAL);
      double *pr = mxGetPr(mxSize);
      pr[0] = (double)(1);
      pr[1] = (double)(31);
      mxSetField(mxData,0,"size",mxSize);
    }

    {
      const char *typeFields[] = { "base", "fixpt" };

      mxArray *mxType = mxCreateStructMatrix(1,1,2,typeFields);
      mxSetField(mxType,0,"base",mxCreateDoubleScalar(3));
      mxSetField(mxType,0,"fixpt",mxCreateDoubleMatrix(0,0,mxREAL));
      mxSetField(mxData,0,"type",mxType);
    }

    mxSetField(mxData,0,"complexity",mxCreateDoubleScalar(0));
    mxSetField(mxAutoinheritanceInfo,0,"outputs",mxData);
  }

  {
    mxSetField(mxAutoinheritanceInfo,0,"locals",mxCreateDoubleMatrix(0,0,mxREAL));
  }

  {
    mxArray* mxPostCodegenInfo = sf_c34_speedTest_get_post_codegen_info();
    mxSetField(mxAutoinheritanceInfo,0,"postCodegenInfo",mxPostCodegenInfo);
  }

  return(mxAutoinheritanceInfo);
}

mxArray *sf_c34_speedTest_third_party_uses_info(void)
{
  mxArray * mxcell3p = mxCreateCellMatrix(1,0);
  return(mxcell3p);
}

mxArray *sf_c34_speedTest_jit_fallback_info(void)
{
  const char *infoFields[] = { "fallbackType", "fallbackReason",
    "incompatibleSymbol", };

  mxArray *mxInfo = mxCreateStructMatrix(1, 1, 3, infoFields);
  mxArray *fallbackReason = mxCreateString("feature_off");
  mxArray *incompatibleSymbol = mxCreateString("");
  mxArray *fallbackType = mxCreateString("early");
  mxSetField(mxInfo, 0, infoFields[0], fallbackType);
  mxSetField(mxInfo, 0, infoFields[1], fallbackReason);
  mxSetField(mxInfo, 0, infoFields[2], incompatibleSymbol);
  return mxInfo;
}

mxArray *sf_c34_speedTest_updateBuildInfo_args_info(void)
{
  mxArray *mxBIArgs = mxCreateCellMatrix(1,0);
  return mxBIArgs;
}

mxArray* sf_c34_speedTest_get_post_codegen_info(void)
{
  const char* fieldNames[] = { "exportedFunctionsUsedByThisChart",
    "exportedFunctionsChecksum" };

  mwSize dims[2] = { 1, 1 };

  mxArray* mxPostCodegenInfo = mxCreateStructArray(2, dims, sizeof(fieldNames)/
    sizeof(fieldNames[0]), fieldNames);

  {
    mxArray* mxExportedFunctionsChecksum = mxCreateString("");
    mwSize exp_dims[2] = { 0, 1 };

    mxArray* mxExportedFunctionsUsedByThisChart = mxCreateCellArray(2, exp_dims);
    mxSetField(mxPostCodegenInfo, 0, "exportedFunctionsUsedByThisChart",
               mxExportedFunctionsUsedByThisChart);
    mxSetField(mxPostCodegenInfo, 0, "exportedFunctionsChecksum",
               mxExportedFunctionsChecksum);
  }

  return mxPostCodegenInfo;
}

static const mxArray *sf_get_sim_state_info_c34_speedTest(void)
{
  const char *infoFields[] = { "chartChecksum", "varInfo" };

  mxArray *mxInfo = mxCreateStructMatrix(1, 1, 2, infoFields);
  const char *infoEncStr[] = {
    "100 S1x2'type','srcId','name','auxInfo'{{M[1],M[5],T\"message\",},{M[8],M[0],T\"is_active_c34_speedTest\",}}"
  };

  mxArray *mxVarInfo = sf_mex_decode_encoded_mx_struct_array(infoEncStr, 2, 10);
  mxArray *mxChecksum = mxCreateDoubleMatrix(1, 4, mxREAL);
  sf_c34_speedTest_get_check_sum(&mxChecksum);
  mxSetField(mxInfo, 0, infoFields[0], mxChecksum);
  mxSetField(mxInfo, 0, infoFields[1], mxVarInfo);
  return mxInfo;
}

static void chart_debug_initialization(SimStruct *S, unsigned int
  fullDebuggerInitialization)
{
  if (!sim_mode_is_rtw_gen(S)) {
    SFc34_speedTestInstanceStruct *chartInstance;
    ChartRunTimeInfo * crtInfo = (ChartRunTimeInfo *)(ssGetUserData(S));
    ChartInfoStruct * chartInfo = (ChartInfoStruct *)(crtInfo->instanceInfo);
    chartInstance = (SFc34_speedTestInstanceStruct *) chartInfo->chartInstance;
    if (ssIsFirstInitCond(S) && fullDebuggerInitialization==1) {
      /* do this only if simulation is starting */
      {
        unsigned int chartAlreadyPresent;
        chartAlreadyPresent = sf_debug_initialize_chart
          (sfGlobalDebugInstanceStruct,
           _speedTestMachineNumber_,
           34,
           1,
           1,
           0,
           2,
           0,
           0,
           0,
           0,
           0,
           &(chartInstance->chartNumber),
           &(chartInstance->instanceNumber),
           (void *)S);

        /* Each instance must initialize its own list of scripts */
        init_script_number_translation(_speedTestMachineNumber_,
          chartInstance->chartNumber,chartInstance->instanceNumber);
        if (chartAlreadyPresent==0) {
          /* this is the first instance */
          sf_debug_set_chart_disable_implicit_casting
            (sfGlobalDebugInstanceStruct,_speedTestMachineNumber_,
             chartInstance->chartNumber,1);
          sf_debug_set_chart_event_thresholds(sfGlobalDebugInstanceStruct,
            _speedTestMachineNumber_,
            chartInstance->chartNumber,
            0,
            0,
            0);
          _SFD_SET_DATA_PROPS(0,1,1,0,"messageBuffer");
          _SFD_SET_DATA_PROPS(1,2,0,1,"message");
          _SFD_STATE_INFO(0,0,2);
          _SFD_CH_SUBSTATE_COUNT(0);
          _SFD_CH_SUBSTATE_DECOMP(0);
        }

        _SFD_CV_INIT_CHART(0,0,0,0);

        {
          _SFD_CV_INIT_STATE(0,0,0,0,0,0,NULL,NULL);
        }

        _SFD_CV_INIT_TRANS(0,0,NULL,NULL,0,NULL);

        /* Initialization of MATLAB Function Model Coverage */
        _SFD_CV_INIT_EML(0,1,1,0,0,0,0,0,0,0,0);
        _SFD_CV_INIT_EML_FCN(0,0,"eML_blk_kernel",0,-1,81);

        {
          unsigned int dimVector[2];
          dimVector[0]= 1;
          dimVector[1]= 64;
          _SFD_SET_DATA_COMPILED_PROPS(0,SF_UINT8,2,&(dimVector[0]),0,0,0,0.0,
            1.0,0,0,(MexFcnForType)c34_b_sf_marshallOut,(MexInFcnForType)NULL);
        }

        {
          unsigned int dimVector[2];
          dimVector[0]= 1;
          dimVector[1]= 31;
          _SFD_SET_DATA_COMPILED_PROPS(1,SF_UINT8,2,&(dimVector[0]),0,0,0,0.0,
            1.0,0,0,(MexFcnForType)c34_sf_marshallOut,(MexInFcnForType)
            c34_sf_marshallIn);
        }

        _SFD_SET_DATA_VALUE_PTR_VAR_DIM(0U,
          *chartInstance->c34_messageBuffer_data, (void *)
          chartInstance->c34_messageBuffer_sizes);
        _SFD_SET_DATA_VALUE_PTR(1U, *chartInstance->c34_message);
      }
    } else {
      sf_debug_reset_current_state_configuration(sfGlobalDebugInstanceStruct,
        _speedTestMachineNumber_,chartInstance->chartNumber,
        chartInstance->instanceNumber);
    }
  }
}

static const char* sf_get_instance_specialization(void)
{
  return "xNYm6GNN8jSBExJBkcuC2B";
}

static void sf_opaque_initialize_c34_speedTest(void *chartInstanceVar)
{
  chart_debug_initialization(((SFc34_speedTestInstanceStruct*) chartInstanceVar
    )->S,0);
  initialize_params_c34_speedTest((SFc34_speedTestInstanceStruct*)
    chartInstanceVar);
  initialize_c34_speedTest((SFc34_speedTestInstanceStruct*) chartInstanceVar);
}

static void sf_opaque_enable_c34_speedTest(void *chartInstanceVar)
{
  enable_c34_speedTest((SFc34_speedTestInstanceStruct*) chartInstanceVar);
}

static void sf_opaque_disable_c34_speedTest(void *chartInstanceVar)
{
  disable_c34_speedTest((SFc34_speedTestInstanceStruct*) chartInstanceVar);
}

static void sf_opaque_gateway_c34_speedTest(void *chartInstanceVar)
{
  sf_gateway_c34_speedTest((SFc34_speedTestInstanceStruct*) chartInstanceVar);
}

static const mxArray* sf_opaque_get_sim_state_c34_speedTest(SimStruct* S)
{
  ChartRunTimeInfo * crtInfo = (ChartRunTimeInfo *)(ssGetUserData(S));
  ChartInfoStruct * chartInfo = (ChartInfoStruct *)(crtInfo->instanceInfo);
  return get_sim_state_c34_speedTest((SFc34_speedTestInstanceStruct*)
    chartInfo->chartInstance);         /* raw sim ctx */
}

static void sf_opaque_set_sim_state_c34_speedTest(SimStruct* S, const mxArray
  *st)
{
  ChartRunTimeInfo * crtInfo = (ChartRunTimeInfo *)(ssGetUserData(S));
  ChartInfoStruct * chartInfo = (ChartInfoStruct *)(crtInfo->instanceInfo);
  set_sim_state_c34_speedTest((SFc34_speedTestInstanceStruct*)
    chartInfo->chartInstance, st);
}

static void sf_opaque_terminate_c34_speedTest(void *chartInstanceVar)
{
  if (chartInstanceVar!=NULL) {
    SimStruct *S = ((SFc34_speedTestInstanceStruct*) chartInstanceVar)->S;
    ChartRunTimeInfo * crtInfo = (ChartRunTimeInfo *)(ssGetUserData(S));
    if (sim_mode_is_rtw_gen(S) || sim_mode_is_external(S)) {
      sf_clear_rtw_identifier(S);
      unload_speedTest_optimization_info();
    }

    finalize_c34_speedTest((SFc34_speedTestInstanceStruct*) chartInstanceVar);
    utFree(chartInstanceVar);
    if (crtInfo != NULL) {
      utFree(crtInfo);
    }

    ssSetUserData(S,NULL);
  }
}

static void sf_opaque_init_subchart_simstructs(void *chartInstanceVar)
{
  initSimStructsc34_speedTest((SFc34_speedTestInstanceStruct*) chartInstanceVar);
}

extern unsigned int sf_machine_global_initializer_called(void);
static void mdlProcessParameters_c34_speedTest(SimStruct *S)
{
  int i;
  for (i=0;i<ssGetNumRunTimeParams(S);i++) {
    if (ssGetSFcnParamTunable(S,i)) {
      ssUpdateDlgParamAsRunTimeParam(S,i);
    }
  }

  if (sf_machine_global_initializer_called()) {
    ChartRunTimeInfo * crtInfo = (ChartRunTimeInfo *)(ssGetUserData(S));
    ChartInfoStruct * chartInfo = (ChartInfoStruct *)(crtInfo->instanceInfo);
    initialize_params_c34_speedTest((SFc34_speedTestInstanceStruct*)
      (chartInfo->chartInstance));
  }
}

static void mdlSetWorkWidths_c34_speedTest(SimStruct *S)
{
  if (sim_mode_is_rtw_gen(S) || sim_mode_is_external(S)) {
    mxArray *infoStruct = load_speedTest_optimization_info();
    int_T chartIsInlinable =
      (int_T)sf_is_chart_inlinable(sf_get_instance_specialization(),infoStruct,
      34);
    ssSetStateflowIsInlinable(S,chartIsInlinable);
    ssSetRTWCG(S,sf_rtw_info_uint_prop(sf_get_instance_specialization(),
                infoStruct,34,"RTWCG"));
    ssSetEnableFcnIsTrivial(S,1);
    ssSetDisableFcnIsTrivial(S,1);
    ssSetNotMultipleInlinable(S,sf_rtw_info_uint_prop
      (sf_get_instance_specialization(),infoStruct,34,
       "gatewayCannotBeInlinedMultipleTimes"));
    sf_update_buildInfo(sf_get_instance_specialization(),infoStruct,34);
    if (chartIsInlinable) {
      ssSetInputPortOptimOpts(S, 0, SS_REUSABLE_AND_LOCAL);
      sf_mark_chart_expressionable_inputs(S,sf_get_instance_specialization(),
        infoStruct,34,1);
      sf_mark_chart_reusable_outputs(S,sf_get_instance_specialization(),
        infoStruct,34,1);
    }

    {
      unsigned int outPortIdx;
      for (outPortIdx=1; outPortIdx<=1; ++outPortIdx) {
        ssSetOutputPortOptimizeInIR(S, outPortIdx, 1U);
      }
    }

    {
      unsigned int inPortIdx;
      for (inPortIdx=0; inPortIdx < 1; ++inPortIdx) {
        ssSetInputPortOptimizeInIR(S, inPortIdx, 1U);
      }
    }

    sf_set_rtw_dwork_info(S,sf_get_instance_specialization(),infoStruct,34);
    ssSetHasSubFunctions(S,!(chartIsInlinable));
  } else {
  }

  ssSetOptions(S,ssGetOptions(S)|SS_OPTION_WORKS_WITH_CODE_REUSE);
  ssSetChecksum0(S,(2041924168U));
  ssSetChecksum1(S,(2094826546U));
  ssSetChecksum2(S,(3970551402U));
  ssSetChecksum3(S,(769607429U));
  ssSetmdlDerivatives(S, NULL);
  ssSetExplicitFCSSCtrl(S,1);
  ssSupportsMultipleExecInstances(S,1);
}

static void mdlRTW_c34_speedTest(SimStruct *S)
{
  if (sim_mode_is_rtw_gen(S)) {
    ssWriteRTWStrParam(S, "StateflowChartType", "Embedded MATLAB");
  }
}

static void mdlStart_c34_speedTest(SimStruct *S)
{
  SFc34_speedTestInstanceStruct *chartInstance;
  ChartRunTimeInfo * crtInfo = (ChartRunTimeInfo *)utMalloc(sizeof
    (ChartRunTimeInfo));
  chartInstance = (SFc34_speedTestInstanceStruct *)utMalloc(sizeof
    (SFc34_speedTestInstanceStruct));
  memset(chartInstance, 0, sizeof(SFc34_speedTestInstanceStruct));
  if (chartInstance==NULL) {
    sf_mex_error_message("Could not allocate memory for chart instance.");
  }

  chartInstance->chartInfo.chartInstance = chartInstance;
  chartInstance->chartInfo.isEMLChart = 1;
  chartInstance->chartInfo.chartInitialized = 0;
  chartInstance->chartInfo.sFunctionGateway = sf_opaque_gateway_c34_speedTest;
  chartInstance->chartInfo.initializeChart = sf_opaque_initialize_c34_speedTest;
  chartInstance->chartInfo.terminateChart = sf_opaque_terminate_c34_speedTest;
  chartInstance->chartInfo.enableChart = sf_opaque_enable_c34_speedTest;
  chartInstance->chartInfo.disableChart = sf_opaque_disable_c34_speedTest;
  chartInstance->chartInfo.getSimState = sf_opaque_get_sim_state_c34_speedTest;
  chartInstance->chartInfo.setSimState = sf_opaque_set_sim_state_c34_speedTest;
  chartInstance->chartInfo.getSimStateInfo = sf_get_sim_state_info_c34_speedTest;
  chartInstance->chartInfo.zeroCrossings = NULL;
  chartInstance->chartInfo.outputs = NULL;
  chartInstance->chartInfo.derivatives = NULL;
  chartInstance->chartInfo.mdlRTW = mdlRTW_c34_speedTest;
  chartInstance->chartInfo.mdlStart = mdlStart_c34_speedTest;
  chartInstance->chartInfo.mdlSetWorkWidths = mdlSetWorkWidths_c34_speedTest;
  chartInstance->chartInfo.extModeExec = NULL;
  chartInstance->chartInfo.restoreLastMajorStepConfiguration = NULL;
  chartInstance->chartInfo.restoreBeforeLastMajorStepConfiguration = NULL;
  chartInstance->chartInfo.storeCurrentConfiguration = NULL;
  chartInstance->chartInfo.callAtomicSubchartUserFcn = NULL;
  chartInstance->chartInfo.callAtomicSubchartAutoFcn = NULL;
  chartInstance->chartInfo.debugInstance = sfGlobalDebugInstanceStruct;
  chartInstance->S = S;
  crtInfo->checksum = SF_RUNTIME_INFO_CHECKSUM;
  crtInfo->instanceInfo = (&(chartInstance->chartInfo));
  crtInfo->isJITEnabled = false;
  crtInfo->compiledInfo = NULL;
  ssSetUserData(S,(void *)(crtInfo));  /* register the chart instance with simstruct */
  init_dsm_address_info(chartInstance);
  init_simulink_io_address(chartInstance);
  if (!sim_mode_is_rtw_gen(S)) {
  }

  sf_opaque_init_subchart_simstructs(chartInstance->chartInfo.chartInstance);
  chart_debug_initialization(S,1);
}

void c34_speedTest_method_dispatcher(SimStruct *S, int_T method, void *data)
{
  switch (method) {
   case SS_CALL_MDL_START:
    mdlStart_c34_speedTest(S);
    break;

   case SS_CALL_MDL_SET_WORK_WIDTHS:
    mdlSetWorkWidths_c34_speedTest(S);
    break;

   case SS_CALL_MDL_PROCESS_PARAMETERS:
    mdlProcessParameters_c34_speedTest(S);
    break;

   default:
    /* Unhandled method */
    sf_mex_error_message("Stateflow Internal Error:\n"
                         "Error calling c34_speedTest_method_dispatcher.\n"
                         "Can't handle method %d.\n", method);
    break;
  }
}

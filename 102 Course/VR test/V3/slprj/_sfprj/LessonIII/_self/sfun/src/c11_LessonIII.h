#ifndef __c11_LessonIII_h__
#define __c11_LessonIII_h__

/* Include files */
#include "sf_runtime/sfc_sf.h"
#include "sf_runtime/sfc_mex.h"
#include "rtwtypes.h"
#include "multiword_types.h"

/* Type Definitions */
#ifndef struct_Player_tag
#define struct_Player_tag

struct Player_tag
{
  int8_T x;
  int8_T y;
  int16_T orientation;
  uint8_T color;
  uint8_T position;
  uint8_T valid;
};

#endif                                 /*struct_Player_tag*/

#ifndef typedef_c11_Player
#define typedef_c11_Player

typedef struct Player_tag c11_Player;

#endif                                 /*typedef_c11_Player*/

#ifndef struct_Ball_tag
#define struct_Ball_tag

struct Ball_tag
{
  int8_T x;
  int8_T y;
  uint8_T valid;
};

#endif                                 /*struct_Ball_tag*/

#ifndef typedef_c11_Ball
#define typedef_c11_Ball

typedef struct Ball_tag c11_Ball;

#endif                                 /*typedef_c11_Ball*/

#ifndef struct_Waypoint_tag
#define struct_Waypoint_tag

struct Waypoint_tag
{
  int8_T x;
  int8_T y;
  int16_T orientation;
};

#endif                                 /*struct_Waypoint_tag*/

#ifndef typedef_c11_Waypoint
#define typedef_c11_Waypoint

typedef struct Waypoint_tag c11_Waypoint;

#endif                                 /*typedef_c11_Waypoint*/

#ifndef typedef_SFc11_LessonIIIInstanceStruct
#define typedef_SFc11_LessonIIIInstanceStruct

typedef struct {
  SimStruct *S;
  ChartInfoStruct chartInfo;
  uint32_T chartNumber;
  uint32_T instanceNumber;
  int32_T c11_sfEvent;
  uint8_T c11_tp_GameIsOn_Goalie;
  uint8_T c11_tp_goalie;
  uint8_T c11_tp_Idle;
  uint8_T c11_tp_waiting;
  uint8_T c11_tp_Idle1;
  uint8_T c11_tp_goToPosition;
  boolean_T c11_isStable;
  uint8_T c11_is_active_c11_LessonIII;
  uint8_T c11_is_c11_LessonIII;
  uint8_T c11_is_GameIsOn_Goalie;
  uint8_T c11_is_waiting;
  int8_T c11_startingPos[2];
  boolean_T c11_dataWrittenToVector[3];
  uint8_T c11_doSetSimStateSideEffects;
  const mxArray *c11_setSimStateSideEffectsInfo;
  c11_Player *c11_me;
  c11_Player (*c11_players)[6];
  c11_Ball *c11_ball;
  c11_Waypoint *c11_finalWay;
  c11_Waypoint *c11_manualWay;
  uint8_T *c11_GameOn;
} SFc11_LessonIIIInstanceStruct;

#endif                                 /*typedef_SFc11_LessonIIIInstanceStruct*/

/* Named Constants */

/* Variable Declarations */
extern struct SfDebugInstanceStruct *sfGlobalDebugInstanceStruct;

/* Variable Definitions */

/* Function Declarations */
extern const mxArray *sf_c11_LessonIII_get_eml_resolved_functions_info(void);

/* Function Definitions */
extern void sf_c11_LessonIII_get_check_sum(mxArray *plhs[]);
extern void c11_LessonIII_method_dispatcher(SimStruct *S, int_T method, void
  *data);

#endif

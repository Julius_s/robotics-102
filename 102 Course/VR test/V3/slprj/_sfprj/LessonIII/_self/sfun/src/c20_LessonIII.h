#ifndef __c20_LessonIII_h__
#define __c20_LessonIII_h__

/* Include files */
#include "sf_runtime/sfc_sf.h"
#include "sf_runtime/sfc_mex.h"
#include "rtwtypes.h"
#include "multiword_types.h"

/* Type Definitions */
#ifndef typedef_SFc20_LessonIIIInstanceStruct
#define typedef_SFc20_LessonIIIInstanceStruct

typedef struct {
  SimStruct *S;
  ChartInfoStruct chartInfo;
  uint32_T chartNumber;
  uint32_T instanceNumber;
  int32_T c20_sfEvent;
  boolean_T c20_isStable;
  boolean_T c20_doneDoubleBufferReInit;
  uint8_T c20_is_active_c20_LessonIII;
  int16_T *c20_targetO;
  int16_T *c20_realDorien;
  int16_T *c20_direc;
  int16_T *c20_myO;
} SFc20_LessonIIIInstanceStruct;

#endif                                 /*typedef_SFc20_LessonIIIInstanceStruct*/

/* Named Constants */

/* Variable Declarations */
extern struct SfDebugInstanceStruct *sfGlobalDebugInstanceStruct;

/* Variable Definitions */

/* Function Declarations */
extern const mxArray *sf_c20_LessonIII_get_eml_resolved_functions_info(void);

/* Function Definitions */
extern void sf_c20_LessonIII_get_check_sum(mxArray *plhs[]);
extern void c20_LessonIII_method_dispatcher(SimStruct *S, int_T method, void
  *data);

#endif

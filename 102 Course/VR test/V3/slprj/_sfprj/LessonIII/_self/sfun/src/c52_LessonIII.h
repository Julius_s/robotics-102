#ifndef __c52_LessonIII_h__
#define __c52_LessonIII_h__

/* Include files */
#include "sf_runtime/sfc_sf.h"
#include "sf_runtime/sfc_mex.h"
#include "rtwtypes.h"
#include "multiword_types.h"

/* Type Definitions */
#ifndef struct_Player_tag
#define struct_Player_tag

struct Player_tag
{
  int8_T x;
  int8_T y;
  int16_T orientation;
  uint8_T color;
  uint8_T position;
  uint8_T valid;
};

#endif                                 /*struct_Player_tag*/

#ifndef typedef_c52_Player
#define typedef_c52_Player

typedef struct Player_tag c52_Player;

#endif                                 /*typedef_c52_Player*/

#ifndef struct_Ball_tag
#define struct_Ball_tag

struct Ball_tag
{
  int8_T x;
  int8_T y;
  uint8_T valid;
};

#endif                                 /*struct_Ball_tag*/

#ifndef typedef_c52_Ball
#define typedef_c52_Ball

typedef struct Ball_tag c52_Ball;

#endif                                 /*typedef_c52_Ball*/

#ifndef struct_Waypoint_tag
#define struct_Waypoint_tag

struct Waypoint_tag
{
  int8_T x;
  int8_T y;
  int16_T orientation;
};

#endif                                 /*struct_Waypoint_tag*/

#ifndef typedef_c52_Waypoint
#define typedef_c52_Waypoint

typedef struct Waypoint_tag c52_Waypoint;

#endif                                 /*typedef_c52_Waypoint*/

#ifndef typedef_SFc52_LessonIIIInstanceStruct
#define typedef_SFc52_LessonIIIInstanceStruct

typedef struct {
  SimStruct *S;
  ChartInfoStruct chartInfo;
  uint32_T chartNumber;
  uint32_T instanceNumber;
  int32_T c52_sfEvent;
  uint8_T c52_tp_GameIsOn_Offence;
  uint8_T c52_tp_GetToTheBall;
  uint8_T c52_tp_Idle;
  uint8_T c52_tp_Aim;
  uint8_T c52_tp_Kick;
  uint8_T c52_tp_waiting;
  uint8_T c52_tp_Idle1;
  uint8_T c52_tp_goToPosition;
  boolean_T c52_isStable;
  uint8_T c52_is_active_c52_LessonIII;
  uint8_T c52_is_c52_LessonIII;
  uint8_T c52_is_GameIsOn_Offence;
  uint8_T c52_is_waiting;
  int8_T c52_startingPos[2];
  c52_Player c52_enemy[3];
  c52_Player c52_friends[3];
  c52_Waypoint c52_shootWay;
  c52_Waypoint c52_kickWay;
  uint8_T c52_temporalCounter_i1;
  boolean_T c52_dataWrittenToVector[7];
  uint8_T c52_doSetSimStateSideEffects;
  const mxArray *c52_setSimStateSideEffectsInfo;
  c52_Player *c52_me;
  c52_Player (*c52_players)[6];
  c52_Ball *c52_ball;
  c52_Waypoint *c52_finalWay;
  c52_Waypoint *c52_manualWay;
  uint8_T *c52_GameOn;
} SFc52_LessonIIIInstanceStruct;

#endif                                 /*typedef_SFc52_LessonIIIInstanceStruct*/

/* Named Constants */

/* Variable Declarations */
extern struct SfDebugInstanceStruct *sfGlobalDebugInstanceStruct;

/* Variable Definitions */

/* Function Declarations */
extern const mxArray *sf_c52_LessonIII_get_eml_resolved_functions_info(void);

/* Function Definitions */
extern void sf_c52_LessonIII_get_check_sum(mxArray *plhs[]);
extern void c52_LessonIII_method_dispatcher(SimStruct *S, int_T method, void
  *data);

#endif

#ifndef __c30_LessonIII_h__
#define __c30_LessonIII_h__

/* Include files */
#include "sf_runtime/sfc_sf.h"
#include "sf_runtime/sfc_mex.h"
#include "rtwtypes.h"
#include "multiword_types.h"

/* Type Definitions */
#ifndef typedef_SFc30_LessonIIIInstanceStruct
#define typedef_SFc30_LessonIIIInstanceStruct

typedef struct {
  SimStruct *S;
  ChartInfoStruct chartInfo;
  uint32_T chartNumber;
  uint32_T instanceNumber;
  int32_T c30_sfEvent;
  boolean_T c30_isStable;
  boolean_T c30_doneDoubleBufferReInit;
  uint8_T c30_is_active_c30_LessonIII;
  real_T (*c30_leftArray)[6];
  real_T (*c30_left)[6];
  real_T (*c30_rightArray)[6];
  uint8_T *c30_playerPos;
  real_T (*c30_right)[6];
  real_T *c30_playerLeft;
  real_T *c30_playerRight;
} SFc30_LessonIIIInstanceStruct;

#endif                                 /*typedef_SFc30_LessonIIIInstanceStruct*/

/* Named Constants */

/* Variable Declarations */
extern struct SfDebugInstanceStruct *sfGlobalDebugInstanceStruct;

/* Variable Definitions */

/* Function Declarations */
extern const mxArray *sf_c30_LessonIII_get_eml_resolved_functions_info(void);

/* Function Definitions */
extern void sf_c30_LessonIII_get_check_sum(mxArray *plhs[]);
extern void c30_LessonIII_method_dispatcher(SimStruct *S, int_T method, void
  *data);

#endif

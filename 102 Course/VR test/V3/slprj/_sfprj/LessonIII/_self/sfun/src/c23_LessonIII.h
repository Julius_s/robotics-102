#ifndef __c23_LessonIII_h__
#define __c23_LessonIII_h__

/* Include files */
#include "sf_runtime/sfc_sf.h"
#include "sf_runtime/sfc_mex.h"
#include "rtwtypes.h"
#include "multiword_types.h"

/* Type Definitions */
#ifndef struct_Ball_tag
#define struct_Ball_tag

struct Ball_tag
{
  int8_T x;
  int8_T y;
  uint8_T valid;
};

#endif                                 /*struct_Ball_tag*/

#ifndef typedef_c23_Ball
#define typedef_c23_Ball

typedef struct Ball_tag c23_Ball;

#endif                                 /*typedef_c23_Ball*/

#ifndef struct_Player_tag
#define struct_Player_tag

struct Player_tag
{
  int8_T x;
  int8_T y;
  int16_T orientation;
  uint8_T color;
  uint8_T position;
  uint8_T valid;
};

#endif                                 /*struct_Player_tag*/

#ifndef typedef_c23_Player
#define typedef_c23_Player

typedef struct Player_tag c23_Player;

#endif                                 /*typedef_c23_Player*/

#include <stddef.h>
#ifndef typedef_SFc23_LessonIIIInstanceStruct
#define typedef_SFc23_LessonIIIInstanceStruct

typedef struct {
  SimStruct *S;
  ChartInfoStruct chartInfo;
  uint32_T chartNumber;
  uint32_T instanceNumber;
  int32_T c23_sfEvent;
  boolean_T c23_isStable;
  boolean_T c23_doneDoubleBufferReInit;
  uint8_T c23_is_active_c23_LessonIII;
  uint8_T (*c23_message)[31];
  c23_Ball *c23_b_Ball;
  c23_Player (*c23_players)[6];
  uint8_T *c23_gameOn;
} SFc23_LessonIIIInstanceStruct;

#endif                                 /*typedef_SFc23_LessonIIIInstanceStruct*/

/* Named Constants */

/* Variable Declarations */
extern struct SfDebugInstanceStruct *sfGlobalDebugInstanceStruct;

/* Variable Definitions */

/* Function Declarations */
extern const mxArray *sf_c23_LessonIII_get_eml_resolved_functions_info(void);

/* Function Definitions */
extern void sf_c23_LessonIII_get_check_sum(mxArray *plhs[]);
extern void c23_LessonIII_method_dispatcher(SimStruct *S, int_T method, void
  *data);

#endif

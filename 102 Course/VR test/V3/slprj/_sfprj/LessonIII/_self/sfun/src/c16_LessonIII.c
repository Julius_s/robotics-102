/* Include files */

#include <stddef.h>
#include "blas.h"
#include "LessonIII_sfun.h"
#include "c16_LessonIII.h"
#include "mwmathutil.h"
#define CHARTINSTANCE_CHARTNUMBER      (chartInstance->chartNumber)
#define CHARTINSTANCE_INSTANCENUMBER   (chartInstance->instanceNumber)
#include "LessonIII_sfun_debug_macros.h"
#define _SF_MEX_LISTEN_FOR_CTRL_C(S)   sf_mex_listen_for_ctrl_c(sfGlobalDebugInstanceStruct,S);

/* Type Definitions */

/* Named Constants */
#define CALL_EVENT                     (-1)

/* Variable Declarations */

/* Variable Definitions */
static real_T _sfTime_;
static const char * c16_debug_family_names[4] = { "nargin", "nargout", "data",
  "way" };

/* Function Declarations */
static void initialize_c16_LessonIII(SFc16_LessonIIIInstanceStruct
  *chartInstance);
static void initialize_params_c16_LessonIII(SFc16_LessonIIIInstanceStruct
  *chartInstance);
static void enable_c16_LessonIII(SFc16_LessonIIIInstanceStruct *chartInstance);
static void disable_c16_LessonIII(SFc16_LessonIIIInstanceStruct *chartInstance);
static void c16_update_debugger_state_c16_LessonIII
  (SFc16_LessonIIIInstanceStruct *chartInstance);
static const mxArray *get_sim_state_c16_LessonIII(SFc16_LessonIIIInstanceStruct *
  chartInstance);
static void set_sim_state_c16_LessonIII(SFc16_LessonIIIInstanceStruct
  *chartInstance, const mxArray *c16_st);
static void finalize_c16_LessonIII(SFc16_LessonIIIInstanceStruct *chartInstance);
static void sf_gateway_c16_LessonIII(SFc16_LessonIIIInstanceStruct
  *chartInstance);
static void mdl_start_c16_LessonIII(SFc16_LessonIIIInstanceStruct *chartInstance);
static void initSimStructsc16_LessonIII(SFc16_LessonIIIInstanceStruct
  *chartInstance);
static void init_script_number_translation(uint32_T c16_machineNumber, uint32_T
  c16_chartNumber, uint32_T c16_instanceNumber);
static const mxArray *c16_sf_marshallOut(void *chartInstanceVoid, void
  *c16_inData);
static c16_Waypoint c16_emlrt_marshallIn(SFc16_LessonIIIInstanceStruct
  *chartInstance, const mxArray *c16_b_way, const char_T *c16_identifier);
static c16_Waypoint c16_b_emlrt_marshallIn(SFc16_LessonIIIInstanceStruct
  *chartInstance, const mxArray *c16_u, const emlrtMsgIdentifier *c16_parentId);
static int8_T c16_c_emlrt_marshallIn(SFc16_LessonIIIInstanceStruct
  *chartInstance, const mxArray *c16_u, const emlrtMsgIdentifier *c16_parentId);
static int16_T c16_d_emlrt_marshallIn(SFc16_LessonIIIInstanceStruct
  *chartInstance, const mxArray *c16_u, const emlrtMsgIdentifier *c16_parentId);
static void c16_sf_marshallIn(void *chartInstanceVoid, const mxArray
  *c16_mxArrayInData, const char_T *c16_varName, void *c16_outData);
static const mxArray *c16_b_sf_marshallOut(void *chartInstanceVoid, void
  *c16_inData);
static const mxArray *c16_c_sf_marshallOut(void *chartInstanceVoid, void
  *c16_inData);
static real_T c16_e_emlrt_marshallIn(SFc16_LessonIIIInstanceStruct
  *chartInstance, const mxArray *c16_u, const emlrtMsgIdentifier *c16_parentId);
static void c16_b_sf_marshallIn(void *chartInstanceVoid, const mxArray
  *c16_mxArrayInData, const char_T *c16_varName, void *c16_outData);
static const mxArray *c16_d_sf_marshallOut(void *chartInstanceVoid, void
  *c16_inData);
static int32_T c16_f_emlrt_marshallIn(SFc16_LessonIIIInstanceStruct
  *chartInstance, const mxArray *c16_u, const emlrtMsgIdentifier *c16_parentId);
static void c16_c_sf_marshallIn(void *chartInstanceVoid, const mxArray
  *c16_mxArrayInData, const char_T *c16_varName, void *c16_outData);
static const mxArray *c16_way_bus_io(void *chartInstanceVoid, void *c16_pData);
static uint8_T c16_g_emlrt_marshallIn(SFc16_LessonIIIInstanceStruct
  *chartInstance, const mxArray *c16_b_is_active_c16_LessonIII, const char_T
  *c16_identifier);
static uint8_T c16_h_emlrt_marshallIn(SFc16_LessonIIIInstanceStruct
  *chartInstance, const mxArray *c16_u, const emlrtMsgIdentifier *c16_parentId);
static void init_dsm_address_info(SFc16_LessonIIIInstanceStruct *chartInstance);
static void init_simulink_io_address(SFc16_LessonIIIInstanceStruct
  *chartInstance);

/* Function Definitions */
static void initialize_c16_LessonIII(SFc16_LessonIIIInstanceStruct
  *chartInstance)
{
  chartInstance->c16_sfEvent = CALL_EVENT;
  _sfTime_ = sf_get_time(chartInstance->S);
  chartInstance->c16_is_active_c16_LessonIII = 0U;
}

static void initialize_params_c16_LessonIII(SFc16_LessonIIIInstanceStruct
  *chartInstance)
{
  (void)chartInstance;
}

static void enable_c16_LessonIII(SFc16_LessonIIIInstanceStruct *chartInstance)
{
  _sfTime_ = sf_get_time(chartInstance->S);
}

static void disable_c16_LessonIII(SFc16_LessonIIIInstanceStruct *chartInstance)
{
  _sfTime_ = sf_get_time(chartInstance->S);
}

static void c16_update_debugger_state_c16_LessonIII
  (SFc16_LessonIIIInstanceStruct *chartInstance)
{
  (void)chartInstance;
}

static const mxArray *get_sim_state_c16_LessonIII(SFc16_LessonIIIInstanceStruct *
  chartInstance)
{
  const mxArray *c16_st;
  const mxArray *c16_y = NULL;
  const mxArray *c16_b_y = NULL;
  int8_T c16_u;
  const mxArray *c16_c_y = NULL;
  int8_T c16_b_u;
  const mxArray *c16_d_y = NULL;
  int16_T c16_c_u;
  const mxArray *c16_e_y = NULL;
  uint8_T c16_hoistedGlobal;
  uint8_T c16_d_u;
  const mxArray *c16_f_y = NULL;
  c16_st = NULL;
  c16_st = NULL;
  c16_y = NULL;
  sf_mex_assign(&c16_y, sf_mex_createcellmatrix(2, 1), false);
  c16_b_y = NULL;
  sf_mex_assign(&c16_b_y, sf_mex_createstruct("structure", 2, 1, 1), false);
  c16_u = *(int8_T *)&((char_T *)chartInstance->c16_way)[0];
  c16_c_y = NULL;
  sf_mex_assign(&c16_c_y, sf_mex_create("y", &c16_u, 2, 0U, 0U, 0U, 0), false);
  sf_mex_addfield(c16_b_y, c16_c_y, "x", "x", 0);
  c16_b_u = *(int8_T *)&((char_T *)chartInstance->c16_way)[1];
  c16_d_y = NULL;
  sf_mex_assign(&c16_d_y, sf_mex_create("y", &c16_b_u, 2, 0U, 0U, 0U, 0), false);
  sf_mex_addfield(c16_b_y, c16_d_y, "y", "y", 0);
  c16_c_u = *(int16_T *)&((char_T *)chartInstance->c16_way)[2];
  c16_e_y = NULL;
  sf_mex_assign(&c16_e_y, sf_mex_create("y", &c16_c_u, 4, 0U, 0U, 0U, 0), false);
  sf_mex_addfield(c16_b_y, c16_e_y, "orientation", "orientation", 0);
  sf_mex_setcell(c16_y, 0, c16_b_y);
  c16_hoistedGlobal = chartInstance->c16_is_active_c16_LessonIII;
  c16_d_u = c16_hoistedGlobal;
  c16_f_y = NULL;
  sf_mex_assign(&c16_f_y, sf_mex_create("y", &c16_d_u, 3, 0U, 0U, 0U, 0), false);
  sf_mex_setcell(c16_y, 1, c16_f_y);
  sf_mex_assign(&c16_st, c16_y, false);
  return c16_st;
}

static void set_sim_state_c16_LessonIII(SFc16_LessonIIIInstanceStruct
  *chartInstance, const mxArray *c16_st)
{
  const mxArray *c16_u;
  c16_Waypoint c16_r0;
  chartInstance->c16_doneDoubleBufferReInit = true;
  c16_u = sf_mex_dup(c16_st);
  c16_r0 = c16_emlrt_marshallIn(chartInstance, sf_mex_dup(sf_mex_getcell(c16_u,
    0)), "way");
  *(int8_T *)&((char_T *)chartInstance->c16_way)[0] = c16_r0.x;
  *(int8_T *)&((char_T *)chartInstance->c16_way)[1] = c16_r0.y;
  *(int16_T *)&((char_T *)chartInstance->c16_way)[2] = c16_r0.orientation;
  chartInstance->c16_is_active_c16_LessonIII = c16_g_emlrt_marshallIn
    (chartInstance, sf_mex_dup(sf_mex_getcell(c16_u, 1)),
     "is_active_c16_LessonIII");
  sf_mex_destroy(&c16_u);
  c16_update_debugger_state_c16_LessonIII(chartInstance);
  sf_mex_destroy(&c16_st);
}

static void finalize_c16_LessonIII(SFc16_LessonIIIInstanceStruct *chartInstance)
{
  (void)chartInstance;
}

static void sf_gateway_c16_LessonIII(SFc16_LessonIIIInstanceStruct
  *chartInstance)
{
  int32_T c16_i0;
  int32_T c16_i1;
  real32_T c16_b_data[3];
  uint32_T c16_debug_family_var_map[4];
  real_T c16_nargin = 1.0;
  real_T c16_nargout = 1.0;
  c16_Waypoint c16_b_way;
  real32_T c16_f0;
  int8_T c16_i2;
  real32_T c16_f1;
  int8_T c16_i3;
  real32_T c16_f2;
  int16_T c16_i4;
  _SFD_SYMBOL_SCOPE_PUSH(0U, 0U);
  _sfTime_ = sf_get_time(chartInstance->S);
  _SFD_CC_CALL(CHART_ENTER_SFUNCTION_TAG, 11U, chartInstance->c16_sfEvent);
  for (c16_i0 = 0; c16_i0 < 3; c16_i0++) {
    _SFD_DATA_RANGE_CHECK((real_T)(*chartInstance->c16_data)[c16_i0], 0U);
  }

  chartInstance->c16_sfEvent = CALL_EVENT;
  _SFD_CC_CALL(CHART_ENTER_DURING_FUNCTION_TAG, 11U, chartInstance->c16_sfEvent);
  for (c16_i1 = 0; c16_i1 < 3; c16_i1++) {
    c16_b_data[c16_i1] = (*chartInstance->c16_data)[c16_i1];
  }

  _SFD_SYMBOL_SCOPE_PUSH_EML(0U, 4U, 4U, c16_debug_family_names,
    c16_debug_family_var_map);
  _SFD_SYMBOL_SCOPE_ADD_EML_IMPORTABLE(&c16_nargin, 0U, c16_c_sf_marshallOut,
    c16_b_sf_marshallIn);
  _SFD_SYMBOL_SCOPE_ADD_EML_IMPORTABLE(&c16_nargout, 1U, c16_c_sf_marshallOut,
    c16_b_sf_marshallIn);
  _SFD_SYMBOL_SCOPE_ADD_EML(c16_b_data, 2U, c16_b_sf_marshallOut);
  _SFD_SYMBOL_SCOPE_ADD_EML_IMPORTABLE(&c16_b_way, 3U, c16_sf_marshallOut,
    c16_sf_marshallIn);
  CV_EML_FCN(0, 0);
  _SFD_EML_CALL(0U, chartInstance->c16_sfEvent, 2);
  c16_f0 = muSingleScalarRound(c16_b_data[0]);
  if (c16_f0 < 128.0F) {
    if (CV_SATURATION_EVAL(4, 0, 0, 1, c16_f0 >= -128.0F)) {
      c16_i2 = (int8_T)c16_f0;
    } else {
      c16_i2 = MIN_int8_T;
    }
  } else if (CV_SATURATION_EVAL(4, 0, 0, 0, c16_f0 >= 128.0F)) {
    c16_i2 = MAX_int8_T;
  } else {
    c16_i2 = 0;
  }

  c16_b_way.x = c16_i2;
  _SFD_EML_CALL(0U, chartInstance->c16_sfEvent, 3);
  c16_f1 = muSingleScalarRound(c16_b_data[1]);
  if (c16_f1 < 128.0F) {
    if (CV_SATURATION_EVAL(4, 0, 1, 1, c16_f1 >= -128.0F)) {
      c16_i3 = (int8_T)c16_f1;
    } else {
      c16_i3 = MIN_int8_T;
    }
  } else if (CV_SATURATION_EVAL(4, 0, 1, 0, c16_f1 >= 128.0F)) {
    c16_i3 = MAX_int8_T;
  } else {
    c16_i3 = 0;
  }

  c16_b_way.y = c16_i3;
  _SFD_EML_CALL(0U, chartInstance->c16_sfEvent, 4);
  c16_f2 = muSingleScalarRound(c16_b_data[2]);
  if (c16_f2 < 32768.0F) {
    if (CV_SATURATION_EVAL(4, 0, 2, 1, c16_f2 >= -32768.0F)) {
      c16_i4 = (int16_T)c16_f2;
    } else {
      c16_i4 = MIN_int16_T;
    }
  } else if (CV_SATURATION_EVAL(4, 0, 2, 0, c16_f2 >= 32768.0F)) {
    c16_i4 = MAX_int16_T;
  } else {
    c16_i4 = 0;
  }

  c16_b_way.orientation = c16_i4;
  _SFD_EML_CALL(0U, chartInstance->c16_sfEvent, -4);
  _SFD_SYMBOL_SCOPE_POP();
  *(int8_T *)&((char_T *)chartInstance->c16_way)[0] = c16_b_way.x;
  *(int8_T *)&((char_T *)chartInstance->c16_way)[1] = c16_b_way.y;
  *(int16_T *)&((char_T *)chartInstance->c16_way)[2] = c16_b_way.orientation;
  _SFD_CC_CALL(EXIT_OUT_OF_FUNCTION_TAG, 11U, chartInstance->c16_sfEvent);
  _SFD_SYMBOL_SCOPE_POP();
  _SFD_CHECK_FOR_STATE_INCONSISTENCY(_LessonIIIMachineNumber_,
    chartInstance->chartNumber, chartInstance->instanceNumber);
}

static void mdl_start_c16_LessonIII(SFc16_LessonIIIInstanceStruct *chartInstance)
{
  (void)chartInstance;
}

static void initSimStructsc16_LessonIII(SFc16_LessonIIIInstanceStruct
  *chartInstance)
{
  (void)chartInstance;
}

static void init_script_number_translation(uint32_T c16_machineNumber, uint32_T
  c16_chartNumber, uint32_T c16_instanceNumber)
{
  (void)c16_machineNumber;
  (void)c16_chartNumber;
  (void)c16_instanceNumber;
}

static const mxArray *c16_sf_marshallOut(void *chartInstanceVoid, void
  *c16_inData)
{
  const mxArray *c16_mxArrayOutData = NULL;
  c16_Waypoint c16_u;
  const mxArray *c16_y = NULL;
  int8_T c16_b_u;
  const mxArray *c16_b_y = NULL;
  int8_T c16_c_u;
  const mxArray *c16_c_y = NULL;
  int16_T c16_d_u;
  const mxArray *c16_d_y = NULL;
  SFc16_LessonIIIInstanceStruct *chartInstance;
  chartInstance = (SFc16_LessonIIIInstanceStruct *)chartInstanceVoid;
  c16_mxArrayOutData = NULL;
  c16_u = *(c16_Waypoint *)c16_inData;
  c16_y = NULL;
  sf_mex_assign(&c16_y, sf_mex_createstruct("structure", 2, 1, 1), false);
  c16_b_u = c16_u.x;
  c16_b_y = NULL;
  sf_mex_assign(&c16_b_y, sf_mex_create("y", &c16_b_u, 2, 0U, 0U, 0U, 0), false);
  sf_mex_addfield(c16_y, c16_b_y, "x", "x", 0);
  c16_c_u = c16_u.y;
  c16_c_y = NULL;
  sf_mex_assign(&c16_c_y, sf_mex_create("y", &c16_c_u, 2, 0U, 0U, 0U, 0), false);
  sf_mex_addfield(c16_y, c16_c_y, "y", "y", 0);
  c16_d_u = c16_u.orientation;
  c16_d_y = NULL;
  sf_mex_assign(&c16_d_y, sf_mex_create("y", &c16_d_u, 4, 0U, 0U, 0U, 0), false);
  sf_mex_addfield(c16_y, c16_d_y, "orientation", "orientation", 0);
  sf_mex_assign(&c16_mxArrayOutData, c16_y, false);
  return c16_mxArrayOutData;
}

static c16_Waypoint c16_emlrt_marshallIn(SFc16_LessonIIIInstanceStruct
  *chartInstance, const mxArray *c16_b_way, const char_T *c16_identifier)
{
  c16_Waypoint c16_y;
  emlrtMsgIdentifier c16_thisId;
  c16_thisId.fIdentifier = c16_identifier;
  c16_thisId.fParent = NULL;
  c16_y = c16_b_emlrt_marshallIn(chartInstance, sf_mex_dup(c16_b_way),
    &c16_thisId);
  sf_mex_destroy(&c16_b_way);
  return c16_y;
}

static c16_Waypoint c16_b_emlrt_marshallIn(SFc16_LessonIIIInstanceStruct
  *chartInstance, const mxArray *c16_u, const emlrtMsgIdentifier *c16_parentId)
{
  c16_Waypoint c16_y;
  emlrtMsgIdentifier c16_thisId;
  static const char * c16_fieldNames[3] = { "x", "y", "orientation" };

  c16_thisId.fParent = c16_parentId;
  sf_mex_check_struct(c16_parentId, c16_u, 3, c16_fieldNames, 0U, NULL);
  c16_thisId.fIdentifier = "x";
  c16_y.x = c16_c_emlrt_marshallIn(chartInstance, sf_mex_dup(sf_mex_getfield
    (c16_u, "x", "x", 0)), &c16_thisId);
  c16_thisId.fIdentifier = "y";
  c16_y.y = c16_c_emlrt_marshallIn(chartInstance, sf_mex_dup(sf_mex_getfield
    (c16_u, "y", "y", 0)), &c16_thisId);
  c16_thisId.fIdentifier = "orientation";
  c16_y.orientation = c16_d_emlrt_marshallIn(chartInstance, sf_mex_dup
    (sf_mex_getfield(c16_u, "orientation", "orientation", 0)), &c16_thisId);
  sf_mex_destroy(&c16_u);
  return c16_y;
}

static int8_T c16_c_emlrt_marshallIn(SFc16_LessonIIIInstanceStruct
  *chartInstance, const mxArray *c16_u, const emlrtMsgIdentifier *c16_parentId)
{
  int8_T c16_y;
  int8_T c16_i5;
  (void)chartInstance;
  sf_mex_import(c16_parentId, sf_mex_dup(c16_u), &c16_i5, 1, 2, 0U, 0, 0U, 0);
  c16_y = c16_i5;
  sf_mex_destroy(&c16_u);
  return c16_y;
}

static int16_T c16_d_emlrt_marshallIn(SFc16_LessonIIIInstanceStruct
  *chartInstance, const mxArray *c16_u, const emlrtMsgIdentifier *c16_parentId)
{
  int16_T c16_y;
  int16_T c16_i6;
  (void)chartInstance;
  sf_mex_import(c16_parentId, sf_mex_dup(c16_u), &c16_i6, 1, 4, 0U, 0, 0U, 0);
  c16_y = c16_i6;
  sf_mex_destroy(&c16_u);
  return c16_y;
}

static void c16_sf_marshallIn(void *chartInstanceVoid, const mxArray
  *c16_mxArrayInData, const char_T *c16_varName, void *c16_outData)
{
  const mxArray *c16_b_way;
  const char_T *c16_identifier;
  emlrtMsgIdentifier c16_thisId;
  c16_Waypoint c16_y;
  SFc16_LessonIIIInstanceStruct *chartInstance;
  chartInstance = (SFc16_LessonIIIInstanceStruct *)chartInstanceVoid;
  c16_b_way = sf_mex_dup(c16_mxArrayInData);
  c16_identifier = c16_varName;
  c16_thisId.fIdentifier = c16_identifier;
  c16_thisId.fParent = NULL;
  c16_y = c16_b_emlrt_marshallIn(chartInstance, sf_mex_dup(c16_b_way),
    &c16_thisId);
  sf_mex_destroy(&c16_b_way);
  *(c16_Waypoint *)c16_outData = c16_y;
  sf_mex_destroy(&c16_mxArrayInData);
}

static const mxArray *c16_b_sf_marshallOut(void *chartInstanceVoid, void
  *c16_inData)
{
  const mxArray *c16_mxArrayOutData = NULL;
  int32_T c16_i7;
  real32_T c16_b_inData[3];
  int32_T c16_i8;
  real32_T c16_u[3];
  const mxArray *c16_y = NULL;
  SFc16_LessonIIIInstanceStruct *chartInstance;
  chartInstance = (SFc16_LessonIIIInstanceStruct *)chartInstanceVoid;
  c16_mxArrayOutData = NULL;
  for (c16_i7 = 0; c16_i7 < 3; c16_i7++) {
    c16_b_inData[c16_i7] = (*(real32_T (*)[3])c16_inData)[c16_i7];
  }

  for (c16_i8 = 0; c16_i8 < 3; c16_i8++) {
    c16_u[c16_i8] = c16_b_inData[c16_i8];
  }

  c16_y = NULL;
  sf_mex_assign(&c16_y, sf_mex_create("y", c16_u, 1, 0U, 1U, 0U, 1, 3), false);
  sf_mex_assign(&c16_mxArrayOutData, c16_y, false);
  return c16_mxArrayOutData;
}

static const mxArray *c16_c_sf_marshallOut(void *chartInstanceVoid, void
  *c16_inData)
{
  const mxArray *c16_mxArrayOutData = NULL;
  real_T c16_u;
  const mxArray *c16_y = NULL;
  SFc16_LessonIIIInstanceStruct *chartInstance;
  chartInstance = (SFc16_LessonIIIInstanceStruct *)chartInstanceVoid;
  c16_mxArrayOutData = NULL;
  c16_u = *(real_T *)c16_inData;
  c16_y = NULL;
  sf_mex_assign(&c16_y, sf_mex_create("y", &c16_u, 0, 0U, 0U, 0U, 0), false);
  sf_mex_assign(&c16_mxArrayOutData, c16_y, false);
  return c16_mxArrayOutData;
}

static real_T c16_e_emlrt_marshallIn(SFc16_LessonIIIInstanceStruct
  *chartInstance, const mxArray *c16_u, const emlrtMsgIdentifier *c16_parentId)
{
  real_T c16_y;
  real_T c16_d0;
  (void)chartInstance;
  sf_mex_import(c16_parentId, sf_mex_dup(c16_u), &c16_d0, 1, 0, 0U, 0, 0U, 0);
  c16_y = c16_d0;
  sf_mex_destroy(&c16_u);
  return c16_y;
}

static void c16_b_sf_marshallIn(void *chartInstanceVoid, const mxArray
  *c16_mxArrayInData, const char_T *c16_varName, void *c16_outData)
{
  const mxArray *c16_nargout;
  const char_T *c16_identifier;
  emlrtMsgIdentifier c16_thisId;
  real_T c16_y;
  SFc16_LessonIIIInstanceStruct *chartInstance;
  chartInstance = (SFc16_LessonIIIInstanceStruct *)chartInstanceVoid;
  c16_nargout = sf_mex_dup(c16_mxArrayInData);
  c16_identifier = c16_varName;
  c16_thisId.fIdentifier = c16_identifier;
  c16_thisId.fParent = NULL;
  c16_y = c16_e_emlrt_marshallIn(chartInstance, sf_mex_dup(c16_nargout),
    &c16_thisId);
  sf_mex_destroy(&c16_nargout);
  *(real_T *)c16_outData = c16_y;
  sf_mex_destroy(&c16_mxArrayInData);
}

const mxArray *sf_c16_LessonIII_get_eml_resolved_functions_info(void)
{
  const mxArray *c16_nameCaptureInfo = NULL;
  c16_nameCaptureInfo = NULL;
  sf_mex_assign(&c16_nameCaptureInfo, sf_mex_create("nameCaptureInfo", NULL, 0,
    0U, 1U, 0U, 2, 0, 1), false);
  return c16_nameCaptureInfo;
}

static const mxArray *c16_d_sf_marshallOut(void *chartInstanceVoid, void
  *c16_inData)
{
  const mxArray *c16_mxArrayOutData = NULL;
  int32_T c16_u;
  const mxArray *c16_y = NULL;
  SFc16_LessonIIIInstanceStruct *chartInstance;
  chartInstance = (SFc16_LessonIIIInstanceStruct *)chartInstanceVoid;
  c16_mxArrayOutData = NULL;
  c16_u = *(int32_T *)c16_inData;
  c16_y = NULL;
  sf_mex_assign(&c16_y, sf_mex_create("y", &c16_u, 6, 0U, 0U, 0U, 0), false);
  sf_mex_assign(&c16_mxArrayOutData, c16_y, false);
  return c16_mxArrayOutData;
}

static int32_T c16_f_emlrt_marshallIn(SFc16_LessonIIIInstanceStruct
  *chartInstance, const mxArray *c16_u, const emlrtMsgIdentifier *c16_parentId)
{
  int32_T c16_y;
  int32_T c16_i9;
  (void)chartInstance;
  sf_mex_import(c16_parentId, sf_mex_dup(c16_u), &c16_i9, 1, 6, 0U, 0, 0U, 0);
  c16_y = c16_i9;
  sf_mex_destroy(&c16_u);
  return c16_y;
}

static void c16_c_sf_marshallIn(void *chartInstanceVoid, const mxArray
  *c16_mxArrayInData, const char_T *c16_varName, void *c16_outData)
{
  const mxArray *c16_b_sfEvent;
  const char_T *c16_identifier;
  emlrtMsgIdentifier c16_thisId;
  int32_T c16_y;
  SFc16_LessonIIIInstanceStruct *chartInstance;
  chartInstance = (SFc16_LessonIIIInstanceStruct *)chartInstanceVoid;
  c16_b_sfEvent = sf_mex_dup(c16_mxArrayInData);
  c16_identifier = c16_varName;
  c16_thisId.fIdentifier = c16_identifier;
  c16_thisId.fParent = NULL;
  c16_y = c16_f_emlrt_marshallIn(chartInstance, sf_mex_dup(c16_b_sfEvent),
    &c16_thisId);
  sf_mex_destroy(&c16_b_sfEvent);
  *(int32_T *)c16_outData = c16_y;
  sf_mex_destroy(&c16_mxArrayInData);
}

static const mxArray *c16_way_bus_io(void *chartInstanceVoid, void *c16_pData)
{
  const mxArray *c16_mxVal = NULL;
  c16_Waypoint c16_tmp;
  SFc16_LessonIIIInstanceStruct *chartInstance;
  chartInstance = (SFc16_LessonIIIInstanceStruct *)chartInstanceVoid;
  c16_mxVal = NULL;
  c16_tmp.x = *(int8_T *)&((char_T *)(c16_Waypoint *)c16_pData)[0];
  c16_tmp.y = *(int8_T *)&((char_T *)(c16_Waypoint *)c16_pData)[1];
  c16_tmp.orientation = *(int16_T *)&((char_T *)(c16_Waypoint *)c16_pData)[2];
  sf_mex_assign(&c16_mxVal, c16_sf_marshallOut(chartInstance, &c16_tmp), false);
  return c16_mxVal;
}

static uint8_T c16_g_emlrt_marshallIn(SFc16_LessonIIIInstanceStruct
  *chartInstance, const mxArray *c16_b_is_active_c16_LessonIII, const char_T
  *c16_identifier)
{
  uint8_T c16_y;
  emlrtMsgIdentifier c16_thisId;
  c16_thisId.fIdentifier = c16_identifier;
  c16_thisId.fParent = NULL;
  c16_y = c16_h_emlrt_marshallIn(chartInstance, sf_mex_dup
    (c16_b_is_active_c16_LessonIII), &c16_thisId);
  sf_mex_destroy(&c16_b_is_active_c16_LessonIII);
  return c16_y;
}

static uint8_T c16_h_emlrt_marshallIn(SFc16_LessonIIIInstanceStruct
  *chartInstance, const mxArray *c16_u, const emlrtMsgIdentifier *c16_parentId)
{
  uint8_T c16_y;
  uint8_T c16_u0;
  (void)chartInstance;
  sf_mex_import(c16_parentId, sf_mex_dup(c16_u), &c16_u0, 1, 3, 0U, 0, 0U, 0);
  c16_y = c16_u0;
  sf_mex_destroy(&c16_u);
  return c16_y;
}

static void init_dsm_address_info(SFc16_LessonIIIInstanceStruct *chartInstance)
{
  (void)chartInstance;
}

static void init_simulink_io_address(SFc16_LessonIIIInstanceStruct
  *chartInstance)
{
  chartInstance->c16_data = (real32_T (*)[3])ssGetInputPortSignal_wrapper
    (chartInstance->S, 0);
  chartInstance->c16_way = (c16_Waypoint *)ssGetOutputPortSignal_wrapper
    (chartInstance->S, 1);
}

/* SFunction Glue Code */
#ifdef utFree
#undef utFree
#endif

#ifdef utMalloc
#undef utMalloc
#endif

#ifdef __cplusplus

extern "C" void *utMalloc(size_t size);
extern "C" void utFree(void*);

#else

extern void *utMalloc(size_t size);
extern void utFree(void*);

#endif

void sf_c16_LessonIII_get_check_sum(mxArray *plhs[])
{
  ((real_T *)mxGetPr((plhs[0])))[0] = (real_T)(499780559U);
  ((real_T *)mxGetPr((plhs[0])))[1] = (real_T)(4066560223U);
  ((real_T *)mxGetPr((plhs[0])))[2] = (real_T)(4138772518U);
  ((real_T *)mxGetPr((plhs[0])))[3] = (real_T)(3318275091U);
}

mxArray* sf_c16_LessonIII_get_post_codegen_info(void);
mxArray *sf_c16_LessonIII_get_autoinheritance_info(void)
{
  const char *autoinheritanceFields[] = { "checksum", "inputs", "parameters",
    "outputs", "locals", "postCodegenInfo" };

  mxArray *mxAutoinheritanceInfo = mxCreateStructMatrix(1, 1, sizeof
    (autoinheritanceFields)/sizeof(autoinheritanceFields[0]),
    autoinheritanceFields);

  {
    mxArray *mxChecksum = mxCreateString("avMqIanoQY6XoECCReQu1D");
    mxSetField(mxAutoinheritanceInfo,0,"checksum",mxChecksum);
  }

  {
    const char *dataFields[] = { "size", "type", "complexity" };

    mxArray *mxData = mxCreateStructMatrix(1,1,3,dataFields);

    {
      mxArray *mxSize = mxCreateDoubleMatrix(1,2,mxREAL);
      double *pr = mxGetPr(mxSize);
      pr[0] = (double)(3);
      pr[1] = (double)(1);
      mxSetField(mxData,0,"size",mxSize);
    }

    {
      const char *typeFields[] = { "base", "fixpt" };

      mxArray *mxType = mxCreateStructMatrix(1,1,2,typeFields);
      mxSetField(mxType,0,"base",mxCreateDoubleScalar(9));
      mxSetField(mxType,0,"fixpt",mxCreateDoubleMatrix(0,0,mxREAL));
      mxSetField(mxData,0,"type",mxType);
    }

    mxSetField(mxData,0,"complexity",mxCreateDoubleScalar(0));
    mxSetField(mxAutoinheritanceInfo,0,"inputs",mxData);
  }

  {
    mxSetField(mxAutoinheritanceInfo,0,"parameters",mxCreateDoubleMatrix(0,0,
                mxREAL));
  }

  {
    const char *dataFields[] = { "size", "type", "complexity" };

    mxArray *mxData = mxCreateStructMatrix(1,1,3,dataFields);

    {
      mxArray *mxSize = mxCreateDoubleMatrix(1,2,mxREAL);
      double *pr = mxGetPr(mxSize);
      pr[0] = (double)(1);
      pr[1] = (double)(1);
      mxSetField(mxData,0,"size",mxSize);
    }

    {
      const char *typeFields[] = { "base", "fixpt" };

      mxArray *mxType = mxCreateStructMatrix(1,1,2,typeFields);
      mxSetField(mxType,0,"base",mxCreateDoubleScalar(13));
      mxSetField(mxType,0,"fixpt",mxCreateDoubleMatrix(0,0,mxREAL));
      mxSetField(mxData,0,"type",mxType);
    }

    mxSetField(mxData,0,"complexity",mxCreateDoubleScalar(0));
    mxSetField(mxAutoinheritanceInfo,0,"outputs",mxData);
  }

  {
    mxSetField(mxAutoinheritanceInfo,0,"locals",mxCreateDoubleMatrix(0,0,mxREAL));
  }

  {
    mxArray* mxPostCodegenInfo = sf_c16_LessonIII_get_post_codegen_info();
    mxSetField(mxAutoinheritanceInfo,0,"postCodegenInfo",mxPostCodegenInfo);
  }

  return(mxAutoinheritanceInfo);
}

mxArray *sf_c16_LessonIII_third_party_uses_info(void)
{
  mxArray * mxcell3p = mxCreateCellMatrix(1,0);
  return(mxcell3p);
}

mxArray *sf_c16_LessonIII_jit_fallback_info(void)
{
  const char *infoFields[] = { "fallbackType", "fallbackReason",
    "incompatibleSymbol", };

  mxArray *mxInfo = mxCreateStructMatrix(1, 1, 3, infoFields);
  mxArray *fallbackReason = mxCreateString("feature_off");
  mxArray *incompatibleSymbol = mxCreateString("");
  mxArray *fallbackType = mxCreateString("early");
  mxSetField(mxInfo, 0, infoFields[0], fallbackType);
  mxSetField(mxInfo, 0, infoFields[1], fallbackReason);
  mxSetField(mxInfo, 0, infoFields[2], incompatibleSymbol);
  return mxInfo;
}

mxArray *sf_c16_LessonIII_updateBuildInfo_args_info(void)
{
  mxArray *mxBIArgs = mxCreateCellMatrix(1,0);
  return mxBIArgs;
}

mxArray* sf_c16_LessonIII_get_post_codegen_info(void)
{
  const char* fieldNames[] = { "exportedFunctionsUsedByThisChart",
    "exportedFunctionsChecksum" };

  mwSize dims[2] = { 1, 1 };

  mxArray* mxPostCodegenInfo = mxCreateStructArray(2, dims, sizeof(fieldNames)/
    sizeof(fieldNames[0]), fieldNames);

  {
    mxArray* mxExportedFunctionsChecksum = mxCreateString("");
    mwSize exp_dims[2] = { 0, 1 };

    mxArray* mxExportedFunctionsUsedByThisChart = mxCreateCellArray(2, exp_dims);
    mxSetField(mxPostCodegenInfo, 0, "exportedFunctionsUsedByThisChart",
               mxExportedFunctionsUsedByThisChart);
    mxSetField(mxPostCodegenInfo, 0, "exportedFunctionsChecksum",
               mxExportedFunctionsChecksum);
  }

  return mxPostCodegenInfo;
}

static const mxArray *sf_get_sim_state_info_c16_LessonIII(void)
{
  const char *infoFields[] = { "chartChecksum", "varInfo" };

  mxArray *mxInfo = mxCreateStructMatrix(1, 1, 2, infoFields);
  const char *infoEncStr[] = {
    "100 S1x2'type','srcId','name','auxInfo'{{M[1],M[5],T\"way\",},{M[8],M[0],T\"is_active_c16_LessonIII\",}}"
  };

  mxArray *mxVarInfo = sf_mex_decode_encoded_mx_struct_array(infoEncStr, 2, 10);
  mxArray *mxChecksum = mxCreateDoubleMatrix(1, 4, mxREAL);
  sf_c16_LessonIII_get_check_sum(&mxChecksum);
  mxSetField(mxInfo, 0, infoFields[0], mxChecksum);
  mxSetField(mxInfo, 0, infoFields[1], mxVarInfo);
  return mxInfo;
}

static void chart_debug_initialization(SimStruct *S, unsigned int
  fullDebuggerInitialization)
{
  if (!sim_mode_is_rtw_gen(S)) {
    SFc16_LessonIIIInstanceStruct *chartInstance;
    ChartRunTimeInfo * crtInfo = (ChartRunTimeInfo *)(ssGetUserData(S));
    ChartInfoStruct * chartInfo = (ChartInfoStruct *)(crtInfo->instanceInfo);
    chartInstance = (SFc16_LessonIIIInstanceStruct *) chartInfo->chartInstance;
    if (ssIsFirstInitCond(S) && fullDebuggerInitialization==1) {
      /* do this only if simulation is starting */
      {
        unsigned int chartAlreadyPresent;
        chartAlreadyPresent = sf_debug_initialize_chart
          (sfGlobalDebugInstanceStruct,
           _LessonIIIMachineNumber_,
           16,
           1,
           1,
           0,
           2,
           0,
           0,
           0,
           0,
           0,
           &(chartInstance->chartNumber),
           &(chartInstance->instanceNumber),
           (void *)S);

        /* Each instance must initialize its own list of scripts */
        init_script_number_translation(_LessonIIIMachineNumber_,
          chartInstance->chartNumber,chartInstance->instanceNumber);
        if (chartAlreadyPresent==0) {
          /* this is the first instance */
          sf_debug_set_chart_disable_implicit_casting
            (sfGlobalDebugInstanceStruct,_LessonIIIMachineNumber_,
             chartInstance->chartNumber,1);
          sf_debug_set_chart_event_thresholds(sfGlobalDebugInstanceStruct,
            _LessonIIIMachineNumber_,
            chartInstance->chartNumber,
            0,
            0,
            0);
          _SFD_SET_DATA_PROPS(0,1,1,0,"data");
          _SFD_SET_DATA_PROPS(1,2,0,1,"way");
          _SFD_STATE_INFO(0,0,2);
          _SFD_CH_SUBSTATE_COUNT(0);
          _SFD_CH_SUBSTATE_DECOMP(0);
        }

        _SFD_CV_INIT_CHART(0,0,0,0);

        {
          _SFD_CV_INIT_STATE(0,0,0,0,0,0,NULL,NULL);
        }

        _SFD_CV_INIT_TRANS(0,0,NULL,NULL,0,NULL);

        /* Initialization of MATLAB Function Model Coverage */
        _SFD_CV_INIT_EML(0,1,1,0,0,3,0,0,0,0,0);
        _SFD_CV_INIT_EML_FCN(0,0,"eML_blk_kernel",0,-1,104);
        _SFD_CV_INIT_EML_SATURATION(0,1,0,35,-1,48);
        _SFD_CV_INIT_EML_SATURATION(0,1,1,56,-1,69);
        _SFD_CV_INIT_EML_SATURATION(0,1,2,87,-1,101);

        {
          unsigned int dimVector[1];
          dimVector[0]= 3;
          _SFD_SET_DATA_COMPILED_PROPS(0,SF_SINGLE,1,&(dimVector[0]),0,0,0,0.0,
            1.0,0,0,(MexFcnForType)c16_b_sf_marshallOut,(MexInFcnForType)NULL);
        }

        _SFD_SET_DATA_COMPILED_PROPS(1,SF_STRUCT,0,NULL,0,0,0,0.0,1.0,0,0,
          (MexFcnForType)c16_way_bus_io,(MexInFcnForType)NULL);
        _SFD_SET_DATA_VALUE_PTR(0U, *chartInstance->c16_data);
        _SFD_SET_DATA_VALUE_PTR(1U, chartInstance->c16_way);
      }
    } else {
      sf_debug_reset_current_state_configuration(sfGlobalDebugInstanceStruct,
        _LessonIIIMachineNumber_,chartInstance->chartNumber,
        chartInstance->instanceNumber);
    }
  }
}

static const char* sf_get_instance_specialization(void)
{
  return "ynY5ttWD0J2n5VW3gZiZPC";
}

static void sf_opaque_initialize_c16_LessonIII(void *chartInstanceVar)
{
  chart_debug_initialization(((SFc16_LessonIIIInstanceStruct*) chartInstanceVar
    )->S,0);
  initialize_params_c16_LessonIII((SFc16_LessonIIIInstanceStruct*)
    chartInstanceVar);
  initialize_c16_LessonIII((SFc16_LessonIIIInstanceStruct*) chartInstanceVar);
}

static void sf_opaque_enable_c16_LessonIII(void *chartInstanceVar)
{
  enable_c16_LessonIII((SFc16_LessonIIIInstanceStruct*) chartInstanceVar);
}

static void sf_opaque_disable_c16_LessonIII(void *chartInstanceVar)
{
  disable_c16_LessonIII((SFc16_LessonIIIInstanceStruct*) chartInstanceVar);
}

static void sf_opaque_gateway_c16_LessonIII(void *chartInstanceVar)
{
  sf_gateway_c16_LessonIII((SFc16_LessonIIIInstanceStruct*) chartInstanceVar);
}

static const mxArray* sf_opaque_get_sim_state_c16_LessonIII(SimStruct* S)
{
  ChartRunTimeInfo * crtInfo = (ChartRunTimeInfo *)(ssGetUserData(S));
  ChartInfoStruct * chartInfo = (ChartInfoStruct *)(crtInfo->instanceInfo);
  return get_sim_state_c16_LessonIII((SFc16_LessonIIIInstanceStruct*)
    chartInfo->chartInstance);         /* raw sim ctx */
}

static void sf_opaque_set_sim_state_c16_LessonIII(SimStruct* S, const mxArray
  *st)
{
  ChartRunTimeInfo * crtInfo = (ChartRunTimeInfo *)(ssGetUserData(S));
  ChartInfoStruct * chartInfo = (ChartInfoStruct *)(crtInfo->instanceInfo);
  set_sim_state_c16_LessonIII((SFc16_LessonIIIInstanceStruct*)
    chartInfo->chartInstance, st);
}

static void sf_opaque_terminate_c16_LessonIII(void *chartInstanceVar)
{
  if (chartInstanceVar!=NULL) {
    SimStruct *S = ((SFc16_LessonIIIInstanceStruct*) chartInstanceVar)->S;
    ChartRunTimeInfo * crtInfo = (ChartRunTimeInfo *)(ssGetUserData(S));
    if (sim_mode_is_rtw_gen(S) || sim_mode_is_external(S)) {
      sf_clear_rtw_identifier(S);
      unload_LessonIII_optimization_info();
    }

    finalize_c16_LessonIII((SFc16_LessonIIIInstanceStruct*) chartInstanceVar);
    utFree(chartInstanceVar);
    if (crtInfo != NULL) {
      utFree(crtInfo);
    }

    ssSetUserData(S,NULL);
  }
}

static void sf_opaque_init_subchart_simstructs(void *chartInstanceVar)
{
  initSimStructsc16_LessonIII((SFc16_LessonIIIInstanceStruct*) chartInstanceVar);
}

extern unsigned int sf_machine_global_initializer_called(void);
static void mdlProcessParameters_c16_LessonIII(SimStruct *S)
{
  int i;
  for (i=0;i<ssGetNumRunTimeParams(S);i++) {
    if (ssGetSFcnParamTunable(S,i)) {
      ssUpdateDlgParamAsRunTimeParam(S,i);
    }
  }

  if (sf_machine_global_initializer_called()) {
    ChartRunTimeInfo * crtInfo = (ChartRunTimeInfo *)(ssGetUserData(S));
    ChartInfoStruct * chartInfo = (ChartInfoStruct *)(crtInfo->instanceInfo);
    initialize_params_c16_LessonIII((SFc16_LessonIIIInstanceStruct*)
      (chartInfo->chartInstance));
  }
}

static void mdlSetWorkWidths_c16_LessonIII(SimStruct *S)
{
  if (sim_mode_is_rtw_gen(S) || sim_mode_is_external(S)) {
    mxArray *infoStruct = load_LessonIII_optimization_info();
    int_T chartIsInlinable =
      (int_T)sf_is_chart_inlinable(sf_get_instance_specialization(),infoStruct,
      16);
    ssSetStateflowIsInlinable(S,chartIsInlinable);
    ssSetRTWCG(S,sf_rtw_info_uint_prop(sf_get_instance_specialization(),
                infoStruct,16,"RTWCG"));
    ssSetEnableFcnIsTrivial(S,1);
    ssSetDisableFcnIsTrivial(S,1);
    ssSetNotMultipleInlinable(S,sf_rtw_info_uint_prop
      (sf_get_instance_specialization(),infoStruct,16,
       "gatewayCannotBeInlinedMultipleTimes"));
    sf_update_buildInfo(sf_get_instance_specialization(),infoStruct,16);
    if (chartIsInlinable) {
      ssSetInputPortOptimOpts(S, 0, SS_REUSABLE_AND_LOCAL);
      sf_mark_chart_expressionable_inputs(S,sf_get_instance_specialization(),
        infoStruct,16,1);
      sf_mark_chart_reusable_outputs(S,sf_get_instance_specialization(),
        infoStruct,16,1);
    }

    {
      unsigned int outPortIdx;
      for (outPortIdx=1; outPortIdx<=1; ++outPortIdx) {
        ssSetOutputPortOptimizeInIR(S, outPortIdx, 1U);
      }
    }

    {
      unsigned int inPortIdx;
      for (inPortIdx=0; inPortIdx < 1; ++inPortIdx) {
        ssSetInputPortOptimizeInIR(S, inPortIdx, 1U);
      }
    }

    sf_set_rtw_dwork_info(S,sf_get_instance_specialization(),infoStruct,16);
    ssSetHasSubFunctions(S,!(chartIsInlinable));
  } else {
  }

  ssSetOptions(S,ssGetOptions(S)|SS_OPTION_WORKS_WITH_CODE_REUSE);
  ssSetChecksum0(S,(1329588326U));
  ssSetChecksum1(S,(1955521559U));
  ssSetChecksum2(S,(80393245U));
  ssSetChecksum3(S,(521053753U));
  ssSetmdlDerivatives(S, NULL);
  ssSetExplicitFCSSCtrl(S,1);
  ssSupportsMultipleExecInstances(S,1);
}

static void mdlRTW_c16_LessonIII(SimStruct *S)
{
  if (sim_mode_is_rtw_gen(S)) {
    ssWriteRTWStrParam(S, "StateflowChartType", "Embedded MATLAB");
  }
}

static void mdlStart_c16_LessonIII(SimStruct *S)
{
  SFc16_LessonIIIInstanceStruct *chartInstance;
  ChartRunTimeInfo * crtInfo = (ChartRunTimeInfo *)utMalloc(sizeof
    (ChartRunTimeInfo));
  chartInstance = (SFc16_LessonIIIInstanceStruct *)utMalloc(sizeof
    (SFc16_LessonIIIInstanceStruct));
  memset(chartInstance, 0, sizeof(SFc16_LessonIIIInstanceStruct));
  if (chartInstance==NULL) {
    sf_mex_error_message("Could not allocate memory for chart instance.");
  }

  chartInstance->chartInfo.chartInstance = chartInstance;
  chartInstance->chartInfo.isEMLChart = 1;
  chartInstance->chartInfo.chartInitialized = 0;
  chartInstance->chartInfo.sFunctionGateway = sf_opaque_gateway_c16_LessonIII;
  chartInstance->chartInfo.initializeChart = sf_opaque_initialize_c16_LessonIII;
  chartInstance->chartInfo.terminateChart = sf_opaque_terminate_c16_LessonIII;
  chartInstance->chartInfo.enableChart = sf_opaque_enable_c16_LessonIII;
  chartInstance->chartInfo.disableChart = sf_opaque_disable_c16_LessonIII;
  chartInstance->chartInfo.getSimState = sf_opaque_get_sim_state_c16_LessonIII;
  chartInstance->chartInfo.setSimState = sf_opaque_set_sim_state_c16_LessonIII;
  chartInstance->chartInfo.getSimStateInfo = sf_get_sim_state_info_c16_LessonIII;
  chartInstance->chartInfo.zeroCrossings = NULL;
  chartInstance->chartInfo.outputs = NULL;
  chartInstance->chartInfo.derivatives = NULL;
  chartInstance->chartInfo.mdlRTW = mdlRTW_c16_LessonIII;
  chartInstance->chartInfo.mdlStart = mdlStart_c16_LessonIII;
  chartInstance->chartInfo.mdlSetWorkWidths = mdlSetWorkWidths_c16_LessonIII;
  chartInstance->chartInfo.extModeExec = NULL;
  chartInstance->chartInfo.restoreLastMajorStepConfiguration = NULL;
  chartInstance->chartInfo.restoreBeforeLastMajorStepConfiguration = NULL;
  chartInstance->chartInfo.storeCurrentConfiguration = NULL;
  chartInstance->chartInfo.callAtomicSubchartUserFcn = NULL;
  chartInstance->chartInfo.callAtomicSubchartAutoFcn = NULL;
  chartInstance->chartInfo.debugInstance = sfGlobalDebugInstanceStruct;
  chartInstance->S = S;
  crtInfo->checksum = SF_RUNTIME_INFO_CHECKSUM;
  crtInfo->instanceInfo = (&(chartInstance->chartInfo));
  crtInfo->isJITEnabled = false;
  crtInfo->compiledInfo = NULL;
  ssSetUserData(S,(void *)(crtInfo));  /* register the chart instance with simstruct */
  init_dsm_address_info(chartInstance);
  init_simulink_io_address(chartInstance);
  if (!sim_mode_is_rtw_gen(S)) {
  }

  sf_opaque_init_subchart_simstructs(chartInstance->chartInfo.chartInstance);
  chart_debug_initialization(S,1);
}

void c16_LessonIII_method_dispatcher(SimStruct *S, int_T method, void *data)
{
  switch (method) {
   case SS_CALL_MDL_START:
    mdlStart_c16_LessonIII(S);
    break;

   case SS_CALL_MDL_SET_WORK_WIDTHS:
    mdlSetWorkWidths_c16_LessonIII(S);
    break;

   case SS_CALL_MDL_PROCESS_PARAMETERS:
    mdlProcessParameters_c16_LessonIII(S);
    break;

   default:
    /* Unhandled method */
    sf_mex_error_message("Stateflow Internal Error:\n"
                         "Error calling c16_LessonIII_method_dispatcher.\n"
                         "Can't handle method %d.\n", method);
    break;
  }
}

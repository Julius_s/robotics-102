/* Include files */

#include <stddef.h>
#include "blas.h"
#include "LessonI_start_sfun.h"
#include "c32_LessonI_start.h"
#include <string.h>
#define CHARTINSTANCE_CHARTNUMBER      (chartInstance->chartNumber)
#define CHARTINSTANCE_INSTANCENUMBER   (chartInstance->instanceNumber)
#include "LessonI_start_sfun_debug_macros.h"
#define _SF_MEX_LISTEN_FOR_CTRL_C(S)   sf_mex_listen_for_ctrl_c_with_debugger(S, sfGlobalDebugInstanceStruct);

static void chart_debug_initialization(SimStruct *S, unsigned int
  fullDebuggerInitialization);
static void chart_debug_initialize_data_addresses(SimStruct *S);

/* Type Definitions */

/* Named Constants */
#define CALL_EVENT                     (-1)

/* Variable Declarations */

/* Variable Definitions */
static real_T _sfTime_;
static const char * c32_debug_family_names[10] = { "tempEmpty", "colors",
  "positions", "ii", "message", "nargin", "nargout", "Ball", "players", "gameOn"
};

/* Function Declarations */
static void initialize_c32_LessonI_start(SFc32_LessonI_startInstanceStruct
  *chartInstance);
static void initialize_params_c32_LessonI_start
  (SFc32_LessonI_startInstanceStruct *chartInstance);
static void enable_c32_LessonI_start(SFc32_LessonI_startInstanceStruct
  *chartInstance);
static void disable_c32_LessonI_start(SFc32_LessonI_startInstanceStruct
  *chartInstance);
static void c32_update_debugger_state_c32_LessonI_start
  (SFc32_LessonI_startInstanceStruct *chartInstance);
static const mxArray *get_sim_state_c32_LessonI_start
  (SFc32_LessonI_startInstanceStruct *chartInstance);
static void set_sim_state_c32_LessonI_start(SFc32_LessonI_startInstanceStruct
  *chartInstance, const mxArray *c32_st);
static void finalize_c32_LessonI_start(SFc32_LessonI_startInstanceStruct
  *chartInstance);
static void sf_gateway_c32_LessonI_start(SFc32_LessonI_startInstanceStruct
  *chartInstance);
static void mdl_start_c32_LessonI_start(SFc32_LessonI_startInstanceStruct
  *chartInstance);
static void initSimStructsc32_LessonI_start(SFc32_LessonI_startInstanceStruct
  *chartInstance);
static void init_script_number_translation(uint32_T c32_machineNumber, uint32_T
  c32_chartNumber, uint32_T c32_instanceNumber);
static const mxArray *c32_sf_marshallOut(void *chartInstanceVoid, void
  *c32_inData);
static uint8_T c32_emlrt_marshallIn(SFc32_LessonI_startInstanceStruct
  *chartInstance, const mxArray *c32_b_gameOn, const char_T *c32_identifier);
static uint8_T c32_b_emlrt_marshallIn(SFc32_LessonI_startInstanceStruct
  *chartInstance, const mxArray *c32_u, const emlrtMsgIdentifier *c32_parentId);
static void c32_sf_marshallIn(void *chartInstanceVoid, const mxArray
  *c32_mxArrayInData, const char_T *c32_varName, void *c32_outData);
static const mxArray *c32_b_sf_marshallOut(void *chartInstanceVoid, void
  *c32_inData);
static void c32_c_emlrt_marshallIn(SFc32_LessonI_startInstanceStruct
  *chartInstance, const mxArray *c32_b_players, const char_T *c32_identifier,
  c32_Player c32_y[6]);
static void c32_d_emlrt_marshallIn(SFc32_LessonI_startInstanceStruct
  *chartInstance, const mxArray *c32_u, const emlrtMsgIdentifier *c32_parentId,
  c32_Player c32_y[6]);
static int8_T c32_e_emlrt_marshallIn(SFc32_LessonI_startInstanceStruct
  *chartInstance, const mxArray *c32_u, const emlrtMsgIdentifier *c32_parentId);
static int16_T c32_f_emlrt_marshallIn(SFc32_LessonI_startInstanceStruct
  *chartInstance, const mxArray *c32_u, const emlrtMsgIdentifier *c32_parentId);
static void c32_b_sf_marshallIn(void *chartInstanceVoid, const mxArray
  *c32_mxArrayInData, const char_T *c32_varName, void *c32_outData);
static const mxArray *c32_c_sf_marshallOut(void *chartInstanceVoid, void
  *c32_inData);
static c32_Ball c32_g_emlrt_marshallIn(SFc32_LessonI_startInstanceStruct
  *chartInstance, const mxArray *c32_c_Ball, const char_T *c32_identifier);
static c32_Ball c32_h_emlrt_marshallIn(SFc32_LessonI_startInstanceStruct
  *chartInstance, const mxArray *c32_u, const emlrtMsgIdentifier *c32_parentId);
static void c32_c_sf_marshallIn(void *chartInstanceVoid, const mxArray
  *c32_mxArrayInData, const char_T *c32_varName, void *c32_outData);
static const mxArray *c32_d_sf_marshallOut(void *chartInstanceVoid, void
  *c32_inData);
static const mxArray *c32_e_sf_marshallOut(void *chartInstanceVoid, void
  *c32_inData);
static real_T c32_i_emlrt_marshallIn(SFc32_LessonI_startInstanceStruct
  *chartInstance, const mxArray *c32_u, const emlrtMsgIdentifier *c32_parentId);
static void c32_d_sf_marshallIn(void *chartInstanceVoid, const mxArray
  *c32_mxArrayInData, const char_T *c32_varName, void *c32_outData);
static const mxArray *c32_f_sf_marshallOut(void *chartInstanceVoid, void
  *c32_inData);
static void c32_j_emlrt_marshallIn(SFc32_LessonI_startInstanceStruct
  *chartInstance, const mxArray *c32_u, const emlrtMsgIdentifier *c32_parentId,
  uint8_T c32_y[24]);
static void c32_e_sf_marshallIn(void *chartInstanceVoid, const mxArray
  *c32_mxArrayInData, const char_T *c32_varName, void *c32_outData);
static const mxArray *c32_g_sf_marshallOut(void *chartInstanceVoid, void
  *c32_inData);
static void c32_k_emlrt_marshallIn(SFc32_LessonI_startInstanceStruct
  *chartInstance, const mxArray *c32_u, const emlrtMsgIdentifier *c32_parentId,
  uint8_T c32_y[26]);
static void c32_f_sf_marshallIn(void *chartInstanceVoid, const mxArray
  *c32_mxArrayInData, const char_T *c32_varName, void *c32_outData);
static const mxArray *c32_h_sf_marshallOut(void *chartInstanceVoid, void
  *c32_inData);
static void c32_l_emlrt_marshallIn(SFc32_LessonI_startInstanceStruct
  *chartInstance, const mxArray *c32_u, const emlrtMsgIdentifier *c32_parentId,
  uint8_T c32_y[27]);
static void c32_g_sf_marshallIn(void *chartInstanceVoid, const mxArray
  *c32_mxArrayInData, const char_T *c32_varName, void *c32_outData);
static const mxArray *c32_i_sf_marshallOut(void *chartInstanceVoid, void
  *c32_inData);
static void c32_m_emlrt_marshallIn(SFc32_LessonI_startInstanceStruct
  *chartInstance, const mxArray *c32_u, const emlrtMsgIdentifier *c32_parentId,
  uint8_T c32_y[6]);
static void c32_h_sf_marshallIn(void *chartInstanceVoid, const mxArray
  *c32_mxArrayInData, const char_T *c32_varName, void *c32_outData);
static const mxArray *c32_j_sf_marshallOut(void *chartInstanceVoid, void
  *c32_inData);
static c32_Player c32_n_emlrt_marshallIn(SFc32_LessonI_startInstanceStruct
  *chartInstance, const mxArray *c32_u, const emlrtMsgIdentifier *c32_parentId);
static void c32_i_sf_marshallIn(void *chartInstanceVoid, const mxArray
  *c32_mxArrayInData, const char_T *c32_varName, void *c32_outData);
static int8_T c32_typecast(SFc32_LessonI_startInstanceStruct *chartInstance,
  uint8_T c32_x);
static const mxArray *c32_k_sf_marshallOut(void *chartInstanceVoid, void
  *c32_inData);
static int32_T c32_o_emlrt_marshallIn(SFc32_LessonI_startInstanceStruct
  *chartInstance, const mxArray *c32_u, const emlrtMsgIdentifier *c32_parentId);
static void c32_j_sf_marshallIn(void *chartInstanceVoid, const mxArray
  *c32_mxArrayInData, const char_T *c32_varName, void *c32_outData);
static const mxArray *c32_Ball_bus_io(void *chartInstanceVoid, void *c32_pData);
static const mxArray *c32_players_bus_io(void *chartInstanceVoid, void
  *c32_pData);
static void init_dsm_address_info(SFc32_LessonI_startInstanceStruct
  *chartInstance);
static void init_simulink_io_address(SFc32_LessonI_startInstanceStruct
  *chartInstance);

/* Function Definitions */
static void initialize_c32_LessonI_start(SFc32_LessonI_startInstanceStruct
  *chartInstance)
{
  if (sf_is_first_init_cond(chartInstance->S)) {
    initSimStructsc32_LessonI_start(chartInstance);
    chart_debug_initialize_data_addresses(chartInstance->S);
  }

  chartInstance->c32_sfEvent = CALL_EVENT;
  _sfTime_ = sf_get_time(chartInstance->S);
  chartInstance->c32_is_active_c32_LessonI_start = 0U;
}

static void initialize_params_c32_LessonI_start
  (SFc32_LessonI_startInstanceStruct *chartInstance)
{
  (void)chartInstance;
}

static void enable_c32_LessonI_start(SFc32_LessonI_startInstanceStruct
  *chartInstance)
{
  _sfTime_ = sf_get_time(chartInstance->S);
}

static void disable_c32_LessonI_start(SFc32_LessonI_startInstanceStruct
  *chartInstance)
{
  _sfTime_ = sf_get_time(chartInstance->S);
}

static void c32_update_debugger_state_c32_LessonI_start
  (SFc32_LessonI_startInstanceStruct *chartInstance)
{
  (void)chartInstance;
}

static const mxArray *get_sim_state_c32_LessonI_start
  (SFc32_LessonI_startInstanceStruct *chartInstance)
{
  const mxArray *c32_st;
  const mxArray *c32_y = NULL;
  const mxArray *c32_b_y = NULL;
  int8_T c32_u;
  const mxArray *c32_c_y = NULL;
  int8_T c32_b_u;
  const mxArray *c32_d_y = NULL;
  uint8_T c32_c_u;
  const mxArray *c32_e_y = NULL;
  uint8_T c32_hoistedGlobal;
  uint8_T c32_d_u;
  const mxArray *c32_f_y = NULL;
  int32_T c32_i0;
  c32_Player c32_e_u[6];
  const mxArray *c32_g_y = NULL;
  static int32_T c32_iv0[1] = { 6 };

  int32_T c32_iv1[1];
  int32_T c32_i1;
  const c32_Player *c32_r0;
  int8_T c32_f_u;
  const mxArray *c32_h_y = NULL;
  int8_T c32_g_u;
  const mxArray *c32_i_y = NULL;
  int16_T c32_h_u;
  const mxArray *c32_j_y = NULL;
  uint8_T c32_i_u;
  const mxArray *c32_k_y = NULL;
  uint8_T c32_j_u;
  const mxArray *c32_l_y = NULL;
  uint8_T c32_k_u;
  const mxArray *c32_m_y = NULL;
  uint8_T c32_b_hoistedGlobal;
  uint8_T c32_l_u;
  const mxArray *c32_n_y = NULL;
  c32_st = NULL;
  c32_st = NULL;
  c32_y = NULL;
  sf_mex_assign(&c32_y, sf_mex_createcellmatrix(4, 1), false);
  c32_b_y = NULL;
  sf_mex_assign(&c32_b_y, sf_mex_createstruct("structure", 2, 1, 1), false);
  c32_u = *(int8_T *)&((char_T *)chartInstance->c32_b_Ball)[0];
  c32_c_y = NULL;
  sf_mex_assign(&c32_c_y, sf_mex_create("y", &c32_u, 2, 0U, 0U, 0U, 0), false);
  sf_mex_addfield(c32_b_y, c32_c_y, "x", "x", 0);
  c32_b_u = *(int8_T *)&((char_T *)chartInstance->c32_b_Ball)[1];
  c32_d_y = NULL;
  sf_mex_assign(&c32_d_y, sf_mex_create("y", &c32_b_u, 2, 0U, 0U, 0U, 0), false);
  sf_mex_addfield(c32_b_y, c32_d_y, "y", "y", 0);
  c32_c_u = *(uint8_T *)&((char_T *)chartInstance->c32_b_Ball)[2];
  c32_e_y = NULL;
  sf_mex_assign(&c32_e_y, sf_mex_create("y", &c32_c_u, 3, 0U, 0U, 0U, 0), false);
  sf_mex_addfield(c32_b_y, c32_e_y, "valid", "valid", 0);
  sf_mex_setcell(c32_y, 0, c32_b_y);
  c32_hoistedGlobal = *chartInstance->c32_gameOn;
  c32_d_u = c32_hoistedGlobal;
  c32_f_y = NULL;
  sf_mex_assign(&c32_f_y, sf_mex_create("y", &c32_d_u, 3, 0U, 0U, 0U, 0), false);
  sf_mex_setcell(c32_y, 1, c32_f_y);
  for (c32_i0 = 0; c32_i0 < 6; c32_i0++) {
    c32_e_u[c32_i0].x = *(int8_T *)&((char_T *)(c32_Player *)&((char_T *)
      chartInstance->c32_players)[8 * c32_i0])[0];
    c32_e_u[c32_i0].y = *(int8_T *)&((char_T *)(c32_Player *)&((char_T *)
      chartInstance->c32_players)[8 * c32_i0])[1];
    c32_e_u[c32_i0].orientation = *(int16_T *)&((char_T *)(c32_Player *)
      &((char_T *)chartInstance->c32_players)[8 * c32_i0])[2];
    c32_e_u[c32_i0].color = *(uint8_T *)&((char_T *)(c32_Player *)&((char_T *)
      chartInstance->c32_players)[8 * c32_i0])[4];
    c32_e_u[c32_i0].position = *(uint8_T *)&((char_T *)(c32_Player *)&((char_T *)
      chartInstance->c32_players)[8 * c32_i0])[5];
    c32_e_u[c32_i0].valid = *(uint8_T *)&((char_T *)(c32_Player *)&((char_T *)
      chartInstance->c32_players)[8 * c32_i0])[6];
  }

  c32_g_y = NULL;
  c32_iv1[0] = c32_iv0[0];
  sf_mex_assign(&c32_g_y, sf_mex_createstructarray("structure", 1, c32_iv1),
                false);
  for (c32_i1 = 0; c32_i1 < 6; c32_i1++) {
    c32_r0 = &c32_e_u[c32_i1];
    c32_f_u = c32_r0->x;
    c32_h_y = NULL;
    sf_mex_assign(&c32_h_y, sf_mex_create("y", &c32_f_u, 2, 0U, 0U, 0U, 0),
                  false);
    sf_mex_addfield(c32_g_y, c32_h_y, "x", "x", c32_i1);
    c32_g_u = c32_r0->y;
    c32_i_y = NULL;
    sf_mex_assign(&c32_i_y, sf_mex_create("y", &c32_g_u, 2, 0U, 0U, 0U, 0),
                  false);
    sf_mex_addfield(c32_g_y, c32_i_y, "y", "y", c32_i1);
    c32_h_u = c32_r0->orientation;
    c32_j_y = NULL;
    sf_mex_assign(&c32_j_y, sf_mex_create("y", &c32_h_u, 4, 0U, 0U, 0U, 0),
                  false);
    sf_mex_addfield(c32_g_y, c32_j_y, "orientation", "orientation", c32_i1);
    c32_i_u = c32_r0->color;
    c32_k_y = NULL;
    sf_mex_assign(&c32_k_y, sf_mex_create("y", &c32_i_u, 3, 0U, 0U, 0U, 0),
                  false);
    sf_mex_addfield(c32_g_y, c32_k_y, "color", "color", c32_i1);
    c32_j_u = c32_r0->position;
    c32_l_y = NULL;
    sf_mex_assign(&c32_l_y, sf_mex_create("y", &c32_j_u, 3, 0U, 0U, 0U, 0),
                  false);
    sf_mex_addfield(c32_g_y, c32_l_y, "position", "position", c32_i1);
    c32_k_u = c32_r0->valid;
    c32_m_y = NULL;
    sf_mex_assign(&c32_m_y, sf_mex_create("y", &c32_k_u, 3, 0U, 0U, 0U, 0),
                  false);
    sf_mex_addfield(c32_g_y, c32_m_y, "valid", "valid", c32_i1);
  }

  sf_mex_setcell(c32_y, 2, c32_g_y);
  c32_b_hoistedGlobal = chartInstance->c32_is_active_c32_LessonI_start;
  c32_l_u = c32_b_hoistedGlobal;
  c32_n_y = NULL;
  sf_mex_assign(&c32_n_y, sf_mex_create("y", &c32_l_u, 3, 0U, 0U, 0U, 0), false);
  sf_mex_setcell(c32_y, 3, c32_n_y);
  sf_mex_assign(&c32_st, c32_y, false);
  return c32_st;
}

static void set_sim_state_c32_LessonI_start(SFc32_LessonI_startInstanceStruct
  *chartInstance, const mxArray *c32_st)
{
  const mxArray *c32_u;
  c32_Ball c32_r1;
  c32_Player c32_rv0[6];
  int32_T c32_i2;
  chartInstance->c32_doneDoubleBufferReInit = true;
  c32_u = sf_mex_dup(c32_st);
  c32_r1 = c32_g_emlrt_marshallIn(chartInstance, sf_mex_dup(sf_mex_getcell(
    "Ball", c32_u, 0)), "Ball");
  *(int8_T *)&((char_T *)chartInstance->c32_b_Ball)[0] = c32_r1.x;
  *(int8_T *)&((char_T *)chartInstance->c32_b_Ball)[1] = c32_r1.y;
  *(uint8_T *)&((char_T *)chartInstance->c32_b_Ball)[2] = c32_r1.valid;
  *chartInstance->c32_gameOn = c32_emlrt_marshallIn(chartInstance, sf_mex_dup
    (sf_mex_getcell("gameOn", c32_u, 1)), "gameOn");
  c32_c_emlrt_marshallIn(chartInstance, sf_mex_dup(sf_mex_getcell("players",
    c32_u, 2)), "players", c32_rv0);
  for (c32_i2 = 0; c32_i2 < 6; c32_i2++) {
    *(int8_T *)&((char_T *)(c32_Player *)&((char_T *)chartInstance->c32_players)
                 [8 * c32_i2])[0] = c32_rv0[c32_i2].x;
    *(int8_T *)&((char_T *)(c32_Player *)&((char_T *)chartInstance->c32_players)
                 [8 * c32_i2])[1] = c32_rv0[c32_i2].y;
    *(int16_T *)&((char_T *)(c32_Player *)&((char_T *)chartInstance->c32_players)
                  [8 * c32_i2])[2] = c32_rv0[c32_i2].orientation;
    *(uint8_T *)&((char_T *)(c32_Player *)&((char_T *)chartInstance->c32_players)
                  [8 * c32_i2])[4] = c32_rv0[c32_i2].color;
    *(uint8_T *)&((char_T *)(c32_Player *)&((char_T *)chartInstance->c32_players)
                  [8 * c32_i2])[5] = c32_rv0[c32_i2].position;
    *(uint8_T *)&((char_T *)(c32_Player *)&((char_T *)chartInstance->c32_players)
                  [8 * c32_i2])[6] = c32_rv0[c32_i2].valid;
  }

  chartInstance->c32_is_active_c32_LessonI_start = c32_emlrt_marshallIn
    (chartInstance, sf_mex_dup(sf_mex_getcell("is_active_c32_LessonI_start",
       c32_u, 3)), "is_active_c32_LessonI_start");
  sf_mex_destroy(&c32_u);
  c32_update_debugger_state_c32_LessonI_start(chartInstance);
  sf_mex_destroy(&c32_st);
}

static void finalize_c32_LessonI_start(SFc32_LessonI_startInstanceStruct
  *chartInstance)
{
  (void)chartInstance;
}

static void sf_gateway_c32_LessonI_start(SFc32_LessonI_startInstanceStruct
  *chartInstance)
{
  int32_T c32_i3;
  int32_T c32_i4;
  uint8_T c32_b_message[31];
  uint32_T c32_debug_family_var_map[10];
  c32_Player c32_tempEmpty;
  uint8_T c32_colors[6];
  uint8_T c32_positions[6];
  real_T c32_ii;
  uint8_T c32_c_message[27];
  uint8_T c32_d_message[26];
  uint8_T c32_e_message[24];
  real_T c32_nargin = 1.0;
  real_T c32_nargout = 3.0;
  c32_Ball c32_c_Ball;
  c32_Player c32_b_players[6];
  uint8_T c32_b_gameOn;
  c32_Player c32_b_tempEmpty[6];
  int32_T c32_i5;
  int32_T c32_i6;
  int32_T c32_i7;
  int32_T c32_i8;
  int32_T c32_i9;
  static uint8_T c32_uv0[6] = { 103U, 103U, 103U, 98U, 98U, 98U };

  int32_T c32_i10;
  static uint8_T c32_uv1[6] = { 111U, 100U, 103U, 111U, 100U, 103U };

  int32_T c32_b_ii;
  real_T c32_c_ii;
  int32_T c32_i11;
  uint8_T c32_x[2];
  int16_T c32_y;
  int32_T c32_i12;
  _SFD_SYMBOL_SCOPE_PUSH(0U, 0U);
  _sfTime_ = sf_get_time(chartInstance->S);
  _SFD_CC_CALL(CHART_ENTER_SFUNCTION_TAG, 19U, chartInstance->c32_sfEvent);
  for (c32_i3 = 0; c32_i3 < 31; c32_i3++) {
    _SFD_DATA_RANGE_CHECK((real_T)(*chartInstance->c32_message)[c32_i3], 0U, 1U,
                          0U, chartInstance->c32_sfEvent, false);
  }

  chartInstance->c32_sfEvent = CALL_EVENT;
  _SFD_CC_CALL(CHART_ENTER_DURING_FUNCTION_TAG, 19U, chartInstance->c32_sfEvent);
  for (c32_i4 = 0; c32_i4 < 31; c32_i4++) {
    c32_b_message[c32_i4] = (*chartInstance->c32_message)[c32_i4];
  }

  _SFD_SYMBOL_SCOPE_PUSH_EML(0U, 10U, 13U, c32_debug_family_names,
    c32_debug_family_var_map);
  _SFD_SYMBOL_SCOPE_ADD_EML_IMPORTABLE(&c32_tempEmpty, 0U, c32_j_sf_marshallOut,
    c32_i_sf_marshallIn);
  _SFD_SYMBOL_SCOPE_ADD_EML_IMPORTABLE(c32_colors, 1U, c32_i_sf_marshallOut,
    c32_h_sf_marshallIn);
  _SFD_SYMBOL_SCOPE_ADD_EML_IMPORTABLE(c32_positions, 2U, c32_i_sf_marshallOut,
    c32_h_sf_marshallIn);
  _SFD_SYMBOL_SCOPE_ADD_EML_IMPORTABLE(&c32_ii, 3U, c32_e_sf_marshallOut,
    c32_d_sf_marshallIn);
  _SFD_SYMBOL_SCOPE_ADD_EML_IMPORTABLE(c32_c_message, MAX_uint32_T,
    c32_h_sf_marshallOut, c32_g_sf_marshallIn);
  _SFD_SYMBOL_SCOPE_ADD_EML_IMPORTABLE(c32_d_message, MAX_uint32_T,
    c32_g_sf_marshallOut, c32_f_sf_marshallIn);
  _SFD_SYMBOL_SCOPE_ADD_EML_IMPORTABLE(c32_e_message, MAX_uint32_T,
    c32_f_sf_marshallOut, c32_e_sf_marshallIn);
  _SFD_SYMBOL_SCOPE_ADD_EML_IMPORTABLE(&c32_nargin, 5U, c32_e_sf_marshallOut,
    c32_d_sf_marshallIn);
  _SFD_SYMBOL_SCOPE_ADD_EML_IMPORTABLE(&c32_nargout, 6U, c32_e_sf_marshallOut,
    c32_d_sf_marshallIn);
  _SFD_SYMBOL_SCOPE_ADD_EML(c32_b_message, 4U, c32_d_sf_marshallOut);
  _SFD_SYMBOL_SCOPE_ADD_EML_IMPORTABLE(&c32_c_Ball, 7U, c32_c_sf_marshallOut,
    c32_c_sf_marshallIn);
  _SFD_SYMBOL_SCOPE_ADD_EML_IMPORTABLE(c32_b_players, 8U, c32_b_sf_marshallOut,
    c32_b_sf_marshallIn);
  _SFD_SYMBOL_SCOPE_ADD_EML_IMPORTABLE(&c32_b_gameOn, 9U, c32_sf_marshallOut,
    c32_sf_marshallIn);
  CV_EML_FCN(0, 0);
  _SFD_EML_CALL(0U, chartInstance->c32_sfEvent, 3);
  c32_tempEmpty.x = 0;
  _SFD_EML_CALL(0U, chartInstance->c32_sfEvent, 4);
  c32_tempEmpty.y = 0;
  _SFD_EML_CALL(0U, chartInstance->c32_sfEvent, 5);
  c32_tempEmpty.orientation = 0;
  _SFD_EML_CALL(0U, chartInstance->c32_sfEvent, 6);
  c32_tempEmpty.color = 0U;
  _SFD_EML_CALL(0U, chartInstance->c32_sfEvent, 7);
  c32_tempEmpty.position = 0U;
  _SFD_EML_CALL(0U, chartInstance->c32_sfEvent, 8);
  c32_tempEmpty.valid = 0U;
  _SFD_EML_CALL(0U, chartInstance->c32_sfEvent, 9);
  c32_b_tempEmpty[0] = c32_tempEmpty;
  c32_b_tempEmpty[1] = c32_tempEmpty;
  c32_b_tempEmpty[2] = c32_tempEmpty;
  c32_b_tempEmpty[3] = c32_tempEmpty;
  c32_b_tempEmpty[4] = c32_tempEmpty;
  c32_b_tempEmpty[5] = c32_tempEmpty;
  for (c32_i5 = 0; c32_i5 < 6; c32_i5++) {
    c32_b_players[c32_i5] = c32_b_tempEmpty[c32_i5];
  }

  _SFD_EML_CALL(0U, chartInstance->c32_sfEvent, 11);
  for (c32_i6 = 0; c32_i6 < 27; c32_i6++) {
    c32_c_message[c32_i6] = c32_b_message[c32_i6 + 4];
  }

  _SFD_SYMBOL_SWITCH(4U, 4U);
  _SFD_EML_CALL(0U, chartInstance->c32_sfEvent, 12);
  c32_b_gameOn = c32_c_message[0];
  _SFD_EML_CALL(0U, chartInstance->c32_sfEvent, 13);
  for (c32_i7 = 0; c32_i7 < 26; c32_i7++) {
    c32_d_message[c32_i7] = c32_c_message[c32_i7 + 1];
  }

  _SFD_SYMBOL_SWITCH(4U, 5U);
  _SFD_EML_CALL(0U, chartInstance->c32_sfEvent, 14);
  c32_c_Ball.x = c32_typecast(chartInstance, c32_d_message[0]);
  _SFD_EML_CALL(0U, chartInstance->c32_sfEvent, 15);
  c32_c_Ball.y = c32_typecast(chartInstance, c32_d_message[1]);
  _SFD_EML_CALL(0U, chartInstance->c32_sfEvent, 16);
  c32_c_Ball.valid = 1U;
  _SFD_EML_CALL(0U, chartInstance->c32_sfEvent, 17);
  for (c32_i8 = 0; c32_i8 < 24; c32_i8++) {
    c32_e_message[c32_i8] = c32_d_message[c32_i8 + 2];
  }

  _SFD_SYMBOL_SWITCH(4U, 6U);
  _SFD_EML_CALL(0U, chartInstance->c32_sfEvent, 18);
  for (c32_i9 = 0; c32_i9 < 6; c32_i9++) {
    c32_colors[c32_i9] = c32_uv0[c32_i9];
  }

  _SFD_EML_CALL(0U, chartInstance->c32_sfEvent, 19);
  for (c32_i10 = 0; c32_i10 < 6; c32_i10++) {
    c32_positions[c32_i10] = c32_uv1[c32_i10];
  }

  _SFD_EML_CALL(0U, chartInstance->c32_sfEvent, 20);
  c32_ii = 1.0;
  c32_b_ii = 0;
  while (c32_b_ii < 6) {
    c32_ii = 1.0 + (real_T)c32_b_ii;
    CV_EML_FOR(0, 1, 0, 1);
    _SFD_EML_CALL(0U, chartInstance->c32_sfEvent, 21);
    c32_b_players[_SFD_EML_ARRAY_BOUNDS_CHECK("players", (int32_T)
      _SFD_INTEGER_CHECK("ii", c32_ii), 1, 6, 1, 0) - 1].x = c32_typecast
      (chartInstance, c32_e_message[_SFD_EML_ARRAY_BOUNDS_CHECK("message",
        (int32_T)_SFD_INTEGER_CHECK("(ii-1)*4+1", (c32_ii - 1.0) * 4.0 + 1.0), 1,
        24, 1, 0) - 1]);
    _SFD_EML_CALL(0U, chartInstance->c32_sfEvent, 22);
    c32_b_players[_SFD_EML_ARRAY_BOUNDS_CHECK("players", (int32_T)
      _SFD_INTEGER_CHECK("ii", c32_ii), 1, 6, 1, 0) - 1].y = c32_typecast
      (chartInstance, c32_e_message[_SFD_EML_ARRAY_BOUNDS_CHECK("message",
        (int32_T)_SFD_INTEGER_CHECK("(ii-1)*4+2", (c32_ii - 1.0) * 4.0 + 2.0), 1,
        24, 1, 0) - 1]);
    _SFD_EML_CALL(0U, chartInstance->c32_sfEvent, 23);
    c32_c_ii = (c32_ii - 1.0) * 4.0;
    for (c32_i11 = 0; c32_i11 < 2; c32_i11++) {
      c32_x[c32_i11] = c32_e_message[_SFD_EML_ARRAY_BOUNDS_CHECK("message",
        (int32_T)_SFD_INTEGER_CHECK("(ii-1)*4+3:(ii-1)*4+4", c32_c_ii + (3.0 +
        (real_T)c32_i11)), 1, 24, 1, 0) - 1];
    }

    memcpy(&c32_y, &c32_x[0], (size_t)1 * sizeof(int16_T));
    c32_b_players[_SFD_EML_ARRAY_BOUNDS_CHECK("players", (int32_T)
      _SFD_INTEGER_CHECK("ii", c32_ii), 1, 6, 1, 0) - 1].orientation = c32_y;
    _SFD_EML_CALL(0U, chartInstance->c32_sfEvent, 24);
    c32_b_players[_SFD_EML_ARRAY_BOUNDS_CHECK("players", (int32_T)
      _SFD_INTEGER_CHECK("ii", c32_ii), 1, 6, 1, 0) - 1].color =
      c32_colors[_SFD_EML_ARRAY_BOUNDS_CHECK("colors", (int32_T)
      _SFD_INTEGER_CHECK("ii", c32_ii), 1, 6, 1, 0) - 1];
    _SFD_EML_CALL(0U, chartInstance->c32_sfEvent, 25);
    c32_b_players[_SFD_EML_ARRAY_BOUNDS_CHECK("players", (int32_T)
      _SFD_INTEGER_CHECK("ii", c32_ii), 1, 6, 1, 0) - 1].position =
      c32_positions[_SFD_EML_ARRAY_BOUNDS_CHECK("positions", (int32_T)
      _SFD_INTEGER_CHECK("ii", c32_ii), 1, 6, 1, 0) - 1];
    _SFD_EML_CALL(0U, chartInstance->c32_sfEvent, 26);
    c32_b_players[_SFD_EML_ARRAY_BOUNDS_CHECK("players", (int32_T)
      _SFD_INTEGER_CHECK("ii", c32_ii), 1, 6, 1, 0) - 1].valid = 1U;
    c32_b_ii++;
    _SF_MEX_LISTEN_FOR_CTRL_C(chartInstance->S);
  }

  CV_EML_FOR(0, 1, 0, 0);
  _SFD_EML_CALL(0U, chartInstance->c32_sfEvent, -26);
  _SFD_SYMBOL_SCOPE_POP();
  *(int8_T *)&((char_T *)chartInstance->c32_b_Ball)[0] = c32_c_Ball.x;
  *(int8_T *)&((char_T *)chartInstance->c32_b_Ball)[1] = c32_c_Ball.y;
  *(uint8_T *)&((char_T *)chartInstance->c32_b_Ball)[2] = c32_c_Ball.valid;
  for (c32_i12 = 0; c32_i12 < 6; c32_i12++) {
    *(int8_T *)&((char_T *)(c32_Player *)&((char_T *)chartInstance->c32_players)
                 [8 * c32_i12])[0] = c32_b_players[c32_i12].x;
    *(int8_T *)&((char_T *)(c32_Player *)&((char_T *)chartInstance->c32_players)
                 [8 * c32_i12])[1] = c32_b_players[c32_i12].y;
    *(int16_T *)&((char_T *)(c32_Player *)&((char_T *)chartInstance->c32_players)
                  [8 * c32_i12])[2] = c32_b_players[c32_i12].orientation;
    *(uint8_T *)&((char_T *)(c32_Player *)&((char_T *)chartInstance->c32_players)
                  [8 * c32_i12])[4] = c32_b_players[c32_i12].color;
    *(uint8_T *)&((char_T *)(c32_Player *)&((char_T *)chartInstance->c32_players)
                  [8 * c32_i12])[5] = c32_b_players[c32_i12].position;
    *(uint8_T *)&((char_T *)(c32_Player *)&((char_T *)chartInstance->c32_players)
                  [8 * c32_i12])[6] = c32_b_players[c32_i12].valid;
  }

  *chartInstance->c32_gameOn = c32_b_gameOn;
  _SFD_CC_CALL(EXIT_OUT_OF_FUNCTION_TAG, 19U, chartInstance->c32_sfEvent);
  _SFD_SYMBOL_SCOPE_POP();
  _SFD_CHECK_FOR_STATE_INCONSISTENCY(_LessonI_startMachineNumber_,
    chartInstance->chartNumber, chartInstance->instanceNumber);
  _SFD_DATA_RANGE_CHECK((real_T)*chartInstance->c32_gameOn, 3U, 1U, 0U,
                        chartInstance->c32_sfEvent, false);
}

static void mdl_start_c32_LessonI_start(SFc32_LessonI_startInstanceStruct
  *chartInstance)
{
  (void)chartInstance;
}

static void initSimStructsc32_LessonI_start(SFc32_LessonI_startInstanceStruct
  *chartInstance)
{
  (void)chartInstance;
}

static void init_script_number_translation(uint32_T c32_machineNumber, uint32_T
  c32_chartNumber, uint32_T c32_instanceNumber)
{
  (void)c32_machineNumber;
  (void)c32_chartNumber;
  (void)c32_instanceNumber;
}

static const mxArray *c32_sf_marshallOut(void *chartInstanceVoid, void
  *c32_inData)
{
  const mxArray *c32_mxArrayOutData = NULL;
  uint8_T c32_u;
  const mxArray *c32_y = NULL;
  SFc32_LessonI_startInstanceStruct *chartInstance;
  chartInstance = (SFc32_LessonI_startInstanceStruct *)chartInstanceVoid;
  c32_mxArrayOutData = NULL;
  c32_u = *(uint8_T *)c32_inData;
  c32_y = NULL;
  sf_mex_assign(&c32_y, sf_mex_create("y", &c32_u, 3, 0U, 0U, 0U, 0), false);
  sf_mex_assign(&c32_mxArrayOutData, c32_y, false);
  return c32_mxArrayOutData;
}

static uint8_T c32_emlrt_marshallIn(SFc32_LessonI_startInstanceStruct
  *chartInstance, const mxArray *c32_b_gameOn, const char_T *c32_identifier)
{
  uint8_T c32_y;
  emlrtMsgIdentifier c32_thisId;
  c32_thisId.fIdentifier = c32_identifier;
  c32_thisId.fParent = NULL;
  c32_y = c32_b_emlrt_marshallIn(chartInstance, sf_mex_dup(c32_b_gameOn),
    &c32_thisId);
  sf_mex_destroy(&c32_b_gameOn);
  return c32_y;
}

static uint8_T c32_b_emlrt_marshallIn(SFc32_LessonI_startInstanceStruct
  *chartInstance, const mxArray *c32_u, const emlrtMsgIdentifier *c32_parentId)
{
  uint8_T c32_y;
  uint8_T c32_u0;
  (void)chartInstance;
  sf_mex_import(c32_parentId, sf_mex_dup(c32_u), &c32_u0, 1, 3, 0U, 0, 0U, 0);
  c32_y = c32_u0;
  sf_mex_destroy(&c32_u);
  return c32_y;
}

static void c32_sf_marshallIn(void *chartInstanceVoid, const mxArray
  *c32_mxArrayInData, const char_T *c32_varName, void *c32_outData)
{
  const mxArray *c32_b_gameOn;
  const char_T *c32_identifier;
  emlrtMsgIdentifier c32_thisId;
  uint8_T c32_y;
  SFc32_LessonI_startInstanceStruct *chartInstance;
  chartInstance = (SFc32_LessonI_startInstanceStruct *)chartInstanceVoid;
  c32_b_gameOn = sf_mex_dup(c32_mxArrayInData);
  c32_identifier = c32_varName;
  c32_thisId.fIdentifier = c32_identifier;
  c32_thisId.fParent = NULL;
  c32_y = c32_b_emlrt_marshallIn(chartInstance, sf_mex_dup(c32_b_gameOn),
    &c32_thisId);
  sf_mex_destroy(&c32_b_gameOn);
  *(uint8_T *)c32_outData = c32_y;
  sf_mex_destroy(&c32_mxArrayInData);
}

static const mxArray *c32_b_sf_marshallOut(void *chartInstanceVoid, void
  *c32_inData)
{
  const mxArray *c32_mxArrayOutData;
  int32_T c32_i13;
  c32_Player c32_b_inData[6];
  int32_T c32_i14;
  c32_Player c32_u[6];
  const mxArray *c32_y = NULL;
  static int32_T c32_iv2[1] = { 6 };

  int32_T c32_iv3[1];
  int32_T c32_i15;
  const c32_Player *c32_r2;
  int8_T c32_b_u;
  const mxArray *c32_b_y = NULL;
  int8_T c32_c_u;
  const mxArray *c32_c_y = NULL;
  int16_T c32_d_u;
  const mxArray *c32_d_y = NULL;
  uint8_T c32_e_u;
  const mxArray *c32_e_y = NULL;
  uint8_T c32_f_u;
  const mxArray *c32_f_y = NULL;
  uint8_T c32_g_u;
  const mxArray *c32_g_y = NULL;
  SFc32_LessonI_startInstanceStruct *chartInstance;
  chartInstance = (SFc32_LessonI_startInstanceStruct *)chartInstanceVoid;
  c32_mxArrayOutData = NULL;
  c32_mxArrayOutData = NULL;
  for (c32_i13 = 0; c32_i13 < 6; c32_i13++) {
    c32_b_inData[c32_i13] = (*(c32_Player (*)[6])c32_inData)[c32_i13];
  }

  for (c32_i14 = 0; c32_i14 < 6; c32_i14++) {
    c32_u[c32_i14] = c32_b_inData[c32_i14];
  }

  c32_y = NULL;
  c32_iv3[0] = c32_iv2[0];
  sf_mex_assign(&c32_y, sf_mex_createstructarray("structure", 1, c32_iv3), false);
  for (c32_i15 = 0; c32_i15 < 6; c32_i15++) {
    c32_r2 = &c32_u[c32_i15];
    c32_b_u = c32_r2->x;
    c32_b_y = NULL;
    sf_mex_assign(&c32_b_y, sf_mex_create("y", &c32_b_u, 2, 0U, 0U, 0U, 0),
                  false);
    sf_mex_addfield(c32_y, c32_b_y, "x", "x", c32_i15);
    c32_c_u = c32_r2->y;
    c32_c_y = NULL;
    sf_mex_assign(&c32_c_y, sf_mex_create("y", &c32_c_u, 2, 0U, 0U, 0U, 0),
                  false);
    sf_mex_addfield(c32_y, c32_c_y, "y", "y", c32_i15);
    c32_d_u = c32_r2->orientation;
    c32_d_y = NULL;
    sf_mex_assign(&c32_d_y, sf_mex_create("y", &c32_d_u, 4, 0U, 0U, 0U, 0),
                  false);
    sf_mex_addfield(c32_y, c32_d_y, "orientation", "orientation", c32_i15);
    c32_e_u = c32_r2->color;
    c32_e_y = NULL;
    sf_mex_assign(&c32_e_y, sf_mex_create("y", &c32_e_u, 3, 0U, 0U, 0U, 0),
                  false);
    sf_mex_addfield(c32_y, c32_e_y, "color", "color", c32_i15);
    c32_f_u = c32_r2->position;
    c32_f_y = NULL;
    sf_mex_assign(&c32_f_y, sf_mex_create("y", &c32_f_u, 3, 0U, 0U, 0U, 0),
                  false);
    sf_mex_addfield(c32_y, c32_f_y, "position", "position", c32_i15);
    c32_g_u = c32_r2->valid;
    c32_g_y = NULL;
    sf_mex_assign(&c32_g_y, sf_mex_create("y", &c32_g_u, 3, 0U, 0U, 0U, 0),
                  false);
    sf_mex_addfield(c32_y, c32_g_y, "valid", "valid", c32_i15);
  }

  sf_mex_assign(&c32_mxArrayOutData, c32_y, false);
  return c32_mxArrayOutData;
}

static void c32_c_emlrt_marshallIn(SFc32_LessonI_startInstanceStruct
  *chartInstance, const mxArray *c32_b_players, const char_T *c32_identifier,
  c32_Player c32_y[6])
{
  emlrtMsgIdentifier c32_thisId;
  c32_thisId.fIdentifier = c32_identifier;
  c32_thisId.fParent = NULL;
  c32_d_emlrt_marshallIn(chartInstance, sf_mex_dup(c32_b_players), &c32_thisId,
    c32_y);
  sf_mex_destroy(&c32_b_players);
}

static void c32_d_emlrt_marshallIn(SFc32_LessonI_startInstanceStruct
  *chartInstance, const mxArray *c32_u, const emlrtMsgIdentifier *c32_parentId,
  c32_Player c32_y[6])
{
  static uint32_T c32_uv2[1] = { 6U };

  uint32_T c32_uv3[1];
  emlrtMsgIdentifier c32_thisId;
  static const char * c32_fieldNames[6] = { "x", "y", "orientation", "color",
    "position", "valid" };

  int32_T c32_i16;
  c32_uv3[0] = c32_uv2[0];
  c32_thisId.fParent = c32_parentId;
  sf_mex_check_struct(c32_parentId, c32_u, 6, c32_fieldNames, 1U, c32_uv3);
  for (c32_i16 = 0; c32_i16 < 6; c32_i16++) {
    c32_thisId.fIdentifier = "x";
    c32_y[c32_i16].x = c32_e_emlrt_marshallIn(chartInstance, sf_mex_dup
      (sf_mex_getfield(c32_u, "x", "x", c32_i16)), &c32_thisId);
    c32_thisId.fIdentifier = "y";
    c32_y[c32_i16].y = c32_e_emlrt_marshallIn(chartInstance, sf_mex_dup
      (sf_mex_getfield(c32_u, "y", "y", c32_i16)), &c32_thisId);
    c32_thisId.fIdentifier = "orientation";
    c32_y[c32_i16].orientation = c32_f_emlrt_marshallIn(chartInstance,
      sf_mex_dup(sf_mex_getfield(c32_u, "orientation", "orientation", c32_i16)),
      &c32_thisId);
    c32_thisId.fIdentifier = "color";
    c32_y[c32_i16].color = c32_b_emlrt_marshallIn(chartInstance, sf_mex_dup
      (sf_mex_getfield(c32_u, "color", "color", c32_i16)), &c32_thisId);
    c32_thisId.fIdentifier = "position";
    c32_y[c32_i16].position = c32_b_emlrt_marshallIn(chartInstance, sf_mex_dup
      (sf_mex_getfield(c32_u, "position", "position", c32_i16)), &c32_thisId);
    c32_thisId.fIdentifier = "valid";
    c32_y[c32_i16].valid = c32_b_emlrt_marshallIn(chartInstance, sf_mex_dup
      (sf_mex_getfield(c32_u, "valid", "valid", c32_i16)), &c32_thisId);
  }

  sf_mex_destroy(&c32_u);
}

static int8_T c32_e_emlrt_marshallIn(SFc32_LessonI_startInstanceStruct
  *chartInstance, const mxArray *c32_u, const emlrtMsgIdentifier *c32_parentId)
{
  int8_T c32_y;
  int8_T c32_i17;
  (void)chartInstance;
  sf_mex_import(c32_parentId, sf_mex_dup(c32_u), &c32_i17, 1, 2, 0U, 0, 0U, 0);
  c32_y = c32_i17;
  sf_mex_destroy(&c32_u);
  return c32_y;
}

static int16_T c32_f_emlrt_marshallIn(SFc32_LessonI_startInstanceStruct
  *chartInstance, const mxArray *c32_u, const emlrtMsgIdentifier *c32_parentId)
{
  int16_T c32_y;
  int16_T c32_i18;
  (void)chartInstance;
  sf_mex_import(c32_parentId, sf_mex_dup(c32_u), &c32_i18, 1, 4, 0U, 0, 0U, 0);
  c32_y = c32_i18;
  sf_mex_destroy(&c32_u);
  return c32_y;
}

static void c32_b_sf_marshallIn(void *chartInstanceVoid, const mxArray
  *c32_mxArrayInData, const char_T *c32_varName, void *c32_outData)
{
  const mxArray *c32_b_players;
  const char_T *c32_identifier;
  emlrtMsgIdentifier c32_thisId;
  c32_Player c32_y[6];
  int32_T c32_i19;
  SFc32_LessonI_startInstanceStruct *chartInstance;
  chartInstance = (SFc32_LessonI_startInstanceStruct *)chartInstanceVoid;
  c32_b_players = sf_mex_dup(c32_mxArrayInData);
  c32_identifier = c32_varName;
  c32_thisId.fIdentifier = c32_identifier;
  c32_thisId.fParent = NULL;
  c32_d_emlrt_marshallIn(chartInstance, sf_mex_dup(c32_b_players), &c32_thisId,
    c32_y);
  sf_mex_destroy(&c32_b_players);
  for (c32_i19 = 0; c32_i19 < 6; c32_i19++) {
    (*(c32_Player (*)[6])c32_outData)[c32_i19] = c32_y[c32_i19];
  }

  sf_mex_destroy(&c32_mxArrayInData);
}

static const mxArray *c32_c_sf_marshallOut(void *chartInstanceVoid, void
  *c32_inData)
{
  const mxArray *c32_mxArrayOutData;
  c32_Ball c32_u;
  const mxArray *c32_y = NULL;
  int8_T c32_b_u;
  const mxArray *c32_b_y = NULL;
  int8_T c32_c_u;
  const mxArray *c32_c_y = NULL;
  uint8_T c32_d_u;
  const mxArray *c32_d_y = NULL;
  SFc32_LessonI_startInstanceStruct *chartInstance;
  chartInstance = (SFc32_LessonI_startInstanceStruct *)chartInstanceVoid;
  c32_mxArrayOutData = NULL;
  c32_mxArrayOutData = NULL;
  c32_u = *(c32_Ball *)c32_inData;
  c32_y = NULL;
  sf_mex_assign(&c32_y, sf_mex_createstruct("structure", 2, 1, 1), false);
  c32_b_u = c32_u.x;
  c32_b_y = NULL;
  sf_mex_assign(&c32_b_y, sf_mex_create("y", &c32_b_u, 2, 0U, 0U, 0U, 0), false);
  sf_mex_addfield(c32_y, c32_b_y, "x", "x", 0);
  c32_c_u = c32_u.y;
  c32_c_y = NULL;
  sf_mex_assign(&c32_c_y, sf_mex_create("y", &c32_c_u, 2, 0U, 0U, 0U, 0), false);
  sf_mex_addfield(c32_y, c32_c_y, "y", "y", 0);
  c32_d_u = c32_u.valid;
  c32_d_y = NULL;
  sf_mex_assign(&c32_d_y, sf_mex_create("y", &c32_d_u, 3, 0U, 0U, 0U, 0), false);
  sf_mex_addfield(c32_y, c32_d_y, "valid", "valid", 0);
  sf_mex_assign(&c32_mxArrayOutData, c32_y, false);
  return c32_mxArrayOutData;
}

static c32_Ball c32_g_emlrt_marshallIn(SFc32_LessonI_startInstanceStruct
  *chartInstance, const mxArray *c32_c_Ball, const char_T *c32_identifier)
{
  c32_Ball c32_y;
  emlrtMsgIdentifier c32_thisId;
  c32_thisId.fIdentifier = c32_identifier;
  c32_thisId.fParent = NULL;
  c32_y = c32_h_emlrt_marshallIn(chartInstance, sf_mex_dup(c32_c_Ball),
    &c32_thisId);
  sf_mex_destroy(&c32_c_Ball);
  return c32_y;
}

static c32_Ball c32_h_emlrt_marshallIn(SFc32_LessonI_startInstanceStruct
  *chartInstance, const mxArray *c32_u, const emlrtMsgIdentifier *c32_parentId)
{
  c32_Ball c32_y;
  emlrtMsgIdentifier c32_thisId;
  static const char * c32_fieldNames[3] = { "x", "y", "valid" };

  c32_thisId.fParent = c32_parentId;
  sf_mex_check_struct(c32_parentId, c32_u, 3, c32_fieldNames, 0U, NULL);
  c32_thisId.fIdentifier = "x";
  c32_y.x = c32_e_emlrt_marshallIn(chartInstance, sf_mex_dup(sf_mex_getfield
    (c32_u, "x", "x", 0)), &c32_thisId);
  c32_thisId.fIdentifier = "y";
  c32_y.y = c32_e_emlrt_marshallIn(chartInstance, sf_mex_dup(sf_mex_getfield
    (c32_u, "y", "y", 0)), &c32_thisId);
  c32_thisId.fIdentifier = "valid";
  c32_y.valid = c32_b_emlrt_marshallIn(chartInstance, sf_mex_dup(sf_mex_getfield
    (c32_u, "valid", "valid", 0)), &c32_thisId);
  sf_mex_destroy(&c32_u);
  return c32_y;
}

static void c32_c_sf_marshallIn(void *chartInstanceVoid, const mxArray
  *c32_mxArrayInData, const char_T *c32_varName, void *c32_outData)
{
  const mxArray *c32_c_Ball;
  const char_T *c32_identifier;
  emlrtMsgIdentifier c32_thisId;
  c32_Ball c32_y;
  SFc32_LessonI_startInstanceStruct *chartInstance;
  chartInstance = (SFc32_LessonI_startInstanceStruct *)chartInstanceVoid;
  c32_c_Ball = sf_mex_dup(c32_mxArrayInData);
  c32_identifier = c32_varName;
  c32_thisId.fIdentifier = c32_identifier;
  c32_thisId.fParent = NULL;
  c32_y = c32_h_emlrt_marshallIn(chartInstance, sf_mex_dup(c32_c_Ball),
    &c32_thisId);
  sf_mex_destroy(&c32_c_Ball);
  *(c32_Ball *)c32_outData = c32_y;
  sf_mex_destroy(&c32_mxArrayInData);
}

static const mxArray *c32_d_sf_marshallOut(void *chartInstanceVoid, void
  *c32_inData)
{
  const mxArray *c32_mxArrayOutData = NULL;
  int32_T c32_i20;
  uint8_T c32_u[31];
  const mxArray *c32_y = NULL;
  SFc32_LessonI_startInstanceStruct *chartInstance;
  chartInstance = (SFc32_LessonI_startInstanceStruct *)chartInstanceVoid;
  c32_mxArrayOutData = NULL;
  for (c32_i20 = 0; c32_i20 < 31; c32_i20++) {
    c32_u[c32_i20] = (*(uint8_T (*)[31])c32_inData)[c32_i20];
  }

  c32_y = NULL;
  sf_mex_assign(&c32_y, sf_mex_create("y", c32_u, 3, 0U, 1U, 0U, 2, 1, 31),
                false);
  sf_mex_assign(&c32_mxArrayOutData, c32_y, false);
  return c32_mxArrayOutData;
}

static const mxArray *c32_e_sf_marshallOut(void *chartInstanceVoid, void
  *c32_inData)
{
  const mxArray *c32_mxArrayOutData = NULL;
  real_T c32_u;
  const mxArray *c32_y = NULL;
  SFc32_LessonI_startInstanceStruct *chartInstance;
  chartInstance = (SFc32_LessonI_startInstanceStruct *)chartInstanceVoid;
  c32_mxArrayOutData = NULL;
  c32_u = *(real_T *)c32_inData;
  c32_y = NULL;
  sf_mex_assign(&c32_y, sf_mex_create("y", &c32_u, 0, 0U, 0U, 0U, 0), false);
  sf_mex_assign(&c32_mxArrayOutData, c32_y, false);
  return c32_mxArrayOutData;
}

static real_T c32_i_emlrt_marshallIn(SFc32_LessonI_startInstanceStruct
  *chartInstance, const mxArray *c32_u, const emlrtMsgIdentifier *c32_parentId)
{
  real_T c32_y;
  real_T c32_d0;
  (void)chartInstance;
  sf_mex_import(c32_parentId, sf_mex_dup(c32_u), &c32_d0, 1, 0, 0U, 0, 0U, 0);
  c32_y = c32_d0;
  sf_mex_destroy(&c32_u);
  return c32_y;
}

static void c32_d_sf_marshallIn(void *chartInstanceVoid, const mxArray
  *c32_mxArrayInData, const char_T *c32_varName, void *c32_outData)
{
  const mxArray *c32_nargout;
  const char_T *c32_identifier;
  emlrtMsgIdentifier c32_thisId;
  real_T c32_y;
  SFc32_LessonI_startInstanceStruct *chartInstance;
  chartInstance = (SFc32_LessonI_startInstanceStruct *)chartInstanceVoid;
  c32_nargout = sf_mex_dup(c32_mxArrayInData);
  c32_identifier = c32_varName;
  c32_thisId.fIdentifier = c32_identifier;
  c32_thisId.fParent = NULL;
  c32_y = c32_i_emlrt_marshallIn(chartInstance, sf_mex_dup(c32_nargout),
    &c32_thisId);
  sf_mex_destroy(&c32_nargout);
  *(real_T *)c32_outData = c32_y;
  sf_mex_destroy(&c32_mxArrayInData);
}

static const mxArray *c32_f_sf_marshallOut(void *chartInstanceVoid, void
  *c32_inData)
{
  const mxArray *c32_mxArrayOutData = NULL;
  int32_T c32_i21;
  uint8_T c32_u[24];
  const mxArray *c32_y = NULL;
  SFc32_LessonI_startInstanceStruct *chartInstance;
  chartInstance = (SFc32_LessonI_startInstanceStruct *)chartInstanceVoid;
  c32_mxArrayOutData = NULL;
  for (c32_i21 = 0; c32_i21 < 24; c32_i21++) {
    c32_u[c32_i21] = (*(uint8_T (*)[24])c32_inData)[c32_i21];
  }

  c32_y = NULL;
  sf_mex_assign(&c32_y, sf_mex_create("y", c32_u, 3, 0U, 1U, 0U, 2, 1, 24),
                false);
  sf_mex_assign(&c32_mxArrayOutData, c32_y, false);
  return c32_mxArrayOutData;
}

static void c32_j_emlrt_marshallIn(SFc32_LessonI_startInstanceStruct
  *chartInstance, const mxArray *c32_u, const emlrtMsgIdentifier *c32_parentId,
  uint8_T c32_y[24])
{
  uint8_T c32_uv4[24];
  int32_T c32_i22;
  (void)chartInstance;
  sf_mex_import(c32_parentId, sf_mex_dup(c32_u), c32_uv4, 1, 3, 0U, 1, 0U, 2, 1,
                24);
  for (c32_i22 = 0; c32_i22 < 24; c32_i22++) {
    c32_y[c32_i22] = c32_uv4[c32_i22];
  }

  sf_mex_destroy(&c32_u);
}

static void c32_e_sf_marshallIn(void *chartInstanceVoid, const mxArray
  *c32_mxArrayInData, const char_T *c32_varName, void *c32_outData)
{
  const mxArray *c32_b_message;
  const char_T *c32_identifier;
  emlrtMsgIdentifier c32_thisId;
  uint8_T c32_y[24];
  int32_T c32_i23;
  SFc32_LessonI_startInstanceStruct *chartInstance;
  chartInstance = (SFc32_LessonI_startInstanceStruct *)chartInstanceVoid;
  c32_b_message = sf_mex_dup(c32_mxArrayInData);
  c32_identifier = c32_varName;
  c32_thisId.fIdentifier = c32_identifier;
  c32_thisId.fParent = NULL;
  c32_j_emlrt_marshallIn(chartInstance, sf_mex_dup(c32_b_message), &c32_thisId,
    c32_y);
  sf_mex_destroy(&c32_b_message);
  for (c32_i23 = 0; c32_i23 < 24; c32_i23++) {
    (*(uint8_T (*)[24])c32_outData)[c32_i23] = c32_y[c32_i23];
  }

  sf_mex_destroy(&c32_mxArrayInData);
}

static const mxArray *c32_g_sf_marshallOut(void *chartInstanceVoid, void
  *c32_inData)
{
  const mxArray *c32_mxArrayOutData = NULL;
  int32_T c32_i24;
  uint8_T c32_u[26];
  const mxArray *c32_y = NULL;
  SFc32_LessonI_startInstanceStruct *chartInstance;
  chartInstance = (SFc32_LessonI_startInstanceStruct *)chartInstanceVoid;
  c32_mxArrayOutData = NULL;
  for (c32_i24 = 0; c32_i24 < 26; c32_i24++) {
    c32_u[c32_i24] = (*(uint8_T (*)[26])c32_inData)[c32_i24];
  }

  c32_y = NULL;
  sf_mex_assign(&c32_y, sf_mex_create("y", c32_u, 3, 0U, 1U, 0U, 2, 1, 26),
                false);
  sf_mex_assign(&c32_mxArrayOutData, c32_y, false);
  return c32_mxArrayOutData;
}

static void c32_k_emlrt_marshallIn(SFc32_LessonI_startInstanceStruct
  *chartInstance, const mxArray *c32_u, const emlrtMsgIdentifier *c32_parentId,
  uint8_T c32_y[26])
{
  uint8_T c32_uv5[26];
  int32_T c32_i25;
  (void)chartInstance;
  sf_mex_import(c32_parentId, sf_mex_dup(c32_u), c32_uv5, 1, 3, 0U, 1, 0U, 2, 1,
                26);
  for (c32_i25 = 0; c32_i25 < 26; c32_i25++) {
    c32_y[c32_i25] = c32_uv5[c32_i25];
  }

  sf_mex_destroy(&c32_u);
}

static void c32_f_sf_marshallIn(void *chartInstanceVoid, const mxArray
  *c32_mxArrayInData, const char_T *c32_varName, void *c32_outData)
{
  const mxArray *c32_b_message;
  const char_T *c32_identifier;
  emlrtMsgIdentifier c32_thisId;
  uint8_T c32_y[26];
  int32_T c32_i26;
  SFc32_LessonI_startInstanceStruct *chartInstance;
  chartInstance = (SFc32_LessonI_startInstanceStruct *)chartInstanceVoid;
  c32_b_message = sf_mex_dup(c32_mxArrayInData);
  c32_identifier = c32_varName;
  c32_thisId.fIdentifier = c32_identifier;
  c32_thisId.fParent = NULL;
  c32_k_emlrt_marshallIn(chartInstance, sf_mex_dup(c32_b_message), &c32_thisId,
    c32_y);
  sf_mex_destroy(&c32_b_message);
  for (c32_i26 = 0; c32_i26 < 26; c32_i26++) {
    (*(uint8_T (*)[26])c32_outData)[c32_i26] = c32_y[c32_i26];
  }

  sf_mex_destroy(&c32_mxArrayInData);
}

static const mxArray *c32_h_sf_marshallOut(void *chartInstanceVoid, void
  *c32_inData)
{
  const mxArray *c32_mxArrayOutData = NULL;
  int32_T c32_i27;
  uint8_T c32_u[27];
  const mxArray *c32_y = NULL;
  SFc32_LessonI_startInstanceStruct *chartInstance;
  chartInstance = (SFc32_LessonI_startInstanceStruct *)chartInstanceVoid;
  c32_mxArrayOutData = NULL;
  for (c32_i27 = 0; c32_i27 < 27; c32_i27++) {
    c32_u[c32_i27] = (*(uint8_T (*)[27])c32_inData)[c32_i27];
  }

  c32_y = NULL;
  sf_mex_assign(&c32_y, sf_mex_create("y", c32_u, 3, 0U, 1U, 0U, 2, 1, 27),
                false);
  sf_mex_assign(&c32_mxArrayOutData, c32_y, false);
  return c32_mxArrayOutData;
}

static void c32_l_emlrt_marshallIn(SFc32_LessonI_startInstanceStruct
  *chartInstance, const mxArray *c32_u, const emlrtMsgIdentifier *c32_parentId,
  uint8_T c32_y[27])
{
  uint8_T c32_uv6[27];
  int32_T c32_i28;
  (void)chartInstance;
  sf_mex_import(c32_parentId, sf_mex_dup(c32_u), c32_uv6, 1, 3, 0U, 1, 0U, 2, 1,
                27);
  for (c32_i28 = 0; c32_i28 < 27; c32_i28++) {
    c32_y[c32_i28] = c32_uv6[c32_i28];
  }

  sf_mex_destroy(&c32_u);
}

static void c32_g_sf_marshallIn(void *chartInstanceVoid, const mxArray
  *c32_mxArrayInData, const char_T *c32_varName, void *c32_outData)
{
  const mxArray *c32_b_message;
  const char_T *c32_identifier;
  emlrtMsgIdentifier c32_thisId;
  uint8_T c32_y[27];
  int32_T c32_i29;
  SFc32_LessonI_startInstanceStruct *chartInstance;
  chartInstance = (SFc32_LessonI_startInstanceStruct *)chartInstanceVoid;
  c32_b_message = sf_mex_dup(c32_mxArrayInData);
  c32_identifier = c32_varName;
  c32_thisId.fIdentifier = c32_identifier;
  c32_thisId.fParent = NULL;
  c32_l_emlrt_marshallIn(chartInstance, sf_mex_dup(c32_b_message), &c32_thisId,
    c32_y);
  sf_mex_destroy(&c32_b_message);
  for (c32_i29 = 0; c32_i29 < 27; c32_i29++) {
    (*(uint8_T (*)[27])c32_outData)[c32_i29] = c32_y[c32_i29];
  }

  sf_mex_destroy(&c32_mxArrayInData);
}

static const mxArray *c32_i_sf_marshallOut(void *chartInstanceVoid, void
  *c32_inData)
{
  const mxArray *c32_mxArrayOutData = NULL;
  int32_T c32_i30;
  uint8_T c32_u[6];
  const mxArray *c32_y = NULL;
  SFc32_LessonI_startInstanceStruct *chartInstance;
  chartInstance = (SFc32_LessonI_startInstanceStruct *)chartInstanceVoid;
  c32_mxArrayOutData = NULL;
  for (c32_i30 = 0; c32_i30 < 6; c32_i30++) {
    c32_u[c32_i30] = (*(uint8_T (*)[6])c32_inData)[c32_i30];
  }

  c32_y = NULL;
  sf_mex_assign(&c32_y, sf_mex_create("y", c32_u, 3, 0U, 1U, 0U, 2, 1, 6), false);
  sf_mex_assign(&c32_mxArrayOutData, c32_y, false);
  return c32_mxArrayOutData;
}

static void c32_m_emlrt_marshallIn(SFc32_LessonI_startInstanceStruct
  *chartInstance, const mxArray *c32_u, const emlrtMsgIdentifier *c32_parentId,
  uint8_T c32_y[6])
{
  uint8_T c32_uv7[6];
  int32_T c32_i31;
  (void)chartInstance;
  sf_mex_import(c32_parentId, sf_mex_dup(c32_u), c32_uv7, 1, 3, 0U, 1, 0U, 2, 1,
                6);
  for (c32_i31 = 0; c32_i31 < 6; c32_i31++) {
    c32_y[c32_i31] = c32_uv7[c32_i31];
  }

  sf_mex_destroy(&c32_u);
}

static void c32_h_sf_marshallIn(void *chartInstanceVoid, const mxArray
  *c32_mxArrayInData, const char_T *c32_varName, void *c32_outData)
{
  const mxArray *c32_positions;
  const char_T *c32_identifier;
  emlrtMsgIdentifier c32_thisId;
  uint8_T c32_y[6];
  int32_T c32_i32;
  SFc32_LessonI_startInstanceStruct *chartInstance;
  chartInstance = (SFc32_LessonI_startInstanceStruct *)chartInstanceVoid;
  c32_positions = sf_mex_dup(c32_mxArrayInData);
  c32_identifier = c32_varName;
  c32_thisId.fIdentifier = c32_identifier;
  c32_thisId.fParent = NULL;
  c32_m_emlrt_marshallIn(chartInstance, sf_mex_dup(c32_positions), &c32_thisId,
    c32_y);
  sf_mex_destroy(&c32_positions);
  for (c32_i32 = 0; c32_i32 < 6; c32_i32++) {
    (*(uint8_T (*)[6])c32_outData)[c32_i32] = c32_y[c32_i32];
  }

  sf_mex_destroy(&c32_mxArrayInData);
}

static const mxArray *c32_j_sf_marshallOut(void *chartInstanceVoid, void
  *c32_inData)
{
  const mxArray *c32_mxArrayOutData;
  c32_Player c32_u;
  const mxArray *c32_y = NULL;
  int8_T c32_b_u;
  const mxArray *c32_b_y = NULL;
  int8_T c32_c_u;
  const mxArray *c32_c_y = NULL;
  int16_T c32_d_u;
  const mxArray *c32_d_y = NULL;
  uint8_T c32_e_u;
  const mxArray *c32_e_y = NULL;
  uint8_T c32_f_u;
  const mxArray *c32_f_y = NULL;
  uint8_T c32_g_u;
  const mxArray *c32_g_y = NULL;
  SFc32_LessonI_startInstanceStruct *chartInstance;
  chartInstance = (SFc32_LessonI_startInstanceStruct *)chartInstanceVoid;
  c32_mxArrayOutData = NULL;
  c32_mxArrayOutData = NULL;
  c32_u = *(c32_Player *)c32_inData;
  c32_y = NULL;
  sf_mex_assign(&c32_y, sf_mex_createstruct("structure", 2, 1, 1), false);
  c32_b_u = c32_u.x;
  c32_b_y = NULL;
  sf_mex_assign(&c32_b_y, sf_mex_create("y", &c32_b_u, 2, 0U, 0U, 0U, 0), false);
  sf_mex_addfield(c32_y, c32_b_y, "x", "x", 0);
  c32_c_u = c32_u.y;
  c32_c_y = NULL;
  sf_mex_assign(&c32_c_y, sf_mex_create("y", &c32_c_u, 2, 0U, 0U, 0U, 0), false);
  sf_mex_addfield(c32_y, c32_c_y, "y", "y", 0);
  c32_d_u = c32_u.orientation;
  c32_d_y = NULL;
  sf_mex_assign(&c32_d_y, sf_mex_create("y", &c32_d_u, 4, 0U, 0U, 0U, 0), false);
  sf_mex_addfield(c32_y, c32_d_y, "orientation", "orientation", 0);
  c32_e_u = c32_u.color;
  c32_e_y = NULL;
  sf_mex_assign(&c32_e_y, sf_mex_create("y", &c32_e_u, 3, 0U, 0U, 0U, 0), false);
  sf_mex_addfield(c32_y, c32_e_y, "color", "color", 0);
  c32_f_u = c32_u.position;
  c32_f_y = NULL;
  sf_mex_assign(&c32_f_y, sf_mex_create("y", &c32_f_u, 3, 0U, 0U, 0U, 0), false);
  sf_mex_addfield(c32_y, c32_f_y, "position", "position", 0);
  c32_g_u = c32_u.valid;
  c32_g_y = NULL;
  sf_mex_assign(&c32_g_y, sf_mex_create("y", &c32_g_u, 3, 0U, 0U, 0U, 0), false);
  sf_mex_addfield(c32_y, c32_g_y, "valid", "valid", 0);
  sf_mex_assign(&c32_mxArrayOutData, c32_y, false);
  return c32_mxArrayOutData;
}

static c32_Player c32_n_emlrt_marshallIn(SFc32_LessonI_startInstanceStruct
  *chartInstance, const mxArray *c32_u, const emlrtMsgIdentifier *c32_parentId)
{
  c32_Player c32_y;
  emlrtMsgIdentifier c32_thisId;
  static const char * c32_fieldNames[6] = { "x", "y", "orientation", "color",
    "position", "valid" };

  c32_thisId.fParent = c32_parentId;
  sf_mex_check_struct(c32_parentId, c32_u, 6, c32_fieldNames, 0U, NULL);
  c32_thisId.fIdentifier = "x";
  c32_y.x = c32_e_emlrt_marshallIn(chartInstance, sf_mex_dup(sf_mex_getfield
    (c32_u, "x", "x", 0)), &c32_thisId);
  c32_thisId.fIdentifier = "y";
  c32_y.y = c32_e_emlrt_marshallIn(chartInstance, sf_mex_dup(sf_mex_getfield
    (c32_u, "y", "y", 0)), &c32_thisId);
  c32_thisId.fIdentifier = "orientation";
  c32_y.orientation = c32_f_emlrt_marshallIn(chartInstance, sf_mex_dup
    (sf_mex_getfield(c32_u, "orientation", "orientation", 0)), &c32_thisId);
  c32_thisId.fIdentifier = "color";
  c32_y.color = c32_b_emlrt_marshallIn(chartInstance, sf_mex_dup(sf_mex_getfield
    (c32_u, "color", "color", 0)), &c32_thisId);
  c32_thisId.fIdentifier = "position";
  c32_y.position = c32_b_emlrt_marshallIn(chartInstance, sf_mex_dup
    (sf_mex_getfield(c32_u, "position", "position", 0)), &c32_thisId);
  c32_thisId.fIdentifier = "valid";
  c32_y.valid = c32_b_emlrt_marshallIn(chartInstance, sf_mex_dup(sf_mex_getfield
    (c32_u, "valid", "valid", 0)), &c32_thisId);
  sf_mex_destroy(&c32_u);
  return c32_y;
}

static void c32_i_sf_marshallIn(void *chartInstanceVoid, const mxArray
  *c32_mxArrayInData, const char_T *c32_varName, void *c32_outData)
{
  const mxArray *c32_tempEmpty;
  const char_T *c32_identifier;
  emlrtMsgIdentifier c32_thisId;
  c32_Player c32_y;
  SFc32_LessonI_startInstanceStruct *chartInstance;
  chartInstance = (SFc32_LessonI_startInstanceStruct *)chartInstanceVoid;
  c32_tempEmpty = sf_mex_dup(c32_mxArrayInData);
  c32_identifier = c32_varName;
  c32_thisId.fIdentifier = c32_identifier;
  c32_thisId.fParent = NULL;
  c32_y = c32_n_emlrt_marshallIn(chartInstance, sf_mex_dup(c32_tempEmpty),
    &c32_thisId);
  sf_mex_destroy(&c32_tempEmpty);
  *(c32_Player *)c32_outData = c32_y;
  sf_mex_destroy(&c32_mxArrayInData);
}

const mxArray *sf_c32_LessonI_start_get_eml_resolved_functions_info(void)
{
  const mxArray *c32_nameCaptureInfo = NULL;
  c32_nameCaptureInfo = NULL;
  sf_mex_assign(&c32_nameCaptureInfo, sf_mex_create("nameCaptureInfo", NULL, 0,
    0U, 1U, 0U, 2, 0, 1), false);
  return c32_nameCaptureInfo;
}

static int8_T c32_typecast(SFc32_LessonI_startInstanceStruct *chartInstance,
  uint8_T c32_x)
{
  int8_T c32_y;
  (void)chartInstance;
  memcpy(&c32_y, &c32_x, (size_t)1 * sizeof(int8_T));
  return c32_y;
}

static const mxArray *c32_k_sf_marshallOut(void *chartInstanceVoid, void
  *c32_inData)
{
  const mxArray *c32_mxArrayOutData = NULL;
  int32_T c32_u;
  const mxArray *c32_y = NULL;
  SFc32_LessonI_startInstanceStruct *chartInstance;
  chartInstance = (SFc32_LessonI_startInstanceStruct *)chartInstanceVoid;
  c32_mxArrayOutData = NULL;
  c32_u = *(int32_T *)c32_inData;
  c32_y = NULL;
  sf_mex_assign(&c32_y, sf_mex_create("y", &c32_u, 6, 0U, 0U, 0U, 0), false);
  sf_mex_assign(&c32_mxArrayOutData, c32_y, false);
  return c32_mxArrayOutData;
}

static int32_T c32_o_emlrt_marshallIn(SFc32_LessonI_startInstanceStruct
  *chartInstance, const mxArray *c32_u, const emlrtMsgIdentifier *c32_parentId)
{
  int32_T c32_y;
  int32_T c32_i33;
  (void)chartInstance;
  sf_mex_import(c32_parentId, sf_mex_dup(c32_u), &c32_i33, 1, 6, 0U, 0, 0U, 0);
  c32_y = c32_i33;
  sf_mex_destroy(&c32_u);
  return c32_y;
}

static void c32_j_sf_marshallIn(void *chartInstanceVoid, const mxArray
  *c32_mxArrayInData, const char_T *c32_varName, void *c32_outData)
{
  const mxArray *c32_b_sfEvent;
  const char_T *c32_identifier;
  emlrtMsgIdentifier c32_thisId;
  int32_T c32_y;
  SFc32_LessonI_startInstanceStruct *chartInstance;
  chartInstance = (SFc32_LessonI_startInstanceStruct *)chartInstanceVoid;
  c32_b_sfEvent = sf_mex_dup(c32_mxArrayInData);
  c32_identifier = c32_varName;
  c32_thisId.fIdentifier = c32_identifier;
  c32_thisId.fParent = NULL;
  c32_y = c32_o_emlrt_marshallIn(chartInstance, sf_mex_dup(c32_b_sfEvent),
    &c32_thisId);
  sf_mex_destroy(&c32_b_sfEvent);
  *(int32_T *)c32_outData = c32_y;
  sf_mex_destroy(&c32_mxArrayInData);
}

static const mxArray *c32_Ball_bus_io(void *chartInstanceVoid, void *c32_pData)
{
  const mxArray *c32_mxVal = NULL;
  c32_Ball c32_tmp;
  SFc32_LessonI_startInstanceStruct *chartInstance;
  chartInstance = (SFc32_LessonI_startInstanceStruct *)chartInstanceVoid;
  c32_mxVal = NULL;
  c32_tmp.x = *(int8_T *)&((char_T *)(c32_Ball *)c32_pData)[0];
  c32_tmp.y = *(int8_T *)&((char_T *)(c32_Ball *)c32_pData)[1];
  c32_tmp.valid = *(uint8_T *)&((char_T *)(c32_Ball *)c32_pData)[2];
  sf_mex_assign(&c32_mxVal, c32_c_sf_marshallOut(chartInstance, &c32_tmp), false);
  return c32_mxVal;
}

static const mxArray *c32_players_bus_io(void *chartInstanceVoid, void
  *c32_pData)
{
  const mxArray *c32_mxVal = NULL;
  int32_T c32_i34;
  c32_Player c32_tmp[6];
  SFc32_LessonI_startInstanceStruct *chartInstance;
  chartInstance = (SFc32_LessonI_startInstanceStruct *)chartInstanceVoid;
  c32_mxVal = NULL;
  for (c32_i34 = 0; c32_i34 < 6; c32_i34++) {
    c32_tmp[c32_i34].x = *(int8_T *)&((char_T *)(c32_Player *)&((char_T *)
      (c32_Player (*)[6])c32_pData)[8 * c32_i34])[0];
    c32_tmp[c32_i34].y = *(int8_T *)&((char_T *)(c32_Player *)&((char_T *)
      (c32_Player (*)[6])c32_pData)[8 * c32_i34])[1];
    c32_tmp[c32_i34].orientation = *(int16_T *)&((char_T *)(c32_Player *)
      &((char_T *)(c32_Player (*)[6])c32_pData)[8 * c32_i34])[2];
    c32_tmp[c32_i34].color = *(uint8_T *)&((char_T *)(c32_Player *)&((char_T *)
      (c32_Player (*)[6])c32_pData)[8 * c32_i34])[4];
    c32_tmp[c32_i34].position = *(uint8_T *)&((char_T *)(c32_Player *)&((char_T *)
      (c32_Player (*)[6])c32_pData)[8 * c32_i34])[5];
    c32_tmp[c32_i34].valid = *(uint8_T *)&((char_T *)(c32_Player *)&((char_T *)
      (c32_Player (*)[6])c32_pData)[8 * c32_i34])[6];
  }

  sf_mex_assign(&c32_mxVal, c32_b_sf_marshallOut(chartInstance, c32_tmp), false);
  return c32_mxVal;
}

static void init_dsm_address_info(SFc32_LessonI_startInstanceStruct
  *chartInstance)
{
  (void)chartInstance;
}

static void init_simulink_io_address(SFc32_LessonI_startInstanceStruct
  *chartInstance)
{
  chartInstance->c32_message = (uint8_T (*)[31])ssGetInputPortSignal_wrapper
    (chartInstance->S, 0);
  chartInstance->c32_b_Ball = (c32_Ball *)ssGetOutputPortSignal_wrapper
    (chartInstance->S, 1);
  chartInstance->c32_players = (c32_Player (*)[6])ssGetOutputPortSignal_wrapper
    (chartInstance->S, 2);
  chartInstance->c32_gameOn = (uint8_T *)ssGetOutputPortSignal_wrapper
    (chartInstance->S, 3);
}

/* SFunction Glue Code */
#ifdef utFree
#undef utFree
#endif

#ifdef utMalloc
#undef utMalloc
#endif

#ifdef __cplusplus

extern "C" void *utMalloc(size_t size);
extern "C" void utFree(void*);

#else

extern void *utMalloc(size_t size);
extern void utFree(void*);

#endif

void sf_c32_LessonI_start_get_check_sum(mxArray *plhs[])
{
  ((real_T *)mxGetPr((plhs[0])))[0] = (real_T)(496924623U);
  ((real_T *)mxGetPr((plhs[0])))[1] = (real_T)(214315009U);
  ((real_T *)mxGetPr((plhs[0])))[2] = (real_T)(1293669646U);
  ((real_T *)mxGetPr((plhs[0])))[3] = (real_T)(4138820961U);
}

mxArray* sf_c32_LessonI_start_get_post_codegen_info(void);
mxArray *sf_c32_LessonI_start_get_autoinheritance_info(void)
{
  const char *autoinheritanceFields[] = { "checksum", "inputs", "parameters",
    "outputs", "locals", "postCodegenInfo" };

  mxArray *mxAutoinheritanceInfo = mxCreateStructMatrix(1, 1, sizeof
    (autoinheritanceFields)/sizeof(autoinheritanceFields[0]),
    autoinheritanceFields);

  {
    mxArray *mxChecksum = mxCreateString("Jy2Iko6tMZGMZeDXqZSACD");
    mxSetField(mxAutoinheritanceInfo,0,"checksum",mxChecksum);
  }

  {
    const char *dataFields[] = { "size", "type", "complexity" };

    mxArray *mxData = mxCreateStructMatrix(1,1,3,dataFields);

    {
      mxArray *mxSize = mxCreateDoubleMatrix(1,2,mxREAL);
      double *pr = mxGetPr(mxSize);
      pr[0] = (double)(1);
      pr[1] = (double)(31);
      mxSetField(mxData,0,"size",mxSize);
    }

    {
      const char *typeFields[] = { "base", "fixpt", "isFixedPointType" };

      mxArray *mxType = mxCreateStructMatrix(1,1,sizeof(typeFields)/sizeof
        (typeFields[0]),typeFields);
      mxSetField(mxType,0,"base",mxCreateDoubleScalar(3));
      mxSetField(mxType,0,"fixpt",mxCreateDoubleMatrix(0,0,mxREAL));
      mxSetField(mxType,0,"isFixedPointType",mxCreateDoubleScalar(0));
      mxSetField(mxData,0,"type",mxType);
    }

    mxSetField(mxData,0,"complexity",mxCreateDoubleScalar(0));
    mxSetField(mxAutoinheritanceInfo,0,"inputs",mxData);
  }

  {
    mxSetField(mxAutoinheritanceInfo,0,"parameters",mxCreateDoubleMatrix(0,0,
                mxREAL));
  }

  {
    const char *dataFields[] = { "size", "type", "complexity" };

    mxArray *mxData = mxCreateStructMatrix(1,3,3,dataFields);

    {
      mxArray *mxSize = mxCreateDoubleMatrix(1,2,mxREAL);
      double *pr = mxGetPr(mxSize);
      pr[0] = (double)(1);
      pr[1] = (double)(1);
      mxSetField(mxData,0,"size",mxSize);
    }

    {
      const char *typeFields[] = { "base", "fixpt", "isFixedPointType" };

      mxArray *mxType = mxCreateStructMatrix(1,1,sizeof(typeFields)/sizeof
        (typeFields[0]),typeFields);
      mxSetField(mxType,0,"base",mxCreateDoubleScalar(13));
      mxSetField(mxType,0,"fixpt",mxCreateDoubleMatrix(0,0,mxREAL));
      mxSetField(mxType,0,"isFixedPointType",mxCreateDoubleScalar(0));
      mxSetField(mxData,0,"type",mxType);
    }

    mxSetField(mxData,0,"complexity",mxCreateDoubleScalar(0));

    {
      mxArray *mxSize = mxCreateDoubleMatrix(1,2,mxREAL);
      double *pr = mxGetPr(mxSize);
      pr[0] = (double)(6);
      pr[1] = (double)(1);
      mxSetField(mxData,1,"size",mxSize);
    }

    {
      const char *typeFields[] = { "base", "fixpt", "isFixedPointType" };

      mxArray *mxType = mxCreateStructMatrix(1,1,sizeof(typeFields)/sizeof
        (typeFields[0]),typeFields);
      mxSetField(mxType,0,"base",mxCreateDoubleScalar(13));
      mxSetField(mxType,0,"fixpt",mxCreateDoubleMatrix(0,0,mxREAL));
      mxSetField(mxType,0,"isFixedPointType",mxCreateDoubleScalar(0));
      mxSetField(mxData,1,"type",mxType);
    }

    mxSetField(mxData,1,"complexity",mxCreateDoubleScalar(0));

    {
      mxArray *mxSize = mxCreateDoubleMatrix(1,2,mxREAL);
      double *pr = mxGetPr(mxSize);
      pr[0] = (double)(1);
      pr[1] = (double)(1);
      mxSetField(mxData,2,"size",mxSize);
    }

    {
      const char *typeFields[] = { "base", "fixpt", "isFixedPointType" };

      mxArray *mxType = mxCreateStructMatrix(1,1,sizeof(typeFields)/sizeof
        (typeFields[0]),typeFields);
      mxSetField(mxType,0,"base",mxCreateDoubleScalar(3));
      mxSetField(mxType,0,"fixpt",mxCreateDoubleMatrix(0,0,mxREAL));
      mxSetField(mxType,0,"isFixedPointType",mxCreateDoubleScalar(0));
      mxSetField(mxData,2,"type",mxType);
    }

    mxSetField(mxData,2,"complexity",mxCreateDoubleScalar(0));
    mxSetField(mxAutoinheritanceInfo,0,"outputs",mxData);
  }

  {
    mxSetField(mxAutoinheritanceInfo,0,"locals",mxCreateDoubleMatrix(0,0,mxREAL));
  }

  {
    mxArray* mxPostCodegenInfo = sf_c32_LessonI_start_get_post_codegen_info();
    mxSetField(mxAutoinheritanceInfo,0,"postCodegenInfo",mxPostCodegenInfo);
  }

  return(mxAutoinheritanceInfo);
}

mxArray *sf_c32_LessonI_start_third_party_uses_info(void)
{
  mxArray * mxcell3p = mxCreateCellMatrix(1,0);
  return(mxcell3p);
}

mxArray *sf_c32_LessonI_start_jit_fallback_info(void)
{
  const char *infoFields[] = { "fallbackType", "fallbackReason",
    "hiddenFallbackType", "hiddenFallbackReason", "incompatibleSymbol" };

  mxArray *mxInfo = mxCreateStructMatrix(1, 1, 5, infoFields);
  mxArray *fallbackType = mxCreateString("late");
  mxArray *fallbackReason = mxCreateString("ir_function_calls");
  mxArray *hiddenFallbackType = mxCreateString("");
  mxArray *hiddenFallbackReason = mxCreateString("");
  mxArray *incompatibleSymbol = mxCreateString("memcpy");
  mxSetField(mxInfo, 0, infoFields[0], fallbackType);
  mxSetField(mxInfo, 0, infoFields[1], fallbackReason);
  mxSetField(mxInfo, 0, infoFields[2], hiddenFallbackType);
  mxSetField(mxInfo, 0, infoFields[3], hiddenFallbackReason);
  mxSetField(mxInfo, 0, infoFields[4], incompatibleSymbol);
  return mxInfo;
}

mxArray *sf_c32_LessonI_start_updateBuildInfo_args_info(void)
{
  mxArray *mxBIArgs = mxCreateCellMatrix(1,0);
  return mxBIArgs;
}

mxArray* sf_c32_LessonI_start_get_post_codegen_info(void)
{
  const char* fieldNames[] = { "exportedFunctionsUsedByThisChart",
    "exportedFunctionsChecksum" };

  mwSize dims[2] = { 1, 1 };

  mxArray* mxPostCodegenInfo = mxCreateStructArray(2, dims, sizeof(fieldNames)/
    sizeof(fieldNames[0]), fieldNames);

  {
    mxArray* mxExportedFunctionsChecksum = mxCreateString("");
    mwSize exp_dims[2] = { 0, 1 };

    mxArray* mxExportedFunctionsUsedByThisChart = mxCreateCellArray(2, exp_dims);
    mxSetField(mxPostCodegenInfo, 0, "exportedFunctionsUsedByThisChart",
               mxExportedFunctionsUsedByThisChart);
    mxSetField(mxPostCodegenInfo, 0, "exportedFunctionsChecksum",
               mxExportedFunctionsChecksum);
  }

  return mxPostCodegenInfo;
}

static const mxArray *sf_get_sim_state_info_c32_LessonI_start(void)
{
  const char *infoFields[] = { "chartChecksum", "varInfo" };

  mxArray *mxInfo = mxCreateStructMatrix(1, 1, 2, infoFields);
  const char *infoEncStr[] = {
    "100 S1x4'type','srcId','name','auxInfo'{{M[1],M[5],T\"Ball\",},{M[1],M[9],T\"gameOn\",},{M[1],M[8],T\"players\",},{M[8],M[0],T\"is_active_c32_LessonI_start\",}}"
  };

  mxArray *mxVarInfo = sf_mex_decode_encoded_mx_struct_array(infoEncStr, 4, 10);
  mxArray *mxChecksum = mxCreateDoubleMatrix(1, 4, mxREAL);
  sf_c32_LessonI_start_get_check_sum(&mxChecksum);
  mxSetField(mxInfo, 0, infoFields[0], mxChecksum);
  mxSetField(mxInfo, 0, infoFields[1], mxVarInfo);
  return mxInfo;
}

static void chart_debug_initialization(SimStruct *S, unsigned int
  fullDebuggerInitialization)
{
  if (!sim_mode_is_rtw_gen(S)) {
    SFc32_LessonI_startInstanceStruct *chartInstance;
    ChartRunTimeInfo * crtInfo = (ChartRunTimeInfo *)(ssGetUserData(S));
    ChartInfoStruct * chartInfo = (ChartInfoStruct *)(crtInfo->instanceInfo);
    chartInstance = (SFc32_LessonI_startInstanceStruct *)
      chartInfo->chartInstance;
    if (ssIsFirstInitCond(S) && fullDebuggerInitialization==1) {
      /* do this only if simulation is starting */
      {
        unsigned int chartAlreadyPresent;
        chartAlreadyPresent = sf_debug_initialize_chart
          (sfGlobalDebugInstanceStruct,
           _LessonI_startMachineNumber_,
           32,
           1,
           1,
           0,
           4,
           0,
           0,
           0,
           0,
           0,
           &(chartInstance->chartNumber),
           &(chartInstance->instanceNumber),
           (void *)S);

        /* Each instance must initialize its own list of scripts */
        init_script_number_translation(_LessonI_startMachineNumber_,
          chartInstance->chartNumber,chartInstance->instanceNumber);
        if (chartAlreadyPresent==0) {
          /* this is the first instance */
          sf_debug_set_chart_disable_implicit_casting
            (sfGlobalDebugInstanceStruct,_LessonI_startMachineNumber_,
             chartInstance->chartNumber,1);
          sf_debug_set_chart_event_thresholds(sfGlobalDebugInstanceStruct,
            _LessonI_startMachineNumber_,
            chartInstance->chartNumber,
            0,
            0,
            0);
          _SFD_SET_DATA_PROPS(0,1,1,0,"message");
          _SFD_SET_DATA_PROPS(1,2,0,1,"Ball");
          _SFD_SET_DATA_PROPS(2,2,0,1,"players");
          _SFD_SET_DATA_PROPS(3,2,0,1,"gameOn");
          _SFD_STATE_INFO(0,0,2);
          _SFD_CH_SUBSTATE_COUNT(0);
          _SFD_CH_SUBSTATE_DECOMP(0);
        }

        _SFD_CV_INIT_CHART(0,0,0,0);

        {
          _SFD_CV_INIT_STATE(0,0,0,0,0,0,NULL,NULL);
        }

        _SFD_CV_INIT_TRANS(0,0,NULL,NULL,0,NULL);

        /* Initialization of MATLAB Function Model Coverage */
        _SFD_CV_INIT_EML(0,1,1,0,0,0,0,0,1,0,0,0);
        _SFD_CV_INIT_EML_FCN(0,0,"eML_blk_kernel",0,-1,885);
        _SFD_CV_INIT_EML_FOR(0,1,0,573,584,885);

        {
          unsigned int dimVector[2];
          dimVector[0]= 1;
          dimVector[1]= 31;
          _SFD_SET_DATA_COMPILED_PROPS(0,SF_UINT8,2,&(dimVector[0]),0,0,0,0.0,
            1.0,0,0,(MexFcnForType)c32_d_sf_marshallOut,(MexInFcnForType)NULL);
        }

        _SFD_SET_DATA_COMPILED_PROPS(1,SF_STRUCT,0,NULL,0,0,0,0.0,1.0,0,0,
          (MexFcnForType)c32_Ball_bus_io,(MexInFcnForType)NULL);

        {
          unsigned int dimVector[1];
          dimVector[0]= 6;
          _SFD_SET_DATA_COMPILED_PROPS(2,SF_STRUCT,1,&(dimVector[0]),0,0,0,0.0,
            1.0,0,0,(MexFcnForType)c32_players_bus_io,(MexInFcnForType)NULL);
        }

        _SFD_SET_DATA_COMPILED_PROPS(3,SF_UINT8,0,NULL,0,0,0,0.0,1.0,0,0,
          (MexFcnForType)c32_sf_marshallOut,(MexInFcnForType)c32_sf_marshallIn);
      }
    } else {
      sf_debug_reset_current_state_configuration(sfGlobalDebugInstanceStruct,
        _LessonI_startMachineNumber_,chartInstance->chartNumber,
        chartInstance->instanceNumber);
    }
  }
}

static void chart_debug_initialize_data_addresses(SimStruct *S)
{
  if (!sim_mode_is_rtw_gen(S)) {
    SFc32_LessonI_startInstanceStruct *chartInstance;
    ChartRunTimeInfo * crtInfo = (ChartRunTimeInfo *)(ssGetUserData(S));
    ChartInfoStruct * chartInfo = (ChartInfoStruct *)(crtInfo->instanceInfo);
    chartInstance = (SFc32_LessonI_startInstanceStruct *)
      chartInfo->chartInstance;
    if (ssIsFirstInitCond(S)) {
      /* do this only if simulation is starting and after we know the addresses of all data */
      {
        _SFD_SET_DATA_VALUE_PTR(0U, *chartInstance->c32_message);
        _SFD_SET_DATA_VALUE_PTR(1U, chartInstance->c32_b_Ball);
        _SFD_SET_DATA_VALUE_PTR(2U, *chartInstance->c32_players);
        _SFD_SET_DATA_VALUE_PTR(3U, chartInstance->c32_gameOn);
      }
    }
  }
}

static const char* sf_get_instance_specialization(void)
{
  return "sELumGX5fhAQC4ek2lInCkD";
}

static void sf_opaque_initialize_c32_LessonI_start(void *chartInstanceVar)
{
  chart_debug_initialization(((SFc32_LessonI_startInstanceStruct*)
    chartInstanceVar)->S,0);
  initialize_params_c32_LessonI_start((SFc32_LessonI_startInstanceStruct*)
    chartInstanceVar);
  initialize_c32_LessonI_start((SFc32_LessonI_startInstanceStruct*)
    chartInstanceVar);
}

static void sf_opaque_enable_c32_LessonI_start(void *chartInstanceVar)
{
  enable_c32_LessonI_start((SFc32_LessonI_startInstanceStruct*) chartInstanceVar);
}

static void sf_opaque_disable_c32_LessonI_start(void *chartInstanceVar)
{
  disable_c32_LessonI_start((SFc32_LessonI_startInstanceStruct*)
    chartInstanceVar);
}

static void sf_opaque_gateway_c32_LessonI_start(void *chartInstanceVar)
{
  sf_gateway_c32_LessonI_start((SFc32_LessonI_startInstanceStruct*)
    chartInstanceVar);
}

static const mxArray* sf_opaque_get_sim_state_c32_LessonI_start(SimStruct* S)
{
  ChartRunTimeInfo * crtInfo = (ChartRunTimeInfo *)(ssGetUserData(S));
  ChartInfoStruct * chartInfo = (ChartInfoStruct *)(crtInfo->instanceInfo);
  return get_sim_state_c32_LessonI_start((SFc32_LessonI_startInstanceStruct*)
    chartInfo->chartInstance);         /* raw sim ctx */
}

static void sf_opaque_set_sim_state_c32_LessonI_start(SimStruct* S, const
  mxArray *st)
{
  ChartRunTimeInfo * crtInfo = (ChartRunTimeInfo *)(ssGetUserData(S));
  ChartInfoStruct * chartInfo = (ChartInfoStruct *)(crtInfo->instanceInfo);
  set_sim_state_c32_LessonI_start((SFc32_LessonI_startInstanceStruct*)
    chartInfo->chartInstance, st);
}

static void sf_opaque_terminate_c32_LessonI_start(void *chartInstanceVar)
{
  if (chartInstanceVar!=NULL) {
    SimStruct *S = ((SFc32_LessonI_startInstanceStruct*) chartInstanceVar)->S;
    ChartRunTimeInfo * crtInfo = (ChartRunTimeInfo *)(ssGetUserData(S));
    if (sim_mode_is_rtw_gen(S) || sim_mode_is_external(S)) {
      sf_clear_rtw_identifier(S);
      unload_LessonI_start_optimization_info();
    }

    finalize_c32_LessonI_start((SFc32_LessonI_startInstanceStruct*)
      chartInstanceVar);
    utFree(chartInstanceVar);
    if (crtInfo != NULL) {
      utFree(crtInfo);
    }

    ssSetUserData(S,NULL);
  }
}

static void sf_opaque_init_subchart_simstructs(void *chartInstanceVar)
{
  initSimStructsc32_LessonI_start((SFc32_LessonI_startInstanceStruct*)
    chartInstanceVar);
}

extern unsigned int sf_machine_global_initializer_called(void);
static void mdlProcessParameters_c32_LessonI_start(SimStruct *S)
{
  int i;
  for (i=0;i<ssGetNumRunTimeParams(S);i++) {
    if (ssGetSFcnParamTunable(S,i)) {
      ssUpdateDlgParamAsRunTimeParam(S,i);
    }
  }

  if (sf_machine_global_initializer_called()) {
    ChartRunTimeInfo * crtInfo = (ChartRunTimeInfo *)(ssGetUserData(S));
    ChartInfoStruct * chartInfo = (ChartInfoStruct *)(crtInfo->instanceInfo);
    initialize_params_c32_LessonI_start((SFc32_LessonI_startInstanceStruct*)
      (chartInfo->chartInstance));
  }
}

static void mdlSetWorkWidths_c32_LessonI_start(SimStruct *S)
{
  ssMdlUpdateIsEmpty(S, 1);
  if (sim_mode_is_rtw_gen(S) || sim_mode_is_external(S)) {
    mxArray *infoStruct = load_LessonI_start_optimization_info();
    int_T chartIsInlinable =
      (int_T)sf_is_chart_inlinable(sf_get_instance_specialization(),infoStruct,
      32);
    ssSetStateflowIsInlinable(S,chartIsInlinable);
    ssSetRTWCG(S,1);
    ssSetEnableFcnIsTrivial(S,1);
    ssSetDisableFcnIsTrivial(S,1);
    ssSetNotMultipleInlinable(S,sf_rtw_info_uint_prop
      (sf_get_instance_specialization(),infoStruct,32,
       "gatewayCannotBeInlinedMultipleTimes"));
    sf_update_buildInfo(sf_get_instance_specialization(),infoStruct,32);
    if (chartIsInlinable) {
      ssSetInputPortOptimOpts(S, 0, SS_REUSABLE_AND_LOCAL);
      sf_mark_chart_expressionable_inputs(S,sf_get_instance_specialization(),
        infoStruct,32,1);
      sf_mark_chart_reusable_outputs(S,sf_get_instance_specialization(),
        infoStruct,32,3);
    }

    {
      unsigned int outPortIdx;
      for (outPortIdx=1; outPortIdx<=3; ++outPortIdx) {
        ssSetOutputPortOptimizeInIR(S, outPortIdx, 1U);
      }
    }

    {
      unsigned int inPortIdx;
      for (inPortIdx=0; inPortIdx < 1; ++inPortIdx) {
        ssSetInputPortOptimizeInIR(S, inPortIdx, 1U);
      }
    }

    sf_set_rtw_dwork_info(S,sf_get_instance_specialization(),infoStruct,32);
    ssSetHasSubFunctions(S,!(chartIsInlinable));
  } else {
  }

  ssSetOptions(S,ssGetOptions(S)|SS_OPTION_WORKS_WITH_CODE_REUSE);
  ssSetChecksum0(S,(2473014197U));
  ssSetChecksum1(S,(1036485692U));
  ssSetChecksum2(S,(2484679956U));
  ssSetChecksum3(S,(2229932412U));
  ssSetmdlDerivatives(S, NULL);
  ssSetExplicitFCSSCtrl(S,1);
  ssSupportsMultipleExecInstances(S,1);
}

static void mdlRTW_c32_LessonI_start(SimStruct *S)
{
  if (sim_mode_is_rtw_gen(S)) {
    ssWriteRTWStrParam(S, "StateflowChartType", "Embedded MATLAB");
  }
}

static void mdlStart_c32_LessonI_start(SimStruct *S)
{
  SFc32_LessonI_startInstanceStruct *chartInstance;
  ChartRunTimeInfo * crtInfo = (ChartRunTimeInfo *)utMalloc(sizeof
    (ChartRunTimeInfo));
  chartInstance = (SFc32_LessonI_startInstanceStruct *)utMalloc(sizeof
    (SFc32_LessonI_startInstanceStruct));
  memset(chartInstance, 0, sizeof(SFc32_LessonI_startInstanceStruct));
  if (chartInstance==NULL) {
    sf_mex_error_message("Could not allocate memory for chart instance.");
  }

  chartInstance->chartInfo.chartInstance = chartInstance;
  chartInstance->chartInfo.isEMLChart = 1;
  chartInstance->chartInfo.chartInitialized = 0;
  chartInstance->chartInfo.sFunctionGateway =
    sf_opaque_gateway_c32_LessonI_start;
  chartInstance->chartInfo.initializeChart =
    sf_opaque_initialize_c32_LessonI_start;
  chartInstance->chartInfo.terminateChart =
    sf_opaque_terminate_c32_LessonI_start;
  chartInstance->chartInfo.enableChart = sf_opaque_enable_c32_LessonI_start;
  chartInstance->chartInfo.disableChart = sf_opaque_disable_c32_LessonI_start;
  chartInstance->chartInfo.getSimState =
    sf_opaque_get_sim_state_c32_LessonI_start;
  chartInstance->chartInfo.setSimState =
    sf_opaque_set_sim_state_c32_LessonI_start;
  chartInstance->chartInfo.getSimStateInfo =
    sf_get_sim_state_info_c32_LessonI_start;
  chartInstance->chartInfo.zeroCrossings = NULL;
  chartInstance->chartInfo.outputs = NULL;
  chartInstance->chartInfo.derivatives = NULL;
  chartInstance->chartInfo.mdlRTW = mdlRTW_c32_LessonI_start;
  chartInstance->chartInfo.mdlStart = mdlStart_c32_LessonI_start;
  chartInstance->chartInfo.mdlSetWorkWidths = mdlSetWorkWidths_c32_LessonI_start;
  chartInstance->chartInfo.extModeExec = NULL;
  chartInstance->chartInfo.restoreLastMajorStepConfiguration = NULL;
  chartInstance->chartInfo.restoreBeforeLastMajorStepConfiguration = NULL;
  chartInstance->chartInfo.storeCurrentConfiguration = NULL;
  chartInstance->chartInfo.callAtomicSubchartUserFcn = NULL;
  chartInstance->chartInfo.callAtomicSubchartAutoFcn = NULL;
  chartInstance->chartInfo.debugInstance = sfGlobalDebugInstanceStruct;
  chartInstance->S = S;
  crtInfo->isEnhancedMooreMachine = 0;
  crtInfo->checksum = SF_RUNTIME_INFO_CHECKSUM;
  crtInfo->fCheckOverflow = sf_runtime_overflow_check_is_on(S);
  crtInfo->instanceInfo = (&(chartInstance->chartInfo));
  crtInfo->isJITEnabled = false;
  crtInfo->compiledInfo = NULL;
  ssSetUserData(S,(void *)(crtInfo));  /* register the chart instance with simstruct */
  init_dsm_address_info(chartInstance);
  init_simulink_io_address(chartInstance);
  if (!sim_mode_is_rtw_gen(S)) {
  }

  chart_debug_initialization(S,1);
}

void c32_LessonI_start_method_dispatcher(SimStruct *S, int_T method, void *data)
{
  switch (method) {
   case SS_CALL_MDL_START:
    mdlStart_c32_LessonI_start(S);
    break;

   case SS_CALL_MDL_SET_WORK_WIDTHS:
    mdlSetWorkWidths_c32_LessonI_start(S);
    break;

   case SS_CALL_MDL_PROCESS_PARAMETERS:
    mdlProcessParameters_c32_LessonI_start(S);
    break;

   default:
    /* Unhandled method */
    sf_mex_error_message("Stateflow Internal Error:\n"
                         "Error calling c32_LessonI_start_method_dispatcher.\n"
                         "Can't handle method %d.\n", method);
    break;
  }
}

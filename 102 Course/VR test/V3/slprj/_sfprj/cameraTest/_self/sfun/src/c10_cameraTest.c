/* Include files */

#include <stddef.h>
#include "blas.h"
#include "cameraTest_sfun.h"
#include "c10_cameraTest.h"
#define CHARTINSTANCE_CHARTNUMBER      (chartInstance->chartNumber)
#define CHARTINSTANCE_INSTANCENUMBER   (chartInstance->instanceNumber)
#include "cameraTest_sfun_debug_macros.h"
#define _SF_MEX_LISTEN_FOR_CTRL_C(S)   sf_mex_listen_for_ctrl_c(sfGlobalDebugInstanceStruct,S);

/* Type Definitions */

/* Named Constants */
#define CALL_EVENT                     (-1)
#define c10_IN_NO_ACTIVE_CHILD         ((uint8_T)0U)
#define c10_IN_gameIsOn                ((uint8_T)1U)
#define c10_IN_notOn                   ((uint8_T)2U)

/* Variable Declarations */

/* Variable Definitions */
static real_T _sfTime_;
static const char * c10_debug_family_names[2] = { "nargin", "nargout" };

static const char * c10_b_debug_family_names[2] = { "nargin", "nargout" };

static const char * c10_c_debug_family_names[5] = { "position", "ii", "nargin",
  "nargout", "ready" };

static const char * c10_d_debug_family_names[2] = { "nargin", "nargout" };

static const char * c10_e_debug_family_names[3] = { "nargin", "nargout",
  "sf_internal_predicateOutput" };

static const char * c10_f_debug_family_names[3] = { "nargin", "nargout", "goal"
};

static const char * c10_g_debug_family_names[3] = { "nargin", "nargout",
  "sf_internal_predicateOutput" };

static const char * c10_h_debug_family_names[3] = { "nargin", "nargout",
  "sf_internal_predicateOutput" };

static const char * c10_i_debug_family_names[3] = { "nargin", "nargout",
  "sf_internal_predicateOutput" };

/* Function Declarations */
static void initialize_c10_cameraTest(SFc10_cameraTestInstanceStruct
  *chartInstance);
static void initialize_params_c10_cameraTest(SFc10_cameraTestInstanceStruct
  *chartInstance);
static void enable_c10_cameraTest(SFc10_cameraTestInstanceStruct *chartInstance);
static void disable_c10_cameraTest(SFc10_cameraTestInstanceStruct *chartInstance);
static void c10_update_debugger_state_c10_cameraTest
  (SFc10_cameraTestInstanceStruct *chartInstance);
static void ext_mode_exec_c10_cameraTest(SFc10_cameraTestInstanceStruct
  *chartInstance);
static const mxArray *get_sim_state_c10_cameraTest
  (SFc10_cameraTestInstanceStruct *chartInstance);
static void set_sim_state_c10_cameraTest(SFc10_cameraTestInstanceStruct
  *chartInstance, const mxArray *c10_st);
static void c10_set_sim_state_side_effects_c10_cameraTest
  (SFc10_cameraTestInstanceStruct *chartInstance);
static void finalize_c10_cameraTest(SFc10_cameraTestInstanceStruct
  *chartInstance);
static void sf_gateway_c10_cameraTest(SFc10_cameraTestInstanceStruct
  *chartInstance);
static void mdl_start_c10_cameraTest(SFc10_cameraTestInstanceStruct
  *chartInstance);
static void initSimStructsc10_cameraTest(SFc10_cameraTestInstanceStruct
  *chartInstance);
static void init_script_number_translation(uint32_T c10_machineNumber, uint32_T
  c10_chartNumber, uint32_T c10_instanceNumber);
static const mxArray *c10_sf_marshallOut(void *chartInstanceVoid, void
  *c10_inData);
static real_T c10_emlrt_marshallIn(SFc10_cameraTestInstanceStruct *chartInstance,
  const mxArray *c10_u, const emlrtMsgIdentifier *c10_parentId);
static void c10_sf_marshallIn(void *chartInstanceVoid, const mxArray
  *c10_mxArrayInData, const char_T *c10_varName, void *c10_outData);
static const mxArray *c10_b_sf_marshallOut(void *chartInstanceVoid, void
  *c10_inData);
static uint8_T c10_b_emlrt_marshallIn(SFc10_cameraTestInstanceStruct
  *chartInstance, const mxArray *c10_ready, const char_T *c10_identifier);
static uint8_T c10_c_emlrt_marshallIn(SFc10_cameraTestInstanceStruct
  *chartInstance, const mxArray *c10_u, const emlrtMsgIdentifier *c10_parentId);
static void c10_b_sf_marshallIn(void *chartInstanceVoid, const mxArray
  *c10_mxArrayInData, const char_T *c10_varName, void *c10_outData);
static const mxArray *c10_c_sf_marshallOut(void *chartInstanceVoid, void
  *c10_inData);
static void c10_d_emlrt_marshallIn(SFc10_cameraTestInstanceStruct *chartInstance,
  const mxArray *c10_u, const emlrtMsgIdentifier *c10_parentId, int8_T c10_y[6]);
static void c10_c_sf_marshallIn(void *chartInstanceVoid, const mxArray
  *c10_mxArrayInData, const char_T *c10_varName, void *c10_outData);
static const mxArray *c10_d_sf_marshallOut(void *chartInstanceVoid, void
  *c10_inData);
static boolean_T c10_e_emlrt_marshallIn(SFc10_cameraTestInstanceStruct
  *chartInstance, const mxArray *c10_u, const emlrtMsgIdentifier *c10_parentId);
static void c10_d_sf_marshallIn(void *chartInstanceVoid, const mxArray
  *c10_mxArrayInData, const char_T *c10_varName, void *c10_outData);
static void c10_info_helper(const mxArray **c10_info);
static const mxArray *c10_emlrt_marshallOut(const char * c10_u);
static const mxArray *c10_b_emlrt_marshallOut(const uint32_T c10_u);
static uint8_T c10_calcReady(SFc10_cameraTestInstanceStruct *chartInstance);
static uint8_T c10_calcGoal(SFc10_cameraTestInstanceStruct *chartInstance);
static const mxArray *c10_e_sf_marshallOut(void *chartInstanceVoid, void
  *c10_inData);
static int32_T c10_f_emlrt_marshallIn(SFc10_cameraTestInstanceStruct
  *chartInstance, const mxArray *c10_u, const emlrtMsgIdentifier *c10_parentId);
static void c10_e_sf_marshallIn(void *chartInstanceVoid, const mxArray
  *c10_mxArrayInData, const char_T *c10_varName, void *c10_outData);
static const mxArray *c10_players_bus_io(void *chartInstanceVoid, void
  *c10_pData);
static const mxArray *c10_f_sf_marshallOut(void *chartInstanceVoid, void
  *c10_inData);
static const mxArray *c10_ball_bus_io(void *chartInstanceVoid, void *c10_pData);
static const mxArray *c10_g_sf_marshallOut(void *chartInstanceVoid, void
  *c10_inData);
static void c10_g_emlrt_marshallIn(SFc10_cameraTestInstanceStruct *chartInstance,
  const mxArray *c10_b_dataWrittenToVector, const char_T *c10_identifier,
  boolean_T c10_y[4]);
static void c10_h_emlrt_marshallIn(SFc10_cameraTestInstanceStruct *chartInstance,
  const mxArray *c10_u, const emlrtMsgIdentifier *c10_parentId, boolean_T c10_y
  [4]);
static const mxArray *c10_i_emlrt_marshallIn(SFc10_cameraTestInstanceStruct
  *chartInstance, const mxArray *c10_b_setSimStateSideEffectsInfo, const char_T *
  c10_identifier);
static const mxArray *c10_j_emlrt_marshallIn(SFc10_cameraTestInstanceStruct
  *chartInstance, const mxArray *c10_u, const emlrtMsgIdentifier *c10_parentId);
static void c10_updateDataWrittenToVector(SFc10_cameraTestInstanceStruct
  *chartInstance, uint32_T c10_vectorIndex);
static void c10_errorIfDataNotWrittenToFcn(SFc10_cameraTestInstanceStruct
  *chartInstance, uint32_T c10_vectorIndex, uint32_T c10_dataNumber, uint32_T
  c10_ssIdOfSourceObject, int32_T c10_offsetInSourceObject, int32_T
  c10_lengthInSourceObject);
static void init_dsm_address_info(SFc10_cameraTestInstanceStruct *chartInstance);
static void init_simulink_io_address(SFc10_cameraTestInstanceStruct
  *chartInstance);

/* Function Definitions */
static void initialize_c10_cameraTest(SFc10_cameraTestInstanceStruct
  *chartInstance)
{
  *chartInstance->c10_sfEvent = CALL_EVENT;
  _sfTime_ = sf_get_time(chartInstance->S);
  chartInstance->c10_doSetSimStateSideEffects = 0U;
  chartInstance->c10_setSimStateSideEffectsInfo = NULL;
  *chartInstance->c10_is_active_c10_cameraTest = 0U;
  *chartInstance->c10_is_c10_cameraTest = c10_IN_NO_ACTIVE_CHILD;
}

static void initialize_params_c10_cameraTest(SFc10_cameraTestInstanceStruct
  *chartInstance)
{
  (void)chartInstance;
}

static void enable_c10_cameraTest(SFc10_cameraTestInstanceStruct *chartInstance)
{
  _sfTime_ = sf_get_time(chartInstance->S);
}

static void disable_c10_cameraTest(SFc10_cameraTestInstanceStruct *chartInstance)
{
  _sfTime_ = sf_get_time(chartInstance->S);
}

static void c10_update_debugger_state_c10_cameraTest
  (SFc10_cameraTestInstanceStruct *chartInstance)
{
  uint32_T c10_prevAniVal;
  c10_prevAniVal = _SFD_GET_ANIMATION();
  _SFD_SET_ANIMATION(0U);
  _SFD_SET_HONOR_BREAKPOINTS(0U);
  if (*chartInstance->c10_is_active_c10_cameraTest == 1U) {
    _SFD_CC_CALL(CHART_ACTIVE_TAG, 6U, *chartInstance->c10_sfEvent);
  }

  if (*chartInstance->c10_is_c10_cameraTest == c10_IN_notOn) {
    _SFD_CS_CALL(STATE_ACTIVE_TAG, 3U, *chartInstance->c10_sfEvent);
  } else {
    _SFD_CS_CALL(STATE_INACTIVE_TAG, 3U, *chartInstance->c10_sfEvent);
  }

  if (*chartInstance->c10_is_c10_cameraTest == c10_IN_gameIsOn) {
    _SFD_CS_CALL(STATE_ACTIVE_TAG, 2U, *chartInstance->c10_sfEvent);
  } else {
    _SFD_CS_CALL(STATE_INACTIVE_TAG, 2U, *chartInstance->c10_sfEvent);
  }

  _SFD_SET_ANIMATION(c10_prevAniVal);
  _SFD_SET_HONOR_BREAKPOINTS(1U);
  _SFD_ANIMATE();
}

static void ext_mode_exec_c10_cameraTest(SFc10_cameraTestInstanceStruct
  *chartInstance)
{
  c10_update_debugger_state_c10_cameraTest(chartInstance);
}

static const mxArray *get_sim_state_c10_cameraTest
  (SFc10_cameraTestInstanceStruct *chartInstance)
{
  const mxArray *c10_st;
  const mxArray *c10_y = NULL;
  uint8_T c10_hoistedGlobal;
  uint8_T c10_u;
  const mxArray *c10_b_y = NULL;
  uint8_T c10_b_hoistedGlobal;
  uint8_T c10_b_u;
  const mxArray *c10_c_y = NULL;
  uint8_T c10_c_hoistedGlobal;
  uint8_T c10_c_u;
  const mxArray *c10_d_y = NULL;
  uint8_T c10_d_hoistedGlobal;
  uint8_T c10_d_u;
  const mxArray *c10_e_y = NULL;
  int32_T c10_i0;
  boolean_T c10_e_u[4];
  const mxArray *c10_f_y = NULL;
  c10_st = NULL;
  c10_st = NULL;
  c10_y = NULL;
  sf_mex_assign(&c10_y, sf_mex_createcellmatrix(5, 1), false);
  c10_hoistedGlobal = *chartInstance->c10_gameOn;
  c10_u = c10_hoistedGlobal;
  c10_b_y = NULL;
  sf_mex_assign(&c10_b_y, sf_mex_create("y", &c10_u, 3, 0U, 0U, 0U, 0), false);
  sf_mex_setcell(c10_y, 0, c10_b_y);
  c10_b_hoistedGlobal = *chartInstance->c10_idleTicks;
  c10_b_u = c10_b_hoistedGlobal;
  c10_c_y = NULL;
  sf_mex_assign(&c10_c_y, sf_mex_create("y", &c10_b_u, 3, 0U, 0U, 0U, 0), false);
  sf_mex_setcell(c10_y, 1, c10_c_y);
  c10_c_hoistedGlobal = *chartInstance->c10_is_active_c10_cameraTest;
  c10_c_u = c10_c_hoistedGlobal;
  c10_d_y = NULL;
  sf_mex_assign(&c10_d_y, sf_mex_create("y", &c10_c_u, 3, 0U, 0U, 0U, 0), false);
  sf_mex_setcell(c10_y, 2, c10_d_y);
  c10_d_hoistedGlobal = *chartInstance->c10_is_c10_cameraTest;
  c10_d_u = c10_d_hoistedGlobal;
  c10_e_y = NULL;
  sf_mex_assign(&c10_e_y, sf_mex_create("y", &c10_d_u, 3, 0U, 0U, 0U, 0), false);
  sf_mex_setcell(c10_y, 3, c10_e_y);
  for (c10_i0 = 0; c10_i0 < 4; c10_i0++) {
    c10_e_u[c10_i0] = chartInstance->c10_dataWrittenToVector[c10_i0];
  }

  c10_f_y = NULL;
  sf_mex_assign(&c10_f_y, sf_mex_create("y", c10_e_u, 11, 0U, 1U, 0U, 1, 4),
                false);
  sf_mex_setcell(c10_y, 4, c10_f_y);
  sf_mex_assign(&c10_st, c10_y, false);
  return c10_st;
}

static void set_sim_state_c10_cameraTest(SFc10_cameraTestInstanceStruct
  *chartInstance, const mxArray *c10_st)
{
  const mxArray *c10_u;
  boolean_T c10_bv0[4];
  int32_T c10_i1;
  c10_u = sf_mex_dup(c10_st);
  *chartInstance->c10_gameOn = c10_b_emlrt_marshallIn(chartInstance, sf_mex_dup
    (sf_mex_getcell(c10_u, 0)), "gameOn");
  *chartInstance->c10_idleTicks = c10_b_emlrt_marshallIn(chartInstance,
    sf_mex_dup(sf_mex_getcell(c10_u, 1)), "idleTicks");
  *chartInstance->c10_is_active_c10_cameraTest = c10_b_emlrt_marshallIn
    (chartInstance, sf_mex_dup(sf_mex_getcell(c10_u, 2)),
     "is_active_c10_cameraTest");
  *chartInstance->c10_is_c10_cameraTest = c10_b_emlrt_marshallIn(chartInstance,
    sf_mex_dup(sf_mex_getcell(c10_u, 3)), "is_c10_cameraTest");
  c10_g_emlrt_marshallIn(chartInstance, sf_mex_dup(sf_mex_getcell(c10_u, 4)),
    "dataWrittenToVector", c10_bv0);
  for (c10_i1 = 0; c10_i1 < 4; c10_i1++) {
    chartInstance->c10_dataWrittenToVector[c10_i1] = c10_bv0[c10_i1];
  }

  sf_mex_assign(&chartInstance->c10_setSimStateSideEffectsInfo,
                c10_i_emlrt_marshallIn(chartInstance, sf_mex_dup(sf_mex_getcell
    (c10_u, 5)), "setSimStateSideEffectsInfo"), true);
  sf_mex_destroy(&c10_u);
  chartInstance->c10_doSetSimStateSideEffects = 1U;
  c10_update_debugger_state_c10_cameraTest(chartInstance);
  sf_mex_destroy(&c10_st);
}

static void c10_set_sim_state_side_effects_c10_cameraTest
  (SFc10_cameraTestInstanceStruct *chartInstance)
{
  if (chartInstance->c10_doSetSimStateSideEffects != 0) {
    chartInstance->c10_doSetSimStateSideEffects = 0U;
  }
}

static void finalize_c10_cameraTest(SFc10_cameraTestInstanceStruct
  *chartInstance)
{
  sf_mex_destroy(&chartInstance->c10_setSimStateSideEffectsInfo);
}

static void sf_gateway_c10_cameraTest(SFc10_cameraTestInstanceStruct
  *chartInstance)
{
  uint32_T c10_debug_family_var_map[2];
  real_T c10_nargin = 0.0;
  real_T c10_nargout = 0.0;
  uint32_T c10_b_debug_family_var_map[3];
  real_T c10_b_nargin = 0.0;
  real_T c10_b_nargout = 1.0;
  boolean_T c10_out;
  uint8_T c10_u0;
  real_T c10_c_nargin = 0.0;
  real_T c10_c_nargout = 0.0;
  real_T c10_d_nargin = 0.0;
  real_T c10_d_nargout = 1.0;
  boolean_T c10_b_out;
  real_T c10_e_nargin = 0.0;
  real_T c10_e_nargout = 0.0;
  real_T c10_f_nargin = 0.0;
  real_T c10_f_nargout = 0.0;
  uint32_T c10_u1;
  c10_set_sim_state_side_effects_c10_cameraTest(chartInstance);
  _SFD_SYMBOL_SCOPE_PUSH(0U, 0U);
  _sfTime_ = sf_get_time(chartInstance->S);
  _SFD_CC_CALL(CHART_ENTER_SFUNCTION_TAG, 6U, *chartInstance->c10_sfEvent);
  _SFD_DATA_RANGE_CHECK((real_T)*chartInstance->c10_gameOn, 2U);
  _SFD_DATA_RANGE_CHECK((real_T)*chartInstance->c10_idleTicks, 3U);
  *chartInstance->c10_sfEvent = CALL_EVENT;
  _SFD_CC_CALL(CHART_ENTER_DURING_FUNCTION_TAG, 6U, *chartInstance->c10_sfEvent);
  if (*chartInstance->c10_is_active_c10_cameraTest == 0U) {
    _SFD_CC_CALL(CHART_ENTER_ENTRY_FUNCTION_TAG, 6U, *chartInstance->c10_sfEvent);
    *chartInstance->c10_is_active_c10_cameraTest = 1U;
    _SFD_CC_CALL(EXIT_OUT_OF_FUNCTION_TAG, 6U, *chartInstance->c10_sfEvent);
    _SFD_CT_CALL(TRANSITION_ACTIVE_TAG, 1U, *chartInstance->c10_sfEvent);
    *chartInstance->c10_is_c10_cameraTest = c10_IN_notOn;
    _SFD_CS_CALL(STATE_ACTIVE_TAG, 3U, *chartInstance->c10_sfEvent);
    _SFD_SYMBOL_SCOPE_PUSH_EML(0U, 2U, 2U, c10_debug_family_names,
      c10_debug_family_var_map);
    _SFD_SYMBOL_SCOPE_ADD_EML_IMPORTABLE(&c10_nargin, 0U, c10_sf_marshallOut,
      c10_sf_marshallIn);
    _SFD_SYMBOL_SCOPE_ADD_EML_IMPORTABLE(&c10_nargout, 1U, c10_sf_marshallOut,
      c10_sf_marshallIn);
    *chartInstance->c10_gameOn = 0U;
    c10_updateDataWrittenToVector(chartInstance, 0U);
    _SFD_DATA_RANGE_CHECK((real_T)*chartInstance->c10_gameOn, 2U);
    *chartInstance->c10_idleTicks = 0U;
    c10_updateDataWrittenToVector(chartInstance, 1U);
    _SFD_DATA_RANGE_CHECK((real_T)*chartInstance->c10_idleTicks, 3U);
    _SFD_SYMBOL_SCOPE_POP();
  } else {
    switch (*chartInstance->c10_is_c10_cameraTest) {
     case c10_IN_gameIsOn:
      CV_CHART_EVAL(6, 0, 1);
      _SFD_CT_CALL(TRANSITION_BEFORE_PROCESSING_TAG, 0U,
                   *chartInstance->c10_sfEvent);
      _SFD_SYMBOL_SCOPE_PUSH_EML(0U, 3U, 3U, c10_e_debug_family_names,
        c10_b_debug_family_var_map);
      _SFD_SYMBOL_SCOPE_ADD_EML_IMPORTABLE(&c10_b_nargin, 0U, c10_sf_marshallOut,
        c10_sf_marshallIn);
      _SFD_SYMBOL_SCOPE_ADD_EML_IMPORTABLE(&c10_b_nargout, 1U,
        c10_sf_marshallOut, c10_sf_marshallIn);
      _SFD_SYMBOL_SCOPE_ADD_EML_IMPORTABLE(&c10_out, 2U, c10_d_sf_marshallOut,
        c10_d_sf_marshallIn);
      c10_u0 = c10_calcGoal(chartInstance);
      c10_out = CV_EML_IF(0, 0, 0, CV_RELATIONAL_EVAL(5U, 0U, 0, (real_T)c10_u0,
        1.0, 0, 0U, c10_u0 == 1));
      _SFD_SYMBOL_SCOPE_POP();
      if (c10_out) {
        _SFD_CT_CALL(TRANSITION_ACTIVE_TAG, 0U, *chartInstance->c10_sfEvent);
        _SFD_CS_CALL(STATE_INACTIVE_TAG, 2U, *chartInstance->c10_sfEvent);
        *chartInstance->c10_is_c10_cameraTest = c10_IN_notOn;
        _SFD_CS_CALL(STATE_ACTIVE_TAG, 3U, *chartInstance->c10_sfEvent);
        _SFD_SYMBOL_SCOPE_PUSH_EML(0U, 2U, 2U, c10_debug_family_names,
          c10_debug_family_var_map);
        _SFD_SYMBOL_SCOPE_ADD_EML_IMPORTABLE(&c10_c_nargin, 0U,
          c10_sf_marshallOut, c10_sf_marshallIn);
        _SFD_SYMBOL_SCOPE_ADD_EML_IMPORTABLE(&c10_c_nargout, 1U,
          c10_sf_marshallOut, c10_sf_marshallIn);
        *chartInstance->c10_gameOn = 0U;
        c10_updateDataWrittenToVector(chartInstance, 0U);
        _SFD_DATA_RANGE_CHECK((real_T)*chartInstance->c10_gameOn, 2U);
        *chartInstance->c10_idleTicks = 0U;
        c10_updateDataWrittenToVector(chartInstance, 1U);
        _SFD_DATA_RANGE_CHECK((real_T)*chartInstance->c10_idleTicks, 3U);
        _SFD_SYMBOL_SCOPE_POP();
      } else {
        _SFD_CS_CALL(STATE_ENTER_DURING_FUNCTION_TAG, 2U,
                     *chartInstance->c10_sfEvent);
      }

      _SFD_CS_CALL(EXIT_OUT_OF_FUNCTION_TAG, 2U, *chartInstance->c10_sfEvent);
      break;

     case c10_IN_notOn:
      CV_CHART_EVAL(6, 0, 2);
      _SFD_CT_CALL(TRANSITION_BEFORE_PROCESSING_TAG, 2U,
                   *chartInstance->c10_sfEvent);
      _SFD_SYMBOL_SCOPE_PUSH_EML(0U, 3U, 3U, c10_g_debug_family_names,
        c10_b_debug_family_var_map);
      _SFD_SYMBOL_SCOPE_ADD_EML_IMPORTABLE(&c10_d_nargin, 0U, c10_sf_marshallOut,
        c10_sf_marshallIn);
      _SFD_SYMBOL_SCOPE_ADD_EML_IMPORTABLE(&c10_d_nargout, 1U,
        c10_sf_marshallOut, c10_sf_marshallIn);
      _SFD_SYMBOL_SCOPE_ADD_EML_IMPORTABLE(&c10_b_out, 2U, c10_d_sf_marshallOut,
        c10_d_sf_marshallIn);
      c10_errorIfDataNotWrittenToFcn(chartInstance, 1U, 3U, 12U, 1, 9);
      c10_b_out = CV_EML_IF(2, 0, 0, CV_RELATIONAL_EVAL(5U, 2U, 0, (real_T)
        *chartInstance->c10_idleTicks, 10.0, 0, 4U,
        *chartInstance->c10_idleTicks > 10));
      _SFD_SYMBOL_SCOPE_POP();
      if (c10_b_out) {
        _SFD_CT_CALL(TRANSITION_ACTIVE_TAG, 2U, *chartInstance->c10_sfEvent);
        _SFD_CS_CALL(STATE_INACTIVE_TAG, 3U, *chartInstance->c10_sfEvent);
        *chartInstance->c10_is_c10_cameraTest = c10_IN_gameIsOn;
        _SFD_CS_CALL(STATE_ACTIVE_TAG, 2U, *chartInstance->c10_sfEvent);
        _SFD_SYMBOL_SCOPE_PUSH_EML(0U, 2U, 2U, c10_d_debug_family_names,
          c10_debug_family_var_map);
        _SFD_SYMBOL_SCOPE_ADD_EML_IMPORTABLE(&c10_e_nargin, 0U,
          c10_sf_marshallOut, c10_sf_marshallIn);
        _SFD_SYMBOL_SCOPE_ADD_EML_IMPORTABLE(&c10_e_nargout, 1U,
          c10_sf_marshallOut, c10_sf_marshallIn);
        *chartInstance->c10_gameOn = 1U;
        c10_updateDataWrittenToVector(chartInstance, 0U);
        _SFD_DATA_RANGE_CHECK((real_T)*chartInstance->c10_gameOn, 2U);
        _SFD_SYMBOL_SCOPE_POP();
      } else {
        _SFD_CS_CALL(STATE_ENTER_DURING_FUNCTION_TAG, 3U,
                     *chartInstance->c10_sfEvent);
        _SFD_SYMBOL_SCOPE_PUSH_EML(0U, 2U, 2U, c10_b_debug_family_names,
          c10_debug_family_var_map);
        _SFD_SYMBOL_SCOPE_ADD_EML_IMPORTABLE(&c10_f_nargin, 0U,
          c10_sf_marshallOut, c10_sf_marshallIn);
        _SFD_SYMBOL_SCOPE_ADD_EML_IMPORTABLE(&c10_f_nargout, 1U,
          c10_sf_marshallOut, c10_sf_marshallIn);
        c10_errorIfDataNotWrittenToFcn(chartInstance, 1U, 3U, 5U, 63, 9);
        c10_u1 = (uint32_T)*chartInstance->c10_idleTicks + (uint32_T)
          c10_calcReady(chartInstance);
        if (CV_SATURATION_EVAL(4, 3, 0, 0, c10_u1 > 255U)) {
          c10_u1 = 255U;
        }

        *chartInstance->c10_idleTicks = (uint8_T)c10_u1;
        c10_updateDataWrittenToVector(chartInstance, 1U);
        _SFD_DATA_RANGE_CHECK((real_T)*chartInstance->c10_idleTicks, 3U);
        _SFD_SYMBOL_SCOPE_POP();
      }

      _SFD_CS_CALL(EXIT_OUT_OF_FUNCTION_TAG, 3U, *chartInstance->c10_sfEvent);
      break;

     default:
      CV_CHART_EVAL(6, 0, 0);
      *chartInstance->c10_is_c10_cameraTest = c10_IN_NO_ACTIVE_CHILD;
      _SFD_CS_CALL(STATE_INACTIVE_TAG, 2U, *chartInstance->c10_sfEvent);
      break;
    }
  }

  _SFD_CC_CALL(EXIT_OUT_OF_FUNCTION_TAG, 6U, *chartInstance->c10_sfEvent);
  _SFD_SYMBOL_SCOPE_POP();
  _SFD_CHECK_FOR_STATE_INCONSISTENCY(_cameraTestMachineNumber_,
    chartInstance->chartNumber, chartInstance->instanceNumber);
}

static void mdl_start_c10_cameraTest(SFc10_cameraTestInstanceStruct
  *chartInstance)
{
  (void)chartInstance;
}

static void initSimStructsc10_cameraTest(SFc10_cameraTestInstanceStruct
  *chartInstance)
{
  (void)chartInstance;
}

static void init_script_number_translation(uint32_T c10_machineNumber, uint32_T
  c10_chartNumber, uint32_T c10_instanceNumber)
{
  (void)c10_machineNumber;
  (void)c10_chartNumber;
  (void)c10_instanceNumber;
}

static const mxArray *c10_sf_marshallOut(void *chartInstanceVoid, void
  *c10_inData)
{
  const mxArray *c10_mxArrayOutData = NULL;
  real_T c10_u;
  const mxArray *c10_y = NULL;
  SFc10_cameraTestInstanceStruct *chartInstance;
  chartInstance = (SFc10_cameraTestInstanceStruct *)chartInstanceVoid;
  c10_mxArrayOutData = NULL;
  c10_u = *(real_T *)c10_inData;
  c10_y = NULL;
  sf_mex_assign(&c10_y, sf_mex_create("y", &c10_u, 0, 0U, 0U, 0U, 0), false);
  sf_mex_assign(&c10_mxArrayOutData, c10_y, false);
  return c10_mxArrayOutData;
}

static real_T c10_emlrt_marshallIn(SFc10_cameraTestInstanceStruct *chartInstance,
  const mxArray *c10_u, const emlrtMsgIdentifier *c10_parentId)
{
  real_T c10_y;
  real_T c10_d0;
  (void)chartInstance;
  sf_mex_import(c10_parentId, sf_mex_dup(c10_u), &c10_d0, 1, 0, 0U, 0, 0U, 0);
  c10_y = c10_d0;
  sf_mex_destroy(&c10_u);
  return c10_y;
}

static void c10_sf_marshallIn(void *chartInstanceVoid, const mxArray
  *c10_mxArrayInData, const char_T *c10_varName, void *c10_outData)
{
  const mxArray *c10_nargout;
  const char_T *c10_identifier;
  emlrtMsgIdentifier c10_thisId;
  real_T c10_y;
  SFc10_cameraTestInstanceStruct *chartInstance;
  chartInstance = (SFc10_cameraTestInstanceStruct *)chartInstanceVoid;
  c10_nargout = sf_mex_dup(c10_mxArrayInData);
  c10_identifier = c10_varName;
  c10_thisId.fIdentifier = c10_identifier;
  c10_thisId.fParent = NULL;
  c10_y = c10_emlrt_marshallIn(chartInstance, sf_mex_dup(c10_nargout),
    &c10_thisId);
  sf_mex_destroy(&c10_nargout);
  *(real_T *)c10_outData = c10_y;
  sf_mex_destroy(&c10_mxArrayInData);
}

static const mxArray *c10_b_sf_marshallOut(void *chartInstanceVoid, void
  *c10_inData)
{
  const mxArray *c10_mxArrayOutData = NULL;
  uint8_T c10_u;
  const mxArray *c10_y = NULL;
  SFc10_cameraTestInstanceStruct *chartInstance;
  chartInstance = (SFc10_cameraTestInstanceStruct *)chartInstanceVoid;
  c10_mxArrayOutData = NULL;
  c10_u = *(uint8_T *)c10_inData;
  c10_y = NULL;
  sf_mex_assign(&c10_y, sf_mex_create("y", &c10_u, 3, 0U, 0U, 0U, 0), false);
  sf_mex_assign(&c10_mxArrayOutData, c10_y, false);
  return c10_mxArrayOutData;
}

static uint8_T c10_b_emlrt_marshallIn(SFc10_cameraTestInstanceStruct
  *chartInstance, const mxArray *c10_ready, const char_T *c10_identifier)
{
  uint8_T c10_y;
  emlrtMsgIdentifier c10_thisId;
  c10_thisId.fIdentifier = c10_identifier;
  c10_thisId.fParent = NULL;
  c10_y = c10_c_emlrt_marshallIn(chartInstance, sf_mex_dup(c10_ready),
    &c10_thisId);
  sf_mex_destroy(&c10_ready);
  return c10_y;
}

static uint8_T c10_c_emlrt_marshallIn(SFc10_cameraTestInstanceStruct
  *chartInstance, const mxArray *c10_u, const emlrtMsgIdentifier *c10_parentId)
{
  uint8_T c10_y;
  uint8_T c10_u2;
  (void)chartInstance;
  sf_mex_import(c10_parentId, sf_mex_dup(c10_u), &c10_u2, 1, 3, 0U, 0, 0U, 0);
  c10_y = c10_u2;
  sf_mex_destroy(&c10_u);
  return c10_y;
}

static void c10_b_sf_marshallIn(void *chartInstanceVoid, const mxArray
  *c10_mxArrayInData, const char_T *c10_varName, void *c10_outData)
{
  const mxArray *c10_ready;
  const char_T *c10_identifier;
  emlrtMsgIdentifier c10_thisId;
  uint8_T c10_y;
  SFc10_cameraTestInstanceStruct *chartInstance;
  chartInstance = (SFc10_cameraTestInstanceStruct *)chartInstanceVoid;
  c10_ready = sf_mex_dup(c10_mxArrayInData);
  c10_identifier = c10_varName;
  c10_thisId.fIdentifier = c10_identifier;
  c10_thisId.fParent = NULL;
  c10_y = c10_c_emlrt_marshallIn(chartInstance, sf_mex_dup(c10_ready),
    &c10_thisId);
  sf_mex_destroy(&c10_ready);
  *(uint8_T *)c10_outData = c10_y;
  sf_mex_destroy(&c10_mxArrayInData);
}

static const mxArray *c10_c_sf_marshallOut(void *chartInstanceVoid, void
  *c10_inData)
{
  const mxArray *c10_mxArrayOutData = NULL;
  int32_T c10_i2;
  int8_T c10_b_inData[6];
  int32_T c10_i3;
  int8_T c10_u[6];
  const mxArray *c10_y = NULL;
  SFc10_cameraTestInstanceStruct *chartInstance;
  chartInstance = (SFc10_cameraTestInstanceStruct *)chartInstanceVoid;
  c10_mxArrayOutData = NULL;
  for (c10_i2 = 0; c10_i2 < 6; c10_i2++) {
    c10_b_inData[c10_i2] = (*(int8_T (*)[6])c10_inData)[c10_i2];
  }

  for (c10_i3 = 0; c10_i3 < 6; c10_i3++) {
    c10_u[c10_i3] = c10_b_inData[c10_i3];
  }

  c10_y = NULL;
  sf_mex_assign(&c10_y, sf_mex_create("y", c10_u, 2, 0U, 1U, 0U, 2, 1, 6), false);
  sf_mex_assign(&c10_mxArrayOutData, c10_y, false);
  return c10_mxArrayOutData;
}

static void c10_d_emlrt_marshallIn(SFc10_cameraTestInstanceStruct *chartInstance,
  const mxArray *c10_u, const emlrtMsgIdentifier *c10_parentId, int8_T c10_y[6])
{
  int8_T c10_iv0[6];
  int32_T c10_i4;
  (void)chartInstance;
  sf_mex_import(c10_parentId, sf_mex_dup(c10_u), c10_iv0, 1, 2, 0U, 1, 0U, 2, 1,
                6);
  for (c10_i4 = 0; c10_i4 < 6; c10_i4++) {
    c10_y[c10_i4] = c10_iv0[c10_i4];
  }

  sf_mex_destroy(&c10_u);
}

static void c10_c_sf_marshallIn(void *chartInstanceVoid, const mxArray
  *c10_mxArrayInData, const char_T *c10_varName, void *c10_outData)
{
  const mxArray *c10_position;
  const char_T *c10_identifier;
  emlrtMsgIdentifier c10_thisId;
  int8_T c10_y[6];
  int32_T c10_i5;
  SFc10_cameraTestInstanceStruct *chartInstance;
  chartInstance = (SFc10_cameraTestInstanceStruct *)chartInstanceVoid;
  c10_position = sf_mex_dup(c10_mxArrayInData);
  c10_identifier = c10_varName;
  c10_thisId.fIdentifier = c10_identifier;
  c10_thisId.fParent = NULL;
  c10_d_emlrt_marshallIn(chartInstance, sf_mex_dup(c10_position), &c10_thisId,
    c10_y);
  sf_mex_destroy(&c10_position);
  for (c10_i5 = 0; c10_i5 < 6; c10_i5++) {
    (*(int8_T (*)[6])c10_outData)[c10_i5] = c10_y[c10_i5];
  }

  sf_mex_destroy(&c10_mxArrayInData);
}

static const mxArray *c10_d_sf_marshallOut(void *chartInstanceVoid, void
  *c10_inData)
{
  const mxArray *c10_mxArrayOutData = NULL;
  boolean_T c10_u;
  const mxArray *c10_y = NULL;
  SFc10_cameraTestInstanceStruct *chartInstance;
  chartInstance = (SFc10_cameraTestInstanceStruct *)chartInstanceVoid;
  c10_mxArrayOutData = NULL;
  c10_u = *(boolean_T *)c10_inData;
  c10_y = NULL;
  sf_mex_assign(&c10_y, sf_mex_create("y", &c10_u, 11, 0U, 0U, 0U, 0), false);
  sf_mex_assign(&c10_mxArrayOutData, c10_y, false);
  return c10_mxArrayOutData;
}

static boolean_T c10_e_emlrt_marshallIn(SFc10_cameraTestInstanceStruct
  *chartInstance, const mxArray *c10_u, const emlrtMsgIdentifier *c10_parentId)
{
  boolean_T c10_y;
  boolean_T c10_b0;
  (void)chartInstance;
  sf_mex_import(c10_parentId, sf_mex_dup(c10_u), &c10_b0, 1, 11, 0U, 0, 0U, 0);
  c10_y = c10_b0;
  sf_mex_destroy(&c10_u);
  return c10_y;
}

static void c10_d_sf_marshallIn(void *chartInstanceVoid, const mxArray
  *c10_mxArrayInData, const char_T *c10_varName, void *c10_outData)
{
  const mxArray *c10_sf_internal_predicateOutput;
  const char_T *c10_identifier;
  emlrtMsgIdentifier c10_thisId;
  boolean_T c10_y;
  SFc10_cameraTestInstanceStruct *chartInstance;
  chartInstance = (SFc10_cameraTestInstanceStruct *)chartInstanceVoid;
  c10_sf_internal_predicateOutput = sf_mex_dup(c10_mxArrayInData);
  c10_identifier = c10_varName;
  c10_thisId.fIdentifier = c10_identifier;
  c10_thisId.fParent = NULL;
  c10_y = c10_e_emlrt_marshallIn(chartInstance, sf_mex_dup
    (c10_sf_internal_predicateOutput), &c10_thisId);
  sf_mex_destroy(&c10_sf_internal_predicateOutput);
  *(boolean_T *)c10_outData = c10_y;
  sf_mex_destroy(&c10_mxArrayInData);
}

const mxArray *sf_c10_cameraTest_get_eml_resolved_functions_info(void)
{
  const mxArray *c10_nameCaptureInfo = NULL;
  c10_nameCaptureInfo = NULL;
  sf_mex_assign(&c10_nameCaptureInfo, sf_mex_createstruct("structure", 2, 3, 1),
                false);
  c10_info_helper(&c10_nameCaptureInfo);
  sf_mex_emlrtNameCapturePostProcessR2012a(&c10_nameCaptureInfo);
  return c10_nameCaptureInfo;
}

static void c10_info_helper(const mxArray **c10_info)
{
  const mxArray *c10_rhs0 = NULL;
  const mxArray *c10_lhs0 = NULL;
  const mxArray *c10_rhs1 = NULL;
  const mxArray *c10_lhs1 = NULL;
  const mxArray *c10_rhs2 = NULL;
  const mxArray *c10_lhs2 = NULL;
  sf_mex_addfield(*c10_info, c10_emlrt_marshallOut(""), "context", "context", 0);
  sf_mex_addfield(*c10_info, c10_emlrt_marshallOut("abs"), "name", "name", 0);
  sf_mex_addfield(*c10_info, c10_emlrt_marshallOut("int8"), "dominantType",
                  "dominantType", 0);
  sf_mex_addfield(*c10_info, c10_emlrt_marshallOut(
    "[ILXE]$matlabroot$/toolbox/eml/lib/matlab/elfun/abs.m"), "resolved",
                  "resolved", 0);
  sf_mex_addfield(*c10_info, c10_b_emlrt_marshallOut(1363717452U), "fileTimeLo",
                  "fileTimeLo", 0);
  sf_mex_addfield(*c10_info, c10_b_emlrt_marshallOut(0U), "fileTimeHi",
                  "fileTimeHi", 0);
  sf_mex_addfield(*c10_info, c10_b_emlrt_marshallOut(0U), "mFileTimeLo",
                  "mFileTimeLo", 0);
  sf_mex_addfield(*c10_info, c10_b_emlrt_marshallOut(0U), "mFileTimeHi",
                  "mFileTimeHi", 0);
  sf_mex_assign(&c10_rhs0, sf_mex_createcellmatrix(0, 1), false);
  sf_mex_assign(&c10_lhs0, sf_mex_createcellmatrix(0, 1), false);
  sf_mex_addfield(*c10_info, sf_mex_duplicatearraysafe(&c10_rhs0), "rhs", "rhs",
                  0);
  sf_mex_addfield(*c10_info, sf_mex_duplicatearraysafe(&c10_lhs0), "lhs", "lhs",
                  0);
  sf_mex_addfield(*c10_info, c10_emlrt_marshallOut(
    "[ILXE]$matlabroot$/toolbox/eml/lib/matlab/elfun/abs.m"), "context",
                  "context", 1);
  sf_mex_addfield(*c10_info, c10_emlrt_marshallOut(
    "coder.internal.isBuiltInNumeric"), "name", "name", 1);
  sf_mex_addfield(*c10_info, c10_emlrt_marshallOut("int8"), "dominantType",
                  "dominantType", 1);
  sf_mex_addfield(*c10_info, c10_emlrt_marshallOut(
    "[ILXE]$matlabroot$/toolbox/shared/coder/coder/+coder/+internal/isBuiltInNumeric.m"),
                  "resolved", "resolved", 1);
  sf_mex_addfield(*c10_info, c10_b_emlrt_marshallOut(1395935456U), "fileTimeLo",
                  "fileTimeLo", 1);
  sf_mex_addfield(*c10_info, c10_b_emlrt_marshallOut(0U), "fileTimeHi",
                  "fileTimeHi", 1);
  sf_mex_addfield(*c10_info, c10_b_emlrt_marshallOut(0U), "mFileTimeLo",
                  "mFileTimeLo", 1);
  sf_mex_addfield(*c10_info, c10_b_emlrt_marshallOut(0U), "mFileTimeHi",
                  "mFileTimeHi", 1);
  sf_mex_assign(&c10_rhs1, sf_mex_createcellmatrix(0, 1), false);
  sf_mex_assign(&c10_lhs1, sf_mex_createcellmatrix(0, 1), false);
  sf_mex_addfield(*c10_info, sf_mex_duplicatearraysafe(&c10_rhs1), "rhs", "rhs",
                  1);
  sf_mex_addfield(*c10_info, sf_mex_duplicatearraysafe(&c10_lhs1), "lhs", "lhs",
                  1);
  sf_mex_addfield(*c10_info, c10_emlrt_marshallOut(
    "[ILXE]$matlabroot$/toolbox/eml/lib/matlab/elfun/abs.m"), "context",
                  "context", 2);
  sf_mex_addfield(*c10_info, c10_emlrt_marshallOut("eml_scalar_abs"), "name",
                  "name", 2);
  sf_mex_addfield(*c10_info, c10_emlrt_marshallOut("int8"), "dominantType",
                  "dominantType", 2);
  sf_mex_addfield(*c10_info, c10_emlrt_marshallOut(
    "[ILXE]$matlabroot$/toolbox/eml/lib/matlab/elfun/eml_scalar_abs.m"),
                  "resolved", "resolved", 2);
  sf_mex_addfield(*c10_info, c10_b_emlrt_marshallOut(1286822312U), "fileTimeLo",
                  "fileTimeLo", 2);
  sf_mex_addfield(*c10_info, c10_b_emlrt_marshallOut(0U), "fileTimeHi",
                  "fileTimeHi", 2);
  sf_mex_addfield(*c10_info, c10_b_emlrt_marshallOut(0U), "mFileTimeLo",
                  "mFileTimeLo", 2);
  sf_mex_addfield(*c10_info, c10_b_emlrt_marshallOut(0U), "mFileTimeHi",
                  "mFileTimeHi", 2);
  sf_mex_assign(&c10_rhs2, sf_mex_createcellmatrix(0, 1), false);
  sf_mex_assign(&c10_lhs2, sf_mex_createcellmatrix(0, 1), false);
  sf_mex_addfield(*c10_info, sf_mex_duplicatearraysafe(&c10_rhs2), "rhs", "rhs",
                  2);
  sf_mex_addfield(*c10_info, sf_mex_duplicatearraysafe(&c10_lhs2), "lhs", "lhs",
                  2);
  sf_mex_destroy(&c10_rhs0);
  sf_mex_destroy(&c10_lhs0);
  sf_mex_destroy(&c10_rhs1);
  sf_mex_destroy(&c10_lhs1);
  sf_mex_destroy(&c10_rhs2);
  sf_mex_destroy(&c10_lhs2);
}

static const mxArray *c10_emlrt_marshallOut(const char * c10_u)
{
  const mxArray *c10_y = NULL;
  c10_y = NULL;
  sf_mex_assign(&c10_y, sf_mex_create("y", c10_u, 15, 0U, 0U, 0U, 2, 1, strlen
    (c10_u)), false);
  return c10_y;
}

static const mxArray *c10_b_emlrt_marshallOut(const uint32_T c10_u)
{
  const mxArray *c10_y = NULL;
  c10_y = NULL;
  sf_mex_assign(&c10_y, sf_mex_create("y", &c10_u, 7, 0U, 0U, 0U, 0), false);
  return c10_y;
}

static uint8_T c10_calcReady(SFc10_cameraTestInstanceStruct *chartInstance)
{
  uint8_T c10_ready;
  uint32_T c10_debug_family_var_map[5];
  int8_T c10_position[6];
  real_T c10_ii;
  real_T c10_nargin = 0.0;
  real_T c10_nargout = 1.0;
  int32_T c10_i6;
  static int8_T c10_iv1[6] = { -10, -30, -70, 10, 30, 70 };

  int32_T c10_b_ii;
  int32_T c10_i7;
  int8_T c10_x;
  int8_T c10_b_x;
  int32_T c10_i8;
  int8_T c10_y;
  real_T c10_d1;
  int8_T c10_c_x;
  int8_T c10_d_x;
  int32_T c10_i9;
  int8_T c10_b_y;
  real_T c10_d2;
  int32_T exitg1;
  boolean_T guard1 = false;
  _SFD_SYMBOL_SCOPE_PUSH_EML(0U, 5U, 5U, c10_c_debug_family_names,
    c10_debug_family_var_map);
  _SFD_SYMBOL_SCOPE_ADD_EML_IMPORTABLE(c10_position, 0U, c10_c_sf_marshallOut,
    c10_c_sf_marshallIn);
  _SFD_SYMBOL_SCOPE_ADD_EML_IMPORTABLE(&c10_ii, 1U, c10_sf_marshallOut,
    c10_sf_marshallIn);
  _SFD_SYMBOL_SCOPE_ADD_EML_IMPORTABLE(&c10_nargin, 2U, c10_sf_marshallOut,
    c10_sf_marshallIn);
  _SFD_SYMBOL_SCOPE_ADD_EML_IMPORTABLE(&c10_nargout, 3U, c10_sf_marshallOut,
    c10_sf_marshallIn);
  _SFD_SYMBOL_SCOPE_ADD_EML_IMPORTABLE(&c10_ready, 4U, c10_b_sf_marshallOut,
    c10_b_sf_marshallIn);
  CV_EML_FCN(1, 0);
  _SFD_EML_CALL(1U, *chartInstance->c10_sfEvent, 2);
  c10_ready = 1U;
  _SFD_EML_CALL(1U, *chartInstance->c10_sfEvent, 3);
  for (c10_i6 = 0; c10_i6 < 6; c10_i6++) {
    c10_position[c10_i6] = c10_iv1[c10_i6];
  }

  _SFD_EML_CALL(1U, *chartInstance->c10_sfEvent, 4);
  c10_ii = 1.0;
  c10_b_ii = 0;
  do {
    exitg1 = 0;
    if (c10_b_ii < 6) {
      c10_ii = 1.0 + (real_T)c10_b_ii;
      CV_EML_FOR(1, 1, 0, 1);
      _SFD_EML_CALL(1U, *chartInstance->c10_sfEvent, 5);
      c10_i7 = *(int8_T *)&((char_T *)(c10_Player *)&((char_T *)
        chartInstance->c10_players)[8 * (_SFD_EML_ARRAY_BOUNDS_CHECK("players",
                              (int32_T)_SFD_INTEGER_CHECK("ii", c10_ii), 1, 6, 1,
        0) - 1)])[0] - c10_position[_SFD_EML_ARRAY_BOUNDS_CHECK("position",
        (int32_T)_SFD_INTEGER_CHECK("ii", c10_ii), 1, 6, 1, 0) - 1];
      if (c10_i7 > 127) {
        CV_SATURATION_EVAL(4, 1, 0, 0, 1);
        c10_i7 = 127;
      } else {
        if (CV_SATURATION_EVAL(4, 1, 0, 0, c10_i7 < -128)) {
          c10_i7 = -128;
        }
      }

      c10_x = (int8_T)c10_i7;
      c10_b_x = c10_x;
      c10_i8 = -c10_b_x;
      if (c10_i8 > 127) {
        CV_SATURATION_EVAL(4, 1, 2, 0, 1);
        c10_i8 = 127;
      } else {
        if (CV_SATURATION_EVAL(4, 1, 2, 0, c10_i8 < -128)) {
          c10_i8 = -128;
        }
      }

      if ((real_T)c10_b_x < 0.0) {
        c10_y = (int8_T)c10_i8;
      } else {
        c10_y = c10_b_x;
      }

      c10_d1 = (real_T)c10_y;
      guard1 = false;
      if (CV_EML_COND(1, 1, 0, CV_RELATIONAL_EVAL(4U, 1U, 0, c10_d1, 3.0, -1, 4U,
            c10_d1 > 3.0))) {
        c10_c_x = *(int8_T *)&((char_T *)(c10_Player *)&((char_T *)
          chartInstance->c10_players)[8 * (_SFD_EML_ARRAY_BOUNDS_CHECK("players",
                                 (int32_T)_SFD_INTEGER_CHECK("ii", c10_ii), 1, 6,
          1, 0) - 1)])[1];
        c10_d_x = c10_c_x;
        c10_i9 = -c10_d_x;
        if (c10_i9 > 127) {
          CV_SATURATION_EVAL(4, 1, 1, 0, 1);
          c10_i9 = 127;
        } else {
          if (CV_SATURATION_EVAL(4, 1, 1, 0, c10_i9 < -128)) {
            c10_i9 = -128;
          }
        }

        if ((real_T)c10_d_x < 0.0) {
          c10_b_y = (int8_T)c10_i9;
        } else {
          c10_b_y = c10_d_x;
        }

        c10_d2 = (real_T)c10_b_y;
        if (CV_EML_COND(1, 1, 1, CV_RELATIONAL_EVAL(4U, 1U, 1, c10_d2, 3.0, -1,
              4U, c10_d2 > 3.0))) {
          CV_EML_MCDC(1, 1, 0, true);
          CV_EML_IF(1, 1, 0, true);
          _SFD_EML_CALL(1U, *chartInstance->c10_sfEvent, 6);
          c10_ready = 0U;
          _SFD_EML_CALL(1U, *chartInstance->c10_sfEvent, 7);
          exitg1 = 1;
        } else {
          guard1 = true;
        }
      } else {
        guard1 = true;
      }

      if (guard1 == true) {
        CV_EML_MCDC(1, 1, 0, false);
        CV_EML_IF(1, 1, 0, false);
        c10_b_ii++;
        _SF_MEX_LISTEN_FOR_CTRL_C(chartInstance->S);
      }
    } else {
      CV_EML_FOR(1, 1, 0, 0);
      exitg1 = 1;
    }
  } while (exitg1 == 0);

  _SFD_EML_CALL(1U, *chartInstance->c10_sfEvent, -7);
  _SFD_SYMBOL_SCOPE_POP();
  return c10_ready;
}

static uint8_T c10_calcGoal(SFc10_cameraTestInstanceStruct *chartInstance)
{
  uint8_T c10_goal;
  uint32_T c10_debug_family_var_map[3];
  real_T c10_nargin = 0.0;
  real_T c10_nargout = 1.0;
  int8_T c10_x;
  int8_T c10_b_x;
  int32_T c10_i10;
  int8_T c10_y;
  real_T c10_d3;
  _SFD_SYMBOL_SCOPE_PUSH_EML(0U, 3U, 3U, c10_f_debug_family_names,
    c10_debug_family_var_map);
  _SFD_SYMBOL_SCOPE_ADD_EML_IMPORTABLE(&c10_nargin, 0U, c10_sf_marshallOut,
    c10_sf_marshallIn);
  _SFD_SYMBOL_SCOPE_ADD_EML_IMPORTABLE(&c10_nargout, 1U, c10_sf_marshallOut,
    c10_sf_marshallIn);
  _SFD_SYMBOL_SCOPE_ADD_EML_IMPORTABLE(&c10_goal, 2U, c10_b_sf_marshallOut,
    c10_b_sf_marshallIn);
  CV_EML_FCN(0, 0);
  _SFD_EML_CALL(0U, *chartInstance->c10_sfEvent, 2);
  c10_goal = 0U;
  _SFD_EML_CALL(0U, *chartInstance->c10_sfEvent, 3);
  c10_x = *(int8_T *)&((char_T *)chartInstance->c10_ball)[0];
  c10_b_x = c10_x;
  c10_i10 = -c10_b_x;
  if (c10_i10 > 127) {
    CV_SATURATION_EVAL(4, 0, 0, 0, 1);
    c10_i10 = 127;
  } else {
    if (CV_SATURATION_EVAL(4, 0, 0, 0, c10_i10 < -128)) {
      c10_i10 = -128;
    }
  }

  if ((real_T)c10_b_x < 0.0) {
    c10_y = (int8_T)c10_i10;
  } else {
    c10_y = c10_b_x;
  }

  c10_d3 = (real_T)c10_y;
  if (CV_EML_IF(0, 1, 0, CV_RELATIONAL_EVAL(4U, 0U, 0, c10_d3, 75.0, -1, 4U,
        c10_d3 > 75.0))) {
    _SFD_EML_CALL(0U, *chartInstance->c10_sfEvent, 4);
    c10_goal = 1U;
  }

  _SFD_EML_CALL(0U, *chartInstance->c10_sfEvent, -4);
  _SFD_SYMBOL_SCOPE_POP();
  return c10_goal;
}

static const mxArray *c10_e_sf_marshallOut(void *chartInstanceVoid, void
  *c10_inData)
{
  const mxArray *c10_mxArrayOutData = NULL;
  int32_T c10_u;
  const mxArray *c10_y = NULL;
  SFc10_cameraTestInstanceStruct *chartInstance;
  chartInstance = (SFc10_cameraTestInstanceStruct *)chartInstanceVoid;
  c10_mxArrayOutData = NULL;
  c10_u = *(int32_T *)c10_inData;
  c10_y = NULL;
  sf_mex_assign(&c10_y, sf_mex_create("y", &c10_u, 6, 0U, 0U, 0U, 0), false);
  sf_mex_assign(&c10_mxArrayOutData, c10_y, false);
  return c10_mxArrayOutData;
}

static int32_T c10_f_emlrt_marshallIn(SFc10_cameraTestInstanceStruct
  *chartInstance, const mxArray *c10_u, const emlrtMsgIdentifier *c10_parentId)
{
  int32_T c10_y;
  int32_T c10_i11;
  (void)chartInstance;
  sf_mex_import(c10_parentId, sf_mex_dup(c10_u), &c10_i11, 1, 6, 0U, 0, 0U, 0);
  c10_y = c10_i11;
  sf_mex_destroy(&c10_u);
  return c10_y;
}

static void c10_e_sf_marshallIn(void *chartInstanceVoid, const mxArray
  *c10_mxArrayInData, const char_T *c10_varName, void *c10_outData)
{
  const mxArray *c10_b_sfEvent;
  const char_T *c10_identifier;
  emlrtMsgIdentifier c10_thisId;
  int32_T c10_y;
  SFc10_cameraTestInstanceStruct *chartInstance;
  chartInstance = (SFc10_cameraTestInstanceStruct *)chartInstanceVoid;
  c10_b_sfEvent = sf_mex_dup(c10_mxArrayInData);
  c10_identifier = c10_varName;
  c10_thisId.fIdentifier = c10_identifier;
  c10_thisId.fParent = NULL;
  c10_y = c10_f_emlrt_marshallIn(chartInstance, sf_mex_dup(c10_b_sfEvent),
    &c10_thisId);
  sf_mex_destroy(&c10_b_sfEvent);
  *(int32_T *)c10_outData = c10_y;
  sf_mex_destroy(&c10_mxArrayInData);
}

static const mxArray *c10_players_bus_io(void *chartInstanceVoid, void
  *c10_pData)
{
  const mxArray *c10_mxVal = NULL;
  int32_T c10_i12;
  c10_Player c10_tmp[6];
  SFc10_cameraTestInstanceStruct *chartInstance;
  chartInstance = (SFc10_cameraTestInstanceStruct *)chartInstanceVoid;
  c10_mxVal = NULL;
  for (c10_i12 = 0; c10_i12 < 6; c10_i12++) {
    c10_tmp[c10_i12].x = *(int8_T *)&((char_T *)(c10_Player *)&((char_T *)
      (c10_Player (*)[6])c10_pData)[8 * c10_i12])[0];
    c10_tmp[c10_i12].y = *(int8_T *)&((char_T *)(c10_Player *)&((char_T *)
      (c10_Player (*)[6])c10_pData)[8 * c10_i12])[1];
    c10_tmp[c10_i12].orientation = *(int16_T *)&((char_T *)(c10_Player *)
      &((char_T *)(c10_Player (*)[6])c10_pData)[8 * c10_i12])[2];
    c10_tmp[c10_i12].color = *(uint8_T *)&((char_T *)(c10_Player *)&((char_T *)
      (c10_Player (*)[6])c10_pData)[8 * c10_i12])[4];
    c10_tmp[c10_i12].position = *(uint8_T *)&((char_T *)(c10_Player *)&((char_T *)
      (c10_Player (*)[6])c10_pData)[8 * c10_i12])[5];
    c10_tmp[c10_i12].valid = *(uint8_T *)&((char_T *)(c10_Player *)&((char_T *)
      (c10_Player (*)[6])c10_pData)[8 * c10_i12])[6];
  }

  sf_mex_assign(&c10_mxVal, c10_f_sf_marshallOut(chartInstance, c10_tmp), false);
  return c10_mxVal;
}

static const mxArray *c10_f_sf_marshallOut(void *chartInstanceVoid, void
  *c10_inData)
{
  const mxArray *c10_mxArrayOutData;
  int32_T c10_i13;
  c10_Player c10_b_inData[6];
  int32_T c10_i14;
  c10_Player c10_u[6];
  const mxArray *c10_y = NULL;
  static int32_T c10_iv2[1] = { 6 };

  int32_T c10_iv3[1];
  int32_T c10_i15;
  const c10_Player *c10_r0;
  int8_T c10_b_u;
  const mxArray *c10_b_y = NULL;
  int8_T c10_c_u;
  const mxArray *c10_c_y = NULL;
  int16_T c10_d_u;
  const mxArray *c10_d_y = NULL;
  uint8_T c10_e_u;
  const mxArray *c10_e_y = NULL;
  uint8_T c10_f_u;
  const mxArray *c10_f_y = NULL;
  uint8_T c10_g_u;
  const mxArray *c10_g_y = NULL;
  SFc10_cameraTestInstanceStruct *chartInstance;
  chartInstance = (SFc10_cameraTestInstanceStruct *)chartInstanceVoid;
  c10_mxArrayOutData = NULL;
  c10_mxArrayOutData = NULL;
  for (c10_i13 = 0; c10_i13 < 6; c10_i13++) {
    c10_b_inData[c10_i13] = (*(c10_Player (*)[6])c10_inData)[c10_i13];
  }

  for (c10_i14 = 0; c10_i14 < 6; c10_i14++) {
    c10_u[c10_i14] = c10_b_inData[c10_i14];
  }

  c10_y = NULL;
  c10_iv3[0] = c10_iv2[0];
  sf_mex_assign(&c10_y, sf_mex_createstructarray("structure", 1, c10_iv3), false);
  for (c10_i15 = 0; c10_i15 < 6; c10_i15++) {
    c10_r0 = &c10_u[c10_i15];
    c10_b_u = c10_r0->x;
    c10_b_y = NULL;
    sf_mex_assign(&c10_b_y, sf_mex_create("y", &c10_b_u, 2, 0U, 0U, 0U, 0),
                  false);
    sf_mex_addfield(c10_y, c10_b_y, "x", "x", c10_i15);
    c10_c_u = c10_r0->y;
    c10_c_y = NULL;
    sf_mex_assign(&c10_c_y, sf_mex_create("y", &c10_c_u, 2, 0U, 0U, 0U, 0),
                  false);
    sf_mex_addfield(c10_y, c10_c_y, "y", "y", c10_i15);
    c10_d_u = c10_r0->orientation;
    c10_d_y = NULL;
    sf_mex_assign(&c10_d_y, sf_mex_create("y", &c10_d_u, 4, 0U, 0U, 0U, 0),
                  false);
    sf_mex_addfield(c10_y, c10_d_y, "orientation", "orientation", c10_i15);
    c10_e_u = c10_r0->color;
    c10_e_y = NULL;
    sf_mex_assign(&c10_e_y, sf_mex_create("y", &c10_e_u, 3, 0U, 0U, 0U, 0),
                  false);
    sf_mex_addfield(c10_y, c10_e_y, "color", "color", c10_i15);
    c10_f_u = c10_r0->position;
    c10_f_y = NULL;
    sf_mex_assign(&c10_f_y, sf_mex_create("y", &c10_f_u, 3, 0U, 0U, 0U, 0),
                  false);
    sf_mex_addfield(c10_y, c10_f_y, "position", "position", c10_i15);
    c10_g_u = c10_r0->valid;
    c10_g_y = NULL;
    sf_mex_assign(&c10_g_y, sf_mex_create("y", &c10_g_u, 3, 0U, 0U, 0U, 0),
                  false);
    sf_mex_addfield(c10_y, c10_g_y, "valid", "valid", c10_i15);
  }

  sf_mex_assign(&c10_mxArrayOutData, c10_y, false);
  return c10_mxArrayOutData;
}

static const mxArray *c10_ball_bus_io(void *chartInstanceVoid, void *c10_pData)
{
  const mxArray *c10_mxVal = NULL;
  c10_Ball c10_tmp;
  SFc10_cameraTestInstanceStruct *chartInstance;
  chartInstance = (SFc10_cameraTestInstanceStruct *)chartInstanceVoid;
  c10_mxVal = NULL;
  c10_tmp.x = *(int8_T *)&((char_T *)(c10_Ball *)c10_pData)[0];
  c10_tmp.y = *(int8_T *)&((char_T *)(c10_Ball *)c10_pData)[1];
  c10_tmp.valid = *(uint8_T *)&((char_T *)(c10_Ball *)c10_pData)[2];
  sf_mex_assign(&c10_mxVal, c10_g_sf_marshallOut(chartInstance, &c10_tmp), false);
  return c10_mxVal;
}

static const mxArray *c10_g_sf_marshallOut(void *chartInstanceVoid, void
  *c10_inData)
{
  const mxArray *c10_mxArrayOutData = NULL;
  c10_Ball c10_u;
  const mxArray *c10_y = NULL;
  int8_T c10_b_u;
  const mxArray *c10_b_y = NULL;
  int8_T c10_c_u;
  const mxArray *c10_c_y = NULL;
  uint8_T c10_d_u;
  const mxArray *c10_d_y = NULL;
  SFc10_cameraTestInstanceStruct *chartInstance;
  chartInstance = (SFc10_cameraTestInstanceStruct *)chartInstanceVoid;
  c10_mxArrayOutData = NULL;
  c10_u = *(c10_Ball *)c10_inData;
  c10_y = NULL;
  sf_mex_assign(&c10_y, sf_mex_createstruct("structure", 2, 1, 1), false);
  c10_b_u = c10_u.x;
  c10_b_y = NULL;
  sf_mex_assign(&c10_b_y, sf_mex_create("y", &c10_b_u, 2, 0U, 0U, 0U, 0), false);
  sf_mex_addfield(c10_y, c10_b_y, "x", "x", 0);
  c10_c_u = c10_u.y;
  c10_c_y = NULL;
  sf_mex_assign(&c10_c_y, sf_mex_create("y", &c10_c_u, 2, 0U, 0U, 0U, 0), false);
  sf_mex_addfield(c10_y, c10_c_y, "y", "y", 0);
  c10_d_u = c10_u.valid;
  c10_d_y = NULL;
  sf_mex_assign(&c10_d_y, sf_mex_create("y", &c10_d_u, 3, 0U, 0U, 0U, 0), false);
  sf_mex_addfield(c10_y, c10_d_y, "valid", "valid", 0);
  sf_mex_assign(&c10_mxArrayOutData, c10_y, false);
  return c10_mxArrayOutData;
}

static void c10_g_emlrt_marshallIn(SFc10_cameraTestInstanceStruct *chartInstance,
  const mxArray *c10_b_dataWrittenToVector, const char_T *c10_identifier,
  boolean_T c10_y[4])
{
  emlrtMsgIdentifier c10_thisId;
  c10_thisId.fIdentifier = c10_identifier;
  c10_thisId.fParent = NULL;
  c10_h_emlrt_marshallIn(chartInstance, sf_mex_dup(c10_b_dataWrittenToVector),
    &c10_thisId, c10_y);
  sf_mex_destroy(&c10_b_dataWrittenToVector);
}

static void c10_h_emlrt_marshallIn(SFc10_cameraTestInstanceStruct *chartInstance,
  const mxArray *c10_u, const emlrtMsgIdentifier *c10_parentId, boolean_T c10_y
  [4])
{
  boolean_T c10_bv1[4];
  int32_T c10_i16;
  (void)chartInstance;
  sf_mex_import(c10_parentId, sf_mex_dup(c10_u), c10_bv1, 1, 11, 0U, 1, 0U, 1, 4);
  for (c10_i16 = 0; c10_i16 < 4; c10_i16++) {
    c10_y[c10_i16] = c10_bv1[c10_i16];
  }

  sf_mex_destroy(&c10_u);
}

static const mxArray *c10_i_emlrt_marshallIn(SFc10_cameraTestInstanceStruct
  *chartInstance, const mxArray *c10_b_setSimStateSideEffectsInfo, const char_T *
  c10_identifier)
{
  const mxArray *c10_y = NULL;
  emlrtMsgIdentifier c10_thisId;
  c10_y = NULL;
  c10_thisId.fIdentifier = c10_identifier;
  c10_thisId.fParent = NULL;
  sf_mex_assign(&c10_y, c10_j_emlrt_marshallIn(chartInstance, sf_mex_dup
    (c10_b_setSimStateSideEffectsInfo), &c10_thisId), false);
  sf_mex_destroy(&c10_b_setSimStateSideEffectsInfo);
  return c10_y;
}

static const mxArray *c10_j_emlrt_marshallIn(SFc10_cameraTestInstanceStruct
  *chartInstance, const mxArray *c10_u, const emlrtMsgIdentifier *c10_parentId)
{
  const mxArray *c10_y = NULL;
  (void)chartInstance;
  (void)c10_parentId;
  c10_y = NULL;
  sf_mex_assign(&c10_y, sf_mex_duplicatearraysafe(&c10_u), false);
  sf_mex_destroy(&c10_u);
  return c10_y;
}

static void c10_updateDataWrittenToVector(SFc10_cameraTestInstanceStruct
  *chartInstance, uint32_T c10_vectorIndex)
{
  chartInstance->c10_dataWrittenToVector[(uint32_T)_SFD_EML_ARRAY_BOUNDS_CHECK
    (0U, (int32_T)c10_vectorIndex, 0, 3, 1, 0)] = true;
}

static void c10_errorIfDataNotWrittenToFcn(SFc10_cameraTestInstanceStruct
  *chartInstance, uint32_T c10_vectorIndex, uint32_T c10_dataNumber, uint32_T
  c10_ssIdOfSourceObject, int32_T c10_offsetInSourceObject, int32_T
  c10_lengthInSourceObject)
{
  (void)c10_ssIdOfSourceObject;
  (void)c10_offsetInSourceObject;
  (void)c10_lengthInSourceObject;
  if (!chartInstance->c10_dataWrittenToVector[(uint32_T)
      _SFD_EML_ARRAY_BOUNDS_CHECK(0U, (int32_T)c10_vectorIndex, 0, 3, 1, 0)]) {
    _SFD_DATA_READ_BEFORE_WRITE_ERROR(c10_dataNumber);
  }
}

static void init_dsm_address_info(SFc10_cameraTestInstanceStruct *chartInstance)
{
  (void)chartInstance;
}

static void init_simulink_io_address(SFc10_cameraTestInstanceStruct
  *chartInstance)
{
  chartInstance->c10_sfEvent = (int32_T *)ssGetDWork_wrapper(chartInstance->S, 0);
  chartInstance->c10_isStable = (boolean_T *)ssGetDWork_wrapper(chartInstance->S,
    1);
  chartInstance->c10_is_active_c10_cameraTest = (uint8_T *)ssGetDWork_wrapper
    (chartInstance->S, 2);
  chartInstance->c10_is_c10_cameraTest = (uint8_T *)ssGetDWork_wrapper
    (chartInstance->S, 3);
  chartInstance->c10_players = (c10_Player (*)[6])ssGetInputPortSignal_wrapper
    (chartInstance->S, 0);
  chartInstance->c10_ball = (c10_Ball *)ssGetInputPortSignal_wrapper
    (chartInstance->S, 1);
  chartInstance->c10_gameOn = (uint8_T *)ssGetOutputPortSignal_wrapper
    (chartInstance->S, 1);
  chartInstance->c10_idleTicks = (uint8_T *)ssGetDWork_wrapper(chartInstance->S,
    4);
}

/* SFunction Glue Code */
#ifdef utFree
#undef utFree
#endif

#ifdef utMalloc
#undef utMalloc
#endif

#ifdef __cplusplus

extern "C" void *utMalloc(size_t size);
extern "C" void utFree(void*);

#else

extern void *utMalloc(size_t size);
extern void utFree(void*);

#endif

static uint32_T* sf_get_sfun_dwork_checksum(void);
void sf_c10_cameraTest_get_check_sum(mxArray *plhs[])
{
  ((real_T *)mxGetPr((plhs[0])))[0] = (real_T)(284600U);
  ((real_T *)mxGetPr((plhs[0])))[1] = (real_T)(297073182U);
  ((real_T *)mxGetPr((plhs[0])))[2] = (real_T)(1254950876U);
  ((real_T *)mxGetPr((plhs[0])))[3] = (real_T)(2468141013U);
}

mxArray* sf_c10_cameraTest_get_post_codegen_info(void);
mxArray *sf_c10_cameraTest_get_autoinheritance_info(void)
{
  const char *autoinheritanceFields[] = { "checksum", "inputs", "parameters",
    "outputs", "locals", "postCodegenInfo" };

  mxArray *mxAutoinheritanceInfo = mxCreateStructMatrix(1, 1, sizeof
    (autoinheritanceFields)/sizeof(autoinheritanceFields[0]),
    autoinheritanceFields);

  {
    mxArray *mxChecksum = mxCreateString("5GCUruyFPNfSoASjim60bB");
    mxSetField(mxAutoinheritanceInfo,0,"checksum",mxChecksum);
  }

  {
    const char *dataFields[] = { "size", "type", "complexity" };

    mxArray *mxData = mxCreateStructMatrix(1,2,3,dataFields);

    {
      mxArray *mxSize = mxCreateDoubleMatrix(1,2,mxREAL);
      double *pr = mxGetPr(mxSize);
      pr[0] = (double)(6);
      pr[1] = (double)(1);
      mxSetField(mxData,0,"size",mxSize);
    }

    {
      const char *typeFields[] = { "base", "fixpt" };

      mxArray *mxType = mxCreateStructMatrix(1,1,2,typeFields);
      mxSetField(mxType,0,"base",mxCreateDoubleScalar(13));
      mxSetField(mxType,0,"fixpt",mxCreateDoubleMatrix(0,0,mxREAL));
      mxSetField(mxData,0,"type",mxType);
    }

    mxSetField(mxData,0,"complexity",mxCreateDoubleScalar(0));

    {
      mxArray *mxSize = mxCreateDoubleMatrix(1,2,mxREAL);
      double *pr = mxGetPr(mxSize);
      pr[0] = (double)(1);
      pr[1] = (double)(1);
      mxSetField(mxData,1,"size",mxSize);
    }

    {
      const char *typeFields[] = { "base", "fixpt" };

      mxArray *mxType = mxCreateStructMatrix(1,1,2,typeFields);
      mxSetField(mxType,0,"base",mxCreateDoubleScalar(13));
      mxSetField(mxType,0,"fixpt",mxCreateDoubleMatrix(0,0,mxREAL));
      mxSetField(mxData,1,"type",mxType);
    }

    mxSetField(mxData,1,"complexity",mxCreateDoubleScalar(0));
    mxSetField(mxAutoinheritanceInfo,0,"inputs",mxData);
  }

  {
    mxSetField(mxAutoinheritanceInfo,0,"parameters",mxCreateDoubleMatrix(0,0,
                mxREAL));
  }

  {
    const char *dataFields[] = { "size", "type", "complexity" };

    mxArray *mxData = mxCreateStructMatrix(1,1,3,dataFields);

    {
      mxArray *mxSize = mxCreateDoubleMatrix(1,2,mxREAL);
      double *pr = mxGetPr(mxSize);
      pr[0] = (double)(1);
      pr[1] = (double)(1);
      mxSetField(mxData,0,"size",mxSize);
    }

    {
      const char *typeFields[] = { "base", "fixpt" };

      mxArray *mxType = mxCreateStructMatrix(1,1,2,typeFields);
      mxSetField(mxType,0,"base",mxCreateDoubleScalar(3));
      mxSetField(mxType,0,"fixpt",mxCreateDoubleMatrix(0,0,mxREAL));
      mxSetField(mxData,0,"type",mxType);
    }

    mxSetField(mxData,0,"complexity",mxCreateDoubleScalar(0));
    mxSetField(mxAutoinheritanceInfo,0,"outputs",mxData);
  }

  {
    const char *dataFields[] = { "size", "type", "complexity" };

    mxArray *mxData = mxCreateStructMatrix(1,1,3,dataFields);

    {
      mxArray *mxSize = mxCreateDoubleMatrix(1,2,mxREAL);
      double *pr = mxGetPr(mxSize);
      pr[0] = (double)(1);
      pr[1] = (double)(1);
      mxSetField(mxData,0,"size",mxSize);
    }

    {
      const char *typeFields[] = { "base", "fixpt" };

      mxArray *mxType = mxCreateStructMatrix(1,1,2,typeFields);
      mxSetField(mxType,0,"base",mxCreateDoubleScalar(3));
      mxSetField(mxType,0,"fixpt",mxCreateDoubleMatrix(0,0,mxREAL));
      mxSetField(mxData,0,"type",mxType);
    }

    mxSetField(mxData,0,"complexity",mxCreateDoubleScalar(0));
    mxSetField(mxAutoinheritanceInfo,0,"locals",mxData);
  }

  {
    mxArray* mxPostCodegenInfo = sf_c10_cameraTest_get_post_codegen_info();
    mxSetField(mxAutoinheritanceInfo,0,"postCodegenInfo",mxPostCodegenInfo);
  }

  return(mxAutoinheritanceInfo);
}

mxArray *sf_c10_cameraTest_third_party_uses_info(void)
{
  mxArray * mxcell3p = mxCreateCellMatrix(1,0);
  return(mxcell3p);
}

mxArray *sf_c10_cameraTest_jit_fallback_info(void)
{
  const char *infoFields[] = { "fallbackType", "fallbackReason",
    "incompatibleSymbol", };

  mxArray *mxInfo = mxCreateStructMatrix(1, 1, 3, infoFields);
  mxArray *fallbackReason = mxCreateString("feature_off");
  mxArray *incompatibleSymbol = mxCreateString("");
  mxArray *fallbackType = mxCreateString("early");
  mxSetField(mxInfo, 0, infoFields[0], fallbackType);
  mxSetField(mxInfo, 0, infoFields[1], fallbackReason);
  mxSetField(mxInfo, 0, infoFields[2], incompatibleSymbol);
  return mxInfo;
}

mxArray *sf_c10_cameraTest_updateBuildInfo_args_info(void)
{
  mxArray *mxBIArgs = mxCreateCellMatrix(1,0);
  return mxBIArgs;
}

mxArray* sf_c10_cameraTest_get_post_codegen_info(void)
{
  const char* fieldNames[] = { "exportedFunctionsUsedByThisChart",
    "exportedFunctionsChecksum" };

  mwSize dims[2] = { 1, 1 };

  mxArray* mxPostCodegenInfo = mxCreateStructArray(2, dims, sizeof(fieldNames)/
    sizeof(fieldNames[0]), fieldNames);

  {
    mxArray* mxExportedFunctionsChecksum = mxCreateString("");
    mwSize exp_dims[2] = { 0, 1 };

    mxArray* mxExportedFunctionsUsedByThisChart = mxCreateCellArray(2, exp_dims);
    mxSetField(mxPostCodegenInfo, 0, "exportedFunctionsUsedByThisChart",
               mxExportedFunctionsUsedByThisChart);
    mxSetField(mxPostCodegenInfo, 0, "exportedFunctionsChecksum",
               mxExportedFunctionsChecksum);
  }

  return mxPostCodegenInfo;
}

static const mxArray *sf_get_sim_state_info_c10_cameraTest(void)
{
  const char *infoFields[] = { "chartChecksum", "varInfo" };

  mxArray *mxInfo = mxCreateStructMatrix(1, 1, 2, infoFields);
  const char *infoEncStr[] = {
    "100 S1x5'type','srcId','name','auxInfo'{{M[1],M[3],T\"gameOn\",},{M[3],M[13],T\"idleTicks\",},{M[8],M[0],T\"is_active_c10_cameraTest\",},{M[9],M[0],T\"is_c10_cameraTest\",},{M[15],M[0],T\"dataWrittenToVector\",}}"
  };

  mxArray *mxVarInfo = sf_mex_decode_encoded_mx_struct_array(infoEncStr, 5, 10);
  mxArray *mxChecksum = mxCreateDoubleMatrix(1, 4, mxREAL);
  sf_c10_cameraTest_get_check_sum(&mxChecksum);
  mxSetField(mxInfo, 0, infoFields[0], mxChecksum);
  mxSetField(mxInfo, 0, infoFields[1], mxVarInfo);
  return mxInfo;
}

static void chart_debug_initialization(SimStruct *S, unsigned int
  fullDebuggerInitialization)
{
  if (!sim_mode_is_rtw_gen(S)) {
    SFc10_cameraTestInstanceStruct *chartInstance;
    ChartRunTimeInfo * crtInfo = (ChartRunTimeInfo *)(ssGetUserData(S));
    ChartInfoStruct * chartInfo = (ChartInfoStruct *)(crtInfo->instanceInfo);
    chartInstance = (SFc10_cameraTestInstanceStruct *) chartInfo->chartInstance;
    if (ssIsFirstInitCond(S) && fullDebuggerInitialization==1) {
      /* do this only if simulation is starting */
      {
        unsigned int chartAlreadyPresent;
        chartAlreadyPresent = sf_debug_initialize_chart
          (sfGlobalDebugInstanceStruct,
           _cameraTestMachineNumber_,
           10,
           4,
           3,
           0,
           6,
           0,
           0,
           0,
           0,
           0,
           &(chartInstance->chartNumber),
           &(chartInstance->instanceNumber),
           (void *)S);

        /* Each instance must initialize its own list of scripts */
        init_script_number_translation(_cameraTestMachineNumber_,
          chartInstance->chartNumber,chartInstance->instanceNumber);
        if (chartAlreadyPresent==0) {
          /* this is the first instance */
          sf_debug_set_chart_disable_implicit_casting
            (sfGlobalDebugInstanceStruct,_cameraTestMachineNumber_,
             chartInstance->chartNumber,1);
          sf_debug_set_chart_event_thresholds(sfGlobalDebugInstanceStruct,
            _cameraTestMachineNumber_,
            chartInstance->chartNumber,
            0,
            0,
            0);
          _SFD_SET_DATA_PROPS(0,1,1,0,"players");
          _SFD_SET_DATA_PROPS(1,1,1,0,"ball");
          _SFD_SET_DATA_PROPS(2,2,0,1,"gameOn");
          _SFD_SET_DATA_PROPS(3,0,0,0,"idleTicks");
          _SFD_SET_DATA_PROPS(4,9,0,0,"");
          _SFD_SET_DATA_PROPS(5,9,0,0,"");
          _SFD_STATE_INFO(2,0,0);
          _SFD_STATE_INFO(3,0,0);
          _SFD_STATE_INFO(0,0,2);
          _SFD_STATE_INFO(1,0,2);
          _SFD_CH_SUBSTATE_COUNT(2);
          _SFD_CH_SUBSTATE_DECOMP(0);
          _SFD_CH_SUBSTATE_INDEX(0,2);
          _SFD_CH_SUBSTATE_INDEX(1,3);
          _SFD_ST_SUBSTATE_COUNT(2,0);
          _SFD_ST_SUBSTATE_COUNT(3,0);
        }

        _SFD_CV_INIT_CHART(2,1,0,0);

        {
          _SFD_CV_INIT_STATE(2,0,0,0,0,0,NULL,NULL);
        }

        {
          _SFD_CV_INIT_STATE(3,0,0,0,0,0,NULL,NULL);
        }

        {
          _SFD_CV_INIT_STATE(0,0,0,0,0,0,NULL,NULL);
        }

        {
          _SFD_CV_INIT_STATE(1,0,0,0,0,0,NULL,NULL);
        }

        _SFD_CV_INIT_TRANS(1,0,NULL,NULL,0,NULL);
        _SFD_CV_INIT_TRANS(0,0,NULL,NULL,0,NULL);
        _SFD_CV_INIT_TRANS(2,0,NULL,NULL,0,NULL);

        /* Initialization of MATLAB Function Model Coverage */
        _SFD_CV_INIT_EML(0,1,1,1,0,1,0,0,0,0,0);
        _SFD_CV_INIT_EML_FCN(0,0,"calcGoal",0,-1,81);
        _SFD_CV_INIT_EML_SATURATION(0,1,0,41,-1,52);
        _SFD_CV_INIT_EML_IF(0,1,0,38,55,-1,78);
        _SFD_CV_INIT_EML_RELATIONAL(0,1,0,41,55,-1,4);
        _SFD_CV_INIT_EML(1,1,1,1,0,3,0,1,0,2,1);
        _SFD_CV_INIT_EML_FCN(1,0,"calcReady",0,-1,203);
        _SFD_CV_INIT_EML_SATURATION(1,1,0,101,-1,128);
        _SFD_CV_INIT_EML_SATURATION(1,1,1,135,-1,153);
        _SFD_CV_INIT_EML_SATURATION(1,1,2,97,-1,129);
        _SFD_CV_INIT_EML_IF(1,1,0,93,156,-1,-2);
        _SFD_CV_INIT_EML_FOR(1,1,0,80,91,202);

        {
          static int condStart[] = { 97, 135 };

          static int condEnd[] = { 131, 156 };

          static int pfixExpr[] = { 0, 1, -3 };

          _SFD_CV_INIT_EML_MCDC(1,1,0,97,156,2,0,&(condStart[0]),&(condEnd[0]),3,
                                &(pfixExpr[0]));
        }

        _SFD_CV_INIT_EML_RELATIONAL(1,1,0,97,131,-1,4);
        _SFD_CV_INIT_EML_RELATIONAL(1,1,1,135,156,-1,4);
        _SFD_CV_INIT_EML(3,1,0,0,0,1,0,0,0,0,0);
        _SFD_CV_INIT_EML_SATURATION(3,1,0,63,-1,82);
        _SFD_CV_INIT_EML(2,1,0,0,0,0,0,0,0,0,0);
        _SFD_CV_INIT_EML(0,0,0,1,0,0,0,0,0,0,0);
        _SFD_CV_INIT_EML_IF(0,0,0,1,19,1,19);
        _SFD_CV_INIT_EML_RELATIONAL(0,0,0,1,19,0,0);
        _SFD_CV_INIT_EML(2,0,0,1,0,0,0,0,0,0,0);
        _SFD_CV_INIT_EML_IF(2,0,0,1,20,1,20);
        _SFD_CV_INIT_EML_RELATIONAL(2,0,0,1,20,0,4);

        {
          unsigned int dimVector[1];
          dimVector[0]= 6;
          _SFD_SET_DATA_COMPILED_PROPS(0,SF_STRUCT,1,&(dimVector[0]),0,0,0,0.0,
            1.0,0,0,(MexFcnForType)c10_players_bus_io,(MexInFcnForType)NULL);
        }

        _SFD_SET_DATA_COMPILED_PROPS(1,SF_STRUCT,0,NULL,0,0,0,0.0,1.0,0,0,
          (MexFcnForType)c10_ball_bus_io,(MexInFcnForType)NULL);
        _SFD_SET_DATA_COMPILED_PROPS(2,SF_UINT8,0,NULL,0,0,0,0.0,1.0,0,0,
          (MexFcnForType)c10_b_sf_marshallOut,(MexInFcnForType)
          c10_b_sf_marshallIn);
        _SFD_SET_DATA_COMPILED_PROPS(3,SF_UINT8,0,NULL,0,0,0,0.0,1.0,0,0,
          (MexFcnForType)c10_b_sf_marshallOut,(MexInFcnForType)
          c10_b_sf_marshallIn);

        {
          unsigned int dimVector[1];
          dimVector[0]= 4294967295;
          _SFD_SET_DATA_COMPILED_PROPS(4,SF_DOUBLE,1,&(dimVector[0]),0,0,0,0.0,
            1.0,0,0,(MexFcnForType)NULL,(MexInFcnForType)NULL);
        }

        {
          unsigned int dimVector[1];
          dimVector[0]= 4294967295;
          _SFD_SET_DATA_COMPILED_PROPS(5,SF_DOUBLE,1,&(dimVector[0]),0,0,0,0.0,
            1.0,0,0,(MexFcnForType)NULL,(MexInFcnForType)NULL);
        }

        _SFD_SET_DATA_VALUE_PTR(4,(void *)(NULL));
        _SFD_SET_DATA_VALUE_PTR(5,(void *)(NULL));
        _SFD_SET_DATA_VALUE_PTR(0U, *chartInstance->c10_players);
        _SFD_SET_DATA_VALUE_PTR(1U, chartInstance->c10_ball);
        _SFD_SET_DATA_VALUE_PTR(2U, chartInstance->c10_gameOn);
        _SFD_SET_DATA_VALUE_PTR(3U, chartInstance->c10_idleTicks);
      }
    } else {
      sf_debug_reset_current_state_configuration(sfGlobalDebugInstanceStruct,
        _cameraTestMachineNumber_,chartInstance->chartNumber,
        chartInstance->instanceNumber);
    }
  }
}

static const char* sf_get_instance_specialization(void)
{
  return "3lr4VjdexkDRLGej5QFKzF";
}

static void sf_check_dwork_consistency(SimStruct *S)
{
  if (sim_mode_is_rtw_gen(S) || sim_mode_is_external(S)) {
    const uint32_T *sfunDWorkChecksum = sf_get_sfun_dwork_checksum();
    mxArray *infoStruct = load_cameraTest_optimization_info();
    mxArray* mxRTWDWorkChecksum = sf_get_dwork_info_from_mat_file(S,
      sf_get_instance_specialization(), infoStruct, 10, "dworkChecksum");
    if (mxRTWDWorkChecksum != NULL) {
      double *pr = mxGetPr(mxRTWDWorkChecksum);
      if ((uint32_T)pr[0] != sfunDWorkChecksum[0] ||
          (uint32_T)pr[1] != sfunDWorkChecksum[1] ||
          (uint32_T)pr[2] != sfunDWorkChecksum[2] ||
          (uint32_T)pr[3] != sfunDWorkChecksum[3]) {
        sf_mex_error_message("Code generation and simulation targets registered different sets of persistent variables for the block. "
                             "External or Rapid Accelerator mode simulation requires code generation and simulation targets to "
                             "register the same set of persistent variables for this block. "
                             "This discrepancy is typically caused by MATLAB functions that have different code paths for "
                             "simulation and code generation targets where these code paths define different sets of persistent variables. "
                             "Please identify these code paths in the offending block and rewrite the MATLAB code so that "
                             "the set of persistent variables is the same between simulation and code generation.");
      }
    }
  }
}

static void sf_opaque_initialize_c10_cameraTest(void *chartInstanceVar)
{
  sf_check_dwork_consistency(((SFc10_cameraTestInstanceStruct*) chartInstanceVar)
    ->S);
  chart_debug_initialization(((SFc10_cameraTestInstanceStruct*) chartInstanceVar)
    ->S,0);
  initialize_params_c10_cameraTest((SFc10_cameraTestInstanceStruct*)
    chartInstanceVar);
  initialize_c10_cameraTest((SFc10_cameraTestInstanceStruct*) chartInstanceVar);
}

static void sf_opaque_enable_c10_cameraTest(void *chartInstanceVar)
{
  enable_c10_cameraTest((SFc10_cameraTestInstanceStruct*) chartInstanceVar);
}

static void sf_opaque_disable_c10_cameraTest(void *chartInstanceVar)
{
  disable_c10_cameraTest((SFc10_cameraTestInstanceStruct*) chartInstanceVar);
}

static void sf_opaque_gateway_c10_cameraTest(void *chartInstanceVar)
{
  sf_gateway_c10_cameraTest((SFc10_cameraTestInstanceStruct*) chartInstanceVar);
}

static void sf_opaque_ext_mode_exec_c10_cameraTest(void *chartInstanceVar)
{
  ext_mode_exec_c10_cameraTest((SFc10_cameraTestInstanceStruct*)
    chartInstanceVar);
}

static const mxArray* sf_opaque_get_sim_state_c10_cameraTest(SimStruct* S)
{
  ChartRunTimeInfo * crtInfo = (ChartRunTimeInfo *)(ssGetUserData(S));
  ChartInfoStruct * chartInfo = (ChartInfoStruct *)(crtInfo->instanceInfo);
  return get_sim_state_c10_cameraTest((SFc10_cameraTestInstanceStruct*)
    chartInfo->chartInstance);         /* raw sim ctx */
}

static void sf_opaque_set_sim_state_c10_cameraTest(SimStruct* S, const mxArray
  *st)
{
  ChartRunTimeInfo * crtInfo = (ChartRunTimeInfo *)(ssGetUserData(S));
  ChartInfoStruct * chartInfo = (ChartInfoStruct *)(crtInfo->instanceInfo);
  set_sim_state_c10_cameraTest((SFc10_cameraTestInstanceStruct*)
    chartInfo->chartInstance, st);
}

static void sf_opaque_terminate_c10_cameraTest(void *chartInstanceVar)
{
  if (chartInstanceVar!=NULL) {
    SimStruct *S = ((SFc10_cameraTestInstanceStruct*) chartInstanceVar)->S;
    ChartRunTimeInfo * crtInfo = (ChartRunTimeInfo *)(ssGetUserData(S));
    if (sim_mode_is_rtw_gen(S) || sim_mode_is_external(S)) {
      sf_clear_rtw_identifier(S);
      unload_cameraTest_optimization_info();
    }

    finalize_c10_cameraTest((SFc10_cameraTestInstanceStruct*) chartInstanceVar);
    utFree(chartInstanceVar);
    if (crtInfo != NULL) {
      utFree(crtInfo);
    }

    ssSetUserData(S,NULL);
  }
}

static void sf_opaque_init_subchart_simstructs(void *chartInstanceVar)
{
  initSimStructsc10_cameraTest((SFc10_cameraTestInstanceStruct*)
    chartInstanceVar);
}

extern unsigned int sf_machine_global_initializer_called(void);
static void mdlProcessParameters_c10_cameraTest(SimStruct *S)
{
  int i;
  for (i=0;i<ssGetNumRunTimeParams(S);i++) {
    if (ssGetSFcnParamTunable(S,i)) {
      ssUpdateDlgParamAsRunTimeParam(S,i);
    }
  }

  if (sf_machine_global_initializer_called()) {
    ChartRunTimeInfo * crtInfo = (ChartRunTimeInfo *)(ssGetUserData(S));
    ChartInfoStruct * chartInfo = (ChartInfoStruct *)(crtInfo->instanceInfo);
    initialize_params_c10_cameraTest((SFc10_cameraTestInstanceStruct*)
      (chartInfo->chartInstance));
  }
}

mxArray *sf_c10_cameraTest_get_testpoint_info(void)
{
  const char *infoEncStr[] = {
    "100 S1x2'varName','path'{{T\"is_active_c10_cameraTest\",T\"is_active_c10_cameraTest\"},{T\"is_c10_cameraTest\",T\"is_c10_cameraTest\"}}"
  };

  mxArray *mxTpInfo = sf_mex_decode_encoded_mx_struct_array(infoEncStr, 2, 10);
  return mxTpInfo;
}

static void sf_set_sfun_dwork_info(SimStruct *S)
{
  const char *dworkEncStr[] = {
    "100 S1x5'type','isSigned','wordLength','bias','slope','exponent','isScaledDouble','isComplex','size'{{T\"int32\",,,,,,,M[0],M[]},{T\"boolean\",,,,,,,M[0],M[]},{T\"uint8\",,,,,,,M[0],M[]},{T\"uint8\",,,,,,,M[0],M[]},{T\"uint8\",,,,,,,M[0],M[]}}"
  };

  sf_set_encoded_dwork_info(S, dworkEncStr, 5, 10);
}

static uint32_T* sf_get_sfun_dwork_checksum()
{
  static uint32_T checksum[4] = { 3377120800U, 631791034U, 2310890518U,
    96930596U };

  return checksum;
}

static void mdlSetWorkWidths_c10_cameraTest(SimStruct *S)
{
  if (sim_mode_is_rtw_gen(S) || sim_mode_is_external(S)) {
    mxArray *infoStruct = load_cameraTest_optimization_info();
    int_T chartIsInlinable =
      (int_T)sf_is_chart_inlinable(sf_get_instance_specialization(),infoStruct,
      10);
    ssSetStateflowIsInlinable(S,chartIsInlinable);
    ssSetRTWCG(S,sf_rtw_info_uint_prop(sf_get_instance_specialization(),
                infoStruct,10,"RTWCG"));
    ssSetEnableFcnIsTrivial(S,1);
    ssSetDisableFcnIsTrivial(S,1);
    ssSetNotMultipleInlinable(S,sf_rtw_info_uint_prop
      (sf_get_instance_specialization(),infoStruct,10,
       "gatewayCannotBeInlinedMultipleTimes"));
    sf_update_buildInfo(sf_get_instance_specialization(),infoStruct,10);
    if (chartIsInlinable) {
      ssSetInputPortOptimOpts(S, 0, SS_REUSABLE_AND_LOCAL);
      ssSetInputPortOptimOpts(S, 1, SS_REUSABLE_AND_LOCAL);
      sf_mark_chart_expressionable_inputs(S,sf_get_instance_specialization(),
        infoStruct,10,2);
      sf_mark_chart_reusable_outputs(S,sf_get_instance_specialization(),
        infoStruct,10,1);
    }

    {
      unsigned int outPortIdx;
      for (outPortIdx=1; outPortIdx<=1; ++outPortIdx) {
        ssSetOutputPortOptimizeInIR(S, outPortIdx, 1U);
      }
    }

    {
      unsigned int inPortIdx;
      for (inPortIdx=0; inPortIdx < 2; ++inPortIdx) {
        ssSetInputPortOptimizeInIR(S, inPortIdx, 1U);
      }
    }

    sf_set_rtw_dwork_info(S,sf_get_instance_specialization(),infoStruct,10);
    ssSetHasSubFunctions(S,!(chartIsInlinable));
  } else {
    sf_set_sfun_dwork_info(S);
  }

  ssSetOptions(S,ssGetOptions(S)|SS_OPTION_WORKS_WITH_CODE_REUSE);
  ssSetChecksum0(S,(3637724364U));
  ssSetChecksum1(S,(1607616551U));
  ssSetChecksum2(S,(3158677079U));
  ssSetChecksum3(S,(1812867276U));
  ssSetmdlDerivatives(S, NULL);
  ssSetExplicitFCSSCtrl(S,1);
  ssSupportsMultipleExecInstances(S,1);
}

static void mdlRTW_c10_cameraTest(SimStruct *S)
{
  if (sim_mode_is_rtw_gen(S)) {
    ssWriteRTWStrParam(S, "StateflowChartType", "Stateflow");
  }
}

static void mdlStart_c10_cameraTest(SimStruct *S)
{
  SFc10_cameraTestInstanceStruct *chartInstance;
  ChartRunTimeInfo * crtInfo = (ChartRunTimeInfo *)utMalloc(sizeof
    (ChartRunTimeInfo));
  chartInstance = (SFc10_cameraTestInstanceStruct *)utMalloc(sizeof
    (SFc10_cameraTestInstanceStruct));
  memset(chartInstance, 0, sizeof(SFc10_cameraTestInstanceStruct));
  if (chartInstance==NULL) {
    sf_mex_error_message("Could not allocate memory for chart instance.");
  }

  chartInstance->chartInfo.chartInstance = chartInstance;
  chartInstance->chartInfo.isEMLChart = 0;
  chartInstance->chartInfo.chartInitialized = 0;
  chartInstance->chartInfo.sFunctionGateway = sf_opaque_gateway_c10_cameraTest;
  chartInstance->chartInfo.initializeChart = sf_opaque_initialize_c10_cameraTest;
  chartInstance->chartInfo.terminateChart = sf_opaque_terminate_c10_cameraTest;
  chartInstance->chartInfo.enableChart = sf_opaque_enable_c10_cameraTest;
  chartInstance->chartInfo.disableChart = sf_opaque_disable_c10_cameraTest;
  chartInstance->chartInfo.getSimState = sf_opaque_get_sim_state_c10_cameraTest;
  chartInstance->chartInfo.setSimState = sf_opaque_set_sim_state_c10_cameraTest;
  chartInstance->chartInfo.getSimStateInfo =
    sf_get_sim_state_info_c10_cameraTest;
  chartInstance->chartInfo.zeroCrossings = NULL;
  chartInstance->chartInfo.outputs = NULL;
  chartInstance->chartInfo.derivatives = NULL;
  chartInstance->chartInfo.mdlRTW = mdlRTW_c10_cameraTest;
  chartInstance->chartInfo.mdlStart = mdlStart_c10_cameraTest;
  chartInstance->chartInfo.mdlSetWorkWidths = mdlSetWorkWidths_c10_cameraTest;
  chartInstance->chartInfo.extModeExec = sf_opaque_ext_mode_exec_c10_cameraTest;
  chartInstance->chartInfo.restoreLastMajorStepConfiguration = NULL;
  chartInstance->chartInfo.restoreBeforeLastMajorStepConfiguration = NULL;
  chartInstance->chartInfo.storeCurrentConfiguration = NULL;
  chartInstance->chartInfo.callAtomicSubchartUserFcn = NULL;
  chartInstance->chartInfo.callAtomicSubchartAutoFcn = NULL;
  chartInstance->chartInfo.debugInstance = sfGlobalDebugInstanceStruct;
  chartInstance->S = S;
  crtInfo->checksum = SF_RUNTIME_INFO_CHECKSUM;
  crtInfo->instanceInfo = (&(chartInstance->chartInfo));
  crtInfo->isJITEnabled = false;
  crtInfo->compiledInfo = NULL;
  ssSetUserData(S,(void *)(crtInfo));  /* register the chart instance with simstruct */
  init_dsm_address_info(chartInstance);
  init_simulink_io_address(chartInstance);
  if (!sim_mode_is_rtw_gen(S)) {
  }

  sf_opaque_init_subchart_simstructs(chartInstance->chartInfo.chartInstance);
  chart_debug_initialization(S,1);
}

void c10_cameraTest_method_dispatcher(SimStruct *S, int_T method, void *data)
{
  switch (method) {
   case SS_CALL_MDL_START:
    mdlStart_c10_cameraTest(S);
    break;

   case SS_CALL_MDL_SET_WORK_WIDTHS:
    mdlSetWorkWidths_c10_cameraTest(S);
    break;

   case SS_CALL_MDL_PROCESS_PARAMETERS:
    mdlProcessParameters_c10_cameraTest(S);
    break;

   default:
    /* Unhandled method */
    sf_mex_error_message("Stateflow Internal Error:\n"
                         "Error calling c10_cameraTest_method_dispatcher.\n"
                         "Can't handle method %d.\n", method);
    break;
  }
}

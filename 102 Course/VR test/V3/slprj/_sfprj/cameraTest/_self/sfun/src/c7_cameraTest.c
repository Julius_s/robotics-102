/* Include files */

#include <stddef.h>
#include "blas.h"
#include "cameraTest_sfun.h"
#include "c7_cameraTest.h"
#define CHARTINSTANCE_CHARTNUMBER      (chartInstance->chartNumber)
#define CHARTINSTANCE_INSTANCENUMBER   (chartInstance->instanceNumber)
#include "cameraTest_sfun_debug_macros.h"
#define _SF_MEX_LISTEN_FOR_CTRL_C(S)   sf_mex_listen_for_ctrl_c(sfGlobalDebugInstanceStruct,S);

/* Type Definitions */

/* Named Constants */
#define CALL_EVENT                     (-1)

/* Variable Declarations */

/* Variable Definitions */
static real_T _sfTime_;
static const char * c7_debug_family_names[6] = { "bytesRead", "dataRead",
  "dataLength", "nargin", "nargout", "data" };

/* Function Declarations */
static void initialize_c7_cameraTest(SFc7_cameraTestInstanceStruct
  *chartInstance);
static void initialize_params_c7_cameraTest(SFc7_cameraTestInstanceStruct
  *chartInstance);
static void enable_c7_cameraTest(SFc7_cameraTestInstanceStruct *chartInstance);
static void disable_c7_cameraTest(SFc7_cameraTestInstanceStruct *chartInstance);
static void c7_update_debugger_state_c7_cameraTest(SFc7_cameraTestInstanceStruct
  *chartInstance);
static const mxArray *get_sim_state_c7_cameraTest(SFc7_cameraTestInstanceStruct *
  chartInstance);
static void set_sim_state_c7_cameraTest(SFc7_cameraTestInstanceStruct
  *chartInstance, const mxArray *c7_st);
static void finalize_c7_cameraTest(SFc7_cameraTestInstanceStruct *chartInstance);
static void sf_gateway_c7_cameraTest(SFc7_cameraTestInstanceStruct
  *chartInstance);
static void mdl_start_c7_cameraTest(SFc7_cameraTestInstanceStruct *chartInstance);
static void initSimStructsc7_cameraTest(SFc7_cameraTestInstanceStruct
  *chartInstance);
static void init_script_number_translation(uint32_T c7_machineNumber, uint32_T
  c7_chartNumber, uint32_T c7_instanceNumber);
static const mxArray *c7_sf_marshallOut(void *chartInstanceVoid, uint8_T
  c7_inData_data[], int32_T *c7_inData_sizes);
static void c7_emlrt_marshallIn(SFc7_cameraTestInstanceStruct *chartInstance,
  const mxArray *c7_data, const char_T *c7_identifier, uint8_T c7_y_data[],
  int32_T *c7_y_sizes);
static void c7_b_emlrt_marshallIn(SFc7_cameraTestInstanceStruct *chartInstance,
  const mxArray *c7_u, const emlrtMsgIdentifier *c7_parentId, uint8_T c7_y_data[],
  int32_T *c7_y_sizes);
static void c7_sf_marshallIn(void *chartInstanceVoid, const mxArray
  *c7_mxArrayInData, const char_T *c7_varName, uint8_T c7_outData_data[],
  int32_T *c7_outData_sizes);
static const mxArray *c7_b_sf_marshallOut(void *chartInstanceVoid, void
  *c7_inData);
static real_T c7_c_emlrt_marshallIn(SFc7_cameraTestInstanceStruct *chartInstance,
  const mxArray *c7_u, const emlrtMsgIdentifier *c7_parentId);
static void c7_b_sf_marshallIn(void *chartInstanceVoid, const mxArray
  *c7_mxArrayInData, const char_T *c7_varName, void *c7_outData);
static const mxArray *c7_c_sf_marshallOut(void *chartInstanceVoid, void
  *c7_inData);
static const mxArray *c7_d_sf_marshallOut(void *chartInstanceVoid, void
  *c7_inData);
static void c7_d_emlrt_marshallIn(SFc7_cameraTestInstanceStruct *chartInstance,
  const mxArray *c7_u, const emlrtMsgIdentifier *c7_parentId, uint8_T c7_y[512]);
static void c7_c_sf_marshallIn(void *chartInstanceVoid, const mxArray
  *c7_mxArrayInData, const char_T *c7_varName, void *c7_outData);
static int32_T c7_e_emlrt_marshallIn(SFc7_cameraTestInstanceStruct
  *chartInstance, const mxArray *c7_u, const emlrtMsgIdentifier *c7_parentId);
static void c7_d_sf_marshallIn(void *chartInstanceVoid, const mxArray
  *c7_mxArrayInData, const char_T *c7_varName, void *c7_outData);
static uint8_T c7_f_emlrt_marshallIn(SFc7_cameraTestInstanceStruct
  *chartInstance, const mxArray *c7_b_is_active_c7_cameraTest, const char_T
  *c7_identifier);
static uint8_T c7_g_emlrt_marshallIn(SFc7_cameraTestInstanceStruct
  *chartInstance, const mxArray *c7_u, const emlrtMsgIdentifier *c7_parentId);
static int32_T c7_get_spHandle(SFc7_cameraTestInstanceStruct *chartInstance,
  uint32_T c7_elementIndex);
static void c7_set_spHandle(SFc7_cameraTestInstanceStruct *chartInstance,
  uint32_T c7_elementIndex, int32_T c7_elementValue);
static int32_T *c7_access_spHandle(SFc7_cameraTestInstanceStruct *chartInstance,
  uint32_T c7_rdOnly);
static void init_dsm_address_info(SFc7_cameraTestInstanceStruct *chartInstance);
static void init_simulink_io_address(SFc7_cameraTestInstanceStruct
  *chartInstance);

/* Function Definitions */
static void initialize_c7_cameraTest(SFc7_cameraTestInstanceStruct
  *chartInstance)
{
  chartInstance->c7_sfEvent = CALL_EVENT;
  _sfTime_ = sf_get_time(chartInstance->S);
  chartInstance->c7_is_active_c7_cameraTest = 0U;
}

static void initialize_params_c7_cameraTest(SFc7_cameraTestInstanceStruct
  *chartInstance)
{
  (void)chartInstance;
}

static void enable_c7_cameraTest(SFc7_cameraTestInstanceStruct *chartInstance)
{
  _sfTime_ = sf_get_time(chartInstance->S);
}

static void disable_c7_cameraTest(SFc7_cameraTestInstanceStruct *chartInstance)
{
  _sfTime_ = sf_get_time(chartInstance->S);
}

static void c7_update_debugger_state_c7_cameraTest(SFc7_cameraTestInstanceStruct
  *chartInstance)
{
  (void)chartInstance;
}

static const mxArray *get_sim_state_c7_cameraTest(SFc7_cameraTestInstanceStruct *
  chartInstance)
{
  const mxArray *c7_st;
  const mxArray *c7_y = NULL;
  int32_T c7_u_sizes;
  int32_T c7_loop_ub;
  int32_T c7_i0;
  uint8_T c7_u_data[512];
  const mxArray *c7_b_y = NULL;
  uint8_T c7_hoistedGlobal;
  uint8_T c7_u;
  const mxArray *c7_c_y = NULL;
  c7_st = NULL;
  c7_st = NULL;
  c7_y = NULL;
  sf_mex_assign(&c7_y, sf_mex_createcellmatrix(2, 1), false);
  c7_u_sizes = *chartInstance->c7_data_sizes;
  c7_loop_ub = *chartInstance->c7_data_sizes - 1;
  for (c7_i0 = 0; c7_i0 <= c7_loop_ub; c7_i0++) {
    c7_u_data[c7_i0] = (*chartInstance->c7_data_data)[c7_i0];
  }

  c7_b_y = NULL;
  sf_mex_assign(&c7_b_y, sf_mex_create("y", c7_u_data, 3, 0U, 1U, 0U, 1,
    c7_u_sizes), false);
  sf_mex_setcell(c7_y, 0, c7_b_y);
  c7_hoistedGlobal = chartInstance->c7_is_active_c7_cameraTest;
  c7_u = c7_hoistedGlobal;
  c7_c_y = NULL;
  sf_mex_assign(&c7_c_y, sf_mex_create("y", &c7_u, 3, 0U, 0U, 0U, 0), false);
  sf_mex_setcell(c7_y, 1, c7_c_y);
  sf_mex_assign(&c7_st, c7_y, false);
  return c7_st;
}

static void set_sim_state_c7_cameraTest(SFc7_cameraTestInstanceStruct
  *chartInstance, const mxArray *c7_st)
{
  const mxArray *c7_u;
  int32_T c7_tmp_sizes;
  uint8_T c7_tmp_data[512];
  int32_T c7_loop_ub;
  int32_T c7_i1;
  chartInstance->c7_doneDoubleBufferReInit = true;
  c7_u = sf_mex_dup(c7_st);
  c7_emlrt_marshallIn(chartInstance, sf_mex_dup(sf_mex_getcell(c7_u, 0)), "data",
                      c7_tmp_data, &c7_tmp_sizes);
  *chartInstance->c7_data_sizes = c7_tmp_sizes;
  c7_loop_ub = c7_tmp_sizes - 1;
  for (c7_i1 = 0; c7_i1 <= c7_loop_ub; c7_i1++) {
    (*chartInstance->c7_data_data)[c7_i1] = c7_tmp_data[c7_i1];
  }

  chartInstance->c7_is_active_c7_cameraTest = c7_f_emlrt_marshallIn
    (chartInstance, sf_mex_dup(sf_mex_getcell(c7_u, 1)),
     "is_active_c7_cameraTest");
  sf_mex_destroy(&c7_u);
  c7_update_debugger_state_c7_cameraTest(chartInstance);
  sf_mex_destroy(&c7_st);
}

static void finalize_c7_cameraTest(SFc7_cameraTestInstanceStruct *chartInstance)
{
  (void)chartInstance;
}

static void sf_gateway_c7_cameraTest(SFc7_cameraTestInstanceStruct
  *chartInstance)
{
  uint32_T c7_debug_family_var_map[6];
  int32_T c7_bytesRead;
  uint8_T c7_dataRead[512];
  int32_T c7_dataLength;
  real_T c7_nargin = 0.0;
  real_T c7_nargout = 1.0;
  int32_T c7_b_data_sizes;
  uint8_T c7_b_data_data[512];
  int32_T c7_i2;
  int32_T c7_loop_ub;
  int32_T c7_i3;
  int32_T c7_b_loop_ub;
  int32_T c7_i4;
  _SFD_SYMBOL_SCOPE_PUSH(0U, 0U);
  _sfTime_ = sf_get_time(chartInstance->S);
  _SFD_CC_CALL(CHART_ENTER_SFUNCTION_TAG, 2U, chartInstance->c7_sfEvent);
  chartInstance->c7_sfEvent = CALL_EVENT;
  _SFD_CC_CALL(CHART_ENTER_DURING_FUNCTION_TAG, 2U, chartInstance->c7_sfEvent);
  _SFD_SYMBOL_SCOPE_PUSH_EML(0U, 6U, 6U, c7_debug_family_names,
    c7_debug_family_var_map);
  _SFD_SYMBOL_SCOPE_ADD_EML_IMPORTABLE(&c7_bytesRead, 0U, c7_c_sf_marshallOut,
    c7_d_sf_marshallIn);
  _SFD_SYMBOL_SCOPE_ADD_EML_IMPORTABLE(c7_dataRead, 1U, c7_d_sf_marshallOut,
    c7_c_sf_marshallIn);
  _SFD_SYMBOL_SCOPE_ADD_EML(&c7_dataLength, 2U, c7_c_sf_marshallOut);
  _SFD_SYMBOL_SCOPE_ADD_EML_IMPORTABLE(&c7_nargin, 3U, c7_b_sf_marshallOut,
    c7_b_sf_marshallIn);
  _SFD_SYMBOL_SCOPE_ADD_EML_IMPORTABLE(&c7_nargout, 4U, c7_b_sf_marshallOut,
    c7_b_sf_marshallIn);
  _SFD_SYMBOL_SCOPE_ADD_EML_DYN_IMPORTABLE(c7_b_data_data, (const int32_T *)
    &c7_b_data_sizes, NULL, 0, 5, (void *)c7_sf_marshallOut, (void *)
    c7_sf_marshallIn);
  CV_EML_FCN(0, 0);
  _SFD_EML_CALL(0U, chartInstance->c7_sfEvent, 2);
  _SFD_EML_CALL(0U, chartInstance->c7_sfEvent, 3);
  c7_bytesRead = 0;
  _SFD_EML_CALL(0U, chartInstance->c7_sfEvent, 4);
  for (c7_i2 = 0; c7_i2 < 512; c7_i2++) {
    c7_dataRead[c7_i2] = 0U;
  }

  _SFD_EML_CALL(0U, chartInstance->c7_sfEvent, 5);
  c7_dataLength = 0;
  _SFD_EML_CALL(0U, chartInstance->c7_sfEvent, 6);
  CV_EML_COND(0, 1, 0, true);
  CV_EML_MCDC(0, 1, 0, false);
  CV_EML_IF(0, 1, 0, false);
  _SFD_EML_CALL(0U, chartInstance->c7_sfEvent, 14);
  _SFD_DIM_SIZE_GEQ_CHECK(512, 0, 1);
  c7_b_data_sizes = 0;
  _SFD_EML_CALL(0U, chartInstance->c7_sfEvent, -14);
  _SFD_SYMBOL_SCOPE_POP();
  *chartInstance->c7_data_sizes = c7_b_data_sizes;
  c7_loop_ub = c7_b_data_sizes - 1;
  for (c7_i3 = 0; c7_i3 <= c7_loop_ub; c7_i3++) {
    (*chartInstance->c7_data_data)[c7_i3] = c7_b_data_data[c7_i3];
  }

  _SFD_CC_CALL(EXIT_OUT_OF_FUNCTION_TAG, 2U, chartInstance->c7_sfEvent);
  _SFD_SYMBOL_SCOPE_POP();
  _SFD_CHECK_FOR_STATE_INCONSISTENCY(_cameraTestMachineNumber_,
    chartInstance->chartNumber, chartInstance->instanceNumber);
  c7_b_loop_ub = *chartInstance->c7_data_sizes - 1;
  for (c7_i4 = 0; c7_i4 <= c7_b_loop_ub; c7_i4++) {
    _SFD_DATA_RANGE_CHECK((real_T)(*chartInstance->c7_data_data)[c7_i4], 0U);
  }
}

static void mdl_start_c7_cameraTest(SFc7_cameraTestInstanceStruct *chartInstance)
{
  (void)chartInstance;
}

static void initSimStructsc7_cameraTest(SFc7_cameraTestInstanceStruct
  *chartInstance)
{
  (void)chartInstance;
}

static void init_script_number_translation(uint32_T c7_machineNumber, uint32_T
  c7_chartNumber, uint32_T c7_instanceNumber)
{
  (void)c7_machineNumber;
  (void)c7_chartNumber;
  (void)c7_instanceNumber;
}

static const mxArray *c7_sf_marshallOut(void *chartInstanceVoid, uint8_T
  c7_inData_data[], int32_T *c7_inData_sizes)
{
  const mxArray *c7_mxArrayOutData = NULL;
  int32_T c7_b_inData_sizes;
  int32_T c7_loop_ub;
  int32_T c7_i5;
  uint8_T c7_b_inData_data[512];
  int32_T c7_u_sizes;
  int32_T c7_b_loop_ub;
  int32_T c7_i6;
  uint8_T c7_u_data[512];
  const mxArray *c7_y = NULL;
  SFc7_cameraTestInstanceStruct *chartInstance;
  chartInstance = (SFc7_cameraTestInstanceStruct *)chartInstanceVoid;
  c7_mxArrayOutData = NULL;
  c7_b_inData_sizes = *c7_inData_sizes;
  c7_loop_ub = *c7_inData_sizes - 1;
  for (c7_i5 = 0; c7_i5 <= c7_loop_ub; c7_i5++) {
    c7_b_inData_data[c7_i5] = c7_inData_data[c7_i5];
  }

  c7_u_sizes = c7_b_inData_sizes;
  c7_b_loop_ub = c7_b_inData_sizes - 1;
  for (c7_i6 = 0; c7_i6 <= c7_b_loop_ub; c7_i6++) {
    c7_u_data[c7_i6] = c7_b_inData_data[c7_i6];
  }

  c7_y = NULL;
  sf_mex_assign(&c7_y, sf_mex_create("y", c7_u_data, 3, 0U, 1U, 0U, 1,
    c7_u_sizes), false);
  sf_mex_assign(&c7_mxArrayOutData, c7_y, false);
  return c7_mxArrayOutData;
}

static void c7_emlrt_marshallIn(SFc7_cameraTestInstanceStruct *chartInstance,
  const mxArray *c7_data, const char_T *c7_identifier, uint8_T c7_y_data[],
  int32_T *c7_y_sizes)
{
  emlrtMsgIdentifier c7_thisId;
  c7_thisId.fIdentifier = c7_identifier;
  c7_thisId.fParent = NULL;
  c7_b_emlrt_marshallIn(chartInstance, sf_mex_dup(c7_data), &c7_thisId,
                        c7_y_data, c7_y_sizes);
  sf_mex_destroy(&c7_data);
}

static void c7_b_emlrt_marshallIn(SFc7_cameraTestInstanceStruct *chartInstance,
  const mxArray *c7_u, const emlrtMsgIdentifier *c7_parentId, uint8_T c7_y_data[],
  int32_T *c7_y_sizes)
{
  static uint32_T c7_uv0[1] = { 512U };

  uint32_T c7_uv1[1];
  static boolean_T c7_bv0[1] = { true };

  boolean_T c7_bv1[1];
  int32_T c7_tmp_sizes;
  uint8_T c7_tmp_data[512];
  int32_T c7_loop_ub;
  int32_T c7_i7;
  (void)chartInstance;
  c7_uv1[0] = c7_uv0[0];
  c7_bv1[0] = c7_bv0[0];
  sf_mex_import_vs(c7_parentId, sf_mex_dup(c7_u), c7_tmp_data, 1, 3, 0U, 1, 0U,
                   1, c7_bv1, c7_uv1, &c7_tmp_sizes);
  *c7_y_sizes = c7_tmp_sizes;
  c7_loop_ub = c7_tmp_sizes - 1;
  for (c7_i7 = 0; c7_i7 <= c7_loop_ub; c7_i7++) {
    c7_y_data[c7_i7] = c7_tmp_data[c7_i7];
  }

  sf_mex_destroy(&c7_u);
}

static void c7_sf_marshallIn(void *chartInstanceVoid, const mxArray
  *c7_mxArrayInData, const char_T *c7_varName, uint8_T c7_outData_data[],
  int32_T *c7_outData_sizes)
{
  const mxArray *c7_data;
  const char_T *c7_identifier;
  emlrtMsgIdentifier c7_thisId;
  int32_T c7_y_sizes;
  uint8_T c7_y_data[512];
  int32_T c7_loop_ub;
  int32_T c7_i8;
  SFc7_cameraTestInstanceStruct *chartInstance;
  chartInstance = (SFc7_cameraTestInstanceStruct *)chartInstanceVoid;
  c7_data = sf_mex_dup(c7_mxArrayInData);
  c7_identifier = c7_varName;
  c7_thisId.fIdentifier = c7_identifier;
  c7_thisId.fParent = NULL;
  c7_b_emlrt_marshallIn(chartInstance, sf_mex_dup(c7_data), &c7_thisId,
                        c7_y_data, &c7_y_sizes);
  sf_mex_destroy(&c7_data);
  *c7_outData_sizes = c7_y_sizes;
  c7_loop_ub = c7_y_sizes - 1;
  for (c7_i8 = 0; c7_i8 <= c7_loop_ub; c7_i8++) {
    c7_outData_data[c7_i8] = c7_y_data[c7_i8];
  }

  sf_mex_destroy(&c7_mxArrayInData);
}

static const mxArray *c7_b_sf_marshallOut(void *chartInstanceVoid, void
  *c7_inData)
{
  const mxArray *c7_mxArrayOutData = NULL;
  real_T c7_u;
  const mxArray *c7_y = NULL;
  SFc7_cameraTestInstanceStruct *chartInstance;
  chartInstance = (SFc7_cameraTestInstanceStruct *)chartInstanceVoid;
  c7_mxArrayOutData = NULL;
  c7_u = *(real_T *)c7_inData;
  c7_y = NULL;
  sf_mex_assign(&c7_y, sf_mex_create("y", &c7_u, 0, 0U, 0U, 0U, 0), false);
  sf_mex_assign(&c7_mxArrayOutData, c7_y, false);
  return c7_mxArrayOutData;
}

static real_T c7_c_emlrt_marshallIn(SFc7_cameraTestInstanceStruct *chartInstance,
  const mxArray *c7_u, const emlrtMsgIdentifier *c7_parentId)
{
  real_T c7_y;
  real_T c7_d0;
  (void)chartInstance;
  sf_mex_import(c7_parentId, sf_mex_dup(c7_u), &c7_d0, 1, 0, 0U, 0, 0U, 0);
  c7_y = c7_d0;
  sf_mex_destroy(&c7_u);
  return c7_y;
}

static void c7_b_sf_marshallIn(void *chartInstanceVoid, const mxArray
  *c7_mxArrayInData, const char_T *c7_varName, void *c7_outData)
{
  const mxArray *c7_nargout;
  const char_T *c7_identifier;
  emlrtMsgIdentifier c7_thisId;
  real_T c7_y;
  SFc7_cameraTestInstanceStruct *chartInstance;
  chartInstance = (SFc7_cameraTestInstanceStruct *)chartInstanceVoid;
  c7_nargout = sf_mex_dup(c7_mxArrayInData);
  c7_identifier = c7_varName;
  c7_thisId.fIdentifier = c7_identifier;
  c7_thisId.fParent = NULL;
  c7_y = c7_c_emlrt_marshallIn(chartInstance, sf_mex_dup(c7_nargout), &c7_thisId);
  sf_mex_destroy(&c7_nargout);
  *(real_T *)c7_outData = c7_y;
  sf_mex_destroy(&c7_mxArrayInData);
}

static const mxArray *c7_c_sf_marshallOut(void *chartInstanceVoid, void
  *c7_inData)
{
  const mxArray *c7_mxArrayOutData = NULL;
  int32_T c7_u;
  const mxArray *c7_y = NULL;
  SFc7_cameraTestInstanceStruct *chartInstance;
  chartInstance = (SFc7_cameraTestInstanceStruct *)chartInstanceVoid;
  c7_mxArrayOutData = NULL;
  c7_u = *(int32_T *)c7_inData;
  c7_y = NULL;
  sf_mex_assign(&c7_y, sf_mex_create("y", &c7_u, 6, 0U, 0U, 0U, 0), false);
  sf_mex_assign(&c7_mxArrayOutData, c7_y, false);
  return c7_mxArrayOutData;
}

static const mxArray *c7_d_sf_marshallOut(void *chartInstanceVoid, void
  *c7_inData)
{
  const mxArray *c7_mxArrayOutData = NULL;
  int32_T c7_i9;
  uint8_T c7_b_inData[512];
  int32_T c7_i10;
  uint8_T c7_u[512];
  const mxArray *c7_y = NULL;
  SFc7_cameraTestInstanceStruct *chartInstance;
  chartInstance = (SFc7_cameraTestInstanceStruct *)chartInstanceVoid;
  c7_mxArrayOutData = NULL;
  for (c7_i9 = 0; c7_i9 < 512; c7_i9++) {
    c7_b_inData[c7_i9] = (*(uint8_T (*)[512])c7_inData)[c7_i9];
  }

  for (c7_i10 = 0; c7_i10 < 512; c7_i10++) {
    c7_u[c7_i10] = c7_b_inData[c7_i10];
  }

  c7_y = NULL;
  sf_mex_assign(&c7_y, sf_mex_create("y", c7_u, 3, 0U, 1U, 0U, 1, 512), false);
  sf_mex_assign(&c7_mxArrayOutData, c7_y, false);
  return c7_mxArrayOutData;
}

static void c7_d_emlrt_marshallIn(SFc7_cameraTestInstanceStruct *chartInstance,
  const mxArray *c7_u, const emlrtMsgIdentifier *c7_parentId, uint8_T c7_y[512])
{
  uint8_T c7_uv2[512];
  int32_T c7_i11;
  (void)chartInstance;
  sf_mex_import(c7_parentId, sf_mex_dup(c7_u), c7_uv2, 1, 3, 0U, 1, 0U, 1, 512);
  for (c7_i11 = 0; c7_i11 < 512; c7_i11++) {
    c7_y[c7_i11] = c7_uv2[c7_i11];
  }

  sf_mex_destroy(&c7_u);
}

static void c7_c_sf_marshallIn(void *chartInstanceVoid, const mxArray
  *c7_mxArrayInData, const char_T *c7_varName, void *c7_outData)
{
  const mxArray *c7_dataRead;
  const char_T *c7_identifier;
  emlrtMsgIdentifier c7_thisId;
  uint8_T c7_y[512];
  int32_T c7_i12;
  SFc7_cameraTestInstanceStruct *chartInstance;
  chartInstance = (SFc7_cameraTestInstanceStruct *)chartInstanceVoid;
  c7_dataRead = sf_mex_dup(c7_mxArrayInData);
  c7_identifier = c7_varName;
  c7_thisId.fIdentifier = c7_identifier;
  c7_thisId.fParent = NULL;
  c7_d_emlrt_marshallIn(chartInstance, sf_mex_dup(c7_dataRead), &c7_thisId, c7_y);
  sf_mex_destroy(&c7_dataRead);
  for (c7_i12 = 0; c7_i12 < 512; c7_i12++) {
    (*(uint8_T (*)[512])c7_outData)[c7_i12] = c7_y[c7_i12];
  }

  sf_mex_destroy(&c7_mxArrayInData);
}

static int32_T c7_e_emlrt_marshallIn(SFc7_cameraTestInstanceStruct
  *chartInstance, const mxArray *c7_u, const emlrtMsgIdentifier *c7_parentId)
{
  int32_T c7_y;
  int32_T c7_i13;
  (void)chartInstance;
  sf_mex_import(c7_parentId, sf_mex_dup(c7_u), &c7_i13, 1, 6, 0U, 0, 0U, 0);
  c7_y = c7_i13;
  sf_mex_destroy(&c7_u);
  return c7_y;
}

static void c7_d_sf_marshallIn(void *chartInstanceVoid, const mxArray
  *c7_mxArrayInData, const char_T *c7_varName, void *c7_outData)
{
  const mxArray *c7_bytesRead;
  const char_T *c7_identifier;
  emlrtMsgIdentifier c7_thisId;
  int32_T c7_y;
  SFc7_cameraTestInstanceStruct *chartInstance;
  chartInstance = (SFc7_cameraTestInstanceStruct *)chartInstanceVoid;
  c7_bytesRead = sf_mex_dup(c7_mxArrayInData);
  c7_identifier = c7_varName;
  c7_thisId.fIdentifier = c7_identifier;
  c7_thisId.fParent = NULL;
  c7_y = c7_e_emlrt_marshallIn(chartInstance, sf_mex_dup(c7_bytesRead),
    &c7_thisId);
  sf_mex_destroy(&c7_bytesRead);
  *(int32_T *)c7_outData = c7_y;
  sf_mex_destroy(&c7_mxArrayInData);
}

const mxArray *sf_c7_cameraTest_get_eml_resolved_functions_info(void)
{
  const mxArray *c7_nameCaptureInfo = NULL;
  c7_nameCaptureInfo = NULL;
  sf_mex_assign(&c7_nameCaptureInfo, sf_mex_create("nameCaptureInfo", NULL, 0,
    0U, 1U, 0U, 2, 0, 1), false);
  return c7_nameCaptureInfo;
}

static uint8_T c7_f_emlrt_marshallIn(SFc7_cameraTestInstanceStruct
  *chartInstance, const mxArray *c7_b_is_active_c7_cameraTest, const char_T
  *c7_identifier)
{
  uint8_T c7_y;
  emlrtMsgIdentifier c7_thisId;
  c7_thisId.fIdentifier = c7_identifier;
  c7_thisId.fParent = NULL;
  c7_y = c7_g_emlrt_marshallIn(chartInstance, sf_mex_dup
    (c7_b_is_active_c7_cameraTest), &c7_thisId);
  sf_mex_destroy(&c7_b_is_active_c7_cameraTest);
  return c7_y;
}

static uint8_T c7_g_emlrt_marshallIn(SFc7_cameraTestInstanceStruct
  *chartInstance, const mxArray *c7_u, const emlrtMsgIdentifier *c7_parentId)
{
  uint8_T c7_y;
  uint8_T c7_u0;
  (void)chartInstance;
  sf_mex_import(c7_parentId, sf_mex_dup(c7_u), &c7_u0, 1, 3, 0U, 0, 0U, 0);
  c7_y = c7_u0;
  sf_mex_destroy(&c7_u);
  return c7_y;
}

static int32_T c7_get_spHandle(SFc7_cameraTestInstanceStruct *chartInstance,
  uint32_T c7_elementIndex)
{
  ssReadFromDataStoreElement_wrapper(chartInstance->S, 0, NULL, c7_elementIndex);
  return *chartInstance->c7_spHandle_address;
}

static void c7_set_spHandle(SFc7_cameraTestInstanceStruct *chartInstance,
  uint32_T c7_elementIndex, int32_T c7_elementValue)
{
  ssWriteToDataStoreElement_wrapper(chartInstance->S, 0, NULL, c7_elementIndex);
  *chartInstance->c7_spHandle_address = c7_elementValue;
}

static int32_T *c7_access_spHandle(SFc7_cameraTestInstanceStruct *chartInstance,
  uint32_T c7_rdOnly)
{
  int32_T *c7_dsmAddr;
  ssReadFromDataStore_wrapper(chartInstance->S, 0, NULL);
  c7_dsmAddr = chartInstance->c7_spHandle_address;
  if (c7_rdOnly == 0U) {
    ssWriteToDataStore_wrapper(chartInstance->S, 0, NULL);
  }

  return c7_dsmAddr;
}

static void init_dsm_address_info(SFc7_cameraTestInstanceStruct *chartInstance)
{
  ssGetSFcnDataStoreNameAddrIdx_wrapper(chartInstance->S, "spHandle", (void **)
    &chartInstance->c7_spHandle_address, &chartInstance->c7_spHandle_index);
}

static void init_simulink_io_address(SFc7_cameraTestInstanceStruct
  *chartInstance)
{
  chartInstance->c7_data_data = (uint8_T (*)[512])ssGetOutputPortSignal_wrapper
    (chartInstance->S, 1);
  chartInstance->c7_data_sizes = (int32_T *)
    ssGetCurrentOutputPortDimensions_wrapper(chartInstance->S, 1);
}

/* SFunction Glue Code */
#ifdef utFree
#undef utFree
#endif

#ifdef utMalloc
#undef utMalloc
#endif

#ifdef __cplusplus

extern "C" void *utMalloc(size_t size);
extern "C" void utFree(void*);

#else

extern void *utMalloc(size_t size);
extern void utFree(void*);

#endif

void sf_c7_cameraTest_get_check_sum(mxArray *plhs[])
{
  ((real_T *)mxGetPr((plhs[0])))[0] = (real_T)(111034236U);
  ((real_T *)mxGetPr((plhs[0])))[1] = (real_T)(996763359U);
  ((real_T *)mxGetPr((plhs[0])))[2] = (real_T)(593157181U);
  ((real_T *)mxGetPr((plhs[0])))[3] = (real_T)(1665841856U);
}

mxArray* sf_c7_cameraTest_get_post_codegen_info(void);
mxArray *sf_c7_cameraTest_get_autoinheritance_info(void)
{
  const char *autoinheritanceFields[] = { "checksum", "inputs", "parameters",
    "outputs", "locals", "postCodegenInfo" };

  mxArray *mxAutoinheritanceInfo = mxCreateStructMatrix(1, 1, sizeof
    (autoinheritanceFields)/sizeof(autoinheritanceFields[0]),
    autoinheritanceFields);

  {
    mxArray *mxChecksum = mxCreateString("MgBLIqUIqxZrfDMaHwiUUG");
    mxSetField(mxAutoinheritanceInfo,0,"checksum",mxChecksum);
  }

  {
    mxSetField(mxAutoinheritanceInfo,0,"inputs",mxCreateDoubleMatrix(0,0,mxREAL));
  }

  {
    mxSetField(mxAutoinheritanceInfo,0,"parameters",mxCreateDoubleMatrix(0,0,
                mxREAL));
  }

  {
    const char *dataFields[] = { "size", "type", "complexity" };

    mxArray *mxData = mxCreateStructMatrix(1,1,3,dataFields);

    {
      mxArray *mxSize = mxCreateDoubleMatrix(1,2,mxREAL);
      double *pr = mxGetPr(mxSize);
      pr[0] = (double)(512);
      pr[1] = (double)(1);
      mxSetField(mxData,0,"size",mxSize);
    }

    {
      const char *typeFields[] = { "base", "fixpt" };

      mxArray *mxType = mxCreateStructMatrix(1,1,2,typeFields);
      mxSetField(mxType,0,"base",mxCreateDoubleScalar(3));
      mxSetField(mxType,0,"fixpt",mxCreateDoubleMatrix(0,0,mxREAL));
      mxSetField(mxData,0,"type",mxType);
    }

    mxSetField(mxData,0,"complexity",mxCreateDoubleScalar(0));
    mxSetField(mxAutoinheritanceInfo,0,"outputs",mxData);
  }

  {
    mxSetField(mxAutoinheritanceInfo,0,"locals",mxCreateDoubleMatrix(0,0,mxREAL));
  }

  {
    mxArray* mxPostCodegenInfo = sf_c7_cameraTest_get_post_codegen_info();
    mxSetField(mxAutoinheritanceInfo,0,"postCodegenInfo",mxPostCodegenInfo);
  }

  return(mxAutoinheritanceInfo);
}

mxArray *sf_c7_cameraTest_third_party_uses_info(void)
{
  mxArray * mxcell3p = mxCreateCellMatrix(1,0);
  return(mxcell3p);
}

mxArray *sf_c7_cameraTest_jit_fallback_info(void)
{
  const char *infoFields[] = { "fallbackType", "fallbackReason",
    "incompatibleSymbol", };

  mxArray *mxInfo = mxCreateStructMatrix(1, 1, 3, infoFields);
  mxArray *fallbackReason = mxCreateString("feature_off");
  mxArray *incompatibleSymbol = mxCreateString("");
  mxArray *fallbackType = mxCreateString("early");
  mxSetField(mxInfo, 0, infoFields[0], fallbackType);
  mxSetField(mxInfo, 0, infoFields[1], fallbackReason);
  mxSetField(mxInfo, 0, infoFields[2], incompatibleSymbol);
  return mxInfo;
}

mxArray *sf_c7_cameraTest_updateBuildInfo_args_info(void)
{
  mxArray *mxBIArgs = mxCreateCellMatrix(1,0);
  return mxBIArgs;
}

mxArray* sf_c7_cameraTest_get_post_codegen_info(void)
{
  const char* fieldNames[] = { "exportedFunctionsUsedByThisChart",
    "exportedFunctionsChecksum" };

  mwSize dims[2] = { 1, 1 };

  mxArray* mxPostCodegenInfo = mxCreateStructArray(2, dims, sizeof(fieldNames)/
    sizeof(fieldNames[0]), fieldNames);

  {
    mxArray* mxExportedFunctionsChecksum = mxCreateString("");
    mwSize exp_dims[2] = { 0, 1 };

    mxArray* mxExportedFunctionsUsedByThisChart = mxCreateCellArray(2, exp_dims);
    mxSetField(mxPostCodegenInfo, 0, "exportedFunctionsUsedByThisChart",
               mxExportedFunctionsUsedByThisChart);
    mxSetField(mxPostCodegenInfo, 0, "exportedFunctionsChecksum",
               mxExportedFunctionsChecksum);
  }

  return mxPostCodegenInfo;
}

static const mxArray *sf_get_sim_state_info_c7_cameraTest(void)
{
  const char *infoFields[] = { "chartChecksum", "varInfo" };

  mxArray *mxInfo = mxCreateStructMatrix(1, 1, 2, infoFields);
  const char *infoEncStr[] = {
    "100 S1x2'type','srcId','name','auxInfo'{{M[1],M[11],T\"data\",},{M[8],M[0],T\"is_active_c7_cameraTest\",}}"
  };

  mxArray *mxVarInfo = sf_mex_decode_encoded_mx_struct_array(infoEncStr, 2, 10);
  mxArray *mxChecksum = mxCreateDoubleMatrix(1, 4, mxREAL);
  sf_c7_cameraTest_get_check_sum(&mxChecksum);
  mxSetField(mxInfo, 0, infoFields[0], mxChecksum);
  mxSetField(mxInfo, 0, infoFields[1], mxVarInfo);
  return mxInfo;
}

static void chart_debug_initialization(SimStruct *S, unsigned int
  fullDebuggerInitialization)
{
  if (!sim_mode_is_rtw_gen(S)) {
    SFc7_cameraTestInstanceStruct *chartInstance;
    ChartRunTimeInfo * crtInfo = (ChartRunTimeInfo *)(ssGetUserData(S));
    ChartInfoStruct * chartInfo = (ChartInfoStruct *)(crtInfo->instanceInfo);
    chartInstance = (SFc7_cameraTestInstanceStruct *) chartInfo->chartInstance;
    if (ssIsFirstInitCond(S) && fullDebuggerInitialization==1) {
      /* do this only if simulation is starting */
      {
        unsigned int chartAlreadyPresent;
        chartAlreadyPresent = sf_debug_initialize_chart
          (sfGlobalDebugInstanceStruct,
           _cameraTestMachineNumber_,
           7,
           1,
           1,
           0,
           2,
           0,
           0,
           0,
           0,
           0,
           &(chartInstance->chartNumber),
           &(chartInstance->instanceNumber),
           (void *)S);

        /* Each instance must initialize its own list of scripts */
        init_script_number_translation(_cameraTestMachineNumber_,
          chartInstance->chartNumber,chartInstance->instanceNumber);
        if (chartAlreadyPresent==0) {
          /* this is the first instance */
          sf_debug_set_chart_disable_implicit_casting
            (sfGlobalDebugInstanceStruct,_cameraTestMachineNumber_,
             chartInstance->chartNumber,1);
          sf_debug_set_chart_event_thresholds(sfGlobalDebugInstanceStruct,
            _cameraTestMachineNumber_,
            chartInstance->chartNumber,
            0,
            0,
            0);
          _SFD_SET_DATA_PROPS(0,2,0,1,"data");
          _SFD_SET_DATA_PROPS(1,11,0,0,"spHandle");
          _SFD_STATE_INFO(0,0,2);
          _SFD_CH_SUBSTATE_COUNT(0);
          _SFD_CH_SUBSTATE_DECOMP(0);
        }

        _SFD_CV_INIT_CHART(0,0,0,0);

        {
          _SFD_CV_INIT_STATE(0,0,0,0,0,0,NULL,NULL);
        }

        _SFD_CV_INIT_TRANS(0,0,NULL,NULL,0,NULL);

        /* Initialization of MATLAB Function Model Coverage */
        _SFD_CV_INIT_EML(0,1,1,3,0,0,0,0,0,1,1);
        _SFD_CV_INIT_EML_FCN(0,0,"eML_blk_kernel",0,-1,373);
        _SFD_CV_INIT_EML_IF(0,1,0,120,144,-1,341);
        _SFD_CV_INIT_EML_IF(0,1,1,153,166,-1,334);
        _SFD_CV_INIT_EML_IF(0,1,2,259,273,-1,326);

        {
          static int condStart[] = { 124 };

          static int condEnd[] = { 144 };

          static int pfixExpr[] = { 0, -1 };

          _SFD_CV_INIT_EML_MCDC(0,1,0,123,144,1,0,&(condStart[0]),&(condEnd[0]),
                                2,&(pfixExpr[0]));
        }

        {
          unsigned int dimVector[1];
          dimVector[0]= 512;
          _SFD_SET_DATA_COMPILED_PROPS(0,SF_UINT8,1,&(dimVector[0]),0,0,0,0.0,
            1.0,0,0,(MexFcnForType)c7_sf_marshallOut,(MexInFcnForType)
            c7_sf_marshallIn);
        }

        _SFD_SET_DATA_COMPILED_PROPS(1,SF_INT32,0,NULL,0,0,0,0.0,1.0,0,0,
          (MexFcnForType)c7_c_sf_marshallOut,(MexInFcnForType)c7_d_sf_marshallIn);
        _SFD_SET_DATA_VALUE_PTR_VAR_DIM(0U, *chartInstance->c7_data_data, (void *)
          chartInstance->c7_data_sizes);
        _SFD_SET_DATA_VALUE_PTR(1U, chartInstance->c7_spHandle_address);
      }
    } else {
      sf_debug_reset_current_state_configuration(sfGlobalDebugInstanceStruct,
        _cameraTestMachineNumber_,chartInstance->chartNumber,
        chartInstance->instanceNumber);
    }
  }
}

static const char* sf_get_instance_specialization(void)
{
  return "T4yiVL4eeEJjdlRSUrgeQH";
}

static void sf_opaque_initialize_c7_cameraTest(void *chartInstanceVar)
{
  chart_debug_initialization(((SFc7_cameraTestInstanceStruct*) chartInstanceVar
    )->S,0);
  initialize_params_c7_cameraTest((SFc7_cameraTestInstanceStruct*)
    chartInstanceVar);
  initialize_c7_cameraTest((SFc7_cameraTestInstanceStruct*) chartInstanceVar);
}

static void sf_opaque_enable_c7_cameraTest(void *chartInstanceVar)
{
  enable_c7_cameraTest((SFc7_cameraTestInstanceStruct*) chartInstanceVar);
}

static void sf_opaque_disable_c7_cameraTest(void *chartInstanceVar)
{
  disable_c7_cameraTest((SFc7_cameraTestInstanceStruct*) chartInstanceVar);
}

static void sf_opaque_gateway_c7_cameraTest(void *chartInstanceVar)
{
  sf_gateway_c7_cameraTest((SFc7_cameraTestInstanceStruct*) chartInstanceVar);
}

static const mxArray* sf_opaque_get_sim_state_c7_cameraTest(SimStruct* S)
{
  ChartRunTimeInfo * crtInfo = (ChartRunTimeInfo *)(ssGetUserData(S));
  ChartInfoStruct * chartInfo = (ChartInfoStruct *)(crtInfo->instanceInfo);
  return get_sim_state_c7_cameraTest((SFc7_cameraTestInstanceStruct*)
    chartInfo->chartInstance);         /* raw sim ctx */
}

static void sf_opaque_set_sim_state_c7_cameraTest(SimStruct* S, const mxArray
  *st)
{
  ChartRunTimeInfo * crtInfo = (ChartRunTimeInfo *)(ssGetUserData(S));
  ChartInfoStruct * chartInfo = (ChartInfoStruct *)(crtInfo->instanceInfo);
  set_sim_state_c7_cameraTest((SFc7_cameraTestInstanceStruct*)
    chartInfo->chartInstance, st);
}

static void sf_opaque_terminate_c7_cameraTest(void *chartInstanceVar)
{
  if (chartInstanceVar!=NULL) {
    SimStruct *S = ((SFc7_cameraTestInstanceStruct*) chartInstanceVar)->S;
    ChartRunTimeInfo * crtInfo = (ChartRunTimeInfo *)(ssGetUserData(S));
    if (sim_mode_is_rtw_gen(S) || sim_mode_is_external(S)) {
      sf_clear_rtw_identifier(S);
      unload_cameraTest_optimization_info();
    }

    finalize_c7_cameraTest((SFc7_cameraTestInstanceStruct*) chartInstanceVar);
    utFree(chartInstanceVar);
    if (crtInfo != NULL) {
      utFree(crtInfo);
    }

    ssSetUserData(S,NULL);
  }
}

static void sf_opaque_init_subchart_simstructs(void *chartInstanceVar)
{
  initSimStructsc7_cameraTest((SFc7_cameraTestInstanceStruct*) chartInstanceVar);
}

extern unsigned int sf_machine_global_initializer_called(void);
static void mdlProcessParameters_c7_cameraTest(SimStruct *S)
{
  int i;
  for (i=0;i<ssGetNumRunTimeParams(S);i++) {
    if (ssGetSFcnParamTunable(S,i)) {
      ssUpdateDlgParamAsRunTimeParam(S,i);
    }
  }

  if (sf_machine_global_initializer_called()) {
    ChartRunTimeInfo * crtInfo = (ChartRunTimeInfo *)(ssGetUserData(S));
    ChartInfoStruct * chartInfo = (ChartInfoStruct *)(crtInfo->instanceInfo);
    initialize_params_c7_cameraTest((SFc7_cameraTestInstanceStruct*)
      (chartInfo->chartInstance));
  }
}

static void mdlSetWorkWidths_c7_cameraTest(SimStruct *S)
{
  if (sim_mode_is_rtw_gen(S) || sim_mode_is_external(S)) {
    mxArray *infoStruct = load_cameraTest_optimization_info();
    int_T chartIsInlinable =
      (int_T)sf_is_chart_inlinable(sf_get_instance_specialization(),infoStruct,7);
    ssSetStateflowIsInlinable(S,chartIsInlinable);
    ssSetRTWCG(S,sf_rtw_info_uint_prop(sf_get_instance_specialization(),
                infoStruct,7,"RTWCG"));
    ssSetEnableFcnIsTrivial(S,1);
    ssSetDisableFcnIsTrivial(S,1);
    ssSetNotMultipleInlinable(S,sf_rtw_info_uint_prop
      (sf_get_instance_specialization(),infoStruct,7,
       "gatewayCannotBeInlinedMultipleTimes"));
    sf_update_buildInfo(sf_get_instance_specialization(),infoStruct,7);
    if (chartIsInlinable) {
      sf_mark_chart_reusable_outputs(S,sf_get_instance_specialization(),
        infoStruct,7,1);
    }

    {
      unsigned int outPortIdx;
      for (outPortIdx=1; outPortIdx<=1; ++outPortIdx) {
        ssSetOutputPortOptimizeInIR(S, outPortIdx, 1U);
      }
    }

    sf_set_rtw_dwork_info(S,sf_get_instance_specialization(),infoStruct,7);
    ssSetHasSubFunctions(S,!(chartIsInlinable));
  } else {
  }

  ssSetOptions(S,ssGetOptions(S)|SS_OPTION_WORKS_WITH_CODE_REUSE);
  ssSetChecksum0(S,(3348568622U));
  ssSetChecksum1(S,(295131241U));
  ssSetChecksum2(S,(4236659939U));
  ssSetChecksum3(S,(2814788452U));
  ssSetmdlDerivatives(S, NULL);
  ssSetExplicitFCSSCtrl(S,1);
  ssSupportsMultipleExecInstances(S,0);
}

static void mdlRTW_c7_cameraTest(SimStruct *S)
{
  if (sim_mode_is_rtw_gen(S)) {
    ssWriteRTWStrParam(S, "StateflowChartType", "Embedded MATLAB");
  }
}

static void mdlStart_c7_cameraTest(SimStruct *S)
{
  SFc7_cameraTestInstanceStruct *chartInstance;
  ChartRunTimeInfo * crtInfo = (ChartRunTimeInfo *)utMalloc(sizeof
    (ChartRunTimeInfo));
  chartInstance = (SFc7_cameraTestInstanceStruct *)utMalloc(sizeof
    (SFc7_cameraTestInstanceStruct));
  memset(chartInstance, 0, sizeof(SFc7_cameraTestInstanceStruct));
  if (chartInstance==NULL) {
    sf_mex_error_message("Could not allocate memory for chart instance.");
  }

  chartInstance->chartInfo.chartInstance = chartInstance;
  chartInstance->chartInfo.isEMLChart = 1;
  chartInstance->chartInfo.chartInitialized = 0;
  chartInstance->chartInfo.sFunctionGateway = sf_opaque_gateway_c7_cameraTest;
  chartInstance->chartInfo.initializeChart = sf_opaque_initialize_c7_cameraTest;
  chartInstance->chartInfo.terminateChart = sf_opaque_terminate_c7_cameraTest;
  chartInstance->chartInfo.enableChart = sf_opaque_enable_c7_cameraTest;
  chartInstance->chartInfo.disableChart = sf_opaque_disable_c7_cameraTest;
  chartInstance->chartInfo.getSimState = sf_opaque_get_sim_state_c7_cameraTest;
  chartInstance->chartInfo.setSimState = sf_opaque_set_sim_state_c7_cameraTest;
  chartInstance->chartInfo.getSimStateInfo = sf_get_sim_state_info_c7_cameraTest;
  chartInstance->chartInfo.zeroCrossings = NULL;
  chartInstance->chartInfo.outputs = NULL;
  chartInstance->chartInfo.derivatives = NULL;
  chartInstance->chartInfo.mdlRTW = mdlRTW_c7_cameraTest;
  chartInstance->chartInfo.mdlStart = mdlStart_c7_cameraTest;
  chartInstance->chartInfo.mdlSetWorkWidths = mdlSetWorkWidths_c7_cameraTest;
  chartInstance->chartInfo.extModeExec = NULL;
  chartInstance->chartInfo.restoreLastMajorStepConfiguration = NULL;
  chartInstance->chartInfo.restoreBeforeLastMajorStepConfiguration = NULL;
  chartInstance->chartInfo.storeCurrentConfiguration = NULL;
  chartInstance->chartInfo.callAtomicSubchartUserFcn = NULL;
  chartInstance->chartInfo.callAtomicSubchartAutoFcn = NULL;
  chartInstance->chartInfo.debugInstance = sfGlobalDebugInstanceStruct;
  chartInstance->S = S;
  crtInfo->checksum = SF_RUNTIME_INFO_CHECKSUM;
  crtInfo->instanceInfo = (&(chartInstance->chartInfo));
  crtInfo->isJITEnabled = false;
  crtInfo->compiledInfo = NULL;
  ssSetUserData(S,(void *)(crtInfo));  /* register the chart instance with simstruct */
  init_dsm_address_info(chartInstance);
  init_simulink_io_address(chartInstance);
  if (!sim_mode_is_rtw_gen(S)) {
  }

  sf_opaque_init_subchart_simstructs(chartInstance->chartInfo.chartInstance);
  chart_debug_initialization(S,1);
}

void c7_cameraTest_method_dispatcher(SimStruct *S, int_T method, void *data)
{
  switch (method) {
   case SS_CALL_MDL_START:
    mdlStart_c7_cameraTest(S);
    break;

   case SS_CALL_MDL_SET_WORK_WIDTHS:
    mdlSetWorkWidths_c7_cameraTest(S);
    break;

   case SS_CALL_MDL_PROCESS_PARAMETERS:
    mdlProcessParameters_c7_cameraTest(S);
    break;

   default:
    /* Unhandled method */
    sf_mex_error_message("Stateflow Internal Error:\n"
                         "Error calling c7_cameraTest_method_dispatcher.\n"
                         "Can't handle method %d.\n", method);
    break;
  }
}

/* Include files */

#include "cameraTest_sfun.h"
#include "cameraTest_sfun_debug_macros.h"
#include "c1_cameraTest.h"
#include "c2_cameraTest.h"
#include "c3_cameraTest.h"
#include "c4_cameraTest.h"
#include "c8_cameraTest.h"
#include "c9_cameraTest.h"
#include "c10_cameraTest.h"

/* Type Definitions */

/* Named Constants */

/* Variable Declarations */

/* Variable Definitions */
uint32_T _cameraTestMachineNumber_;

/* Function Declarations */

/* Function Definitions */
void cameraTest_initializer(void)
{
}

void cameraTest_terminator(void)
{
}

/* SFunction Glue Code */
unsigned int sf_cameraTest_method_dispatcher(SimStruct *simstructPtr, unsigned
  int chartFileNumber, const char* specsCksum, int_T method, void *data)
{
  if (chartFileNumber==1) {
    c1_cameraTest_method_dispatcher(simstructPtr, method, data);
    return 1;
  }

  if (chartFileNumber==2) {
    c2_cameraTest_method_dispatcher(simstructPtr, method, data);
    return 1;
  }

  if (chartFileNumber==3) {
    c3_cameraTest_method_dispatcher(simstructPtr, method, data);
    return 1;
  }

  if (chartFileNumber==4) {
    c4_cameraTest_method_dispatcher(simstructPtr, method, data);
    return 1;
  }

  if (chartFileNumber==8) {
    c8_cameraTest_method_dispatcher(simstructPtr, method, data);
    return 1;
  }

  if (chartFileNumber==9) {
    c9_cameraTest_method_dispatcher(simstructPtr, method, data);
    return 1;
  }

  if (chartFileNumber==10) {
    c10_cameraTest_method_dispatcher(simstructPtr, method, data);
    return 1;
  }

  return 0;
}

extern void sf_cameraTest_uses_exported_functions(int nlhs, mxArray * plhs[],
  int nrhs, const mxArray * prhs[])
{
  plhs[0] = mxCreateLogicalScalar(0);
}

unsigned int sf_cameraTest_process_testpoint_info_call( int nlhs, mxArray *
  plhs[], int nrhs, const mxArray * prhs[] )
{

#ifdef MATLAB_MEX_FILE

  char commandName[32];
  char machineName[128];
  if (nrhs < 3 || !mxIsChar(prhs[0]) || !mxIsChar(prhs[1]))
    return 0;

  /* Possible call to get testpoint info. */
  mxGetString(prhs[0], commandName,sizeof(commandName)/sizeof(char));
  commandName[(sizeof(commandName)/sizeof(char)-1)] = '\0';
  if (strcmp(commandName,"get_testpoint_info"))
    return 0;
  mxGetString(prhs[1], machineName, sizeof(machineName)/sizeof(char));
  machineName[(sizeof(machineName)/sizeof(char)-1)] = '\0';
  if (!strcmp(machineName, "cameraTest")) {
    unsigned int chartFileNumber;
    chartFileNumber = (unsigned int)mxGetScalar(prhs[2]);
    switch (chartFileNumber) {
     case 10:
      {
        extern mxArray *sf_c10_cameraTest_get_testpoint_info(void);
        plhs[0] = sf_c10_cameraTest_get_testpoint_info();
        break;
      }

     default:
      plhs[0] = mxCreateDoubleMatrix(0,0,mxREAL);
    }

    return 1;
  }

  return 0;

#else

  return 0;

#endif

}

unsigned int sf_cameraTest_process_check_sum_call( int nlhs, mxArray * plhs[],
  int nrhs, const mxArray * prhs[] )
{

#ifdef MATLAB_MEX_FILE

  char commandName[20];
  if (nrhs<1 || !mxIsChar(prhs[0]) )
    return 0;

  /* Possible call to get the checksum */
  mxGetString(prhs[0], commandName,sizeof(commandName)/sizeof(char));
  commandName[(sizeof(commandName)/sizeof(char)-1)] = '\0';
  if (strcmp(commandName,"sf_get_check_sum"))
    return 0;
  plhs[0] = mxCreateDoubleMatrix( 1,4,mxREAL);
  if (nrhs>1 && mxIsChar(prhs[1])) {
    mxGetString(prhs[1], commandName,sizeof(commandName)/sizeof(char));
    commandName[(sizeof(commandName)/sizeof(char)-1)] = '\0';
    if (!strcmp(commandName,"machine")) {
      ((real_T *)mxGetPr((plhs[0])))[0] = (real_T)(904986131U);
      ((real_T *)mxGetPr((plhs[0])))[1] = (real_T)(2597705440U);
      ((real_T *)mxGetPr((plhs[0])))[2] = (real_T)(2920421315U);
      ((real_T *)mxGetPr((plhs[0])))[3] = (real_T)(4061061351U);
    } else if (!strcmp(commandName,"exportedFcn")) {
      ((real_T *)mxGetPr((plhs[0])))[0] = (real_T)(0U);
      ((real_T *)mxGetPr((plhs[0])))[1] = (real_T)(0U);
      ((real_T *)mxGetPr((plhs[0])))[2] = (real_T)(0U);
      ((real_T *)mxGetPr((plhs[0])))[3] = (real_T)(0U);
    } else if (!strcmp(commandName,"makefile")) {
      ((real_T *)mxGetPr((plhs[0])))[0] = (real_T)(3565240644U);
      ((real_T *)mxGetPr((plhs[0])))[1] = (real_T)(2568637667U);
      ((real_T *)mxGetPr((plhs[0])))[2] = (real_T)(218157380U);
      ((real_T *)mxGetPr((plhs[0])))[3] = (real_T)(1903325568U);
    } else if (nrhs==3 && !strcmp(commandName,"chart")) {
      unsigned int chartFileNumber;
      chartFileNumber = (unsigned int)mxGetScalar(prhs[2]);
      switch (chartFileNumber) {
       case 1:
        {
          extern void sf_c1_cameraTest_get_check_sum(mxArray *plhs[]);
          sf_c1_cameraTest_get_check_sum(plhs);
          break;
        }

       case 2:
        {
          extern void sf_c2_cameraTest_get_check_sum(mxArray *plhs[]);
          sf_c2_cameraTest_get_check_sum(plhs);
          break;
        }

       case 3:
        {
          extern void sf_c3_cameraTest_get_check_sum(mxArray *plhs[]);
          sf_c3_cameraTest_get_check_sum(plhs);
          break;
        }

       case 4:
        {
          extern void sf_c4_cameraTest_get_check_sum(mxArray *plhs[]);
          sf_c4_cameraTest_get_check_sum(plhs);
          break;
        }

       case 8:
        {
          extern void sf_c8_cameraTest_get_check_sum(mxArray *plhs[]);
          sf_c8_cameraTest_get_check_sum(plhs);
          break;
        }

       case 9:
        {
          extern void sf_c9_cameraTest_get_check_sum(mxArray *plhs[]);
          sf_c9_cameraTest_get_check_sum(plhs);
          break;
        }

       case 10:
        {
          extern void sf_c10_cameraTest_get_check_sum(mxArray *plhs[]);
          sf_c10_cameraTest_get_check_sum(plhs);
          break;
        }

       default:
        ((real_T *)mxGetPr((plhs[0])))[0] = (real_T)(0.0);
        ((real_T *)mxGetPr((plhs[0])))[1] = (real_T)(0.0);
        ((real_T *)mxGetPr((plhs[0])))[2] = (real_T)(0.0);
        ((real_T *)mxGetPr((plhs[0])))[3] = (real_T)(0.0);
      }
    } else if (!strcmp(commandName,"target")) {
      ((real_T *)mxGetPr((plhs[0])))[0] = (real_T)(1079105835U);
      ((real_T *)mxGetPr((plhs[0])))[1] = (real_T)(4093772408U);
      ((real_T *)mxGetPr((plhs[0])))[2] = (real_T)(525736737U);
      ((real_T *)mxGetPr((plhs[0])))[3] = (real_T)(2157381758U);
    } else {
      return 0;
    }
  } else {
    ((real_T *)mxGetPr((plhs[0])))[0] = (real_T)(1389771759U);
    ((real_T *)mxGetPr((plhs[0])))[1] = (real_T)(2808382994U);
    ((real_T *)mxGetPr((plhs[0])))[2] = (real_T)(1407948162U);
    ((real_T *)mxGetPr((plhs[0])))[3] = (real_T)(2665488921U);
  }

  return 1;

#else

  return 0;

#endif

}

unsigned int sf_cameraTest_autoinheritance_info( int nlhs, mxArray * plhs[], int
  nrhs, const mxArray * prhs[] )
{

#ifdef MATLAB_MEX_FILE

  char commandName[32];
  char aiChksum[64];
  if (nrhs<3 || !mxIsChar(prhs[0]) )
    return 0;

  /* Possible call to get the autoinheritance_info */
  mxGetString(prhs[0], commandName,sizeof(commandName)/sizeof(char));
  commandName[(sizeof(commandName)/sizeof(char)-1)] = '\0';
  if (strcmp(commandName,"get_autoinheritance_info"))
    return 0;
  mxGetString(prhs[2], aiChksum,sizeof(aiChksum)/sizeof(char));
  aiChksum[(sizeof(aiChksum)/sizeof(char)-1)] = '\0';

  {
    unsigned int chartFileNumber;
    chartFileNumber = (unsigned int)mxGetScalar(prhs[1]);
    switch (chartFileNumber) {
     case 1:
      {
        if (strcmp(aiChksum, "1jREDWdNB9c7UGdtzAQKDC") == 0) {
          extern mxArray *sf_c1_cameraTest_get_autoinheritance_info(void);
          plhs[0] = sf_c1_cameraTest_get_autoinheritance_info();
          break;
        }

        plhs[0] = mxCreateDoubleMatrix(0,0,mxREAL);
        break;
      }

     case 2:
      {
        if (strcmp(aiChksum, "QplwCtBy6GpBsfCG1W7TQC") == 0) {
          extern mxArray *sf_c2_cameraTest_get_autoinheritance_info(void);
          plhs[0] = sf_c2_cameraTest_get_autoinheritance_info();
          break;
        }

        plhs[0] = mxCreateDoubleMatrix(0,0,mxREAL);
        break;
      }

     case 3:
      {
        if (strcmp(aiChksum, "yJ02ua9vZA7j7lVl179CTF") == 0) {
          extern mxArray *sf_c3_cameraTest_get_autoinheritance_info(void);
          plhs[0] = sf_c3_cameraTest_get_autoinheritance_info();
          break;
        }

        plhs[0] = mxCreateDoubleMatrix(0,0,mxREAL);
        break;
      }

     case 4:
      {
        if (strcmp(aiChksum, "B96iBpHx3shZ3np6OuLbm") == 0) {
          extern mxArray *sf_c4_cameraTest_get_autoinheritance_info(void);
          plhs[0] = sf_c4_cameraTest_get_autoinheritance_info();
          break;
        }

        plhs[0] = mxCreateDoubleMatrix(0,0,mxREAL);
        break;
      }

     case 8:
      {
        if (strcmp(aiChksum, "mS5I32QvLiMnJkZ2enz0HH") == 0) {
          extern mxArray *sf_c8_cameraTest_get_autoinheritance_info(void);
          plhs[0] = sf_c8_cameraTest_get_autoinheritance_info();
          break;
        }

        plhs[0] = mxCreateDoubleMatrix(0,0,mxREAL);
        break;
      }

     case 9:
      {
        if (strcmp(aiChksum, "ArIrdR8KduvZC6FfJiWpp") == 0) {
          extern mxArray *sf_c9_cameraTest_get_autoinheritance_info(void);
          plhs[0] = sf_c9_cameraTest_get_autoinheritance_info();
          break;
        }

        plhs[0] = mxCreateDoubleMatrix(0,0,mxREAL);
        break;
      }

     case 10:
      {
        if (strcmp(aiChksum, "5GCUruyFPNfSoASjim60bB") == 0) {
          extern mxArray *sf_c10_cameraTest_get_autoinheritance_info(void);
          plhs[0] = sf_c10_cameraTest_get_autoinheritance_info();
          break;
        }

        plhs[0] = mxCreateDoubleMatrix(0,0,mxREAL);
        break;
      }

     default:
      plhs[0] = mxCreateDoubleMatrix(0,0,mxREAL);
    }
  }

  return 1;

#else

  return 0;

#endif

}

unsigned int sf_cameraTest_get_eml_resolved_functions_info( int nlhs, mxArray *
  plhs[], int nrhs, const mxArray * prhs[] )
{

#ifdef MATLAB_MEX_FILE

  char commandName[64];
  if (nrhs<2 || !mxIsChar(prhs[0]))
    return 0;

  /* Possible call to get the get_eml_resolved_functions_info */
  mxGetString(prhs[0], commandName,sizeof(commandName)/sizeof(char));
  commandName[(sizeof(commandName)/sizeof(char)-1)] = '\0';
  if (strcmp(commandName,"get_eml_resolved_functions_info"))
    return 0;

  {
    unsigned int chartFileNumber;
    chartFileNumber = (unsigned int)mxGetScalar(prhs[1]);
    switch (chartFileNumber) {
     case 1:
      {
        extern const mxArray *sf_c1_cameraTest_get_eml_resolved_functions_info
          (void);
        mxArray *persistentMxArray = (mxArray *)
          sf_c1_cameraTest_get_eml_resolved_functions_info();
        plhs[0] = mxDuplicateArray(persistentMxArray);
        mxDestroyArray(persistentMxArray);
        break;
      }

     case 2:
      {
        extern const mxArray *sf_c2_cameraTest_get_eml_resolved_functions_info
          (void);
        mxArray *persistentMxArray = (mxArray *)
          sf_c2_cameraTest_get_eml_resolved_functions_info();
        plhs[0] = mxDuplicateArray(persistentMxArray);
        mxDestroyArray(persistentMxArray);
        break;
      }

     case 3:
      {
        extern const mxArray *sf_c3_cameraTest_get_eml_resolved_functions_info
          (void);
        mxArray *persistentMxArray = (mxArray *)
          sf_c3_cameraTest_get_eml_resolved_functions_info();
        plhs[0] = mxDuplicateArray(persistentMxArray);
        mxDestroyArray(persistentMxArray);
        break;
      }

     case 4:
      {
        extern const mxArray *sf_c4_cameraTest_get_eml_resolved_functions_info
          (void);
        mxArray *persistentMxArray = (mxArray *)
          sf_c4_cameraTest_get_eml_resolved_functions_info();
        plhs[0] = mxDuplicateArray(persistentMxArray);
        mxDestroyArray(persistentMxArray);
        break;
      }

     case 8:
      {
        extern const mxArray *sf_c8_cameraTest_get_eml_resolved_functions_info
          (void);
        mxArray *persistentMxArray = (mxArray *)
          sf_c8_cameraTest_get_eml_resolved_functions_info();
        plhs[0] = mxDuplicateArray(persistentMxArray);
        mxDestroyArray(persistentMxArray);
        break;
      }

     case 9:
      {
        extern const mxArray *sf_c9_cameraTest_get_eml_resolved_functions_info
          (void);
        mxArray *persistentMxArray = (mxArray *)
          sf_c9_cameraTest_get_eml_resolved_functions_info();
        plhs[0] = mxDuplicateArray(persistentMxArray);
        mxDestroyArray(persistentMxArray);
        break;
      }

     case 10:
      {
        extern const mxArray *sf_c10_cameraTest_get_eml_resolved_functions_info
          (void);
        mxArray *persistentMxArray = (mxArray *)
          sf_c10_cameraTest_get_eml_resolved_functions_info();
        plhs[0] = mxDuplicateArray(persistentMxArray);
        mxDestroyArray(persistentMxArray);
        break;
      }

     default:
      plhs[0] = mxCreateDoubleMatrix(0,0,mxREAL);
    }
  }

  return 1;

#else

  return 0;

#endif

}

unsigned int sf_cameraTest_third_party_uses_info( int nlhs, mxArray * plhs[],
  int nrhs, const mxArray * prhs[] )
{
  char commandName[64];
  char tpChksum[64];
  if (nrhs<3 || !mxIsChar(prhs[0]))
    return 0;

  /* Possible call to get the third_party_uses_info */
  mxGetString(prhs[0], commandName,sizeof(commandName)/sizeof(char));
  commandName[(sizeof(commandName)/sizeof(char)-1)] = '\0';
  mxGetString(prhs[2], tpChksum,sizeof(tpChksum)/sizeof(char));
  tpChksum[(sizeof(tpChksum)/sizeof(char)-1)] = '\0';
  if (strcmp(commandName,"get_third_party_uses_info"))
    return 0;

  {
    unsigned int chartFileNumber;
    chartFileNumber = (unsigned int)mxGetScalar(prhs[1]);
    switch (chartFileNumber) {
     case 1:
      {
        if (strcmp(tpChksum, "aVDZbfY42Mztqkw811HH0D") == 0) {
          extern mxArray *sf_c1_cameraTest_third_party_uses_info(void);
          plhs[0] = sf_c1_cameraTest_third_party_uses_info();
          break;
        }
      }

     case 2:
      {
        if (strcmp(tpChksum, "2dubw5i9OcBM4A4N1SkUzC") == 0) {
          extern mxArray *sf_c2_cameraTest_third_party_uses_info(void);
          plhs[0] = sf_c2_cameraTest_third_party_uses_info();
          break;
        }
      }

     case 3:
      {
        if (strcmp(tpChksum, "Ryz8aqhwC0Z2WbA3s6GIHC") == 0) {
          extern mxArray *sf_c3_cameraTest_third_party_uses_info(void);
          plhs[0] = sf_c3_cameraTest_third_party_uses_info();
          break;
        }
      }

     case 4:
      {
        if (strcmp(tpChksum, "NvlIKdVtBmb3peR69WmcVF") == 0) {
          extern mxArray *sf_c4_cameraTest_third_party_uses_info(void);
          plhs[0] = sf_c4_cameraTest_third_party_uses_info();
          break;
        }
      }

     case 8:
      {
        if (strcmp(tpChksum, "9toHrvpFgntX5dClaa7xRC") == 0) {
          extern mxArray *sf_c8_cameraTest_third_party_uses_info(void);
          plhs[0] = sf_c8_cameraTest_third_party_uses_info();
          break;
        }
      }

     case 9:
      {
        if (strcmp(tpChksum, "cFtwoRDsiuvx30E55B2oa") == 0) {
          extern mxArray *sf_c9_cameraTest_third_party_uses_info(void);
          plhs[0] = sf_c9_cameraTest_third_party_uses_info();
          break;
        }
      }

     case 10:
      {
        if (strcmp(tpChksum, "3lr4VjdexkDRLGej5QFKzF") == 0) {
          extern mxArray *sf_c10_cameraTest_third_party_uses_info(void);
          plhs[0] = sf_c10_cameraTest_third_party_uses_info();
          break;
        }
      }

     default:
      plhs[0] = mxCreateDoubleMatrix(0,0,mxREAL);
    }
  }

  return 1;
}

unsigned int sf_cameraTest_jit_fallback_info( int nlhs, mxArray * plhs[], int
  nrhs, const mxArray * prhs[] )
{
  char commandName[64];
  char tpChksum[64];
  if (nrhs<3 || !mxIsChar(prhs[0]))
    return 0;

  /* Possible call to get the jit_fallback_info */
  mxGetString(prhs[0], commandName,sizeof(commandName)/sizeof(char));
  commandName[(sizeof(commandName)/sizeof(char)-1)] = '\0';
  mxGetString(prhs[2], tpChksum,sizeof(tpChksum)/sizeof(char));
  tpChksum[(sizeof(tpChksum)/sizeof(char)-1)] = '\0';
  if (strcmp(commandName,"get_jit_fallback_info"))
    return 0;

  {
    unsigned int chartFileNumber;
    chartFileNumber = (unsigned int)mxGetScalar(prhs[1]);
    switch (chartFileNumber) {
     case 1:
      {
        if (strcmp(tpChksum, "aVDZbfY42Mztqkw811HH0D") == 0) {
          extern mxArray *sf_c1_cameraTest_jit_fallback_info(void);
          plhs[0] = sf_c1_cameraTest_jit_fallback_info();
          break;
        }
      }

     case 2:
      {
        if (strcmp(tpChksum, "2dubw5i9OcBM4A4N1SkUzC") == 0) {
          extern mxArray *sf_c2_cameraTest_jit_fallback_info(void);
          plhs[0] = sf_c2_cameraTest_jit_fallback_info();
          break;
        }
      }

     case 3:
      {
        if (strcmp(tpChksum, "Ryz8aqhwC0Z2WbA3s6GIHC") == 0) {
          extern mxArray *sf_c3_cameraTest_jit_fallback_info(void);
          plhs[0] = sf_c3_cameraTest_jit_fallback_info();
          break;
        }
      }

     case 4:
      {
        if (strcmp(tpChksum, "NvlIKdVtBmb3peR69WmcVF") == 0) {
          extern mxArray *sf_c4_cameraTest_jit_fallback_info(void);
          plhs[0] = sf_c4_cameraTest_jit_fallback_info();
          break;
        }
      }

     case 8:
      {
        if (strcmp(tpChksum, "9toHrvpFgntX5dClaa7xRC") == 0) {
          extern mxArray *sf_c8_cameraTest_jit_fallback_info(void);
          plhs[0] = sf_c8_cameraTest_jit_fallback_info();
          break;
        }
      }

     case 9:
      {
        if (strcmp(tpChksum, "cFtwoRDsiuvx30E55B2oa") == 0) {
          extern mxArray *sf_c9_cameraTest_jit_fallback_info(void);
          plhs[0] = sf_c9_cameraTest_jit_fallback_info();
          break;
        }
      }

     case 10:
      {
        if (strcmp(tpChksum, "3lr4VjdexkDRLGej5QFKzF") == 0) {
          extern mxArray *sf_c10_cameraTest_jit_fallback_info(void);
          plhs[0] = sf_c10_cameraTest_jit_fallback_info();
          break;
        }
      }

     default:
      plhs[0] = mxCreateDoubleMatrix(0,0,mxREAL);
    }
  }

  return 1;
}

unsigned int sf_cameraTest_updateBuildInfo_args_info( int nlhs, mxArray * plhs[],
  int nrhs, const mxArray * prhs[] )
{
  char commandName[64];
  char tpChksum[64];
  if (nrhs<3 || !mxIsChar(prhs[0]))
    return 0;

  /* Possible call to get the updateBuildInfo_args_info */
  mxGetString(prhs[0], commandName,sizeof(commandName)/sizeof(char));
  commandName[(sizeof(commandName)/sizeof(char)-1)] = '\0';
  mxGetString(prhs[2], tpChksum,sizeof(tpChksum)/sizeof(char));
  tpChksum[(sizeof(tpChksum)/sizeof(char)-1)] = '\0';
  if (strcmp(commandName,"get_updateBuildInfo_args_info"))
    return 0;

  {
    unsigned int chartFileNumber;
    chartFileNumber = (unsigned int)mxGetScalar(prhs[1]);
    switch (chartFileNumber) {
     case 1:
      {
        if (strcmp(tpChksum, "aVDZbfY42Mztqkw811HH0D") == 0) {
          extern mxArray *sf_c1_cameraTest_updateBuildInfo_args_info(void);
          plhs[0] = sf_c1_cameraTest_updateBuildInfo_args_info();
          break;
        }
      }

     case 2:
      {
        if (strcmp(tpChksum, "2dubw5i9OcBM4A4N1SkUzC") == 0) {
          extern mxArray *sf_c2_cameraTest_updateBuildInfo_args_info(void);
          plhs[0] = sf_c2_cameraTest_updateBuildInfo_args_info();
          break;
        }
      }

     case 3:
      {
        if (strcmp(tpChksum, "Ryz8aqhwC0Z2WbA3s6GIHC") == 0) {
          extern mxArray *sf_c3_cameraTest_updateBuildInfo_args_info(void);
          plhs[0] = sf_c3_cameraTest_updateBuildInfo_args_info();
          break;
        }
      }

     case 4:
      {
        if (strcmp(tpChksum, "NvlIKdVtBmb3peR69WmcVF") == 0) {
          extern mxArray *sf_c4_cameraTest_updateBuildInfo_args_info(void);
          plhs[0] = sf_c4_cameraTest_updateBuildInfo_args_info();
          break;
        }
      }

     case 8:
      {
        if (strcmp(tpChksum, "9toHrvpFgntX5dClaa7xRC") == 0) {
          extern mxArray *sf_c8_cameraTest_updateBuildInfo_args_info(void);
          plhs[0] = sf_c8_cameraTest_updateBuildInfo_args_info();
          break;
        }
      }

     case 9:
      {
        if (strcmp(tpChksum, "cFtwoRDsiuvx30E55B2oa") == 0) {
          extern mxArray *sf_c9_cameraTest_updateBuildInfo_args_info(void);
          plhs[0] = sf_c9_cameraTest_updateBuildInfo_args_info();
          break;
        }
      }

     case 10:
      {
        if (strcmp(tpChksum, "3lr4VjdexkDRLGej5QFKzF") == 0) {
          extern mxArray *sf_c10_cameraTest_updateBuildInfo_args_info(void);
          plhs[0] = sf_c10_cameraTest_updateBuildInfo_args_info();
          break;
        }
      }

     default:
      plhs[0] = mxCreateDoubleMatrix(0,0,mxREAL);
    }
  }

  return 1;
}

void sf_cameraTest_get_post_codegen_info( int nlhs, mxArray * plhs[], int nrhs,
  const mxArray * prhs[] )
{
  unsigned int chartFileNumber = (unsigned int) mxGetScalar(prhs[0]);
  char tpChksum[64];
  mxGetString(prhs[1], tpChksum,sizeof(tpChksum)/sizeof(char));
  tpChksum[(sizeof(tpChksum)/sizeof(char)-1)] = '\0';
  switch (chartFileNumber) {
   case 1:
    {
      if (strcmp(tpChksum, "aVDZbfY42Mztqkw811HH0D") == 0) {
        extern mxArray *sf_c1_cameraTest_get_post_codegen_info(void);
        plhs[0] = sf_c1_cameraTest_get_post_codegen_info();
        return;
      }
    }
    break;

   case 2:
    {
      if (strcmp(tpChksum, "2dubw5i9OcBM4A4N1SkUzC") == 0) {
        extern mxArray *sf_c2_cameraTest_get_post_codegen_info(void);
        plhs[0] = sf_c2_cameraTest_get_post_codegen_info();
        return;
      }
    }
    break;

   case 3:
    {
      if (strcmp(tpChksum, "Ryz8aqhwC0Z2WbA3s6GIHC") == 0) {
        extern mxArray *sf_c3_cameraTest_get_post_codegen_info(void);
        plhs[0] = sf_c3_cameraTest_get_post_codegen_info();
        return;
      }
    }
    break;

   case 4:
    {
      if (strcmp(tpChksum, "NvlIKdVtBmb3peR69WmcVF") == 0) {
        extern mxArray *sf_c4_cameraTest_get_post_codegen_info(void);
        plhs[0] = sf_c4_cameraTest_get_post_codegen_info();
        return;
      }
    }
    break;

   case 8:
    {
      if (strcmp(tpChksum, "9toHrvpFgntX5dClaa7xRC") == 0) {
        extern mxArray *sf_c8_cameraTest_get_post_codegen_info(void);
        plhs[0] = sf_c8_cameraTest_get_post_codegen_info();
        return;
      }
    }
    break;

   case 9:
    {
      if (strcmp(tpChksum, "cFtwoRDsiuvx30E55B2oa") == 0) {
        extern mxArray *sf_c9_cameraTest_get_post_codegen_info(void);
        plhs[0] = sf_c9_cameraTest_get_post_codegen_info();
        return;
      }
    }
    break;

   case 10:
    {
      if (strcmp(tpChksum, "3lr4VjdexkDRLGej5QFKzF") == 0) {
        extern mxArray *sf_c10_cameraTest_get_post_codegen_info(void);
        plhs[0] = sf_c10_cameraTest_get_post_codegen_info();
        return;
      }
    }
    break;

   default:
    break;
  }

  plhs[0] = mxCreateDoubleMatrix(0,0,mxREAL);
}

void cameraTest_debug_initialize(struct SfDebugInstanceStruct* debugInstance)
{
  _cameraTestMachineNumber_ = sf_debug_initialize_machine(debugInstance,
    "cameraTest","sfun",0,7,0,0,0);
  sf_debug_set_machine_event_thresholds(debugInstance,_cameraTestMachineNumber_,
    0,0);
  sf_debug_set_machine_data_thresholds(debugInstance,_cameraTestMachineNumber_,0);
}

void cameraTest_register_exported_symbols(SimStruct* S)
{
}

static mxArray* sRtwOptimizationInfoStruct= NULL;
mxArray* load_cameraTest_optimization_info(void)
{
  if (sRtwOptimizationInfoStruct==NULL) {
    sRtwOptimizationInfoStruct = sf_load_rtw_optimization_info("cameraTest",
      "cameraTest");
    mexMakeArrayPersistent(sRtwOptimizationInfoStruct);
  }

  return(sRtwOptimizationInfoStruct);
}

void unload_cameraTest_optimization_info(void)
{
  if (sRtwOptimizationInfoStruct!=NULL) {
    mxDestroyArray(sRtwOptimizationInfoStruct);
    sRtwOptimizationInfoStruct = NULL;
  }
}

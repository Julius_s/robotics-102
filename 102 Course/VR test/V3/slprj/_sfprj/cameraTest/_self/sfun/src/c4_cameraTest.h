#ifndef __c4_cameraTest_h__
#define __c4_cameraTest_h__

/* Include files */
#include "sf_runtime/sfc_sf.h"
#include "sf_runtime/sfc_mex.h"
#include "rtwtypes.h"
#include "multiword_types.h"

/* Type Definitions */
#ifndef struct_Player_tag
#define struct_Player_tag

struct Player_tag
{
  int8_T x;
  int8_T y;
  int16_T orientation;
  uint8_T color;
  uint8_T position;
  uint8_T valid;
};

#endif                                 /*struct_Player_tag*/

#ifndef typedef_c4_Player
#define typedef_c4_Player

typedef struct Player_tag c4_Player;

#endif                                 /*typedef_c4_Player*/

#ifndef struct_Ball_tag
#define struct_Ball_tag

struct Ball_tag
{
  int8_T x;
  int8_T y;
  uint8_T valid;
};

#endif                                 /*struct_Ball_tag*/

#ifndef typedef_c4_Ball
#define typedef_c4_Ball

typedef struct Ball_tag c4_Ball;

#endif                                 /*typedef_c4_Ball*/

#include <stddef.h>
#ifndef typedef_SFc4_cameraTestInstanceStruct
#define typedef_SFc4_cameraTestInstanceStruct

typedef struct {
  SimStruct *S;
  ChartInfoStruct chartInfo;
  uint32_T chartNumber;
  uint32_T instanceNumber;
  int32_T c4_sfEvent;
  boolean_T c4_isStable;
  boolean_T c4_doneDoubleBufferReInit;
  uint8_T c4_is_active_c4_cameraTest;
  c4_Player (*c4_players)[6];
  uint8_T *c4_gameOn;
  uint8_T (*c4_infoToTransmit_data)[64];
  int32_T (*c4_infoToTransmit_sizes)[2];
  c4_Ball *c4_ball;
} SFc4_cameraTestInstanceStruct;

#endif                                 /*typedef_SFc4_cameraTestInstanceStruct*/

/* Named Constants */

/* Variable Declarations */
extern struct SfDebugInstanceStruct *sfGlobalDebugInstanceStruct;

/* Variable Definitions */

/* Function Declarations */
extern const mxArray *sf_c4_cameraTest_get_eml_resolved_functions_info(void);

/* Function Definitions */
extern void sf_c4_cameraTest_get_check_sum(mxArray *plhs[]);
extern void c4_cameraTest_method_dispatcher(SimStruct *S, int_T method, void
  *data);

#endif

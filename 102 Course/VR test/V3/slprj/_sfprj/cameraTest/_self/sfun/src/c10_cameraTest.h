#ifndef __c10_cameraTest_h__
#define __c10_cameraTest_h__

/* Include files */
#include "sf_runtime/sfc_sf.h"
#include "sf_runtime/sfc_mex.h"
#include "rtwtypes.h"
#include "multiword_types.h"

/* Type Definitions */
#ifndef struct_Player_tag
#define struct_Player_tag

struct Player_tag
{
  int8_T x;
  int8_T y;
  int16_T orientation;
  uint8_T color;
  uint8_T position;
  uint8_T valid;
};

#endif                                 /*struct_Player_tag*/

#ifndef typedef_c10_Player
#define typedef_c10_Player

typedef struct Player_tag c10_Player;

#endif                                 /*typedef_c10_Player*/

#ifndef struct_Ball_tag
#define struct_Ball_tag

struct Ball_tag
{
  int8_T x;
  int8_T y;
  uint8_T valid;
};

#endif                                 /*struct_Ball_tag*/

#ifndef typedef_c10_Ball
#define typedef_c10_Ball

typedef struct Ball_tag c10_Ball;

#endif                                 /*typedef_c10_Ball*/

#ifndef typedef_SFc10_cameraTestInstanceStruct
#define typedef_SFc10_cameraTestInstanceStruct

typedef struct {
  SimStruct *S;
  ChartInfoStruct chartInfo;
  uint32_T chartNumber;
  uint32_T instanceNumber;
  boolean_T c10_dataWrittenToVector[4];
  uint8_T c10_doSetSimStateSideEffects;
  const mxArray *c10_setSimStateSideEffectsInfo;
  int32_T *c10_sfEvent;
  boolean_T *c10_isStable;
  uint8_T *c10_is_active_c10_cameraTest;
  uint8_T *c10_is_c10_cameraTest;
  c10_Player (*c10_players)[6];
  c10_Ball *c10_ball;
  uint8_T *c10_gameOn;
  uint8_T *c10_idleTicks;
} SFc10_cameraTestInstanceStruct;

#endif                                 /*typedef_SFc10_cameraTestInstanceStruct*/

/* Named Constants */

/* Variable Declarations */
extern struct SfDebugInstanceStruct *sfGlobalDebugInstanceStruct;

/* Variable Definitions */

/* Function Declarations */
extern const mxArray *sf_c10_cameraTest_get_eml_resolved_functions_info(void);

/* Function Definitions */
extern void sf_c10_cameraTest_get_check_sum(mxArray *plhs[]);
extern void c10_cameraTest_method_dispatcher(SimStruct *S, int_T method, void
  *data);

#endif

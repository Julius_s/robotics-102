/* Include files */

#include <stddef.h>
#include "blas.h"
#include "cameraTest_sfun.h"
#include "c1_cameraTest.h"
#include "mwmathutil.h"
#define CHARTINSTANCE_CHARTNUMBER      (chartInstance->chartNumber)
#define CHARTINSTANCE_INSTANCENUMBER   (chartInstance->instanceNumber)
#include "cameraTest_sfun_debug_macros.h"
#define _SF_MEX_LISTEN_FOR_CTRL_C(S)   sf_mex_listen_for_ctrl_c(sfGlobalDebugInstanceStruct,S);

/* Type Definitions */

/* Named Constants */
#define CALL_EVENT                     (-1)

/* Variable Declarations */

/* Variable Definitions */
static real_T _sfTime_;
static const char * c1_debug_family_names[16] = { "lenA", "centroidsInt", "ii",
  "cr", "cg", "cb", "colorVec", "color", "nargin", "nargout", "centroids",
  "area", "RBW", "GBW", "BBW", "blobs" };

/* Function Declarations */
static void initialize_c1_cameraTest(SFc1_cameraTestInstanceStruct
  *chartInstance);
static void initialize_params_c1_cameraTest(SFc1_cameraTestInstanceStruct
  *chartInstance);
static void enable_c1_cameraTest(SFc1_cameraTestInstanceStruct *chartInstance);
static void disable_c1_cameraTest(SFc1_cameraTestInstanceStruct *chartInstance);
static void c1_update_debugger_state_c1_cameraTest(SFc1_cameraTestInstanceStruct
  *chartInstance);
static const mxArray *get_sim_state_c1_cameraTest(SFc1_cameraTestInstanceStruct *
  chartInstance);
static void set_sim_state_c1_cameraTest(SFc1_cameraTestInstanceStruct
  *chartInstance, const mxArray *c1_st);
static void finalize_c1_cameraTest(SFc1_cameraTestInstanceStruct *chartInstance);
static void sf_gateway_c1_cameraTest(SFc1_cameraTestInstanceStruct
  *chartInstance);
static void mdl_start_c1_cameraTest(SFc1_cameraTestInstanceStruct *chartInstance);
static void c1_chartstep_c1_cameraTest(SFc1_cameraTestInstanceStruct
  *chartInstance);
static void initSimStructsc1_cameraTest(SFc1_cameraTestInstanceStruct
  *chartInstance);
static void init_script_number_translation(uint32_T c1_machineNumber, uint32_T
  c1_chartNumber, uint32_T c1_instanceNumber);
static const mxArray *c1_sf_marshallOut(void *chartInstanceVoid, c1_BlobData
  *c1_inData_data, c1_BlobData_size *c1_inData_elems_sizes);
static void c1_emlrt_marshallIn(SFc1_cameraTestInstanceStruct *chartInstance,
  const mxArray *c1_blobs, const char_T *c1_identifier, c1_BlobData *c1_y_data,
  c1_BlobData_size *c1_y_elems_sizes);
static void c1_b_emlrt_marshallIn(SFc1_cameraTestInstanceStruct *chartInstance,
  const mxArray *c1_u, const emlrtMsgIdentifier *c1_parentId, c1_BlobData
  *c1_y_data, c1_BlobData_size *c1_y_elems_sizes);
static void c1_c_emlrt_marshallIn(SFc1_cameraTestInstanceStruct *chartInstance,
  const mxArray *c1_u, const emlrtMsgIdentifier *c1_parentId, real32_T
  c1_y_data[], int32_T c1_y_sizes[2]);
static void c1_d_emlrt_marshallIn(SFc1_cameraTestInstanceStruct *chartInstance,
  const mxArray *c1_u, const emlrtMsgIdentifier *c1_parentId, int32_T c1_y_data[],
  int32_T *c1_y_sizes);
static void c1_e_emlrt_marshallIn(SFc1_cameraTestInstanceStruct *chartInstance,
  const mxArray *c1_u, const emlrtMsgIdentifier *c1_parentId, uint8_T c1_y_data[],
  int32_T *c1_y_sizes);
static void c1_sf_marshallIn(void *chartInstanceVoid, const mxArray
  *c1_mxArrayInData, const char_T *c1_varName, c1_BlobData *c1_outData_data,
  c1_BlobData_size *c1_outData_elems_sizes);
static const mxArray *c1_b_sf_marshallOut(void *chartInstanceVoid, void
  *c1_inData);
static const mxArray *c1_c_sf_marshallOut(void *chartInstanceVoid, int32_T
  c1_inData_data[], int32_T c1_inData_sizes[2]);
static const mxArray *c1_d_sf_marshallOut(void *chartInstanceVoid, real32_T
  c1_inData_data[], int32_T c1_inData_sizes[2]);
static const mxArray *c1_e_sf_marshallOut(void *chartInstanceVoid, void
  *c1_inData);
static real_T c1_f_emlrt_marshallIn(SFc1_cameraTestInstanceStruct *chartInstance,
  const mxArray *c1_u, const emlrtMsgIdentifier *c1_parentId);
static void c1_b_sf_marshallIn(void *chartInstanceVoid, const mxArray
  *c1_mxArrayInData, const char_T *c1_varName, void *c1_outData);
static const mxArray *c1_f_sf_marshallOut(void *chartInstanceVoid, real_T
  c1_inData_data[], int32_T c1_inData_sizes[2]);
static void c1_g_emlrt_marshallIn(SFc1_cameraTestInstanceStruct *chartInstance,
  const mxArray *c1_u, const emlrtMsgIdentifier *c1_parentId, real_T c1_y_data[],
  int32_T c1_y_sizes[2]);
static void c1_c_sf_marshallIn(void *chartInstanceVoid, const mxArray
  *c1_mxArrayInData, const char_T *c1_varName, real_T c1_outData_data[], int32_T
  c1_outData_sizes[2]);
static const mxArray *c1_g_sf_marshallOut(void *chartInstanceVoid, void
  *c1_inData);
static void c1_h_emlrt_marshallIn(SFc1_cameraTestInstanceStruct *chartInstance,
  const mxArray *c1_u, const emlrtMsgIdentifier *c1_parentId, boolean_T c1_y[3]);
static void c1_d_sf_marshallIn(void *chartInstanceVoid, const mxArray
  *c1_mxArrayInData, const char_T *c1_varName, void *c1_outData);
static const mxArray *c1_h_sf_marshallOut(void *chartInstanceVoid, void
  *c1_inData);
static boolean_T c1_i_emlrt_marshallIn(SFc1_cameraTestInstanceStruct
  *chartInstance, const mxArray *c1_u, const emlrtMsgIdentifier *c1_parentId);
static void c1_e_sf_marshallIn(void *chartInstanceVoid, const mxArray
  *c1_mxArrayInData, const char_T *c1_varName, void *c1_outData);
static void c1_f_sf_marshallIn(void *chartInstanceVoid, const mxArray
  *c1_mxArrayInData, const char_T *c1_varName, real32_T c1_outData_data[],
  int32_T c1_outData_sizes[2]);
static void c1_info_helper(const mxArray **c1_info);
static const mxArray *c1_emlrt_marshallOut(const char * c1_u);
static const mxArray *c1_b_emlrt_marshallOut(const uint32_T c1_u);
static void c1_floor(SFc1_cameraTestInstanceStruct *chartInstance, real32_T
                     c1_x_data[], int32_T c1_x_sizes[2], real32_T c1_b_x_data[],
                     int32_T c1_b_x_sizes[2]);
static const mxArray *c1_i_sf_marshallOut(void *chartInstanceVoid, void
  *c1_inData);
static int32_T c1_j_emlrt_marshallIn(SFc1_cameraTestInstanceStruct
  *chartInstance, const mxArray *c1_u, const emlrtMsgIdentifier *c1_parentId);
static void c1_g_sf_marshallIn(void *chartInstanceVoid, const mxArray
  *c1_mxArrayInData, const char_T *c1_varName, void *c1_outData);
static const mxArray *c1_blobs_bus_io(void *chartInstanceVoid, void *c1_pData);
static const mxArray *c1_sf_marshall_unsupported(void *chartInstanceVoid);
static const mxArray *c1_c_emlrt_marshallOut(SFc1_cameraTestInstanceStruct
  *chartInstance, const char * c1_u);
static uint8_T c1_k_emlrt_marshallIn(SFc1_cameraTestInstanceStruct
  *chartInstance, const mxArray *c1_b_is_active_c1_cameraTest, const char_T
  *c1_identifier);
static uint8_T c1_l_emlrt_marshallIn(SFc1_cameraTestInstanceStruct
  *chartInstance, const mxArray *c1_u, const emlrtMsgIdentifier *c1_parentId);
static void c1_b_floor(SFc1_cameraTestInstanceStruct *chartInstance, real32_T
  c1_x_data[], int32_T c1_x_sizes[2]);
static void init_dsm_address_info(SFc1_cameraTestInstanceStruct *chartInstance);
static void init_simulink_io_address(SFc1_cameraTestInstanceStruct
  *chartInstance);

/* Function Definitions */
static void initialize_c1_cameraTest(SFc1_cameraTestInstanceStruct
  *chartInstance)
{
  chartInstance->c1_sfEvent = CALL_EVENT;
  _sfTime_ = sf_get_time(chartInstance->S);
  chartInstance->c1_is_active_c1_cameraTest = 0U;
}

static void initialize_params_c1_cameraTest(SFc1_cameraTestInstanceStruct
  *chartInstance)
{
  (void)chartInstance;
}

static void enable_c1_cameraTest(SFc1_cameraTestInstanceStruct *chartInstance)
{
  _sfTime_ = sf_get_time(chartInstance->S);
}

static void disable_c1_cameraTest(SFc1_cameraTestInstanceStruct *chartInstance)
{
  _sfTime_ = sf_get_time(chartInstance->S);
}

static void c1_update_debugger_state_c1_cameraTest(SFc1_cameraTestInstanceStruct
  *chartInstance)
{
  (void)chartInstance;
}

static const mxArray *get_sim_state_c1_cameraTest(SFc1_cameraTestInstanceStruct *
  chartInstance)
{
  const mxArray *c1_st;
  const mxArray *c1_y = NULL;
  const mxArray *c1_b_y = NULL;
  int32_T c1_u_sizes[2];
  int32_T c1_u;
  int32_T c1_b_u;
  int32_T c1_loop_ub;
  int32_T c1_i0;
  real32_T c1_u_data[32];
  const mxArray *c1_c_y = NULL;
  int32_T c1_b_u_sizes;
  int32_T c1_b_loop_ub;
  int32_T c1_i1;
  int32_T c1_b_u_data[16];
  const mxArray *c1_d_y = NULL;
  int32_T c1_c_u_sizes;
  int32_T c1_c_loop_ub;
  int32_T c1_i2;
  uint8_T c1_c_u_data[16];
  const mxArray *c1_e_y = NULL;
  uint8_T c1_hoistedGlobal;
  uint8_T c1_c_u;
  const mxArray *c1_f_y = NULL;
  c1_st = NULL;
  c1_st = NULL;
  c1_y = NULL;
  sf_mex_assign(&c1_y, sf_mex_createcellmatrix(2, 1), false);
  c1_b_y = NULL;
  sf_mex_assign(&c1_b_y, sf_mex_createstruct("structure", 2, 1, 1), false);
  c1_u_sizes[0] = chartInstance->c1_blobs_elems_sizes->centroid[0];
  c1_u_sizes[1] = chartInstance->c1_blobs_elems_sizes->centroid[1];
  c1_u = c1_u_sizes[0];
  c1_b_u = c1_u_sizes[1];
  c1_loop_ub = chartInstance->c1_blobs_elems_sizes->centroid[0] *
    chartInstance->c1_blobs_elems_sizes->centroid[1] - 1;
  for (c1_i0 = 0; c1_i0 <= c1_loop_ub; c1_i0++) {
    c1_u_data[c1_i0] = ((real32_T *)&((char_T *)chartInstance->c1_blobs_data)[0])
      [c1_i0];
  }

  c1_c_y = NULL;
  sf_mex_assign(&c1_c_y, sf_mex_create("y", c1_u_data, 1, 0U, 1U, 0U, 2,
    c1_u_sizes[0], c1_u_sizes[1]), false);
  sf_mex_addfield(c1_b_y, c1_c_y, "centroid", "centroid", 0);
  c1_b_u_sizes = chartInstance->c1_blobs_elems_sizes->area;
  c1_b_loop_ub = chartInstance->c1_blobs_elems_sizes->area - 1;
  for (c1_i1 = 0; c1_i1 <= c1_b_loop_ub; c1_i1++) {
    c1_b_u_data[c1_i1] = ((int32_T *)&((char_T *)chartInstance->c1_blobs_data)
                          [128])[c1_i1];
  }

  c1_d_y = NULL;
  sf_mex_assign(&c1_d_y, sf_mex_create("y", c1_b_u_data, 6, 0U, 1U, 0U, 1,
    c1_b_u_sizes), false);
  sf_mex_addfield(c1_b_y, c1_d_y, "area", "area", 0);
  c1_c_u_sizes = chartInstance->c1_blobs_elems_sizes->color;
  c1_c_loop_ub = chartInstance->c1_blobs_elems_sizes->color - 1;
  for (c1_i2 = 0; c1_i2 <= c1_c_loop_ub; c1_i2++) {
    c1_c_u_data[c1_i2] = ((uint8_T *)&((char_T *)chartInstance->c1_blobs_data)
                          [192])[c1_i2];
  }

  c1_e_y = NULL;
  sf_mex_assign(&c1_e_y, sf_mex_create("y", c1_c_u_data, 3, 0U, 1U, 0U, 1,
    c1_c_u_sizes), false);
  sf_mex_addfield(c1_b_y, c1_e_y, "color", "color", 0);
  sf_mex_setcell(c1_y, 0, c1_b_y);
  c1_hoistedGlobal = chartInstance->c1_is_active_c1_cameraTest;
  c1_c_u = c1_hoistedGlobal;
  c1_f_y = NULL;
  sf_mex_assign(&c1_f_y, sf_mex_create("y", &c1_c_u, 3, 0U, 0U, 0U, 0), false);
  sf_mex_setcell(c1_y, 1, c1_f_y);
  sf_mex_assign(&c1_st, c1_y, false);
  return c1_st;
}

static void set_sim_state_c1_cameraTest(SFc1_cameraTestInstanceStruct
  *chartInstance, const mxArray *c1_st)
{
  const mxArray *c1_u;
  c1_BlobData_size c1_tmp_elems_sizes;
  c1_BlobData c1_tmp_data;
  int32_T c1_loop_ub;
  int32_T c1_i3;
  int32_T c1_b_loop_ub;
  int32_T c1_i4;
  int32_T c1_c_loop_ub;
  int32_T c1_i5;
  int32_T c1_d_loop_ub;
  int32_T c1_i6;
  chartInstance->c1_doneDoubleBufferReInit = true;
  c1_u = sf_mex_dup(c1_st);
  c1_emlrt_marshallIn(chartInstance, sf_mex_dup(sf_mex_getcell(c1_u, 0)),
                      "blobs", &c1_tmp_data, &c1_tmp_elems_sizes);
  chartInstance->c1_blobs_elems_sizes->centroid[0] =
    c1_tmp_elems_sizes.centroid[0];
  chartInstance->c1_blobs_elems_sizes->centroid[1] =
    c1_tmp_elems_sizes.centroid[1];
  c1_loop_ub = c1_tmp_elems_sizes.centroid[1] - 1;
  for (c1_i3 = 0; c1_i3 <= c1_loop_ub; c1_i3++) {
    c1_b_loop_ub = c1_tmp_elems_sizes.centroid[0] - 1;
    for (c1_i4 = 0; c1_i4 <= c1_b_loop_ub; c1_i4++) {
      ((real32_T *)&((char_T *)chartInstance->c1_blobs_data)[0])[c1_i4 +
        chartInstance->c1_blobs_elems_sizes->centroid[0] * c1_i3] =
        c1_tmp_data.centroid[c1_i4 + c1_tmp_elems_sizes.centroid[0] * c1_i3];
    }
  }

  chartInstance->c1_blobs_elems_sizes->area = c1_tmp_elems_sizes.area;
  c1_c_loop_ub = c1_tmp_elems_sizes.area - 1;
  for (c1_i5 = 0; c1_i5 <= c1_c_loop_ub; c1_i5++) {
    ((int32_T *)&((char_T *)chartInstance->c1_blobs_data)[128])[c1_i5] =
      c1_tmp_data.area[c1_i5];
  }

  chartInstance->c1_blobs_elems_sizes->color = c1_tmp_elems_sizes.color;
  c1_d_loop_ub = c1_tmp_elems_sizes.color - 1;
  for (c1_i6 = 0; c1_i6 <= c1_d_loop_ub; c1_i6++) {
    ((uint8_T *)&((char_T *)chartInstance->c1_blobs_data)[192])[c1_i6] =
      c1_tmp_data.color[c1_i6];
  }

  chartInstance->c1_is_active_c1_cameraTest = c1_k_emlrt_marshallIn
    (chartInstance, sf_mex_dup(sf_mex_getcell(c1_u, 1)),
     "is_active_c1_cameraTest");
  sf_mex_destroy(&c1_u);
  c1_update_debugger_state_c1_cameraTest(chartInstance);
  sf_mex_destroy(&c1_st);
}

static void finalize_c1_cameraTest(SFc1_cameraTestInstanceStruct *chartInstance)
{
  (void)chartInstance;
}

static void sf_gateway_c1_cameraTest(SFc1_cameraTestInstanceStruct
  *chartInstance)
{
  int32_T c1_loop_ub;
  int32_T c1_i7;
  int32_T c1_b_loop_ub;
  int32_T c1_i8;
  int32_T c1_i9;
  int32_T c1_i10;
  int32_T c1_i11;
  _SFD_SYMBOL_SCOPE_PUSH(0U, 0U);
  _sfTime_ = sf_get_time(chartInstance->S);
  _SFD_CC_CALL(CHART_ENTER_SFUNCTION_TAG, 0U, chartInstance->c1_sfEvent);
  c1_loop_ub = (*chartInstance->c1_centroids_sizes)[0] *
    (*chartInstance->c1_centroids_sizes)[1] - 1;
  for (c1_i7 = 0; c1_i7 <= c1_loop_ub; c1_i7++) {
    _SFD_DATA_RANGE_CHECK((real_T)(*chartInstance->c1_centroids_data)[c1_i7], 0U);
  }

  c1_b_loop_ub = (*chartInstance->c1_area_sizes)[0] *
    (*chartInstance->c1_area_sizes)[1] - 1;
  for (c1_i8 = 0; c1_i8 <= c1_b_loop_ub; c1_i8++) {
    _SFD_DATA_RANGE_CHECK((real_T)(*chartInstance->c1_area_data)[c1_i8], 1U);
  }

  for (c1_i9 = 0; c1_i9 < 76800; c1_i9++) {
    _SFD_DATA_RANGE_CHECK((real_T)(*chartInstance->c1_b_RBW)[c1_i9], 2U);
  }

  for (c1_i10 = 0; c1_i10 < 76800; c1_i10++) {
    _SFD_DATA_RANGE_CHECK((real_T)(*chartInstance->c1_GBW)[c1_i10], 3U);
  }

  for (c1_i11 = 0; c1_i11 < 76800; c1_i11++) {
    _SFD_DATA_RANGE_CHECK((real_T)(*chartInstance->c1_BBW)[c1_i11], 4U);
  }

  chartInstance->c1_sfEvent = CALL_EVENT;
  c1_chartstep_c1_cameraTest(chartInstance);
  _SFD_SYMBOL_SCOPE_POP();
  _SFD_CHECK_FOR_STATE_INCONSISTENCY(_cameraTestMachineNumber_,
    chartInstance->chartNumber, chartInstance->instanceNumber);
}

static void mdl_start_c1_cameraTest(SFc1_cameraTestInstanceStruct *chartInstance)
{
  (void)chartInstance;
}

static void c1_chartstep_c1_cameraTest(SFc1_cameraTestInstanceStruct
  *chartInstance)
{
  int32_T c1_b_centroids_sizes[2];
  int32_T c1_centroids;
  int32_T c1_b_centroids;
  int32_T c1_loop_ub;
  int32_T c1_i12;
  real32_T c1_b_centroids_data[32];
  int32_T c1_b_area_sizes[2];
  int32_T c1_area;
  int32_T c1_b_area;
  int32_T c1_b_loop_ub;
  int32_T c1_i13;
  int32_T c1_b_area_data[16];
  int32_T c1_i14;
  int32_T c1_i15;
  boolean_T c1_b_GBW[76800];
  int32_T c1_i16;
  boolean_T c1_b_BBW[76800];
  uint32_T c1_debug_family_var_map[16];
  real_T c1_lenA;
  int32_T c1_centroidsInt_sizes[2];
  real32_T c1_centroidsInt_data[32];
  real_T c1_ii;
  boolean_T c1_cr;
  boolean_T c1_cg;
  boolean_T c1_cb;
  boolean_T c1_colorVec[3];
  int32_T c1_color_sizes[2];
  real_T c1_color_data[3];
  real_T c1_nargin = 5.0;
  real_T c1_nargout = 1.0;
  c1_BlobData_size c1_b_blobs_elems_sizes;
  c1_BlobData c1_b_blobs_data;
  int32_T c1_x_sizes;
  int32_T c1_c_loop_ub;
  int32_T c1_i17;
  int32_T c1_x_data[16];
  int32_T c1_tmp_sizes[2];
  int32_T c1_i18;
  int32_T c1_i19;
  int32_T c1_d_loop_ub;
  int32_T c1_i20;
  real32_T c1_tmp_data[32];
  int32_T c1_centroidsInt;
  int32_T c1_b_centroidsInt;
  int32_T c1_e_loop_ub;
  int32_T c1_i21;
  int32_T c1_blobs;
  int32_T c1_b_blobs;
  real_T c1_b_lenA;
  int32_T c1_i22;
  int32_T c1_b_ii;
  int32_T c1_i23;
  boolean_T c1_x[3];
  int32_T c1_idx;
  int32_T c1_i24;
  int32_T c1_ii_sizes[2];
  int32_T c1_c_ii;
  int32_T c1_d_ii;
  int32_T c1_a;
  int32_T c1_b_a;
  int32_T c1_ii_data[3];
  boolean_T c1_b0;
  boolean_T c1_b1;
  boolean_T c1_b2;
  int32_T c1_i25;
  int32_T c1_color;
  int32_T c1_b_color;
  int32_T c1_f_loop_ub;
  int32_T c1_i26;
  int32_T c1_b_x_sizes[2];
  int32_T c1_b_x;
  int32_T c1_c_x;
  int32_T c1_g_loop_ub;
  int32_T c1_i27;
  real_T c1_b_x_data[3];
  real_T c1_d0;
  int32_T c1_i28;
  int32_T c1_e_ii;
  int32_T c1_b_tmp_sizes[2];
  int32_T c1_h_loop_ub;
  int32_T c1_i29;
  real32_T c1_b_tmp_data[2];
  int32_T c1_c_tmp_sizes[2];
  int32_T c1_i_loop_ub;
  int32_T c1_i30;
  int32_T c1_j_loop_ub;
  int32_T c1_i31;
  real32_T c1_c_tmp_data[34];
  int32_T c1_k_loop_ub;
  int32_T c1_i32;
  int32_T c1_c_blobs;
  int32_T c1_d_blobs;
  int32_T c1_l_loop_ub;
  int32_T c1_i33;
  int32_T c1_m_loop_ub;
  int32_T c1_i34;
  int32_T c1_d_tmp_sizes;
  int32_T c1_n_loop_ub;
  int32_T c1_i35;
  int32_T c1_d_tmp_data[17];
  int32_T c1_o_loop_ub;
  int32_T c1_i36;
  int32_T c1_e_tmp_sizes;
  int32_T c1_p_loop_ub;
  int32_T c1_i37;
  uint8_T c1_e_tmp_data[17];
  real_T c1_d1;
  uint8_T c1_u0;
  int32_T c1_q_loop_ub;
  int32_T c1_i38;
  int32_T c1_i39;
  int32_T c1_c_x_sizes;
  int32_T c1_r_loop_ub;
  int32_T c1_i40;
  real32_T c1_c_x_data[16];
  real_T c1_n;
  int32_T c1_d_x_sizes;
  int32_T c1_s_loop_ub;
  int32_T c1_i41;
  uint8_T c1_d_x_data[16];
  real_T c1_b_n;
  const mxArray *c1_y = NULL;
  int32_T c1_t_loop_ub;
  int32_T c1_i42;
  int32_T c1_u_loop_ub;
  int32_T c1_i43;
  int32_T c1_v_loop_ub;
  int32_T c1_i44;
  int32_T c1_w_loop_ub;
  int32_T c1_i45;
  boolean_T exitg1;
  boolean_T guard1 = false;
  _SFD_CC_CALL(CHART_ENTER_DURING_FUNCTION_TAG, 0U, chartInstance->c1_sfEvent);
  c1_b_centroids_sizes[0] = (*chartInstance->c1_centroids_sizes)[0];
  c1_b_centroids_sizes[1] = (*chartInstance->c1_centroids_sizes)[1];
  c1_centroids = c1_b_centroids_sizes[0];
  c1_b_centroids = c1_b_centroids_sizes[1];
  c1_loop_ub = (*chartInstance->c1_centroids_sizes)[0] *
    (*chartInstance->c1_centroids_sizes)[1] - 1;
  for (c1_i12 = 0; c1_i12 <= c1_loop_ub; c1_i12++) {
    c1_b_centroids_data[c1_i12] = (*chartInstance->c1_centroids_data)[c1_i12];
  }

  c1_b_area_sizes[0] = (*chartInstance->c1_area_sizes)[0];
  c1_b_area_sizes[1] = 1;
  c1_area = c1_b_area_sizes[0];
  c1_b_area = c1_b_area_sizes[1];
  c1_b_loop_ub = (*chartInstance->c1_area_sizes)[0] *
    (*chartInstance->c1_area_sizes)[1] - 1;
  for (c1_i13 = 0; c1_i13 <= c1_b_loop_ub; c1_i13++) {
    c1_b_area_data[c1_i13] = (*chartInstance->c1_area_data)[c1_i13];
  }

  for (c1_i14 = 0; c1_i14 < 76800; c1_i14++) {
    chartInstance->c1_RBW[c1_i14] = (*chartInstance->c1_b_RBW)[c1_i14];
  }

  for (c1_i15 = 0; c1_i15 < 76800; c1_i15++) {
    c1_b_GBW[c1_i15] = (*chartInstance->c1_GBW)[c1_i15];
  }

  for (c1_i16 = 0; c1_i16 < 76800; c1_i16++) {
    c1_b_BBW[c1_i16] = (*chartInstance->c1_BBW)[c1_i16];
  }

  _SFD_SYMBOL_SCOPE_PUSH_EML(0U, 16U, 16U, c1_debug_family_names,
    c1_debug_family_var_map);
  _SFD_SYMBOL_SCOPE_ADD_EML_IMPORTABLE(&c1_lenA, 0U, c1_e_sf_marshallOut,
    c1_b_sf_marshallIn);
  _SFD_SYMBOL_SCOPE_ADD_EML_DYN_IMPORTABLE(c1_centroidsInt_data, (const int32_T *)
    &c1_centroidsInt_sizes, NULL, 0, 1, (void *)c1_d_sf_marshallOut, (void *)
    c1_f_sf_marshallIn);
  _SFD_SYMBOL_SCOPE_ADD_EML_IMPORTABLE(&c1_ii, 2U, c1_e_sf_marshallOut,
    c1_b_sf_marshallIn);
  _SFD_SYMBOL_SCOPE_ADD_EML_IMPORTABLE(&c1_cr, 3U, c1_h_sf_marshallOut,
    c1_e_sf_marshallIn);
  _SFD_SYMBOL_SCOPE_ADD_EML_IMPORTABLE(&c1_cg, 4U, c1_h_sf_marshallOut,
    c1_e_sf_marshallIn);
  _SFD_SYMBOL_SCOPE_ADD_EML_IMPORTABLE(&c1_cb, 5U, c1_h_sf_marshallOut,
    c1_e_sf_marshallIn);
  _SFD_SYMBOL_SCOPE_ADD_EML_IMPORTABLE(c1_colorVec, 6U, c1_g_sf_marshallOut,
    c1_d_sf_marshallIn);
  _SFD_SYMBOL_SCOPE_ADD_EML_DYN_IMPORTABLE(c1_color_data, (const int32_T *)
    &c1_color_sizes, NULL, 0, 7, (void *)c1_f_sf_marshallOut, (void *)
    c1_c_sf_marshallIn);
  _SFD_SYMBOL_SCOPE_ADD_EML_IMPORTABLE(&c1_nargin, 8U, c1_e_sf_marshallOut,
    c1_b_sf_marshallIn);
  _SFD_SYMBOL_SCOPE_ADD_EML_IMPORTABLE(&c1_nargout, 9U, c1_e_sf_marshallOut,
    c1_b_sf_marshallIn);
  _SFD_SYMBOL_SCOPE_ADD_EML_DYN(c1_b_centroids_data, (const int32_T *)
    &c1_b_centroids_sizes, NULL, 1, 10, (void *)c1_d_sf_marshallOut);
  _SFD_SYMBOL_SCOPE_ADD_EML_DYN(c1_b_area_data, (const int32_T *)
    &c1_b_area_sizes, NULL, 1, 11, (void *)c1_c_sf_marshallOut);
  _SFD_SYMBOL_SCOPE_ADD_EML(chartInstance->c1_RBW, 12U, c1_b_sf_marshallOut);
  _SFD_SYMBOL_SCOPE_ADD_EML(c1_b_GBW, 13U, c1_b_sf_marshallOut);
  _SFD_SYMBOL_SCOPE_ADD_EML(c1_b_BBW, 14U, c1_b_sf_marshallOut);
  _SFD_SYMBOL_SCOPE_ADD_EML_DYN_IMPORTABLE(&c1_b_blobs_data, NULL,
    &c1_b_blobs_elems_sizes, 0, 15, (void *)c1_sf_marshallOut, (void *)
    c1_sf_marshallIn);
  CV_EML_FCN(0, 0);
  _SFD_EML_CALL(0U, chartInstance->c1_sfEvent, 4);
  c1_x_sizes = c1_b_area_sizes[0];
  c1_c_loop_ub = c1_b_area_sizes[0] - 1;
  for (c1_i17 = 0; c1_i17 <= c1_c_loop_ub; c1_i17++) {
    c1_x_data[c1_i17] = c1_b_area_data[c1_i17];
  }

  c1_lenA = (real_T)c1_x_sizes;
  _SFD_EML_CALL(0U, chartInstance->c1_sfEvent, 5);
  c1_tmp_sizes[0] = c1_b_centroids_sizes[0];
  c1_tmp_sizes[1] = c1_b_centroids_sizes[1];
  c1_i18 = c1_tmp_sizes[0];
  c1_i19 = c1_tmp_sizes[1];
  c1_d_loop_ub = c1_b_centroids_sizes[0] * c1_b_centroids_sizes[1] - 1;
  for (c1_i20 = 0; c1_i20 <= c1_d_loop_ub; c1_i20++) {
    c1_tmp_data[c1_i20] = c1_b_centroids_data[c1_i20];
  }

  c1_b_floor(chartInstance, c1_tmp_data, c1_tmp_sizes);
  c1_centroidsInt_sizes[0] = c1_tmp_sizes[0];
  c1_centroidsInt_sizes[1] = c1_tmp_sizes[1];
  c1_centroidsInt = c1_centroidsInt_sizes[0];
  c1_b_centroidsInt = c1_centroidsInt_sizes[1];
  c1_e_loop_ub = c1_tmp_sizes[0] * c1_tmp_sizes[1] - 1;
  for (c1_i21 = 0; c1_i21 <= c1_e_loop_ub; c1_i21++) {
    c1_centroidsInt_data[c1_i21] = c1_tmp_data[c1_i21];
  }

  _SFD_EML_CALL(0U, chartInstance->c1_sfEvent, 6);
  _SFD_DIM_SIZE_GEQ_CHECK(16, 0, 1);
  c1_b_blobs_elems_sizes.centroid[0] = 0;
  c1_b_blobs_elems_sizes.centroid[1] = 2;
  c1_blobs = c1_b_blobs_elems_sizes.centroid[0];
  c1_b_blobs = c1_b_blobs_elems_sizes.centroid[1];
  _SFD_EML_CALL(0U, chartInstance->c1_sfEvent, 7);
  _SFD_DIM_SIZE_GEQ_CHECK(16, 0, 1);
  c1_b_blobs_elems_sizes.area = 0;
  _SFD_EML_CALL(0U, chartInstance->c1_sfEvent, 8);
  _SFD_DIM_SIZE_GEQ_CHECK(16, 0, 1);
  c1_b_blobs_elems_sizes.color = 0;
  _SFD_EML_CALL(0U, chartInstance->c1_sfEvent, 9);
  _SFD_EML_CALL(0U, chartInstance->c1_sfEvent, 10);
  _SFD_EML_CALL(0U, chartInstance->c1_sfEvent, 11);
  _SFD_EML_CALL(0U, chartInstance->c1_sfEvent, 13);
  c1_b_lenA = c1_lenA;
  c1_i22 = (int32_T)c1_b_lenA - 1;
  c1_ii = 1.0;
  c1_b_ii = 0;
  while (c1_b_ii <= c1_i22) {
    c1_ii = 1.0 + (real_T)c1_b_ii;
    CV_EML_FOR(0, 1, 0, 1);
    _SFD_EML_CALL(0U, chartInstance->c1_sfEvent, 14);
    (real_T)_SFD_EML_ARRAY_BOUNDS_CHECK("centroidsInt", 2, 1,
      c1_centroidsInt_sizes[1], 2, 0);
    (real_T)_SFD_EML_ARRAY_BOUNDS_CHECK("centroidsInt", 1, 1,
      c1_centroidsInt_sizes[1], 2, 0);
    c1_cr = chartInstance->c1_RBW[((int32_T)(real32_T)
      _SFD_EML_ARRAY_BOUNDS_CHECK("RBW", (int32_T)(real32_T)_SFD_INTEGER_CHECK(
      "centroidsInt(ii,2)", (real_T)c1_centroidsInt_data
      [(_SFD_EML_ARRAY_BOUNDS_CHECK("centroidsInt", (int32_T)c1_ii, 1,
      c1_centroidsInt_sizes[0], 1, 0) + c1_centroidsInt_sizes[0]) - 1]), 1, 320,
      1, 0) + 320 * ((int32_T)(real32_T)_SFD_EML_ARRAY_BOUNDS_CHECK("RBW",
      (int32_T)(real32_T)_SFD_INTEGER_CHECK("centroidsInt(ii,1)", (real_T)
      c1_centroidsInt_data[_SFD_EML_ARRAY_BOUNDS_CHECK("centroidsInt", (int32_T)
      c1_ii, 1, c1_centroidsInt_sizes[0], 1, 0) - 1]), 1, 240, 2, 0) - 1)) - 1];
    _SFD_EML_CALL(0U, chartInstance->c1_sfEvent, 15);
    (real_T)_SFD_EML_ARRAY_BOUNDS_CHECK("centroidsInt", 2, 1,
      c1_centroidsInt_sizes[1], 2, 0);
    (real_T)_SFD_EML_ARRAY_BOUNDS_CHECK("centroidsInt", 1, 1,
      c1_centroidsInt_sizes[1], 2, 0);
    c1_cg = c1_b_GBW[((int32_T)(real32_T)_SFD_EML_ARRAY_BOUNDS_CHECK("GBW",
      (int32_T)(real32_T)_SFD_INTEGER_CHECK("centroidsInt(ii,2)", (real_T)
      c1_centroidsInt_data[(_SFD_EML_ARRAY_BOUNDS_CHECK("centroidsInt", (int32_T)
      c1_ii, 1, c1_centroidsInt_sizes[0], 1, 0) + c1_centroidsInt_sizes[0]) - 1]),
      1, 320, 1, 0) + 320 * ((int32_T)(real32_T)_SFD_EML_ARRAY_BOUNDS_CHECK(
      "GBW", (int32_T)(real32_T)_SFD_INTEGER_CHECK("centroidsInt(ii,1)", (real_T)
      c1_centroidsInt_data[_SFD_EML_ARRAY_BOUNDS_CHECK("centroidsInt", (int32_T)
      c1_ii, 1, c1_centroidsInt_sizes[0], 1, 0) - 1]), 1, 240, 2, 0) - 1)) - 1];
    _SFD_EML_CALL(0U, chartInstance->c1_sfEvent, 16);
    (real_T)_SFD_EML_ARRAY_BOUNDS_CHECK("centroidsInt", 2, 1,
      c1_centroidsInt_sizes[1], 2, 0);
    (real_T)_SFD_EML_ARRAY_BOUNDS_CHECK("centroidsInt", 1, 1,
      c1_centroidsInt_sizes[1], 2, 0);
    c1_cb = c1_b_BBW[((int32_T)(real32_T)_SFD_EML_ARRAY_BOUNDS_CHECK("BBW",
      (int32_T)(real32_T)_SFD_INTEGER_CHECK("centroidsInt(ii,2)", (real_T)
      c1_centroidsInt_data[(_SFD_EML_ARRAY_BOUNDS_CHECK("centroidsInt", (int32_T)
      c1_ii, 1, c1_centroidsInt_sizes[0], 1, 0) + c1_centroidsInt_sizes[0]) - 1]),
      1, 320, 1, 0) + 320 * ((int32_T)(real32_T)_SFD_EML_ARRAY_BOUNDS_CHECK(
      "BBW", (int32_T)(real32_T)_SFD_INTEGER_CHECK("centroidsInt(ii,1)", (real_T)
      c1_centroidsInt_data[_SFD_EML_ARRAY_BOUNDS_CHECK("centroidsInt", (int32_T)
      c1_ii, 1, c1_centroidsInt_sizes[0], 1, 0) - 1]), 1, 240, 2, 0) - 1)) - 1];
    _SFD_EML_CALL(0U, chartInstance->c1_sfEvent, 17);
    c1_colorVec[0] = c1_cr;
    c1_colorVec[1] = c1_cg;
    c1_colorVec[2] = c1_cb;
    _SFD_EML_CALL(0U, chartInstance->c1_sfEvent, 18);
    for (c1_i23 = 0; c1_i23 < 3; c1_i23++) {
      c1_x[c1_i23] = c1_colorVec[c1_i23];
    }

    c1_idx = 0;
    for (c1_i24 = 0; c1_i24 < 2; c1_i24++) {
      c1_ii_sizes[c1_i24] = 1 + (c1_i24 << 1);
    }

    c1_c_ii = 1;
    exitg1 = false;
    while ((exitg1 == false) && (c1_c_ii < 4)) {
      c1_d_ii = c1_c_ii;
      guard1 = false;
      if (c1_x[c1_d_ii - 1]) {
        c1_a = c1_idx;
        c1_b_a = c1_a + 1;
        c1_idx = c1_b_a;
        c1_ii_data[c1_idx - 1] = c1_d_ii;
        if (c1_idx >= 3) {
          exitg1 = true;
        } else {
          guard1 = true;
        }
      } else {
        guard1 = true;
      }

      if (guard1 == true) {
        c1_c_ii++;
      }
    }

    c1_b0 = (1 > c1_idx);
    c1_b1 = c1_b0;
    c1_b2 = c1_b1;
    if (c1_b2) {
      c1_i25 = 0;
    } else {
      c1_i25 = _SFD_EML_ARRAY_BOUNDS_CHECK("", c1_idx, 1, 3, 0, 0);
    }

    c1_ii_sizes[1] = c1_i25;
    c1_color_sizes[0] = 1;
    c1_color_sizes[1] = c1_ii_sizes[1];
    c1_color = c1_color_sizes[0];
    c1_b_color = c1_color_sizes[1];
    c1_f_loop_ub = c1_ii_sizes[0] * c1_ii_sizes[1] - 1;
    for (c1_i26 = 0; c1_i26 <= c1_f_loop_ub; c1_i26++) {
      c1_color_data[c1_i26] = (real_T)c1_ii_data[c1_i26];
    }

    _SFD_EML_CALL(0U, chartInstance->c1_sfEvent, 19);
    c1_b_x_sizes[0] = 1;
    c1_b_x_sizes[1] = c1_color_sizes[1];
    c1_b_x = c1_b_x_sizes[0];
    c1_c_x = c1_b_x_sizes[1];
    c1_g_loop_ub = c1_color_sizes[0] * c1_color_sizes[1] - 1;
    for (c1_i27 = 0; c1_i27 <= c1_g_loop_ub; c1_i27++) {
      c1_b_x_data[c1_i27] = c1_color_data[c1_i27];
    }

    c1_d0 = (real_T)c1_b_x_sizes[1];
    if (CV_EML_IF(0, 1, 0, CV_RELATIONAL_EVAL(4U, 0U, 0, c1_d0, 1.0, -1, 0U,
          c1_d0 == 1.0))) {
      _SFD_EML_CALL(0U, chartInstance->c1_sfEvent, 20);
      c1_i28 = c1_b_centroids_sizes[1];
      c1_e_ii = _SFD_EML_ARRAY_BOUNDS_CHECK("centroids", (int32_T)c1_ii, 1,
        c1_b_centroids_sizes[0], 1, 0) - 1;
      c1_b_tmp_sizes[0] = 1;
      c1_b_tmp_sizes[1] = c1_i28;
      c1_h_loop_ub = c1_i28 - 1;
      for (c1_i29 = 0; c1_i29 <= c1_h_loop_ub; c1_i29++) {
        c1_b_tmp_data[c1_b_tmp_sizes[0] * c1_i29] = c1_b_centroids_data[c1_e_ii
          + c1_b_centroids_sizes[0] * c1_i29];
      }

      _SFD_DIM_SIZE_EQ_CHECK(2, c1_b_tmp_sizes[1], 2);
      c1_c_tmp_sizes[0] = c1_b_blobs_elems_sizes.centroid[0] + 1;
      c1_c_tmp_sizes[1] = c1_b_blobs_elems_sizes.centroid[1];
      c1_i_loop_ub = c1_b_blobs_elems_sizes.centroid[1] - 1;
      for (c1_i30 = 0; c1_i30 <= c1_i_loop_ub; c1_i30++) {
        c1_j_loop_ub = c1_b_blobs_elems_sizes.centroid[0] - 1;
        for (c1_i31 = 0; c1_i31 <= c1_j_loop_ub; c1_i31++) {
          c1_c_tmp_data[c1_i31 + c1_c_tmp_sizes[0] * c1_i30] =
            c1_b_blobs_data.centroid[c1_i31 + c1_b_blobs_elems_sizes.centroid[0]
            * c1_i30];
        }
      }

      c1_k_loop_ub = c1_b_tmp_sizes[1] - 1;
      for (c1_i32 = 0; c1_i32 <= c1_k_loop_ub; c1_i32++) {
        c1_c_tmp_data[c1_b_blobs_elems_sizes.centroid[0] + c1_c_tmp_sizes[0] *
          c1_i32] = c1_b_tmp_data[c1_b_tmp_sizes[0] * c1_i32];
      }

      _SFD_DIM_SIZE_GEQ_CHECK(16, c1_c_tmp_sizes[0], 1);
      c1_b_blobs_elems_sizes.centroid[0] = c1_c_tmp_sizes[0];
      c1_b_blobs_elems_sizes.centroid[1] = 2;
      c1_c_blobs = c1_b_blobs_elems_sizes.centroid[0];
      c1_d_blobs = c1_b_blobs_elems_sizes.centroid[1];
      c1_l_loop_ub = c1_c_tmp_sizes[0] * c1_c_tmp_sizes[1] - 1;
      for (c1_i33 = 0; c1_i33 <= c1_l_loop_ub; c1_i33++) {
        c1_b_blobs_data.centroid[c1_i33] = c1_c_tmp_data[c1_i33];
      }

      _SFD_EML_CALL(0U, chartInstance->c1_sfEvent, 21);
      c1_x_sizes = c1_b_area_sizes[0];
      c1_m_loop_ub = c1_b_area_sizes[0] - 1;
      for (c1_i34 = 0; c1_i34 <= c1_m_loop_ub; c1_i34++) {
        c1_x_data[c1_i34] = c1_b_area_data[c1_i34];
      }

      c1_d_tmp_sizes = c1_b_blobs_elems_sizes.area + 1;
      c1_n_loop_ub = c1_b_blobs_elems_sizes.area - 1;
      for (c1_i35 = 0; c1_i35 <= c1_n_loop_ub; c1_i35++) {
        c1_d_tmp_data[c1_i35] = c1_b_blobs_data.area[c1_i35];
      }

      c1_d_tmp_data[c1_b_blobs_elems_sizes.area] =
        c1_x_data[_SFD_EML_ARRAY_BOUNDS_CHECK("area", (int32_T)c1_ii, 1,
        c1_x_sizes, 1, 0) - 1];
      _SFD_DIM_SIZE_GEQ_CHECK(16, c1_d_tmp_sizes, 1);
      c1_b_blobs_elems_sizes.area = c1_d_tmp_sizes;
      c1_o_loop_ub = c1_d_tmp_sizes - 1;
      for (c1_i36 = 0; c1_i36 <= c1_o_loop_ub; c1_i36++) {
        c1_b_blobs_data.area[c1_i36] = c1_d_tmp_data[c1_i36];
      }

      _SFD_EML_CALL(0U, chartInstance->c1_sfEvent, 22);
      (real_T)_SFD_EML_ARRAY_BOUNDS_CHECK("color", 1, 1, c1_color_sizes[1], 2, 0);
      c1_e_tmp_sizes = c1_b_blobs_elems_sizes.color + 1;
      c1_p_loop_ub = c1_b_blobs_elems_sizes.color - 1;
      for (c1_i37 = 0; c1_i37 <= c1_p_loop_ub; c1_i37++) {
        c1_e_tmp_data[c1_i37] = c1_b_blobs_data.color[c1_i37];
      }

      c1_d1 = muDoubleScalarRound(c1_color_data[0]);
      if (c1_d1 < 256.0) {
        if (CV_SATURATION_EVAL(4, 0, 0, 1, c1_d1 >= 0.0)) {
          c1_u0 = (uint8_T)c1_d1;
        } else {
          c1_u0 = 0U;
        }
      } else if (CV_SATURATION_EVAL(4, 0, 0, 0, c1_d1 >= 256.0)) {
        c1_u0 = MAX_uint8_T;
      } else {
        c1_u0 = 0U;
      }

      c1_e_tmp_data[c1_b_blobs_elems_sizes.color] = c1_u0;
      _SFD_DIM_SIZE_GEQ_CHECK(16, c1_e_tmp_sizes, 1);
      c1_b_blobs_elems_sizes.color = c1_e_tmp_sizes;
      c1_q_loop_ub = c1_e_tmp_sizes - 1;
      for (c1_i38 = 0; c1_i38 <= c1_q_loop_ub; c1_i38++) {
        c1_b_blobs_data.color[c1_i38] = c1_e_tmp_data[c1_i38];
      }
    }

    c1_b_ii++;
    _SF_MEX_LISTEN_FOR_CTRL_C(chartInstance->S);
  }

  CV_EML_FOR(0, 1, 0, 0);
  _SFD_EML_CALL(0U, chartInstance->c1_sfEvent, 25);
  c1_i39 = c1_b_blobs_elems_sizes.centroid[0];
  c1_c_x_sizes = c1_i39;
  c1_r_loop_ub = c1_i39 - 1;
  for (c1_i40 = 0; c1_i40 <= c1_r_loop_ub; c1_i40++) {
    c1_c_x_data[c1_i40] = c1_b_blobs_data.centroid[c1_i40];
  }

  c1_n = (real_T)c1_c_x_sizes;
  c1_d_x_sizes = c1_b_blobs_elems_sizes.color;
  c1_s_loop_ub = c1_b_blobs_elems_sizes.color - 1;
  for (c1_i41 = 0; c1_i41 <= c1_s_loop_ub; c1_i41++) {
    c1_d_x_data[c1_i41] = c1_b_blobs_data.color[c1_i41];
  }

  c1_b_n = (real_T)c1_d_x_sizes;
  if (c1_n == c1_b_n) {
  } else {
    c1_y = NULL;
    sf_mex_assign(&c1_y, sf_mex_create("y", "Assertion failed.", 15, 0U, 0U, 0U,
      2, 1, strlen("Assertion failed.")), false);
    sf_mex_call_debug(sfGlobalDebugInstanceStruct, "error", 0U, 1U, 14, c1_y);
  }

  _SFD_EML_CALL(0U, chartInstance->c1_sfEvent, -25);
  _SFD_SYMBOL_SCOPE_POP();
  chartInstance->c1_blobs_elems_sizes->centroid[0] =
    c1_b_blobs_elems_sizes.centroid[0];
  chartInstance->c1_blobs_elems_sizes->centroid[1] =
    c1_b_blobs_elems_sizes.centroid[1];
  c1_t_loop_ub = c1_b_blobs_elems_sizes.centroid[1] - 1;
  for (c1_i42 = 0; c1_i42 <= c1_t_loop_ub; c1_i42++) {
    c1_u_loop_ub = c1_b_blobs_elems_sizes.centroid[0] - 1;
    for (c1_i43 = 0; c1_i43 <= c1_u_loop_ub; c1_i43++) {
      ((real32_T *)&((char_T *)chartInstance->c1_blobs_data)[0])[c1_i43 +
        chartInstance->c1_blobs_elems_sizes->centroid[0] * c1_i42] =
        c1_b_blobs_data.centroid[c1_i43 + c1_b_blobs_elems_sizes.centroid[0] *
        c1_i42];
    }
  }

  chartInstance->c1_blobs_elems_sizes->area = c1_b_blobs_elems_sizes.area;
  c1_v_loop_ub = c1_b_blobs_elems_sizes.area - 1;
  for (c1_i44 = 0; c1_i44 <= c1_v_loop_ub; c1_i44++) {
    ((int32_T *)&((char_T *)chartInstance->c1_blobs_data)[128])[c1_i44] =
      c1_b_blobs_data.area[c1_i44];
  }

  chartInstance->c1_blobs_elems_sizes->color = c1_b_blobs_elems_sizes.color;
  c1_w_loop_ub = c1_b_blobs_elems_sizes.color - 1;
  for (c1_i45 = 0; c1_i45 <= c1_w_loop_ub; c1_i45++) {
    ((uint8_T *)&((char_T *)chartInstance->c1_blobs_data)[192])[c1_i45] =
      c1_b_blobs_data.color[c1_i45];
  }

  _SFD_CC_CALL(EXIT_OUT_OF_FUNCTION_TAG, 0U, chartInstance->c1_sfEvent);
}

static void initSimStructsc1_cameraTest(SFc1_cameraTestInstanceStruct
  *chartInstance)
{
  (void)chartInstance;
}

static void init_script_number_translation(uint32_T c1_machineNumber, uint32_T
  c1_chartNumber, uint32_T c1_instanceNumber)
{
  (void)c1_machineNumber;
  (void)c1_chartNumber;
  (void)c1_instanceNumber;
}

static const mxArray *c1_sf_marshallOut(void *chartInstanceVoid, c1_BlobData
  *c1_inData_data, c1_BlobData_size *c1_inData_elems_sizes)
{
  const mxArray *c1_mxArrayOutData = NULL;
  c1_BlobData_size c1_u_elems_sizes;
  c1_BlobData c1_u_data;
  const mxArray *c1_y = NULL;
  int32_T c1_u_sizes[2];
  int32_T c1_u;
  int32_T c1_b_u;
  int32_T c1_loop_ub;
  int32_T c1_i46;
  real32_T c1_b_u_data[32];
  const mxArray *c1_b_y = NULL;
  int32_T c1_b_u_sizes;
  int32_T c1_b_loop_ub;
  int32_T c1_i47;
  int32_T c1_c_u_data[16];
  const mxArray *c1_c_y = NULL;
  int32_T c1_c_u_sizes;
  int32_T c1_c_loop_ub;
  int32_T c1_i48;
  uint8_T c1_d_u_data[16];
  const mxArray *c1_d_y = NULL;
  SFc1_cameraTestInstanceStruct *chartInstance;
  chartInstance = (SFc1_cameraTestInstanceStruct *)chartInstanceVoid;
  c1_mxArrayOutData = NULL;
  c1_u_elems_sizes = *c1_inData_elems_sizes;
  c1_u_data = *c1_inData_data;
  c1_y = NULL;
  sf_mex_assign(&c1_y, sf_mex_createstruct("structure", 2, 1, 1), false);
  c1_u_sizes[0] = c1_u_elems_sizes.centroid[0];
  c1_u_sizes[1] = c1_u_elems_sizes.centroid[1];
  c1_u = c1_u_sizes[0];
  c1_b_u = c1_u_sizes[1];
  c1_loop_ub = c1_u_elems_sizes.centroid[0] * c1_u_elems_sizes.centroid[1] - 1;
  for (c1_i46 = 0; c1_i46 <= c1_loop_ub; c1_i46++) {
    c1_b_u_data[c1_i46] = c1_u_data.centroid[c1_i46];
  }

  c1_b_y = NULL;
  sf_mex_assign(&c1_b_y, sf_mex_create("y", c1_b_u_data, 1, 0U, 1U, 0U, 2,
    c1_u_sizes[0], c1_u_sizes[1]), false);
  sf_mex_addfield(c1_y, c1_b_y, "centroid", "centroid", 0);
  c1_b_u_sizes = c1_u_elems_sizes.area;
  c1_b_loop_ub = c1_u_elems_sizes.area - 1;
  for (c1_i47 = 0; c1_i47 <= c1_b_loop_ub; c1_i47++) {
    c1_c_u_data[c1_i47] = c1_u_data.area[c1_i47];
  }

  c1_c_y = NULL;
  sf_mex_assign(&c1_c_y, sf_mex_create("y", c1_c_u_data, 6, 0U, 1U, 0U, 1,
    c1_b_u_sizes), false);
  sf_mex_addfield(c1_y, c1_c_y, "area", "area", 0);
  c1_c_u_sizes = c1_u_elems_sizes.color;
  c1_c_loop_ub = c1_u_elems_sizes.color - 1;
  for (c1_i48 = 0; c1_i48 <= c1_c_loop_ub; c1_i48++) {
    c1_d_u_data[c1_i48] = c1_u_data.color[c1_i48];
  }

  c1_d_y = NULL;
  sf_mex_assign(&c1_d_y, sf_mex_create("y", c1_d_u_data, 3, 0U, 1U, 0U, 1,
    c1_c_u_sizes), false);
  sf_mex_addfield(c1_y, c1_d_y, "color", "color", 0);
  sf_mex_assign(&c1_mxArrayOutData, c1_y, false);
  return c1_mxArrayOutData;
}

static void c1_emlrt_marshallIn(SFc1_cameraTestInstanceStruct *chartInstance,
  const mxArray *c1_blobs, const char_T *c1_identifier, c1_BlobData *c1_y_data,
  c1_BlobData_size *c1_y_elems_sizes)
{
  emlrtMsgIdentifier c1_thisId;
  c1_thisId.fIdentifier = c1_identifier;
  c1_thisId.fParent = NULL;
  c1_b_emlrt_marshallIn(chartInstance, sf_mex_dup(c1_blobs), &c1_thisId,
                        c1_y_data, c1_y_elems_sizes);
  sf_mex_destroy(&c1_blobs);
}

static void c1_b_emlrt_marshallIn(SFc1_cameraTestInstanceStruct *chartInstance,
  const mxArray *c1_u, const emlrtMsgIdentifier *c1_parentId, c1_BlobData
  *c1_y_data, c1_BlobData_size *c1_y_elems_sizes)
{
  emlrtMsgIdentifier c1_thisId;
  static const char * c1_fieldNames[3] = { "centroid", "area", "color" };

  c1_thisId.fParent = c1_parentId;
  sf_mex_check_struct(c1_parentId, c1_u, 3, c1_fieldNames, 0U, NULL);
  c1_thisId.fIdentifier = "centroid";
  c1_c_emlrt_marshallIn(chartInstance, sf_mex_dup(sf_mex_getfield(c1_u,
    "centroid", "centroid", 0)), &c1_thisId, c1_y_data->centroid,
                        c1_y_elems_sizes->centroid);
  c1_thisId.fIdentifier = "area";
  c1_d_emlrt_marshallIn(chartInstance, sf_mex_dup(sf_mex_getfield(c1_u, "area",
    "area", 0)), &c1_thisId, c1_y_data->area, &c1_y_elems_sizes->area);
  c1_thisId.fIdentifier = "color";
  c1_e_emlrt_marshallIn(chartInstance, sf_mex_dup(sf_mex_getfield(c1_u, "color",
    "color", 0)), &c1_thisId, c1_y_data->color, &c1_y_elems_sizes->color);
  sf_mex_destroy(&c1_u);
}

static void c1_c_emlrt_marshallIn(SFc1_cameraTestInstanceStruct *chartInstance,
  const mxArray *c1_u, const emlrtMsgIdentifier *c1_parentId, real32_T
  c1_y_data[], int32_T c1_y_sizes[2])
{
  int32_T c1_i49;
  uint32_T c1_uv0[2];
  int32_T c1_i50;
  boolean_T c1_bv0[2];
  int32_T c1_tmp_sizes[2];
  real32_T c1_tmp_data[32];
  int32_T c1_y;
  int32_T c1_b_y;
  int32_T c1_loop_ub;
  int32_T c1_i51;
  (void)chartInstance;
  for (c1_i49 = 0; c1_i49 < 2; c1_i49++) {
    c1_uv0[c1_i49] = 16U + (uint32_T)(-14 * c1_i49);
  }

  for (c1_i50 = 0; c1_i50 < 2; c1_i50++) {
    c1_bv0[c1_i50] = true;
  }

  sf_mex_import_vs(c1_parentId, sf_mex_dup(c1_u), c1_tmp_data, 1, 1, 0U, 1, 0U,
                   2, c1_bv0, c1_uv0, c1_tmp_sizes);
  c1_y_sizes[0] = c1_tmp_sizes[0];
  c1_y_sizes[1] = c1_tmp_sizes[1];
  c1_y = c1_y_sizes[0];
  c1_b_y = c1_y_sizes[1];
  c1_loop_ub = c1_tmp_sizes[0] * c1_tmp_sizes[1] - 1;
  for (c1_i51 = 0; c1_i51 <= c1_loop_ub; c1_i51++) {
    c1_y_data[c1_i51] = c1_tmp_data[c1_i51];
  }

  sf_mex_destroy(&c1_u);
}

static void c1_d_emlrt_marshallIn(SFc1_cameraTestInstanceStruct *chartInstance,
  const mxArray *c1_u, const emlrtMsgIdentifier *c1_parentId, int32_T c1_y_data[],
  int32_T *c1_y_sizes)
{
  static uint32_T c1_uv1[1] = { 16U };

  uint32_T c1_uv2[1];
  static boolean_T c1_bv1[1] = { true };

  boolean_T c1_bv2[1];
  int32_T c1_tmp_sizes;
  int32_T c1_tmp_data[16];
  int32_T c1_loop_ub;
  int32_T c1_i52;
  (void)chartInstance;
  c1_uv2[0] = c1_uv1[0];
  c1_bv2[0] = c1_bv1[0];
  sf_mex_import_vs(c1_parentId, sf_mex_dup(c1_u), c1_tmp_data, 1, 6, 0U, 1, 0U,
                   1, c1_bv2, c1_uv2, &c1_tmp_sizes);
  *c1_y_sizes = c1_tmp_sizes;
  c1_loop_ub = c1_tmp_sizes - 1;
  for (c1_i52 = 0; c1_i52 <= c1_loop_ub; c1_i52++) {
    c1_y_data[c1_i52] = c1_tmp_data[c1_i52];
  }

  sf_mex_destroy(&c1_u);
}

static void c1_e_emlrt_marshallIn(SFc1_cameraTestInstanceStruct *chartInstance,
  const mxArray *c1_u, const emlrtMsgIdentifier *c1_parentId, uint8_T c1_y_data[],
  int32_T *c1_y_sizes)
{
  static uint32_T c1_uv3[1] = { 16U };

  uint32_T c1_uv4[1];
  static boolean_T c1_bv3[1] = { true };

  boolean_T c1_bv4[1];
  int32_T c1_tmp_sizes;
  uint8_T c1_tmp_data[16];
  int32_T c1_loop_ub;
  int32_T c1_i53;
  (void)chartInstance;
  c1_uv4[0] = c1_uv3[0];
  c1_bv4[0] = c1_bv3[0];
  sf_mex_import_vs(c1_parentId, sf_mex_dup(c1_u), c1_tmp_data, 1, 3, 0U, 1, 0U,
                   1, c1_bv4, c1_uv4, &c1_tmp_sizes);
  *c1_y_sizes = c1_tmp_sizes;
  c1_loop_ub = c1_tmp_sizes - 1;
  for (c1_i53 = 0; c1_i53 <= c1_loop_ub; c1_i53++) {
    c1_y_data[c1_i53] = c1_tmp_data[c1_i53];
  }

  sf_mex_destroy(&c1_u);
}

static void c1_sf_marshallIn(void *chartInstanceVoid, const mxArray
  *c1_mxArrayInData, const char_T *c1_varName, c1_BlobData *c1_outData_data,
  c1_BlobData_size *c1_outData_elems_sizes)
{
  const mxArray *c1_blobs;
  const char_T *c1_identifier;
  emlrtMsgIdentifier c1_thisId;
  c1_BlobData_size c1_y_elems_sizes;
  c1_BlobData c1_y_data;
  SFc1_cameraTestInstanceStruct *chartInstance;
  chartInstance = (SFc1_cameraTestInstanceStruct *)chartInstanceVoid;
  c1_blobs = sf_mex_dup(c1_mxArrayInData);
  c1_identifier = c1_varName;
  c1_thisId.fIdentifier = c1_identifier;
  c1_thisId.fParent = NULL;
  c1_b_emlrt_marshallIn(chartInstance, sf_mex_dup(c1_blobs), &c1_thisId,
                        &c1_y_data, &c1_y_elems_sizes);
  sf_mex_destroy(&c1_blobs);
  *c1_outData_elems_sizes = c1_y_elems_sizes;
  *c1_outData_data = c1_y_data;
  sf_mex_destroy(&c1_mxArrayInData);
}

static const mxArray *c1_b_sf_marshallOut(void *chartInstanceVoid, void
  *c1_inData)
{
  const mxArray *c1_mxArrayOutData = NULL;
  int32_T c1_i54;
  int32_T c1_i55;
  int32_T c1_i56;
  boolean_T c1_b_inData[76800];
  int32_T c1_i57;
  int32_T c1_i58;
  int32_T c1_i59;
  boolean_T c1_u[76800];
  const mxArray *c1_y = NULL;
  SFc1_cameraTestInstanceStruct *chartInstance;
  chartInstance = (SFc1_cameraTestInstanceStruct *)chartInstanceVoid;
  c1_mxArrayOutData = NULL;
  c1_i54 = 0;
  for (c1_i55 = 0; c1_i55 < 240; c1_i55++) {
    for (c1_i56 = 0; c1_i56 < 320; c1_i56++) {
      c1_b_inData[c1_i56 + c1_i54] = (*(boolean_T (*)[76800])c1_inData)[c1_i56 +
        c1_i54];
    }

    c1_i54 += 320;
  }

  c1_i57 = 0;
  for (c1_i58 = 0; c1_i58 < 240; c1_i58++) {
    for (c1_i59 = 0; c1_i59 < 320; c1_i59++) {
      c1_u[c1_i59 + c1_i57] = c1_b_inData[c1_i59 + c1_i57];
    }

    c1_i57 += 320;
  }

  c1_y = NULL;
  sf_mex_assign(&c1_y, sf_mex_create("y", c1_u, 11, 0U, 1U, 0U, 2, 320, 240),
                false);
  sf_mex_assign(&c1_mxArrayOutData, c1_y, false);
  return c1_mxArrayOutData;
}

static const mxArray *c1_c_sf_marshallOut(void *chartInstanceVoid, int32_T
  c1_inData_data[], int32_T c1_inData_sizes[2])
{
  const mxArray *c1_mxArrayOutData = NULL;
  int32_T c1_u_sizes[2];
  int32_T c1_u;
  int32_T c1_b_u;
  int32_T c1_inData;
  int32_T c1_b_inData;
  int32_T c1_b_inData_sizes;
  int32_T c1_loop_ub;
  int32_T c1_i60;
  int32_T c1_b_inData_data[16];
  int32_T c1_b_loop_ub;
  int32_T c1_i61;
  int32_T c1_u_data[16];
  const mxArray *c1_y = NULL;
  SFc1_cameraTestInstanceStruct *chartInstance;
  chartInstance = (SFc1_cameraTestInstanceStruct *)chartInstanceVoid;
  c1_mxArrayOutData = NULL;
  c1_u_sizes[0] = c1_inData_sizes[0];
  c1_u_sizes[1] = 1;
  c1_u = c1_u_sizes[0];
  c1_b_u = c1_u_sizes[1];
  c1_inData = c1_inData_sizes[0];
  c1_b_inData = c1_inData_sizes[1];
  c1_b_inData_sizes = c1_inData * c1_b_inData;
  c1_loop_ub = c1_inData * c1_b_inData - 1;
  for (c1_i60 = 0; c1_i60 <= c1_loop_ub; c1_i60++) {
    c1_b_inData_data[c1_i60] = c1_inData_data[c1_i60];
  }

  c1_b_loop_ub = c1_b_inData_sizes - 1;
  for (c1_i61 = 0; c1_i61 <= c1_b_loop_ub; c1_i61++) {
    c1_u_data[c1_i61] = c1_b_inData_data[c1_i61];
  }

  c1_y = NULL;
  sf_mex_assign(&c1_y, sf_mex_create("y", c1_u_data, 6, 0U, 1U, 0U, 2,
    c1_u_sizes[0], c1_u_sizes[1]), false);
  sf_mex_assign(&c1_mxArrayOutData, c1_y, false);
  return c1_mxArrayOutData;
}

static const mxArray *c1_d_sf_marshallOut(void *chartInstanceVoid, real32_T
  c1_inData_data[], int32_T c1_inData_sizes[2])
{
  const mxArray *c1_mxArrayOutData = NULL;
  int32_T c1_u_sizes[2];
  int32_T c1_u;
  int32_T c1_b_u;
  int32_T c1_inData;
  int32_T c1_b_inData;
  int32_T c1_b_inData_sizes;
  int32_T c1_loop_ub;
  int32_T c1_i62;
  real32_T c1_b_inData_data[32];
  int32_T c1_b_loop_ub;
  int32_T c1_i63;
  real32_T c1_u_data[32];
  const mxArray *c1_y = NULL;
  SFc1_cameraTestInstanceStruct *chartInstance;
  chartInstance = (SFc1_cameraTestInstanceStruct *)chartInstanceVoid;
  c1_mxArrayOutData = NULL;
  c1_u_sizes[0] = c1_inData_sizes[0];
  c1_u_sizes[1] = c1_inData_sizes[1];
  c1_u = c1_u_sizes[0];
  c1_b_u = c1_u_sizes[1];
  c1_inData = c1_inData_sizes[0];
  c1_b_inData = c1_inData_sizes[1];
  c1_b_inData_sizes = c1_inData * c1_b_inData;
  c1_loop_ub = c1_inData * c1_b_inData - 1;
  for (c1_i62 = 0; c1_i62 <= c1_loop_ub; c1_i62++) {
    c1_b_inData_data[c1_i62] = c1_inData_data[c1_i62];
  }

  c1_b_loop_ub = c1_b_inData_sizes - 1;
  for (c1_i63 = 0; c1_i63 <= c1_b_loop_ub; c1_i63++) {
    c1_u_data[c1_i63] = c1_b_inData_data[c1_i63];
  }

  c1_y = NULL;
  sf_mex_assign(&c1_y, sf_mex_create("y", c1_u_data, 1, 0U, 1U, 0U, 2,
    c1_u_sizes[0], c1_u_sizes[1]), false);
  sf_mex_assign(&c1_mxArrayOutData, c1_y, false);
  return c1_mxArrayOutData;
}

static const mxArray *c1_e_sf_marshallOut(void *chartInstanceVoid, void
  *c1_inData)
{
  const mxArray *c1_mxArrayOutData = NULL;
  real_T c1_u;
  const mxArray *c1_y = NULL;
  SFc1_cameraTestInstanceStruct *chartInstance;
  chartInstance = (SFc1_cameraTestInstanceStruct *)chartInstanceVoid;
  c1_mxArrayOutData = NULL;
  c1_u = *(real_T *)c1_inData;
  c1_y = NULL;
  sf_mex_assign(&c1_y, sf_mex_create("y", &c1_u, 0, 0U, 0U, 0U, 0), false);
  sf_mex_assign(&c1_mxArrayOutData, c1_y, false);
  return c1_mxArrayOutData;
}

static real_T c1_f_emlrt_marshallIn(SFc1_cameraTestInstanceStruct *chartInstance,
  const mxArray *c1_u, const emlrtMsgIdentifier *c1_parentId)
{
  real_T c1_y;
  real_T c1_d2;
  (void)chartInstance;
  sf_mex_import(c1_parentId, sf_mex_dup(c1_u), &c1_d2, 1, 0, 0U, 0, 0U, 0);
  c1_y = c1_d2;
  sf_mex_destroy(&c1_u);
  return c1_y;
}

static void c1_b_sf_marshallIn(void *chartInstanceVoid, const mxArray
  *c1_mxArrayInData, const char_T *c1_varName, void *c1_outData)
{
  const mxArray *c1_nargout;
  const char_T *c1_identifier;
  emlrtMsgIdentifier c1_thisId;
  real_T c1_y;
  SFc1_cameraTestInstanceStruct *chartInstance;
  chartInstance = (SFc1_cameraTestInstanceStruct *)chartInstanceVoid;
  c1_nargout = sf_mex_dup(c1_mxArrayInData);
  c1_identifier = c1_varName;
  c1_thisId.fIdentifier = c1_identifier;
  c1_thisId.fParent = NULL;
  c1_y = c1_f_emlrt_marshallIn(chartInstance, sf_mex_dup(c1_nargout), &c1_thisId);
  sf_mex_destroy(&c1_nargout);
  *(real_T *)c1_outData = c1_y;
  sf_mex_destroy(&c1_mxArrayInData);
}

static const mxArray *c1_f_sf_marshallOut(void *chartInstanceVoid, real_T
  c1_inData_data[], int32_T c1_inData_sizes[2])
{
  const mxArray *c1_mxArrayOutData = NULL;
  int32_T c1_u_sizes[2];
  int32_T c1_u;
  int32_T c1_b_u;
  int32_T c1_inData;
  int32_T c1_b_inData;
  int32_T c1_b_inData_sizes;
  int32_T c1_loop_ub;
  int32_T c1_i64;
  real_T c1_b_inData_data[3];
  int32_T c1_b_loop_ub;
  int32_T c1_i65;
  real_T c1_u_data[3];
  const mxArray *c1_y = NULL;
  SFc1_cameraTestInstanceStruct *chartInstance;
  chartInstance = (SFc1_cameraTestInstanceStruct *)chartInstanceVoid;
  c1_mxArrayOutData = NULL;
  c1_u_sizes[0] = 1;
  c1_u_sizes[1] = c1_inData_sizes[1];
  c1_u = c1_u_sizes[0];
  c1_b_u = c1_u_sizes[1];
  c1_inData = c1_inData_sizes[0];
  c1_b_inData = c1_inData_sizes[1];
  c1_b_inData_sizes = c1_inData * c1_b_inData;
  c1_loop_ub = c1_inData * c1_b_inData - 1;
  for (c1_i64 = 0; c1_i64 <= c1_loop_ub; c1_i64++) {
    c1_b_inData_data[c1_i64] = c1_inData_data[c1_i64];
  }

  c1_b_loop_ub = c1_b_inData_sizes - 1;
  for (c1_i65 = 0; c1_i65 <= c1_b_loop_ub; c1_i65++) {
    c1_u_data[c1_i65] = c1_b_inData_data[c1_i65];
  }

  c1_y = NULL;
  sf_mex_assign(&c1_y, sf_mex_create("y", c1_u_data, 0, 0U, 1U, 0U, 2,
    c1_u_sizes[0], c1_u_sizes[1]), false);
  sf_mex_assign(&c1_mxArrayOutData, c1_y, false);
  return c1_mxArrayOutData;
}

static void c1_g_emlrt_marshallIn(SFc1_cameraTestInstanceStruct *chartInstance,
  const mxArray *c1_u, const emlrtMsgIdentifier *c1_parentId, real_T c1_y_data[],
  int32_T c1_y_sizes[2])
{
  int32_T c1_i66;
  uint32_T c1_uv5[2];
  int32_T c1_i67;
  static boolean_T c1_bv5[2] = { false, true };

  boolean_T c1_bv6[2];
  int32_T c1_tmp_sizes[2];
  real_T c1_tmp_data[3];
  int32_T c1_y;
  int32_T c1_b_y;
  int32_T c1_loop_ub;
  int32_T c1_i68;
  (void)chartInstance;
  for (c1_i66 = 0; c1_i66 < 2; c1_i66++) {
    c1_uv5[c1_i66] = 1U + ((uint32_T)c1_i66 << 1);
  }

  for (c1_i67 = 0; c1_i67 < 2; c1_i67++) {
    c1_bv6[c1_i67] = c1_bv5[c1_i67];
  }

  sf_mex_import_vs(c1_parentId, sf_mex_dup(c1_u), c1_tmp_data, 1, 0, 0U, 1, 0U,
                   2, c1_bv6, c1_uv5, c1_tmp_sizes);
  c1_y_sizes[0] = 1;
  c1_y_sizes[1] = c1_tmp_sizes[1];
  c1_y = c1_y_sizes[0];
  c1_b_y = c1_y_sizes[1];
  c1_loop_ub = c1_tmp_sizes[0] * c1_tmp_sizes[1] - 1;
  for (c1_i68 = 0; c1_i68 <= c1_loop_ub; c1_i68++) {
    c1_y_data[c1_i68] = c1_tmp_data[c1_i68];
  }

  sf_mex_destroy(&c1_u);
}

static void c1_c_sf_marshallIn(void *chartInstanceVoid, const mxArray
  *c1_mxArrayInData, const char_T *c1_varName, real_T c1_outData_data[], int32_T
  c1_outData_sizes[2])
{
  const mxArray *c1_color;
  const char_T *c1_identifier;
  emlrtMsgIdentifier c1_thisId;
  int32_T c1_y_sizes[2];
  real_T c1_y_data[3];
  int32_T c1_loop_ub;
  int32_T c1_i69;
  SFc1_cameraTestInstanceStruct *chartInstance;
  chartInstance = (SFc1_cameraTestInstanceStruct *)chartInstanceVoid;
  c1_color = sf_mex_dup(c1_mxArrayInData);
  c1_identifier = c1_varName;
  c1_thisId.fIdentifier = c1_identifier;
  c1_thisId.fParent = NULL;
  c1_g_emlrt_marshallIn(chartInstance, sf_mex_dup(c1_color), &c1_thisId,
                        c1_y_data, c1_y_sizes);
  sf_mex_destroy(&c1_color);
  c1_outData_sizes[0] = 1;
  c1_outData_sizes[1] = c1_y_sizes[1];
  c1_loop_ub = c1_y_sizes[1] - 1;
  for (c1_i69 = 0; c1_i69 <= c1_loop_ub; c1_i69++) {
    c1_outData_data[c1_outData_sizes[0] * c1_i69] = c1_y_data[c1_y_sizes[0] *
      c1_i69];
  }

  sf_mex_destroy(&c1_mxArrayInData);
}

static const mxArray *c1_g_sf_marshallOut(void *chartInstanceVoid, void
  *c1_inData)
{
  const mxArray *c1_mxArrayOutData = NULL;
  int32_T c1_i70;
  boolean_T c1_b_inData[3];
  int32_T c1_i71;
  boolean_T c1_u[3];
  const mxArray *c1_y = NULL;
  SFc1_cameraTestInstanceStruct *chartInstance;
  chartInstance = (SFc1_cameraTestInstanceStruct *)chartInstanceVoid;
  c1_mxArrayOutData = NULL;
  for (c1_i70 = 0; c1_i70 < 3; c1_i70++) {
    c1_b_inData[c1_i70] = (*(boolean_T (*)[3])c1_inData)[c1_i70];
  }

  for (c1_i71 = 0; c1_i71 < 3; c1_i71++) {
    c1_u[c1_i71] = c1_b_inData[c1_i71];
  }

  c1_y = NULL;
  sf_mex_assign(&c1_y, sf_mex_create("y", c1_u, 11, 0U, 1U, 0U, 2, 1, 3), false);
  sf_mex_assign(&c1_mxArrayOutData, c1_y, false);
  return c1_mxArrayOutData;
}

static void c1_h_emlrt_marshallIn(SFc1_cameraTestInstanceStruct *chartInstance,
  const mxArray *c1_u, const emlrtMsgIdentifier *c1_parentId, boolean_T c1_y[3])
{
  boolean_T c1_bv7[3];
  int32_T c1_i72;
  (void)chartInstance;
  sf_mex_import(c1_parentId, sf_mex_dup(c1_u), c1_bv7, 1, 11, 0U, 1, 0U, 2, 1, 3);
  for (c1_i72 = 0; c1_i72 < 3; c1_i72++) {
    c1_y[c1_i72] = c1_bv7[c1_i72];
  }

  sf_mex_destroy(&c1_u);
}

static void c1_d_sf_marshallIn(void *chartInstanceVoid, const mxArray
  *c1_mxArrayInData, const char_T *c1_varName, void *c1_outData)
{
  const mxArray *c1_colorVec;
  const char_T *c1_identifier;
  emlrtMsgIdentifier c1_thisId;
  boolean_T c1_y[3];
  int32_T c1_i73;
  SFc1_cameraTestInstanceStruct *chartInstance;
  chartInstance = (SFc1_cameraTestInstanceStruct *)chartInstanceVoid;
  c1_colorVec = sf_mex_dup(c1_mxArrayInData);
  c1_identifier = c1_varName;
  c1_thisId.fIdentifier = c1_identifier;
  c1_thisId.fParent = NULL;
  c1_h_emlrt_marshallIn(chartInstance, sf_mex_dup(c1_colorVec), &c1_thisId, c1_y);
  sf_mex_destroy(&c1_colorVec);
  for (c1_i73 = 0; c1_i73 < 3; c1_i73++) {
    (*(boolean_T (*)[3])c1_outData)[c1_i73] = c1_y[c1_i73];
  }

  sf_mex_destroy(&c1_mxArrayInData);
}

static const mxArray *c1_h_sf_marshallOut(void *chartInstanceVoid, void
  *c1_inData)
{
  const mxArray *c1_mxArrayOutData = NULL;
  boolean_T c1_u;
  const mxArray *c1_y = NULL;
  SFc1_cameraTestInstanceStruct *chartInstance;
  chartInstance = (SFc1_cameraTestInstanceStruct *)chartInstanceVoid;
  c1_mxArrayOutData = NULL;
  c1_u = *(boolean_T *)c1_inData;
  c1_y = NULL;
  sf_mex_assign(&c1_y, sf_mex_create("y", &c1_u, 11, 0U, 0U, 0U, 0), false);
  sf_mex_assign(&c1_mxArrayOutData, c1_y, false);
  return c1_mxArrayOutData;
}

static boolean_T c1_i_emlrt_marshallIn(SFc1_cameraTestInstanceStruct
  *chartInstance, const mxArray *c1_u, const emlrtMsgIdentifier *c1_parentId)
{
  boolean_T c1_y;
  boolean_T c1_b3;
  (void)chartInstance;
  sf_mex_import(c1_parentId, sf_mex_dup(c1_u), &c1_b3, 1, 11, 0U, 0, 0U, 0);
  c1_y = c1_b3;
  sf_mex_destroy(&c1_u);
  return c1_y;
}

static void c1_e_sf_marshallIn(void *chartInstanceVoid, const mxArray
  *c1_mxArrayInData, const char_T *c1_varName, void *c1_outData)
{
  const mxArray *c1_cb;
  const char_T *c1_identifier;
  emlrtMsgIdentifier c1_thisId;
  boolean_T c1_y;
  SFc1_cameraTestInstanceStruct *chartInstance;
  chartInstance = (SFc1_cameraTestInstanceStruct *)chartInstanceVoid;
  c1_cb = sf_mex_dup(c1_mxArrayInData);
  c1_identifier = c1_varName;
  c1_thisId.fIdentifier = c1_identifier;
  c1_thisId.fParent = NULL;
  c1_y = c1_i_emlrt_marshallIn(chartInstance, sf_mex_dup(c1_cb), &c1_thisId);
  sf_mex_destroy(&c1_cb);
  *(boolean_T *)c1_outData = c1_y;
  sf_mex_destroy(&c1_mxArrayInData);
}

static void c1_f_sf_marshallIn(void *chartInstanceVoid, const mxArray
  *c1_mxArrayInData, const char_T *c1_varName, real32_T c1_outData_data[],
  int32_T c1_outData_sizes[2])
{
  const mxArray *c1_centroidsInt;
  const char_T *c1_identifier;
  emlrtMsgIdentifier c1_thisId;
  int32_T c1_y_sizes[2];
  real32_T c1_y_data[32];
  int32_T c1_loop_ub;
  int32_T c1_i74;
  int32_T c1_b_loop_ub;
  int32_T c1_i75;
  SFc1_cameraTestInstanceStruct *chartInstance;
  chartInstance = (SFc1_cameraTestInstanceStruct *)chartInstanceVoid;
  c1_centroidsInt = sf_mex_dup(c1_mxArrayInData);
  c1_identifier = c1_varName;
  c1_thisId.fIdentifier = c1_identifier;
  c1_thisId.fParent = NULL;
  c1_c_emlrt_marshallIn(chartInstance, sf_mex_dup(c1_centroidsInt), &c1_thisId,
                        c1_y_data, c1_y_sizes);
  sf_mex_destroy(&c1_centroidsInt);
  c1_outData_sizes[0] = c1_y_sizes[0];
  c1_outData_sizes[1] = c1_y_sizes[1];
  c1_loop_ub = c1_y_sizes[1] - 1;
  for (c1_i74 = 0; c1_i74 <= c1_loop_ub; c1_i74++) {
    c1_b_loop_ub = c1_y_sizes[0] - 1;
    for (c1_i75 = 0; c1_i75 <= c1_b_loop_ub; c1_i75++) {
      c1_outData_data[c1_i75 + c1_outData_sizes[0] * c1_i74] = c1_y_data[c1_i75
        + c1_y_sizes[0] * c1_i74];
    }
  }

  sf_mex_destroy(&c1_mxArrayInData);
}

const mxArray *sf_c1_cameraTest_get_eml_resolved_functions_info(void)
{
  const mxArray *c1_nameCaptureInfo = NULL;
  c1_nameCaptureInfo = NULL;
  sf_mex_assign(&c1_nameCaptureInfo, sf_mex_createstruct("structure", 2, 20, 1),
                false);
  c1_info_helper(&c1_nameCaptureInfo);
  sf_mex_emlrtNameCapturePostProcessR2012a(&c1_nameCaptureInfo);
  return c1_nameCaptureInfo;
}

static void c1_info_helper(const mxArray **c1_info)
{
  const mxArray *c1_rhs0 = NULL;
  const mxArray *c1_lhs0 = NULL;
  const mxArray *c1_rhs1 = NULL;
  const mxArray *c1_lhs1 = NULL;
  const mxArray *c1_rhs2 = NULL;
  const mxArray *c1_lhs2 = NULL;
  const mxArray *c1_rhs3 = NULL;
  const mxArray *c1_lhs3 = NULL;
  const mxArray *c1_rhs4 = NULL;
  const mxArray *c1_lhs4 = NULL;
  const mxArray *c1_rhs5 = NULL;
  const mxArray *c1_lhs5 = NULL;
  const mxArray *c1_rhs6 = NULL;
  const mxArray *c1_lhs6 = NULL;
  const mxArray *c1_rhs7 = NULL;
  const mxArray *c1_lhs7 = NULL;
  const mxArray *c1_rhs8 = NULL;
  const mxArray *c1_lhs8 = NULL;
  const mxArray *c1_rhs9 = NULL;
  const mxArray *c1_lhs9 = NULL;
  const mxArray *c1_rhs10 = NULL;
  const mxArray *c1_lhs10 = NULL;
  const mxArray *c1_rhs11 = NULL;
  const mxArray *c1_lhs11 = NULL;
  const mxArray *c1_rhs12 = NULL;
  const mxArray *c1_lhs12 = NULL;
  const mxArray *c1_rhs13 = NULL;
  const mxArray *c1_lhs13 = NULL;
  const mxArray *c1_rhs14 = NULL;
  const mxArray *c1_lhs14 = NULL;
  const mxArray *c1_rhs15 = NULL;
  const mxArray *c1_lhs15 = NULL;
  const mxArray *c1_rhs16 = NULL;
  const mxArray *c1_lhs16 = NULL;
  const mxArray *c1_rhs17 = NULL;
  const mxArray *c1_lhs17 = NULL;
  const mxArray *c1_rhs18 = NULL;
  const mxArray *c1_lhs18 = NULL;
  const mxArray *c1_rhs19 = NULL;
  const mxArray *c1_lhs19 = NULL;
  sf_mex_addfield(*c1_info, c1_emlrt_marshallOut(""), "context", "context", 0);
  sf_mex_addfield(*c1_info, c1_emlrt_marshallOut("length"), "name", "name", 0);
  sf_mex_addfield(*c1_info, c1_emlrt_marshallOut("int32"), "dominantType",
                  "dominantType", 0);
  sf_mex_addfield(*c1_info, c1_emlrt_marshallOut(
    "[ILXE]$matlabroot$/toolbox/eml/lib/matlab/elmat/length.m"), "resolved",
                  "resolved", 0);
  sf_mex_addfield(*c1_info, c1_b_emlrt_marshallOut(1303149806U), "fileTimeLo",
                  "fileTimeLo", 0);
  sf_mex_addfield(*c1_info, c1_b_emlrt_marshallOut(0U), "fileTimeHi",
                  "fileTimeHi", 0);
  sf_mex_addfield(*c1_info, c1_b_emlrt_marshallOut(0U), "mFileTimeLo",
                  "mFileTimeLo", 0);
  sf_mex_addfield(*c1_info, c1_b_emlrt_marshallOut(0U), "mFileTimeHi",
                  "mFileTimeHi", 0);
  sf_mex_assign(&c1_rhs0, sf_mex_createcellmatrix(0, 1), false);
  sf_mex_assign(&c1_lhs0, sf_mex_createcellmatrix(0, 1), false);
  sf_mex_addfield(*c1_info, sf_mex_duplicatearraysafe(&c1_rhs0), "rhs", "rhs", 0);
  sf_mex_addfield(*c1_info, sf_mex_duplicatearraysafe(&c1_lhs0), "lhs", "lhs", 0);
  sf_mex_addfield(*c1_info, c1_emlrt_marshallOut(""), "context", "context", 1);
  sf_mex_addfield(*c1_info, c1_emlrt_marshallOut("floor"), "name", "name", 1);
  sf_mex_addfield(*c1_info, c1_emlrt_marshallOut("single"), "dominantType",
                  "dominantType", 1);
  sf_mex_addfield(*c1_info, c1_emlrt_marshallOut(
    "[ILXE]$matlabroot$/toolbox/eml/lib/matlab/elfun/floor.m"), "resolved",
                  "resolved", 1);
  sf_mex_addfield(*c1_info, c1_b_emlrt_marshallOut(1363717454U), "fileTimeLo",
                  "fileTimeLo", 1);
  sf_mex_addfield(*c1_info, c1_b_emlrt_marshallOut(0U), "fileTimeHi",
                  "fileTimeHi", 1);
  sf_mex_addfield(*c1_info, c1_b_emlrt_marshallOut(0U), "mFileTimeLo",
                  "mFileTimeLo", 1);
  sf_mex_addfield(*c1_info, c1_b_emlrt_marshallOut(0U), "mFileTimeHi",
                  "mFileTimeHi", 1);
  sf_mex_assign(&c1_rhs1, sf_mex_createcellmatrix(0, 1), false);
  sf_mex_assign(&c1_lhs1, sf_mex_createcellmatrix(0, 1), false);
  sf_mex_addfield(*c1_info, sf_mex_duplicatearraysafe(&c1_rhs1), "rhs", "rhs", 1);
  sf_mex_addfield(*c1_info, sf_mex_duplicatearraysafe(&c1_lhs1), "lhs", "lhs", 1);
  sf_mex_addfield(*c1_info, c1_emlrt_marshallOut(
    "[ILXE]$matlabroot$/toolbox/eml/lib/matlab/elfun/floor.m"), "context",
                  "context", 2);
  sf_mex_addfield(*c1_info, c1_emlrt_marshallOut(
    "coder.internal.isBuiltInNumeric"), "name", "name", 2);
  sf_mex_addfield(*c1_info, c1_emlrt_marshallOut("single"), "dominantType",
                  "dominantType", 2);
  sf_mex_addfield(*c1_info, c1_emlrt_marshallOut(
    "[ILXE]$matlabroot$/toolbox/shared/coder/coder/+coder/+internal/isBuiltInNumeric.m"),
                  "resolved", "resolved", 2);
  sf_mex_addfield(*c1_info, c1_b_emlrt_marshallOut(1395935456U), "fileTimeLo",
                  "fileTimeLo", 2);
  sf_mex_addfield(*c1_info, c1_b_emlrt_marshallOut(0U), "fileTimeHi",
                  "fileTimeHi", 2);
  sf_mex_addfield(*c1_info, c1_b_emlrt_marshallOut(0U), "mFileTimeLo",
                  "mFileTimeLo", 2);
  sf_mex_addfield(*c1_info, c1_b_emlrt_marshallOut(0U), "mFileTimeHi",
                  "mFileTimeHi", 2);
  sf_mex_assign(&c1_rhs2, sf_mex_createcellmatrix(0, 1), false);
  sf_mex_assign(&c1_lhs2, sf_mex_createcellmatrix(0, 1), false);
  sf_mex_addfield(*c1_info, sf_mex_duplicatearraysafe(&c1_rhs2), "rhs", "rhs", 2);
  sf_mex_addfield(*c1_info, sf_mex_duplicatearraysafe(&c1_lhs2), "lhs", "lhs", 2);
  sf_mex_addfield(*c1_info, c1_emlrt_marshallOut(
    "[ILXE]$matlabroot$/toolbox/eml/lib/matlab/elfun/floor.m"), "context",
                  "context", 3);
  sf_mex_addfield(*c1_info, c1_emlrt_marshallOut("eml_scalar_floor"), "name",
                  "name", 3);
  sf_mex_addfield(*c1_info, c1_emlrt_marshallOut("single"), "dominantType",
                  "dominantType", 3);
  sf_mex_addfield(*c1_info, c1_emlrt_marshallOut(
    "[ILXE]$matlabroot$/toolbox/eml/lib/matlab/elfun/eml_scalar_floor.m"),
                  "resolved", "resolved", 3);
  sf_mex_addfield(*c1_info, c1_b_emlrt_marshallOut(1286822326U), "fileTimeLo",
                  "fileTimeLo", 3);
  sf_mex_addfield(*c1_info, c1_b_emlrt_marshallOut(0U), "fileTimeHi",
                  "fileTimeHi", 3);
  sf_mex_addfield(*c1_info, c1_b_emlrt_marshallOut(0U), "mFileTimeLo",
                  "mFileTimeLo", 3);
  sf_mex_addfield(*c1_info, c1_b_emlrt_marshallOut(0U), "mFileTimeHi",
                  "mFileTimeHi", 3);
  sf_mex_assign(&c1_rhs3, sf_mex_createcellmatrix(0, 1), false);
  sf_mex_assign(&c1_lhs3, sf_mex_createcellmatrix(0, 1), false);
  sf_mex_addfield(*c1_info, sf_mex_duplicatearraysafe(&c1_rhs3), "rhs", "rhs", 3);
  sf_mex_addfield(*c1_info, sf_mex_duplicatearraysafe(&c1_lhs3), "lhs", "lhs", 3);
  sf_mex_addfield(*c1_info, c1_emlrt_marshallOut(""), "context", "context", 4);
  sf_mex_addfield(*c1_info, c1_emlrt_marshallOut("find"), "name", "name", 4);
  sf_mex_addfield(*c1_info, c1_emlrt_marshallOut("logical"), "dominantType",
                  "dominantType", 4);
  sf_mex_addfield(*c1_info, c1_emlrt_marshallOut(
    "[ILXE]$matlabroot$/toolbox/eml/lib/matlab/elmat/find.m"), "resolved",
                  "resolved", 4);
  sf_mex_addfield(*c1_info, c1_b_emlrt_marshallOut(1303149806U), "fileTimeLo",
                  "fileTimeLo", 4);
  sf_mex_addfield(*c1_info, c1_b_emlrt_marshallOut(0U), "fileTimeHi",
                  "fileTimeHi", 4);
  sf_mex_addfield(*c1_info, c1_b_emlrt_marshallOut(0U), "mFileTimeLo",
                  "mFileTimeLo", 4);
  sf_mex_addfield(*c1_info, c1_b_emlrt_marshallOut(0U), "mFileTimeHi",
                  "mFileTimeHi", 4);
  sf_mex_assign(&c1_rhs4, sf_mex_createcellmatrix(0, 1), false);
  sf_mex_assign(&c1_lhs4, sf_mex_createcellmatrix(0, 1), false);
  sf_mex_addfield(*c1_info, sf_mex_duplicatearraysafe(&c1_rhs4), "rhs", "rhs", 4);
  sf_mex_addfield(*c1_info, sf_mex_duplicatearraysafe(&c1_lhs4), "lhs", "lhs", 4);
  sf_mex_addfield(*c1_info, c1_emlrt_marshallOut(
    "[ILXE]$matlabroot$/toolbox/eml/lib/matlab/elmat/find.m!eml_find"),
                  "context", "context", 5);
  sf_mex_addfield(*c1_info, c1_emlrt_marshallOut("eml_index_class"), "name",
                  "name", 5);
  sf_mex_addfield(*c1_info, c1_emlrt_marshallOut(""), "dominantType",
                  "dominantType", 5);
  sf_mex_addfield(*c1_info, c1_emlrt_marshallOut(
    "[ILXE]$matlabroot$/toolbox/eml/lib/matlab/eml/eml_index_class.m"),
                  "resolved", "resolved", 5);
  sf_mex_addfield(*c1_info, c1_b_emlrt_marshallOut(1323174178U), "fileTimeLo",
                  "fileTimeLo", 5);
  sf_mex_addfield(*c1_info, c1_b_emlrt_marshallOut(0U), "fileTimeHi",
                  "fileTimeHi", 5);
  sf_mex_addfield(*c1_info, c1_b_emlrt_marshallOut(0U), "mFileTimeLo",
                  "mFileTimeLo", 5);
  sf_mex_addfield(*c1_info, c1_b_emlrt_marshallOut(0U), "mFileTimeHi",
                  "mFileTimeHi", 5);
  sf_mex_assign(&c1_rhs5, sf_mex_createcellmatrix(0, 1), false);
  sf_mex_assign(&c1_lhs5, sf_mex_createcellmatrix(0, 1), false);
  sf_mex_addfield(*c1_info, sf_mex_duplicatearraysafe(&c1_rhs5), "rhs", "rhs", 5);
  sf_mex_addfield(*c1_info, sf_mex_duplicatearraysafe(&c1_lhs5), "lhs", "lhs", 5);
  sf_mex_addfield(*c1_info, c1_emlrt_marshallOut(
    "[ILXE]$matlabroot$/toolbox/eml/lib/matlab/elmat/find.m!eml_find"),
                  "context", "context", 6);
  sf_mex_addfield(*c1_info, c1_emlrt_marshallOut("eml_scalar_eg"), "name",
                  "name", 6);
  sf_mex_addfield(*c1_info, c1_emlrt_marshallOut("logical"), "dominantType",
                  "dominantType", 6);
  sf_mex_addfield(*c1_info, c1_emlrt_marshallOut(
    "[ILXE]$matlabroot$/toolbox/eml/lib/matlab/eml/eml_scalar_eg.m"), "resolved",
                  "resolved", 6);
  sf_mex_addfield(*c1_info, c1_b_emlrt_marshallOut(1375984288U), "fileTimeLo",
                  "fileTimeLo", 6);
  sf_mex_addfield(*c1_info, c1_b_emlrt_marshallOut(0U), "fileTimeHi",
                  "fileTimeHi", 6);
  sf_mex_addfield(*c1_info, c1_b_emlrt_marshallOut(0U), "mFileTimeLo",
                  "mFileTimeLo", 6);
  sf_mex_addfield(*c1_info, c1_b_emlrt_marshallOut(0U), "mFileTimeHi",
                  "mFileTimeHi", 6);
  sf_mex_assign(&c1_rhs6, sf_mex_createcellmatrix(0, 1), false);
  sf_mex_assign(&c1_lhs6, sf_mex_createcellmatrix(0, 1), false);
  sf_mex_addfield(*c1_info, sf_mex_duplicatearraysafe(&c1_rhs6), "rhs", "rhs", 6);
  sf_mex_addfield(*c1_info, sf_mex_duplicatearraysafe(&c1_lhs6), "lhs", "lhs", 6);
  sf_mex_addfield(*c1_info, c1_emlrt_marshallOut(
    "[ILXE]$matlabroot$/toolbox/eml/lib/matlab/eml/eml_scalar_eg.m"), "context",
                  "context", 7);
  sf_mex_addfield(*c1_info, c1_emlrt_marshallOut("coder.internal.scalarEg"),
                  "name", "name", 7);
  sf_mex_addfield(*c1_info, c1_emlrt_marshallOut("logical"), "dominantType",
                  "dominantType", 7);
  sf_mex_addfield(*c1_info, c1_emlrt_marshallOut(
    "[ILXE]$matlabroot$/toolbox/coder/coder/+coder/+internal/scalarEg.p"),
                  "resolved", "resolved", 7);
  sf_mex_addfield(*c1_info, c1_b_emlrt_marshallOut(1410811370U), "fileTimeLo",
                  "fileTimeLo", 7);
  sf_mex_addfield(*c1_info, c1_b_emlrt_marshallOut(0U), "fileTimeHi",
                  "fileTimeHi", 7);
  sf_mex_addfield(*c1_info, c1_b_emlrt_marshallOut(0U), "mFileTimeLo",
                  "mFileTimeLo", 7);
  sf_mex_addfield(*c1_info, c1_b_emlrt_marshallOut(0U), "mFileTimeHi",
                  "mFileTimeHi", 7);
  sf_mex_assign(&c1_rhs7, sf_mex_createcellmatrix(0, 1), false);
  sf_mex_assign(&c1_lhs7, sf_mex_createcellmatrix(0, 1), false);
  sf_mex_addfield(*c1_info, sf_mex_duplicatearraysafe(&c1_rhs7), "rhs", "rhs", 7);
  sf_mex_addfield(*c1_info, sf_mex_duplicatearraysafe(&c1_lhs7), "lhs", "lhs", 7);
  sf_mex_addfield(*c1_info, c1_emlrt_marshallOut(
    "[ILXE]$matlabroot$/toolbox/eml/lib/matlab/elmat/find.m!eml_find"),
                  "context", "context", 8);
  sf_mex_addfield(*c1_info, c1_emlrt_marshallOut(
    "eml_int_forloop_overflow_check"), "name", "name", 8);
  sf_mex_addfield(*c1_info, c1_emlrt_marshallOut(""), "dominantType",
                  "dominantType", 8);
  sf_mex_addfield(*c1_info, c1_emlrt_marshallOut(
    "[ILXE]$matlabroot$/toolbox/eml/lib/matlab/eml/eml_int_forloop_overflow_check.m"),
                  "resolved", "resolved", 8);
  sf_mex_addfield(*c1_info, c1_b_emlrt_marshallOut(1397261022U), "fileTimeLo",
                  "fileTimeLo", 8);
  sf_mex_addfield(*c1_info, c1_b_emlrt_marshallOut(0U), "fileTimeHi",
                  "fileTimeHi", 8);
  sf_mex_addfield(*c1_info, c1_b_emlrt_marshallOut(0U), "mFileTimeLo",
                  "mFileTimeLo", 8);
  sf_mex_addfield(*c1_info, c1_b_emlrt_marshallOut(0U), "mFileTimeHi",
                  "mFileTimeHi", 8);
  sf_mex_assign(&c1_rhs8, sf_mex_createcellmatrix(0, 1), false);
  sf_mex_assign(&c1_lhs8, sf_mex_createcellmatrix(0, 1), false);
  sf_mex_addfield(*c1_info, sf_mex_duplicatearraysafe(&c1_rhs8), "rhs", "rhs", 8);
  sf_mex_addfield(*c1_info, sf_mex_duplicatearraysafe(&c1_lhs8), "lhs", "lhs", 8);
  sf_mex_addfield(*c1_info, c1_emlrt_marshallOut(
    "[ILXE]$matlabroot$/toolbox/eml/lib/matlab/eml/eml_int_forloop_overflow_check.m!eml_int_forloop_overflow_check_helper"),
                  "context", "context", 9);
  sf_mex_addfield(*c1_info, c1_emlrt_marshallOut("isfi"), "name", "name", 9);
  sf_mex_addfield(*c1_info, c1_emlrt_marshallOut("coder.internal.indexInt"),
                  "dominantType", "dominantType", 9);
  sf_mex_addfield(*c1_info, c1_emlrt_marshallOut(
    "[ILXE]$matlabroot$/toolbox/eml/lib/fixedpoint/isfi.m"), "resolved",
                  "resolved", 9);
  sf_mex_addfield(*c1_info, c1_b_emlrt_marshallOut(1346513958U), "fileTimeLo",
                  "fileTimeLo", 9);
  sf_mex_addfield(*c1_info, c1_b_emlrt_marshallOut(0U), "fileTimeHi",
                  "fileTimeHi", 9);
  sf_mex_addfield(*c1_info, c1_b_emlrt_marshallOut(0U), "mFileTimeLo",
                  "mFileTimeLo", 9);
  sf_mex_addfield(*c1_info, c1_b_emlrt_marshallOut(0U), "mFileTimeHi",
                  "mFileTimeHi", 9);
  sf_mex_assign(&c1_rhs9, sf_mex_createcellmatrix(0, 1), false);
  sf_mex_assign(&c1_lhs9, sf_mex_createcellmatrix(0, 1), false);
  sf_mex_addfield(*c1_info, sf_mex_duplicatearraysafe(&c1_rhs9), "rhs", "rhs", 9);
  sf_mex_addfield(*c1_info, sf_mex_duplicatearraysafe(&c1_lhs9), "lhs", "lhs", 9);
  sf_mex_addfield(*c1_info, c1_emlrt_marshallOut(
    "[ILXE]$matlabroot$/toolbox/eml/lib/fixedpoint/isfi.m"), "context",
                  "context", 10);
  sf_mex_addfield(*c1_info, c1_emlrt_marshallOut("isnumerictype"), "name",
                  "name", 10);
  sf_mex_addfield(*c1_info, c1_emlrt_marshallOut("char"), "dominantType",
                  "dominantType", 10);
  sf_mex_addfield(*c1_info, c1_emlrt_marshallOut(
    "[ILXE]$matlabroot$/toolbox/eml/lib/fixedpoint/isnumerictype.m"), "resolved",
                  "resolved", 10);
  sf_mex_addfield(*c1_info, c1_b_emlrt_marshallOut(1398879198U), "fileTimeLo",
                  "fileTimeLo", 10);
  sf_mex_addfield(*c1_info, c1_b_emlrt_marshallOut(0U), "fileTimeHi",
                  "fileTimeHi", 10);
  sf_mex_addfield(*c1_info, c1_b_emlrt_marshallOut(0U), "mFileTimeLo",
                  "mFileTimeLo", 10);
  sf_mex_addfield(*c1_info, c1_b_emlrt_marshallOut(0U), "mFileTimeHi",
                  "mFileTimeHi", 10);
  sf_mex_assign(&c1_rhs10, sf_mex_createcellmatrix(0, 1), false);
  sf_mex_assign(&c1_lhs10, sf_mex_createcellmatrix(0, 1), false);
  sf_mex_addfield(*c1_info, sf_mex_duplicatearraysafe(&c1_rhs10), "rhs", "rhs",
                  10);
  sf_mex_addfield(*c1_info, sf_mex_duplicatearraysafe(&c1_lhs10), "lhs", "lhs",
                  10);
  sf_mex_addfield(*c1_info, c1_emlrt_marshallOut(
    "[ILXE]$matlabroot$/toolbox/eml/lib/matlab/eml/eml_int_forloop_overflow_check.m!eml_int_forloop_overflow_check_helper"),
                  "context", "context", 11);
  sf_mex_addfield(*c1_info, c1_emlrt_marshallOut("intmax"), "name", "name", 11);
  sf_mex_addfield(*c1_info, c1_emlrt_marshallOut("char"), "dominantType",
                  "dominantType", 11);
  sf_mex_addfield(*c1_info, c1_emlrt_marshallOut(
    "[ILXE]$matlabroot$/toolbox/eml/lib/matlab/elmat/intmax.m"), "resolved",
                  "resolved", 11);
  sf_mex_addfield(*c1_info, c1_b_emlrt_marshallOut(1362265482U), "fileTimeLo",
                  "fileTimeLo", 11);
  sf_mex_addfield(*c1_info, c1_b_emlrt_marshallOut(0U), "fileTimeHi",
                  "fileTimeHi", 11);
  sf_mex_addfield(*c1_info, c1_b_emlrt_marshallOut(0U), "mFileTimeLo",
                  "mFileTimeLo", 11);
  sf_mex_addfield(*c1_info, c1_b_emlrt_marshallOut(0U), "mFileTimeHi",
                  "mFileTimeHi", 11);
  sf_mex_assign(&c1_rhs11, sf_mex_createcellmatrix(0, 1), false);
  sf_mex_assign(&c1_lhs11, sf_mex_createcellmatrix(0, 1), false);
  sf_mex_addfield(*c1_info, sf_mex_duplicatearraysafe(&c1_rhs11), "rhs", "rhs",
                  11);
  sf_mex_addfield(*c1_info, sf_mex_duplicatearraysafe(&c1_lhs11), "lhs", "lhs",
                  11);
  sf_mex_addfield(*c1_info, c1_emlrt_marshallOut(
    "[ILXE]$matlabroot$/toolbox/eml/lib/matlab/elmat/intmax.m"), "context",
                  "context", 12);
  sf_mex_addfield(*c1_info, c1_emlrt_marshallOut("eml_switch_helper"), "name",
                  "name", 12);
  sf_mex_addfield(*c1_info, c1_emlrt_marshallOut(""), "dominantType",
                  "dominantType", 12);
  sf_mex_addfield(*c1_info, c1_emlrt_marshallOut(
    "[ILXE]$matlabroot$/toolbox/eml/lib/matlab/eml/eml_switch_helper.m"),
                  "resolved", "resolved", 12);
  sf_mex_addfield(*c1_info, c1_b_emlrt_marshallOut(1393334458U), "fileTimeLo",
                  "fileTimeLo", 12);
  sf_mex_addfield(*c1_info, c1_b_emlrt_marshallOut(0U), "fileTimeHi",
                  "fileTimeHi", 12);
  sf_mex_addfield(*c1_info, c1_b_emlrt_marshallOut(0U), "mFileTimeLo",
                  "mFileTimeLo", 12);
  sf_mex_addfield(*c1_info, c1_b_emlrt_marshallOut(0U), "mFileTimeHi",
                  "mFileTimeHi", 12);
  sf_mex_assign(&c1_rhs12, sf_mex_createcellmatrix(0, 1), false);
  sf_mex_assign(&c1_lhs12, sf_mex_createcellmatrix(0, 1), false);
  sf_mex_addfield(*c1_info, sf_mex_duplicatearraysafe(&c1_rhs12), "rhs", "rhs",
                  12);
  sf_mex_addfield(*c1_info, sf_mex_duplicatearraysafe(&c1_lhs12), "lhs", "lhs",
                  12);
  sf_mex_addfield(*c1_info, c1_emlrt_marshallOut(
    "[ILXE]$matlabroot$/toolbox/eml/lib/matlab/eml/eml_int_forloop_overflow_check.m!eml_int_forloop_overflow_check_helper"),
                  "context", "context", 13);
  sf_mex_addfield(*c1_info, c1_emlrt_marshallOut("intmin"), "name", "name", 13);
  sf_mex_addfield(*c1_info, c1_emlrt_marshallOut("char"), "dominantType",
                  "dominantType", 13);
  sf_mex_addfield(*c1_info, c1_emlrt_marshallOut(
    "[ILXE]$matlabroot$/toolbox/eml/lib/matlab/elmat/intmin.m"), "resolved",
                  "resolved", 13);
  sf_mex_addfield(*c1_info, c1_b_emlrt_marshallOut(1362265482U), "fileTimeLo",
                  "fileTimeLo", 13);
  sf_mex_addfield(*c1_info, c1_b_emlrt_marshallOut(0U), "fileTimeHi",
                  "fileTimeHi", 13);
  sf_mex_addfield(*c1_info, c1_b_emlrt_marshallOut(0U), "mFileTimeLo",
                  "mFileTimeLo", 13);
  sf_mex_addfield(*c1_info, c1_b_emlrt_marshallOut(0U), "mFileTimeHi",
                  "mFileTimeHi", 13);
  sf_mex_assign(&c1_rhs13, sf_mex_createcellmatrix(0, 1), false);
  sf_mex_assign(&c1_lhs13, sf_mex_createcellmatrix(0, 1), false);
  sf_mex_addfield(*c1_info, sf_mex_duplicatearraysafe(&c1_rhs13), "rhs", "rhs",
                  13);
  sf_mex_addfield(*c1_info, sf_mex_duplicatearraysafe(&c1_lhs13), "lhs", "lhs",
                  13);
  sf_mex_addfield(*c1_info, c1_emlrt_marshallOut(
    "[ILXE]$matlabroot$/toolbox/eml/lib/matlab/elmat/intmin.m"), "context",
                  "context", 14);
  sf_mex_addfield(*c1_info, c1_emlrt_marshallOut("eml_switch_helper"), "name",
                  "name", 14);
  sf_mex_addfield(*c1_info, c1_emlrt_marshallOut(""), "dominantType",
                  "dominantType", 14);
  sf_mex_addfield(*c1_info, c1_emlrt_marshallOut(
    "[ILXE]$matlabroot$/toolbox/eml/lib/matlab/eml/eml_switch_helper.m"),
                  "resolved", "resolved", 14);
  sf_mex_addfield(*c1_info, c1_b_emlrt_marshallOut(1393334458U), "fileTimeLo",
                  "fileTimeLo", 14);
  sf_mex_addfield(*c1_info, c1_b_emlrt_marshallOut(0U), "fileTimeHi",
                  "fileTimeHi", 14);
  sf_mex_addfield(*c1_info, c1_b_emlrt_marshallOut(0U), "mFileTimeLo",
                  "mFileTimeLo", 14);
  sf_mex_addfield(*c1_info, c1_b_emlrt_marshallOut(0U), "mFileTimeHi",
                  "mFileTimeHi", 14);
  sf_mex_assign(&c1_rhs14, sf_mex_createcellmatrix(0, 1), false);
  sf_mex_assign(&c1_lhs14, sf_mex_createcellmatrix(0, 1), false);
  sf_mex_addfield(*c1_info, sf_mex_duplicatearraysafe(&c1_rhs14), "rhs", "rhs",
                  14);
  sf_mex_addfield(*c1_info, sf_mex_duplicatearraysafe(&c1_lhs14), "lhs", "lhs",
                  14);
  sf_mex_addfield(*c1_info, c1_emlrt_marshallOut(
    "[ILXE]$matlabroot$/toolbox/eml/lib/matlab/elmat/find.m!eml_find"),
                  "context", "context", 15);
  sf_mex_addfield(*c1_info, c1_emlrt_marshallOut("eml_index_plus"), "name",
                  "name", 15);
  sf_mex_addfield(*c1_info, c1_emlrt_marshallOut("double"), "dominantType",
                  "dominantType", 15);
  sf_mex_addfield(*c1_info, c1_emlrt_marshallOut(
    "[ILXE]$matlabroot$/toolbox/eml/lib/matlab/eml/eml_index_plus.m"),
                  "resolved", "resolved", 15);
  sf_mex_addfield(*c1_info, c1_b_emlrt_marshallOut(1372586016U), "fileTimeLo",
                  "fileTimeLo", 15);
  sf_mex_addfield(*c1_info, c1_b_emlrt_marshallOut(0U), "fileTimeHi",
                  "fileTimeHi", 15);
  sf_mex_addfield(*c1_info, c1_b_emlrt_marshallOut(0U), "mFileTimeLo",
                  "mFileTimeLo", 15);
  sf_mex_addfield(*c1_info, c1_b_emlrt_marshallOut(0U), "mFileTimeHi",
                  "mFileTimeHi", 15);
  sf_mex_assign(&c1_rhs15, sf_mex_createcellmatrix(0, 1), false);
  sf_mex_assign(&c1_lhs15, sf_mex_createcellmatrix(0, 1), false);
  sf_mex_addfield(*c1_info, sf_mex_duplicatearraysafe(&c1_rhs15), "rhs", "rhs",
                  15);
  sf_mex_addfield(*c1_info, sf_mex_duplicatearraysafe(&c1_lhs15), "lhs", "lhs",
                  15);
  sf_mex_addfield(*c1_info, c1_emlrt_marshallOut(
    "[ILXE]$matlabroot$/toolbox/eml/lib/matlab/eml/eml_index_plus.m"), "context",
                  "context", 16);
  sf_mex_addfield(*c1_info, c1_emlrt_marshallOut("coder.internal.indexPlus"),
                  "name", "name", 16);
  sf_mex_addfield(*c1_info, c1_emlrt_marshallOut("double"), "dominantType",
                  "dominantType", 16);
  sf_mex_addfield(*c1_info, c1_emlrt_marshallOut(
    "[ILXE]$matlabroot$/toolbox/shared/coder/coder/+coder/+internal/indexPlus.m"),
                  "resolved", "resolved", 16);
  sf_mex_addfield(*c1_info, c1_b_emlrt_marshallOut(1372586760U), "fileTimeLo",
                  "fileTimeLo", 16);
  sf_mex_addfield(*c1_info, c1_b_emlrt_marshallOut(0U), "fileTimeHi",
                  "fileTimeHi", 16);
  sf_mex_addfield(*c1_info, c1_b_emlrt_marshallOut(0U), "mFileTimeLo",
                  "mFileTimeLo", 16);
  sf_mex_addfield(*c1_info, c1_b_emlrt_marshallOut(0U), "mFileTimeHi",
                  "mFileTimeHi", 16);
  sf_mex_assign(&c1_rhs16, sf_mex_createcellmatrix(0, 1), false);
  sf_mex_assign(&c1_lhs16, sf_mex_createcellmatrix(0, 1), false);
  sf_mex_addfield(*c1_info, sf_mex_duplicatearraysafe(&c1_rhs16), "rhs", "rhs",
                  16);
  sf_mex_addfield(*c1_info, sf_mex_duplicatearraysafe(&c1_lhs16), "lhs", "lhs",
                  16);
  sf_mex_addfield(*c1_info, c1_emlrt_marshallOut(""), "context", "context", 17);
  sf_mex_addfield(*c1_info, c1_emlrt_marshallOut("length"), "name", "name", 17);
  sf_mex_addfield(*c1_info, c1_emlrt_marshallOut("double"), "dominantType",
                  "dominantType", 17);
  sf_mex_addfield(*c1_info, c1_emlrt_marshallOut(
    "[ILXE]$matlabroot$/toolbox/eml/lib/matlab/elmat/length.m"), "resolved",
                  "resolved", 17);
  sf_mex_addfield(*c1_info, c1_b_emlrt_marshallOut(1303149806U), "fileTimeLo",
                  "fileTimeLo", 17);
  sf_mex_addfield(*c1_info, c1_b_emlrt_marshallOut(0U), "fileTimeHi",
                  "fileTimeHi", 17);
  sf_mex_addfield(*c1_info, c1_b_emlrt_marshallOut(0U), "mFileTimeLo",
                  "mFileTimeLo", 17);
  sf_mex_addfield(*c1_info, c1_b_emlrt_marshallOut(0U), "mFileTimeHi",
                  "mFileTimeHi", 17);
  sf_mex_assign(&c1_rhs17, sf_mex_createcellmatrix(0, 1), false);
  sf_mex_assign(&c1_lhs17, sf_mex_createcellmatrix(0, 1), false);
  sf_mex_addfield(*c1_info, sf_mex_duplicatearraysafe(&c1_rhs17), "rhs", "rhs",
                  17);
  sf_mex_addfield(*c1_info, sf_mex_duplicatearraysafe(&c1_lhs17), "lhs", "lhs",
                  17);
  sf_mex_addfield(*c1_info, c1_emlrt_marshallOut(""), "context", "context", 18);
  sf_mex_addfield(*c1_info, c1_emlrt_marshallOut("length"), "name", "name", 18);
  sf_mex_addfield(*c1_info, c1_emlrt_marshallOut("single"), "dominantType",
                  "dominantType", 18);
  sf_mex_addfield(*c1_info, c1_emlrt_marshallOut(
    "[ILXE]$matlabroot$/toolbox/eml/lib/matlab/elmat/length.m"), "resolved",
                  "resolved", 18);
  sf_mex_addfield(*c1_info, c1_b_emlrt_marshallOut(1303149806U), "fileTimeLo",
                  "fileTimeLo", 18);
  sf_mex_addfield(*c1_info, c1_b_emlrt_marshallOut(0U), "fileTimeHi",
                  "fileTimeHi", 18);
  sf_mex_addfield(*c1_info, c1_b_emlrt_marshallOut(0U), "mFileTimeLo",
                  "mFileTimeLo", 18);
  sf_mex_addfield(*c1_info, c1_b_emlrt_marshallOut(0U), "mFileTimeHi",
                  "mFileTimeHi", 18);
  sf_mex_assign(&c1_rhs18, sf_mex_createcellmatrix(0, 1), false);
  sf_mex_assign(&c1_lhs18, sf_mex_createcellmatrix(0, 1), false);
  sf_mex_addfield(*c1_info, sf_mex_duplicatearraysafe(&c1_rhs18), "rhs", "rhs",
                  18);
  sf_mex_addfield(*c1_info, sf_mex_duplicatearraysafe(&c1_lhs18), "lhs", "lhs",
                  18);
  sf_mex_addfield(*c1_info, c1_emlrt_marshallOut(""), "context", "context", 19);
  sf_mex_addfield(*c1_info, c1_emlrt_marshallOut("length"), "name", "name", 19);
  sf_mex_addfield(*c1_info, c1_emlrt_marshallOut("uint8"), "dominantType",
                  "dominantType", 19);
  sf_mex_addfield(*c1_info, c1_emlrt_marshallOut(
    "[ILXE]$matlabroot$/toolbox/eml/lib/matlab/elmat/length.m"), "resolved",
                  "resolved", 19);
  sf_mex_addfield(*c1_info, c1_b_emlrt_marshallOut(1303149806U), "fileTimeLo",
                  "fileTimeLo", 19);
  sf_mex_addfield(*c1_info, c1_b_emlrt_marshallOut(0U), "fileTimeHi",
                  "fileTimeHi", 19);
  sf_mex_addfield(*c1_info, c1_b_emlrt_marshallOut(0U), "mFileTimeLo",
                  "mFileTimeLo", 19);
  sf_mex_addfield(*c1_info, c1_b_emlrt_marshallOut(0U), "mFileTimeHi",
                  "mFileTimeHi", 19);
  sf_mex_assign(&c1_rhs19, sf_mex_createcellmatrix(0, 1), false);
  sf_mex_assign(&c1_lhs19, sf_mex_createcellmatrix(0, 1), false);
  sf_mex_addfield(*c1_info, sf_mex_duplicatearraysafe(&c1_rhs19), "rhs", "rhs",
                  19);
  sf_mex_addfield(*c1_info, sf_mex_duplicatearraysafe(&c1_lhs19), "lhs", "lhs",
                  19);
  sf_mex_destroy(&c1_rhs0);
  sf_mex_destroy(&c1_lhs0);
  sf_mex_destroy(&c1_rhs1);
  sf_mex_destroy(&c1_lhs1);
  sf_mex_destroy(&c1_rhs2);
  sf_mex_destroy(&c1_lhs2);
  sf_mex_destroy(&c1_rhs3);
  sf_mex_destroy(&c1_lhs3);
  sf_mex_destroy(&c1_rhs4);
  sf_mex_destroy(&c1_lhs4);
  sf_mex_destroy(&c1_rhs5);
  sf_mex_destroy(&c1_lhs5);
  sf_mex_destroy(&c1_rhs6);
  sf_mex_destroy(&c1_lhs6);
  sf_mex_destroy(&c1_rhs7);
  sf_mex_destroy(&c1_lhs7);
  sf_mex_destroy(&c1_rhs8);
  sf_mex_destroy(&c1_lhs8);
  sf_mex_destroy(&c1_rhs9);
  sf_mex_destroy(&c1_lhs9);
  sf_mex_destroy(&c1_rhs10);
  sf_mex_destroy(&c1_lhs10);
  sf_mex_destroy(&c1_rhs11);
  sf_mex_destroy(&c1_lhs11);
  sf_mex_destroy(&c1_rhs12);
  sf_mex_destroy(&c1_lhs12);
  sf_mex_destroy(&c1_rhs13);
  sf_mex_destroy(&c1_lhs13);
  sf_mex_destroy(&c1_rhs14);
  sf_mex_destroy(&c1_lhs14);
  sf_mex_destroy(&c1_rhs15);
  sf_mex_destroy(&c1_lhs15);
  sf_mex_destroy(&c1_rhs16);
  sf_mex_destroy(&c1_lhs16);
  sf_mex_destroy(&c1_rhs17);
  sf_mex_destroy(&c1_lhs17);
  sf_mex_destroy(&c1_rhs18);
  sf_mex_destroy(&c1_lhs18);
  sf_mex_destroy(&c1_rhs19);
  sf_mex_destroy(&c1_lhs19);
}

static const mxArray *c1_emlrt_marshallOut(const char * c1_u)
{
  const mxArray *c1_y = NULL;
  c1_y = NULL;
  sf_mex_assign(&c1_y, sf_mex_create("y", c1_u, 15, 0U, 0U, 0U, 2, 1, strlen
    (c1_u)), false);
  return c1_y;
}

static const mxArray *c1_b_emlrt_marshallOut(const uint32_T c1_u)
{
  const mxArray *c1_y = NULL;
  c1_y = NULL;
  sf_mex_assign(&c1_y, sf_mex_create("y", &c1_u, 7, 0U, 0U, 0U, 0), false);
  return c1_y;
}

static void c1_floor(SFc1_cameraTestInstanceStruct *chartInstance, real32_T
                     c1_x_data[], int32_T c1_x_sizes[2], real32_T c1_b_x_data[],
                     int32_T c1_b_x_sizes[2])
{
  int32_T c1_x;
  int32_T c1_b_x;
  int32_T c1_loop_ub;
  int32_T c1_i76;
  c1_b_x_sizes[0] = c1_x_sizes[0];
  c1_b_x_sizes[1] = c1_x_sizes[1];
  c1_x = c1_b_x_sizes[0];
  c1_b_x = c1_b_x_sizes[1];
  c1_loop_ub = c1_x_sizes[0] * c1_x_sizes[1] - 1;
  for (c1_i76 = 0; c1_i76 <= c1_loop_ub; c1_i76++) {
    c1_b_x_data[c1_i76] = c1_x_data[c1_i76];
  }

  c1_b_floor(chartInstance, c1_b_x_data, c1_b_x_sizes);
}

static const mxArray *c1_i_sf_marshallOut(void *chartInstanceVoid, void
  *c1_inData)
{
  const mxArray *c1_mxArrayOutData = NULL;
  int32_T c1_u;
  const mxArray *c1_y = NULL;
  SFc1_cameraTestInstanceStruct *chartInstance;
  chartInstance = (SFc1_cameraTestInstanceStruct *)chartInstanceVoid;
  c1_mxArrayOutData = NULL;
  c1_u = *(int32_T *)c1_inData;
  c1_y = NULL;
  sf_mex_assign(&c1_y, sf_mex_create("y", &c1_u, 6, 0U, 0U, 0U, 0), false);
  sf_mex_assign(&c1_mxArrayOutData, c1_y, false);
  return c1_mxArrayOutData;
}

static int32_T c1_j_emlrt_marshallIn(SFc1_cameraTestInstanceStruct
  *chartInstance, const mxArray *c1_u, const emlrtMsgIdentifier *c1_parentId)
{
  int32_T c1_y;
  int32_T c1_i77;
  (void)chartInstance;
  sf_mex_import(c1_parentId, sf_mex_dup(c1_u), &c1_i77, 1, 6, 0U, 0, 0U, 0);
  c1_y = c1_i77;
  sf_mex_destroy(&c1_u);
  return c1_y;
}

static void c1_g_sf_marshallIn(void *chartInstanceVoid, const mxArray
  *c1_mxArrayInData, const char_T *c1_varName, void *c1_outData)
{
  const mxArray *c1_b_sfEvent;
  const char_T *c1_identifier;
  emlrtMsgIdentifier c1_thisId;
  int32_T c1_y;
  SFc1_cameraTestInstanceStruct *chartInstance;
  chartInstance = (SFc1_cameraTestInstanceStruct *)chartInstanceVoid;
  c1_b_sfEvent = sf_mex_dup(c1_mxArrayInData);
  c1_identifier = c1_varName;
  c1_thisId.fIdentifier = c1_identifier;
  c1_thisId.fParent = NULL;
  c1_y = c1_j_emlrt_marshallIn(chartInstance, sf_mex_dup(c1_b_sfEvent),
    &c1_thisId);
  sf_mex_destroy(&c1_b_sfEvent);
  *(int32_T *)c1_outData = c1_y;
  sf_mex_destroy(&c1_mxArrayInData);
}

static const mxArray *c1_blobs_bus_io(void *chartInstanceVoid, void *c1_pData)
{
  const mxArray *c1_mxVal = NULL;
  SFc1_cameraTestInstanceStruct *chartInstance;
  (void)c1_pData;
  chartInstance = (SFc1_cameraTestInstanceStruct *)chartInstanceVoid;
  c1_mxVal = NULL;
  sf_mex_assign(&c1_mxVal, c1_sf_marshall_unsupported(chartInstance), false);
  return c1_mxVal;
}

static const mxArray *c1_sf_marshall_unsupported(void *chartInstanceVoid)
{
  const mxArray *c1_y = NULL;
  SFc1_cameraTestInstanceStruct *chartInstance;
  chartInstance = (SFc1_cameraTestInstanceStruct *)chartInstanceVoid;
  c1_y = NULL;
  sf_mex_assign(&c1_y, c1_c_emlrt_marshallOut(chartInstance,
    "Structures with variable-sized fields unsupported for debugging."), false);
  return c1_y;
}

static const mxArray *c1_c_emlrt_marshallOut(SFc1_cameraTestInstanceStruct
  *chartInstance, const char * c1_u)
{
  const mxArray *c1_y = NULL;
  (void)chartInstance;
  c1_y = NULL;
  sf_mex_assign(&c1_y, sf_mex_create("y", c1_u, 15, 0U, 0U, 0U, 2, 1, strlen
    (c1_u)), false);
  return c1_y;
}

static uint8_T c1_k_emlrt_marshallIn(SFc1_cameraTestInstanceStruct
  *chartInstance, const mxArray *c1_b_is_active_c1_cameraTest, const char_T
  *c1_identifier)
{
  uint8_T c1_y;
  emlrtMsgIdentifier c1_thisId;
  c1_thisId.fIdentifier = c1_identifier;
  c1_thisId.fParent = NULL;
  c1_y = c1_l_emlrt_marshallIn(chartInstance, sf_mex_dup
    (c1_b_is_active_c1_cameraTest), &c1_thisId);
  sf_mex_destroy(&c1_b_is_active_c1_cameraTest);
  return c1_y;
}

static uint8_T c1_l_emlrt_marshallIn(SFc1_cameraTestInstanceStruct
  *chartInstance, const mxArray *c1_u, const emlrtMsgIdentifier *c1_parentId)
{
  uint8_T c1_y;
  uint8_T c1_u1;
  (void)chartInstance;
  sf_mex_import(c1_parentId, sf_mex_dup(c1_u), &c1_u1, 1, 3, 0U, 0, 0U, 0);
  c1_y = c1_u1;
  sf_mex_destroy(&c1_u);
  return c1_y;
}

static void c1_b_floor(SFc1_cameraTestInstanceStruct *chartInstance, real32_T
  c1_x_data[], int32_T c1_x_sizes[2])
{
  real_T c1_d3;
  int32_T c1_i78;
  int32_T c1_k;
  real_T c1_b_k;
  real32_T c1_x;
  real32_T c1_b_x;
  int32_T c1_c_x[1];
  (void)chartInstance;
  c1_d3 = (real_T)(c1_x_sizes[0] * c1_x_sizes[1]);
  c1_i78 = (int32_T)c1_d3 - 1;
  for (c1_k = 0; c1_k <= c1_i78; c1_k++) {
    c1_b_k = 1.0 + (real_T)c1_k;
    c1_x = c1_x_data[_SFD_EML_ARRAY_BOUNDS_CHECK("", (int32_T)c1_b_k, 1,
      c1_x_sizes[0] * c1_x_sizes[1], 1, 0) - 1];
    c1_b_x = c1_x;
    c1_b_x = muSingleScalarFloor(c1_b_x);
    c1_c_x[0] = c1_x_sizes[0] * c1_x_sizes[1];
    c1_x_data[_SFD_EML_ARRAY_BOUNDS_CHECK("", (int32_T)c1_b_k, 1, c1_x_sizes[0] *
      c1_x_sizes[1], 1, 0) - 1] = c1_b_x;
  }
}

static void init_dsm_address_info(SFc1_cameraTestInstanceStruct *chartInstance)
{
  (void)chartInstance;
}

static void init_simulink_io_address(SFc1_cameraTestInstanceStruct
  *chartInstance)
{
  chartInstance->c1_centroids_data = (real32_T (*)[32])
    ssGetInputPortSignal_wrapper(chartInstance->S, 0);
  chartInstance->c1_centroids_sizes = (int32_T (*)[2])
    ssGetCurrentInputPortDimensions_wrapper(chartInstance->S, 0);
  chartInstance->c1_area_data = (int32_T (*)[16])ssGetInputPortSignal_wrapper
    (chartInstance->S, 1);
  chartInstance->c1_area_sizes = (int32_T (*)[2])
    ssGetCurrentInputPortDimensions_wrapper(chartInstance->S, 1);
  sf_mex_size_one_check(((*chartInstance->c1_area_sizes)[1U] == 0) &&
                        (!((*chartInstance->c1_area_sizes)[0U] == 0)), "area");
  chartInstance->c1_b_RBW = (boolean_T (*)[76800])ssGetInputPortSignal_wrapper
    (chartInstance->S, 2);
  chartInstance->c1_GBW = (boolean_T (*)[76800])ssGetInputPortSignal_wrapper
    (chartInstance->S, 3);
  chartInstance->c1_BBW = (boolean_T (*)[76800])ssGetInputPortSignal_wrapper
    (chartInstance->S, 4);
  chartInstance->c1_blobs_data = (c1_BlobData *)ssGetOutputPortSignal_wrapper
    (chartInstance->S, 1);
  chartInstance->c1_blobs_elems_sizes = (c1_BlobData_size *)
    ssGetCurrentOutputPortDimensions_wrapper(chartInstance->S, 1);
}

/* SFunction Glue Code */
#ifdef utFree
#undef utFree
#endif

#ifdef utMalloc
#undef utMalloc
#endif

#ifdef __cplusplus

extern "C" void *utMalloc(size_t size);
extern "C" void utFree(void*);

#else

extern void *utMalloc(size_t size);
extern void utFree(void*);

#endif

void sf_c1_cameraTest_get_check_sum(mxArray *plhs[])
{
  ((real_T *)mxGetPr((plhs[0])))[0] = (real_T)(15145274U);
  ((real_T *)mxGetPr((plhs[0])))[1] = (real_T)(809127435U);
  ((real_T *)mxGetPr((plhs[0])))[2] = (real_T)(598788559U);
  ((real_T *)mxGetPr((plhs[0])))[3] = (real_T)(19878952U);
}

mxArray* sf_c1_cameraTest_get_post_codegen_info(void);
mxArray *sf_c1_cameraTest_get_autoinheritance_info(void)
{
  const char *autoinheritanceFields[] = { "checksum", "inputs", "parameters",
    "outputs", "locals", "postCodegenInfo" };

  mxArray *mxAutoinheritanceInfo = mxCreateStructMatrix(1, 1, sizeof
    (autoinheritanceFields)/sizeof(autoinheritanceFields[0]),
    autoinheritanceFields);

  {
    mxArray *mxChecksum = mxCreateString("1jREDWdNB9c7UGdtzAQKDC");
    mxSetField(mxAutoinheritanceInfo,0,"checksum",mxChecksum);
  }

  {
    const char *dataFields[] = { "size", "type", "complexity" };

    mxArray *mxData = mxCreateStructMatrix(1,5,3,dataFields);

    {
      mxArray *mxSize = mxCreateDoubleMatrix(1,2,mxREAL);
      double *pr = mxGetPr(mxSize);
      pr[0] = (double)(16);
      pr[1] = (double)(2);
      mxSetField(mxData,0,"size",mxSize);
    }

    {
      const char *typeFields[] = { "base", "fixpt" };

      mxArray *mxType = mxCreateStructMatrix(1,1,2,typeFields);
      mxSetField(mxType,0,"base",mxCreateDoubleScalar(9));
      mxSetField(mxType,0,"fixpt",mxCreateDoubleMatrix(0,0,mxREAL));
      mxSetField(mxData,0,"type",mxType);
    }

    mxSetField(mxData,0,"complexity",mxCreateDoubleScalar(0));

    {
      mxArray *mxSize = mxCreateDoubleMatrix(1,2,mxREAL);
      double *pr = mxGetPr(mxSize);
      pr[0] = (double)(16);
      pr[1] = (double)(1);
      mxSetField(mxData,1,"size",mxSize);
    }

    {
      const char *typeFields[] = { "base", "fixpt" };

      mxArray *mxType = mxCreateStructMatrix(1,1,2,typeFields);
      mxSetField(mxType,0,"base",mxCreateDoubleScalar(8));
      mxSetField(mxType,0,"fixpt",mxCreateDoubleMatrix(0,0,mxREAL));
      mxSetField(mxData,1,"type",mxType);
    }

    mxSetField(mxData,1,"complexity",mxCreateDoubleScalar(0));

    {
      mxArray *mxSize = mxCreateDoubleMatrix(1,2,mxREAL);
      double *pr = mxGetPr(mxSize);
      pr[0] = (double)(320);
      pr[1] = (double)(240);
      mxSetField(mxData,2,"size",mxSize);
    }

    {
      const char *typeFields[] = { "base", "fixpt" };

      mxArray *mxType = mxCreateStructMatrix(1,1,2,typeFields);
      mxSetField(mxType,0,"base",mxCreateDoubleScalar(1));
      mxSetField(mxType,0,"fixpt",mxCreateDoubleMatrix(0,0,mxREAL));
      mxSetField(mxData,2,"type",mxType);
    }

    mxSetField(mxData,2,"complexity",mxCreateDoubleScalar(0));

    {
      mxArray *mxSize = mxCreateDoubleMatrix(1,2,mxREAL);
      double *pr = mxGetPr(mxSize);
      pr[0] = (double)(320);
      pr[1] = (double)(240);
      mxSetField(mxData,3,"size",mxSize);
    }

    {
      const char *typeFields[] = { "base", "fixpt" };

      mxArray *mxType = mxCreateStructMatrix(1,1,2,typeFields);
      mxSetField(mxType,0,"base",mxCreateDoubleScalar(1));
      mxSetField(mxType,0,"fixpt",mxCreateDoubleMatrix(0,0,mxREAL));
      mxSetField(mxData,3,"type",mxType);
    }

    mxSetField(mxData,3,"complexity",mxCreateDoubleScalar(0));

    {
      mxArray *mxSize = mxCreateDoubleMatrix(1,2,mxREAL);
      double *pr = mxGetPr(mxSize);
      pr[0] = (double)(320);
      pr[1] = (double)(240);
      mxSetField(mxData,4,"size",mxSize);
    }

    {
      const char *typeFields[] = { "base", "fixpt" };

      mxArray *mxType = mxCreateStructMatrix(1,1,2,typeFields);
      mxSetField(mxType,0,"base",mxCreateDoubleScalar(1));
      mxSetField(mxType,0,"fixpt",mxCreateDoubleMatrix(0,0,mxREAL));
      mxSetField(mxData,4,"type",mxType);
    }

    mxSetField(mxData,4,"complexity",mxCreateDoubleScalar(0));
    mxSetField(mxAutoinheritanceInfo,0,"inputs",mxData);
  }

  {
    mxSetField(mxAutoinheritanceInfo,0,"parameters",mxCreateDoubleMatrix(0,0,
                mxREAL));
  }

  {
    const char *dataFields[] = { "size", "type", "complexity" };

    mxArray *mxData = mxCreateStructMatrix(1,1,3,dataFields);

    {
      mxArray *mxSize = mxCreateDoubleMatrix(1,2,mxREAL);
      double *pr = mxGetPr(mxSize);
      pr[0] = (double)(1);
      pr[1] = (double)(1);
      mxSetField(mxData,0,"size",mxSize);
    }

    {
      const char *typeFields[] = { "base", "fixpt" };

      mxArray *mxType = mxCreateStructMatrix(1,1,2,typeFields);
      mxSetField(mxType,0,"base",mxCreateDoubleScalar(13));
      mxSetField(mxType,0,"fixpt",mxCreateDoubleMatrix(0,0,mxREAL));
      mxSetField(mxData,0,"type",mxType);
    }

    mxSetField(mxData,0,"complexity",mxCreateDoubleScalar(0));
    mxSetField(mxAutoinheritanceInfo,0,"outputs",mxData);
  }

  {
    mxSetField(mxAutoinheritanceInfo,0,"locals",mxCreateDoubleMatrix(0,0,mxREAL));
  }

  {
    mxArray* mxPostCodegenInfo = sf_c1_cameraTest_get_post_codegen_info();
    mxSetField(mxAutoinheritanceInfo,0,"postCodegenInfo",mxPostCodegenInfo);
  }

  return(mxAutoinheritanceInfo);
}

mxArray *sf_c1_cameraTest_third_party_uses_info(void)
{
  mxArray * mxcell3p = mxCreateCellMatrix(1,0);
  return(mxcell3p);
}

mxArray *sf_c1_cameraTest_jit_fallback_info(void)
{
  const char *infoFields[] = { "fallbackType", "fallbackReason",
    "incompatibleSymbol", };

  mxArray *mxInfo = mxCreateStructMatrix(1, 1, 3, infoFields);
  mxArray *fallbackReason = mxCreateString("feature_off");
  mxArray *incompatibleSymbol = mxCreateString("");
  mxArray *fallbackType = mxCreateString("early");
  mxSetField(mxInfo, 0, infoFields[0], fallbackType);
  mxSetField(mxInfo, 0, infoFields[1], fallbackReason);
  mxSetField(mxInfo, 0, infoFields[2], incompatibleSymbol);
  return mxInfo;
}

mxArray *sf_c1_cameraTest_updateBuildInfo_args_info(void)
{
  mxArray *mxBIArgs = mxCreateCellMatrix(1,0);
  return mxBIArgs;
}

mxArray* sf_c1_cameraTest_get_post_codegen_info(void)
{
  const char* fieldNames[] = { "exportedFunctionsUsedByThisChart",
    "exportedFunctionsChecksum" };

  mwSize dims[2] = { 1, 1 };

  mxArray* mxPostCodegenInfo = mxCreateStructArray(2, dims, sizeof(fieldNames)/
    sizeof(fieldNames[0]), fieldNames);

  {
    mxArray* mxExportedFunctionsChecksum = mxCreateString("");
    mwSize exp_dims[2] = { 0, 1 };

    mxArray* mxExportedFunctionsUsedByThisChart = mxCreateCellArray(2, exp_dims);
    mxSetField(mxPostCodegenInfo, 0, "exportedFunctionsUsedByThisChart",
               mxExportedFunctionsUsedByThisChart);
    mxSetField(mxPostCodegenInfo, 0, "exportedFunctionsChecksum",
               mxExportedFunctionsChecksum);
  }

  return mxPostCodegenInfo;
}

static const mxArray *sf_get_sim_state_info_c1_cameraTest(void)
{
  const char *infoFields[] = { "chartChecksum", "varInfo" };

  mxArray *mxInfo = mxCreateStructMatrix(1, 1, 2, infoFields);
  const char *infoEncStr[] = {
    "100 S1x2'type','srcId','name','auxInfo'{{M[1],M[10],T\"blobs\",},{M[8],M[0],T\"is_active_c1_cameraTest\",}}"
  };

  mxArray *mxVarInfo = sf_mex_decode_encoded_mx_struct_array(infoEncStr, 2, 10);
  mxArray *mxChecksum = mxCreateDoubleMatrix(1, 4, mxREAL);
  sf_c1_cameraTest_get_check_sum(&mxChecksum);
  mxSetField(mxInfo, 0, infoFields[0], mxChecksum);
  mxSetField(mxInfo, 0, infoFields[1], mxVarInfo);
  return mxInfo;
}

static void chart_debug_initialization(SimStruct *S, unsigned int
  fullDebuggerInitialization)
{
  if (!sim_mode_is_rtw_gen(S)) {
    SFc1_cameraTestInstanceStruct *chartInstance;
    ChartRunTimeInfo * crtInfo = (ChartRunTimeInfo *)(ssGetUserData(S));
    ChartInfoStruct * chartInfo = (ChartInfoStruct *)(crtInfo->instanceInfo);
    chartInstance = (SFc1_cameraTestInstanceStruct *) chartInfo->chartInstance;
    if (ssIsFirstInitCond(S) && fullDebuggerInitialization==1) {
      /* do this only if simulation is starting */
      {
        unsigned int chartAlreadyPresent;
        chartAlreadyPresent = sf_debug_initialize_chart
          (sfGlobalDebugInstanceStruct,
           _cameraTestMachineNumber_,
           1,
           1,
           1,
           0,
           6,
           0,
           0,
           0,
           0,
           0,
           &(chartInstance->chartNumber),
           &(chartInstance->instanceNumber),
           (void *)S);

        /* Each instance must initialize its own list of scripts */
        init_script_number_translation(_cameraTestMachineNumber_,
          chartInstance->chartNumber,chartInstance->instanceNumber);
        if (chartAlreadyPresent==0) {
          /* this is the first instance */
          sf_debug_set_chart_disable_implicit_casting
            (sfGlobalDebugInstanceStruct,_cameraTestMachineNumber_,
             chartInstance->chartNumber,1);
          sf_debug_set_chart_event_thresholds(sfGlobalDebugInstanceStruct,
            _cameraTestMachineNumber_,
            chartInstance->chartNumber,
            0,
            0,
            0);
          _SFD_SET_DATA_PROPS(0,1,1,0,"centroids");
          _SFD_SET_DATA_PROPS(1,1,1,0,"area");
          _SFD_SET_DATA_PROPS(2,1,1,0,"RBW");
          _SFD_SET_DATA_PROPS(3,1,1,0,"GBW");
          _SFD_SET_DATA_PROPS(4,1,1,0,"BBW");
          _SFD_SET_DATA_PROPS(5,2,0,1,"blobs");
          _SFD_STATE_INFO(0,0,2);
          _SFD_CH_SUBSTATE_COUNT(0);
          _SFD_CH_SUBSTATE_DECOMP(0);
        }

        _SFD_CV_INIT_CHART(0,0,0,0);

        {
          _SFD_CV_INIT_STATE(0,0,0,0,0,0,NULL,NULL);
        }

        _SFD_CV_INIT_TRANS(0,0,NULL,NULL,0,NULL);

        /* Initialization of MATLAB Function Model Coverage */
        _SFD_CV_INIT_EML(0,1,1,1,0,1,0,1,0,0,0);
        _SFD_CV_INIT_EML_FCN(0,0,"eML_blk_kernel",0,-1,851);
        _SFD_CV_INIT_EML_SATURATION(0,1,0,761,-1,778);
        _SFD_CV_INIT_EML_IF(0,1,0,595,614,-1,789);
        _SFD_CV_INIT_EML_FOR(0,1,0,371,387,793);
        _SFD_CV_INIT_EML_RELATIONAL(0,1,0,598,614,-1,0);

        {
          unsigned int dimVector[2];
          dimVector[0]= 16;
          dimVector[1]= 2;
          _SFD_SET_DATA_COMPILED_PROPS(0,SF_SINGLE,2,&(dimVector[0]),0,0,0,0.0,
            1.0,0,0,(MexFcnForType)c1_d_sf_marshallOut,(MexInFcnForType)NULL);
        }

        {
          unsigned int dimVector[2];
          dimVector[0]= 16;
          dimVector[1]= 1;
          _SFD_SET_DATA_COMPILED_PROPS(1,SF_INT32,2,&(dimVector[0]),0,0,0,0.0,
            1.0,0,0,(MexFcnForType)c1_c_sf_marshallOut,(MexInFcnForType)NULL);
        }

        {
          unsigned int dimVector[2];
          dimVector[0]= 320;
          dimVector[1]= 240;
          _SFD_SET_DATA_COMPILED_PROPS(2,SF_UINT8,2,&(dimVector[0]),0,0,0,0.0,
            1.0,0,0,(MexFcnForType)c1_b_sf_marshallOut,(MexInFcnForType)NULL);
        }

        {
          unsigned int dimVector[2];
          dimVector[0]= 320;
          dimVector[1]= 240;
          _SFD_SET_DATA_COMPILED_PROPS(3,SF_UINT8,2,&(dimVector[0]),0,0,0,0.0,
            1.0,0,0,(MexFcnForType)c1_b_sf_marshallOut,(MexInFcnForType)NULL);
        }

        {
          unsigned int dimVector[2];
          dimVector[0]= 320;
          dimVector[1]= 240;
          _SFD_SET_DATA_COMPILED_PROPS(4,SF_UINT8,2,&(dimVector[0]),0,0,0,0.0,
            1.0,0,0,(MexFcnForType)c1_b_sf_marshallOut,(MexInFcnForType)NULL);
        }

        _SFD_SET_DATA_COMPILED_PROPS(5,SF_STRUCT,0,NULL,0,0,0,0.0,1.0,0,0,
          (MexFcnForType)c1_blobs_bus_io,(MexInFcnForType)NULL);
        _SFD_SET_DATA_VALUE_PTR_VAR_DIM(0U, *chartInstance->c1_centroids_data,
          (void *)chartInstance->c1_centroids_sizes);
        _SFD_SET_DATA_VALUE_PTR_VAR_DIM(1U, *chartInstance->c1_area_data, (void *)
          chartInstance->c1_area_sizes);
        _SFD_SET_DATA_VALUE_PTR(2U, *chartInstance->c1_b_RBW);
        _SFD_SET_DATA_VALUE_PTR(3U, *chartInstance->c1_GBW);
        _SFD_SET_DATA_VALUE_PTR(4U, *chartInstance->c1_BBW);
        _SFD_SET_DATA_VALUE_PTR(5U, chartInstance->c1_blobs_data);
      }
    } else {
      sf_debug_reset_current_state_configuration(sfGlobalDebugInstanceStruct,
        _cameraTestMachineNumber_,chartInstance->chartNumber,
        chartInstance->instanceNumber);
    }
  }
}

static const char* sf_get_instance_specialization(void)
{
  return "aVDZbfY42Mztqkw811HH0D";
}

static void sf_opaque_initialize_c1_cameraTest(void *chartInstanceVar)
{
  chart_debug_initialization(((SFc1_cameraTestInstanceStruct*) chartInstanceVar
    )->S,0);
  initialize_params_c1_cameraTest((SFc1_cameraTestInstanceStruct*)
    chartInstanceVar);
  initialize_c1_cameraTest((SFc1_cameraTestInstanceStruct*) chartInstanceVar);
}

static void sf_opaque_enable_c1_cameraTest(void *chartInstanceVar)
{
  enable_c1_cameraTest((SFc1_cameraTestInstanceStruct*) chartInstanceVar);
}

static void sf_opaque_disable_c1_cameraTest(void *chartInstanceVar)
{
  disable_c1_cameraTest((SFc1_cameraTestInstanceStruct*) chartInstanceVar);
}

static void sf_opaque_gateway_c1_cameraTest(void *chartInstanceVar)
{
  sf_gateway_c1_cameraTest((SFc1_cameraTestInstanceStruct*) chartInstanceVar);
}

static const mxArray* sf_opaque_get_sim_state_c1_cameraTest(SimStruct* S)
{
  ChartRunTimeInfo * crtInfo = (ChartRunTimeInfo *)(ssGetUserData(S));
  ChartInfoStruct * chartInfo = (ChartInfoStruct *)(crtInfo->instanceInfo);
  return get_sim_state_c1_cameraTest((SFc1_cameraTestInstanceStruct*)
    chartInfo->chartInstance);         /* raw sim ctx */
}

static void sf_opaque_set_sim_state_c1_cameraTest(SimStruct* S, const mxArray
  *st)
{
  ChartRunTimeInfo * crtInfo = (ChartRunTimeInfo *)(ssGetUserData(S));
  ChartInfoStruct * chartInfo = (ChartInfoStruct *)(crtInfo->instanceInfo);
  set_sim_state_c1_cameraTest((SFc1_cameraTestInstanceStruct*)
    chartInfo->chartInstance, st);
}

static void sf_opaque_terminate_c1_cameraTest(void *chartInstanceVar)
{
  if (chartInstanceVar!=NULL) {
    SimStruct *S = ((SFc1_cameraTestInstanceStruct*) chartInstanceVar)->S;
    ChartRunTimeInfo * crtInfo = (ChartRunTimeInfo *)(ssGetUserData(S));
    if (sim_mode_is_rtw_gen(S) || sim_mode_is_external(S)) {
      sf_clear_rtw_identifier(S);
      unload_cameraTest_optimization_info();
    }

    finalize_c1_cameraTest((SFc1_cameraTestInstanceStruct*) chartInstanceVar);
    utFree(chartInstanceVar);
    if (crtInfo != NULL) {
      utFree(crtInfo);
    }

    ssSetUserData(S,NULL);
  }
}

static void sf_opaque_init_subchart_simstructs(void *chartInstanceVar)
{
  initSimStructsc1_cameraTest((SFc1_cameraTestInstanceStruct*) chartInstanceVar);
}

extern unsigned int sf_machine_global_initializer_called(void);
static void mdlProcessParameters_c1_cameraTest(SimStruct *S)
{
  int i;
  for (i=0;i<ssGetNumRunTimeParams(S);i++) {
    if (ssGetSFcnParamTunable(S,i)) {
      ssUpdateDlgParamAsRunTimeParam(S,i);
    }
  }

  if (sf_machine_global_initializer_called()) {
    ChartRunTimeInfo * crtInfo = (ChartRunTimeInfo *)(ssGetUserData(S));
    ChartInfoStruct * chartInfo = (ChartInfoStruct *)(crtInfo->instanceInfo);
    initialize_params_c1_cameraTest((SFc1_cameraTestInstanceStruct*)
      (chartInfo->chartInstance));
  }
}

static void mdlSetWorkWidths_c1_cameraTest(SimStruct *S)
{
  if (sim_mode_is_rtw_gen(S) || sim_mode_is_external(S)) {
    mxArray *infoStruct = load_cameraTest_optimization_info();
    int_T chartIsInlinable =
      (int_T)sf_is_chart_inlinable(sf_get_instance_specialization(),infoStruct,1);
    ssSetStateflowIsInlinable(S,chartIsInlinable);
    ssSetRTWCG(S,sf_rtw_info_uint_prop(sf_get_instance_specialization(),
                infoStruct,1,"RTWCG"));
    ssSetEnableFcnIsTrivial(S,1);
    ssSetDisableFcnIsTrivial(S,1);
    ssSetNotMultipleInlinable(S,sf_rtw_info_uint_prop
      (sf_get_instance_specialization(),infoStruct,1,
       "gatewayCannotBeInlinedMultipleTimes"));
    sf_update_buildInfo(sf_get_instance_specialization(),infoStruct,1);
    if (chartIsInlinable) {
      ssSetInputPortOptimOpts(S, 0, SS_REUSABLE_AND_LOCAL);
      ssSetInputPortOptimOpts(S, 1, SS_REUSABLE_AND_LOCAL);
      ssSetInputPortOptimOpts(S, 2, SS_REUSABLE_AND_LOCAL);
      ssSetInputPortOptimOpts(S, 3, SS_REUSABLE_AND_LOCAL);
      ssSetInputPortOptimOpts(S, 4, SS_REUSABLE_AND_LOCAL);
      sf_mark_chart_expressionable_inputs(S,sf_get_instance_specialization(),
        infoStruct,1,5);
      sf_mark_chart_reusable_outputs(S,sf_get_instance_specialization(),
        infoStruct,1,1);
    }

    {
      unsigned int outPortIdx;
      for (outPortIdx=1; outPortIdx<=1; ++outPortIdx) {
        ssSetOutputPortOptimizeInIR(S, outPortIdx, 1U);
      }
    }

    {
      unsigned int inPortIdx;
      for (inPortIdx=0; inPortIdx < 5; ++inPortIdx) {
        ssSetInputPortOptimizeInIR(S, inPortIdx, 1U);
      }
    }

    sf_set_rtw_dwork_info(S,sf_get_instance_specialization(),infoStruct,1);
    ssSetHasSubFunctions(S,!(chartIsInlinable));
  } else {
  }

  ssSetOptions(S,ssGetOptions(S)|SS_OPTION_WORKS_WITH_CODE_REUSE);
  ssSetChecksum0(S,(1688072655U));
  ssSetChecksum1(S,(2195017827U));
  ssSetChecksum2(S,(3617827596U));
  ssSetChecksum3(S,(1733403603U));
  ssSetmdlDerivatives(S, NULL);
  ssSetExplicitFCSSCtrl(S,1);
  ssSupportsMultipleExecInstances(S,1);
}

static void mdlRTW_c1_cameraTest(SimStruct *S)
{
  if (sim_mode_is_rtw_gen(S)) {
    ssWriteRTWStrParam(S, "StateflowChartType", "Embedded MATLAB");
  }
}

static void mdlStart_c1_cameraTest(SimStruct *S)
{
  SFc1_cameraTestInstanceStruct *chartInstance;
  ChartRunTimeInfo * crtInfo = (ChartRunTimeInfo *)utMalloc(sizeof
    (ChartRunTimeInfo));
  chartInstance = (SFc1_cameraTestInstanceStruct *)utMalloc(sizeof
    (SFc1_cameraTestInstanceStruct));
  memset(chartInstance, 0, sizeof(SFc1_cameraTestInstanceStruct));
  if (chartInstance==NULL) {
    sf_mex_error_message("Could not allocate memory for chart instance.");
  }

  chartInstance->chartInfo.chartInstance = chartInstance;
  chartInstance->chartInfo.isEMLChart = 1;
  chartInstance->chartInfo.chartInitialized = 0;
  chartInstance->chartInfo.sFunctionGateway = sf_opaque_gateway_c1_cameraTest;
  chartInstance->chartInfo.initializeChart = sf_opaque_initialize_c1_cameraTest;
  chartInstance->chartInfo.terminateChart = sf_opaque_terminate_c1_cameraTest;
  chartInstance->chartInfo.enableChart = sf_opaque_enable_c1_cameraTest;
  chartInstance->chartInfo.disableChart = sf_opaque_disable_c1_cameraTest;
  chartInstance->chartInfo.getSimState = sf_opaque_get_sim_state_c1_cameraTest;
  chartInstance->chartInfo.setSimState = sf_opaque_set_sim_state_c1_cameraTest;
  chartInstance->chartInfo.getSimStateInfo = sf_get_sim_state_info_c1_cameraTest;
  chartInstance->chartInfo.zeroCrossings = NULL;
  chartInstance->chartInfo.outputs = NULL;
  chartInstance->chartInfo.derivatives = NULL;
  chartInstance->chartInfo.mdlRTW = mdlRTW_c1_cameraTest;
  chartInstance->chartInfo.mdlStart = mdlStart_c1_cameraTest;
  chartInstance->chartInfo.mdlSetWorkWidths = mdlSetWorkWidths_c1_cameraTest;
  chartInstance->chartInfo.extModeExec = NULL;
  chartInstance->chartInfo.restoreLastMajorStepConfiguration = NULL;
  chartInstance->chartInfo.restoreBeforeLastMajorStepConfiguration = NULL;
  chartInstance->chartInfo.storeCurrentConfiguration = NULL;
  chartInstance->chartInfo.callAtomicSubchartUserFcn = NULL;
  chartInstance->chartInfo.callAtomicSubchartAutoFcn = NULL;
  chartInstance->chartInfo.debugInstance = sfGlobalDebugInstanceStruct;
  chartInstance->S = S;
  crtInfo->checksum = SF_RUNTIME_INFO_CHECKSUM;
  crtInfo->instanceInfo = (&(chartInstance->chartInfo));
  crtInfo->isJITEnabled = false;
  crtInfo->compiledInfo = NULL;
  ssSetUserData(S,(void *)(crtInfo));  /* register the chart instance with simstruct */
  init_dsm_address_info(chartInstance);
  init_simulink_io_address(chartInstance);
  if (!sim_mode_is_rtw_gen(S)) {
  }

  sf_opaque_init_subchart_simstructs(chartInstance->chartInfo.chartInstance);
  chart_debug_initialization(S,1);
}

void c1_cameraTest_method_dispatcher(SimStruct *S, int_T method, void *data)
{
  switch (method) {
   case SS_CALL_MDL_START:
    mdlStart_c1_cameraTest(S);
    break;

   case SS_CALL_MDL_SET_WORK_WIDTHS:
    mdlSetWorkWidths_c1_cameraTest(S);
    break;

   case SS_CALL_MDL_PROCESS_PARAMETERS:
    mdlProcessParameters_c1_cameraTest(S);
    break;

   default:
    /* Unhandled method */
    sf_mex_error_message("Stateflow Internal Error:\n"
                         "Error calling c1_cameraTest_method_dispatcher.\n"
                         "Can't handle method %d.\n", method);
    break;
  }
}

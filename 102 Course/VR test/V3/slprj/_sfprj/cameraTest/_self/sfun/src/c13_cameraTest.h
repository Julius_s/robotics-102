#ifndef __c13_cameraTest_h__
#define __c13_cameraTest_h__

/* Include files */
#include "sf_runtime/sfc_sf.h"
#include "sf_runtime/sfc_mex.h"
#include "rtwtypes.h"
#include "multiword_types.h"

/* Type Definitions */
#ifndef typedef_SFc13_cameraTestInstanceStruct
#define typedef_SFc13_cameraTestInstanceStruct

typedef struct {
  SimStruct *S;
  ChartInfoStruct chartInfo;
  uint32_T chartNumber;
  uint32_T instanceNumber;
  int32_T c13_sfEvent;
  boolean_T c13_isStable;
  boolean_T c13_doneDoubleBufferReInit;
  uint8_T c13_is_active_c13_cameraTest;
  real32_T c13_H[76800];
  real32_T c13_S[76800];
  real32_T c13_V[76800];
  boolean_T c13_ballBool[76800];
  real32_T c13_inData[76800];
  real32_T c13_u[76800];
  real32_T (*c13_b_H)[76800];
  real32_T (*c13_b_S)[76800];
  real32_T (*c13_b_V)[76800];
  boolean_T (*c13_b_ballBool)[76800];
} SFc13_cameraTestInstanceStruct;

#endif                                 /*typedef_SFc13_cameraTestInstanceStruct*/

/* Named Constants */

/* Variable Declarations */
extern struct SfDebugInstanceStruct *sfGlobalDebugInstanceStruct;

/* Variable Definitions */

/* Function Declarations */
extern const mxArray *sf_c13_cameraTest_get_eml_resolved_functions_info(void);

/* Function Definitions */
extern void sf_c13_cameraTest_get_check_sum(mxArray *plhs[]);
extern void c13_cameraTest_method_dispatcher(SimStruct *S, int_T method, void
  *data);

#endif

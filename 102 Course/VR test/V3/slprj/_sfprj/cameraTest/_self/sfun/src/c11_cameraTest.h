#ifndef __c11_cameraTest_h__
#define __c11_cameraTest_h__

/* Include files */
#include "sf_runtime/sfc_sf.h"
#include "sf_runtime/sfc_mex.h"
#include "rtwtypes.h"
#include "multiword_types.h"

/* Type Definitions */
#ifndef typedef_SFc11_cameraTestInstanceStruct
#define typedef_SFc11_cameraTestInstanceStruct

typedef struct {
  SimStruct *S;
  ChartInfoStruct chartInfo;
  uint32_T chartNumber;
  uint32_T instanceNumber;
  int32_T c11_sfEvent;
  boolean_T c11_isStable;
  boolean_T c11_doneDoubleBufferReInit;
  uint8_T c11_is_active_c11_cameraTest;
  int32_T *c11_spHandle_address;
  int32_T c11_spHandle_index;
  uint8_T (*c11_dataOut)[35];
  real_T *c11_send;
} SFc11_cameraTestInstanceStruct;

#endif                                 /*typedef_SFc11_cameraTestInstanceStruct*/

/* Named Constants */

/* Variable Declarations */
extern struct SfDebugInstanceStruct *sfGlobalDebugInstanceStruct;

/* Variable Definitions */

/* Function Declarations */
extern const mxArray *sf_c11_cameraTest_get_eml_resolved_functions_info(void);

/* Function Definitions */
extern void sf_c11_cameraTest_get_check_sum(mxArray *plhs[]);
extern void c11_cameraTest_method_dispatcher(SimStruct *S, int_T method, void
  *data);

#endif

#ifndef __c9_LessonI_h__
#define __c9_LessonI_h__

/* Include files */
#include "sf_runtime/sfc_sf.h"
#include "sf_runtime/sfc_mex.h"
#include "rtwtypes.h"
#include "multiword_types.h"

/* Type Definitions */
#ifndef typedef_SFc9_LessonIInstanceStruct
#define typedef_SFc9_LessonIInstanceStruct

typedef struct {
  SimStruct *S;
  ChartInfoStruct chartInfo;
  uint32_T chartNumber;
  uint32_T instanceNumber;
  int32_T c9_sfEvent;
  boolean_T c9_isStable;
  boolean_T c9_doneDoubleBufferReInit;
  uint8_T c9_is_active_c9_LessonI;
  int32_T (*c9_area_data)[16];
  int32_T (*c9_area_sizes)[2];
  real_T *c9_lengthh;
} SFc9_LessonIInstanceStruct;

#endif                                 /*typedef_SFc9_LessonIInstanceStruct*/

/* Named Constants */

/* Variable Declarations */
extern struct SfDebugInstanceStruct *sfGlobalDebugInstanceStruct;

/* Variable Definitions */

/* Function Declarations */
extern const mxArray *sf_c9_LessonI_get_eml_resolved_functions_info(void);

/* Function Definitions */
extern void sf_c9_LessonI_get_check_sum(mxArray *plhs[]);
extern void c9_LessonI_method_dispatcher(SimStruct *S, int_T method, void *data);

#endif

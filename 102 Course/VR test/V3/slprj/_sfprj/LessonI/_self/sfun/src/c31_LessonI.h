#ifndef __c31_LessonI_h__
#define __c31_LessonI_h__

/* Include files */
#include "sf_runtime/sfc_sf.h"
#include "sf_runtime/sfc_mex.h"
#include "rtwtypes.h"
#include "multiword_types.h"

/* Type Definitions */
#ifndef struct_Player_tag
#define struct_Player_tag

struct Player_tag
{
  int8_T x;
  int8_T y;
  int16_T orientation;
  uint8_T color;
  uint8_T position;
  uint8_T valid;
};

#endif                                 /*struct_Player_tag*/

#ifndef typedef_c31_Player
#define typedef_c31_Player

typedef struct Player_tag c31_Player;

#endif                                 /*typedef_c31_Player*/

#ifndef struct_Ball_tag
#define struct_Ball_tag

struct Ball_tag
{
  int8_T x;
  int8_T y;
  uint8_T valid;
};

#endif                                 /*struct_Ball_tag*/

#ifndef typedef_c31_Ball
#define typedef_c31_Ball

typedef struct Ball_tag c31_Ball;

#endif                                 /*typedef_c31_Ball*/

#ifndef struct_Waypoint_tag
#define struct_Waypoint_tag

struct Waypoint_tag
{
  int8_T x;
  int8_T y;
  int16_T orientation;
};

#endif                                 /*struct_Waypoint_tag*/

#ifndef typedef_c31_Waypoint
#define typedef_c31_Waypoint

typedef struct Waypoint_tag c31_Waypoint;

#endif                                 /*typedef_c31_Waypoint*/

#ifndef typedef_SFc31_LessonIInstanceStruct
#define typedef_SFc31_LessonIInstanceStruct

typedef struct {
  SimStruct *S;
  ChartInfoStruct chartInfo;
  uint32_T chartNumber;
  uint32_T instanceNumber;
  int32_T c31_sfEvent;
  uint8_T c31_tp_GameIsOn_Goalie;
  uint8_T c31_tp_goalie;
  uint8_T c31_tp_Idle;
  uint8_T c31_tp_waiting;
  uint8_T c31_tp_Idle1;
  uint8_T c31_tp_goToPosition;
  boolean_T c31_isStable;
  uint8_T c31_is_active_c31_LessonI;
  uint8_T c31_is_c31_LessonI;
  uint8_T c31_is_GameIsOn_Goalie;
  uint8_T c31_is_waiting;
  int8_T c31_startingPos[2];
  boolean_T c31_dataWrittenToVector[3];
  uint8_T c31_doSetSimStateSideEffects;
  const mxArray *c31_setSimStateSideEffectsInfo;
  c31_Player *c31_me;
  c31_Player (*c31_players)[6];
  c31_Ball *c31_ball;
  c31_Waypoint *c31_finalWay;
  c31_Waypoint *c31_manualWay;
  uint8_T *c31_GameOn;
} SFc31_LessonIInstanceStruct;

#endif                                 /*typedef_SFc31_LessonIInstanceStruct*/

/* Named Constants */

/* Variable Declarations */
extern struct SfDebugInstanceStruct *sfGlobalDebugInstanceStruct;

/* Variable Definitions */

/* Function Declarations */
extern const mxArray *sf_c31_LessonI_get_eml_resolved_functions_info(void);

/* Function Definitions */
extern void sf_c31_LessonI_get_check_sum(mxArray *plhs[]);
extern void c31_LessonI_method_dispatcher(SimStruct *S, int_T method, void *data);

#endif

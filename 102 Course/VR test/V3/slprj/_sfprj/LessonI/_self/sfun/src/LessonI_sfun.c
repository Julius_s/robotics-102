/* Include files */

#include "LessonI_sfun.h"
#include "LessonI_sfun_debug_macros.h"
#include "c1_LessonI.h"
#include "c2_LessonI.h"
#include "c3_LessonI.h"
#include "c4_LessonI.h"
#include "c7_LessonI.h"
#include "c8_LessonI.h"
#include "c9_LessonI.h"
#include "c10_LessonI.h"
#include "c12_LessonI.h"
#include "c16_LessonI.h"
#include "c18_LessonI.h"
#include "c19_LessonI.h"
#include "c23_LessonI.h"
#include "c26_LessonI.h"
#include "c27_LessonI.h"
#include "c28_LessonI.h"
#include "c29_LessonI.h"
#include "c30_LessonI.h"
#include "c31_LessonI.h"
#include "c32_LessonI.h"
#include "c34_LessonI.h"
#include "c36_LessonI.h"
#include "c37_LessonI.h"
#include "c38_LessonI.h"
#include "c39_LessonI.h"
#include "c41_LessonI.h"
#include "c42_LessonI.h"
#include "c43_LessonI.h"
#include "c46_LessonI.h"
#include "c47_LessonI.h"
#include "c48_LessonI.h"
#include "c49_LessonI.h"
#include "c51_LessonI.h"
#include "c52_LessonI.h"
#include "c53_LessonI.h"
#include "c56_LessonI.h"
#include "c57_LessonI.h"
#include "c58_LessonI.h"
#include "c59_LessonI.h"
#include "c60_LessonI.h"

/* Type Definitions */

/* Named Constants */

/* Variable Declarations */

/* Variable Definitions */
uint32_T _LessonIMachineNumber_;

/* Function Declarations */

/* Function Definitions */
void LessonI_initializer(void)
{
}

void LessonI_terminator(void)
{
}

/* SFunction Glue Code */
unsigned int sf_LessonI_method_dispatcher(SimStruct *simstructPtr, unsigned int
  chartFileNumber, const char* specsCksum, int_T method, void *data)
{
  if (chartFileNumber==1) {
    c1_LessonI_method_dispatcher(simstructPtr, method, data);
    return 1;
  }

  if (chartFileNumber==2) {
    c2_LessonI_method_dispatcher(simstructPtr, method, data);
    return 1;
  }

  if (chartFileNumber==3) {
    c3_LessonI_method_dispatcher(simstructPtr, method, data);
    return 1;
  }

  if (chartFileNumber==4) {
    c4_LessonI_method_dispatcher(simstructPtr, method, data);
    return 1;
  }

  if (chartFileNumber==7) {
    c7_LessonI_method_dispatcher(simstructPtr, method, data);
    return 1;
  }

  if (chartFileNumber==8) {
    c8_LessonI_method_dispatcher(simstructPtr, method, data);
    return 1;
  }

  if (chartFileNumber==9) {
    c9_LessonI_method_dispatcher(simstructPtr, method, data);
    return 1;
  }

  if (chartFileNumber==10) {
    c10_LessonI_method_dispatcher(simstructPtr, method, data);
    return 1;
  }

  if (chartFileNumber==12) {
    c12_LessonI_method_dispatcher(simstructPtr, method, data);
    return 1;
  }

  if (chartFileNumber==16) {
    c16_LessonI_method_dispatcher(simstructPtr, method, data);
    return 1;
  }

  if (chartFileNumber==18) {
    c18_LessonI_method_dispatcher(simstructPtr, method, data);
    return 1;
  }

  if (chartFileNumber==19) {
    c19_LessonI_method_dispatcher(simstructPtr, method, data);
    return 1;
  }

  if (chartFileNumber==23) {
    c23_LessonI_method_dispatcher(simstructPtr, method, data);
    return 1;
  }

  if (chartFileNumber==26) {
    c26_LessonI_method_dispatcher(simstructPtr, method, data);
    return 1;
  }

  if (chartFileNumber==27) {
    c27_LessonI_method_dispatcher(simstructPtr, method, data);
    return 1;
  }

  if (chartFileNumber==28) {
    c28_LessonI_method_dispatcher(simstructPtr, method, data);
    return 1;
  }

  if (chartFileNumber==29) {
    c29_LessonI_method_dispatcher(simstructPtr, method, data);
    return 1;
  }

  if (chartFileNumber==30) {
    c30_LessonI_method_dispatcher(simstructPtr, method, data);
    return 1;
  }

  if (chartFileNumber==31) {
    c31_LessonI_method_dispatcher(simstructPtr, method, data);
    return 1;
  }

  if (chartFileNumber==32) {
    c32_LessonI_method_dispatcher(simstructPtr, method, data);
    return 1;
  }

  if (chartFileNumber==34) {
    c34_LessonI_method_dispatcher(simstructPtr, method, data);
    return 1;
  }

  if (chartFileNumber==36) {
    c36_LessonI_method_dispatcher(simstructPtr, method, data);
    return 1;
  }

  if (chartFileNumber==37) {
    c37_LessonI_method_dispatcher(simstructPtr, method, data);
    return 1;
  }

  if (chartFileNumber==38) {
    c38_LessonI_method_dispatcher(simstructPtr, method, data);
    return 1;
  }

  if (chartFileNumber==39) {
    c39_LessonI_method_dispatcher(simstructPtr, method, data);
    return 1;
  }

  if (chartFileNumber==41) {
    c41_LessonI_method_dispatcher(simstructPtr, method, data);
    return 1;
  }

  if (chartFileNumber==42) {
    c42_LessonI_method_dispatcher(simstructPtr, method, data);
    return 1;
  }

  if (chartFileNumber==43) {
    c43_LessonI_method_dispatcher(simstructPtr, method, data);
    return 1;
  }

  if (chartFileNumber==46) {
    c46_LessonI_method_dispatcher(simstructPtr, method, data);
    return 1;
  }

  if (chartFileNumber==47) {
    c47_LessonI_method_dispatcher(simstructPtr, method, data);
    return 1;
  }

  if (chartFileNumber==48) {
    c48_LessonI_method_dispatcher(simstructPtr, method, data);
    return 1;
  }

  if (chartFileNumber==49) {
    c49_LessonI_method_dispatcher(simstructPtr, method, data);
    return 1;
  }

  if (chartFileNumber==51) {
    c51_LessonI_method_dispatcher(simstructPtr, method, data);
    return 1;
  }

  if (chartFileNumber==52) {
    c52_LessonI_method_dispatcher(simstructPtr, method, data);
    return 1;
  }

  if (chartFileNumber==53) {
    c53_LessonI_method_dispatcher(simstructPtr, method, data);
    return 1;
  }

  if (chartFileNumber==56) {
    c56_LessonI_method_dispatcher(simstructPtr, method, data);
    return 1;
  }

  if (chartFileNumber==57) {
    c57_LessonI_method_dispatcher(simstructPtr, method, data);
    return 1;
  }

  if (chartFileNumber==58) {
    c58_LessonI_method_dispatcher(simstructPtr, method, data);
    return 1;
  }

  if (chartFileNumber==59) {
    c59_LessonI_method_dispatcher(simstructPtr, method, data);
    return 1;
  }

  if (chartFileNumber==60) {
    c60_LessonI_method_dispatcher(simstructPtr, method, data);
    return 1;
  }

  return 0;
}

extern void sf_LessonI_uses_exported_functions(int nlhs, mxArray * plhs[], int
  nrhs, const mxArray * prhs[])
{
  plhs[0] = mxCreateLogicalScalar(0);
}

unsigned int sf_LessonI_process_check_sum_call( int nlhs, mxArray * plhs[], int
  nrhs, const mxArray * prhs[] )
{

#ifdef MATLAB_MEX_FILE

  char commandName[20];
  if (nrhs<1 || !mxIsChar(prhs[0]) )
    return 0;

  /* Possible call to get the checksum */
  mxGetString(prhs[0], commandName,sizeof(commandName)/sizeof(char));
  commandName[(sizeof(commandName)/sizeof(char)-1)] = '\0';
  if (strcmp(commandName,"sf_get_check_sum"))
    return 0;
  plhs[0] = mxCreateDoubleMatrix( 1,4,mxREAL);
  if (nrhs>1 && mxIsChar(prhs[1])) {
    mxGetString(prhs[1], commandName,sizeof(commandName)/sizeof(char));
    commandName[(sizeof(commandName)/sizeof(char)-1)] = '\0';
    if (!strcmp(commandName,"machine")) {
      ((real_T *)mxGetPr((plhs[0])))[0] = (real_T)(1301896295U);
      ((real_T *)mxGetPr((plhs[0])))[1] = (real_T)(2577270405U);
      ((real_T *)mxGetPr((plhs[0])))[2] = (real_T)(2645680082U);
      ((real_T *)mxGetPr((plhs[0])))[3] = (real_T)(3165828502U);
    } else if (!strcmp(commandName,"exportedFcn")) {
      ((real_T *)mxGetPr((plhs[0])))[0] = (real_T)(0U);
      ((real_T *)mxGetPr((plhs[0])))[1] = (real_T)(0U);
      ((real_T *)mxGetPr((plhs[0])))[2] = (real_T)(0U);
      ((real_T *)mxGetPr((plhs[0])))[3] = (real_T)(0U);
    } else if (!strcmp(commandName,"makefile")) {
      ((real_T *)mxGetPr((plhs[0])))[0] = (real_T)(436368481U);
      ((real_T *)mxGetPr((plhs[0])))[1] = (real_T)(1835622134U);
      ((real_T *)mxGetPr((plhs[0])))[2] = (real_T)(2661767910U);
      ((real_T *)mxGetPr((plhs[0])))[3] = (real_T)(692450584U);
    } else if (nrhs==3 && !strcmp(commandName,"chart")) {
      unsigned int chartFileNumber;
      chartFileNumber = (unsigned int)mxGetScalar(prhs[2]);
      switch (chartFileNumber) {
       case 1:
        {
          extern void sf_c1_LessonI_get_check_sum(mxArray *plhs[]);
          sf_c1_LessonI_get_check_sum(plhs);
          break;
        }

       case 2:
        {
          extern void sf_c2_LessonI_get_check_sum(mxArray *plhs[]);
          sf_c2_LessonI_get_check_sum(plhs);
          break;
        }

       case 3:
        {
          extern void sf_c3_LessonI_get_check_sum(mxArray *plhs[]);
          sf_c3_LessonI_get_check_sum(plhs);
          break;
        }

       case 4:
        {
          extern void sf_c4_LessonI_get_check_sum(mxArray *plhs[]);
          sf_c4_LessonI_get_check_sum(plhs);
          break;
        }

       case 7:
        {
          extern void sf_c7_LessonI_get_check_sum(mxArray *plhs[]);
          sf_c7_LessonI_get_check_sum(plhs);
          break;
        }

       case 8:
        {
          extern void sf_c8_LessonI_get_check_sum(mxArray *plhs[]);
          sf_c8_LessonI_get_check_sum(plhs);
          break;
        }

       case 9:
        {
          extern void sf_c9_LessonI_get_check_sum(mxArray *plhs[]);
          sf_c9_LessonI_get_check_sum(plhs);
          break;
        }

       case 10:
        {
          extern void sf_c10_LessonI_get_check_sum(mxArray *plhs[]);
          sf_c10_LessonI_get_check_sum(plhs);
          break;
        }

       case 12:
        {
          extern void sf_c12_LessonI_get_check_sum(mxArray *plhs[]);
          sf_c12_LessonI_get_check_sum(plhs);
          break;
        }

       case 16:
        {
          extern void sf_c16_LessonI_get_check_sum(mxArray *plhs[]);
          sf_c16_LessonI_get_check_sum(plhs);
          break;
        }

       case 18:
        {
          extern void sf_c18_LessonI_get_check_sum(mxArray *plhs[]);
          sf_c18_LessonI_get_check_sum(plhs);
          break;
        }

       case 19:
        {
          extern void sf_c19_LessonI_get_check_sum(mxArray *plhs[]);
          sf_c19_LessonI_get_check_sum(plhs);
          break;
        }

       case 23:
        {
          extern void sf_c23_LessonI_get_check_sum(mxArray *plhs[]);
          sf_c23_LessonI_get_check_sum(plhs);
          break;
        }

       case 26:
        {
          extern void sf_c26_LessonI_get_check_sum(mxArray *plhs[]);
          sf_c26_LessonI_get_check_sum(plhs);
          break;
        }

       case 27:
        {
          extern void sf_c27_LessonI_get_check_sum(mxArray *plhs[]);
          sf_c27_LessonI_get_check_sum(plhs);
          break;
        }

       case 28:
        {
          extern void sf_c28_LessonI_get_check_sum(mxArray *plhs[]);
          sf_c28_LessonI_get_check_sum(plhs);
          break;
        }

       case 29:
        {
          extern void sf_c29_LessonI_get_check_sum(mxArray *plhs[]);
          sf_c29_LessonI_get_check_sum(plhs);
          break;
        }

       case 30:
        {
          extern void sf_c30_LessonI_get_check_sum(mxArray *plhs[]);
          sf_c30_LessonI_get_check_sum(plhs);
          break;
        }

       case 31:
        {
          extern void sf_c31_LessonI_get_check_sum(mxArray *plhs[]);
          sf_c31_LessonI_get_check_sum(plhs);
          break;
        }

       case 32:
        {
          extern void sf_c32_LessonI_get_check_sum(mxArray *plhs[]);
          sf_c32_LessonI_get_check_sum(plhs);
          break;
        }

       case 34:
        {
          extern void sf_c34_LessonI_get_check_sum(mxArray *plhs[]);
          sf_c34_LessonI_get_check_sum(plhs);
          break;
        }

       case 36:
        {
          extern void sf_c36_LessonI_get_check_sum(mxArray *plhs[]);
          sf_c36_LessonI_get_check_sum(plhs);
          break;
        }

       case 37:
        {
          extern void sf_c37_LessonI_get_check_sum(mxArray *plhs[]);
          sf_c37_LessonI_get_check_sum(plhs);
          break;
        }

       case 38:
        {
          extern void sf_c38_LessonI_get_check_sum(mxArray *plhs[]);
          sf_c38_LessonI_get_check_sum(plhs);
          break;
        }

       case 39:
        {
          extern void sf_c39_LessonI_get_check_sum(mxArray *plhs[]);
          sf_c39_LessonI_get_check_sum(plhs);
          break;
        }

       case 41:
        {
          extern void sf_c41_LessonI_get_check_sum(mxArray *plhs[]);
          sf_c41_LessonI_get_check_sum(plhs);
          break;
        }

       case 42:
        {
          extern void sf_c42_LessonI_get_check_sum(mxArray *plhs[]);
          sf_c42_LessonI_get_check_sum(plhs);
          break;
        }

       case 43:
        {
          extern void sf_c43_LessonI_get_check_sum(mxArray *plhs[]);
          sf_c43_LessonI_get_check_sum(plhs);
          break;
        }

       case 46:
        {
          extern void sf_c46_LessonI_get_check_sum(mxArray *plhs[]);
          sf_c46_LessonI_get_check_sum(plhs);
          break;
        }

       case 47:
        {
          extern void sf_c47_LessonI_get_check_sum(mxArray *plhs[]);
          sf_c47_LessonI_get_check_sum(plhs);
          break;
        }

       case 48:
        {
          extern void sf_c48_LessonI_get_check_sum(mxArray *plhs[]);
          sf_c48_LessonI_get_check_sum(plhs);
          break;
        }

       case 49:
        {
          extern void sf_c49_LessonI_get_check_sum(mxArray *plhs[]);
          sf_c49_LessonI_get_check_sum(plhs);
          break;
        }

       case 51:
        {
          extern void sf_c51_LessonI_get_check_sum(mxArray *plhs[]);
          sf_c51_LessonI_get_check_sum(plhs);
          break;
        }

       case 52:
        {
          extern void sf_c52_LessonI_get_check_sum(mxArray *plhs[]);
          sf_c52_LessonI_get_check_sum(plhs);
          break;
        }

       case 53:
        {
          extern void sf_c53_LessonI_get_check_sum(mxArray *plhs[]);
          sf_c53_LessonI_get_check_sum(plhs);
          break;
        }

       case 56:
        {
          extern void sf_c56_LessonI_get_check_sum(mxArray *plhs[]);
          sf_c56_LessonI_get_check_sum(plhs);
          break;
        }

       case 57:
        {
          extern void sf_c57_LessonI_get_check_sum(mxArray *plhs[]);
          sf_c57_LessonI_get_check_sum(plhs);
          break;
        }

       case 58:
        {
          extern void sf_c58_LessonI_get_check_sum(mxArray *plhs[]);
          sf_c58_LessonI_get_check_sum(plhs);
          break;
        }

       case 59:
        {
          extern void sf_c59_LessonI_get_check_sum(mxArray *plhs[]);
          sf_c59_LessonI_get_check_sum(plhs);
          break;
        }

       case 60:
        {
          extern void sf_c60_LessonI_get_check_sum(mxArray *plhs[]);
          sf_c60_LessonI_get_check_sum(plhs);
          break;
        }

       default:
        ((real_T *)mxGetPr((plhs[0])))[0] = (real_T)(0.0);
        ((real_T *)mxGetPr((plhs[0])))[1] = (real_T)(0.0);
        ((real_T *)mxGetPr((plhs[0])))[2] = (real_T)(0.0);
        ((real_T *)mxGetPr((plhs[0])))[3] = (real_T)(0.0);
      }
    } else if (!strcmp(commandName,"target")) {
      ((real_T *)mxGetPr((plhs[0])))[0] = (real_T)(3061339410U);
      ((real_T *)mxGetPr((plhs[0])))[1] = (real_T)(1991824845U);
      ((real_T *)mxGetPr((plhs[0])))[2] = (real_T)(3599338742U);
      ((real_T *)mxGetPr((plhs[0])))[3] = (real_T)(2357874978U);
    } else {
      return 0;
    }
  } else {
    ((real_T *)mxGetPr((plhs[0])))[0] = (real_T)(1503488826U);
    ((real_T *)mxGetPr((plhs[0])))[1] = (real_T)(4212236801U);
    ((real_T *)mxGetPr((plhs[0])))[2] = (real_T)(1679139204U);
    ((real_T *)mxGetPr((plhs[0])))[3] = (real_T)(3034793280U);
  }

  return 1;

#else

  return 0;

#endif

}

unsigned int sf_LessonI_autoinheritance_info( int nlhs, mxArray * plhs[], int
  nrhs, const mxArray * prhs[] )
{

#ifdef MATLAB_MEX_FILE

  char commandName[32];
  char aiChksum[64];
  if (nrhs<3 || !mxIsChar(prhs[0]) )
    return 0;

  /* Possible call to get the autoinheritance_info */
  mxGetString(prhs[0], commandName,sizeof(commandName)/sizeof(char));
  commandName[(sizeof(commandName)/sizeof(char)-1)] = '\0';
  if (strcmp(commandName,"get_autoinheritance_info"))
    return 0;
  mxGetString(prhs[2], aiChksum,sizeof(aiChksum)/sizeof(char));
  aiChksum[(sizeof(aiChksum)/sizeof(char)-1)] = '\0';

  {
    unsigned int chartFileNumber;
    chartFileNumber = (unsigned int)mxGetScalar(prhs[1]);
    switch (chartFileNumber) {
     case 1:
      {
        if (strcmp(aiChksum, "y6Xs4WcS8WLRGoiPgg9VIE") == 0) {
          extern mxArray *sf_c1_LessonI_get_autoinheritance_info(void);
          plhs[0] = sf_c1_LessonI_get_autoinheritance_info();
          break;
        }

        plhs[0] = mxCreateDoubleMatrix(0,0,mxREAL);
        break;
      }

     case 2:
      {
        if (strcmp(aiChksum, "kHfYduJyIVyU5ctHVH1c5") == 0) {
          extern mxArray *sf_c2_LessonI_get_autoinheritance_info(void);
          plhs[0] = sf_c2_LessonI_get_autoinheritance_info();
          break;
        }

        plhs[0] = mxCreateDoubleMatrix(0,0,mxREAL);
        break;
      }

     case 3:
      {
        if (strcmp(aiChksum, "UdXCtx3vKeGTbSGWNIJsGB") == 0) {
          extern mxArray *sf_c3_LessonI_get_autoinheritance_info(void);
          plhs[0] = sf_c3_LessonI_get_autoinheritance_info();
          break;
        }

        plhs[0] = mxCreateDoubleMatrix(0,0,mxREAL);
        break;
      }

     case 4:
      {
        if (strcmp(aiChksum, "B96iBpHx3shZ3np6OuLbm") == 0) {
          extern mxArray *sf_c4_LessonI_get_autoinheritance_info(void);
          plhs[0] = sf_c4_LessonI_get_autoinheritance_info();
          break;
        }

        plhs[0] = mxCreateDoubleMatrix(0,0,mxREAL);
        break;
      }

     case 7:
      {
        if (strcmp(aiChksum, "YxuJ3VIU6BKAvA58gYIRPD") == 0) {
          extern mxArray *sf_c7_LessonI_get_autoinheritance_info(void);
          plhs[0] = sf_c7_LessonI_get_autoinheritance_info();
          break;
        }

        plhs[0] = mxCreateDoubleMatrix(0,0,mxREAL);
        break;
      }

     case 8:
      {
        if (strcmp(aiChksum, "XEXsRmqFscIqZkSUMcSdQF") == 0) {
          extern mxArray *sf_c8_LessonI_get_autoinheritance_info(void);
          plhs[0] = sf_c8_LessonI_get_autoinheritance_info();
          break;
        }

        plhs[0] = mxCreateDoubleMatrix(0,0,mxREAL);
        break;
      }

     case 9:
      {
        if (strcmp(aiChksum, "ArIrdR8KduvZC6FfJiWpp") == 0) {
          extern mxArray *sf_c9_LessonI_get_autoinheritance_info(void);
          plhs[0] = sf_c9_LessonI_get_autoinheritance_info();
          break;
        }

        plhs[0] = mxCreateDoubleMatrix(0,0,mxREAL);
        break;
      }

     case 10:
      {
        if (strcmp(aiChksum, "5GCUruyFPNfSoASjim60bB") == 0) {
          extern mxArray *sf_c10_LessonI_get_autoinheritance_info(void);
          plhs[0] = sf_c10_LessonI_get_autoinheritance_info();
          break;
        }

        plhs[0] = mxCreateDoubleMatrix(0,0,mxREAL);
        break;
      }

     case 12:
      {
        if (strcmp(aiChksum, "kp5o8oGyeKrNrmZ45YqG3D") == 0) {
          extern mxArray *sf_c12_LessonI_get_autoinheritance_info(void);
          plhs[0] = sf_c12_LessonI_get_autoinheritance_info();
          break;
        }

        plhs[0] = mxCreateDoubleMatrix(0,0,mxREAL);
        break;
      }

     case 16:
      {
        if (strcmp(aiChksum, "avMqIanoQY6XoECCReQu1D") == 0) {
          extern mxArray *sf_c16_LessonI_get_autoinheritance_info(void);
          plhs[0] = sf_c16_LessonI_get_autoinheritance_info();
          break;
        }

        plhs[0] = mxCreateDoubleMatrix(0,0,mxREAL);
        break;
      }

     case 18:
      {
        if (strcmp(aiChksum, "7uso0gZBLh79xCuiuG5PtG") == 0) {
          extern mxArray *sf_c18_LessonI_get_autoinheritance_info(void);
          plhs[0] = sf_c18_LessonI_get_autoinheritance_info();
          break;
        }

        plhs[0] = mxCreateDoubleMatrix(0,0,mxREAL);
        break;
      }

     case 19:
      {
        if (strcmp(aiChksum, "avMqIanoQY6XoECCReQu1D") == 0) {
          extern mxArray *sf_c19_LessonI_get_autoinheritance_info(void);
          plhs[0] = sf_c19_LessonI_get_autoinheritance_info();
          break;
        }

        plhs[0] = mxCreateDoubleMatrix(0,0,mxREAL);
        break;
      }

     case 23:
      {
        if (strcmp(aiChksum, "YxuJ3VIU6BKAvA58gYIRPD") == 0) {
          extern mxArray *sf_c23_LessonI_get_autoinheritance_info(void);
          plhs[0] = sf_c23_LessonI_get_autoinheritance_info();
          break;
        }

        plhs[0] = mxCreateDoubleMatrix(0,0,mxREAL);
        break;
      }

     case 26:
      {
        if (strcmp(aiChksum, "2z7Fwh6WZoLlXSSoEUax0D") == 0) {
          extern mxArray *sf_c26_LessonI_get_autoinheritance_info(void);
          plhs[0] = sf_c26_LessonI_get_autoinheritance_info();
          break;
        }

        plhs[0] = mxCreateDoubleMatrix(0,0,mxREAL);
        break;
      }

     case 27:
      {
        if (strcmp(aiChksum, "7ivw4cCNB5MBh41uVb3SPB") == 0) {
          extern mxArray *sf_c27_LessonI_get_autoinheritance_info(void);
          plhs[0] = sf_c27_LessonI_get_autoinheritance_info();
          break;
        }

        plhs[0] = mxCreateDoubleMatrix(0,0,mxREAL);
        break;
      }

     case 28:
      {
        if (strcmp(aiChksum, "aeIe7Vb6eDndSoISjztQnH") == 0) {
          extern mxArray *sf_c28_LessonI_get_autoinheritance_info(void);
          plhs[0] = sf_c28_LessonI_get_autoinheritance_info();
          break;
        }

        plhs[0] = mxCreateDoubleMatrix(0,0,mxREAL);
        break;
      }

     case 29:
      {
        if (strcmp(aiChksum, "xTDZbH0g8R2TeJ7jn9aNrF") == 0) {
          extern mxArray *sf_c29_LessonI_get_autoinheritance_info(void);
          plhs[0] = sf_c29_LessonI_get_autoinheritance_info();
          break;
        }

        plhs[0] = mxCreateDoubleMatrix(0,0,mxREAL);
        break;
      }

     case 30:
      {
        if (strcmp(aiChksum, "VqNKr9ToCKKeFTkP3bLInG") == 0) {
          extern mxArray *sf_c30_LessonI_get_autoinheritance_info(void);
          plhs[0] = sf_c30_LessonI_get_autoinheritance_info();
          break;
        }

        plhs[0] = mxCreateDoubleMatrix(0,0,mxREAL);
        break;
      }

     case 31:
      {
        if (strcmp(aiChksum, "ekiGpsH5X28KKFyXr6UOfH") == 0) {
          extern mxArray *sf_c31_LessonI_get_autoinheritance_info(void);
          plhs[0] = sf_c31_LessonI_get_autoinheritance_info();
          break;
        }

        plhs[0] = mxCreateDoubleMatrix(0,0,mxREAL);
        break;
      }

     case 32:
      {
        if (strcmp(aiChksum, "YxuJ3VIU6BKAvA58gYIRPD") == 0) {
          extern mxArray *sf_c32_LessonI_get_autoinheritance_info(void);
          plhs[0] = sf_c32_LessonI_get_autoinheritance_info();
          break;
        }

        plhs[0] = mxCreateDoubleMatrix(0,0,mxREAL);
        break;
      }

     case 34:
      {
        if (strcmp(aiChksum, "2z7Fwh6WZoLlXSSoEUax0D") == 0) {
          extern mxArray *sf_c34_LessonI_get_autoinheritance_info(void);
          plhs[0] = sf_c34_LessonI_get_autoinheritance_info();
          break;
        }

        plhs[0] = mxCreateDoubleMatrix(0,0,mxREAL);
        break;
      }

     case 36:
      {
        if (strcmp(aiChksum, "2z7Fwh6WZoLlXSSoEUax0D") == 0) {
          extern mxArray *sf_c36_LessonI_get_autoinheritance_info(void);
          plhs[0] = sf_c36_LessonI_get_autoinheritance_info();
          break;
        }

        plhs[0] = mxCreateDoubleMatrix(0,0,mxREAL);
        break;
      }

     case 37:
      {
        if (strcmp(aiChksum, "7ivw4cCNB5MBh41uVb3SPB") == 0) {
          extern mxArray *sf_c37_LessonI_get_autoinheritance_info(void);
          plhs[0] = sf_c37_LessonI_get_autoinheritance_info();
          break;
        }

        plhs[0] = mxCreateDoubleMatrix(0,0,mxREAL);
        break;
      }

     case 38:
      {
        if (strcmp(aiChksum, "aeIe7Vb6eDndSoISjztQnH") == 0) {
          extern mxArray *sf_c38_LessonI_get_autoinheritance_info(void);
          plhs[0] = sf_c38_LessonI_get_autoinheritance_info();
          break;
        }

        plhs[0] = mxCreateDoubleMatrix(0,0,mxREAL);
        break;
      }

     case 39:
      {
        if (strcmp(aiChksum, "xTDZbH0g8R2TeJ7jn9aNrF") == 0) {
          extern mxArray *sf_c39_LessonI_get_autoinheritance_info(void);
          plhs[0] = sf_c39_LessonI_get_autoinheritance_info();
          break;
        }

        plhs[0] = mxCreateDoubleMatrix(0,0,mxREAL);
        break;
      }

     case 41:
      {
        if (strcmp(aiChksum, "avMqIanoQY6XoECCReQu1D") == 0) {
          extern mxArray *sf_c41_LessonI_get_autoinheritance_info(void);
          plhs[0] = sf_c41_LessonI_get_autoinheritance_info();
          break;
        }

        plhs[0] = mxCreateDoubleMatrix(0,0,mxREAL);
        break;
      }

     case 42:
      {
        if (strcmp(aiChksum, "ho0uzvNbzXohUNGgcZMDQ") == 0) {
          extern mxArray *sf_c42_LessonI_get_autoinheritance_info(void);
          plhs[0] = sf_c42_LessonI_get_autoinheritance_info();
          break;
        }

        plhs[0] = mxCreateDoubleMatrix(0,0,mxREAL);
        break;
      }

     case 43:
      {
        if (strcmp(aiChksum, "YxuJ3VIU6BKAvA58gYIRPD") == 0) {
          extern mxArray *sf_c43_LessonI_get_autoinheritance_info(void);
          plhs[0] = sf_c43_LessonI_get_autoinheritance_info();
          break;
        }

        plhs[0] = mxCreateDoubleMatrix(0,0,mxREAL);
        break;
      }

     case 46:
      {
        if (strcmp(aiChksum, "2z7Fwh6WZoLlXSSoEUax0D") == 0) {
          extern mxArray *sf_c46_LessonI_get_autoinheritance_info(void);
          plhs[0] = sf_c46_LessonI_get_autoinheritance_info();
          break;
        }

        plhs[0] = mxCreateDoubleMatrix(0,0,mxREAL);
        break;
      }

     case 47:
      {
        if (strcmp(aiChksum, "7ivw4cCNB5MBh41uVb3SPB") == 0) {
          extern mxArray *sf_c47_LessonI_get_autoinheritance_info(void);
          plhs[0] = sf_c47_LessonI_get_autoinheritance_info();
          break;
        }

        plhs[0] = mxCreateDoubleMatrix(0,0,mxREAL);
        break;
      }

     case 48:
      {
        if (strcmp(aiChksum, "aeIe7Vb6eDndSoISjztQnH") == 0) {
          extern mxArray *sf_c48_LessonI_get_autoinheritance_info(void);
          plhs[0] = sf_c48_LessonI_get_autoinheritance_info();
          break;
        }

        plhs[0] = mxCreateDoubleMatrix(0,0,mxREAL);
        break;
      }

     case 49:
      {
        if (strcmp(aiChksum, "xTDZbH0g8R2TeJ7jn9aNrF") == 0) {
          extern mxArray *sf_c49_LessonI_get_autoinheritance_info(void);
          plhs[0] = sf_c49_LessonI_get_autoinheritance_info();
          break;
        }

        plhs[0] = mxCreateDoubleMatrix(0,0,mxREAL);
        break;
      }

     case 51:
      {
        if (strcmp(aiChksum, "avMqIanoQY6XoECCReQu1D") == 0) {
          extern mxArray *sf_c51_LessonI_get_autoinheritance_info(void);
          plhs[0] = sf_c51_LessonI_get_autoinheritance_info();
          break;
        }

        plhs[0] = mxCreateDoubleMatrix(0,0,mxREAL);
        break;
      }

     case 52:
      {
        if (strcmp(aiChksum, "ho0uzvNbzXohUNGgcZMDQ") == 0) {
          extern mxArray *sf_c52_LessonI_get_autoinheritance_info(void);
          plhs[0] = sf_c52_LessonI_get_autoinheritance_info();
          break;
        }

        plhs[0] = mxCreateDoubleMatrix(0,0,mxREAL);
        break;
      }

     case 53:
      {
        if (strcmp(aiChksum, "YxuJ3VIU6BKAvA58gYIRPD") == 0) {
          extern mxArray *sf_c53_LessonI_get_autoinheritance_info(void);
          plhs[0] = sf_c53_LessonI_get_autoinheritance_info();
          break;
        }

        plhs[0] = mxCreateDoubleMatrix(0,0,mxREAL);
        break;
      }

     case 56:
      {
        if (strcmp(aiChksum, "2z7Fwh6WZoLlXSSoEUax0D") == 0) {
          extern mxArray *sf_c56_LessonI_get_autoinheritance_info(void);
          plhs[0] = sf_c56_LessonI_get_autoinheritance_info();
          break;
        }

        plhs[0] = mxCreateDoubleMatrix(0,0,mxREAL);
        break;
      }

     case 57:
      {
        if (strcmp(aiChksum, "7ivw4cCNB5MBh41uVb3SPB") == 0) {
          extern mxArray *sf_c57_LessonI_get_autoinheritance_info(void);
          plhs[0] = sf_c57_LessonI_get_autoinheritance_info();
          break;
        }

        plhs[0] = mxCreateDoubleMatrix(0,0,mxREAL);
        break;
      }

     case 58:
      {
        if (strcmp(aiChksum, "aeIe7Vb6eDndSoISjztQnH") == 0) {
          extern mxArray *sf_c58_LessonI_get_autoinheritance_info(void);
          plhs[0] = sf_c58_LessonI_get_autoinheritance_info();
          break;
        }

        plhs[0] = mxCreateDoubleMatrix(0,0,mxREAL);
        break;
      }

     case 59:
      {
        if (strcmp(aiChksum, "xTDZbH0g8R2TeJ7jn9aNrF") == 0) {
          extern mxArray *sf_c59_LessonI_get_autoinheritance_info(void);
          plhs[0] = sf_c59_LessonI_get_autoinheritance_info();
          break;
        }

        plhs[0] = mxCreateDoubleMatrix(0,0,mxREAL);
        break;
      }

     case 60:
      {
        if (strcmp(aiChksum, "piRjQqKoVYuQZLYYx7d1EC") == 0) {
          extern mxArray *sf_c60_LessonI_get_autoinheritance_info(void);
          plhs[0] = sf_c60_LessonI_get_autoinheritance_info();
          break;
        }

        plhs[0] = mxCreateDoubleMatrix(0,0,mxREAL);
        break;
      }

     default:
      plhs[0] = mxCreateDoubleMatrix(0,0,mxREAL);
    }
  }

  return 1;

#else

  return 0;

#endif

}

unsigned int sf_LessonI_get_eml_resolved_functions_info( int nlhs, mxArray *
  plhs[], int nrhs, const mxArray * prhs[] )
{

#ifdef MATLAB_MEX_FILE

  char commandName[64];
  if (nrhs<2 || !mxIsChar(prhs[0]))
    return 0;

  /* Possible call to get the get_eml_resolved_functions_info */
  mxGetString(prhs[0], commandName,sizeof(commandName)/sizeof(char));
  commandName[(sizeof(commandName)/sizeof(char)-1)] = '\0';
  if (strcmp(commandName,"get_eml_resolved_functions_info"))
    return 0;

  {
    unsigned int chartFileNumber;
    chartFileNumber = (unsigned int)mxGetScalar(prhs[1]);
    switch (chartFileNumber) {
     case 1:
      {
        extern const mxArray *sf_c1_LessonI_get_eml_resolved_functions_info(void);
        mxArray *persistentMxArray = (mxArray *)
          sf_c1_LessonI_get_eml_resolved_functions_info();
        plhs[0] = mxDuplicateArray(persistentMxArray);
        mxDestroyArray(persistentMxArray);
        break;
      }

     case 2:
      {
        extern const mxArray *sf_c2_LessonI_get_eml_resolved_functions_info(void);
        mxArray *persistentMxArray = (mxArray *)
          sf_c2_LessonI_get_eml_resolved_functions_info();
        plhs[0] = mxDuplicateArray(persistentMxArray);
        mxDestroyArray(persistentMxArray);
        break;
      }

     case 3:
      {
        extern const mxArray *sf_c3_LessonI_get_eml_resolved_functions_info(void);
        mxArray *persistentMxArray = (mxArray *)
          sf_c3_LessonI_get_eml_resolved_functions_info();
        plhs[0] = mxDuplicateArray(persistentMxArray);
        mxDestroyArray(persistentMxArray);
        break;
      }

     case 4:
      {
        extern const mxArray *sf_c4_LessonI_get_eml_resolved_functions_info(void);
        mxArray *persistentMxArray = (mxArray *)
          sf_c4_LessonI_get_eml_resolved_functions_info();
        plhs[0] = mxDuplicateArray(persistentMxArray);
        mxDestroyArray(persistentMxArray);
        break;
      }

     case 7:
      {
        extern const mxArray *sf_c7_LessonI_get_eml_resolved_functions_info(void);
        mxArray *persistentMxArray = (mxArray *)
          sf_c7_LessonI_get_eml_resolved_functions_info();
        plhs[0] = mxDuplicateArray(persistentMxArray);
        mxDestroyArray(persistentMxArray);
        break;
      }

     case 8:
      {
        extern const mxArray *sf_c8_LessonI_get_eml_resolved_functions_info(void);
        mxArray *persistentMxArray = (mxArray *)
          sf_c8_LessonI_get_eml_resolved_functions_info();
        plhs[0] = mxDuplicateArray(persistentMxArray);
        mxDestroyArray(persistentMxArray);
        break;
      }

     case 9:
      {
        extern const mxArray *sf_c9_LessonI_get_eml_resolved_functions_info(void);
        mxArray *persistentMxArray = (mxArray *)
          sf_c9_LessonI_get_eml_resolved_functions_info();
        plhs[0] = mxDuplicateArray(persistentMxArray);
        mxDestroyArray(persistentMxArray);
        break;
      }

     case 10:
      {
        extern const mxArray *sf_c10_LessonI_get_eml_resolved_functions_info
          (void);
        mxArray *persistentMxArray = (mxArray *)
          sf_c10_LessonI_get_eml_resolved_functions_info();
        plhs[0] = mxDuplicateArray(persistentMxArray);
        mxDestroyArray(persistentMxArray);
        break;
      }

     case 12:
      {
        extern const mxArray *sf_c12_LessonI_get_eml_resolved_functions_info
          (void);
        mxArray *persistentMxArray = (mxArray *)
          sf_c12_LessonI_get_eml_resolved_functions_info();
        plhs[0] = mxDuplicateArray(persistentMxArray);
        mxDestroyArray(persistentMxArray);
        break;
      }

     case 16:
      {
        extern const mxArray *sf_c16_LessonI_get_eml_resolved_functions_info
          (void);
        mxArray *persistentMxArray = (mxArray *)
          sf_c16_LessonI_get_eml_resolved_functions_info();
        plhs[0] = mxDuplicateArray(persistentMxArray);
        mxDestroyArray(persistentMxArray);
        break;
      }

     case 18:
      {
        extern const mxArray *sf_c18_LessonI_get_eml_resolved_functions_info
          (void);
        mxArray *persistentMxArray = (mxArray *)
          sf_c18_LessonI_get_eml_resolved_functions_info();
        plhs[0] = mxDuplicateArray(persistentMxArray);
        mxDestroyArray(persistentMxArray);
        break;
      }

     case 19:
      {
        extern const mxArray *sf_c19_LessonI_get_eml_resolved_functions_info
          (void);
        mxArray *persistentMxArray = (mxArray *)
          sf_c19_LessonI_get_eml_resolved_functions_info();
        plhs[0] = mxDuplicateArray(persistentMxArray);
        mxDestroyArray(persistentMxArray);
        break;
      }

     case 23:
      {
        extern const mxArray *sf_c23_LessonI_get_eml_resolved_functions_info
          (void);
        mxArray *persistentMxArray = (mxArray *)
          sf_c23_LessonI_get_eml_resolved_functions_info();
        plhs[0] = mxDuplicateArray(persistentMxArray);
        mxDestroyArray(persistentMxArray);
        break;
      }

     case 26:
      {
        extern const mxArray *sf_c26_LessonI_get_eml_resolved_functions_info
          (void);
        mxArray *persistentMxArray = (mxArray *)
          sf_c26_LessonI_get_eml_resolved_functions_info();
        plhs[0] = mxDuplicateArray(persistentMxArray);
        mxDestroyArray(persistentMxArray);
        break;
      }

     case 27:
      {
        extern const mxArray *sf_c27_LessonI_get_eml_resolved_functions_info
          (void);
        mxArray *persistentMxArray = (mxArray *)
          sf_c27_LessonI_get_eml_resolved_functions_info();
        plhs[0] = mxDuplicateArray(persistentMxArray);
        mxDestroyArray(persistentMxArray);
        break;
      }

     case 28:
      {
        extern const mxArray *sf_c28_LessonI_get_eml_resolved_functions_info
          (void);
        mxArray *persistentMxArray = (mxArray *)
          sf_c28_LessonI_get_eml_resolved_functions_info();
        plhs[0] = mxDuplicateArray(persistentMxArray);
        mxDestroyArray(persistentMxArray);
        break;
      }

     case 29:
      {
        extern const mxArray *sf_c29_LessonI_get_eml_resolved_functions_info
          (void);
        mxArray *persistentMxArray = (mxArray *)
          sf_c29_LessonI_get_eml_resolved_functions_info();
        plhs[0] = mxDuplicateArray(persistentMxArray);
        mxDestroyArray(persistentMxArray);
        break;
      }

     case 30:
      {
        extern const mxArray *sf_c30_LessonI_get_eml_resolved_functions_info
          (void);
        mxArray *persistentMxArray = (mxArray *)
          sf_c30_LessonI_get_eml_resolved_functions_info();
        plhs[0] = mxDuplicateArray(persistentMxArray);
        mxDestroyArray(persistentMxArray);
        break;
      }

     case 31:
      {
        extern const mxArray *sf_c31_LessonI_get_eml_resolved_functions_info
          (void);
        mxArray *persistentMxArray = (mxArray *)
          sf_c31_LessonI_get_eml_resolved_functions_info();
        plhs[0] = mxDuplicateArray(persistentMxArray);
        mxDestroyArray(persistentMxArray);
        break;
      }

     case 32:
      {
        extern const mxArray *sf_c32_LessonI_get_eml_resolved_functions_info
          (void);
        mxArray *persistentMxArray = (mxArray *)
          sf_c32_LessonI_get_eml_resolved_functions_info();
        plhs[0] = mxDuplicateArray(persistentMxArray);
        mxDestroyArray(persistentMxArray);
        break;
      }

     case 34:
      {
        extern const mxArray *sf_c34_LessonI_get_eml_resolved_functions_info
          (void);
        mxArray *persistentMxArray = (mxArray *)
          sf_c34_LessonI_get_eml_resolved_functions_info();
        plhs[0] = mxDuplicateArray(persistentMxArray);
        mxDestroyArray(persistentMxArray);
        break;
      }

     case 36:
      {
        extern const mxArray *sf_c36_LessonI_get_eml_resolved_functions_info
          (void);
        mxArray *persistentMxArray = (mxArray *)
          sf_c36_LessonI_get_eml_resolved_functions_info();
        plhs[0] = mxDuplicateArray(persistentMxArray);
        mxDestroyArray(persistentMxArray);
        break;
      }

     case 37:
      {
        extern const mxArray *sf_c37_LessonI_get_eml_resolved_functions_info
          (void);
        mxArray *persistentMxArray = (mxArray *)
          sf_c37_LessonI_get_eml_resolved_functions_info();
        plhs[0] = mxDuplicateArray(persistentMxArray);
        mxDestroyArray(persistentMxArray);
        break;
      }

     case 38:
      {
        extern const mxArray *sf_c38_LessonI_get_eml_resolved_functions_info
          (void);
        mxArray *persistentMxArray = (mxArray *)
          sf_c38_LessonI_get_eml_resolved_functions_info();
        plhs[0] = mxDuplicateArray(persistentMxArray);
        mxDestroyArray(persistentMxArray);
        break;
      }

     case 39:
      {
        extern const mxArray *sf_c39_LessonI_get_eml_resolved_functions_info
          (void);
        mxArray *persistentMxArray = (mxArray *)
          sf_c39_LessonI_get_eml_resolved_functions_info();
        plhs[0] = mxDuplicateArray(persistentMxArray);
        mxDestroyArray(persistentMxArray);
        break;
      }

     case 41:
      {
        extern const mxArray *sf_c41_LessonI_get_eml_resolved_functions_info
          (void);
        mxArray *persistentMxArray = (mxArray *)
          sf_c41_LessonI_get_eml_resolved_functions_info();
        plhs[0] = mxDuplicateArray(persistentMxArray);
        mxDestroyArray(persistentMxArray);
        break;
      }

     case 42:
      {
        extern const mxArray *sf_c42_LessonI_get_eml_resolved_functions_info
          (void);
        mxArray *persistentMxArray = (mxArray *)
          sf_c42_LessonI_get_eml_resolved_functions_info();
        plhs[0] = mxDuplicateArray(persistentMxArray);
        mxDestroyArray(persistentMxArray);
        break;
      }

     case 43:
      {
        extern const mxArray *sf_c43_LessonI_get_eml_resolved_functions_info
          (void);
        mxArray *persistentMxArray = (mxArray *)
          sf_c43_LessonI_get_eml_resolved_functions_info();
        plhs[0] = mxDuplicateArray(persistentMxArray);
        mxDestroyArray(persistentMxArray);
        break;
      }

     case 46:
      {
        extern const mxArray *sf_c46_LessonI_get_eml_resolved_functions_info
          (void);
        mxArray *persistentMxArray = (mxArray *)
          sf_c46_LessonI_get_eml_resolved_functions_info();
        plhs[0] = mxDuplicateArray(persistentMxArray);
        mxDestroyArray(persistentMxArray);
        break;
      }

     case 47:
      {
        extern const mxArray *sf_c47_LessonI_get_eml_resolved_functions_info
          (void);
        mxArray *persistentMxArray = (mxArray *)
          sf_c47_LessonI_get_eml_resolved_functions_info();
        plhs[0] = mxDuplicateArray(persistentMxArray);
        mxDestroyArray(persistentMxArray);
        break;
      }

     case 48:
      {
        extern const mxArray *sf_c48_LessonI_get_eml_resolved_functions_info
          (void);
        mxArray *persistentMxArray = (mxArray *)
          sf_c48_LessonI_get_eml_resolved_functions_info();
        plhs[0] = mxDuplicateArray(persistentMxArray);
        mxDestroyArray(persistentMxArray);
        break;
      }

     case 49:
      {
        extern const mxArray *sf_c49_LessonI_get_eml_resolved_functions_info
          (void);
        mxArray *persistentMxArray = (mxArray *)
          sf_c49_LessonI_get_eml_resolved_functions_info();
        plhs[0] = mxDuplicateArray(persistentMxArray);
        mxDestroyArray(persistentMxArray);
        break;
      }

     case 51:
      {
        extern const mxArray *sf_c51_LessonI_get_eml_resolved_functions_info
          (void);
        mxArray *persistentMxArray = (mxArray *)
          sf_c51_LessonI_get_eml_resolved_functions_info();
        plhs[0] = mxDuplicateArray(persistentMxArray);
        mxDestroyArray(persistentMxArray);
        break;
      }

     case 52:
      {
        extern const mxArray *sf_c52_LessonI_get_eml_resolved_functions_info
          (void);
        mxArray *persistentMxArray = (mxArray *)
          sf_c52_LessonI_get_eml_resolved_functions_info();
        plhs[0] = mxDuplicateArray(persistentMxArray);
        mxDestroyArray(persistentMxArray);
        break;
      }

     case 53:
      {
        extern const mxArray *sf_c53_LessonI_get_eml_resolved_functions_info
          (void);
        mxArray *persistentMxArray = (mxArray *)
          sf_c53_LessonI_get_eml_resolved_functions_info();
        plhs[0] = mxDuplicateArray(persistentMxArray);
        mxDestroyArray(persistentMxArray);
        break;
      }

     case 56:
      {
        extern const mxArray *sf_c56_LessonI_get_eml_resolved_functions_info
          (void);
        mxArray *persistentMxArray = (mxArray *)
          sf_c56_LessonI_get_eml_resolved_functions_info();
        plhs[0] = mxDuplicateArray(persistentMxArray);
        mxDestroyArray(persistentMxArray);
        break;
      }

     case 57:
      {
        extern const mxArray *sf_c57_LessonI_get_eml_resolved_functions_info
          (void);
        mxArray *persistentMxArray = (mxArray *)
          sf_c57_LessonI_get_eml_resolved_functions_info();
        plhs[0] = mxDuplicateArray(persistentMxArray);
        mxDestroyArray(persistentMxArray);
        break;
      }

     case 58:
      {
        extern const mxArray *sf_c58_LessonI_get_eml_resolved_functions_info
          (void);
        mxArray *persistentMxArray = (mxArray *)
          sf_c58_LessonI_get_eml_resolved_functions_info();
        plhs[0] = mxDuplicateArray(persistentMxArray);
        mxDestroyArray(persistentMxArray);
        break;
      }

     case 59:
      {
        extern const mxArray *sf_c59_LessonI_get_eml_resolved_functions_info
          (void);
        mxArray *persistentMxArray = (mxArray *)
          sf_c59_LessonI_get_eml_resolved_functions_info();
        plhs[0] = mxDuplicateArray(persistentMxArray);
        mxDestroyArray(persistentMxArray);
        break;
      }

     case 60:
      {
        extern const mxArray *sf_c60_LessonI_get_eml_resolved_functions_info
          (void);
        mxArray *persistentMxArray = (mxArray *)
          sf_c60_LessonI_get_eml_resolved_functions_info();
        plhs[0] = mxDuplicateArray(persistentMxArray);
        mxDestroyArray(persistentMxArray);
        break;
      }

     default:
      plhs[0] = mxCreateDoubleMatrix(0,0,mxREAL);
    }
  }

  return 1;

#else

  return 0;

#endif

}

unsigned int sf_LessonI_third_party_uses_info( int nlhs, mxArray * plhs[], int
  nrhs, const mxArray * prhs[] )
{
  char commandName[64];
  char tpChksum[64];
  if (nrhs<3 || !mxIsChar(prhs[0]))
    return 0;

  /* Possible call to get the third_party_uses_info */
  mxGetString(prhs[0], commandName,sizeof(commandName)/sizeof(char));
  commandName[(sizeof(commandName)/sizeof(char)-1)] = '\0';
  mxGetString(prhs[2], tpChksum,sizeof(tpChksum)/sizeof(char));
  tpChksum[(sizeof(tpChksum)/sizeof(char)-1)] = '\0';
  if (strcmp(commandName,"get_third_party_uses_info"))
    return 0;

  {
    unsigned int chartFileNumber;
    chartFileNumber = (unsigned int)mxGetScalar(prhs[1]);
    switch (chartFileNumber) {
     case 1:
      {
        if (strcmp(tpChksum, "77qs0O1aFQP9R5KVWKglD") == 0) {
          extern mxArray *sf_c1_LessonI_third_party_uses_info(void);
          plhs[0] = sf_c1_LessonI_third_party_uses_info();
          break;
        }
      }

     case 2:
      {
        if (strcmp(tpChksum, "BPbr6mnccR4iiqbbOejEIC") == 0) {
          extern mxArray *sf_c2_LessonI_third_party_uses_info(void);
          plhs[0] = sf_c2_LessonI_third_party_uses_info();
          break;
        }
      }

     case 3:
      {
        if (strcmp(tpChksum, "gblOQFxJdbh8vdZcLHybtE") == 0) {
          extern mxArray *sf_c3_LessonI_third_party_uses_info(void);
          plhs[0] = sf_c3_LessonI_third_party_uses_info();
          break;
        }
      }

     case 4:
      {
        if (strcmp(tpChksum, "9dC2ipLYw0brxEDvXnTWxC") == 0) {
          extern mxArray *sf_c4_LessonI_third_party_uses_info(void);
          plhs[0] = sf_c4_LessonI_third_party_uses_info();
          break;
        }
      }

     case 7:
      {
        if (strcmp(tpChksum, "gkxnlDDaCol3tTf1u27nYB") == 0) {
          extern mxArray *sf_c7_LessonI_third_party_uses_info(void);
          plhs[0] = sf_c7_LessonI_third_party_uses_info();
          break;
        }
      }

     case 8:
      {
        if (strcmp(tpChksum, "Jbiqze4DlRPbPmbIL0wS8F") == 0) {
          extern mxArray *sf_c8_LessonI_third_party_uses_info(void);
          plhs[0] = sf_c8_LessonI_third_party_uses_info();
          break;
        }
      }

     case 9:
      {
        if (strcmp(tpChksum, "SafIxCjmM9qEPS6BmID8AG") == 0) {
          extern mxArray *sf_c9_LessonI_third_party_uses_info(void);
          plhs[0] = sf_c9_LessonI_third_party_uses_info();
          break;
        }
      }

     case 10:
      {
        if (strcmp(tpChksum, "ykND2QY3AIDhFCaEB5eP5F") == 0) {
          extern mxArray *sf_c10_LessonI_third_party_uses_info(void);
          plhs[0] = sf_c10_LessonI_third_party_uses_info();
          break;
        }
      }

     case 12:
      {
        if (strcmp(tpChksum, "wt65AZ2hxioUIGw90mtxBB") == 0) {
          extern mxArray *sf_c12_LessonI_third_party_uses_info(void);
          plhs[0] = sf_c12_LessonI_third_party_uses_info();
          break;
        }
      }

     case 16:
      {
        if (strcmp(tpChksum, "ynY5ttWD0J2n5VW3gZiZPC") == 0) {
          extern mxArray *sf_c16_LessonI_third_party_uses_info(void);
          plhs[0] = sf_c16_LessonI_third_party_uses_info();
          break;
        }
      }

     case 18:
      {
        if (strcmp(tpChksum, "4pv0ujrLf6jH7XE7GMsCnH") == 0) {
          extern mxArray *sf_c18_LessonI_third_party_uses_info(void);
          plhs[0] = sf_c18_LessonI_third_party_uses_info();
          break;
        }
      }

     case 19:
      {
        if (strcmp(tpChksum, "ynY5ttWD0J2n5VW3gZiZPC") == 0) {
          extern mxArray *sf_c19_LessonI_third_party_uses_info(void);
          plhs[0] = sf_c19_LessonI_third_party_uses_info();
          break;
        }
      }

     case 23:
      {
        if (strcmp(tpChksum, "gkxnlDDaCol3tTf1u27nYB") == 0) {
          extern mxArray *sf_c23_LessonI_third_party_uses_info(void);
          plhs[0] = sf_c23_LessonI_third_party_uses_info();
          break;
        }
      }

     case 26:
      {
        if (strcmp(tpChksum, "xNYm6GNN8jSBExJBkcuC2B") == 0) {
          extern mxArray *sf_c26_LessonI_third_party_uses_info(void);
          plhs[0] = sf_c26_LessonI_third_party_uses_info();
          break;
        }
      }

     case 27:
      {
        if (strcmp(tpChksum, "iLKkTZfgeUWbCHGkvxHUyF") == 0) {
          extern mxArray *sf_c27_LessonI_third_party_uses_info(void);
          plhs[0] = sf_c27_LessonI_third_party_uses_info();
          break;
        }
      }

     case 28:
      {
        if (strcmp(tpChksum, "mTPa5Pzbe3w9l3J7cRjFdB") == 0) {
          extern mxArray *sf_c28_LessonI_third_party_uses_info(void);
          plhs[0] = sf_c28_LessonI_third_party_uses_info();
          break;
        }
      }

     case 29:
      {
        if (strcmp(tpChksum, "Rlx5hbPfiijZHfd9WQUKiG") == 0) {
          extern mxArray *sf_c29_LessonI_third_party_uses_info(void);
          plhs[0] = sf_c29_LessonI_third_party_uses_info();
          break;
        }
      }

     case 30:
      {
        if (strcmp(tpChksum, "3eIjVhVEkbA6prTKjxYplG") == 0) {
          extern mxArray *sf_c30_LessonI_third_party_uses_info(void);
          plhs[0] = sf_c30_LessonI_third_party_uses_info();
          break;
        }
      }

     case 31:
      {
        if (strcmp(tpChksum, "0PKeH5rD45G8wAjm9BCJ1D") == 0) {
          extern mxArray *sf_c31_LessonI_third_party_uses_info(void);
          plhs[0] = sf_c31_LessonI_third_party_uses_info();
          break;
        }
      }

     case 32:
      {
        if (strcmp(tpChksum, "gkxnlDDaCol3tTf1u27nYB") == 0) {
          extern mxArray *sf_c32_LessonI_third_party_uses_info(void);
          plhs[0] = sf_c32_LessonI_third_party_uses_info();
          break;
        }
      }

     case 34:
      {
        if (strcmp(tpChksum, "xNYm6GNN8jSBExJBkcuC2B") == 0) {
          extern mxArray *sf_c34_LessonI_third_party_uses_info(void);
          plhs[0] = sf_c34_LessonI_third_party_uses_info();
          break;
        }
      }

     case 36:
      {
        if (strcmp(tpChksum, "xNYm6GNN8jSBExJBkcuC2B") == 0) {
          extern mxArray *sf_c36_LessonI_third_party_uses_info(void);
          plhs[0] = sf_c36_LessonI_third_party_uses_info();
          break;
        }
      }

     case 37:
      {
        if (strcmp(tpChksum, "iLKkTZfgeUWbCHGkvxHUyF") == 0) {
          extern mxArray *sf_c37_LessonI_third_party_uses_info(void);
          plhs[0] = sf_c37_LessonI_third_party_uses_info();
          break;
        }
      }

     case 38:
      {
        if (strcmp(tpChksum, "mTPa5Pzbe3w9l3J7cRjFdB") == 0) {
          extern mxArray *sf_c38_LessonI_third_party_uses_info(void);
          plhs[0] = sf_c38_LessonI_third_party_uses_info();
          break;
        }
      }

     case 39:
      {
        if (strcmp(tpChksum, "Rlx5hbPfiijZHfd9WQUKiG") == 0) {
          extern mxArray *sf_c39_LessonI_third_party_uses_info(void);
          plhs[0] = sf_c39_LessonI_third_party_uses_info();
          break;
        }
      }

     case 41:
      {
        if (strcmp(tpChksum, "ynY5ttWD0J2n5VW3gZiZPC") == 0) {
          extern mxArray *sf_c41_LessonI_third_party_uses_info(void);
          plhs[0] = sf_c41_LessonI_third_party_uses_info();
          break;
        }
      }

     case 42:
      {
        if (strcmp(tpChksum, "gKRazsXtkVuGLxRXB6vTwG") == 0) {
          extern mxArray *sf_c42_LessonI_third_party_uses_info(void);
          plhs[0] = sf_c42_LessonI_third_party_uses_info();
          break;
        }
      }

     case 43:
      {
        if (strcmp(tpChksum, "gkxnlDDaCol3tTf1u27nYB") == 0) {
          extern mxArray *sf_c43_LessonI_third_party_uses_info(void);
          plhs[0] = sf_c43_LessonI_third_party_uses_info();
          break;
        }
      }

     case 46:
      {
        if (strcmp(tpChksum, "xNYm6GNN8jSBExJBkcuC2B") == 0) {
          extern mxArray *sf_c46_LessonI_third_party_uses_info(void);
          plhs[0] = sf_c46_LessonI_third_party_uses_info();
          break;
        }
      }

     case 47:
      {
        if (strcmp(tpChksum, "iLKkTZfgeUWbCHGkvxHUyF") == 0) {
          extern mxArray *sf_c47_LessonI_third_party_uses_info(void);
          plhs[0] = sf_c47_LessonI_third_party_uses_info();
          break;
        }
      }

     case 48:
      {
        if (strcmp(tpChksum, "mTPa5Pzbe3w9l3J7cRjFdB") == 0) {
          extern mxArray *sf_c48_LessonI_third_party_uses_info(void);
          plhs[0] = sf_c48_LessonI_third_party_uses_info();
          break;
        }
      }

     case 49:
      {
        if (strcmp(tpChksum, "Rlx5hbPfiijZHfd9WQUKiG") == 0) {
          extern mxArray *sf_c49_LessonI_third_party_uses_info(void);
          plhs[0] = sf_c49_LessonI_third_party_uses_info();
          break;
        }
      }

     case 51:
      {
        if (strcmp(tpChksum, "ynY5ttWD0J2n5VW3gZiZPC") == 0) {
          extern mxArray *sf_c51_LessonI_third_party_uses_info(void);
          plhs[0] = sf_c51_LessonI_third_party_uses_info();
          break;
        }
      }

     case 52:
      {
        if (strcmp(tpChksum, "gKRazsXtkVuGLxRXB6vTwG") == 0) {
          extern mxArray *sf_c52_LessonI_third_party_uses_info(void);
          plhs[0] = sf_c52_LessonI_third_party_uses_info();
          break;
        }
      }

     case 53:
      {
        if (strcmp(tpChksum, "gkxnlDDaCol3tTf1u27nYB") == 0) {
          extern mxArray *sf_c53_LessonI_third_party_uses_info(void);
          plhs[0] = sf_c53_LessonI_third_party_uses_info();
          break;
        }
      }

     case 56:
      {
        if (strcmp(tpChksum, "xNYm6GNN8jSBExJBkcuC2B") == 0) {
          extern mxArray *sf_c56_LessonI_third_party_uses_info(void);
          plhs[0] = sf_c56_LessonI_third_party_uses_info();
          break;
        }
      }

     case 57:
      {
        if (strcmp(tpChksum, "iLKkTZfgeUWbCHGkvxHUyF") == 0) {
          extern mxArray *sf_c57_LessonI_third_party_uses_info(void);
          plhs[0] = sf_c57_LessonI_third_party_uses_info();
          break;
        }
      }

     case 58:
      {
        if (strcmp(tpChksum, "mTPa5Pzbe3w9l3J7cRjFdB") == 0) {
          extern mxArray *sf_c58_LessonI_third_party_uses_info(void);
          plhs[0] = sf_c58_LessonI_third_party_uses_info();
          break;
        }
      }

     case 59:
      {
        if (strcmp(tpChksum, "Rlx5hbPfiijZHfd9WQUKiG") == 0) {
          extern mxArray *sf_c59_LessonI_third_party_uses_info(void);
          plhs[0] = sf_c59_LessonI_third_party_uses_info();
          break;
        }
      }

     case 60:
      {
        if (strcmp(tpChksum, "H3SrOtX8Tlo0LVyUVlbGyE") == 0) {
          extern mxArray *sf_c60_LessonI_third_party_uses_info(void);
          plhs[0] = sf_c60_LessonI_third_party_uses_info();
          break;
        }
      }

     default:
      plhs[0] = mxCreateDoubleMatrix(0,0,mxREAL);
    }
  }

  return 1;
}

unsigned int sf_LessonI_jit_fallback_info( int nlhs, mxArray * plhs[], int nrhs,
  const mxArray * prhs[] )
{
  char commandName[64];
  char tpChksum[64];
  if (nrhs<3 || !mxIsChar(prhs[0]))
    return 0;

  /* Possible call to get the jit_fallback_info */
  mxGetString(prhs[0], commandName,sizeof(commandName)/sizeof(char));
  commandName[(sizeof(commandName)/sizeof(char)-1)] = '\0';
  mxGetString(prhs[2], tpChksum,sizeof(tpChksum)/sizeof(char));
  tpChksum[(sizeof(tpChksum)/sizeof(char)-1)] = '\0';
  if (strcmp(commandName,"get_jit_fallback_info"))
    return 0;

  {
    unsigned int chartFileNumber;
    chartFileNumber = (unsigned int)mxGetScalar(prhs[1]);
    switch (chartFileNumber) {
     case 1:
      {
        if (strcmp(tpChksum, "77qs0O1aFQP9R5KVWKglD") == 0) {
          extern mxArray *sf_c1_LessonI_jit_fallback_info(void);
          plhs[0] = sf_c1_LessonI_jit_fallback_info();
          break;
        }
      }

     case 2:
      {
        if (strcmp(tpChksum, "BPbr6mnccR4iiqbbOejEIC") == 0) {
          extern mxArray *sf_c2_LessonI_jit_fallback_info(void);
          plhs[0] = sf_c2_LessonI_jit_fallback_info();
          break;
        }
      }

     case 3:
      {
        if (strcmp(tpChksum, "gblOQFxJdbh8vdZcLHybtE") == 0) {
          extern mxArray *sf_c3_LessonI_jit_fallback_info(void);
          plhs[0] = sf_c3_LessonI_jit_fallback_info();
          break;
        }
      }

     case 4:
      {
        if (strcmp(tpChksum, "9dC2ipLYw0brxEDvXnTWxC") == 0) {
          extern mxArray *sf_c4_LessonI_jit_fallback_info(void);
          plhs[0] = sf_c4_LessonI_jit_fallback_info();
          break;
        }
      }

     case 7:
      {
        if (strcmp(tpChksum, "gkxnlDDaCol3tTf1u27nYB") == 0) {
          extern mxArray *sf_c7_LessonI_jit_fallback_info(void);
          plhs[0] = sf_c7_LessonI_jit_fallback_info();
          break;
        }
      }

     case 8:
      {
        if (strcmp(tpChksum, "Jbiqze4DlRPbPmbIL0wS8F") == 0) {
          extern mxArray *sf_c8_LessonI_jit_fallback_info(void);
          plhs[0] = sf_c8_LessonI_jit_fallback_info();
          break;
        }
      }

     case 9:
      {
        if (strcmp(tpChksum, "SafIxCjmM9qEPS6BmID8AG") == 0) {
          extern mxArray *sf_c9_LessonI_jit_fallback_info(void);
          plhs[0] = sf_c9_LessonI_jit_fallback_info();
          break;
        }
      }

     case 10:
      {
        if (strcmp(tpChksum, "ykND2QY3AIDhFCaEB5eP5F") == 0) {
          extern mxArray *sf_c10_LessonI_jit_fallback_info(void);
          plhs[0] = sf_c10_LessonI_jit_fallback_info();
          break;
        }
      }

     case 12:
      {
        if (strcmp(tpChksum, "wt65AZ2hxioUIGw90mtxBB") == 0) {
          extern mxArray *sf_c12_LessonI_jit_fallback_info(void);
          plhs[0] = sf_c12_LessonI_jit_fallback_info();
          break;
        }
      }

     case 16:
      {
        if (strcmp(tpChksum, "ynY5ttWD0J2n5VW3gZiZPC") == 0) {
          extern mxArray *sf_c16_LessonI_jit_fallback_info(void);
          plhs[0] = sf_c16_LessonI_jit_fallback_info();
          break;
        }
      }

     case 18:
      {
        if (strcmp(tpChksum, "4pv0ujrLf6jH7XE7GMsCnH") == 0) {
          extern mxArray *sf_c18_LessonI_jit_fallback_info(void);
          plhs[0] = sf_c18_LessonI_jit_fallback_info();
          break;
        }
      }

     case 19:
      {
        if (strcmp(tpChksum, "ynY5ttWD0J2n5VW3gZiZPC") == 0) {
          extern mxArray *sf_c19_LessonI_jit_fallback_info(void);
          plhs[0] = sf_c19_LessonI_jit_fallback_info();
          break;
        }
      }

     case 23:
      {
        if (strcmp(tpChksum, "gkxnlDDaCol3tTf1u27nYB") == 0) {
          extern mxArray *sf_c23_LessonI_jit_fallback_info(void);
          plhs[0] = sf_c23_LessonI_jit_fallback_info();
          break;
        }
      }

     case 26:
      {
        if (strcmp(tpChksum, "xNYm6GNN8jSBExJBkcuC2B") == 0) {
          extern mxArray *sf_c26_LessonI_jit_fallback_info(void);
          plhs[0] = sf_c26_LessonI_jit_fallback_info();
          break;
        }
      }

     case 27:
      {
        if (strcmp(tpChksum, "iLKkTZfgeUWbCHGkvxHUyF") == 0) {
          extern mxArray *sf_c27_LessonI_jit_fallback_info(void);
          plhs[0] = sf_c27_LessonI_jit_fallback_info();
          break;
        }
      }

     case 28:
      {
        if (strcmp(tpChksum, "mTPa5Pzbe3w9l3J7cRjFdB") == 0) {
          extern mxArray *sf_c28_LessonI_jit_fallback_info(void);
          plhs[0] = sf_c28_LessonI_jit_fallback_info();
          break;
        }
      }

     case 29:
      {
        if (strcmp(tpChksum, "Rlx5hbPfiijZHfd9WQUKiG") == 0) {
          extern mxArray *sf_c29_LessonI_jit_fallback_info(void);
          plhs[0] = sf_c29_LessonI_jit_fallback_info();
          break;
        }
      }

     case 30:
      {
        if (strcmp(tpChksum, "3eIjVhVEkbA6prTKjxYplG") == 0) {
          extern mxArray *sf_c30_LessonI_jit_fallback_info(void);
          plhs[0] = sf_c30_LessonI_jit_fallback_info();
          break;
        }
      }

     case 31:
      {
        if (strcmp(tpChksum, "0PKeH5rD45G8wAjm9BCJ1D") == 0) {
          extern mxArray *sf_c31_LessonI_jit_fallback_info(void);
          plhs[0] = sf_c31_LessonI_jit_fallback_info();
          break;
        }
      }

     case 32:
      {
        if (strcmp(tpChksum, "gkxnlDDaCol3tTf1u27nYB") == 0) {
          extern mxArray *sf_c32_LessonI_jit_fallback_info(void);
          plhs[0] = sf_c32_LessonI_jit_fallback_info();
          break;
        }
      }

     case 34:
      {
        if (strcmp(tpChksum, "xNYm6GNN8jSBExJBkcuC2B") == 0) {
          extern mxArray *sf_c34_LessonI_jit_fallback_info(void);
          plhs[0] = sf_c34_LessonI_jit_fallback_info();
          break;
        }
      }

     case 36:
      {
        if (strcmp(tpChksum, "xNYm6GNN8jSBExJBkcuC2B") == 0) {
          extern mxArray *sf_c36_LessonI_jit_fallback_info(void);
          plhs[0] = sf_c36_LessonI_jit_fallback_info();
          break;
        }
      }

     case 37:
      {
        if (strcmp(tpChksum, "iLKkTZfgeUWbCHGkvxHUyF") == 0) {
          extern mxArray *sf_c37_LessonI_jit_fallback_info(void);
          plhs[0] = sf_c37_LessonI_jit_fallback_info();
          break;
        }
      }

     case 38:
      {
        if (strcmp(tpChksum, "mTPa5Pzbe3w9l3J7cRjFdB") == 0) {
          extern mxArray *sf_c38_LessonI_jit_fallback_info(void);
          plhs[0] = sf_c38_LessonI_jit_fallback_info();
          break;
        }
      }

     case 39:
      {
        if (strcmp(tpChksum, "Rlx5hbPfiijZHfd9WQUKiG") == 0) {
          extern mxArray *sf_c39_LessonI_jit_fallback_info(void);
          plhs[0] = sf_c39_LessonI_jit_fallback_info();
          break;
        }
      }

     case 41:
      {
        if (strcmp(tpChksum, "ynY5ttWD0J2n5VW3gZiZPC") == 0) {
          extern mxArray *sf_c41_LessonI_jit_fallback_info(void);
          plhs[0] = sf_c41_LessonI_jit_fallback_info();
          break;
        }
      }

     case 42:
      {
        if (strcmp(tpChksum, "gKRazsXtkVuGLxRXB6vTwG") == 0) {
          extern mxArray *sf_c42_LessonI_jit_fallback_info(void);
          plhs[0] = sf_c42_LessonI_jit_fallback_info();
          break;
        }
      }

     case 43:
      {
        if (strcmp(tpChksum, "gkxnlDDaCol3tTf1u27nYB") == 0) {
          extern mxArray *sf_c43_LessonI_jit_fallback_info(void);
          plhs[0] = sf_c43_LessonI_jit_fallback_info();
          break;
        }
      }

     case 46:
      {
        if (strcmp(tpChksum, "xNYm6GNN8jSBExJBkcuC2B") == 0) {
          extern mxArray *sf_c46_LessonI_jit_fallback_info(void);
          plhs[0] = sf_c46_LessonI_jit_fallback_info();
          break;
        }
      }

     case 47:
      {
        if (strcmp(tpChksum, "iLKkTZfgeUWbCHGkvxHUyF") == 0) {
          extern mxArray *sf_c47_LessonI_jit_fallback_info(void);
          plhs[0] = sf_c47_LessonI_jit_fallback_info();
          break;
        }
      }

     case 48:
      {
        if (strcmp(tpChksum, "mTPa5Pzbe3w9l3J7cRjFdB") == 0) {
          extern mxArray *sf_c48_LessonI_jit_fallback_info(void);
          plhs[0] = sf_c48_LessonI_jit_fallback_info();
          break;
        }
      }

     case 49:
      {
        if (strcmp(tpChksum, "Rlx5hbPfiijZHfd9WQUKiG") == 0) {
          extern mxArray *sf_c49_LessonI_jit_fallback_info(void);
          plhs[0] = sf_c49_LessonI_jit_fallback_info();
          break;
        }
      }

     case 51:
      {
        if (strcmp(tpChksum, "ynY5ttWD0J2n5VW3gZiZPC") == 0) {
          extern mxArray *sf_c51_LessonI_jit_fallback_info(void);
          plhs[0] = sf_c51_LessonI_jit_fallback_info();
          break;
        }
      }

     case 52:
      {
        if (strcmp(tpChksum, "gKRazsXtkVuGLxRXB6vTwG") == 0) {
          extern mxArray *sf_c52_LessonI_jit_fallback_info(void);
          plhs[0] = sf_c52_LessonI_jit_fallback_info();
          break;
        }
      }

     case 53:
      {
        if (strcmp(tpChksum, "gkxnlDDaCol3tTf1u27nYB") == 0) {
          extern mxArray *sf_c53_LessonI_jit_fallback_info(void);
          plhs[0] = sf_c53_LessonI_jit_fallback_info();
          break;
        }
      }

     case 56:
      {
        if (strcmp(tpChksum, "xNYm6GNN8jSBExJBkcuC2B") == 0) {
          extern mxArray *sf_c56_LessonI_jit_fallback_info(void);
          plhs[0] = sf_c56_LessonI_jit_fallback_info();
          break;
        }
      }

     case 57:
      {
        if (strcmp(tpChksum, "iLKkTZfgeUWbCHGkvxHUyF") == 0) {
          extern mxArray *sf_c57_LessonI_jit_fallback_info(void);
          plhs[0] = sf_c57_LessonI_jit_fallback_info();
          break;
        }
      }

     case 58:
      {
        if (strcmp(tpChksum, "mTPa5Pzbe3w9l3J7cRjFdB") == 0) {
          extern mxArray *sf_c58_LessonI_jit_fallback_info(void);
          plhs[0] = sf_c58_LessonI_jit_fallback_info();
          break;
        }
      }

     case 59:
      {
        if (strcmp(tpChksum, "Rlx5hbPfiijZHfd9WQUKiG") == 0) {
          extern mxArray *sf_c59_LessonI_jit_fallback_info(void);
          plhs[0] = sf_c59_LessonI_jit_fallback_info();
          break;
        }
      }

     case 60:
      {
        if (strcmp(tpChksum, "H3SrOtX8Tlo0LVyUVlbGyE") == 0) {
          extern mxArray *sf_c60_LessonI_jit_fallback_info(void);
          plhs[0] = sf_c60_LessonI_jit_fallback_info();
          break;
        }
      }

     default:
      plhs[0] = mxCreateDoubleMatrix(0,0,mxREAL);
    }
  }

  return 1;
}

unsigned int sf_LessonI_updateBuildInfo_args_info( int nlhs, mxArray * plhs[],
  int nrhs, const mxArray * prhs[] )
{
  char commandName[64];
  char tpChksum[64];
  if (nrhs<3 || !mxIsChar(prhs[0]))
    return 0;

  /* Possible call to get the updateBuildInfo_args_info */
  mxGetString(prhs[0], commandName,sizeof(commandName)/sizeof(char));
  commandName[(sizeof(commandName)/sizeof(char)-1)] = '\0';
  mxGetString(prhs[2], tpChksum,sizeof(tpChksum)/sizeof(char));
  tpChksum[(sizeof(tpChksum)/sizeof(char)-1)] = '\0';
  if (strcmp(commandName,"get_updateBuildInfo_args_info"))
    return 0;

  {
    unsigned int chartFileNumber;
    chartFileNumber = (unsigned int)mxGetScalar(prhs[1]);
    switch (chartFileNumber) {
     case 1:
      {
        if (strcmp(tpChksum, "77qs0O1aFQP9R5KVWKglD") == 0) {
          extern mxArray *sf_c1_LessonI_updateBuildInfo_args_info(void);
          plhs[0] = sf_c1_LessonI_updateBuildInfo_args_info();
          break;
        }
      }

     case 2:
      {
        if (strcmp(tpChksum, "BPbr6mnccR4iiqbbOejEIC") == 0) {
          extern mxArray *sf_c2_LessonI_updateBuildInfo_args_info(void);
          plhs[0] = sf_c2_LessonI_updateBuildInfo_args_info();
          break;
        }
      }

     case 3:
      {
        if (strcmp(tpChksum, "gblOQFxJdbh8vdZcLHybtE") == 0) {
          extern mxArray *sf_c3_LessonI_updateBuildInfo_args_info(void);
          plhs[0] = sf_c3_LessonI_updateBuildInfo_args_info();
          break;
        }
      }

     case 4:
      {
        if (strcmp(tpChksum, "9dC2ipLYw0brxEDvXnTWxC") == 0) {
          extern mxArray *sf_c4_LessonI_updateBuildInfo_args_info(void);
          plhs[0] = sf_c4_LessonI_updateBuildInfo_args_info();
          break;
        }
      }

     case 7:
      {
        if (strcmp(tpChksum, "gkxnlDDaCol3tTf1u27nYB") == 0) {
          extern mxArray *sf_c7_LessonI_updateBuildInfo_args_info(void);
          plhs[0] = sf_c7_LessonI_updateBuildInfo_args_info();
          break;
        }
      }

     case 8:
      {
        if (strcmp(tpChksum, "Jbiqze4DlRPbPmbIL0wS8F") == 0) {
          extern mxArray *sf_c8_LessonI_updateBuildInfo_args_info(void);
          plhs[0] = sf_c8_LessonI_updateBuildInfo_args_info();
          break;
        }
      }

     case 9:
      {
        if (strcmp(tpChksum, "SafIxCjmM9qEPS6BmID8AG") == 0) {
          extern mxArray *sf_c9_LessonI_updateBuildInfo_args_info(void);
          plhs[0] = sf_c9_LessonI_updateBuildInfo_args_info();
          break;
        }
      }

     case 10:
      {
        if (strcmp(tpChksum, "ykND2QY3AIDhFCaEB5eP5F") == 0) {
          extern mxArray *sf_c10_LessonI_updateBuildInfo_args_info(void);
          plhs[0] = sf_c10_LessonI_updateBuildInfo_args_info();
          break;
        }
      }

     case 12:
      {
        if (strcmp(tpChksum, "wt65AZ2hxioUIGw90mtxBB") == 0) {
          extern mxArray *sf_c12_LessonI_updateBuildInfo_args_info(void);
          plhs[0] = sf_c12_LessonI_updateBuildInfo_args_info();
          break;
        }
      }

     case 16:
      {
        if (strcmp(tpChksum, "ynY5ttWD0J2n5VW3gZiZPC") == 0) {
          extern mxArray *sf_c16_LessonI_updateBuildInfo_args_info(void);
          plhs[0] = sf_c16_LessonI_updateBuildInfo_args_info();
          break;
        }
      }

     case 18:
      {
        if (strcmp(tpChksum, "4pv0ujrLf6jH7XE7GMsCnH") == 0) {
          extern mxArray *sf_c18_LessonI_updateBuildInfo_args_info(void);
          plhs[0] = sf_c18_LessonI_updateBuildInfo_args_info();
          break;
        }
      }

     case 19:
      {
        if (strcmp(tpChksum, "ynY5ttWD0J2n5VW3gZiZPC") == 0) {
          extern mxArray *sf_c19_LessonI_updateBuildInfo_args_info(void);
          plhs[0] = sf_c19_LessonI_updateBuildInfo_args_info();
          break;
        }
      }

     case 23:
      {
        if (strcmp(tpChksum, "gkxnlDDaCol3tTf1u27nYB") == 0) {
          extern mxArray *sf_c23_LessonI_updateBuildInfo_args_info(void);
          plhs[0] = sf_c23_LessonI_updateBuildInfo_args_info();
          break;
        }
      }

     case 26:
      {
        if (strcmp(tpChksum, "xNYm6GNN8jSBExJBkcuC2B") == 0) {
          extern mxArray *sf_c26_LessonI_updateBuildInfo_args_info(void);
          plhs[0] = sf_c26_LessonI_updateBuildInfo_args_info();
          break;
        }
      }

     case 27:
      {
        if (strcmp(tpChksum, "iLKkTZfgeUWbCHGkvxHUyF") == 0) {
          extern mxArray *sf_c27_LessonI_updateBuildInfo_args_info(void);
          plhs[0] = sf_c27_LessonI_updateBuildInfo_args_info();
          break;
        }
      }

     case 28:
      {
        if (strcmp(tpChksum, "mTPa5Pzbe3w9l3J7cRjFdB") == 0) {
          extern mxArray *sf_c28_LessonI_updateBuildInfo_args_info(void);
          plhs[0] = sf_c28_LessonI_updateBuildInfo_args_info();
          break;
        }
      }

     case 29:
      {
        if (strcmp(tpChksum, "Rlx5hbPfiijZHfd9WQUKiG") == 0) {
          extern mxArray *sf_c29_LessonI_updateBuildInfo_args_info(void);
          plhs[0] = sf_c29_LessonI_updateBuildInfo_args_info();
          break;
        }
      }

     case 30:
      {
        if (strcmp(tpChksum, "3eIjVhVEkbA6prTKjxYplG") == 0) {
          extern mxArray *sf_c30_LessonI_updateBuildInfo_args_info(void);
          plhs[0] = sf_c30_LessonI_updateBuildInfo_args_info();
          break;
        }
      }

     case 31:
      {
        if (strcmp(tpChksum, "0PKeH5rD45G8wAjm9BCJ1D") == 0) {
          extern mxArray *sf_c31_LessonI_updateBuildInfo_args_info(void);
          plhs[0] = sf_c31_LessonI_updateBuildInfo_args_info();
          break;
        }
      }

     case 32:
      {
        if (strcmp(tpChksum, "gkxnlDDaCol3tTf1u27nYB") == 0) {
          extern mxArray *sf_c32_LessonI_updateBuildInfo_args_info(void);
          plhs[0] = sf_c32_LessonI_updateBuildInfo_args_info();
          break;
        }
      }

     case 34:
      {
        if (strcmp(tpChksum, "xNYm6GNN8jSBExJBkcuC2B") == 0) {
          extern mxArray *sf_c34_LessonI_updateBuildInfo_args_info(void);
          plhs[0] = sf_c34_LessonI_updateBuildInfo_args_info();
          break;
        }
      }

     case 36:
      {
        if (strcmp(tpChksum, "xNYm6GNN8jSBExJBkcuC2B") == 0) {
          extern mxArray *sf_c36_LessonI_updateBuildInfo_args_info(void);
          plhs[0] = sf_c36_LessonI_updateBuildInfo_args_info();
          break;
        }
      }

     case 37:
      {
        if (strcmp(tpChksum, "iLKkTZfgeUWbCHGkvxHUyF") == 0) {
          extern mxArray *sf_c37_LessonI_updateBuildInfo_args_info(void);
          plhs[0] = sf_c37_LessonI_updateBuildInfo_args_info();
          break;
        }
      }

     case 38:
      {
        if (strcmp(tpChksum, "mTPa5Pzbe3w9l3J7cRjFdB") == 0) {
          extern mxArray *sf_c38_LessonI_updateBuildInfo_args_info(void);
          plhs[0] = sf_c38_LessonI_updateBuildInfo_args_info();
          break;
        }
      }

     case 39:
      {
        if (strcmp(tpChksum, "Rlx5hbPfiijZHfd9WQUKiG") == 0) {
          extern mxArray *sf_c39_LessonI_updateBuildInfo_args_info(void);
          plhs[0] = sf_c39_LessonI_updateBuildInfo_args_info();
          break;
        }
      }

     case 41:
      {
        if (strcmp(tpChksum, "ynY5ttWD0J2n5VW3gZiZPC") == 0) {
          extern mxArray *sf_c41_LessonI_updateBuildInfo_args_info(void);
          plhs[0] = sf_c41_LessonI_updateBuildInfo_args_info();
          break;
        }
      }

     case 42:
      {
        if (strcmp(tpChksum, "gKRazsXtkVuGLxRXB6vTwG") == 0) {
          extern mxArray *sf_c42_LessonI_updateBuildInfo_args_info(void);
          plhs[0] = sf_c42_LessonI_updateBuildInfo_args_info();
          break;
        }
      }

     case 43:
      {
        if (strcmp(tpChksum, "gkxnlDDaCol3tTf1u27nYB") == 0) {
          extern mxArray *sf_c43_LessonI_updateBuildInfo_args_info(void);
          plhs[0] = sf_c43_LessonI_updateBuildInfo_args_info();
          break;
        }
      }

     case 46:
      {
        if (strcmp(tpChksum, "xNYm6GNN8jSBExJBkcuC2B") == 0) {
          extern mxArray *sf_c46_LessonI_updateBuildInfo_args_info(void);
          plhs[0] = sf_c46_LessonI_updateBuildInfo_args_info();
          break;
        }
      }

     case 47:
      {
        if (strcmp(tpChksum, "iLKkTZfgeUWbCHGkvxHUyF") == 0) {
          extern mxArray *sf_c47_LessonI_updateBuildInfo_args_info(void);
          plhs[0] = sf_c47_LessonI_updateBuildInfo_args_info();
          break;
        }
      }

     case 48:
      {
        if (strcmp(tpChksum, "mTPa5Pzbe3w9l3J7cRjFdB") == 0) {
          extern mxArray *sf_c48_LessonI_updateBuildInfo_args_info(void);
          plhs[0] = sf_c48_LessonI_updateBuildInfo_args_info();
          break;
        }
      }

     case 49:
      {
        if (strcmp(tpChksum, "Rlx5hbPfiijZHfd9WQUKiG") == 0) {
          extern mxArray *sf_c49_LessonI_updateBuildInfo_args_info(void);
          plhs[0] = sf_c49_LessonI_updateBuildInfo_args_info();
          break;
        }
      }

     case 51:
      {
        if (strcmp(tpChksum, "ynY5ttWD0J2n5VW3gZiZPC") == 0) {
          extern mxArray *sf_c51_LessonI_updateBuildInfo_args_info(void);
          plhs[0] = sf_c51_LessonI_updateBuildInfo_args_info();
          break;
        }
      }

     case 52:
      {
        if (strcmp(tpChksum, "gKRazsXtkVuGLxRXB6vTwG") == 0) {
          extern mxArray *sf_c52_LessonI_updateBuildInfo_args_info(void);
          plhs[0] = sf_c52_LessonI_updateBuildInfo_args_info();
          break;
        }
      }

     case 53:
      {
        if (strcmp(tpChksum, "gkxnlDDaCol3tTf1u27nYB") == 0) {
          extern mxArray *sf_c53_LessonI_updateBuildInfo_args_info(void);
          plhs[0] = sf_c53_LessonI_updateBuildInfo_args_info();
          break;
        }
      }

     case 56:
      {
        if (strcmp(tpChksum, "xNYm6GNN8jSBExJBkcuC2B") == 0) {
          extern mxArray *sf_c56_LessonI_updateBuildInfo_args_info(void);
          plhs[0] = sf_c56_LessonI_updateBuildInfo_args_info();
          break;
        }
      }

     case 57:
      {
        if (strcmp(tpChksum, "iLKkTZfgeUWbCHGkvxHUyF") == 0) {
          extern mxArray *sf_c57_LessonI_updateBuildInfo_args_info(void);
          plhs[0] = sf_c57_LessonI_updateBuildInfo_args_info();
          break;
        }
      }

     case 58:
      {
        if (strcmp(tpChksum, "mTPa5Pzbe3w9l3J7cRjFdB") == 0) {
          extern mxArray *sf_c58_LessonI_updateBuildInfo_args_info(void);
          plhs[0] = sf_c58_LessonI_updateBuildInfo_args_info();
          break;
        }
      }

     case 59:
      {
        if (strcmp(tpChksum, "Rlx5hbPfiijZHfd9WQUKiG") == 0) {
          extern mxArray *sf_c59_LessonI_updateBuildInfo_args_info(void);
          plhs[0] = sf_c59_LessonI_updateBuildInfo_args_info();
          break;
        }
      }

     case 60:
      {
        if (strcmp(tpChksum, "H3SrOtX8Tlo0LVyUVlbGyE") == 0) {
          extern mxArray *sf_c60_LessonI_updateBuildInfo_args_info(void);
          plhs[0] = sf_c60_LessonI_updateBuildInfo_args_info();
          break;
        }
      }

     default:
      plhs[0] = mxCreateDoubleMatrix(0,0,mxREAL);
    }
  }

  return 1;
}

void sf_LessonI_get_post_codegen_info( int nlhs, mxArray * plhs[], int nrhs,
  const mxArray * prhs[] )
{
  unsigned int chartFileNumber = (unsigned int) mxGetScalar(prhs[0]);
  char tpChksum[64];
  mxGetString(prhs[1], tpChksum,sizeof(tpChksum)/sizeof(char));
  tpChksum[(sizeof(tpChksum)/sizeof(char)-1)] = '\0';
  switch (chartFileNumber) {
   case 1:
    {
      if (strcmp(tpChksum, "77qs0O1aFQP9R5KVWKglD") == 0) {
        extern mxArray *sf_c1_LessonI_get_post_codegen_info(void);
        plhs[0] = sf_c1_LessonI_get_post_codegen_info();
        return;
      }
    }
    break;

   case 2:
    {
      if (strcmp(tpChksum, "BPbr6mnccR4iiqbbOejEIC") == 0) {
        extern mxArray *sf_c2_LessonI_get_post_codegen_info(void);
        plhs[0] = sf_c2_LessonI_get_post_codegen_info();
        return;
      }
    }
    break;

   case 3:
    {
      if (strcmp(tpChksum, "gblOQFxJdbh8vdZcLHybtE") == 0) {
        extern mxArray *sf_c3_LessonI_get_post_codegen_info(void);
        plhs[0] = sf_c3_LessonI_get_post_codegen_info();
        return;
      }
    }
    break;

   case 4:
    {
      if (strcmp(tpChksum, "9dC2ipLYw0brxEDvXnTWxC") == 0) {
        extern mxArray *sf_c4_LessonI_get_post_codegen_info(void);
        plhs[0] = sf_c4_LessonI_get_post_codegen_info();
        return;
      }
    }
    break;

   case 7:
    {
      if (strcmp(tpChksum, "gkxnlDDaCol3tTf1u27nYB") == 0) {
        extern mxArray *sf_c7_LessonI_get_post_codegen_info(void);
        plhs[0] = sf_c7_LessonI_get_post_codegen_info();
        return;
      }
    }
    break;

   case 8:
    {
      if (strcmp(tpChksum, "Jbiqze4DlRPbPmbIL0wS8F") == 0) {
        extern mxArray *sf_c8_LessonI_get_post_codegen_info(void);
        plhs[0] = sf_c8_LessonI_get_post_codegen_info();
        return;
      }
    }
    break;

   case 9:
    {
      if (strcmp(tpChksum, "SafIxCjmM9qEPS6BmID8AG") == 0) {
        extern mxArray *sf_c9_LessonI_get_post_codegen_info(void);
        plhs[0] = sf_c9_LessonI_get_post_codegen_info();
        return;
      }
    }
    break;

   case 10:
    {
      if (strcmp(tpChksum, "ykND2QY3AIDhFCaEB5eP5F") == 0) {
        extern mxArray *sf_c10_LessonI_get_post_codegen_info(void);
        plhs[0] = sf_c10_LessonI_get_post_codegen_info();
        return;
      }
    }
    break;

   case 12:
    {
      if (strcmp(tpChksum, "wt65AZ2hxioUIGw90mtxBB") == 0) {
        extern mxArray *sf_c12_LessonI_get_post_codegen_info(void);
        plhs[0] = sf_c12_LessonI_get_post_codegen_info();
        return;
      }
    }
    break;

   case 16:
    {
      if (strcmp(tpChksum, "ynY5ttWD0J2n5VW3gZiZPC") == 0) {
        extern mxArray *sf_c16_LessonI_get_post_codegen_info(void);
        plhs[0] = sf_c16_LessonI_get_post_codegen_info();
        return;
      }
    }
    break;

   case 18:
    {
      if (strcmp(tpChksum, "4pv0ujrLf6jH7XE7GMsCnH") == 0) {
        extern mxArray *sf_c18_LessonI_get_post_codegen_info(void);
        plhs[0] = sf_c18_LessonI_get_post_codegen_info();
        return;
      }
    }
    break;

   case 19:
    {
      if (strcmp(tpChksum, "ynY5ttWD0J2n5VW3gZiZPC") == 0) {
        extern mxArray *sf_c19_LessonI_get_post_codegen_info(void);
        plhs[0] = sf_c19_LessonI_get_post_codegen_info();
        return;
      }
    }
    break;

   case 23:
    {
      if (strcmp(tpChksum, "gkxnlDDaCol3tTf1u27nYB") == 0) {
        extern mxArray *sf_c23_LessonI_get_post_codegen_info(void);
        plhs[0] = sf_c23_LessonI_get_post_codegen_info();
        return;
      }
    }
    break;

   case 26:
    {
      if (strcmp(tpChksum, "xNYm6GNN8jSBExJBkcuC2B") == 0) {
        extern mxArray *sf_c26_LessonI_get_post_codegen_info(void);
        plhs[0] = sf_c26_LessonI_get_post_codegen_info();
        return;
      }
    }
    break;

   case 27:
    {
      if (strcmp(tpChksum, "iLKkTZfgeUWbCHGkvxHUyF") == 0) {
        extern mxArray *sf_c27_LessonI_get_post_codegen_info(void);
        plhs[0] = sf_c27_LessonI_get_post_codegen_info();
        return;
      }
    }
    break;

   case 28:
    {
      if (strcmp(tpChksum, "mTPa5Pzbe3w9l3J7cRjFdB") == 0) {
        extern mxArray *sf_c28_LessonI_get_post_codegen_info(void);
        plhs[0] = sf_c28_LessonI_get_post_codegen_info();
        return;
      }
    }
    break;

   case 29:
    {
      if (strcmp(tpChksum, "Rlx5hbPfiijZHfd9WQUKiG") == 0) {
        extern mxArray *sf_c29_LessonI_get_post_codegen_info(void);
        plhs[0] = sf_c29_LessonI_get_post_codegen_info();
        return;
      }
    }
    break;

   case 30:
    {
      if (strcmp(tpChksum, "3eIjVhVEkbA6prTKjxYplG") == 0) {
        extern mxArray *sf_c30_LessonI_get_post_codegen_info(void);
        plhs[0] = sf_c30_LessonI_get_post_codegen_info();
        return;
      }
    }
    break;

   case 31:
    {
      if (strcmp(tpChksum, "0PKeH5rD45G8wAjm9BCJ1D") == 0) {
        extern mxArray *sf_c31_LessonI_get_post_codegen_info(void);
        plhs[0] = sf_c31_LessonI_get_post_codegen_info();
        return;
      }
    }
    break;

   case 32:
    {
      if (strcmp(tpChksum, "gkxnlDDaCol3tTf1u27nYB") == 0) {
        extern mxArray *sf_c32_LessonI_get_post_codegen_info(void);
        plhs[0] = sf_c32_LessonI_get_post_codegen_info();
        return;
      }
    }
    break;

   case 34:
    {
      if (strcmp(tpChksum, "xNYm6GNN8jSBExJBkcuC2B") == 0) {
        extern mxArray *sf_c34_LessonI_get_post_codegen_info(void);
        plhs[0] = sf_c34_LessonI_get_post_codegen_info();
        return;
      }
    }
    break;

   case 36:
    {
      if (strcmp(tpChksum, "xNYm6GNN8jSBExJBkcuC2B") == 0) {
        extern mxArray *sf_c36_LessonI_get_post_codegen_info(void);
        plhs[0] = sf_c36_LessonI_get_post_codegen_info();
        return;
      }
    }
    break;

   case 37:
    {
      if (strcmp(tpChksum, "iLKkTZfgeUWbCHGkvxHUyF") == 0) {
        extern mxArray *sf_c37_LessonI_get_post_codegen_info(void);
        plhs[0] = sf_c37_LessonI_get_post_codegen_info();
        return;
      }
    }
    break;

   case 38:
    {
      if (strcmp(tpChksum, "mTPa5Pzbe3w9l3J7cRjFdB") == 0) {
        extern mxArray *sf_c38_LessonI_get_post_codegen_info(void);
        plhs[0] = sf_c38_LessonI_get_post_codegen_info();
        return;
      }
    }
    break;

   case 39:
    {
      if (strcmp(tpChksum, "Rlx5hbPfiijZHfd9WQUKiG") == 0) {
        extern mxArray *sf_c39_LessonI_get_post_codegen_info(void);
        plhs[0] = sf_c39_LessonI_get_post_codegen_info();
        return;
      }
    }
    break;

   case 41:
    {
      if (strcmp(tpChksum, "ynY5ttWD0J2n5VW3gZiZPC") == 0) {
        extern mxArray *sf_c41_LessonI_get_post_codegen_info(void);
        plhs[0] = sf_c41_LessonI_get_post_codegen_info();
        return;
      }
    }
    break;

   case 42:
    {
      if (strcmp(tpChksum, "gKRazsXtkVuGLxRXB6vTwG") == 0) {
        extern mxArray *sf_c42_LessonI_get_post_codegen_info(void);
        plhs[0] = sf_c42_LessonI_get_post_codegen_info();
        return;
      }
    }
    break;

   case 43:
    {
      if (strcmp(tpChksum, "gkxnlDDaCol3tTf1u27nYB") == 0) {
        extern mxArray *sf_c43_LessonI_get_post_codegen_info(void);
        plhs[0] = sf_c43_LessonI_get_post_codegen_info();
        return;
      }
    }
    break;

   case 46:
    {
      if (strcmp(tpChksum, "xNYm6GNN8jSBExJBkcuC2B") == 0) {
        extern mxArray *sf_c46_LessonI_get_post_codegen_info(void);
        plhs[0] = sf_c46_LessonI_get_post_codegen_info();
        return;
      }
    }
    break;

   case 47:
    {
      if (strcmp(tpChksum, "iLKkTZfgeUWbCHGkvxHUyF") == 0) {
        extern mxArray *sf_c47_LessonI_get_post_codegen_info(void);
        plhs[0] = sf_c47_LessonI_get_post_codegen_info();
        return;
      }
    }
    break;

   case 48:
    {
      if (strcmp(tpChksum, "mTPa5Pzbe3w9l3J7cRjFdB") == 0) {
        extern mxArray *sf_c48_LessonI_get_post_codegen_info(void);
        plhs[0] = sf_c48_LessonI_get_post_codegen_info();
        return;
      }
    }
    break;

   case 49:
    {
      if (strcmp(tpChksum, "Rlx5hbPfiijZHfd9WQUKiG") == 0) {
        extern mxArray *sf_c49_LessonI_get_post_codegen_info(void);
        plhs[0] = sf_c49_LessonI_get_post_codegen_info();
        return;
      }
    }
    break;

   case 51:
    {
      if (strcmp(tpChksum, "ynY5ttWD0J2n5VW3gZiZPC") == 0) {
        extern mxArray *sf_c51_LessonI_get_post_codegen_info(void);
        plhs[0] = sf_c51_LessonI_get_post_codegen_info();
        return;
      }
    }
    break;

   case 52:
    {
      if (strcmp(tpChksum, "gKRazsXtkVuGLxRXB6vTwG") == 0) {
        extern mxArray *sf_c52_LessonI_get_post_codegen_info(void);
        plhs[0] = sf_c52_LessonI_get_post_codegen_info();
        return;
      }
    }
    break;

   case 53:
    {
      if (strcmp(tpChksum, "gkxnlDDaCol3tTf1u27nYB") == 0) {
        extern mxArray *sf_c53_LessonI_get_post_codegen_info(void);
        plhs[0] = sf_c53_LessonI_get_post_codegen_info();
        return;
      }
    }
    break;

   case 56:
    {
      if (strcmp(tpChksum, "xNYm6GNN8jSBExJBkcuC2B") == 0) {
        extern mxArray *sf_c56_LessonI_get_post_codegen_info(void);
        plhs[0] = sf_c56_LessonI_get_post_codegen_info();
        return;
      }
    }
    break;

   case 57:
    {
      if (strcmp(tpChksum, "iLKkTZfgeUWbCHGkvxHUyF") == 0) {
        extern mxArray *sf_c57_LessonI_get_post_codegen_info(void);
        plhs[0] = sf_c57_LessonI_get_post_codegen_info();
        return;
      }
    }
    break;

   case 58:
    {
      if (strcmp(tpChksum, "mTPa5Pzbe3w9l3J7cRjFdB") == 0) {
        extern mxArray *sf_c58_LessonI_get_post_codegen_info(void);
        plhs[0] = sf_c58_LessonI_get_post_codegen_info();
        return;
      }
    }
    break;

   case 59:
    {
      if (strcmp(tpChksum, "Rlx5hbPfiijZHfd9WQUKiG") == 0) {
        extern mxArray *sf_c59_LessonI_get_post_codegen_info(void);
        plhs[0] = sf_c59_LessonI_get_post_codegen_info();
        return;
      }
    }
    break;

   case 60:
    {
      if (strcmp(tpChksum, "H3SrOtX8Tlo0LVyUVlbGyE") == 0) {
        extern mxArray *sf_c60_LessonI_get_post_codegen_info(void);
        plhs[0] = sf_c60_LessonI_get_post_codegen_info();
        return;
      }
    }
    break;

   default:
    break;
  }

  plhs[0] = mxCreateDoubleMatrix(0,0,mxREAL);
}

void LessonI_debug_initialize(struct SfDebugInstanceStruct* debugInstance)
{
  _LessonIMachineNumber_ = sf_debug_initialize_machine(debugInstance,"LessonI",
    "sfun",0,40,0,0,0);
  sf_debug_set_machine_event_thresholds(debugInstance,_LessonIMachineNumber_,0,0);
  sf_debug_set_machine_data_thresholds(debugInstance,_LessonIMachineNumber_,0);
}

void LessonI_register_exported_symbols(SimStruct* S)
{
}

static mxArray* sRtwOptimizationInfoStruct= NULL;
mxArray* load_LessonI_optimization_info(void)
{
  if (sRtwOptimizationInfoStruct==NULL) {
    sRtwOptimizationInfoStruct = sf_load_rtw_optimization_info("LessonI",
      "LessonI");
    mexMakeArrayPersistent(sRtwOptimizationInfoStruct);
  }

  return(sRtwOptimizationInfoStruct);
}

void unload_LessonI_optimization_info(void)
{
  if (sRtwOptimizationInfoStruct!=NULL) {
    mxDestroyArray(sRtwOptimizationInfoStruct);
    sRtwOptimizationInfoStruct = NULL;
  }
}

#ifndef __c47_LessonI_h__
#define __c47_LessonI_h__

/* Include files */
#include "sf_runtime/sfc_sf.h"
#include "sf_runtime/sfc_mex.h"
#include "rtwtypes.h"
#include "multiword_types.h"

/* Type Definitions */
#ifndef typedef_SFc47_LessonIInstanceStruct
#define typedef_SFc47_LessonIInstanceStruct

typedef struct {
  SimStruct *S;
  ChartInfoStruct chartInfo;
  uint32_T chartNumber;
  uint32_T instanceNumber;
  int32_T c47_sfEvent;
  boolean_T c47_isStable;
  boolean_T c47_doneDoubleBufferReInit;
  uint8_T c47_is_active_c47_LessonI;
  int16_T *c47_targetO;
  int16_T *c47_realDorien;
  int16_T *c47_direc;
  int16_T *c47_myO;
} SFc47_LessonIInstanceStruct;

#endif                                 /*typedef_SFc47_LessonIInstanceStruct*/

/* Named Constants */

/* Variable Declarations */
extern struct SfDebugInstanceStruct *sfGlobalDebugInstanceStruct;

/* Variable Definitions */

/* Function Declarations */
extern const mxArray *sf_c47_LessonI_get_eml_resolved_functions_info(void);

/* Function Definitions */
extern void sf_c47_LessonI_get_check_sum(mxArray *plhs[]);
extern void c47_LessonI_method_dispatcher(SimStruct *S, int_T method, void *data);

#endif

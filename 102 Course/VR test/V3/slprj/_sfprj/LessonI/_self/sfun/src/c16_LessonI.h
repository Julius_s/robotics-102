#ifndef __c16_LessonI_h__
#define __c16_LessonI_h__

/* Include files */
#include "sf_runtime/sfc_sf.h"
#include "sf_runtime/sfc_mex.h"
#include "rtwtypes.h"
#include "multiword_types.h"

/* Type Definitions */
#ifndef struct_Waypoint_tag
#define struct_Waypoint_tag

struct Waypoint_tag
{
  int8_T x;
  int8_T y;
  int16_T orientation;
};

#endif                                 /*struct_Waypoint_tag*/

#ifndef typedef_c16_Waypoint
#define typedef_c16_Waypoint

typedef struct Waypoint_tag c16_Waypoint;

#endif                                 /*typedef_c16_Waypoint*/

#ifndef typedef_SFc16_LessonIInstanceStruct
#define typedef_SFc16_LessonIInstanceStruct

typedef struct {
  SimStruct *S;
  ChartInfoStruct chartInfo;
  uint32_T chartNumber;
  uint32_T instanceNumber;
  int32_T c16_sfEvent;
  boolean_T c16_isStable;
  boolean_T c16_doneDoubleBufferReInit;
  uint8_T c16_is_active_c16_LessonI;
  real32_T (*c16_data)[3];
  c16_Waypoint *c16_way;
} SFc16_LessonIInstanceStruct;

#endif                                 /*typedef_SFc16_LessonIInstanceStruct*/

/* Named Constants */

/* Variable Declarations */
extern struct SfDebugInstanceStruct *sfGlobalDebugInstanceStruct;

/* Variable Definitions */

/* Function Declarations */
extern const mxArray *sf_c16_LessonI_get_eml_resolved_functions_info(void);

/* Function Definitions */
extern void sf_c16_LessonI_get_check_sum(mxArray *plhs[]);
extern void c16_LessonI_method_dispatcher(SimStruct *S, int_T method, void *data);

#endif

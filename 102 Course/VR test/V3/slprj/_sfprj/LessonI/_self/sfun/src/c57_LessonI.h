#ifndef __c57_LessonI_h__
#define __c57_LessonI_h__

/* Include files */
#include "sf_runtime/sfc_sf.h"
#include "sf_runtime/sfc_mex.h"
#include "rtwtypes.h"
#include "multiword_types.h"

/* Type Definitions */
#ifndef typedef_SFc57_LessonIInstanceStruct
#define typedef_SFc57_LessonIInstanceStruct

typedef struct {
  SimStruct *S;
  ChartInfoStruct chartInfo;
  uint32_T chartNumber;
  uint32_T instanceNumber;
  int32_T c57_sfEvent;
  boolean_T c57_isStable;
  boolean_T c57_doneDoubleBufferReInit;
  uint8_T c57_is_active_c57_LessonI;
  int16_T *c57_targetO;
  int16_T *c57_realDorien;
  int16_T *c57_direc;
  int16_T *c57_myO;
} SFc57_LessonIInstanceStruct;

#endif                                 /*typedef_SFc57_LessonIInstanceStruct*/

/* Named Constants */

/* Variable Declarations */
extern struct SfDebugInstanceStruct *sfGlobalDebugInstanceStruct;

/* Variable Definitions */

/* Function Declarations */
extern const mxArray *sf_c57_LessonI_get_eml_resolved_functions_info(void);

/* Function Definitions */
extern void sf_c57_LessonI_get_check_sum(mxArray *plhs[]);
extern void c57_LessonI_method_dispatcher(SimStruct *S, int_T method, void *data);

#endif

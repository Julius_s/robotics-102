/* Include files */

#include <stddef.h>
#include "blas.h"
#include "LessonI_sfun.h"
#include "c46_LessonI.h"
#define CHARTINSTANCE_CHARTNUMBER      (chartInstance->chartNumber)
#define CHARTINSTANCE_INSTANCENUMBER   (chartInstance->instanceNumber)
#include "LessonI_sfun_debug_macros.h"
#define _SF_MEX_LISTEN_FOR_CTRL_C(S)   sf_mex_listen_for_ctrl_c(sfGlobalDebugInstanceStruct,S);

/* Type Definitions */

/* Named Constants */
#define CALL_EVENT                     (-1)

/* Variable Declarations */

/* Variable Definitions */
static real_T _sfTime_;
static const char * c46_debug_family_names[4] = { "nargin", "nargout",
  "messageBuffer", "message" };

/* Function Declarations */
static void initialize_c46_LessonI(SFc46_LessonIInstanceStruct *chartInstance);
static void initialize_params_c46_LessonI(SFc46_LessonIInstanceStruct
  *chartInstance);
static void enable_c46_LessonI(SFc46_LessonIInstanceStruct *chartInstance);
static void disable_c46_LessonI(SFc46_LessonIInstanceStruct *chartInstance);
static void c46_update_debugger_state_c46_LessonI(SFc46_LessonIInstanceStruct
  *chartInstance);
static const mxArray *get_sim_state_c46_LessonI(SFc46_LessonIInstanceStruct
  *chartInstance);
static void set_sim_state_c46_LessonI(SFc46_LessonIInstanceStruct *chartInstance,
  const mxArray *c46_st);
static void finalize_c46_LessonI(SFc46_LessonIInstanceStruct *chartInstance);
static void sf_gateway_c46_LessonI(SFc46_LessonIInstanceStruct *chartInstance);
static void mdl_start_c46_LessonI(SFc46_LessonIInstanceStruct *chartInstance);
static void initSimStructsc46_LessonI(SFc46_LessonIInstanceStruct *chartInstance);
static void init_script_number_translation(uint32_T c46_machineNumber, uint32_T
  c46_chartNumber, uint32_T c46_instanceNumber);
static const mxArray *c46_sf_marshallOut(void *chartInstanceVoid, void
  *c46_inData);
static void c46_emlrt_marshallIn(SFc46_LessonIInstanceStruct *chartInstance,
  const mxArray *c46_b_message, const char_T *c46_identifier, uint8_T c46_y[31]);
static void c46_b_emlrt_marshallIn(SFc46_LessonIInstanceStruct *chartInstance,
  const mxArray *c46_u, const emlrtMsgIdentifier *c46_parentId, uint8_T c46_y[31]);
static void c46_sf_marshallIn(void *chartInstanceVoid, const mxArray
  *c46_mxArrayInData, const char_T *c46_varName, void *c46_outData);
static const mxArray *c46_b_sf_marshallOut(void *chartInstanceVoid, uint8_T
  c46_inData_data[], int32_T c46_inData_sizes[2]);
static const mxArray *c46_c_sf_marshallOut(void *chartInstanceVoid, void
  *c46_inData);
static real_T c46_c_emlrt_marshallIn(SFc46_LessonIInstanceStruct *chartInstance,
  const mxArray *c46_u, const emlrtMsgIdentifier *c46_parentId);
static void c46_b_sf_marshallIn(void *chartInstanceVoid, const mxArray
  *c46_mxArrayInData, const char_T *c46_varName, void *c46_outData);
static const mxArray *c46_d_sf_marshallOut(void *chartInstanceVoid, void
  *c46_inData);
static int32_T c46_d_emlrt_marshallIn(SFc46_LessonIInstanceStruct *chartInstance,
  const mxArray *c46_u, const emlrtMsgIdentifier *c46_parentId);
static void c46_c_sf_marshallIn(void *chartInstanceVoid, const mxArray
  *c46_mxArrayInData, const char_T *c46_varName, void *c46_outData);
static uint8_T c46_e_emlrt_marshallIn(SFc46_LessonIInstanceStruct *chartInstance,
  const mxArray *c46_b_is_active_c46_LessonI, const char_T *c46_identifier);
static uint8_T c46_f_emlrt_marshallIn(SFc46_LessonIInstanceStruct *chartInstance,
  const mxArray *c46_u, const emlrtMsgIdentifier *c46_parentId);
static void init_dsm_address_info(SFc46_LessonIInstanceStruct *chartInstance);
static void init_simulink_io_address(SFc46_LessonIInstanceStruct *chartInstance);

/* Function Definitions */
static void initialize_c46_LessonI(SFc46_LessonIInstanceStruct *chartInstance)
{
  chartInstance->c46_sfEvent = CALL_EVENT;
  _sfTime_ = sf_get_time(chartInstance->S);
  chartInstance->c46_is_active_c46_LessonI = 0U;
}

static void initialize_params_c46_LessonI(SFc46_LessonIInstanceStruct
  *chartInstance)
{
  (void)chartInstance;
}

static void enable_c46_LessonI(SFc46_LessonIInstanceStruct *chartInstance)
{
  _sfTime_ = sf_get_time(chartInstance->S);
}

static void disable_c46_LessonI(SFc46_LessonIInstanceStruct *chartInstance)
{
  _sfTime_ = sf_get_time(chartInstance->S);
}

static void c46_update_debugger_state_c46_LessonI(SFc46_LessonIInstanceStruct
  *chartInstance)
{
  (void)chartInstance;
}

static const mxArray *get_sim_state_c46_LessonI(SFc46_LessonIInstanceStruct
  *chartInstance)
{
  const mxArray *c46_st;
  const mxArray *c46_y = NULL;
  int32_T c46_i0;
  uint8_T c46_u[31];
  const mxArray *c46_b_y = NULL;
  uint8_T c46_hoistedGlobal;
  uint8_T c46_b_u;
  const mxArray *c46_c_y = NULL;
  c46_st = NULL;
  c46_st = NULL;
  c46_y = NULL;
  sf_mex_assign(&c46_y, sf_mex_createcellmatrix(2, 1), false);
  for (c46_i0 = 0; c46_i0 < 31; c46_i0++) {
    c46_u[c46_i0] = (*chartInstance->c46_message)[c46_i0];
  }

  c46_b_y = NULL;
  sf_mex_assign(&c46_b_y, sf_mex_create("y", c46_u, 3, 0U, 1U, 0U, 2, 1, 31),
                false);
  sf_mex_setcell(c46_y, 0, c46_b_y);
  c46_hoistedGlobal = chartInstance->c46_is_active_c46_LessonI;
  c46_b_u = c46_hoistedGlobal;
  c46_c_y = NULL;
  sf_mex_assign(&c46_c_y, sf_mex_create("y", &c46_b_u, 3, 0U, 0U, 0U, 0), false);
  sf_mex_setcell(c46_y, 1, c46_c_y);
  sf_mex_assign(&c46_st, c46_y, false);
  return c46_st;
}

static void set_sim_state_c46_LessonI(SFc46_LessonIInstanceStruct *chartInstance,
  const mxArray *c46_st)
{
  const mxArray *c46_u;
  uint8_T c46_uv0[31];
  int32_T c46_i1;
  chartInstance->c46_doneDoubleBufferReInit = true;
  c46_u = sf_mex_dup(c46_st);
  c46_emlrt_marshallIn(chartInstance, sf_mex_dup(sf_mex_getcell(c46_u, 0)),
                       "message", c46_uv0);
  for (c46_i1 = 0; c46_i1 < 31; c46_i1++) {
    (*chartInstance->c46_message)[c46_i1] = c46_uv0[c46_i1];
  }

  chartInstance->c46_is_active_c46_LessonI = c46_e_emlrt_marshallIn
    (chartInstance, sf_mex_dup(sf_mex_getcell(c46_u, 1)),
     "is_active_c46_LessonI");
  sf_mex_destroy(&c46_u);
  c46_update_debugger_state_c46_LessonI(chartInstance);
  sf_mex_destroy(&c46_st);
}

static void finalize_c46_LessonI(SFc46_LessonIInstanceStruct *chartInstance)
{
  (void)chartInstance;
}

static void sf_gateway_c46_LessonI(SFc46_LessonIInstanceStruct *chartInstance)
{
  int32_T c46_loop_ub;
  int32_T c46_i2;
  int32_T c46_b_messageBuffer_sizes[2];
  int32_T c46_messageBuffer;
  int32_T c46_b_messageBuffer;
  int32_T c46_b_loop_ub;
  int32_T c46_i3;
  uint8_T c46_b_messageBuffer_data[64];
  uint32_T c46_debug_family_var_map[4];
  real_T c46_nargin = 1.0;
  real_T c46_nargout = 1.0;
  uint8_T c46_b_message[31];
  int32_T c46_i4;
  int32_T c46_i5;
  int32_T c46_i6;
  _SFD_SYMBOL_SCOPE_PUSH(0U, 0U);
  _sfTime_ = sf_get_time(chartInstance->S);
  _SFD_CC_CALL(CHART_ENTER_SFUNCTION_TAG, 28U, chartInstance->c46_sfEvent);
  c46_loop_ub = (*chartInstance->c46_messageBuffer_sizes)[0] *
    (*chartInstance->c46_messageBuffer_sizes)[1] - 1;
  for (c46_i2 = 0; c46_i2 <= c46_loop_ub; c46_i2++) {
    _SFD_DATA_RANGE_CHECK((real_T)(*chartInstance->c46_messageBuffer_data)
                          [c46_i2], 0U);
  }

  chartInstance->c46_sfEvent = CALL_EVENT;
  _SFD_CC_CALL(CHART_ENTER_DURING_FUNCTION_TAG, 28U, chartInstance->c46_sfEvent);
  c46_b_messageBuffer_sizes[0] = 1;
  c46_b_messageBuffer_sizes[1] = (*chartInstance->c46_messageBuffer_sizes)[1];
  c46_messageBuffer = c46_b_messageBuffer_sizes[0];
  c46_b_messageBuffer = c46_b_messageBuffer_sizes[1];
  c46_b_loop_ub = (*chartInstance->c46_messageBuffer_sizes)[0] *
    (*chartInstance->c46_messageBuffer_sizes)[1] - 1;
  for (c46_i3 = 0; c46_i3 <= c46_b_loop_ub; c46_i3++) {
    c46_b_messageBuffer_data[c46_i3] = (*chartInstance->c46_messageBuffer_data)
      [c46_i3];
  }

  _SFD_SYMBOL_SCOPE_PUSH_EML(0U, 4U, 4U, c46_debug_family_names,
    c46_debug_family_var_map);
  _SFD_SYMBOL_SCOPE_ADD_EML_IMPORTABLE(&c46_nargin, 0U, c46_c_sf_marshallOut,
    c46_b_sf_marshallIn);
  _SFD_SYMBOL_SCOPE_ADD_EML_IMPORTABLE(&c46_nargout, 1U, c46_c_sf_marshallOut,
    c46_b_sf_marshallIn);
  _SFD_SYMBOL_SCOPE_ADD_EML_DYN(c46_b_messageBuffer_data, (const int32_T *)
    &c46_b_messageBuffer_sizes, NULL, 1, 2, (void *)c46_b_sf_marshallOut);
  _SFD_SYMBOL_SCOPE_ADD_EML_IMPORTABLE(c46_b_message, 3U, c46_sf_marshallOut,
    c46_sf_marshallIn);
  CV_EML_FCN(0, 0);
  _SFD_EML_CALL(0U, chartInstance->c46_sfEvent, 2);
  for (c46_i4 = 0; c46_i4 < 31; c46_i4++) {
    c46_b_message[c46_i4] = c46_b_messageBuffer_data[_SFD_EML_ARRAY_BOUNDS_CHECK
      ("messageBuffer", c46_i4 + 1, 1, c46_b_messageBuffer_sizes[1], 1, 0) - 1];
  }

  _SFD_EML_CALL(0U, chartInstance->c46_sfEvent, -2);
  _SFD_SYMBOL_SCOPE_POP();
  for (c46_i5 = 0; c46_i5 < 31; c46_i5++) {
    (*chartInstance->c46_message)[c46_i5] = c46_b_message[c46_i5];
  }

  _SFD_CC_CALL(EXIT_OUT_OF_FUNCTION_TAG, 28U, chartInstance->c46_sfEvent);
  _SFD_SYMBOL_SCOPE_POP();
  _SFD_CHECK_FOR_STATE_INCONSISTENCY(_LessonIMachineNumber_,
    chartInstance->chartNumber, chartInstance->instanceNumber);
  for (c46_i6 = 0; c46_i6 < 31; c46_i6++) {
    _SFD_DATA_RANGE_CHECK((real_T)(*chartInstance->c46_message)[c46_i6], 1U);
  }
}

static void mdl_start_c46_LessonI(SFc46_LessonIInstanceStruct *chartInstance)
{
  (void)chartInstance;
}

static void initSimStructsc46_LessonI(SFc46_LessonIInstanceStruct *chartInstance)
{
  (void)chartInstance;
}

static void init_script_number_translation(uint32_T c46_machineNumber, uint32_T
  c46_chartNumber, uint32_T c46_instanceNumber)
{
  (void)c46_machineNumber;
  (void)c46_chartNumber;
  (void)c46_instanceNumber;
}

static const mxArray *c46_sf_marshallOut(void *chartInstanceVoid, void
  *c46_inData)
{
  const mxArray *c46_mxArrayOutData = NULL;
  int32_T c46_i7;
  uint8_T c46_b_inData[31];
  int32_T c46_i8;
  uint8_T c46_u[31];
  const mxArray *c46_y = NULL;
  SFc46_LessonIInstanceStruct *chartInstance;
  chartInstance = (SFc46_LessonIInstanceStruct *)chartInstanceVoid;
  c46_mxArrayOutData = NULL;
  for (c46_i7 = 0; c46_i7 < 31; c46_i7++) {
    c46_b_inData[c46_i7] = (*(uint8_T (*)[31])c46_inData)[c46_i7];
  }

  for (c46_i8 = 0; c46_i8 < 31; c46_i8++) {
    c46_u[c46_i8] = c46_b_inData[c46_i8];
  }

  c46_y = NULL;
  sf_mex_assign(&c46_y, sf_mex_create("y", c46_u, 3, 0U, 1U, 0U, 2, 1, 31),
                false);
  sf_mex_assign(&c46_mxArrayOutData, c46_y, false);
  return c46_mxArrayOutData;
}

static void c46_emlrt_marshallIn(SFc46_LessonIInstanceStruct *chartInstance,
  const mxArray *c46_b_message, const char_T *c46_identifier, uint8_T c46_y[31])
{
  emlrtMsgIdentifier c46_thisId;
  c46_thisId.fIdentifier = c46_identifier;
  c46_thisId.fParent = NULL;
  c46_b_emlrt_marshallIn(chartInstance, sf_mex_dup(c46_b_message), &c46_thisId,
    c46_y);
  sf_mex_destroy(&c46_b_message);
}

static void c46_b_emlrt_marshallIn(SFc46_LessonIInstanceStruct *chartInstance,
  const mxArray *c46_u, const emlrtMsgIdentifier *c46_parentId, uint8_T c46_y[31])
{
  uint8_T c46_uv1[31];
  int32_T c46_i9;
  (void)chartInstance;
  sf_mex_import(c46_parentId, sf_mex_dup(c46_u), c46_uv1, 1, 3, 0U, 1, 0U, 2, 1,
                31);
  for (c46_i9 = 0; c46_i9 < 31; c46_i9++) {
    c46_y[c46_i9] = c46_uv1[c46_i9];
  }

  sf_mex_destroy(&c46_u);
}

static void c46_sf_marshallIn(void *chartInstanceVoid, const mxArray
  *c46_mxArrayInData, const char_T *c46_varName, void *c46_outData)
{
  const mxArray *c46_b_message;
  const char_T *c46_identifier;
  emlrtMsgIdentifier c46_thisId;
  uint8_T c46_y[31];
  int32_T c46_i10;
  SFc46_LessonIInstanceStruct *chartInstance;
  chartInstance = (SFc46_LessonIInstanceStruct *)chartInstanceVoid;
  c46_b_message = sf_mex_dup(c46_mxArrayInData);
  c46_identifier = c46_varName;
  c46_thisId.fIdentifier = c46_identifier;
  c46_thisId.fParent = NULL;
  c46_b_emlrt_marshallIn(chartInstance, sf_mex_dup(c46_b_message), &c46_thisId,
    c46_y);
  sf_mex_destroy(&c46_b_message);
  for (c46_i10 = 0; c46_i10 < 31; c46_i10++) {
    (*(uint8_T (*)[31])c46_outData)[c46_i10] = c46_y[c46_i10];
  }

  sf_mex_destroy(&c46_mxArrayInData);
}

static const mxArray *c46_b_sf_marshallOut(void *chartInstanceVoid, uint8_T
  c46_inData_data[], int32_T c46_inData_sizes[2])
{
  const mxArray *c46_mxArrayOutData = NULL;
  int32_T c46_u_sizes[2];
  int32_T c46_u;
  int32_T c46_b_u;
  int32_T c46_inData;
  int32_T c46_b_inData;
  int32_T c46_b_inData_sizes;
  int32_T c46_loop_ub;
  int32_T c46_i11;
  uint8_T c46_b_inData_data[64];
  int32_T c46_b_loop_ub;
  int32_T c46_i12;
  uint8_T c46_u_data[64];
  const mxArray *c46_y = NULL;
  SFc46_LessonIInstanceStruct *chartInstance;
  chartInstance = (SFc46_LessonIInstanceStruct *)chartInstanceVoid;
  c46_mxArrayOutData = NULL;
  c46_u_sizes[0] = 1;
  c46_u_sizes[1] = c46_inData_sizes[1];
  c46_u = c46_u_sizes[0];
  c46_b_u = c46_u_sizes[1];
  c46_inData = c46_inData_sizes[0];
  c46_b_inData = c46_inData_sizes[1];
  c46_b_inData_sizes = c46_inData * c46_b_inData;
  c46_loop_ub = c46_inData * c46_b_inData - 1;
  for (c46_i11 = 0; c46_i11 <= c46_loop_ub; c46_i11++) {
    c46_b_inData_data[c46_i11] = c46_inData_data[c46_i11];
  }

  c46_b_loop_ub = c46_b_inData_sizes - 1;
  for (c46_i12 = 0; c46_i12 <= c46_b_loop_ub; c46_i12++) {
    c46_u_data[c46_i12] = c46_b_inData_data[c46_i12];
  }

  c46_y = NULL;
  sf_mex_assign(&c46_y, sf_mex_create("y", c46_u_data, 3, 0U, 1U, 0U, 2,
    c46_u_sizes[0], c46_u_sizes[1]), false);
  sf_mex_assign(&c46_mxArrayOutData, c46_y, false);
  return c46_mxArrayOutData;
}

static const mxArray *c46_c_sf_marshallOut(void *chartInstanceVoid, void
  *c46_inData)
{
  const mxArray *c46_mxArrayOutData = NULL;
  real_T c46_u;
  const mxArray *c46_y = NULL;
  SFc46_LessonIInstanceStruct *chartInstance;
  chartInstance = (SFc46_LessonIInstanceStruct *)chartInstanceVoid;
  c46_mxArrayOutData = NULL;
  c46_u = *(real_T *)c46_inData;
  c46_y = NULL;
  sf_mex_assign(&c46_y, sf_mex_create("y", &c46_u, 0, 0U, 0U, 0U, 0), false);
  sf_mex_assign(&c46_mxArrayOutData, c46_y, false);
  return c46_mxArrayOutData;
}

static real_T c46_c_emlrt_marshallIn(SFc46_LessonIInstanceStruct *chartInstance,
  const mxArray *c46_u, const emlrtMsgIdentifier *c46_parentId)
{
  real_T c46_y;
  real_T c46_d0;
  (void)chartInstance;
  sf_mex_import(c46_parentId, sf_mex_dup(c46_u), &c46_d0, 1, 0, 0U, 0, 0U, 0);
  c46_y = c46_d0;
  sf_mex_destroy(&c46_u);
  return c46_y;
}

static void c46_b_sf_marshallIn(void *chartInstanceVoid, const mxArray
  *c46_mxArrayInData, const char_T *c46_varName, void *c46_outData)
{
  const mxArray *c46_nargout;
  const char_T *c46_identifier;
  emlrtMsgIdentifier c46_thisId;
  real_T c46_y;
  SFc46_LessonIInstanceStruct *chartInstance;
  chartInstance = (SFc46_LessonIInstanceStruct *)chartInstanceVoid;
  c46_nargout = sf_mex_dup(c46_mxArrayInData);
  c46_identifier = c46_varName;
  c46_thisId.fIdentifier = c46_identifier;
  c46_thisId.fParent = NULL;
  c46_y = c46_c_emlrt_marshallIn(chartInstance, sf_mex_dup(c46_nargout),
    &c46_thisId);
  sf_mex_destroy(&c46_nargout);
  *(real_T *)c46_outData = c46_y;
  sf_mex_destroy(&c46_mxArrayInData);
}

const mxArray *sf_c46_LessonI_get_eml_resolved_functions_info(void)
{
  const mxArray *c46_nameCaptureInfo = NULL;
  c46_nameCaptureInfo = NULL;
  sf_mex_assign(&c46_nameCaptureInfo, sf_mex_create("nameCaptureInfo", NULL, 0,
    0U, 1U, 0U, 2, 0, 1), false);
  return c46_nameCaptureInfo;
}

static const mxArray *c46_d_sf_marshallOut(void *chartInstanceVoid, void
  *c46_inData)
{
  const mxArray *c46_mxArrayOutData = NULL;
  int32_T c46_u;
  const mxArray *c46_y = NULL;
  SFc46_LessonIInstanceStruct *chartInstance;
  chartInstance = (SFc46_LessonIInstanceStruct *)chartInstanceVoid;
  c46_mxArrayOutData = NULL;
  c46_u = *(int32_T *)c46_inData;
  c46_y = NULL;
  sf_mex_assign(&c46_y, sf_mex_create("y", &c46_u, 6, 0U, 0U, 0U, 0), false);
  sf_mex_assign(&c46_mxArrayOutData, c46_y, false);
  return c46_mxArrayOutData;
}

static int32_T c46_d_emlrt_marshallIn(SFc46_LessonIInstanceStruct *chartInstance,
  const mxArray *c46_u, const emlrtMsgIdentifier *c46_parentId)
{
  int32_T c46_y;
  int32_T c46_i13;
  (void)chartInstance;
  sf_mex_import(c46_parentId, sf_mex_dup(c46_u), &c46_i13, 1, 6, 0U, 0, 0U, 0);
  c46_y = c46_i13;
  sf_mex_destroy(&c46_u);
  return c46_y;
}

static void c46_c_sf_marshallIn(void *chartInstanceVoid, const mxArray
  *c46_mxArrayInData, const char_T *c46_varName, void *c46_outData)
{
  const mxArray *c46_b_sfEvent;
  const char_T *c46_identifier;
  emlrtMsgIdentifier c46_thisId;
  int32_T c46_y;
  SFc46_LessonIInstanceStruct *chartInstance;
  chartInstance = (SFc46_LessonIInstanceStruct *)chartInstanceVoid;
  c46_b_sfEvent = sf_mex_dup(c46_mxArrayInData);
  c46_identifier = c46_varName;
  c46_thisId.fIdentifier = c46_identifier;
  c46_thisId.fParent = NULL;
  c46_y = c46_d_emlrt_marshallIn(chartInstance, sf_mex_dup(c46_b_sfEvent),
    &c46_thisId);
  sf_mex_destroy(&c46_b_sfEvent);
  *(int32_T *)c46_outData = c46_y;
  sf_mex_destroy(&c46_mxArrayInData);
}

static uint8_T c46_e_emlrt_marshallIn(SFc46_LessonIInstanceStruct *chartInstance,
  const mxArray *c46_b_is_active_c46_LessonI, const char_T *c46_identifier)
{
  uint8_T c46_y;
  emlrtMsgIdentifier c46_thisId;
  c46_thisId.fIdentifier = c46_identifier;
  c46_thisId.fParent = NULL;
  c46_y = c46_f_emlrt_marshallIn(chartInstance, sf_mex_dup
    (c46_b_is_active_c46_LessonI), &c46_thisId);
  sf_mex_destroy(&c46_b_is_active_c46_LessonI);
  return c46_y;
}

static uint8_T c46_f_emlrt_marshallIn(SFc46_LessonIInstanceStruct *chartInstance,
  const mxArray *c46_u, const emlrtMsgIdentifier *c46_parentId)
{
  uint8_T c46_y;
  uint8_T c46_u0;
  (void)chartInstance;
  sf_mex_import(c46_parentId, sf_mex_dup(c46_u), &c46_u0, 1, 3, 0U, 0, 0U, 0);
  c46_y = c46_u0;
  sf_mex_destroy(&c46_u);
  return c46_y;
}

static void init_dsm_address_info(SFc46_LessonIInstanceStruct *chartInstance)
{
  (void)chartInstance;
}

static void init_simulink_io_address(SFc46_LessonIInstanceStruct *chartInstance)
{
  chartInstance->c46_messageBuffer_data = (uint8_T (*)[64])
    ssGetInputPortSignal_wrapper(chartInstance->S, 0);
  chartInstance->c46_messageBuffer_sizes = (int32_T (*)[2])
    ssGetCurrentInputPortDimensions_wrapper(chartInstance->S, 0);
  sf_mex_size_one_check(((*chartInstance->c46_messageBuffer_sizes)[0U] == 0) &&
                        (!((*chartInstance->c46_messageBuffer_sizes)[1U] == 0)),
                        "messageBuffer");
  chartInstance->c46_message = (uint8_T (*)[31])ssGetOutputPortSignal_wrapper
    (chartInstance->S, 1);
}

/* SFunction Glue Code */
#ifdef utFree
#undef utFree
#endif

#ifdef utMalloc
#undef utMalloc
#endif

#ifdef __cplusplus

extern "C" void *utMalloc(size_t size);
extern "C" void utFree(void*);

#else

extern void *utMalloc(size_t size);
extern void utFree(void*);

#endif

void sf_c46_LessonI_get_check_sum(mxArray *plhs[])
{
  ((real_T *)mxGetPr((plhs[0])))[0] = (real_T)(1042695787U);
  ((real_T *)mxGetPr((plhs[0])))[1] = (real_T)(2659539410U);
  ((real_T *)mxGetPr((plhs[0])))[2] = (real_T)(1804896145U);
  ((real_T *)mxGetPr((plhs[0])))[3] = (real_T)(1827938802U);
}

mxArray* sf_c46_LessonI_get_post_codegen_info(void);
mxArray *sf_c46_LessonI_get_autoinheritance_info(void)
{
  const char *autoinheritanceFields[] = { "checksum", "inputs", "parameters",
    "outputs", "locals", "postCodegenInfo" };

  mxArray *mxAutoinheritanceInfo = mxCreateStructMatrix(1, 1, sizeof
    (autoinheritanceFields)/sizeof(autoinheritanceFields[0]),
    autoinheritanceFields);

  {
    mxArray *mxChecksum = mxCreateString("2z7Fwh6WZoLlXSSoEUax0D");
    mxSetField(mxAutoinheritanceInfo,0,"checksum",mxChecksum);
  }

  {
    const char *dataFields[] = { "size", "type", "complexity" };

    mxArray *mxData = mxCreateStructMatrix(1,1,3,dataFields);

    {
      mxArray *mxSize = mxCreateDoubleMatrix(1,2,mxREAL);
      double *pr = mxGetPr(mxSize);
      pr[0] = (double)(1);
      pr[1] = (double)(64);
      mxSetField(mxData,0,"size",mxSize);
    }

    {
      const char *typeFields[] = { "base", "fixpt" };

      mxArray *mxType = mxCreateStructMatrix(1,1,2,typeFields);
      mxSetField(mxType,0,"base",mxCreateDoubleScalar(3));
      mxSetField(mxType,0,"fixpt",mxCreateDoubleMatrix(0,0,mxREAL));
      mxSetField(mxData,0,"type",mxType);
    }

    mxSetField(mxData,0,"complexity",mxCreateDoubleScalar(0));
    mxSetField(mxAutoinheritanceInfo,0,"inputs",mxData);
  }

  {
    mxSetField(mxAutoinheritanceInfo,0,"parameters",mxCreateDoubleMatrix(0,0,
                mxREAL));
  }

  {
    const char *dataFields[] = { "size", "type", "complexity" };

    mxArray *mxData = mxCreateStructMatrix(1,1,3,dataFields);

    {
      mxArray *mxSize = mxCreateDoubleMatrix(1,2,mxREAL);
      double *pr = mxGetPr(mxSize);
      pr[0] = (double)(1);
      pr[1] = (double)(31);
      mxSetField(mxData,0,"size",mxSize);
    }

    {
      const char *typeFields[] = { "base", "fixpt" };

      mxArray *mxType = mxCreateStructMatrix(1,1,2,typeFields);
      mxSetField(mxType,0,"base",mxCreateDoubleScalar(3));
      mxSetField(mxType,0,"fixpt",mxCreateDoubleMatrix(0,0,mxREAL));
      mxSetField(mxData,0,"type",mxType);
    }

    mxSetField(mxData,0,"complexity",mxCreateDoubleScalar(0));
    mxSetField(mxAutoinheritanceInfo,0,"outputs",mxData);
  }

  {
    mxSetField(mxAutoinheritanceInfo,0,"locals",mxCreateDoubleMatrix(0,0,mxREAL));
  }

  {
    mxArray* mxPostCodegenInfo = sf_c46_LessonI_get_post_codegen_info();
    mxSetField(mxAutoinheritanceInfo,0,"postCodegenInfo",mxPostCodegenInfo);
  }

  return(mxAutoinheritanceInfo);
}

mxArray *sf_c46_LessonI_third_party_uses_info(void)
{
  mxArray * mxcell3p = mxCreateCellMatrix(1,0);
  return(mxcell3p);
}

mxArray *sf_c46_LessonI_jit_fallback_info(void)
{
  const char *infoFields[] = { "fallbackType", "fallbackReason",
    "incompatibleSymbol", };

  mxArray *mxInfo = mxCreateStructMatrix(1, 1, 3, infoFields);
  mxArray *fallbackReason = mxCreateString("feature_off");
  mxArray *incompatibleSymbol = mxCreateString("");
  mxArray *fallbackType = mxCreateString("early");
  mxSetField(mxInfo, 0, infoFields[0], fallbackType);
  mxSetField(mxInfo, 0, infoFields[1], fallbackReason);
  mxSetField(mxInfo, 0, infoFields[2], incompatibleSymbol);
  return mxInfo;
}

mxArray *sf_c46_LessonI_updateBuildInfo_args_info(void)
{
  mxArray *mxBIArgs = mxCreateCellMatrix(1,0);
  return mxBIArgs;
}

mxArray* sf_c46_LessonI_get_post_codegen_info(void)
{
  const char* fieldNames[] = { "exportedFunctionsUsedByThisChart",
    "exportedFunctionsChecksum" };

  mwSize dims[2] = { 1, 1 };

  mxArray* mxPostCodegenInfo = mxCreateStructArray(2, dims, sizeof(fieldNames)/
    sizeof(fieldNames[0]), fieldNames);

  {
    mxArray* mxExportedFunctionsChecksum = mxCreateString("");
    mwSize exp_dims[2] = { 0, 1 };

    mxArray* mxExportedFunctionsUsedByThisChart = mxCreateCellArray(2, exp_dims);
    mxSetField(mxPostCodegenInfo, 0, "exportedFunctionsUsedByThisChart",
               mxExportedFunctionsUsedByThisChart);
    mxSetField(mxPostCodegenInfo, 0, "exportedFunctionsChecksum",
               mxExportedFunctionsChecksum);
  }

  return mxPostCodegenInfo;
}

static const mxArray *sf_get_sim_state_info_c46_LessonI(void)
{
  const char *infoFields[] = { "chartChecksum", "varInfo" };

  mxArray *mxInfo = mxCreateStructMatrix(1, 1, 2, infoFields);
  const char *infoEncStr[] = {
    "100 S1x2'type','srcId','name','auxInfo'{{M[1],M[5],T\"message\",},{M[8],M[0],T\"is_active_c46_LessonI\",}}"
  };

  mxArray *mxVarInfo = sf_mex_decode_encoded_mx_struct_array(infoEncStr, 2, 10);
  mxArray *mxChecksum = mxCreateDoubleMatrix(1, 4, mxREAL);
  sf_c46_LessonI_get_check_sum(&mxChecksum);
  mxSetField(mxInfo, 0, infoFields[0], mxChecksum);
  mxSetField(mxInfo, 0, infoFields[1], mxVarInfo);
  return mxInfo;
}

static void chart_debug_initialization(SimStruct *S, unsigned int
  fullDebuggerInitialization)
{
  if (!sim_mode_is_rtw_gen(S)) {
    SFc46_LessonIInstanceStruct *chartInstance;
    ChartRunTimeInfo * crtInfo = (ChartRunTimeInfo *)(ssGetUserData(S));
    ChartInfoStruct * chartInfo = (ChartInfoStruct *)(crtInfo->instanceInfo);
    chartInstance = (SFc46_LessonIInstanceStruct *) chartInfo->chartInstance;
    if (ssIsFirstInitCond(S) && fullDebuggerInitialization==1) {
      /* do this only if simulation is starting */
      {
        unsigned int chartAlreadyPresent;
        chartAlreadyPresent = sf_debug_initialize_chart
          (sfGlobalDebugInstanceStruct,
           _LessonIMachineNumber_,
           46,
           1,
           1,
           0,
           2,
           0,
           0,
           0,
           0,
           0,
           &(chartInstance->chartNumber),
           &(chartInstance->instanceNumber),
           (void *)S);

        /* Each instance must initialize its own list of scripts */
        init_script_number_translation(_LessonIMachineNumber_,
          chartInstance->chartNumber,chartInstance->instanceNumber);
        if (chartAlreadyPresent==0) {
          /* this is the first instance */
          sf_debug_set_chart_disable_implicit_casting
            (sfGlobalDebugInstanceStruct,_LessonIMachineNumber_,
             chartInstance->chartNumber,1);
          sf_debug_set_chart_event_thresholds(sfGlobalDebugInstanceStruct,
            _LessonIMachineNumber_,
            chartInstance->chartNumber,
            0,
            0,
            0);
          _SFD_SET_DATA_PROPS(0,1,1,0,"messageBuffer");
          _SFD_SET_DATA_PROPS(1,2,0,1,"message");
          _SFD_STATE_INFO(0,0,2);
          _SFD_CH_SUBSTATE_COUNT(0);
          _SFD_CH_SUBSTATE_DECOMP(0);
        }

        _SFD_CV_INIT_CHART(0,0,0,0);

        {
          _SFD_CV_INIT_STATE(0,0,0,0,0,0,NULL,NULL);
        }

        _SFD_CV_INIT_TRANS(0,0,NULL,NULL,0,NULL);

        /* Initialization of MATLAB Function Model Coverage */
        _SFD_CV_INIT_EML(0,1,1,0,0,0,0,0,0,0,0);
        _SFD_CV_INIT_EML_FCN(0,0,"eML_blk_kernel",0,-1,81);

        {
          unsigned int dimVector[2];
          dimVector[0]= 1;
          dimVector[1]= 64;
          _SFD_SET_DATA_COMPILED_PROPS(0,SF_UINT8,2,&(dimVector[0]),0,0,0,0.0,
            1.0,0,0,(MexFcnForType)c46_b_sf_marshallOut,(MexInFcnForType)NULL);
        }

        {
          unsigned int dimVector[2];
          dimVector[0]= 1;
          dimVector[1]= 31;
          _SFD_SET_DATA_COMPILED_PROPS(1,SF_UINT8,2,&(dimVector[0]),0,0,0,0.0,
            1.0,0,0,(MexFcnForType)c46_sf_marshallOut,(MexInFcnForType)
            c46_sf_marshallIn);
        }

        _SFD_SET_DATA_VALUE_PTR_VAR_DIM(0U,
          *chartInstance->c46_messageBuffer_data, (void *)
          chartInstance->c46_messageBuffer_sizes);
        _SFD_SET_DATA_VALUE_PTR(1U, *chartInstance->c46_message);
      }
    } else {
      sf_debug_reset_current_state_configuration(sfGlobalDebugInstanceStruct,
        _LessonIMachineNumber_,chartInstance->chartNumber,
        chartInstance->instanceNumber);
    }
  }
}

static const char* sf_get_instance_specialization(void)
{
  return "xNYm6GNN8jSBExJBkcuC2B";
}

static void sf_opaque_initialize_c46_LessonI(void *chartInstanceVar)
{
  chart_debug_initialization(((SFc46_LessonIInstanceStruct*) chartInstanceVar)
    ->S,0);
  initialize_params_c46_LessonI((SFc46_LessonIInstanceStruct*) chartInstanceVar);
  initialize_c46_LessonI((SFc46_LessonIInstanceStruct*) chartInstanceVar);
}

static void sf_opaque_enable_c46_LessonI(void *chartInstanceVar)
{
  enable_c46_LessonI((SFc46_LessonIInstanceStruct*) chartInstanceVar);
}

static void sf_opaque_disable_c46_LessonI(void *chartInstanceVar)
{
  disable_c46_LessonI((SFc46_LessonIInstanceStruct*) chartInstanceVar);
}

static void sf_opaque_gateway_c46_LessonI(void *chartInstanceVar)
{
  sf_gateway_c46_LessonI((SFc46_LessonIInstanceStruct*) chartInstanceVar);
}

static const mxArray* sf_opaque_get_sim_state_c46_LessonI(SimStruct* S)
{
  ChartRunTimeInfo * crtInfo = (ChartRunTimeInfo *)(ssGetUserData(S));
  ChartInfoStruct * chartInfo = (ChartInfoStruct *)(crtInfo->instanceInfo);
  return get_sim_state_c46_LessonI((SFc46_LessonIInstanceStruct*)
    chartInfo->chartInstance);         /* raw sim ctx */
}

static void sf_opaque_set_sim_state_c46_LessonI(SimStruct* S, const mxArray *st)
{
  ChartRunTimeInfo * crtInfo = (ChartRunTimeInfo *)(ssGetUserData(S));
  ChartInfoStruct * chartInfo = (ChartInfoStruct *)(crtInfo->instanceInfo);
  set_sim_state_c46_LessonI((SFc46_LessonIInstanceStruct*)
    chartInfo->chartInstance, st);
}

static void sf_opaque_terminate_c46_LessonI(void *chartInstanceVar)
{
  if (chartInstanceVar!=NULL) {
    SimStruct *S = ((SFc46_LessonIInstanceStruct*) chartInstanceVar)->S;
    ChartRunTimeInfo * crtInfo = (ChartRunTimeInfo *)(ssGetUserData(S));
    if (sim_mode_is_rtw_gen(S) || sim_mode_is_external(S)) {
      sf_clear_rtw_identifier(S);
      unload_LessonI_optimization_info();
    }

    finalize_c46_LessonI((SFc46_LessonIInstanceStruct*) chartInstanceVar);
    utFree(chartInstanceVar);
    if (crtInfo != NULL) {
      utFree(crtInfo);
    }

    ssSetUserData(S,NULL);
  }
}

static void sf_opaque_init_subchart_simstructs(void *chartInstanceVar)
{
  initSimStructsc46_LessonI((SFc46_LessonIInstanceStruct*) chartInstanceVar);
}

extern unsigned int sf_machine_global_initializer_called(void);
static void mdlProcessParameters_c46_LessonI(SimStruct *S)
{
  int i;
  for (i=0;i<ssGetNumRunTimeParams(S);i++) {
    if (ssGetSFcnParamTunable(S,i)) {
      ssUpdateDlgParamAsRunTimeParam(S,i);
    }
  }

  if (sf_machine_global_initializer_called()) {
    ChartRunTimeInfo * crtInfo = (ChartRunTimeInfo *)(ssGetUserData(S));
    ChartInfoStruct * chartInfo = (ChartInfoStruct *)(crtInfo->instanceInfo);
    initialize_params_c46_LessonI((SFc46_LessonIInstanceStruct*)
      (chartInfo->chartInstance));
  }
}

static void mdlSetWorkWidths_c46_LessonI(SimStruct *S)
{
  if (sim_mode_is_rtw_gen(S) || sim_mode_is_external(S)) {
    mxArray *infoStruct = load_LessonI_optimization_info();
    int_T chartIsInlinable =
      (int_T)sf_is_chart_inlinable(sf_get_instance_specialization(),infoStruct,
      46);
    ssSetStateflowIsInlinable(S,chartIsInlinable);
    ssSetRTWCG(S,sf_rtw_info_uint_prop(sf_get_instance_specialization(),
                infoStruct,46,"RTWCG"));
    ssSetEnableFcnIsTrivial(S,1);
    ssSetDisableFcnIsTrivial(S,1);
    ssSetNotMultipleInlinable(S,sf_rtw_info_uint_prop
      (sf_get_instance_specialization(),infoStruct,46,
       "gatewayCannotBeInlinedMultipleTimes"));
    sf_update_buildInfo(sf_get_instance_specialization(),infoStruct,46);
    if (chartIsInlinable) {
      ssSetInputPortOptimOpts(S, 0, SS_REUSABLE_AND_LOCAL);
      sf_mark_chart_expressionable_inputs(S,sf_get_instance_specialization(),
        infoStruct,46,1);
      sf_mark_chart_reusable_outputs(S,sf_get_instance_specialization(),
        infoStruct,46,1);
    }

    {
      unsigned int outPortIdx;
      for (outPortIdx=1; outPortIdx<=1; ++outPortIdx) {
        ssSetOutputPortOptimizeInIR(S, outPortIdx, 1U);
      }
    }

    {
      unsigned int inPortIdx;
      for (inPortIdx=0; inPortIdx < 1; ++inPortIdx) {
        ssSetInputPortOptimizeInIR(S, inPortIdx, 1U);
      }
    }

    sf_set_rtw_dwork_info(S,sf_get_instance_specialization(),infoStruct,46);
    ssSetHasSubFunctions(S,!(chartIsInlinable));
  } else {
  }

  ssSetOptions(S,ssGetOptions(S)|SS_OPTION_WORKS_WITH_CODE_REUSE);
  ssSetChecksum0(S,(2041924168U));
  ssSetChecksum1(S,(2094826546U));
  ssSetChecksum2(S,(3970551402U));
  ssSetChecksum3(S,(769607429U));
  ssSetmdlDerivatives(S, NULL);
  ssSetExplicitFCSSCtrl(S,1);
  ssSupportsMultipleExecInstances(S,1);
}

static void mdlRTW_c46_LessonI(SimStruct *S)
{
  if (sim_mode_is_rtw_gen(S)) {
    ssWriteRTWStrParam(S, "StateflowChartType", "Embedded MATLAB");
  }
}

static void mdlStart_c46_LessonI(SimStruct *S)
{
  SFc46_LessonIInstanceStruct *chartInstance;
  ChartRunTimeInfo * crtInfo = (ChartRunTimeInfo *)utMalloc(sizeof
    (ChartRunTimeInfo));
  chartInstance = (SFc46_LessonIInstanceStruct *)utMalloc(sizeof
    (SFc46_LessonIInstanceStruct));
  memset(chartInstance, 0, sizeof(SFc46_LessonIInstanceStruct));
  if (chartInstance==NULL) {
    sf_mex_error_message("Could not allocate memory for chart instance.");
  }

  chartInstance->chartInfo.chartInstance = chartInstance;
  chartInstance->chartInfo.isEMLChart = 1;
  chartInstance->chartInfo.chartInitialized = 0;
  chartInstance->chartInfo.sFunctionGateway = sf_opaque_gateway_c46_LessonI;
  chartInstance->chartInfo.initializeChart = sf_opaque_initialize_c46_LessonI;
  chartInstance->chartInfo.terminateChart = sf_opaque_terminate_c46_LessonI;
  chartInstance->chartInfo.enableChart = sf_opaque_enable_c46_LessonI;
  chartInstance->chartInfo.disableChart = sf_opaque_disable_c46_LessonI;
  chartInstance->chartInfo.getSimState = sf_opaque_get_sim_state_c46_LessonI;
  chartInstance->chartInfo.setSimState = sf_opaque_set_sim_state_c46_LessonI;
  chartInstance->chartInfo.getSimStateInfo = sf_get_sim_state_info_c46_LessonI;
  chartInstance->chartInfo.zeroCrossings = NULL;
  chartInstance->chartInfo.outputs = NULL;
  chartInstance->chartInfo.derivatives = NULL;
  chartInstance->chartInfo.mdlRTW = mdlRTW_c46_LessonI;
  chartInstance->chartInfo.mdlStart = mdlStart_c46_LessonI;
  chartInstance->chartInfo.mdlSetWorkWidths = mdlSetWorkWidths_c46_LessonI;
  chartInstance->chartInfo.extModeExec = NULL;
  chartInstance->chartInfo.restoreLastMajorStepConfiguration = NULL;
  chartInstance->chartInfo.restoreBeforeLastMajorStepConfiguration = NULL;
  chartInstance->chartInfo.storeCurrentConfiguration = NULL;
  chartInstance->chartInfo.callAtomicSubchartUserFcn = NULL;
  chartInstance->chartInfo.callAtomicSubchartAutoFcn = NULL;
  chartInstance->chartInfo.debugInstance = sfGlobalDebugInstanceStruct;
  chartInstance->S = S;
  crtInfo->checksum = SF_RUNTIME_INFO_CHECKSUM;
  crtInfo->instanceInfo = (&(chartInstance->chartInfo));
  crtInfo->isJITEnabled = false;
  crtInfo->compiledInfo = NULL;
  ssSetUserData(S,(void *)(crtInfo));  /* register the chart instance with simstruct */
  init_dsm_address_info(chartInstance);
  init_simulink_io_address(chartInstance);
  if (!sim_mode_is_rtw_gen(S)) {
  }

  sf_opaque_init_subchart_simstructs(chartInstance->chartInfo.chartInstance);
  chart_debug_initialization(S,1);
}

void c46_LessonI_method_dispatcher(SimStruct *S, int_T method, void *data)
{
  switch (method) {
   case SS_CALL_MDL_START:
    mdlStart_c46_LessonI(S);
    break;

   case SS_CALL_MDL_SET_WORK_WIDTHS:
    mdlSetWorkWidths_c46_LessonI(S);
    break;

   case SS_CALL_MDL_PROCESS_PARAMETERS:
    mdlProcessParameters_c46_LessonI(S);
    break;

   default:
    /* Unhandled method */
    sf_mex_error_message("Stateflow Internal Error:\n"
                         "Error calling c46_LessonI_method_dispatcher.\n"
                         "Can't handle method %d.\n", method);
    break;
  }
}

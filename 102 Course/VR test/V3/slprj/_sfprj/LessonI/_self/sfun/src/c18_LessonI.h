#ifndef __c18_LessonI_h__
#define __c18_LessonI_h__

/* Include files */
#include "sf_runtime/sfc_sf.h"
#include "sf_runtime/sfc_mex.h"
#include "rtwtypes.h"
#include "multiword_types.h"

/* Type Definitions */
#ifndef struct_Player_tag
#define struct_Player_tag

struct Player_tag
{
  int8_T x;
  int8_T y;
  int16_T orientation;
  uint8_T color;
  uint8_T position;
  uint8_T valid;
};

#endif                                 /*struct_Player_tag*/

#ifndef typedef_c18_Player
#define typedef_c18_Player

typedef struct Player_tag c18_Player;

#endif                                 /*typedef_c18_Player*/

#ifndef struct_Ball_tag
#define struct_Ball_tag

struct Ball_tag
{
  int8_T x;
  int8_T y;
  uint8_T valid;
};

#endif                                 /*struct_Ball_tag*/

#ifndef typedef_c18_Ball
#define typedef_c18_Ball

typedef struct Ball_tag c18_Ball;

#endif                                 /*typedef_c18_Ball*/

#ifndef struct_Waypoint_tag
#define struct_Waypoint_tag

struct Waypoint_tag
{
  int8_T x;
  int8_T y;
  int16_T orientation;
};

#endif                                 /*struct_Waypoint_tag*/

#ifndef typedef_c18_Waypoint
#define typedef_c18_Waypoint

typedef struct Waypoint_tag c18_Waypoint;

#endif                                 /*typedef_c18_Waypoint*/

#ifndef typedef_SFc18_LessonIInstanceStruct
#define typedef_SFc18_LessonIInstanceStruct

typedef struct {
  SimStruct *S;
  ChartInfoStruct chartInfo;
  uint32_T chartNumber;
  uint32_T instanceNumber;
  int32_T c18_sfEvent;
  uint8_T c18_tp_GameIsOn_Goalie;
  uint8_T c18_tp_goalie;
  uint8_T c18_tp_Idle;
  uint8_T c18_tp_waiting;
  uint8_T c18_tp_Idle1;
  uint8_T c18_tp_goToPosition;
  boolean_T c18_isStable;
  uint8_T c18_is_active_c18_LessonI;
  uint8_T c18_is_c18_LessonI;
  uint8_T c18_is_GameIsOn_Goalie;
  uint8_T c18_is_waiting;
  int8_T c18_startingPos[2];
  boolean_T c18_dataWrittenToVector[3];
  uint8_T c18_doSetSimStateSideEffects;
  const mxArray *c18_setSimStateSideEffectsInfo;
  c18_Player *c18_me;
  c18_Player (*c18_players)[6];
  c18_Ball *c18_ball;
  c18_Waypoint *c18_finalWay;
  c18_Waypoint *c18_manualWay;
  uint8_T *c18_GameOn;
} SFc18_LessonIInstanceStruct;

#endif                                 /*typedef_SFc18_LessonIInstanceStruct*/

/* Named Constants */

/* Variable Declarations */
extern struct SfDebugInstanceStruct *sfGlobalDebugInstanceStruct;

/* Variable Definitions */

/* Function Declarations */
extern const mxArray *sf_c18_LessonI_get_eml_resolved_functions_info(void);

/* Function Definitions */
extern void sf_c18_LessonI_get_check_sum(mxArray *plhs[]);
extern void c18_LessonI_method_dispatcher(SimStruct *S, int_T method, void *data);

#endif

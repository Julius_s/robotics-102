#ifndef __c57_LessonII_h__
#define __c57_LessonII_h__

/* Include files */
#include "sf_runtime/sfc_sf.h"
#include "sf_runtime/sfc_mex.h"
#include "rtwtypes.h"
#include "multiword_types.h"

/* Type Definitions */
#ifndef typedef_SFc57_LessonIIInstanceStruct
#define typedef_SFc57_LessonIIInstanceStruct

typedef struct {
  SimStruct *S;
  ChartInfoStruct chartInfo;
  uint32_T chartNumber;
  uint32_T instanceNumber;
  int32_T c57_sfEvent;
  boolean_T c57_isStable;
  boolean_T c57_doneDoubleBufferReInit;
  uint8_T c57_is_active_c57_LessonII;
  int16_T *c57_targetO;
  int16_T *c57_realDorien;
  int16_T *c57_direc;
  int16_T *c57_myO;
} SFc57_LessonIIInstanceStruct;

#endif                                 /*typedef_SFc57_LessonIIInstanceStruct*/

/* Named Constants */

/* Variable Declarations */
extern struct SfDebugInstanceStruct *sfGlobalDebugInstanceStruct;

/* Variable Definitions */

/* Function Declarations */
extern const mxArray *sf_c57_LessonII_get_eml_resolved_functions_info(void);

/* Function Definitions */
extern void sf_c57_LessonII_get_check_sum(mxArray *plhs[]);
extern void c57_LessonII_method_dispatcher(SimStruct *S, int_T method, void
  *data);

#endif

/* Include files */

#include <stddef.h>
#include "blas.h"
#include "LessonII_sfun.h"
#include "c8_LessonII.h"
#include "mwmathutil.h"
#define CHARTINSTANCE_CHARTNUMBER      (chartInstance->chartNumber)
#define CHARTINSTANCE_INSTANCENUMBER   (chartInstance->instanceNumber)
#include "LessonII_sfun_debug_macros.h"
#define _SF_MEX_LISTEN_FOR_CTRL_C(S)   sf_mex_listen_for_ctrl_c(sfGlobalDebugInstanceStruct,S);

/* Type Definitions */

/* Named Constants */
#define CALL_EVENT                     (-1)

/* Variable Declarations */

/* Variable Definitions */
static real_T _sfTime_;
static const char * c8_debug_family_names[15] = { "ii", "pos", "dist", "index",
  "jj", "temp", "tempEmpty", "nargin", "nargout", "playerData", "BallData",
  "players", "ball", "latestPlayers", "latestBall" };

/* Function Declarations */
static void initialize_c8_LessonII(SFc8_LessonIIInstanceStruct *chartInstance);
static void initialize_params_c8_LessonII(SFc8_LessonIIInstanceStruct
  *chartInstance);
static void enable_c8_LessonII(SFc8_LessonIIInstanceStruct *chartInstance);
static void disable_c8_LessonII(SFc8_LessonIIInstanceStruct *chartInstance);
static void c8_update_debugger_state_c8_LessonII(SFc8_LessonIIInstanceStruct
  *chartInstance);
static const mxArray *get_sim_state_c8_LessonII(SFc8_LessonIIInstanceStruct
  *chartInstance);
static void set_sim_state_c8_LessonII(SFc8_LessonIIInstanceStruct *chartInstance,
  const mxArray *c8_st);
static void finalize_c8_LessonII(SFc8_LessonIIInstanceStruct *chartInstance);
static void sf_gateway_c8_LessonII(SFc8_LessonIIInstanceStruct *chartInstance);
static void mdl_start_c8_LessonII(SFc8_LessonIIInstanceStruct *chartInstance);
static void c8_chartstep_c8_LessonII(SFc8_LessonIIInstanceStruct *chartInstance);
static void initSimStructsc8_LessonII(SFc8_LessonIIInstanceStruct *chartInstance);
static void init_script_number_translation(uint32_T c8_machineNumber, uint32_T
  c8_chartNumber, uint32_T c8_instanceNumber);
static const mxArray *c8_sf_marshallOut(void *chartInstanceVoid, void *c8_inData);
static c8_Ball c8_emlrt_marshallIn(SFc8_LessonIIInstanceStruct *chartInstance,
  const mxArray *c8_b_latestBall, const char_T *c8_identifier);
static c8_Ball c8_b_emlrt_marshallIn(SFc8_LessonIIInstanceStruct *chartInstance,
  const mxArray *c8_u, const emlrtMsgIdentifier *c8_parentId);
static int8_T c8_c_emlrt_marshallIn(SFc8_LessonIIInstanceStruct *chartInstance,
  const mxArray *c8_u, const emlrtMsgIdentifier *c8_parentId);
static uint8_T c8_d_emlrt_marshallIn(SFc8_LessonIIInstanceStruct *chartInstance,
  const mxArray *c8_u, const emlrtMsgIdentifier *c8_parentId);
static void c8_sf_marshallIn(void *chartInstanceVoid, const mxArray
  *c8_mxArrayInData, const char_T *c8_varName, void *c8_outData);
static const mxArray *c8_b_sf_marshallOut(void *chartInstanceVoid, void
  *c8_inData);
static c8_stETj7QHScyDJroYDDeQcXH c8_e_emlrt_marshallIn
  (SFc8_LessonIIInstanceStruct *chartInstance, const mxArray *c8_b_latestPlayers,
   const char_T *c8_identifier);
static c8_stETj7QHScyDJroYDDeQcXH c8_f_emlrt_marshallIn
  (SFc8_LessonIIInstanceStruct *chartInstance, const mxArray *c8_u, const
   emlrtMsgIdentifier *c8_parentId);
static void c8_g_emlrt_marshallIn(SFc8_LessonIIInstanceStruct *chartInstance,
  const mxArray *c8_u, const emlrtMsgIdentifier *c8_parentId, int8_T c8_y[6]);
static void c8_h_emlrt_marshallIn(SFc8_LessonIIInstanceStruct *chartInstance,
  const mxArray *c8_u, const emlrtMsgIdentifier *c8_parentId, int16_T c8_y[6]);
static void c8_i_emlrt_marshallIn(SFc8_LessonIIInstanceStruct *chartInstance,
  const mxArray *c8_u, const emlrtMsgIdentifier *c8_parentId, uint8_T c8_y[6]);
static void c8_b_sf_marshallIn(void *chartInstanceVoid, const mxArray
  *c8_mxArrayInData, const char_T *c8_varName, void *c8_outData);
static const mxArray *c8_c_sf_marshallOut(void *chartInstanceVoid, void
  *c8_inData);
static c8_Ball c8_j_emlrt_marshallIn(SFc8_LessonIIInstanceStruct *chartInstance,
  const mxArray *c8_b_ball, const char_T *c8_identifier);
static c8_Ball c8_k_emlrt_marshallIn(SFc8_LessonIIInstanceStruct *chartInstance,
  const mxArray *c8_u, const emlrtMsgIdentifier *c8_parentId);
static void c8_c_sf_marshallIn(void *chartInstanceVoid, const mxArray
  *c8_mxArrayInData, const char_T *c8_varName, void *c8_outData);
static const mxArray *c8_d_sf_marshallOut(void *chartInstanceVoid, void
  *c8_inData);
static void c8_l_emlrt_marshallIn(SFc8_LessonIIInstanceStruct *chartInstance,
  const mxArray *c8_b_players, const char_T *c8_identifier, c8_Player c8_y[6]);
static void c8_m_emlrt_marshallIn(SFc8_LessonIIInstanceStruct *chartInstance,
  const mxArray *c8_u, const emlrtMsgIdentifier *c8_parentId, c8_Player c8_y[6]);
static int16_T c8_n_emlrt_marshallIn(SFc8_LessonIIInstanceStruct *chartInstance,
  const mxArray *c8_u, const emlrtMsgIdentifier *c8_parentId);
static void c8_d_sf_marshallIn(void *chartInstanceVoid, const mxArray
  *c8_mxArrayInData, const char_T *c8_varName, void *c8_outData);
static const mxArray *c8_e_sf_marshallOut(void *chartInstanceVoid, c8_BallData
  *c8_inData_data, c8_BallData_size *c8_inData_elems_sizes);
static const mxArray *c8_f_sf_marshallOut(void *chartInstanceVoid, c8_PlayerData
  *c8_inData_data, c8_PlayerData_size *c8_inData_elems_sizes);
static const mxArray *c8_g_sf_marshallOut(void *chartInstanceVoid, void
  *c8_inData);
static real_T c8_o_emlrt_marshallIn(SFc8_LessonIIInstanceStruct *chartInstance,
  const mxArray *c8_u, const emlrtMsgIdentifier *c8_parentId);
static void c8_e_sf_marshallIn(void *chartInstanceVoid, const mxArray
  *c8_mxArrayInData, const char_T *c8_varName, void *c8_outData);
static const mxArray *c8_h_sf_marshallOut(void *chartInstanceVoid, void
  *c8_inData);
static c8_Player c8_p_emlrt_marshallIn(SFc8_LessonIIInstanceStruct
  *chartInstance, const mxArray *c8_u, const emlrtMsgIdentifier *c8_parentId);
static void c8_f_sf_marshallIn(void *chartInstanceVoid, const mxArray
  *c8_mxArrayInData, const char_T *c8_varName, void *c8_outData);
static const mxArray *c8_i_sf_marshallOut(void *chartInstanceVoid, void
  *c8_inData);
static int32_T c8_q_emlrt_marshallIn(SFc8_LessonIIInstanceStruct *chartInstance,
  const mxArray *c8_u, const emlrtMsgIdentifier *c8_parentId);
static void c8_g_sf_marshallIn(void *chartInstanceVoid, const mxArray
  *c8_mxArrayInData, const char_T *c8_varName, void *c8_outData);
static const mxArray *c8_j_sf_marshallOut(void *chartInstanceVoid, void
  *c8_inData);
static uint8_T c8_r_emlrt_marshallIn(SFc8_LessonIIInstanceStruct *chartInstance,
  const mxArray *c8_index, const char_T *c8_identifier);
static void c8_h_sf_marshallIn(void *chartInstanceVoid, const mxArray
  *c8_mxArrayInData, const char_T *c8_varName, void *c8_outData);
static const mxArray *c8_k_sf_marshallOut(void *chartInstanceVoid, void
  *c8_inData);
static void c8_s_emlrt_marshallIn(SFc8_LessonIIInstanceStruct *chartInstance,
  const mxArray *c8_u, const emlrtMsgIdentifier *c8_parentId, int8_T c8_y[2]);
static void c8_i_sf_marshallIn(void *chartInstanceVoid, const mxArray
  *c8_mxArrayInData, const char_T *c8_varName, void *c8_outData);
static void c8_info_helper(const mxArray **c8_info);
static const mxArray *c8_emlrt_marshallOut(const char * c8_u);
static const mxArray *c8_b_emlrt_marshallOut(const uint32_T c8_u);
static int32_T c8_intlength(SFc8_LessonIIInstanceStruct *chartInstance, int8_T
  c8_x_data[], int32_T c8_x_sizes[2]);
static real32_T c8_eml_xnrm2(SFc8_LessonIIInstanceStruct *chartInstance,
  real32_T c8_x[2]);
static void c8_below_threshold(SFc8_LessonIIInstanceStruct *chartInstance);
static const mxArray *c8_playerData_bus_io(void *chartInstanceVoid, void
  *c8_pData);
static const mxArray *c8_sf_marshall_unsupported(void *chartInstanceVoid);
static const mxArray *c8_c_emlrt_marshallOut(SFc8_LessonIIInstanceStruct
  *chartInstance, const char * c8_u);
static const mxArray *c8_BallData_bus_io(void *chartInstanceVoid, void *c8_pData);
static const mxArray *c8_players_bus_io(void *chartInstanceVoid, void *c8_pData);
static const mxArray *c8_ball_bus_io(void *chartInstanceVoid, void *c8_pData);
static void init_dsm_address_info(SFc8_LessonIIInstanceStruct *chartInstance);
static void init_simulink_io_address(SFc8_LessonIIInstanceStruct *chartInstance);

/* Function Definitions */
static void initialize_c8_LessonII(SFc8_LessonIIInstanceStruct *chartInstance)
{
  chartInstance->c8_sfEvent = CALL_EVENT;
  _sfTime_ = sf_get_time(chartInstance->S);
  chartInstance->c8_latestPlayers_not_empty = false;
  chartInstance->c8_latestBall_not_empty = false;
  chartInstance->c8_is_active_c8_LessonII = 0U;
}

static void initialize_params_c8_LessonII(SFc8_LessonIIInstanceStruct
  *chartInstance)
{
  (void)chartInstance;
}

static void enable_c8_LessonII(SFc8_LessonIIInstanceStruct *chartInstance)
{
  _sfTime_ = sf_get_time(chartInstance->S);
}

static void disable_c8_LessonII(SFc8_LessonIIInstanceStruct *chartInstance)
{
  _sfTime_ = sf_get_time(chartInstance->S);
}

static void c8_update_debugger_state_c8_LessonII(SFc8_LessonIIInstanceStruct
  *chartInstance)
{
  (void)chartInstance;
}

static const mxArray *get_sim_state_c8_LessonII(SFc8_LessonIIInstanceStruct
  *chartInstance)
{
  const mxArray *c8_st;
  const mxArray *c8_y = NULL;
  const mxArray *c8_b_y = NULL;
  int8_T c8_u;
  const mxArray *c8_c_y = NULL;
  int8_T c8_b_u;
  const mxArray *c8_d_y = NULL;
  uint8_T c8_c_u;
  const mxArray *c8_e_y = NULL;
  int32_T c8_i0;
  int32_T c8_i1;
  c8_Player c8_rv0[6];
  int32_T c8_i2;
  c8_Player c8_d_u[6];
  const mxArray *c8_f_y = NULL;
  int32_T c8_i3;
  int32_T c8_iv0[2];
  int32_T c8_i4;
  const c8_Player *c8_r0;
  int8_T c8_e_u;
  const mxArray *c8_g_y = NULL;
  int8_T c8_f_u;
  const mxArray *c8_h_y = NULL;
  int16_T c8_g_u;
  const mxArray *c8_i_y = NULL;
  uint8_T c8_h_u;
  const mxArray *c8_j_y = NULL;
  uint8_T c8_i_u;
  const mxArray *c8_k_y = NULL;
  uint8_T c8_j_u;
  const mxArray *c8_l_y = NULL;
  const mxArray *c8_m_y = NULL;
  int8_T c8_k_u;
  const mxArray *c8_n_y = NULL;
  int8_T c8_l_u;
  const mxArray *c8_o_y = NULL;
  uint8_T c8_m_u;
  const mxArray *c8_p_y = NULL;
  const mxArray *c8_q_y = NULL;
  int32_T c8_i5;
  int8_T c8_n_u[6];
  const mxArray *c8_r_y = NULL;
  int32_T c8_i6;
  int8_T c8_o_u[6];
  const mxArray *c8_s_y = NULL;
  int32_T c8_i7;
  int16_T c8_p_u[6];
  const mxArray *c8_t_y = NULL;
  int32_T c8_i8;
  uint8_T c8_q_u[6];
  const mxArray *c8_u_y = NULL;
  int32_T c8_i9;
  uint8_T c8_r_u[6];
  const mxArray *c8_v_y = NULL;
  int32_T c8_i10;
  uint8_T c8_s_u[6];
  const mxArray *c8_w_y = NULL;
  uint8_T c8_hoistedGlobal;
  uint8_T c8_t_u;
  const mxArray *c8_x_y = NULL;
  c8_st = NULL;
  c8_st = NULL;
  c8_y = NULL;
  sf_mex_assign(&c8_y, sf_mex_createcellmatrix(5, 1), false);
  c8_b_y = NULL;
  sf_mex_assign(&c8_b_y, sf_mex_createstruct("structure", 2, 1, 1), false);
  c8_u = *(int8_T *)&((char_T *)chartInstance->c8_ball)[0];
  c8_c_y = NULL;
  sf_mex_assign(&c8_c_y, sf_mex_create("y", &c8_u, 2, 0U, 0U, 0U, 0), false);
  sf_mex_addfield(c8_b_y, c8_c_y, "x", "x", 0);
  c8_b_u = *(int8_T *)&((char_T *)chartInstance->c8_ball)[1];
  c8_d_y = NULL;
  sf_mex_assign(&c8_d_y, sf_mex_create("y", &c8_b_u, 2, 0U, 0U, 0U, 0), false);
  sf_mex_addfield(c8_b_y, c8_d_y, "y", "y", 0);
  c8_c_u = *(uint8_T *)&((char_T *)chartInstance->c8_ball)[2];
  c8_e_y = NULL;
  sf_mex_assign(&c8_e_y, sf_mex_create("y", &c8_c_u, 3, 0U, 0U, 0U, 0), false);
  sf_mex_addfield(c8_b_y, c8_e_y, "valid", "valid", 0);
  sf_mex_setcell(c8_y, 0, c8_b_y);
  for (c8_i0 = 0; c8_i0 < 6; c8_i0++) {
    for (c8_i1 = 0; c8_i1 < 1; c8_i1++) {
      c8_rv0[c8_i1 + c8_i0].x = *(int8_T *)&((char_T *)(c8_Player *)&((char_T *)
        chartInstance->c8_players)[8 * (c8_i1 + c8_i0)])[0];
      c8_rv0[c8_i1 + c8_i0].y = *(int8_T *)&((char_T *)(c8_Player *)&((char_T *)
        chartInstance->c8_players)[8 * (c8_i1 + c8_i0)])[1];
      c8_rv0[c8_i1 + c8_i0].orientation = *(int16_T *)&((char_T *)(c8_Player *)
        &((char_T *)chartInstance->c8_players)[8 * (c8_i1 + c8_i0)])[2];
      c8_rv0[c8_i1 + c8_i0].color = *(uint8_T *)&((char_T *)(c8_Player *)
        &((char_T *)chartInstance->c8_players)[8 * (c8_i1 + c8_i0)])[4];
      c8_rv0[c8_i1 + c8_i0].position = *(uint8_T *)&((char_T *)(c8_Player *)
        &((char_T *)chartInstance->c8_players)[8 * (c8_i1 + c8_i0)])[5];
      c8_rv0[c8_i1 + c8_i0].valid = *(uint8_T *)&((char_T *)(c8_Player *)
        &((char_T *)chartInstance->c8_players)[8 * (c8_i1 + c8_i0)])[6];
    }
  }

  for (c8_i2 = 0; c8_i2 < 6; c8_i2++) {
    c8_d_u[c8_i2] = c8_rv0[c8_i2];
  }

  c8_f_y = NULL;
  for (c8_i3 = 0; c8_i3 < 2; c8_i3++) {
    c8_iv0[c8_i3] = 1 + 5 * c8_i3;
  }

  sf_mex_assign(&c8_f_y, sf_mex_createstructarray("structure", 2, c8_iv0), false);
  for (c8_i4 = 0; c8_i4 < 6; c8_i4++) {
    c8_r0 = &c8_d_u[c8_i4];
    c8_e_u = c8_r0->x;
    c8_g_y = NULL;
    sf_mex_assign(&c8_g_y, sf_mex_create("y", &c8_e_u, 2, 0U, 0U, 0U, 0), false);
    sf_mex_addfield(c8_f_y, c8_g_y, "x", "x", c8_i4);
    c8_f_u = c8_r0->y;
    c8_h_y = NULL;
    sf_mex_assign(&c8_h_y, sf_mex_create("y", &c8_f_u, 2, 0U, 0U, 0U, 0), false);
    sf_mex_addfield(c8_f_y, c8_h_y, "y", "y", c8_i4);
    c8_g_u = c8_r0->orientation;
    c8_i_y = NULL;
    sf_mex_assign(&c8_i_y, sf_mex_create("y", &c8_g_u, 4, 0U, 0U, 0U, 0), false);
    sf_mex_addfield(c8_f_y, c8_i_y, "orientation", "orientation", c8_i4);
    c8_h_u = c8_r0->color;
    c8_j_y = NULL;
    sf_mex_assign(&c8_j_y, sf_mex_create("y", &c8_h_u, 3, 0U, 0U, 0U, 0), false);
    sf_mex_addfield(c8_f_y, c8_j_y, "color", "color", c8_i4);
    c8_i_u = c8_r0->position;
    c8_k_y = NULL;
    sf_mex_assign(&c8_k_y, sf_mex_create("y", &c8_i_u, 3, 0U, 0U, 0U, 0), false);
    sf_mex_addfield(c8_f_y, c8_k_y, "position", "position", c8_i4);
    c8_j_u = c8_r0->valid;
    c8_l_y = NULL;
    sf_mex_assign(&c8_l_y, sf_mex_create("y", &c8_j_u, 3, 0U, 0U, 0U, 0), false);
    sf_mex_addfield(c8_f_y, c8_l_y, "valid", "valid", c8_i4);
  }

  sf_mex_setcell(c8_y, 1, c8_f_y);
  c8_m_y = NULL;
  if (!chartInstance->c8_latestBall_not_empty) {
    sf_mex_assign(&c8_m_y, sf_mex_create("y", NULL, 0, 0U, 1U, 0U, 2, 0, 0),
                  false);
  } else {
    sf_mex_assign(&c8_m_y, sf_mex_createstruct("structure", 2, 1, 1), false);
    c8_k_u = chartInstance->c8_latestBall.x;
    c8_n_y = NULL;
    sf_mex_assign(&c8_n_y, sf_mex_create("y", &c8_k_u, 2, 0U, 0U, 0U, 0), false);
    sf_mex_addfield(c8_m_y, c8_n_y, "x", "x", 0);
    c8_l_u = chartInstance->c8_latestBall.y;
    c8_o_y = NULL;
    sf_mex_assign(&c8_o_y, sf_mex_create("y", &c8_l_u, 2, 0U, 0U, 0U, 0), false);
    sf_mex_addfield(c8_m_y, c8_o_y, "y", "y", 0);
    c8_m_u = chartInstance->c8_latestBall.valid;
    c8_p_y = NULL;
    sf_mex_assign(&c8_p_y, sf_mex_create("y", &c8_m_u, 3, 0U, 0U, 0U, 0), false);
    sf_mex_addfield(c8_m_y, c8_p_y, "valid", "valid", 0);
  }

  sf_mex_setcell(c8_y, 2, c8_m_y);
  c8_q_y = NULL;
  if (!chartInstance->c8_latestPlayers_not_empty) {
    sf_mex_assign(&c8_q_y, sf_mex_create("y", NULL, 0, 0U, 1U, 0U, 2, 0, 0),
                  false);
  } else {
    sf_mex_assign(&c8_q_y, sf_mex_createstruct("structure", 2, 1, 1), false);
    for (c8_i5 = 0; c8_i5 < 6; c8_i5++) {
      c8_n_u[c8_i5] = chartInstance->c8_latestPlayers.x[c8_i5];
    }

    c8_r_y = NULL;
    sf_mex_assign(&c8_r_y, sf_mex_create("y", c8_n_u, 2, 0U, 1U, 0U, 2, 1, 6),
                  false);
    sf_mex_addfield(c8_q_y, c8_r_y, "x", "x", 0);
    for (c8_i6 = 0; c8_i6 < 6; c8_i6++) {
      c8_o_u[c8_i6] = chartInstance->c8_latestPlayers.y[c8_i6];
    }

    c8_s_y = NULL;
    sf_mex_assign(&c8_s_y, sf_mex_create("y", c8_o_u, 2, 0U, 1U, 0U, 2, 1, 6),
                  false);
    sf_mex_addfield(c8_q_y, c8_s_y, "y", "y", 0);
    for (c8_i7 = 0; c8_i7 < 6; c8_i7++) {
      c8_p_u[c8_i7] = chartInstance->c8_latestPlayers.orientation[c8_i7];
    }

    c8_t_y = NULL;
    sf_mex_assign(&c8_t_y, sf_mex_create("y", c8_p_u, 4, 0U, 1U, 0U, 2, 1, 6),
                  false);
    sf_mex_addfield(c8_q_y, c8_t_y, "orientation", "orientation", 0);
    for (c8_i8 = 0; c8_i8 < 6; c8_i8++) {
      c8_q_u[c8_i8] = chartInstance->c8_latestPlayers.color[c8_i8];
    }

    c8_u_y = NULL;
    sf_mex_assign(&c8_u_y, sf_mex_create("y", c8_q_u, 3, 0U, 1U, 0U, 2, 1, 6),
                  false);
    sf_mex_addfield(c8_q_y, c8_u_y, "color", "color", 0);
    for (c8_i9 = 0; c8_i9 < 6; c8_i9++) {
      c8_r_u[c8_i9] = chartInstance->c8_latestPlayers.position[c8_i9];
    }

    c8_v_y = NULL;
    sf_mex_assign(&c8_v_y, sf_mex_create("y", c8_r_u, 3, 0U, 1U, 0U, 2, 1, 6),
                  false);
    sf_mex_addfield(c8_q_y, c8_v_y, "position", "position", 0);
    for (c8_i10 = 0; c8_i10 < 6; c8_i10++) {
      c8_s_u[c8_i10] = chartInstance->c8_latestPlayers.valid[c8_i10];
    }

    c8_w_y = NULL;
    sf_mex_assign(&c8_w_y, sf_mex_create("y", c8_s_u, 3, 0U, 1U, 0U, 2, 1, 6),
                  false);
    sf_mex_addfield(c8_q_y, c8_w_y, "valid", "valid", 0);
  }

  sf_mex_setcell(c8_y, 3, c8_q_y);
  c8_hoistedGlobal = chartInstance->c8_is_active_c8_LessonII;
  c8_t_u = c8_hoistedGlobal;
  c8_x_y = NULL;
  sf_mex_assign(&c8_x_y, sf_mex_create("y", &c8_t_u, 3, 0U, 0U, 0U, 0), false);
  sf_mex_setcell(c8_y, 4, c8_x_y);
  sf_mex_assign(&c8_st, c8_y, false);
  return c8_st;
}

static void set_sim_state_c8_LessonII(SFc8_LessonIIInstanceStruct *chartInstance,
  const mxArray *c8_st)
{
  const mxArray *c8_u;
  c8_Ball c8_r1;
  c8_Player c8_rv1[6];
  int32_T c8_i11;
  c8_Player c8_rv2[6];
  int32_T c8_i12;
  int32_T c8_i13;
  chartInstance->c8_doneDoubleBufferReInit = true;
  c8_u = sf_mex_dup(c8_st);
  c8_r1 = c8_j_emlrt_marshallIn(chartInstance, sf_mex_dup(sf_mex_getcell(c8_u, 0)),
    "ball");
  *(int8_T *)&((char_T *)chartInstance->c8_ball)[0] = c8_r1.x;
  *(int8_T *)&((char_T *)chartInstance->c8_ball)[1] = c8_r1.y;
  *(uint8_T *)&((char_T *)chartInstance->c8_ball)[2] = c8_r1.valid;
  c8_l_emlrt_marshallIn(chartInstance, sf_mex_dup(sf_mex_getcell(c8_u, 1)),
                        "players", c8_rv1);
  for (c8_i11 = 0; c8_i11 < 6; c8_i11++) {
    c8_rv2[c8_i11] = c8_rv1[c8_i11];
  }

  for (c8_i12 = 0; c8_i12 < 6; c8_i12++) {
    for (c8_i13 = 0; c8_i13 < 1; c8_i13++) {
      *(int8_T *)&((char_T *)(c8_Player *)&((char_T *)chartInstance->c8_players)
                   [8 * (c8_i13 + c8_i12)])[0] = c8_rv2[c8_i13 + c8_i12].x;
      *(int8_T *)&((char_T *)(c8_Player *)&((char_T *)chartInstance->c8_players)
                   [8 * (c8_i13 + c8_i12)])[1] = c8_rv2[c8_i13 + c8_i12].y;
      *(int16_T *)&((char_T *)(c8_Player *)&((char_T *)chartInstance->c8_players)
                    [8 * (c8_i13 + c8_i12)])[2] = c8_rv2[c8_i13 + c8_i12].
        orientation;
      *(uint8_T *)&((char_T *)(c8_Player *)&((char_T *)chartInstance->c8_players)
                    [8 * (c8_i13 + c8_i12)])[4] = c8_rv2[c8_i13 + c8_i12].color;
      *(uint8_T *)&((char_T *)(c8_Player *)&((char_T *)chartInstance->c8_players)
                    [8 * (c8_i13 + c8_i12)])[5] = c8_rv2[c8_i13 + c8_i12].
        position;
      *(uint8_T *)&((char_T *)(c8_Player *)&((char_T *)chartInstance->c8_players)
                    [8 * (c8_i13 + c8_i12)])[6] = c8_rv2[c8_i13 + c8_i12].valid;
    }
  }

  chartInstance->c8_latestBall = c8_emlrt_marshallIn(chartInstance, sf_mex_dup
    (sf_mex_getcell(c8_u, 2)), "latestBall");
  chartInstance->c8_latestPlayers = c8_e_emlrt_marshallIn(chartInstance,
    sf_mex_dup(sf_mex_getcell(c8_u, 3)), "latestPlayers");
  chartInstance->c8_is_active_c8_LessonII = c8_r_emlrt_marshallIn(chartInstance,
    sf_mex_dup(sf_mex_getcell(c8_u, 4)), "is_active_c8_LessonII");
  sf_mex_destroy(&c8_u);
  c8_update_debugger_state_c8_LessonII(chartInstance);
  sf_mex_destroy(&c8_st);
}

static void finalize_c8_LessonII(SFc8_LessonIIInstanceStruct *chartInstance)
{
  (void)chartInstance;
}

static void sf_gateway_c8_LessonII(SFc8_LessonIIInstanceStruct *chartInstance)
{
  _SFD_SYMBOL_SCOPE_PUSH(0U, 0U);
  _sfTime_ = sf_get_time(chartInstance->S);
  _SFD_CC_CALL(CHART_ENTER_SFUNCTION_TAG, 5U, chartInstance->c8_sfEvent);
  chartInstance->c8_sfEvent = CALL_EVENT;
  c8_chartstep_c8_LessonII(chartInstance);
  _SFD_SYMBOL_SCOPE_POP();
  _SFD_CHECK_FOR_STATE_INCONSISTENCY(_LessonIIMachineNumber_,
    chartInstance->chartNumber, chartInstance->instanceNumber);
}

static void mdl_start_c8_LessonII(SFc8_LessonIIInstanceStruct *chartInstance)
{
  (void)chartInstance;
}

static void c8_chartstep_c8_LessonII(SFc8_LessonIIInstanceStruct *chartInstance)
{
  c8_PlayerData_size c8_b_playerData_elems_sizes;
  int32_T c8_loop_ub;
  int32_T c8_i14;
  int32_T c8_b_loop_ub;
  int32_T c8_i15;
  c8_PlayerData c8_b_playerData_data;
  int32_T c8_c_loop_ub;
  int32_T c8_i16;
  int32_T c8_d_loop_ub;
  int32_T c8_i17;
  int32_T c8_e_loop_ub;
  int32_T c8_i18;
  int32_T c8_f_loop_ub;
  int32_T c8_i19;
  int32_T c8_g_loop_ub;
  int32_T c8_i20;
  int32_T c8_h_loop_ub;
  int32_T c8_i21;
  c8_BallData_size c8_b_BallData_elems_sizes;
  int32_T c8_i_loop_ub;
  int32_T c8_i22;
  c8_BallData c8_b_BallData_data;
  int32_T c8_j_loop_ub;
  int32_T c8_i23;
  uint32_T c8_debug_family_var_map[15];
  real_T c8_ii;
  int8_T c8_pos[2];
  int32_T c8_dist;
  uint8_T c8_index;
  real_T c8_jj;
  int32_T c8_temp;
  c8_Player c8_tempEmpty;
  real_T c8_nargin = 2.0;
  real_T c8_nargout = 2.0;
  c8_Player c8_b_players[6];
  c8_Ball c8_b_ball;
  int32_T c8_i24;
  static int8_T c8_iv1[6] = { -10, -30, -70, 10, 30, 70 };

  int32_T c8_i25;
  int32_T c8_i26;
  static int16_T c8_iv2[6] = { 0, 0, 0, 180, 180, 180 };

  int32_T c8_i27;
  static uint8_T c8_uv0[6] = { 103U, 103U, 103U, 98U, 98U, 98U };

  int32_T c8_i28;
  static uint8_T c8_uv1[6] = { 111U, 100U, 103U, 111U, 100U, 103U };

  int32_T c8_i29;
  int32_T c8_i30;
  int32_T c8_x_sizes[2];
  int32_T c8_x;
  int32_T c8_b_x;
  int32_T c8_k_loop_ub;
  int32_T c8_i31;
  int8_T c8_x_data[6];
  int32_T c8_b_x_sizes[2];
  int32_T c8_c_x;
  int32_T c8_d_x;
  int32_T c8_l_loop_ub;
  int32_T c8_i32;
  int8_T c8_b_x_data[6];
  real_T c8_d0;
  int32_T c8_i33;
  int32_T c8_b_ii;
  int32_T c8_playerData[1];
  int32_T c8_b_playerData[1];
  int32_T c8_b_jj;
  uint8_T c8_u0;
  c8_stETj7QHScyDJroYDDeQcXH c8_hoistedGlobal;
  c8_stETj7QHScyDJroYDDeQcXH c8_b_hoistedGlobal;
  int8_T c8_c_hoistedGlobal[2];
  int32_T c8_i34;
  int32_T c8_i35;
  real32_T c8_e_x[2];
  int32_T c8_i36;
  real32_T c8_f_x[2];
  real32_T c8_y;
  real32_T c8_f0;
  int32_T c8_i37;
  real_T c8_d1;
  uint8_T c8_u1;
  int32_T c8_c_ii;
  int32_T c8_i38;
  c8_Player c8_rv3[6];
  int32_T c8_i39;
  int32_T c8_i40;
  boolean_T guard1 = false;
  _SFD_CC_CALL(CHART_ENTER_DURING_FUNCTION_TAG, 5U, chartInstance->c8_sfEvent);
  c8_b_playerData_elems_sizes.x[0] = chartInstance->c8_playerData_elems_sizes->
    x[0];
  c8_b_playerData_elems_sizes.x[1] = chartInstance->c8_playerData_elems_sizes->
    x[1];
  c8_loop_ub = chartInstance->c8_playerData_elems_sizes->x[1] - 1;
  for (c8_i14 = 0; c8_i14 <= c8_loop_ub; c8_i14++) {
    c8_b_loop_ub = chartInstance->c8_playerData_elems_sizes->x[0] - 1;
    for (c8_i15 = 0; c8_i15 <= c8_b_loop_ub; c8_i15++) {
      c8_b_playerData_data.x[c8_i15 + c8_b_playerData_elems_sizes.x[0] * c8_i14]
        = ((int8_T *)&((char_T *)chartInstance->c8_playerData_data)[0])[c8_i15 +
        chartInstance->c8_playerData_elems_sizes->x[0] * c8_i14];
    }
  }

  c8_b_playerData_elems_sizes.y[0] = chartInstance->c8_playerData_elems_sizes->
    y[0];
  c8_b_playerData_elems_sizes.y[1] = chartInstance->c8_playerData_elems_sizes->
    y[1];
  c8_c_loop_ub = chartInstance->c8_playerData_elems_sizes->y[1] - 1;
  for (c8_i16 = 0; c8_i16 <= c8_c_loop_ub; c8_i16++) {
    c8_d_loop_ub = chartInstance->c8_playerData_elems_sizes->y[0] - 1;
    for (c8_i17 = 0; c8_i17 <= c8_d_loop_ub; c8_i17++) {
      c8_b_playerData_data.y[c8_i17 + c8_b_playerData_elems_sizes.y[0] * c8_i16]
        = ((int8_T *)&((char_T *)chartInstance->c8_playerData_data)[6])[c8_i17 +
        chartInstance->c8_playerData_elems_sizes->y[0] * c8_i16];
    }
  }

  c8_b_playerData_elems_sizes.orientation[0] =
    chartInstance->c8_playerData_elems_sizes->orientation[0];
  c8_b_playerData_elems_sizes.orientation[1] =
    chartInstance->c8_playerData_elems_sizes->orientation[1];
  c8_e_loop_ub = chartInstance->c8_playerData_elems_sizes->orientation[1] - 1;
  for (c8_i18 = 0; c8_i18 <= c8_e_loop_ub; c8_i18++) {
    c8_f_loop_ub = chartInstance->c8_playerData_elems_sizes->orientation[0] - 1;
    for (c8_i19 = 0; c8_i19 <= c8_f_loop_ub; c8_i19++) {
      c8_b_playerData_data.orientation[c8_i19 +
        c8_b_playerData_elems_sizes.orientation[0] * c8_i18] = ((int16_T *)
        &((char_T *)chartInstance->c8_playerData_data)[12])[c8_i19 +
        chartInstance->c8_playerData_elems_sizes->orientation[0] * c8_i18];
    }
  }

  c8_b_playerData_elems_sizes.color[0] =
    chartInstance->c8_playerData_elems_sizes->color[0];
  c8_b_playerData_elems_sizes.color[1] =
    chartInstance->c8_playerData_elems_sizes->color[1];
  c8_g_loop_ub = chartInstance->c8_playerData_elems_sizes->color[1] - 1;
  for (c8_i20 = 0; c8_i20 <= c8_g_loop_ub; c8_i20++) {
    c8_h_loop_ub = chartInstance->c8_playerData_elems_sizes->color[0] - 1;
    for (c8_i21 = 0; c8_i21 <= c8_h_loop_ub; c8_i21++) {
      c8_b_playerData_data.color[c8_i21 + c8_b_playerData_elems_sizes.color[0] *
        c8_i20] = ((uint8_T *)&((char_T *)chartInstance->c8_playerData_data)[24])
        [c8_i21 + chartInstance->c8_playerData_elems_sizes->color[0] * c8_i20];
    }
  }

  c8_b_BallData_elems_sizes.x = chartInstance->c8_BallData_elems_sizes->x;
  c8_i_loop_ub = chartInstance->c8_BallData_elems_sizes->x - 1;
  for (c8_i22 = 0; c8_i22 <= c8_i_loop_ub; c8_i22++) {
    c8_b_BallData_data.x[c8_i22] = ((int8_T *)&((char_T *)
      chartInstance->c8_BallData_data)[0])[c8_i22];
  }

  c8_b_BallData_elems_sizes.y = chartInstance->c8_BallData_elems_sizes->y;
  c8_j_loop_ub = chartInstance->c8_BallData_elems_sizes->y - 1;
  for (c8_i23 = 0; c8_i23 <= c8_j_loop_ub; c8_i23++) {
    c8_b_BallData_data.y[c8_i23] = ((int8_T *)&((char_T *)
      chartInstance->c8_BallData_data)[2])[c8_i23];
  }

  _SFD_SYMBOL_SCOPE_PUSH_EML(0U, 15U, 15U, c8_debug_family_names,
    c8_debug_family_var_map);
  _SFD_SYMBOL_SCOPE_ADD_EML_IMPORTABLE(&c8_ii, 0U, c8_g_sf_marshallOut,
    c8_e_sf_marshallIn);
  _SFD_SYMBOL_SCOPE_ADD_EML_IMPORTABLE(c8_pos, 1U, c8_k_sf_marshallOut,
    c8_i_sf_marshallIn);
  _SFD_SYMBOL_SCOPE_ADD_EML_IMPORTABLE(&c8_dist, 2U, c8_i_sf_marshallOut,
    c8_g_sf_marshallIn);
  _SFD_SYMBOL_SCOPE_ADD_EML_IMPORTABLE(&c8_index, 3U, c8_j_sf_marshallOut,
    c8_h_sf_marshallIn);
  _SFD_SYMBOL_SCOPE_ADD_EML_IMPORTABLE(&c8_jj, 4U, c8_g_sf_marshallOut,
    c8_e_sf_marshallIn);
  _SFD_SYMBOL_SCOPE_ADD_EML_IMPORTABLE(&c8_temp, 5U, c8_i_sf_marshallOut,
    c8_g_sf_marshallIn);
  _SFD_SYMBOL_SCOPE_ADD_EML_IMPORTABLE(&c8_tempEmpty, 6U, c8_h_sf_marshallOut,
    c8_f_sf_marshallIn);
  _SFD_SYMBOL_SCOPE_ADD_EML_IMPORTABLE(&c8_nargin, 7U, c8_g_sf_marshallOut,
    c8_e_sf_marshallIn);
  _SFD_SYMBOL_SCOPE_ADD_EML_IMPORTABLE(&c8_nargout, 8U, c8_g_sf_marshallOut,
    c8_e_sf_marshallIn);
  _SFD_SYMBOL_SCOPE_ADD_EML_DYN(&c8_b_playerData_data, NULL,
    &c8_b_playerData_elems_sizes, 1, 9, (void *)c8_f_sf_marshallOut);
  _SFD_SYMBOL_SCOPE_ADD_EML_DYN(&c8_b_BallData_data, NULL,
    &c8_b_BallData_elems_sizes, 1, 10, (void *)c8_e_sf_marshallOut);
  _SFD_SYMBOL_SCOPE_ADD_EML_IMPORTABLE(c8_b_players, 11U, c8_d_sf_marshallOut,
    c8_d_sf_marshallIn);
  _SFD_SYMBOL_SCOPE_ADD_EML_IMPORTABLE(&c8_b_ball, 12U, c8_c_sf_marshallOut,
    c8_c_sf_marshallIn);
  _SFD_SYMBOL_SCOPE_ADD_EML_IMPORTABLE(&chartInstance->c8_latestPlayers, 13U,
    c8_b_sf_marshallOut, c8_b_sf_marshallIn);
  _SFD_SYMBOL_SCOPE_ADD_EML_IMPORTABLE(&chartInstance->c8_latestBall, 14U,
    c8_sf_marshallOut, c8_sf_marshallIn);
  CV_EML_FCN(0, 0);
  _SFD_EML_CALL(0U, chartInstance->c8_sfEvent, 3);
  _SFD_EML_CALL(0U, chartInstance->c8_sfEvent, 4);
  _SFD_EML_CALL(0U, chartInstance->c8_sfEvent, 6);
  if (CV_EML_IF(0, 1, 0, !chartInstance->c8_latestPlayers_not_empty)) {
    _SFD_EML_CALL(0U, chartInstance->c8_sfEvent, 7);
    for (c8_i24 = 0; c8_i24 < 6; c8_i24++) {
      chartInstance->c8_latestPlayers.x[c8_i24] = c8_iv1[c8_i24];
    }

    chartInstance->c8_latestPlayers_not_empty = true;
    _SFD_EML_CALL(0U, chartInstance->c8_sfEvent, 9);
    for (c8_i25 = 0; c8_i25 < 6; c8_i25++) {
      chartInstance->c8_latestPlayers.y[c8_i25] = 0;
    }

    _SFD_EML_CALL(0U, chartInstance->c8_sfEvent, 10);
    for (c8_i26 = 0; c8_i26 < 6; c8_i26++) {
      chartInstance->c8_latestPlayers.orientation[c8_i26] = c8_iv2[c8_i26];
    }

    _SFD_EML_CALL(0U, chartInstance->c8_sfEvent, 12);
    for (c8_i27 = 0; c8_i27 < 6; c8_i27++) {
      chartInstance->c8_latestPlayers.color[c8_i27] = c8_uv0[c8_i27];
    }

    _SFD_EML_CALL(0U, chartInstance->c8_sfEvent, 13);
    for (c8_i28 = 0; c8_i28 < 6; c8_i28++) {
      chartInstance->c8_latestPlayers.position[c8_i28] = c8_uv1[c8_i28];
    }

    _SFD_EML_CALL(0U, chartInstance->c8_sfEvent, 14);
    for (c8_i29 = 0; c8_i29 < 6; c8_i29++) {
      chartInstance->c8_latestPlayers.valid[c8_i29] = 0U;
    }
  }

  _SFD_EML_CALL(0U, chartInstance->c8_sfEvent, 17);
  if (CV_EML_IF(0, 1, 1, !chartInstance->c8_latestBall_not_empty)) {
    _SFD_EML_CALL(0U, chartInstance->c8_sfEvent, 18);
    chartInstance->c8_latestBall.x = 0;
    chartInstance->c8_latestBall_not_empty = true;
    _SFD_EML_CALL(0U, chartInstance->c8_sfEvent, 19);
    chartInstance->c8_latestBall.y = 0;
    _SFD_EML_CALL(0U, chartInstance->c8_sfEvent, 20);
    chartInstance->c8_latestBall.valid = 1U;
  }

  _SFD_EML_CALL(0U, chartInstance->c8_sfEvent, 24);
  for (c8_i30 = 0; c8_i30 < 6; c8_i30++) {
    chartInstance->c8_latestPlayers.valid[c8_i30] = 0U;
  }

  _SFD_EML_CALL(0U, chartInstance->c8_sfEvent, 25);
  chartInstance->c8_latestBall.valid = 0U;
  _SFD_EML_CALL(0U, chartInstance->c8_sfEvent, 27);
  if (CV_EML_IF(0, 1, 2, CV_EML_MCDC(0, 1, 0, !CV_EML_COND(0, 1, 0,
         c8_b_BallData_elems_sizes.x == 0)))) {
    _SFD_EML_CALL(0U, chartInstance->c8_sfEvent, 28);
    (real_T)_SFD_EML_ARRAY_BOUNDS_CHECK("BallData.x", 1, 1,
      c8_b_BallData_elems_sizes.x, 1, 0);
    chartInstance->c8_latestBall.x = c8_b_BallData_data.x[0];
    _SFD_EML_CALL(0U, chartInstance->c8_sfEvent, 29);
    (real_T)_SFD_EML_ARRAY_BOUNDS_CHECK("BallData.y", 1, 1,
      c8_b_BallData_elems_sizes.y, 1, 0);
    chartInstance->c8_latestBall.y = c8_b_BallData_data.y[0];
    _SFD_EML_CALL(0U, chartInstance->c8_sfEvent, 30);
    chartInstance->c8_latestBall.valid = 1U;
  }

  _SFD_EML_CALL(0U, chartInstance->c8_sfEvent, 33);
  c8_x_sizes[0] = c8_b_playerData_elems_sizes.x[0];
  c8_x_sizes[1] = c8_b_playerData_elems_sizes.x[1];
  c8_x = c8_x_sizes[0];
  c8_b_x = c8_x_sizes[1];
  c8_k_loop_ub = c8_b_playerData_elems_sizes.x[0] *
    c8_b_playerData_elems_sizes.x[1] - 1;
  for (c8_i31 = 0; c8_i31 <= c8_k_loop_ub; c8_i31++) {
    c8_x_data[c8_i31] = c8_b_playerData_data.x[c8_i31];
  }

  c8_b_x_sizes[0] = c8_x_sizes[0];
  c8_b_x_sizes[1] = c8_x_sizes[1];
  c8_c_x = c8_b_x_sizes[0];
  c8_d_x = c8_b_x_sizes[1];
  c8_l_loop_ub = c8_x_sizes[0] * c8_x_sizes[1] - 1;
  for (c8_i32 = 0; c8_i32 <= c8_l_loop_ub; c8_i32++) {
    c8_b_x_data[c8_i32] = c8_x_data[c8_i32];
  }

  c8_d0 = (real_T)c8_intlength(chartInstance, c8_b_x_data, c8_b_x_sizes);
  c8_i33 = (int32_T)c8_d0 - 1;
  c8_ii = 1.0;
  c8_b_ii = 0;
  while (c8_b_ii <= c8_i33) {
    c8_ii = 1.0 + (real_T)c8_b_ii;
    CV_EML_FOR(0, 1, 0, 1);
    _SFD_EML_CALL(0U, chartInstance->c8_sfEvent, 34);
    c8_playerData[0] = c8_b_playerData_elems_sizes.x[0] *
      c8_b_playerData_elems_sizes.x[1];
    c8_b_playerData[0] = c8_b_playerData_elems_sizes.y[0] *
      c8_b_playerData_elems_sizes.y[1];
    c8_pos[0] = c8_b_playerData_data.x[_SFD_EML_ARRAY_BOUNDS_CHECK(
      "playerData.x", (int32_T)c8_ii, 1, c8_b_playerData_elems_sizes.x[0] *
      c8_b_playerData_elems_sizes.x[1], 1, 0) - 1];
    c8_pos[1] = c8_b_playerData_data.y[_SFD_EML_ARRAY_BOUNDS_CHECK(
      "playerData.y", (int32_T)c8_ii, 1, c8_b_playerData_elems_sizes.y[0] *
      c8_b_playerData_elems_sizes.y[1], 1, 0) - 1];
    _SFD_EML_CALL(0U, chartInstance->c8_sfEvent, 35);
    c8_dist = MAX_int32_T;
    _SFD_EML_CALL(0U, chartInstance->c8_sfEvent, 36);
    c8_index = 0U;
    _SFD_EML_CALL(0U, chartInstance->c8_sfEvent, 37);
    c8_jj = 1.0;
    c8_b_jj = 0;
    while (c8_b_jj < 6) {
      c8_jj = 1.0 + (real_T)c8_b_jj;
      CV_EML_FOR(0, 1, 1, 1);
      _SFD_EML_CALL(0U, chartInstance->c8_sfEvent, 38);
      c8_u0 = c8_b_playerData_data.color[_SFD_EML_ARRAY_BOUNDS_CHECK(
        "playerData.color", (int32_T)c8_ii, 1,
        c8_b_playerData_elems_sizes.color[0] *
        c8_b_playerData_elems_sizes.color[1], 1, 0) - 1];
      if (CV_EML_IF(0, 1, 3, CV_RELATIONAL_EVAL(4U, 0U, 0, (real_T)c8_u0,
            (real_T)chartInstance->c8_latestPlayers.color[(int32_T)c8_jj - 1], 0,
            0U, c8_u0 == chartInstance->c8_latestPlayers.color[(int32_T)c8_jj -
            1]))) {
        _SFD_EML_CALL(0U, chartInstance->c8_sfEvent, 39);
        c8_hoistedGlobal = chartInstance->c8_latestPlayers;
        c8_b_hoistedGlobal = chartInstance->c8_latestPlayers;
        c8_c_hoistedGlobal[0] = c8_hoistedGlobal.x[(int32_T)c8_jj - 1];
        c8_c_hoistedGlobal[1] = c8_b_hoistedGlobal.y[(int32_T)c8_jj - 1];
        for (c8_i34 = 0; c8_i34 < 2; c8_i34++) {
          c8_i35 = c8_pos[c8_i34] - c8_c_hoistedGlobal[c8_i34];
          if (c8_i35 > 127) {
            CV_SATURATION_EVAL(4, 0, 1, 0, 1);
            c8_i35 = 127;
          } else {
            if (CV_SATURATION_EVAL(4, 0, 1, 0, c8_i35 < -128)) {
              c8_i35 = -128;
            }
          }

          c8_e_x[c8_i34] = (real32_T)(int8_T)c8_i35;
        }

        for (c8_i36 = 0; c8_i36 < 2; c8_i36++) {
          c8_f_x[c8_i36] = c8_e_x[c8_i36];
        }

        c8_y = c8_eml_xnrm2(chartInstance, c8_f_x);
        c8_f0 = muSingleScalarRound(c8_y);
        if (c8_f0 < 2.14748365E+9F) {
          if (CV_SATURATION_EVAL(4, 0, 0, 1, c8_f0 >= -2.14748365E+9F)) {
            c8_i37 = (int32_T)c8_f0;
          } else {
            c8_i37 = MIN_int32_T;
          }
        } else if (CV_SATURATION_EVAL(4, 0, 0, 0, c8_f0 >= 2.14748365E+9F)) {
          c8_i37 = MAX_int32_T;
        } else {
          c8_i37 = 0;
        }

        c8_temp = c8_i37;
        _SFD_EML_CALL(0U, chartInstance->c8_sfEvent, 40);
        guard1 = false;
        if (CV_EML_COND(0, 1, 1, CV_RELATIONAL_EVAL(4U, 0U, 1, (real_T)c8_temp,
              (real_T)c8_dist, 0, 2U, c8_temp < c8_dist))) {
          if (CV_EML_COND(0, 1, 2, CV_RELATIONAL_EVAL(4U, 0U, 2, (real_T)c8_temp,
                10.0, -1, 2U, (real_T)c8_temp < 10.0))) {
            CV_EML_MCDC(0, 1, 1, true);
            CV_EML_IF(0, 1, 4, true);
            _SFD_EML_CALL(0U, chartInstance->c8_sfEvent, 41);
            c8_dist = c8_temp;
            _SFD_EML_CALL(0U, chartInstance->c8_sfEvent, 42);
            c8_d1 = muDoubleScalarRound(c8_jj);
            if (c8_d1 < 256.0) {
              if (CV_SATURATION_EVAL(4, 0, 2, 1, c8_d1 >= 0.0)) {
                c8_u1 = (uint8_T)c8_d1;
              } else {
                c8_u1 = 0U;
              }
            } else if (CV_SATURATION_EVAL(4, 0, 2, 0, c8_d1 >= 256.0)) {
              c8_u1 = MAX_uint8_T;
            } else {
              c8_u1 = 0U;
            }

            c8_index = c8_u1;
          } else {
            guard1 = true;
          }
        } else {
          guard1 = true;
        }

        if (guard1 == true) {
          CV_EML_MCDC(0, 1, 1, false);
          CV_EML_IF(0, 1, 4, false);
        }
      }

      c8_b_jj++;
      _SF_MEX_LISTEN_FOR_CTRL_C(chartInstance->S);
    }

    CV_EML_FOR(0, 1, 1, 0);
    _SFD_EML_CALL(0U, chartInstance->c8_sfEvent, 46);
    if (CV_EML_IF(0, 1, 5, CV_RELATIONAL_EVAL(4U, 0U, 3, (real_T)c8_index, 0.0,
          0, 1U, c8_index != 0))) {
      _SFD_EML_CALL(0U, chartInstance->c8_sfEvent, 47);
      chartInstance->c8_latestPlayers.x[c8_index - 1] =
        c8_b_playerData_data.x[_SFD_EML_ARRAY_BOUNDS_CHECK("playerData.x",
        (int32_T)c8_ii, 1, c8_b_playerData_elems_sizes.x[0] *
        c8_b_playerData_elems_sizes.x[1], 1, 0) - 1];
      _SFD_EML_CALL(0U, chartInstance->c8_sfEvent, 48);
      chartInstance->c8_latestPlayers.y[c8_index - 1] =
        c8_b_playerData_data.y[_SFD_EML_ARRAY_BOUNDS_CHECK("playerData.y",
        (int32_T)c8_ii, 1, c8_b_playerData_elems_sizes.y[0] *
        c8_b_playerData_elems_sizes.y[1], 1, 0) - 1];
      _SFD_EML_CALL(0U, chartInstance->c8_sfEvent, 49);
      chartInstance->c8_latestPlayers.orientation[c8_index - 1] =
        c8_b_playerData_data.orientation[_SFD_EML_ARRAY_BOUNDS_CHECK(
        "playerData.orientation", (int32_T)c8_ii, 1,
        c8_b_playerData_elems_sizes.orientation[0] *
        c8_b_playerData_elems_sizes.orientation[1], 1, 0) - 1];
      _SFD_EML_CALL(0U, chartInstance->c8_sfEvent, 50);
      chartInstance->c8_latestPlayers.valid[c8_index - 1] = 1U;
    }

    c8_b_ii++;
    _SF_MEX_LISTEN_FOR_CTRL_C(chartInstance->S);
  }

  CV_EML_FOR(0, 1, 0, 0);
  _SFD_EML_CALL(0U, chartInstance->c8_sfEvent, 53);
  c8_b_ball = chartInstance->c8_latestBall;
  _SFD_EML_CALL(0U, chartInstance->c8_sfEvent, 54);
  c8_tempEmpty.x = 0;
  _SFD_EML_CALL(0U, chartInstance->c8_sfEvent, 55);
  c8_tempEmpty.y = 0;
  _SFD_EML_CALL(0U, chartInstance->c8_sfEvent, 56);
  c8_tempEmpty.orientation = 0;
  _SFD_EML_CALL(0U, chartInstance->c8_sfEvent, 57);
  c8_tempEmpty.color = 0U;
  _SFD_EML_CALL(0U, chartInstance->c8_sfEvent, 58);
  c8_tempEmpty.position = 0U;
  _SFD_EML_CALL(0U, chartInstance->c8_sfEvent, 59);
  c8_tempEmpty.valid = 0U;
  _SFD_EML_CALL(0U, chartInstance->c8_sfEvent, 60);
  c8_b_players[0] = c8_tempEmpty;
  c8_b_players[1] = c8_tempEmpty;
  c8_b_players[2] = c8_tempEmpty;
  c8_b_players[3] = c8_tempEmpty;
  c8_b_players[4] = c8_tempEmpty;
  c8_b_players[5] = c8_tempEmpty;
  _SFD_EML_CALL(0U, chartInstance->c8_sfEvent, 61);
  c8_ii = 1.0;
  c8_c_ii = 0;
  while (c8_c_ii < 6) {
    c8_ii = 1.0 + (real_T)c8_c_ii;
    CV_EML_FOR(0, 1, 2, 1);
    _SFD_EML_CALL(0U, chartInstance->c8_sfEvent, 62);
    c8_b_players[(int32_T)c8_ii - 1].x = chartInstance->c8_latestPlayers.x
      [(int32_T)c8_ii - 1];
    _SFD_EML_CALL(0U, chartInstance->c8_sfEvent, 63);
    c8_b_players[(int32_T)c8_ii - 1].y = chartInstance->c8_latestPlayers.y
      [(int32_T)c8_ii - 1];
    _SFD_EML_CALL(0U, chartInstance->c8_sfEvent, 64);
    c8_b_players[(int32_T)c8_ii - 1].orientation =
      chartInstance->c8_latestPlayers.orientation[(int32_T)c8_ii - 1];
    _SFD_EML_CALL(0U, chartInstance->c8_sfEvent, 65);
    c8_b_players[(int32_T)c8_ii - 1].color =
      chartInstance->c8_latestPlayers.color[(int32_T)c8_ii - 1];
    _SFD_EML_CALL(0U, chartInstance->c8_sfEvent, 66);
    c8_b_players[(int32_T)c8_ii - 1].position =
      chartInstance->c8_latestPlayers.position[(int32_T)c8_ii - 1];
    _SFD_EML_CALL(0U, chartInstance->c8_sfEvent, 67);
    c8_b_players[(int32_T)c8_ii - 1].valid =
      chartInstance->c8_latestPlayers.valid[(int32_T)c8_ii - 1];
    c8_c_ii++;
    _SF_MEX_LISTEN_FOR_CTRL_C(chartInstance->S);
  }

  CV_EML_FOR(0, 1, 2, 0);
  _SFD_EML_CALL(0U, chartInstance->c8_sfEvent, -67);
  _SFD_SYMBOL_SCOPE_POP();
  for (c8_i38 = 0; c8_i38 < 6; c8_i38++) {
    c8_rv3[c8_i38] = c8_b_players[c8_i38];
  }

  for (c8_i39 = 0; c8_i39 < 6; c8_i39++) {
    for (c8_i40 = 0; c8_i40 < 1; c8_i40++) {
      *(int8_T *)&((char_T *)(c8_Player *)&((char_T *)chartInstance->c8_players)
                   [8 * (c8_i40 + c8_i39)])[0] = c8_rv3[c8_i40 + c8_i39].x;
      *(int8_T *)&((char_T *)(c8_Player *)&((char_T *)chartInstance->c8_players)
                   [8 * (c8_i40 + c8_i39)])[1] = c8_rv3[c8_i40 + c8_i39].y;
      *(int16_T *)&((char_T *)(c8_Player *)&((char_T *)chartInstance->c8_players)
                    [8 * (c8_i40 + c8_i39)])[2] = c8_rv3[c8_i40 + c8_i39].
        orientation;
      *(uint8_T *)&((char_T *)(c8_Player *)&((char_T *)chartInstance->c8_players)
                    [8 * (c8_i40 + c8_i39)])[4] = c8_rv3[c8_i40 + c8_i39].color;
      *(uint8_T *)&((char_T *)(c8_Player *)&((char_T *)chartInstance->c8_players)
                    [8 * (c8_i40 + c8_i39)])[5] = c8_rv3[c8_i40 + c8_i39].
        position;
      *(uint8_T *)&((char_T *)(c8_Player *)&((char_T *)chartInstance->c8_players)
                    [8 * (c8_i40 + c8_i39)])[6] = c8_rv3[c8_i40 + c8_i39].valid;
    }
  }

  *(int8_T *)&((char_T *)chartInstance->c8_ball)[0] = c8_b_ball.x;
  *(int8_T *)&((char_T *)chartInstance->c8_ball)[1] = c8_b_ball.y;
  *(uint8_T *)&((char_T *)chartInstance->c8_ball)[2] = c8_b_ball.valid;
  _SFD_CC_CALL(EXIT_OUT_OF_FUNCTION_TAG, 5U, chartInstance->c8_sfEvent);
}

static void initSimStructsc8_LessonII(SFc8_LessonIIInstanceStruct *chartInstance)
{
  (void)chartInstance;
}

static void init_script_number_translation(uint32_T c8_machineNumber, uint32_T
  c8_chartNumber, uint32_T c8_instanceNumber)
{
  (void)c8_machineNumber;
  (void)c8_chartNumber;
  (void)c8_instanceNumber;
}

static const mxArray *c8_sf_marshallOut(void *chartInstanceVoid, void *c8_inData)
{
  const mxArray *c8_mxArrayOutData;
  c8_Ball c8_u;
  const mxArray *c8_y = NULL;
  int8_T c8_b_u;
  const mxArray *c8_b_y = NULL;
  int8_T c8_c_u;
  const mxArray *c8_c_y = NULL;
  uint8_T c8_d_u;
  const mxArray *c8_d_y = NULL;
  SFc8_LessonIIInstanceStruct *chartInstance;
  chartInstance = (SFc8_LessonIIInstanceStruct *)chartInstanceVoid;
  c8_mxArrayOutData = NULL;
  c8_mxArrayOutData = NULL;
  c8_u = *(c8_Ball *)c8_inData;
  c8_y = NULL;
  if (!chartInstance->c8_latestBall_not_empty) {
    sf_mex_assign(&c8_y, sf_mex_create("y", NULL, 0, 0U, 1U, 0U, 2, 0, 0), false);
  } else {
    sf_mex_assign(&c8_y, sf_mex_createstruct("structure", 2, 1, 1), false);
    c8_b_u = c8_u.x;
    c8_b_y = NULL;
    sf_mex_assign(&c8_b_y, sf_mex_create("y", &c8_b_u, 2, 0U, 0U, 0U, 0), false);
    sf_mex_addfield(c8_y, c8_b_y, "x", "x", 0);
    c8_c_u = c8_u.y;
    c8_c_y = NULL;
    sf_mex_assign(&c8_c_y, sf_mex_create("y", &c8_c_u, 2, 0U, 0U, 0U, 0), false);
    sf_mex_addfield(c8_y, c8_c_y, "y", "y", 0);
    c8_d_u = c8_u.valid;
    c8_d_y = NULL;
    sf_mex_assign(&c8_d_y, sf_mex_create("y", &c8_d_u, 3, 0U, 0U, 0U, 0), false);
    sf_mex_addfield(c8_y, c8_d_y, "valid", "valid", 0);
  }

  sf_mex_assign(&c8_mxArrayOutData, c8_y, false);
  return c8_mxArrayOutData;
}

static c8_Ball c8_emlrt_marshallIn(SFc8_LessonIIInstanceStruct *chartInstance,
  const mxArray *c8_b_latestBall, const char_T *c8_identifier)
{
  c8_Ball c8_y;
  emlrtMsgIdentifier c8_thisId;
  c8_thisId.fIdentifier = c8_identifier;
  c8_thisId.fParent = NULL;
  c8_y = c8_b_emlrt_marshallIn(chartInstance, sf_mex_dup(c8_b_latestBall),
    &c8_thisId);
  sf_mex_destroy(&c8_b_latestBall);
  return c8_y;
}

static c8_Ball c8_b_emlrt_marshallIn(SFc8_LessonIIInstanceStruct *chartInstance,
  const mxArray *c8_u, const emlrtMsgIdentifier *c8_parentId)
{
  c8_Ball c8_y;
  emlrtMsgIdentifier c8_thisId;
  static const char * c8_fieldNames[3] = { "x", "y", "valid" };

  c8_thisId.fParent = c8_parentId;
  if (mxIsEmpty(c8_u)) {
    chartInstance->c8_latestBall_not_empty = false;
  } else {
    chartInstance->c8_latestBall_not_empty = true;
    sf_mex_check_struct(c8_parentId, c8_u, 3, c8_fieldNames, 0U, NULL);
    c8_thisId.fIdentifier = "x";
    c8_y.x = c8_c_emlrt_marshallIn(chartInstance, sf_mex_dup(sf_mex_getfield
      (c8_u, "x", "x", 0)), &c8_thisId);
    c8_thisId.fIdentifier = "y";
    c8_y.y = c8_c_emlrt_marshallIn(chartInstance, sf_mex_dup(sf_mex_getfield
      (c8_u, "y", "y", 0)), &c8_thisId);
    c8_thisId.fIdentifier = "valid";
    c8_y.valid = c8_d_emlrt_marshallIn(chartInstance, sf_mex_dup(sf_mex_getfield
                                        (c8_u, "valid", "valid", 0)), &c8_thisId);
  }

  sf_mex_destroy(&c8_u);
  return c8_y;
}

static int8_T c8_c_emlrt_marshallIn(SFc8_LessonIIInstanceStruct *chartInstance,
  const mxArray *c8_u, const emlrtMsgIdentifier *c8_parentId)
{
  int8_T c8_y;
  int8_T c8_i41;
  (void)chartInstance;
  sf_mex_import(c8_parentId, sf_mex_dup(c8_u), &c8_i41, 1, 2, 0U, 0, 0U, 0);
  c8_y = c8_i41;
  sf_mex_destroy(&c8_u);
  return c8_y;
}

static uint8_T c8_d_emlrt_marshallIn(SFc8_LessonIIInstanceStruct *chartInstance,
  const mxArray *c8_u, const emlrtMsgIdentifier *c8_parentId)
{
  uint8_T c8_y;
  uint8_T c8_u2;
  (void)chartInstance;
  sf_mex_import(c8_parentId, sf_mex_dup(c8_u), &c8_u2, 1, 3, 0U, 0, 0U, 0);
  c8_y = c8_u2;
  sf_mex_destroy(&c8_u);
  return c8_y;
}

static void c8_sf_marshallIn(void *chartInstanceVoid, const mxArray
  *c8_mxArrayInData, const char_T *c8_varName, void *c8_outData)
{
  const mxArray *c8_b_latestBall;
  const char_T *c8_identifier;
  emlrtMsgIdentifier c8_thisId;
  c8_Ball c8_y;
  SFc8_LessonIIInstanceStruct *chartInstance;
  chartInstance = (SFc8_LessonIIInstanceStruct *)chartInstanceVoid;
  c8_b_latestBall = sf_mex_dup(c8_mxArrayInData);
  c8_identifier = c8_varName;
  c8_thisId.fIdentifier = c8_identifier;
  c8_thisId.fParent = NULL;
  c8_y = c8_b_emlrt_marshallIn(chartInstance, sf_mex_dup(c8_b_latestBall),
    &c8_thisId);
  sf_mex_destroy(&c8_b_latestBall);
  *(c8_Ball *)c8_outData = c8_y;
  sf_mex_destroy(&c8_mxArrayInData);
}

static const mxArray *c8_b_sf_marshallOut(void *chartInstanceVoid, void
  *c8_inData)
{
  const mxArray *c8_mxArrayOutData;
  c8_stETj7QHScyDJroYDDeQcXH c8_u;
  const mxArray *c8_y = NULL;
  int32_T c8_i42;
  int8_T c8_b_u[6];
  const mxArray *c8_b_y = NULL;
  int32_T c8_i43;
  int8_T c8_c_u[6];
  const mxArray *c8_c_y = NULL;
  int32_T c8_i44;
  int16_T c8_d_u[6];
  const mxArray *c8_d_y = NULL;
  int32_T c8_i45;
  uint8_T c8_e_u[6];
  const mxArray *c8_e_y = NULL;
  int32_T c8_i46;
  uint8_T c8_f_u[6];
  const mxArray *c8_f_y = NULL;
  int32_T c8_i47;
  uint8_T c8_g_u[6];
  const mxArray *c8_g_y = NULL;
  SFc8_LessonIIInstanceStruct *chartInstance;
  chartInstance = (SFc8_LessonIIInstanceStruct *)chartInstanceVoid;
  c8_mxArrayOutData = NULL;
  c8_mxArrayOutData = NULL;
  c8_u = *(c8_stETj7QHScyDJroYDDeQcXH *)c8_inData;
  c8_y = NULL;
  if (!chartInstance->c8_latestPlayers_not_empty) {
    sf_mex_assign(&c8_y, sf_mex_create("y", NULL, 0, 0U, 1U, 0U, 2, 0, 0), false);
  } else {
    sf_mex_assign(&c8_y, sf_mex_createstruct("structure", 2, 1, 1), false);
    for (c8_i42 = 0; c8_i42 < 6; c8_i42++) {
      c8_b_u[c8_i42] = c8_u.x[c8_i42];
    }

    c8_b_y = NULL;
    sf_mex_assign(&c8_b_y, sf_mex_create("y", c8_b_u, 2, 0U, 1U, 0U, 2, 1, 6),
                  false);
    sf_mex_addfield(c8_y, c8_b_y, "x", "x", 0);
    for (c8_i43 = 0; c8_i43 < 6; c8_i43++) {
      c8_c_u[c8_i43] = c8_u.y[c8_i43];
    }

    c8_c_y = NULL;
    sf_mex_assign(&c8_c_y, sf_mex_create("y", c8_c_u, 2, 0U, 1U, 0U, 2, 1, 6),
                  false);
    sf_mex_addfield(c8_y, c8_c_y, "y", "y", 0);
    for (c8_i44 = 0; c8_i44 < 6; c8_i44++) {
      c8_d_u[c8_i44] = c8_u.orientation[c8_i44];
    }

    c8_d_y = NULL;
    sf_mex_assign(&c8_d_y, sf_mex_create("y", c8_d_u, 4, 0U, 1U, 0U, 2, 1, 6),
                  false);
    sf_mex_addfield(c8_y, c8_d_y, "orientation", "orientation", 0);
    for (c8_i45 = 0; c8_i45 < 6; c8_i45++) {
      c8_e_u[c8_i45] = c8_u.color[c8_i45];
    }

    c8_e_y = NULL;
    sf_mex_assign(&c8_e_y, sf_mex_create("y", c8_e_u, 3, 0U, 1U, 0U, 2, 1, 6),
                  false);
    sf_mex_addfield(c8_y, c8_e_y, "color", "color", 0);
    for (c8_i46 = 0; c8_i46 < 6; c8_i46++) {
      c8_f_u[c8_i46] = c8_u.position[c8_i46];
    }

    c8_f_y = NULL;
    sf_mex_assign(&c8_f_y, sf_mex_create("y", c8_f_u, 3, 0U, 1U, 0U, 2, 1, 6),
                  false);
    sf_mex_addfield(c8_y, c8_f_y, "position", "position", 0);
    for (c8_i47 = 0; c8_i47 < 6; c8_i47++) {
      c8_g_u[c8_i47] = c8_u.valid[c8_i47];
    }

    c8_g_y = NULL;
    sf_mex_assign(&c8_g_y, sf_mex_create("y", c8_g_u, 3, 0U, 1U, 0U, 2, 1, 6),
                  false);
    sf_mex_addfield(c8_y, c8_g_y, "valid", "valid", 0);
  }

  sf_mex_assign(&c8_mxArrayOutData, c8_y, false);
  return c8_mxArrayOutData;
}

static c8_stETj7QHScyDJroYDDeQcXH c8_e_emlrt_marshallIn
  (SFc8_LessonIIInstanceStruct *chartInstance, const mxArray *c8_b_latestPlayers,
   const char_T *c8_identifier)
{
  c8_stETj7QHScyDJroYDDeQcXH c8_y;
  emlrtMsgIdentifier c8_thisId;
  c8_thisId.fIdentifier = c8_identifier;
  c8_thisId.fParent = NULL;
  c8_y = c8_f_emlrt_marshallIn(chartInstance, sf_mex_dup(c8_b_latestPlayers),
    &c8_thisId);
  sf_mex_destroy(&c8_b_latestPlayers);
  return c8_y;
}

static c8_stETj7QHScyDJroYDDeQcXH c8_f_emlrt_marshallIn
  (SFc8_LessonIIInstanceStruct *chartInstance, const mxArray *c8_u, const
   emlrtMsgIdentifier *c8_parentId)
{
  c8_stETj7QHScyDJroYDDeQcXH c8_y;
  emlrtMsgIdentifier c8_thisId;
  static const char * c8_fieldNames[6] = { "x", "y", "orientation", "color",
    "position", "valid" };

  c8_thisId.fParent = c8_parentId;
  if (mxIsEmpty(c8_u)) {
    chartInstance->c8_latestPlayers_not_empty = false;
  } else {
    chartInstance->c8_latestPlayers_not_empty = true;
    sf_mex_check_struct(c8_parentId, c8_u, 6, c8_fieldNames, 0U, NULL);
    c8_thisId.fIdentifier = "x";
    c8_g_emlrt_marshallIn(chartInstance, sf_mex_dup(sf_mex_getfield(c8_u, "x",
      "x", 0)), &c8_thisId, c8_y.x);
    c8_thisId.fIdentifier = "y";
    c8_g_emlrt_marshallIn(chartInstance, sf_mex_dup(sf_mex_getfield(c8_u, "y",
      "y", 0)), &c8_thisId, c8_y.y);
    c8_thisId.fIdentifier = "orientation";
    c8_h_emlrt_marshallIn(chartInstance, sf_mex_dup(sf_mex_getfield(c8_u,
      "orientation", "orientation", 0)), &c8_thisId, c8_y.orientation);
    c8_thisId.fIdentifier = "color";
    c8_i_emlrt_marshallIn(chartInstance, sf_mex_dup(sf_mex_getfield(c8_u,
      "color", "color", 0)), &c8_thisId, c8_y.color);
    c8_thisId.fIdentifier = "position";
    c8_i_emlrt_marshallIn(chartInstance, sf_mex_dup(sf_mex_getfield(c8_u,
      "position", "position", 0)), &c8_thisId, c8_y.position);
    c8_thisId.fIdentifier = "valid";
    c8_i_emlrt_marshallIn(chartInstance, sf_mex_dup(sf_mex_getfield(c8_u,
      "valid", "valid", 0)), &c8_thisId, c8_y.valid);
  }

  sf_mex_destroy(&c8_u);
  return c8_y;
}

static void c8_g_emlrt_marshallIn(SFc8_LessonIIInstanceStruct *chartInstance,
  const mxArray *c8_u, const emlrtMsgIdentifier *c8_parentId, int8_T c8_y[6])
{
  int8_T c8_iv3[6];
  int32_T c8_i48;
  (void)chartInstance;
  sf_mex_import(c8_parentId, sf_mex_dup(c8_u), c8_iv3, 1, 2, 0U, 1, 0U, 2, 1, 6);
  for (c8_i48 = 0; c8_i48 < 6; c8_i48++) {
    c8_y[c8_i48] = c8_iv3[c8_i48];
  }

  sf_mex_destroy(&c8_u);
}

static void c8_h_emlrt_marshallIn(SFc8_LessonIIInstanceStruct *chartInstance,
  const mxArray *c8_u, const emlrtMsgIdentifier *c8_parentId, int16_T c8_y[6])
{
  int16_T c8_iv4[6];
  int32_T c8_i49;
  (void)chartInstance;
  sf_mex_import(c8_parentId, sf_mex_dup(c8_u), c8_iv4, 1, 4, 0U, 1, 0U, 2, 1, 6);
  for (c8_i49 = 0; c8_i49 < 6; c8_i49++) {
    c8_y[c8_i49] = c8_iv4[c8_i49];
  }

  sf_mex_destroy(&c8_u);
}

static void c8_i_emlrt_marshallIn(SFc8_LessonIIInstanceStruct *chartInstance,
  const mxArray *c8_u, const emlrtMsgIdentifier *c8_parentId, uint8_T c8_y[6])
{
  uint8_T c8_uv2[6];
  int32_T c8_i50;
  (void)chartInstance;
  sf_mex_import(c8_parentId, sf_mex_dup(c8_u), c8_uv2, 1, 3, 0U, 1, 0U, 2, 1, 6);
  for (c8_i50 = 0; c8_i50 < 6; c8_i50++) {
    c8_y[c8_i50] = c8_uv2[c8_i50];
  }

  sf_mex_destroy(&c8_u);
}

static void c8_b_sf_marshallIn(void *chartInstanceVoid, const mxArray
  *c8_mxArrayInData, const char_T *c8_varName, void *c8_outData)
{
  const mxArray *c8_b_latestPlayers;
  const char_T *c8_identifier;
  emlrtMsgIdentifier c8_thisId;
  c8_stETj7QHScyDJroYDDeQcXH c8_y;
  SFc8_LessonIIInstanceStruct *chartInstance;
  chartInstance = (SFc8_LessonIIInstanceStruct *)chartInstanceVoid;
  c8_b_latestPlayers = sf_mex_dup(c8_mxArrayInData);
  c8_identifier = c8_varName;
  c8_thisId.fIdentifier = c8_identifier;
  c8_thisId.fParent = NULL;
  c8_y = c8_f_emlrt_marshallIn(chartInstance, sf_mex_dup(c8_b_latestPlayers),
    &c8_thisId);
  sf_mex_destroy(&c8_b_latestPlayers);
  *(c8_stETj7QHScyDJroYDDeQcXH *)c8_outData = c8_y;
  sf_mex_destroy(&c8_mxArrayInData);
}

static const mxArray *c8_c_sf_marshallOut(void *chartInstanceVoid, void
  *c8_inData)
{
  const mxArray *c8_mxArrayOutData = NULL;
  c8_Ball c8_u;
  const mxArray *c8_y = NULL;
  int8_T c8_b_u;
  const mxArray *c8_b_y = NULL;
  int8_T c8_c_u;
  const mxArray *c8_c_y = NULL;
  uint8_T c8_d_u;
  const mxArray *c8_d_y = NULL;
  SFc8_LessonIIInstanceStruct *chartInstance;
  chartInstance = (SFc8_LessonIIInstanceStruct *)chartInstanceVoid;
  c8_mxArrayOutData = NULL;
  c8_u = *(c8_Ball *)c8_inData;
  c8_y = NULL;
  sf_mex_assign(&c8_y, sf_mex_createstruct("structure", 2, 1, 1), false);
  c8_b_u = c8_u.x;
  c8_b_y = NULL;
  sf_mex_assign(&c8_b_y, sf_mex_create("y", &c8_b_u, 2, 0U, 0U, 0U, 0), false);
  sf_mex_addfield(c8_y, c8_b_y, "x", "x", 0);
  c8_c_u = c8_u.y;
  c8_c_y = NULL;
  sf_mex_assign(&c8_c_y, sf_mex_create("y", &c8_c_u, 2, 0U, 0U, 0U, 0), false);
  sf_mex_addfield(c8_y, c8_c_y, "y", "y", 0);
  c8_d_u = c8_u.valid;
  c8_d_y = NULL;
  sf_mex_assign(&c8_d_y, sf_mex_create("y", &c8_d_u, 3, 0U, 0U, 0U, 0), false);
  sf_mex_addfield(c8_y, c8_d_y, "valid", "valid", 0);
  sf_mex_assign(&c8_mxArrayOutData, c8_y, false);
  return c8_mxArrayOutData;
}

static c8_Ball c8_j_emlrt_marshallIn(SFc8_LessonIIInstanceStruct *chartInstance,
  const mxArray *c8_b_ball, const char_T *c8_identifier)
{
  c8_Ball c8_y;
  emlrtMsgIdentifier c8_thisId;
  c8_thisId.fIdentifier = c8_identifier;
  c8_thisId.fParent = NULL;
  c8_y = c8_k_emlrt_marshallIn(chartInstance, sf_mex_dup(c8_b_ball), &c8_thisId);
  sf_mex_destroy(&c8_b_ball);
  return c8_y;
}

static c8_Ball c8_k_emlrt_marshallIn(SFc8_LessonIIInstanceStruct *chartInstance,
  const mxArray *c8_u, const emlrtMsgIdentifier *c8_parentId)
{
  c8_Ball c8_y;
  emlrtMsgIdentifier c8_thisId;
  static const char * c8_fieldNames[3] = { "x", "y", "valid" };

  c8_thisId.fParent = c8_parentId;
  sf_mex_check_struct(c8_parentId, c8_u, 3, c8_fieldNames, 0U, NULL);
  c8_thisId.fIdentifier = "x";
  c8_y.x = c8_c_emlrt_marshallIn(chartInstance, sf_mex_dup(sf_mex_getfield(c8_u,
    "x", "x", 0)), &c8_thisId);
  c8_thisId.fIdentifier = "y";
  c8_y.y = c8_c_emlrt_marshallIn(chartInstance, sf_mex_dup(sf_mex_getfield(c8_u,
    "y", "y", 0)), &c8_thisId);
  c8_thisId.fIdentifier = "valid";
  c8_y.valid = c8_d_emlrt_marshallIn(chartInstance, sf_mex_dup(sf_mex_getfield
    (c8_u, "valid", "valid", 0)), &c8_thisId);
  sf_mex_destroy(&c8_u);
  return c8_y;
}

static void c8_c_sf_marshallIn(void *chartInstanceVoid, const mxArray
  *c8_mxArrayInData, const char_T *c8_varName, void *c8_outData)
{
  const mxArray *c8_b_ball;
  const char_T *c8_identifier;
  emlrtMsgIdentifier c8_thisId;
  c8_Ball c8_y;
  SFc8_LessonIIInstanceStruct *chartInstance;
  chartInstance = (SFc8_LessonIIInstanceStruct *)chartInstanceVoid;
  c8_b_ball = sf_mex_dup(c8_mxArrayInData);
  c8_identifier = c8_varName;
  c8_thisId.fIdentifier = c8_identifier;
  c8_thisId.fParent = NULL;
  c8_y = c8_k_emlrt_marshallIn(chartInstance, sf_mex_dup(c8_b_ball), &c8_thisId);
  sf_mex_destroy(&c8_b_ball);
  *(c8_Ball *)c8_outData = c8_y;
  sf_mex_destroy(&c8_mxArrayInData);
}

static const mxArray *c8_d_sf_marshallOut(void *chartInstanceVoid, void
  *c8_inData)
{
  const mxArray *c8_mxArrayOutData;
  int32_T c8_i51;
  c8_Player c8_b_inData[6];
  int32_T c8_i52;
  c8_Player c8_u[6];
  const mxArray *c8_y = NULL;
  int32_T c8_i53;
  int32_T c8_iv5[2];
  int32_T c8_i54;
  const c8_Player *c8_r2;
  int8_T c8_b_u;
  const mxArray *c8_b_y = NULL;
  int8_T c8_c_u;
  const mxArray *c8_c_y = NULL;
  int16_T c8_d_u;
  const mxArray *c8_d_y = NULL;
  uint8_T c8_e_u;
  const mxArray *c8_e_y = NULL;
  uint8_T c8_f_u;
  const mxArray *c8_f_y = NULL;
  uint8_T c8_g_u;
  const mxArray *c8_g_y = NULL;
  SFc8_LessonIIInstanceStruct *chartInstance;
  chartInstance = (SFc8_LessonIIInstanceStruct *)chartInstanceVoid;
  c8_mxArrayOutData = NULL;
  c8_mxArrayOutData = NULL;
  for (c8_i51 = 0; c8_i51 < 6; c8_i51++) {
    c8_b_inData[c8_i51] = (*(c8_Player (*)[6])c8_inData)[c8_i51];
  }

  for (c8_i52 = 0; c8_i52 < 6; c8_i52++) {
    c8_u[c8_i52] = c8_b_inData[c8_i52];
  }

  c8_y = NULL;
  for (c8_i53 = 0; c8_i53 < 2; c8_i53++) {
    c8_iv5[c8_i53] = 1 + 5 * c8_i53;
  }

  sf_mex_assign(&c8_y, sf_mex_createstructarray("structure", 2, c8_iv5), false);
  for (c8_i54 = 0; c8_i54 < 6; c8_i54++) {
    c8_r2 = &c8_u[c8_i54];
    c8_b_u = c8_r2->x;
    c8_b_y = NULL;
    sf_mex_assign(&c8_b_y, sf_mex_create("y", &c8_b_u, 2, 0U, 0U, 0U, 0), false);
    sf_mex_addfield(c8_y, c8_b_y, "x", "x", c8_i54);
    c8_c_u = c8_r2->y;
    c8_c_y = NULL;
    sf_mex_assign(&c8_c_y, sf_mex_create("y", &c8_c_u, 2, 0U, 0U, 0U, 0), false);
    sf_mex_addfield(c8_y, c8_c_y, "y", "y", c8_i54);
    c8_d_u = c8_r2->orientation;
    c8_d_y = NULL;
    sf_mex_assign(&c8_d_y, sf_mex_create("y", &c8_d_u, 4, 0U, 0U, 0U, 0), false);
    sf_mex_addfield(c8_y, c8_d_y, "orientation", "orientation", c8_i54);
    c8_e_u = c8_r2->color;
    c8_e_y = NULL;
    sf_mex_assign(&c8_e_y, sf_mex_create("y", &c8_e_u, 3, 0U, 0U, 0U, 0), false);
    sf_mex_addfield(c8_y, c8_e_y, "color", "color", c8_i54);
    c8_f_u = c8_r2->position;
    c8_f_y = NULL;
    sf_mex_assign(&c8_f_y, sf_mex_create("y", &c8_f_u, 3, 0U, 0U, 0U, 0), false);
    sf_mex_addfield(c8_y, c8_f_y, "position", "position", c8_i54);
    c8_g_u = c8_r2->valid;
    c8_g_y = NULL;
    sf_mex_assign(&c8_g_y, sf_mex_create("y", &c8_g_u, 3, 0U, 0U, 0U, 0), false);
    sf_mex_addfield(c8_y, c8_g_y, "valid", "valid", c8_i54);
  }

  sf_mex_assign(&c8_mxArrayOutData, c8_y, false);
  return c8_mxArrayOutData;
}

static void c8_l_emlrt_marshallIn(SFc8_LessonIIInstanceStruct *chartInstance,
  const mxArray *c8_b_players, const char_T *c8_identifier, c8_Player c8_y[6])
{
  emlrtMsgIdentifier c8_thisId;
  c8_thisId.fIdentifier = c8_identifier;
  c8_thisId.fParent = NULL;
  c8_m_emlrt_marshallIn(chartInstance, sf_mex_dup(c8_b_players), &c8_thisId,
                        c8_y);
  sf_mex_destroy(&c8_b_players);
}

static void c8_m_emlrt_marshallIn(SFc8_LessonIIInstanceStruct *chartInstance,
  const mxArray *c8_u, const emlrtMsgIdentifier *c8_parentId, c8_Player c8_y[6])
{
  int32_T c8_i55;
  uint32_T c8_uv3[2];
  emlrtMsgIdentifier c8_thisId;
  static const char * c8_fieldNames[6] = { "x", "y", "orientation", "color",
    "position", "valid" };

  c8_Player (*c8_r3)[6];
  int32_T c8_i56;
  for (c8_i55 = 0; c8_i55 < 2; c8_i55++) {
    c8_uv3[c8_i55] = 1U + 5U * (uint32_T)c8_i55;
  }

  c8_thisId.fParent = c8_parentId;
  sf_mex_check_struct(c8_parentId, c8_u, 6, c8_fieldNames, 2U, c8_uv3);
  c8_r3 = (c8_Player (*)[6])c8_y;
  for (c8_i56 = 0; c8_i56 < 6; c8_i56++) {
    c8_thisId.fIdentifier = "x";
    (*c8_r3)[c8_i56].x = c8_c_emlrt_marshallIn(chartInstance, sf_mex_dup
      (sf_mex_getfield(c8_u, "x", "x", c8_i56)), &c8_thisId);
    c8_thisId.fIdentifier = "y";
    (*c8_r3)[c8_i56].y = c8_c_emlrt_marshallIn(chartInstance, sf_mex_dup
      (sf_mex_getfield(c8_u, "y", "y", c8_i56)), &c8_thisId);
    c8_thisId.fIdentifier = "orientation";
    (*c8_r3)[c8_i56].orientation = c8_n_emlrt_marshallIn(chartInstance,
      sf_mex_dup(sf_mex_getfield(c8_u, "orientation", "orientation", c8_i56)),
      &c8_thisId);
    c8_thisId.fIdentifier = "color";
    (*c8_r3)[c8_i56].color = c8_d_emlrt_marshallIn(chartInstance, sf_mex_dup
      (sf_mex_getfield(c8_u, "color", "color", c8_i56)), &c8_thisId);
    c8_thisId.fIdentifier = "position";
    (*c8_r3)[c8_i56].position = c8_d_emlrt_marshallIn(chartInstance, sf_mex_dup
      (sf_mex_getfield(c8_u, "position", "position", c8_i56)), &c8_thisId);
    c8_thisId.fIdentifier = "valid";
    (*c8_r3)[c8_i56].valid = c8_d_emlrt_marshallIn(chartInstance, sf_mex_dup
      (sf_mex_getfield(c8_u, "valid", "valid", c8_i56)), &c8_thisId);
  }

  sf_mex_destroy(&c8_u);
}

static int16_T c8_n_emlrt_marshallIn(SFc8_LessonIIInstanceStruct *chartInstance,
  const mxArray *c8_u, const emlrtMsgIdentifier *c8_parentId)
{
  int16_T c8_y;
  int16_T c8_i57;
  (void)chartInstance;
  sf_mex_import(c8_parentId, sf_mex_dup(c8_u), &c8_i57, 1, 4, 0U, 0, 0U, 0);
  c8_y = c8_i57;
  sf_mex_destroy(&c8_u);
  return c8_y;
}

static void c8_d_sf_marshallIn(void *chartInstanceVoid, const mxArray
  *c8_mxArrayInData, const char_T *c8_varName, void *c8_outData)
{
  const mxArray *c8_b_players;
  const char_T *c8_identifier;
  emlrtMsgIdentifier c8_thisId;
  c8_Player c8_y[6];
  int32_T c8_i58;
  SFc8_LessonIIInstanceStruct *chartInstance;
  chartInstance = (SFc8_LessonIIInstanceStruct *)chartInstanceVoid;
  c8_b_players = sf_mex_dup(c8_mxArrayInData);
  c8_identifier = c8_varName;
  c8_thisId.fIdentifier = c8_identifier;
  c8_thisId.fParent = NULL;
  c8_m_emlrt_marshallIn(chartInstance, sf_mex_dup(c8_b_players), &c8_thisId,
                        c8_y);
  sf_mex_destroy(&c8_b_players);
  for (c8_i58 = 0; c8_i58 < 6; c8_i58++) {
    (*(c8_Player (*)[6])c8_outData)[c8_i58] = c8_y[c8_i58];
  }

  sf_mex_destroy(&c8_mxArrayInData);
}

static const mxArray *c8_e_sf_marshallOut(void *chartInstanceVoid, c8_BallData
  *c8_inData_data, c8_BallData_size *c8_inData_elems_sizes)
{
  const mxArray *c8_mxArrayOutData = NULL;
  c8_BallData_size c8_u_elems_sizes;
  c8_BallData c8_u_data;
  const mxArray *c8_y = NULL;
  int32_T c8_u_sizes;
  int32_T c8_loop_ub;
  int32_T c8_i59;
  int8_T c8_b_u_data[2];
  const mxArray *c8_b_y = NULL;
  int32_T c8_b_u_sizes;
  int32_T c8_b_loop_ub;
  int32_T c8_i60;
  int8_T c8_c_u_data[2];
  const mxArray *c8_c_y = NULL;
  SFc8_LessonIIInstanceStruct *chartInstance;
  chartInstance = (SFc8_LessonIIInstanceStruct *)chartInstanceVoid;
  c8_mxArrayOutData = NULL;
  c8_u_elems_sizes = *c8_inData_elems_sizes;
  c8_u_data = *c8_inData_data;
  c8_y = NULL;
  sf_mex_assign(&c8_y, sf_mex_createstruct("structure", 2, 1, 1), false);
  c8_u_sizes = c8_u_elems_sizes.x;
  c8_loop_ub = c8_u_elems_sizes.x - 1;
  for (c8_i59 = 0; c8_i59 <= c8_loop_ub; c8_i59++) {
    c8_b_u_data[c8_i59] = c8_u_data.x[c8_i59];
  }

  c8_b_y = NULL;
  sf_mex_assign(&c8_b_y, sf_mex_create("y", c8_b_u_data, 2, 0U, 1U, 0U, 1,
    c8_u_sizes), false);
  sf_mex_addfield(c8_y, c8_b_y, "x", "x", 0);
  c8_b_u_sizes = c8_u_elems_sizes.y;
  c8_b_loop_ub = c8_u_elems_sizes.y - 1;
  for (c8_i60 = 0; c8_i60 <= c8_b_loop_ub; c8_i60++) {
    c8_c_u_data[c8_i60] = c8_u_data.y[c8_i60];
  }

  c8_c_y = NULL;
  sf_mex_assign(&c8_c_y, sf_mex_create("y", c8_c_u_data, 2, 0U, 1U, 0U, 1,
    c8_b_u_sizes), false);
  sf_mex_addfield(c8_y, c8_c_y, "y", "y", 0);
  sf_mex_assign(&c8_mxArrayOutData, c8_y, false);
  return c8_mxArrayOutData;
}

static const mxArray *c8_f_sf_marshallOut(void *chartInstanceVoid, c8_PlayerData
  *c8_inData_data, c8_PlayerData_size *c8_inData_elems_sizes)
{
  const mxArray *c8_mxArrayOutData = NULL;
  c8_PlayerData_size c8_u_elems_sizes;
  c8_PlayerData c8_u_data;
  const mxArray *c8_y = NULL;
  int32_T c8_u_sizes[2];
  int32_T c8_u;
  int32_T c8_b_u;
  int32_T c8_loop_ub;
  int32_T c8_i61;
  int8_T c8_b_u_data[6];
  const mxArray *c8_b_y = NULL;
  int32_T c8_b_u_sizes[2];
  int32_T c8_c_u;
  int32_T c8_d_u;
  int32_T c8_b_loop_ub;
  int32_T c8_i62;
  int8_T c8_c_u_data[6];
  const mxArray *c8_c_y = NULL;
  int32_T c8_c_u_sizes[2];
  int32_T c8_e_u;
  int32_T c8_f_u;
  int32_T c8_c_loop_ub;
  int32_T c8_i63;
  int16_T c8_d_u_data[6];
  const mxArray *c8_d_y = NULL;
  int32_T c8_d_u_sizes[2];
  int32_T c8_g_u;
  int32_T c8_h_u;
  int32_T c8_d_loop_ub;
  int32_T c8_i64;
  uint8_T c8_e_u_data[6];
  const mxArray *c8_e_y = NULL;
  SFc8_LessonIIInstanceStruct *chartInstance;
  chartInstance = (SFc8_LessonIIInstanceStruct *)chartInstanceVoid;
  c8_mxArrayOutData = NULL;
  c8_u_elems_sizes = *c8_inData_elems_sizes;
  c8_u_data = *c8_inData_data;
  c8_y = NULL;
  sf_mex_assign(&c8_y, sf_mex_createstruct("structure", 2, 1, 1), false);
  c8_u_sizes[0] = c8_u_elems_sizes.x[0];
  c8_u_sizes[1] = c8_u_elems_sizes.x[1];
  c8_u = c8_u_sizes[0];
  c8_b_u = c8_u_sizes[1];
  c8_loop_ub = c8_u_elems_sizes.x[0] * c8_u_elems_sizes.x[1] - 1;
  for (c8_i61 = 0; c8_i61 <= c8_loop_ub; c8_i61++) {
    c8_b_u_data[c8_i61] = c8_u_data.x[c8_i61];
  }

  c8_b_y = NULL;
  sf_mex_assign(&c8_b_y, sf_mex_create("y", c8_b_u_data, 2, 0U, 1U, 0U, 2,
    c8_u_sizes[0], c8_u_sizes[1]), false);
  sf_mex_addfield(c8_y, c8_b_y, "x", "x", 0);
  c8_b_u_sizes[0] = c8_u_elems_sizes.y[0];
  c8_b_u_sizes[1] = c8_u_elems_sizes.y[1];
  c8_c_u = c8_b_u_sizes[0];
  c8_d_u = c8_b_u_sizes[1];
  c8_b_loop_ub = c8_u_elems_sizes.y[0] * c8_u_elems_sizes.y[1] - 1;
  for (c8_i62 = 0; c8_i62 <= c8_b_loop_ub; c8_i62++) {
    c8_c_u_data[c8_i62] = c8_u_data.y[c8_i62];
  }

  c8_c_y = NULL;
  sf_mex_assign(&c8_c_y, sf_mex_create("y", c8_c_u_data, 2, 0U, 1U, 0U, 2,
    c8_b_u_sizes[0], c8_b_u_sizes[1]), false);
  sf_mex_addfield(c8_y, c8_c_y, "y", "y", 0);
  c8_c_u_sizes[0] = c8_u_elems_sizes.orientation[0];
  c8_c_u_sizes[1] = c8_u_elems_sizes.orientation[1];
  c8_e_u = c8_c_u_sizes[0];
  c8_f_u = c8_c_u_sizes[1];
  c8_c_loop_ub = c8_u_elems_sizes.orientation[0] * c8_u_elems_sizes.orientation
    [1] - 1;
  for (c8_i63 = 0; c8_i63 <= c8_c_loop_ub; c8_i63++) {
    c8_d_u_data[c8_i63] = c8_u_data.orientation[c8_i63];
  }

  c8_d_y = NULL;
  sf_mex_assign(&c8_d_y, sf_mex_create("y", c8_d_u_data, 4, 0U, 1U, 0U, 2,
    c8_c_u_sizes[0], c8_c_u_sizes[1]), false);
  sf_mex_addfield(c8_y, c8_d_y, "orientation", "orientation", 0);
  c8_d_u_sizes[0] = c8_u_elems_sizes.color[0];
  c8_d_u_sizes[1] = c8_u_elems_sizes.color[1];
  c8_g_u = c8_d_u_sizes[0];
  c8_h_u = c8_d_u_sizes[1];
  c8_d_loop_ub = c8_u_elems_sizes.color[0] * c8_u_elems_sizes.color[1] - 1;
  for (c8_i64 = 0; c8_i64 <= c8_d_loop_ub; c8_i64++) {
    c8_e_u_data[c8_i64] = c8_u_data.color[c8_i64];
  }

  c8_e_y = NULL;
  sf_mex_assign(&c8_e_y, sf_mex_create("y", c8_e_u_data, 3, 0U, 1U, 0U, 2,
    c8_d_u_sizes[0], c8_d_u_sizes[1]), false);
  sf_mex_addfield(c8_y, c8_e_y, "color", "color", 0);
  sf_mex_assign(&c8_mxArrayOutData, c8_y, false);
  return c8_mxArrayOutData;
}

static const mxArray *c8_g_sf_marshallOut(void *chartInstanceVoid, void
  *c8_inData)
{
  const mxArray *c8_mxArrayOutData = NULL;
  real_T c8_u;
  const mxArray *c8_y = NULL;
  SFc8_LessonIIInstanceStruct *chartInstance;
  chartInstance = (SFc8_LessonIIInstanceStruct *)chartInstanceVoid;
  c8_mxArrayOutData = NULL;
  c8_u = *(real_T *)c8_inData;
  c8_y = NULL;
  sf_mex_assign(&c8_y, sf_mex_create("y", &c8_u, 0, 0U, 0U, 0U, 0), false);
  sf_mex_assign(&c8_mxArrayOutData, c8_y, false);
  return c8_mxArrayOutData;
}

static real_T c8_o_emlrt_marshallIn(SFc8_LessonIIInstanceStruct *chartInstance,
  const mxArray *c8_u, const emlrtMsgIdentifier *c8_parentId)
{
  real_T c8_y;
  real_T c8_d2;
  (void)chartInstance;
  sf_mex_import(c8_parentId, sf_mex_dup(c8_u), &c8_d2, 1, 0, 0U, 0, 0U, 0);
  c8_y = c8_d2;
  sf_mex_destroy(&c8_u);
  return c8_y;
}

static void c8_e_sf_marshallIn(void *chartInstanceVoid, const mxArray
  *c8_mxArrayInData, const char_T *c8_varName, void *c8_outData)
{
  const mxArray *c8_nargout;
  const char_T *c8_identifier;
  emlrtMsgIdentifier c8_thisId;
  real_T c8_y;
  SFc8_LessonIIInstanceStruct *chartInstance;
  chartInstance = (SFc8_LessonIIInstanceStruct *)chartInstanceVoid;
  c8_nargout = sf_mex_dup(c8_mxArrayInData);
  c8_identifier = c8_varName;
  c8_thisId.fIdentifier = c8_identifier;
  c8_thisId.fParent = NULL;
  c8_y = c8_o_emlrt_marshallIn(chartInstance, sf_mex_dup(c8_nargout), &c8_thisId);
  sf_mex_destroy(&c8_nargout);
  *(real_T *)c8_outData = c8_y;
  sf_mex_destroy(&c8_mxArrayInData);
}

static const mxArray *c8_h_sf_marshallOut(void *chartInstanceVoid, void
  *c8_inData)
{
  const mxArray *c8_mxArrayOutData = NULL;
  c8_Player c8_u;
  const mxArray *c8_y = NULL;
  int8_T c8_b_u;
  const mxArray *c8_b_y = NULL;
  int8_T c8_c_u;
  const mxArray *c8_c_y = NULL;
  int16_T c8_d_u;
  const mxArray *c8_d_y = NULL;
  uint8_T c8_e_u;
  const mxArray *c8_e_y = NULL;
  uint8_T c8_f_u;
  const mxArray *c8_f_y = NULL;
  uint8_T c8_g_u;
  const mxArray *c8_g_y = NULL;
  SFc8_LessonIIInstanceStruct *chartInstance;
  chartInstance = (SFc8_LessonIIInstanceStruct *)chartInstanceVoid;
  c8_mxArrayOutData = NULL;
  c8_u = *(c8_Player *)c8_inData;
  c8_y = NULL;
  sf_mex_assign(&c8_y, sf_mex_createstruct("structure", 2, 1, 1), false);
  c8_b_u = c8_u.x;
  c8_b_y = NULL;
  sf_mex_assign(&c8_b_y, sf_mex_create("y", &c8_b_u, 2, 0U, 0U, 0U, 0), false);
  sf_mex_addfield(c8_y, c8_b_y, "x", "x", 0);
  c8_c_u = c8_u.y;
  c8_c_y = NULL;
  sf_mex_assign(&c8_c_y, sf_mex_create("y", &c8_c_u, 2, 0U, 0U, 0U, 0), false);
  sf_mex_addfield(c8_y, c8_c_y, "y", "y", 0);
  c8_d_u = c8_u.orientation;
  c8_d_y = NULL;
  sf_mex_assign(&c8_d_y, sf_mex_create("y", &c8_d_u, 4, 0U, 0U, 0U, 0), false);
  sf_mex_addfield(c8_y, c8_d_y, "orientation", "orientation", 0);
  c8_e_u = c8_u.color;
  c8_e_y = NULL;
  sf_mex_assign(&c8_e_y, sf_mex_create("y", &c8_e_u, 3, 0U, 0U, 0U, 0), false);
  sf_mex_addfield(c8_y, c8_e_y, "color", "color", 0);
  c8_f_u = c8_u.position;
  c8_f_y = NULL;
  sf_mex_assign(&c8_f_y, sf_mex_create("y", &c8_f_u, 3, 0U, 0U, 0U, 0), false);
  sf_mex_addfield(c8_y, c8_f_y, "position", "position", 0);
  c8_g_u = c8_u.valid;
  c8_g_y = NULL;
  sf_mex_assign(&c8_g_y, sf_mex_create("y", &c8_g_u, 3, 0U, 0U, 0U, 0), false);
  sf_mex_addfield(c8_y, c8_g_y, "valid", "valid", 0);
  sf_mex_assign(&c8_mxArrayOutData, c8_y, false);
  return c8_mxArrayOutData;
}

static c8_Player c8_p_emlrt_marshallIn(SFc8_LessonIIInstanceStruct
  *chartInstance, const mxArray *c8_u, const emlrtMsgIdentifier *c8_parentId)
{
  c8_Player c8_y;
  emlrtMsgIdentifier c8_thisId;
  static const char * c8_fieldNames[6] = { "x", "y", "orientation", "color",
    "position", "valid" };

  c8_thisId.fParent = c8_parentId;
  sf_mex_check_struct(c8_parentId, c8_u, 6, c8_fieldNames, 0U, NULL);
  c8_thisId.fIdentifier = "x";
  c8_y.x = c8_c_emlrt_marshallIn(chartInstance, sf_mex_dup(sf_mex_getfield(c8_u,
    "x", "x", 0)), &c8_thisId);
  c8_thisId.fIdentifier = "y";
  c8_y.y = c8_c_emlrt_marshallIn(chartInstance, sf_mex_dup(sf_mex_getfield(c8_u,
    "y", "y", 0)), &c8_thisId);
  c8_thisId.fIdentifier = "orientation";
  c8_y.orientation = c8_n_emlrt_marshallIn(chartInstance, sf_mex_dup
    (sf_mex_getfield(c8_u, "orientation", "orientation", 0)), &c8_thisId);
  c8_thisId.fIdentifier = "color";
  c8_y.color = c8_d_emlrt_marshallIn(chartInstance, sf_mex_dup(sf_mex_getfield
    (c8_u, "color", "color", 0)), &c8_thisId);
  c8_thisId.fIdentifier = "position";
  c8_y.position = c8_d_emlrt_marshallIn(chartInstance, sf_mex_dup
    (sf_mex_getfield(c8_u, "position", "position", 0)), &c8_thisId);
  c8_thisId.fIdentifier = "valid";
  c8_y.valid = c8_d_emlrt_marshallIn(chartInstance, sf_mex_dup(sf_mex_getfield
    (c8_u, "valid", "valid", 0)), &c8_thisId);
  sf_mex_destroy(&c8_u);
  return c8_y;
}

static void c8_f_sf_marshallIn(void *chartInstanceVoid, const mxArray
  *c8_mxArrayInData, const char_T *c8_varName, void *c8_outData)
{
  const mxArray *c8_tempEmpty;
  const char_T *c8_identifier;
  emlrtMsgIdentifier c8_thisId;
  c8_Player c8_y;
  SFc8_LessonIIInstanceStruct *chartInstance;
  chartInstance = (SFc8_LessonIIInstanceStruct *)chartInstanceVoid;
  c8_tempEmpty = sf_mex_dup(c8_mxArrayInData);
  c8_identifier = c8_varName;
  c8_thisId.fIdentifier = c8_identifier;
  c8_thisId.fParent = NULL;
  c8_y = c8_p_emlrt_marshallIn(chartInstance, sf_mex_dup(c8_tempEmpty),
    &c8_thisId);
  sf_mex_destroy(&c8_tempEmpty);
  *(c8_Player *)c8_outData = c8_y;
  sf_mex_destroy(&c8_mxArrayInData);
}

static const mxArray *c8_i_sf_marshallOut(void *chartInstanceVoid, void
  *c8_inData)
{
  const mxArray *c8_mxArrayOutData = NULL;
  int32_T c8_u;
  const mxArray *c8_y = NULL;
  SFc8_LessonIIInstanceStruct *chartInstance;
  chartInstance = (SFc8_LessonIIInstanceStruct *)chartInstanceVoid;
  c8_mxArrayOutData = NULL;
  c8_u = *(int32_T *)c8_inData;
  c8_y = NULL;
  sf_mex_assign(&c8_y, sf_mex_create("y", &c8_u, 6, 0U, 0U, 0U, 0), false);
  sf_mex_assign(&c8_mxArrayOutData, c8_y, false);
  return c8_mxArrayOutData;
}

static int32_T c8_q_emlrt_marshallIn(SFc8_LessonIIInstanceStruct *chartInstance,
  const mxArray *c8_u, const emlrtMsgIdentifier *c8_parentId)
{
  int32_T c8_y;
  int32_T c8_i65;
  (void)chartInstance;
  sf_mex_import(c8_parentId, sf_mex_dup(c8_u), &c8_i65, 1, 6, 0U, 0, 0U, 0);
  c8_y = c8_i65;
  sf_mex_destroy(&c8_u);
  return c8_y;
}

static void c8_g_sf_marshallIn(void *chartInstanceVoid, const mxArray
  *c8_mxArrayInData, const char_T *c8_varName, void *c8_outData)
{
  const mxArray *c8_temp;
  const char_T *c8_identifier;
  emlrtMsgIdentifier c8_thisId;
  int32_T c8_y;
  SFc8_LessonIIInstanceStruct *chartInstance;
  chartInstance = (SFc8_LessonIIInstanceStruct *)chartInstanceVoid;
  c8_temp = sf_mex_dup(c8_mxArrayInData);
  c8_identifier = c8_varName;
  c8_thisId.fIdentifier = c8_identifier;
  c8_thisId.fParent = NULL;
  c8_y = c8_q_emlrt_marshallIn(chartInstance, sf_mex_dup(c8_temp), &c8_thisId);
  sf_mex_destroy(&c8_temp);
  *(int32_T *)c8_outData = c8_y;
  sf_mex_destroy(&c8_mxArrayInData);
}

static const mxArray *c8_j_sf_marshallOut(void *chartInstanceVoid, void
  *c8_inData)
{
  const mxArray *c8_mxArrayOutData = NULL;
  uint8_T c8_u;
  const mxArray *c8_y = NULL;
  SFc8_LessonIIInstanceStruct *chartInstance;
  chartInstance = (SFc8_LessonIIInstanceStruct *)chartInstanceVoid;
  c8_mxArrayOutData = NULL;
  c8_u = *(uint8_T *)c8_inData;
  c8_y = NULL;
  sf_mex_assign(&c8_y, sf_mex_create("y", &c8_u, 3, 0U, 0U, 0U, 0), false);
  sf_mex_assign(&c8_mxArrayOutData, c8_y, false);
  return c8_mxArrayOutData;
}

static uint8_T c8_r_emlrt_marshallIn(SFc8_LessonIIInstanceStruct *chartInstance,
  const mxArray *c8_index, const char_T *c8_identifier)
{
  uint8_T c8_y;
  emlrtMsgIdentifier c8_thisId;
  c8_thisId.fIdentifier = c8_identifier;
  c8_thisId.fParent = NULL;
  c8_y = c8_d_emlrt_marshallIn(chartInstance, sf_mex_dup(c8_index), &c8_thisId);
  sf_mex_destroy(&c8_index);
  return c8_y;
}

static void c8_h_sf_marshallIn(void *chartInstanceVoid, const mxArray
  *c8_mxArrayInData, const char_T *c8_varName, void *c8_outData)
{
  const mxArray *c8_index;
  const char_T *c8_identifier;
  emlrtMsgIdentifier c8_thisId;
  uint8_T c8_y;
  SFc8_LessonIIInstanceStruct *chartInstance;
  chartInstance = (SFc8_LessonIIInstanceStruct *)chartInstanceVoid;
  c8_index = sf_mex_dup(c8_mxArrayInData);
  c8_identifier = c8_varName;
  c8_thisId.fIdentifier = c8_identifier;
  c8_thisId.fParent = NULL;
  c8_y = c8_d_emlrt_marshallIn(chartInstance, sf_mex_dup(c8_index), &c8_thisId);
  sf_mex_destroy(&c8_index);
  *(uint8_T *)c8_outData = c8_y;
  sf_mex_destroy(&c8_mxArrayInData);
}

static const mxArray *c8_k_sf_marshallOut(void *chartInstanceVoid, void
  *c8_inData)
{
  const mxArray *c8_mxArrayOutData = NULL;
  int32_T c8_i66;
  int8_T c8_b_inData[2];
  int32_T c8_i67;
  int8_T c8_u[2];
  const mxArray *c8_y = NULL;
  SFc8_LessonIIInstanceStruct *chartInstance;
  chartInstance = (SFc8_LessonIIInstanceStruct *)chartInstanceVoid;
  c8_mxArrayOutData = NULL;
  for (c8_i66 = 0; c8_i66 < 2; c8_i66++) {
    c8_b_inData[c8_i66] = (*(int8_T (*)[2])c8_inData)[c8_i66];
  }

  for (c8_i67 = 0; c8_i67 < 2; c8_i67++) {
    c8_u[c8_i67] = c8_b_inData[c8_i67];
  }

  c8_y = NULL;
  sf_mex_assign(&c8_y, sf_mex_create("y", c8_u, 2, 0U, 1U, 0U, 2, 1, 2), false);
  sf_mex_assign(&c8_mxArrayOutData, c8_y, false);
  return c8_mxArrayOutData;
}

static void c8_s_emlrt_marshallIn(SFc8_LessonIIInstanceStruct *chartInstance,
  const mxArray *c8_u, const emlrtMsgIdentifier *c8_parentId, int8_T c8_y[2])
{
  int8_T c8_iv6[2];
  int32_T c8_i68;
  (void)chartInstance;
  sf_mex_import(c8_parentId, sf_mex_dup(c8_u), c8_iv6, 1, 2, 0U, 1, 0U, 2, 1, 2);
  for (c8_i68 = 0; c8_i68 < 2; c8_i68++) {
    c8_y[c8_i68] = c8_iv6[c8_i68];
  }

  sf_mex_destroy(&c8_u);
}

static void c8_i_sf_marshallIn(void *chartInstanceVoid, const mxArray
  *c8_mxArrayInData, const char_T *c8_varName, void *c8_outData)
{
  const mxArray *c8_pos;
  const char_T *c8_identifier;
  emlrtMsgIdentifier c8_thisId;
  int8_T c8_y[2];
  int32_T c8_i69;
  SFc8_LessonIIInstanceStruct *chartInstance;
  chartInstance = (SFc8_LessonIIInstanceStruct *)chartInstanceVoid;
  c8_pos = sf_mex_dup(c8_mxArrayInData);
  c8_identifier = c8_varName;
  c8_thisId.fIdentifier = c8_identifier;
  c8_thisId.fParent = NULL;
  c8_s_emlrt_marshallIn(chartInstance, sf_mex_dup(c8_pos), &c8_thisId, c8_y);
  sf_mex_destroy(&c8_pos);
  for (c8_i69 = 0; c8_i69 < 2; c8_i69++) {
    (*(int8_T (*)[2])c8_outData)[c8_i69] = c8_y[c8_i69];
  }

  sf_mex_destroy(&c8_mxArrayInData);
}

const mxArray *sf_c8_LessonII_get_eml_resolved_functions_info(void)
{
  const mxArray *c8_nameCaptureInfo = NULL;
  c8_nameCaptureInfo = NULL;
  sf_mex_assign(&c8_nameCaptureInfo, sf_mex_createstruct("structure", 2, 35, 1),
                false);
  c8_info_helper(&c8_nameCaptureInfo);
  sf_mex_emlrtNameCapturePostProcessR2012a(&c8_nameCaptureInfo);
  return c8_nameCaptureInfo;
}

static void c8_info_helper(const mxArray **c8_info)
{
  const mxArray *c8_rhs0 = NULL;
  const mxArray *c8_lhs0 = NULL;
  const mxArray *c8_rhs1 = NULL;
  const mxArray *c8_lhs1 = NULL;
  const mxArray *c8_rhs2 = NULL;
  const mxArray *c8_lhs2 = NULL;
  const mxArray *c8_rhs3 = NULL;
  const mxArray *c8_lhs3 = NULL;
  const mxArray *c8_rhs4 = NULL;
  const mxArray *c8_lhs4 = NULL;
  const mxArray *c8_rhs5 = NULL;
  const mxArray *c8_lhs5 = NULL;
  const mxArray *c8_rhs6 = NULL;
  const mxArray *c8_lhs6 = NULL;
  const mxArray *c8_rhs7 = NULL;
  const mxArray *c8_lhs7 = NULL;
  const mxArray *c8_rhs8 = NULL;
  const mxArray *c8_lhs8 = NULL;
  const mxArray *c8_rhs9 = NULL;
  const mxArray *c8_lhs9 = NULL;
  const mxArray *c8_rhs10 = NULL;
  const mxArray *c8_lhs10 = NULL;
  const mxArray *c8_rhs11 = NULL;
  const mxArray *c8_lhs11 = NULL;
  const mxArray *c8_rhs12 = NULL;
  const mxArray *c8_lhs12 = NULL;
  const mxArray *c8_rhs13 = NULL;
  const mxArray *c8_lhs13 = NULL;
  const mxArray *c8_rhs14 = NULL;
  const mxArray *c8_lhs14 = NULL;
  const mxArray *c8_rhs15 = NULL;
  const mxArray *c8_lhs15 = NULL;
  const mxArray *c8_rhs16 = NULL;
  const mxArray *c8_lhs16 = NULL;
  const mxArray *c8_rhs17 = NULL;
  const mxArray *c8_lhs17 = NULL;
  const mxArray *c8_rhs18 = NULL;
  const mxArray *c8_lhs18 = NULL;
  const mxArray *c8_rhs19 = NULL;
  const mxArray *c8_lhs19 = NULL;
  const mxArray *c8_rhs20 = NULL;
  const mxArray *c8_lhs20 = NULL;
  const mxArray *c8_rhs21 = NULL;
  const mxArray *c8_lhs21 = NULL;
  const mxArray *c8_rhs22 = NULL;
  const mxArray *c8_lhs22 = NULL;
  const mxArray *c8_rhs23 = NULL;
  const mxArray *c8_lhs23 = NULL;
  const mxArray *c8_rhs24 = NULL;
  const mxArray *c8_lhs24 = NULL;
  const mxArray *c8_rhs25 = NULL;
  const mxArray *c8_lhs25 = NULL;
  const mxArray *c8_rhs26 = NULL;
  const mxArray *c8_lhs26 = NULL;
  const mxArray *c8_rhs27 = NULL;
  const mxArray *c8_lhs27 = NULL;
  const mxArray *c8_rhs28 = NULL;
  const mxArray *c8_lhs28 = NULL;
  const mxArray *c8_rhs29 = NULL;
  const mxArray *c8_lhs29 = NULL;
  const mxArray *c8_rhs30 = NULL;
  const mxArray *c8_lhs30 = NULL;
  const mxArray *c8_rhs31 = NULL;
  const mxArray *c8_lhs31 = NULL;
  const mxArray *c8_rhs32 = NULL;
  const mxArray *c8_lhs32 = NULL;
  const mxArray *c8_rhs33 = NULL;
  const mxArray *c8_lhs33 = NULL;
  const mxArray *c8_rhs34 = NULL;
  const mxArray *c8_lhs34 = NULL;
  sf_mex_addfield(*c8_info, c8_emlrt_marshallOut(""), "context", "context", 0);
  sf_mex_addfield(*c8_info, c8_emlrt_marshallOut("eml_mtimes_helper"), "name",
                  "name", 0);
  sf_mex_addfield(*c8_info, c8_emlrt_marshallOut(""), "dominantType",
                  "dominantType", 0);
  sf_mex_addfield(*c8_info, c8_emlrt_marshallOut(
    "[ILXE]$matlabroot$/toolbox/eml/lib/matlab/ops/eml_mtimes_helper.m"),
                  "resolved", "resolved", 0);
  sf_mex_addfield(*c8_info, c8_b_emlrt_marshallOut(1383880894U), "fileTimeLo",
                  "fileTimeLo", 0);
  sf_mex_addfield(*c8_info, c8_b_emlrt_marshallOut(0U), "fileTimeHi",
                  "fileTimeHi", 0);
  sf_mex_addfield(*c8_info, c8_b_emlrt_marshallOut(0U), "mFileTimeLo",
                  "mFileTimeLo", 0);
  sf_mex_addfield(*c8_info, c8_b_emlrt_marshallOut(0U), "mFileTimeHi",
                  "mFileTimeHi", 0);
  sf_mex_assign(&c8_rhs0, sf_mex_createcellmatrix(0, 1), false);
  sf_mex_assign(&c8_lhs0, sf_mex_createcellmatrix(0, 1), false);
  sf_mex_addfield(*c8_info, sf_mex_duplicatearraysafe(&c8_rhs0), "rhs", "rhs", 0);
  sf_mex_addfield(*c8_info, sf_mex_duplicatearraysafe(&c8_lhs0), "lhs", "lhs", 0);
  sf_mex_addfield(*c8_info, c8_emlrt_marshallOut(
    "[ILXE]$matlabroot$/toolbox/eml/lib/matlab/ops/eml_mtimes_helper.m!common_checks"),
                  "context", "context", 1);
  sf_mex_addfield(*c8_info, c8_emlrt_marshallOut(
    "coder.internal.isBuiltInNumeric"), "name", "name", 1);
  sf_mex_addfield(*c8_info, c8_emlrt_marshallOut("int16"), "dominantType",
                  "dominantType", 1);
  sf_mex_addfield(*c8_info, c8_emlrt_marshallOut(
    "[ILXE]$matlabroot$/toolbox/shared/coder/coder/+coder/+internal/isBuiltInNumeric.m"),
                  "resolved", "resolved", 1);
  sf_mex_addfield(*c8_info, c8_b_emlrt_marshallOut(1395935456U), "fileTimeLo",
                  "fileTimeLo", 1);
  sf_mex_addfield(*c8_info, c8_b_emlrt_marshallOut(0U), "fileTimeHi",
                  "fileTimeHi", 1);
  sf_mex_addfield(*c8_info, c8_b_emlrt_marshallOut(0U), "mFileTimeLo",
                  "mFileTimeLo", 1);
  sf_mex_addfield(*c8_info, c8_b_emlrt_marshallOut(0U), "mFileTimeHi",
                  "mFileTimeHi", 1);
  sf_mex_assign(&c8_rhs1, sf_mex_createcellmatrix(0, 1), false);
  sf_mex_assign(&c8_lhs1, sf_mex_createcellmatrix(0, 1), false);
  sf_mex_addfield(*c8_info, sf_mex_duplicatearraysafe(&c8_rhs1), "rhs", "rhs", 1);
  sf_mex_addfield(*c8_info, sf_mex_duplicatearraysafe(&c8_lhs1), "lhs", "lhs", 1);
  sf_mex_addfield(*c8_info, c8_emlrt_marshallOut(""), "context", "context", 2);
  sf_mex_addfield(*c8_info, c8_emlrt_marshallOut("length"), "name", "name", 2);
  sf_mex_addfield(*c8_info, c8_emlrt_marshallOut("int8"), "dominantType",
                  "dominantType", 2);
  sf_mex_addfield(*c8_info, c8_emlrt_marshallOut(
    "[ILXE]$matlabroot$/toolbox/eml/lib/matlab/elmat/length.m"), "resolved",
                  "resolved", 2);
  sf_mex_addfield(*c8_info, c8_b_emlrt_marshallOut(1303149806U), "fileTimeLo",
                  "fileTimeLo", 2);
  sf_mex_addfield(*c8_info, c8_b_emlrt_marshallOut(0U), "fileTimeHi",
                  "fileTimeHi", 2);
  sf_mex_addfield(*c8_info, c8_b_emlrt_marshallOut(0U), "mFileTimeLo",
                  "mFileTimeLo", 2);
  sf_mex_addfield(*c8_info, c8_b_emlrt_marshallOut(0U), "mFileTimeHi",
                  "mFileTimeHi", 2);
  sf_mex_assign(&c8_rhs2, sf_mex_createcellmatrix(0, 1), false);
  sf_mex_assign(&c8_lhs2, sf_mex_createcellmatrix(0, 1), false);
  sf_mex_addfield(*c8_info, sf_mex_duplicatearraysafe(&c8_rhs2), "rhs", "rhs", 2);
  sf_mex_addfield(*c8_info, sf_mex_duplicatearraysafe(&c8_lhs2), "lhs", "lhs", 2);
  sf_mex_addfield(*c8_info, c8_emlrt_marshallOut(
    "[ILXE]$matlabroot$/toolbox/eml/lib/matlab/elmat/length.m!intlength"),
                  "context", "context", 3);
  sf_mex_addfield(*c8_info, c8_emlrt_marshallOut("eml_index_class"), "name",
                  "name", 3);
  sf_mex_addfield(*c8_info, c8_emlrt_marshallOut(""), "dominantType",
                  "dominantType", 3);
  sf_mex_addfield(*c8_info, c8_emlrt_marshallOut(
    "[ILXE]$matlabroot$/toolbox/eml/lib/matlab/eml/eml_index_class.m"),
                  "resolved", "resolved", 3);
  sf_mex_addfield(*c8_info, c8_b_emlrt_marshallOut(1323174178U), "fileTimeLo",
                  "fileTimeLo", 3);
  sf_mex_addfield(*c8_info, c8_b_emlrt_marshallOut(0U), "fileTimeHi",
                  "fileTimeHi", 3);
  sf_mex_addfield(*c8_info, c8_b_emlrt_marshallOut(0U), "mFileTimeLo",
                  "mFileTimeLo", 3);
  sf_mex_addfield(*c8_info, c8_b_emlrt_marshallOut(0U), "mFileTimeHi",
                  "mFileTimeHi", 3);
  sf_mex_assign(&c8_rhs3, sf_mex_createcellmatrix(0, 1), false);
  sf_mex_assign(&c8_lhs3, sf_mex_createcellmatrix(0, 1), false);
  sf_mex_addfield(*c8_info, sf_mex_duplicatearraysafe(&c8_rhs3), "rhs", "rhs", 3);
  sf_mex_addfield(*c8_info, sf_mex_duplicatearraysafe(&c8_lhs3), "lhs", "lhs", 3);
  sf_mex_addfield(*c8_info, c8_emlrt_marshallOut(
    "[ILXE]$matlabroot$/toolbox/eml/lib/matlab/elmat/length.m!intlength"),
                  "context", "context", 4);
  sf_mex_addfield(*c8_info, c8_emlrt_marshallOut("coder.internal.indexIntRelop"),
                  "name", "name", 4);
  sf_mex_addfield(*c8_info, c8_emlrt_marshallOut(""), "dominantType",
                  "dominantType", 4);
  sf_mex_addfield(*c8_info, c8_emlrt_marshallOut(
    "[ILXE]$matlabroot$/toolbox/shared/coder/coder/+coder/+internal/indexIntRelop.m"),
                  "resolved", "resolved", 4);
  sf_mex_addfield(*c8_info, c8_b_emlrt_marshallOut(1326731922U), "fileTimeLo",
                  "fileTimeLo", 4);
  sf_mex_addfield(*c8_info, c8_b_emlrt_marshallOut(0U), "fileTimeHi",
                  "fileTimeHi", 4);
  sf_mex_addfield(*c8_info, c8_b_emlrt_marshallOut(0U), "mFileTimeLo",
                  "mFileTimeLo", 4);
  sf_mex_addfield(*c8_info, c8_b_emlrt_marshallOut(0U), "mFileTimeHi",
                  "mFileTimeHi", 4);
  sf_mex_assign(&c8_rhs4, sf_mex_createcellmatrix(0, 1), false);
  sf_mex_assign(&c8_lhs4, sf_mex_createcellmatrix(0, 1), false);
  sf_mex_addfield(*c8_info, sf_mex_duplicatearraysafe(&c8_rhs4), "rhs", "rhs", 4);
  sf_mex_addfield(*c8_info, sf_mex_duplicatearraysafe(&c8_lhs4), "lhs", "lhs", 4);
  sf_mex_addfield(*c8_info, c8_emlrt_marshallOut(
    "[ILXE]$matlabroot$/toolbox/shared/coder/coder/+coder/+internal/indexIntRelop.m!apply_float_relop"),
                  "context", "context", 5);
  sf_mex_addfield(*c8_info, c8_emlrt_marshallOut("eml_switch_helper"), "name",
                  "name", 5);
  sf_mex_addfield(*c8_info, c8_emlrt_marshallOut(""), "dominantType",
                  "dominantType", 5);
  sf_mex_addfield(*c8_info, c8_emlrt_marshallOut(
    "[ILXE]$matlabroot$/toolbox/eml/lib/matlab/eml/eml_switch_helper.m"),
                  "resolved", "resolved", 5);
  sf_mex_addfield(*c8_info, c8_b_emlrt_marshallOut(1393334458U), "fileTimeLo",
                  "fileTimeLo", 5);
  sf_mex_addfield(*c8_info, c8_b_emlrt_marshallOut(0U), "fileTimeHi",
                  "fileTimeHi", 5);
  sf_mex_addfield(*c8_info, c8_b_emlrt_marshallOut(0U), "mFileTimeLo",
                  "mFileTimeLo", 5);
  sf_mex_addfield(*c8_info, c8_b_emlrt_marshallOut(0U), "mFileTimeHi",
                  "mFileTimeHi", 5);
  sf_mex_assign(&c8_rhs5, sf_mex_createcellmatrix(0, 1), false);
  sf_mex_assign(&c8_lhs5, sf_mex_createcellmatrix(0, 1), false);
  sf_mex_addfield(*c8_info, sf_mex_duplicatearraysafe(&c8_rhs5), "rhs", "rhs", 5);
  sf_mex_addfield(*c8_info, sf_mex_duplicatearraysafe(&c8_lhs5), "lhs", "lhs", 5);
  sf_mex_addfield(*c8_info, c8_emlrt_marshallOut(
    "[ILXE]$matlabroot$/toolbox/shared/coder/coder/+coder/+internal/indexIntRelop.m!float_class_contains_indexIntClass"),
                  "context", "context", 6);
  sf_mex_addfield(*c8_info, c8_emlrt_marshallOut("eml_float_model"), "name",
                  "name", 6);
  sf_mex_addfield(*c8_info, c8_emlrt_marshallOut("char"), "dominantType",
                  "dominantType", 6);
  sf_mex_addfield(*c8_info, c8_emlrt_marshallOut(
    "[ILXE]$matlabroot$/toolbox/eml/lib/matlab/eml/eml_float_model.m"),
                  "resolved", "resolved", 6);
  sf_mex_addfield(*c8_info, c8_b_emlrt_marshallOut(1326731596U), "fileTimeLo",
                  "fileTimeLo", 6);
  sf_mex_addfield(*c8_info, c8_b_emlrt_marshallOut(0U), "fileTimeHi",
                  "fileTimeHi", 6);
  sf_mex_addfield(*c8_info, c8_b_emlrt_marshallOut(0U), "mFileTimeLo",
                  "mFileTimeLo", 6);
  sf_mex_addfield(*c8_info, c8_b_emlrt_marshallOut(0U), "mFileTimeHi",
                  "mFileTimeHi", 6);
  sf_mex_assign(&c8_rhs6, sf_mex_createcellmatrix(0, 1), false);
  sf_mex_assign(&c8_lhs6, sf_mex_createcellmatrix(0, 1), false);
  sf_mex_addfield(*c8_info, sf_mex_duplicatearraysafe(&c8_rhs6), "rhs", "rhs", 6);
  sf_mex_addfield(*c8_info, sf_mex_duplicatearraysafe(&c8_lhs6), "lhs", "lhs", 6);
  sf_mex_addfield(*c8_info, c8_emlrt_marshallOut(
    "[ILXE]$matlabroot$/toolbox/shared/coder/coder/+coder/+internal/indexIntRelop.m!is_signed_indexIntClass"),
                  "context", "context", 7);
  sf_mex_addfield(*c8_info, c8_emlrt_marshallOut("intmin"), "name", "name", 7);
  sf_mex_addfield(*c8_info, c8_emlrt_marshallOut("char"), "dominantType",
                  "dominantType", 7);
  sf_mex_addfield(*c8_info, c8_emlrt_marshallOut(
    "[ILXE]$matlabroot$/toolbox/eml/lib/matlab/elmat/intmin.m"), "resolved",
                  "resolved", 7);
  sf_mex_addfield(*c8_info, c8_b_emlrt_marshallOut(1362265482U), "fileTimeLo",
                  "fileTimeLo", 7);
  sf_mex_addfield(*c8_info, c8_b_emlrt_marshallOut(0U), "fileTimeHi",
                  "fileTimeHi", 7);
  sf_mex_addfield(*c8_info, c8_b_emlrt_marshallOut(0U), "mFileTimeLo",
                  "mFileTimeLo", 7);
  sf_mex_addfield(*c8_info, c8_b_emlrt_marshallOut(0U), "mFileTimeHi",
                  "mFileTimeHi", 7);
  sf_mex_assign(&c8_rhs7, sf_mex_createcellmatrix(0, 1), false);
  sf_mex_assign(&c8_lhs7, sf_mex_createcellmatrix(0, 1), false);
  sf_mex_addfield(*c8_info, sf_mex_duplicatearraysafe(&c8_rhs7), "rhs", "rhs", 7);
  sf_mex_addfield(*c8_info, sf_mex_duplicatearraysafe(&c8_lhs7), "lhs", "lhs", 7);
  sf_mex_addfield(*c8_info, c8_emlrt_marshallOut(
    "[ILXE]$matlabroot$/toolbox/eml/lib/matlab/elmat/intmin.m"), "context",
                  "context", 8);
  sf_mex_addfield(*c8_info, c8_emlrt_marshallOut("eml_switch_helper"), "name",
                  "name", 8);
  sf_mex_addfield(*c8_info, c8_emlrt_marshallOut(""), "dominantType",
                  "dominantType", 8);
  sf_mex_addfield(*c8_info, c8_emlrt_marshallOut(
    "[ILXE]$matlabroot$/toolbox/eml/lib/matlab/eml/eml_switch_helper.m"),
                  "resolved", "resolved", 8);
  sf_mex_addfield(*c8_info, c8_b_emlrt_marshallOut(1393334458U), "fileTimeLo",
                  "fileTimeLo", 8);
  sf_mex_addfield(*c8_info, c8_b_emlrt_marshallOut(0U), "fileTimeHi",
                  "fileTimeHi", 8);
  sf_mex_addfield(*c8_info, c8_b_emlrt_marshallOut(0U), "mFileTimeLo",
                  "mFileTimeLo", 8);
  sf_mex_addfield(*c8_info, c8_b_emlrt_marshallOut(0U), "mFileTimeHi",
                  "mFileTimeHi", 8);
  sf_mex_assign(&c8_rhs8, sf_mex_createcellmatrix(0, 1), false);
  sf_mex_assign(&c8_lhs8, sf_mex_createcellmatrix(0, 1), false);
  sf_mex_addfield(*c8_info, sf_mex_duplicatearraysafe(&c8_rhs8), "rhs", "rhs", 8);
  sf_mex_addfield(*c8_info, sf_mex_duplicatearraysafe(&c8_lhs8), "lhs", "lhs", 8);
  sf_mex_addfield(*c8_info, c8_emlrt_marshallOut(""), "context", "context", 9);
  sf_mex_addfield(*c8_info, c8_emlrt_marshallOut("intmax"), "name", "name", 9);
  sf_mex_addfield(*c8_info, c8_emlrt_marshallOut(""), "dominantType",
                  "dominantType", 9);
  sf_mex_addfield(*c8_info, c8_emlrt_marshallOut(
    "[ILXE]$matlabroot$/toolbox/eml/lib/matlab/elmat/intmax.m"), "resolved",
                  "resolved", 9);
  sf_mex_addfield(*c8_info, c8_b_emlrt_marshallOut(1362265482U), "fileTimeLo",
                  "fileTimeLo", 9);
  sf_mex_addfield(*c8_info, c8_b_emlrt_marshallOut(0U), "fileTimeHi",
                  "fileTimeHi", 9);
  sf_mex_addfield(*c8_info, c8_b_emlrt_marshallOut(0U), "mFileTimeLo",
                  "mFileTimeLo", 9);
  sf_mex_addfield(*c8_info, c8_b_emlrt_marshallOut(0U), "mFileTimeHi",
                  "mFileTimeHi", 9);
  sf_mex_assign(&c8_rhs9, sf_mex_createcellmatrix(0, 1), false);
  sf_mex_assign(&c8_lhs9, sf_mex_createcellmatrix(0, 1), false);
  sf_mex_addfield(*c8_info, sf_mex_duplicatearraysafe(&c8_rhs9), "rhs", "rhs", 9);
  sf_mex_addfield(*c8_info, sf_mex_duplicatearraysafe(&c8_lhs9), "lhs", "lhs", 9);
  sf_mex_addfield(*c8_info, c8_emlrt_marshallOut(""), "context", "context", 10);
  sf_mex_addfield(*c8_info, c8_emlrt_marshallOut("norm"), "name", "name", 10);
  sf_mex_addfield(*c8_info, c8_emlrt_marshallOut("single"), "dominantType",
                  "dominantType", 10);
  sf_mex_addfield(*c8_info, c8_emlrt_marshallOut(
    "[ILXE]$matlabroot$/toolbox/eml/lib/matlab/matfun/norm.m"), "resolved",
                  "resolved", 10);
  sf_mex_addfield(*c8_info, c8_b_emlrt_marshallOut(1363717468U), "fileTimeLo",
                  "fileTimeLo", 10);
  sf_mex_addfield(*c8_info, c8_b_emlrt_marshallOut(0U), "fileTimeHi",
                  "fileTimeHi", 10);
  sf_mex_addfield(*c8_info, c8_b_emlrt_marshallOut(0U), "mFileTimeLo",
                  "mFileTimeLo", 10);
  sf_mex_addfield(*c8_info, c8_b_emlrt_marshallOut(0U), "mFileTimeHi",
                  "mFileTimeHi", 10);
  sf_mex_assign(&c8_rhs10, sf_mex_createcellmatrix(0, 1), false);
  sf_mex_assign(&c8_lhs10, sf_mex_createcellmatrix(0, 1), false);
  sf_mex_addfield(*c8_info, sf_mex_duplicatearraysafe(&c8_rhs10), "rhs", "rhs",
                  10);
  sf_mex_addfield(*c8_info, sf_mex_duplicatearraysafe(&c8_lhs10), "lhs", "lhs",
                  10);
  sf_mex_addfield(*c8_info, c8_emlrt_marshallOut(
    "[ILXE]$matlabroot$/toolbox/eml/lib/matlab/matfun/norm.m!genpnorm"),
                  "context", "context", 11);
  sf_mex_addfield(*c8_info, c8_emlrt_marshallOut("eml_index_class"), "name",
                  "name", 11);
  sf_mex_addfield(*c8_info, c8_emlrt_marshallOut(""), "dominantType",
                  "dominantType", 11);
  sf_mex_addfield(*c8_info, c8_emlrt_marshallOut(
    "[ILXE]$matlabroot$/toolbox/eml/lib/matlab/eml/eml_index_class.m"),
                  "resolved", "resolved", 11);
  sf_mex_addfield(*c8_info, c8_b_emlrt_marshallOut(1323174178U), "fileTimeLo",
                  "fileTimeLo", 11);
  sf_mex_addfield(*c8_info, c8_b_emlrt_marshallOut(0U), "fileTimeHi",
                  "fileTimeHi", 11);
  sf_mex_addfield(*c8_info, c8_b_emlrt_marshallOut(0U), "mFileTimeLo",
                  "mFileTimeLo", 11);
  sf_mex_addfield(*c8_info, c8_b_emlrt_marshallOut(0U), "mFileTimeHi",
                  "mFileTimeHi", 11);
  sf_mex_assign(&c8_rhs11, sf_mex_createcellmatrix(0, 1), false);
  sf_mex_assign(&c8_lhs11, sf_mex_createcellmatrix(0, 1), false);
  sf_mex_addfield(*c8_info, sf_mex_duplicatearraysafe(&c8_rhs11), "rhs", "rhs",
                  11);
  sf_mex_addfield(*c8_info, sf_mex_duplicatearraysafe(&c8_lhs11), "lhs", "lhs",
                  11);
  sf_mex_addfield(*c8_info, c8_emlrt_marshallOut(
    "[ILXE]$matlabroot$/toolbox/eml/lib/matlab/matfun/norm.m!genpnorm"),
                  "context", "context", 12);
  sf_mex_addfield(*c8_info, c8_emlrt_marshallOut(
    "coder.internal.isBuiltInNumeric"), "name", "name", 12);
  sf_mex_addfield(*c8_info, c8_emlrt_marshallOut("double"), "dominantType",
                  "dominantType", 12);
  sf_mex_addfield(*c8_info, c8_emlrt_marshallOut(
    "[ILXE]$matlabroot$/toolbox/shared/coder/coder/+coder/+internal/isBuiltInNumeric.m"),
                  "resolved", "resolved", 12);
  sf_mex_addfield(*c8_info, c8_b_emlrt_marshallOut(1395935456U), "fileTimeLo",
                  "fileTimeLo", 12);
  sf_mex_addfield(*c8_info, c8_b_emlrt_marshallOut(0U), "fileTimeHi",
                  "fileTimeHi", 12);
  sf_mex_addfield(*c8_info, c8_b_emlrt_marshallOut(0U), "mFileTimeLo",
                  "mFileTimeLo", 12);
  sf_mex_addfield(*c8_info, c8_b_emlrt_marshallOut(0U), "mFileTimeHi",
                  "mFileTimeHi", 12);
  sf_mex_assign(&c8_rhs12, sf_mex_createcellmatrix(0, 1), false);
  sf_mex_assign(&c8_lhs12, sf_mex_createcellmatrix(0, 1), false);
  sf_mex_addfield(*c8_info, sf_mex_duplicatearraysafe(&c8_rhs12), "rhs", "rhs",
                  12);
  sf_mex_addfield(*c8_info, sf_mex_duplicatearraysafe(&c8_lhs12), "lhs", "lhs",
                  12);
  sf_mex_addfield(*c8_info, c8_emlrt_marshallOut(
    "[ILXE]$matlabroot$/toolbox/eml/lib/matlab/matfun/norm.m!genpnorm"),
                  "context", "context", 13);
  sf_mex_addfield(*c8_info, c8_emlrt_marshallOut("eml_xnrm2"), "name", "name",
                  13);
  sf_mex_addfield(*c8_info, c8_emlrt_marshallOut("single"), "dominantType",
                  "dominantType", 13);
  sf_mex_addfield(*c8_info, c8_emlrt_marshallOut(
    "[ILXE]$matlabroot$/toolbox/eml/lib/matlab/eml/blas/eml_xnrm2.m"),
                  "resolved", "resolved", 13);
  sf_mex_addfield(*c8_info, c8_b_emlrt_marshallOut(1375984292U), "fileTimeLo",
                  "fileTimeLo", 13);
  sf_mex_addfield(*c8_info, c8_b_emlrt_marshallOut(0U), "fileTimeHi",
                  "fileTimeHi", 13);
  sf_mex_addfield(*c8_info, c8_b_emlrt_marshallOut(0U), "mFileTimeLo",
                  "mFileTimeLo", 13);
  sf_mex_addfield(*c8_info, c8_b_emlrt_marshallOut(0U), "mFileTimeHi",
                  "mFileTimeHi", 13);
  sf_mex_assign(&c8_rhs13, sf_mex_createcellmatrix(0, 1), false);
  sf_mex_assign(&c8_lhs13, sf_mex_createcellmatrix(0, 1), false);
  sf_mex_addfield(*c8_info, sf_mex_duplicatearraysafe(&c8_rhs13), "rhs", "rhs",
                  13);
  sf_mex_addfield(*c8_info, sf_mex_duplicatearraysafe(&c8_lhs13), "lhs", "lhs",
                  13);
  sf_mex_addfield(*c8_info, c8_emlrt_marshallOut(
    "[ILXE]$matlabroot$/toolbox/eml/lib/matlab/eml/blas/eml_xnrm2.m"), "context",
                  "context", 14);
  sf_mex_addfield(*c8_info, c8_emlrt_marshallOut("coder.internal.blas.inline"),
                  "name", "name", 14);
  sf_mex_addfield(*c8_info, c8_emlrt_marshallOut(""), "dominantType",
                  "dominantType", 14);
  sf_mex_addfield(*c8_info, c8_emlrt_marshallOut(
    "[ILXE]$matlabroot$/toolbox/coder/coder/+coder/+internal/+blas/inline.p"),
                  "resolved", "resolved", 14);
  sf_mex_addfield(*c8_info, c8_b_emlrt_marshallOut(1410811372U), "fileTimeLo",
                  "fileTimeLo", 14);
  sf_mex_addfield(*c8_info, c8_b_emlrt_marshallOut(0U), "fileTimeHi",
                  "fileTimeHi", 14);
  sf_mex_addfield(*c8_info, c8_b_emlrt_marshallOut(0U), "mFileTimeLo",
                  "mFileTimeLo", 14);
  sf_mex_addfield(*c8_info, c8_b_emlrt_marshallOut(0U), "mFileTimeHi",
                  "mFileTimeHi", 14);
  sf_mex_assign(&c8_rhs14, sf_mex_createcellmatrix(0, 1), false);
  sf_mex_assign(&c8_lhs14, sf_mex_createcellmatrix(0, 1), false);
  sf_mex_addfield(*c8_info, sf_mex_duplicatearraysafe(&c8_rhs14), "rhs", "rhs",
                  14);
  sf_mex_addfield(*c8_info, sf_mex_duplicatearraysafe(&c8_lhs14), "lhs", "lhs",
                  14);
  sf_mex_addfield(*c8_info, c8_emlrt_marshallOut(
    "[ILXE]$matlabroot$/toolbox/eml/lib/matlab/eml/blas/eml_xnrm2.m"), "context",
                  "context", 15);
  sf_mex_addfield(*c8_info, c8_emlrt_marshallOut("coder.internal.blas.xnrm2"),
                  "name", "name", 15);
  sf_mex_addfield(*c8_info, c8_emlrt_marshallOut("single"), "dominantType",
                  "dominantType", 15);
  sf_mex_addfield(*c8_info, c8_emlrt_marshallOut(
    "[ILXE]$matlabroot$/toolbox/coder/coder/+coder/+internal/+blas/xnrm2.p"),
                  "resolved", "resolved", 15);
  sf_mex_addfield(*c8_info, c8_b_emlrt_marshallOut(1410811370U), "fileTimeLo",
                  "fileTimeLo", 15);
  sf_mex_addfield(*c8_info, c8_b_emlrt_marshallOut(0U), "fileTimeHi",
                  "fileTimeHi", 15);
  sf_mex_addfield(*c8_info, c8_b_emlrt_marshallOut(0U), "mFileTimeLo",
                  "mFileTimeLo", 15);
  sf_mex_addfield(*c8_info, c8_b_emlrt_marshallOut(0U), "mFileTimeHi",
                  "mFileTimeHi", 15);
  sf_mex_assign(&c8_rhs15, sf_mex_createcellmatrix(0, 1), false);
  sf_mex_assign(&c8_lhs15, sf_mex_createcellmatrix(0, 1), false);
  sf_mex_addfield(*c8_info, sf_mex_duplicatearraysafe(&c8_rhs15), "rhs", "rhs",
                  15);
  sf_mex_addfield(*c8_info, sf_mex_duplicatearraysafe(&c8_lhs15), "lhs", "lhs",
                  15);
  sf_mex_addfield(*c8_info, c8_emlrt_marshallOut(
    "[ILXE]$matlabroot$/toolbox/coder/coder/+coder/+internal/+blas/xnrm2.p"),
                  "context", "context", 16);
  sf_mex_addfield(*c8_info, c8_emlrt_marshallOut(
    "coder.internal.blas.use_refblas"), "name", "name", 16);
  sf_mex_addfield(*c8_info, c8_emlrt_marshallOut(""), "dominantType",
                  "dominantType", 16);
  sf_mex_addfield(*c8_info, c8_emlrt_marshallOut(
    "[ILXE]$matlabroot$/toolbox/coder/coder/+coder/+internal/+blas/use_refblas.p"),
                  "resolved", "resolved", 16);
  sf_mex_addfield(*c8_info, c8_b_emlrt_marshallOut(1410811370U), "fileTimeLo",
                  "fileTimeLo", 16);
  sf_mex_addfield(*c8_info, c8_b_emlrt_marshallOut(0U), "fileTimeHi",
                  "fileTimeHi", 16);
  sf_mex_addfield(*c8_info, c8_b_emlrt_marshallOut(0U), "mFileTimeLo",
                  "mFileTimeLo", 16);
  sf_mex_addfield(*c8_info, c8_b_emlrt_marshallOut(0U), "mFileTimeHi",
                  "mFileTimeHi", 16);
  sf_mex_assign(&c8_rhs16, sf_mex_createcellmatrix(0, 1), false);
  sf_mex_assign(&c8_lhs16, sf_mex_createcellmatrix(0, 1), false);
  sf_mex_addfield(*c8_info, sf_mex_duplicatearraysafe(&c8_rhs16), "rhs", "rhs",
                  16);
  sf_mex_addfield(*c8_info, sf_mex_duplicatearraysafe(&c8_lhs16), "lhs", "lhs",
                  16);
  sf_mex_addfield(*c8_info, c8_emlrt_marshallOut(
    "[ILXE]$matlabroot$/toolbox/coder/coder/+coder/+internal/+blas/xnrm2.p!below_threshold"),
                  "context", "context", 17);
  sf_mex_addfield(*c8_info, c8_emlrt_marshallOut("coder.internal.blas.threshold"),
                  "name", "name", 17);
  sf_mex_addfield(*c8_info, c8_emlrt_marshallOut("char"), "dominantType",
                  "dominantType", 17);
  sf_mex_addfield(*c8_info, c8_emlrt_marshallOut(
    "[ILXE]$matlabroot$/toolbox/coder/coder/+coder/+internal/+blas/threshold.p"),
                  "resolved", "resolved", 17);
  sf_mex_addfield(*c8_info, c8_b_emlrt_marshallOut(1410811372U), "fileTimeLo",
                  "fileTimeLo", 17);
  sf_mex_addfield(*c8_info, c8_b_emlrt_marshallOut(0U), "fileTimeHi",
                  "fileTimeHi", 17);
  sf_mex_addfield(*c8_info, c8_b_emlrt_marshallOut(0U), "mFileTimeLo",
                  "mFileTimeLo", 17);
  sf_mex_addfield(*c8_info, c8_b_emlrt_marshallOut(0U), "mFileTimeHi",
                  "mFileTimeHi", 17);
  sf_mex_assign(&c8_rhs17, sf_mex_createcellmatrix(0, 1), false);
  sf_mex_assign(&c8_lhs17, sf_mex_createcellmatrix(0, 1), false);
  sf_mex_addfield(*c8_info, sf_mex_duplicatearraysafe(&c8_rhs17), "rhs", "rhs",
                  17);
  sf_mex_addfield(*c8_info, sf_mex_duplicatearraysafe(&c8_lhs17), "lhs", "lhs",
                  17);
  sf_mex_addfield(*c8_info, c8_emlrt_marshallOut(
    "[ILXE]$matlabroot$/toolbox/coder/coder/+coder/+internal/+blas/threshold.p"),
                  "context", "context", 18);
  sf_mex_addfield(*c8_info, c8_emlrt_marshallOut("eml_switch_helper"), "name",
                  "name", 18);
  sf_mex_addfield(*c8_info, c8_emlrt_marshallOut(""), "dominantType",
                  "dominantType", 18);
  sf_mex_addfield(*c8_info, c8_emlrt_marshallOut(
    "[ILXE]$matlabroot$/toolbox/eml/lib/matlab/eml/eml_switch_helper.m"),
                  "resolved", "resolved", 18);
  sf_mex_addfield(*c8_info, c8_b_emlrt_marshallOut(1393334458U), "fileTimeLo",
                  "fileTimeLo", 18);
  sf_mex_addfield(*c8_info, c8_b_emlrt_marshallOut(0U), "fileTimeHi",
                  "fileTimeHi", 18);
  sf_mex_addfield(*c8_info, c8_b_emlrt_marshallOut(0U), "mFileTimeLo",
                  "mFileTimeLo", 18);
  sf_mex_addfield(*c8_info, c8_b_emlrt_marshallOut(0U), "mFileTimeHi",
                  "mFileTimeHi", 18);
  sf_mex_assign(&c8_rhs18, sf_mex_createcellmatrix(0, 1), false);
  sf_mex_assign(&c8_lhs18, sf_mex_createcellmatrix(0, 1), false);
  sf_mex_addfield(*c8_info, sf_mex_duplicatearraysafe(&c8_rhs18), "rhs", "rhs",
                  18);
  sf_mex_addfield(*c8_info, sf_mex_duplicatearraysafe(&c8_lhs18), "lhs", "lhs",
                  18);
  sf_mex_addfield(*c8_info, c8_emlrt_marshallOut(
    "[ILXE]$matlabroot$/toolbox/coder/coder/+coder/+internal/+blas/xnrm2.p"),
                  "context", "context", 19);
  sf_mex_addfield(*c8_info, c8_emlrt_marshallOut("coder.internal.refblas.xnrm2"),
                  "name", "name", 19);
  sf_mex_addfield(*c8_info, c8_emlrt_marshallOut("single"), "dominantType",
                  "dominantType", 19);
  sf_mex_addfield(*c8_info, c8_emlrt_marshallOut(
    "[ILXE]$matlabroot$/toolbox/coder/coder/+coder/+internal/+refblas/xnrm2.p"),
                  "resolved", "resolved", 19);
  sf_mex_addfield(*c8_info, c8_b_emlrt_marshallOut(1410811372U), "fileTimeLo",
                  "fileTimeLo", 19);
  sf_mex_addfield(*c8_info, c8_b_emlrt_marshallOut(0U), "fileTimeHi",
                  "fileTimeHi", 19);
  sf_mex_addfield(*c8_info, c8_b_emlrt_marshallOut(0U), "mFileTimeLo",
                  "mFileTimeLo", 19);
  sf_mex_addfield(*c8_info, c8_b_emlrt_marshallOut(0U), "mFileTimeHi",
                  "mFileTimeHi", 19);
  sf_mex_assign(&c8_rhs19, sf_mex_createcellmatrix(0, 1), false);
  sf_mex_assign(&c8_lhs19, sf_mex_createcellmatrix(0, 1), false);
  sf_mex_addfield(*c8_info, sf_mex_duplicatearraysafe(&c8_rhs19), "rhs", "rhs",
                  19);
  sf_mex_addfield(*c8_info, sf_mex_duplicatearraysafe(&c8_lhs19), "lhs", "lhs",
                  19);
  sf_mex_addfield(*c8_info, c8_emlrt_marshallOut(
    "[ILXE]$matlabroot$/toolbox/coder/coder/+coder/+internal/+refblas/xnrm2.p"),
                  "context", "context", 20);
  sf_mex_addfield(*c8_info, c8_emlrt_marshallOut("realmin"), "name", "name", 20);
  sf_mex_addfield(*c8_info, c8_emlrt_marshallOut("char"), "dominantType",
                  "dominantType", 20);
  sf_mex_addfield(*c8_info, c8_emlrt_marshallOut(
    "[ILXE]$matlabroot$/toolbox/eml/lib/matlab/elmat/realmin.m"), "resolved",
                  "resolved", 20);
  sf_mex_addfield(*c8_info, c8_b_emlrt_marshallOut(1307654842U), "fileTimeLo",
                  "fileTimeLo", 20);
  sf_mex_addfield(*c8_info, c8_b_emlrt_marshallOut(0U), "fileTimeHi",
                  "fileTimeHi", 20);
  sf_mex_addfield(*c8_info, c8_b_emlrt_marshallOut(0U), "mFileTimeLo",
                  "mFileTimeLo", 20);
  sf_mex_addfield(*c8_info, c8_b_emlrt_marshallOut(0U), "mFileTimeHi",
                  "mFileTimeHi", 20);
  sf_mex_assign(&c8_rhs20, sf_mex_createcellmatrix(0, 1), false);
  sf_mex_assign(&c8_lhs20, sf_mex_createcellmatrix(0, 1), false);
  sf_mex_addfield(*c8_info, sf_mex_duplicatearraysafe(&c8_rhs20), "rhs", "rhs",
                  20);
  sf_mex_addfield(*c8_info, sf_mex_duplicatearraysafe(&c8_lhs20), "lhs", "lhs",
                  20);
  sf_mex_addfield(*c8_info, c8_emlrt_marshallOut(
    "[ILXE]$matlabroot$/toolbox/eml/lib/matlab/elmat/realmin.m"), "context",
                  "context", 21);
  sf_mex_addfield(*c8_info, c8_emlrt_marshallOut("eml_realmin"), "name", "name",
                  21);
  sf_mex_addfield(*c8_info, c8_emlrt_marshallOut("char"), "dominantType",
                  "dominantType", 21);
  sf_mex_addfield(*c8_info, c8_emlrt_marshallOut(
    "[ILXE]$matlabroot$/toolbox/eml/lib/matlab/eml/eml_realmin.m"), "resolved",
                  "resolved", 21);
  sf_mex_addfield(*c8_info, c8_b_emlrt_marshallOut(1307654844U), "fileTimeLo",
                  "fileTimeLo", 21);
  sf_mex_addfield(*c8_info, c8_b_emlrt_marshallOut(0U), "fileTimeHi",
                  "fileTimeHi", 21);
  sf_mex_addfield(*c8_info, c8_b_emlrt_marshallOut(0U), "mFileTimeLo",
                  "mFileTimeLo", 21);
  sf_mex_addfield(*c8_info, c8_b_emlrt_marshallOut(0U), "mFileTimeHi",
                  "mFileTimeHi", 21);
  sf_mex_assign(&c8_rhs21, sf_mex_createcellmatrix(0, 1), false);
  sf_mex_assign(&c8_lhs21, sf_mex_createcellmatrix(0, 1), false);
  sf_mex_addfield(*c8_info, sf_mex_duplicatearraysafe(&c8_rhs21), "rhs", "rhs",
                  21);
  sf_mex_addfield(*c8_info, sf_mex_duplicatearraysafe(&c8_lhs21), "lhs", "lhs",
                  21);
  sf_mex_addfield(*c8_info, c8_emlrt_marshallOut(
    "[ILXE]$matlabroot$/toolbox/eml/lib/matlab/eml/eml_realmin.m"), "context",
                  "context", 22);
  sf_mex_addfield(*c8_info, c8_emlrt_marshallOut("eml_float_model"), "name",
                  "name", 22);
  sf_mex_addfield(*c8_info, c8_emlrt_marshallOut("char"), "dominantType",
                  "dominantType", 22);
  sf_mex_addfield(*c8_info, c8_emlrt_marshallOut(
    "[ILXE]$matlabroot$/toolbox/eml/lib/matlab/eml/eml_float_model.m"),
                  "resolved", "resolved", 22);
  sf_mex_addfield(*c8_info, c8_b_emlrt_marshallOut(1326731596U), "fileTimeLo",
                  "fileTimeLo", 22);
  sf_mex_addfield(*c8_info, c8_b_emlrt_marshallOut(0U), "fileTimeHi",
                  "fileTimeHi", 22);
  sf_mex_addfield(*c8_info, c8_b_emlrt_marshallOut(0U), "mFileTimeLo",
                  "mFileTimeLo", 22);
  sf_mex_addfield(*c8_info, c8_b_emlrt_marshallOut(0U), "mFileTimeHi",
                  "mFileTimeHi", 22);
  sf_mex_assign(&c8_rhs22, sf_mex_createcellmatrix(0, 1), false);
  sf_mex_assign(&c8_lhs22, sf_mex_createcellmatrix(0, 1), false);
  sf_mex_addfield(*c8_info, sf_mex_duplicatearraysafe(&c8_rhs22), "rhs", "rhs",
                  22);
  sf_mex_addfield(*c8_info, sf_mex_duplicatearraysafe(&c8_lhs22), "lhs", "lhs",
                  22);
  sf_mex_addfield(*c8_info, c8_emlrt_marshallOut(
    "[ILXE]$matlabroot$/toolbox/coder/coder/+coder/+internal/+refblas/xnrm2.p"),
                  "context", "context", 23);
  sf_mex_addfield(*c8_info, c8_emlrt_marshallOut("coder.internal.indexMinus"),
                  "name", "name", 23);
  sf_mex_addfield(*c8_info, c8_emlrt_marshallOut("double"), "dominantType",
                  "dominantType", 23);
  sf_mex_addfield(*c8_info, c8_emlrt_marshallOut(
    "[ILXE]$matlabroot$/toolbox/shared/coder/coder/+coder/+internal/indexMinus.m"),
                  "resolved", "resolved", 23);
  sf_mex_addfield(*c8_info, c8_b_emlrt_marshallOut(1372586760U), "fileTimeLo",
                  "fileTimeLo", 23);
  sf_mex_addfield(*c8_info, c8_b_emlrt_marshallOut(0U), "fileTimeHi",
                  "fileTimeHi", 23);
  sf_mex_addfield(*c8_info, c8_b_emlrt_marshallOut(0U), "mFileTimeLo",
                  "mFileTimeLo", 23);
  sf_mex_addfield(*c8_info, c8_b_emlrt_marshallOut(0U), "mFileTimeHi",
                  "mFileTimeHi", 23);
  sf_mex_assign(&c8_rhs23, sf_mex_createcellmatrix(0, 1), false);
  sf_mex_assign(&c8_lhs23, sf_mex_createcellmatrix(0, 1), false);
  sf_mex_addfield(*c8_info, sf_mex_duplicatearraysafe(&c8_rhs23), "rhs", "rhs",
                  23);
  sf_mex_addfield(*c8_info, sf_mex_duplicatearraysafe(&c8_lhs23), "lhs", "lhs",
                  23);
  sf_mex_addfield(*c8_info, c8_emlrt_marshallOut(
    "[ILXE]$matlabroot$/toolbox/coder/coder/+coder/+internal/+refblas/xnrm2.p"),
                  "context", "context", 24);
  sf_mex_addfield(*c8_info, c8_emlrt_marshallOut("coder.internal.indexTimes"),
                  "name", "name", 24);
  sf_mex_addfield(*c8_info, c8_emlrt_marshallOut("coder.internal.indexInt"),
                  "dominantType", "dominantType", 24);
  sf_mex_addfield(*c8_info, c8_emlrt_marshallOut(
    "[ILXE]$matlabroot$/toolbox/shared/coder/coder/+coder/+internal/indexTimes.m"),
                  "resolved", "resolved", 24);
  sf_mex_addfield(*c8_info, c8_b_emlrt_marshallOut(1372586760U), "fileTimeLo",
                  "fileTimeLo", 24);
  sf_mex_addfield(*c8_info, c8_b_emlrt_marshallOut(0U), "fileTimeHi",
                  "fileTimeHi", 24);
  sf_mex_addfield(*c8_info, c8_b_emlrt_marshallOut(0U), "mFileTimeLo",
                  "mFileTimeLo", 24);
  sf_mex_addfield(*c8_info, c8_b_emlrt_marshallOut(0U), "mFileTimeHi",
                  "mFileTimeHi", 24);
  sf_mex_assign(&c8_rhs24, sf_mex_createcellmatrix(0, 1), false);
  sf_mex_assign(&c8_lhs24, sf_mex_createcellmatrix(0, 1), false);
  sf_mex_addfield(*c8_info, sf_mex_duplicatearraysafe(&c8_rhs24), "rhs", "rhs",
                  24);
  sf_mex_addfield(*c8_info, sf_mex_duplicatearraysafe(&c8_lhs24), "lhs", "lhs",
                  24);
  sf_mex_addfield(*c8_info, c8_emlrt_marshallOut(
    "[ILXE]$matlabroot$/toolbox/coder/coder/+coder/+internal/+refblas/xnrm2.p"),
                  "context", "context", 25);
  sf_mex_addfield(*c8_info, c8_emlrt_marshallOut("coder.internal.indexPlus"),
                  "name", "name", 25);
  sf_mex_addfield(*c8_info, c8_emlrt_marshallOut("coder.internal.indexInt"),
                  "dominantType", "dominantType", 25);
  sf_mex_addfield(*c8_info, c8_emlrt_marshallOut(
    "[ILXE]$matlabroot$/toolbox/shared/coder/coder/+coder/+internal/indexPlus.m"),
                  "resolved", "resolved", 25);
  sf_mex_addfield(*c8_info, c8_b_emlrt_marshallOut(1372586760U), "fileTimeLo",
                  "fileTimeLo", 25);
  sf_mex_addfield(*c8_info, c8_b_emlrt_marshallOut(0U), "fileTimeHi",
                  "fileTimeHi", 25);
  sf_mex_addfield(*c8_info, c8_b_emlrt_marshallOut(0U), "mFileTimeLo",
                  "mFileTimeLo", 25);
  sf_mex_addfield(*c8_info, c8_b_emlrt_marshallOut(0U), "mFileTimeHi",
                  "mFileTimeHi", 25);
  sf_mex_assign(&c8_rhs25, sf_mex_createcellmatrix(0, 1), false);
  sf_mex_assign(&c8_lhs25, sf_mex_createcellmatrix(0, 1), false);
  sf_mex_addfield(*c8_info, sf_mex_duplicatearraysafe(&c8_rhs25), "rhs", "rhs",
                  25);
  sf_mex_addfield(*c8_info, sf_mex_duplicatearraysafe(&c8_lhs25), "lhs", "lhs",
                  25);
  sf_mex_addfield(*c8_info, c8_emlrt_marshallOut(
    "[ILXE]$matlabroot$/toolbox/coder/coder/+coder/+internal/+refblas/xnrm2.p"),
                  "context", "context", 26);
  sf_mex_addfield(*c8_info, c8_emlrt_marshallOut(
    "eml_int_forloop_overflow_check"), "name", "name", 26);
  sf_mex_addfield(*c8_info, c8_emlrt_marshallOut(""), "dominantType",
                  "dominantType", 26);
  sf_mex_addfield(*c8_info, c8_emlrt_marshallOut(
    "[ILXE]$matlabroot$/toolbox/eml/lib/matlab/eml/eml_int_forloop_overflow_check.m"),
                  "resolved", "resolved", 26);
  sf_mex_addfield(*c8_info, c8_b_emlrt_marshallOut(1397261022U), "fileTimeLo",
                  "fileTimeLo", 26);
  sf_mex_addfield(*c8_info, c8_b_emlrt_marshallOut(0U), "fileTimeHi",
                  "fileTimeHi", 26);
  sf_mex_addfield(*c8_info, c8_b_emlrt_marshallOut(0U), "mFileTimeLo",
                  "mFileTimeLo", 26);
  sf_mex_addfield(*c8_info, c8_b_emlrt_marshallOut(0U), "mFileTimeHi",
                  "mFileTimeHi", 26);
  sf_mex_assign(&c8_rhs26, sf_mex_createcellmatrix(0, 1), false);
  sf_mex_assign(&c8_lhs26, sf_mex_createcellmatrix(0, 1), false);
  sf_mex_addfield(*c8_info, sf_mex_duplicatearraysafe(&c8_rhs26), "rhs", "rhs",
                  26);
  sf_mex_addfield(*c8_info, sf_mex_duplicatearraysafe(&c8_lhs26), "lhs", "lhs",
                  26);
  sf_mex_addfield(*c8_info, c8_emlrt_marshallOut(
    "[ILXE]$matlabroot$/toolbox/eml/lib/matlab/eml/eml_int_forloop_overflow_check.m!eml_int_forloop_overflow_check_helper"),
                  "context", "context", 27);
  sf_mex_addfield(*c8_info, c8_emlrt_marshallOut("isfi"), "name", "name", 27);
  sf_mex_addfield(*c8_info, c8_emlrt_marshallOut("coder.internal.indexInt"),
                  "dominantType", "dominantType", 27);
  sf_mex_addfield(*c8_info, c8_emlrt_marshallOut(
    "[ILXE]$matlabroot$/toolbox/eml/lib/fixedpoint/isfi.m"), "resolved",
                  "resolved", 27);
  sf_mex_addfield(*c8_info, c8_b_emlrt_marshallOut(1346513958U), "fileTimeLo",
                  "fileTimeLo", 27);
  sf_mex_addfield(*c8_info, c8_b_emlrt_marshallOut(0U), "fileTimeHi",
                  "fileTimeHi", 27);
  sf_mex_addfield(*c8_info, c8_b_emlrt_marshallOut(0U), "mFileTimeLo",
                  "mFileTimeLo", 27);
  sf_mex_addfield(*c8_info, c8_b_emlrt_marshallOut(0U), "mFileTimeHi",
                  "mFileTimeHi", 27);
  sf_mex_assign(&c8_rhs27, sf_mex_createcellmatrix(0, 1), false);
  sf_mex_assign(&c8_lhs27, sf_mex_createcellmatrix(0, 1), false);
  sf_mex_addfield(*c8_info, sf_mex_duplicatearraysafe(&c8_rhs27), "rhs", "rhs",
                  27);
  sf_mex_addfield(*c8_info, sf_mex_duplicatearraysafe(&c8_lhs27), "lhs", "lhs",
                  27);
  sf_mex_addfield(*c8_info, c8_emlrt_marshallOut(
    "[ILXE]$matlabroot$/toolbox/eml/lib/fixedpoint/isfi.m"), "context",
                  "context", 28);
  sf_mex_addfield(*c8_info, c8_emlrt_marshallOut("isnumerictype"), "name",
                  "name", 28);
  sf_mex_addfield(*c8_info, c8_emlrt_marshallOut("char"), "dominantType",
                  "dominantType", 28);
  sf_mex_addfield(*c8_info, c8_emlrt_marshallOut(
    "[ILXE]$matlabroot$/toolbox/eml/lib/fixedpoint/isnumerictype.m"), "resolved",
                  "resolved", 28);
  sf_mex_addfield(*c8_info, c8_b_emlrt_marshallOut(1398879198U), "fileTimeLo",
                  "fileTimeLo", 28);
  sf_mex_addfield(*c8_info, c8_b_emlrt_marshallOut(0U), "fileTimeHi",
                  "fileTimeHi", 28);
  sf_mex_addfield(*c8_info, c8_b_emlrt_marshallOut(0U), "mFileTimeLo",
                  "mFileTimeLo", 28);
  sf_mex_addfield(*c8_info, c8_b_emlrt_marshallOut(0U), "mFileTimeHi",
                  "mFileTimeHi", 28);
  sf_mex_assign(&c8_rhs28, sf_mex_createcellmatrix(0, 1), false);
  sf_mex_assign(&c8_lhs28, sf_mex_createcellmatrix(0, 1), false);
  sf_mex_addfield(*c8_info, sf_mex_duplicatearraysafe(&c8_rhs28), "rhs", "rhs",
                  28);
  sf_mex_addfield(*c8_info, sf_mex_duplicatearraysafe(&c8_lhs28), "lhs", "lhs",
                  28);
  sf_mex_addfield(*c8_info, c8_emlrt_marshallOut(
    "[ILXE]$matlabroot$/toolbox/eml/lib/matlab/eml/eml_int_forloop_overflow_check.m!eml_int_forloop_overflow_check_helper"),
                  "context", "context", 29);
  sf_mex_addfield(*c8_info, c8_emlrt_marshallOut("intmax"), "name", "name", 29);
  sf_mex_addfield(*c8_info, c8_emlrt_marshallOut("char"), "dominantType",
                  "dominantType", 29);
  sf_mex_addfield(*c8_info, c8_emlrt_marshallOut(
    "[ILXE]$matlabroot$/toolbox/eml/lib/matlab/elmat/intmax.m"), "resolved",
                  "resolved", 29);
  sf_mex_addfield(*c8_info, c8_b_emlrt_marshallOut(1362265482U), "fileTimeLo",
                  "fileTimeLo", 29);
  sf_mex_addfield(*c8_info, c8_b_emlrt_marshallOut(0U), "fileTimeHi",
                  "fileTimeHi", 29);
  sf_mex_addfield(*c8_info, c8_b_emlrt_marshallOut(0U), "mFileTimeLo",
                  "mFileTimeLo", 29);
  sf_mex_addfield(*c8_info, c8_b_emlrt_marshallOut(0U), "mFileTimeHi",
                  "mFileTimeHi", 29);
  sf_mex_assign(&c8_rhs29, sf_mex_createcellmatrix(0, 1), false);
  sf_mex_assign(&c8_lhs29, sf_mex_createcellmatrix(0, 1), false);
  sf_mex_addfield(*c8_info, sf_mex_duplicatearraysafe(&c8_rhs29), "rhs", "rhs",
                  29);
  sf_mex_addfield(*c8_info, sf_mex_duplicatearraysafe(&c8_lhs29), "lhs", "lhs",
                  29);
  sf_mex_addfield(*c8_info, c8_emlrt_marshallOut(
    "[ILXE]$matlabroot$/toolbox/eml/lib/matlab/elmat/intmax.m"), "context",
                  "context", 30);
  sf_mex_addfield(*c8_info, c8_emlrt_marshallOut("eml_switch_helper"), "name",
                  "name", 30);
  sf_mex_addfield(*c8_info, c8_emlrt_marshallOut(""), "dominantType",
                  "dominantType", 30);
  sf_mex_addfield(*c8_info, c8_emlrt_marshallOut(
    "[ILXE]$matlabroot$/toolbox/eml/lib/matlab/eml/eml_switch_helper.m"),
                  "resolved", "resolved", 30);
  sf_mex_addfield(*c8_info, c8_b_emlrt_marshallOut(1393334458U), "fileTimeLo",
                  "fileTimeLo", 30);
  sf_mex_addfield(*c8_info, c8_b_emlrt_marshallOut(0U), "fileTimeHi",
                  "fileTimeHi", 30);
  sf_mex_addfield(*c8_info, c8_b_emlrt_marshallOut(0U), "mFileTimeLo",
                  "mFileTimeLo", 30);
  sf_mex_addfield(*c8_info, c8_b_emlrt_marshallOut(0U), "mFileTimeHi",
                  "mFileTimeHi", 30);
  sf_mex_assign(&c8_rhs30, sf_mex_createcellmatrix(0, 1), false);
  sf_mex_assign(&c8_lhs30, sf_mex_createcellmatrix(0, 1), false);
  sf_mex_addfield(*c8_info, sf_mex_duplicatearraysafe(&c8_rhs30), "rhs", "rhs",
                  30);
  sf_mex_addfield(*c8_info, sf_mex_duplicatearraysafe(&c8_lhs30), "lhs", "lhs",
                  30);
  sf_mex_addfield(*c8_info, c8_emlrt_marshallOut(
    "[ILXE]$matlabroot$/toolbox/eml/lib/matlab/eml/eml_int_forloop_overflow_check.m!eml_int_forloop_overflow_check_helper"),
                  "context", "context", 31);
  sf_mex_addfield(*c8_info, c8_emlrt_marshallOut("intmin"), "name", "name", 31);
  sf_mex_addfield(*c8_info, c8_emlrt_marshallOut("char"), "dominantType",
                  "dominantType", 31);
  sf_mex_addfield(*c8_info, c8_emlrt_marshallOut(
    "[ILXE]$matlabroot$/toolbox/eml/lib/matlab/elmat/intmin.m"), "resolved",
                  "resolved", 31);
  sf_mex_addfield(*c8_info, c8_b_emlrt_marshallOut(1362265482U), "fileTimeLo",
                  "fileTimeLo", 31);
  sf_mex_addfield(*c8_info, c8_b_emlrt_marshallOut(0U), "fileTimeHi",
                  "fileTimeHi", 31);
  sf_mex_addfield(*c8_info, c8_b_emlrt_marshallOut(0U), "mFileTimeLo",
                  "mFileTimeLo", 31);
  sf_mex_addfield(*c8_info, c8_b_emlrt_marshallOut(0U), "mFileTimeHi",
                  "mFileTimeHi", 31);
  sf_mex_assign(&c8_rhs31, sf_mex_createcellmatrix(0, 1), false);
  sf_mex_assign(&c8_lhs31, sf_mex_createcellmatrix(0, 1), false);
  sf_mex_addfield(*c8_info, sf_mex_duplicatearraysafe(&c8_rhs31), "rhs", "rhs",
                  31);
  sf_mex_addfield(*c8_info, sf_mex_duplicatearraysafe(&c8_lhs31), "lhs", "lhs",
                  31);
  sf_mex_addfield(*c8_info, c8_emlrt_marshallOut(
    "[ILXE]$matlabroot$/toolbox/coder/coder/+coder/+internal/+refblas/xnrm2.p"),
                  "context", "context", 32);
  sf_mex_addfield(*c8_info, c8_emlrt_marshallOut("abs"), "name", "name", 32);
  sf_mex_addfield(*c8_info, c8_emlrt_marshallOut("single"), "dominantType",
                  "dominantType", 32);
  sf_mex_addfield(*c8_info, c8_emlrt_marshallOut(
    "[ILXE]$matlabroot$/toolbox/eml/lib/matlab/elfun/abs.m"), "resolved",
                  "resolved", 32);
  sf_mex_addfield(*c8_info, c8_b_emlrt_marshallOut(1363717452U), "fileTimeLo",
                  "fileTimeLo", 32);
  sf_mex_addfield(*c8_info, c8_b_emlrt_marshallOut(0U), "fileTimeHi",
                  "fileTimeHi", 32);
  sf_mex_addfield(*c8_info, c8_b_emlrt_marshallOut(0U), "mFileTimeLo",
                  "mFileTimeLo", 32);
  sf_mex_addfield(*c8_info, c8_b_emlrt_marshallOut(0U), "mFileTimeHi",
                  "mFileTimeHi", 32);
  sf_mex_assign(&c8_rhs32, sf_mex_createcellmatrix(0, 1), false);
  sf_mex_assign(&c8_lhs32, sf_mex_createcellmatrix(0, 1), false);
  sf_mex_addfield(*c8_info, sf_mex_duplicatearraysafe(&c8_rhs32), "rhs", "rhs",
                  32);
  sf_mex_addfield(*c8_info, sf_mex_duplicatearraysafe(&c8_lhs32), "lhs", "lhs",
                  32);
  sf_mex_addfield(*c8_info, c8_emlrt_marshallOut(
    "[ILXE]$matlabroot$/toolbox/eml/lib/matlab/elfun/abs.m"), "context",
                  "context", 33);
  sf_mex_addfield(*c8_info, c8_emlrt_marshallOut(
    "coder.internal.isBuiltInNumeric"), "name", "name", 33);
  sf_mex_addfield(*c8_info, c8_emlrt_marshallOut("single"), "dominantType",
                  "dominantType", 33);
  sf_mex_addfield(*c8_info, c8_emlrt_marshallOut(
    "[ILXE]$matlabroot$/toolbox/shared/coder/coder/+coder/+internal/isBuiltInNumeric.m"),
                  "resolved", "resolved", 33);
  sf_mex_addfield(*c8_info, c8_b_emlrt_marshallOut(1395935456U), "fileTimeLo",
                  "fileTimeLo", 33);
  sf_mex_addfield(*c8_info, c8_b_emlrt_marshallOut(0U), "fileTimeHi",
                  "fileTimeHi", 33);
  sf_mex_addfield(*c8_info, c8_b_emlrt_marshallOut(0U), "mFileTimeLo",
                  "mFileTimeLo", 33);
  sf_mex_addfield(*c8_info, c8_b_emlrt_marshallOut(0U), "mFileTimeHi",
                  "mFileTimeHi", 33);
  sf_mex_assign(&c8_rhs33, sf_mex_createcellmatrix(0, 1), false);
  sf_mex_assign(&c8_lhs33, sf_mex_createcellmatrix(0, 1), false);
  sf_mex_addfield(*c8_info, sf_mex_duplicatearraysafe(&c8_rhs33), "rhs", "rhs",
                  33);
  sf_mex_addfield(*c8_info, sf_mex_duplicatearraysafe(&c8_lhs33), "lhs", "lhs",
                  33);
  sf_mex_addfield(*c8_info, c8_emlrt_marshallOut(
    "[ILXE]$matlabroot$/toolbox/eml/lib/matlab/elfun/abs.m"), "context",
                  "context", 34);
  sf_mex_addfield(*c8_info, c8_emlrt_marshallOut("eml_scalar_abs"), "name",
                  "name", 34);
  sf_mex_addfield(*c8_info, c8_emlrt_marshallOut("single"), "dominantType",
                  "dominantType", 34);
  sf_mex_addfield(*c8_info, c8_emlrt_marshallOut(
    "[ILXE]$matlabroot$/toolbox/eml/lib/matlab/elfun/eml_scalar_abs.m"),
                  "resolved", "resolved", 34);
  sf_mex_addfield(*c8_info, c8_b_emlrt_marshallOut(1286822312U), "fileTimeLo",
                  "fileTimeLo", 34);
  sf_mex_addfield(*c8_info, c8_b_emlrt_marshallOut(0U), "fileTimeHi",
                  "fileTimeHi", 34);
  sf_mex_addfield(*c8_info, c8_b_emlrt_marshallOut(0U), "mFileTimeLo",
                  "mFileTimeLo", 34);
  sf_mex_addfield(*c8_info, c8_b_emlrt_marshallOut(0U), "mFileTimeHi",
                  "mFileTimeHi", 34);
  sf_mex_assign(&c8_rhs34, sf_mex_createcellmatrix(0, 1), false);
  sf_mex_assign(&c8_lhs34, sf_mex_createcellmatrix(0, 1), false);
  sf_mex_addfield(*c8_info, sf_mex_duplicatearraysafe(&c8_rhs34), "rhs", "rhs",
                  34);
  sf_mex_addfield(*c8_info, sf_mex_duplicatearraysafe(&c8_lhs34), "lhs", "lhs",
                  34);
  sf_mex_destroy(&c8_rhs0);
  sf_mex_destroy(&c8_lhs0);
  sf_mex_destroy(&c8_rhs1);
  sf_mex_destroy(&c8_lhs1);
  sf_mex_destroy(&c8_rhs2);
  sf_mex_destroy(&c8_lhs2);
  sf_mex_destroy(&c8_rhs3);
  sf_mex_destroy(&c8_lhs3);
  sf_mex_destroy(&c8_rhs4);
  sf_mex_destroy(&c8_lhs4);
  sf_mex_destroy(&c8_rhs5);
  sf_mex_destroy(&c8_lhs5);
  sf_mex_destroy(&c8_rhs6);
  sf_mex_destroy(&c8_lhs6);
  sf_mex_destroy(&c8_rhs7);
  sf_mex_destroy(&c8_lhs7);
  sf_mex_destroy(&c8_rhs8);
  sf_mex_destroy(&c8_lhs8);
  sf_mex_destroy(&c8_rhs9);
  sf_mex_destroy(&c8_lhs9);
  sf_mex_destroy(&c8_rhs10);
  sf_mex_destroy(&c8_lhs10);
  sf_mex_destroy(&c8_rhs11);
  sf_mex_destroy(&c8_lhs11);
  sf_mex_destroy(&c8_rhs12);
  sf_mex_destroy(&c8_lhs12);
  sf_mex_destroy(&c8_rhs13);
  sf_mex_destroy(&c8_lhs13);
  sf_mex_destroy(&c8_rhs14);
  sf_mex_destroy(&c8_lhs14);
  sf_mex_destroy(&c8_rhs15);
  sf_mex_destroy(&c8_lhs15);
  sf_mex_destroy(&c8_rhs16);
  sf_mex_destroy(&c8_lhs16);
  sf_mex_destroy(&c8_rhs17);
  sf_mex_destroy(&c8_lhs17);
  sf_mex_destroy(&c8_rhs18);
  sf_mex_destroy(&c8_lhs18);
  sf_mex_destroy(&c8_rhs19);
  sf_mex_destroy(&c8_lhs19);
  sf_mex_destroy(&c8_rhs20);
  sf_mex_destroy(&c8_lhs20);
  sf_mex_destroy(&c8_rhs21);
  sf_mex_destroy(&c8_lhs21);
  sf_mex_destroy(&c8_rhs22);
  sf_mex_destroy(&c8_lhs22);
  sf_mex_destroy(&c8_rhs23);
  sf_mex_destroy(&c8_lhs23);
  sf_mex_destroy(&c8_rhs24);
  sf_mex_destroy(&c8_lhs24);
  sf_mex_destroy(&c8_rhs25);
  sf_mex_destroy(&c8_lhs25);
  sf_mex_destroy(&c8_rhs26);
  sf_mex_destroy(&c8_lhs26);
  sf_mex_destroy(&c8_rhs27);
  sf_mex_destroy(&c8_lhs27);
  sf_mex_destroy(&c8_rhs28);
  sf_mex_destroy(&c8_lhs28);
  sf_mex_destroy(&c8_rhs29);
  sf_mex_destroy(&c8_lhs29);
  sf_mex_destroy(&c8_rhs30);
  sf_mex_destroy(&c8_lhs30);
  sf_mex_destroy(&c8_rhs31);
  sf_mex_destroy(&c8_lhs31);
  sf_mex_destroy(&c8_rhs32);
  sf_mex_destroy(&c8_lhs32);
  sf_mex_destroy(&c8_rhs33);
  sf_mex_destroy(&c8_lhs33);
  sf_mex_destroy(&c8_rhs34);
  sf_mex_destroy(&c8_lhs34);
}

static const mxArray *c8_emlrt_marshallOut(const char * c8_u)
{
  const mxArray *c8_y = NULL;
  c8_y = NULL;
  sf_mex_assign(&c8_y, sf_mex_create("y", c8_u, 15, 0U, 0U, 0U, 2, 1, strlen
    (c8_u)), false);
  return c8_y;
}

static const mxArray *c8_b_emlrt_marshallOut(const uint32_T c8_u)
{
  const mxArray *c8_y = NULL;
  c8_y = NULL;
  sf_mex_assign(&c8_y, sf_mex_create("y", &c8_u, 7, 0U, 0U, 0U, 0), false);
  return c8_y;
}

static int32_T c8_intlength(SFc8_LessonIIInstanceStruct *chartInstance, int8_T
  c8_x_data[], int32_T c8_x_sizes[2])
{
  int32_T c8_n;
  real_T c8_a;
  real_T c8_b_a;
  real_T c8_flt;
  boolean_T c8_p;
  real_T c8_c_a;
  real_T c8_d_a;
  real_T c8_b_flt;
  boolean_T c8_b_p;
  boolean_T guard1 = false;
  (void)chartInstance;
  (void)c8_x_data;
  c8_a = (real_T)c8_x_sizes[0];
  c8_b_a = c8_a;
  c8_flt = c8_b_a;
  c8_p = (0.0 == c8_flt);
  guard1 = false;
  if (c8_p) {
    guard1 = true;
  } else {
    c8_c_a = (real_T)c8_x_sizes[1];
    c8_d_a = c8_c_a;
    c8_b_flt = c8_d_a;
    c8_b_p = (0.0 == c8_b_flt);
    if (c8_b_p) {
      guard1 = true;
    } else if ((real_T)c8_x_sizes[0] > (real_T)c8_x_sizes[1]) {
      c8_n = c8_x_sizes[0];
    } else {
      c8_n = c8_x_sizes[1];
    }
  }

  if (guard1 == true) {
    c8_n = 0;
  }

  return c8_n;
}

static real32_T c8_eml_xnrm2(SFc8_LessonIIInstanceStruct *chartInstance,
  real32_T c8_x[2])
{
  real32_T c8_y;
  real32_T c8_scale;
  int32_T c8_k;
  int32_T c8_b_k;
  real32_T c8_b_x;
  real32_T c8_c_x;
  real32_T c8_absxk;
  real32_T c8_t;
  c8_below_threshold(chartInstance);
  c8_y = 0.0F;
  c8_scale = 1.17549435E-38F;
  for (c8_k = 1; c8_k < 3; c8_k++) {
    c8_b_k = c8_k - 1;
    c8_b_x = c8_x[c8_b_k];
    c8_c_x = c8_b_x;
    c8_absxk = muSingleScalarAbs(c8_c_x);
    if (c8_absxk > c8_scale) {
      c8_t = c8_scale / c8_absxk;
      c8_y = 1.0F + c8_y * c8_t * c8_t;
      c8_scale = c8_absxk;
    } else {
      c8_t = c8_absxk / c8_scale;
      c8_y += c8_t * c8_t;
    }
  }

  return c8_scale * muSingleScalarSqrt(c8_y);
}

static void c8_below_threshold(SFc8_LessonIIInstanceStruct *chartInstance)
{
  (void)chartInstance;
}

static const mxArray *c8_playerData_bus_io(void *chartInstanceVoid, void
  *c8_pData)
{
  const mxArray *c8_mxVal = NULL;
  SFc8_LessonIIInstanceStruct *chartInstance;
  (void)c8_pData;
  chartInstance = (SFc8_LessonIIInstanceStruct *)chartInstanceVoid;
  c8_mxVal = NULL;
  sf_mex_assign(&c8_mxVal, c8_sf_marshall_unsupported(chartInstance), false);
  return c8_mxVal;
}

static const mxArray *c8_sf_marshall_unsupported(void *chartInstanceVoid)
{
  const mxArray *c8_y = NULL;
  SFc8_LessonIIInstanceStruct *chartInstance;
  chartInstance = (SFc8_LessonIIInstanceStruct *)chartInstanceVoid;
  c8_y = NULL;
  sf_mex_assign(&c8_y, c8_c_emlrt_marshallOut(chartInstance,
    "Structures with variable-sized fields unsupported for debugging."), false);
  return c8_y;
}

static const mxArray *c8_c_emlrt_marshallOut(SFc8_LessonIIInstanceStruct
  *chartInstance, const char * c8_u)
{
  const mxArray *c8_y = NULL;
  (void)chartInstance;
  c8_y = NULL;
  sf_mex_assign(&c8_y, sf_mex_create("y", c8_u, 15, 0U, 0U, 0U, 2, 1, strlen
    (c8_u)), false);
  return c8_y;
}

static const mxArray *c8_BallData_bus_io(void *chartInstanceVoid, void *c8_pData)
{
  const mxArray *c8_mxVal = NULL;
  SFc8_LessonIIInstanceStruct *chartInstance;
  (void)c8_pData;
  chartInstance = (SFc8_LessonIIInstanceStruct *)chartInstanceVoid;
  c8_mxVal = NULL;
  sf_mex_assign(&c8_mxVal, c8_sf_marshall_unsupported(chartInstance), false);
  return c8_mxVal;
}

static const mxArray *c8_players_bus_io(void *chartInstanceVoid, void *c8_pData)
{
  const mxArray *c8_mxVal = NULL;
  int32_T c8_i70;
  int32_T c8_i71;
  c8_Player c8_tmp[6];
  SFc8_LessonIIInstanceStruct *chartInstance;
  chartInstance = (SFc8_LessonIIInstanceStruct *)chartInstanceVoid;
  c8_mxVal = NULL;
  for (c8_i70 = 0; c8_i70 < 6; c8_i70++) {
    for (c8_i71 = 0; c8_i71 < 1; c8_i71++) {
      c8_tmp[c8_i71 + c8_i70].x = *(int8_T *)&((char_T *)(c8_Player *)&((char_T *)
        (c8_Player (*)[6])c8_pData)[8 * (c8_i71 + c8_i70)])[0];
      c8_tmp[c8_i71 + c8_i70].y = *(int8_T *)&((char_T *)(c8_Player *)&((char_T *)
        (c8_Player (*)[6])c8_pData)[8 * (c8_i71 + c8_i70)])[1];
      c8_tmp[c8_i71 + c8_i70].orientation = *(int16_T *)&((char_T *)(c8_Player *)
        &((char_T *)(c8_Player (*)[6])c8_pData)[8 * (c8_i71 + c8_i70)])[2];
      c8_tmp[c8_i71 + c8_i70].color = *(uint8_T *)&((char_T *)(c8_Player *)
        &((char_T *)(c8_Player (*)[6])c8_pData)[8 * (c8_i71 + c8_i70)])[4];
      c8_tmp[c8_i71 + c8_i70].position = *(uint8_T *)&((char_T *)(c8_Player *)
        &((char_T *)(c8_Player (*)[6])c8_pData)[8 * (c8_i71 + c8_i70)])[5];
      c8_tmp[c8_i71 + c8_i70].valid = *(uint8_T *)&((char_T *)(c8_Player *)
        &((char_T *)(c8_Player (*)[6])c8_pData)[8 * (c8_i71 + c8_i70)])[6];
    }
  }

  sf_mex_assign(&c8_mxVal, c8_d_sf_marshallOut(chartInstance, c8_tmp), false);
  return c8_mxVal;
}

static const mxArray *c8_ball_bus_io(void *chartInstanceVoid, void *c8_pData)
{
  const mxArray *c8_mxVal = NULL;
  c8_Ball c8_tmp;
  SFc8_LessonIIInstanceStruct *chartInstance;
  chartInstance = (SFc8_LessonIIInstanceStruct *)chartInstanceVoid;
  c8_mxVal = NULL;
  c8_tmp.x = *(int8_T *)&((char_T *)(c8_Ball *)c8_pData)[0];
  c8_tmp.y = *(int8_T *)&((char_T *)(c8_Ball *)c8_pData)[1];
  c8_tmp.valid = *(uint8_T *)&((char_T *)(c8_Ball *)c8_pData)[2];
  sf_mex_assign(&c8_mxVal, c8_c_sf_marshallOut(chartInstance, &c8_tmp), false);
  return c8_mxVal;
}

static void init_dsm_address_info(SFc8_LessonIIInstanceStruct *chartInstance)
{
  (void)chartInstance;
}

static void init_simulink_io_address(SFc8_LessonIIInstanceStruct *chartInstance)
{
  chartInstance->c8_playerData_data = (c8_PlayerData *)
    ssGetInputPortSignal_wrapper(chartInstance->S, 0);
  chartInstance->c8_playerData_elems_sizes = (c8_PlayerData_size *)
    ssGetCurrentInputPortDimensions_wrapper(chartInstance->S, 0);
  chartInstance->c8_BallData_data = (c8_BallData *)ssGetInputPortSignal_wrapper
    (chartInstance->S, 1);
  chartInstance->c8_BallData_elems_sizes = (c8_BallData_size *)
    ssGetCurrentInputPortDimensions_wrapper(chartInstance->S, 1);
  chartInstance->c8_players = (c8_Player (*)[6])ssGetOutputPortSignal_wrapper
    (chartInstance->S, 1);
  chartInstance->c8_ball = (c8_Ball *)ssGetOutputPortSignal_wrapper
    (chartInstance->S, 2);
}

/* SFunction Glue Code */
#ifdef utFree
#undef utFree
#endif

#ifdef utMalloc
#undef utMalloc
#endif

#ifdef __cplusplus

extern "C" void *utMalloc(size_t size);
extern "C" void utFree(void*);

#else

extern void *utMalloc(size_t size);
extern void utFree(void*);

#endif

void sf_c8_LessonII_get_check_sum(mxArray *plhs[])
{
  ((real_T *)mxGetPr((plhs[0])))[0] = (real_T)(3464238175U);
  ((real_T *)mxGetPr((plhs[0])))[1] = (real_T)(3723378460U);
  ((real_T *)mxGetPr((plhs[0])))[2] = (real_T)(3129696248U);
  ((real_T *)mxGetPr((plhs[0])))[3] = (real_T)(1771236838U);
}

mxArray* sf_c8_LessonII_get_post_codegen_info(void);
mxArray *sf_c8_LessonII_get_autoinheritance_info(void)
{
  const char *autoinheritanceFields[] = { "checksum", "inputs", "parameters",
    "outputs", "locals", "postCodegenInfo" };

  mxArray *mxAutoinheritanceInfo = mxCreateStructMatrix(1, 1, sizeof
    (autoinheritanceFields)/sizeof(autoinheritanceFields[0]),
    autoinheritanceFields);

  {
    mxArray *mxChecksum = mxCreateString("XEXsRmqFscIqZkSUMcSdQF");
    mxSetField(mxAutoinheritanceInfo,0,"checksum",mxChecksum);
  }

  {
    const char *dataFields[] = { "size", "type", "complexity" };

    mxArray *mxData = mxCreateStructMatrix(1,2,3,dataFields);

    {
      mxArray *mxSize = mxCreateDoubleMatrix(1,2,mxREAL);
      double *pr = mxGetPr(mxSize);
      pr[0] = (double)(1);
      pr[1] = (double)(1);
      mxSetField(mxData,0,"size",mxSize);
    }

    {
      const char *typeFields[] = { "base", "fixpt" };

      mxArray *mxType = mxCreateStructMatrix(1,1,2,typeFields);
      mxSetField(mxType,0,"base",mxCreateDoubleScalar(13));
      mxSetField(mxType,0,"fixpt",mxCreateDoubleMatrix(0,0,mxREAL));
      mxSetField(mxData,0,"type",mxType);
    }

    mxSetField(mxData,0,"complexity",mxCreateDoubleScalar(0));

    {
      mxArray *mxSize = mxCreateDoubleMatrix(1,2,mxREAL);
      double *pr = mxGetPr(mxSize);
      pr[0] = (double)(1);
      pr[1] = (double)(1);
      mxSetField(mxData,1,"size",mxSize);
    }

    {
      const char *typeFields[] = { "base", "fixpt" };

      mxArray *mxType = mxCreateStructMatrix(1,1,2,typeFields);
      mxSetField(mxType,0,"base",mxCreateDoubleScalar(13));
      mxSetField(mxType,0,"fixpt",mxCreateDoubleMatrix(0,0,mxREAL));
      mxSetField(mxData,1,"type",mxType);
    }

    mxSetField(mxData,1,"complexity",mxCreateDoubleScalar(0));
    mxSetField(mxAutoinheritanceInfo,0,"inputs",mxData);
  }

  {
    mxSetField(mxAutoinheritanceInfo,0,"parameters",mxCreateDoubleMatrix(0,0,
                mxREAL));
  }

  {
    const char *dataFields[] = { "size", "type", "complexity" };

    mxArray *mxData = mxCreateStructMatrix(1,2,3,dataFields);

    {
      mxArray *mxSize = mxCreateDoubleMatrix(1,2,mxREAL);
      double *pr = mxGetPr(mxSize);
      pr[0] = (double)(1);
      pr[1] = (double)(6);
      mxSetField(mxData,0,"size",mxSize);
    }

    {
      const char *typeFields[] = { "base", "fixpt" };

      mxArray *mxType = mxCreateStructMatrix(1,1,2,typeFields);
      mxSetField(mxType,0,"base",mxCreateDoubleScalar(13));
      mxSetField(mxType,0,"fixpt",mxCreateDoubleMatrix(0,0,mxREAL));
      mxSetField(mxData,0,"type",mxType);
    }

    mxSetField(mxData,0,"complexity",mxCreateDoubleScalar(0));

    {
      mxArray *mxSize = mxCreateDoubleMatrix(1,2,mxREAL);
      double *pr = mxGetPr(mxSize);
      pr[0] = (double)(1);
      pr[1] = (double)(1);
      mxSetField(mxData,1,"size",mxSize);
    }

    {
      const char *typeFields[] = { "base", "fixpt" };

      mxArray *mxType = mxCreateStructMatrix(1,1,2,typeFields);
      mxSetField(mxType,0,"base",mxCreateDoubleScalar(13));
      mxSetField(mxType,0,"fixpt",mxCreateDoubleMatrix(0,0,mxREAL));
      mxSetField(mxData,1,"type",mxType);
    }

    mxSetField(mxData,1,"complexity",mxCreateDoubleScalar(0));
    mxSetField(mxAutoinheritanceInfo,0,"outputs",mxData);
  }

  {
    mxSetField(mxAutoinheritanceInfo,0,"locals",mxCreateDoubleMatrix(0,0,mxREAL));
  }

  {
    mxArray* mxPostCodegenInfo = sf_c8_LessonII_get_post_codegen_info();
    mxSetField(mxAutoinheritanceInfo,0,"postCodegenInfo",mxPostCodegenInfo);
  }

  return(mxAutoinheritanceInfo);
}

mxArray *sf_c8_LessonII_third_party_uses_info(void)
{
  mxArray * mxcell3p = mxCreateCellMatrix(1,0);
  return(mxcell3p);
}

mxArray *sf_c8_LessonII_jit_fallback_info(void)
{
  const char *infoFields[] = { "fallbackType", "fallbackReason",
    "incompatibleSymbol", };

  mxArray *mxInfo = mxCreateStructMatrix(1, 1, 3, infoFields);
  mxArray *fallbackReason = mxCreateString("feature_off");
  mxArray *incompatibleSymbol = mxCreateString("");
  mxArray *fallbackType = mxCreateString("early");
  mxSetField(mxInfo, 0, infoFields[0], fallbackType);
  mxSetField(mxInfo, 0, infoFields[1], fallbackReason);
  mxSetField(mxInfo, 0, infoFields[2], incompatibleSymbol);
  return mxInfo;
}

mxArray *sf_c8_LessonII_updateBuildInfo_args_info(void)
{
  mxArray *mxBIArgs = mxCreateCellMatrix(1,0);
  return mxBIArgs;
}

mxArray* sf_c8_LessonII_get_post_codegen_info(void)
{
  const char* fieldNames[] = { "exportedFunctionsUsedByThisChart",
    "exportedFunctionsChecksum" };

  mwSize dims[2] = { 1, 1 };

  mxArray* mxPostCodegenInfo = mxCreateStructArray(2, dims, sizeof(fieldNames)/
    sizeof(fieldNames[0]), fieldNames);

  {
    mxArray* mxExportedFunctionsChecksum = mxCreateString("");
    mwSize exp_dims[2] = { 0, 1 };

    mxArray* mxExportedFunctionsUsedByThisChart = mxCreateCellArray(2, exp_dims);
    mxSetField(mxPostCodegenInfo, 0, "exportedFunctionsUsedByThisChart",
               mxExportedFunctionsUsedByThisChart);
    mxSetField(mxPostCodegenInfo, 0, "exportedFunctionsChecksum",
               mxExportedFunctionsChecksum);
  }

  return mxPostCodegenInfo;
}

static const mxArray *sf_get_sim_state_info_c8_LessonII(void)
{
  const char *infoFields[] = { "chartChecksum", "varInfo" };

  mxArray *mxInfo = mxCreateStructMatrix(1, 1, 2, infoFields);
  const char *infoEncStr[] = {
    "100 S1x5'type','srcId','name','auxInfo'{{M[1],M[13],T\"ball\",},{M[1],M[5],T\"players\",},{M[4],M[0],T\"latestBall\",S'l','i','p'{{M1x2[134 144],M[0],}}},{M[4],M[0],T\"latestPlayers\",S'l','i','p'{{M1x2[108 121],M[0],}}},{M[8],M[0],T\"is_active_c8_LessonII\",}}"
  };

  mxArray *mxVarInfo = sf_mex_decode_encoded_mx_struct_array(infoEncStr, 5, 10);
  mxArray *mxChecksum = mxCreateDoubleMatrix(1, 4, mxREAL);
  sf_c8_LessonII_get_check_sum(&mxChecksum);
  mxSetField(mxInfo, 0, infoFields[0], mxChecksum);
  mxSetField(mxInfo, 0, infoFields[1], mxVarInfo);
  return mxInfo;
}

static void chart_debug_initialization(SimStruct *S, unsigned int
  fullDebuggerInitialization)
{
  if (!sim_mode_is_rtw_gen(S)) {
    SFc8_LessonIIInstanceStruct *chartInstance;
    ChartRunTimeInfo * crtInfo = (ChartRunTimeInfo *)(ssGetUserData(S));
    ChartInfoStruct * chartInfo = (ChartInfoStruct *)(crtInfo->instanceInfo);
    chartInstance = (SFc8_LessonIIInstanceStruct *) chartInfo->chartInstance;
    if (ssIsFirstInitCond(S) && fullDebuggerInitialization==1) {
      /* do this only if simulation is starting */
      {
        unsigned int chartAlreadyPresent;
        chartAlreadyPresent = sf_debug_initialize_chart
          (sfGlobalDebugInstanceStruct,
           _LessonIIMachineNumber_,
           8,
           1,
           1,
           0,
           4,
           0,
           0,
           0,
           0,
           0,
           &(chartInstance->chartNumber),
           &(chartInstance->instanceNumber),
           (void *)S);

        /* Each instance must initialize its own list of scripts */
        init_script_number_translation(_LessonIIMachineNumber_,
          chartInstance->chartNumber,chartInstance->instanceNumber);
        if (chartAlreadyPresent==0) {
          /* this is the first instance */
          sf_debug_set_chart_disable_implicit_casting
            (sfGlobalDebugInstanceStruct,_LessonIIMachineNumber_,
             chartInstance->chartNumber,1);
          sf_debug_set_chart_event_thresholds(sfGlobalDebugInstanceStruct,
            _LessonIIMachineNumber_,
            chartInstance->chartNumber,
            0,
            0,
            0);
          _SFD_SET_DATA_PROPS(0,1,1,0,"playerData");
          _SFD_SET_DATA_PROPS(1,1,1,0,"BallData");
          _SFD_SET_DATA_PROPS(2,2,0,1,"players");
          _SFD_SET_DATA_PROPS(3,2,0,1,"ball");
          _SFD_STATE_INFO(0,0,2);
          _SFD_CH_SUBSTATE_COUNT(0);
          _SFD_CH_SUBSTATE_DECOMP(0);
        }

        _SFD_CV_INIT_CHART(0,0,0,0);

        {
          _SFD_CV_INIT_STATE(0,0,0,0,0,0,NULL,NULL);
        }

        _SFD_CV_INIT_TRANS(0,0,NULL,NULL,0,NULL);

        /* Initialization of MATLAB Function Model Coverage */
        _SFD_CV_INIT_EML(0,1,1,6,0,3,0,3,0,3,2);
        _SFD_CV_INIT_EML_FCN(0,0,"eML_blk_kernel",0,-1,2065);
        _SFD_CV_INIT_EML_SATURATION(0,1,0,1071,-1,1137);
        _SFD_CV_INIT_EML_SATURATION(0,1,1,1089,-1,1134);
        _SFD_CV_INIT_EML_SATURATION(0,1,2,1228,-1,1237);
        _SFD_CV_INIT_EML_IF(0,1,0,153,178,-1,524);
        _SFD_CV_INIT_EML_IF(0,1,1,531,553,-1,640);
        _SFD_CV_INIT_EML_IF(0,1,2,740,763,-1,862);
        _SFD_CV_INIT_EML_IF(0,1,3,1005,1053,-1,1266);
        _SFD_CV_INIT_EML_IF(0,1,4,1151,1177,-1,1254);
        _SFD_CV_INIT_EML_IF(0,1,5,1279,1297,-1,1517);
        _SFD_CV_INIT_EML_FOR(0,1,0,864,894,1521);
        _SFD_CV_INIT_EML_FOR(0,1,1,986,997,1274);
        _SFD_CV_INIT_EML_FOR(0,1,2,1765,1776,2063);

        {
          static int condStart[] = { 744 };

          static int condEnd[] = { 763 };

          static int pfixExpr[] = { 0, -1 };

          _SFD_CV_INIT_EML_MCDC(0,1,0,743,763,1,0,&(condStart[0]),&(condEnd[0]),
                                2,&(pfixExpr[0]));
        }

        {
          static int condStart[] = { 1156, 1169 };

          static int condEnd[] = { 1165, 1177 };

          static int pfixExpr[] = { 0, 1, -3 };

          _SFD_CV_INIT_EML_MCDC(0,1,1,1156,1177,2,1,&(condStart[0]),&(condEnd[0]),
                                3,&(pfixExpr[0]));
        }

        _SFD_CV_INIT_EML_RELATIONAL(0,1,0,1008,1053,0,0);
        _SFD_CV_INIT_EML_RELATIONAL(0,1,1,1156,1165,0,2);
        _SFD_CV_INIT_EML_RELATIONAL(0,1,2,1169,1177,-1,2);
        _SFD_CV_INIT_EML_RELATIONAL(0,1,3,1282,1297,0,1);
        _SFD_SET_DATA_COMPILED_PROPS(0,SF_STRUCT,0,NULL,0,0,0,0.0,1.0,0,0,
          (MexFcnForType)c8_playerData_bus_io,(MexInFcnForType)NULL);
        _SFD_SET_DATA_COMPILED_PROPS(1,SF_STRUCT,0,NULL,0,0,0,0.0,1.0,0,0,
          (MexFcnForType)c8_BallData_bus_io,(MexInFcnForType)NULL);

        {
          unsigned int dimVector[2];
          dimVector[0]= 1;
          dimVector[1]= 6;
          _SFD_SET_DATA_COMPILED_PROPS(2,SF_STRUCT,2,&(dimVector[0]),0,0,0,0.0,
            1.0,0,0,(MexFcnForType)c8_players_bus_io,(MexInFcnForType)NULL);
        }

        _SFD_SET_DATA_COMPILED_PROPS(3,SF_STRUCT,0,NULL,0,0,0,0.0,1.0,0,0,
          (MexFcnForType)c8_ball_bus_io,(MexInFcnForType)NULL);
        _SFD_SET_DATA_VALUE_PTR(0U, chartInstance->c8_playerData_data);
        _SFD_SET_DATA_VALUE_PTR(1U, chartInstance->c8_BallData_data);
        _SFD_SET_DATA_VALUE_PTR(2U, *chartInstance->c8_players);
        _SFD_SET_DATA_VALUE_PTR(3U, chartInstance->c8_ball);
      }
    } else {
      sf_debug_reset_current_state_configuration(sfGlobalDebugInstanceStruct,
        _LessonIIMachineNumber_,chartInstance->chartNumber,
        chartInstance->instanceNumber);
    }
  }
}

static const char* sf_get_instance_specialization(void)
{
  return "Jbiqze4DlRPbPmbIL0wS8F";
}

static void sf_opaque_initialize_c8_LessonII(void *chartInstanceVar)
{
  chart_debug_initialization(((SFc8_LessonIIInstanceStruct*) chartInstanceVar)
    ->S,0);
  initialize_params_c8_LessonII((SFc8_LessonIIInstanceStruct*) chartInstanceVar);
  initialize_c8_LessonII((SFc8_LessonIIInstanceStruct*) chartInstanceVar);
}

static void sf_opaque_enable_c8_LessonII(void *chartInstanceVar)
{
  enable_c8_LessonII((SFc8_LessonIIInstanceStruct*) chartInstanceVar);
}

static void sf_opaque_disable_c8_LessonII(void *chartInstanceVar)
{
  disable_c8_LessonII((SFc8_LessonIIInstanceStruct*) chartInstanceVar);
}

static void sf_opaque_gateway_c8_LessonII(void *chartInstanceVar)
{
  sf_gateway_c8_LessonII((SFc8_LessonIIInstanceStruct*) chartInstanceVar);
}

static const mxArray* sf_opaque_get_sim_state_c8_LessonII(SimStruct* S)
{
  ChartRunTimeInfo * crtInfo = (ChartRunTimeInfo *)(ssGetUserData(S));
  ChartInfoStruct * chartInfo = (ChartInfoStruct *)(crtInfo->instanceInfo);
  return get_sim_state_c8_LessonII((SFc8_LessonIIInstanceStruct*)
    chartInfo->chartInstance);         /* raw sim ctx */
}

static void sf_opaque_set_sim_state_c8_LessonII(SimStruct* S, const mxArray *st)
{
  ChartRunTimeInfo * crtInfo = (ChartRunTimeInfo *)(ssGetUserData(S));
  ChartInfoStruct * chartInfo = (ChartInfoStruct *)(crtInfo->instanceInfo);
  set_sim_state_c8_LessonII((SFc8_LessonIIInstanceStruct*)
    chartInfo->chartInstance, st);
}

static void sf_opaque_terminate_c8_LessonII(void *chartInstanceVar)
{
  if (chartInstanceVar!=NULL) {
    SimStruct *S = ((SFc8_LessonIIInstanceStruct*) chartInstanceVar)->S;
    ChartRunTimeInfo * crtInfo = (ChartRunTimeInfo *)(ssGetUserData(S));
    if (sim_mode_is_rtw_gen(S) || sim_mode_is_external(S)) {
      sf_clear_rtw_identifier(S);
      unload_LessonII_optimization_info();
    }

    finalize_c8_LessonII((SFc8_LessonIIInstanceStruct*) chartInstanceVar);
    utFree(chartInstanceVar);
    if (crtInfo != NULL) {
      utFree(crtInfo);
    }

    ssSetUserData(S,NULL);
  }
}

static void sf_opaque_init_subchart_simstructs(void *chartInstanceVar)
{
  initSimStructsc8_LessonII((SFc8_LessonIIInstanceStruct*) chartInstanceVar);
}

extern unsigned int sf_machine_global_initializer_called(void);
static void mdlProcessParameters_c8_LessonII(SimStruct *S)
{
  int i;
  for (i=0;i<ssGetNumRunTimeParams(S);i++) {
    if (ssGetSFcnParamTunable(S,i)) {
      ssUpdateDlgParamAsRunTimeParam(S,i);
    }
  }

  if (sf_machine_global_initializer_called()) {
    ChartRunTimeInfo * crtInfo = (ChartRunTimeInfo *)(ssGetUserData(S));
    ChartInfoStruct * chartInfo = (ChartInfoStruct *)(crtInfo->instanceInfo);
    initialize_params_c8_LessonII((SFc8_LessonIIInstanceStruct*)
      (chartInfo->chartInstance));
  }
}

static void mdlSetWorkWidths_c8_LessonII(SimStruct *S)
{
  if (sim_mode_is_rtw_gen(S) || sim_mode_is_external(S)) {
    mxArray *infoStruct = load_LessonII_optimization_info();
    int_T chartIsInlinable =
      (int_T)sf_is_chart_inlinable(sf_get_instance_specialization(),infoStruct,8);
    ssSetStateflowIsInlinable(S,chartIsInlinable);
    ssSetRTWCG(S,sf_rtw_info_uint_prop(sf_get_instance_specialization(),
                infoStruct,8,"RTWCG"));
    ssSetEnableFcnIsTrivial(S,1);
    ssSetDisableFcnIsTrivial(S,1);
    ssSetNotMultipleInlinable(S,sf_rtw_info_uint_prop
      (sf_get_instance_specialization(),infoStruct,8,
       "gatewayCannotBeInlinedMultipleTimes"));
    sf_update_buildInfo(sf_get_instance_specialization(),infoStruct,8);
    if (chartIsInlinable) {
      ssSetInputPortOptimOpts(S, 0, SS_REUSABLE_AND_LOCAL);
      ssSetInputPortOptimOpts(S, 1, SS_REUSABLE_AND_LOCAL);
      sf_mark_chart_expressionable_inputs(S,sf_get_instance_specialization(),
        infoStruct,8,2);
      sf_mark_chart_reusable_outputs(S,sf_get_instance_specialization(),
        infoStruct,8,2);
    }

    {
      unsigned int outPortIdx;
      for (outPortIdx=1; outPortIdx<=2; ++outPortIdx) {
        ssSetOutputPortOptimizeInIR(S, outPortIdx, 1U);
      }
    }

    {
      unsigned int inPortIdx;
      for (inPortIdx=0; inPortIdx < 2; ++inPortIdx) {
        ssSetInputPortOptimizeInIR(S, inPortIdx, 1U);
      }
    }

    sf_set_rtw_dwork_info(S,sf_get_instance_specialization(),infoStruct,8);
    ssSetHasSubFunctions(S,!(chartIsInlinable));
  } else {
  }

  ssSetOptions(S,ssGetOptions(S)|SS_OPTION_WORKS_WITH_CODE_REUSE);
  ssSetChecksum0(S,(4076360868U));
  ssSetChecksum1(S,(2179013742U));
  ssSetChecksum2(S,(2176894176U));
  ssSetChecksum3(S,(1540316927U));
  ssSetmdlDerivatives(S, NULL);
  ssSetExplicitFCSSCtrl(S,1);
  ssSupportsMultipleExecInstances(S,1);
}

static void mdlRTW_c8_LessonII(SimStruct *S)
{
  if (sim_mode_is_rtw_gen(S)) {
    ssWriteRTWStrParam(S, "StateflowChartType", "Embedded MATLAB");
  }
}

static void mdlStart_c8_LessonII(SimStruct *S)
{
  SFc8_LessonIIInstanceStruct *chartInstance;
  ChartRunTimeInfo * crtInfo = (ChartRunTimeInfo *)utMalloc(sizeof
    (ChartRunTimeInfo));
  chartInstance = (SFc8_LessonIIInstanceStruct *)utMalloc(sizeof
    (SFc8_LessonIIInstanceStruct));
  memset(chartInstance, 0, sizeof(SFc8_LessonIIInstanceStruct));
  if (chartInstance==NULL) {
    sf_mex_error_message("Could not allocate memory for chart instance.");
  }

  chartInstance->chartInfo.chartInstance = chartInstance;
  chartInstance->chartInfo.isEMLChart = 1;
  chartInstance->chartInfo.chartInitialized = 0;
  chartInstance->chartInfo.sFunctionGateway = sf_opaque_gateway_c8_LessonII;
  chartInstance->chartInfo.initializeChart = sf_opaque_initialize_c8_LessonII;
  chartInstance->chartInfo.terminateChart = sf_opaque_terminate_c8_LessonII;
  chartInstance->chartInfo.enableChart = sf_opaque_enable_c8_LessonII;
  chartInstance->chartInfo.disableChart = sf_opaque_disable_c8_LessonII;
  chartInstance->chartInfo.getSimState = sf_opaque_get_sim_state_c8_LessonII;
  chartInstance->chartInfo.setSimState = sf_opaque_set_sim_state_c8_LessonII;
  chartInstance->chartInfo.getSimStateInfo = sf_get_sim_state_info_c8_LessonII;
  chartInstance->chartInfo.zeroCrossings = NULL;
  chartInstance->chartInfo.outputs = NULL;
  chartInstance->chartInfo.derivatives = NULL;
  chartInstance->chartInfo.mdlRTW = mdlRTW_c8_LessonII;
  chartInstance->chartInfo.mdlStart = mdlStart_c8_LessonII;
  chartInstance->chartInfo.mdlSetWorkWidths = mdlSetWorkWidths_c8_LessonII;
  chartInstance->chartInfo.extModeExec = NULL;
  chartInstance->chartInfo.restoreLastMajorStepConfiguration = NULL;
  chartInstance->chartInfo.restoreBeforeLastMajorStepConfiguration = NULL;
  chartInstance->chartInfo.storeCurrentConfiguration = NULL;
  chartInstance->chartInfo.callAtomicSubchartUserFcn = NULL;
  chartInstance->chartInfo.callAtomicSubchartAutoFcn = NULL;
  chartInstance->chartInfo.debugInstance = sfGlobalDebugInstanceStruct;
  chartInstance->S = S;
  crtInfo->checksum = SF_RUNTIME_INFO_CHECKSUM;
  crtInfo->instanceInfo = (&(chartInstance->chartInfo));
  crtInfo->isJITEnabled = false;
  crtInfo->compiledInfo = NULL;
  ssSetUserData(S,(void *)(crtInfo));  /* register the chart instance with simstruct */
  init_dsm_address_info(chartInstance);
  init_simulink_io_address(chartInstance);
  if (!sim_mode_is_rtw_gen(S)) {
  }

  sf_opaque_init_subchart_simstructs(chartInstance->chartInfo.chartInstance);
  chart_debug_initialization(S,1);
}

void c8_LessonII_method_dispatcher(SimStruct *S, int_T method, void *data)
{
  switch (method) {
   case SS_CALL_MDL_START:
    mdlStart_c8_LessonII(S);
    break;

   case SS_CALL_MDL_SET_WORK_WIDTHS:
    mdlSetWorkWidths_c8_LessonII(S);
    break;

   case SS_CALL_MDL_PROCESS_PARAMETERS:
    mdlProcessParameters_c8_LessonII(S);
    break;

   default:
    /* Unhandled method */
    sf_mex_error_message("Stateflow Internal Error:\n"
                         "Error calling c8_LessonII_method_dispatcher.\n"
                         "Can't handle method %d.\n", method);
    break;
  }
}

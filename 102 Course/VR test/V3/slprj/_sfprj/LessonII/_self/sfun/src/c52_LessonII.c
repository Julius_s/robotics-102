/* Include files */

#include <stddef.h>
#include "blas.h"
#include "LessonII_sfun.h"
#include "c52_LessonII.h"
#include "mwmathutil.h"
#define CHARTINSTANCE_CHARTNUMBER      (chartInstance->chartNumber)
#define CHARTINSTANCE_INSTANCENUMBER   (chartInstance->instanceNumber)
#include "LessonII_sfun_debug_macros.h"
#define _SF_MEX_LISTEN_FOR_CTRL_C(S)   sf_mex_listen_for_ctrl_c(sfGlobalDebugInstanceStruct,S);

/* Type Definitions */

/* Named Constants */
#define CALL_EVENT                     (-1)
#define c52_IN_NO_ACTIVE_CHILD         ((uint8_T)0U)
#define c52_IN_GameIsOn_Offence        ((uint8_T)1U)
#define c52_IN_waiting                 ((uint8_T)2U)
#define c52_IN_Aim                     ((uint8_T)1U)
#define c52_IN_GetToTheBall            ((uint8_T)2U)
#define c52_IN_Idle                    ((uint8_T)3U)
#define c52_IN_Kick                    ((uint8_T)4U)
#define c52_IN_Idle1                   ((uint8_T)1U)
#define c52_IN_goToPosition            ((uint8_T)2U)

/* Variable Declarations */

/* Variable Definitions */
static real_T _sfTime_;
static const char * c52_debug_family_names[3] = { "pos", "nargin", "nargout" };

static const char * c52_b_debug_family_names[2] = { "nargin", "nargout" };

static const char * c52_c_debug_family_names[2] = { "nargin", "nargout" };

static const char * c52_d_debug_family_names[2] = { "nargin", "nargout" };

static const char * c52_e_debug_family_names[2] = { "nargin", "nargout" };

static const char * c52_f_debug_family_names[2] = { "nargin", "nargout" };

static const char * c52_g_debug_family_names[2] = { "nargin", "nargout" };

static const char * c52_h_debug_family_names[2] = { "nargin", "nargout" };

static const char * c52_i_debug_family_names[2] = { "nargin", "nargout" };

static const char * c52_j_debug_family_names[2] = { "nargin", "nargout" };

static const char * c52_k_debug_family_names[3] = { "nargin", "nargout",
  "sf_internal_predicateOutput" };

static const char * c52_l_debug_family_names[3] = { "nargin", "nargout",
  "sf_internal_predicateOutput" };

static const char * c52_m_debug_family_names[5] = { "nargin", "nargout", "pos",
  "tol", "posReached" };

static const char * c52_n_debug_family_names[3] = { "nargin", "nargout",
  "sf_internal_predicateOutput" };

static const char * c52_o_debug_family_names[3] = { "nargin", "nargout",
  "sf_internal_predicateOutput" };

static const char * c52_p_debug_family_names[3] = { "nargin", "nargout",
  "sf_internal_predicateOutput" };

static const char * c52_q_debug_family_names[5] = { "nargin", "nargout", "pos",
  "tol", "posReached" };

static const char * c52_r_debug_family_names[2] = { "nargin", "nargout" };

static const char * c52_s_debug_family_names[3] = { "nargin", "nargout",
  "sf_internal_predicateOutput" };

static const char * c52_t_debug_family_names[3] = { "nargin", "nargout",
  "sf_internal_predicateOutput" };

static const char * c52_u_debug_family_names[3] = { "nargin", "nargout",
  "sf_internal_predicateOutput" };

static const char * c52_v_debug_family_names[3] = { "nargin", "nargout",
  "sf_internal_predicateOutput" };

static const char * c52_w_debug_family_names[3] = { "nargin", "nargout",
  "sf_internal_predicateOutput" };

static const char * c52_x_debug_family_names[3] = { "nargin", "nargout",
  "sf_internal_predicateOutput" };

static const char * c52_y_debug_family_names[3] = { "nargin", "nargout",
  "sf_internal_predicateOutput" };

static const char * c52_ab_debug_family_names[3] = { "nargin", "nargout",
  "sf_internal_predicateOutput" };

static const char * c52_bb_debug_family_names[3] = { "nargin", "nargout",
  "sf_internal_predicateOutput" };

/* Function Declarations */
static void initialize_c52_LessonII(SFc52_LessonIIInstanceStruct *chartInstance);
static void initialize_params_c52_LessonII(SFc52_LessonIIInstanceStruct
  *chartInstance);
static void enable_c52_LessonII(SFc52_LessonIIInstanceStruct *chartInstance);
static void disable_c52_LessonII(SFc52_LessonIIInstanceStruct *chartInstance);
static void c52_update_debugger_state_c52_LessonII(SFc52_LessonIIInstanceStruct *
  chartInstance);
static const mxArray *get_sim_state_c52_LessonII(SFc52_LessonIIInstanceStruct
  *chartInstance);
static void set_sim_state_c52_LessonII(SFc52_LessonIIInstanceStruct
  *chartInstance, const mxArray *c52_st);
static void c52_set_sim_state_side_effects_c52_LessonII
  (SFc52_LessonIIInstanceStruct *chartInstance);
static void finalize_c52_LessonII(SFc52_LessonIIInstanceStruct *chartInstance);
static void sf_gateway_c52_LessonII(SFc52_LessonIIInstanceStruct *chartInstance);
static void mdl_start_c52_LessonII(SFc52_LessonIIInstanceStruct *chartInstance);
static void initSimStructsc52_LessonII(SFc52_LessonIIInstanceStruct
  *chartInstance);
static void c52_GameIsOn_Offence(SFc52_LessonIIInstanceStruct *chartInstance);
static void c52_exit_internal_GameIsOn_Offence(SFc52_LessonIIInstanceStruct
  *chartInstance);
static void c52_exit_internal_waiting(SFc52_LessonIIInstanceStruct
  *chartInstance);
static int16_T c52_intmax(SFc52_LessonIIInstanceStruct *chartInstance);
static real32_T c52_eml_xnrm2(SFc52_LessonIIInstanceStruct *chartInstance,
  real32_T c52_x[2]);
static void init_script_number_translation(uint32_T c52_machineNumber, uint32_T
  c52_chartNumber, uint32_T c52_instanceNumber);
static const mxArray *c52_sf_marshallOut(void *chartInstanceVoid, void
  *c52_inData);
static real_T c52_emlrt_marshallIn(SFc52_LessonIIInstanceStruct *chartInstance,
  const mxArray *c52_u, const emlrtMsgIdentifier *c52_parentId);
static void c52_sf_marshallIn(void *chartInstanceVoid, const mxArray
  *c52_mxArrayInData, const char_T *c52_varName, void *c52_outData);
static const mxArray *c52_b_sf_marshallOut(void *chartInstanceVoid, void
  *c52_inData);
static void c52_b_emlrt_marshallIn(SFc52_LessonIIInstanceStruct *chartInstance,
  const mxArray *c52_u, const emlrtMsgIdentifier *c52_parentId, int8_T c52_y[2]);
static void c52_b_sf_marshallIn(void *chartInstanceVoid, const mxArray
  *c52_mxArrayInData, const char_T *c52_varName, void *c52_outData);
static const mxArray *c52_c_sf_marshallOut(void *chartInstanceVoid, void
  *c52_inData);
static boolean_T c52_c_emlrt_marshallIn(SFc52_LessonIIInstanceStruct
  *chartInstance, const mxArray *c52_u, const emlrtMsgIdentifier *c52_parentId);
static void c52_c_sf_marshallIn(void *chartInstanceVoid, const mxArray
  *c52_mxArrayInData, const char_T *c52_varName, void *c52_outData);
static const mxArray *c52_d_sf_marshallOut(void *chartInstanceVoid, void
  *c52_inData);
static uint8_T c52_d_emlrt_marshallIn(SFc52_LessonIIInstanceStruct
  *chartInstance, const mxArray *c52_posReached, const char_T *c52_identifier);
static uint8_T c52_e_emlrt_marshallIn(SFc52_LessonIIInstanceStruct
  *chartInstance, const mxArray *c52_u, const emlrtMsgIdentifier *c52_parentId);
static void c52_d_sf_marshallIn(void *chartInstanceVoid, const mxArray
  *c52_mxArrayInData, const char_T *c52_varName, void *c52_outData);
static const mxArray *c52_e_sf_marshallOut(void *chartInstanceVoid, void
  *c52_inData);
static void c52_f_emlrt_marshallIn(SFc52_LessonIIInstanceStruct *chartInstance,
  const mxArray *c52_u, const emlrtMsgIdentifier *c52_parentId, int8_T c52_y[2]);
static void c52_e_sf_marshallIn(void *chartInstanceVoid, const mxArray
  *c52_mxArrayInData, const char_T *c52_varName, void *c52_outData);
static void c52_info_helper(const mxArray **c52_info);
static const mxArray *c52_emlrt_marshallOut(const char * c52_u);
static const mxArray *c52_b_emlrt_marshallOut(const uint32_T c52_u);
static void c52_calcShootWay(SFc52_LessonIIInstanceStruct *chartInstance);
static void c52_calcStartTeams(SFc52_LessonIIInstanceStruct *chartInstance);
static void c52_calcStartPos(SFc52_LessonIIInstanceStruct *chartInstance);
static real32_T c52_b_eml_xnrm2(SFc52_LessonIIInstanceStruct *chartInstance,
  real32_T c52_x[2]);
static void c52_threshold(SFc52_LessonIIInstanceStruct *chartInstance);
static void c52_realmin(SFc52_LessonIIInstanceStruct *chartInstance);
static uint8_T c52_checkReached(SFc52_LessonIIInstanceStruct *chartInstance,
  int8_T c52_pos[2], real_T c52_tol);
static uint8_T c52_b_checkReached(SFc52_LessonIIInstanceStruct *chartInstance,
  int8_T c52_pos[2], real_T c52_tol);
static const mxArray *c52_f_sf_marshallOut(void *chartInstanceVoid, void
  *c52_inData);
static int32_T c52_g_emlrt_marshallIn(SFc52_LessonIIInstanceStruct
  *chartInstance, const mxArray *c52_u, const emlrtMsgIdentifier *c52_parentId);
static void c52_f_sf_marshallIn(void *chartInstanceVoid, const mxArray
  *c52_mxArrayInData, const char_T *c52_varName, void *c52_outData);
static const mxArray *c52_me_bus_io(void *chartInstanceVoid, void *c52_pData);
static const mxArray *c52_g_sf_marshallOut(void *chartInstanceVoid, void
  *c52_inData);
static const mxArray *c52_players_bus_io(void *chartInstanceVoid, void
  *c52_pData);
static const mxArray *c52_h_sf_marshallOut(void *chartInstanceVoid, void
  *c52_inData);
static const mxArray *c52_ball_bus_io(void *chartInstanceVoid, void *c52_pData);
static const mxArray *c52_i_sf_marshallOut(void *chartInstanceVoid, void
  *c52_inData);
static const mxArray *c52_finalWay_bus_io(void *chartInstanceVoid, void
  *c52_pData);
static const mxArray *c52_j_sf_marshallOut(void *chartInstanceVoid, void
  *c52_inData);
static c52_Waypoint c52_h_emlrt_marshallIn(SFc52_LessonIIInstanceStruct
  *chartInstance, const mxArray *c52_b_finalWay, const char_T *c52_identifier);
static c52_Waypoint c52_i_emlrt_marshallIn(SFc52_LessonIIInstanceStruct
  *chartInstance, const mxArray *c52_u, const emlrtMsgIdentifier *c52_parentId);
static int8_T c52_j_emlrt_marshallIn(SFc52_LessonIIInstanceStruct *chartInstance,
  const mxArray *c52_u, const emlrtMsgIdentifier *c52_parentId);
static int16_T c52_k_emlrt_marshallIn(SFc52_LessonIIInstanceStruct
  *chartInstance, const mxArray *c52_u, const emlrtMsgIdentifier *c52_parentId);
static void c52_g_sf_marshallIn(void *chartInstanceVoid, const mxArray
  *c52_mxArrayInData, const char_T *c52_varName, void *c52_outData);
static const mxArray *c52_k_sf_marshallOut(void *chartInstanceVoid, void
  *c52_inData);
static void c52_l_emlrt_marshallIn(SFc52_LessonIIInstanceStruct *chartInstance,
  const mxArray *c52_b_startingPos, const char_T *c52_identifier, int8_T c52_y[2]);
static void c52_m_emlrt_marshallIn(SFc52_LessonIIInstanceStruct *chartInstance,
  const mxArray *c52_u, const emlrtMsgIdentifier *c52_parentId, int8_T c52_y[2]);
static void c52_h_sf_marshallIn(void *chartInstanceVoid, const mxArray
  *c52_mxArrayInData, const char_T *c52_varName, void *c52_outData);
static const mxArray *c52_l_sf_marshallOut(void *chartInstanceVoid, void
  *c52_inData);
static void c52_n_emlrt_marshallIn(SFc52_LessonIIInstanceStruct *chartInstance,
  const mxArray *c52_b_enemy, const char_T *c52_identifier, c52_Player c52_y[3]);
static void c52_o_emlrt_marshallIn(SFc52_LessonIIInstanceStruct *chartInstance,
  const mxArray *c52_u, const emlrtMsgIdentifier *c52_parentId, c52_Player
  c52_y[3]);
static void c52_i_sf_marshallIn(void *chartInstanceVoid, const mxArray
  *c52_mxArrayInData, const char_T *c52_varName, void *c52_outData);
static void c52_p_emlrt_marshallIn(SFc52_LessonIIInstanceStruct *chartInstance,
  const mxArray *c52_b_dataWrittenToVector, const char_T *c52_identifier,
  boolean_T c52_y[7]);
static void c52_q_emlrt_marshallIn(SFc52_LessonIIInstanceStruct *chartInstance,
  const mxArray *c52_u, const emlrtMsgIdentifier *c52_parentId, boolean_T c52_y
  [7]);
static const mxArray *c52_r_emlrt_marshallIn(SFc52_LessonIIInstanceStruct
  *chartInstance, const mxArray *c52_b_setSimStateSideEffectsInfo, const char_T *
  c52_identifier);
static const mxArray *c52_s_emlrt_marshallIn(SFc52_LessonIIInstanceStruct
  *chartInstance, const mxArray *c52_u, const emlrtMsgIdentifier *c52_parentId);
static void c52_updateDataWrittenToVector(SFc52_LessonIIInstanceStruct
  *chartInstance, uint32_T c52_vectorIndex);
static void c52_errorIfDataNotWrittenToFcn(SFc52_LessonIIInstanceStruct
  *chartInstance, uint32_T c52_vectorIndex, uint32_T c52_dataNumber, uint32_T
  c52_ssIdOfSourceObject, int32_T c52_offsetInSourceObject, int32_T
  c52_lengthInSourceObject);
static void init_dsm_address_info(SFc52_LessonIIInstanceStruct *chartInstance);
static void init_simulink_io_address(SFc52_LessonIIInstanceStruct *chartInstance);

/* Function Definitions */
static void initialize_c52_LessonII(SFc52_LessonIIInstanceStruct *chartInstance)
{
  chartInstance->c52_sfEvent = CALL_EVENT;
  _sfTime_ = sf_get_time(chartInstance->S);
  chartInstance->c52_doSetSimStateSideEffects = 0U;
  chartInstance->c52_setSimStateSideEffectsInfo = NULL;
  chartInstance->c52_is_GameIsOn_Offence = c52_IN_NO_ACTIVE_CHILD;
  chartInstance->c52_tp_GameIsOn_Offence = 0U;
  chartInstance->c52_tp_Aim = 0U;
  chartInstance->c52_tp_GetToTheBall = 0U;
  chartInstance->c52_tp_Idle = 0U;
  chartInstance->c52_temporalCounter_i1 = 0U;
  chartInstance->c52_tp_Kick = 0U;
  chartInstance->c52_temporalCounter_i1 = 0U;
  chartInstance->c52_is_waiting = c52_IN_NO_ACTIVE_CHILD;
  chartInstance->c52_tp_waiting = 0U;
  chartInstance->c52_tp_Idle1 = 0U;
  chartInstance->c52_tp_goToPosition = 0U;
  chartInstance->c52_is_active_c52_LessonII = 0U;
  chartInstance->c52_is_c52_LessonII = c52_IN_NO_ACTIVE_CHILD;
}

static void initialize_params_c52_LessonII(SFc52_LessonIIInstanceStruct
  *chartInstance)
{
  (void)chartInstance;
}

static void enable_c52_LessonII(SFc52_LessonIIInstanceStruct *chartInstance)
{
  _sfTime_ = sf_get_time(chartInstance->S);
}

static void disable_c52_LessonII(SFc52_LessonIIInstanceStruct *chartInstance)
{
  _sfTime_ = sf_get_time(chartInstance->S);
}

static void c52_update_debugger_state_c52_LessonII(SFc52_LessonIIInstanceStruct *
  chartInstance)
{
  uint32_T c52_prevAniVal;
  c52_prevAniVal = _SFD_GET_ANIMATION();
  _SFD_SET_ANIMATION(0U);
  _SFD_SET_HONOR_BREAKPOINTS(0U);
  if (chartInstance->c52_is_active_c52_LessonII == 1U) {
    _SFD_CC_CALL(CHART_ACTIVE_TAG, 37U, chartInstance->c52_sfEvent);
  }

  if (chartInstance->c52_is_c52_LessonII == c52_IN_GameIsOn_Offence) {
    _SFD_CS_CALL(STATE_ACTIVE_TAG, 0U, chartInstance->c52_sfEvent);
  } else {
    _SFD_CS_CALL(STATE_INACTIVE_TAG, 0U, chartInstance->c52_sfEvent);
  }

  if (chartInstance->c52_is_GameIsOn_Offence == c52_IN_GetToTheBall) {
    _SFD_CS_CALL(STATE_ACTIVE_TAG, 2U, chartInstance->c52_sfEvent);
  } else {
    _SFD_CS_CALL(STATE_INACTIVE_TAG, 2U, chartInstance->c52_sfEvent);
  }

  if (chartInstance->c52_is_GameIsOn_Offence == c52_IN_Idle) {
    _SFD_CS_CALL(STATE_ACTIVE_TAG, 3U, chartInstance->c52_sfEvent);
  } else {
    _SFD_CS_CALL(STATE_INACTIVE_TAG, 3U, chartInstance->c52_sfEvent);
  }

  if (chartInstance->c52_is_GameIsOn_Offence == c52_IN_Aim) {
    _SFD_CS_CALL(STATE_ACTIVE_TAG, 1U, chartInstance->c52_sfEvent);
  } else {
    _SFD_CS_CALL(STATE_INACTIVE_TAG, 1U, chartInstance->c52_sfEvent);
  }

  if (chartInstance->c52_is_GameIsOn_Offence == c52_IN_Kick) {
    _SFD_CS_CALL(STATE_ACTIVE_TAG, 4U, chartInstance->c52_sfEvent);
  } else {
    _SFD_CS_CALL(STATE_INACTIVE_TAG, 4U, chartInstance->c52_sfEvent);
  }

  if (chartInstance->c52_is_c52_LessonII == c52_IN_waiting) {
    _SFD_CS_CALL(STATE_ACTIVE_TAG, 9U, chartInstance->c52_sfEvent);
  } else {
    _SFD_CS_CALL(STATE_INACTIVE_TAG, 9U, chartInstance->c52_sfEvent);
  }

  if (chartInstance->c52_is_waiting == c52_IN_Idle1) {
    _SFD_CS_CALL(STATE_ACTIVE_TAG, 10U, chartInstance->c52_sfEvent);
  } else {
    _SFD_CS_CALL(STATE_INACTIVE_TAG, 10U, chartInstance->c52_sfEvent);
  }

  if (chartInstance->c52_is_waiting == c52_IN_goToPosition) {
    _SFD_CS_CALL(STATE_ACTIVE_TAG, 11U, chartInstance->c52_sfEvent);
  } else {
    _SFD_CS_CALL(STATE_INACTIVE_TAG, 11U, chartInstance->c52_sfEvent);
  }

  _SFD_SET_ANIMATION(c52_prevAniVal);
  _SFD_SET_HONOR_BREAKPOINTS(1U);
  _SFD_ANIMATE();
}

static const mxArray *get_sim_state_c52_LessonII(SFc52_LessonIIInstanceStruct
  *chartInstance)
{
  const mxArray *c52_st;
  const mxArray *c52_y = NULL;
  const mxArray *c52_b_y = NULL;
  int8_T c52_u;
  const mxArray *c52_c_y = NULL;
  int8_T c52_b_u;
  const mxArray *c52_d_y = NULL;
  int16_T c52_c_u;
  const mxArray *c52_e_y = NULL;
  int32_T c52_i0;
  c52_Player c52_d_u[3];
  const mxArray *c52_f_y = NULL;
  static int32_T c52_iv0[1] = { 3 };

  int32_T c52_iv1[1];
  int32_T c52_i1;
  const c52_Player *c52_r0;
  int8_T c52_e_u;
  const mxArray *c52_g_y = NULL;
  int8_T c52_f_u;
  const mxArray *c52_h_y = NULL;
  int16_T c52_g_u;
  const mxArray *c52_i_y = NULL;
  uint8_T c52_h_u;
  const mxArray *c52_j_y = NULL;
  uint8_T c52_i_u;
  const mxArray *c52_k_y = NULL;
  uint8_T c52_j_u;
  const mxArray *c52_l_y = NULL;
  int32_T c52_i2;
  c52_Player c52_k_u[3];
  const mxArray *c52_m_y = NULL;
  int32_T c52_iv2[1];
  int32_T c52_i3;
  const c52_Player *c52_r1;
  int8_T c52_l_u;
  const mxArray *c52_n_y = NULL;
  int8_T c52_m_u;
  const mxArray *c52_o_y = NULL;
  int16_T c52_n_u;
  const mxArray *c52_p_y = NULL;
  uint8_T c52_o_u;
  const mxArray *c52_q_y = NULL;
  uint8_T c52_p_u;
  const mxArray *c52_r_y = NULL;
  uint8_T c52_q_u;
  const mxArray *c52_s_y = NULL;
  const mxArray *c52_t_y = NULL;
  int8_T c52_r_u;
  const mxArray *c52_u_y = NULL;
  int8_T c52_s_u;
  const mxArray *c52_v_y = NULL;
  int16_T c52_t_u;
  const mxArray *c52_w_y = NULL;
  const mxArray *c52_x_y = NULL;
  int8_T c52_u_u;
  const mxArray *c52_y_y = NULL;
  int8_T c52_v_u;
  const mxArray *c52_ab_y = NULL;
  int16_T c52_w_u;
  const mxArray *c52_bb_y = NULL;
  int32_T c52_i4;
  int8_T c52_x_u[2];
  const mxArray *c52_cb_y = NULL;
  uint8_T c52_hoistedGlobal;
  uint8_T c52_y_u;
  const mxArray *c52_db_y = NULL;
  uint8_T c52_b_hoistedGlobal;
  uint8_T c52_ab_u;
  const mxArray *c52_eb_y = NULL;
  uint8_T c52_c_hoistedGlobal;
  uint8_T c52_bb_u;
  const mxArray *c52_fb_y = NULL;
  uint8_T c52_d_hoistedGlobal;
  uint8_T c52_cb_u;
  const mxArray *c52_gb_y = NULL;
  uint8_T c52_e_hoistedGlobal;
  uint8_T c52_db_u;
  const mxArray *c52_hb_y = NULL;
  int32_T c52_i5;
  boolean_T c52_eb_u[7];
  const mxArray *c52_ib_y = NULL;
  c52_st = NULL;
  c52_st = NULL;
  c52_y = NULL;
  sf_mex_assign(&c52_y, sf_mex_createcellmatrix(12, 1), false);
  c52_b_y = NULL;
  sf_mex_assign(&c52_b_y, sf_mex_createstruct("structure", 2, 1, 1), false);
  c52_u = *(int8_T *)&((char_T *)chartInstance->c52_finalWay)[0];
  c52_c_y = NULL;
  sf_mex_assign(&c52_c_y, sf_mex_create("y", &c52_u, 2, 0U, 0U, 0U, 0), false);
  sf_mex_addfield(c52_b_y, c52_c_y, "x", "x", 0);
  c52_b_u = *(int8_T *)&((char_T *)chartInstance->c52_finalWay)[1];
  c52_d_y = NULL;
  sf_mex_assign(&c52_d_y, sf_mex_create("y", &c52_b_u, 2, 0U, 0U, 0U, 0), false);
  sf_mex_addfield(c52_b_y, c52_d_y, "y", "y", 0);
  c52_c_u = *(int16_T *)&((char_T *)chartInstance->c52_finalWay)[2];
  c52_e_y = NULL;
  sf_mex_assign(&c52_e_y, sf_mex_create("y", &c52_c_u, 4, 0U, 0U, 0U, 0), false);
  sf_mex_addfield(c52_b_y, c52_e_y, "orientation", "orientation", 0);
  sf_mex_setcell(c52_y, 0, c52_b_y);
  for (c52_i0 = 0; c52_i0 < 3; c52_i0++) {
    c52_d_u[c52_i0] = chartInstance->c52_enemy[c52_i0];
  }

  c52_f_y = NULL;
  c52_iv1[0] = c52_iv0[0];
  sf_mex_assign(&c52_f_y, sf_mex_createstructarray("structure", 1, c52_iv1),
                false);
  for (c52_i1 = 0; c52_i1 < 3; c52_i1++) {
    c52_r0 = &c52_d_u[c52_i1];
    c52_e_u = c52_r0->x;
    c52_g_y = NULL;
    sf_mex_assign(&c52_g_y, sf_mex_create("y", &c52_e_u, 2, 0U, 0U, 0U, 0),
                  false);
    sf_mex_addfield(c52_f_y, c52_g_y, "x", "x", c52_i1);
    c52_f_u = c52_r0->y;
    c52_h_y = NULL;
    sf_mex_assign(&c52_h_y, sf_mex_create("y", &c52_f_u, 2, 0U, 0U, 0U, 0),
                  false);
    sf_mex_addfield(c52_f_y, c52_h_y, "y", "y", c52_i1);
    c52_g_u = c52_r0->orientation;
    c52_i_y = NULL;
    sf_mex_assign(&c52_i_y, sf_mex_create("y", &c52_g_u, 4, 0U, 0U, 0U, 0),
                  false);
    sf_mex_addfield(c52_f_y, c52_i_y, "orientation", "orientation", c52_i1);
    c52_h_u = c52_r0->color;
    c52_j_y = NULL;
    sf_mex_assign(&c52_j_y, sf_mex_create("y", &c52_h_u, 3, 0U, 0U, 0U, 0),
                  false);
    sf_mex_addfield(c52_f_y, c52_j_y, "color", "color", c52_i1);
    c52_i_u = c52_r0->position;
    c52_k_y = NULL;
    sf_mex_assign(&c52_k_y, sf_mex_create("y", &c52_i_u, 3, 0U, 0U, 0U, 0),
                  false);
    sf_mex_addfield(c52_f_y, c52_k_y, "position", "position", c52_i1);
    c52_j_u = c52_r0->valid;
    c52_l_y = NULL;
    sf_mex_assign(&c52_l_y, sf_mex_create("y", &c52_j_u, 3, 0U, 0U, 0U, 0),
                  false);
    sf_mex_addfield(c52_f_y, c52_l_y, "valid", "valid", c52_i1);
  }

  sf_mex_setcell(c52_y, 1, c52_f_y);
  for (c52_i2 = 0; c52_i2 < 3; c52_i2++) {
    c52_k_u[c52_i2] = chartInstance->c52_friends[c52_i2];
  }

  c52_m_y = NULL;
  c52_iv2[0] = c52_iv0[0];
  sf_mex_assign(&c52_m_y, sf_mex_createstructarray("structure", 1, c52_iv2),
                false);
  for (c52_i3 = 0; c52_i3 < 3; c52_i3++) {
    c52_r1 = &c52_k_u[c52_i3];
    c52_l_u = c52_r1->x;
    c52_n_y = NULL;
    sf_mex_assign(&c52_n_y, sf_mex_create("y", &c52_l_u, 2, 0U, 0U, 0U, 0),
                  false);
    sf_mex_addfield(c52_m_y, c52_n_y, "x", "x", c52_i3);
    c52_m_u = c52_r1->y;
    c52_o_y = NULL;
    sf_mex_assign(&c52_o_y, sf_mex_create("y", &c52_m_u, 2, 0U, 0U, 0U, 0),
                  false);
    sf_mex_addfield(c52_m_y, c52_o_y, "y", "y", c52_i3);
    c52_n_u = c52_r1->orientation;
    c52_p_y = NULL;
    sf_mex_assign(&c52_p_y, sf_mex_create("y", &c52_n_u, 4, 0U, 0U, 0U, 0),
                  false);
    sf_mex_addfield(c52_m_y, c52_p_y, "orientation", "orientation", c52_i3);
    c52_o_u = c52_r1->color;
    c52_q_y = NULL;
    sf_mex_assign(&c52_q_y, sf_mex_create("y", &c52_o_u, 3, 0U, 0U, 0U, 0),
                  false);
    sf_mex_addfield(c52_m_y, c52_q_y, "color", "color", c52_i3);
    c52_p_u = c52_r1->position;
    c52_r_y = NULL;
    sf_mex_assign(&c52_r_y, sf_mex_create("y", &c52_p_u, 3, 0U, 0U, 0U, 0),
                  false);
    sf_mex_addfield(c52_m_y, c52_r_y, "position", "position", c52_i3);
    c52_q_u = c52_r1->valid;
    c52_s_y = NULL;
    sf_mex_assign(&c52_s_y, sf_mex_create("y", &c52_q_u, 3, 0U, 0U, 0U, 0),
                  false);
    sf_mex_addfield(c52_m_y, c52_s_y, "valid", "valid", c52_i3);
  }

  sf_mex_setcell(c52_y, 2, c52_m_y);
  c52_t_y = NULL;
  sf_mex_assign(&c52_t_y, sf_mex_createstruct("structure", 2, 1, 1), false);
  c52_r_u = chartInstance->c52_kickWay.x;
  c52_u_y = NULL;
  sf_mex_assign(&c52_u_y, sf_mex_create("y", &c52_r_u, 2, 0U, 0U, 0U, 0), false);
  sf_mex_addfield(c52_t_y, c52_u_y, "x", "x", 0);
  c52_s_u = chartInstance->c52_kickWay.y;
  c52_v_y = NULL;
  sf_mex_assign(&c52_v_y, sf_mex_create("y", &c52_s_u, 2, 0U, 0U, 0U, 0), false);
  sf_mex_addfield(c52_t_y, c52_v_y, "y", "y", 0);
  c52_t_u = chartInstance->c52_kickWay.orientation;
  c52_w_y = NULL;
  sf_mex_assign(&c52_w_y, sf_mex_create("y", &c52_t_u, 4, 0U, 0U, 0U, 0), false);
  sf_mex_addfield(c52_t_y, c52_w_y, "orientation", "orientation", 0);
  sf_mex_setcell(c52_y, 3, c52_t_y);
  c52_x_y = NULL;
  sf_mex_assign(&c52_x_y, sf_mex_createstruct("structure", 2, 1, 1), false);
  c52_u_u = chartInstance->c52_shootWay.x;
  c52_y_y = NULL;
  sf_mex_assign(&c52_y_y, sf_mex_create("y", &c52_u_u, 2, 0U, 0U, 0U, 0), false);
  sf_mex_addfield(c52_x_y, c52_y_y, "x", "x", 0);
  c52_v_u = chartInstance->c52_shootWay.y;
  c52_ab_y = NULL;
  sf_mex_assign(&c52_ab_y, sf_mex_create("y", &c52_v_u, 2, 0U, 0U, 0U, 0), false);
  sf_mex_addfield(c52_x_y, c52_ab_y, "y", "y", 0);
  c52_w_u = chartInstance->c52_shootWay.orientation;
  c52_bb_y = NULL;
  sf_mex_assign(&c52_bb_y, sf_mex_create("y", &c52_w_u, 4, 0U, 0U, 0U, 0), false);
  sf_mex_addfield(c52_x_y, c52_bb_y, "orientation", "orientation", 0);
  sf_mex_setcell(c52_y, 4, c52_x_y);
  for (c52_i4 = 0; c52_i4 < 2; c52_i4++) {
    c52_x_u[c52_i4] = chartInstance->c52_startingPos[c52_i4];
  }

  c52_cb_y = NULL;
  sf_mex_assign(&c52_cb_y, sf_mex_create("y", c52_x_u, 2, 0U, 1U, 0U, 2, 2, 1),
                false);
  sf_mex_setcell(c52_y, 5, c52_cb_y);
  c52_hoistedGlobal = chartInstance->c52_is_active_c52_LessonII;
  c52_y_u = c52_hoistedGlobal;
  c52_db_y = NULL;
  sf_mex_assign(&c52_db_y, sf_mex_create("y", &c52_y_u, 3, 0U, 0U, 0U, 0), false);
  sf_mex_setcell(c52_y, 6, c52_db_y);
  c52_b_hoistedGlobal = chartInstance->c52_is_c52_LessonII;
  c52_ab_u = c52_b_hoistedGlobal;
  c52_eb_y = NULL;
  sf_mex_assign(&c52_eb_y, sf_mex_create("y", &c52_ab_u, 3, 0U, 0U, 0U, 0),
                false);
  sf_mex_setcell(c52_y, 7, c52_eb_y);
  c52_c_hoistedGlobal = chartInstance->c52_is_GameIsOn_Offence;
  c52_bb_u = c52_c_hoistedGlobal;
  c52_fb_y = NULL;
  sf_mex_assign(&c52_fb_y, sf_mex_create("y", &c52_bb_u, 3, 0U, 0U, 0U, 0),
                false);
  sf_mex_setcell(c52_y, 8, c52_fb_y);
  c52_d_hoistedGlobal = chartInstance->c52_is_waiting;
  c52_cb_u = c52_d_hoistedGlobal;
  c52_gb_y = NULL;
  sf_mex_assign(&c52_gb_y, sf_mex_create("y", &c52_cb_u, 3, 0U, 0U, 0U, 0),
                false);
  sf_mex_setcell(c52_y, 9, c52_gb_y);
  c52_e_hoistedGlobal = chartInstance->c52_temporalCounter_i1;
  c52_db_u = c52_e_hoistedGlobal;
  c52_hb_y = NULL;
  sf_mex_assign(&c52_hb_y, sf_mex_create("y", &c52_db_u, 3, 0U, 0U, 0U, 0),
                false);
  sf_mex_setcell(c52_y, 10, c52_hb_y);
  for (c52_i5 = 0; c52_i5 < 7; c52_i5++) {
    c52_eb_u[c52_i5] = chartInstance->c52_dataWrittenToVector[c52_i5];
  }

  c52_ib_y = NULL;
  sf_mex_assign(&c52_ib_y, sf_mex_create("y", c52_eb_u, 11, 0U, 1U, 0U, 1, 7),
                false);
  sf_mex_setcell(c52_y, 11, c52_ib_y);
  sf_mex_assign(&c52_st, c52_y, false);
  return c52_st;
}

static void set_sim_state_c52_LessonII(SFc52_LessonIIInstanceStruct
  *chartInstance, const mxArray *c52_st)
{
  const mxArray *c52_u;
  c52_Waypoint c52_r2;
  c52_Player c52_rv0[3];
  int32_T c52_i6;
  c52_Player c52_rv1[3];
  int32_T c52_i7;
  int8_T c52_iv3[2];
  int32_T c52_i8;
  boolean_T c52_bv0[7];
  int32_T c52_i9;
  c52_u = sf_mex_dup(c52_st);
  c52_r2 = c52_h_emlrt_marshallIn(chartInstance, sf_mex_dup(sf_mex_getcell(c52_u,
    0)), "finalWay");
  *(int8_T *)&((char_T *)chartInstance->c52_finalWay)[0] = c52_r2.x;
  *(int8_T *)&((char_T *)chartInstance->c52_finalWay)[1] = c52_r2.y;
  *(int16_T *)&((char_T *)chartInstance->c52_finalWay)[2] = c52_r2.orientation;
  c52_n_emlrt_marshallIn(chartInstance, sf_mex_dup(sf_mex_getcell(c52_u, 1)),
    "enemy", c52_rv0);
  for (c52_i6 = 0; c52_i6 < 3; c52_i6++) {
    chartInstance->c52_enemy[c52_i6] = c52_rv0[c52_i6];
  }

  c52_n_emlrt_marshallIn(chartInstance, sf_mex_dup(sf_mex_getcell(c52_u, 2)),
    "friends", c52_rv1);
  for (c52_i7 = 0; c52_i7 < 3; c52_i7++) {
    chartInstance->c52_friends[c52_i7] = c52_rv1[c52_i7];
  }

  chartInstance->c52_kickWay = c52_h_emlrt_marshallIn(chartInstance, sf_mex_dup
    (sf_mex_getcell(c52_u, 3)), "kickWay");
  chartInstance->c52_shootWay = c52_h_emlrt_marshallIn(chartInstance, sf_mex_dup
    (sf_mex_getcell(c52_u, 4)), "shootWay");
  c52_l_emlrt_marshallIn(chartInstance, sf_mex_dup(sf_mex_getcell(c52_u, 5)),
    "startingPos", c52_iv3);
  for (c52_i8 = 0; c52_i8 < 2; c52_i8++) {
    chartInstance->c52_startingPos[c52_i8] = c52_iv3[c52_i8];
  }

  chartInstance->c52_is_active_c52_LessonII = c52_d_emlrt_marshallIn
    (chartInstance, sf_mex_dup(sf_mex_getcell(c52_u, 6)),
     "is_active_c52_LessonII");
  chartInstance->c52_is_c52_LessonII = c52_d_emlrt_marshallIn(chartInstance,
    sf_mex_dup(sf_mex_getcell(c52_u, 7)), "is_c52_LessonII");
  chartInstance->c52_is_GameIsOn_Offence = c52_d_emlrt_marshallIn(chartInstance,
    sf_mex_dup(sf_mex_getcell(c52_u, 8)), "is_GameIsOn_Offence");
  chartInstance->c52_is_waiting = c52_d_emlrt_marshallIn(chartInstance,
    sf_mex_dup(sf_mex_getcell(c52_u, 9)), "is_waiting");
  chartInstance->c52_temporalCounter_i1 = c52_d_emlrt_marshallIn(chartInstance,
    sf_mex_dup(sf_mex_getcell(c52_u, 10)), "temporalCounter_i1");
  c52_p_emlrt_marshallIn(chartInstance, sf_mex_dup(sf_mex_getcell(c52_u, 11)),
    "dataWrittenToVector", c52_bv0);
  for (c52_i9 = 0; c52_i9 < 7; c52_i9++) {
    chartInstance->c52_dataWrittenToVector[c52_i9] = c52_bv0[c52_i9];
  }

  sf_mex_assign(&chartInstance->c52_setSimStateSideEffectsInfo,
                c52_r_emlrt_marshallIn(chartInstance, sf_mex_dup(sf_mex_getcell
    (c52_u, 12)), "setSimStateSideEffectsInfo"), true);
  sf_mex_destroy(&c52_u);
  chartInstance->c52_doSetSimStateSideEffects = 1U;
  c52_update_debugger_state_c52_LessonII(chartInstance);
  sf_mex_destroy(&c52_st);
}

static void c52_set_sim_state_side_effects_c52_LessonII
  (SFc52_LessonIIInstanceStruct *chartInstance)
{
  if (chartInstance->c52_doSetSimStateSideEffects != 0) {
    if (chartInstance->c52_is_c52_LessonII == c52_IN_GameIsOn_Offence) {
      chartInstance->c52_tp_GameIsOn_Offence = 1U;
    } else {
      chartInstance->c52_tp_GameIsOn_Offence = 0U;
    }

    if (chartInstance->c52_is_GameIsOn_Offence == c52_IN_Aim) {
      chartInstance->c52_tp_Aim = 1U;
    } else {
      chartInstance->c52_tp_Aim = 0U;
    }

    if (chartInstance->c52_is_GameIsOn_Offence == c52_IN_GetToTheBall) {
      chartInstance->c52_tp_GetToTheBall = 1U;
    } else {
      chartInstance->c52_tp_GetToTheBall = 0U;
    }

    if (chartInstance->c52_is_GameIsOn_Offence == c52_IN_Idle) {
      chartInstance->c52_tp_Idle = 1U;
      if (sf_mex_sub(chartInstance->c52_setSimStateSideEffectsInfo,
                     "setSimStateSideEffectsInfo", 1, 5) == 0.0) {
        chartInstance->c52_temporalCounter_i1 = 0U;
      }
    } else {
      chartInstance->c52_tp_Idle = 0U;
    }

    if (chartInstance->c52_is_GameIsOn_Offence == c52_IN_Kick) {
      chartInstance->c52_tp_Kick = 1U;
      if (sf_mex_sub(chartInstance->c52_setSimStateSideEffectsInfo,
                     "setSimStateSideEffectsInfo", 1, 6) == 0.0) {
        chartInstance->c52_temporalCounter_i1 = 0U;
      }
    } else {
      chartInstance->c52_tp_Kick = 0U;
    }

    if (chartInstance->c52_is_c52_LessonII == c52_IN_waiting) {
      chartInstance->c52_tp_waiting = 1U;
    } else {
      chartInstance->c52_tp_waiting = 0U;
    }

    if (chartInstance->c52_is_waiting == c52_IN_Idle1) {
      chartInstance->c52_tp_Idle1 = 1U;
    } else {
      chartInstance->c52_tp_Idle1 = 0U;
    }

    if (chartInstance->c52_is_waiting == c52_IN_goToPosition) {
      chartInstance->c52_tp_goToPosition = 1U;
    } else {
      chartInstance->c52_tp_goToPosition = 0U;
    }

    chartInstance->c52_doSetSimStateSideEffects = 0U;
  }
}

static void finalize_c52_LessonII(SFc52_LessonIIInstanceStruct *chartInstance)
{
  sf_mex_destroy(&chartInstance->c52_setSimStateSideEffectsInfo);
}

static void sf_gateway_c52_LessonII(SFc52_LessonIIInstanceStruct *chartInstance)
{
  int32_T c52_i10;
  uint32_T c52_debug_family_var_map[2];
  real_T c52_nargin = 0.0;
  real_T c52_nargout = 0.0;
  uint32_T c52_b_debug_family_var_map[3];
  real_T c52_b_nargin = 0.0;
  real_T c52_b_nargout = 1.0;
  boolean_T c52_out;
  real_T c52_c_nargin = 0.0;
  real_T c52_c_nargout = 0.0;
  real_T c52_d_nargin = 0.0;
  real_T c52_d_nargout = 0.0;
  real_T c52_e_nargin = 0.0;
  real_T c52_e_nargout = 1.0;
  boolean_T c52_b_out;
  int32_T c52_i11;
  int8_T c52_iv4[2];
  uint8_T c52_u0;
  real_T c52_f_nargin = 0.0;
  real_T c52_f_nargout = 0.0;
  c52_set_sim_state_side_effects_c52_LessonII(chartInstance);
  _SFD_SYMBOL_SCOPE_PUSH(0U, 0U);
  _sfTime_ = sf_get_time(chartInstance->S);
  _SFD_CC_CALL(CHART_ENTER_SFUNCTION_TAG, 37U, chartInstance->c52_sfEvent);
  for (c52_i10 = 0; c52_i10 < 2; c52_i10++) {
    _SFD_DATA_RANGE_CHECK((real_T)chartInstance->c52_startingPos[c52_i10], 5U);
  }

  _SFD_DATA_RANGE_CHECK((real_T)*chartInstance->c52_GameOn, 6U);
  chartInstance->c52_sfEvent = CALL_EVENT;
  if (chartInstance->c52_temporalCounter_i1 < 15U) {
    chartInstance->c52_temporalCounter_i1++;
  }

  _SFD_CC_CALL(CHART_ENTER_DURING_FUNCTION_TAG, 37U, chartInstance->c52_sfEvent);
  if (chartInstance->c52_is_active_c52_LessonII == 0U) {
    _SFD_CC_CALL(CHART_ENTER_ENTRY_FUNCTION_TAG, 37U, chartInstance->c52_sfEvent);
    chartInstance->c52_is_active_c52_LessonII = 1U;
    _SFD_CC_CALL(EXIT_OUT_OF_FUNCTION_TAG, 37U, chartInstance->c52_sfEvent);
    _SFD_CT_CALL(TRANSITION_ACTIVE_TAG, 3U, chartInstance->c52_sfEvent);
    _SFD_SYMBOL_SCOPE_PUSH_EML(0U, 2U, 2U, c52_r_debug_family_names,
      c52_debug_family_var_map);
    _SFD_SYMBOL_SCOPE_ADD_EML_IMPORTABLE(&c52_nargin, 0U, c52_sf_marshallOut,
      c52_sf_marshallIn);
    _SFD_SYMBOL_SCOPE_ADD_EML_IMPORTABLE(&c52_nargout, 1U, c52_sf_marshallOut,
      c52_sf_marshallIn);
    c52_calcStartPos(chartInstance);
    c52_calcStartTeams(chartInstance);
    _SFD_SYMBOL_SCOPE_POP();
    chartInstance->c52_is_c52_LessonII = c52_IN_waiting;
    _SFD_CS_CALL(STATE_ACTIVE_TAG, 9U, chartInstance->c52_sfEvent);
    chartInstance->c52_tp_waiting = 1U;
    _SFD_CT_CALL(TRANSITION_ACTIVE_TAG, 2U, chartInstance->c52_sfEvent);
    chartInstance->c52_is_waiting = c52_IN_goToPosition;
    _SFD_CS_CALL(STATE_ACTIVE_TAG, 11U, chartInstance->c52_sfEvent);
    chartInstance->c52_tp_goToPosition = 1U;
  } else {
    switch (chartInstance->c52_is_c52_LessonII) {
     case c52_IN_GameIsOn_Offence:
      CV_CHART_EVAL(37, 0, 1);
      c52_GameIsOn_Offence(chartInstance);
      break;

     case c52_IN_waiting:
      CV_CHART_EVAL(37, 0, 2);
      _SFD_CT_CALL(TRANSITION_BEFORE_PROCESSING_TAG, 0U,
                   chartInstance->c52_sfEvent);
      _SFD_SYMBOL_SCOPE_PUSH_EML(0U, 3U, 3U, c52_s_debug_family_names,
        c52_b_debug_family_var_map);
      _SFD_SYMBOL_SCOPE_ADD_EML_IMPORTABLE(&c52_b_nargin, 0U, c52_sf_marshallOut,
        c52_sf_marshallIn);
      _SFD_SYMBOL_SCOPE_ADD_EML_IMPORTABLE(&c52_b_nargout, 1U,
        c52_sf_marshallOut, c52_sf_marshallIn);
      _SFD_SYMBOL_SCOPE_ADD_EML_IMPORTABLE(&c52_out, 2U, c52_c_sf_marshallOut,
        c52_c_sf_marshallIn);
      c52_out = CV_EML_IF(0, 0, 0, CV_RELATIONAL_EVAL(5U, 0U, 0, (real_T)
        *chartInstance->c52_GameOn, 1.0, 0, 0U, *chartInstance->c52_GameOn == 1));
      _SFD_SYMBOL_SCOPE_POP();
      if (c52_out) {
        _SFD_CT_CALL(TRANSITION_ACTIVE_TAG, 0U, chartInstance->c52_sfEvent);
        c52_exit_internal_waiting(chartInstance);
        chartInstance->c52_tp_waiting = 0U;
        _SFD_CS_CALL(STATE_INACTIVE_TAG, 9U, chartInstance->c52_sfEvent);
        chartInstance->c52_is_c52_LessonII = c52_IN_GameIsOn_Offence;
        _SFD_CS_CALL(STATE_ACTIVE_TAG, 0U, chartInstance->c52_sfEvent);
        chartInstance->c52_tp_GameIsOn_Offence = 1U;
        _SFD_CT_CALL(TRANSITION_ACTIVE_TAG, 5U, chartInstance->c52_sfEvent);
        chartInstance->c52_is_GameIsOn_Offence = c52_IN_GetToTheBall;
        _SFD_CS_CALL(STATE_ACTIVE_TAG, 2U, chartInstance->c52_sfEvent);
        chartInstance->c52_tp_GetToTheBall = 1U;
        _SFD_SYMBOL_SCOPE_PUSH_EML(0U, 2U, 2U, c52_d_debug_family_names,
          c52_debug_family_var_map);
        _SFD_SYMBOL_SCOPE_ADD_EML_IMPORTABLE(&c52_c_nargin, 0U,
          c52_sf_marshallOut, c52_sf_marshallIn);
        _SFD_SYMBOL_SCOPE_ADD_EML_IMPORTABLE(&c52_c_nargout, 1U,
          c52_sf_marshallOut, c52_sf_marshallIn);
        c52_calcShootWay(chartInstance);
        _SFD_SYMBOL_SCOPE_POP();
      } else {
        _SFD_CS_CALL(STATE_ENTER_DURING_FUNCTION_TAG, 9U,
                     chartInstance->c52_sfEvent);
        switch (chartInstance->c52_is_waiting) {
         case c52_IN_Idle1:
          CV_STATE_EVAL(9, 0, 1);
          _SFD_CS_CALL(STATE_ENTER_DURING_FUNCTION_TAG, 10U,
                       chartInstance->c52_sfEvent);
          _SFD_SYMBOL_SCOPE_PUSH_EML(0U, 2U, 2U, c52_i_debug_family_names,
            c52_debug_family_var_map);
          _SFD_SYMBOL_SCOPE_ADD_EML_IMPORTABLE(&c52_d_nargin, 0U,
            c52_sf_marshallOut, c52_sf_marshallIn);
          _SFD_SYMBOL_SCOPE_ADD_EML_IMPORTABLE(&c52_d_nargout, 1U,
            c52_sf_marshallOut, c52_sf_marshallIn);
          *(int8_T *)&((char_T *)chartInstance->c52_finalWay)[0] = *(int8_T *)
            &((char_T *)chartInstance->c52_me)[0];
          c52_updateDataWrittenToVector(chartInstance, 0U);
          *(int8_T *)&((char_T *)chartInstance->c52_finalWay)[1] = *(int8_T *)
            &((char_T *)chartInstance->c52_me)[1];
          c52_updateDataWrittenToVector(chartInstance, 0U);
          *(int16_T *)&((char_T *)chartInstance->c52_finalWay)[2] = *(int16_T *)
            &((char_T *)chartInstance->c52_me)[2];
          c52_updateDataWrittenToVector(chartInstance, 0U);
          _SFD_SYMBOL_SCOPE_POP();
          _SFD_CS_CALL(EXIT_OUT_OF_FUNCTION_TAG, 10U, chartInstance->c52_sfEvent);
          break;

         case c52_IN_goToPosition:
          CV_STATE_EVAL(9, 0, 2);
          _SFD_CT_CALL(TRANSITION_BEFORE_PROCESSING_TAG, 4U,
                       chartInstance->c52_sfEvent);
          _SFD_SYMBOL_SCOPE_PUSH_EML(0U, 3U, 3U, c52_p_debug_family_names,
            c52_b_debug_family_var_map);
          _SFD_SYMBOL_SCOPE_ADD_EML_IMPORTABLE(&c52_e_nargin, 0U,
            c52_sf_marshallOut, c52_sf_marshallIn);
          _SFD_SYMBOL_SCOPE_ADD_EML_IMPORTABLE(&c52_e_nargout, 1U,
            c52_sf_marshallOut, c52_sf_marshallIn);
          _SFD_SYMBOL_SCOPE_ADD_EML_IMPORTABLE(&c52_b_out, 2U,
            c52_c_sf_marshallOut, c52_c_sf_marshallIn);
          c52_errorIfDataNotWrittenToFcn(chartInstance, 1U, 5U, 101U, 14, 11);
          for (c52_i11 = 0; c52_i11 < 2; c52_i11++) {
            c52_iv4[c52_i11] = chartInstance->c52_startingPos[c52_i11];
          }

          c52_u0 = c52_b_checkReached(chartInstance, c52_iv4, 3.0);
          c52_b_out = CV_EML_IF(4, 0, 0, CV_RELATIONAL_EVAL(5U, 4U, 0, (real_T)
            c52_u0, 1.0, 0, 0U, c52_u0 == 1));
          _SFD_SYMBOL_SCOPE_POP();
          if (c52_b_out) {
            _SFD_CT_CALL(TRANSITION_ACTIVE_TAG, 4U, chartInstance->c52_sfEvent);
            chartInstance->c52_tp_goToPosition = 0U;
            _SFD_CS_CALL(STATE_INACTIVE_TAG, 11U, chartInstance->c52_sfEvent);
            chartInstance->c52_is_waiting = c52_IN_Idle1;
            _SFD_CS_CALL(STATE_ACTIVE_TAG, 10U, chartInstance->c52_sfEvent);
            chartInstance->c52_tp_Idle1 = 1U;
          } else {
            _SFD_CS_CALL(STATE_ENTER_DURING_FUNCTION_TAG, 11U,
                         chartInstance->c52_sfEvent);
            _SFD_SYMBOL_SCOPE_PUSH_EML(0U, 2U, 2U, c52_j_debug_family_names,
              c52_debug_family_var_map);
            _SFD_SYMBOL_SCOPE_ADD_EML_IMPORTABLE(&c52_f_nargin, 0U,
              c52_sf_marshallOut, c52_sf_marshallIn);
            _SFD_SYMBOL_SCOPE_ADD_EML_IMPORTABLE(&c52_f_nargout, 1U,
              c52_sf_marshallOut, c52_sf_marshallIn);
            c52_errorIfDataNotWrittenToFcn(chartInstance, 1U, 5U, 92U, 29, 11);
            *(int8_T *)&((char_T *)chartInstance->c52_finalWay)[0] =
              chartInstance->c52_startingPos[0];
            c52_updateDataWrittenToVector(chartInstance, 0U);
            c52_errorIfDataNotWrittenToFcn(chartInstance, 1U, 5U, 92U, 56, 11);
            *(int8_T *)&((char_T *)chartInstance->c52_finalWay)[1] =
              chartInstance->c52_startingPos[1];
            c52_updateDataWrittenToVector(chartInstance, 0U);
            *(int16_T *)&((char_T *)chartInstance->c52_finalWay)[2] = c52_intmax
              (chartInstance);
            c52_updateDataWrittenToVector(chartInstance, 0U);
            _SFD_SYMBOL_SCOPE_POP();
          }

          _SFD_CS_CALL(EXIT_OUT_OF_FUNCTION_TAG, 11U, chartInstance->c52_sfEvent);
          break;

         default:
          CV_STATE_EVAL(9, 0, 0);
          chartInstance->c52_is_waiting = c52_IN_NO_ACTIVE_CHILD;
          _SFD_CS_CALL(STATE_INACTIVE_TAG, 10U, chartInstance->c52_sfEvent);
          break;
        }
      }

      _SFD_CS_CALL(EXIT_OUT_OF_FUNCTION_TAG, 9U, chartInstance->c52_sfEvent);
      break;

     default:
      CV_CHART_EVAL(37, 0, 0);
      chartInstance->c52_is_c52_LessonII = c52_IN_NO_ACTIVE_CHILD;
      _SFD_CS_CALL(STATE_INACTIVE_TAG, 0U, chartInstance->c52_sfEvent);
      break;
    }
  }

  _SFD_CC_CALL(EXIT_OUT_OF_FUNCTION_TAG, 37U, chartInstance->c52_sfEvent);
  _SFD_SYMBOL_SCOPE_POP();
  _SFD_CHECK_FOR_STATE_INCONSISTENCY(_LessonIIMachineNumber_,
    chartInstance->chartNumber, chartInstance->instanceNumber);
}

static void mdl_start_c52_LessonII(SFc52_LessonIIInstanceStruct *chartInstance)
{
  (void)chartInstance;
}

static void initSimStructsc52_LessonII(SFc52_LessonIIInstanceStruct
  *chartInstance)
{
  (void)chartInstance;
}

static void c52_GameIsOn_Offence(SFc52_LessonIIInstanceStruct *chartInstance)
{
  uint32_T c52_debug_family_var_map[3];
  real_T c52_nargin = 0.0;
  real_T c52_nargout = 1.0;
  boolean_T c52_out;
  real_T c52_b_nargin = 0.0;
  real_T c52_b_nargout = 1.0;
  boolean_T c52_b_out;
  int32_T c52_i12;
  int16_T c52_x;
  int16_T c52_b_x;
  int32_T c52_i13;
  int16_T c52_y;
  real_T c52_d0;
  uint32_T c52_b_debug_family_var_map[2];
  real_T c52_c_nargin = 0.0;
  real_T c52_c_nargout = 0.0;
  real_T c52_d_nargin = 0.0;
  real_T c52_d_nargout = 0.0;
  real_T c52_e_nargin = 0.0;
  real_T c52_e_nargout = 1.0;
  boolean_T c52_c_out;
  int8_T c52_iv5[2];
  uint8_T c52_u1;
  real_T c52_f_nargin = 0.0;
  real_T c52_f_nargout = 0.0;
  real_T c52_g_nargin = 0.0;
  real_T c52_g_nargout = 1.0;
  boolean_T c52_d_out;
  real_T c52_h_nargin = 0.0;
  real_T c52_h_nargout = 0.0;
  real_T c52_i_nargin = 0.0;
  real_T c52_i_nargout = 0.0;
  real_T c52_j_nargin = 0.0;
  real_T c52_j_nargout = 1.0;
  boolean_T c52_e_out;
  int8_T c52_iv6[2];
  uint8_T c52_u2;
  boolean_T guard1 = false;
  _SFD_CT_CALL(TRANSITION_BEFORE_PROCESSING_TAG, 1U, chartInstance->c52_sfEvent);
  _SFD_SYMBOL_SCOPE_PUSH_EML(0U, 3U, 3U, c52_t_debug_family_names,
    c52_debug_family_var_map);
  _SFD_SYMBOL_SCOPE_ADD_EML_IMPORTABLE(&c52_nargin, 0U, c52_sf_marshallOut,
    c52_sf_marshallIn);
  _SFD_SYMBOL_SCOPE_ADD_EML_IMPORTABLE(&c52_nargout, 1U, c52_sf_marshallOut,
    c52_sf_marshallIn);
  _SFD_SYMBOL_SCOPE_ADD_EML_IMPORTABLE(&c52_out, 2U, c52_c_sf_marshallOut,
    c52_c_sf_marshallIn);
  c52_out = CV_EML_IF(1, 0, 0, CV_RELATIONAL_EVAL(5U, 1U, 0, (real_T)
    *chartInstance->c52_GameOn, 0.0, 0, 0U, *chartInstance->c52_GameOn == 0));
  _SFD_SYMBOL_SCOPE_POP();
  if (c52_out) {
    _SFD_CT_CALL(TRANSITION_ACTIVE_TAG, 1U, chartInstance->c52_sfEvent);
    c52_exit_internal_GameIsOn_Offence(chartInstance);
    chartInstance->c52_tp_GameIsOn_Offence = 0U;
    _SFD_CS_CALL(STATE_INACTIVE_TAG, 0U, chartInstance->c52_sfEvent);
    chartInstance->c52_is_c52_LessonII = c52_IN_waiting;
    _SFD_CS_CALL(STATE_ACTIVE_TAG, 9U, chartInstance->c52_sfEvent);
    chartInstance->c52_tp_waiting = 1U;
    _SFD_CT_CALL(TRANSITION_ACTIVE_TAG, 2U, chartInstance->c52_sfEvent);
    chartInstance->c52_is_waiting = c52_IN_goToPosition;
    _SFD_CS_CALL(STATE_ACTIVE_TAG, 11U, chartInstance->c52_sfEvent);
    chartInstance->c52_tp_goToPosition = 1U;
  } else {
    _SFD_CS_CALL(STATE_ENTER_DURING_FUNCTION_TAG, 0U, chartInstance->c52_sfEvent);
    switch (chartInstance->c52_is_GameIsOn_Offence) {
     case c52_IN_Aim:
      CV_STATE_EVAL(0, 0, 1);
      _SFD_CT_CALL(TRANSITION_BEFORE_PROCESSING_TAG, 8U,
                   chartInstance->c52_sfEvent);
      _SFD_SYMBOL_SCOPE_PUSH_EML(0U, 3U, 3U, c52_o_debug_family_names,
        c52_debug_family_var_map);
      _SFD_SYMBOL_SCOPE_ADD_EML_IMPORTABLE(&c52_b_nargin, 0U, c52_sf_marshallOut,
        c52_sf_marshallIn);
      _SFD_SYMBOL_SCOPE_ADD_EML_IMPORTABLE(&c52_b_nargout, 1U,
        c52_sf_marshallOut, c52_sf_marshallIn);
      _SFD_SYMBOL_SCOPE_ADD_EML_IMPORTABLE(&c52_b_out, 2U, c52_c_sf_marshallOut,
        c52_c_sf_marshallIn);
      c52_errorIfDataNotWrittenToFcn(chartInstance, 0U, 3U, 121U, 22, 20);
      c52_i12 = *(int16_T *)&((char_T *)chartInstance->c52_me)[2] - *(int16_T *)
        &((char_T *)chartInstance->c52_finalWay)[2];
      if (c52_i12 > 32767) {
        CV_SATURATION_EVAL(5, 8, 1, 0, 1);
        c52_i12 = 32767;
      } else {
        if (CV_SATURATION_EVAL(5, 8, 1, 0, c52_i12 < -32768)) {
          c52_i12 = -32768;
        }
      }

      c52_x = (int16_T)c52_i12;
      c52_b_x = c52_x;
      c52_i13 = -c52_b_x;
      if (c52_i13 > 32767) {
        CV_SATURATION_EVAL(5, 8, 0, 0, 1);
        c52_i13 = 32767;
      } else {
        if (CV_SATURATION_EVAL(5, 8, 0, 0, c52_i13 < -32768)) {
          c52_i13 = -32768;
        }
      }

      if ((real_T)c52_b_x < 0.0) {
        c52_y = (int16_T)c52_i13;
      } else {
        c52_y = c52_b_x;
      }

      c52_d0 = (real_T)c52_y;
      c52_b_out = CV_EML_IF(8, 0, 0, CV_RELATIONAL_EVAL(5U, 8U, 0, c52_d0, 5.0,
        -1, 2U, c52_d0 < 5.0));
      _SFD_SYMBOL_SCOPE_POP();
      if (c52_b_out) {
        _SFD_CT_CALL(TRANSITION_ACTIVE_TAG, 8U, chartInstance->c52_sfEvent);
        chartInstance->c52_tp_Aim = 0U;
        _SFD_CS_CALL(STATE_INACTIVE_TAG, 1U, chartInstance->c52_sfEvent);
        chartInstance->c52_is_GameIsOn_Offence = c52_IN_Kick;
        _SFD_CS_CALL(STATE_ACTIVE_TAG, 4U, chartInstance->c52_sfEvent);
        chartInstance->c52_temporalCounter_i1 = 0U;
        chartInstance->c52_tp_Kick = 1U;
        _SFD_SYMBOL_SCOPE_PUSH_EML(0U, 2U, 2U, c52_h_debug_family_names,
          c52_b_debug_family_var_map);
        _SFD_SYMBOL_SCOPE_ADD_EML_IMPORTABLE(&c52_c_nargin, 0U,
          c52_sf_marshallOut, c52_sf_marshallIn);
        _SFD_SYMBOL_SCOPE_ADD_EML_IMPORTABLE(&c52_c_nargout, 1U,
          c52_sf_marshallOut, c52_sf_marshallIn);
        *(int8_T *)&((char_T *)chartInstance->c52_finalWay)[0] = *(int8_T *)
          &((char_T *)chartInstance->c52_ball)[0];
        c52_updateDataWrittenToVector(chartInstance, 0U);
        *(int8_T *)&((char_T *)chartInstance->c52_finalWay)[1] = *(int8_T *)
          &((char_T *)chartInstance->c52_ball)[1];
        c52_updateDataWrittenToVector(chartInstance, 0U);
        *(int16_T *)&((char_T *)chartInstance->c52_finalWay)[2] = c52_intmax
          (chartInstance);
        c52_updateDataWrittenToVector(chartInstance, 0U);
        _SFD_SYMBOL_SCOPE_POP();
      } else {
        _SFD_CS_CALL(STATE_ENTER_DURING_FUNCTION_TAG, 1U,
                     chartInstance->c52_sfEvent);
        _SFD_SYMBOL_SCOPE_PUSH_EML(0U, 2U, 2U, c52_g_debug_family_names,
          c52_b_debug_family_var_map);
        _SFD_SYMBOL_SCOPE_ADD_EML_IMPORTABLE(&c52_d_nargin, 0U,
          c52_sf_marshallOut, c52_sf_marshallIn);
        _SFD_SYMBOL_SCOPE_ADD_EML_IMPORTABLE(&c52_d_nargout, 1U,
          c52_sf_marshallOut, c52_sf_marshallIn);
        c52_calcShootWay(chartInstance);
        *(int8_T *)&((char_T *)chartInstance->c52_finalWay)[0] = *(int8_T *)
          &((char_T *)chartInstance->c52_me)[0];
        c52_updateDataWrittenToVector(chartInstance, 0U);
        *(int8_T *)&((char_T *)chartInstance->c52_finalWay)[1] = *(int8_T *)
          &((char_T *)chartInstance->c52_me)[1];
        c52_updateDataWrittenToVector(chartInstance, 0U);
        c52_errorIfDataNotWrittenToFcn(chartInstance, 4U, 9U, 115U, 78, 20);
        *(int16_T *)&((char_T *)chartInstance->c52_finalWay)[2] =
          chartInstance->c52_shootWay.orientation;
        c52_updateDataWrittenToVector(chartInstance, 0U);
        _SFD_SYMBOL_SCOPE_POP();
      }

      _SFD_CS_CALL(EXIT_OUT_OF_FUNCTION_TAG, 1U, chartInstance->c52_sfEvent);
      break;

     case c52_IN_GetToTheBall:
      CV_STATE_EVAL(0, 0, 2);
      _SFD_CT_CALL(TRANSITION_BEFORE_PROCESSING_TAG, 6U,
                   chartInstance->c52_sfEvent);
      _SFD_SYMBOL_SCOPE_PUSH_EML(0U, 3U, 3U, c52_n_debug_family_names,
        c52_debug_family_var_map);
      _SFD_SYMBOL_SCOPE_ADD_EML_IMPORTABLE(&c52_e_nargin, 0U, c52_sf_marshallOut,
        c52_sf_marshallIn);
      _SFD_SYMBOL_SCOPE_ADD_EML_IMPORTABLE(&c52_e_nargout, 1U,
        c52_sf_marshallOut, c52_sf_marshallIn);
      _SFD_SYMBOL_SCOPE_ADD_EML_IMPORTABLE(&c52_c_out, 2U, c52_c_sf_marshallOut,
        c52_c_sf_marshallIn);
      c52_errorIfDataNotWrittenToFcn(chartInstance, 4U, 9U, 116U, 15, 10);
      c52_iv5[0] = chartInstance->c52_shootWay.x;
      c52_iv5[1] = chartInstance->c52_shootWay.y;
      c52_u1 = c52_checkReached(chartInstance, c52_iv5, 3.0);
      if (CV_EML_IF(6, 0, 0, CV_RELATIONAL_EVAL(5U, 6U, 0, (real_T)c52_u1, 1.0,
            0, 0U, c52_u1 == 1))) {
        c52_c_out = true;
      } else {
        c52_updateDataWrittenToVector(chartInstance, 4U);
        c52_c_out = false;
      }

      _SFD_SYMBOL_SCOPE_POP();
      if (c52_c_out) {
        _SFD_CT_CALL(TRANSITION_ACTIVE_TAG, 6U, chartInstance->c52_sfEvent);
        chartInstance->c52_tp_GetToTheBall = 0U;
        _SFD_CS_CALL(STATE_INACTIVE_TAG, 2U, chartInstance->c52_sfEvent);
        chartInstance->c52_is_GameIsOn_Offence = c52_IN_Aim;
        _SFD_CS_CALL(STATE_ACTIVE_TAG, 1U, chartInstance->c52_sfEvent);
        chartInstance->c52_tp_Aim = 1U;
      } else {
        _SFD_CS_CALL(STATE_ENTER_DURING_FUNCTION_TAG, 2U,
                     chartInstance->c52_sfEvent);
        _SFD_SYMBOL_SCOPE_PUSH_EML(0U, 2U, 2U, c52_e_debug_family_names,
          c52_b_debug_family_var_map);
        _SFD_SYMBOL_SCOPE_ADD_EML_IMPORTABLE(&c52_f_nargin, 0U,
          c52_sf_marshallOut, c52_sf_marshallIn);
        _SFD_SYMBOL_SCOPE_ADD_EML_IMPORTABLE(&c52_f_nargout, 1U,
          c52_sf_marshallOut, c52_sf_marshallIn);
        c52_calcShootWay(chartInstance);
        c52_errorIfDataNotWrittenToFcn(chartInstance, 4U, 9U, 60U, 61, 10);
        *(int8_T *)&((char_T *)chartInstance->c52_finalWay)[0] =
          chartInstance->c52_shootWay.x;
        c52_updateDataWrittenToVector(chartInstance, 0U);
        c52_errorIfDataNotWrittenToFcn(chartInstance, 4U, 9U, 60U, 84, 10);
        *(int8_T *)&((char_T *)chartInstance->c52_finalWay)[1] =
          chartInstance->c52_shootWay.y;
        c52_updateDataWrittenToVector(chartInstance, 0U);
        *(int16_T *)&((char_T *)chartInstance->c52_finalWay)[2] = MAX_int16_T;
        c52_updateDataWrittenToVector(chartInstance, 0U);
        _SFD_SYMBOL_SCOPE_POP();
      }

      _SFD_CS_CALL(EXIT_OUT_OF_FUNCTION_TAG, 2U, chartInstance->c52_sfEvent);
      break;

     case c52_IN_Idle:
      CV_STATE_EVAL(0, 0, 3);
      _SFD_CT_CALL(TRANSITION_BEFORE_PROCESSING_TAG, 7U,
                   chartInstance->c52_sfEvent);
      _SFD_SYMBOL_SCOPE_PUSH_EML(0U, 3U, 3U, c52_k_debug_family_names,
        c52_debug_family_var_map);
      _SFD_SYMBOL_SCOPE_ADD_EML_IMPORTABLE(&c52_g_nargin, 0U, c52_sf_marshallOut,
        c52_sf_marshallIn);
      _SFD_SYMBOL_SCOPE_ADD_EML_IMPORTABLE(&c52_g_nargout, 1U,
        c52_sf_marshallOut, c52_sf_marshallIn);
      _SFD_SYMBOL_SCOPE_ADD_EML_IMPORTABLE(&c52_d_out, 2U, c52_c_sf_marshallOut,
        c52_c_sf_marshallIn);
      c52_d_out = CV_EML_IF(7, 0, 0, chartInstance->c52_temporalCounter_i1 >= 10);
      _SFD_SYMBOL_SCOPE_POP();
      if (c52_d_out) {
        _SFD_CT_CALL(TRANSITION_ACTIVE_TAG, 7U, chartInstance->c52_sfEvent);
        chartInstance->c52_tp_Idle = 0U;
        _SFD_CS_CALL(STATE_INACTIVE_TAG, 3U, chartInstance->c52_sfEvent);
        chartInstance->c52_is_GameIsOn_Offence = c52_IN_GetToTheBall;
        _SFD_CS_CALL(STATE_ACTIVE_TAG, 2U, chartInstance->c52_sfEvent);
        chartInstance->c52_tp_GetToTheBall = 1U;
        _SFD_SYMBOL_SCOPE_PUSH_EML(0U, 2U, 2U, c52_d_debug_family_names,
          c52_b_debug_family_var_map);
        _SFD_SYMBOL_SCOPE_ADD_EML_IMPORTABLE(&c52_h_nargin, 0U,
          c52_sf_marshallOut, c52_sf_marshallIn);
        _SFD_SYMBOL_SCOPE_ADD_EML_IMPORTABLE(&c52_h_nargout, 1U,
          c52_sf_marshallOut, c52_sf_marshallIn);
        c52_calcShootWay(chartInstance);
        _SFD_SYMBOL_SCOPE_POP();
      } else {
        _SFD_CS_CALL(STATE_ENTER_DURING_FUNCTION_TAG, 3U,
                     chartInstance->c52_sfEvent);
        _SFD_SYMBOL_SCOPE_PUSH_EML(0U, 2U, 2U, c52_f_debug_family_names,
          c52_b_debug_family_var_map);
        _SFD_SYMBOL_SCOPE_ADD_EML_IMPORTABLE(&c52_i_nargin, 0U,
          c52_sf_marshallOut, c52_sf_marshallIn);
        _SFD_SYMBOL_SCOPE_ADD_EML_IMPORTABLE(&c52_i_nargout, 1U,
          c52_sf_marshallOut, c52_sf_marshallIn);
        *(int8_T *)&((char_T *)chartInstance->c52_finalWay)[0] = *(int8_T *)
          &((char_T *)chartInstance->c52_me)[0];
        c52_updateDataWrittenToVector(chartInstance, 0U);
        *(int8_T *)&((char_T *)chartInstance->c52_finalWay)[1] = *(int8_T *)
          &((char_T *)chartInstance->c52_me)[1];
        c52_updateDataWrittenToVector(chartInstance, 0U);
        *(int16_T *)&((char_T *)chartInstance->c52_finalWay)[2] = *(int16_T *)
          &((char_T *)chartInstance->c52_me)[2];
        c52_updateDataWrittenToVector(chartInstance, 0U);
        _SFD_SYMBOL_SCOPE_POP();
      }

      _SFD_CS_CALL(EXIT_OUT_OF_FUNCTION_TAG, 3U, chartInstance->c52_sfEvent);
      break;

     case c52_IN_Kick:
      CV_STATE_EVAL(0, 0, 4);
      _SFD_CT_CALL(TRANSITION_BEFORE_PROCESSING_TAG, 9U,
                   chartInstance->c52_sfEvent);
      _SFD_SYMBOL_SCOPE_PUSH_EML(0U, 3U, 3U, c52_l_debug_family_names,
        c52_debug_family_var_map);
      _SFD_SYMBOL_SCOPE_ADD_EML_IMPORTABLE(&c52_j_nargin, 0U, c52_sf_marshallOut,
        c52_sf_marshallIn);
      _SFD_SYMBOL_SCOPE_ADD_EML_IMPORTABLE(&c52_j_nargout, 1U,
        c52_sf_marshallOut, c52_sf_marshallIn);
      _SFD_SYMBOL_SCOPE_ADD_EML_IMPORTABLE(&c52_e_out, 2U, c52_c_sf_marshallOut,
        c52_c_sf_marshallIn);
      guard1 = false;
      if (CV_EML_COND(9, 0, 0, chartInstance->c52_temporalCounter_i1 >= 10)) {
        c52_iv6[0] = *(int8_T *)&((char_T *)chartInstance->c52_ball)[0];
        c52_iv6[1] = *(int8_T *)&((char_T *)chartInstance->c52_ball)[1];
        c52_u2 = c52_checkReached(chartInstance, c52_iv6, 15.0);
        if (CV_EML_COND(9, 0, 1, CV_RELATIONAL_EVAL(5U, 9U, 0, (real_T)c52_u2,
              0.0, 0, 0U, c52_u2 == 0))) {
          CV_EML_MCDC(9, 0, 0, true);
          CV_EML_IF(9, 0, 0, true);
          c52_e_out = true;
        } else {
          guard1 = true;
        }
      } else {
        guard1 = true;
      }

      if (guard1 == true) {
        CV_EML_MCDC(9, 0, 0, false);
        CV_EML_IF(9, 0, 0, false);
        c52_e_out = false;
      }

      _SFD_SYMBOL_SCOPE_POP();
      if (c52_e_out) {
        _SFD_CT_CALL(TRANSITION_ACTIVE_TAG, 9U, chartInstance->c52_sfEvent);
        chartInstance->c52_tp_Kick = 0U;
        _SFD_CS_CALL(STATE_INACTIVE_TAG, 4U, chartInstance->c52_sfEvent);
        chartInstance->c52_is_GameIsOn_Offence = c52_IN_Idle;
        _SFD_CS_CALL(STATE_ACTIVE_TAG, 3U, chartInstance->c52_sfEvent);
        chartInstance->c52_temporalCounter_i1 = 0U;
        chartInstance->c52_tp_Idle = 1U;
      } else {
        _SFD_CS_CALL(STATE_ENTER_DURING_FUNCTION_TAG, 4U,
                     chartInstance->c52_sfEvent);
      }

      _SFD_CS_CALL(EXIT_OUT_OF_FUNCTION_TAG, 4U, chartInstance->c52_sfEvent);
      break;

     default:
      CV_STATE_EVAL(0, 0, 0);
      chartInstance->c52_is_GameIsOn_Offence = c52_IN_NO_ACTIVE_CHILD;
      _SFD_CS_CALL(STATE_INACTIVE_TAG, 1U, chartInstance->c52_sfEvent);
      break;
    }
  }

  _SFD_CS_CALL(EXIT_OUT_OF_FUNCTION_TAG, 0U, chartInstance->c52_sfEvent);
}

static void c52_exit_internal_GameIsOn_Offence(SFc52_LessonIIInstanceStruct
  *chartInstance)
{
  switch (chartInstance->c52_is_GameIsOn_Offence) {
   case c52_IN_Aim:
    CV_STATE_EVAL(0, 1, 1);
    chartInstance->c52_tp_Aim = 0U;
    chartInstance->c52_is_GameIsOn_Offence = c52_IN_NO_ACTIVE_CHILD;
    _SFD_CS_CALL(STATE_INACTIVE_TAG, 1U, chartInstance->c52_sfEvent);
    break;

   case c52_IN_GetToTheBall:
    CV_STATE_EVAL(0, 1, 2);
    chartInstance->c52_tp_GetToTheBall = 0U;
    chartInstance->c52_is_GameIsOn_Offence = c52_IN_NO_ACTIVE_CHILD;
    _SFD_CS_CALL(STATE_INACTIVE_TAG, 2U, chartInstance->c52_sfEvent);
    break;

   case c52_IN_Idle:
    CV_STATE_EVAL(0, 1, 3);
    chartInstance->c52_tp_Idle = 0U;
    chartInstance->c52_is_GameIsOn_Offence = c52_IN_NO_ACTIVE_CHILD;
    _SFD_CS_CALL(STATE_INACTIVE_TAG, 3U, chartInstance->c52_sfEvent);
    break;

   case c52_IN_Kick:
    CV_STATE_EVAL(0, 1, 4);
    chartInstance->c52_tp_Kick = 0U;
    chartInstance->c52_is_GameIsOn_Offence = c52_IN_NO_ACTIVE_CHILD;
    _SFD_CS_CALL(STATE_INACTIVE_TAG, 4U, chartInstance->c52_sfEvent);
    break;

   default:
    CV_STATE_EVAL(0, 1, 0);
    chartInstance->c52_is_GameIsOn_Offence = c52_IN_NO_ACTIVE_CHILD;
    _SFD_CS_CALL(STATE_INACTIVE_TAG, 1U, chartInstance->c52_sfEvent);
    break;
  }
}

static void c52_exit_internal_waiting(SFc52_LessonIIInstanceStruct
  *chartInstance)
{
  switch (chartInstance->c52_is_waiting) {
   case c52_IN_Idle1:
    CV_STATE_EVAL(9, 1, 1);
    chartInstance->c52_tp_Idle1 = 0U;
    chartInstance->c52_is_waiting = c52_IN_NO_ACTIVE_CHILD;
    _SFD_CS_CALL(STATE_INACTIVE_TAG, 10U, chartInstance->c52_sfEvent);
    break;

   case c52_IN_goToPosition:
    CV_STATE_EVAL(9, 1, 2);
    chartInstance->c52_tp_goToPosition = 0U;
    chartInstance->c52_is_waiting = c52_IN_NO_ACTIVE_CHILD;
    _SFD_CS_CALL(STATE_INACTIVE_TAG, 11U, chartInstance->c52_sfEvent);
    break;

   default:
    CV_STATE_EVAL(9, 1, 0);
    chartInstance->c52_is_waiting = c52_IN_NO_ACTIVE_CHILD;
    _SFD_CS_CALL(STATE_INACTIVE_TAG, 10U, chartInstance->c52_sfEvent);
    break;
  }
}

static int16_T c52_intmax(SFc52_LessonIIInstanceStruct *chartInstance)
{
  (void)chartInstance;
  return MAX_int16_T;
}

static real32_T c52_eml_xnrm2(SFc52_LessonIIInstanceStruct *chartInstance,
  real32_T c52_x[2])
{
  real32_T c52_y;
  real32_T c52_scale;
  int32_T c52_k;
  int32_T c52_b_k;
  real32_T c52_b_x;
  real32_T c52_c_x;
  real32_T c52_absxk;
  real32_T c52_t;
  c52_threshold(chartInstance);
  c52_y = 0.0F;
  c52_realmin(chartInstance);
  c52_scale = 1.17549435E-38F;
  for (c52_k = 1; c52_k < 3; c52_k++) {
    c52_b_k = c52_k;
    c52_b_x = c52_x[_SFD_EML_ARRAY_BOUNDS_CHECK("", (int32_T)_SFD_INTEGER_CHECK(
      "", (real_T)c52_b_k), 1, 2, 1, 0) - 1];
    c52_c_x = c52_b_x;
    c52_absxk = muSingleScalarAbs(c52_c_x);
    if (c52_absxk > c52_scale) {
      c52_t = c52_scale / c52_absxk;
      c52_y = 1.0F + c52_y * c52_t * c52_t;
      c52_scale = c52_absxk;
    } else {
      c52_t = c52_absxk / c52_scale;
      c52_y += c52_t * c52_t;
    }
  }

  return c52_scale * muSingleScalarSqrt(c52_y);
}

static void init_script_number_translation(uint32_T c52_machineNumber, uint32_T
  c52_chartNumber, uint32_T c52_instanceNumber)
{
  (void)c52_machineNumber;
  (void)c52_chartNumber;
  (void)c52_instanceNumber;
}

static const mxArray *c52_sf_marshallOut(void *chartInstanceVoid, void
  *c52_inData)
{
  const mxArray *c52_mxArrayOutData = NULL;
  real_T c52_u;
  const mxArray *c52_y = NULL;
  SFc52_LessonIIInstanceStruct *chartInstance;
  chartInstance = (SFc52_LessonIIInstanceStruct *)chartInstanceVoid;
  c52_mxArrayOutData = NULL;
  c52_u = *(real_T *)c52_inData;
  c52_y = NULL;
  sf_mex_assign(&c52_y, sf_mex_create("y", &c52_u, 0, 0U, 0U, 0U, 0), false);
  sf_mex_assign(&c52_mxArrayOutData, c52_y, false);
  return c52_mxArrayOutData;
}

static real_T c52_emlrt_marshallIn(SFc52_LessonIIInstanceStruct *chartInstance,
  const mxArray *c52_u, const emlrtMsgIdentifier *c52_parentId)
{
  real_T c52_y;
  real_T c52_d1;
  (void)chartInstance;
  sf_mex_import(c52_parentId, sf_mex_dup(c52_u), &c52_d1, 1, 0, 0U, 0, 0U, 0);
  c52_y = c52_d1;
  sf_mex_destroy(&c52_u);
  return c52_y;
}

static void c52_sf_marshallIn(void *chartInstanceVoid, const mxArray
  *c52_mxArrayInData, const char_T *c52_varName, void *c52_outData)
{
  const mxArray *c52_nargout;
  const char_T *c52_identifier;
  emlrtMsgIdentifier c52_thisId;
  real_T c52_y;
  SFc52_LessonIIInstanceStruct *chartInstance;
  chartInstance = (SFc52_LessonIIInstanceStruct *)chartInstanceVoid;
  c52_nargout = sf_mex_dup(c52_mxArrayInData);
  c52_identifier = c52_varName;
  c52_thisId.fIdentifier = c52_identifier;
  c52_thisId.fParent = NULL;
  c52_y = c52_emlrt_marshallIn(chartInstance, sf_mex_dup(c52_nargout),
    &c52_thisId);
  sf_mex_destroy(&c52_nargout);
  *(real_T *)c52_outData = c52_y;
  sf_mex_destroy(&c52_mxArrayInData);
}

static const mxArray *c52_b_sf_marshallOut(void *chartInstanceVoid, void
  *c52_inData)
{
  const mxArray *c52_mxArrayOutData = NULL;
  int32_T c52_i14;
  int8_T c52_b_inData[2];
  int32_T c52_i15;
  int8_T c52_u[2];
  const mxArray *c52_y = NULL;
  SFc52_LessonIIInstanceStruct *chartInstance;
  chartInstance = (SFc52_LessonIIInstanceStruct *)chartInstanceVoid;
  c52_mxArrayOutData = NULL;
  for (c52_i14 = 0; c52_i14 < 2; c52_i14++) {
    c52_b_inData[c52_i14] = (*(int8_T (*)[2])c52_inData)[c52_i14];
  }

  for (c52_i15 = 0; c52_i15 < 2; c52_i15++) {
    c52_u[c52_i15] = c52_b_inData[c52_i15];
  }

  c52_y = NULL;
  sf_mex_assign(&c52_y, sf_mex_create("y", c52_u, 2, 0U, 1U, 0U, 1, 2), false);
  sf_mex_assign(&c52_mxArrayOutData, c52_y, false);
  return c52_mxArrayOutData;
}

static void c52_b_emlrt_marshallIn(SFc52_LessonIIInstanceStruct *chartInstance,
  const mxArray *c52_u, const emlrtMsgIdentifier *c52_parentId, int8_T c52_y[2])
{
  int8_T c52_iv7[2];
  int32_T c52_i16;
  (void)chartInstance;
  sf_mex_import(c52_parentId, sf_mex_dup(c52_u), c52_iv7, 1, 2, 0U, 1, 0U, 1, 2);
  for (c52_i16 = 0; c52_i16 < 2; c52_i16++) {
    c52_y[c52_i16] = c52_iv7[c52_i16];
  }

  sf_mex_destroy(&c52_u);
}

static void c52_b_sf_marshallIn(void *chartInstanceVoid, const mxArray
  *c52_mxArrayInData, const char_T *c52_varName, void *c52_outData)
{
  const mxArray *c52_pos;
  const char_T *c52_identifier;
  emlrtMsgIdentifier c52_thisId;
  int8_T c52_y[2];
  int32_T c52_i17;
  SFc52_LessonIIInstanceStruct *chartInstance;
  chartInstance = (SFc52_LessonIIInstanceStruct *)chartInstanceVoid;
  c52_pos = sf_mex_dup(c52_mxArrayInData);
  c52_identifier = c52_varName;
  c52_thisId.fIdentifier = c52_identifier;
  c52_thisId.fParent = NULL;
  c52_b_emlrt_marshallIn(chartInstance, sf_mex_dup(c52_pos), &c52_thisId, c52_y);
  sf_mex_destroy(&c52_pos);
  for (c52_i17 = 0; c52_i17 < 2; c52_i17++) {
    (*(int8_T (*)[2])c52_outData)[c52_i17] = c52_y[c52_i17];
  }

  sf_mex_destroy(&c52_mxArrayInData);
}

static const mxArray *c52_c_sf_marshallOut(void *chartInstanceVoid, void
  *c52_inData)
{
  const mxArray *c52_mxArrayOutData = NULL;
  boolean_T c52_u;
  const mxArray *c52_y = NULL;
  SFc52_LessonIIInstanceStruct *chartInstance;
  chartInstance = (SFc52_LessonIIInstanceStruct *)chartInstanceVoid;
  c52_mxArrayOutData = NULL;
  c52_u = *(boolean_T *)c52_inData;
  c52_y = NULL;
  sf_mex_assign(&c52_y, sf_mex_create("y", &c52_u, 11, 0U, 0U, 0U, 0), false);
  sf_mex_assign(&c52_mxArrayOutData, c52_y, false);
  return c52_mxArrayOutData;
}

static boolean_T c52_c_emlrt_marshallIn(SFc52_LessonIIInstanceStruct
  *chartInstance, const mxArray *c52_u, const emlrtMsgIdentifier *c52_parentId)
{
  boolean_T c52_y;
  boolean_T c52_b0;
  (void)chartInstance;
  sf_mex_import(c52_parentId, sf_mex_dup(c52_u), &c52_b0, 1, 11, 0U, 0, 0U, 0);
  c52_y = c52_b0;
  sf_mex_destroy(&c52_u);
  return c52_y;
}

static void c52_c_sf_marshallIn(void *chartInstanceVoid, const mxArray
  *c52_mxArrayInData, const char_T *c52_varName, void *c52_outData)
{
  const mxArray *c52_sf_internal_predicateOutput;
  const char_T *c52_identifier;
  emlrtMsgIdentifier c52_thisId;
  boolean_T c52_y;
  SFc52_LessonIIInstanceStruct *chartInstance;
  chartInstance = (SFc52_LessonIIInstanceStruct *)chartInstanceVoid;
  c52_sf_internal_predicateOutput = sf_mex_dup(c52_mxArrayInData);
  c52_identifier = c52_varName;
  c52_thisId.fIdentifier = c52_identifier;
  c52_thisId.fParent = NULL;
  c52_y = c52_c_emlrt_marshallIn(chartInstance, sf_mex_dup
    (c52_sf_internal_predicateOutput), &c52_thisId);
  sf_mex_destroy(&c52_sf_internal_predicateOutput);
  *(boolean_T *)c52_outData = c52_y;
  sf_mex_destroy(&c52_mxArrayInData);
}

static const mxArray *c52_d_sf_marshallOut(void *chartInstanceVoid, void
  *c52_inData)
{
  const mxArray *c52_mxArrayOutData = NULL;
  uint8_T c52_u;
  const mxArray *c52_y = NULL;
  SFc52_LessonIIInstanceStruct *chartInstance;
  chartInstance = (SFc52_LessonIIInstanceStruct *)chartInstanceVoid;
  c52_mxArrayOutData = NULL;
  c52_u = *(uint8_T *)c52_inData;
  c52_y = NULL;
  sf_mex_assign(&c52_y, sf_mex_create("y", &c52_u, 3, 0U, 0U, 0U, 0), false);
  sf_mex_assign(&c52_mxArrayOutData, c52_y, false);
  return c52_mxArrayOutData;
}

static uint8_T c52_d_emlrt_marshallIn(SFc52_LessonIIInstanceStruct
  *chartInstance, const mxArray *c52_posReached, const char_T *c52_identifier)
{
  uint8_T c52_y;
  emlrtMsgIdentifier c52_thisId;
  c52_thisId.fIdentifier = c52_identifier;
  c52_thisId.fParent = NULL;
  c52_y = c52_e_emlrt_marshallIn(chartInstance, sf_mex_dup(c52_posReached),
    &c52_thisId);
  sf_mex_destroy(&c52_posReached);
  return c52_y;
}

static uint8_T c52_e_emlrt_marshallIn(SFc52_LessonIIInstanceStruct
  *chartInstance, const mxArray *c52_u, const emlrtMsgIdentifier *c52_parentId)
{
  uint8_T c52_y;
  uint8_T c52_u3;
  (void)chartInstance;
  sf_mex_import(c52_parentId, sf_mex_dup(c52_u), &c52_u3, 1, 3, 0U, 0, 0U, 0);
  c52_y = c52_u3;
  sf_mex_destroy(&c52_u);
  return c52_y;
}

static void c52_d_sf_marshallIn(void *chartInstanceVoid, const mxArray
  *c52_mxArrayInData, const char_T *c52_varName, void *c52_outData)
{
  const mxArray *c52_posReached;
  const char_T *c52_identifier;
  emlrtMsgIdentifier c52_thisId;
  uint8_T c52_y;
  SFc52_LessonIIInstanceStruct *chartInstance;
  chartInstance = (SFc52_LessonIIInstanceStruct *)chartInstanceVoid;
  c52_posReached = sf_mex_dup(c52_mxArrayInData);
  c52_identifier = c52_varName;
  c52_thisId.fIdentifier = c52_identifier;
  c52_thisId.fParent = NULL;
  c52_y = c52_e_emlrt_marshallIn(chartInstance, sf_mex_dup(c52_posReached),
    &c52_thisId);
  sf_mex_destroy(&c52_posReached);
  *(uint8_T *)c52_outData = c52_y;
  sf_mex_destroy(&c52_mxArrayInData);
}

static const mxArray *c52_e_sf_marshallOut(void *chartInstanceVoid, void
  *c52_inData)
{
  const mxArray *c52_mxArrayOutData = NULL;
  int32_T c52_i18;
  int8_T c52_b_inData[2];
  int32_T c52_i19;
  int8_T c52_u[2];
  const mxArray *c52_y = NULL;
  SFc52_LessonIIInstanceStruct *chartInstance;
  chartInstance = (SFc52_LessonIIInstanceStruct *)chartInstanceVoid;
  c52_mxArrayOutData = NULL;
  for (c52_i18 = 0; c52_i18 < 2; c52_i18++) {
    c52_b_inData[c52_i18] = (*(int8_T (*)[2])c52_inData)[c52_i18];
  }

  for (c52_i19 = 0; c52_i19 < 2; c52_i19++) {
    c52_u[c52_i19] = c52_b_inData[c52_i19];
  }

  c52_y = NULL;
  sf_mex_assign(&c52_y, sf_mex_create("y", c52_u, 2, 0U, 1U, 0U, 2, 1, 2), false);
  sf_mex_assign(&c52_mxArrayOutData, c52_y, false);
  return c52_mxArrayOutData;
}

static void c52_f_emlrt_marshallIn(SFc52_LessonIIInstanceStruct *chartInstance,
  const mxArray *c52_u, const emlrtMsgIdentifier *c52_parentId, int8_T c52_y[2])
{
  int8_T c52_iv8[2];
  int32_T c52_i20;
  (void)chartInstance;
  sf_mex_import(c52_parentId, sf_mex_dup(c52_u), c52_iv8, 1, 2, 0U, 1, 0U, 2, 1,
                2);
  for (c52_i20 = 0; c52_i20 < 2; c52_i20++) {
    c52_y[c52_i20] = c52_iv8[c52_i20];
  }

  sf_mex_destroy(&c52_u);
}

static void c52_e_sf_marshallIn(void *chartInstanceVoid, const mxArray
  *c52_mxArrayInData, const char_T *c52_varName, void *c52_outData)
{
  const mxArray *c52_pos;
  const char_T *c52_identifier;
  emlrtMsgIdentifier c52_thisId;
  int8_T c52_y[2];
  int32_T c52_i21;
  SFc52_LessonIIInstanceStruct *chartInstance;
  chartInstance = (SFc52_LessonIIInstanceStruct *)chartInstanceVoid;
  c52_pos = sf_mex_dup(c52_mxArrayInData);
  c52_identifier = c52_varName;
  c52_thisId.fIdentifier = c52_identifier;
  c52_thisId.fParent = NULL;
  c52_f_emlrt_marshallIn(chartInstance, sf_mex_dup(c52_pos), &c52_thisId, c52_y);
  sf_mex_destroy(&c52_pos);
  for (c52_i21 = 0; c52_i21 < 2; c52_i21++) {
    (*(int8_T (*)[2])c52_outData)[c52_i21] = c52_y[c52_i21];
  }

  sf_mex_destroy(&c52_mxArrayInData);
}

const mxArray *sf_c52_LessonII_get_eml_resolved_functions_info(void)
{
  const mxArray *c52_nameCaptureInfo = NULL;
  c52_nameCaptureInfo = NULL;
  sf_mex_assign(&c52_nameCaptureInfo, sf_mex_createstruct("structure", 2, 47, 1),
                false);
  c52_info_helper(&c52_nameCaptureInfo);
  sf_mex_emlrtNameCapturePostProcessR2012a(&c52_nameCaptureInfo);
  return c52_nameCaptureInfo;
}

static void c52_info_helper(const mxArray **c52_info)
{
  const mxArray *c52_rhs0 = NULL;
  const mxArray *c52_lhs0 = NULL;
  const mxArray *c52_rhs1 = NULL;
  const mxArray *c52_lhs1 = NULL;
  const mxArray *c52_rhs2 = NULL;
  const mxArray *c52_lhs2 = NULL;
  const mxArray *c52_rhs3 = NULL;
  const mxArray *c52_lhs3 = NULL;
  const mxArray *c52_rhs4 = NULL;
  const mxArray *c52_lhs4 = NULL;
  const mxArray *c52_rhs5 = NULL;
  const mxArray *c52_lhs5 = NULL;
  const mxArray *c52_rhs6 = NULL;
  const mxArray *c52_lhs6 = NULL;
  const mxArray *c52_rhs7 = NULL;
  const mxArray *c52_lhs7 = NULL;
  const mxArray *c52_rhs8 = NULL;
  const mxArray *c52_lhs8 = NULL;
  const mxArray *c52_rhs9 = NULL;
  const mxArray *c52_lhs9 = NULL;
  const mxArray *c52_rhs10 = NULL;
  const mxArray *c52_lhs10 = NULL;
  const mxArray *c52_rhs11 = NULL;
  const mxArray *c52_lhs11 = NULL;
  const mxArray *c52_rhs12 = NULL;
  const mxArray *c52_lhs12 = NULL;
  const mxArray *c52_rhs13 = NULL;
  const mxArray *c52_lhs13 = NULL;
  const mxArray *c52_rhs14 = NULL;
  const mxArray *c52_lhs14 = NULL;
  const mxArray *c52_rhs15 = NULL;
  const mxArray *c52_lhs15 = NULL;
  const mxArray *c52_rhs16 = NULL;
  const mxArray *c52_lhs16 = NULL;
  const mxArray *c52_rhs17 = NULL;
  const mxArray *c52_lhs17 = NULL;
  const mxArray *c52_rhs18 = NULL;
  const mxArray *c52_lhs18 = NULL;
  const mxArray *c52_rhs19 = NULL;
  const mxArray *c52_lhs19 = NULL;
  const mxArray *c52_rhs20 = NULL;
  const mxArray *c52_lhs20 = NULL;
  const mxArray *c52_rhs21 = NULL;
  const mxArray *c52_lhs21 = NULL;
  const mxArray *c52_rhs22 = NULL;
  const mxArray *c52_lhs22 = NULL;
  const mxArray *c52_rhs23 = NULL;
  const mxArray *c52_lhs23 = NULL;
  const mxArray *c52_rhs24 = NULL;
  const mxArray *c52_lhs24 = NULL;
  const mxArray *c52_rhs25 = NULL;
  const mxArray *c52_lhs25 = NULL;
  const mxArray *c52_rhs26 = NULL;
  const mxArray *c52_lhs26 = NULL;
  const mxArray *c52_rhs27 = NULL;
  const mxArray *c52_lhs27 = NULL;
  const mxArray *c52_rhs28 = NULL;
  const mxArray *c52_lhs28 = NULL;
  const mxArray *c52_rhs29 = NULL;
  const mxArray *c52_lhs29 = NULL;
  const mxArray *c52_rhs30 = NULL;
  const mxArray *c52_lhs30 = NULL;
  const mxArray *c52_rhs31 = NULL;
  const mxArray *c52_lhs31 = NULL;
  const mxArray *c52_rhs32 = NULL;
  const mxArray *c52_lhs32 = NULL;
  const mxArray *c52_rhs33 = NULL;
  const mxArray *c52_lhs33 = NULL;
  const mxArray *c52_rhs34 = NULL;
  const mxArray *c52_lhs34 = NULL;
  const mxArray *c52_rhs35 = NULL;
  const mxArray *c52_lhs35 = NULL;
  const mxArray *c52_rhs36 = NULL;
  const mxArray *c52_lhs36 = NULL;
  const mxArray *c52_rhs37 = NULL;
  const mxArray *c52_lhs37 = NULL;
  const mxArray *c52_rhs38 = NULL;
  const mxArray *c52_lhs38 = NULL;
  const mxArray *c52_rhs39 = NULL;
  const mxArray *c52_lhs39 = NULL;
  const mxArray *c52_rhs40 = NULL;
  const mxArray *c52_lhs40 = NULL;
  const mxArray *c52_rhs41 = NULL;
  const mxArray *c52_lhs41 = NULL;
  const mxArray *c52_rhs42 = NULL;
  const mxArray *c52_lhs42 = NULL;
  const mxArray *c52_rhs43 = NULL;
  const mxArray *c52_lhs43 = NULL;
  const mxArray *c52_rhs44 = NULL;
  const mxArray *c52_lhs44 = NULL;
  const mxArray *c52_rhs45 = NULL;
  const mxArray *c52_lhs45 = NULL;
  const mxArray *c52_rhs46 = NULL;
  const mxArray *c52_lhs46 = NULL;
  sf_mex_addfield(*c52_info, c52_emlrt_marshallOut(""), "context", "context", 0);
  sf_mex_addfield(*c52_info, c52_emlrt_marshallOut("eml_mtimes_helper"), "name",
                  "name", 0);
  sf_mex_addfield(*c52_info, c52_emlrt_marshallOut(""), "dominantType",
                  "dominantType", 0);
  sf_mex_addfield(*c52_info, c52_emlrt_marshallOut(
    "[ILXE]$matlabroot$/toolbox/eml/lib/matlab/ops/eml_mtimes_helper.m"),
                  "resolved", "resolved", 0);
  sf_mex_addfield(*c52_info, c52_b_emlrt_marshallOut(1383880894U), "fileTimeLo",
                  "fileTimeLo", 0);
  sf_mex_addfield(*c52_info, c52_b_emlrt_marshallOut(0U), "fileTimeHi",
                  "fileTimeHi", 0);
  sf_mex_addfield(*c52_info, c52_b_emlrt_marshallOut(0U), "mFileTimeLo",
                  "mFileTimeLo", 0);
  sf_mex_addfield(*c52_info, c52_b_emlrt_marshallOut(0U), "mFileTimeHi",
                  "mFileTimeHi", 0);
  sf_mex_assign(&c52_rhs0, sf_mex_createcellmatrix(0, 1), false);
  sf_mex_assign(&c52_lhs0, sf_mex_createcellmatrix(0, 1), false);
  sf_mex_addfield(*c52_info, sf_mex_duplicatearraysafe(&c52_rhs0), "rhs", "rhs",
                  0);
  sf_mex_addfield(*c52_info, sf_mex_duplicatearraysafe(&c52_lhs0), "lhs", "lhs",
                  0);
  sf_mex_addfield(*c52_info, c52_emlrt_marshallOut(
    "[ILXE]$matlabroot$/toolbox/eml/lib/matlab/ops/eml_mtimes_helper.m!common_checks"),
                  "context", "context", 1);
  sf_mex_addfield(*c52_info, c52_emlrt_marshallOut(
    "coder.internal.isBuiltInNumeric"), "name", "name", 1);
  sf_mex_addfield(*c52_info, c52_emlrt_marshallOut("double"), "dominantType",
                  "dominantType", 1);
  sf_mex_addfield(*c52_info, c52_emlrt_marshallOut(
    "[ILXE]$matlabroot$/toolbox/shared/coder/coder/+coder/+internal/isBuiltInNumeric.m"),
                  "resolved", "resolved", 1);
  sf_mex_addfield(*c52_info, c52_b_emlrt_marshallOut(1395935456U), "fileTimeLo",
                  "fileTimeLo", 1);
  sf_mex_addfield(*c52_info, c52_b_emlrt_marshallOut(0U), "fileTimeHi",
                  "fileTimeHi", 1);
  sf_mex_addfield(*c52_info, c52_b_emlrt_marshallOut(0U), "mFileTimeLo",
                  "mFileTimeLo", 1);
  sf_mex_addfield(*c52_info, c52_b_emlrt_marshallOut(0U), "mFileTimeHi",
                  "mFileTimeHi", 1);
  sf_mex_assign(&c52_rhs1, sf_mex_createcellmatrix(0, 1), false);
  sf_mex_assign(&c52_lhs1, sf_mex_createcellmatrix(0, 1), false);
  sf_mex_addfield(*c52_info, sf_mex_duplicatearraysafe(&c52_rhs1), "rhs", "rhs",
                  1);
  sf_mex_addfield(*c52_info, sf_mex_duplicatearraysafe(&c52_lhs1), "lhs", "lhs",
                  1);
  sf_mex_addfield(*c52_info, c52_emlrt_marshallOut(
    "[ILXE]$matlabroot$/toolbox/eml/lib/matlab/ops/eml_mtimes_helper.m!common_checks"),
                  "context", "context", 2);
  sf_mex_addfield(*c52_info, c52_emlrt_marshallOut(
    "coder.internal.isBuiltInNumeric"), "name", "name", 2);
  sf_mex_addfield(*c52_info, c52_emlrt_marshallOut("single"), "dominantType",
                  "dominantType", 2);
  sf_mex_addfield(*c52_info, c52_emlrt_marshallOut(
    "[ILXE]$matlabroot$/toolbox/shared/coder/coder/+coder/+internal/isBuiltInNumeric.m"),
                  "resolved", "resolved", 2);
  sf_mex_addfield(*c52_info, c52_b_emlrt_marshallOut(1395935456U), "fileTimeLo",
                  "fileTimeLo", 2);
  sf_mex_addfield(*c52_info, c52_b_emlrt_marshallOut(0U), "fileTimeHi",
                  "fileTimeHi", 2);
  sf_mex_addfield(*c52_info, c52_b_emlrt_marshallOut(0U), "mFileTimeLo",
                  "mFileTimeLo", 2);
  sf_mex_addfield(*c52_info, c52_b_emlrt_marshallOut(0U), "mFileTimeHi",
                  "mFileTimeHi", 2);
  sf_mex_assign(&c52_rhs2, sf_mex_createcellmatrix(0, 1), false);
  sf_mex_assign(&c52_lhs2, sf_mex_createcellmatrix(0, 1), false);
  sf_mex_addfield(*c52_info, sf_mex_duplicatearraysafe(&c52_rhs2), "rhs", "rhs",
                  2);
  sf_mex_addfield(*c52_info, sf_mex_duplicatearraysafe(&c52_lhs2), "lhs", "lhs",
                  2);
  sf_mex_addfield(*c52_info, c52_emlrt_marshallOut(""), "context", "context", 3);
  sf_mex_addfield(*c52_info, c52_emlrt_marshallOut("norm"), "name", "name", 3);
  sf_mex_addfield(*c52_info, c52_emlrt_marshallOut("single"), "dominantType",
                  "dominantType", 3);
  sf_mex_addfield(*c52_info, c52_emlrt_marshallOut(
    "[ILXE]$matlabroot$/toolbox/eml/lib/matlab/matfun/norm.m"), "resolved",
                  "resolved", 3);
  sf_mex_addfield(*c52_info, c52_b_emlrt_marshallOut(1363717468U), "fileTimeLo",
                  "fileTimeLo", 3);
  sf_mex_addfield(*c52_info, c52_b_emlrt_marshallOut(0U), "fileTimeHi",
                  "fileTimeHi", 3);
  sf_mex_addfield(*c52_info, c52_b_emlrt_marshallOut(0U), "mFileTimeLo",
                  "mFileTimeLo", 3);
  sf_mex_addfield(*c52_info, c52_b_emlrt_marshallOut(0U), "mFileTimeHi",
                  "mFileTimeHi", 3);
  sf_mex_assign(&c52_rhs3, sf_mex_createcellmatrix(0, 1), false);
  sf_mex_assign(&c52_lhs3, sf_mex_createcellmatrix(0, 1), false);
  sf_mex_addfield(*c52_info, sf_mex_duplicatearraysafe(&c52_rhs3), "rhs", "rhs",
                  3);
  sf_mex_addfield(*c52_info, sf_mex_duplicatearraysafe(&c52_lhs3), "lhs", "lhs",
                  3);
  sf_mex_addfield(*c52_info, c52_emlrt_marshallOut(
    "[ILXE]$matlabroot$/toolbox/eml/lib/matlab/matfun/norm.m!genpnorm"),
                  "context", "context", 4);
  sf_mex_addfield(*c52_info, c52_emlrt_marshallOut("eml_index_class"), "name",
                  "name", 4);
  sf_mex_addfield(*c52_info, c52_emlrt_marshallOut(""), "dominantType",
                  "dominantType", 4);
  sf_mex_addfield(*c52_info, c52_emlrt_marshallOut(
    "[ILXE]$matlabroot$/toolbox/eml/lib/matlab/eml/eml_index_class.m"),
                  "resolved", "resolved", 4);
  sf_mex_addfield(*c52_info, c52_b_emlrt_marshallOut(1323174178U), "fileTimeLo",
                  "fileTimeLo", 4);
  sf_mex_addfield(*c52_info, c52_b_emlrt_marshallOut(0U), "fileTimeHi",
                  "fileTimeHi", 4);
  sf_mex_addfield(*c52_info, c52_b_emlrt_marshallOut(0U), "mFileTimeLo",
                  "mFileTimeLo", 4);
  sf_mex_addfield(*c52_info, c52_b_emlrt_marshallOut(0U), "mFileTimeHi",
                  "mFileTimeHi", 4);
  sf_mex_assign(&c52_rhs4, sf_mex_createcellmatrix(0, 1), false);
  sf_mex_assign(&c52_lhs4, sf_mex_createcellmatrix(0, 1), false);
  sf_mex_addfield(*c52_info, sf_mex_duplicatearraysafe(&c52_rhs4), "rhs", "rhs",
                  4);
  sf_mex_addfield(*c52_info, sf_mex_duplicatearraysafe(&c52_lhs4), "lhs", "lhs",
                  4);
  sf_mex_addfield(*c52_info, c52_emlrt_marshallOut(
    "[ILXE]$matlabroot$/toolbox/eml/lib/matlab/matfun/norm.m!genpnorm"),
                  "context", "context", 5);
  sf_mex_addfield(*c52_info, c52_emlrt_marshallOut(
    "coder.internal.isBuiltInNumeric"), "name", "name", 5);
  sf_mex_addfield(*c52_info, c52_emlrt_marshallOut("double"), "dominantType",
                  "dominantType", 5);
  sf_mex_addfield(*c52_info, c52_emlrt_marshallOut(
    "[ILXE]$matlabroot$/toolbox/shared/coder/coder/+coder/+internal/isBuiltInNumeric.m"),
                  "resolved", "resolved", 5);
  sf_mex_addfield(*c52_info, c52_b_emlrt_marshallOut(1395935456U), "fileTimeLo",
                  "fileTimeLo", 5);
  sf_mex_addfield(*c52_info, c52_b_emlrt_marshallOut(0U), "fileTimeHi",
                  "fileTimeHi", 5);
  sf_mex_addfield(*c52_info, c52_b_emlrt_marshallOut(0U), "mFileTimeLo",
                  "mFileTimeLo", 5);
  sf_mex_addfield(*c52_info, c52_b_emlrt_marshallOut(0U), "mFileTimeHi",
                  "mFileTimeHi", 5);
  sf_mex_assign(&c52_rhs5, sf_mex_createcellmatrix(0, 1), false);
  sf_mex_assign(&c52_lhs5, sf_mex_createcellmatrix(0, 1), false);
  sf_mex_addfield(*c52_info, sf_mex_duplicatearraysafe(&c52_rhs5), "rhs", "rhs",
                  5);
  sf_mex_addfield(*c52_info, sf_mex_duplicatearraysafe(&c52_lhs5), "lhs", "lhs",
                  5);
  sf_mex_addfield(*c52_info, c52_emlrt_marshallOut(
    "[ILXE]$matlabroot$/toolbox/eml/lib/matlab/matfun/norm.m!genpnorm"),
                  "context", "context", 6);
  sf_mex_addfield(*c52_info, c52_emlrt_marshallOut("eml_xnrm2"), "name", "name",
                  6);
  sf_mex_addfield(*c52_info, c52_emlrt_marshallOut("single"), "dominantType",
                  "dominantType", 6);
  sf_mex_addfield(*c52_info, c52_emlrt_marshallOut(
    "[ILXE]$matlabroot$/toolbox/eml/lib/matlab/eml/blas/eml_xnrm2.m"),
                  "resolved", "resolved", 6);
  sf_mex_addfield(*c52_info, c52_b_emlrt_marshallOut(1375984292U), "fileTimeLo",
                  "fileTimeLo", 6);
  sf_mex_addfield(*c52_info, c52_b_emlrt_marshallOut(0U), "fileTimeHi",
                  "fileTimeHi", 6);
  sf_mex_addfield(*c52_info, c52_b_emlrt_marshallOut(0U), "mFileTimeLo",
                  "mFileTimeLo", 6);
  sf_mex_addfield(*c52_info, c52_b_emlrt_marshallOut(0U), "mFileTimeHi",
                  "mFileTimeHi", 6);
  sf_mex_assign(&c52_rhs6, sf_mex_createcellmatrix(0, 1), false);
  sf_mex_assign(&c52_lhs6, sf_mex_createcellmatrix(0, 1), false);
  sf_mex_addfield(*c52_info, sf_mex_duplicatearraysafe(&c52_rhs6), "rhs", "rhs",
                  6);
  sf_mex_addfield(*c52_info, sf_mex_duplicatearraysafe(&c52_lhs6), "lhs", "lhs",
                  6);
  sf_mex_addfield(*c52_info, c52_emlrt_marshallOut(
    "[ILXE]$matlabroot$/toolbox/eml/lib/matlab/eml/blas/eml_xnrm2.m"), "context",
                  "context", 7);
  sf_mex_addfield(*c52_info, c52_emlrt_marshallOut("coder.internal.blas.inline"),
                  "name", "name", 7);
  sf_mex_addfield(*c52_info, c52_emlrt_marshallOut(""), "dominantType",
                  "dominantType", 7);
  sf_mex_addfield(*c52_info, c52_emlrt_marshallOut(
    "[ILXE]$matlabroot$/toolbox/coder/coder/+coder/+internal/+blas/inline.p"),
                  "resolved", "resolved", 7);
  sf_mex_addfield(*c52_info, c52_b_emlrt_marshallOut(1410811372U), "fileTimeLo",
                  "fileTimeLo", 7);
  sf_mex_addfield(*c52_info, c52_b_emlrt_marshallOut(0U), "fileTimeHi",
                  "fileTimeHi", 7);
  sf_mex_addfield(*c52_info, c52_b_emlrt_marshallOut(0U), "mFileTimeLo",
                  "mFileTimeLo", 7);
  sf_mex_addfield(*c52_info, c52_b_emlrt_marshallOut(0U), "mFileTimeHi",
                  "mFileTimeHi", 7);
  sf_mex_assign(&c52_rhs7, sf_mex_createcellmatrix(0, 1), false);
  sf_mex_assign(&c52_lhs7, sf_mex_createcellmatrix(0, 1), false);
  sf_mex_addfield(*c52_info, sf_mex_duplicatearraysafe(&c52_rhs7), "rhs", "rhs",
                  7);
  sf_mex_addfield(*c52_info, sf_mex_duplicatearraysafe(&c52_lhs7), "lhs", "lhs",
                  7);
  sf_mex_addfield(*c52_info, c52_emlrt_marshallOut(
    "[ILXE]$matlabroot$/toolbox/eml/lib/matlab/eml/blas/eml_xnrm2.m"), "context",
                  "context", 8);
  sf_mex_addfield(*c52_info, c52_emlrt_marshallOut("coder.internal.blas.xnrm2"),
                  "name", "name", 8);
  sf_mex_addfield(*c52_info, c52_emlrt_marshallOut("single"), "dominantType",
                  "dominantType", 8);
  sf_mex_addfield(*c52_info, c52_emlrt_marshallOut(
    "[ILXE]$matlabroot$/toolbox/coder/coder/+coder/+internal/+blas/xnrm2.p"),
                  "resolved", "resolved", 8);
  sf_mex_addfield(*c52_info, c52_b_emlrt_marshallOut(1410811370U), "fileTimeLo",
                  "fileTimeLo", 8);
  sf_mex_addfield(*c52_info, c52_b_emlrt_marshallOut(0U), "fileTimeHi",
                  "fileTimeHi", 8);
  sf_mex_addfield(*c52_info, c52_b_emlrt_marshallOut(0U), "mFileTimeLo",
                  "mFileTimeLo", 8);
  sf_mex_addfield(*c52_info, c52_b_emlrt_marshallOut(0U), "mFileTimeHi",
                  "mFileTimeHi", 8);
  sf_mex_assign(&c52_rhs8, sf_mex_createcellmatrix(0, 1), false);
  sf_mex_assign(&c52_lhs8, sf_mex_createcellmatrix(0, 1), false);
  sf_mex_addfield(*c52_info, sf_mex_duplicatearraysafe(&c52_rhs8), "rhs", "rhs",
                  8);
  sf_mex_addfield(*c52_info, sf_mex_duplicatearraysafe(&c52_lhs8), "lhs", "lhs",
                  8);
  sf_mex_addfield(*c52_info, c52_emlrt_marshallOut(
    "[ILXE]$matlabroot$/toolbox/coder/coder/+coder/+internal/+blas/xnrm2.p"),
                  "context", "context", 9);
  sf_mex_addfield(*c52_info, c52_emlrt_marshallOut(
    "coder.internal.blas.use_refblas"), "name", "name", 9);
  sf_mex_addfield(*c52_info, c52_emlrt_marshallOut(""), "dominantType",
                  "dominantType", 9);
  sf_mex_addfield(*c52_info, c52_emlrt_marshallOut(
    "[ILXE]$matlabroot$/toolbox/coder/coder/+coder/+internal/+blas/use_refblas.p"),
                  "resolved", "resolved", 9);
  sf_mex_addfield(*c52_info, c52_b_emlrt_marshallOut(1410811370U), "fileTimeLo",
                  "fileTimeLo", 9);
  sf_mex_addfield(*c52_info, c52_b_emlrt_marshallOut(0U), "fileTimeHi",
                  "fileTimeHi", 9);
  sf_mex_addfield(*c52_info, c52_b_emlrt_marshallOut(0U), "mFileTimeLo",
                  "mFileTimeLo", 9);
  sf_mex_addfield(*c52_info, c52_b_emlrt_marshallOut(0U), "mFileTimeHi",
                  "mFileTimeHi", 9);
  sf_mex_assign(&c52_rhs9, sf_mex_createcellmatrix(0, 1), false);
  sf_mex_assign(&c52_lhs9, sf_mex_createcellmatrix(0, 1), false);
  sf_mex_addfield(*c52_info, sf_mex_duplicatearraysafe(&c52_rhs9), "rhs", "rhs",
                  9);
  sf_mex_addfield(*c52_info, sf_mex_duplicatearraysafe(&c52_lhs9), "lhs", "lhs",
                  9);
  sf_mex_addfield(*c52_info, c52_emlrt_marshallOut(
    "[ILXE]$matlabroot$/toolbox/coder/coder/+coder/+internal/+blas/xnrm2.p!below_threshold"),
                  "context", "context", 10);
  sf_mex_addfield(*c52_info, c52_emlrt_marshallOut(
    "coder.internal.blas.threshold"), "name", "name", 10);
  sf_mex_addfield(*c52_info, c52_emlrt_marshallOut("char"), "dominantType",
                  "dominantType", 10);
  sf_mex_addfield(*c52_info, c52_emlrt_marshallOut(
    "[ILXE]$matlabroot$/toolbox/coder/coder/+coder/+internal/+blas/threshold.p"),
                  "resolved", "resolved", 10);
  sf_mex_addfield(*c52_info, c52_b_emlrt_marshallOut(1410811372U), "fileTimeLo",
                  "fileTimeLo", 10);
  sf_mex_addfield(*c52_info, c52_b_emlrt_marshallOut(0U), "fileTimeHi",
                  "fileTimeHi", 10);
  sf_mex_addfield(*c52_info, c52_b_emlrt_marshallOut(0U), "mFileTimeLo",
                  "mFileTimeLo", 10);
  sf_mex_addfield(*c52_info, c52_b_emlrt_marshallOut(0U), "mFileTimeHi",
                  "mFileTimeHi", 10);
  sf_mex_assign(&c52_rhs10, sf_mex_createcellmatrix(0, 1), false);
  sf_mex_assign(&c52_lhs10, sf_mex_createcellmatrix(0, 1), false);
  sf_mex_addfield(*c52_info, sf_mex_duplicatearraysafe(&c52_rhs10), "rhs", "rhs",
                  10);
  sf_mex_addfield(*c52_info, sf_mex_duplicatearraysafe(&c52_lhs10), "lhs", "lhs",
                  10);
  sf_mex_addfield(*c52_info, c52_emlrt_marshallOut(
    "[ILXE]$matlabroot$/toolbox/coder/coder/+coder/+internal/+blas/threshold.p"),
                  "context", "context", 11);
  sf_mex_addfield(*c52_info, c52_emlrt_marshallOut("eml_switch_helper"), "name",
                  "name", 11);
  sf_mex_addfield(*c52_info, c52_emlrt_marshallOut(""), "dominantType",
                  "dominantType", 11);
  sf_mex_addfield(*c52_info, c52_emlrt_marshallOut(
    "[ILXE]$matlabroot$/toolbox/eml/lib/matlab/eml/eml_switch_helper.m"),
                  "resolved", "resolved", 11);
  sf_mex_addfield(*c52_info, c52_b_emlrt_marshallOut(1393334458U), "fileTimeLo",
                  "fileTimeLo", 11);
  sf_mex_addfield(*c52_info, c52_b_emlrt_marshallOut(0U), "fileTimeHi",
                  "fileTimeHi", 11);
  sf_mex_addfield(*c52_info, c52_b_emlrt_marshallOut(0U), "mFileTimeLo",
                  "mFileTimeLo", 11);
  sf_mex_addfield(*c52_info, c52_b_emlrt_marshallOut(0U), "mFileTimeHi",
                  "mFileTimeHi", 11);
  sf_mex_assign(&c52_rhs11, sf_mex_createcellmatrix(0, 1), false);
  sf_mex_assign(&c52_lhs11, sf_mex_createcellmatrix(0, 1), false);
  sf_mex_addfield(*c52_info, sf_mex_duplicatearraysafe(&c52_rhs11), "rhs", "rhs",
                  11);
  sf_mex_addfield(*c52_info, sf_mex_duplicatearraysafe(&c52_lhs11), "lhs", "lhs",
                  11);
  sf_mex_addfield(*c52_info, c52_emlrt_marshallOut(
    "[ILXE]$matlabroot$/toolbox/coder/coder/+coder/+internal/+blas/xnrm2.p"),
                  "context", "context", 12);
  sf_mex_addfield(*c52_info, c52_emlrt_marshallOut(
    "coder.internal.refblas.xnrm2"), "name", "name", 12);
  sf_mex_addfield(*c52_info, c52_emlrt_marshallOut("single"), "dominantType",
                  "dominantType", 12);
  sf_mex_addfield(*c52_info, c52_emlrt_marshallOut(
    "[ILXE]$matlabroot$/toolbox/coder/coder/+coder/+internal/+refblas/xnrm2.p"),
                  "resolved", "resolved", 12);
  sf_mex_addfield(*c52_info, c52_b_emlrt_marshallOut(1410811372U), "fileTimeLo",
                  "fileTimeLo", 12);
  sf_mex_addfield(*c52_info, c52_b_emlrt_marshallOut(0U), "fileTimeHi",
                  "fileTimeHi", 12);
  sf_mex_addfield(*c52_info, c52_b_emlrt_marshallOut(0U), "mFileTimeLo",
                  "mFileTimeLo", 12);
  sf_mex_addfield(*c52_info, c52_b_emlrt_marshallOut(0U), "mFileTimeHi",
                  "mFileTimeHi", 12);
  sf_mex_assign(&c52_rhs12, sf_mex_createcellmatrix(0, 1), false);
  sf_mex_assign(&c52_lhs12, sf_mex_createcellmatrix(0, 1), false);
  sf_mex_addfield(*c52_info, sf_mex_duplicatearraysafe(&c52_rhs12), "rhs", "rhs",
                  12);
  sf_mex_addfield(*c52_info, sf_mex_duplicatearraysafe(&c52_lhs12), "lhs", "lhs",
                  12);
  sf_mex_addfield(*c52_info, c52_emlrt_marshallOut(
    "[ILXE]$matlabroot$/toolbox/coder/coder/+coder/+internal/+refblas/xnrm2.p"),
                  "context", "context", 13);
  sf_mex_addfield(*c52_info, c52_emlrt_marshallOut("realmin"), "name", "name",
                  13);
  sf_mex_addfield(*c52_info, c52_emlrt_marshallOut("char"), "dominantType",
                  "dominantType", 13);
  sf_mex_addfield(*c52_info, c52_emlrt_marshallOut(
    "[ILXE]$matlabroot$/toolbox/eml/lib/matlab/elmat/realmin.m"), "resolved",
                  "resolved", 13);
  sf_mex_addfield(*c52_info, c52_b_emlrt_marshallOut(1307654842U), "fileTimeLo",
                  "fileTimeLo", 13);
  sf_mex_addfield(*c52_info, c52_b_emlrt_marshallOut(0U), "fileTimeHi",
                  "fileTimeHi", 13);
  sf_mex_addfield(*c52_info, c52_b_emlrt_marshallOut(0U), "mFileTimeLo",
                  "mFileTimeLo", 13);
  sf_mex_addfield(*c52_info, c52_b_emlrt_marshallOut(0U), "mFileTimeHi",
                  "mFileTimeHi", 13);
  sf_mex_assign(&c52_rhs13, sf_mex_createcellmatrix(0, 1), false);
  sf_mex_assign(&c52_lhs13, sf_mex_createcellmatrix(0, 1), false);
  sf_mex_addfield(*c52_info, sf_mex_duplicatearraysafe(&c52_rhs13), "rhs", "rhs",
                  13);
  sf_mex_addfield(*c52_info, sf_mex_duplicatearraysafe(&c52_lhs13), "lhs", "lhs",
                  13);
  sf_mex_addfield(*c52_info, c52_emlrt_marshallOut(
    "[ILXE]$matlabroot$/toolbox/eml/lib/matlab/elmat/realmin.m"), "context",
                  "context", 14);
  sf_mex_addfield(*c52_info, c52_emlrt_marshallOut("eml_realmin"), "name",
                  "name", 14);
  sf_mex_addfield(*c52_info, c52_emlrt_marshallOut("char"), "dominantType",
                  "dominantType", 14);
  sf_mex_addfield(*c52_info, c52_emlrt_marshallOut(
    "[ILXE]$matlabroot$/toolbox/eml/lib/matlab/eml/eml_realmin.m"), "resolved",
                  "resolved", 14);
  sf_mex_addfield(*c52_info, c52_b_emlrt_marshallOut(1307654844U), "fileTimeLo",
                  "fileTimeLo", 14);
  sf_mex_addfield(*c52_info, c52_b_emlrt_marshallOut(0U), "fileTimeHi",
                  "fileTimeHi", 14);
  sf_mex_addfield(*c52_info, c52_b_emlrt_marshallOut(0U), "mFileTimeLo",
                  "mFileTimeLo", 14);
  sf_mex_addfield(*c52_info, c52_b_emlrt_marshallOut(0U), "mFileTimeHi",
                  "mFileTimeHi", 14);
  sf_mex_assign(&c52_rhs14, sf_mex_createcellmatrix(0, 1), false);
  sf_mex_assign(&c52_lhs14, sf_mex_createcellmatrix(0, 1), false);
  sf_mex_addfield(*c52_info, sf_mex_duplicatearraysafe(&c52_rhs14), "rhs", "rhs",
                  14);
  sf_mex_addfield(*c52_info, sf_mex_duplicatearraysafe(&c52_lhs14), "lhs", "lhs",
                  14);
  sf_mex_addfield(*c52_info, c52_emlrt_marshallOut(
    "[ILXE]$matlabroot$/toolbox/eml/lib/matlab/eml/eml_realmin.m"), "context",
                  "context", 15);
  sf_mex_addfield(*c52_info, c52_emlrt_marshallOut("eml_float_model"), "name",
                  "name", 15);
  sf_mex_addfield(*c52_info, c52_emlrt_marshallOut("char"), "dominantType",
                  "dominantType", 15);
  sf_mex_addfield(*c52_info, c52_emlrt_marshallOut(
    "[ILXE]$matlabroot$/toolbox/eml/lib/matlab/eml/eml_float_model.m"),
                  "resolved", "resolved", 15);
  sf_mex_addfield(*c52_info, c52_b_emlrt_marshallOut(1326731596U), "fileTimeLo",
                  "fileTimeLo", 15);
  sf_mex_addfield(*c52_info, c52_b_emlrt_marshallOut(0U), "fileTimeHi",
                  "fileTimeHi", 15);
  sf_mex_addfield(*c52_info, c52_b_emlrt_marshallOut(0U), "mFileTimeLo",
                  "mFileTimeLo", 15);
  sf_mex_addfield(*c52_info, c52_b_emlrt_marshallOut(0U), "mFileTimeHi",
                  "mFileTimeHi", 15);
  sf_mex_assign(&c52_rhs15, sf_mex_createcellmatrix(0, 1), false);
  sf_mex_assign(&c52_lhs15, sf_mex_createcellmatrix(0, 1), false);
  sf_mex_addfield(*c52_info, sf_mex_duplicatearraysafe(&c52_rhs15), "rhs", "rhs",
                  15);
  sf_mex_addfield(*c52_info, sf_mex_duplicatearraysafe(&c52_lhs15), "lhs", "lhs",
                  15);
  sf_mex_addfield(*c52_info, c52_emlrt_marshallOut(
    "[ILXE]$matlabroot$/toolbox/coder/coder/+coder/+internal/+refblas/xnrm2.p"),
                  "context", "context", 16);
  sf_mex_addfield(*c52_info, c52_emlrt_marshallOut("coder.internal.indexMinus"),
                  "name", "name", 16);
  sf_mex_addfield(*c52_info, c52_emlrt_marshallOut("double"), "dominantType",
                  "dominantType", 16);
  sf_mex_addfield(*c52_info, c52_emlrt_marshallOut(
    "[ILXE]$matlabroot$/toolbox/shared/coder/coder/+coder/+internal/indexMinus.m"),
                  "resolved", "resolved", 16);
  sf_mex_addfield(*c52_info, c52_b_emlrt_marshallOut(1372586760U), "fileTimeLo",
                  "fileTimeLo", 16);
  sf_mex_addfield(*c52_info, c52_b_emlrt_marshallOut(0U), "fileTimeHi",
                  "fileTimeHi", 16);
  sf_mex_addfield(*c52_info, c52_b_emlrt_marshallOut(0U), "mFileTimeLo",
                  "mFileTimeLo", 16);
  sf_mex_addfield(*c52_info, c52_b_emlrt_marshallOut(0U), "mFileTimeHi",
                  "mFileTimeHi", 16);
  sf_mex_assign(&c52_rhs16, sf_mex_createcellmatrix(0, 1), false);
  sf_mex_assign(&c52_lhs16, sf_mex_createcellmatrix(0, 1), false);
  sf_mex_addfield(*c52_info, sf_mex_duplicatearraysafe(&c52_rhs16), "rhs", "rhs",
                  16);
  sf_mex_addfield(*c52_info, sf_mex_duplicatearraysafe(&c52_lhs16), "lhs", "lhs",
                  16);
  sf_mex_addfield(*c52_info, c52_emlrt_marshallOut(
    "[ILXE]$matlabroot$/toolbox/coder/coder/+coder/+internal/+refblas/xnrm2.p"),
                  "context", "context", 17);
  sf_mex_addfield(*c52_info, c52_emlrt_marshallOut("coder.internal.indexTimes"),
                  "name", "name", 17);
  sf_mex_addfield(*c52_info, c52_emlrt_marshallOut("coder.internal.indexInt"),
                  "dominantType", "dominantType", 17);
  sf_mex_addfield(*c52_info, c52_emlrt_marshallOut(
    "[ILXE]$matlabroot$/toolbox/shared/coder/coder/+coder/+internal/indexTimes.m"),
                  "resolved", "resolved", 17);
  sf_mex_addfield(*c52_info, c52_b_emlrt_marshallOut(1372586760U), "fileTimeLo",
                  "fileTimeLo", 17);
  sf_mex_addfield(*c52_info, c52_b_emlrt_marshallOut(0U), "fileTimeHi",
                  "fileTimeHi", 17);
  sf_mex_addfield(*c52_info, c52_b_emlrt_marshallOut(0U), "mFileTimeLo",
                  "mFileTimeLo", 17);
  sf_mex_addfield(*c52_info, c52_b_emlrt_marshallOut(0U), "mFileTimeHi",
                  "mFileTimeHi", 17);
  sf_mex_assign(&c52_rhs17, sf_mex_createcellmatrix(0, 1), false);
  sf_mex_assign(&c52_lhs17, sf_mex_createcellmatrix(0, 1), false);
  sf_mex_addfield(*c52_info, sf_mex_duplicatearraysafe(&c52_rhs17), "rhs", "rhs",
                  17);
  sf_mex_addfield(*c52_info, sf_mex_duplicatearraysafe(&c52_lhs17), "lhs", "lhs",
                  17);
  sf_mex_addfield(*c52_info, c52_emlrt_marshallOut(
    "[ILXE]$matlabroot$/toolbox/coder/coder/+coder/+internal/+refblas/xnrm2.p"),
                  "context", "context", 18);
  sf_mex_addfield(*c52_info, c52_emlrt_marshallOut("coder.internal.indexPlus"),
                  "name", "name", 18);
  sf_mex_addfield(*c52_info, c52_emlrt_marshallOut("coder.internal.indexInt"),
                  "dominantType", "dominantType", 18);
  sf_mex_addfield(*c52_info, c52_emlrt_marshallOut(
    "[ILXE]$matlabroot$/toolbox/shared/coder/coder/+coder/+internal/indexPlus.m"),
                  "resolved", "resolved", 18);
  sf_mex_addfield(*c52_info, c52_b_emlrt_marshallOut(1372586760U), "fileTimeLo",
                  "fileTimeLo", 18);
  sf_mex_addfield(*c52_info, c52_b_emlrt_marshallOut(0U), "fileTimeHi",
                  "fileTimeHi", 18);
  sf_mex_addfield(*c52_info, c52_b_emlrt_marshallOut(0U), "mFileTimeLo",
                  "mFileTimeLo", 18);
  sf_mex_addfield(*c52_info, c52_b_emlrt_marshallOut(0U), "mFileTimeHi",
                  "mFileTimeHi", 18);
  sf_mex_assign(&c52_rhs18, sf_mex_createcellmatrix(0, 1), false);
  sf_mex_assign(&c52_lhs18, sf_mex_createcellmatrix(0, 1), false);
  sf_mex_addfield(*c52_info, sf_mex_duplicatearraysafe(&c52_rhs18), "rhs", "rhs",
                  18);
  sf_mex_addfield(*c52_info, sf_mex_duplicatearraysafe(&c52_lhs18), "lhs", "lhs",
                  18);
  sf_mex_addfield(*c52_info, c52_emlrt_marshallOut(
    "[ILXE]$matlabroot$/toolbox/coder/coder/+coder/+internal/+refblas/xnrm2.p"),
                  "context", "context", 19);
  sf_mex_addfield(*c52_info, c52_emlrt_marshallOut(
    "eml_int_forloop_overflow_check"), "name", "name", 19);
  sf_mex_addfield(*c52_info, c52_emlrt_marshallOut(""), "dominantType",
                  "dominantType", 19);
  sf_mex_addfield(*c52_info, c52_emlrt_marshallOut(
    "[ILXE]$matlabroot$/toolbox/eml/lib/matlab/eml/eml_int_forloop_overflow_check.m"),
                  "resolved", "resolved", 19);
  sf_mex_addfield(*c52_info, c52_b_emlrt_marshallOut(1397261022U), "fileTimeLo",
                  "fileTimeLo", 19);
  sf_mex_addfield(*c52_info, c52_b_emlrt_marshallOut(0U), "fileTimeHi",
                  "fileTimeHi", 19);
  sf_mex_addfield(*c52_info, c52_b_emlrt_marshallOut(0U), "mFileTimeLo",
                  "mFileTimeLo", 19);
  sf_mex_addfield(*c52_info, c52_b_emlrt_marshallOut(0U), "mFileTimeHi",
                  "mFileTimeHi", 19);
  sf_mex_assign(&c52_rhs19, sf_mex_createcellmatrix(0, 1), false);
  sf_mex_assign(&c52_lhs19, sf_mex_createcellmatrix(0, 1), false);
  sf_mex_addfield(*c52_info, sf_mex_duplicatearraysafe(&c52_rhs19), "rhs", "rhs",
                  19);
  sf_mex_addfield(*c52_info, sf_mex_duplicatearraysafe(&c52_lhs19), "lhs", "lhs",
                  19);
  sf_mex_addfield(*c52_info, c52_emlrt_marshallOut(
    "[ILXE]$matlabroot$/toolbox/eml/lib/matlab/eml/eml_int_forloop_overflow_check.m!eml_int_forloop_overflow_check_helper"),
                  "context", "context", 20);
  sf_mex_addfield(*c52_info, c52_emlrt_marshallOut("isfi"), "name", "name", 20);
  sf_mex_addfield(*c52_info, c52_emlrt_marshallOut("coder.internal.indexInt"),
                  "dominantType", "dominantType", 20);
  sf_mex_addfield(*c52_info, c52_emlrt_marshallOut(
    "[ILXE]$matlabroot$/toolbox/eml/lib/fixedpoint/isfi.m"), "resolved",
                  "resolved", 20);
  sf_mex_addfield(*c52_info, c52_b_emlrt_marshallOut(1346513958U), "fileTimeLo",
                  "fileTimeLo", 20);
  sf_mex_addfield(*c52_info, c52_b_emlrt_marshallOut(0U), "fileTimeHi",
                  "fileTimeHi", 20);
  sf_mex_addfield(*c52_info, c52_b_emlrt_marshallOut(0U), "mFileTimeLo",
                  "mFileTimeLo", 20);
  sf_mex_addfield(*c52_info, c52_b_emlrt_marshallOut(0U), "mFileTimeHi",
                  "mFileTimeHi", 20);
  sf_mex_assign(&c52_rhs20, sf_mex_createcellmatrix(0, 1), false);
  sf_mex_assign(&c52_lhs20, sf_mex_createcellmatrix(0, 1), false);
  sf_mex_addfield(*c52_info, sf_mex_duplicatearraysafe(&c52_rhs20), "rhs", "rhs",
                  20);
  sf_mex_addfield(*c52_info, sf_mex_duplicatearraysafe(&c52_lhs20), "lhs", "lhs",
                  20);
  sf_mex_addfield(*c52_info, c52_emlrt_marshallOut(
    "[ILXE]$matlabroot$/toolbox/eml/lib/fixedpoint/isfi.m"), "context",
                  "context", 21);
  sf_mex_addfield(*c52_info, c52_emlrt_marshallOut("isnumerictype"), "name",
                  "name", 21);
  sf_mex_addfield(*c52_info, c52_emlrt_marshallOut("char"), "dominantType",
                  "dominantType", 21);
  sf_mex_addfield(*c52_info, c52_emlrt_marshallOut(
    "[ILXE]$matlabroot$/toolbox/eml/lib/fixedpoint/isnumerictype.m"), "resolved",
                  "resolved", 21);
  sf_mex_addfield(*c52_info, c52_b_emlrt_marshallOut(1398879198U), "fileTimeLo",
                  "fileTimeLo", 21);
  sf_mex_addfield(*c52_info, c52_b_emlrt_marshallOut(0U), "fileTimeHi",
                  "fileTimeHi", 21);
  sf_mex_addfield(*c52_info, c52_b_emlrt_marshallOut(0U), "mFileTimeLo",
                  "mFileTimeLo", 21);
  sf_mex_addfield(*c52_info, c52_b_emlrt_marshallOut(0U), "mFileTimeHi",
                  "mFileTimeHi", 21);
  sf_mex_assign(&c52_rhs21, sf_mex_createcellmatrix(0, 1), false);
  sf_mex_assign(&c52_lhs21, sf_mex_createcellmatrix(0, 1), false);
  sf_mex_addfield(*c52_info, sf_mex_duplicatearraysafe(&c52_rhs21), "rhs", "rhs",
                  21);
  sf_mex_addfield(*c52_info, sf_mex_duplicatearraysafe(&c52_lhs21), "lhs", "lhs",
                  21);
  sf_mex_addfield(*c52_info, c52_emlrt_marshallOut(
    "[ILXE]$matlabroot$/toolbox/eml/lib/matlab/eml/eml_int_forloop_overflow_check.m!eml_int_forloop_overflow_check_helper"),
                  "context", "context", 22);
  sf_mex_addfield(*c52_info, c52_emlrt_marshallOut("intmax"), "name", "name", 22);
  sf_mex_addfield(*c52_info, c52_emlrt_marshallOut("char"), "dominantType",
                  "dominantType", 22);
  sf_mex_addfield(*c52_info, c52_emlrt_marshallOut(
    "[ILXE]$matlabroot$/toolbox/eml/lib/matlab/elmat/intmax.m"), "resolved",
                  "resolved", 22);
  sf_mex_addfield(*c52_info, c52_b_emlrt_marshallOut(1362265482U), "fileTimeLo",
                  "fileTimeLo", 22);
  sf_mex_addfield(*c52_info, c52_b_emlrt_marshallOut(0U), "fileTimeHi",
                  "fileTimeHi", 22);
  sf_mex_addfield(*c52_info, c52_b_emlrt_marshallOut(0U), "mFileTimeLo",
                  "mFileTimeLo", 22);
  sf_mex_addfield(*c52_info, c52_b_emlrt_marshallOut(0U), "mFileTimeHi",
                  "mFileTimeHi", 22);
  sf_mex_assign(&c52_rhs22, sf_mex_createcellmatrix(0, 1), false);
  sf_mex_assign(&c52_lhs22, sf_mex_createcellmatrix(0, 1), false);
  sf_mex_addfield(*c52_info, sf_mex_duplicatearraysafe(&c52_rhs22), "rhs", "rhs",
                  22);
  sf_mex_addfield(*c52_info, sf_mex_duplicatearraysafe(&c52_lhs22), "lhs", "lhs",
                  22);
  sf_mex_addfield(*c52_info, c52_emlrt_marshallOut(
    "[ILXE]$matlabroot$/toolbox/eml/lib/matlab/elmat/intmax.m"), "context",
                  "context", 23);
  sf_mex_addfield(*c52_info, c52_emlrt_marshallOut("eml_switch_helper"), "name",
                  "name", 23);
  sf_mex_addfield(*c52_info, c52_emlrt_marshallOut(""), "dominantType",
                  "dominantType", 23);
  sf_mex_addfield(*c52_info, c52_emlrt_marshallOut(
    "[ILXE]$matlabroot$/toolbox/eml/lib/matlab/eml/eml_switch_helper.m"),
                  "resolved", "resolved", 23);
  sf_mex_addfield(*c52_info, c52_b_emlrt_marshallOut(1393334458U), "fileTimeLo",
                  "fileTimeLo", 23);
  sf_mex_addfield(*c52_info, c52_b_emlrt_marshallOut(0U), "fileTimeHi",
                  "fileTimeHi", 23);
  sf_mex_addfield(*c52_info, c52_b_emlrt_marshallOut(0U), "mFileTimeLo",
                  "mFileTimeLo", 23);
  sf_mex_addfield(*c52_info, c52_b_emlrt_marshallOut(0U), "mFileTimeHi",
                  "mFileTimeHi", 23);
  sf_mex_assign(&c52_rhs23, sf_mex_createcellmatrix(0, 1), false);
  sf_mex_assign(&c52_lhs23, sf_mex_createcellmatrix(0, 1), false);
  sf_mex_addfield(*c52_info, sf_mex_duplicatearraysafe(&c52_rhs23), "rhs", "rhs",
                  23);
  sf_mex_addfield(*c52_info, sf_mex_duplicatearraysafe(&c52_lhs23), "lhs", "lhs",
                  23);
  sf_mex_addfield(*c52_info, c52_emlrt_marshallOut(
    "[ILXE]$matlabroot$/toolbox/eml/lib/matlab/eml/eml_int_forloop_overflow_check.m!eml_int_forloop_overflow_check_helper"),
                  "context", "context", 24);
  sf_mex_addfield(*c52_info, c52_emlrt_marshallOut("intmin"), "name", "name", 24);
  sf_mex_addfield(*c52_info, c52_emlrt_marshallOut("char"), "dominantType",
                  "dominantType", 24);
  sf_mex_addfield(*c52_info, c52_emlrt_marshallOut(
    "[ILXE]$matlabroot$/toolbox/eml/lib/matlab/elmat/intmin.m"), "resolved",
                  "resolved", 24);
  sf_mex_addfield(*c52_info, c52_b_emlrt_marshallOut(1362265482U), "fileTimeLo",
                  "fileTimeLo", 24);
  sf_mex_addfield(*c52_info, c52_b_emlrt_marshallOut(0U), "fileTimeHi",
                  "fileTimeHi", 24);
  sf_mex_addfield(*c52_info, c52_b_emlrt_marshallOut(0U), "mFileTimeLo",
                  "mFileTimeLo", 24);
  sf_mex_addfield(*c52_info, c52_b_emlrt_marshallOut(0U), "mFileTimeHi",
                  "mFileTimeHi", 24);
  sf_mex_assign(&c52_rhs24, sf_mex_createcellmatrix(0, 1), false);
  sf_mex_assign(&c52_lhs24, sf_mex_createcellmatrix(0, 1), false);
  sf_mex_addfield(*c52_info, sf_mex_duplicatearraysafe(&c52_rhs24), "rhs", "rhs",
                  24);
  sf_mex_addfield(*c52_info, sf_mex_duplicatearraysafe(&c52_lhs24), "lhs", "lhs",
                  24);
  sf_mex_addfield(*c52_info, c52_emlrt_marshallOut(
    "[ILXE]$matlabroot$/toolbox/eml/lib/matlab/elmat/intmin.m"), "context",
                  "context", 25);
  sf_mex_addfield(*c52_info, c52_emlrt_marshallOut("eml_switch_helper"), "name",
                  "name", 25);
  sf_mex_addfield(*c52_info, c52_emlrt_marshallOut(""), "dominantType",
                  "dominantType", 25);
  sf_mex_addfield(*c52_info, c52_emlrt_marshallOut(
    "[ILXE]$matlabroot$/toolbox/eml/lib/matlab/eml/eml_switch_helper.m"),
                  "resolved", "resolved", 25);
  sf_mex_addfield(*c52_info, c52_b_emlrt_marshallOut(1393334458U), "fileTimeLo",
                  "fileTimeLo", 25);
  sf_mex_addfield(*c52_info, c52_b_emlrt_marshallOut(0U), "fileTimeHi",
                  "fileTimeHi", 25);
  sf_mex_addfield(*c52_info, c52_b_emlrt_marshallOut(0U), "mFileTimeLo",
                  "mFileTimeLo", 25);
  sf_mex_addfield(*c52_info, c52_b_emlrt_marshallOut(0U), "mFileTimeHi",
                  "mFileTimeHi", 25);
  sf_mex_assign(&c52_rhs25, sf_mex_createcellmatrix(0, 1), false);
  sf_mex_assign(&c52_lhs25, sf_mex_createcellmatrix(0, 1), false);
  sf_mex_addfield(*c52_info, sf_mex_duplicatearraysafe(&c52_rhs25), "rhs", "rhs",
                  25);
  sf_mex_addfield(*c52_info, sf_mex_duplicatearraysafe(&c52_lhs25), "lhs", "lhs",
                  25);
  sf_mex_addfield(*c52_info, c52_emlrt_marshallOut(
    "[ILXE]$matlabroot$/toolbox/coder/coder/+coder/+internal/+refblas/xnrm2.p"),
                  "context", "context", 26);
  sf_mex_addfield(*c52_info, c52_emlrt_marshallOut("abs"), "name", "name", 26);
  sf_mex_addfield(*c52_info, c52_emlrt_marshallOut("single"), "dominantType",
                  "dominantType", 26);
  sf_mex_addfield(*c52_info, c52_emlrt_marshallOut(
    "[ILXE]$matlabroot$/toolbox/eml/lib/matlab/elfun/abs.m"), "resolved",
                  "resolved", 26);
  sf_mex_addfield(*c52_info, c52_b_emlrt_marshallOut(1363717452U), "fileTimeLo",
                  "fileTimeLo", 26);
  sf_mex_addfield(*c52_info, c52_b_emlrt_marshallOut(0U), "fileTimeHi",
                  "fileTimeHi", 26);
  sf_mex_addfield(*c52_info, c52_b_emlrt_marshallOut(0U), "mFileTimeLo",
                  "mFileTimeLo", 26);
  sf_mex_addfield(*c52_info, c52_b_emlrt_marshallOut(0U), "mFileTimeHi",
                  "mFileTimeHi", 26);
  sf_mex_assign(&c52_rhs26, sf_mex_createcellmatrix(0, 1), false);
  sf_mex_assign(&c52_lhs26, sf_mex_createcellmatrix(0, 1), false);
  sf_mex_addfield(*c52_info, sf_mex_duplicatearraysafe(&c52_rhs26), "rhs", "rhs",
                  26);
  sf_mex_addfield(*c52_info, sf_mex_duplicatearraysafe(&c52_lhs26), "lhs", "lhs",
                  26);
  sf_mex_addfield(*c52_info, c52_emlrt_marshallOut(
    "[ILXE]$matlabroot$/toolbox/eml/lib/matlab/elfun/abs.m"), "context",
                  "context", 27);
  sf_mex_addfield(*c52_info, c52_emlrt_marshallOut(
    "coder.internal.isBuiltInNumeric"), "name", "name", 27);
  sf_mex_addfield(*c52_info, c52_emlrt_marshallOut("single"), "dominantType",
                  "dominantType", 27);
  sf_mex_addfield(*c52_info, c52_emlrt_marshallOut(
    "[ILXE]$matlabroot$/toolbox/shared/coder/coder/+coder/+internal/isBuiltInNumeric.m"),
                  "resolved", "resolved", 27);
  sf_mex_addfield(*c52_info, c52_b_emlrt_marshallOut(1395935456U), "fileTimeLo",
                  "fileTimeLo", 27);
  sf_mex_addfield(*c52_info, c52_b_emlrt_marshallOut(0U), "fileTimeHi",
                  "fileTimeHi", 27);
  sf_mex_addfield(*c52_info, c52_b_emlrt_marshallOut(0U), "mFileTimeLo",
                  "mFileTimeLo", 27);
  sf_mex_addfield(*c52_info, c52_b_emlrt_marshallOut(0U), "mFileTimeHi",
                  "mFileTimeHi", 27);
  sf_mex_assign(&c52_rhs27, sf_mex_createcellmatrix(0, 1), false);
  sf_mex_assign(&c52_lhs27, sf_mex_createcellmatrix(0, 1), false);
  sf_mex_addfield(*c52_info, sf_mex_duplicatearraysafe(&c52_rhs27), "rhs", "rhs",
                  27);
  sf_mex_addfield(*c52_info, sf_mex_duplicatearraysafe(&c52_lhs27), "lhs", "lhs",
                  27);
  sf_mex_addfield(*c52_info, c52_emlrt_marshallOut(
    "[ILXE]$matlabroot$/toolbox/eml/lib/matlab/elfun/abs.m"), "context",
                  "context", 28);
  sf_mex_addfield(*c52_info, c52_emlrt_marshallOut("eml_scalar_abs"), "name",
                  "name", 28);
  sf_mex_addfield(*c52_info, c52_emlrt_marshallOut("single"), "dominantType",
                  "dominantType", 28);
  sf_mex_addfield(*c52_info, c52_emlrt_marshallOut(
    "[ILXE]$matlabroot$/toolbox/eml/lib/matlab/elfun/eml_scalar_abs.m"),
                  "resolved", "resolved", 28);
  sf_mex_addfield(*c52_info, c52_b_emlrt_marshallOut(1286822312U), "fileTimeLo",
                  "fileTimeLo", 28);
  sf_mex_addfield(*c52_info, c52_b_emlrt_marshallOut(0U), "fileTimeHi",
                  "fileTimeHi", 28);
  sf_mex_addfield(*c52_info, c52_b_emlrt_marshallOut(0U), "mFileTimeLo",
                  "mFileTimeLo", 28);
  sf_mex_addfield(*c52_info, c52_b_emlrt_marshallOut(0U), "mFileTimeHi",
                  "mFileTimeHi", 28);
  sf_mex_assign(&c52_rhs28, sf_mex_createcellmatrix(0, 1), false);
  sf_mex_assign(&c52_lhs28, sf_mex_createcellmatrix(0, 1), false);
  sf_mex_addfield(*c52_info, sf_mex_duplicatearraysafe(&c52_rhs28), "rhs", "rhs",
                  28);
  sf_mex_addfield(*c52_info, sf_mex_duplicatearraysafe(&c52_lhs28), "lhs", "lhs",
                  28);
  sf_mex_addfield(*c52_info, c52_emlrt_marshallOut(""), "context", "context", 29);
  sf_mex_addfield(*c52_info, c52_emlrt_marshallOut("mrdivide"), "name", "name",
                  29);
  sf_mex_addfield(*c52_info, c52_emlrt_marshallOut("single"), "dominantType",
                  "dominantType", 29);
  sf_mex_addfield(*c52_info, c52_emlrt_marshallOut(
    "[ILXE]$matlabroot$/toolbox/eml/lib/matlab/ops/mrdivide.p"), "resolved",
                  "resolved", 29);
  sf_mex_addfield(*c52_info, c52_b_emlrt_marshallOut(1410811248U), "fileTimeLo",
                  "fileTimeLo", 29);
  sf_mex_addfield(*c52_info, c52_b_emlrt_marshallOut(0U), "fileTimeHi",
                  "fileTimeHi", 29);
  sf_mex_addfield(*c52_info, c52_b_emlrt_marshallOut(1370013486U), "mFileTimeLo",
                  "mFileTimeLo", 29);
  sf_mex_addfield(*c52_info, c52_b_emlrt_marshallOut(0U), "mFileTimeHi",
                  "mFileTimeHi", 29);
  sf_mex_assign(&c52_rhs29, sf_mex_createcellmatrix(0, 1), false);
  sf_mex_assign(&c52_lhs29, sf_mex_createcellmatrix(0, 1), false);
  sf_mex_addfield(*c52_info, sf_mex_duplicatearraysafe(&c52_rhs29), "rhs", "rhs",
                  29);
  sf_mex_addfield(*c52_info, sf_mex_duplicatearraysafe(&c52_lhs29), "lhs", "lhs",
                  29);
  sf_mex_addfield(*c52_info, c52_emlrt_marshallOut(
    "[ILXE]$matlabroot$/toolbox/eml/lib/matlab/ops/mrdivide.p"), "context",
                  "context", 30);
  sf_mex_addfield(*c52_info, c52_emlrt_marshallOut("coder.internal.assert"),
                  "name", "name", 30);
  sf_mex_addfield(*c52_info, c52_emlrt_marshallOut("char"), "dominantType",
                  "dominantType", 30);
  sf_mex_addfield(*c52_info, c52_emlrt_marshallOut(
    "[ILXE]$matlabroot$/toolbox/shared/coder/coder/+coder/+internal/assert.m"),
                  "resolved", "resolved", 30);
  sf_mex_addfield(*c52_info, c52_b_emlrt_marshallOut(1389721374U), "fileTimeLo",
                  "fileTimeLo", 30);
  sf_mex_addfield(*c52_info, c52_b_emlrt_marshallOut(0U), "fileTimeHi",
                  "fileTimeHi", 30);
  sf_mex_addfield(*c52_info, c52_b_emlrt_marshallOut(0U), "mFileTimeLo",
                  "mFileTimeLo", 30);
  sf_mex_addfield(*c52_info, c52_b_emlrt_marshallOut(0U), "mFileTimeHi",
                  "mFileTimeHi", 30);
  sf_mex_assign(&c52_rhs30, sf_mex_createcellmatrix(0, 1), false);
  sf_mex_assign(&c52_lhs30, sf_mex_createcellmatrix(0, 1), false);
  sf_mex_addfield(*c52_info, sf_mex_duplicatearraysafe(&c52_rhs30), "rhs", "rhs",
                  30);
  sf_mex_addfield(*c52_info, sf_mex_duplicatearraysafe(&c52_lhs30), "lhs", "lhs",
                  30);
  sf_mex_addfield(*c52_info, c52_emlrt_marshallOut(
    "[ILXE]$matlabroot$/toolbox/eml/lib/matlab/ops/mrdivide.p"), "context",
                  "context", 31);
  sf_mex_addfield(*c52_info, c52_emlrt_marshallOut("rdivide"), "name", "name",
                  31);
  sf_mex_addfield(*c52_info, c52_emlrt_marshallOut("single"), "dominantType",
                  "dominantType", 31);
  sf_mex_addfield(*c52_info, c52_emlrt_marshallOut(
    "[ILXE]$matlabroot$/toolbox/eml/lib/matlab/ops/rdivide.m"), "resolved",
                  "resolved", 31);
  sf_mex_addfield(*c52_info, c52_b_emlrt_marshallOut(1363717480U), "fileTimeLo",
                  "fileTimeLo", 31);
  sf_mex_addfield(*c52_info, c52_b_emlrt_marshallOut(0U), "fileTimeHi",
                  "fileTimeHi", 31);
  sf_mex_addfield(*c52_info, c52_b_emlrt_marshallOut(0U), "mFileTimeLo",
                  "mFileTimeLo", 31);
  sf_mex_addfield(*c52_info, c52_b_emlrt_marshallOut(0U), "mFileTimeHi",
                  "mFileTimeHi", 31);
  sf_mex_assign(&c52_rhs31, sf_mex_createcellmatrix(0, 1), false);
  sf_mex_assign(&c52_lhs31, sf_mex_createcellmatrix(0, 1), false);
  sf_mex_addfield(*c52_info, sf_mex_duplicatearraysafe(&c52_rhs31), "rhs", "rhs",
                  31);
  sf_mex_addfield(*c52_info, sf_mex_duplicatearraysafe(&c52_lhs31), "lhs", "lhs",
                  31);
  sf_mex_addfield(*c52_info, c52_emlrt_marshallOut(
    "[ILXE]$matlabroot$/toolbox/eml/lib/matlab/ops/rdivide.m"), "context",
                  "context", 32);
  sf_mex_addfield(*c52_info, c52_emlrt_marshallOut(
    "coder.internal.isBuiltInNumeric"), "name", "name", 32);
  sf_mex_addfield(*c52_info, c52_emlrt_marshallOut("single"), "dominantType",
                  "dominantType", 32);
  sf_mex_addfield(*c52_info, c52_emlrt_marshallOut(
    "[ILXE]$matlabroot$/toolbox/shared/coder/coder/+coder/+internal/isBuiltInNumeric.m"),
                  "resolved", "resolved", 32);
  sf_mex_addfield(*c52_info, c52_b_emlrt_marshallOut(1395935456U), "fileTimeLo",
                  "fileTimeLo", 32);
  sf_mex_addfield(*c52_info, c52_b_emlrt_marshallOut(0U), "fileTimeHi",
                  "fileTimeHi", 32);
  sf_mex_addfield(*c52_info, c52_b_emlrt_marshallOut(0U), "mFileTimeLo",
                  "mFileTimeLo", 32);
  sf_mex_addfield(*c52_info, c52_b_emlrt_marshallOut(0U), "mFileTimeHi",
                  "mFileTimeHi", 32);
  sf_mex_assign(&c52_rhs32, sf_mex_createcellmatrix(0, 1), false);
  sf_mex_assign(&c52_lhs32, sf_mex_createcellmatrix(0, 1), false);
  sf_mex_addfield(*c52_info, sf_mex_duplicatearraysafe(&c52_rhs32), "rhs", "rhs",
                  32);
  sf_mex_addfield(*c52_info, sf_mex_duplicatearraysafe(&c52_lhs32), "lhs", "lhs",
                  32);
  sf_mex_addfield(*c52_info, c52_emlrt_marshallOut(
    "[ILXE]$matlabroot$/toolbox/eml/lib/matlab/ops/rdivide.m"), "context",
                  "context", 33);
  sf_mex_addfield(*c52_info, c52_emlrt_marshallOut("eml_scalexp_compatible"),
                  "name", "name", 33);
  sf_mex_addfield(*c52_info, c52_emlrt_marshallOut("single"), "dominantType",
                  "dominantType", 33);
  sf_mex_addfield(*c52_info, c52_emlrt_marshallOut(
    "[ILXE]$matlabroot$/toolbox/eml/lib/matlab/eml/eml_scalexp_compatible.m"),
                  "resolved", "resolved", 33);
  sf_mex_addfield(*c52_info, c52_b_emlrt_marshallOut(1286822396U), "fileTimeLo",
                  "fileTimeLo", 33);
  sf_mex_addfield(*c52_info, c52_b_emlrt_marshallOut(0U), "fileTimeHi",
                  "fileTimeHi", 33);
  sf_mex_addfield(*c52_info, c52_b_emlrt_marshallOut(0U), "mFileTimeLo",
                  "mFileTimeLo", 33);
  sf_mex_addfield(*c52_info, c52_b_emlrt_marshallOut(0U), "mFileTimeHi",
                  "mFileTimeHi", 33);
  sf_mex_assign(&c52_rhs33, sf_mex_createcellmatrix(0, 1), false);
  sf_mex_assign(&c52_lhs33, sf_mex_createcellmatrix(0, 1), false);
  sf_mex_addfield(*c52_info, sf_mex_duplicatearraysafe(&c52_rhs33), "rhs", "rhs",
                  33);
  sf_mex_addfield(*c52_info, sf_mex_duplicatearraysafe(&c52_lhs33), "lhs", "lhs",
                  33);
  sf_mex_addfield(*c52_info, c52_emlrt_marshallOut(
    "[ILXE]$matlabroot$/toolbox/eml/lib/matlab/ops/rdivide.m"), "context",
                  "context", 34);
  sf_mex_addfield(*c52_info, c52_emlrt_marshallOut("eml_div"), "name", "name",
                  34);
  sf_mex_addfield(*c52_info, c52_emlrt_marshallOut("single"), "dominantType",
                  "dominantType", 34);
  sf_mex_addfield(*c52_info, c52_emlrt_marshallOut(
    "[ILXE]$matlabroot$/toolbox/eml/lib/matlab/eml/eml_div.m"), "resolved",
                  "resolved", 34);
  sf_mex_addfield(*c52_info, c52_b_emlrt_marshallOut(1386427552U), "fileTimeLo",
                  "fileTimeLo", 34);
  sf_mex_addfield(*c52_info, c52_b_emlrt_marshallOut(0U), "fileTimeHi",
                  "fileTimeHi", 34);
  sf_mex_addfield(*c52_info, c52_b_emlrt_marshallOut(0U), "mFileTimeLo",
                  "mFileTimeLo", 34);
  sf_mex_addfield(*c52_info, c52_b_emlrt_marshallOut(0U), "mFileTimeHi",
                  "mFileTimeHi", 34);
  sf_mex_assign(&c52_rhs34, sf_mex_createcellmatrix(0, 1), false);
  sf_mex_assign(&c52_lhs34, sf_mex_createcellmatrix(0, 1), false);
  sf_mex_addfield(*c52_info, sf_mex_duplicatearraysafe(&c52_rhs34), "rhs", "rhs",
                  34);
  sf_mex_addfield(*c52_info, sf_mex_duplicatearraysafe(&c52_lhs34), "lhs", "lhs",
                  34);
  sf_mex_addfield(*c52_info, c52_emlrt_marshallOut(
    "[ILXE]$matlabroot$/toolbox/eml/lib/matlab/eml/eml_div.m"), "context",
                  "context", 35);
  sf_mex_addfield(*c52_info, c52_emlrt_marshallOut("coder.internal.div"), "name",
                  "name", 35);
  sf_mex_addfield(*c52_info, c52_emlrt_marshallOut("single"), "dominantType",
                  "dominantType", 35);
  sf_mex_addfield(*c52_info, c52_emlrt_marshallOut(
    "[ILXE]$matlabroot$/toolbox/coder/coder/+coder/+internal/div.p"), "resolved",
                  "resolved", 35);
  sf_mex_addfield(*c52_info, c52_b_emlrt_marshallOut(1410811370U), "fileTimeLo",
                  "fileTimeLo", 35);
  sf_mex_addfield(*c52_info, c52_b_emlrt_marshallOut(0U), "fileTimeHi",
                  "fileTimeHi", 35);
  sf_mex_addfield(*c52_info, c52_b_emlrt_marshallOut(0U), "mFileTimeLo",
                  "mFileTimeLo", 35);
  sf_mex_addfield(*c52_info, c52_b_emlrt_marshallOut(0U), "mFileTimeHi",
                  "mFileTimeHi", 35);
  sf_mex_assign(&c52_rhs35, sf_mex_createcellmatrix(0, 1), false);
  sf_mex_assign(&c52_lhs35, sf_mex_createcellmatrix(0, 1), false);
  sf_mex_addfield(*c52_info, sf_mex_duplicatearraysafe(&c52_rhs35), "rhs", "rhs",
                  35);
  sf_mex_addfield(*c52_info, sf_mex_duplicatearraysafe(&c52_lhs35), "lhs", "lhs",
                  35);
  sf_mex_addfield(*c52_info, c52_emlrt_marshallOut(""), "context", "context", 36);
  sf_mex_addfield(*c52_info, c52_emlrt_marshallOut("atan2d"), "name", "name", 36);
  sf_mex_addfield(*c52_info, c52_emlrt_marshallOut("single"), "dominantType",
                  "dominantType", 36);
  sf_mex_addfield(*c52_info, c52_emlrt_marshallOut(
    "[ILXE]$matlabroot$/toolbox/eml/lib/matlab/elfun/atan2d.m"), "resolved",
                  "resolved", 36);
  sf_mex_addfield(*c52_info, c52_b_emlrt_marshallOut(1395332096U), "fileTimeLo",
                  "fileTimeLo", 36);
  sf_mex_addfield(*c52_info, c52_b_emlrt_marshallOut(0U), "fileTimeHi",
                  "fileTimeHi", 36);
  sf_mex_addfield(*c52_info, c52_b_emlrt_marshallOut(0U), "mFileTimeLo",
                  "mFileTimeLo", 36);
  sf_mex_addfield(*c52_info, c52_b_emlrt_marshallOut(0U), "mFileTimeHi",
                  "mFileTimeHi", 36);
  sf_mex_assign(&c52_rhs36, sf_mex_createcellmatrix(0, 1), false);
  sf_mex_assign(&c52_lhs36, sf_mex_createcellmatrix(0, 1), false);
  sf_mex_addfield(*c52_info, sf_mex_duplicatearraysafe(&c52_rhs36), "rhs", "rhs",
                  36);
  sf_mex_addfield(*c52_info, sf_mex_duplicatearraysafe(&c52_lhs36), "lhs", "lhs",
                  36);
  sf_mex_addfield(*c52_info, c52_emlrt_marshallOut(
    "[ILXE]$matlabroot$/toolbox/eml/lib/matlab/elfun/atan2d.m"), "context",
                  "context", 37);
  sf_mex_addfield(*c52_info, c52_emlrt_marshallOut("eml_scalar_eg"), "name",
                  "name", 37);
  sf_mex_addfield(*c52_info, c52_emlrt_marshallOut("single"), "dominantType",
                  "dominantType", 37);
  sf_mex_addfield(*c52_info, c52_emlrt_marshallOut(
    "[ILXE]$matlabroot$/toolbox/eml/lib/matlab/eml/eml_scalar_eg.m"), "resolved",
                  "resolved", 37);
  sf_mex_addfield(*c52_info, c52_b_emlrt_marshallOut(1375984288U), "fileTimeLo",
                  "fileTimeLo", 37);
  sf_mex_addfield(*c52_info, c52_b_emlrt_marshallOut(0U), "fileTimeHi",
                  "fileTimeHi", 37);
  sf_mex_addfield(*c52_info, c52_b_emlrt_marshallOut(0U), "mFileTimeLo",
                  "mFileTimeLo", 37);
  sf_mex_addfield(*c52_info, c52_b_emlrt_marshallOut(0U), "mFileTimeHi",
                  "mFileTimeHi", 37);
  sf_mex_assign(&c52_rhs37, sf_mex_createcellmatrix(0, 1), false);
  sf_mex_assign(&c52_lhs37, sf_mex_createcellmatrix(0, 1), false);
  sf_mex_addfield(*c52_info, sf_mex_duplicatearraysafe(&c52_rhs37), "rhs", "rhs",
                  37);
  sf_mex_addfield(*c52_info, sf_mex_duplicatearraysafe(&c52_lhs37), "lhs", "lhs",
                  37);
  sf_mex_addfield(*c52_info, c52_emlrt_marshallOut(
    "[ILXE]$matlabroot$/toolbox/eml/lib/matlab/eml/eml_scalar_eg.m"), "context",
                  "context", 38);
  sf_mex_addfield(*c52_info, c52_emlrt_marshallOut("coder.internal.scalarEg"),
                  "name", "name", 38);
  sf_mex_addfield(*c52_info, c52_emlrt_marshallOut("single"), "dominantType",
                  "dominantType", 38);
  sf_mex_addfield(*c52_info, c52_emlrt_marshallOut(
    "[ILXE]$matlabroot$/toolbox/coder/coder/+coder/+internal/scalarEg.p"),
                  "resolved", "resolved", 38);
  sf_mex_addfield(*c52_info, c52_b_emlrt_marshallOut(1410811370U), "fileTimeLo",
                  "fileTimeLo", 38);
  sf_mex_addfield(*c52_info, c52_b_emlrt_marshallOut(0U), "fileTimeHi",
                  "fileTimeHi", 38);
  sf_mex_addfield(*c52_info, c52_b_emlrt_marshallOut(0U), "mFileTimeLo",
                  "mFileTimeLo", 38);
  sf_mex_addfield(*c52_info, c52_b_emlrt_marshallOut(0U), "mFileTimeHi",
                  "mFileTimeHi", 38);
  sf_mex_assign(&c52_rhs38, sf_mex_createcellmatrix(0, 1), false);
  sf_mex_assign(&c52_lhs38, sf_mex_createcellmatrix(0, 1), false);
  sf_mex_addfield(*c52_info, sf_mex_duplicatearraysafe(&c52_rhs38), "rhs", "rhs",
                  38);
  sf_mex_addfield(*c52_info, sf_mex_duplicatearraysafe(&c52_lhs38), "lhs", "lhs",
                  38);
  sf_mex_addfield(*c52_info, c52_emlrt_marshallOut(
    "[ILXE]$matlabroot$/toolbox/eml/lib/matlab/elfun/atan2d.m"), "context",
                  "context", 39);
  sf_mex_addfield(*c52_info, c52_emlrt_marshallOut("eml_scalexp_alloc"), "name",
                  "name", 39);
  sf_mex_addfield(*c52_info, c52_emlrt_marshallOut("single"), "dominantType",
                  "dominantType", 39);
  sf_mex_addfield(*c52_info, c52_emlrt_marshallOut(
    "[ILXE]$matlabroot$/toolbox/eml/lib/matlab/eml/eml_scalexp_alloc.m"),
                  "resolved", "resolved", 39);
  sf_mex_addfield(*c52_info, c52_b_emlrt_marshallOut(1375984288U), "fileTimeLo",
                  "fileTimeLo", 39);
  sf_mex_addfield(*c52_info, c52_b_emlrt_marshallOut(0U), "fileTimeHi",
                  "fileTimeHi", 39);
  sf_mex_addfield(*c52_info, c52_b_emlrt_marshallOut(0U), "mFileTimeLo",
                  "mFileTimeLo", 39);
  sf_mex_addfield(*c52_info, c52_b_emlrt_marshallOut(0U), "mFileTimeHi",
                  "mFileTimeHi", 39);
  sf_mex_assign(&c52_rhs39, sf_mex_createcellmatrix(0, 1), false);
  sf_mex_assign(&c52_lhs39, sf_mex_createcellmatrix(0, 1), false);
  sf_mex_addfield(*c52_info, sf_mex_duplicatearraysafe(&c52_rhs39), "rhs", "rhs",
                  39);
  sf_mex_addfield(*c52_info, sf_mex_duplicatearraysafe(&c52_lhs39), "lhs", "lhs",
                  39);
  sf_mex_addfield(*c52_info, c52_emlrt_marshallOut(
    "[ILXE]$matlabroot$/toolbox/eml/lib/matlab/eml/eml_scalexp_alloc.m"),
                  "context", "context", 40);
  sf_mex_addfield(*c52_info, c52_emlrt_marshallOut("coder.internal.scalexpAlloc"),
                  "name", "name", 40);
  sf_mex_addfield(*c52_info, c52_emlrt_marshallOut("single"), "dominantType",
                  "dominantType", 40);
  sf_mex_addfield(*c52_info, c52_emlrt_marshallOut(
    "[ILXE]$matlabroot$/toolbox/coder/coder/+coder/+internal/scalexpAlloc.p"),
                  "resolved", "resolved", 40);
  sf_mex_addfield(*c52_info, c52_b_emlrt_marshallOut(1410811370U), "fileTimeLo",
                  "fileTimeLo", 40);
  sf_mex_addfield(*c52_info, c52_b_emlrt_marshallOut(0U), "fileTimeHi",
                  "fileTimeHi", 40);
  sf_mex_addfield(*c52_info, c52_b_emlrt_marshallOut(0U), "mFileTimeLo",
                  "mFileTimeLo", 40);
  sf_mex_addfield(*c52_info, c52_b_emlrt_marshallOut(0U), "mFileTimeHi",
                  "mFileTimeHi", 40);
  sf_mex_assign(&c52_rhs40, sf_mex_createcellmatrix(0, 1), false);
  sf_mex_assign(&c52_lhs40, sf_mex_createcellmatrix(0, 1), false);
  sf_mex_addfield(*c52_info, sf_mex_duplicatearraysafe(&c52_rhs40), "rhs", "rhs",
                  40);
  sf_mex_addfield(*c52_info, sf_mex_duplicatearraysafe(&c52_lhs40), "lhs", "lhs",
                  40);
  sf_mex_addfield(*c52_info, c52_emlrt_marshallOut(
    "[ILXE]$matlabroot$/toolbox/eml/lib/matlab/elfun/atan2d.m"), "context",
                  "context", 41);
  sf_mex_addfield(*c52_info, c52_emlrt_marshallOut("eml_scalar_atan2"), "name",
                  "name", 41);
  sf_mex_addfield(*c52_info, c52_emlrt_marshallOut("single"), "dominantType",
                  "dominantType", 41);
  sf_mex_addfield(*c52_info, c52_emlrt_marshallOut(
    "[ILXE]$matlabroot$/toolbox/eml/lib/matlab/elfun/eml_scalar_atan2.m"),
                  "resolved", "resolved", 41);
  sf_mex_addfield(*c52_info, c52_b_emlrt_marshallOut(1286822320U), "fileTimeLo",
                  "fileTimeLo", 41);
  sf_mex_addfield(*c52_info, c52_b_emlrt_marshallOut(0U), "fileTimeHi",
                  "fileTimeHi", 41);
  sf_mex_addfield(*c52_info, c52_b_emlrt_marshallOut(0U), "mFileTimeLo",
                  "mFileTimeLo", 41);
  sf_mex_addfield(*c52_info, c52_b_emlrt_marshallOut(0U), "mFileTimeHi",
                  "mFileTimeHi", 41);
  sf_mex_assign(&c52_rhs41, sf_mex_createcellmatrix(0, 1), false);
  sf_mex_assign(&c52_lhs41, sf_mex_createcellmatrix(0, 1), false);
  sf_mex_addfield(*c52_info, sf_mex_duplicatearraysafe(&c52_rhs41), "rhs", "rhs",
                  41);
  sf_mex_addfield(*c52_info, sf_mex_duplicatearraysafe(&c52_lhs41), "lhs", "lhs",
                  41);
  sf_mex_addfield(*c52_info, c52_emlrt_marshallOut(
    "[ILXE]$matlabroot$/toolbox/eml/lib/matlab/elfun/atan2d.m"), "context",
                  "context", 42);
  sf_mex_addfield(*c52_info, c52_emlrt_marshallOut("eml_mtimes_helper"), "name",
                  "name", 42);
  sf_mex_addfield(*c52_info, c52_emlrt_marshallOut(""), "dominantType",
                  "dominantType", 42);
  sf_mex_addfield(*c52_info, c52_emlrt_marshallOut(
    "[ILXE]$matlabroot$/toolbox/eml/lib/matlab/ops/eml_mtimes_helper.m"),
                  "resolved", "resolved", 42);
  sf_mex_addfield(*c52_info, c52_b_emlrt_marshallOut(1383880894U), "fileTimeLo",
                  "fileTimeLo", 42);
  sf_mex_addfield(*c52_info, c52_b_emlrt_marshallOut(0U), "fileTimeHi",
                  "fileTimeHi", 42);
  sf_mex_addfield(*c52_info, c52_b_emlrt_marshallOut(0U), "mFileTimeLo",
                  "mFileTimeLo", 42);
  sf_mex_addfield(*c52_info, c52_b_emlrt_marshallOut(0U), "mFileTimeHi",
                  "mFileTimeHi", 42);
  sf_mex_assign(&c52_rhs42, sf_mex_createcellmatrix(0, 1), false);
  sf_mex_assign(&c52_lhs42, sf_mex_createcellmatrix(0, 1), false);
  sf_mex_addfield(*c52_info, sf_mex_duplicatearraysafe(&c52_rhs42), "rhs", "rhs",
                  42);
  sf_mex_addfield(*c52_info, sf_mex_duplicatearraysafe(&c52_lhs42), "lhs", "lhs",
                  42);
  sf_mex_addfield(*c52_info, c52_emlrt_marshallOut(""), "context", "context", 43);
  sf_mex_addfield(*c52_info, c52_emlrt_marshallOut("intmax"), "name", "name", 43);
  sf_mex_addfield(*c52_info, c52_emlrt_marshallOut("char"), "dominantType",
                  "dominantType", 43);
  sf_mex_addfield(*c52_info, c52_emlrt_marshallOut(
    "[ILXE]$matlabroot$/toolbox/eml/lib/matlab/elmat/intmax.m"), "resolved",
                  "resolved", 43);
  sf_mex_addfield(*c52_info, c52_b_emlrt_marshallOut(1362265482U), "fileTimeLo",
                  "fileTimeLo", 43);
  sf_mex_addfield(*c52_info, c52_b_emlrt_marshallOut(0U), "fileTimeHi",
                  "fileTimeHi", 43);
  sf_mex_addfield(*c52_info, c52_b_emlrt_marshallOut(0U), "mFileTimeLo",
                  "mFileTimeLo", 43);
  sf_mex_addfield(*c52_info, c52_b_emlrt_marshallOut(0U), "mFileTimeHi",
                  "mFileTimeHi", 43);
  sf_mex_assign(&c52_rhs43, sf_mex_createcellmatrix(0, 1), false);
  sf_mex_assign(&c52_lhs43, sf_mex_createcellmatrix(0, 1), false);
  sf_mex_addfield(*c52_info, sf_mex_duplicatearraysafe(&c52_rhs43), "rhs", "rhs",
                  43);
  sf_mex_addfield(*c52_info, sf_mex_duplicatearraysafe(&c52_lhs43), "lhs", "lhs",
                  43);
  sf_mex_addfield(*c52_info, c52_emlrt_marshallOut(""), "context", "context", 44);
  sf_mex_addfield(*c52_info, c52_emlrt_marshallOut("abs"), "name", "name", 44);
  sf_mex_addfield(*c52_info, c52_emlrt_marshallOut("int16"), "dominantType",
                  "dominantType", 44);
  sf_mex_addfield(*c52_info, c52_emlrt_marshallOut(
    "[ILXE]$matlabroot$/toolbox/eml/lib/matlab/elfun/abs.m"), "resolved",
                  "resolved", 44);
  sf_mex_addfield(*c52_info, c52_b_emlrt_marshallOut(1363717452U), "fileTimeLo",
                  "fileTimeLo", 44);
  sf_mex_addfield(*c52_info, c52_b_emlrt_marshallOut(0U), "fileTimeHi",
                  "fileTimeHi", 44);
  sf_mex_addfield(*c52_info, c52_b_emlrt_marshallOut(0U), "mFileTimeLo",
                  "mFileTimeLo", 44);
  sf_mex_addfield(*c52_info, c52_b_emlrt_marshallOut(0U), "mFileTimeHi",
                  "mFileTimeHi", 44);
  sf_mex_assign(&c52_rhs44, sf_mex_createcellmatrix(0, 1), false);
  sf_mex_assign(&c52_lhs44, sf_mex_createcellmatrix(0, 1), false);
  sf_mex_addfield(*c52_info, sf_mex_duplicatearraysafe(&c52_rhs44), "rhs", "rhs",
                  44);
  sf_mex_addfield(*c52_info, sf_mex_duplicatearraysafe(&c52_lhs44), "lhs", "lhs",
                  44);
  sf_mex_addfield(*c52_info, c52_emlrt_marshallOut(
    "[ILXE]$matlabroot$/toolbox/eml/lib/matlab/elfun/abs.m"), "context",
                  "context", 45);
  sf_mex_addfield(*c52_info, c52_emlrt_marshallOut(
    "coder.internal.isBuiltInNumeric"), "name", "name", 45);
  sf_mex_addfield(*c52_info, c52_emlrt_marshallOut("int16"), "dominantType",
                  "dominantType", 45);
  sf_mex_addfield(*c52_info, c52_emlrt_marshallOut(
    "[ILXE]$matlabroot$/toolbox/shared/coder/coder/+coder/+internal/isBuiltInNumeric.m"),
                  "resolved", "resolved", 45);
  sf_mex_addfield(*c52_info, c52_b_emlrt_marshallOut(1395935456U), "fileTimeLo",
                  "fileTimeLo", 45);
  sf_mex_addfield(*c52_info, c52_b_emlrt_marshallOut(0U), "fileTimeHi",
                  "fileTimeHi", 45);
  sf_mex_addfield(*c52_info, c52_b_emlrt_marshallOut(0U), "mFileTimeLo",
                  "mFileTimeLo", 45);
  sf_mex_addfield(*c52_info, c52_b_emlrt_marshallOut(0U), "mFileTimeHi",
                  "mFileTimeHi", 45);
  sf_mex_assign(&c52_rhs45, sf_mex_createcellmatrix(0, 1), false);
  sf_mex_assign(&c52_lhs45, sf_mex_createcellmatrix(0, 1), false);
  sf_mex_addfield(*c52_info, sf_mex_duplicatearraysafe(&c52_rhs45), "rhs", "rhs",
                  45);
  sf_mex_addfield(*c52_info, sf_mex_duplicatearraysafe(&c52_lhs45), "lhs", "lhs",
                  45);
  sf_mex_addfield(*c52_info, c52_emlrt_marshallOut(
    "[ILXE]$matlabroot$/toolbox/eml/lib/matlab/elfun/abs.m"), "context",
                  "context", 46);
  sf_mex_addfield(*c52_info, c52_emlrt_marshallOut("eml_scalar_abs"), "name",
                  "name", 46);
  sf_mex_addfield(*c52_info, c52_emlrt_marshallOut("int16"), "dominantType",
                  "dominantType", 46);
  sf_mex_addfield(*c52_info, c52_emlrt_marshallOut(
    "[ILXE]$matlabroot$/toolbox/eml/lib/matlab/elfun/eml_scalar_abs.m"),
                  "resolved", "resolved", 46);
  sf_mex_addfield(*c52_info, c52_b_emlrt_marshallOut(1286822312U), "fileTimeLo",
                  "fileTimeLo", 46);
  sf_mex_addfield(*c52_info, c52_b_emlrt_marshallOut(0U), "fileTimeHi",
                  "fileTimeHi", 46);
  sf_mex_addfield(*c52_info, c52_b_emlrt_marshallOut(0U), "mFileTimeLo",
                  "mFileTimeLo", 46);
  sf_mex_addfield(*c52_info, c52_b_emlrt_marshallOut(0U), "mFileTimeHi",
                  "mFileTimeHi", 46);
  sf_mex_assign(&c52_rhs46, sf_mex_createcellmatrix(0, 1), false);
  sf_mex_assign(&c52_lhs46, sf_mex_createcellmatrix(0, 1), false);
  sf_mex_addfield(*c52_info, sf_mex_duplicatearraysafe(&c52_rhs46), "rhs", "rhs",
                  46);
  sf_mex_addfield(*c52_info, sf_mex_duplicatearraysafe(&c52_lhs46), "lhs", "lhs",
                  46);
  sf_mex_destroy(&c52_rhs0);
  sf_mex_destroy(&c52_lhs0);
  sf_mex_destroy(&c52_rhs1);
  sf_mex_destroy(&c52_lhs1);
  sf_mex_destroy(&c52_rhs2);
  sf_mex_destroy(&c52_lhs2);
  sf_mex_destroy(&c52_rhs3);
  sf_mex_destroy(&c52_lhs3);
  sf_mex_destroy(&c52_rhs4);
  sf_mex_destroy(&c52_lhs4);
  sf_mex_destroy(&c52_rhs5);
  sf_mex_destroy(&c52_lhs5);
  sf_mex_destroy(&c52_rhs6);
  sf_mex_destroy(&c52_lhs6);
  sf_mex_destroy(&c52_rhs7);
  sf_mex_destroy(&c52_lhs7);
  sf_mex_destroy(&c52_rhs8);
  sf_mex_destroy(&c52_lhs8);
  sf_mex_destroy(&c52_rhs9);
  sf_mex_destroy(&c52_lhs9);
  sf_mex_destroy(&c52_rhs10);
  sf_mex_destroy(&c52_lhs10);
  sf_mex_destroy(&c52_rhs11);
  sf_mex_destroy(&c52_lhs11);
  sf_mex_destroy(&c52_rhs12);
  sf_mex_destroy(&c52_lhs12);
  sf_mex_destroy(&c52_rhs13);
  sf_mex_destroy(&c52_lhs13);
  sf_mex_destroy(&c52_rhs14);
  sf_mex_destroy(&c52_lhs14);
  sf_mex_destroy(&c52_rhs15);
  sf_mex_destroy(&c52_lhs15);
  sf_mex_destroy(&c52_rhs16);
  sf_mex_destroy(&c52_lhs16);
  sf_mex_destroy(&c52_rhs17);
  sf_mex_destroy(&c52_lhs17);
  sf_mex_destroy(&c52_rhs18);
  sf_mex_destroy(&c52_lhs18);
  sf_mex_destroy(&c52_rhs19);
  sf_mex_destroy(&c52_lhs19);
  sf_mex_destroy(&c52_rhs20);
  sf_mex_destroy(&c52_lhs20);
  sf_mex_destroy(&c52_rhs21);
  sf_mex_destroy(&c52_lhs21);
  sf_mex_destroy(&c52_rhs22);
  sf_mex_destroy(&c52_lhs22);
  sf_mex_destroy(&c52_rhs23);
  sf_mex_destroy(&c52_lhs23);
  sf_mex_destroy(&c52_rhs24);
  sf_mex_destroy(&c52_lhs24);
  sf_mex_destroy(&c52_rhs25);
  sf_mex_destroy(&c52_lhs25);
  sf_mex_destroy(&c52_rhs26);
  sf_mex_destroy(&c52_lhs26);
  sf_mex_destroy(&c52_rhs27);
  sf_mex_destroy(&c52_lhs27);
  sf_mex_destroy(&c52_rhs28);
  sf_mex_destroy(&c52_lhs28);
  sf_mex_destroy(&c52_rhs29);
  sf_mex_destroy(&c52_lhs29);
  sf_mex_destroy(&c52_rhs30);
  sf_mex_destroy(&c52_lhs30);
  sf_mex_destroy(&c52_rhs31);
  sf_mex_destroy(&c52_lhs31);
  sf_mex_destroy(&c52_rhs32);
  sf_mex_destroy(&c52_lhs32);
  sf_mex_destroy(&c52_rhs33);
  sf_mex_destroy(&c52_lhs33);
  sf_mex_destroy(&c52_rhs34);
  sf_mex_destroy(&c52_lhs34);
  sf_mex_destroy(&c52_rhs35);
  sf_mex_destroy(&c52_lhs35);
  sf_mex_destroy(&c52_rhs36);
  sf_mex_destroy(&c52_lhs36);
  sf_mex_destroy(&c52_rhs37);
  sf_mex_destroy(&c52_lhs37);
  sf_mex_destroy(&c52_rhs38);
  sf_mex_destroy(&c52_lhs38);
  sf_mex_destroy(&c52_rhs39);
  sf_mex_destroy(&c52_lhs39);
  sf_mex_destroy(&c52_rhs40);
  sf_mex_destroy(&c52_lhs40);
  sf_mex_destroy(&c52_rhs41);
  sf_mex_destroy(&c52_lhs41);
  sf_mex_destroy(&c52_rhs42);
  sf_mex_destroy(&c52_lhs42);
  sf_mex_destroy(&c52_rhs43);
  sf_mex_destroy(&c52_lhs43);
  sf_mex_destroy(&c52_rhs44);
  sf_mex_destroy(&c52_lhs44);
  sf_mex_destroy(&c52_rhs45);
  sf_mex_destroy(&c52_lhs45);
  sf_mex_destroy(&c52_rhs46);
  sf_mex_destroy(&c52_lhs46);
}

static const mxArray *c52_emlrt_marshallOut(const char * c52_u)
{
  const mxArray *c52_y = NULL;
  c52_y = NULL;
  sf_mex_assign(&c52_y, sf_mex_create("y", c52_u, 15, 0U, 0U, 0U, 2, 1, strlen
    (c52_u)), false);
  return c52_y;
}

static const mxArray *c52_b_emlrt_marshallOut(const uint32_T c52_u)
{
  const mxArray *c52_y = NULL;
  c52_y = NULL;
  sf_mex_assign(&c52_y, sf_mex_create("y", &c52_u, 7, 0U, 0U, 0U, 0), false);
  return c52_y;
}

static void c52_calcShootWay(SFc52_LessonIIInstanceStruct *chartInstance)
{
  uint32_T c52_debug_family_var_map[3];
  int8_T c52_pos[2];
  real_T c52_nargin = 0.0;
  real_T c52_nargout = 0.0;
  int32_T c52_i22;
  static int8_T c52_iv9[2] = { 70, -60 };

  int32_T c52_i23;
  int32_T c52_i24;
  int8_T c52_b_pos;
  int8_T c52_c_pos;
  int8_T c52_iv10[2];
  int8_T c52_d_pos[2];
  int32_T c52_i25;
  int32_T c52_i26;
  int32_T c52_i27;
  real32_T c52_b[2];
  int32_T c52_i28;
  int8_T c52_iv11[2];
  int32_T c52_i29;
  int32_T c52_i30;
  real32_T c52_x[2];
  int32_T c52_i31;
  real32_T c52_b_x[2];
  real32_T c52_y;
  real32_T c52_B;
  real32_T c52_b_y;
  real32_T c52_c_y;
  real32_T c52_d_y;
  int32_T c52_i32;
  int32_T c52_i33;
  real32_T c52_f0;
  int8_T c52_i34;
  int32_T c52_i35;
  int32_T c52_i36;
  int32_T c52_i37;
  real32_T c52_e_y;
  int32_T c52_i38;
  real32_T c52_c_x;
  real32_T c52_f_y;
  real32_T c52_d_x;
  real32_T c52_r;
  real32_T c52_b_b;
  real32_T c52_b_r;
  real32_T c52_f1;
  int16_T c52_i39;
  _SFD_SYMBOL_SCOPE_PUSH_EML(0U, 3U, 3U, c52_debug_family_names,
    c52_debug_family_var_map);
  _SFD_SYMBOL_SCOPE_ADD_EML_IMPORTABLE(c52_pos, 0U, c52_b_sf_marshallOut,
    c52_b_sf_marshallIn);
  _SFD_SYMBOL_SCOPE_ADD_EML_IMPORTABLE(&c52_nargin, 1U, c52_sf_marshallOut,
    c52_sf_marshallIn);
  _SFD_SYMBOL_SCOPE_ADD_EML_IMPORTABLE(&c52_nargout, 2U, c52_sf_marshallOut,
    c52_sf_marshallIn);
  CV_EML_FCN(5, 0);
  _SFD_EML_CALL(5U, chartInstance->c52_sfEvent, 2);
  c52_errorIfDataNotWrittenToFcn(chartInstance, 2U, 7U, 126U, 25, 8);
  if (CV_EML_IF(5, 1, 0, CV_RELATIONAL_EVAL(4U, 5U, 0, (real_T)
        chartInstance->c52_enemy[2].y, 0.0, -1, 4U, (real_T)
        chartInstance->c52_enemy[2].y > 0.0))) {
    _SFD_EML_CALL(5U, chartInstance->c52_sfEvent, 3);
    for (c52_i22 = 0; c52_i22 < 2; c52_i22++) {
      c52_pos[c52_i22] = c52_iv9[c52_i22];
    }
  } else {
    _SFD_EML_CALL(5U, chartInstance->c52_sfEvent, 5);
    for (c52_i23 = 0; c52_i23 < 2; c52_i23++) {
      c52_pos[c52_i23] = (int8_T)(70 + (int8_T)(-10 * (int8_T)c52_i23));
    }
  }

  _SFD_EML_CALL(5U, chartInstance->c52_sfEvent, 7);
  if (CV_EML_IF(5, 1, 1, CV_RELATIONAL_EVAL(4U, 5U, 1, (real_T)*(uint8_T *)
        &((char_T *)chartInstance->c52_me)[4], 103.0, 0, 1U, *(uint8_T *)
        &((char_T *)chartInstance->c52_me)[4] != 103))) {
    _SFD_EML_CALL(5U, chartInstance->c52_sfEvent, 8);
    c52_i24 = -c52_pos[0];
    if (c52_i24 > 127) {
      CV_SATURATION_EVAL(4, 5, 0, 0, 1);
      c52_i24 = 127;
    } else {
      if (CV_SATURATION_EVAL(4, 5, 0, 0, c52_i24 < -128)) {
        c52_i24 = -128;
      }
    }

    c52_pos[0] = (int8_T)c52_i24;
  }

  _SFD_EML_CALL(5U, chartInstance->c52_sfEvent, 10);
  c52_b_pos = c52_pos[0];
  c52_c_pos = c52_pos[1];
  c52_iv10[0] = *(int8_T *)&((char_T *)chartInstance->c52_ball)[0];
  c52_iv10[1] = *(int8_T *)&((char_T *)chartInstance->c52_ball)[1];
  c52_d_pos[0] = c52_b_pos;
  c52_d_pos[1] = c52_c_pos;
  for (c52_i25 = 0; c52_i25 < 2; c52_i25++) {
    c52_i26 = c52_iv10[c52_i25] - c52_d_pos[c52_i25];
    if (c52_i26 > 127) {
      CV_SATURATION_EVAL(4, 5, 1, 0, 1);
      c52_i26 = 127;
    } else {
      if (CV_SATURATION_EVAL(4, 5, 1, 0, c52_i26 < -128)) {
        c52_i26 = -128;
      }
    }

    c52_pos[c52_i25] = (int8_T)c52_i26;
  }

  _SFD_EML_CALL(5U, chartInstance->c52_sfEvent, 11);
  for (c52_i27 = 0; c52_i27 < 2; c52_i27++) {
    c52_b[c52_i27] = (real32_T)c52_pos[c52_i27];
  }

  for (c52_i28 = 0; c52_i28 < 2; c52_i28++) {
    c52_b[c52_i28] *= 20.0F;
  }

  c52_iv11[0] = *(int8_T *)&((char_T *)chartInstance->c52_ball)[0];
  c52_iv11[1] = *(int8_T *)&((char_T *)chartInstance->c52_ball)[1];
  for (c52_i29 = 0; c52_i29 < 2; c52_i29++) {
    c52_i30 = c52_iv11[c52_i29] - c52_pos[c52_i29];
    if (c52_i30 > 127) {
      CV_SATURATION_EVAL(4, 5, 3, 0, 1);
      c52_i30 = 127;
    } else {
      if (CV_SATURATION_EVAL(4, 5, 3, 0, c52_i30 < -128)) {
        c52_i30 = -128;
      }
    }

    c52_x[c52_i29] = (real32_T)(int8_T)c52_i30;
  }

  for (c52_i31 = 0; c52_i31 < 2; c52_i31++) {
    c52_b_x[c52_i31] = c52_x[c52_i31];
  }

  c52_y = c52_b_eml_xnrm2(chartInstance, c52_b_x);
  c52_B = c52_y;
  c52_b_y = c52_B;
  c52_c_y = c52_b_y;
  c52_d_y = c52_c_y;
  for (c52_i32 = 0; c52_i32 < 2; c52_i32++) {
    c52_b[c52_i32] /= c52_d_y;
  }

  for (c52_i33 = 0; c52_i33 < 2; c52_i33++) {
    c52_f0 = muSingleScalarRound(c52_b[c52_i33]);
    if (c52_f0 < 128.0F) {
      if (CV_SATURATION_EVAL(4, 5, 2, 1, c52_f0 >= -128.0F)) {
        c52_i34 = (int8_T)c52_f0;
      } else {
        c52_i34 = MIN_int8_T;
      }
    } else if (CV_SATURATION_EVAL(4, 5, 2, 0, c52_f0 >= 128.0F)) {
      c52_i34 = MAX_int8_T;
    } else {
      c52_i34 = 0;
    }

    c52_pos[c52_i33] = c52_i34;
  }

  _SFD_EML_CALL(5U, chartInstance->c52_sfEvent, 13);
  c52_i35 = *(int8_T *)&((char_T *)chartInstance->c52_ball)[0] + c52_pos[0];
  if (c52_i35 > 127) {
    CV_SATURATION_EVAL(4, 5, 4, 0, 1);
    c52_i35 = 127;
  } else {
    if (CV_SATURATION_EVAL(4, 5, 4, 0, c52_i35 < -128)) {
      c52_i35 = -128;
    }
  }

  chartInstance->c52_shootWay.x = (int8_T)c52_i35;
  c52_updateDataWrittenToVector(chartInstance, 4U);
  _SFD_EML_CALL(5U, chartInstance->c52_sfEvent, 14);
  c52_i36 = *(int8_T *)&((char_T *)chartInstance->c52_ball)[1] + c52_pos[1];
  if (c52_i36 > 127) {
    CV_SATURATION_EVAL(4, 5, 5, 0, 1);
    c52_i36 = 127;
  } else {
    if (CV_SATURATION_EVAL(4, 5, 5, 0, c52_i36 < -128)) {
      c52_i36 = -128;
    }
  }

  chartInstance->c52_shootWay.y = (int8_T)c52_i36;
  c52_updateDataWrittenToVector(chartInstance, 4U);
  _SFD_EML_CALL(5U, chartInstance->c52_sfEvent, 15);
  c52_i37 = -c52_pos[1];
  if (c52_i37 > 127) {
    CV_SATURATION_EVAL(4, 5, 7, 0, 1);
    c52_i37 = 127;
  } else {
    if (CV_SATURATION_EVAL(4, 5, 7, 0, c52_i37 < -128)) {
      c52_i37 = -128;
    }
  }

  c52_e_y = (real32_T)(int8_T)c52_i37;
  c52_i38 = -c52_pos[0];
  if (c52_i38 > 127) {
    CV_SATURATION_EVAL(4, 5, 8, 0, 1);
    c52_i38 = 127;
  } else {
    if (CV_SATURATION_EVAL(4, 5, 8, 0, c52_i38 < -128)) {
      c52_i38 = -128;
    }
  }

  c52_c_x = (real32_T)(int8_T)c52_i38;
  c52_f_y = c52_e_y;
  c52_d_x = c52_c_x;
  c52_r = muSingleScalarAtan2(c52_f_y, c52_d_x);
  c52_b_b = c52_r;
  c52_b_r = 57.2957802F * c52_b_b;
  c52_f1 = muSingleScalarRound(c52_b_r);
  if (c52_f1 < 32768.0F) {
    if (CV_SATURATION_EVAL(4, 5, 6, 1, c52_f1 >= -32768.0F)) {
      c52_i39 = (int16_T)c52_f1;
    } else {
      c52_i39 = MIN_int16_T;
    }
  } else if (CV_SATURATION_EVAL(4, 5, 6, 0, c52_f1 >= 32768.0F)) {
    c52_i39 = MAX_int16_T;
  } else {
    c52_i39 = 0;
  }

  chartInstance->c52_shootWay.orientation = c52_i39;
  c52_updateDataWrittenToVector(chartInstance, 4U);
  _SFD_EML_CALL(5U, chartInstance->c52_sfEvent, -15);
  _SFD_SYMBOL_SCOPE_POP();
}

static void c52_calcStartTeams(SFc52_LessonIIInstanceStruct *chartInstance)
{
  uint32_T c52_debug_family_var_map[2];
  real_T c52_nargin = 0.0;
  real_T c52_nargout = 0.0;
  int32_T c52_i40;
  int32_T c52_i41;
  c52_Player c52_rv2[6];
  int32_T c52_i42;
  int32_T c52_i43;
  int32_T c52_i44;
  c52_Player c52_rv3[6];
  int32_T c52_i45;
  int32_T c52_i46;
  int32_T c52_i47;
  c52_Player c52_rv4[6];
  int32_T c52_i48;
  int32_T c52_i49;
  int32_T c52_i50;
  c52_Player c52_rv5[6];
  int32_T c52_i51;
  _SFD_SYMBOL_SCOPE_PUSH_EML(0U, 2U, 2U, c52_b_debug_family_names,
    c52_debug_family_var_map);
  _SFD_SYMBOL_SCOPE_ADD_EML_IMPORTABLE(&c52_nargin, 0U, c52_sf_marshallOut,
    c52_sf_marshallIn);
  _SFD_SYMBOL_SCOPE_ADD_EML_IMPORTABLE(&c52_nargout, 1U, c52_sf_marshallOut,
    c52_sf_marshallIn);
  CV_EML_FCN(7, 0);
  _SFD_EML_CALL(7U, chartInstance->c52_sfEvent, 2);
  switch (*(uint8_T *)&((char_T *)chartInstance->c52_me)[4]) {
   case 103U:
    CV_EML_SWITCH(7, 1, 0, 1);
    _SFD_EML_CALL(7U, chartInstance->c52_sfEvent, 4);
    for (c52_i40 = 0; c52_i40 < 6; c52_i40++) {
      for (c52_i41 = 0; c52_i41 < 1; c52_i41++) {
        c52_rv2[c52_i41 + c52_i40].x = *(int8_T *)&((char_T *)(c52_Player *)
          &((char_T *)chartInstance->c52_players)[8 * (c52_i41 + c52_i40)])[0];
        c52_rv2[c52_i41 + c52_i40].y = *(int8_T *)&((char_T *)(c52_Player *)
          &((char_T *)chartInstance->c52_players)[8 * (c52_i41 + c52_i40)])[1];
        c52_rv2[c52_i41 + c52_i40].orientation = *(int16_T *)&((char_T *)
          (c52_Player *)&((char_T *)chartInstance->c52_players)[8 * (c52_i41 +
          c52_i40)])[2];
        c52_rv2[c52_i41 + c52_i40].color = *(uint8_T *)&((char_T *)(c52_Player *)
          &((char_T *)chartInstance->c52_players)[8 * (c52_i41 + c52_i40)])[4];
        c52_rv2[c52_i41 + c52_i40].position = *(uint8_T *)&((char_T *)
          (c52_Player *)&((char_T *)chartInstance->c52_players)[8 * (c52_i41 +
          c52_i40)])[5];
        c52_rv2[c52_i41 + c52_i40].valid = *(uint8_T *)&((char_T *)(c52_Player *)
          &((char_T *)chartInstance->c52_players)[8 * (c52_i41 + c52_i40)])[6];
      }
    }

    for (c52_i42 = 0; c52_i42 < 3; c52_i42++) {
      chartInstance->c52_enemy[c52_i42] = c52_rv2[c52_i42 + 3];
    }

    c52_updateDataWrittenToVector(chartInstance, 2U);
    _SFD_EML_CALL(7U, chartInstance->c52_sfEvent, 5);
    for (c52_i43 = 0; c52_i43 < 6; c52_i43++) {
      for (c52_i44 = 0; c52_i44 < 1; c52_i44++) {
        c52_rv3[c52_i44 + c52_i43].x = *(int8_T *)&((char_T *)(c52_Player *)
          &((char_T *)chartInstance->c52_players)[8 * (c52_i44 + c52_i43)])[0];
        c52_rv3[c52_i44 + c52_i43].y = *(int8_T *)&((char_T *)(c52_Player *)
          &((char_T *)chartInstance->c52_players)[8 * (c52_i44 + c52_i43)])[1];
        c52_rv3[c52_i44 + c52_i43].orientation = *(int16_T *)&((char_T *)
          (c52_Player *)&((char_T *)chartInstance->c52_players)[8 * (c52_i44 +
          c52_i43)])[2];
        c52_rv3[c52_i44 + c52_i43].color = *(uint8_T *)&((char_T *)(c52_Player *)
          &((char_T *)chartInstance->c52_players)[8 * (c52_i44 + c52_i43)])[4];
        c52_rv3[c52_i44 + c52_i43].position = *(uint8_T *)&((char_T *)
          (c52_Player *)&((char_T *)chartInstance->c52_players)[8 * (c52_i44 +
          c52_i43)])[5];
        c52_rv3[c52_i44 + c52_i43].valid = *(uint8_T *)&((char_T *)(c52_Player *)
          &((char_T *)chartInstance->c52_players)[8 * (c52_i44 + c52_i43)])[6];
      }
    }

    for (c52_i45 = 0; c52_i45 < 3; c52_i45++) {
      chartInstance->c52_friends[c52_i45] = c52_rv3[c52_i45];
    }

    c52_updateDataWrittenToVector(chartInstance, 3U);
    break;

   case 98U:
    CV_EML_SWITCH(7, 1, 0, 2);
    _SFD_EML_CALL(7U, chartInstance->c52_sfEvent, 7);
    for (c52_i46 = 0; c52_i46 < 6; c52_i46++) {
      for (c52_i47 = 0; c52_i47 < 1; c52_i47++) {
        c52_rv4[c52_i47 + c52_i46].x = *(int8_T *)&((char_T *)(c52_Player *)
          &((char_T *)chartInstance->c52_players)[8 * (c52_i47 + c52_i46)])[0];
        c52_rv4[c52_i47 + c52_i46].y = *(int8_T *)&((char_T *)(c52_Player *)
          &((char_T *)chartInstance->c52_players)[8 * (c52_i47 + c52_i46)])[1];
        c52_rv4[c52_i47 + c52_i46].orientation = *(int16_T *)&((char_T *)
          (c52_Player *)&((char_T *)chartInstance->c52_players)[8 * (c52_i47 +
          c52_i46)])[2];
        c52_rv4[c52_i47 + c52_i46].color = *(uint8_T *)&((char_T *)(c52_Player *)
          &((char_T *)chartInstance->c52_players)[8 * (c52_i47 + c52_i46)])[4];
        c52_rv4[c52_i47 + c52_i46].position = *(uint8_T *)&((char_T *)
          (c52_Player *)&((char_T *)chartInstance->c52_players)[8 * (c52_i47 +
          c52_i46)])[5];
        c52_rv4[c52_i47 + c52_i46].valid = *(uint8_T *)&((char_T *)(c52_Player *)
          &((char_T *)chartInstance->c52_players)[8 * (c52_i47 + c52_i46)])[6];
      }
    }

    for (c52_i48 = 0; c52_i48 < 3; c52_i48++) {
      chartInstance->c52_enemy[c52_i48] = c52_rv4[c52_i48];
    }

    c52_updateDataWrittenToVector(chartInstance, 2U);
    _SFD_EML_CALL(7U, chartInstance->c52_sfEvent, 8);
    for (c52_i49 = 0; c52_i49 < 6; c52_i49++) {
      for (c52_i50 = 0; c52_i50 < 1; c52_i50++) {
        c52_rv5[c52_i50 + c52_i49].x = *(int8_T *)&((char_T *)(c52_Player *)
          &((char_T *)chartInstance->c52_players)[8 * (c52_i50 + c52_i49)])[0];
        c52_rv5[c52_i50 + c52_i49].y = *(int8_T *)&((char_T *)(c52_Player *)
          &((char_T *)chartInstance->c52_players)[8 * (c52_i50 + c52_i49)])[1];
        c52_rv5[c52_i50 + c52_i49].orientation = *(int16_T *)&((char_T *)
          (c52_Player *)&((char_T *)chartInstance->c52_players)[8 * (c52_i50 +
          c52_i49)])[2];
        c52_rv5[c52_i50 + c52_i49].color = *(uint8_T *)&((char_T *)(c52_Player *)
          &((char_T *)chartInstance->c52_players)[8 * (c52_i50 + c52_i49)])[4];
        c52_rv5[c52_i50 + c52_i49].position = *(uint8_T *)&((char_T *)
          (c52_Player *)&((char_T *)chartInstance->c52_players)[8 * (c52_i50 +
          c52_i49)])[5];
        c52_rv5[c52_i50 + c52_i49].valid = *(uint8_T *)&((char_T *)(c52_Player *)
          &((char_T *)chartInstance->c52_players)[8 * (c52_i50 + c52_i49)])[6];
      }
    }

    for (c52_i51 = 0; c52_i51 < 3; c52_i51++) {
      chartInstance->c52_friends[c52_i51] = c52_rv5[c52_i51 + 3];
    }

    c52_updateDataWrittenToVector(chartInstance, 3U);
    break;

   default:
    CV_EML_SWITCH(7, 1, 0, 0);
    break;
  }

  _SFD_EML_CALL(7U, chartInstance->c52_sfEvent, -8);
  _SFD_SYMBOL_SCOPE_POP();
}

static void c52_calcStartPos(SFc52_LessonIIInstanceStruct *chartInstance)
{
  uint32_T c52_debug_family_var_map[2];
  real_T c52_nargin = 0.0;
  real_T c52_nargout = 0.0;
  int32_T c52_i52;
  int32_T c52_i53;
  int32_T c52_i54;
  int32_T c52_i55;
  int32_T c52_i56;
  _SFD_SYMBOL_SCOPE_PUSH_EML(0U, 2U, 2U, c52_c_debug_family_names,
    c52_debug_family_var_map);
  _SFD_SYMBOL_SCOPE_ADD_EML_IMPORTABLE(&c52_nargin, 0U, c52_sf_marshallOut,
    c52_sf_marshallIn);
  _SFD_SYMBOL_SCOPE_ADD_EML_IMPORTABLE(&c52_nargout, 1U, c52_sf_marshallOut,
    c52_sf_marshallIn);
  CV_EML_FCN(6, 0);
  _SFD_EML_CALL(6U, chartInstance->c52_sfEvent, 2);
  switch (*(uint8_T *)&((char_T *)chartInstance->c52_me)[5]) {
   case 111U:
    CV_EML_SWITCH(6, 1, 0, 1);
    _SFD_EML_CALL(6U, chartInstance->c52_sfEvent, 4);
    for (c52_i52 = 0; c52_i52 < 2; c52_i52++) {
      chartInstance->c52_startingPos[c52_i52] = (int8_T)(10 + (int8_T)(-10 *
        (int8_T)c52_i52));
    }

    c52_updateDataWrittenToVector(chartInstance, 1U);
    break;

   case 100U:
    CV_EML_SWITCH(6, 1, 0, 2);
    _SFD_EML_CALL(6U, chartInstance->c52_sfEvent, 6);
    for (c52_i53 = 0; c52_i53 < 2; c52_i53++) {
      chartInstance->c52_startingPos[c52_i53] = (int8_T)(30 + (int8_T)(-30 *
        (int8_T)c52_i53));
    }

    c52_updateDataWrittenToVector(chartInstance, 1U);
    break;

   case 103U:
    CV_EML_SWITCH(6, 1, 0, 3);
    _SFD_EML_CALL(6U, chartInstance->c52_sfEvent, 8);
    for (c52_i54 = 0; c52_i54 < 2; c52_i54++) {
      chartInstance->c52_startingPos[c52_i54] = (int8_T)(70 + (int8_T)(-70 *
        (int8_T)c52_i54));
    }

    c52_updateDataWrittenToVector(chartInstance, 1U);
    break;

   default:
    CV_EML_SWITCH(6, 1, 0, 0);
    break;
  }

  _SFD_EML_CALL(6U, chartInstance->c52_sfEvent, 10);
  if (CV_EML_IF(6, 1, 0, CV_RELATIONAL_EVAL(4U, 6U, 0, (real_T)*(uint8_T *)
        &((char_T *)chartInstance->c52_me)[4], 103.0, 0, 0U, *(uint8_T *)
        &((char_T *)chartInstance->c52_me)[4] == 103))) {
    _SFD_EML_CALL(6U, chartInstance->c52_sfEvent, 11);
    c52_errorIfDataNotWrittenToFcn(chartInstance, 1U, 5U, 86U, 244, 11);
    for (c52_i55 = 0; c52_i55 < 2; c52_i55++) {
      c52_i56 = -chartInstance->c52_startingPos[c52_i55];
      if (c52_i56 > 127) {
        CV_SATURATION_EVAL(4, 6, 0, 0, 1);
        c52_i56 = 127;
      } else {
        if (CV_SATURATION_EVAL(4, 6, 0, 0, c52_i56 < -128)) {
          c52_i56 = -128;
        }
      }

      chartInstance->c52_startingPos[c52_i55] = (int8_T)c52_i56;
    }

    c52_updateDataWrittenToVector(chartInstance, 1U);
  }

  _SFD_EML_CALL(6U, chartInstance->c52_sfEvent, -11);
  _SFD_SYMBOL_SCOPE_POP();
}

static real32_T c52_b_eml_xnrm2(SFc52_LessonIIInstanceStruct *chartInstance,
  real32_T c52_x[2])
{
  real32_T c52_y;
  real32_T c52_scale;
  int32_T c52_k;
  int32_T c52_b_k;
  real32_T c52_b_x;
  real32_T c52_c_x;
  real32_T c52_absxk;
  real32_T c52_t;
  c52_threshold(chartInstance);
  c52_y = 0.0F;
  c52_realmin(chartInstance);
  c52_scale = 1.17549435E-38F;
  for (c52_k = 1; c52_k < 3; c52_k++) {
    c52_b_k = c52_k;
    c52_b_x = c52_x[_SFD_EML_ARRAY_BOUNDS_CHECK("", (int32_T)_SFD_INTEGER_CHECK(
      "", (real_T)c52_b_k), 1, 2, 1, 0) - 1];
    c52_c_x = c52_b_x;
    c52_absxk = muSingleScalarAbs(c52_c_x);
    if (c52_absxk > c52_scale) {
      c52_t = c52_scale / c52_absxk;
      c52_y = 1.0F + c52_y * c52_t * c52_t;
      c52_scale = c52_absxk;
    } else {
      c52_t = c52_absxk / c52_scale;
      c52_y += c52_t * c52_t;
    }
  }

  return c52_scale * muSingleScalarSqrt(c52_y);
}

static void c52_threshold(SFc52_LessonIIInstanceStruct *chartInstance)
{
  (void)chartInstance;
}

static void c52_realmin(SFc52_LessonIIInstanceStruct *chartInstance)
{
  (void)chartInstance;
}

static uint8_T c52_checkReached(SFc52_LessonIIInstanceStruct *chartInstance,
  int8_T c52_pos[2], real_T c52_tol)
{
  uint8_T c52_posReached;
  uint32_T c52_debug_family_var_map[5];
  real_T c52_nargin = 2.0;
  real_T c52_nargout = 1.0;
  int8_T c52_b_pos[2];
  int8_T c52_iv12[2];
  int32_T c52_i57;
  int32_T c52_i58;
  real32_T c52_x[2];
  int32_T c52_i59;
  real32_T c52_b_x[2];
  real32_T c52_y;
  real_T c52_d2;
  _SFD_SYMBOL_SCOPE_PUSH_EML(0U, 5U, 5U, c52_m_debug_family_names,
    c52_debug_family_var_map);
  _SFD_SYMBOL_SCOPE_ADD_EML_IMPORTABLE(&c52_nargin, 0U, c52_sf_marshallOut,
    c52_sf_marshallIn);
  _SFD_SYMBOL_SCOPE_ADD_EML_IMPORTABLE(&c52_nargout, 1U, c52_sf_marshallOut,
    c52_sf_marshallIn);
  _SFD_SYMBOL_SCOPE_ADD_EML_IMPORTABLE(c52_pos, 2U, c52_e_sf_marshallOut,
    c52_e_sf_marshallIn);
  _SFD_SYMBOL_SCOPE_ADD_EML_IMPORTABLE(&c52_tol, 3U, c52_sf_marshallOut,
    c52_sf_marshallIn);
  _SFD_SYMBOL_SCOPE_ADD_EML_IMPORTABLE(&c52_posReached, 4U, c52_d_sf_marshallOut,
    c52_d_sf_marshallIn);
  CV_EML_FCN(8, 0);
  _SFD_EML_CALL(8U, chartInstance->c52_sfEvent, 2);
  c52_posReached = 0U;
  _SFD_EML_CALL(8U, chartInstance->c52_sfEvent, 3);
  c52_b_pos[0] = c52_pos[0];
  c52_b_pos[1] = c52_pos[1];
  c52_iv12[0] = *(int8_T *)&((char_T *)chartInstance->c52_me)[0];
  c52_iv12[1] = *(int8_T *)&((char_T *)chartInstance->c52_me)[1];
  for (c52_i57 = 0; c52_i57 < 2; c52_i57++) {
    c52_i58 = c52_b_pos[c52_i57] - c52_iv12[c52_i57];
    if (c52_i58 > 127) {
      CV_SATURATION_EVAL(4, 8, 0, 0, 1);
      c52_i58 = 127;
    } else {
      if (CV_SATURATION_EVAL(4, 8, 0, 0, c52_i58 < -128)) {
        c52_i58 = -128;
      }
    }

    c52_x[c52_i57] = (real32_T)(int8_T)c52_i58;
  }

  for (c52_i59 = 0; c52_i59 < 2; c52_i59++) {
    c52_b_x[c52_i59] = c52_x[c52_i59];
  }

  c52_y = c52_eml_xnrm2(chartInstance, c52_b_x);
  c52_d2 = c52_y;
  if (CV_EML_IF(8, 1, 0, CV_RELATIONAL_EVAL(4U, 8U, 0, c52_d2, c52_tol, -1, 2U,
        c52_d2 < c52_tol))) {
    _SFD_EML_CALL(8U, chartInstance->c52_sfEvent, 4);
    c52_posReached = 1U;
  }

  _SFD_EML_CALL(8U, chartInstance->c52_sfEvent, -4);
  _SFD_SYMBOL_SCOPE_POP();
  return c52_posReached;
}

static uint8_T c52_b_checkReached(SFc52_LessonIIInstanceStruct *chartInstance,
  int8_T c52_pos[2], real_T c52_tol)
{
  uint8_T c52_posReached;
  uint32_T c52_debug_family_var_map[5];
  real_T c52_nargin = 2.0;
  real_T c52_nargout = 1.0;
  int8_T c52_b_pos[2];
  int8_T c52_iv13[2];
  int32_T c52_i60;
  int32_T c52_i61;
  real32_T c52_x[2];
  int32_T c52_i62;
  real32_T c52_b_x[2];
  real32_T c52_y;
  real_T c52_d3;
  _SFD_SYMBOL_SCOPE_PUSH_EML(0U, 5U, 5U, c52_q_debug_family_names,
    c52_debug_family_var_map);
  _SFD_SYMBOL_SCOPE_ADD_EML_IMPORTABLE(&c52_nargin, 0U, c52_sf_marshallOut,
    c52_sf_marshallIn);
  _SFD_SYMBOL_SCOPE_ADD_EML_IMPORTABLE(&c52_nargout, 1U, c52_sf_marshallOut,
    c52_sf_marshallIn);
  _SFD_SYMBOL_SCOPE_ADD_EML_IMPORTABLE(c52_pos, 2U, c52_b_sf_marshallOut,
    c52_b_sf_marshallIn);
  _SFD_SYMBOL_SCOPE_ADD_EML_IMPORTABLE(&c52_tol, 3U, c52_sf_marshallOut,
    c52_sf_marshallIn);
  _SFD_SYMBOL_SCOPE_ADD_EML_IMPORTABLE(&c52_posReached, 4U, c52_d_sf_marshallOut,
    c52_d_sf_marshallIn);
  CV_EML_FCN(8, 0);
  _SFD_EML_CALL(8U, chartInstance->c52_sfEvent, 2);
  c52_posReached = 0U;
  _SFD_EML_CALL(8U, chartInstance->c52_sfEvent, 3);
  c52_b_pos[0] = c52_pos[0];
  c52_b_pos[1] = c52_pos[1];
  c52_iv13[0] = *(int8_T *)&((char_T *)chartInstance->c52_me)[0];
  c52_iv13[1] = *(int8_T *)&((char_T *)chartInstance->c52_me)[1];
  for (c52_i60 = 0; c52_i60 < 2; c52_i60++) {
    c52_i61 = c52_b_pos[c52_i60] - c52_iv13[c52_i60];
    if (c52_i61 > 127) {
      CV_SATURATION_EVAL(4, 8, 0, 0, 1);
      c52_i61 = 127;
    } else {
      if (CV_SATURATION_EVAL(4, 8, 0, 0, c52_i61 < -128)) {
        c52_i61 = -128;
      }
    }

    c52_x[c52_i60] = (real32_T)(int8_T)c52_i61;
  }

  for (c52_i62 = 0; c52_i62 < 2; c52_i62++) {
    c52_b_x[c52_i62] = c52_x[c52_i62];
  }

  c52_y = c52_eml_xnrm2(chartInstance, c52_b_x);
  c52_d3 = c52_y;
  if (CV_EML_IF(8, 1, 0, CV_RELATIONAL_EVAL(4U, 8U, 0, c52_d3, c52_tol, -1, 2U,
        c52_d3 < c52_tol))) {
    _SFD_EML_CALL(8U, chartInstance->c52_sfEvent, 4);
    c52_posReached = 1U;
  }

  _SFD_EML_CALL(8U, chartInstance->c52_sfEvent, -4);
  _SFD_SYMBOL_SCOPE_POP();
  return c52_posReached;
}

static const mxArray *c52_f_sf_marshallOut(void *chartInstanceVoid, void
  *c52_inData)
{
  const mxArray *c52_mxArrayOutData = NULL;
  int32_T c52_u;
  const mxArray *c52_y = NULL;
  SFc52_LessonIIInstanceStruct *chartInstance;
  chartInstance = (SFc52_LessonIIInstanceStruct *)chartInstanceVoid;
  c52_mxArrayOutData = NULL;
  c52_u = *(int32_T *)c52_inData;
  c52_y = NULL;
  sf_mex_assign(&c52_y, sf_mex_create("y", &c52_u, 6, 0U, 0U, 0U, 0), false);
  sf_mex_assign(&c52_mxArrayOutData, c52_y, false);
  return c52_mxArrayOutData;
}

static int32_T c52_g_emlrt_marshallIn(SFc52_LessonIIInstanceStruct
  *chartInstance, const mxArray *c52_u, const emlrtMsgIdentifier *c52_parentId)
{
  int32_T c52_y;
  int32_T c52_i63;
  (void)chartInstance;
  sf_mex_import(c52_parentId, sf_mex_dup(c52_u), &c52_i63, 1, 6, 0U, 0, 0U, 0);
  c52_y = c52_i63;
  sf_mex_destroy(&c52_u);
  return c52_y;
}

static void c52_f_sf_marshallIn(void *chartInstanceVoid, const mxArray
  *c52_mxArrayInData, const char_T *c52_varName, void *c52_outData)
{
  const mxArray *c52_b_sfEvent;
  const char_T *c52_identifier;
  emlrtMsgIdentifier c52_thisId;
  int32_T c52_y;
  SFc52_LessonIIInstanceStruct *chartInstance;
  chartInstance = (SFc52_LessonIIInstanceStruct *)chartInstanceVoid;
  c52_b_sfEvent = sf_mex_dup(c52_mxArrayInData);
  c52_identifier = c52_varName;
  c52_thisId.fIdentifier = c52_identifier;
  c52_thisId.fParent = NULL;
  c52_y = c52_g_emlrt_marshallIn(chartInstance, sf_mex_dup(c52_b_sfEvent),
    &c52_thisId);
  sf_mex_destroy(&c52_b_sfEvent);
  *(int32_T *)c52_outData = c52_y;
  sf_mex_destroy(&c52_mxArrayInData);
}

static const mxArray *c52_me_bus_io(void *chartInstanceVoid, void *c52_pData)
{
  const mxArray *c52_mxVal = NULL;
  c52_Player c52_tmp;
  SFc52_LessonIIInstanceStruct *chartInstance;
  chartInstance = (SFc52_LessonIIInstanceStruct *)chartInstanceVoid;
  c52_mxVal = NULL;
  c52_tmp.x = *(int8_T *)&((char_T *)(c52_Player *)c52_pData)[0];
  c52_tmp.y = *(int8_T *)&((char_T *)(c52_Player *)c52_pData)[1];
  c52_tmp.orientation = *(int16_T *)&((char_T *)(c52_Player *)c52_pData)[2];
  c52_tmp.color = *(uint8_T *)&((char_T *)(c52_Player *)c52_pData)[4];
  c52_tmp.position = *(uint8_T *)&((char_T *)(c52_Player *)c52_pData)[5];
  c52_tmp.valid = *(uint8_T *)&((char_T *)(c52_Player *)c52_pData)[6];
  sf_mex_assign(&c52_mxVal, c52_g_sf_marshallOut(chartInstance, &c52_tmp), false);
  return c52_mxVal;
}

static const mxArray *c52_g_sf_marshallOut(void *chartInstanceVoid, void
  *c52_inData)
{
  const mxArray *c52_mxArrayOutData = NULL;
  c52_Player c52_u;
  const mxArray *c52_y = NULL;
  int8_T c52_b_u;
  const mxArray *c52_b_y = NULL;
  int8_T c52_c_u;
  const mxArray *c52_c_y = NULL;
  int16_T c52_d_u;
  const mxArray *c52_d_y = NULL;
  uint8_T c52_e_u;
  const mxArray *c52_e_y = NULL;
  uint8_T c52_f_u;
  const mxArray *c52_f_y = NULL;
  uint8_T c52_g_u;
  const mxArray *c52_g_y = NULL;
  SFc52_LessonIIInstanceStruct *chartInstance;
  chartInstance = (SFc52_LessonIIInstanceStruct *)chartInstanceVoid;
  c52_mxArrayOutData = NULL;
  c52_u = *(c52_Player *)c52_inData;
  c52_y = NULL;
  sf_mex_assign(&c52_y, sf_mex_createstruct("structure", 2, 1, 1), false);
  c52_b_u = c52_u.x;
  c52_b_y = NULL;
  sf_mex_assign(&c52_b_y, sf_mex_create("y", &c52_b_u, 2, 0U, 0U, 0U, 0), false);
  sf_mex_addfield(c52_y, c52_b_y, "x", "x", 0);
  c52_c_u = c52_u.y;
  c52_c_y = NULL;
  sf_mex_assign(&c52_c_y, sf_mex_create("y", &c52_c_u, 2, 0U, 0U, 0U, 0), false);
  sf_mex_addfield(c52_y, c52_c_y, "y", "y", 0);
  c52_d_u = c52_u.orientation;
  c52_d_y = NULL;
  sf_mex_assign(&c52_d_y, sf_mex_create("y", &c52_d_u, 4, 0U, 0U, 0U, 0), false);
  sf_mex_addfield(c52_y, c52_d_y, "orientation", "orientation", 0);
  c52_e_u = c52_u.color;
  c52_e_y = NULL;
  sf_mex_assign(&c52_e_y, sf_mex_create("y", &c52_e_u, 3, 0U, 0U, 0U, 0), false);
  sf_mex_addfield(c52_y, c52_e_y, "color", "color", 0);
  c52_f_u = c52_u.position;
  c52_f_y = NULL;
  sf_mex_assign(&c52_f_y, sf_mex_create("y", &c52_f_u, 3, 0U, 0U, 0U, 0), false);
  sf_mex_addfield(c52_y, c52_f_y, "position", "position", 0);
  c52_g_u = c52_u.valid;
  c52_g_y = NULL;
  sf_mex_assign(&c52_g_y, sf_mex_create("y", &c52_g_u, 3, 0U, 0U, 0U, 0), false);
  sf_mex_addfield(c52_y, c52_g_y, "valid", "valid", 0);
  sf_mex_assign(&c52_mxArrayOutData, c52_y, false);
  return c52_mxArrayOutData;
}

static const mxArray *c52_players_bus_io(void *chartInstanceVoid, void
  *c52_pData)
{
  const mxArray *c52_mxVal = NULL;
  int32_T c52_i64;
  int32_T c52_i65;
  c52_Player c52_tmp[6];
  SFc52_LessonIIInstanceStruct *chartInstance;
  chartInstance = (SFc52_LessonIIInstanceStruct *)chartInstanceVoid;
  c52_mxVal = NULL;
  for (c52_i64 = 0; c52_i64 < 6; c52_i64++) {
    for (c52_i65 = 0; c52_i65 < 1; c52_i65++) {
      c52_tmp[c52_i65 + c52_i64].x = *(int8_T *)&((char_T *)(c52_Player *)
        &((char_T *)(c52_Player (*)[6])c52_pData)[8 * (c52_i65 + c52_i64)])[0];
      c52_tmp[c52_i65 + c52_i64].y = *(int8_T *)&((char_T *)(c52_Player *)
        &((char_T *)(c52_Player (*)[6])c52_pData)[8 * (c52_i65 + c52_i64)])[1];
      c52_tmp[c52_i65 + c52_i64].orientation = *(int16_T *)&((char_T *)
        (c52_Player *)&((char_T *)(c52_Player (*)[6])c52_pData)[8 * (c52_i65 +
        c52_i64)])[2];
      c52_tmp[c52_i65 + c52_i64].color = *(uint8_T *)&((char_T *)(c52_Player *)
        &((char_T *)(c52_Player (*)[6])c52_pData)[8 * (c52_i65 + c52_i64)])[4];
      c52_tmp[c52_i65 + c52_i64].position = *(uint8_T *)&((char_T *)(c52_Player *)
        &((char_T *)(c52_Player (*)[6])c52_pData)[8 * (c52_i65 + c52_i64)])[5];
      c52_tmp[c52_i65 + c52_i64].valid = *(uint8_T *)&((char_T *)(c52_Player *)
        &((char_T *)(c52_Player (*)[6])c52_pData)[8 * (c52_i65 + c52_i64)])[6];
    }
  }

  sf_mex_assign(&c52_mxVal, c52_h_sf_marshallOut(chartInstance, c52_tmp), false);
  return c52_mxVal;
}

static const mxArray *c52_h_sf_marshallOut(void *chartInstanceVoid, void
  *c52_inData)
{
  const mxArray *c52_mxArrayOutData;
  int32_T c52_i66;
  c52_Player c52_b_inData[6];
  int32_T c52_i67;
  c52_Player c52_u[6];
  const mxArray *c52_y = NULL;
  int32_T c52_i68;
  int32_T c52_iv14[2];
  int32_T c52_i69;
  const c52_Player *c52_r3;
  int8_T c52_b_u;
  const mxArray *c52_b_y = NULL;
  int8_T c52_c_u;
  const mxArray *c52_c_y = NULL;
  int16_T c52_d_u;
  const mxArray *c52_d_y = NULL;
  uint8_T c52_e_u;
  const mxArray *c52_e_y = NULL;
  uint8_T c52_f_u;
  const mxArray *c52_f_y = NULL;
  uint8_T c52_g_u;
  const mxArray *c52_g_y = NULL;
  SFc52_LessonIIInstanceStruct *chartInstance;
  chartInstance = (SFc52_LessonIIInstanceStruct *)chartInstanceVoid;
  c52_mxArrayOutData = NULL;
  c52_mxArrayOutData = NULL;
  for (c52_i66 = 0; c52_i66 < 6; c52_i66++) {
    c52_b_inData[c52_i66] = (*(c52_Player (*)[6])c52_inData)[c52_i66];
  }

  for (c52_i67 = 0; c52_i67 < 6; c52_i67++) {
    c52_u[c52_i67] = c52_b_inData[c52_i67];
  }

  c52_y = NULL;
  for (c52_i68 = 0; c52_i68 < 2; c52_i68++) {
    c52_iv14[c52_i68] = 1 + 5 * c52_i68;
  }

  sf_mex_assign(&c52_y, sf_mex_createstructarray("structure", 2, c52_iv14),
                false);
  for (c52_i69 = 0; c52_i69 < 6; c52_i69++) {
    c52_r3 = &c52_u[c52_i69];
    c52_b_u = c52_r3->x;
    c52_b_y = NULL;
    sf_mex_assign(&c52_b_y, sf_mex_create("y", &c52_b_u, 2, 0U, 0U, 0U, 0),
                  false);
    sf_mex_addfield(c52_y, c52_b_y, "x", "x", c52_i69);
    c52_c_u = c52_r3->y;
    c52_c_y = NULL;
    sf_mex_assign(&c52_c_y, sf_mex_create("y", &c52_c_u, 2, 0U, 0U, 0U, 0),
                  false);
    sf_mex_addfield(c52_y, c52_c_y, "y", "y", c52_i69);
    c52_d_u = c52_r3->orientation;
    c52_d_y = NULL;
    sf_mex_assign(&c52_d_y, sf_mex_create("y", &c52_d_u, 4, 0U, 0U, 0U, 0),
                  false);
    sf_mex_addfield(c52_y, c52_d_y, "orientation", "orientation", c52_i69);
    c52_e_u = c52_r3->color;
    c52_e_y = NULL;
    sf_mex_assign(&c52_e_y, sf_mex_create("y", &c52_e_u, 3, 0U, 0U, 0U, 0),
                  false);
    sf_mex_addfield(c52_y, c52_e_y, "color", "color", c52_i69);
    c52_f_u = c52_r3->position;
    c52_f_y = NULL;
    sf_mex_assign(&c52_f_y, sf_mex_create("y", &c52_f_u, 3, 0U, 0U, 0U, 0),
                  false);
    sf_mex_addfield(c52_y, c52_f_y, "position", "position", c52_i69);
    c52_g_u = c52_r3->valid;
    c52_g_y = NULL;
    sf_mex_assign(&c52_g_y, sf_mex_create("y", &c52_g_u, 3, 0U, 0U, 0U, 0),
                  false);
    sf_mex_addfield(c52_y, c52_g_y, "valid", "valid", c52_i69);
  }

  sf_mex_assign(&c52_mxArrayOutData, c52_y, false);
  return c52_mxArrayOutData;
}

static const mxArray *c52_ball_bus_io(void *chartInstanceVoid, void *c52_pData)
{
  const mxArray *c52_mxVal = NULL;
  c52_Ball c52_tmp;
  SFc52_LessonIIInstanceStruct *chartInstance;
  chartInstance = (SFc52_LessonIIInstanceStruct *)chartInstanceVoid;
  c52_mxVal = NULL;
  c52_tmp.x = *(int8_T *)&((char_T *)(c52_Ball *)c52_pData)[0];
  c52_tmp.y = *(int8_T *)&((char_T *)(c52_Ball *)c52_pData)[1];
  c52_tmp.valid = *(uint8_T *)&((char_T *)(c52_Ball *)c52_pData)[2];
  sf_mex_assign(&c52_mxVal, c52_i_sf_marshallOut(chartInstance, &c52_tmp), false);
  return c52_mxVal;
}

static const mxArray *c52_i_sf_marshallOut(void *chartInstanceVoid, void
  *c52_inData)
{
  const mxArray *c52_mxArrayOutData = NULL;
  c52_Ball c52_u;
  const mxArray *c52_y = NULL;
  int8_T c52_b_u;
  const mxArray *c52_b_y = NULL;
  int8_T c52_c_u;
  const mxArray *c52_c_y = NULL;
  uint8_T c52_d_u;
  const mxArray *c52_d_y = NULL;
  SFc52_LessonIIInstanceStruct *chartInstance;
  chartInstance = (SFc52_LessonIIInstanceStruct *)chartInstanceVoid;
  c52_mxArrayOutData = NULL;
  c52_u = *(c52_Ball *)c52_inData;
  c52_y = NULL;
  sf_mex_assign(&c52_y, sf_mex_createstruct("structure", 2, 1, 1), false);
  c52_b_u = c52_u.x;
  c52_b_y = NULL;
  sf_mex_assign(&c52_b_y, sf_mex_create("y", &c52_b_u, 2, 0U, 0U, 0U, 0), false);
  sf_mex_addfield(c52_y, c52_b_y, "x", "x", 0);
  c52_c_u = c52_u.y;
  c52_c_y = NULL;
  sf_mex_assign(&c52_c_y, sf_mex_create("y", &c52_c_u, 2, 0U, 0U, 0U, 0), false);
  sf_mex_addfield(c52_y, c52_c_y, "y", "y", 0);
  c52_d_u = c52_u.valid;
  c52_d_y = NULL;
  sf_mex_assign(&c52_d_y, sf_mex_create("y", &c52_d_u, 3, 0U, 0U, 0U, 0), false);
  sf_mex_addfield(c52_y, c52_d_y, "valid", "valid", 0);
  sf_mex_assign(&c52_mxArrayOutData, c52_y, false);
  return c52_mxArrayOutData;
}

static const mxArray *c52_finalWay_bus_io(void *chartInstanceVoid, void
  *c52_pData)
{
  const mxArray *c52_mxVal = NULL;
  c52_Waypoint c52_tmp;
  SFc52_LessonIIInstanceStruct *chartInstance;
  chartInstance = (SFc52_LessonIIInstanceStruct *)chartInstanceVoid;
  c52_mxVal = NULL;
  c52_tmp.x = *(int8_T *)&((char_T *)(c52_Waypoint *)c52_pData)[0];
  c52_tmp.y = *(int8_T *)&((char_T *)(c52_Waypoint *)c52_pData)[1];
  c52_tmp.orientation = *(int16_T *)&((char_T *)(c52_Waypoint *)c52_pData)[2];
  sf_mex_assign(&c52_mxVal, c52_j_sf_marshallOut(chartInstance, &c52_tmp), false);
  return c52_mxVal;
}

static const mxArray *c52_j_sf_marshallOut(void *chartInstanceVoid, void
  *c52_inData)
{
  const mxArray *c52_mxArrayOutData = NULL;
  c52_Waypoint c52_u;
  const mxArray *c52_y = NULL;
  int8_T c52_b_u;
  const mxArray *c52_b_y = NULL;
  int8_T c52_c_u;
  const mxArray *c52_c_y = NULL;
  int16_T c52_d_u;
  const mxArray *c52_d_y = NULL;
  SFc52_LessonIIInstanceStruct *chartInstance;
  chartInstance = (SFc52_LessonIIInstanceStruct *)chartInstanceVoid;
  c52_mxArrayOutData = NULL;
  c52_u = *(c52_Waypoint *)c52_inData;
  c52_y = NULL;
  sf_mex_assign(&c52_y, sf_mex_createstruct("structure", 2, 1, 1), false);
  c52_b_u = c52_u.x;
  c52_b_y = NULL;
  sf_mex_assign(&c52_b_y, sf_mex_create("y", &c52_b_u, 2, 0U, 0U, 0U, 0), false);
  sf_mex_addfield(c52_y, c52_b_y, "x", "x", 0);
  c52_c_u = c52_u.y;
  c52_c_y = NULL;
  sf_mex_assign(&c52_c_y, sf_mex_create("y", &c52_c_u, 2, 0U, 0U, 0U, 0), false);
  sf_mex_addfield(c52_y, c52_c_y, "y", "y", 0);
  c52_d_u = c52_u.orientation;
  c52_d_y = NULL;
  sf_mex_assign(&c52_d_y, sf_mex_create("y", &c52_d_u, 4, 0U, 0U, 0U, 0), false);
  sf_mex_addfield(c52_y, c52_d_y, "orientation", "orientation", 0);
  sf_mex_assign(&c52_mxArrayOutData, c52_y, false);
  return c52_mxArrayOutData;
}

static c52_Waypoint c52_h_emlrt_marshallIn(SFc52_LessonIIInstanceStruct
  *chartInstance, const mxArray *c52_b_finalWay, const char_T *c52_identifier)
{
  c52_Waypoint c52_y;
  emlrtMsgIdentifier c52_thisId;
  c52_thisId.fIdentifier = c52_identifier;
  c52_thisId.fParent = NULL;
  c52_y = c52_i_emlrt_marshallIn(chartInstance, sf_mex_dup(c52_b_finalWay),
    &c52_thisId);
  sf_mex_destroy(&c52_b_finalWay);
  return c52_y;
}

static c52_Waypoint c52_i_emlrt_marshallIn(SFc52_LessonIIInstanceStruct
  *chartInstance, const mxArray *c52_u, const emlrtMsgIdentifier *c52_parentId)
{
  c52_Waypoint c52_y;
  emlrtMsgIdentifier c52_thisId;
  static const char * c52_fieldNames[3] = { "x", "y", "orientation" };

  c52_thisId.fParent = c52_parentId;
  sf_mex_check_struct(c52_parentId, c52_u, 3, c52_fieldNames, 0U, NULL);
  c52_thisId.fIdentifier = "x";
  c52_y.x = c52_j_emlrt_marshallIn(chartInstance, sf_mex_dup(sf_mex_getfield
    (c52_u, "x", "x", 0)), &c52_thisId);
  c52_thisId.fIdentifier = "y";
  c52_y.y = c52_j_emlrt_marshallIn(chartInstance, sf_mex_dup(sf_mex_getfield
    (c52_u, "y", "y", 0)), &c52_thisId);
  c52_thisId.fIdentifier = "orientation";
  c52_y.orientation = c52_k_emlrt_marshallIn(chartInstance, sf_mex_dup
    (sf_mex_getfield(c52_u, "orientation", "orientation", 0)), &c52_thisId);
  sf_mex_destroy(&c52_u);
  return c52_y;
}

static int8_T c52_j_emlrt_marshallIn(SFc52_LessonIIInstanceStruct *chartInstance,
  const mxArray *c52_u, const emlrtMsgIdentifier *c52_parentId)
{
  int8_T c52_y;
  int8_T c52_i70;
  (void)chartInstance;
  sf_mex_import(c52_parentId, sf_mex_dup(c52_u), &c52_i70, 1, 2, 0U, 0, 0U, 0);
  c52_y = c52_i70;
  sf_mex_destroy(&c52_u);
  return c52_y;
}

static int16_T c52_k_emlrt_marshallIn(SFc52_LessonIIInstanceStruct
  *chartInstance, const mxArray *c52_u, const emlrtMsgIdentifier *c52_parentId)
{
  int16_T c52_y;
  int16_T c52_i71;
  (void)chartInstance;
  sf_mex_import(c52_parentId, sf_mex_dup(c52_u), &c52_i71, 1, 4, 0U, 0, 0U, 0);
  c52_y = c52_i71;
  sf_mex_destroy(&c52_u);
  return c52_y;
}

static void c52_g_sf_marshallIn(void *chartInstanceVoid, const mxArray
  *c52_mxArrayInData, const char_T *c52_varName, void *c52_outData)
{
  const mxArray *c52_b_finalWay;
  const char_T *c52_identifier;
  emlrtMsgIdentifier c52_thisId;
  c52_Waypoint c52_y;
  SFc52_LessonIIInstanceStruct *chartInstance;
  chartInstance = (SFc52_LessonIIInstanceStruct *)chartInstanceVoid;
  c52_b_finalWay = sf_mex_dup(c52_mxArrayInData);
  c52_identifier = c52_varName;
  c52_thisId.fIdentifier = c52_identifier;
  c52_thisId.fParent = NULL;
  c52_y = c52_i_emlrt_marshallIn(chartInstance, sf_mex_dup(c52_b_finalWay),
    &c52_thisId);
  sf_mex_destroy(&c52_b_finalWay);
  *(c52_Waypoint *)c52_outData = c52_y;
  sf_mex_destroy(&c52_mxArrayInData);
}

static const mxArray *c52_k_sf_marshallOut(void *chartInstanceVoid, void
  *c52_inData)
{
  const mxArray *c52_mxArrayOutData = NULL;
  int32_T c52_i72;
  int8_T c52_b_inData[2];
  int32_T c52_i73;
  int8_T c52_u[2];
  const mxArray *c52_y = NULL;
  SFc52_LessonIIInstanceStruct *chartInstance;
  chartInstance = (SFc52_LessonIIInstanceStruct *)chartInstanceVoid;
  c52_mxArrayOutData = NULL;
  for (c52_i72 = 0; c52_i72 < 2; c52_i72++) {
    c52_b_inData[c52_i72] = (*(int8_T (*)[2])c52_inData)[c52_i72];
  }

  for (c52_i73 = 0; c52_i73 < 2; c52_i73++) {
    c52_u[c52_i73] = c52_b_inData[c52_i73];
  }

  c52_y = NULL;
  sf_mex_assign(&c52_y, sf_mex_create("y", c52_u, 2, 0U, 1U, 0U, 2, 2, 1), false);
  sf_mex_assign(&c52_mxArrayOutData, c52_y, false);
  return c52_mxArrayOutData;
}

static void c52_l_emlrt_marshallIn(SFc52_LessonIIInstanceStruct *chartInstance,
  const mxArray *c52_b_startingPos, const char_T *c52_identifier, int8_T c52_y[2])
{
  emlrtMsgIdentifier c52_thisId;
  c52_thisId.fIdentifier = c52_identifier;
  c52_thisId.fParent = NULL;
  c52_m_emlrt_marshallIn(chartInstance, sf_mex_dup(c52_b_startingPos),
    &c52_thisId, c52_y);
  sf_mex_destroy(&c52_b_startingPos);
}

static void c52_m_emlrt_marshallIn(SFc52_LessonIIInstanceStruct *chartInstance,
  const mxArray *c52_u, const emlrtMsgIdentifier *c52_parentId, int8_T c52_y[2])
{
  int8_T c52_iv15[2];
  int32_T c52_i74;
  (void)chartInstance;
  sf_mex_import(c52_parentId, sf_mex_dup(c52_u), c52_iv15, 1, 2, 0U, 1, 0U, 2, 2,
                1);
  for (c52_i74 = 0; c52_i74 < 2; c52_i74++) {
    c52_y[c52_i74] = c52_iv15[c52_i74];
  }

  sf_mex_destroy(&c52_u);
}

static void c52_h_sf_marshallIn(void *chartInstanceVoid, const mxArray
  *c52_mxArrayInData, const char_T *c52_varName, void *c52_outData)
{
  const mxArray *c52_b_startingPos;
  const char_T *c52_identifier;
  emlrtMsgIdentifier c52_thisId;
  int8_T c52_y[2];
  int32_T c52_i75;
  SFc52_LessonIIInstanceStruct *chartInstance;
  chartInstance = (SFc52_LessonIIInstanceStruct *)chartInstanceVoid;
  c52_b_startingPos = sf_mex_dup(c52_mxArrayInData);
  c52_identifier = c52_varName;
  c52_thisId.fIdentifier = c52_identifier;
  c52_thisId.fParent = NULL;
  c52_m_emlrt_marshallIn(chartInstance, sf_mex_dup(c52_b_startingPos),
    &c52_thisId, c52_y);
  sf_mex_destroy(&c52_b_startingPos);
  for (c52_i75 = 0; c52_i75 < 2; c52_i75++) {
    (*(int8_T (*)[2])c52_outData)[c52_i75] = c52_y[c52_i75];
  }

  sf_mex_destroy(&c52_mxArrayInData);
}

static const mxArray *c52_l_sf_marshallOut(void *chartInstanceVoid, void
  *c52_inData)
{
  const mxArray *c52_mxArrayOutData;
  int32_T c52_i76;
  c52_Player c52_b_inData[3];
  int32_T c52_i77;
  c52_Player c52_u[3];
  const mxArray *c52_y = NULL;
  static int32_T c52_iv16[1] = { 3 };

  int32_T c52_iv17[1];
  int32_T c52_i78;
  const c52_Player *c52_r4;
  int8_T c52_b_u;
  const mxArray *c52_b_y = NULL;
  int8_T c52_c_u;
  const mxArray *c52_c_y = NULL;
  int16_T c52_d_u;
  const mxArray *c52_d_y = NULL;
  uint8_T c52_e_u;
  const mxArray *c52_e_y = NULL;
  uint8_T c52_f_u;
  const mxArray *c52_f_y = NULL;
  uint8_T c52_g_u;
  const mxArray *c52_g_y = NULL;
  SFc52_LessonIIInstanceStruct *chartInstance;
  chartInstance = (SFc52_LessonIIInstanceStruct *)chartInstanceVoid;
  c52_mxArrayOutData = NULL;
  c52_mxArrayOutData = NULL;
  for (c52_i76 = 0; c52_i76 < 3; c52_i76++) {
    c52_b_inData[c52_i76] = (*(c52_Player (*)[3])c52_inData)[c52_i76];
  }

  for (c52_i77 = 0; c52_i77 < 3; c52_i77++) {
    c52_u[c52_i77] = c52_b_inData[c52_i77];
  }

  c52_y = NULL;
  c52_iv17[0] = c52_iv16[0];
  sf_mex_assign(&c52_y, sf_mex_createstructarray("structure", 1, c52_iv17),
                false);
  for (c52_i78 = 0; c52_i78 < 3; c52_i78++) {
    c52_r4 = &c52_u[c52_i78];
    c52_b_u = c52_r4->x;
    c52_b_y = NULL;
    sf_mex_assign(&c52_b_y, sf_mex_create("y", &c52_b_u, 2, 0U, 0U, 0U, 0),
                  false);
    sf_mex_addfield(c52_y, c52_b_y, "x", "x", c52_i78);
    c52_c_u = c52_r4->y;
    c52_c_y = NULL;
    sf_mex_assign(&c52_c_y, sf_mex_create("y", &c52_c_u, 2, 0U, 0U, 0U, 0),
                  false);
    sf_mex_addfield(c52_y, c52_c_y, "y", "y", c52_i78);
    c52_d_u = c52_r4->orientation;
    c52_d_y = NULL;
    sf_mex_assign(&c52_d_y, sf_mex_create("y", &c52_d_u, 4, 0U, 0U, 0U, 0),
                  false);
    sf_mex_addfield(c52_y, c52_d_y, "orientation", "orientation", c52_i78);
    c52_e_u = c52_r4->color;
    c52_e_y = NULL;
    sf_mex_assign(&c52_e_y, sf_mex_create("y", &c52_e_u, 3, 0U, 0U, 0U, 0),
                  false);
    sf_mex_addfield(c52_y, c52_e_y, "color", "color", c52_i78);
    c52_f_u = c52_r4->position;
    c52_f_y = NULL;
    sf_mex_assign(&c52_f_y, sf_mex_create("y", &c52_f_u, 3, 0U, 0U, 0U, 0),
                  false);
    sf_mex_addfield(c52_y, c52_f_y, "position", "position", c52_i78);
    c52_g_u = c52_r4->valid;
    c52_g_y = NULL;
    sf_mex_assign(&c52_g_y, sf_mex_create("y", &c52_g_u, 3, 0U, 0U, 0U, 0),
                  false);
    sf_mex_addfield(c52_y, c52_g_y, "valid", "valid", c52_i78);
  }

  sf_mex_assign(&c52_mxArrayOutData, c52_y, false);
  return c52_mxArrayOutData;
}

static void c52_n_emlrt_marshallIn(SFc52_LessonIIInstanceStruct *chartInstance,
  const mxArray *c52_b_enemy, const char_T *c52_identifier, c52_Player c52_y[3])
{
  emlrtMsgIdentifier c52_thisId;
  c52_thisId.fIdentifier = c52_identifier;
  c52_thisId.fParent = NULL;
  c52_o_emlrt_marshallIn(chartInstance, sf_mex_dup(c52_b_enemy), &c52_thisId,
    c52_y);
  sf_mex_destroy(&c52_b_enemy);
}

static void c52_o_emlrt_marshallIn(SFc52_LessonIIInstanceStruct *chartInstance,
  const mxArray *c52_u, const emlrtMsgIdentifier *c52_parentId, c52_Player
  c52_y[3])
{
  static uint32_T c52_uv0[1] = { 3U };

  uint32_T c52_uv1[1];
  emlrtMsgIdentifier c52_thisId;
  static const char * c52_fieldNames[6] = { "x", "y", "orientation", "color",
    "position", "valid" };

  c52_Player (*c52_r5)[3];
  int32_T c52_i79;
  c52_uv1[0] = c52_uv0[0];
  c52_thisId.fParent = c52_parentId;
  sf_mex_check_struct(c52_parentId, c52_u, 6, c52_fieldNames, 1U, c52_uv1);
  c52_r5 = (c52_Player (*)[3])c52_y;
  for (c52_i79 = 0; c52_i79 < 3; c52_i79++) {
    c52_thisId.fIdentifier = "x";
    (*c52_r5)[c52_i79].x = c52_j_emlrt_marshallIn(chartInstance, sf_mex_dup
      (sf_mex_getfield(c52_u, "x", "x", c52_i79)), &c52_thisId);
    c52_thisId.fIdentifier = "y";
    (*c52_r5)[c52_i79].y = c52_j_emlrt_marshallIn(chartInstance, sf_mex_dup
      (sf_mex_getfield(c52_u, "y", "y", c52_i79)), &c52_thisId);
    c52_thisId.fIdentifier = "orientation";
    (*c52_r5)[c52_i79].orientation = c52_k_emlrt_marshallIn(chartInstance,
      sf_mex_dup(sf_mex_getfield(c52_u, "orientation", "orientation", c52_i79)),
      &c52_thisId);
    c52_thisId.fIdentifier = "color";
    (*c52_r5)[c52_i79].color = c52_e_emlrt_marshallIn(chartInstance, sf_mex_dup
      (sf_mex_getfield(c52_u, "color", "color", c52_i79)), &c52_thisId);
    c52_thisId.fIdentifier = "position";
    (*c52_r5)[c52_i79].position = c52_e_emlrt_marshallIn(chartInstance,
      sf_mex_dup(sf_mex_getfield(c52_u, "position", "position", c52_i79)),
      &c52_thisId);
    c52_thisId.fIdentifier = "valid";
    (*c52_r5)[c52_i79].valid = c52_e_emlrt_marshallIn(chartInstance, sf_mex_dup
      (sf_mex_getfield(c52_u, "valid", "valid", c52_i79)), &c52_thisId);
  }

  sf_mex_destroy(&c52_u);
}

static void c52_i_sf_marshallIn(void *chartInstanceVoid, const mxArray
  *c52_mxArrayInData, const char_T *c52_varName, void *c52_outData)
{
  const mxArray *c52_b_enemy;
  const char_T *c52_identifier;
  emlrtMsgIdentifier c52_thisId;
  c52_Player c52_y[3];
  int32_T c52_i80;
  SFc52_LessonIIInstanceStruct *chartInstance;
  chartInstance = (SFc52_LessonIIInstanceStruct *)chartInstanceVoid;
  c52_b_enemy = sf_mex_dup(c52_mxArrayInData);
  c52_identifier = c52_varName;
  c52_thisId.fIdentifier = c52_identifier;
  c52_thisId.fParent = NULL;
  c52_o_emlrt_marshallIn(chartInstance, sf_mex_dup(c52_b_enemy), &c52_thisId,
    c52_y);
  sf_mex_destroy(&c52_b_enemy);
  for (c52_i80 = 0; c52_i80 < 3; c52_i80++) {
    (*(c52_Player (*)[3])c52_outData)[c52_i80] = c52_y[c52_i80];
  }

  sf_mex_destroy(&c52_mxArrayInData);
}

static void c52_p_emlrt_marshallIn(SFc52_LessonIIInstanceStruct *chartInstance,
  const mxArray *c52_b_dataWrittenToVector, const char_T *c52_identifier,
  boolean_T c52_y[7])
{
  emlrtMsgIdentifier c52_thisId;
  c52_thisId.fIdentifier = c52_identifier;
  c52_thisId.fParent = NULL;
  c52_q_emlrt_marshallIn(chartInstance, sf_mex_dup(c52_b_dataWrittenToVector),
    &c52_thisId, c52_y);
  sf_mex_destroy(&c52_b_dataWrittenToVector);
}

static void c52_q_emlrt_marshallIn(SFc52_LessonIIInstanceStruct *chartInstance,
  const mxArray *c52_u, const emlrtMsgIdentifier *c52_parentId, boolean_T c52_y
  [7])
{
  boolean_T c52_bv1[7];
  int32_T c52_i81;
  (void)chartInstance;
  sf_mex_import(c52_parentId, sf_mex_dup(c52_u), c52_bv1, 1, 11, 0U, 1, 0U, 1, 7);
  for (c52_i81 = 0; c52_i81 < 7; c52_i81++) {
    c52_y[c52_i81] = c52_bv1[c52_i81];
  }

  sf_mex_destroy(&c52_u);
}

static const mxArray *c52_r_emlrt_marshallIn(SFc52_LessonIIInstanceStruct
  *chartInstance, const mxArray *c52_b_setSimStateSideEffectsInfo, const char_T *
  c52_identifier)
{
  const mxArray *c52_y = NULL;
  emlrtMsgIdentifier c52_thisId;
  c52_y = NULL;
  c52_thisId.fIdentifier = c52_identifier;
  c52_thisId.fParent = NULL;
  sf_mex_assign(&c52_y, c52_s_emlrt_marshallIn(chartInstance, sf_mex_dup
    (c52_b_setSimStateSideEffectsInfo), &c52_thisId), false);
  sf_mex_destroy(&c52_b_setSimStateSideEffectsInfo);
  return c52_y;
}

static const mxArray *c52_s_emlrt_marshallIn(SFc52_LessonIIInstanceStruct
  *chartInstance, const mxArray *c52_u, const emlrtMsgIdentifier *c52_parentId)
{
  const mxArray *c52_y = NULL;
  (void)chartInstance;
  (void)c52_parentId;
  c52_y = NULL;
  sf_mex_assign(&c52_y, sf_mex_duplicatearraysafe(&c52_u), false);
  sf_mex_destroy(&c52_u);
  return c52_y;
}

static void c52_updateDataWrittenToVector(SFc52_LessonIIInstanceStruct
  *chartInstance, uint32_T c52_vectorIndex)
{
  chartInstance->c52_dataWrittenToVector[(uint32_T)_SFD_EML_ARRAY_BOUNDS_CHECK
    (0U, (int32_T)c52_vectorIndex, 0, 6, 1, 0)] = true;
}

static void c52_errorIfDataNotWrittenToFcn(SFc52_LessonIIInstanceStruct
  *chartInstance, uint32_T c52_vectorIndex, uint32_T c52_dataNumber, uint32_T
  c52_ssIdOfSourceObject, int32_T c52_offsetInSourceObject, int32_T
  c52_lengthInSourceObject)
{
  (void)c52_ssIdOfSourceObject;
  (void)c52_offsetInSourceObject;
  (void)c52_lengthInSourceObject;
  if (!chartInstance->c52_dataWrittenToVector[(uint32_T)
      _SFD_EML_ARRAY_BOUNDS_CHECK(0U, (int32_T)c52_vectorIndex, 0, 6, 1, 0)]) {
    _SFD_DATA_READ_BEFORE_WRITE_ERROR(c52_dataNumber);
  }
}

static void init_dsm_address_info(SFc52_LessonIIInstanceStruct *chartInstance)
{
  (void)chartInstance;
}

static void init_simulink_io_address(SFc52_LessonIIInstanceStruct *chartInstance)
{
  chartInstance->c52_me = (c52_Player *)ssGetInputPortSignal_wrapper
    (chartInstance->S, 0);
  chartInstance->c52_players = (c52_Player (*)[6])ssGetInputPortSignal_wrapper
    (chartInstance->S, 1);
  chartInstance->c52_ball = (c52_Ball *)ssGetInputPortSignal_wrapper
    (chartInstance->S, 2);
  chartInstance->c52_finalWay = (c52_Waypoint *)ssGetOutputPortSignal_wrapper
    (chartInstance->S, 1);
  chartInstance->c52_manualWay = (c52_Waypoint *)ssGetInputPortSignal_wrapper
    (chartInstance->S, 3);
  chartInstance->c52_GameOn = (uint8_T *)ssGetInputPortSignal_wrapper
    (chartInstance->S, 4);
}

/* SFunction Glue Code */
#ifdef utFree
#undef utFree
#endif

#ifdef utMalloc
#undef utMalloc
#endif

#ifdef __cplusplus

extern "C" void *utMalloc(size_t size);
extern "C" void utFree(void*);

#else

extern void *utMalloc(size_t size);
extern void utFree(void*);

#endif

void sf_c52_LessonII_get_check_sum(mxArray *plhs[])
{
  ((real_T *)mxGetPr((plhs[0])))[0] = (real_T)(2330218360U);
  ((real_T *)mxGetPr((plhs[0])))[1] = (real_T)(309663806U);
  ((real_T *)mxGetPr((plhs[0])))[2] = (real_T)(3250216747U);
  ((real_T *)mxGetPr((plhs[0])))[3] = (real_T)(1637680433U);
}

mxArray* sf_c52_LessonII_get_post_codegen_info(void);
mxArray *sf_c52_LessonII_get_autoinheritance_info(void)
{
  const char *autoinheritanceFields[] = { "checksum", "inputs", "parameters",
    "outputs", "locals", "postCodegenInfo" };

  mxArray *mxAutoinheritanceInfo = mxCreateStructMatrix(1, 1, sizeof
    (autoinheritanceFields)/sizeof(autoinheritanceFields[0]),
    autoinheritanceFields);

  {
    mxArray *mxChecksum = mxCreateString("ho0uzvNbzXohUNGgcZMDQ");
    mxSetField(mxAutoinheritanceInfo,0,"checksum",mxChecksum);
  }

  {
    const char *dataFields[] = { "size", "type", "complexity" };

    mxArray *mxData = mxCreateStructMatrix(1,5,3,dataFields);

    {
      mxArray *mxSize = mxCreateDoubleMatrix(1,2,mxREAL);
      double *pr = mxGetPr(mxSize);
      pr[0] = (double)(1);
      pr[1] = (double)(1);
      mxSetField(mxData,0,"size",mxSize);
    }

    {
      const char *typeFields[] = { "base", "fixpt" };

      mxArray *mxType = mxCreateStructMatrix(1,1,2,typeFields);
      mxSetField(mxType,0,"base",mxCreateDoubleScalar(13));
      mxSetField(mxType,0,"fixpt",mxCreateDoubleMatrix(0,0,mxREAL));
      mxSetField(mxData,0,"type",mxType);
    }

    mxSetField(mxData,0,"complexity",mxCreateDoubleScalar(0));

    {
      mxArray *mxSize = mxCreateDoubleMatrix(1,2,mxREAL);
      double *pr = mxGetPr(mxSize);
      pr[0] = (double)(1);
      pr[1] = (double)(6);
      mxSetField(mxData,1,"size",mxSize);
    }

    {
      const char *typeFields[] = { "base", "fixpt" };

      mxArray *mxType = mxCreateStructMatrix(1,1,2,typeFields);
      mxSetField(mxType,0,"base",mxCreateDoubleScalar(13));
      mxSetField(mxType,0,"fixpt",mxCreateDoubleMatrix(0,0,mxREAL));
      mxSetField(mxData,1,"type",mxType);
    }

    mxSetField(mxData,1,"complexity",mxCreateDoubleScalar(0));

    {
      mxArray *mxSize = mxCreateDoubleMatrix(1,2,mxREAL);
      double *pr = mxGetPr(mxSize);
      pr[0] = (double)(1);
      pr[1] = (double)(1);
      mxSetField(mxData,2,"size",mxSize);
    }

    {
      const char *typeFields[] = { "base", "fixpt" };

      mxArray *mxType = mxCreateStructMatrix(1,1,2,typeFields);
      mxSetField(mxType,0,"base",mxCreateDoubleScalar(13));
      mxSetField(mxType,0,"fixpt",mxCreateDoubleMatrix(0,0,mxREAL));
      mxSetField(mxData,2,"type",mxType);
    }

    mxSetField(mxData,2,"complexity",mxCreateDoubleScalar(0));

    {
      mxArray *mxSize = mxCreateDoubleMatrix(1,2,mxREAL);
      double *pr = mxGetPr(mxSize);
      pr[0] = (double)(1);
      pr[1] = (double)(1);
      mxSetField(mxData,3,"size",mxSize);
    }

    {
      const char *typeFields[] = { "base", "fixpt" };

      mxArray *mxType = mxCreateStructMatrix(1,1,2,typeFields);
      mxSetField(mxType,0,"base",mxCreateDoubleScalar(13));
      mxSetField(mxType,0,"fixpt",mxCreateDoubleMatrix(0,0,mxREAL));
      mxSetField(mxData,3,"type",mxType);
    }

    mxSetField(mxData,3,"complexity",mxCreateDoubleScalar(0));

    {
      mxArray *mxSize = mxCreateDoubleMatrix(1,2,mxREAL);
      double *pr = mxGetPr(mxSize);
      pr[0] = (double)(1);
      pr[1] = (double)(1);
      mxSetField(mxData,4,"size",mxSize);
    }

    {
      const char *typeFields[] = { "base", "fixpt" };

      mxArray *mxType = mxCreateStructMatrix(1,1,2,typeFields);
      mxSetField(mxType,0,"base",mxCreateDoubleScalar(3));
      mxSetField(mxType,0,"fixpt",mxCreateDoubleMatrix(0,0,mxREAL));
      mxSetField(mxData,4,"type",mxType);
    }

    mxSetField(mxData,4,"complexity",mxCreateDoubleScalar(0));
    mxSetField(mxAutoinheritanceInfo,0,"inputs",mxData);
  }

  {
    mxSetField(mxAutoinheritanceInfo,0,"parameters",mxCreateDoubleMatrix(0,0,
                mxREAL));
  }

  {
    const char *dataFields[] = { "size", "type", "complexity" };

    mxArray *mxData = mxCreateStructMatrix(1,1,3,dataFields);

    {
      mxArray *mxSize = mxCreateDoubleMatrix(1,2,mxREAL);
      double *pr = mxGetPr(mxSize);
      pr[0] = (double)(1);
      pr[1] = (double)(1);
      mxSetField(mxData,0,"size",mxSize);
    }

    {
      const char *typeFields[] = { "base", "fixpt" };

      mxArray *mxType = mxCreateStructMatrix(1,1,2,typeFields);
      mxSetField(mxType,0,"base",mxCreateDoubleScalar(13));
      mxSetField(mxType,0,"fixpt",mxCreateDoubleMatrix(0,0,mxREAL));
      mxSetField(mxData,0,"type",mxType);
    }

    mxSetField(mxData,0,"complexity",mxCreateDoubleScalar(0));
    mxSetField(mxAutoinheritanceInfo,0,"outputs",mxData);
  }

  {
    const char *dataFields[] = { "size", "type", "complexity" };

    mxArray *mxData = mxCreateStructMatrix(1,5,3,dataFields);

    {
      mxArray *mxSize = mxCreateDoubleMatrix(1,2,mxREAL);
      double *pr = mxGetPr(mxSize);
      pr[0] = (double)(2);
      pr[1] = (double)(1);
      mxSetField(mxData,0,"size",mxSize);
    }

    {
      const char *typeFields[] = { "base", "fixpt" };

      mxArray *mxType = mxCreateStructMatrix(1,1,2,typeFields);
      mxSetField(mxType,0,"base",mxCreateDoubleScalar(4));
      mxSetField(mxType,0,"fixpt",mxCreateDoubleMatrix(0,0,mxREAL));
      mxSetField(mxData,0,"type",mxType);
    }

    mxSetField(mxData,0,"complexity",mxCreateDoubleScalar(0));

    {
      mxArray *mxSize = mxCreateDoubleMatrix(1,2,mxREAL);
      double *pr = mxGetPr(mxSize);
      pr[0] = (double)(3);
      pr[1] = (double)(1);
      mxSetField(mxData,1,"size",mxSize);
    }

    {
      const char *typeFields[] = { "base", "fixpt" };

      mxArray *mxType = mxCreateStructMatrix(1,1,2,typeFields);
      mxSetField(mxType,0,"base",mxCreateDoubleScalar(13));
      mxSetField(mxType,0,"fixpt",mxCreateDoubleMatrix(0,0,mxREAL));
      mxSetField(mxData,1,"type",mxType);
    }

    mxSetField(mxData,1,"complexity",mxCreateDoubleScalar(0));

    {
      mxArray *mxSize = mxCreateDoubleMatrix(1,2,mxREAL);
      double *pr = mxGetPr(mxSize);
      pr[0] = (double)(3);
      pr[1] = (double)(1);
      mxSetField(mxData,2,"size",mxSize);
    }

    {
      const char *typeFields[] = { "base", "fixpt" };

      mxArray *mxType = mxCreateStructMatrix(1,1,2,typeFields);
      mxSetField(mxType,0,"base",mxCreateDoubleScalar(13));
      mxSetField(mxType,0,"fixpt",mxCreateDoubleMatrix(0,0,mxREAL));
      mxSetField(mxData,2,"type",mxType);
    }

    mxSetField(mxData,2,"complexity",mxCreateDoubleScalar(0));

    {
      mxArray *mxSize = mxCreateDoubleMatrix(1,2,mxREAL);
      double *pr = mxGetPr(mxSize);
      pr[0] = (double)(1);
      pr[1] = (double)(1);
      mxSetField(mxData,3,"size",mxSize);
    }

    {
      const char *typeFields[] = { "base", "fixpt" };

      mxArray *mxType = mxCreateStructMatrix(1,1,2,typeFields);
      mxSetField(mxType,0,"base",mxCreateDoubleScalar(13));
      mxSetField(mxType,0,"fixpt",mxCreateDoubleMatrix(0,0,mxREAL));
      mxSetField(mxData,3,"type",mxType);
    }

    mxSetField(mxData,3,"complexity",mxCreateDoubleScalar(0));

    {
      mxArray *mxSize = mxCreateDoubleMatrix(1,2,mxREAL);
      double *pr = mxGetPr(mxSize);
      pr[0] = (double)(1);
      pr[1] = (double)(1);
      mxSetField(mxData,4,"size",mxSize);
    }

    {
      const char *typeFields[] = { "base", "fixpt" };

      mxArray *mxType = mxCreateStructMatrix(1,1,2,typeFields);
      mxSetField(mxType,0,"base",mxCreateDoubleScalar(13));
      mxSetField(mxType,0,"fixpt",mxCreateDoubleMatrix(0,0,mxREAL));
      mxSetField(mxData,4,"type",mxType);
    }

    mxSetField(mxData,4,"complexity",mxCreateDoubleScalar(0));
    mxSetField(mxAutoinheritanceInfo,0,"locals",mxData);
  }

  {
    mxArray* mxPostCodegenInfo = sf_c52_LessonII_get_post_codegen_info();
    mxSetField(mxAutoinheritanceInfo,0,"postCodegenInfo",mxPostCodegenInfo);
  }

  return(mxAutoinheritanceInfo);
}

mxArray *sf_c52_LessonII_third_party_uses_info(void)
{
  mxArray * mxcell3p = mxCreateCellMatrix(1,0);
  return(mxcell3p);
}

mxArray *sf_c52_LessonII_jit_fallback_info(void)
{
  const char *infoFields[] = { "fallbackType", "fallbackReason",
    "incompatibleSymbol", };

  mxArray *mxInfo = mxCreateStructMatrix(1, 1, 3, infoFields);
  mxArray *fallbackReason = mxCreateString("feature_off");
  mxArray *incompatibleSymbol = mxCreateString("");
  mxArray *fallbackType = mxCreateString("early");
  mxSetField(mxInfo, 0, infoFields[0], fallbackType);
  mxSetField(mxInfo, 0, infoFields[1], fallbackReason);
  mxSetField(mxInfo, 0, infoFields[2], incompatibleSymbol);
  return mxInfo;
}

mxArray *sf_c52_LessonII_updateBuildInfo_args_info(void)
{
  mxArray *mxBIArgs = mxCreateCellMatrix(1,0);
  return mxBIArgs;
}

mxArray* sf_c52_LessonII_get_post_codegen_info(void)
{
  const char* fieldNames[] = { "exportedFunctionsUsedByThisChart",
    "exportedFunctionsChecksum" };

  mwSize dims[2] = { 1, 1 };

  mxArray* mxPostCodegenInfo = mxCreateStructArray(2, dims, sizeof(fieldNames)/
    sizeof(fieldNames[0]), fieldNames);

  {
    mxArray* mxExportedFunctionsChecksum = mxCreateString("");
    mwSize exp_dims[2] = { 0, 1 };

    mxArray* mxExportedFunctionsUsedByThisChart = mxCreateCellArray(2, exp_dims);
    mxSetField(mxPostCodegenInfo, 0, "exportedFunctionsUsedByThisChart",
               mxExportedFunctionsUsedByThisChart);
    mxSetField(mxPostCodegenInfo, 0, "exportedFunctionsChecksum",
               mxExportedFunctionsChecksum);
  }

  return mxPostCodegenInfo;
}

static const mxArray *sf_get_sim_state_info_c52_LessonII(void)
{
  const char *infoFields[] = { "chartChecksum", "varInfo" };

  mxArray *mxInfo = mxCreateStructMatrix(1, 1, 2, infoFields);
  const char *infoEncStr[] = {
    "100 S1x10'type','srcId','name','auxInfo'{{M[1],M[27],T\"finalWay\",},{M[3],M[113],T\"enemy\",},{M[3],M[114],T\"friends\",},{M[3],M[125],T\"kickWay\",},{M[3],M[124],T\"shootWay\",},{M[3],M[98],T\"startingPos\",},{M[8],M[0],T\"is_active_c52_LessonII\",},{M[9],M[0],T\"is_c52_LessonII\",},{M[9],M[17],T\"is_GameIsOn_Offence\",},{M[9],M[88],T\"is_waiting\",}}",
    "100 S1x2'type','srcId','name','auxInfo'{{M[11],M[0],T\"temporalCounter_i1\",S'et','os','ct'{{T\"wu\",M1x2[105 118],M[1]}}},{M[15],M[0],T\"dataWrittenToVector\",}}"
  };

  mxArray *mxVarInfo = sf_mex_decode_encoded_mx_struct_array(infoEncStr, 12, 10);
  mxArray *mxChecksum = mxCreateDoubleMatrix(1, 4, mxREAL);
  sf_c52_LessonII_get_check_sum(&mxChecksum);
  mxSetField(mxInfo, 0, infoFields[0], mxChecksum);
  mxSetField(mxInfo, 0, infoFields[1], mxVarInfo);
  return mxInfo;
}

static void chart_debug_initialization(SimStruct *S, unsigned int
  fullDebuggerInitialization)
{
  if (!sim_mode_is_rtw_gen(S)) {
    SFc52_LessonIIInstanceStruct *chartInstance;
    ChartRunTimeInfo * crtInfo = (ChartRunTimeInfo *)(ssGetUserData(S));
    ChartInfoStruct * chartInfo = (ChartInfoStruct *)(crtInfo->instanceInfo);
    chartInstance = (SFc52_LessonIIInstanceStruct *) chartInfo->chartInstance;
    if (ssIsFirstInitCond(S) && fullDebuggerInitialization==1) {
      /* do this only if simulation is starting */
      {
        unsigned int chartAlreadyPresent;
        chartAlreadyPresent = sf_debug_initialize_chart
          (sfGlobalDebugInstanceStruct,
           _LessonIIMachineNumber_,
           52,
           12,
           10,
           0,
           14,
           0,
           0,
           0,
           0,
           0,
           &(chartInstance->chartNumber),
           &(chartInstance->instanceNumber),
           (void *)S);

        /* Each instance must initialize its own list of scripts */
        init_script_number_translation(_LessonIIMachineNumber_,
          chartInstance->chartNumber,chartInstance->instanceNumber);
        if (chartAlreadyPresent==0) {
          /* this is the first instance */
          sf_debug_set_chart_disable_implicit_casting
            (sfGlobalDebugInstanceStruct,_LessonIIMachineNumber_,
             chartInstance->chartNumber,1);
          sf_debug_set_chart_event_thresholds(sfGlobalDebugInstanceStruct,
            _LessonIIMachineNumber_,
            chartInstance->chartNumber,
            0,
            0,
            0);
          _SFD_SET_DATA_PROPS(0,1,1,0,"me");
          _SFD_SET_DATA_PROPS(1,1,1,0,"players");
          _SFD_SET_DATA_PROPS(2,1,1,0,"ball");
          _SFD_SET_DATA_PROPS(3,2,0,1,"finalWay");
          _SFD_SET_DATA_PROPS(4,1,1,0,"manualWay");
          _SFD_SET_DATA_PROPS(5,0,0,0,"startingPos");
          _SFD_SET_DATA_PROPS(6,1,1,0,"GameOn");
          _SFD_SET_DATA_PROPS(7,0,0,0,"enemy");
          _SFD_SET_DATA_PROPS(8,0,0,0,"friends");
          _SFD_SET_DATA_PROPS(9,0,0,0,"shootWay");
          _SFD_SET_DATA_PROPS(10,0,0,0,"kickWay");
          _SFD_SET_DATA_PROPS(11,8,0,0,"");
          _SFD_SET_DATA_PROPS(12,8,0,0,"");
          _SFD_SET_DATA_PROPS(13,9,0,0,"");
          _SFD_STATE_INFO(0,0,0);
          _SFD_STATE_INFO(1,0,0);
          _SFD_STATE_INFO(2,0,0);
          _SFD_STATE_INFO(3,0,0);
          _SFD_STATE_INFO(4,0,0);
          _SFD_STATE_INFO(9,0,0);
          _SFD_STATE_INFO(10,0,0);
          _SFD_STATE_INFO(11,0,0);
          _SFD_STATE_INFO(5,0,2);
          _SFD_STATE_INFO(6,0,2);
          _SFD_STATE_INFO(7,0,2);
          _SFD_STATE_INFO(8,0,2);
          _SFD_CH_SUBSTATE_COUNT(2);
          _SFD_CH_SUBSTATE_DECOMP(0);
          _SFD_CH_SUBSTATE_INDEX(0,0);
          _SFD_CH_SUBSTATE_INDEX(1,9);
          _SFD_ST_SUBSTATE_COUNT(0,4);
          _SFD_ST_SUBSTATE_INDEX(0,0,1);
          _SFD_ST_SUBSTATE_INDEX(0,1,2);
          _SFD_ST_SUBSTATE_INDEX(0,2,3);
          _SFD_ST_SUBSTATE_INDEX(0,3,4);
          _SFD_ST_SUBSTATE_COUNT(1,0);
          _SFD_ST_SUBSTATE_COUNT(2,0);
          _SFD_ST_SUBSTATE_COUNT(3,0);
          _SFD_ST_SUBSTATE_COUNT(4,0);
          _SFD_ST_SUBSTATE_COUNT(9,2);
          _SFD_ST_SUBSTATE_INDEX(9,0,10);
          _SFD_ST_SUBSTATE_INDEX(9,1,11);
          _SFD_ST_SUBSTATE_COUNT(10,0);
          _SFD_ST_SUBSTATE_COUNT(11,0);
        }

        _SFD_CV_INIT_CHART(2,1,0,0);

        {
          _SFD_CV_INIT_STATE(0,4,1,1,0,0,NULL,NULL);
        }

        {
          _SFD_CV_INIT_STATE(1,0,0,0,0,0,NULL,NULL);
        }

        {
          _SFD_CV_INIT_STATE(2,0,0,0,0,0,NULL,NULL);
        }

        {
          _SFD_CV_INIT_STATE(3,0,0,0,0,0,NULL,NULL);
        }

        {
          _SFD_CV_INIT_STATE(4,0,0,0,0,0,NULL,NULL);
        }

        {
          _SFD_CV_INIT_STATE(9,2,1,1,0,0,NULL,NULL);
        }

        {
          _SFD_CV_INIT_STATE(10,0,0,0,0,0,NULL,NULL);
        }

        {
          _SFD_CV_INIT_STATE(11,0,0,0,0,0,NULL,NULL);
        }

        {
          _SFD_CV_INIT_STATE(5,0,0,0,0,0,NULL,NULL);
        }

        {
          _SFD_CV_INIT_STATE(6,0,0,0,0,0,NULL,NULL);
        }

        {
          _SFD_CV_INIT_STATE(7,0,0,0,0,0,NULL,NULL);
        }

        {
          _SFD_CV_INIT_STATE(8,0,0,0,0,0,NULL,NULL);
        }

        _SFD_CV_INIT_TRANS(3,0,NULL,NULL,0,NULL);
        _SFD_CV_INIT_TRANS(0,0,NULL,NULL,0,NULL);
        _SFD_CV_INIT_TRANS(1,0,NULL,NULL,0,NULL);
        _SFD_CV_INIT_TRANS(5,0,NULL,NULL,0,NULL);
        _SFD_CV_INIT_TRANS(7,0,NULL,NULL,0,NULL);
        _SFD_CV_INIT_TRANS(9,0,NULL,NULL,0,NULL);
        _SFD_CV_INIT_TRANS(6,0,NULL,NULL,0,NULL);
        _SFD_CV_INIT_TRANS(8,0,NULL,NULL,0,NULL);
        _SFD_CV_INIT_TRANS(2,0,NULL,NULL,0,NULL);
        _SFD_CV_INIT_TRANS(4,0,NULL,NULL,0,NULL);

        /* Initialization of MATLAB Function Model Coverage */
        _SFD_CV_INIT_EML(5,1,1,2,0,9,0,0,0,0,0);
        _SFD_CV_INIT_EML_FCN(5,0,"calcShootWay",0,-1,382);
        _SFD_CV_INIT_EML_SATURATION(5,1,0,131,-1,138);
        _SFD_CV_INIT_EML_SATURATION(5,1,1,148,-1,187);
        _SFD_CV_INIT_EML_SATURATION(5,1,2,193,-1,254);
        _SFD_CV_INIT_EML_SATURATION(5,1,3,225,-1,251);
        _SFD_CV_INIT_EML_SATURATION(5,1,4,268,-1,281);
        _SFD_CV_INIT_EML_SATURATION(5,1,5,294,-1,307);
        _SFD_CV_INIT_EML_SATURATION(5,1,6,330,-1,376);
        _SFD_CV_INIT_EML_SATURATION(5,1,7,350,-1,357);
        _SFD_CV_INIT_EML_SATURATION(5,1,8,366,-1,373);
        _SFD_CV_INIT_EML_IF(5,1,0,22,37,63,95);
        _SFD_CV_INIT_EML_IF(5,1,1,96,119,-1,143);
        _SFD_CV_INIT_EML_RELATIONAL(5,1,0,25,37,-1,4);
        _SFD_CV_INIT_EML_RELATIONAL(5,1,1,99,119,0,1);
        _SFD_CV_INIT_EML(7,1,1,0,0,0,1,0,0,0,0);
        _SFD_CV_INIT_EML_FCN(7,0,"calcStartTeams",0,-1,221);

        {
          static int caseStart[] = { -1, 44, 132 };

          static int caseExprEnd[] = { 8, 59, 147 };

          _SFD_CV_INIT_EML_SWITCH(7,1,0,24,40,219,3,&(caseStart[0]),
            &(caseExprEnd[0]));
        }

        _SFD_CV_INIT_EML(6,1,1,1,0,1,1,0,0,0,0);
        _SFD_CV_INIT_EML_FCN(6,0,"calcStartPos",0,-1,262);
        _SFD_CV_INIT_EML_SATURATION(6,1,0,243,-1,255);
        _SFD_CV_INIT_EML_IF(6,1,0,210,226,-1,260);

        {
          static int caseStart[] = { -1, 45, 100, 155 };

          static int caseExprEnd[] = { 8, 60, 115, 170 };

          _SFD_CV_INIT_EML_SWITCH(6,1,0,22,41,209,4,&(caseStart[0]),
            &(caseExprEnd[0]));
        }

        _SFD_CV_INIT_EML_RELATIONAL(6,1,0,213,226,0,0);
        _SFD_CV_INIT_EML(8,1,1,1,0,1,0,0,0,0,0);
        _SFD_CV_INIT_EML_FCN(8,0,"checkReached",0,-1,145);
        _SFD_CV_INIT_EML_SATURATION(8,1,0,78,-1,105);
        _SFD_CV_INIT_EML_IF(8,1,0,63,113,-1,142);
        _SFD_CV_INIT_EML_RELATIONAL(8,1,0,66,113,-1,2);
        _SFD_CV_INIT_EML(2,1,0,0,0,0,0,0,0,0,0);
        _SFD_CV_INIT_EML(3,1,0,0,0,0,0,0,0,0,0);
        _SFD_CV_INIT_EML(1,1,0,0,0,0,0,0,0,0,0);
        _SFD_CV_INIT_EML(4,1,0,0,0,0,0,0,0,0,0);
        _SFD_CV_INIT_EML(10,1,0,0,0,0,0,0,0,0,0);
        _SFD_CV_INIT_EML(11,1,0,0,0,0,0,0,0,0,0);
        _SFD_CV_INIT_EML(7,0,0,1,0,0,0,0,0,0,0);
        _SFD_CV_INIT_EML_IF(7,0,0,0,14,0,14);
        _SFD_CV_INIT_EML(9,0,0,1,0,0,0,0,0,2,1);
        _SFD_CV_INIT_EML_IF(9,0,0,1,61,1,48);

        {
          static int condStart[] = { 1, 19 };

          static int condEnd[] = { 15, 61 };

          static int pfixExpr[] = { 0, 1, -3 };

          _SFD_CV_INIT_EML_MCDC(9,0,0,1,61,2,0,&(condStart[0]),&(condEnd[0]),3,
                                &(pfixExpr[0]));
        }

        _SFD_CV_INIT_EML_RELATIONAL(9,0,0,19,61,0,0);
        _SFD_CV_INIT_EML(6,0,0,1,0,0,0,0,0,0,0);
        _SFD_CV_INIT_EML_IF(6,0,0,1,50,1,48);
        _SFD_CV_INIT_EML_RELATIONAL(6,0,0,1,50,0,0);
        _SFD_CV_INIT_EML(8,0,0,1,0,2,0,0,0,0,0);
        _SFD_CV_INIT_EML_SATURATION(8,0,0,1,-1,43);
        _SFD_CV_INIT_EML_SATURATION(8,0,1,5,-1,42);
        _SFD_CV_INIT_EML_IF(8,0,0,1,45,1,45);
        _SFD_CV_INIT_EML_RELATIONAL(8,0,0,1,45,-1,2);
        _SFD_CV_INIT_EML(4,0,0,1,0,0,0,0,0,0,0);
        _SFD_CV_INIT_EML_IF(4,0,0,1,38,1,38);
        _SFD_CV_INIT_EML_RELATIONAL(4,0,0,1,38,0,0);
        _SFD_CV_INIT_EML(3,0,0,0,0,0,0,0,0,0,0);
        _SFD_CV_INIT_EML(0,0,0,1,0,0,0,0,0,0,0);
        _SFD_CV_INIT_EML_IF(0,0,0,1,17,1,17);
        _SFD_CV_INIT_EML_RELATIONAL(0,0,0,1,17,0,0);
        _SFD_CV_INIT_EML(1,0,0,1,0,0,0,0,0,0,0);
        _SFD_CV_INIT_EML_IF(1,0,0,1,17,1,17);
        _SFD_CV_INIT_EML_RELATIONAL(1,0,0,1,17,0,0);
        _SFD_SET_DATA_COMPILED_PROPS(0,SF_STRUCT,0,NULL,0,0,0,0.0,1.0,0,0,
          (MexFcnForType)c52_me_bus_io,(MexInFcnForType)NULL);

        {
          unsigned int dimVector[2];
          dimVector[0]= 1;
          dimVector[1]= 6;
          _SFD_SET_DATA_COMPILED_PROPS(1,SF_STRUCT,2,&(dimVector[0]),0,0,0,0.0,
            1.0,0,0,(MexFcnForType)c52_players_bus_io,(MexInFcnForType)NULL);
        }

        _SFD_SET_DATA_COMPILED_PROPS(2,SF_STRUCT,0,NULL,0,0,0,0.0,1.0,0,0,
          (MexFcnForType)c52_ball_bus_io,(MexInFcnForType)NULL);
        _SFD_SET_DATA_COMPILED_PROPS(3,SF_STRUCT,0,NULL,0,0,0,0.0,1.0,0,0,
          (MexFcnForType)c52_finalWay_bus_io,(MexInFcnForType)NULL);
        _SFD_SET_DATA_COMPILED_PROPS(4,SF_STRUCT,0,NULL,0,0,0,0.0,1.0,0,0,
          (MexFcnForType)c52_finalWay_bus_io,(MexInFcnForType)NULL);

        {
          unsigned int dimVector[2];
          dimVector[0]= 2;
          dimVector[1]= 1;
          _SFD_SET_DATA_COMPILED_PROPS(5,SF_INT8,2,&(dimVector[0]),0,0,0,0.0,1.0,
            0,0,(MexFcnForType)c52_k_sf_marshallOut,(MexInFcnForType)
            c52_h_sf_marshallIn);
        }

        _SFD_SET_DATA_COMPILED_PROPS(6,SF_UINT8,0,NULL,0,0,0,0.0,1.0,0,0,
          (MexFcnForType)c52_d_sf_marshallOut,(MexInFcnForType)NULL);

        {
          unsigned int dimVector[1];
          dimVector[0]= 3;
          _SFD_SET_DATA_COMPILED_PROPS(7,SF_STRUCT,1,&(dimVector[0]),0,0,0,0.0,
            1.0,0,0,(MexFcnForType)c52_l_sf_marshallOut,(MexInFcnForType)
            c52_i_sf_marshallIn);
        }

        {
          unsigned int dimVector[1];
          dimVector[0]= 3;
          _SFD_SET_DATA_COMPILED_PROPS(8,SF_STRUCT,1,&(dimVector[0]),0,0,0,0.0,
            1.0,0,0,(MexFcnForType)c52_l_sf_marshallOut,(MexInFcnForType)
            c52_i_sf_marshallIn);
        }

        _SFD_SET_DATA_COMPILED_PROPS(9,SF_STRUCT,0,NULL,0,0,0,0.0,1.0,0,0,
          (MexFcnForType)c52_j_sf_marshallOut,(MexInFcnForType)
          c52_g_sf_marshallIn);
        _SFD_SET_DATA_COMPILED_PROPS(10,SF_STRUCT,0,NULL,0,0,0,0.0,1.0,0,0,
          (MexFcnForType)c52_j_sf_marshallOut,(MexInFcnForType)
          c52_g_sf_marshallIn);

        {
          unsigned int dimVector[1];
          dimVector[0]= 4294967295;
          _SFD_SET_DATA_COMPILED_PROPS(11,SF_INT8,1,&(dimVector[0]),0,0,0,0.0,
            1.0,0,0,(MexFcnForType)NULL,(MexInFcnForType)NULL);
        }

        {
          unsigned int dimVector[1];
          dimVector[0]= 4294967295;
          _SFD_SET_DATA_COMPILED_PROPS(12,SF_DOUBLE,1,&(dimVector[0]),0,0,0,0.0,
            1.0,0,0,(MexFcnForType)NULL,(MexInFcnForType)NULL);
        }

        {
          unsigned int dimVector[1];
          dimVector[0]= 4294967295;
          _SFD_SET_DATA_COMPILED_PROPS(13,SF_DOUBLE,1,&(dimVector[0]),0,0,0,0.0,
            1.0,0,0,(MexFcnForType)NULL,(MexInFcnForType)NULL);
        }

        _SFD_SET_DATA_VALUE_PTR(11,(void *)(NULL));
        _SFD_SET_DATA_VALUE_PTR(12,(void *)(NULL));
        _SFD_SET_DATA_VALUE_PTR(13,(void *)(NULL));
        _SFD_SET_DATA_VALUE_PTR(0U, chartInstance->c52_me);
        _SFD_SET_DATA_VALUE_PTR(1U, *chartInstance->c52_players);
        _SFD_SET_DATA_VALUE_PTR(2U, chartInstance->c52_ball);
        _SFD_SET_DATA_VALUE_PTR(3U, chartInstance->c52_finalWay);
        _SFD_SET_DATA_VALUE_PTR(4U, chartInstance->c52_manualWay);
        _SFD_SET_DATA_VALUE_PTR(5U, chartInstance->c52_startingPos);
        _SFD_SET_DATA_VALUE_PTR(6U, chartInstance->c52_GameOn);
        _SFD_SET_DATA_VALUE_PTR(7U, chartInstance->c52_enemy);
        _SFD_SET_DATA_VALUE_PTR(8U, chartInstance->c52_friends);
        _SFD_SET_DATA_VALUE_PTR(9U, &chartInstance->c52_shootWay);
        _SFD_SET_DATA_VALUE_PTR(10U, &chartInstance->c52_kickWay);
      }
    } else {
      sf_debug_reset_current_state_configuration(sfGlobalDebugInstanceStruct,
        _LessonIIMachineNumber_,chartInstance->chartNumber,
        chartInstance->instanceNumber);
    }
  }
}

static const char* sf_get_instance_specialization(void)
{
  return "gKRazsXtkVuGLxRXB6vTwG";
}

static void sf_opaque_initialize_c52_LessonII(void *chartInstanceVar)
{
  chart_debug_initialization(((SFc52_LessonIIInstanceStruct*) chartInstanceVar
    )->S,0);
  initialize_params_c52_LessonII((SFc52_LessonIIInstanceStruct*)
    chartInstanceVar);
  initialize_c52_LessonII((SFc52_LessonIIInstanceStruct*) chartInstanceVar);
}

static void sf_opaque_enable_c52_LessonII(void *chartInstanceVar)
{
  enable_c52_LessonII((SFc52_LessonIIInstanceStruct*) chartInstanceVar);
}

static void sf_opaque_disable_c52_LessonII(void *chartInstanceVar)
{
  disable_c52_LessonII((SFc52_LessonIIInstanceStruct*) chartInstanceVar);
}

static void sf_opaque_gateway_c52_LessonII(void *chartInstanceVar)
{
  sf_gateway_c52_LessonII((SFc52_LessonIIInstanceStruct*) chartInstanceVar);
}

static const mxArray* sf_opaque_get_sim_state_c52_LessonII(SimStruct* S)
{
  ChartRunTimeInfo * crtInfo = (ChartRunTimeInfo *)(ssGetUserData(S));
  ChartInfoStruct * chartInfo = (ChartInfoStruct *)(crtInfo->instanceInfo);
  return get_sim_state_c52_LessonII((SFc52_LessonIIInstanceStruct*)
    chartInfo->chartInstance);         /* raw sim ctx */
}

static void sf_opaque_set_sim_state_c52_LessonII(SimStruct* S, const mxArray *st)
{
  ChartRunTimeInfo * crtInfo = (ChartRunTimeInfo *)(ssGetUserData(S));
  ChartInfoStruct * chartInfo = (ChartInfoStruct *)(crtInfo->instanceInfo);
  set_sim_state_c52_LessonII((SFc52_LessonIIInstanceStruct*)
    chartInfo->chartInstance, st);
}

static void sf_opaque_terminate_c52_LessonII(void *chartInstanceVar)
{
  if (chartInstanceVar!=NULL) {
    SimStruct *S = ((SFc52_LessonIIInstanceStruct*) chartInstanceVar)->S;
    ChartRunTimeInfo * crtInfo = (ChartRunTimeInfo *)(ssGetUserData(S));
    if (sim_mode_is_rtw_gen(S) || sim_mode_is_external(S)) {
      sf_clear_rtw_identifier(S);
      unload_LessonII_optimization_info();
    }

    finalize_c52_LessonII((SFc52_LessonIIInstanceStruct*) chartInstanceVar);
    utFree(chartInstanceVar);
    if (crtInfo != NULL) {
      utFree(crtInfo);
    }

    ssSetUserData(S,NULL);
  }
}

static void sf_opaque_init_subchart_simstructs(void *chartInstanceVar)
{
  initSimStructsc52_LessonII((SFc52_LessonIIInstanceStruct*) chartInstanceVar);
}

extern unsigned int sf_machine_global_initializer_called(void);
static void mdlProcessParameters_c52_LessonII(SimStruct *S)
{
  int i;
  for (i=0;i<ssGetNumRunTimeParams(S);i++) {
    if (ssGetSFcnParamTunable(S,i)) {
      ssUpdateDlgParamAsRunTimeParam(S,i);
    }
  }

  if (sf_machine_global_initializer_called()) {
    ChartRunTimeInfo * crtInfo = (ChartRunTimeInfo *)(ssGetUserData(S));
    ChartInfoStruct * chartInfo = (ChartInfoStruct *)(crtInfo->instanceInfo);
    initialize_params_c52_LessonII((SFc52_LessonIIInstanceStruct*)
      (chartInfo->chartInstance));
  }
}

static void mdlSetWorkWidths_c52_LessonII(SimStruct *S)
{
  if (sim_mode_is_rtw_gen(S) || sim_mode_is_external(S)) {
    mxArray *infoStruct = load_LessonII_optimization_info();
    int_T chartIsInlinable =
      (int_T)sf_is_chart_inlinable(sf_get_instance_specialization(),infoStruct,
      52);
    ssSetStateflowIsInlinable(S,chartIsInlinable);
    ssSetRTWCG(S,sf_rtw_info_uint_prop(sf_get_instance_specialization(),
                infoStruct,52,"RTWCG"));
    ssSetEnableFcnIsTrivial(S,1);
    ssSetDisableFcnIsTrivial(S,1);
    ssSetNotMultipleInlinable(S,sf_rtw_info_uint_prop
      (sf_get_instance_specialization(),infoStruct,52,
       "gatewayCannotBeInlinedMultipleTimes"));
    sf_update_buildInfo(sf_get_instance_specialization(),infoStruct,52);
    if (chartIsInlinable) {
      ssSetInputPortOptimOpts(S, 0, SS_REUSABLE_AND_LOCAL);
      ssSetInputPortOptimOpts(S, 1, SS_REUSABLE_AND_LOCAL);
      ssSetInputPortOptimOpts(S, 2, SS_REUSABLE_AND_LOCAL);
      ssSetInputPortOptimOpts(S, 3, SS_REUSABLE_AND_LOCAL);
      ssSetInputPortOptimOpts(S, 4, SS_REUSABLE_AND_LOCAL);
      sf_mark_chart_expressionable_inputs(S,sf_get_instance_specialization(),
        infoStruct,52,5);
      sf_mark_chart_reusable_outputs(S,sf_get_instance_specialization(),
        infoStruct,52,1);
    }

    {
      unsigned int outPortIdx;
      for (outPortIdx=1; outPortIdx<=1; ++outPortIdx) {
        ssSetOutputPortOptimizeInIR(S, outPortIdx, 1U);
      }
    }

    {
      unsigned int inPortIdx;
      for (inPortIdx=0; inPortIdx < 5; ++inPortIdx) {
        ssSetInputPortOptimizeInIR(S, inPortIdx, 1U);
      }
    }

    sf_set_rtw_dwork_info(S,sf_get_instance_specialization(),infoStruct,52);
    ssSetHasSubFunctions(S,!(chartIsInlinable));
  } else {
  }

  ssSetOptions(S,ssGetOptions(S)|SS_OPTION_WORKS_WITH_CODE_REUSE);
  ssSetChecksum0(S,(3514302883U));
  ssSetChecksum1(S,(421320559U));
  ssSetChecksum2(S,(4109649298U));
  ssSetChecksum3(S,(3551280953U));
  ssSetmdlDerivatives(S, NULL);
  ssSetExplicitFCSSCtrl(S,1);
  ssSupportsMultipleExecInstances(S,1);
}

static void mdlRTW_c52_LessonII(SimStruct *S)
{
  if (sim_mode_is_rtw_gen(S)) {
    ssWriteRTWStrParam(S, "StateflowChartType", "Stateflow");
  }
}

static void mdlStart_c52_LessonII(SimStruct *S)
{
  SFc52_LessonIIInstanceStruct *chartInstance;
  ChartRunTimeInfo * crtInfo = (ChartRunTimeInfo *)utMalloc(sizeof
    (ChartRunTimeInfo));
  chartInstance = (SFc52_LessonIIInstanceStruct *)utMalloc(sizeof
    (SFc52_LessonIIInstanceStruct));
  memset(chartInstance, 0, sizeof(SFc52_LessonIIInstanceStruct));
  if (chartInstance==NULL) {
    sf_mex_error_message("Could not allocate memory for chart instance.");
  }

  chartInstance->chartInfo.chartInstance = chartInstance;
  chartInstance->chartInfo.isEMLChart = 0;
  chartInstance->chartInfo.chartInitialized = 0;
  chartInstance->chartInfo.sFunctionGateway = sf_opaque_gateway_c52_LessonII;
  chartInstance->chartInfo.initializeChart = sf_opaque_initialize_c52_LessonII;
  chartInstance->chartInfo.terminateChart = sf_opaque_terminate_c52_LessonII;
  chartInstance->chartInfo.enableChart = sf_opaque_enable_c52_LessonII;
  chartInstance->chartInfo.disableChart = sf_opaque_disable_c52_LessonII;
  chartInstance->chartInfo.getSimState = sf_opaque_get_sim_state_c52_LessonII;
  chartInstance->chartInfo.setSimState = sf_opaque_set_sim_state_c52_LessonII;
  chartInstance->chartInfo.getSimStateInfo = sf_get_sim_state_info_c52_LessonII;
  chartInstance->chartInfo.zeroCrossings = NULL;
  chartInstance->chartInfo.outputs = NULL;
  chartInstance->chartInfo.derivatives = NULL;
  chartInstance->chartInfo.mdlRTW = mdlRTW_c52_LessonII;
  chartInstance->chartInfo.mdlStart = mdlStart_c52_LessonII;
  chartInstance->chartInfo.mdlSetWorkWidths = mdlSetWorkWidths_c52_LessonII;
  chartInstance->chartInfo.extModeExec = NULL;
  chartInstance->chartInfo.restoreLastMajorStepConfiguration = NULL;
  chartInstance->chartInfo.restoreBeforeLastMajorStepConfiguration = NULL;
  chartInstance->chartInfo.storeCurrentConfiguration = NULL;
  chartInstance->chartInfo.callAtomicSubchartUserFcn = NULL;
  chartInstance->chartInfo.callAtomicSubchartAutoFcn = NULL;
  chartInstance->chartInfo.debugInstance = sfGlobalDebugInstanceStruct;
  chartInstance->S = S;
  crtInfo->checksum = SF_RUNTIME_INFO_CHECKSUM;
  crtInfo->instanceInfo = (&(chartInstance->chartInfo));
  crtInfo->isJITEnabled = false;
  crtInfo->compiledInfo = NULL;
  ssSetUserData(S,(void *)(crtInfo));  /* register the chart instance with simstruct */
  init_dsm_address_info(chartInstance);
  init_simulink_io_address(chartInstance);
  if (!sim_mode_is_rtw_gen(S)) {
  }

  sf_opaque_init_subchart_simstructs(chartInstance->chartInfo.chartInstance);
  chart_debug_initialization(S,1);
}

void c52_LessonII_method_dispatcher(SimStruct *S, int_T method, void *data)
{
  switch (method) {
   case SS_CALL_MDL_START:
    mdlStart_c52_LessonII(S);
    break;

   case SS_CALL_MDL_SET_WORK_WIDTHS:
    mdlSetWorkWidths_c52_LessonII(S);
    break;

   case SS_CALL_MDL_PROCESS_PARAMETERS:
    mdlProcessParameters_c52_LessonII(S);
    break;

   default:
    /* Unhandled method */
    sf_mex_error_message("Stateflow Internal Error:\n"
                         "Error calling c52_LessonII_method_dispatcher.\n"
                         "Can't handle method %d.\n", method);
    break;
  }
}

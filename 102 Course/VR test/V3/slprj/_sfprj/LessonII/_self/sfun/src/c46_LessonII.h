#ifndef __c46_LessonII_h__
#define __c46_LessonII_h__

/* Include files */
#include "sf_runtime/sfc_sf.h"
#include "sf_runtime/sfc_mex.h"
#include "rtwtypes.h"
#include "multiword_types.h"

/* Type Definitions */
#ifndef typedef_SFc46_LessonIIInstanceStruct
#define typedef_SFc46_LessonIIInstanceStruct

typedef struct {
  SimStruct *S;
  ChartInfoStruct chartInfo;
  uint32_T chartNumber;
  uint32_T instanceNumber;
  int32_T c46_sfEvent;
  boolean_T c46_isStable;
  boolean_T c46_doneDoubleBufferReInit;
  uint8_T c46_is_active_c46_LessonII;
  uint8_T (*c46_messageBuffer_data)[64];
  int32_T (*c46_messageBuffer_sizes)[2];
  uint8_T (*c46_message)[31];
} SFc46_LessonIIInstanceStruct;

#endif                                 /*typedef_SFc46_LessonIIInstanceStruct*/

/* Named Constants */

/* Variable Declarations */
extern struct SfDebugInstanceStruct *sfGlobalDebugInstanceStruct;

/* Variable Definitions */

/* Function Declarations */
extern const mxArray *sf_c46_LessonII_get_eml_resolved_functions_info(void);

/* Function Definitions */
extern void sf_c46_LessonII_get_check_sum(mxArray *plhs[]);
extern void c46_LessonII_method_dispatcher(SimStruct *S, int_T method, void
  *data);

#endif

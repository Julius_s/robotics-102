#ifndef __c49_LessonII_h__
#define __c49_LessonII_h__

/* Include files */
#include "sf_runtime/sfc_sf.h"
#include "sf_runtime/sfc_mex.h"
#include "rtwtypes.h"
#include "multiword_types.h"

/* Type Definitions */
#ifndef struct_sMB5GaXc40ozYzvPFeCX3Q
#define struct_sMB5GaXc40ozYzvPFeCX3Q

struct sMB5GaXc40ozYzvPFeCX3Q
{
  int32_T dummy;
};

#endif                                 /*struct_sMB5GaXc40ozYzvPFeCX3Q*/

#ifndef typedef_c49_coder_internal_cell
#define typedef_c49_coder_internal_cell

typedef struct sMB5GaXc40ozYzvPFeCX3Q c49_coder_internal_cell;

#endif                                 /*typedef_c49_coder_internal_cell*/

#ifndef typedef_SFc49_LessonIIInstanceStruct
#define typedef_SFc49_LessonIIInstanceStruct

typedef struct {
  SimStruct *S;
  ChartInfoStruct chartInfo;
  uint32_T chartNumber;
  uint32_T instanceNumber;
  int32_T c49_sfEvent;
  boolean_T c49_isStable;
  boolean_T c49_doneDoubleBufferReInit;
  uint8_T c49_is_active_c49_LessonII;
  int16_T *c49_dO;
  real_T *c49_limitFwd;
  uint8_T *c49_certainty;
} SFc49_LessonIIInstanceStruct;

#endif                                 /*typedef_SFc49_LessonIIInstanceStruct*/

/* Named Constants */

/* Variable Declarations */
extern struct SfDebugInstanceStruct *sfGlobalDebugInstanceStruct;

/* Variable Definitions */

/* Function Declarations */
extern const mxArray *sf_c49_LessonII_get_eml_resolved_functions_info(void);

/* Function Definitions */
extern void sf_c49_LessonII_get_check_sum(mxArray *plhs[]);
extern void c49_LessonII_method_dispatcher(SimStruct *S, int_T method, void
  *data);

#endif

#ifndef __c2_LessonII_h__
#define __c2_LessonII_h__

/* Include files */
#include "sf_runtime/sfc_sf.h"
#include "sf_runtime/sfc_mex.h"
#include "rtwtypes.h"
#include "multiword_types.h"

/* Type Definitions */
#ifndef typedef_SFc2_LessonIIInstanceStruct
#define typedef_SFc2_LessonIIInstanceStruct

typedef struct {
  SimStruct *S;
  ChartInfoStruct chartInfo;
  uint32_T chartNumber;
  uint32_T instanceNumber;
  int32_T c2_sfEvent;
  boolean_T c2_isStable;
  boolean_T c2_doneDoubleBufferReInit;
  uint8_T c2_is_active_c2_LessonII;
  uint8_T c2_Image[230400];
  uint8_T c2_R[76800];
  uint8_T c2_G[76800];
  uint8_T c2_B[76800];
  uint8_T c2_hR[76800];
  uint8_T c2_hG[76800];
  uint8_T c2_hB[76800];
  uint8_T c2_RI[76800];
  uint8_T c2_GI[76800];
  uint8_T c2_inData[230400];
  uint8_T c2_u[230400];
  uint8_T c2_uv0[76800];
  uint8_T c2_uv1[76800];
  uint8_T c2_b_u[76800];
  uint8_T (*c2_b_Image)[230400];
  uint8_T (*c2_b_RI)[76800];
  uint8_T (*c2_b_GI)[76800];
  uint8_T (*c2_BI)[76800];
} SFc2_LessonIIInstanceStruct;

#endif                                 /*typedef_SFc2_LessonIIInstanceStruct*/

/* Named Constants */

/* Variable Declarations */
extern struct SfDebugInstanceStruct *sfGlobalDebugInstanceStruct;

/* Variable Definitions */

/* Function Declarations */
extern const mxArray *sf_c2_LessonII_get_eml_resolved_functions_info(void);

/* Function Definitions */
extern void sf_c2_LessonII_get_check_sum(mxArray *plhs[]);
extern void c2_LessonII_method_dispatcher(SimStruct *S, int_T method, void *data);

#endif

#ifndef __c51_LessonII_h__
#define __c51_LessonII_h__

/* Include files */
#include "sf_runtime/sfc_sf.h"
#include "sf_runtime/sfc_mex.h"
#include "rtwtypes.h"
#include "multiword_types.h"

/* Type Definitions */
#ifndef struct_Waypoint_tag
#define struct_Waypoint_tag

struct Waypoint_tag
{
  int8_T x;
  int8_T y;
  int16_T orientation;
};

#endif                                 /*struct_Waypoint_tag*/

#ifndef typedef_c51_Waypoint
#define typedef_c51_Waypoint

typedef struct Waypoint_tag c51_Waypoint;

#endif                                 /*typedef_c51_Waypoint*/

#ifndef typedef_SFc51_LessonIIInstanceStruct
#define typedef_SFc51_LessonIIInstanceStruct

typedef struct {
  SimStruct *S;
  ChartInfoStruct chartInfo;
  uint32_T chartNumber;
  uint32_T instanceNumber;
  int32_T c51_sfEvent;
  boolean_T c51_isStable;
  boolean_T c51_doneDoubleBufferReInit;
  uint8_T c51_is_active_c51_LessonII;
  real32_T (*c51_data)[3];
  c51_Waypoint *c51_way;
} SFc51_LessonIIInstanceStruct;

#endif                                 /*typedef_SFc51_LessonIIInstanceStruct*/

/* Named Constants */

/* Variable Declarations */
extern struct SfDebugInstanceStruct *sfGlobalDebugInstanceStruct;

/* Variable Definitions */

/* Function Declarations */
extern const mxArray *sf_c51_LessonII_get_eml_resolved_functions_info(void);

/* Function Definitions */
extern void sf_c51_LessonII_get_check_sum(mxArray *plhs[]);
extern void c51_LessonII_method_dispatcher(SimStruct *S, int_T method, void
  *data);

#endif

#ifndef __c12_LessonII_h__
#define __c12_LessonII_h__

/* Include files */
#include "sf_runtime/sfc_sf.h"
#include "sf_runtime/sfc_mex.h"
#include "rtwtypes.h"
#include "multiword_types.h"

/* Type Definitions */
#ifndef typedef_SFc12_LessonIIInstanceStruct
#define typedef_SFc12_LessonIIInstanceStruct

typedef struct {
  SimStruct *S;
  ChartInfoStruct chartInfo;
  uint32_T chartNumber;
  uint32_T instanceNumber;
  int32_T c12_sfEvent;
  boolean_T c12_isStable;
  boolean_T c12_doneDoubleBufferReInit;
  uint8_T c12_is_active_c12_LessonII;
  real32_T c12_position[2];
  boolean_T c12_position_not_empty;
  real32_T c12_vel[2];
  boolean_T c12_vel_not_empty;
  real32_T (*c12_transposes)[18];
  real32_T (*c12_Ctranspose)[3];
  real_T *c12_disipation;
  real_T *c12_timestep;
  real_T *c12_kickStrength;
  uint8_T *c12_gameOn;
} SFc12_LessonIIInstanceStruct;

#endif                                 /*typedef_SFc12_LessonIIInstanceStruct*/

/* Named Constants */

/* Variable Declarations */
extern struct SfDebugInstanceStruct *sfGlobalDebugInstanceStruct;

/* Variable Definitions */

/* Function Declarations */
extern const mxArray *sf_c12_LessonII_get_eml_resolved_functions_info(void);

/* Function Definitions */
extern void sf_c12_LessonII_get_check_sum(mxArray *plhs[]);
extern void c12_LessonII_method_dispatcher(SimStruct *S, int_T method, void
  *data);

#endif

#ifndef __c21_LessonIII_start_h__
#define __c21_LessonIII_start_h__

/* Include files */
#include "sf_runtime/sfc_sf.h"
#include "sf_runtime/sfc_mex.h"
#include "rtwtypes.h"
#include "multiword_types.h"

/* Type Definitions */
#ifndef typedef_SFc21_LessonIII_startInstanceStruct
#define typedef_SFc21_LessonIII_startInstanceStruct

typedef struct {
  SimStruct *S;
  ChartInfoStruct chartInfo;
  uint32_T chartNumber;
  uint32_T instanceNumber;
  int32_T c21_sfEvent;
  boolean_T c21_isStable;
  boolean_T c21_doneDoubleBufferReInit;
  uint8_T c21_is_active_c21_LessonIII_start;
  int8_T (*c21_targetPos)[2];
  int16_T *c21_targetOrientation;
  int8_T (*c21_myPos)[2];
} SFc21_LessonIII_startInstanceStruct;

#endif                                 /*typedef_SFc21_LessonIII_startInstanceStruct*/

/* Named Constants */

/* Variable Declarations */
extern struct SfDebugInstanceStruct *sfGlobalDebugInstanceStruct;

/* Variable Definitions */

/* Function Declarations */
extern const mxArray *sf_c21_LessonIII_start_get_eml_resolved_functions_info
  (void);

/* Function Definitions */
extern void sf_c21_LessonIII_start_get_check_sum(mxArray *plhs[]);
extern void c21_LessonIII_start_method_dispatcher(SimStruct *S, int_T method,
  void *data);

#endif

#ifndef __c13_LessonIII_start_h__
#define __c13_LessonIII_start_h__

/* Include files */
#include "sf_runtime/sfc_sf.h"
#include "sf_runtime/sfc_mex.h"
#include "rtwtypes.h"
#include "multiword_types.h"

/* Type Definitions */
#ifndef struct_Waypoint_tag
#define struct_Waypoint_tag

struct Waypoint_tag
{
  int8_T x;
  int8_T y;
  int16_T orientation;
};

#endif                                 /*struct_Waypoint_tag*/

#ifndef typedef_c13_Waypoint
#define typedef_c13_Waypoint

typedef struct Waypoint_tag c13_Waypoint;

#endif                                 /*typedef_c13_Waypoint*/

#ifndef typedef_SFc13_LessonIII_startInstanceStruct
#define typedef_SFc13_LessonIII_startInstanceStruct

typedef struct {
  SimStruct *S;
  ChartInfoStruct chartInfo;
  uint32_T chartNumber;
  uint32_T instanceNumber;
  int32_T c13_sfEvent;
  boolean_T c13_isStable;
  boolean_T c13_doneDoubleBufferReInit;
  uint8_T c13_is_active_c13_LessonIII_start;
  real32_T (*c13_data)[3];
  c13_Waypoint *c13_way;
} SFc13_LessonIII_startInstanceStruct;

#endif                                 /*typedef_SFc13_LessonIII_startInstanceStruct*/

/* Named Constants */

/* Variable Declarations */
extern struct SfDebugInstanceStruct *sfGlobalDebugInstanceStruct;

/* Variable Definitions */

/* Function Declarations */
extern const mxArray *sf_c13_LessonIII_start_get_eml_resolved_functions_info
  (void);

/* Function Definitions */
extern void sf_c13_LessonIII_start_get_check_sum(mxArray *plhs[]);
extern void c13_LessonIII_start_method_dispatcher(SimStruct *S, int_T method,
  void *data);

#endif

/* Include files */

#include <stddef.h>
#include "blas.h"
#include "LessonIII_start_sfun.h"
#include "c23_LessonIII_start.h"
#include <string.h>
#define CHARTINSTANCE_CHARTNUMBER      (chartInstance->chartNumber)
#define CHARTINSTANCE_INSTANCENUMBER   (chartInstance->instanceNumber)
#include "LessonIII_start_sfun_debug_macros.h"
#define _SF_MEX_LISTEN_FOR_CTRL_C(S)   sf_mex_listen_for_ctrl_c(sfGlobalDebugInstanceStruct,S);

/* Type Definitions */

/* Named Constants */
#define CALL_EVENT                     (-1)

/* Variable Declarations */

/* Variable Definitions */
static real_T _sfTime_;
static const char * c23_debug_family_names[10] = { "tempEmpty", "colors",
  "positions", "ii", "message", "nargin", "nargout", "Ball", "players", "gameOn"
};

/* Function Declarations */
static void initialize_c23_LessonIII_start(SFc23_LessonIII_startInstanceStruct
  *chartInstance);
static void initialize_params_c23_LessonIII_start
  (SFc23_LessonIII_startInstanceStruct *chartInstance);
static void enable_c23_LessonIII_start(SFc23_LessonIII_startInstanceStruct
  *chartInstance);
static void disable_c23_LessonIII_start(SFc23_LessonIII_startInstanceStruct
  *chartInstance);
static void c23_update_debugger_state_c23_LessonIII_start
  (SFc23_LessonIII_startInstanceStruct *chartInstance);
static const mxArray *get_sim_state_c23_LessonIII_start
  (SFc23_LessonIII_startInstanceStruct *chartInstance);
static void set_sim_state_c23_LessonIII_start
  (SFc23_LessonIII_startInstanceStruct *chartInstance, const mxArray *c23_st);
static void finalize_c23_LessonIII_start(SFc23_LessonIII_startInstanceStruct
  *chartInstance);
static void sf_gateway_c23_LessonIII_start(SFc23_LessonIII_startInstanceStruct
  *chartInstance);
static void mdl_start_c23_LessonIII_start(SFc23_LessonIII_startInstanceStruct
  *chartInstance);
static void initSimStructsc23_LessonIII_start
  (SFc23_LessonIII_startInstanceStruct *chartInstance);
static void init_script_number_translation(uint32_T c23_machineNumber, uint32_T
  c23_chartNumber, uint32_T c23_instanceNumber);
static const mxArray *c23_sf_marshallOut(void *chartInstanceVoid, void
  *c23_inData);
static uint8_T c23_emlrt_marshallIn(SFc23_LessonIII_startInstanceStruct
  *chartInstance, const mxArray *c23_b_gameOn, const char_T *c23_identifier);
static uint8_T c23_b_emlrt_marshallIn(SFc23_LessonIII_startInstanceStruct
  *chartInstance, const mxArray *c23_u, const emlrtMsgIdentifier *c23_parentId);
static void c23_sf_marshallIn(void *chartInstanceVoid, const mxArray
  *c23_mxArrayInData, const char_T *c23_varName, void *c23_outData);
static const mxArray *c23_b_sf_marshallOut(void *chartInstanceVoid, void
  *c23_inData);
static void c23_c_emlrt_marshallIn(SFc23_LessonIII_startInstanceStruct
  *chartInstance, const mxArray *c23_b_players, const char_T *c23_identifier,
  c23_Player c23_y[6]);
static void c23_d_emlrt_marshallIn(SFc23_LessonIII_startInstanceStruct
  *chartInstance, const mxArray *c23_u, const emlrtMsgIdentifier *c23_parentId,
  c23_Player c23_y[6]);
static int8_T c23_e_emlrt_marshallIn(SFc23_LessonIII_startInstanceStruct
  *chartInstance, const mxArray *c23_u, const emlrtMsgIdentifier *c23_parentId);
static int16_T c23_f_emlrt_marshallIn(SFc23_LessonIII_startInstanceStruct
  *chartInstance, const mxArray *c23_u, const emlrtMsgIdentifier *c23_parentId);
static void c23_b_sf_marshallIn(void *chartInstanceVoid, const mxArray
  *c23_mxArrayInData, const char_T *c23_varName, void *c23_outData);
static const mxArray *c23_c_sf_marshallOut(void *chartInstanceVoid, void
  *c23_inData);
static c23_Ball c23_g_emlrt_marshallIn(SFc23_LessonIII_startInstanceStruct
  *chartInstance, const mxArray *c23_c_Ball, const char_T *c23_identifier);
static c23_Ball c23_h_emlrt_marshallIn(SFc23_LessonIII_startInstanceStruct
  *chartInstance, const mxArray *c23_u, const emlrtMsgIdentifier *c23_parentId);
static void c23_c_sf_marshallIn(void *chartInstanceVoid, const mxArray
  *c23_mxArrayInData, const char_T *c23_varName, void *c23_outData);
static const mxArray *c23_d_sf_marshallOut(void *chartInstanceVoid, void
  *c23_inData);
static const mxArray *c23_e_sf_marshallOut(void *chartInstanceVoid, void
  *c23_inData);
static real_T c23_i_emlrt_marshallIn(SFc23_LessonIII_startInstanceStruct
  *chartInstance, const mxArray *c23_u, const emlrtMsgIdentifier *c23_parentId);
static void c23_d_sf_marshallIn(void *chartInstanceVoid, const mxArray
  *c23_mxArrayInData, const char_T *c23_varName, void *c23_outData);
static const mxArray *c23_f_sf_marshallOut(void *chartInstanceVoid, void
  *c23_inData);
static void c23_j_emlrt_marshallIn(SFc23_LessonIII_startInstanceStruct
  *chartInstance, const mxArray *c23_u, const emlrtMsgIdentifier *c23_parentId,
  uint8_T c23_y[24]);
static void c23_e_sf_marshallIn(void *chartInstanceVoid, const mxArray
  *c23_mxArrayInData, const char_T *c23_varName, void *c23_outData);
static const mxArray *c23_g_sf_marshallOut(void *chartInstanceVoid, void
  *c23_inData);
static void c23_k_emlrt_marshallIn(SFc23_LessonIII_startInstanceStruct
  *chartInstance, const mxArray *c23_u, const emlrtMsgIdentifier *c23_parentId,
  uint8_T c23_y[26]);
static void c23_f_sf_marshallIn(void *chartInstanceVoid, const mxArray
  *c23_mxArrayInData, const char_T *c23_varName, void *c23_outData);
static const mxArray *c23_h_sf_marshallOut(void *chartInstanceVoid, void
  *c23_inData);
static void c23_l_emlrt_marshallIn(SFc23_LessonIII_startInstanceStruct
  *chartInstance, const mxArray *c23_u, const emlrtMsgIdentifier *c23_parentId,
  uint8_T c23_y[27]);
static void c23_g_sf_marshallIn(void *chartInstanceVoid, const mxArray
  *c23_mxArrayInData, const char_T *c23_varName, void *c23_outData);
static const mxArray *c23_i_sf_marshallOut(void *chartInstanceVoid, void
  *c23_inData);
static void c23_m_emlrt_marshallIn(SFc23_LessonIII_startInstanceStruct
  *chartInstance, const mxArray *c23_u, const emlrtMsgIdentifier *c23_parentId,
  uint8_T c23_y[6]);
static void c23_h_sf_marshallIn(void *chartInstanceVoid, const mxArray
  *c23_mxArrayInData, const char_T *c23_varName, void *c23_outData);
static const mxArray *c23_j_sf_marshallOut(void *chartInstanceVoid, void
  *c23_inData);
static c23_Player c23_n_emlrt_marshallIn(SFc23_LessonIII_startInstanceStruct
  *chartInstance, const mxArray *c23_u, const emlrtMsgIdentifier *c23_parentId);
static void c23_i_sf_marshallIn(void *chartInstanceVoid, const mxArray
  *c23_mxArrayInData, const char_T *c23_varName, void *c23_outData);
static void c23_info_helper(const mxArray **c23_info);
static const mxArray *c23_emlrt_marshallOut(const char * c23_u);
static const mxArray *c23_b_emlrt_marshallOut(const uint32_T c23_u);
static int8_T c23_typecast(SFc23_LessonIII_startInstanceStruct *chartInstance,
  uint8_T c23_x);
static const mxArray *c23_k_sf_marshallOut(void *chartInstanceVoid, void
  *c23_inData);
static int32_T c23_o_emlrt_marshallIn(SFc23_LessonIII_startInstanceStruct
  *chartInstance, const mxArray *c23_u, const emlrtMsgIdentifier *c23_parentId);
static void c23_j_sf_marshallIn(void *chartInstanceVoid, const mxArray
  *c23_mxArrayInData, const char_T *c23_varName, void *c23_outData);
static const mxArray *c23_Ball_bus_io(void *chartInstanceVoid, void *c23_pData);
static const mxArray *c23_players_bus_io(void *chartInstanceVoid, void
  *c23_pData);
static void init_dsm_address_info(SFc23_LessonIII_startInstanceStruct
  *chartInstance);
static void init_simulink_io_address(SFc23_LessonIII_startInstanceStruct
  *chartInstance);

/* Function Definitions */
static void initialize_c23_LessonIII_start(SFc23_LessonIII_startInstanceStruct
  *chartInstance)
{
  chartInstance->c23_sfEvent = CALL_EVENT;
  _sfTime_ = sf_get_time(chartInstance->S);
  chartInstance->c23_is_active_c23_LessonIII_start = 0U;
}

static void initialize_params_c23_LessonIII_start
  (SFc23_LessonIII_startInstanceStruct *chartInstance)
{
  (void)chartInstance;
}

static void enable_c23_LessonIII_start(SFc23_LessonIII_startInstanceStruct
  *chartInstance)
{
  _sfTime_ = sf_get_time(chartInstance->S);
}

static void disable_c23_LessonIII_start(SFc23_LessonIII_startInstanceStruct
  *chartInstance)
{
  _sfTime_ = sf_get_time(chartInstance->S);
}

static void c23_update_debugger_state_c23_LessonIII_start
  (SFc23_LessonIII_startInstanceStruct *chartInstance)
{
  (void)chartInstance;
}

static const mxArray *get_sim_state_c23_LessonIII_start
  (SFc23_LessonIII_startInstanceStruct *chartInstance)
{
  const mxArray *c23_st;
  const mxArray *c23_y = NULL;
  const mxArray *c23_b_y = NULL;
  int8_T c23_u;
  const mxArray *c23_c_y = NULL;
  int8_T c23_b_u;
  const mxArray *c23_d_y = NULL;
  uint8_T c23_c_u;
  const mxArray *c23_e_y = NULL;
  uint8_T c23_hoistedGlobal;
  uint8_T c23_d_u;
  const mxArray *c23_f_y = NULL;
  int32_T c23_i0;
  c23_Player c23_e_u[6];
  const mxArray *c23_g_y = NULL;
  static int32_T c23_iv0[1] = { 6 };

  int32_T c23_iv1[1];
  int32_T c23_i1;
  const c23_Player *c23_r0;
  int8_T c23_f_u;
  const mxArray *c23_h_y = NULL;
  int8_T c23_g_u;
  const mxArray *c23_i_y = NULL;
  int16_T c23_h_u;
  const mxArray *c23_j_y = NULL;
  uint8_T c23_i_u;
  const mxArray *c23_k_y = NULL;
  uint8_T c23_j_u;
  const mxArray *c23_l_y = NULL;
  uint8_T c23_k_u;
  const mxArray *c23_m_y = NULL;
  uint8_T c23_b_hoistedGlobal;
  uint8_T c23_l_u;
  const mxArray *c23_n_y = NULL;
  c23_st = NULL;
  c23_st = NULL;
  c23_y = NULL;
  sf_mex_assign(&c23_y, sf_mex_createcellmatrix(4, 1), false);
  c23_b_y = NULL;
  sf_mex_assign(&c23_b_y, sf_mex_createstruct("structure", 2, 1, 1), false);
  c23_u = *(int8_T *)&((char_T *)chartInstance->c23_b_Ball)[0];
  c23_c_y = NULL;
  sf_mex_assign(&c23_c_y, sf_mex_create("y", &c23_u, 2, 0U, 0U, 0U, 0), false);
  sf_mex_addfield(c23_b_y, c23_c_y, "x", "x", 0);
  c23_b_u = *(int8_T *)&((char_T *)chartInstance->c23_b_Ball)[1];
  c23_d_y = NULL;
  sf_mex_assign(&c23_d_y, sf_mex_create("y", &c23_b_u, 2, 0U, 0U, 0U, 0), false);
  sf_mex_addfield(c23_b_y, c23_d_y, "y", "y", 0);
  c23_c_u = *(uint8_T *)&((char_T *)chartInstance->c23_b_Ball)[2];
  c23_e_y = NULL;
  sf_mex_assign(&c23_e_y, sf_mex_create("y", &c23_c_u, 3, 0U, 0U, 0U, 0), false);
  sf_mex_addfield(c23_b_y, c23_e_y, "valid", "valid", 0);
  sf_mex_setcell(c23_y, 0, c23_b_y);
  c23_hoistedGlobal = *chartInstance->c23_gameOn;
  c23_d_u = c23_hoistedGlobal;
  c23_f_y = NULL;
  sf_mex_assign(&c23_f_y, sf_mex_create("y", &c23_d_u, 3, 0U, 0U, 0U, 0), false);
  sf_mex_setcell(c23_y, 1, c23_f_y);
  for (c23_i0 = 0; c23_i0 < 6; c23_i0++) {
    c23_e_u[c23_i0].x = *(int8_T *)&((char_T *)(c23_Player *)&((char_T *)
      chartInstance->c23_players)[8 * c23_i0])[0];
    c23_e_u[c23_i0].y = *(int8_T *)&((char_T *)(c23_Player *)&((char_T *)
      chartInstance->c23_players)[8 * c23_i0])[1];
    c23_e_u[c23_i0].orientation = *(int16_T *)&((char_T *)(c23_Player *)
      &((char_T *)chartInstance->c23_players)[8 * c23_i0])[2];
    c23_e_u[c23_i0].color = *(uint8_T *)&((char_T *)(c23_Player *)&((char_T *)
      chartInstance->c23_players)[8 * c23_i0])[4];
    c23_e_u[c23_i0].position = *(uint8_T *)&((char_T *)(c23_Player *)&((char_T *)
      chartInstance->c23_players)[8 * c23_i0])[5];
    c23_e_u[c23_i0].valid = *(uint8_T *)&((char_T *)(c23_Player *)&((char_T *)
      chartInstance->c23_players)[8 * c23_i0])[6];
  }

  c23_g_y = NULL;
  c23_iv1[0] = c23_iv0[0];
  sf_mex_assign(&c23_g_y, sf_mex_createstructarray("structure", 1, c23_iv1),
                false);
  for (c23_i1 = 0; c23_i1 < 6; c23_i1++) {
    c23_r0 = &c23_e_u[c23_i1];
    c23_f_u = c23_r0->x;
    c23_h_y = NULL;
    sf_mex_assign(&c23_h_y, sf_mex_create("y", &c23_f_u, 2, 0U, 0U, 0U, 0),
                  false);
    sf_mex_addfield(c23_g_y, c23_h_y, "x", "x", c23_i1);
    c23_g_u = c23_r0->y;
    c23_i_y = NULL;
    sf_mex_assign(&c23_i_y, sf_mex_create("y", &c23_g_u, 2, 0U, 0U, 0U, 0),
                  false);
    sf_mex_addfield(c23_g_y, c23_i_y, "y", "y", c23_i1);
    c23_h_u = c23_r0->orientation;
    c23_j_y = NULL;
    sf_mex_assign(&c23_j_y, sf_mex_create("y", &c23_h_u, 4, 0U, 0U, 0U, 0),
                  false);
    sf_mex_addfield(c23_g_y, c23_j_y, "orientation", "orientation", c23_i1);
    c23_i_u = c23_r0->color;
    c23_k_y = NULL;
    sf_mex_assign(&c23_k_y, sf_mex_create("y", &c23_i_u, 3, 0U, 0U, 0U, 0),
                  false);
    sf_mex_addfield(c23_g_y, c23_k_y, "color", "color", c23_i1);
    c23_j_u = c23_r0->position;
    c23_l_y = NULL;
    sf_mex_assign(&c23_l_y, sf_mex_create("y", &c23_j_u, 3, 0U, 0U, 0U, 0),
                  false);
    sf_mex_addfield(c23_g_y, c23_l_y, "position", "position", c23_i1);
    c23_k_u = c23_r0->valid;
    c23_m_y = NULL;
    sf_mex_assign(&c23_m_y, sf_mex_create("y", &c23_k_u, 3, 0U, 0U, 0U, 0),
                  false);
    sf_mex_addfield(c23_g_y, c23_m_y, "valid", "valid", c23_i1);
  }

  sf_mex_setcell(c23_y, 2, c23_g_y);
  c23_b_hoistedGlobal = chartInstance->c23_is_active_c23_LessonIII_start;
  c23_l_u = c23_b_hoistedGlobal;
  c23_n_y = NULL;
  sf_mex_assign(&c23_n_y, sf_mex_create("y", &c23_l_u, 3, 0U, 0U, 0U, 0), false);
  sf_mex_setcell(c23_y, 3, c23_n_y);
  sf_mex_assign(&c23_st, c23_y, false);
  return c23_st;
}

static void set_sim_state_c23_LessonIII_start
  (SFc23_LessonIII_startInstanceStruct *chartInstance, const mxArray *c23_st)
{
  const mxArray *c23_u;
  c23_Ball c23_r1;
  c23_Player c23_rv0[6];
  int32_T c23_i2;
  chartInstance->c23_doneDoubleBufferReInit = true;
  c23_u = sf_mex_dup(c23_st);
  c23_r1 = c23_g_emlrt_marshallIn(chartInstance, sf_mex_dup(sf_mex_getcell(c23_u,
    0)), "Ball");
  *(int8_T *)&((char_T *)chartInstance->c23_b_Ball)[0] = c23_r1.x;
  *(int8_T *)&((char_T *)chartInstance->c23_b_Ball)[1] = c23_r1.y;
  *(uint8_T *)&((char_T *)chartInstance->c23_b_Ball)[2] = c23_r1.valid;
  *chartInstance->c23_gameOn = c23_emlrt_marshallIn(chartInstance, sf_mex_dup
    (sf_mex_getcell(c23_u, 1)), "gameOn");
  c23_c_emlrt_marshallIn(chartInstance, sf_mex_dup(sf_mex_getcell(c23_u, 2)),
    "players", c23_rv0);
  for (c23_i2 = 0; c23_i2 < 6; c23_i2++) {
    *(int8_T *)&((char_T *)(c23_Player *)&((char_T *)chartInstance->c23_players)
                 [8 * c23_i2])[0] = c23_rv0[c23_i2].x;
    *(int8_T *)&((char_T *)(c23_Player *)&((char_T *)chartInstance->c23_players)
                 [8 * c23_i2])[1] = c23_rv0[c23_i2].y;
    *(int16_T *)&((char_T *)(c23_Player *)&((char_T *)chartInstance->c23_players)
                  [8 * c23_i2])[2] = c23_rv0[c23_i2].orientation;
    *(uint8_T *)&((char_T *)(c23_Player *)&((char_T *)chartInstance->c23_players)
                  [8 * c23_i2])[4] = c23_rv0[c23_i2].color;
    *(uint8_T *)&((char_T *)(c23_Player *)&((char_T *)chartInstance->c23_players)
                  [8 * c23_i2])[5] = c23_rv0[c23_i2].position;
    *(uint8_T *)&((char_T *)(c23_Player *)&((char_T *)chartInstance->c23_players)
                  [8 * c23_i2])[6] = c23_rv0[c23_i2].valid;
  }

  chartInstance->c23_is_active_c23_LessonIII_start = c23_emlrt_marshallIn
    (chartInstance, sf_mex_dup(sf_mex_getcell(c23_u, 3)),
     "is_active_c23_LessonIII_start");
  sf_mex_destroy(&c23_u);
  c23_update_debugger_state_c23_LessonIII_start(chartInstance);
  sf_mex_destroy(&c23_st);
}

static void finalize_c23_LessonIII_start(SFc23_LessonIII_startInstanceStruct
  *chartInstance)
{
  (void)chartInstance;
}

static void sf_gateway_c23_LessonIII_start(SFc23_LessonIII_startInstanceStruct
  *chartInstance)
{
  int32_T c23_i3;
  int32_T c23_i4;
  uint8_T c23_b_message[31];
  uint32_T c23_debug_family_var_map[10];
  c23_Player c23_tempEmpty;
  uint8_T c23_colors[6];
  uint8_T c23_positions[6];
  real_T c23_ii;
  uint8_T c23_c_message[27];
  uint8_T c23_d_message[26];
  uint8_T c23_e_message[24];
  real_T c23_nargin = 1.0;
  real_T c23_nargout = 3.0;
  c23_Ball c23_c_Ball;
  c23_Player c23_b_players[6];
  uint8_T c23_b_gameOn;
  c23_Player c23_b_tempEmpty[6];
  int32_T c23_i5;
  int32_T c23_i6;
  int32_T c23_i7;
  int32_T c23_i8;
  int32_T c23_i9;
  static uint8_T c23_uv0[6] = { 103U, 103U, 103U, 98U, 98U, 98U };

  int32_T c23_i10;
  static uint8_T c23_uv1[6] = { 111U, 100U, 103U, 111U, 100U, 103U };

  int32_T c23_b_ii;
  real_T c23_c_ii;
  int32_T c23_i11;
  uint8_T c23_x[2];
  int16_T c23_y;
  int32_T c23_i12;
  _SFD_SYMBOL_SCOPE_PUSH(0U, 0U);
  _sfTime_ = sf_get_time(chartInstance->S);
  _SFD_CC_CALL(CHART_ENTER_SFUNCTION_TAG, 17U, chartInstance->c23_sfEvent);
  for (c23_i3 = 0; c23_i3 < 31; c23_i3++) {
    _SFD_DATA_RANGE_CHECK((real_T)(*chartInstance->c23_message)[c23_i3], 0U);
  }

  chartInstance->c23_sfEvent = CALL_EVENT;
  _SFD_CC_CALL(CHART_ENTER_DURING_FUNCTION_TAG, 17U, chartInstance->c23_sfEvent);
  for (c23_i4 = 0; c23_i4 < 31; c23_i4++) {
    c23_b_message[c23_i4] = (*chartInstance->c23_message)[c23_i4];
  }

  _SFD_SYMBOL_SCOPE_PUSH_EML(0U, 10U, 13U, c23_debug_family_names,
    c23_debug_family_var_map);
  _SFD_SYMBOL_SCOPE_ADD_EML_IMPORTABLE(&c23_tempEmpty, 0U, c23_j_sf_marshallOut,
    c23_i_sf_marshallIn);
  _SFD_SYMBOL_SCOPE_ADD_EML_IMPORTABLE(c23_colors, 1U, c23_i_sf_marshallOut,
    c23_h_sf_marshallIn);
  _SFD_SYMBOL_SCOPE_ADD_EML_IMPORTABLE(c23_positions, 2U, c23_i_sf_marshallOut,
    c23_h_sf_marshallIn);
  _SFD_SYMBOL_SCOPE_ADD_EML_IMPORTABLE(&c23_ii, 3U, c23_e_sf_marshallOut,
    c23_d_sf_marshallIn);
  _SFD_SYMBOL_SCOPE_ADD_EML_IMPORTABLE(c23_c_message, MAX_uint32_T,
    c23_h_sf_marshallOut, c23_g_sf_marshallIn);
  _SFD_SYMBOL_SCOPE_ADD_EML_IMPORTABLE(c23_d_message, MAX_uint32_T,
    c23_g_sf_marshallOut, c23_f_sf_marshallIn);
  _SFD_SYMBOL_SCOPE_ADD_EML_IMPORTABLE(c23_e_message, MAX_uint32_T,
    c23_f_sf_marshallOut, c23_e_sf_marshallIn);
  _SFD_SYMBOL_SCOPE_ADD_EML_IMPORTABLE(&c23_nargin, 5U, c23_e_sf_marshallOut,
    c23_d_sf_marshallIn);
  _SFD_SYMBOL_SCOPE_ADD_EML_IMPORTABLE(&c23_nargout, 6U, c23_e_sf_marshallOut,
    c23_d_sf_marshallIn);
  _SFD_SYMBOL_SCOPE_ADD_EML(c23_b_message, 4U, c23_d_sf_marshallOut);
  _SFD_SYMBOL_SCOPE_ADD_EML_IMPORTABLE(&c23_c_Ball, 7U, c23_c_sf_marshallOut,
    c23_c_sf_marshallIn);
  _SFD_SYMBOL_SCOPE_ADD_EML_IMPORTABLE(c23_b_players, 8U, c23_b_sf_marshallOut,
    c23_b_sf_marshallIn);
  _SFD_SYMBOL_SCOPE_ADD_EML_IMPORTABLE(&c23_b_gameOn, 9U, c23_sf_marshallOut,
    c23_sf_marshallIn);
  CV_EML_FCN(0, 0);
  _SFD_EML_CALL(0U, chartInstance->c23_sfEvent, 3);
  c23_tempEmpty.x = 0;
  _SFD_EML_CALL(0U, chartInstance->c23_sfEvent, 4);
  c23_tempEmpty.y = 0;
  _SFD_EML_CALL(0U, chartInstance->c23_sfEvent, 5);
  c23_tempEmpty.orientation = 0;
  _SFD_EML_CALL(0U, chartInstance->c23_sfEvent, 6);
  c23_tempEmpty.color = 0U;
  _SFD_EML_CALL(0U, chartInstance->c23_sfEvent, 7);
  c23_tempEmpty.position = 0U;
  _SFD_EML_CALL(0U, chartInstance->c23_sfEvent, 8);
  c23_tempEmpty.valid = 0U;
  _SFD_EML_CALL(0U, chartInstance->c23_sfEvent, 9);
  c23_b_tempEmpty[0] = c23_tempEmpty;
  c23_b_tempEmpty[1] = c23_tempEmpty;
  c23_b_tempEmpty[2] = c23_tempEmpty;
  c23_b_tempEmpty[3] = c23_tempEmpty;
  c23_b_tempEmpty[4] = c23_tempEmpty;
  c23_b_tempEmpty[5] = c23_tempEmpty;
  for (c23_i5 = 0; c23_i5 < 6; c23_i5++) {
    c23_b_players[c23_i5] = c23_b_tempEmpty[c23_i5];
  }

  _SFD_EML_CALL(0U, chartInstance->c23_sfEvent, 11);
  for (c23_i6 = 0; c23_i6 < 27; c23_i6++) {
    c23_c_message[c23_i6] = c23_b_message[c23_i6 + 4];
  }

  _SFD_SYMBOL_SWITCH(4U, 4U);
  _SFD_EML_CALL(0U, chartInstance->c23_sfEvent, 12);
  c23_b_gameOn = c23_c_message[0];
  _SFD_EML_CALL(0U, chartInstance->c23_sfEvent, 13);
  for (c23_i7 = 0; c23_i7 < 26; c23_i7++) {
    c23_d_message[c23_i7] = c23_c_message[c23_i7 + 1];
  }

  _SFD_SYMBOL_SWITCH(4U, 5U);
  _SFD_EML_CALL(0U, chartInstance->c23_sfEvent, 14);
  c23_c_Ball.x = c23_typecast(chartInstance, c23_d_message[0]);
  _SFD_EML_CALL(0U, chartInstance->c23_sfEvent, 15);
  c23_c_Ball.y = c23_typecast(chartInstance, c23_d_message[1]);
  _SFD_EML_CALL(0U, chartInstance->c23_sfEvent, 16);
  c23_c_Ball.valid = 1U;
  _SFD_EML_CALL(0U, chartInstance->c23_sfEvent, 17);
  for (c23_i8 = 0; c23_i8 < 24; c23_i8++) {
    c23_e_message[c23_i8] = c23_d_message[c23_i8 + 2];
  }

  _SFD_SYMBOL_SWITCH(4U, 6U);
  _SFD_EML_CALL(0U, chartInstance->c23_sfEvent, 18);
  for (c23_i9 = 0; c23_i9 < 6; c23_i9++) {
    c23_colors[c23_i9] = c23_uv0[c23_i9];
  }

  _SFD_EML_CALL(0U, chartInstance->c23_sfEvent, 19);
  for (c23_i10 = 0; c23_i10 < 6; c23_i10++) {
    c23_positions[c23_i10] = c23_uv1[c23_i10];
  }

  _SFD_EML_CALL(0U, chartInstance->c23_sfEvent, 20);
  c23_ii = 1.0;
  c23_b_ii = 0;
  while (c23_b_ii < 6) {
    c23_ii = 1.0 + (real_T)c23_b_ii;
    CV_EML_FOR(0, 1, 0, 1);
    _SFD_EML_CALL(0U, chartInstance->c23_sfEvent, 21);
    c23_b_players[_SFD_EML_ARRAY_BOUNDS_CHECK("players", (int32_T)
      _SFD_INTEGER_CHECK("ii", c23_ii), 1, 6, 1, 0) - 1].x = c23_typecast
      (chartInstance, c23_e_message[_SFD_EML_ARRAY_BOUNDS_CHECK("message",
        (int32_T)_SFD_INTEGER_CHECK("(ii-1)*4+1", (c23_ii - 1.0) * 4.0 + 1.0), 1,
        24, 1, 0) - 1]);
    _SFD_EML_CALL(0U, chartInstance->c23_sfEvent, 22);
    c23_b_players[_SFD_EML_ARRAY_BOUNDS_CHECK("players", (int32_T)
      _SFD_INTEGER_CHECK("ii", c23_ii), 1, 6, 1, 0) - 1].y = c23_typecast
      (chartInstance, c23_e_message[_SFD_EML_ARRAY_BOUNDS_CHECK("message",
        (int32_T)_SFD_INTEGER_CHECK("(ii-1)*4+2", (c23_ii - 1.0) * 4.0 + 2.0), 1,
        24, 1, 0) - 1]);
    _SFD_EML_CALL(0U, chartInstance->c23_sfEvent, 23);
    c23_c_ii = (c23_ii - 1.0) * 4.0;
    for (c23_i11 = 0; c23_i11 < 2; c23_i11++) {
      c23_x[c23_i11] = c23_e_message[_SFD_EML_ARRAY_BOUNDS_CHECK("message",
        (int32_T)_SFD_INTEGER_CHECK("(ii-1)*4+3:(ii-1)*4+4", c23_c_ii + (3.0 +
        (real_T)c23_i11)), 1, 24, 1, 0) - 1];
    }

    memcpy(&c23_y, &c23_x[0], (size_t)1 * sizeof(int16_T));
    c23_b_players[_SFD_EML_ARRAY_BOUNDS_CHECK("players", (int32_T)
      _SFD_INTEGER_CHECK("ii", c23_ii), 1, 6, 1, 0) - 1].orientation = c23_y;
    _SFD_EML_CALL(0U, chartInstance->c23_sfEvent, 24);
    c23_b_players[_SFD_EML_ARRAY_BOUNDS_CHECK("players", (int32_T)
      _SFD_INTEGER_CHECK("ii", c23_ii), 1, 6, 1, 0) - 1].color =
      c23_colors[_SFD_EML_ARRAY_BOUNDS_CHECK("colors", (int32_T)
      _SFD_INTEGER_CHECK("ii", c23_ii), 1, 6, 1, 0) - 1];
    _SFD_EML_CALL(0U, chartInstance->c23_sfEvent, 25);
    c23_b_players[_SFD_EML_ARRAY_BOUNDS_CHECK("players", (int32_T)
      _SFD_INTEGER_CHECK("ii", c23_ii), 1, 6, 1, 0) - 1].position =
      c23_positions[_SFD_EML_ARRAY_BOUNDS_CHECK("positions", (int32_T)
      _SFD_INTEGER_CHECK("ii", c23_ii), 1, 6, 1, 0) - 1];
    _SFD_EML_CALL(0U, chartInstance->c23_sfEvent, 26);
    c23_b_players[_SFD_EML_ARRAY_BOUNDS_CHECK("players", (int32_T)
      _SFD_INTEGER_CHECK("ii", c23_ii), 1, 6, 1, 0) - 1].valid = 1U;
    c23_b_ii++;
    _SF_MEX_LISTEN_FOR_CTRL_C(chartInstance->S);
  }

  CV_EML_FOR(0, 1, 0, 0);
  _SFD_EML_CALL(0U, chartInstance->c23_sfEvent, -26);
  _SFD_SYMBOL_SCOPE_POP();
  *(int8_T *)&((char_T *)chartInstance->c23_b_Ball)[0] = c23_c_Ball.x;
  *(int8_T *)&((char_T *)chartInstance->c23_b_Ball)[1] = c23_c_Ball.y;
  *(uint8_T *)&((char_T *)chartInstance->c23_b_Ball)[2] = c23_c_Ball.valid;
  for (c23_i12 = 0; c23_i12 < 6; c23_i12++) {
    *(int8_T *)&((char_T *)(c23_Player *)&((char_T *)chartInstance->c23_players)
                 [8 * c23_i12])[0] = c23_b_players[c23_i12].x;
    *(int8_T *)&((char_T *)(c23_Player *)&((char_T *)chartInstance->c23_players)
                 [8 * c23_i12])[1] = c23_b_players[c23_i12].y;
    *(int16_T *)&((char_T *)(c23_Player *)&((char_T *)chartInstance->c23_players)
                  [8 * c23_i12])[2] = c23_b_players[c23_i12].orientation;
    *(uint8_T *)&((char_T *)(c23_Player *)&((char_T *)chartInstance->c23_players)
                  [8 * c23_i12])[4] = c23_b_players[c23_i12].color;
    *(uint8_T *)&((char_T *)(c23_Player *)&((char_T *)chartInstance->c23_players)
                  [8 * c23_i12])[5] = c23_b_players[c23_i12].position;
    *(uint8_T *)&((char_T *)(c23_Player *)&((char_T *)chartInstance->c23_players)
                  [8 * c23_i12])[6] = c23_b_players[c23_i12].valid;
  }

  *chartInstance->c23_gameOn = c23_b_gameOn;
  _SFD_CC_CALL(EXIT_OUT_OF_FUNCTION_TAG, 17U, chartInstance->c23_sfEvent);
  _SFD_SYMBOL_SCOPE_POP();
  _SFD_CHECK_FOR_STATE_INCONSISTENCY(_LessonIII_startMachineNumber_,
    chartInstance->chartNumber, chartInstance->instanceNumber);
  _SFD_DATA_RANGE_CHECK((real_T)*chartInstance->c23_gameOn, 3U);
}

static void mdl_start_c23_LessonIII_start(SFc23_LessonIII_startInstanceStruct
  *chartInstance)
{
  (void)chartInstance;
}

static void initSimStructsc23_LessonIII_start
  (SFc23_LessonIII_startInstanceStruct *chartInstance)
{
  (void)chartInstance;
}

static void init_script_number_translation(uint32_T c23_machineNumber, uint32_T
  c23_chartNumber, uint32_T c23_instanceNumber)
{
  (void)c23_machineNumber;
  (void)c23_chartNumber;
  (void)c23_instanceNumber;
}

static const mxArray *c23_sf_marshallOut(void *chartInstanceVoid, void
  *c23_inData)
{
  const mxArray *c23_mxArrayOutData = NULL;
  uint8_T c23_u;
  const mxArray *c23_y = NULL;
  SFc23_LessonIII_startInstanceStruct *chartInstance;
  chartInstance = (SFc23_LessonIII_startInstanceStruct *)chartInstanceVoid;
  c23_mxArrayOutData = NULL;
  c23_u = *(uint8_T *)c23_inData;
  c23_y = NULL;
  sf_mex_assign(&c23_y, sf_mex_create("y", &c23_u, 3, 0U, 0U, 0U, 0), false);
  sf_mex_assign(&c23_mxArrayOutData, c23_y, false);
  return c23_mxArrayOutData;
}

static uint8_T c23_emlrt_marshallIn(SFc23_LessonIII_startInstanceStruct
  *chartInstance, const mxArray *c23_b_gameOn, const char_T *c23_identifier)
{
  uint8_T c23_y;
  emlrtMsgIdentifier c23_thisId;
  c23_thisId.fIdentifier = c23_identifier;
  c23_thisId.fParent = NULL;
  c23_y = c23_b_emlrt_marshallIn(chartInstance, sf_mex_dup(c23_b_gameOn),
    &c23_thisId);
  sf_mex_destroy(&c23_b_gameOn);
  return c23_y;
}

static uint8_T c23_b_emlrt_marshallIn(SFc23_LessonIII_startInstanceStruct
  *chartInstance, const mxArray *c23_u, const emlrtMsgIdentifier *c23_parentId)
{
  uint8_T c23_y;
  uint8_T c23_u0;
  (void)chartInstance;
  sf_mex_import(c23_parentId, sf_mex_dup(c23_u), &c23_u0, 1, 3, 0U, 0, 0U, 0);
  c23_y = c23_u0;
  sf_mex_destroy(&c23_u);
  return c23_y;
}

static void c23_sf_marshallIn(void *chartInstanceVoid, const mxArray
  *c23_mxArrayInData, const char_T *c23_varName, void *c23_outData)
{
  const mxArray *c23_b_gameOn;
  const char_T *c23_identifier;
  emlrtMsgIdentifier c23_thisId;
  uint8_T c23_y;
  SFc23_LessonIII_startInstanceStruct *chartInstance;
  chartInstance = (SFc23_LessonIII_startInstanceStruct *)chartInstanceVoid;
  c23_b_gameOn = sf_mex_dup(c23_mxArrayInData);
  c23_identifier = c23_varName;
  c23_thisId.fIdentifier = c23_identifier;
  c23_thisId.fParent = NULL;
  c23_y = c23_b_emlrt_marshallIn(chartInstance, sf_mex_dup(c23_b_gameOn),
    &c23_thisId);
  sf_mex_destroy(&c23_b_gameOn);
  *(uint8_T *)c23_outData = c23_y;
  sf_mex_destroy(&c23_mxArrayInData);
}

static const mxArray *c23_b_sf_marshallOut(void *chartInstanceVoid, void
  *c23_inData)
{
  const mxArray *c23_mxArrayOutData;
  int32_T c23_i13;
  c23_Player c23_b_inData[6];
  int32_T c23_i14;
  c23_Player c23_u[6];
  const mxArray *c23_y = NULL;
  static int32_T c23_iv2[1] = { 6 };

  int32_T c23_iv3[1];
  int32_T c23_i15;
  const c23_Player *c23_r2;
  int8_T c23_b_u;
  const mxArray *c23_b_y = NULL;
  int8_T c23_c_u;
  const mxArray *c23_c_y = NULL;
  int16_T c23_d_u;
  const mxArray *c23_d_y = NULL;
  uint8_T c23_e_u;
  const mxArray *c23_e_y = NULL;
  uint8_T c23_f_u;
  const mxArray *c23_f_y = NULL;
  uint8_T c23_g_u;
  const mxArray *c23_g_y = NULL;
  SFc23_LessonIII_startInstanceStruct *chartInstance;
  chartInstance = (SFc23_LessonIII_startInstanceStruct *)chartInstanceVoid;
  c23_mxArrayOutData = NULL;
  c23_mxArrayOutData = NULL;
  for (c23_i13 = 0; c23_i13 < 6; c23_i13++) {
    c23_b_inData[c23_i13] = (*(c23_Player (*)[6])c23_inData)[c23_i13];
  }

  for (c23_i14 = 0; c23_i14 < 6; c23_i14++) {
    c23_u[c23_i14] = c23_b_inData[c23_i14];
  }

  c23_y = NULL;
  c23_iv3[0] = c23_iv2[0];
  sf_mex_assign(&c23_y, sf_mex_createstructarray("structure", 1, c23_iv3), false);
  for (c23_i15 = 0; c23_i15 < 6; c23_i15++) {
    c23_r2 = &c23_u[c23_i15];
    c23_b_u = c23_r2->x;
    c23_b_y = NULL;
    sf_mex_assign(&c23_b_y, sf_mex_create("y", &c23_b_u, 2, 0U, 0U, 0U, 0),
                  false);
    sf_mex_addfield(c23_y, c23_b_y, "x", "x", c23_i15);
    c23_c_u = c23_r2->y;
    c23_c_y = NULL;
    sf_mex_assign(&c23_c_y, sf_mex_create("y", &c23_c_u, 2, 0U, 0U, 0U, 0),
                  false);
    sf_mex_addfield(c23_y, c23_c_y, "y", "y", c23_i15);
    c23_d_u = c23_r2->orientation;
    c23_d_y = NULL;
    sf_mex_assign(&c23_d_y, sf_mex_create("y", &c23_d_u, 4, 0U, 0U, 0U, 0),
                  false);
    sf_mex_addfield(c23_y, c23_d_y, "orientation", "orientation", c23_i15);
    c23_e_u = c23_r2->color;
    c23_e_y = NULL;
    sf_mex_assign(&c23_e_y, sf_mex_create("y", &c23_e_u, 3, 0U, 0U, 0U, 0),
                  false);
    sf_mex_addfield(c23_y, c23_e_y, "color", "color", c23_i15);
    c23_f_u = c23_r2->position;
    c23_f_y = NULL;
    sf_mex_assign(&c23_f_y, sf_mex_create("y", &c23_f_u, 3, 0U, 0U, 0U, 0),
                  false);
    sf_mex_addfield(c23_y, c23_f_y, "position", "position", c23_i15);
    c23_g_u = c23_r2->valid;
    c23_g_y = NULL;
    sf_mex_assign(&c23_g_y, sf_mex_create("y", &c23_g_u, 3, 0U, 0U, 0U, 0),
                  false);
    sf_mex_addfield(c23_y, c23_g_y, "valid", "valid", c23_i15);
  }

  sf_mex_assign(&c23_mxArrayOutData, c23_y, false);
  return c23_mxArrayOutData;
}

static void c23_c_emlrt_marshallIn(SFc23_LessonIII_startInstanceStruct
  *chartInstance, const mxArray *c23_b_players, const char_T *c23_identifier,
  c23_Player c23_y[6])
{
  emlrtMsgIdentifier c23_thisId;
  c23_thisId.fIdentifier = c23_identifier;
  c23_thisId.fParent = NULL;
  c23_d_emlrt_marshallIn(chartInstance, sf_mex_dup(c23_b_players), &c23_thisId,
    c23_y);
  sf_mex_destroy(&c23_b_players);
}

static void c23_d_emlrt_marshallIn(SFc23_LessonIII_startInstanceStruct
  *chartInstance, const mxArray *c23_u, const emlrtMsgIdentifier *c23_parentId,
  c23_Player c23_y[6])
{
  static uint32_T c23_uv2[1] = { 6U };

  uint32_T c23_uv3[1];
  emlrtMsgIdentifier c23_thisId;
  static const char * c23_fieldNames[6] = { "x", "y", "orientation", "color",
    "position", "valid" };

  c23_Player (*c23_r3)[6];
  int32_T c23_i16;
  c23_uv3[0] = c23_uv2[0];
  c23_thisId.fParent = c23_parentId;
  sf_mex_check_struct(c23_parentId, c23_u, 6, c23_fieldNames, 1U, c23_uv3);
  c23_r3 = (c23_Player (*)[6])c23_y;
  for (c23_i16 = 0; c23_i16 < 6; c23_i16++) {
    c23_thisId.fIdentifier = "x";
    (*c23_r3)[c23_i16].x = c23_e_emlrt_marshallIn(chartInstance, sf_mex_dup
      (sf_mex_getfield(c23_u, "x", "x", c23_i16)), &c23_thisId);
    c23_thisId.fIdentifier = "y";
    (*c23_r3)[c23_i16].y = c23_e_emlrt_marshallIn(chartInstance, sf_mex_dup
      (sf_mex_getfield(c23_u, "y", "y", c23_i16)), &c23_thisId);
    c23_thisId.fIdentifier = "orientation";
    (*c23_r3)[c23_i16].orientation = c23_f_emlrt_marshallIn(chartInstance,
      sf_mex_dup(sf_mex_getfield(c23_u, "orientation", "orientation", c23_i16)),
      &c23_thisId);
    c23_thisId.fIdentifier = "color";
    (*c23_r3)[c23_i16].color = c23_b_emlrt_marshallIn(chartInstance, sf_mex_dup
      (sf_mex_getfield(c23_u, "color", "color", c23_i16)), &c23_thisId);
    c23_thisId.fIdentifier = "position";
    (*c23_r3)[c23_i16].position = c23_b_emlrt_marshallIn(chartInstance,
      sf_mex_dup(sf_mex_getfield(c23_u, "position", "position", c23_i16)),
      &c23_thisId);
    c23_thisId.fIdentifier = "valid";
    (*c23_r3)[c23_i16].valid = c23_b_emlrt_marshallIn(chartInstance, sf_mex_dup
      (sf_mex_getfield(c23_u, "valid", "valid", c23_i16)), &c23_thisId);
  }

  sf_mex_destroy(&c23_u);
}

static int8_T c23_e_emlrt_marshallIn(SFc23_LessonIII_startInstanceStruct
  *chartInstance, const mxArray *c23_u, const emlrtMsgIdentifier *c23_parentId)
{
  int8_T c23_y;
  int8_T c23_i17;
  (void)chartInstance;
  sf_mex_import(c23_parentId, sf_mex_dup(c23_u), &c23_i17, 1, 2, 0U, 0, 0U, 0);
  c23_y = c23_i17;
  sf_mex_destroy(&c23_u);
  return c23_y;
}

static int16_T c23_f_emlrt_marshallIn(SFc23_LessonIII_startInstanceStruct
  *chartInstance, const mxArray *c23_u, const emlrtMsgIdentifier *c23_parentId)
{
  int16_T c23_y;
  int16_T c23_i18;
  (void)chartInstance;
  sf_mex_import(c23_parentId, sf_mex_dup(c23_u), &c23_i18, 1, 4, 0U, 0, 0U, 0);
  c23_y = c23_i18;
  sf_mex_destroy(&c23_u);
  return c23_y;
}

static void c23_b_sf_marshallIn(void *chartInstanceVoid, const mxArray
  *c23_mxArrayInData, const char_T *c23_varName, void *c23_outData)
{
  const mxArray *c23_b_players;
  const char_T *c23_identifier;
  emlrtMsgIdentifier c23_thisId;
  c23_Player c23_y[6];
  int32_T c23_i19;
  SFc23_LessonIII_startInstanceStruct *chartInstance;
  chartInstance = (SFc23_LessonIII_startInstanceStruct *)chartInstanceVoid;
  c23_b_players = sf_mex_dup(c23_mxArrayInData);
  c23_identifier = c23_varName;
  c23_thisId.fIdentifier = c23_identifier;
  c23_thisId.fParent = NULL;
  c23_d_emlrt_marshallIn(chartInstance, sf_mex_dup(c23_b_players), &c23_thisId,
    c23_y);
  sf_mex_destroy(&c23_b_players);
  for (c23_i19 = 0; c23_i19 < 6; c23_i19++) {
    (*(c23_Player (*)[6])c23_outData)[c23_i19] = c23_y[c23_i19];
  }

  sf_mex_destroy(&c23_mxArrayInData);
}

static const mxArray *c23_c_sf_marshallOut(void *chartInstanceVoid, void
  *c23_inData)
{
  const mxArray *c23_mxArrayOutData = NULL;
  c23_Ball c23_u;
  const mxArray *c23_y = NULL;
  int8_T c23_b_u;
  const mxArray *c23_b_y = NULL;
  int8_T c23_c_u;
  const mxArray *c23_c_y = NULL;
  uint8_T c23_d_u;
  const mxArray *c23_d_y = NULL;
  SFc23_LessonIII_startInstanceStruct *chartInstance;
  chartInstance = (SFc23_LessonIII_startInstanceStruct *)chartInstanceVoid;
  c23_mxArrayOutData = NULL;
  c23_u = *(c23_Ball *)c23_inData;
  c23_y = NULL;
  sf_mex_assign(&c23_y, sf_mex_createstruct("structure", 2, 1, 1), false);
  c23_b_u = c23_u.x;
  c23_b_y = NULL;
  sf_mex_assign(&c23_b_y, sf_mex_create("y", &c23_b_u, 2, 0U, 0U, 0U, 0), false);
  sf_mex_addfield(c23_y, c23_b_y, "x", "x", 0);
  c23_c_u = c23_u.y;
  c23_c_y = NULL;
  sf_mex_assign(&c23_c_y, sf_mex_create("y", &c23_c_u, 2, 0U, 0U, 0U, 0), false);
  sf_mex_addfield(c23_y, c23_c_y, "y", "y", 0);
  c23_d_u = c23_u.valid;
  c23_d_y = NULL;
  sf_mex_assign(&c23_d_y, sf_mex_create("y", &c23_d_u, 3, 0U, 0U, 0U, 0), false);
  sf_mex_addfield(c23_y, c23_d_y, "valid", "valid", 0);
  sf_mex_assign(&c23_mxArrayOutData, c23_y, false);
  return c23_mxArrayOutData;
}

static c23_Ball c23_g_emlrt_marshallIn(SFc23_LessonIII_startInstanceStruct
  *chartInstance, const mxArray *c23_c_Ball, const char_T *c23_identifier)
{
  c23_Ball c23_y;
  emlrtMsgIdentifier c23_thisId;
  c23_thisId.fIdentifier = c23_identifier;
  c23_thisId.fParent = NULL;
  c23_y = c23_h_emlrt_marshallIn(chartInstance, sf_mex_dup(c23_c_Ball),
    &c23_thisId);
  sf_mex_destroy(&c23_c_Ball);
  return c23_y;
}

static c23_Ball c23_h_emlrt_marshallIn(SFc23_LessonIII_startInstanceStruct
  *chartInstance, const mxArray *c23_u, const emlrtMsgIdentifier *c23_parentId)
{
  c23_Ball c23_y;
  emlrtMsgIdentifier c23_thisId;
  static const char * c23_fieldNames[3] = { "x", "y", "valid" };

  c23_thisId.fParent = c23_parentId;
  sf_mex_check_struct(c23_parentId, c23_u, 3, c23_fieldNames, 0U, NULL);
  c23_thisId.fIdentifier = "x";
  c23_y.x = c23_e_emlrt_marshallIn(chartInstance, sf_mex_dup(sf_mex_getfield
    (c23_u, "x", "x", 0)), &c23_thisId);
  c23_thisId.fIdentifier = "y";
  c23_y.y = c23_e_emlrt_marshallIn(chartInstance, sf_mex_dup(sf_mex_getfield
    (c23_u, "y", "y", 0)), &c23_thisId);
  c23_thisId.fIdentifier = "valid";
  c23_y.valid = c23_b_emlrt_marshallIn(chartInstance, sf_mex_dup(sf_mex_getfield
                                        (c23_u, "valid", "valid", 0)),
    &c23_thisId);
  sf_mex_destroy(&c23_u);
  return c23_y;
}

static void c23_c_sf_marshallIn(void *chartInstanceVoid, const mxArray
  *c23_mxArrayInData, const char_T *c23_varName, void *c23_outData)
{
  const mxArray *c23_c_Ball;
  const char_T *c23_identifier;
  emlrtMsgIdentifier c23_thisId;
  c23_Ball c23_y;
  SFc23_LessonIII_startInstanceStruct *chartInstance;
  chartInstance = (SFc23_LessonIII_startInstanceStruct *)chartInstanceVoid;
  c23_c_Ball = sf_mex_dup(c23_mxArrayInData);
  c23_identifier = c23_varName;
  c23_thisId.fIdentifier = c23_identifier;
  c23_thisId.fParent = NULL;
  c23_y = c23_h_emlrt_marshallIn(chartInstance, sf_mex_dup(c23_c_Ball),
    &c23_thisId);
  sf_mex_destroy(&c23_c_Ball);
  *(c23_Ball *)c23_outData = c23_y;
  sf_mex_destroy(&c23_mxArrayInData);
}

static const mxArray *c23_d_sf_marshallOut(void *chartInstanceVoid, void
  *c23_inData)
{
  const mxArray *c23_mxArrayOutData = NULL;
  int32_T c23_i20;
  uint8_T c23_b_inData[31];
  int32_T c23_i21;
  uint8_T c23_u[31];
  const mxArray *c23_y = NULL;
  SFc23_LessonIII_startInstanceStruct *chartInstance;
  chartInstance = (SFc23_LessonIII_startInstanceStruct *)chartInstanceVoid;
  c23_mxArrayOutData = NULL;
  for (c23_i20 = 0; c23_i20 < 31; c23_i20++) {
    c23_b_inData[c23_i20] = (*(uint8_T (*)[31])c23_inData)[c23_i20];
  }

  for (c23_i21 = 0; c23_i21 < 31; c23_i21++) {
    c23_u[c23_i21] = c23_b_inData[c23_i21];
  }

  c23_y = NULL;
  sf_mex_assign(&c23_y, sf_mex_create("y", c23_u, 3, 0U, 1U, 0U, 2, 1, 31),
                false);
  sf_mex_assign(&c23_mxArrayOutData, c23_y, false);
  return c23_mxArrayOutData;
}

static const mxArray *c23_e_sf_marshallOut(void *chartInstanceVoid, void
  *c23_inData)
{
  const mxArray *c23_mxArrayOutData = NULL;
  real_T c23_u;
  const mxArray *c23_y = NULL;
  SFc23_LessonIII_startInstanceStruct *chartInstance;
  chartInstance = (SFc23_LessonIII_startInstanceStruct *)chartInstanceVoid;
  c23_mxArrayOutData = NULL;
  c23_u = *(real_T *)c23_inData;
  c23_y = NULL;
  sf_mex_assign(&c23_y, sf_mex_create("y", &c23_u, 0, 0U, 0U, 0U, 0), false);
  sf_mex_assign(&c23_mxArrayOutData, c23_y, false);
  return c23_mxArrayOutData;
}

static real_T c23_i_emlrt_marshallIn(SFc23_LessonIII_startInstanceStruct
  *chartInstance, const mxArray *c23_u, const emlrtMsgIdentifier *c23_parentId)
{
  real_T c23_y;
  real_T c23_d0;
  (void)chartInstance;
  sf_mex_import(c23_parentId, sf_mex_dup(c23_u), &c23_d0, 1, 0, 0U, 0, 0U, 0);
  c23_y = c23_d0;
  sf_mex_destroy(&c23_u);
  return c23_y;
}

static void c23_d_sf_marshallIn(void *chartInstanceVoid, const mxArray
  *c23_mxArrayInData, const char_T *c23_varName, void *c23_outData)
{
  const mxArray *c23_nargout;
  const char_T *c23_identifier;
  emlrtMsgIdentifier c23_thisId;
  real_T c23_y;
  SFc23_LessonIII_startInstanceStruct *chartInstance;
  chartInstance = (SFc23_LessonIII_startInstanceStruct *)chartInstanceVoid;
  c23_nargout = sf_mex_dup(c23_mxArrayInData);
  c23_identifier = c23_varName;
  c23_thisId.fIdentifier = c23_identifier;
  c23_thisId.fParent = NULL;
  c23_y = c23_i_emlrt_marshallIn(chartInstance, sf_mex_dup(c23_nargout),
    &c23_thisId);
  sf_mex_destroy(&c23_nargout);
  *(real_T *)c23_outData = c23_y;
  sf_mex_destroy(&c23_mxArrayInData);
}

static const mxArray *c23_f_sf_marshallOut(void *chartInstanceVoid, void
  *c23_inData)
{
  const mxArray *c23_mxArrayOutData = NULL;
  int32_T c23_i22;
  uint8_T c23_b_inData[24];
  int32_T c23_i23;
  uint8_T c23_u[24];
  const mxArray *c23_y = NULL;
  SFc23_LessonIII_startInstanceStruct *chartInstance;
  chartInstance = (SFc23_LessonIII_startInstanceStruct *)chartInstanceVoid;
  c23_mxArrayOutData = NULL;
  for (c23_i22 = 0; c23_i22 < 24; c23_i22++) {
    c23_b_inData[c23_i22] = (*(uint8_T (*)[24])c23_inData)[c23_i22];
  }

  for (c23_i23 = 0; c23_i23 < 24; c23_i23++) {
    c23_u[c23_i23] = c23_b_inData[c23_i23];
  }

  c23_y = NULL;
  sf_mex_assign(&c23_y, sf_mex_create("y", c23_u, 3, 0U, 1U, 0U, 2, 1, 24),
                false);
  sf_mex_assign(&c23_mxArrayOutData, c23_y, false);
  return c23_mxArrayOutData;
}

static void c23_j_emlrt_marshallIn(SFc23_LessonIII_startInstanceStruct
  *chartInstance, const mxArray *c23_u, const emlrtMsgIdentifier *c23_parentId,
  uint8_T c23_y[24])
{
  uint8_T c23_uv4[24];
  int32_T c23_i24;
  (void)chartInstance;
  sf_mex_import(c23_parentId, sf_mex_dup(c23_u), c23_uv4, 1, 3, 0U, 1, 0U, 2, 1,
                24);
  for (c23_i24 = 0; c23_i24 < 24; c23_i24++) {
    c23_y[c23_i24] = c23_uv4[c23_i24];
  }

  sf_mex_destroy(&c23_u);
}

static void c23_e_sf_marshallIn(void *chartInstanceVoid, const mxArray
  *c23_mxArrayInData, const char_T *c23_varName, void *c23_outData)
{
  const mxArray *c23_b_message;
  const char_T *c23_identifier;
  emlrtMsgIdentifier c23_thisId;
  uint8_T c23_y[24];
  int32_T c23_i25;
  SFc23_LessonIII_startInstanceStruct *chartInstance;
  chartInstance = (SFc23_LessonIII_startInstanceStruct *)chartInstanceVoid;
  c23_b_message = sf_mex_dup(c23_mxArrayInData);
  c23_identifier = c23_varName;
  c23_thisId.fIdentifier = c23_identifier;
  c23_thisId.fParent = NULL;
  c23_j_emlrt_marshallIn(chartInstance, sf_mex_dup(c23_b_message), &c23_thisId,
    c23_y);
  sf_mex_destroy(&c23_b_message);
  for (c23_i25 = 0; c23_i25 < 24; c23_i25++) {
    (*(uint8_T (*)[24])c23_outData)[c23_i25] = c23_y[c23_i25];
  }

  sf_mex_destroy(&c23_mxArrayInData);
}

static const mxArray *c23_g_sf_marshallOut(void *chartInstanceVoid, void
  *c23_inData)
{
  const mxArray *c23_mxArrayOutData = NULL;
  int32_T c23_i26;
  uint8_T c23_b_inData[26];
  int32_T c23_i27;
  uint8_T c23_u[26];
  const mxArray *c23_y = NULL;
  SFc23_LessonIII_startInstanceStruct *chartInstance;
  chartInstance = (SFc23_LessonIII_startInstanceStruct *)chartInstanceVoid;
  c23_mxArrayOutData = NULL;
  for (c23_i26 = 0; c23_i26 < 26; c23_i26++) {
    c23_b_inData[c23_i26] = (*(uint8_T (*)[26])c23_inData)[c23_i26];
  }

  for (c23_i27 = 0; c23_i27 < 26; c23_i27++) {
    c23_u[c23_i27] = c23_b_inData[c23_i27];
  }

  c23_y = NULL;
  sf_mex_assign(&c23_y, sf_mex_create("y", c23_u, 3, 0U, 1U, 0U, 2, 1, 26),
                false);
  sf_mex_assign(&c23_mxArrayOutData, c23_y, false);
  return c23_mxArrayOutData;
}

static void c23_k_emlrt_marshallIn(SFc23_LessonIII_startInstanceStruct
  *chartInstance, const mxArray *c23_u, const emlrtMsgIdentifier *c23_parentId,
  uint8_T c23_y[26])
{
  uint8_T c23_uv5[26];
  int32_T c23_i28;
  (void)chartInstance;
  sf_mex_import(c23_parentId, sf_mex_dup(c23_u), c23_uv5, 1, 3, 0U, 1, 0U, 2, 1,
                26);
  for (c23_i28 = 0; c23_i28 < 26; c23_i28++) {
    c23_y[c23_i28] = c23_uv5[c23_i28];
  }

  sf_mex_destroy(&c23_u);
}

static void c23_f_sf_marshallIn(void *chartInstanceVoid, const mxArray
  *c23_mxArrayInData, const char_T *c23_varName, void *c23_outData)
{
  const mxArray *c23_b_message;
  const char_T *c23_identifier;
  emlrtMsgIdentifier c23_thisId;
  uint8_T c23_y[26];
  int32_T c23_i29;
  SFc23_LessonIII_startInstanceStruct *chartInstance;
  chartInstance = (SFc23_LessonIII_startInstanceStruct *)chartInstanceVoid;
  c23_b_message = sf_mex_dup(c23_mxArrayInData);
  c23_identifier = c23_varName;
  c23_thisId.fIdentifier = c23_identifier;
  c23_thisId.fParent = NULL;
  c23_k_emlrt_marshallIn(chartInstance, sf_mex_dup(c23_b_message), &c23_thisId,
    c23_y);
  sf_mex_destroy(&c23_b_message);
  for (c23_i29 = 0; c23_i29 < 26; c23_i29++) {
    (*(uint8_T (*)[26])c23_outData)[c23_i29] = c23_y[c23_i29];
  }

  sf_mex_destroy(&c23_mxArrayInData);
}

static const mxArray *c23_h_sf_marshallOut(void *chartInstanceVoid, void
  *c23_inData)
{
  const mxArray *c23_mxArrayOutData = NULL;
  int32_T c23_i30;
  uint8_T c23_b_inData[27];
  int32_T c23_i31;
  uint8_T c23_u[27];
  const mxArray *c23_y = NULL;
  SFc23_LessonIII_startInstanceStruct *chartInstance;
  chartInstance = (SFc23_LessonIII_startInstanceStruct *)chartInstanceVoid;
  c23_mxArrayOutData = NULL;
  for (c23_i30 = 0; c23_i30 < 27; c23_i30++) {
    c23_b_inData[c23_i30] = (*(uint8_T (*)[27])c23_inData)[c23_i30];
  }

  for (c23_i31 = 0; c23_i31 < 27; c23_i31++) {
    c23_u[c23_i31] = c23_b_inData[c23_i31];
  }

  c23_y = NULL;
  sf_mex_assign(&c23_y, sf_mex_create("y", c23_u, 3, 0U, 1U, 0U, 2, 1, 27),
                false);
  sf_mex_assign(&c23_mxArrayOutData, c23_y, false);
  return c23_mxArrayOutData;
}

static void c23_l_emlrt_marshallIn(SFc23_LessonIII_startInstanceStruct
  *chartInstance, const mxArray *c23_u, const emlrtMsgIdentifier *c23_parentId,
  uint8_T c23_y[27])
{
  uint8_T c23_uv6[27];
  int32_T c23_i32;
  (void)chartInstance;
  sf_mex_import(c23_parentId, sf_mex_dup(c23_u), c23_uv6, 1, 3, 0U, 1, 0U, 2, 1,
                27);
  for (c23_i32 = 0; c23_i32 < 27; c23_i32++) {
    c23_y[c23_i32] = c23_uv6[c23_i32];
  }

  sf_mex_destroy(&c23_u);
}

static void c23_g_sf_marshallIn(void *chartInstanceVoid, const mxArray
  *c23_mxArrayInData, const char_T *c23_varName, void *c23_outData)
{
  const mxArray *c23_b_message;
  const char_T *c23_identifier;
  emlrtMsgIdentifier c23_thisId;
  uint8_T c23_y[27];
  int32_T c23_i33;
  SFc23_LessonIII_startInstanceStruct *chartInstance;
  chartInstance = (SFc23_LessonIII_startInstanceStruct *)chartInstanceVoid;
  c23_b_message = sf_mex_dup(c23_mxArrayInData);
  c23_identifier = c23_varName;
  c23_thisId.fIdentifier = c23_identifier;
  c23_thisId.fParent = NULL;
  c23_l_emlrt_marshallIn(chartInstance, sf_mex_dup(c23_b_message), &c23_thisId,
    c23_y);
  sf_mex_destroy(&c23_b_message);
  for (c23_i33 = 0; c23_i33 < 27; c23_i33++) {
    (*(uint8_T (*)[27])c23_outData)[c23_i33] = c23_y[c23_i33];
  }

  sf_mex_destroy(&c23_mxArrayInData);
}

static const mxArray *c23_i_sf_marshallOut(void *chartInstanceVoid, void
  *c23_inData)
{
  const mxArray *c23_mxArrayOutData = NULL;
  int32_T c23_i34;
  uint8_T c23_b_inData[6];
  int32_T c23_i35;
  uint8_T c23_u[6];
  const mxArray *c23_y = NULL;
  SFc23_LessonIII_startInstanceStruct *chartInstance;
  chartInstance = (SFc23_LessonIII_startInstanceStruct *)chartInstanceVoid;
  c23_mxArrayOutData = NULL;
  for (c23_i34 = 0; c23_i34 < 6; c23_i34++) {
    c23_b_inData[c23_i34] = (*(uint8_T (*)[6])c23_inData)[c23_i34];
  }

  for (c23_i35 = 0; c23_i35 < 6; c23_i35++) {
    c23_u[c23_i35] = c23_b_inData[c23_i35];
  }

  c23_y = NULL;
  sf_mex_assign(&c23_y, sf_mex_create("y", c23_u, 3, 0U, 1U, 0U, 2, 1, 6), false);
  sf_mex_assign(&c23_mxArrayOutData, c23_y, false);
  return c23_mxArrayOutData;
}

static void c23_m_emlrt_marshallIn(SFc23_LessonIII_startInstanceStruct
  *chartInstance, const mxArray *c23_u, const emlrtMsgIdentifier *c23_parentId,
  uint8_T c23_y[6])
{
  uint8_T c23_uv7[6];
  int32_T c23_i36;
  (void)chartInstance;
  sf_mex_import(c23_parentId, sf_mex_dup(c23_u), c23_uv7, 1, 3, 0U, 1, 0U, 2, 1,
                6);
  for (c23_i36 = 0; c23_i36 < 6; c23_i36++) {
    c23_y[c23_i36] = c23_uv7[c23_i36];
  }

  sf_mex_destroy(&c23_u);
}

static void c23_h_sf_marshallIn(void *chartInstanceVoid, const mxArray
  *c23_mxArrayInData, const char_T *c23_varName, void *c23_outData)
{
  const mxArray *c23_positions;
  const char_T *c23_identifier;
  emlrtMsgIdentifier c23_thisId;
  uint8_T c23_y[6];
  int32_T c23_i37;
  SFc23_LessonIII_startInstanceStruct *chartInstance;
  chartInstance = (SFc23_LessonIII_startInstanceStruct *)chartInstanceVoid;
  c23_positions = sf_mex_dup(c23_mxArrayInData);
  c23_identifier = c23_varName;
  c23_thisId.fIdentifier = c23_identifier;
  c23_thisId.fParent = NULL;
  c23_m_emlrt_marshallIn(chartInstance, sf_mex_dup(c23_positions), &c23_thisId,
    c23_y);
  sf_mex_destroy(&c23_positions);
  for (c23_i37 = 0; c23_i37 < 6; c23_i37++) {
    (*(uint8_T (*)[6])c23_outData)[c23_i37] = c23_y[c23_i37];
  }

  sf_mex_destroy(&c23_mxArrayInData);
}

static const mxArray *c23_j_sf_marshallOut(void *chartInstanceVoid, void
  *c23_inData)
{
  const mxArray *c23_mxArrayOutData = NULL;
  c23_Player c23_u;
  const mxArray *c23_y = NULL;
  int8_T c23_b_u;
  const mxArray *c23_b_y = NULL;
  int8_T c23_c_u;
  const mxArray *c23_c_y = NULL;
  int16_T c23_d_u;
  const mxArray *c23_d_y = NULL;
  uint8_T c23_e_u;
  const mxArray *c23_e_y = NULL;
  uint8_T c23_f_u;
  const mxArray *c23_f_y = NULL;
  uint8_T c23_g_u;
  const mxArray *c23_g_y = NULL;
  SFc23_LessonIII_startInstanceStruct *chartInstance;
  chartInstance = (SFc23_LessonIII_startInstanceStruct *)chartInstanceVoid;
  c23_mxArrayOutData = NULL;
  c23_u = *(c23_Player *)c23_inData;
  c23_y = NULL;
  sf_mex_assign(&c23_y, sf_mex_createstruct("structure", 2, 1, 1), false);
  c23_b_u = c23_u.x;
  c23_b_y = NULL;
  sf_mex_assign(&c23_b_y, sf_mex_create("y", &c23_b_u, 2, 0U, 0U, 0U, 0), false);
  sf_mex_addfield(c23_y, c23_b_y, "x", "x", 0);
  c23_c_u = c23_u.y;
  c23_c_y = NULL;
  sf_mex_assign(&c23_c_y, sf_mex_create("y", &c23_c_u, 2, 0U, 0U, 0U, 0), false);
  sf_mex_addfield(c23_y, c23_c_y, "y", "y", 0);
  c23_d_u = c23_u.orientation;
  c23_d_y = NULL;
  sf_mex_assign(&c23_d_y, sf_mex_create("y", &c23_d_u, 4, 0U, 0U, 0U, 0), false);
  sf_mex_addfield(c23_y, c23_d_y, "orientation", "orientation", 0);
  c23_e_u = c23_u.color;
  c23_e_y = NULL;
  sf_mex_assign(&c23_e_y, sf_mex_create("y", &c23_e_u, 3, 0U, 0U, 0U, 0), false);
  sf_mex_addfield(c23_y, c23_e_y, "color", "color", 0);
  c23_f_u = c23_u.position;
  c23_f_y = NULL;
  sf_mex_assign(&c23_f_y, sf_mex_create("y", &c23_f_u, 3, 0U, 0U, 0U, 0), false);
  sf_mex_addfield(c23_y, c23_f_y, "position", "position", 0);
  c23_g_u = c23_u.valid;
  c23_g_y = NULL;
  sf_mex_assign(&c23_g_y, sf_mex_create("y", &c23_g_u, 3, 0U, 0U, 0U, 0), false);
  sf_mex_addfield(c23_y, c23_g_y, "valid", "valid", 0);
  sf_mex_assign(&c23_mxArrayOutData, c23_y, false);
  return c23_mxArrayOutData;
}

static c23_Player c23_n_emlrt_marshallIn(SFc23_LessonIII_startInstanceStruct
  *chartInstance, const mxArray *c23_u, const emlrtMsgIdentifier *c23_parentId)
{
  c23_Player c23_y;
  emlrtMsgIdentifier c23_thisId;
  static const char * c23_fieldNames[6] = { "x", "y", "orientation", "color",
    "position", "valid" };

  c23_thisId.fParent = c23_parentId;
  sf_mex_check_struct(c23_parentId, c23_u, 6, c23_fieldNames, 0U, NULL);
  c23_thisId.fIdentifier = "x";
  c23_y.x = c23_e_emlrt_marshallIn(chartInstance, sf_mex_dup(sf_mex_getfield
    (c23_u, "x", "x", 0)), &c23_thisId);
  c23_thisId.fIdentifier = "y";
  c23_y.y = c23_e_emlrt_marshallIn(chartInstance, sf_mex_dup(sf_mex_getfield
    (c23_u, "y", "y", 0)), &c23_thisId);
  c23_thisId.fIdentifier = "orientation";
  c23_y.orientation = c23_f_emlrt_marshallIn(chartInstance, sf_mex_dup
    (sf_mex_getfield(c23_u, "orientation", "orientation", 0)), &c23_thisId);
  c23_thisId.fIdentifier = "color";
  c23_y.color = c23_b_emlrt_marshallIn(chartInstance, sf_mex_dup(sf_mex_getfield
                                        (c23_u, "color", "color", 0)),
    &c23_thisId);
  c23_thisId.fIdentifier = "position";
  c23_y.position = c23_b_emlrt_marshallIn(chartInstance, sf_mex_dup
    (sf_mex_getfield(c23_u, "position", "position", 0)), &c23_thisId);
  c23_thisId.fIdentifier = "valid";
  c23_y.valid = c23_b_emlrt_marshallIn(chartInstance, sf_mex_dup(sf_mex_getfield
                                        (c23_u, "valid", "valid", 0)),
    &c23_thisId);
  sf_mex_destroy(&c23_u);
  return c23_y;
}

static void c23_i_sf_marshallIn(void *chartInstanceVoid, const mxArray
  *c23_mxArrayInData, const char_T *c23_varName, void *c23_outData)
{
  const mxArray *c23_tempEmpty;
  const char_T *c23_identifier;
  emlrtMsgIdentifier c23_thisId;
  c23_Player c23_y;
  SFc23_LessonIII_startInstanceStruct *chartInstance;
  chartInstance = (SFc23_LessonIII_startInstanceStruct *)chartInstanceVoid;
  c23_tempEmpty = sf_mex_dup(c23_mxArrayInData);
  c23_identifier = c23_varName;
  c23_thisId.fIdentifier = c23_identifier;
  c23_thisId.fParent = NULL;
  c23_y = c23_n_emlrt_marshallIn(chartInstance, sf_mex_dup(c23_tempEmpty),
    &c23_thisId);
  sf_mex_destroy(&c23_tempEmpty);
  *(c23_Player *)c23_outData = c23_y;
  sf_mex_destroy(&c23_mxArrayInData);
}

const mxArray *sf_c23_LessonIII_start_get_eml_resolved_functions_info(void)
{
  const mxArray *c23_nameCaptureInfo = NULL;
  c23_nameCaptureInfo = NULL;
  sf_mex_assign(&c23_nameCaptureInfo, sf_mex_createstruct("structure", 2, 52, 1),
                false);
  c23_info_helper(&c23_nameCaptureInfo);
  sf_mex_emlrtNameCapturePostProcessR2012a(&c23_nameCaptureInfo);
  return c23_nameCaptureInfo;
}

static void c23_info_helper(const mxArray **c23_info)
{
  const mxArray *c23_rhs0 = NULL;
  const mxArray *c23_lhs0 = NULL;
  const mxArray *c23_rhs1 = NULL;
  const mxArray *c23_lhs1 = NULL;
  const mxArray *c23_rhs2 = NULL;
  const mxArray *c23_lhs2 = NULL;
  const mxArray *c23_rhs3 = NULL;
  const mxArray *c23_lhs3 = NULL;
  const mxArray *c23_rhs4 = NULL;
  const mxArray *c23_lhs4 = NULL;
  const mxArray *c23_rhs5 = NULL;
  const mxArray *c23_lhs5 = NULL;
  const mxArray *c23_rhs6 = NULL;
  const mxArray *c23_lhs6 = NULL;
  const mxArray *c23_rhs7 = NULL;
  const mxArray *c23_lhs7 = NULL;
  const mxArray *c23_rhs8 = NULL;
  const mxArray *c23_lhs8 = NULL;
  const mxArray *c23_rhs9 = NULL;
  const mxArray *c23_lhs9 = NULL;
  const mxArray *c23_rhs10 = NULL;
  const mxArray *c23_lhs10 = NULL;
  const mxArray *c23_rhs11 = NULL;
  const mxArray *c23_lhs11 = NULL;
  const mxArray *c23_rhs12 = NULL;
  const mxArray *c23_lhs12 = NULL;
  const mxArray *c23_rhs13 = NULL;
  const mxArray *c23_lhs13 = NULL;
  const mxArray *c23_rhs14 = NULL;
  const mxArray *c23_lhs14 = NULL;
  const mxArray *c23_rhs15 = NULL;
  const mxArray *c23_lhs15 = NULL;
  const mxArray *c23_rhs16 = NULL;
  const mxArray *c23_lhs16 = NULL;
  const mxArray *c23_rhs17 = NULL;
  const mxArray *c23_lhs17 = NULL;
  const mxArray *c23_rhs18 = NULL;
  const mxArray *c23_lhs18 = NULL;
  const mxArray *c23_rhs19 = NULL;
  const mxArray *c23_lhs19 = NULL;
  const mxArray *c23_rhs20 = NULL;
  const mxArray *c23_lhs20 = NULL;
  const mxArray *c23_rhs21 = NULL;
  const mxArray *c23_lhs21 = NULL;
  const mxArray *c23_rhs22 = NULL;
  const mxArray *c23_lhs22 = NULL;
  const mxArray *c23_rhs23 = NULL;
  const mxArray *c23_lhs23 = NULL;
  const mxArray *c23_rhs24 = NULL;
  const mxArray *c23_lhs24 = NULL;
  const mxArray *c23_rhs25 = NULL;
  const mxArray *c23_lhs25 = NULL;
  const mxArray *c23_rhs26 = NULL;
  const mxArray *c23_lhs26 = NULL;
  const mxArray *c23_rhs27 = NULL;
  const mxArray *c23_lhs27 = NULL;
  const mxArray *c23_rhs28 = NULL;
  const mxArray *c23_lhs28 = NULL;
  const mxArray *c23_rhs29 = NULL;
  const mxArray *c23_lhs29 = NULL;
  const mxArray *c23_rhs30 = NULL;
  const mxArray *c23_lhs30 = NULL;
  const mxArray *c23_rhs31 = NULL;
  const mxArray *c23_lhs31 = NULL;
  const mxArray *c23_rhs32 = NULL;
  const mxArray *c23_lhs32 = NULL;
  const mxArray *c23_rhs33 = NULL;
  const mxArray *c23_lhs33 = NULL;
  const mxArray *c23_rhs34 = NULL;
  const mxArray *c23_lhs34 = NULL;
  const mxArray *c23_rhs35 = NULL;
  const mxArray *c23_lhs35 = NULL;
  const mxArray *c23_rhs36 = NULL;
  const mxArray *c23_lhs36 = NULL;
  const mxArray *c23_rhs37 = NULL;
  const mxArray *c23_lhs37 = NULL;
  const mxArray *c23_rhs38 = NULL;
  const mxArray *c23_lhs38 = NULL;
  const mxArray *c23_rhs39 = NULL;
  const mxArray *c23_lhs39 = NULL;
  const mxArray *c23_rhs40 = NULL;
  const mxArray *c23_lhs40 = NULL;
  const mxArray *c23_rhs41 = NULL;
  const mxArray *c23_lhs41 = NULL;
  const mxArray *c23_rhs42 = NULL;
  const mxArray *c23_lhs42 = NULL;
  const mxArray *c23_rhs43 = NULL;
  const mxArray *c23_lhs43 = NULL;
  const mxArray *c23_rhs44 = NULL;
  const mxArray *c23_lhs44 = NULL;
  const mxArray *c23_rhs45 = NULL;
  const mxArray *c23_lhs45 = NULL;
  const mxArray *c23_rhs46 = NULL;
  const mxArray *c23_lhs46 = NULL;
  const mxArray *c23_rhs47 = NULL;
  const mxArray *c23_lhs47 = NULL;
  const mxArray *c23_rhs48 = NULL;
  const mxArray *c23_lhs48 = NULL;
  const mxArray *c23_rhs49 = NULL;
  const mxArray *c23_lhs49 = NULL;
  const mxArray *c23_rhs50 = NULL;
  const mxArray *c23_lhs50 = NULL;
  const mxArray *c23_rhs51 = NULL;
  const mxArray *c23_lhs51 = NULL;
  sf_mex_addfield(*c23_info, c23_emlrt_marshallOut(""), "context", "context", 0);
  sf_mex_addfield(*c23_info, c23_emlrt_marshallOut("typecast"), "name", "name",
                  0);
  sf_mex_addfield(*c23_info, c23_emlrt_marshallOut("uint8"), "dominantType",
                  "dominantType", 0);
  sf_mex_addfield(*c23_info, c23_emlrt_marshallOut(
    "[ILXE]$matlabroot$/toolbox/eml/lib/matlab/datatypes/typecast.m"),
                  "resolved", "resolved", 0);
  sf_mex_addfield(*c23_info, c23_b_emlrt_marshallOut(1407168096U), "fileTimeLo",
                  "fileTimeLo", 0);
  sf_mex_addfield(*c23_info, c23_b_emlrt_marshallOut(0U), "fileTimeHi",
                  "fileTimeHi", 0);
  sf_mex_addfield(*c23_info, c23_b_emlrt_marshallOut(0U), "mFileTimeLo",
                  "mFileTimeLo", 0);
  sf_mex_addfield(*c23_info, c23_b_emlrt_marshallOut(0U), "mFileTimeHi",
                  "mFileTimeHi", 0);
  sf_mex_assign(&c23_rhs0, sf_mex_createcellmatrix(0, 1), false);
  sf_mex_assign(&c23_lhs0, sf_mex_createcellmatrix(0, 1), false);
  sf_mex_addfield(*c23_info, sf_mex_duplicatearraysafe(&c23_rhs0), "rhs", "rhs",
                  0);
  sf_mex_addfield(*c23_info, sf_mex_duplicatearraysafe(&c23_lhs0), "lhs", "lhs",
                  0);
  sf_mex_addfield(*c23_info, c23_emlrt_marshallOut(
    "[ILXE]$matlabroot$/toolbox/eml/lib/matlab/datatypes/typecast.m"), "context",
                  "context", 1);
  sf_mex_addfield(*c23_info, c23_emlrt_marshallOut(
    "coder.internal.isBuiltInNumeric"), "name", "name", 1);
  sf_mex_addfield(*c23_info, c23_emlrt_marshallOut("uint8"), "dominantType",
                  "dominantType", 1);
  sf_mex_addfield(*c23_info, c23_emlrt_marshallOut(
    "[ILXE]$matlabroot$/toolbox/shared/coder/coder/+coder/+internal/isBuiltInNumeric.m"),
                  "resolved", "resolved", 1);
  sf_mex_addfield(*c23_info, c23_b_emlrt_marshallOut(1395935456U), "fileTimeLo",
                  "fileTimeLo", 1);
  sf_mex_addfield(*c23_info, c23_b_emlrt_marshallOut(0U), "fileTimeHi",
                  "fileTimeHi", 1);
  sf_mex_addfield(*c23_info, c23_b_emlrt_marshallOut(0U), "mFileTimeLo",
                  "mFileTimeLo", 1);
  sf_mex_addfield(*c23_info, c23_b_emlrt_marshallOut(0U), "mFileTimeHi",
                  "mFileTimeHi", 1);
  sf_mex_assign(&c23_rhs1, sf_mex_createcellmatrix(0, 1), false);
  sf_mex_assign(&c23_lhs1, sf_mex_createcellmatrix(0, 1), false);
  sf_mex_addfield(*c23_info, sf_mex_duplicatearraysafe(&c23_rhs1), "rhs", "rhs",
                  1);
  sf_mex_addfield(*c23_info, sf_mex_duplicatearraysafe(&c23_lhs1), "lhs", "lhs",
                  1);
  sf_mex_addfield(*c23_info, c23_emlrt_marshallOut(
    "[ILXE]$matlabroot$/toolbox/eml/lib/matlab/datatypes/typecast.m"), "context",
                  "context", 2);
  sf_mex_addfield(*c23_info, c23_emlrt_marshallOut("deblank"), "name", "name", 2);
  sf_mex_addfield(*c23_info, c23_emlrt_marshallOut("char"), "dominantType",
                  "dominantType", 2);
  sf_mex_addfield(*c23_info, c23_emlrt_marshallOut(
    "[ILXE]$matlabroot$/toolbox/eml/lib/matlab/strfun/deblank.m"), "resolved",
                  "resolved", 2);
  sf_mex_addfield(*c23_info, c23_b_emlrt_marshallOut(1331308488U), "fileTimeLo",
                  "fileTimeLo", 2);
  sf_mex_addfield(*c23_info, c23_b_emlrt_marshallOut(0U), "fileTimeHi",
                  "fileTimeHi", 2);
  sf_mex_addfield(*c23_info, c23_b_emlrt_marshallOut(0U), "mFileTimeLo",
                  "mFileTimeLo", 2);
  sf_mex_addfield(*c23_info, c23_b_emlrt_marshallOut(0U), "mFileTimeHi",
                  "mFileTimeHi", 2);
  sf_mex_assign(&c23_rhs2, sf_mex_createcellmatrix(0, 1), false);
  sf_mex_assign(&c23_lhs2, sf_mex_createcellmatrix(0, 1), false);
  sf_mex_addfield(*c23_info, sf_mex_duplicatearraysafe(&c23_rhs2), "rhs", "rhs",
                  2);
  sf_mex_addfield(*c23_info, sf_mex_duplicatearraysafe(&c23_lhs2), "lhs", "lhs",
                  2);
  sf_mex_addfield(*c23_info, c23_emlrt_marshallOut(
    "[ILXE]$matlabroot$/toolbox/eml/lib/matlab/strfun/deblank.m"), "context",
                  "context", 3);
  sf_mex_addfield(*c23_info, c23_emlrt_marshallOut("ismatrix"), "name", "name",
                  3);
  sf_mex_addfield(*c23_info, c23_emlrt_marshallOut("char"), "dominantType",
                  "dominantType", 3);
  sf_mex_addfield(*c23_info, c23_emlrt_marshallOut(
    "[ILXE]$matlabroot$/toolbox/eml/lib/matlab/elmat/ismatrix.m"), "resolved",
                  "resolved", 3);
  sf_mex_addfield(*c23_info, c23_b_emlrt_marshallOut(1331308458U), "fileTimeLo",
                  "fileTimeLo", 3);
  sf_mex_addfield(*c23_info, c23_b_emlrt_marshallOut(0U), "fileTimeHi",
                  "fileTimeHi", 3);
  sf_mex_addfield(*c23_info, c23_b_emlrt_marshallOut(0U), "mFileTimeLo",
                  "mFileTimeLo", 3);
  sf_mex_addfield(*c23_info, c23_b_emlrt_marshallOut(0U), "mFileTimeHi",
                  "mFileTimeHi", 3);
  sf_mex_assign(&c23_rhs3, sf_mex_createcellmatrix(0, 1), false);
  sf_mex_assign(&c23_lhs3, sf_mex_createcellmatrix(0, 1), false);
  sf_mex_addfield(*c23_info, sf_mex_duplicatearraysafe(&c23_rhs3), "rhs", "rhs",
                  3);
  sf_mex_addfield(*c23_info, sf_mex_duplicatearraysafe(&c23_lhs3), "lhs", "lhs",
                  3);
  sf_mex_addfield(*c23_info, c23_emlrt_marshallOut(
    "[ILXE]$matlabroot$/toolbox/eml/lib/matlab/strfun/deblank.m!allwspace"),
                  "context", "context", 4);
  sf_mex_addfield(*c23_info, c23_emlrt_marshallOut("isstrprop"), "name", "name",
                  4);
  sf_mex_addfield(*c23_info, c23_emlrt_marshallOut("char"), "dominantType",
                  "dominantType", 4);
  sf_mex_addfield(*c23_info, c23_emlrt_marshallOut(
    "[ILXE]$matlabroot$/toolbox/eml/lib/matlab/strfun/isstrprop.m"), "resolved",
                  "resolved", 4);
  sf_mex_addfield(*c23_info, c23_b_emlrt_marshallOut(1375984294U), "fileTimeLo",
                  "fileTimeLo", 4);
  sf_mex_addfield(*c23_info, c23_b_emlrt_marshallOut(0U), "fileTimeHi",
                  "fileTimeHi", 4);
  sf_mex_addfield(*c23_info, c23_b_emlrt_marshallOut(0U), "mFileTimeLo",
                  "mFileTimeLo", 4);
  sf_mex_addfield(*c23_info, c23_b_emlrt_marshallOut(0U), "mFileTimeHi",
                  "mFileTimeHi", 4);
  sf_mex_assign(&c23_rhs4, sf_mex_createcellmatrix(0, 1), false);
  sf_mex_assign(&c23_lhs4, sf_mex_createcellmatrix(0, 1), false);
  sf_mex_addfield(*c23_info, sf_mex_duplicatearraysafe(&c23_rhs4), "rhs", "rhs",
                  4);
  sf_mex_addfield(*c23_info, sf_mex_duplicatearraysafe(&c23_lhs4), "lhs", "lhs",
                  4);
  sf_mex_addfield(*c23_info, c23_emlrt_marshallOut(
    "[ILXE]$matlabroot$/toolbox/eml/lib/matlab/strfun/isstrprop.m!apply_property_predicate"),
                  "context", "context", 5);
  sf_mex_addfield(*c23_info, c23_emlrt_marshallOut("eml_assert_supported_string"),
                  "name", "name", 5);
  sf_mex_addfield(*c23_info, c23_emlrt_marshallOut("char"), "dominantType",
                  "dominantType", 5);
  sf_mex_addfield(*c23_info, c23_emlrt_marshallOut(
    "[ILXE]$matlabroot$/toolbox/eml/lib/matlab/strfun/eml_assert_supported_string.m"),
                  "resolved", "resolved", 5);
  sf_mex_addfield(*c23_info, c23_b_emlrt_marshallOut(1327422710U), "fileTimeLo",
                  "fileTimeLo", 5);
  sf_mex_addfield(*c23_info, c23_b_emlrt_marshallOut(0U), "fileTimeHi",
                  "fileTimeHi", 5);
  sf_mex_addfield(*c23_info, c23_b_emlrt_marshallOut(0U), "mFileTimeLo",
                  "mFileTimeLo", 5);
  sf_mex_addfield(*c23_info, c23_b_emlrt_marshallOut(0U), "mFileTimeHi",
                  "mFileTimeHi", 5);
  sf_mex_assign(&c23_rhs5, sf_mex_createcellmatrix(0, 1), false);
  sf_mex_assign(&c23_lhs5, sf_mex_createcellmatrix(0, 1), false);
  sf_mex_addfield(*c23_info, sf_mex_duplicatearraysafe(&c23_rhs5), "rhs", "rhs",
                  5);
  sf_mex_addfield(*c23_info, sf_mex_duplicatearraysafe(&c23_lhs5), "lhs", "lhs",
                  5);
  sf_mex_addfield(*c23_info, c23_emlrt_marshallOut(
    "[ILXE]$matlabroot$/toolbox/eml/lib/matlab/strfun/eml_assert_supported_string.m!inrange"),
                  "context", "context", 6);
  sf_mex_addfield(*c23_info, c23_emlrt_marshallOut("eml_charmax"), "name",
                  "name", 6);
  sf_mex_addfield(*c23_info, c23_emlrt_marshallOut(""), "dominantType",
                  "dominantType", 6);
  sf_mex_addfield(*c23_info, c23_emlrt_marshallOut(
    "[ILXE]$matlabroot$/toolbox/eml/lib/matlab/strfun/eml_charmax.m"),
                  "resolved", "resolved", 6);
  sf_mex_addfield(*c23_info, c23_b_emlrt_marshallOut(1327422710U), "fileTimeLo",
                  "fileTimeLo", 6);
  sf_mex_addfield(*c23_info, c23_b_emlrt_marshallOut(0U), "fileTimeHi",
                  "fileTimeHi", 6);
  sf_mex_addfield(*c23_info, c23_b_emlrt_marshallOut(0U), "mFileTimeLo",
                  "mFileTimeLo", 6);
  sf_mex_addfield(*c23_info, c23_b_emlrt_marshallOut(0U), "mFileTimeHi",
                  "mFileTimeHi", 6);
  sf_mex_assign(&c23_rhs6, sf_mex_createcellmatrix(0, 1), false);
  sf_mex_assign(&c23_lhs6, sf_mex_createcellmatrix(0, 1), false);
  sf_mex_addfield(*c23_info, sf_mex_duplicatearraysafe(&c23_rhs6), "rhs", "rhs",
                  6);
  sf_mex_addfield(*c23_info, sf_mex_duplicatearraysafe(&c23_lhs6), "lhs", "lhs",
                  6);
  sf_mex_addfield(*c23_info, c23_emlrt_marshallOut(
    "[ILXE]$matlabroot$/toolbox/eml/lib/matlab/strfun/eml_charmax.m"), "context",
                  "context", 7);
  sf_mex_addfield(*c23_info, c23_emlrt_marshallOut("intmax"), "name", "name", 7);
  sf_mex_addfield(*c23_info, c23_emlrt_marshallOut("char"), "dominantType",
                  "dominantType", 7);
  sf_mex_addfield(*c23_info, c23_emlrt_marshallOut(
    "[ILXE]$matlabroot$/toolbox/eml/lib/matlab/elmat/intmax.m"), "resolved",
                  "resolved", 7);
  sf_mex_addfield(*c23_info, c23_b_emlrt_marshallOut(1362265482U), "fileTimeLo",
                  "fileTimeLo", 7);
  sf_mex_addfield(*c23_info, c23_b_emlrt_marshallOut(0U), "fileTimeHi",
                  "fileTimeHi", 7);
  sf_mex_addfield(*c23_info, c23_b_emlrt_marshallOut(0U), "mFileTimeLo",
                  "mFileTimeLo", 7);
  sf_mex_addfield(*c23_info, c23_b_emlrt_marshallOut(0U), "mFileTimeHi",
                  "mFileTimeHi", 7);
  sf_mex_assign(&c23_rhs7, sf_mex_createcellmatrix(0, 1), false);
  sf_mex_assign(&c23_lhs7, sf_mex_createcellmatrix(0, 1), false);
  sf_mex_addfield(*c23_info, sf_mex_duplicatearraysafe(&c23_rhs7), "rhs", "rhs",
                  7);
  sf_mex_addfield(*c23_info, sf_mex_duplicatearraysafe(&c23_lhs7), "lhs", "lhs",
                  7);
  sf_mex_addfield(*c23_info, c23_emlrt_marshallOut(
    "[ILXE]$matlabroot$/toolbox/eml/lib/matlab/elmat/intmax.m"), "context",
                  "context", 8);
  sf_mex_addfield(*c23_info, c23_emlrt_marshallOut("eml_switch_helper"), "name",
                  "name", 8);
  sf_mex_addfield(*c23_info, c23_emlrt_marshallOut(""), "dominantType",
                  "dominantType", 8);
  sf_mex_addfield(*c23_info, c23_emlrt_marshallOut(
    "[ILXE]$matlabroot$/toolbox/eml/lib/matlab/eml/eml_switch_helper.m"),
                  "resolved", "resolved", 8);
  sf_mex_addfield(*c23_info, c23_b_emlrt_marshallOut(1393334458U), "fileTimeLo",
                  "fileTimeLo", 8);
  sf_mex_addfield(*c23_info, c23_b_emlrt_marshallOut(0U), "fileTimeHi",
                  "fileTimeHi", 8);
  sf_mex_addfield(*c23_info, c23_b_emlrt_marshallOut(0U), "mFileTimeLo",
                  "mFileTimeLo", 8);
  sf_mex_addfield(*c23_info, c23_b_emlrt_marshallOut(0U), "mFileTimeHi",
                  "mFileTimeHi", 8);
  sf_mex_assign(&c23_rhs8, sf_mex_createcellmatrix(0, 1), false);
  sf_mex_assign(&c23_lhs8, sf_mex_createcellmatrix(0, 1), false);
  sf_mex_addfield(*c23_info, sf_mex_duplicatearraysafe(&c23_rhs8), "rhs", "rhs",
                  8);
  sf_mex_addfield(*c23_info, sf_mex_duplicatearraysafe(&c23_lhs8), "lhs", "lhs",
                  8);
  sf_mex_addfield(*c23_info, c23_emlrt_marshallOut(
    "[ILXE]$matlabroot$/toolbox/eml/lib/matlab/strfun/eml_assert_supported_string.m"),
                  "context", "context", 9);
  sf_mex_addfield(*c23_info, c23_emlrt_marshallOut("eml_charmax"), "name",
                  "name", 9);
  sf_mex_addfield(*c23_info, c23_emlrt_marshallOut(""), "dominantType",
                  "dominantType", 9);
  sf_mex_addfield(*c23_info, c23_emlrt_marshallOut(
    "[ILXE]$matlabroot$/toolbox/eml/lib/matlab/strfun/eml_charmax.m"),
                  "resolved", "resolved", 9);
  sf_mex_addfield(*c23_info, c23_b_emlrt_marshallOut(1327422710U), "fileTimeLo",
                  "fileTimeLo", 9);
  sf_mex_addfield(*c23_info, c23_b_emlrt_marshallOut(0U), "fileTimeHi",
                  "fileTimeHi", 9);
  sf_mex_addfield(*c23_info, c23_b_emlrt_marshallOut(0U), "mFileTimeLo",
                  "mFileTimeLo", 9);
  sf_mex_addfield(*c23_info, c23_b_emlrt_marshallOut(0U), "mFileTimeHi",
                  "mFileTimeHi", 9);
  sf_mex_assign(&c23_rhs9, sf_mex_createcellmatrix(0, 1), false);
  sf_mex_assign(&c23_lhs9, sf_mex_createcellmatrix(0, 1), false);
  sf_mex_addfield(*c23_info, sf_mex_duplicatearraysafe(&c23_rhs9), "rhs", "rhs",
                  9);
  sf_mex_addfield(*c23_info, sf_mex_duplicatearraysafe(&c23_lhs9), "lhs", "lhs",
                  9);
  sf_mex_addfield(*c23_info, c23_emlrt_marshallOut(
    "[ILXE]$matlabroot$/toolbox/eml/lib/matlab/strfun/isstrprop.m!apply_property_predicate"),
                  "context", "context", 10);
  sf_mex_addfield(*c23_info, c23_emlrt_marshallOut("eml_charmax"), "name",
                  "name", 10);
  sf_mex_addfield(*c23_info, c23_emlrt_marshallOut(""), "dominantType",
                  "dominantType", 10);
  sf_mex_addfield(*c23_info, c23_emlrt_marshallOut(
    "[ILXE]$matlabroot$/toolbox/eml/lib/matlab/strfun/eml_charmax.m"),
                  "resolved", "resolved", 10);
  sf_mex_addfield(*c23_info, c23_b_emlrt_marshallOut(1327422710U), "fileTimeLo",
                  "fileTimeLo", 10);
  sf_mex_addfield(*c23_info, c23_b_emlrt_marshallOut(0U), "fileTimeHi",
                  "fileTimeHi", 10);
  sf_mex_addfield(*c23_info, c23_b_emlrt_marshallOut(0U), "mFileTimeLo",
                  "mFileTimeLo", 10);
  sf_mex_addfield(*c23_info, c23_b_emlrt_marshallOut(0U), "mFileTimeHi",
                  "mFileTimeHi", 10);
  sf_mex_assign(&c23_rhs10, sf_mex_createcellmatrix(0, 1), false);
  sf_mex_assign(&c23_lhs10, sf_mex_createcellmatrix(0, 1), false);
  sf_mex_addfield(*c23_info, sf_mex_duplicatearraysafe(&c23_rhs10), "rhs", "rhs",
                  10);
  sf_mex_addfield(*c23_info, sf_mex_duplicatearraysafe(&c23_lhs10), "lhs", "lhs",
                  10);
  sf_mex_addfield(*c23_info, c23_emlrt_marshallOut(
    "[ILXE]$matlabroot$/toolbox/eml/lib/matlab/strfun/isstrprop.m!apply_property_predicate"),
                  "context", "context", 11);
  sf_mex_addfield(*c23_info, c23_emlrt_marshallOut("colon"), "name", "name", 11);
  sf_mex_addfield(*c23_info, c23_emlrt_marshallOut("int8"), "dominantType",
                  "dominantType", 11);
  sf_mex_addfield(*c23_info, c23_emlrt_marshallOut(
    "[ILXE]$matlabroot$/toolbox/eml/lib/matlab/ops/colon.m"), "resolved",
                  "resolved", 11);
  sf_mex_addfield(*c23_info, c23_b_emlrt_marshallOut(1378299588U), "fileTimeLo",
                  "fileTimeLo", 11);
  sf_mex_addfield(*c23_info, c23_b_emlrt_marshallOut(0U), "fileTimeHi",
                  "fileTimeHi", 11);
  sf_mex_addfield(*c23_info, c23_b_emlrt_marshallOut(0U), "mFileTimeLo",
                  "mFileTimeLo", 11);
  sf_mex_addfield(*c23_info, c23_b_emlrt_marshallOut(0U), "mFileTimeHi",
                  "mFileTimeHi", 11);
  sf_mex_assign(&c23_rhs11, sf_mex_createcellmatrix(0, 1), false);
  sf_mex_assign(&c23_lhs11, sf_mex_createcellmatrix(0, 1), false);
  sf_mex_addfield(*c23_info, sf_mex_duplicatearraysafe(&c23_rhs11), "rhs", "rhs",
                  11);
  sf_mex_addfield(*c23_info, sf_mex_duplicatearraysafe(&c23_lhs11), "lhs", "lhs",
                  11);
  sf_mex_addfield(*c23_info, c23_emlrt_marshallOut(
    "[ILXE]$matlabroot$/toolbox/eml/lib/matlab/ops/colon.m"), "context",
                  "context", 12);
  sf_mex_addfield(*c23_info, c23_emlrt_marshallOut("colon"), "name", "name", 12);
  sf_mex_addfield(*c23_info, c23_emlrt_marshallOut("int8"), "dominantType",
                  "dominantType", 12);
  sf_mex_addfield(*c23_info, c23_emlrt_marshallOut(
    "[ILXE]$matlabroot$/toolbox/eml/lib/matlab/ops/colon.m"), "resolved",
                  "resolved", 12);
  sf_mex_addfield(*c23_info, c23_b_emlrt_marshallOut(1378299588U), "fileTimeLo",
                  "fileTimeLo", 12);
  sf_mex_addfield(*c23_info, c23_b_emlrt_marshallOut(0U), "fileTimeHi",
                  "fileTimeHi", 12);
  sf_mex_addfield(*c23_info, c23_b_emlrt_marshallOut(0U), "mFileTimeLo",
                  "mFileTimeLo", 12);
  sf_mex_addfield(*c23_info, c23_b_emlrt_marshallOut(0U), "mFileTimeHi",
                  "mFileTimeHi", 12);
  sf_mex_assign(&c23_rhs12, sf_mex_createcellmatrix(0, 1), false);
  sf_mex_assign(&c23_lhs12, sf_mex_createcellmatrix(0, 1), false);
  sf_mex_addfield(*c23_info, sf_mex_duplicatearraysafe(&c23_rhs12), "rhs", "rhs",
                  12);
  sf_mex_addfield(*c23_info, sf_mex_duplicatearraysafe(&c23_lhs12), "lhs", "lhs",
                  12);
  sf_mex_addfield(*c23_info, c23_emlrt_marshallOut(
    "[ILXE]$matlabroot$/toolbox/eml/lib/matlab/ops/colon.m"), "context",
                  "context", 13);
  sf_mex_addfield(*c23_info, c23_emlrt_marshallOut(
    "coder.internal.isBuiltInNumeric"), "name", "name", 13);
  sf_mex_addfield(*c23_info, c23_emlrt_marshallOut("double"), "dominantType",
                  "dominantType", 13);
  sf_mex_addfield(*c23_info, c23_emlrt_marshallOut(
    "[ILXE]$matlabroot$/toolbox/shared/coder/coder/+coder/+internal/isBuiltInNumeric.m"),
                  "resolved", "resolved", 13);
  sf_mex_addfield(*c23_info, c23_b_emlrt_marshallOut(1395935456U), "fileTimeLo",
                  "fileTimeLo", 13);
  sf_mex_addfield(*c23_info, c23_b_emlrt_marshallOut(0U), "fileTimeHi",
                  "fileTimeHi", 13);
  sf_mex_addfield(*c23_info, c23_b_emlrt_marshallOut(0U), "mFileTimeLo",
                  "mFileTimeLo", 13);
  sf_mex_addfield(*c23_info, c23_b_emlrt_marshallOut(0U), "mFileTimeHi",
                  "mFileTimeHi", 13);
  sf_mex_assign(&c23_rhs13, sf_mex_createcellmatrix(0, 1), false);
  sf_mex_assign(&c23_lhs13, sf_mex_createcellmatrix(0, 1), false);
  sf_mex_addfield(*c23_info, sf_mex_duplicatearraysafe(&c23_rhs13), "rhs", "rhs",
                  13);
  sf_mex_addfield(*c23_info, sf_mex_duplicatearraysafe(&c23_lhs13), "lhs", "lhs",
                  13);
  sf_mex_addfield(*c23_info, c23_emlrt_marshallOut(
    "[ILXE]$matlabroot$/toolbox/eml/lib/matlab/ops/colon.m"), "context",
                  "context", 14);
  sf_mex_addfield(*c23_info, c23_emlrt_marshallOut(
    "coder.internal.isBuiltInNumeric"), "name", "name", 14);
  sf_mex_addfield(*c23_info, c23_emlrt_marshallOut("int8"), "dominantType",
                  "dominantType", 14);
  sf_mex_addfield(*c23_info, c23_emlrt_marshallOut(
    "[ILXE]$matlabroot$/toolbox/shared/coder/coder/+coder/+internal/isBuiltInNumeric.m"),
                  "resolved", "resolved", 14);
  sf_mex_addfield(*c23_info, c23_b_emlrt_marshallOut(1395935456U), "fileTimeLo",
                  "fileTimeLo", 14);
  sf_mex_addfield(*c23_info, c23_b_emlrt_marshallOut(0U), "fileTimeHi",
                  "fileTimeHi", 14);
  sf_mex_addfield(*c23_info, c23_b_emlrt_marshallOut(0U), "mFileTimeLo",
                  "mFileTimeLo", 14);
  sf_mex_addfield(*c23_info, c23_b_emlrt_marshallOut(0U), "mFileTimeHi",
                  "mFileTimeHi", 14);
  sf_mex_assign(&c23_rhs14, sf_mex_createcellmatrix(0, 1), false);
  sf_mex_assign(&c23_lhs14, sf_mex_createcellmatrix(0, 1), false);
  sf_mex_addfield(*c23_info, sf_mex_duplicatearraysafe(&c23_rhs14), "rhs", "rhs",
                  14);
  sf_mex_addfield(*c23_info, sf_mex_duplicatearraysafe(&c23_lhs14), "lhs", "lhs",
                  14);
  sf_mex_addfield(*c23_info, c23_emlrt_marshallOut(
    "[ILXE]$matlabroot$/toolbox/eml/lib/matlab/ops/colon.m"), "context",
                  "context", 15);
  sf_mex_addfield(*c23_info, c23_emlrt_marshallOut("floor"), "name", "name", 15);
  sf_mex_addfield(*c23_info, c23_emlrt_marshallOut("double"), "dominantType",
                  "dominantType", 15);
  sf_mex_addfield(*c23_info, c23_emlrt_marshallOut(
    "[ILXE]$matlabroot$/toolbox/eml/lib/matlab/elfun/floor.m"), "resolved",
                  "resolved", 15);
  sf_mex_addfield(*c23_info, c23_b_emlrt_marshallOut(1363717454U), "fileTimeLo",
                  "fileTimeLo", 15);
  sf_mex_addfield(*c23_info, c23_b_emlrt_marshallOut(0U), "fileTimeHi",
                  "fileTimeHi", 15);
  sf_mex_addfield(*c23_info, c23_b_emlrt_marshallOut(0U), "mFileTimeLo",
                  "mFileTimeLo", 15);
  sf_mex_addfield(*c23_info, c23_b_emlrt_marshallOut(0U), "mFileTimeHi",
                  "mFileTimeHi", 15);
  sf_mex_assign(&c23_rhs15, sf_mex_createcellmatrix(0, 1), false);
  sf_mex_assign(&c23_lhs15, sf_mex_createcellmatrix(0, 1), false);
  sf_mex_addfield(*c23_info, sf_mex_duplicatearraysafe(&c23_rhs15), "rhs", "rhs",
                  15);
  sf_mex_addfield(*c23_info, sf_mex_duplicatearraysafe(&c23_lhs15), "lhs", "lhs",
                  15);
  sf_mex_addfield(*c23_info, c23_emlrt_marshallOut(
    "[ILXE]$matlabroot$/toolbox/eml/lib/matlab/elfun/floor.m"), "context",
                  "context", 16);
  sf_mex_addfield(*c23_info, c23_emlrt_marshallOut(
    "coder.internal.isBuiltInNumeric"), "name", "name", 16);
  sf_mex_addfield(*c23_info, c23_emlrt_marshallOut("double"), "dominantType",
                  "dominantType", 16);
  sf_mex_addfield(*c23_info, c23_emlrt_marshallOut(
    "[ILXE]$matlabroot$/toolbox/shared/coder/coder/+coder/+internal/isBuiltInNumeric.m"),
                  "resolved", "resolved", 16);
  sf_mex_addfield(*c23_info, c23_b_emlrt_marshallOut(1395935456U), "fileTimeLo",
                  "fileTimeLo", 16);
  sf_mex_addfield(*c23_info, c23_b_emlrt_marshallOut(0U), "fileTimeHi",
                  "fileTimeHi", 16);
  sf_mex_addfield(*c23_info, c23_b_emlrt_marshallOut(0U), "mFileTimeLo",
                  "mFileTimeLo", 16);
  sf_mex_addfield(*c23_info, c23_b_emlrt_marshallOut(0U), "mFileTimeHi",
                  "mFileTimeHi", 16);
  sf_mex_assign(&c23_rhs16, sf_mex_createcellmatrix(0, 1), false);
  sf_mex_assign(&c23_lhs16, sf_mex_createcellmatrix(0, 1), false);
  sf_mex_addfield(*c23_info, sf_mex_duplicatearraysafe(&c23_rhs16), "rhs", "rhs",
                  16);
  sf_mex_addfield(*c23_info, sf_mex_duplicatearraysafe(&c23_lhs16), "lhs", "lhs",
                  16);
  sf_mex_addfield(*c23_info, c23_emlrt_marshallOut(
    "[ILXE]$matlabroot$/toolbox/eml/lib/matlab/elfun/floor.m"), "context",
                  "context", 17);
  sf_mex_addfield(*c23_info, c23_emlrt_marshallOut("eml_scalar_floor"), "name",
                  "name", 17);
  sf_mex_addfield(*c23_info, c23_emlrt_marshallOut("double"), "dominantType",
                  "dominantType", 17);
  sf_mex_addfield(*c23_info, c23_emlrt_marshallOut(
    "[ILXE]$matlabroot$/toolbox/eml/lib/matlab/elfun/eml_scalar_floor.m"),
                  "resolved", "resolved", 17);
  sf_mex_addfield(*c23_info, c23_b_emlrt_marshallOut(1286822326U), "fileTimeLo",
                  "fileTimeLo", 17);
  sf_mex_addfield(*c23_info, c23_b_emlrt_marshallOut(0U), "fileTimeHi",
                  "fileTimeHi", 17);
  sf_mex_addfield(*c23_info, c23_b_emlrt_marshallOut(0U), "mFileTimeLo",
                  "mFileTimeLo", 17);
  sf_mex_addfield(*c23_info, c23_b_emlrt_marshallOut(0U), "mFileTimeHi",
                  "mFileTimeHi", 17);
  sf_mex_assign(&c23_rhs17, sf_mex_createcellmatrix(0, 1), false);
  sf_mex_assign(&c23_lhs17, sf_mex_createcellmatrix(0, 1), false);
  sf_mex_addfield(*c23_info, sf_mex_duplicatearraysafe(&c23_rhs17), "rhs", "rhs",
                  17);
  sf_mex_addfield(*c23_info, sf_mex_duplicatearraysafe(&c23_lhs17), "lhs", "lhs",
                  17);
  sf_mex_addfield(*c23_info, c23_emlrt_marshallOut(
    "[ILXE]$matlabroot$/toolbox/eml/lib/matlab/ops/colon.m!checkrange"),
                  "context", "context", 18);
  sf_mex_addfield(*c23_info, c23_emlrt_marshallOut("intmin"), "name", "name", 18);
  sf_mex_addfield(*c23_info, c23_emlrt_marshallOut("char"), "dominantType",
                  "dominantType", 18);
  sf_mex_addfield(*c23_info, c23_emlrt_marshallOut(
    "[ILXE]$matlabroot$/toolbox/eml/lib/matlab/elmat/intmin.m"), "resolved",
                  "resolved", 18);
  sf_mex_addfield(*c23_info, c23_b_emlrt_marshallOut(1362265482U), "fileTimeLo",
                  "fileTimeLo", 18);
  sf_mex_addfield(*c23_info, c23_b_emlrt_marshallOut(0U), "fileTimeHi",
                  "fileTimeHi", 18);
  sf_mex_addfield(*c23_info, c23_b_emlrt_marshallOut(0U), "mFileTimeLo",
                  "mFileTimeLo", 18);
  sf_mex_addfield(*c23_info, c23_b_emlrt_marshallOut(0U), "mFileTimeHi",
                  "mFileTimeHi", 18);
  sf_mex_assign(&c23_rhs18, sf_mex_createcellmatrix(0, 1), false);
  sf_mex_assign(&c23_lhs18, sf_mex_createcellmatrix(0, 1), false);
  sf_mex_addfield(*c23_info, sf_mex_duplicatearraysafe(&c23_rhs18), "rhs", "rhs",
                  18);
  sf_mex_addfield(*c23_info, sf_mex_duplicatearraysafe(&c23_lhs18), "lhs", "lhs",
                  18);
  sf_mex_addfield(*c23_info, c23_emlrt_marshallOut(
    "[ILXE]$matlabroot$/toolbox/eml/lib/matlab/elmat/intmin.m"), "context",
                  "context", 19);
  sf_mex_addfield(*c23_info, c23_emlrt_marshallOut("eml_switch_helper"), "name",
                  "name", 19);
  sf_mex_addfield(*c23_info, c23_emlrt_marshallOut(""), "dominantType",
                  "dominantType", 19);
  sf_mex_addfield(*c23_info, c23_emlrt_marshallOut(
    "[ILXE]$matlabroot$/toolbox/eml/lib/matlab/eml/eml_switch_helper.m"),
                  "resolved", "resolved", 19);
  sf_mex_addfield(*c23_info, c23_b_emlrt_marshallOut(1393334458U), "fileTimeLo",
                  "fileTimeLo", 19);
  sf_mex_addfield(*c23_info, c23_b_emlrt_marshallOut(0U), "fileTimeHi",
                  "fileTimeHi", 19);
  sf_mex_addfield(*c23_info, c23_b_emlrt_marshallOut(0U), "mFileTimeLo",
                  "mFileTimeLo", 19);
  sf_mex_addfield(*c23_info, c23_b_emlrt_marshallOut(0U), "mFileTimeHi",
                  "mFileTimeHi", 19);
  sf_mex_assign(&c23_rhs19, sf_mex_createcellmatrix(0, 1), false);
  sf_mex_assign(&c23_lhs19, sf_mex_createcellmatrix(0, 1), false);
  sf_mex_addfield(*c23_info, sf_mex_duplicatearraysafe(&c23_rhs19), "rhs", "rhs",
                  19);
  sf_mex_addfield(*c23_info, sf_mex_duplicatearraysafe(&c23_lhs19), "lhs", "lhs",
                  19);
  sf_mex_addfield(*c23_info, c23_emlrt_marshallOut(
    "[ILXE]$matlabroot$/toolbox/eml/lib/matlab/ops/colon.m!checkrange"),
                  "context", "context", 20);
  sf_mex_addfield(*c23_info, c23_emlrt_marshallOut("intmax"), "name", "name", 20);
  sf_mex_addfield(*c23_info, c23_emlrt_marshallOut("char"), "dominantType",
                  "dominantType", 20);
  sf_mex_addfield(*c23_info, c23_emlrt_marshallOut(
    "[ILXE]$matlabroot$/toolbox/eml/lib/matlab/elmat/intmax.m"), "resolved",
                  "resolved", 20);
  sf_mex_addfield(*c23_info, c23_b_emlrt_marshallOut(1362265482U), "fileTimeLo",
                  "fileTimeLo", 20);
  sf_mex_addfield(*c23_info, c23_b_emlrt_marshallOut(0U), "fileTimeHi",
                  "fileTimeHi", 20);
  sf_mex_addfield(*c23_info, c23_b_emlrt_marshallOut(0U), "mFileTimeLo",
                  "mFileTimeLo", 20);
  sf_mex_addfield(*c23_info, c23_b_emlrt_marshallOut(0U), "mFileTimeHi",
                  "mFileTimeHi", 20);
  sf_mex_assign(&c23_rhs20, sf_mex_createcellmatrix(0, 1), false);
  sf_mex_assign(&c23_lhs20, sf_mex_createcellmatrix(0, 1), false);
  sf_mex_addfield(*c23_info, sf_mex_duplicatearraysafe(&c23_rhs20), "rhs", "rhs",
                  20);
  sf_mex_addfield(*c23_info, sf_mex_duplicatearraysafe(&c23_lhs20), "lhs", "lhs",
                  20);
  sf_mex_addfield(*c23_info, c23_emlrt_marshallOut(
    "[ILXE]$matlabroot$/toolbox/eml/lib/matlab/ops/colon.m!eml_integer_colon_dispatcher"),
                  "context", "context", 21);
  sf_mex_addfield(*c23_info, c23_emlrt_marshallOut("intmin"), "name", "name", 21);
  sf_mex_addfield(*c23_info, c23_emlrt_marshallOut("char"), "dominantType",
                  "dominantType", 21);
  sf_mex_addfield(*c23_info, c23_emlrt_marshallOut(
    "[ILXE]$matlabroot$/toolbox/eml/lib/matlab/elmat/intmin.m"), "resolved",
                  "resolved", 21);
  sf_mex_addfield(*c23_info, c23_b_emlrt_marshallOut(1362265482U), "fileTimeLo",
                  "fileTimeLo", 21);
  sf_mex_addfield(*c23_info, c23_b_emlrt_marshallOut(0U), "fileTimeHi",
                  "fileTimeHi", 21);
  sf_mex_addfield(*c23_info, c23_b_emlrt_marshallOut(0U), "mFileTimeLo",
                  "mFileTimeLo", 21);
  sf_mex_addfield(*c23_info, c23_b_emlrt_marshallOut(0U), "mFileTimeHi",
                  "mFileTimeHi", 21);
  sf_mex_assign(&c23_rhs21, sf_mex_createcellmatrix(0, 1), false);
  sf_mex_assign(&c23_lhs21, sf_mex_createcellmatrix(0, 1), false);
  sf_mex_addfield(*c23_info, sf_mex_duplicatearraysafe(&c23_rhs21), "rhs", "rhs",
                  21);
  sf_mex_addfield(*c23_info, sf_mex_duplicatearraysafe(&c23_lhs21), "lhs", "lhs",
                  21);
  sf_mex_addfield(*c23_info, c23_emlrt_marshallOut(
    "[ILXE]$matlabroot$/toolbox/eml/lib/matlab/ops/colon.m!eml_integer_colon_dispatcher"),
                  "context", "context", 22);
  sf_mex_addfield(*c23_info, c23_emlrt_marshallOut("intmax"), "name", "name", 22);
  sf_mex_addfield(*c23_info, c23_emlrt_marshallOut("char"), "dominantType",
                  "dominantType", 22);
  sf_mex_addfield(*c23_info, c23_emlrt_marshallOut(
    "[ILXE]$matlabroot$/toolbox/eml/lib/matlab/elmat/intmax.m"), "resolved",
                  "resolved", 22);
  sf_mex_addfield(*c23_info, c23_b_emlrt_marshallOut(1362265482U), "fileTimeLo",
                  "fileTimeLo", 22);
  sf_mex_addfield(*c23_info, c23_b_emlrt_marshallOut(0U), "fileTimeHi",
                  "fileTimeHi", 22);
  sf_mex_addfield(*c23_info, c23_b_emlrt_marshallOut(0U), "mFileTimeLo",
                  "mFileTimeLo", 22);
  sf_mex_addfield(*c23_info, c23_b_emlrt_marshallOut(0U), "mFileTimeHi",
                  "mFileTimeHi", 22);
  sf_mex_assign(&c23_rhs22, sf_mex_createcellmatrix(0, 1), false);
  sf_mex_assign(&c23_lhs22, sf_mex_createcellmatrix(0, 1), false);
  sf_mex_addfield(*c23_info, sf_mex_duplicatearraysafe(&c23_rhs22), "rhs", "rhs",
                  22);
  sf_mex_addfield(*c23_info, sf_mex_duplicatearraysafe(&c23_lhs22), "lhs", "lhs",
                  22);
  sf_mex_addfield(*c23_info, c23_emlrt_marshallOut(
    "[ILXE]$matlabroot$/toolbox/eml/lib/matlab/ops/colon.m!eml_integer_colon_dispatcher"),
                  "context", "context", 23);
  sf_mex_addfield(*c23_info, c23_emlrt_marshallOut("eml_isa_uint"), "name",
                  "name", 23);
  sf_mex_addfield(*c23_info, c23_emlrt_marshallOut("int8"), "dominantType",
                  "dominantType", 23);
  sf_mex_addfield(*c23_info, c23_emlrt_marshallOut(
    "[ILXE]$matlabroot$/toolbox/eml/lib/matlab/eml/eml_isa_uint.m"), "resolved",
                  "resolved", 23);
  sf_mex_addfield(*c23_info, c23_b_emlrt_marshallOut(1375984288U), "fileTimeLo",
                  "fileTimeLo", 23);
  sf_mex_addfield(*c23_info, c23_b_emlrt_marshallOut(0U), "fileTimeHi",
                  "fileTimeHi", 23);
  sf_mex_addfield(*c23_info, c23_b_emlrt_marshallOut(0U), "mFileTimeLo",
                  "mFileTimeLo", 23);
  sf_mex_addfield(*c23_info, c23_b_emlrt_marshallOut(0U), "mFileTimeHi",
                  "mFileTimeHi", 23);
  sf_mex_assign(&c23_rhs23, sf_mex_createcellmatrix(0, 1), false);
  sf_mex_assign(&c23_lhs23, sf_mex_createcellmatrix(0, 1), false);
  sf_mex_addfield(*c23_info, sf_mex_duplicatearraysafe(&c23_rhs23), "rhs", "rhs",
                  23);
  sf_mex_addfield(*c23_info, sf_mex_duplicatearraysafe(&c23_lhs23), "lhs", "lhs",
                  23);
  sf_mex_addfield(*c23_info, c23_emlrt_marshallOut(
    "[ILXE]$matlabroot$/toolbox/eml/lib/matlab/eml/eml_isa_uint.m"), "context",
                  "context", 24);
  sf_mex_addfield(*c23_info, c23_emlrt_marshallOut("coder.internal.isaUint"),
                  "name", "name", 24);
  sf_mex_addfield(*c23_info, c23_emlrt_marshallOut("int8"), "dominantType",
                  "dominantType", 24);
  sf_mex_addfield(*c23_info, c23_emlrt_marshallOut(
    "[ILXE]$matlabroot$/toolbox/coder/coder/+coder/+internal/isaUint.p"),
                  "resolved", "resolved", 24);
  sf_mex_addfield(*c23_info, c23_b_emlrt_marshallOut(1410811370U), "fileTimeLo",
                  "fileTimeLo", 24);
  sf_mex_addfield(*c23_info, c23_b_emlrt_marshallOut(0U), "fileTimeHi",
                  "fileTimeHi", 24);
  sf_mex_addfield(*c23_info, c23_b_emlrt_marshallOut(0U), "mFileTimeLo",
                  "mFileTimeLo", 24);
  sf_mex_addfield(*c23_info, c23_b_emlrt_marshallOut(0U), "mFileTimeHi",
                  "mFileTimeHi", 24);
  sf_mex_assign(&c23_rhs24, sf_mex_createcellmatrix(0, 1), false);
  sf_mex_assign(&c23_lhs24, sf_mex_createcellmatrix(0, 1), false);
  sf_mex_addfield(*c23_info, sf_mex_duplicatearraysafe(&c23_rhs24), "rhs", "rhs",
                  24);
  sf_mex_addfield(*c23_info, sf_mex_duplicatearraysafe(&c23_lhs24), "lhs", "lhs",
                  24);
  sf_mex_addfield(*c23_info, c23_emlrt_marshallOut(
    "[ILXE]$matlabroot$/toolbox/eml/lib/matlab/ops/colon.m!integer_colon_length_nonnegd"),
                  "context", "context", 25);
  sf_mex_addfield(*c23_info, c23_emlrt_marshallOut("eml_unsigned_class"), "name",
                  "name", 25);
  sf_mex_addfield(*c23_info, c23_emlrt_marshallOut("char"), "dominantType",
                  "dominantType", 25);
  sf_mex_addfield(*c23_info, c23_emlrt_marshallOut(
    "[ILXE]$matlabroot$/toolbox/eml/lib/matlab/eml/eml_unsigned_class.m"),
                  "resolved", "resolved", 25);
  sf_mex_addfield(*c23_info, c23_b_emlrt_marshallOut(1375984288U), "fileTimeLo",
                  "fileTimeLo", 25);
  sf_mex_addfield(*c23_info, c23_b_emlrt_marshallOut(0U), "fileTimeHi",
                  "fileTimeHi", 25);
  sf_mex_addfield(*c23_info, c23_b_emlrt_marshallOut(0U), "mFileTimeLo",
                  "mFileTimeLo", 25);
  sf_mex_addfield(*c23_info, c23_b_emlrt_marshallOut(0U), "mFileTimeHi",
                  "mFileTimeHi", 25);
  sf_mex_assign(&c23_rhs25, sf_mex_createcellmatrix(0, 1), false);
  sf_mex_assign(&c23_lhs25, sf_mex_createcellmatrix(0, 1), false);
  sf_mex_addfield(*c23_info, sf_mex_duplicatearraysafe(&c23_rhs25), "rhs", "rhs",
                  25);
  sf_mex_addfield(*c23_info, sf_mex_duplicatearraysafe(&c23_lhs25), "lhs", "lhs",
                  25);
  sf_mex_addfield(*c23_info, c23_emlrt_marshallOut(
    "[ILXE]$matlabroot$/toolbox/eml/lib/matlab/eml/eml_unsigned_class.m"),
                  "context", "context", 26);
  sf_mex_addfield(*c23_info, c23_emlrt_marshallOut(
    "coder.internal.unsignedClass"), "name", "name", 26);
  sf_mex_addfield(*c23_info, c23_emlrt_marshallOut("char"), "dominantType",
                  "dominantType", 26);
  sf_mex_addfield(*c23_info, c23_emlrt_marshallOut(
    "[ILXE]$matlabroot$/toolbox/coder/coder/+coder/+internal/unsignedClass.p"),
                  "resolved", "resolved", 26);
  sf_mex_addfield(*c23_info, c23_b_emlrt_marshallOut(1410811370U), "fileTimeLo",
                  "fileTimeLo", 26);
  sf_mex_addfield(*c23_info, c23_b_emlrt_marshallOut(0U), "fileTimeHi",
                  "fileTimeHi", 26);
  sf_mex_addfield(*c23_info, c23_b_emlrt_marshallOut(0U), "mFileTimeLo",
                  "mFileTimeLo", 26);
  sf_mex_addfield(*c23_info, c23_b_emlrt_marshallOut(0U), "mFileTimeHi",
                  "mFileTimeHi", 26);
  sf_mex_assign(&c23_rhs26, sf_mex_createcellmatrix(0, 1), false);
  sf_mex_assign(&c23_lhs26, sf_mex_createcellmatrix(0, 1), false);
  sf_mex_addfield(*c23_info, sf_mex_duplicatearraysafe(&c23_rhs26), "rhs", "rhs",
                  26);
  sf_mex_addfield(*c23_info, sf_mex_duplicatearraysafe(&c23_lhs26), "lhs", "lhs",
                  26);
  sf_mex_addfield(*c23_info, c23_emlrt_marshallOut(
    "[ILXE]$matlabroot$/toolbox/coder/coder/+coder/+internal/unsignedClass.p"),
                  "context", "context", 27);
  sf_mex_addfield(*c23_info, c23_emlrt_marshallOut("eml_switch_helper"), "name",
                  "name", 27);
  sf_mex_addfield(*c23_info, c23_emlrt_marshallOut(""), "dominantType",
                  "dominantType", 27);
  sf_mex_addfield(*c23_info, c23_emlrt_marshallOut(
    "[ILXE]$matlabroot$/toolbox/eml/lib/matlab/eml/eml_switch_helper.m"),
                  "resolved", "resolved", 27);
  sf_mex_addfield(*c23_info, c23_b_emlrt_marshallOut(1393334458U), "fileTimeLo",
                  "fileTimeLo", 27);
  sf_mex_addfield(*c23_info, c23_b_emlrt_marshallOut(0U), "fileTimeHi",
                  "fileTimeHi", 27);
  sf_mex_addfield(*c23_info, c23_b_emlrt_marshallOut(0U), "mFileTimeLo",
                  "mFileTimeLo", 27);
  sf_mex_addfield(*c23_info, c23_b_emlrt_marshallOut(0U), "mFileTimeHi",
                  "mFileTimeHi", 27);
  sf_mex_assign(&c23_rhs27, sf_mex_createcellmatrix(0, 1), false);
  sf_mex_assign(&c23_lhs27, sf_mex_createcellmatrix(0, 1), false);
  sf_mex_addfield(*c23_info, sf_mex_duplicatearraysafe(&c23_rhs27), "rhs", "rhs",
                  27);
  sf_mex_addfield(*c23_info, sf_mex_duplicatearraysafe(&c23_lhs27), "lhs", "lhs",
                  27);
  sf_mex_addfield(*c23_info, c23_emlrt_marshallOut(
    "[ILXE]$matlabroot$/toolbox/eml/lib/matlab/ops/colon.m!integer_colon_length_nonnegd"),
                  "context", "context", 28);
  sf_mex_addfield(*c23_info, c23_emlrt_marshallOut("eml_index_class"), "name",
                  "name", 28);
  sf_mex_addfield(*c23_info, c23_emlrt_marshallOut(""), "dominantType",
                  "dominantType", 28);
  sf_mex_addfield(*c23_info, c23_emlrt_marshallOut(
    "[ILXE]$matlabroot$/toolbox/eml/lib/matlab/eml/eml_index_class.m"),
                  "resolved", "resolved", 28);
  sf_mex_addfield(*c23_info, c23_b_emlrt_marshallOut(1323174178U), "fileTimeLo",
                  "fileTimeLo", 28);
  sf_mex_addfield(*c23_info, c23_b_emlrt_marshallOut(0U), "fileTimeHi",
                  "fileTimeHi", 28);
  sf_mex_addfield(*c23_info, c23_b_emlrt_marshallOut(0U), "mFileTimeLo",
                  "mFileTimeLo", 28);
  sf_mex_addfield(*c23_info, c23_b_emlrt_marshallOut(0U), "mFileTimeHi",
                  "mFileTimeHi", 28);
  sf_mex_assign(&c23_rhs28, sf_mex_createcellmatrix(0, 1), false);
  sf_mex_assign(&c23_lhs28, sf_mex_createcellmatrix(0, 1), false);
  sf_mex_addfield(*c23_info, sf_mex_duplicatearraysafe(&c23_rhs28), "rhs", "rhs",
                  28);
  sf_mex_addfield(*c23_info, sf_mex_duplicatearraysafe(&c23_lhs28), "lhs", "lhs",
                  28);
  sf_mex_addfield(*c23_info, c23_emlrt_marshallOut(
    "[ILXE]$matlabroot$/toolbox/eml/lib/matlab/ops/colon.m!integer_colon_length_nonnegd"),
                  "context", "context", 29);
  sf_mex_addfield(*c23_info, c23_emlrt_marshallOut("intmax"), "name", "name", 29);
  sf_mex_addfield(*c23_info, c23_emlrt_marshallOut("char"), "dominantType",
                  "dominantType", 29);
  sf_mex_addfield(*c23_info, c23_emlrt_marshallOut(
    "[ILXE]$matlabroot$/toolbox/eml/lib/matlab/elmat/intmax.m"), "resolved",
                  "resolved", 29);
  sf_mex_addfield(*c23_info, c23_b_emlrt_marshallOut(1362265482U), "fileTimeLo",
                  "fileTimeLo", 29);
  sf_mex_addfield(*c23_info, c23_b_emlrt_marshallOut(0U), "fileTimeHi",
                  "fileTimeHi", 29);
  sf_mex_addfield(*c23_info, c23_b_emlrt_marshallOut(0U), "mFileTimeLo",
                  "mFileTimeLo", 29);
  sf_mex_addfield(*c23_info, c23_b_emlrt_marshallOut(0U), "mFileTimeHi",
                  "mFileTimeHi", 29);
  sf_mex_assign(&c23_rhs29, sf_mex_createcellmatrix(0, 1), false);
  sf_mex_assign(&c23_lhs29, sf_mex_createcellmatrix(0, 1), false);
  sf_mex_addfield(*c23_info, sf_mex_duplicatearraysafe(&c23_rhs29), "rhs", "rhs",
                  29);
  sf_mex_addfield(*c23_info, sf_mex_duplicatearraysafe(&c23_lhs29), "lhs", "lhs",
                  29);
  sf_mex_addfield(*c23_info, c23_emlrt_marshallOut(
    "[ILXE]$matlabroot$/toolbox/eml/lib/matlab/ops/colon.m!integer_colon_length_nonnegd"),
                  "context", "context", 30);
  sf_mex_addfield(*c23_info, c23_emlrt_marshallOut("eml_isa_uint"), "name",
                  "name", 30);
  sf_mex_addfield(*c23_info, c23_emlrt_marshallOut("int8"), "dominantType",
                  "dominantType", 30);
  sf_mex_addfield(*c23_info, c23_emlrt_marshallOut(
    "[ILXE]$matlabroot$/toolbox/eml/lib/matlab/eml/eml_isa_uint.m"), "resolved",
                  "resolved", 30);
  sf_mex_addfield(*c23_info, c23_b_emlrt_marshallOut(1375984288U), "fileTimeLo",
                  "fileTimeLo", 30);
  sf_mex_addfield(*c23_info, c23_b_emlrt_marshallOut(0U), "fileTimeHi",
                  "fileTimeHi", 30);
  sf_mex_addfield(*c23_info, c23_b_emlrt_marshallOut(0U), "mFileTimeLo",
                  "mFileTimeLo", 30);
  sf_mex_addfield(*c23_info, c23_b_emlrt_marshallOut(0U), "mFileTimeHi",
                  "mFileTimeHi", 30);
  sf_mex_assign(&c23_rhs30, sf_mex_createcellmatrix(0, 1), false);
  sf_mex_assign(&c23_lhs30, sf_mex_createcellmatrix(0, 1), false);
  sf_mex_addfield(*c23_info, sf_mex_duplicatearraysafe(&c23_rhs30), "rhs", "rhs",
                  30);
  sf_mex_addfield(*c23_info, sf_mex_duplicatearraysafe(&c23_lhs30), "lhs", "lhs",
                  30);
  sf_mex_addfield(*c23_info, c23_emlrt_marshallOut(
    "[ILXE]$matlabroot$/toolbox/eml/lib/matlab/ops/colon.m!integer_colon_length_nonnegd"),
                  "context", "context", 31);
  sf_mex_addfield(*c23_info, c23_emlrt_marshallOut("eml_index_plus"), "name",
                  "name", 31);
  sf_mex_addfield(*c23_info, c23_emlrt_marshallOut("double"), "dominantType",
                  "dominantType", 31);
  sf_mex_addfield(*c23_info, c23_emlrt_marshallOut(
    "[ILXE]$matlabroot$/toolbox/eml/lib/matlab/eml/eml_index_plus.m"),
                  "resolved", "resolved", 31);
  sf_mex_addfield(*c23_info, c23_b_emlrt_marshallOut(1372586016U), "fileTimeLo",
                  "fileTimeLo", 31);
  sf_mex_addfield(*c23_info, c23_b_emlrt_marshallOut(0U), "fileTimeHi",
                  "fileTimeHi", 31);
  sf_mex_addfield(*c23_info, c23_b_emlrt_marshallOut(0U), "mFileTimeLo",
                  "mFileTimeLo", 31);
  sf_mex_addfield(*c23_info, c23_b_emlrt_marshallOut(0U), "mFileTimeHi",
                  "mFileTimeHi", 31);
  sf_mex_assign(&c23_rhs31, sf_mex_createcellmatrix(0, 1), false);
  sf_mex_assign(&c23_lhs31, sf_mex_createcellmatrix(0, 1), false);
  sf_mex_addfield(*c23_info, sf_mex_duplicatearraysafe(&c23_rhs31), "rhs", "rhs",
                  31);
  sf_mex_addfield(*c23_info, sf_mex_duplicatearraysafe(&c23_lhs31), "lhs", "lhs",
                  31);
  sf_mex_addfield(*c23_info, c23_emlrt_marshallOut(
    "[ILXE]$matlabroot$/toolbox/eml/lib/matlab/eml/eml_index_plus.m"), "context",
                  "context", 32);
  sf_mex_addfield(*c23_info, c23_emlrt_marshallOut("coder.internal.indexPlus"),
                  "name", "name", 32);
  sf_mex_addfield(*c23_info, c23_emlrt_marshallOut("double"), "dominantType",
                  "dominantType", 32);
  sf_mex_addfield(*c23_info, c23_emlrt_marshallOut(
    "[ILXE]$matlabroot$/toolbox/shared/coder/coder/+coder/+internal/indexPlus.m"),
                  "resolved", "resolved", 32);
  sf_mex_addfield(*c23_info, c23_b_emlrt_marshallOut(1372586760U), "fileTimeLo",
                  "fileTimeLo", 32);
  sf_mex_addfield(*c23_info, c23_b_emlrt_marshallOut(0U), "fileTimeHi",
                  "fileTimeHi", 32);
  sf_mex_addfield(*c23_info, c23_b_emlrt_marshallOut(0U), "mFileTimeLo",
                  "mFileTimeLo", 32);
  sf_mex_addfield(*c23_info, c23_b_emlrt_marshallOut(0U), "mFileTimeHi",
                  "mFileTimeHi", 32);
  sf_mex_assign(&c23_rhs32, sf_mex_createcellmatrix(0, 1), false);
  sf_mex_assign(&c23_lhs32, sf_mex_createcellmatrix(0, 1), false);
  sf_mex_addfield(*c23_info, sf_mex_duplicatearraysafe(&c23_rhs32), "rhs", "rhs",
                  32);
  sf_mex_addfield(*c23_info, sf_mex_duplicatearraysafe(&c23_lhs32), "lhs", "lhs",
                  32);
  sf_mex_addfield(*c23_info, c23_emlrt_marshallOut(
    "[ILXE]$matlabroot$/toolbox/eml/lib/matlab/ops/colon.m!eml_signed_integer_colon"),
                  "context", "context", 33);
  sf_mex_addfield(*c23_info, c23_emlrt_marshallOut(
    "eml_int_forloop_overflow_check"), "name", "name", 33);
  sf_mex_addfield(*c23_info, c23_emlrt_marshallOut(""), "dominantType",
                  "dominantType", 33);
  sf_mex_addfield(*c23_info, c23_emlrt_marshallOut(
    "[ILXE]$matlabroot$/toolbox/eml/lib/matlab/eml/eml_int_forloop_overflow_check.m"),
                  "resolved", "resolved", 33);
  sf_mex_addfield(*c23_info, c23_b_emlrt_marshallOut(1397261022U), "fileTimeLo",
                  "fileTimeLo", 33);
  sf_mex_addfield(*c23_info, c23_b_emlrt_marshallOut(0U), "fileTimeHi",
                  "fileTimeHi", 33);
  sf_mex_addfield(*c23_info, c23_b_emlrt_marshallOut(0U), "mFileTimeLo",
                  "mFileTimeLo", 33);
  sf_mex_addfield(*c23_info, c23_b_emlrt_marshallOut(0U), "mFileTimeHi",
                  "mFileTimeHi", 33);
  sf_mex_assign(&c23_rhs33, sf_mex_createcellmatrix(0, 1), false);
  sf_mex_assign(&c23_lhs33, sf_mex_createcellmatrix(0, 1), false);
  sf_mex_addfield(*c23_info, sf_mex_duplicatearraysafe(&c23_rhs33), "rhs", "rhs",
                  33);
  sf_mex_addfield(*c23_info, sf_mex_duplicatearraysafe(&c23_lhs33), "lhs", "lhs",
                  33);
  sf_mex_addfield(*c23_info, c23_emlrt_marshallOut(
    "[ILXE]$matlabroot$/toolbox/eml/lib/matlab/eml/eml_int_forloop_overflow_check.m!eml_int_forloop_overflow_check_helper"),
                  "context", "context", 34);
  sf_mex_addfield(*c23_info, c23_emlrt_marshallOut("isfi"), "name", "name", 34);
  sf_mex_addfield(*c23_info, c23_emlrt_marshallOut("coder.internal.indexInt"),
                  "dominantType", "dominantType", 34);
  sf_mex_addfield(*c23_info, c23_emlrt_marshallOut(
    "[ILXE]$matlabroot$/toolbox/eml/lib/fixedpoint/isfi.m"), "resolved",
                  "resolved", 34);
  sf_mex_addfield(*c23_info, c23_b_emlrt_marshallOut(1346513958U), "fileTimeLo",
                  "fileTimeLo", 34);
  sf_mex_addfield(*c23_info, c23_b_emlrt_marshallOut(0U), "fileTimeHi",
                  "fileTimeHi", 34);
  sf_mex_addfield(*c23_info, c23_b_emlrt_marshallOut(0U), "mFileTimeLo",
                  "mFileTimeLo", 34);
  sf_mex_addfield(*c23_info, c23_b_emlrt_marshallOut(0U), "mFileTimeHi",
                  "mFileTimeHi", 34);
  sf_mex_assign(&c23_rhs34, sf_mex_createcellmatrix(0, 1), false);
  sf_mex_assign(&c23_lhs34, sf_mex_createcellmatrix(0, 1), false);
  sf_mex_addfield(*c23_info, sf_mex_duplicatearraysafe(&c23_rhs34), "rhs", "rhs",
                  34);
  sf_mex_addfield(*c23_info, sf_mex_duplicatearraysafe(&c23_lhs34), "lhs", "lhs",
                  34);
  sf_mex_addfield(*c23_info, c23_emlrt_marshallOut(
    "[ILXE]$matlabroot$/toolbox/eml/lib/fixedpoint/isfi.m"), "context",
                  "context", 35);
  sf_mex_addfield(*c23_info, c23_emlrt_marshallOut("isnumerictype"), "name",
                  "name", 35);
  sf_mex_addfield(*c23_info, c23_emlrt_marshallOut("char"), "dominantType",
                  "dominantType", 35);
  sf_mex_addfield(*c23_info, c23_emlrt_marshallOut(
    "[ILXE]$matlabroot$/toolbox/eml/lib/fixedpoint/isnumerictype.m"), "resolved",
                  "resolved", 35);
  sf_mex_addfield(*c23_info, c23_b_emlrt_marshallOut(1398879198U), "fileTimeLo",
                  "fileTimeLo", 35);
  sf_mex_addfield(*c23_info, c23_b_emlrt_marshallOut(0U), "fileTimeHi",
                  "fileTimeHi", 35);
  sf_mex_addfield(*c23_info, c23_b_emlrt_marshallOut(0U), "mFileTimeLo",
                  "mFileTimeLo", 35);
  sf_mex_addfield(*c23_info, c23_b_emlrt_marshallOut(0U), "mFileTimeHi",
                  "mFileTimeHi", 35);
  sf_mex_assign(&c23_rhs35, sf_mex_createcellmatrix(0, 1), false);
  sf_mex_assign(&c23_lhs35, sf_mex_createcellmatrix(0, 1), false);
  sf_mex_addfield(*c23_info, sf_mex_duplicatearraysafe(&c23_rhs35), "rhs", "rhs",
                  35);
  sf_mex_addfield(*c23_info, sf_mex_duplicatearraysafe(&c23_lhs35), "lhs", "lhs",
                  35);
  sf_mex_addfield(*c23_info, c23_emlrt_marshallOut(
    "[ILXE]$matlabroot$/toolbox/eml/lib/matlab/eml/eml_int_forloop_overflow_check.m!eml_int_forloop_overflow_check_helper"),
                  "context", "context", 36);
  sf_mex_addfield(*c23_info, c23_emlrt_marshallOut("intmax"), "name", "name", 36);
  sf_mex_addfield(*c23_info, c23_emlrt_marshallOut("char"), "dominantType",
                  "dominantType", 36);
  sf_mex_addfield(*c23_info, c23_emlrt_marshallOut(
    "[ILXE]$matlabroot$/toolbox/eml/lib/matlab/elmat/intmax.m"), "resolved",
                  "resolved", 36);
  sf_mex_addfield(*c23_info, c23_b_emlrt_marshallOut(1362265482U), "fileTimeLo",
                  "fileTimeLo", 36);
  sf_mex_addfield(*c23_info, c23_b_emlrt_marshallOut(0U), "fileTimeHi",
                  "fileTimeHi", 36);
  sf_mex_addfield(*c23_info, c23_b_emlrt_marshallOut(0U), "mFileTimeLo",
                  "mFileTimeLo", 36);
  sf_mex_addfield(*c23_info, c23_b_emlrt_marshallOut(0U), "mFileTimeHi",
                  "mFileTimeHi", 36);
  sf_mex_assign(&c23_rhs36, sf_mex_createcellmatrix(0, 1), false);
  sf_mex_assign(&c23_lhs36, sf_mex_createcellmatrix(0, 1), false);
  sf_mex_addfield(*c23_info, sf_mex_duplicatearraysafe(&c23_rhs36), "rhs", "rhs",
                  36);
  sf_mex_addfield(*c23_info, sf_mex_duplicatearraysafe(&c23_lhs36), "lhs", "lhs",
                  36);
  sf_mex_addfield(*c23_info, c23_emlrt_marshallOut(
    "[ILXE]$matlabroot$/toolbox/eml/lib/matlab/eml/eml_int_forloop_overflow_check.m!eml_int_forloop_overflow_check_helper"),
                  "context", "context", 37);
  sf_mex_addfield(*c23_info, c23_emlrt_marshallOut("intmin"), "name", "name", 37);
  sf_mex_addfield(*c23_info, c23_emlrt_marshallOut("char"), "dominantType",
                  "dominantType", 37);
  sf_mex_addfield(*c23_info, c23_emlrt_marshallOut(
    "[ILXE]$matlabroot$/toolbox/eml/lib/matlab/elmat/intmin.m"), "resolved",
                  "resolved", 37);
  sf_mex_addfield(*c23_info, c23_b_emlrt_marshallOut(1362265482U), "fileTimeLo",
                  "fileTimeLo", 37);
  sf_mex_addfield(*c23_info, c23_b_emlrt_marshallOut(0U), "fileTimeHi",
                  "fileTimeHi", 37);
  sf_mex_addfield(*c23_info, c23_b_emlrt_marshallOut(0U), "mFileTimeLo",
                  "mFileTimeLo", 37);
  sf_mex_addfield(*c23_info, c23_b_emlrt_marshallOut(0U), "mFileTimeHi",
                  "mFileTimeHi", 37);
  sf_mex_assign(&c23_rhs37, sf_mex_createcellmatrix(0, 1), false);
  sf_mex_assign(&c23_lhs37, sf_mex_createcellmatrix(0, 1), false);
  sf_mex_addfield(*c23_info, sf_mex_duplicatearraysafe(&c23_rhs37), "rhs", "rhs",
                  37);
  sf_mex_addfield(*c23_info, sf_mex_duplicatearraysafe(&c23_lhs37), "lhs", "lhs",
                  37);
  sf_mex_addfield(*c23_info, c23_emlrt_marshallOut(
    "[ILXE]$matlabroot$/toolbox/eml/lib/matlab/strfun/isstrprop.m!apply_property_predicate"),
                  "context", "context", 38);
  sf_mex_addfield(*c23_info, c23_emlrt_marshallOut("char"), "name", "name", 38);
  sf_mex_addfield(*c23_info, c23_emlrt_marshallOut("int8"), "dominantType",
                  "dominantType", 38);
  sf_mex_addfield(*c23_info, c23_emlrt_marshallOut(
    "[ILXE]$matlabroot$/toolbox/eml/lib/matlab/strfun/char.m"), "resolved",
                  "resolved", 38);
  sf_mex_addfield(*c23_info, c23_b_emlrt_marshallOut(1319733568U), "fileTimeLo",
                  "fileTimeLo", 38);
  sf_mex_addfield(*c23_info, c23_b_emlrt_marshallOut(0U), "fileTimeHi",
                  "fileTimeHi", 38);
  sf_mex_addfield(*c23_info, c23_b_emlrt_marshallOut(0U), "mFileTimeLo",
                  "mFileTimeLo", 38);
  sf_mex_addfield(*c23_info, c23_b_emlrt_marshallOut(0U), "mFileTimeHi",
                  "mFileTimeHi", 38);
  sf_mex_assign(&c23_rhs38, sf_mex_createcellmatrix(0, 1), false);
  sf_mex_assign(&c23_lhs38, sf_mex_createcellmatrix(0, 1), false);
  sf_mex_addfield(*c23_info, sf_mex_duplicatearraysafe(&c23_rhs38), "rhs", "rhs",
                  38);
  sf_mex_addfield(*c23_info, sf_mex_duplicatearraysafe(&c23_lhs38), "lhs", "lhs",
                  38);
  sf_mex_addfield(*c23_info, c23_emlrt_marshallOut(
    "[ILXE]$matlabroot$/toolbox/eml/lib/matlab/datatypes/typecast.m!bytes_per_element"),
                  "context", "context", 39);
  sf_mex_addfield(*c23_info, c23_emlrt_marshallOut("eml_switch_helper"), "name",
                  "name", 39);
  sf_mex_addfield(*c23_info, c23_emlrt_marshallOut(""), "dominantType",
                  "dominantType", 39);
  sf_mex_addfield(*c23_info, c23_emlrt_marshallOut(
    "[ILXE]$matlabroot$/toolbox/eml/lib/matlab/eml/eml_switch_helper.m"),
                  "resolved", "resolved", 39);
  sf_mex_addfield(*c23_info, c23_b_emlrt_marshallOut(1393334458U), "fileTimeLo",
                  "fileTimeLo", 39);
  sf_mex_addfield(*c23_info, c23_b_emlrt_marshallOut(0U), "fileTimeHi",
                  "fileTimeHi", 39);
  sf_mex_addfield(*c23_info, c23_b_emlrt_marshallOut(0U), "mFileTimeLo",
                  "mFileTimeLo", 39);
  sf_mex_addfield(*c23_info, c23_b_emlrt_marshallOut(0U), "mFileTimeHi",
                  "mFileTimeHi", 39);
  sf_mex_assign(&c23_rhs39, sf_mex_createcellmatrix(0, 1), false);
  sf_mex_assign(&c23_lhs39, sf_mex_createcellmatrix(0, 1), false);
  sf_mex_addfield(*c23_info, sf_mex_duplicatearraysafe(&c23_rhs39), "rhs", "rhs",
                  39);
  sf_mex_addfield(*c23_info, sf_mex_duplicatearraysafe(&c23_lhs39), "lhs", "lhs",
                  39);
  sf_mex_addfield(*c23_info, c23_emlrt_marshallOut(
    "[ILXE]$matlabroot$/toolbox/eml/lib/matlab/datatypes/typecast.m!bytes_per_element"),
                  "context", "context", 40);
  sf_mex_addfield(*c23_info, c23_emlrt_marshallOut("eml_int_nbits"), "name",
                  "name", 40);
  sf_mex_addfield(*c23_info, c23_emlrt_marshallOut("char"), "dominantType",
                  "dominantType", 40);
  sf_mex_addfield(*c23_info, c23_emlrt_marshallOut(
    "[ILXE]$matlabroot$/toolbox/eml/lib/matlab/eml/eml_int_nbits.m"), "resolved",
                  "resolved", 40);
  sf_mex_addfield(*c23_info, c23_b_emlrt_marshallOut(1323174178U), "fileTimeLo",
                  "fileTimeLo", 40);
  sf_mex_addfield(*c23_info, c23_b_emlrt_marshallOut(0U), "fileTimeHi",
                  "fileTimeHi", 40);
  sf_mex_addfield(*c23_info, c23_b_emlrt_marshallOut(0U), "mFileTimeLo",
                  "mFileTimeLo", 40);
  sf_mex_addfield(*c23_info, c23_b_emlrt_marshallOut(0U), "mFileTimeHi",
                  "mFileTimeHi", 40);
  sf_mex_assign(&c23_rhs40, sf_mex_createcellmatrix(0, 1), false);
  sf_mex_assign(&c23_lhs40, sf_mex_createcellmatrix(0, 1), false);
  sf_mex_addfield(*c23_info, sf_mex_duplicatearraysafe(&c23_rhs40), "rhs", "rhs",
                  40);
  sf_mex_addfield(*c23_info, sf_mex_duplicatearraysafe(&c23_lhs40), "lhs", "lhs",
                  40);
  sf_mex_addfield(*c23_info, c23_emlrt_marshallOut(
    "[ILXE]$matlabroot$/toolbox/eml/lib/matlab/eml/eml_int_nbits.m"), "context",
                  "context", 41);
  sf_mex_addfield(*c23_info, c23_emlrt_marshallOut("eml_switch_helper"), "name",
                  "name", 41);
  sf_mex_addfield(*c23_info, c23_emlrt_marshallOut(""), "dominantType",
                  "dominantType", 41);
  sf_mex_addfield(*c23_info, c23_emlrt_marshallOut(
    "[ILXE]$matlabroot$/toolbox/eml/lib/matlab/eml/eml_switch_helper.m"),
                  "resolved", "resolved", 41);
  sf_mex_addfield(*c23_info, c23_b_emlrt_marshallOut(1393334458U), "fileTimeLo",
                  "fileTimeLo", 41);
  sf_mex_addfield(*c23_info, c23_b_emlrt_marshallOut(0U), "fileTimeHi",
                  "fileTimeHi", 41);
  sf_mex_addfield(*c23_info, c23_b_emlrt_marshallOut(0U), "mFileTimeLo",
                  "mFileTimeLo", 41);
  sf_mex_addfield(*c23_info, c23_b_emlrt_marshallOut(0U), "mFileTimeHi",
                  "mFileTimeHi", 41);
  sf_mex_assign(&c23_rhs41, sf_mex_createcellmatrix(0, 1), false);
  sf_mex_assign(&c23_lhs41, sf_mex_createcellmatrix(0, 1), false);
  sf_mex_addfield(*c23_info, sf_mex_duplicatearraysafe(&c23_rhs41), "rhs", "rhs",
                  41);
  sf_mex_addfield(*c23_info, sf_mex_duplicatearraysafe(&c23_lhs41), "lhs", "lhs",
                  41);
  sf_mex_addfield(*c23_info, c23_emlrt_marshallOut(
    "[ILXE]$matlabroot$/toolbox/eml/lib/matlab/datatypes/typecast.m!bytes_per_element"),
                  "context", "context", 42);
  sf_mex_addfield(*c23_info, c23_emlrt_marshallOut("eml_index_rdivide"), "name",
                  "name", 42);
  sf_mex_addfield(*c23_info, c23_emlrt_marshallOut("uint8"), "dominantType",
                  "dominantType", 42);
  sf_mex_addfield(*c23_info, c23_emlrt_marshallOut(
    "[ILXE]$matlabroot$/toolbox/eml/lib/matlab/eml/eml_index_rdivide.m"),
                  "resolved", "resolved", 42);
  sf_mex_addfield(*c23_info, c23_b_emlrt_marshallOut(1372586016U), "fileTimeLo",
                  "fileTimeLo", 42);
  sf_mex_addfield(*c23_info, c23_b_emlrt_marshallOut(0U), "fileTimeHi",
                  "fileTimeHi", 42);
  sf_mex_addfield(*c23_info, c23_b_emlrt_marshallOut(0U), "mFileTimeLo",
                  "mFileTimeLo", 42);
  sf_mex_addfield(*c23_info, c23_b_emlrt_marshallOut(0U), "mFileTimeHi",
                  "mFileTimeHi", 42);
  sf_mex_assign(&c23_rhs42, sf_mex_createcellmatrix(0, 1), false);
  sf_mex_assign(&c23_lhs42, sf_mex_createcellmatrix(0, 1), false);
  sf_mex_addfield(*c23_info, sf_mex_duplicatearraysafe(&c23_rhs42), "rhs", "rhs",
                  42);
  sf_mex_addfield(*c23_info, sf_mex_duplicatearraysafe(&c23_lhs42), "lhs", "lhs",
                  42);
  sf_mex_addfield(*c23_info, c23_emlrt_marshallOut(
    "[ILXE]$matlabroot$/toolbox/eml/lib/matlab/eml/eml_index_rdivide.m"),
                  "context", "context", 43);
  sf_mex_addfield(*c23_info, c23_emlrt_marshallOut("coder.internal.indexDivide"),
                  "name", "name", 43);
  sf_mex_addfield(*c23_info, c23_emlrt_marshallOut("uint8"), "dominantType",
                  "dominantType", 43);
  sf_mex_addfield(*c23_info, c23_emlrt_marshallOut(
    "[ILXE]$matlabroot$/toolbox/shared/coder/coder/+coder/+internal/indexDivide.m"),
                  "resolved", "resolved", 43);
  sf_mex_addfield(*c23_info, c23_b_emlrt_marshallOut(1372586760U), "fileTimeLo",
                  "fileTimeLo", 43);
  sf_mex_addfield(*c23_info, c23_b_emlrt_marshallOut(0U), "fileTimeHi",
                  "fileTimeHi", 43);
  sf_mex_addfield(*c23_info, c23_b_emlrt_marshallOut(0U), "mFileTimeLo",
                  "mFileTimeLo", 43);
  sf_mex_addfield(*c23_info, c23_b_emlrt_marshallOut(0U), "mFileTimeHi",
                  "mFileTimeHi", 43);
  sf_mex_assign(&c23_rhs43, sf_mex_createcellmatrix(0, 1), false);
  sf_mex_assign(&c23_lhs43, sf_mex_createcellmatrix(0, 1), false);
  sf_mex_addfield(*c23_info, sf_mex_duplicatearraysafe(&c23_rhs43), "rhs", "rhs",
                  43);
  sf_mex_addfield(*c23_info, sf_mex_duplicatearraysafe(&c23_lhs43), "lhs", "lhs",
                  43);
  sf_mex_addfield(*c23_info, c23_emlrt_marshallOut(
    "[ILXE]$matlabroot$/toolbox/eml/lib/matlab/datatypes/typecast.m"), "context",
                  "context", 44);
  sf_mex_addfield(*c23_info, c23_emlrt_marshallOut("eml_index_times"), "name",
                  "name", 44);
  sf_mex_addfield(*c23_info, c23_emlrt_marshallOut("double"), "dominantType",
                  "dominantType", 44);
  sf_mex_addfield(*c23_info, c23_emlrt_marshallOut(
    "[ILXE]$matlabroot$/toolbox/eml/lib/matlab/eml/eml_index_times.m"),
                  "resolved", "resolved", 44);
  sf_mex_addfield(*c23_info, c23_b_emlrt_marshallOut(1372586016U), "fileTimeLo",
                  "fileTimeLo", 44);
  sf_mex_addfield(*c23_info, c23_b_emlrt_marshallOut(0U), "fileTimeHi",
                  "fileTimeHi", 44);
  sf_mex_addfield(*c23_info, c23_b_emlrt_marshallOut(0U), "mFileTimeLo",
                  "mFileTimeLo", 44);
  sf_mex_addfield(*c23_info, c23_b_emlrt_marshallOut(0U), "mFileTimeHi",
                  "mFileTimeHi", 44);
  sf_mex_assign(&c23_rhs44, sf_mex_createcellmatrix(0, 1), false);
  sf_mex_assign(&c23_lhs44, sf_mex_createcellmatrix(0, 1), false);
  sf_mex_addfield(*c23_info, sf_mex_duplicatearraysafe(&c23_rhs44), "rhs", "rhs",
                  44);
  sf_mex_addfield(*c23_info, sf_mex_duplicatearraysafe(&c23_lhs44), "lhs", "lhs",
                  44);
  sf_mex_addfield(*c23_info, c23_emlrt_marshallOut(
    "[ILXE]$matlabroot$/toolbox/eml/lib/matlab/eml/eml_index_times.m"),
                  "context", "context", 45);
  sf_mex_addfield(*c23_info, c23_emlrt_marshallOut("coder.internal.indexTimes"),
                  "name", "name", 45);
  sf_mex_addfield(*c23_info, c23_emlrt_marshallOut("double"), "dominantType",
                  "dominantType", 45);
  sf_mex_addfield(*c23_info, c23_emlrt_marshallOut(
    "[ILXE]$matlabroot$/toolbox/shared/coder/coder/+coder/+internal/indexTimes.m"),
                  "resolved", "resolved", 45);
  sf_mex_addfield(*c23_info, c23_b_emlrt_marshallOut(1372586760U), "fileTimeLo",
                  "fileTimeLo", 45);
  sf_mex_addfield(*c23_info, c23_b_emlrt_marshallOut(0U), "fileTimeHi",
                  "fileTimeHi", 45);
  sf_mex_addfield(*c23_info, c23_b_emlrt_marshallOut(0U), "mFileTimeLo",
                  "mFileTimeLo", 45);
  sf_mex_addfield(*c23_info, c23_b_emlrt_marshallOut(0U), "mFileTimeHi",
                  "mFileTimeHi", 45);
  sf_mex_assign(&c23_rhs45, sf_mex_createcellmatrix(0, 1), false);
  sf_mex_assign(&c23_lhs45, sf_mex_createcellmatrix(0, 1), false);
  sf_mex_addfield(*c23_info, sf_mex_duplicatearraysafe(&c23_rhs45), "rhs", "rhs",
                  45);
  sf_mex_addfield(*c23_info, sf_mex_duplicatearraysafe(&c23_lhs45), "lhs", "lhs",
                  45);
  sf_mex_addfield(*c23_info, c23_emlrt_marshallOut(
    "[ILXE]$matlabroot$/toolbox/eml/lib/matlab/datatypes/typecast.m"), "context",
                  "context", 46);
  sf_mex_addfield(*c23_info, c23_emlrt_marshallOut("eml_index_rdivide"), "name",
                  "name", 46);
  sf_mex_addfield(*c23_info, c23_emlrt_marshallOut("coder.internal.indexInt"),
                  "dominantType", "dominantType", 46);
  sf_mex_addfield(*c23_info, c23_emlrt_marshallOut(
    "[ILXE]$matlabroot$/toolbox/eml/lib/matlab/eml/eml_index_rdivide.m"),
                  "resolved", "resolved", 46);
  sf_mex_addfield(*c23_info, c23_b_emlrt_marshallOut(1372586016U), "fileTimeLo",
                  "fileTimeLo", 46);
  sf_mex_addfield(*c23_info, c23_b_emlrt_marshallOut(0U), "fileTimeHi",
                  "fileTimeHi", 46);
  sf_mex_addfield(*c23_info, c23_b_emlrt_marshallOut(0U), "mFileTimeLo",
                  "mFileTimeLo", 46);
  sf_mex_addfield(*c23_info, c23_b_emlrt_marshallOut(0U), "mFileTimeHi",
                  "mFileTimeHi", 46);
  sf_mex_assign(&c23_rhs46, sf_mex_createcellmatrix(0, 1), false);
  sf_mex_assign(&c23_lhs46, sf_mex_createcellmatrix(0, 1), false);
  sf_mex_addfield(*c23_info, sf_mex_duplicatearraysafe(&c23_rhs46), "rhs", "rhs",
                  46);
  sf_mex_addfield(*c23_info, sf_mex_duplicatearraysafe(&c23_lhs46), "lhs", "lhs",
                  46);
  sf_mex_addfield(*c23_info, c23_emlrt_marshallOut(
    "[ILXE]$matlabroot$/toolbox/eml/lib/matlab/eml/eml_index_rdivide.m"),
                  "context", "context", 47);
  sf_mex_addfield(*c23_info, c23_emlrt_marshallOut("coder.internal.indexDivide"),
                  "name", "name", 47);
  sf_mex_addfield(*c23_info, c23_emlrt_marshallOut("coder.internal.indexInt"),
                  "dominantType", "dominantType", 47);
  sf_mex_addfield(*c23_info, c23_emlrt_marshallOut(
    "[ILXE]$matlabroot$/toolbox/shared/coder/coder/+coder/+internal/indexDivide.m"),
                  "resolved", "resolved", 47);
  sf_mex_addfield(*c23_info, c23_b_emlrt_marshallOut(1372586760U), "fileTimeLo",
                  "fileTimeLo", 47);
  sf_mex_addfield(*c23_info, c23_b_emlrt_marshallOut(0U), "fileTimeHi",
                  "fileTimeHi", 47);
  sf_mex_addfield(*c23_info, c23_b_emlrt_marshallOut(0U), "mFileTimeLo",
                  "mFileTimeLo", 47);
  sf_mex_addfield(*c23_info, c23_b_emlrt_marshallOut(0U), "mFileTimeHi",
                  "mFileTimeHi", 47);
  sf_mex_assign(&c23_rhs47, sf_mex_createcellmatrix(0, 1), false);
  sf_mex_assign(&c23_lhs47, sf_mex_createcellmatrix(0, 1), false);
  sf_mex_addfield(*c23_info, sf_mex_duplicatearraysafe(&c23_rhs47), "rhs", "rhs",
                  47);
  sf_mex_addfield(*c23_info, sf_mex_duplicatearraysafe(&c23_lhs47), "lhs", "lhs",
                  47);
  sf_mex_addfield(*c23_info, c23_emlrt_marshallOut(
    "[ILXE]$matlabroot$/toolbox/eml/lib/matlab/datatypes/typecast.m"), "context",
                  "context", 48);
  sf_mex_addfield(*c23_info, c23_emlrt_marshallOut("eml_index_times"), "name",
                  "name", 48);
  sf_mex_addfield(*c23_info, c23_emlrt_marshallOut("coder.internal.indexInt"),
                  "dominantType", "dominantType", 48);
  sf_mex_addfield(*c23_info, c23_emlrt_marshallOut(
    "[ILXE]$matlabroot$/toolbox/eml/lib/matlab/eml/eml_index_times.m"),
                  "resolved", "resolved", 48);
  sf_mex_addfield(*c23_info, c23_b_emlrt_marshallOut(1372586016U), "fileTimeLo",
                  "fileTimeLo", 48);
  sf_mex_addfield(*c23_info, c23_b_emlrt_marshallOut(0U), "fileTimeHi",
                  "fileTimeHi", 48);
  sf_mex_addfield(*c23_info, c23_b_emlrt_marshallOut(0U), "mFileTimeLo",
                  "mFileTimeLo", 48);
  sf_mex_addfield(*c23_info, c23_b_emlrt_marshallOut(0U), "mFileTimeHi",
                  "mFileTimeHi", 48);
  sf_mex_assign(&c23_rhs48, sf_mex_createcellmatrix(0, 1), false);
  sf_mex_assign(&c23_lhs48, sf_mex_createcellmatrix(0, 1), false);
  sf_mex_addfield(*c23_info, sf_mex_duplicatearraysafe(&c23_rhs48), "rhs", "rhs",
                  48);
  sf_mex_addfield(*c23_info, sf_mex_duplicatearraysafe(&c23_lhs48), "lhs", "lhs",
                  48);
  sf_mex_addfield(*c23_info, c23_emlrt_marshallOut(
    "[ILXE]$matlabroot$/toolbox/eml/lib/matlab/eml/eml_index_times.m"),
                  "context", "context", 49);
  sf_mex_addfield(*c23_info, c23_emlrt_marshallOut("coder.internal.indexTimes"),
                  "name", "name", 49);
  sf_mex_addfield(*c23_info, c23_emlrt_marshallOut("coder.internal.indexInt"),
                  "dominantType", "dominantType", 49);
  sf_mex_addfield(*c23_info, c23_emlrt_marshallOut(
    "[ILXE]$matlabroot$/toolbox/shared/coder/coder/+coder/+internal/indexTimes.m"),
                  "resolved", "resolved", 49);
  sf_mex_addfield(*c23_info, c23_b_emlrt_marshallOut(1372586760U), "fileTimeLo",
                  "fileTimeLo", 49);
  sf_mex_addfield(*c23_info, c23_b_emlrt_marshallOut(0U), "fileTimeHi",
                  "fileTimeHi", 49);
  sf_mex_addfield(*c23_info, c23_b_emlrt_marshallOut(0U), "mFileTimeLo",
                  "mFileTimeLo", 49);
  sf_mex_addfield(*c23_info, c23_b_emlrt_marshallOut(0U), "mFileTimeHi",
                  "mFileTimeHi", 49);
  sf_mex_assign(&c23_rhs49, sf_mex_createcellmatrix(0, 1), false);
  sf_mex_assign(&c23_lhs49, sf_mex_createcellmatrix(0, 1), false);
  sf_mex_addfield(*c23_info, sf_mex_duplicatearraysafe(&c23_rhs49), "rhs", "rhs",
                  49);
  sf_mex_addfield(*c23_info, sf_mex_duplicatearraysafe(&c23_lhs49), "lhs", "lhs",
                  49);
  sf_mex_addfield(*c23_info, c23_emlrt_marshallOut(
    "[ILXE]$matlabroot$/toolbox/eml/lib/matlab/datatypes/typecast.m"), "context",
                  "context", 50);
  sf_mex_addfield(*c23_info, c23_emlrt_marshallOut("coder.internal.scalarEg"),
                  "name", "name", 50);
  sf_mex_addfield(*c23_info, c23_emlrt_marshallOut("int8"), "dominantType",
                  "dominantType", 50);
  sf_mex_addfield(*c23_info, c23_emlrt_marshallOut(
    "[ILXE]$matlabroot$/toolbox/coder/coder/+coder/+internal/scalarEg.p"),
                  "resolved", "resolved", 50);
  sf_mex_addfield(*c23_info, c23_b_emlrt_marshallOut(1410811370U), "fileTimeLo",
                  "fileTimeLo", 50);
  sf_mex_addfield(*c23_info, c23_b_emlrt_marshallOut(0U), "fileTimeHi",
                  "fileTimeHi", 50);
  sf_mex_addfield(*c23_info, c23_b_emlrt_marshallOut(0U), "mFileTimeLo",
                  "mFileTimeLo", 50);
  sf_mex_addfield(*c23_info, c23_b_emlrt_marshallOut(0U), "mFileTimeHi",
                  "mFileTimeHi", 50);
  sf_mex_assign(&c23_rhs50, sf_mex_createcellmatrix(0, 1), false);
  sf_mex_assign(&c23_lhs50, sf_mex_createcellmatrix(0, 1), false);
  sf_mex_addfield(*c23_info, sf_mex_duplicatearraysafe(&c23_rhs50), "rhs", "rhs",
                  50);
  sf_mex_addfield(*c23_info, sf_mex_duplicatearraysafe(&c23_lhs50), "lhs", "lhs",
                  50);
  sf_mex_addfield(*c23_info, c23_emlrt_marshallOut(
    "[ILXE]$matlabroot$/toolbox/eml/lib/matlab/datatypes/typecast.m"), "context",
                  "context", 51);
  sf_mex_addfield(*c23_info, c23_emlrt_marshallOut("coder.internal.scalarEg"),
                  "name", "name", 51);
  sf_mex_addfield(*c23_info, c23_emlrt_marshallOut("int16"), "dominantType",
                  "dominantType", 51);
  sf_mex_addfield(*c23_info, c23_emlrt_marshallOut(
    "[ILXE]$matlabroot$/toolbox/coder/coder/+coder/+internal/scalarEg.p"),
                  "resolved", "resolved", 51);
  sf_mex_addfield(*c23_info, c23_b_emlrt_marshallOut(1410811370U), "fileTimeLo",
                  "fileTimeLo", 51);
  sf_mex_addfield(*c23_info, c23_b_emlrt_marshallOut(0U), "fileTimeHi",
                  "fileTimeHi", 51);
  sf_mex_addfield(*c23_info, c23_b_emlrt_marshallOut(0U), "mFileTimeLo",
                  "mFileTimeLo", 51);
  sf_mex_addfield(*c23_info, c23_b_emlrt_marshallOut(0U), "mFileTimeHi",
                  "mFileTimeHi", 51);
  sf_mex_assign(&c23_rhs51, sf_mex_createcellmatrix(0, 1), false);
  sf_mex_assign(&c23_lhs51, sf_mex_createcellmatrix(0, 1), false);
  sf_mex_addfield(*c23_info, sf_mex_duplicatearraysafe(&c23_rhs51), "rhs", "rhs",
                  51);
  sf_mex_addfield(*c23_info, sf_mex_duplicatearraysafe(&c23_lhs51), "lhs", "lhs",
                  51);
  sf_mex_destroy(&c23_rhs0);
  sf_mex_destroy(&c23_lhs0);
  sf_mex_destroy(&c23_rhs1);
  sf_mex_destroy(&c23_lhs1);
  sf_mex_destroy(&c23_rhs2);
  sf_mex_destroy(&c23_lhs2);
  sf_mex_destroy(&c23_rhs3);
  sf_mex_destroy(&c23_lhs3);
  sf_mex_destroy(&c23_rhs4);
  sf_mex_destroy(&c23_lhs4);
  sf_mex_destroy(&c23_rhs5);
  sf_mex_destroy(&c23_lhs5);
  sf_mex_destroy(&c23_rhs6);
  sf_mex_destroy(&c23_lhs6);
  sf_mex_destroy(&c23_rhs7);
  sf_mex_destroy(&c23_lhs7);
  sf_mex_destroy(&c23_rhs8);
  sf_mex_destroy(&c23_lhs8);
  sf_mex_destroy(&c23_rhs9);
  sf_mex_destroy(&c23_lhs9);
  sf_mex_destroy(&c23_rhs10);
  sf_mex_destroy(&c23_lhs10);
  sf_mex_destroy(&c23_rhs11);
  sf_mex_destroy(&c23_lhs11);
  sf_mex_destroy(&c23_rhs12);
  sf_mex_destroy(&c23_lhs12);
  sf_mex_destroy(&c23_rhs13);
  sf_mex_destroy(&c23_lhs13);
  sf_mex_destroy(&c23_rhs14);
  sf_mex_destroy(&c23_lhs14);
  sf_mex_destroy(&c23_rhs15);
  sf_mex_destroy(&c23_lhs15);
  sf_mex_destroy(&c23_rhs16);
  sf_mex_destroy(&c23_lhs16);
  sf_mex_destroy(&c23_rhs17);
  sf_mex_destroy(&c23_lhs17);
  sf_mex_destroy(&c23_rhs18);
  sf_mex_destroy(&c23_lhs18);
  sf_mex_destroy(&c23_rhs19);
  sf_mex_destroy(&c23_lhs19);
  sf_mex_destroy(&c23_rhs20);
  sf_mex_destroy(&c23_lhs20);
  sf_mex_destroy(&c23_rhs21);
  sf_mex_destroy(&c23_lhs21);
  sf_mex_destroy(&c23_rhs22);
  sf_mex_destroy(&c23_lhs22);
  sf_mex_destroy(&c23_rhs23);
  sf_mex_destroy(&c23_lhs23);
  sf_mex_destroy(&c23_rhs24);
  sf_mex_destroy(&c23_lhs24);
  sf_mex_destroy(&c23_rhs25);
  sf_mex_destroy(&c23_lhs25);
  sf_mex_destroy(&c23_rhs26);
  sf_mex_destroy(&c23_lhs26);
  sf_mex_destroy(&c23_rhs27);
  sf_mex_destroy(&c23_lhs27);
  sf_mex_destroy(&c23_rhs28);
  sf_mex_destroy(&c23_lhs28);
  sf_mex_destroy(&c23_rhs29);
  sf_mex_destroy(&c23_lhs29);
  sf_mex_destroy(&c23_rhs30);
  sf_mex_destroy(&c23_lhs30);
  sf_mex_destroy(&c23_rhs31);
  sf_mex_destroy(&c23_lhs31);
  sf_mex_destroy(&c23_rhs32);
  sf_mex_destroy(&c23_lhs32);
  sf_mex_destroy(&c23_rhs33);
  sf_mex_destroy(&c23_lhs33);
  sf_mex_destroy(&c23_rhs34);
  sf_mex_destroy(&c23_lhs34);
  sf_mex_destroy(&c23_rhs35);
  sf_mex_destroy(&c23_lhs35);
  sf_mex_destroy(&c23_rhs36);
  sf_mex_destroy(&c23_lhs36);
  sf_mex_destroy(&c23_rhs37);
  sf_mex_destroy(&c23_lhs37);
  sf_mex_destroy(&c23_rhs38);
  sf_mex_destroy(&c23_lhs38);
  sf_mex_destroy(&c23_rhs39);
  sf_mex_destroy(&c23_lhs39);
  sf_mex_destroy(&c23_rhs40);
  sf_mex_destroy(&c23_lhs40);
  sf_mex_destroy(&c23_rhs41);
  sf_mex_destroy(&c23_lhs41);
  sf_mex_destroy(&c23_rhs42);
  sf_mex_destroy(&c23_lhs42);
  sf_mex_destroy(&c23_rhs43);
  sf_mex_destroy(&c23_lhs43);
  sf_mex_destroy(&c23_rhs44);
  sf_mex_destroy(&c23_lhs44);
  sf_mex_destroy(&c23_rhs45);
  sf_mex_destroy(&c23_lhs45);
  sf_mex_destroy(&c23_rhs46);
  sf_mex_destroy(&c23_lhs46);
  sf_mex_destroy(&c23_rhs47);
  sf_mex_destroy(&c23_lhs47);
  sf_mex_destroy(&c23_rhs48);
  sf_mex_destroy(&c23_lhs48);
  sf_mex_destroy(&c23_rhs49);
  sf_mex_destroy(&c23_lhs49);
  sf_mex_destroy(&c23_rhs50);
  sf_mex_destroy(&c23_lhs50);
  sf_mex_destroy(&c23_rhs51);
  sf_mex_destroy(&c23_lhs51);
}

static const mxArray *c23_emlrt_marshallOut(const char * c23_u)
{
  const mxArray *c23_y = NULL;
  c23_y = NULL;
  sf_mex_assign(&c23_y, sf_mex_create("y", c23_u, 15, 0U, 0U, 0U, 2, 1, strlen
    (c23_u)), false);
  return c23_y;
}

static const mxArray *c23_b_emlrt_marshallOut(const uint32_T c23_u)
{
  const mxArray *c23_y = NULL;
  c23_y = NULL;
  sf_mex_assign(&c23_y, sf_mex_create("y", &c23_u, 7, 0U, 0U, 0U, 0), false);
  return c23_y;
}

static int8_T c23_typecast(SFc23_LessonIII_startInstanceStruct *chartInstance,
  uint8_T c23_x)
{
  int8_T c23_y;
  (void)chartInstance;
  memcpy(&c23_y, &c23_x, (size_t)1 * sizeof(int8_T));
  return c23_y;
}

static const mxArray *c23_k_sf_marshallOut(void *chartInstanceVoid, void
  *c23_inData)
{
  const mxArray *c23_mxArrayOutData = NULL;
  int32_T c23_u;
  const mxArray *c23_y = NULL;
  SFc23_LessonIII_startInstanceStruct *chartInstance;
  chartInstance = (SFc23_LessonIII_startInstanceStruct *)chartInstanceVoid;
  c23_mxArrayOutData = NULL;
  c23_u = *(int32_T *)c23_inData;
  c23_y = NULL;
  sf_mex_assign(&c23_y, sf_mex_create("y", &c23_u, 6, 0U, 0U, 0U, 0), false);
  sf_mex_assign(&c23_mxArrayOutData, c23_y, false);
  return c23_mxArrayOutData;
}

static int32_T c23_o_emlrt_marshallIn(SFc23_LessonIII_startInstanceStruct
  *chartInstance, const mxArray *c23_u, const emlrtMsgIdentifier *c23_parentId)
{
  int32_T c23_y;
  int32_T c23_i38;
  (void)chartInstance;
  sf_mex_import(c23_parentId, sf_mex_dup(c23_u), &c23_i38, 1, 6, 0U, 0, 0U, 0);
  c23_y = c23_i38;
  sf_mex_destroy(&c23_u);
  return c23_y;
}

static void c23_j_sf_marshallIn(void *chartInstanceVoid, const mxArray
  *c23_mxArrayInData, const char_T *c23_varName, void *c23_outData)
{
  const mxArray *c23_b_sfEvent;
  const char_T *c23_identifier;
  emlrtMsgIdentifier c23_thisId;
  int32_T c23_y;
  SFc23_LessonIII_startInstanceStruct *chartInstance;
  chartInstance = (SFc23_LessonIII_startInstanceStruct *)chartInstanceVoid;
  c23_b_sfEvent = sf_mex_dup(c23_mxArrayInData);
  c23_identifier = c23_varName;
  c23_thisId.fIdentifier = c23_identifier;
  c23_thisId.fParent = NULL;
  c23_y = c23_o_emlrt_marshallIn(chartInstance, sf_mex_dup(c23_b_sfEvent),
    &c23_thisId);
  sf_mex_destroy(&c23_b_sfEvent);
  *(int32_T *)c23_outData = c23_y;
  sf_mex_destroy(&c23_mxArrayInData);
}

static const mxArray *c23_Ball_bus_io(void *chartInstanceVoid, void *c23_pData)
{
  const mxArray *c23_mxVal = NULL;
  c23_Ball c23_tmp;
  SFc23_LessonIII_startInstanceStruct *chartInstance;
  chartInstance = (SFc23_LessonIII_startInstanceStruct *)chartInstanceVoid;
  c23_mxVal = NULL;
  c23_tmp.x = *(int8_T *)&((char_T *)(c23_Ball *)c23_pData)[0];
  c23_tmp.y = *(int8_T *)&((char_T *)(c23_Ball *)c23_pData)[1];
  c23_tmp.valid = *(uint8_T *)&((char_T *)(c23_Ball *)c23_pData)[2];
  sf_mex_assign(&c23_mxVal, c23_c_sf_marshallOut(chartInstance, &c23_tmp), false);
  return c23_mxVal;
}

static const mxArray *c23_players_bus_io(void *chartInstanceVoid, void
  *c23_pData)
{
  const mxArray *c23_mxVal = NULL;
  int32_T c23_i39;
  c23_Player c23_tmp[6];
  SFc23_LessonIII_startInstanceStruct *chartInstance;
  chartInstance = (SFc23_LessonIII_startInstanceStruct *)chartInstanceVoid;
  c23_mxVal = NULL;
  for (c23_i39 = 0; c23_i39 < 6; c23_i39++) {
    c23_tmp[c23_i39].x = *(int8_T *)&((char_T *)(c23_Player *)&((char_T *)
      (c23_Player (*)[6])c23_pData)[8 * c23_i39])[0];
    c23_tmp[c23_i39].y = *(int8_T *)&((char_T *)(c23_Player *)&((char_T *)
      (c23_Player (*)[6])c23_pData)[8 * c23_i39])[1];
    c23_tmp[c23_i39].orientation = *(int16_T *)&((char_T *)(c23_Player *)
      &((char_T *)(c23_Player (*)[6])c23_pData)[8 * c23_i39])[2];
    c23_tmp[c23_i39].color = *(uint8_T *)&((char_T *)(c23_Player *)&((char_T *)
      (c23_Player (*)[6])c23_pData)[8 * c23_i39])[4];
    c23_tmp[c23_i39].position = *(uint8_T *)&((char_T *)(c23_Player *)&((char_T *)
      (c23_Player (*)[6])c23_pData)[8 * c23_i39])[5];
    c23_tmp[c23_i39].valid = *(uint8_T *)&((char_T *)(c23_Player *)&((char_T *)
      (c23_Player (*)[6])c23_pData)[8 * c23_i39])[6];
  }

  sf_mex_assign(&c23_mxVal, c23_b_sf_marshallOut(chartInstance, c23_tmp), false);
  return c23_mxVal;
}

static void init_dsm_address_info(SFc23_LessonIII_startInstanceStruct
  *chartInstance)
{
  (void)chartInstance;
}

static void init_simulink_io_address(SFc23_LessonIII_startInstanceStruct
  *chartInstance)
{
  chartInstance->c23_message = (uint8_T (*)[31])ssGetInputPortSignal_wrapper
    (chartInstance->S, 0);
  chartInstance->c23_b_Ball = (c23_Ball *)ssGetOutputPortSignal_wrapper
    (chartInstance->S, 1);
  chartInstance->c23_players = (c23_Player (*)[6])ssGetOutputPortSignal_wrapper
    (chartInstance->S, 2);
  chartInstance->c23_gameOn = (uint8_T *)ssGetOutputPortSignal_wrapper
    (chartInstance->S, 3);
}

/* SFunction Glue Code */
#ifdef utFree
#undef utFree
#endif

#ifdef utMalloc
#undef utMalloc
#endif

#ifdef __cplusplus

extern "C" void *utMalloc(size_t size);
extern "C" void utFree(void*);

#else

extern void *utMalloc(size_t size);
extern void utFree(void*);

#endif

void sf_c23_LessonIII_start_get_check_sum(mxArray *plhs[])
{
  ((real_T *)mxGetPr((plhs[0])))[0] = (real_T)(2192425464U);
  ((real_T *)mxGetPr((plhs[0])))[1] = (real_T)(3298638615U);
  ((real_T *)mxGetPr((plhs[0])))[2] = (real_T)(1266005252U);
  ((real_T *)mxGetPr((plhs[0])))[3] = (real_T)(649278063U);
}

mxArray* sf_c23_LessonIII_start_get_post_codegen_info(void);
mxArray *sf_c23_LessonIII_start_get_autoinheritance_info(void)
{
  const char *autoinheritanceFields[] = { "checksum", "inputs", "parameters",
    "outputs", "locals", "postCodegenInfo" };

  mxArray *mxAutoinheritanceInfo = mxCreateStructMatrix(1, 1, sizeof
    (autoinheritanceFields)/sizeof(autoinheritanceFields[0]),
    autoinheritanceFields);

  {
    mxArray *mxChecksum = mxCreateString("YxuJ3VIU6BKAvA58gYIRPD");
    mxSetField(mxAutoinheritanceInfo,0,"checksum",mxChecksum);
  }

  {
    const char *dataFields[] = { "size", "type", "complexity" };

    mxArray *mxData = mxCreateStructMatrix(1,1,3,dataFields);

    {
      mxArray *mxSize = mxCreateDoubleMatrix(1,2,mxREAL);
      double *pr = mxGetPr(mxSize);
      pr[0] = (double)(1);
      pr[1] = (double)(31);
      mxSetField(mxData,0,"size",mxSize);
    }

    {
      const char *typeFields[] = { "base", "fixpt" };

      mxArray *mxType = mxCreateStructMatrix(1,1,2,typeFields);
      mxSetField(mxType,0,"base",mxCreateDoubleScalar(3));
      mxSetField(mxType,0,"fixpt",mxCreateDoubleMatrix(0,0,mxREAL));
      mxSetField(mxData,0,"type",mxType);
    }

    mxSetField(mxData,0,"complexity",mxCreateDoubleScalar(0));
    mxSetField(mxAutoinheritanceInfo,0,"inputs",mxData);
  }

  {
    mxSetField(mxAutoinheritanceInfo,0,"parameters",mxCreateDoubleMatrix(0,0,
                mxREAL));
  }

  {
    const char *dataFields[] = { "size", "type", "complexity" };

    mxArray *mxData = mxCreateStructMatrix(1,3,3,dataFields);

    {
      mxArray *mxSize = mxCreateDoubleMatrix(1,2,mxREAL);
      double *pr = mxGetPr(mxSize);
      pr[0] = (double)(1);
      pr[1] = (double)(1);
      mxSetField(mxData,0,"size",mxSize);
    }

    {
      const char *typeFields[] = { "base", "fixpt" };

      mxArray *mxType = mxCreateStructMatrix(1,1,2,typeFields);
      mxSetField(mxType,0,"base",mxCreateDoubleScalar(13));
      mxSetField(mxType,0,"fixpt",mxCreateDoubleMatrix(0,0,mxREAL));
      mxSetField(mxData,0,"type",mxType);
    }

    mxSetField(mxData,0,"complexity",mxCreateDoubleScalar(0));

    {
      mxArray *mxSize = mxCreateDoubleMatrix(1,2,mxREAL);
      double *pr = mxGetPr(mxSize);
      pr[0] = (double)(6);
      pr[1] = (double)(1);
      mxSetField(mxData,1,"size",mxSize);
    }

    {
      const char *typeFields[] = { "base", "fixpt" };

      mxArray *mxType = mxCreateStructMatrix(1,1,2,typeFields);
      mxSetField(mxType,0,"base",mxCreateDoubleScalar(13));
      mxSetField(mxType,0,"fixpt",mxCreateDoubleMatrix(0,0,mxREAL));
      mxSetField(mxData,1,"type",mxType);
    }

    mxSetField(mxData,1,"complexity",mxCreateDoubleScalar(0));

    {
      mxArray *mxSize = mxCreateDoubleMatrix(1,2,mxREAL);
      double *pr = mxGetPr(mxSize);
      pr[0] = (double)(1);
      pr[1] = (double)(1);
      mxSetField(mxData,2,"size",mxSize);
    }

    {
      const char *typeFields[] = { "base", "fixpt" };

      mxArray *mxType = mxCreateStructMatrix(1,1,2,typeFields);
      mxSetField(mxType,0,"base",mxCreateDoubleScalar(3));
      mxSetField(mxType,0,"fixpt",mxCreateDoubleMatrix(0,0,mxREAL));
      mxSetField(mxData,2,"type",mxType);
    }

    mxSetField(mxData,2,"complexity",mxCreateDoubleScalar(0));
    mxSetField(mxAutoinheritanceInfo,0,"outputs",mxData);
  }

  {
    mxSetField(mxAutoinheritanceInfo,0,"locals",mxCreateDoubleMatrix(0,0,mxREAL));
  }

  {
    mxArray* mxPostCodegenInfo = sf_c23_LessonIII_start_get_post_codegen_info();
    mxSetField(mxAutoinheritanceInfo,0,"postCodegenInfo",mxPostCodegenInfo);
  }

  return(mxAutoinheritanceInfo);
}

mxArray *sf_c23_LessonIII_start_third_party_uses_info(void)
{
  mxArray * mxcell3p = mxCreateCellMatrix(1,0);
  return(mxcell3p);
}

mxArray *sf_c23_LessonIII_start_jit_fallback_info(void)
{
  const char *infoFields[] = { "fallbackType", "fallbackReason",
    "incompatibleSymbol", };

  mxArray *mxInfo = mxCreateStructMatrix(1, 1, 3, infoFields);
  mxArray *fallbackReason = mxCreateString("feature_off");
  mxArray *incompatibleSymbol = mxCreateString("");
  mxArray *fallbackType = mxCreateString("early");
  mxSetField(mxInfo, 0, infoFields[0], fallbackType);
  mxSetField(mxInfo, 0, infoFields[1], fallbackReason);
  mxSetField(mxInfo, 0, infoFields[2], incompatibleSymbol);
  return mxInfo;
}

mxArray *sf_c23_LessonIII_start_updateBuildInfo_args_info(void)
{
  mxArray *mxBIArgs = mxCreateCellMatrix(1,0);
  return mxBIArgs;
}

mxArray* sf_c23_LessonIII_start_get_post_codegen_info(void)
{
  const char* fieldNames[] = { "exportedFunctionsUsedByThisChart",
    "exportedFunctionsChecksum" };

  mwSize dims[2] = { 1, 1 };

  mxArray* mxPostCodegenInfo = mxCreateStructArray(2, dims, sizeof(fieldNames)/
    sizeof(fieldNames[0]), fieldNames);

  {
    mxArray* mxExportedFunctionsChecksum = mxCreateString("");
    mwSize exp_dims[2] = { 0, 1 };

    mxArray* mxExportedFunctionsUsedByThisChart = mxCreateCellArray(2, exp_dims);
    mxSetField(mxPostCodegenInfo, 0, "exportedFunctionsUsedByThisChart",
               mxExportedFunctionsUsedByThisChart);
    mxSetField(mxPostCodegenInfo, 0, "exportedFunctionsChecksum",
               mxExportedFunctionsChecksum);
  }

  return mxPostCodegenInfo;
}

static const mxArray *sf_get_sim_state_info_c23_LessonIII_start(void)
{
  const char *infoFields[] = { "chartChecksum", "varInfo" };

  mxArray *mxInfo = mxCreateStructMatrix(1, 1, 2, infoFields);
  const char *infoEncStr[] = {
    "100 S1x4'type','srcId','name','auxInfo'{{M[1],M[5],T\"Ball\",},{M[1],M[9],T\"gameOn\",},{M[1],M[8],T\"players\",},{M[8],M[0],T\"is_active_c23_LessonIII_start\",}}"
  };

  mxArray *mxVarInfo = sf_mex_decode_encoded_mx_struct_array(infoEncStr, 4, 10);
  mxArray *mxChecksum = mxCreateDoubleMatrix(1, 4, mxREAL);
  sf_c23_LessonIII_start_get_check_sum(&mxChecksum);
  mxSetField(mxInfo, 0, infoFields[0], mxChecksum);
  mxSetField(mxInfo, 0, infoFields[1], mxVarInfo);
  return mxInfo;
}

static void chart_debug_initialization(SimStruct *S, unsigned int
  fullDebuggerInitialization)
{
  if (!sim_mode_is_rtw_gen(S)) {
    SFc23_LessonIII_startInstanceStruct *chartInstance;
    ChartRunTimeInfo * crtInfo = (ChartRunTimeInfo *)(ssGetUserData(S));
    ChartInfoStruct * chartInfo = (ChartInfoStruct *)(crtInfo->instanceInfo);
    chartInstance = (SFc23_LessonIII_startInstanceStruct *)
      chartInfo->chartInstance;
    if (ssIsFirstInitCond(S) && fullDebuggerInitialization==1) {
      /* do this only if simulation is starting */
      {
        unsigned int chartAlreadyPresent;
        chartAlreadyPresent = sf_debug_initialize_chart
          (sfGlobalDebugInstanceStruct,
           _LessonIII_startMachineNumber_,
           23,
           1,
           1,
           0,
           4,
           0,
           0,
           0,
           0,
           0,
           &(chartInstance->chartNumber),
           &(chartInstance->instanceNumber),
           (void *)S);

        /* Each instance must initialize its own list of scripts */
        init_script_number_translation(_LessonIII_startMachineNumber_,
          chartInstance->chartNumber,chartInstance->instanceNumber);
        if (chartAlreadyPresent==0) {
          /* this is the first instance */
          sf_debug_set_chart_disable_implicit_casting
            (sfGlobalDebugInstanceStruct,_LessonIII_startMachineNumber_,
             chartInstance->chartNumber,1);
          sf_debug_set_chart_event_thresholds(sfGlobalDebugInstanceStruct,
            _LessonIII_startMachineNumber_,
            chartInstance->chartNumber,
            0,
            0,
            0);
          _SFD_SET_DATA_PROPS(0,1,1,0,"message");
          _SFD_SET_DATA_PROPS(1,2,0,1,"Ball");
          _SFD_SET_DATA_PROPS(2,2,0,1,"players");
          _SFD_SET_DATA_PROPS(3,2,0,1,"gameOn");
          _SFD_STATE_INFO(0,0,2);
          _SFD_CH_SUBSTATE_COUNT(0);
          _SFD_CH_SUBSTATE_DECOMP(0);
        }

        _SFD_CV_INIT_CHART(0,0,0,0);

        {
          _SFD_CV_INIT_STATE(0,0,0,0,0,0,NULL,NULL);
        }

        _SFD_CV_INIT_TRANS(0,0,NULL,NULL,0,NULL);

        /* Initialization of MATLAB Function Model Coverage */
        _SFD_CV_INIT_EML(0,1,1,0,0,0,0,1,0,0,0);
        _SFD_CV_INIT_EML_FCN(0,0,"eML_blk_kernel",0,-1,885);
        _SFD_CV_INIT_EML_FOR(0,1,0,573,584,885);

        {
          unsigned int dimVector[2];
          dimVector[0]= 1;
          dimVector[1]= 31;
          _SFD_SET_DATA_COMPILED_PROPS(0,SF_UINT8,2,&(dimVector[0]),0,0,0,0.0,
            1.0,0,0,(MexFcnForType)c23_d_sf_marshallOut,(MexInFcnForType)NULL);
        }

        _SFD_SET_DATA_COMPILED_PROPS(1,SF_STRUCT,0,NULL,0,0,0,0.0,1.0,0,0,
          (MexFcnForType)c23_Ball_bus_io,(MexInFcnForType)NULL);

        {
          unsigned int dimVector[1];
          dimVector[0]= 6;
          _SFD_SET_DATA_COMPILED_PROPS(2,SF_STRUCT,1,&(dimVector[0]),0,0,0,0.0,
            1.0,0,0,(MexFcnForType)c23_players_bus_io,(MexInFcnForType)NULL);
        }

        _SFD_SET_DATA_COMPILED_PROPS(3,SF_UINT8,0,NULL,0,0,0,0.0,1.0,0,0,
          (MexFcnForType)c23_sf_marshallOut,(MexInFcnForType)c23_sf_marshallIn);
        _SFD_SET_DATA_VALUE_PTR(0U, *chartInstance->c23_message);
        _SFD_SET_DATA_VALUE_PTR(1U, chartInstance->c23_b_Ball);
        _SFD_SET_DATA_VALUE_PTR(2U, *chartInstance->c23_players);
        _SFD_SET_DATA_VALUE_PTR(3U, chartInstance->c23_gameOn);
      }
    } else {
      sf_debug_reset_current_state_configuration(sfGlobalDebugInstanceStruct,
        _LessonIII_startMachineNumber_,chartInstance->chartNumber,
        chartInstance->instanceNumber);
    }
  }
}

static const char* sf_get_instance_specialization(void)
{
  return "gkxnlDDaCol3tTf1u27nYB";
}

static void sf_opaque_initialize_c23_LessonIII_start(void *chartInstanceVar)
{
  chart_debug_initialization(((SFc23_LessonIII_startInstanceStruct*)
    chartInstanceVar)->S,0);
  initialize_params_c23_LessonIII_start((SFc23_LessonIII_startInstanceStruct*)
    chartInstanceVar);
  initialize_c23_LessonIII_start((SFc23_LessonIII_startInstanceStruct*)
    chartInstanceVar);
}

static void sf_opaque_enable_c23_LessonIII_start(void *chartInstanceVar)
{
  enable_c23_LessonIII_start((SFc23_LessonIII_startInstanceStruct*)
    chartInstanceVar);
}

static void sf_opaque_disable_c23_LessonIII_start(void *chartInstanceVar)
{
  disable_c23_LessonIII_start((SFc23_LessonIII_startInstanceStruct*)
    chartInstanceVar);
}

static void sf_opaque_gateway_c23_LessonIII_start(void *chartInstanceVar)
{
  sf_gateway_c23_LessonIII_start((SFc23_LessonIII_startInstanceStruct*)
    chartInstanceVar);
}

static const mxArray* sf_opaque_get_sim_state_c23_LessonIII_start(SimStruct* S)
{
  ChartRunTimeInfo * crtInfo = (ChartRunTimeInfo *)(ssGetUserData(S));
  ChartInfoStruct * chartInfo = (ChartInfoStruct *)(crtInfo->instanceInfo);
  return get_sim_state_c23_LessonIII_start((SFc23_LessonIII_startInstanceStruct*)
    chartInfo->chartInstance);         /* raw sim ctx */
}

static void sf_opaque_set_sim_state_c23_LessonIII_start(SimStruct* S, const
  mxArray *st)
{
  ChartRunTimeInfo * crtInfo = (ChartRunTimeInfo *)(ssGetUserData(S));
  ChartInfoStruct * chartInfo = (ChartInfoStruct *)(crtInfo->instanceInfo);
  set_sim_state_c23_LessonIII_start((SFc23_LessonIII_startInstanceStruct*)
    chartInfo->chartInstance, st);
}

static void sf_opaque_terminate_c23_LessonIII_start(void *chartInstanceVar)
{
  if (chartInstanceVar!=NULL) {
    SimStruct *S = ((SFc23_LessonIII_startInstanceStruct*) chartInstanceVar)->S;
    ChartRunTimeInfo * crtInfo = (ChartRunTimeInfo *)(ssGetUserData(S));
    if (sim_mode_is_rtw_gen(S) || sim_mode_is_external(S)) {
      sf_clear_rtw_identifier(S);
      unload_LessonIII_start_optimization_info();
    }

    finalize_c23_LessonIII_start((SFc23_LessonIII_startInstanceStruct*)
      chartInstanceVar);
    utFree(chartInstanceVar);
    if (crtInfo != NULL) {
      utFree(crtInfo);
    }

    ssSetUserData(S,NULL);
  }
}

static void sf_opaque_init_subchart_simstructs(void *chartInstanceVar)
{
  initSimStructsc23_LessonIII_start((SFc23_LessonIII_startInstanceStruct*)
    chartInstanceVar);
}

extern unsigned int sf_machine_global_initializer_called(void);
static void mdlProcessParameters_c23_LessonIII_start(SimStruct *S)
{
  int i;
  for (i=0;i<ssGetNumRunTimeParams(S);i++) {
    if (ssGetSFcnParamTunable(S,i)) {
      ssUpdateDlgParamAsRunTimeParam(S,i);
    }
  }

  if (sf_machine_global_initializer_called()) {
    ChartRunTimeInfo * crtInfo = (ChartRunTimeInfo *)(ssGetUserData(S));
    ChartInfoStruct * chartInfo = (ChartInfoStruct *)(crtInfo->instanceInfo);
    initialize_params_c23_LessonIII_start((SFc23_LessonIII_startInstanceStruct*)
      (chartInfo->chartInstance));
  }
}

static void mdlSetWorkWidths_c23_LessonIII_start(SimStruct *S)
{
  if (sim_mode_is_rtw_gen(S) || sim_mode_is_external(S)) {
    mxArray *infoStruct = load_LessonIII_start_optimization_info();
    int_T chartIsInlinable =
      (int_T)sf_is_chart_inlinable(sf_get_instance_specialization(),infoStruct,
      23);
    ssSetStateflowIsInlinable(S,chartIsInlinable);
    ssSetRTWCG(S,sf_rtw_info_uint_prop(sf_get_instance_specialization(),
                infoStruct,23,"RTWCG"));
    ssSetEnableFcnIsTrivial(S,1);
    ssSetDisableFcnIsTrivial(S,1);
    ssSetNotMultipleInlinable(S,sf_rtw_info_uint_prop
      (sf_get_instance_specialization(),infoStruct,23,
       "gatewayCannotBeInlinedMultipleTimes"));
    sf_update_buildInfo(sf_get_instance_specialization(),infoStruct,23);
    if (chartIsInlinable) {
      ssSetInputPortOptimOpts(S, 0, SS_REUSABLE_AND_LOCAL);
      sf_mark_chart_expressionable_inputs(S,sf_get_instance_specialization(),
        infoStruct,23,1);
      sf_mark_chart_reusable_outputs(S,sf_get_instance_specialization(),
        infoStruct,23,3);
    }

    {
      unsigned int outPortIdx;
      for (outPortIdx=1; outPortIdx<=3; ++outPortIdx) {
        ssSetOutputPortOptimizeInIR(S, outPortIdx, 1U);
      }
    }

    {
      unsigned int inPortIdx;
      for (inPortIdx=0; inPortIdx < 1; ++inPortIdx) {
        ssSetInputPortOptimizeInIR(S, inPortIdx, 1U);
      }
    }

    sf_set_rtw_dwork_info(S,sf_get_instance_specialization(),infoStruct,23);
    ssSetHasSubFunctions(S,!(chartIsInlinable));
  } else {
  }

  ssSetOptions(S,ssGetOptions(S)|SS_OPTION_WORKS_WITH_CODE_REUSE);
  ssSetChecksum0(S,(2829770082U));
  ssSetChecksum1(S,(1790413495U));
  ssSetChecksum2(S,(3146178914U));
  ssSetChecksum3(S,(438869340U));
  ssSetmdlDerivatives(S, NULL);
  ssSetExplicitFCSSCtrl(S,1);
  ssSupportsMultipleExecInstances(S,1);
}

static void mdlRTW_c23_LessonIII_start(SimStruct *S)
{
  if (sim_mode_is_rtw_gen(S)) {
    ssWriteRTWStrParam(S, "StateflowChartType", "Embedded MATLAB");
  }
}

static void mdlStart_c23_LessonIII_start(SimStruct *S)
{
  SFc23_LessonIII_startInstanceStruct *chartInstance;
  ChartRunTimeInfo * crtInfo = (ChartRunTimeInfo *)utMalloc(sizeof
    (ChartRunTimeInfo));
  chartInstance = (SFc23_LessonIII_startInstanceStruct *)utMalloc(sizeof
    (SFc23_LessonIII_startInstanceStruct));
  memset(chartInstance, 0, sizeof(SFc23_LessonIII_startInstanceStruct));
  if (chartInstance==NULL) {
    sf_mex_error_message("Could not allocate memory for chart instance.");
  }

  chartInstance->chartInfo.chartInstance = chartInstance;
  chartInstance->chartInfo.isEMLChart = 1;
  chartInstance->chartInfo.chartInitialized = 0;
  chartInstance->chartInfo.sFunctionGateway =
    sf_opaque_gateway_c23_LessonIII_start;
  chartInstance->chartInfo.initializeChart =
    sf_opaque_initialize_c23_LessonIII_start;
  chartInstance->chartInfo.terminateChart =
    sf_opaque_terminate_c23_LessonIII_start;
  chartInstance->chartInfo.enableChart = sf_opaque_enable_c23_LessonIII_start;
  chartInstance->chartInfo.disableChart = sf_opaque_disable_c23_LessonIII_start;
  chartInstance->chartInfo.getSimState =
    sf_opaque_get_sim_state_c23_LessonIII_start;
  chartInstance->chartInfo.setSimState =
    sf_opaque_set_sim_state_c23_LessonIII_start;
  chartInstance->chartInfo.getSimStateInfo =
    sf_get_sim_state_info_c23_LessonIII_start;
  chartInstance->chartInfo.zeroCrossings = NULL;
  chartInstance->chartInfo.outputs = NULL;
  chartInstance->chartInfo.derivatives = NULL;
  chartInstance->chartInfo.mdlRTW = mdlRTW_c23_LessonIII_start;
  chartInstance->chartInfo.mdlStart = mdlStart_c23_LessonIII_start;
  chartInstance->chartInfo.mdlSetWorkWidths =
    mdlSetWorkWidths_c23_LessonIII_start;
  chartInstance->chartInfo.extModeExec = NULL;
  chartInstance->chartInfo.restoreLastMajorStepConfiguration = NULL;
  chartInstance->chartInfo.restoreBeforeLastMajorStepConfiguration = NULL;
  chartInstance->chartInfo.storeCurrentConfiguration = NULL;
  chartInstance->chartInfo.callAtomicSubchartUserFcn = NULL;
  chartInstance->chartInfo.callAtomicSubchartAutoFcn = NULL;
  chartInstance->chartInfo.debugInstance = sfGlobalDebugInstanceStruct;
  chartInstance->S = S;
  crtInfo->checksum = SF_RUNTIME_INFO_CHECKSUM;
  crtInfo->instanceInfo = (&(chartInstance->chartInfo));
  crtInfo->isJITEnabled = false;
  crtInfo->compiledInfo = NULL;
  ssSetUserData(S,(void *)(crtInfo));  /* register the chart instance with simstruct */
  init_dsm_address_info(chartInstance);
  init_simulink_io_address(chartInstance);
  if (!sim_mode_is_rtw_gen(S)) {
  }

  sf_opaque_init_subchart_simstructs(chartInstance->chartInfo.chartInstance);
  chart_debug_initialization(S,1);
}

void c23_LessonIII_start_method_dispatcher(SimStruct *S, int_T method, void
  *data)
{
  switch (method) {
   case SS_CALL_MDL_START:
    mdlStart_c23_LessonIII_start(S);
    break;

   case SS_CALL_MDL_SET_WORK_WIDTHS:
    mdlSetWorkWidths_c23_LessonIII_start(S);
    break;

   case SS_CALL_MDL_PROCESS_PARAMETERS:
    mdlProcessParameters_c23_LessonIII_start(S);
    break;

   default:
    /* Unhandled method */
    sf_mex_error_message("Stateflow Internal Error:\n"
                         "Error calling c23_LessonIII_start_method_dispatcher.\n"
                         "Can't handle method %d.\n", method);
    break;
  }
}

/* Include files */

#include <stddef.h>
#include "blas.h"
#include "LessonIII_start_sfun.h"
#include "c56_LessonIII_start.h"
#define CHARTINSTANCE_CHARTNUMBER      (chartInstance->chartNumber)
#define CHARTINSTANCE_INSTANCENUMBER   (chartInstance->instanceNumber)
#include "LessonIII_start_sfun_debug_macros.h"
#define _SF_MEX_LISTEN_FOR_CTRL_C(S)   sf_mex_listen_for_ctrl_c(sfGlobalDebugInstanceStruct,S);

/* Type Definitions */

/* Named Constants */
#define CALL_EVENT                     (-1)

/* Variable Declarations */

/* Variable Definitions */
static real_T _sfTime_;
static const char * c56_debug_family_names[4] = { "nargin", "nargout",
  "messageBuffer", "message" };

/* Function Declarations */
static void initialize_c56_LessonIII_start(SFc56_LessonIII_startInstanceStruct
  *chartInstance);
static void initialize_params_c56_LessonIII_start
  (SFc56_LessonIII_startInstanceStruct *chartInstance);
static void enable_c56_LessonIII_start(SFc56_LessonIII_startInstanceStruct
  *chartInstance);
static void disable_c56_LessonIII_start(SFc56_LessonIII_startInstanceStruct
  *chartInstance);
static void c56_update_debugger_state_c56_LessonIII_start
  (SFc56_LessonIII_startInstanceStruct *chartInstance);
static const mxArray *get_sim_state_c56_LessonIII_start
  (SFc56_LessonIII_startInstanceStruct *chartInstance);
static void set_sim_state_c56_LessonIII_start
  (SFc56_LessonIII_startInstanceStruct *chartInstance, const mxArray *c56_st);
static void finalize_c56_LessonIII_start(SFc56_LessonIII_startInstanceStruct
  *chartInstance);
static void sf_gateway_c56_LessonIII_start(SFc56_LessonIII_startInstanceStruct
  *chartInstance);
static void mdl_start_c56_LessonIII_start(SFc56_LessonIII_startInstanceStruct
  *chartInstance);
static void initSimStructsc56_LessonIII_start
  (SFc56_LessonIII_startInstanceStruct *chartInstance);
static void init_script_number_translation(uint32_T c56_machineNumber, uint32_T
  c56_chartNumber, uint32_T c56_instanceNumber);
static const mxArray *c56_sf_marshallOut(void *chartInstanceVoid, void
  *c56_inData);
static void c56_emlrt_marshallIn(SFc56_LessonIII_startInstanceStruct
  *chartInstance, const mxArray *c56_b_message, const char_T *c56_identifier,
  uint8_T c56_y[31]);
static void c56_b_emlrt_marshallIn(SFc56_LessonIII_startInstanceStruct
  *chartInstance, const mxArray *c56_u, const emlrtMsgIdentifier *c56_parentId,
  uint8_T c56_y[31]);
static void c56_sf_marshallIn(void *chartInstanceVoid, const mxArray
  *c56_mxArrayInData, const char_T *c56_varName, void *c56_outData);
static const mxArray *c56_b_sf_marshallOut(void *chartInstanceVoid, uint8_T
  c56_inData_data[], int32_T c56_inData_sizes[2]);
static const mxArray *c56_c_sf_marshallOut(void *chartInstanceVoid, void
  *c56_inData);
static real_T c56_c_emlrt_marshallIn(SFc56_LessonIII_startInstanceStruct
  *chartInstance, const mxArray *c56_u, const emlrtMsgIdentifier *c56_parentId);
static void c56_b_sf_marshallIn(void *chartInstanceVoid, const mxArray
  *c56_mxArrayInData, const char_T *c56_varName, void *c56_outData);
static const mxArray *c56_d_sf_marshallOut(void *chartInstanceVoid, void
  *c56_inData);
static int32_T c56_d_emlrt_marshallIn(SFc56_LessonIII_startInstanceStruct
  *chartInstance, const mxArray *c56_u, const emlrtMsgIdentifier *c56_parentId);
static void c56_c_sf_marshallIn(void *chartInstanceVoid, const mxArray
  *c56_mxArrayInData, const char_T *c56_varName, void *c56_outData);
static uint8_T c56_e_emlrt_marshallIn(SFc56_LessonIII_startInstanceStruct
  *chartInstance, const mxArray *c56_b_is_active_c56_LessonIII_start, const
  char_T *c56_identifier);
static uint8_T c56_f_emlrt_marshallIn(SFc56_LessonIII_startInstanceStruct
  *chartInstance, const mxArray *c56_u, const emlrtMsgIdentifier *c56_parentId);
static void init_dsm_address_info(SFc56_LessonIII_startInstanceStruct
  *chartInstance);
static void init_simulink_io_address(SFc56_LessonIII_startInstanceStruct
  *chartInstance);

/* Function Definitions */
static void initialize_c56_LessonIII_start(SFc56_LessonIII_startInstanceStruct
  *chartInstance)
{
  chartInstance->c56_sfEvent = CALL_EVENT;
  _sfTime_ = sf_get_time(chartInstance->S);
  chartInstance->c56_is_active_c56_LessonIII_start = 0U;
}

static void initialize_params_c56_LessonIII_start
  (SFc56_LessonIII_startInstanceStruct *chartInstance)
{
  (void)chartInstance;
}

static void enable_c56_LessonIII_start(SFc56_LessonIII_startInstanceStruct
  *chartInstance)
{
  _sfTime_ = sf_get_time(chartInstance->S);
}

static void disable_c56_LessonIII_start(SFc56_LessonIII_startInstanceStruct
  *chartInstance)
{
  _sfTime_ = sf_get_time(chartInstance->S);
}

static void c56_update_debugger_state_c56_LessonIII_start
  (SFc56_LessonIII_startInstanceStruct *chartInstance)
{
  (void)chartInstance;
}

static const mxArray *get_sim_state_c56_LessonIII_start
  (SFc56_LessonIII_startInstanceStruct *chartInstance)
{
  const mxArray *c56_st;
  const mxArray *c56_y = NULL;
  int32_T c56_i0;
  uint8_T c56_u[31];
  const mxArray *c56_b_y = NULL;
  uint8_T c56_hoistedGlobal;
  uint8_T c56_b_u;
  const mxArray *c56_c_y = NULL;
  c56_st = NULL;
  c56_st = NULL;
  c56_y = NULL;
  sf_mex_assign(&c56_y, sf_mex_createcellmatrix(2, 1), false);
  for (c56_i0 = 0; c56_i0 < 31; c56_i0++) {
    c56_u[c56_i0] = (*chartInstance->c56_message)[c56_i0];
  }

  c56_b_y = NULL;
  sf_mex_assign(&c56_b_y, sf_mex_create("y", c56_u, 3, 0U, 1U, 0U, 2, 1, 31),
                false);
  sf_mex_setcell(c56_y, 0, c56_b_y);
  c56_hoistedGlobal = chartInstance->c56_is_active_c56_LessonIII_start;
  c56_b_u = c56_hoistedGlobal;
  c56_c_y = NULL;
  sf_mex_assign(&c56_c_y, sf_mex_create("y", &c56_b_u, 3, 0U, 0U, 0U, 0), false);
  sf_mex_setcell(c56_y, 1, c56_c_y);
  sf_mex_assign(&c56_st, c56_y, false);
  return c56_st;
}

static void set_sim_state_c56_LessonIII_start
  (SFc56_LessonIII_startInstanceStruct *chartInstance, const mxArray *c56_st)
{
  const mxArray *c56_u;
  uint8_T c56_uv0[31];
  int32_T c56_i1;
  chartInstance->c56_doneDoubleBufferReInit = true;
  c56_u = sf_mex_dup(c56_st);
  c56_emlrt_marshallIn(chartInstance, sf_mex_dup(sf_mex_getcell(c56_u, 0)),
                       "message", c56_uv0);
  for (c56_i1 = 0; c56_i1 < 31; c56_i1++) {
    (*chartInstance->c56_message)[c56_i1] = c56_uv0[c56_i1];
  }

  chartInstance->c56_is_active_c56_LessonIII_start = c56_e_emlrt_marshallIn
    (chartInstance, sf_mex_dup(sf_mex_getcell(c56_u, 1)),
     "is_active_c56_LessonIII_start");
  sf_mex_destroy(&c56_u);
  c56_update_debugger_state_c56_LessonIII_start(chartInstance);
  sf_mex_destroy(&c56_st);
}

static void finalize_c56_LessonIII_start(SFc56_LessonIII_startInstanceStruct
  *chartInstance)
{
  (void)chartInstance;
}

static void sf_gateway_c56_LessonIII_start(SFc56_LessonIII_startInstanceStruct
  *chartInstance)
{
  int32_T c56_loop_ub;
  int32_T c56_i2;
  int32_T c56_b_messageBuffer_sizes[2];
  int32_T c56_messageBuffer;
  int32_T c56_b_messageBuffer;
  int32_T c56_b_loop_ub;
  int32_T c56_i3;
  uint8_T c56_b_messageBuffer_data[64];
  uint32_T c56_debug_family_var_map[4];
  real_T c56_nargin = 1.0;
  real_T c56_nargout = 1.0;
  uint8_T c56_b_message[31];
  int32_T c56_i4;
  int32_T c56_i5;
  int32_T c56_i6;
  _SFD_SYMBOL_SCOPE_PUSH(0U, 0U);
  _sfTime_ = sf_get_time(chartInstance->S);
  _SFD_CC_CALL(CHART_ENTER_SFUNCTION_TAG, 40U, chartInstance->c56_sfEvent);
  c56_loop_ub = (*chartInstance->c56_messageBuffer_sizes)[0] *
    (*chartInstance->c56_messageBuffer_sizes)[1] - 1;
  for (c56_i2 = 0; c56_i2 <= c56_loop_ub; c56_i2++) {
    _SFD_DATA_RANGE_CHECK((real_T)(*chartInstance->c56_messageBuffer_data)
                          [c56_i2], 0U);
  }

  chartInstance->c56_sfEvent = CALL_EVENT;
  _SFD_CC_CALL(CHART_ENTER_DURING_FUNCTION_TAG, 40U, chartInstance->c56_sfEvent);
  c56_b_messageBuffer_sizes[0] = 1;
  c56_b_messageBuffer_sizes[1] = (*chartInstance->c56_messageBuffer_sizes)[1];
  c56_messageBuffer = c56_b_messageBuffer_sizes[0];
  c56_b_messageBuffer = c56_b_messageBuffer_sizes[1];
  c56_b_loop_ub = (*chartInstance->c56_messageBuffer_sizes)[0] *
    (*chartInstance->c56_messageBuffer_sizes)[1] - 1;
  for (c56_i3 = 0; c56_i3 <= c56_b_loop_ub; c56_i3++) {
    c56_b_messageBuffer_data[c56_i3] = (*chartInstance->c56_messageBuffer_data)
      [c56_i3];
  }

  _SFD_SYMBOL_SCOPE_PUSH_EML(0U, 4U, 4U, c56_debug_family_names,
    c56_debug_family_var_map);
  _SFD_SYMBOL_SCOPE_ADD_EML_IMPORTABLE(&c56_nargin, 0U, c56_c_sf_marshallOut,
    c56_b_sf_marshallIn);
  _SFD_SYMBOL_SCOPE_ADD_EML_IMPORTABLE(&c56_nargout, 1U, c56_c_sf_marshallOut,
    c56_b_sf_marshallIn);
  _SFD_SYMBOL_SCOPE_ADD_EML_DYN(c56_b_messageBuffer_data, (const int32_T *)
    &c56_b_messageBuffer_sizes, NULL, 1, 2, (void *)c56_b_sf_marshallOut);
  _SFD_SYMBOL_SCOPE_ADD_EML_IMPORTABLE(c56_b_message, 3U, c56_sf_marshallOut,
    c56_sf_marshallIn);
  CV_EML_FCN(0, 0);
  _SFD_EML_CALL(0U, chartInstance->c56_sfEvent, 2);
  for (c56_i4 = 0; c56_i4 < 31; c56_i4++) {
    c56_b_message[c56_i4] = c56_b_messageBuffer_data[_SFD_EML_ARRAY_BOUNDS_CHECK
      ("messageBuffer", c56_i4 + 1, 1, c56_b_messageBuffer_sizes[1], 1, 0) - 1];
  }

  _SFD_EML_CALL(0U, chartInstance->c56_sfEvent, -2);
  _SFD_SYMBOL_SCOPE_POP();
  for (c56_i5 = 0; c56_i5 < 31; c56_i5++) {
    (*chartInstance->c56_message)[c56_i5] = c56_b_message[c56_i5];
  }

  _SFD_CC_CALL(EXIT_OUT_OF_FUNCTION_TAG, 40U, chartInstance->c56_sfEvent);
  _SFD_SYMBOL_SCOPE_POP();
  _SFD_CHECK_FOR_STATE_INCONSISTENCY(_LessonIII_startMachineNumber_,
    chartInstance->chartNumber, chartInstance->instanceNumber);
  for (c56_i6 = 0; c56_i6 < 31; c56_i6++) {
    _SFD_DATA_RANGE_CHECK((real_T)(*chartInstance->c56_message)[c56_i6], 1U);
  }
}

static void mdl_start_c56_LessonIII_start(SFc56_LessonIII_startInstanceStruct
  *chartInstance)
{
  (void)chartInstance;
}

static void initSimStructsc56_LessonIII_start
  (SFc56_LessonIII_startInstanceStruct *chartInstance)
{
  (void)chartInstance;
}

static void init_script_number_translation(uint32_T c56_machineNumber, uint32_T
  c56_chartNumber, uint32_T c56_instanceNumber)
{
  (void)c56_machineNumber;
  (void)c56_chartNumber;
  (void)c56_instanceNumber;
}

static const mxArray *c56_sf_marshallOut(void *chartInstanceVoid, void
  *c56_inData)
{
  const mxArray *c56_mxArrayOutData = NULL;
  int32_T c56_i7;
  uint8_T c56_b_inData[31];
  int32_T c56_i8;
  uint8_T c56_u[31];
  const mxArray *c56_y = NULL;
  SFc56_LessonIII_startInstanceStruct *chartInstance;
  chartInstance = (SFc56_LessonIII_startInstanceStruct *)chartInstanceVoid;
  c56_mxArrayOutData = NULL;
  for (c56_i7 = 0; c56_i7 < 31; c56_i7++) {
    c56_b_inData[c56_i7] = (*(uint8_T (*)[31])c56_inData)[c56_i7];
  }

  for (c56_i8 = 0; c56_i8 < 31; c56_i8++) {
    c56_u[c56_i8] = c56_b_inData[c56_i8];
  }

  c56_y = NULL;
  sf_mex_assign(&c56_y, sf_mex_create("y", c56_u, 3, 0U, 1U, 0U, 2, 1, 31),
                false);
  sf_mex_assign(&c56_mxArrayOutData, c56_y, false);
  return c56_mxArrayOutData;
}

static void c56_emlrt_marshallIn(SFc56_LessonIII_startInstanceStruct
  *chartInstance, const mxArray *c56_b_message, const char_T *c56_identifier,
  uint8_T c56_y[31])
{
  emlrtMsgIdentifier c56_thisId;
  c56_thisId.fIdentifier = c56_identifier;
  c56_thisId.fParent = NULL;
  c56_b_emlrt_marshallIn(chartInstance, sf_mex_dup(c56_b_message), &c56_thisId,
    c56_y);
  sf_mex_destroy(&c56_b_message);
}

static void c56_b_emlrt_marshallIn(SFc56_LessonIII_startInstanceStruct
  *chartInstance, const mxArray *c56_u, const emlrtMsgIdentifier *c56_parentId,
  uint8_T c56_y[31])
{
  uint8_T c56_uv1[31];
  int32_T c56_i9;
  (void)chartInstance;
  sf_mex_import(c56_parentId, sf_mex_dup(c56_u), c56_uv1, 1, 3, 0U, 1, 0U, 2, 1,
                31);
  for (c56_i9 = 0; c56_i9 < 31; c56_i9++) {
    c56_y[c56_i9] = c56_uv1[c56_i9];
  }

  sf_mex_destroy(&c56_u);
}

static void c56_sf_marshallIn(void *chartInstanceVoid, const mxArray
  *c56_mxArrayInData, const char_T *c56_varName, void *c56_outData)
{
  const mxArray *c56_b_message;
  const char_T *c56_identifier;
  emlrtMsgIdentifier c56_thisId;
  uint8_T c56_y[31];
  int32_T c56_i10;
  SFc56_LessonIII_startInstanceStruct *chartInstance;
  chartInstance = (SFc56_LessonIII_startInstanceStruct *)chartInstanceVoid;
  c56_b_message = sf_mex_dup(c56_mxArrayInData);
  c56_identifier = c56_varName;
  c56_thisId.fIdentifier = c56_identifier;
  c56_thisId.fParent = NULL;
  c56_b_emlrt_marshallIn(chartInstance, sf_mex_dup(c56_b_message), &c56_thisId,
    c56_y);
  sf_mex_destroy(&c56_b_message);
  for (c56_i10 = 0; c56_i10 < 31; c56_i10++) {
    (*(uint8_T (*)[31])c56_outData)[c56_i10] = c56_y[c56_i10];
  }

  sf_mex_destroy(&c56_mxArrayInData);
}

static const mxArray *c56_b_sf_marshallOut(void *chartInstanceVoid, uint8_T
  c56_inData_data[], int32_T c56_inData_sizes[2])
{
  const mxArray *c56_mxArrayOutData = NULL;
  int32_T c56_u_sizes[2];
  int32_T c56_u;
  int32_T c56_b_u;
  int32_T c56_inData;
  int32_T c56_b_inData;
  int32_T c56_b_inData_sizes;
  int32_T c56_loop_ub;
  int32_T c56_i11;
  uint8_T c56_b_inData_data[64];
  int32_T c56_b_loop_ub;
  int32_T c56_i12;
  uint8_T c56_u_data[64];
  const mxArray *c56_y = NULL;
  SFc56_LessonIII_startInstanceStruct *chartInstance;
  chartInstance = (SFc56_LessonIII_startInstanceStruct *)chartInstanceVoid;
  c56_mxArrayOutData = NULL;
  c56_u_sizes[0] = 1;
  c56_u_sizes[1] = c56_inData_sizes[1];
  c56_u = c56_u_sizes[0];
  c56_b_u = c56_u_sizes[1];
  c56_inData = c56_inData_sizes[0];
  c56_b_inData = c56_inData_sizes[1];
  c56_b_inData_sizes = c56_inData * c56_b_inData;
  c56_loop_ub = c56_inData * c56_b_inData - 1;
  for (c56_i11 = 0; c56_i11 <= c56_loop_ub; c56_i11++) {
    c56_b_inData_data[c56_i11] = c56_inData_data[c56_i11];
  }

  c56_b_loop_ub = c56_b_inData_sizes - 1;
  for (c56_i12 = 0; c56_i12 <= c56_b_loop_ub; c56_i12++) {
    c56_u_data[c56_i12] = c56_b_inData_data[c56_i12];
  }

  c56_y = NULL;
  sf_mex_assign(&c56_y, sf_mex_create("y", c56_u_data, 3, 0U, 1U, 0U, 2,
    c56_u_sizes[0], c56_u_sizes[1]), false);
  sf_mex_assign(&c56_mxArrayOutData, c56_y, false);
  return c56_mxArrayOutData;
}

static const mxArray *c56_c_sf_marshallOut(void *chartInstanceVoid, void
  *c56_inData)
{
  const mxArray *c56_mxArrayOutData = NULL;
  real_T c56_u;
  const mxArray *c56_y = NULL;
  SFc56_LessonIII_startInstanceStruct *chartInstance;
  chartInstance = (SFc56_LessonIII_startInstanceStruct *)chartInstanceVoid;
  c56_mxArrayOutData = NULL;
  c56_u = *(real_T *)c56_inData;
  c56_y = NULL;
  sf_mex_assign(&c56_y, sf_mex_create("y", &c56_u, 0, 0U, 0U, 0U, 0), false);
  sf_mex_assign(&c56_mxArrayOutData, c56_y, false);
  return c56_mxArrayOutData;
}

static real_T c56_c_emlrt_marshallIn(SFc56_LessonIII_startInstanceStruct
  *chartInstance, const mxArray *c56_u, const emlrtMsgIdentifier *c56_parentId)
{
  real_T c56_y;
  real_T c56_d0;
  (void)chartInstance;
  sf_mex_import(c56_parentId, sf_mex_dup(c56_u), &c56_d0, 1, 0, 0U, 0, 0U, 0);
  c56_y = c56_d0;
  sf_mex_destroy(&c56_u);
  return c56_y;
}

static void c56_b_sf_marshallIn(void *chartInstanceVoid, const mxArray
  *c56_mxArrayInData, const char_T *c56_varName, void *c56_outData)
{
  const mxArray *c56_nargout;
  const char_T *c56_identifier;
  emlrtMsgIdentifier c56_thisId;
  real_T c56_y;
  SFc56_LessonIII_startInstanceStruct *chartInstance;
  chartInstance = (SFc56_LessonIII_startInstanceStruct *)chartInstanceVoid;
  c56_nargout = sf_mex_dup(c56_mxArrayInData);
  c56_identifier = c56_varName;
  c56_thisId.fIdentifier = c56_identifier;
  c56_thisId.fParent = NULL;
  c56_y = c56_c_emlrt_marshallIn(chartInstance, sf_mex_dup(c56_nargout),
    &c56_thisId);
  sf_mex_destroy(&c56_nargout);
  *(real_T *)c56_outData = c56_y;
  sf_mex_destroy(&c56_mxArrayInData);
}

const mxArray *sf_c56_LessonIII_start_get_eml_resolved_functions_info(void)
{
  const mxArray *c56_nameCaptureInfo = NULL;
  c56_nameCaptureInfo = NULL;
  sf_mex_assign(&c56_nameCaptureInfo, sf_mex_create("nameCaptureInfo", NULL, 0,
    0U, 1U, 0U, 2, 0, 1), false);
  return c56_nameCaptureInfo;
}

static const mxArray *c56_d_sf_marshallOut(void *chartInstanceVoid, void
  *c56_inData)
{
  const mxArray *c56_mxArrayOutData = NULL;
  int32_T c56_u;
  const mxArray *c56_y = NULL;
  SFc56_LessonIII_startInstanceStruct *chartInstance;
  chartInstance = (SFc56_LessonIII_startInstanceStruct *)chartInstanceVoid;
  c56_mxArrayOutData = NULL;
  c56_u = *(int32_T *)c56_inData;
  c56_y = NULL;
  sf_mex_assign(&c56_y, sf_mex_create("y", &c56_u, 6, 0U, 0U, 0U, 0), false);
  sf_mex_assign(&c56_mxArrayOutData, c56_y, false);
  return c56_mxArrayOutData;
}

static int32_T c56_d_emlrt_marshallIn(SFc56_LessonIII_startInstanceStruct
  *chartInstance, const mxArray *c56_u, const emlrtMsgIdentifier *c56_parentId)
{
  int32_T c56_y;
  int32_T c56_i13;
  (void)chartInstance;
  sf_mex_import(c56_parentId, sf_mex_dup(c56_u), &c56_i13, 1, 6, 0U, 0, 0U, 0);
  c56_y = c56_i13;
  sf_mex_destroy(&c56_u);
  return c56_y;
}

static void c56_c_sf_marshallIn(void *chartInstanceVoid, const mxArray
  *c56_mxArrayInData, const char_T *c56_varName, void *c56_outData)
{
  const mxArray *c56_b_sfEvent;
  const char_T *c56_identifier;
  emlrtMsgIdentifier c56_thisId;
  int32_T c56_y;
  SFc56_LessonIII_startInstanceStruct *chartInstance;
  chartInstance = (SFc56_LessonIII_startInstanceStruct *)chartInstanceVoid;
  c56_b_sfEvent = sf_mex_dup(c56_mxArrayInData);
  c56_identifier = c56_varName;
  c56_thisId.fIdentifier = c56_identifier;
  c56_thisId.fParent = NULL;
  c56_y = c56_d_emlrt_marshallIn(chartInstance, sf_mex_dup(c56_b_sfEvent),
    &c56_thisId);
  sf_mex_destroy(&c56_b_sfEvent);
  *(int32_T *)c56_outData = c56_y;
  sf_mex_destroy(&c56_mxArrayInData);
}

static uint8_T c56_e_emlrt_marshallIn(SFc56_LessonIII_startInstanceStruct
  *chartInstance, const mxArray *c56_b_is_active_c56_LessonIII_start, const
  char_T *c56_identifier)
{
  uint8_T c56_y;
  emlrtMsgIdentifier c56_thisId;
  c56_thisId.fIdentifier = c56_identifier;
  c56_thisId.fParent = NULL;
  c56_y = c56_f_emlrt_marshallIn(chartInstance, sf_mex_dup
    (c56_b_is_active_c56_LessonIII_start), &c56_thisId);
  sf_mex_destroy(&c56_b_is_active_c56_LessonIII_start);
  return c56_y;
}

static uint8_T c56_f_emlrt_marshallIn(SFc56_LessonIII_startInstanceStruct
  *chartInstance, const mxArray *c56_u, const emlrtMsgIdentifier *c56_parentId)
{
  uint8_T c56_y;
  uint8_T c56_u0;
  (void)chartInstance;
  sf_mex_import(c56_parentId, sf_mex_dup(c56_u), &c56_u0, 1, 3, 0U, 0, 0U, 0);
  c56_y = c56_u0;
  sf_mex_destroy(&c56_u);
  return c56_y;
}

static void init_dsm_address_info(SFc56_LessonIII_startInstanceStruct
  *chartInstance)
{
  (void)chartInstance;
}

static void init_simulink_io_address(SFc56_LessonIII_startInstanceStruct
  *chartInstance)
{
  chartInstance->c56_messageBuffer_data = (uint8_T (*)[64])
    ssGetInputPortSignal_wrapper(chartInstance->S, 0);
  chartInstance->c56_messageBuffer_sizes = (int32_T (*)[2])
    ssGetCurrentInputPortDimensions_wrapper(chartInstance->S, 0);
  sf_mex_size_one_check(((*chartInstance->c56_messageBuffer_sizes)[0U] == 0) &&
                        (!((*chartInstance->c56_messageBuffer_sizes)[1U] == 0)),
                        "messageBuffer");
  chartInstance->c56_message = (uint8_T (*)[31])ssGetOutputPortSignal_wrapper
    (chartInstance->S, 1);
}

/* SFunction Glue Code */
#ifdef utFree
#undef utFree
#endif

#ifdef utMalloc
#undef utMalloc
#endif

#ifdef __cplusplus

extern "C" void *utMalloc(size_t size);
extern "C" void utFree(void*);

#else

extern void *utMalloc(size_t size);
extern void utFree(void*);

#endif

void sf_c56_LessonIII_start_get_check_sum(mxArray *plhs[])
{
  ((real_T *)mxGetPr((plhs[0])))[0] = (real_T)(1042695787U);
  ((real_T *)mxGetPr((plhs[0])))[1] = (real_T)(2659539410U);
  ((real_T *)mxGetPr((plhs[0])))[2] = (real_T)(1804896145U);
  ((real_T *)mxGetPr((plhs[0])))[3] = (real_T)(1827938802U);
}

mxArray* sf_c56_LessonIII_start_get_post_codegen_info(void);
mxArray *sf_c56_LessonIII_start_get_autoinheritance_info(void)
{
  const char *autoinheritanceFields[] = { "checksum", "inputs", "parameters",
    "outputs", "locals", "postCodegenInfo" };

  mxArray *mxAutoinheritanceInfo = mxCreateStructMatrix(1, 1, sizeof
    (autoinheritanceFields)/sizeof(autoinheritanceFields[0]),
    autoinheritanceFields);

  {
    mxArray *mxChecksum = mxCreateString("2z7Fwh6WZoLlXSSoEUax0D");
    mxSetField(mxAutoinheritanceInfo,0,"checksum",mxChecksum);
  }

  {
    const char *dataFields[] = { "size", "type", "complexity" };

    mxArray *mxData = mxCreateStructMatrix(1,1,3,dataFields);

    {
      mxArray *mxSize = mxCreateDoubleMatrix(1,2,mxREAL);
      double *pr = mxGetPr(mxSize);
      pr[0] = (double)(1);
      pr[1] = (double)(64);
      mxSetField(mxData,0,"size",mxSize);
    }

    {
      const char *typeFields[] = { "base", "fixpt" };

      mxArray *mxType = mxCreateStructMatrix(1,1,2,typeFields);
      mxSetField(mxType,0,"base",mxCreateDoubleScalar(3));
      mxSetField(mxType,0,"fixpt",mxCreateDoubleMatrix(0,0,mxREAL));
      mxSetField(mxData,0,"type",mxType);
    }

    mxSetField(mxData,0,"complexity",mxCreateDoubleScalar(0));
    mxSetField(mxAutoinheritanceInfo,0,"inputs",mxData);
  }

  {
    mxSetField(mxAutoinheritanceInfo,0,"parameters",mxCreateDoubleMatrix(0,0,
                mxREAL));
  }

  {
    const char *dataFields[] = { "size", "type", "complexity" };

    mxArray *mxData = mxCreateStructMatrix(1,1,3,dataFields);

    {
      mxArray *mxSize = mxCreateDoubleMatrix(1,2,mxREAL);
      double *pr = mxGetPr(mxSize);
      pr[0] = (double)(1);
      pr[1] = (double)(31);
      mxSetField(mxData,0,"size",mxSize);
    }

    {
      const char *typeFields[] = { "base", "fixpt" };

      mxArray *mxType = mxCreateStructMatrix(1,1,2,typeFields);
      mxSetField(mxType,0,"base",mxCreateDoubleScalar(3));
      mxSetField(mxType,0,"fixpt",mxCreateDoubleMatrix(0,0,mxREAL));
      mxSetField(mxData,0,"type",mxType);
    }

    mxSetField(mxData,0,"complexity",mxCreateDoubleScalar(0));
    mxSetField(mxAutoinheritanceInfo,0,"outputs",mxData);
  }

  {
    mxSetField(mxAutoinheritanceInfo,0,"locals",mxCreateDoubleMatrix(0,0,mxREAL));
  }

  {
    mxArray* mxPostCodegenInfo = sf_c56_LessonIII_start_get_post_codegen_info();
    mxSetField(mxAutoinheritanceInfo,0,"postCodegenInfo",mxPostCodegenInfo);
  }

  return(mxAutoinheritanceInfo);
}

mxArray *sf_c56_LessonIII_start_third_party_uses_info(void)
{
  mxArray * mxcell3p = mxCreateCellMatrix(1,0);
  return(mxcell3p);
}

mxArray *sf_c56_LessonIII_start_jit_fallback_info(void)
{
  const char *infoFields[] = { "fallbackType", "fallbackReason",
    "incompatibleSymbol", };

  mxArray *mxInfo = mxCreateStructMatrix(1, 1, 3, infoFields);
  mxArray *fallbackReason = mxCreateString("feature_off");
  mxArray *incompatibleSymbol = mxCreateString("");
  mxArray *fallbackType = mxCreateString("early");
  mxSetField(mxInfo, 0, infoFields[0], fallbackType);
  mxSetField(mxInfo, 0, infoFields[1], fallbackReason);
  mxSetField(mxInfo, 0, infoFields[2], incompatibleSymbol);
  return mxInfo;
}

mxArray *sf_c56_LessonIII_start_updateBuildInfo_args_info(void)
{
  mxArray *mxBIArgs = mxCreateCellMatrix(1,0);
  return mxBIArgs;
}

mxArray* sf_c56_LessonIII_start_get_post_codegen_info(void)
{
  const char* fieldNames[] = { "exportedFunctionsUsedByThisChart",
    "exportedFunctionsChecksum" };

  mwSize dims[2] = { 1, 1 };

  mxArray* mxPostCodegenInfo = mxCreateStructArray(2, dims, sizeof(fieldNames)/
    sizeof(fieldNames[0]), fieldNames);

  {
    mxArray* mxExportedFunctionsChecksum = mxCreateString("");
    mwSize exp_dims[2] = { 0, 1 };

    mxArray* mxExportedFunctionsUsedByThisChart = mxCreateCellArray(2, exp_dims);
    mxSetField(mxPostCodegenInfo, 0, "exportedFunctionsUsedByThisChart",
               mxExportedFunctionsUsedByThisChart);
    mxSetField(mxPostCodegenInfo, 0, "exportedFunctionsChecksum",
               mxExportedFunctionsChecksum);
  }

  return mxPostCodegenInfo;
}

static const mxArray *sf_get_sim_state_info_c56_LessonIII_start(void)
{
  const char *infoFields[] = { "chartChecksum", "varInfo" };

  mxArray *mxInfo = mxCreateStructMatrix(1, 1, 2, infoFields);
  const char *infoEncStr[] = {
    "100 S1x2'type','srcId','name','auxInfo'{{M[1],M[5],T\"message\",},{M[8],M[0],T\"is_active_c56_LessonIII_start\",}}"
  };

  mxArray *mxVarInfo = sf_mex_decode_encoded_mx_struct_array(infoEncStr, 2, 10);
  mxArray *mxChecksum = mxCreateDoubleMatrix(1, 4, mxREAL);
  sf_c56_LessonIII_start_get_check_sum(&mxChecksum);
  mxSetField(mxInfo, 0, infoFields[0], mxChecksum);
  mxSetField(mxInfo, 0, infoFields[1], mxVarInfo);
  return mxInfo;
}

static void chart_debug_initialization(SimStruct *S, unsigned int
  fullDebuggerInitialization)
{
  if (!sim_mode_is_rtw_gen(S)) {
    SFc56_LessonIII_startInstanceStruct *chartInstance;
    ChartRunTimeInfo * crtInfo = (ChartRunTimeInfo *)(ssGetUserData(S));
    ChartInfoStruct * chartInfo = (ChartInfoStruct *)(crtInfo->instanceInfo);
    chartInstance = (SFc56_LessonIII_startInstanceStruct *)
      chartInfo->chartInstance;
    if (ssIsFirstInitCond(S) && fullDebuggerInitialization==1) {
      /* do this only if simulation is starting */
      {
        unsigned int chartAlreadyPresent;
        chartAlreadyPresent = sf_debug_initialize_chart
          (sfGlobalDebugInstanceStruct,
           _LessonIII_startMachineNumber_,
           56,
           1,
           1,
           0,
           2,
           0,
           0,
           0,
           0,
           0,
           &(chartInstance->chartNumber),
           &(chartInstance->instanceNumber),
           (void *)S);

        /* Each instance must initialize its own list of scripts */
        init_script_number_translation(_LessonIII_startMachineNumber_,
          chartInstance->chartNumber,chartInstance->instanceNumber);
        if (chartAlreadyPresent==0) {
          /* this is the first instance */
          sf_debug_set_chart_disable_implicit_casting
            (sfGlobalDebugInstanceStruct,_LessonIII_startMachineNumber_,
             chartInstance->chartNumber,1);
          sf_debug_set_chart_event_thresholds(sfGlobalDebugInstanceStruct,
            _LessonIII_startMachineNumber_,
            chartInstance->chartNumber,
            0,
            0,
            0);
          _SFD_SET_DATA_PROPS(0,1,1,0,"messageBuffer");
          _SFD_SET_DATA_PROPS(1,2,0,1,"message");
          _SFD_STATE_INFO(0,0,2);
          _SFD_CH_SUBSTATE_COUNT(0);
          _SFD_CH_SUBSTATE_DECOMP(0);
        }

        _SFD_CV_INIT_CHART(0,0,0,0);

        {
          _SFD_CV_INIT_STATE(0,0,0,0,0,0,NULL,NULL);
        }

        _SFD_CV_INIT_TRANS(0,0,NULL,NULL,0,NULL);

        /* Initialization of MATLAB Function Model Coverage */
        _SFD_CV_INIT_EML(0,1,1,0,0,0,0,0,0,0,0);
        _SFD_CV_INIT_EML_FCN(0,0,"eML_blk_kernel",0,-1,81);

        {
          unsigned int dimVector[2];
          dimVector[0]= 1;
          dimVector[1]= 64;
          _SFD_SET_DATA_COMPILED_PROPS(0,SF_UINT8,2,&(dimVector[0]),0,0,0,0.0,
            1.0,0,0,(MexFcnForType)c56_b_sf_marshallOut,(MexInFcnForType)NULL);
        }

        {
          unsigned int dimVector[2];
          dimVector[0]= 1;
          dimVector[1]= 31;
          _SFD_SET_DATA_COMPILED_PROPS(1,SF_UINT8,2,&(dimVector[0]),0,0,0,0.0,
            1.0,0,0,(MexFcnForType)c56_sf_marshallOut,(MexInFcnForType)
            c56_sf_marshallIn);
        }

        _SFD_SET_DATA_VALUE_PTR_VAR_DIM(0U,
          *chartInstance->c56_messageBuffer_data, (void *)
          chartInstance->c56_messageBuffer_sizes);
        _SFD_SET_DATA_VALUE_PTR(1U, *chartInstance->c56_message);
      }
    } else {
      sf_debug_reset_current_state_configuration(sfGlobalDebugInstanceStruct,
        _LessonIII_startMachineNumber_,chartInstance->chartNumber,
        chartInstance->instanceNumber);
    }
  }
}

static const char* sf_get_instance_specialization(void)
{
  return "xNYm6GNN8jSBExJBkcuC2B";
}

static void sf_opaque_initialize_c56_LessonIII_start(void *chartInstanceVar)
{
  chart_debug_initialization(((SFc56_LessonIII_startInstanceStruct*)
    chartInstanceVar)->S,0);
  initialize_params_c56_LessonIII_start((SFc56_LessonIII_startInstanceStruct*)
    chartInstanceVar);
  initialize_c56_LessonIII_start((SFc56_LessonIII_startInstanceStruct*)
    chartInstanceVar);
}

static void sf_opaque_enable_c56_LessonIII_start(void *chartInstanceVar)
{
  enable_c56_LessonIII_start((SFc56_LessonIII_startInstanceStruct*)
    chartInstanceVar);
}

static void sf_opaque_disable_c56_LessonIII_start(void *chartInstanceVar)
{
  disable_c56_LessonIII_start((SFc56_LessonIII_startInstanceStruct*)
    chartInstanceVar);
}

static void sf_opaque_gateway_c56_LessonIII_start(void *chartInstanceVar)
{
  sf_gateway_c56_LessonIII_start((SFc56_LessonIII_startInstanceStruct*)
    chartInstanceVar);
}

static const mxArray* sf_opaque_get_sim_state_c56_LessonIII_start(SimStruct* S)
{
  ChartRunTimeInfo * crtInfo = (ChartRunTimeInfo *)(ssGetUserData(S));
  ChartInfoStruct * chartInfo = (ChartInfoStruct *)(crtInfo->instanceInfo);
  return get_sim_state_c56_LessonIII_start((SFc56_LessonIII_startInstanceStruct*)
    chartInfo->chartInstance);         /* raw sim ctx */
}

static void sf_opaque_set_sim_state_c56_LessonIII_start(SimStruct* S, const
  mxArray *st)
{
  ChartRunTimeInfo * crtInfo = (ChartRunTimeInfo *)(ssGetUserData(S));
  ChartInfoStruct * chartInfo = (ChartInfoStruct *)(crtInfo->instanceInfo);
  set_sim_state_c56_LessonIII_start((SFc56_LessonIII_startInstanceStruct*)
    chartInfo->chartInstance, st);
}

static void sf_opaque_terminate_c56_LessonIII_start(void *chartInstanceVar)
{
  if (chartInstanceVar!=NULL) {
    SimStruct *S = ((SFc56_LessonIII_startInstanceStruct*) chartInstanceVar)->S;
    ChartRunTimeInfo * crtInfo = (ChartRunTimeInfo *)(ssGetUserData(S));
    if (sim_mode_is_rtw_gen(S) || sim_mode_is_external(S)) {
      sf_clear_rtw_identifier(S);
      unload_LessonIII_start_optimization_info();
    }

    finalize_c56_LessonIII_start((SFc56_LessonIII_startInstanceStruct*)
      chartInstanceVar);
    utFree(chartInstanceVar);
    if (crtInfo != NULL) {
      utFree(crtInfo);
    }

    ssSetUserData(S,NULL);
  }
}

static void sf_opaque_init_subchart_simstructs(void *chartInstanceVar)
{
  initSimStructsc56_LessonIII_start((SFc56_LessonIII_startInstanceStruct*)
    chartInstanceVar);
}

extern unsigned int sf_machine_global_initializer_called(void);
static void mdlProcessParameters_c56_LessonIII_start(SimStruct *S)
{
  int i;
  for (i=0;i<ssGetNumRunTimeParams(S);i++) {
    if (ssGetSFcnParamTunable(S,i)) {
      ssUpdateDlgParamAsRunTimeParam(S,i);
    }
  }

  if (sf_machine_global_initializer_called()) {
    ChartRunTimeInfo * crtInfo = (ChartRunTimeInfo *)(ssGetUserData(S));
    ChartInfoStruct * chartInfo = (ChartInfoStruct *)(crtInfo->instanceInfo);
    initialize_params_c56_LessonIII_start((SFc56_LessonIII_startInstanceStruct*)
      (chartInfo->chartInstance));
  }
}

static void mdlSetWorkWidths_c56_LessonIII_start(SimStruct *S)
{
  if (sim_mode_is_rtw_gen(S) || sim_mode_is_external(S)) {
    mxArray *infoStruct = load_LessonIII_start_optimization_info();
    int_T chartIsInlinable =
      (int_T)sf_is_chart_inlinable(sf_get_instance_specialization(),infoStruct,
      56);
    ssSetStateflowIsInlinable(S,chartIsInlinable);
    ssSetRTWCG(S,sf_rtw_info_uint_prop(sf_get_instance_specialization(),
                infoStruct,56,"RTWCG"));
    ssSetEnableFcnIsTrivial(S,1);
    ssSetDisableFcnIsTrivial(S,1);
    ssSetNotMultipleInlinable(S,sf_rtw_info_uint_prop
      (sf_get_instance_specialization(),infoStruct,56,
       "gatewayCannotBeInlinedMultipleTimes"));
    sf_update_buildInfo(sf_get_instance_specialization(),infoStruct,56);
    if (chartIsInlinable) {
      ssSetInputPortOptimOpts(S, 0, SS_REUSABLE_AND_LOCAL);
      sf_mark_chart_expressionable_inputs(S,sf_get_instance_specialization(),
        infoStruct,56,1);
      sf_mark_chart_reusable_outputs(S,sf_get_instance_specialization(),
        infoStruct,56,1);
    }

    {
      unsigned int outPortIdx;
      for (outPortIdx=1; outPortIdx<=1; ++outPortIdx) {
        ssSetOutputPortOptimizeInIR(S, outPortIdx, 1U);
      }
    }

    {
      unsigned int inPortIdx;
      for (inPortIdx=0; inPortIdx < 1; ++inPortIdx) {
        ssSetInputPortOptimizeInIR(S, inPortIdx, 1U);
      }
    }

    sf_set_rtw_dwork_info(S,sf_get_instance_specialization(),infoStruct,56);
    ssSetHasSubFunctions(S,!(chartIsInlinable));
  } else {
  }

  ssSetOptions(S,ssGetOptions(S)|SS_OPTION_WORKS_WITH_CODE_REUSE);
  ssSetChecksum0(S,(2041924168U));
  ssSetChecksum1(S,(2094826546U));
  ssSetChecksum2(S,(3970551402U));
  ssSetChecksum3(S,(769607429U));
  ssSetmdlDerivatives(S, NULL);
  ssSetExplicitFCSSCtrl(S,1);
  ssSupportsMultipleExecInstances(S,1);
}

static void mdlRTW_c56_LessonIII_start(SimStruct *S)
{
  if (sim_mode_is_rtw_gen(S)) {
    ssWriteRTWStrParam(S, "StateflowChartType", "Embedded MATLAB");
  }
}

static void mdlStart_c56_LessonIII_start(SimStruct *S)
{
  SFc56_LessonIII_startInstanceStruct *chartInstance;
  ChartRunTimeInfo * crtInfo = (ChartRunTimeInfo *)utMalloc(sizeof
    (ChartRunTimeInfo));
  chartInstance = (SFc56_LessonIII_startInstanceStruct *)utMalloc(sizeof
    (SFc56_LessonIII_startInstanceStruct));
  memset(chartInstance, 0, sizeof(SFc56_LessonIII_startInstanceStruct));
  if (chartInstance==NULL) {
    sf_mex_error_message("Could not allocate memory for chart instance.");
  }

  chartInstance->chartInfo.chartInstance = chartInstance;
  chartInstance->chartInfo.isEMLChart = 1;
  chartInstance->chartInfo.chartInitialized = 0;
  chartInstance->chartInfo.sFunctionGateway =
    sf_opaque_gateway_c56_LessonIII_start;
  chartInstance->chartInfo.initializeChart =
    sf_opaque_initialize_c56_LessonIII_start;
  chartInstance->chartInfo.terminateChart =
    sf_opaque_terminate_c56_LessonIII_start;
  chartInstance->chartInfo.enableChart = sf_opaque_enable_c56_LessonIII_start;
  chartInstance->chartInfo.disableChart = sf_opaque_disable_c56_LessonIII_start;
  chartInstance->chartInfo.getSimState =
    sf_opaque_get_sim_state_c56_LessonIII_start;
  chartInstance->chartInfo.setSimState =
    sf_opaque_set_sim_state_c56_LessonIII_start;
  chartInstance->chartInfo.getSimStateInfo =
    sf_get_sim_state_info_c56_LessonIII_start;
  chartInstance->chartInfo.zeroCrossings = NULL;
  chartInstance->chartInfo.outputs = NULL;
  chartInstance->chartInfo.derivatives = NULL;
  chartInstance->chartInfo.mdlRTW = mdlRTW_c56_LessonIII_start;
  chartInstance->chartInfo.mdlStart = mdlStart_c56_LessonIII_start;
  chartInstance->chartInfo.mdlSetWorkWidths =
    mdlSetWorkWidths_c56_LessonIII_start;
  chartInstance->chartInfo.extModeExec = NULL;
  chartInstance->chartInfo.restoreLastMajorStepConfiguration = NULL;
  chartInstance->chartInfo.restoreBeforeLastMajorStepConfiguration = NULL;
  chartInstance->chartInfo.storeCurrentConfiguration = NULL;
  chartInstance->chartInfo.callAtomicSubchartUserFcn = NULL;
  chartInstance->chartInfo.callAtomicSubchartAutoFcn = NULL;
  chartInstance->chartInfo.debugInstance = sfGlobalDebugInstanceStruct;
  chartInstance->S = S;
  crtInfo->checksum = SF_RUNTIME_INFO_CHECKSUM;
  crtInfo->instanceInfo = (&(chartInstance->chartInfo));
  crtInfo->isJITEnabled = false;
  crtInfo->compiledInfo = NULL;
  ssSetUserData(S,(void *)(crtInfo));  /* register the chart instance with simstruct */
  init_dsm_address_info(chartInstance);
  init_simulink_io_address(chartInstance);
  if (!sim_mode_is_rtw_gen(S)) {
  }

  sf_opaque_init_subchart_simstructs(chartInstance->chartInfo.chartInstance);
  chart_debug_initialization(S,1);
}

void c56_LessonIII_start_method_dispatcher(SimStruct *S, int_T method, void
  *data)
{
  switch (method) {
   case SS_CALL_MDL_START:
    mdlStart_c56_LessonIII_start(S);
    break;

   case SS_CALL_MDL_SET_WORK_WIDTHS:
    mdlSetWorkWidths_c56_LessonIII_start(S);
    break;

   case SS_CALL_MDL_PROCESS_PARAMETERS:
    mdlProcessParameters_c56_LessonIII_start(S);
    break;

   default:
    /* Unhandled method */
    sf_mex_error_message("Stateflow Internal Error:\n"
                         "Error calling c56_LessonIII_start_method_dispatcher.\n"
                         "Can't handle method %d.\n", method);
    break;
  }
}

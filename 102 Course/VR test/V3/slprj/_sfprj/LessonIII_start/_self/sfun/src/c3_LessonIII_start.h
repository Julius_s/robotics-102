#ifndef __c3_LessonIII_start_h__
#define __c3_LessonIII_start_h__

/* Include files */
#include "sf_runtime/sfc_sf.h"
#include "sf_runtime/sfc_mex.h"
#include "rtwtypes.h"
#include "multiword_types.h"

/* Type Definitions */
#ifndef struct_BlobData_tag
#define struct_BlobData_tag

struct BlobData_tag
{
  real32_T centroid[32];
  int32_T area[16];
  uint8_T color[16];
};

#endif                                 /*struct_BlobData_tag*/

#ifndef typedef_c3_BlobData
#define typedef_c3_BlobData

typedef struct BlobData_tag c3_BlobData;

#endif                                 /*typedef_c3_BlobData*/

#ifndef struct_BlobData_tag_size
#define struct_BlobData_tag_size

struct BlobData_tag_size
{
  int32_T centroid[2];
  int32_T area;
  int32_T color;
};

#endif                                 /*struct_BlobData_tag_size*/

#ifndef typedef_c3_BlobData_size
#define typedef_c3_BlobData_size

typedef struct BlobData_tag_size c3_BlobData_size;

#endif                                 /*typedef_c3_BlobData_size*/

#ifndef struct_PlayerData_tag
#define struct_PlayerData_tag

struct PlayerData_tag
{
  int8_T x[6];
  int8_T y[6];
  int16_T orientation[6];
  uint8_T color[6];
};

#endif                                 /*struct_PlayerData_tag*/

#ifndef typedef_c3_PlayerData
#define typedef_c3_PlayerData

typedef struct PlayerData_tag c3_PlayerData;

#endif                                 /*typedef_c3_PlayerData*/

#ifndef struct_PlayerData_tag_size
#define struct_PlayerData_tag_size

struct PlayerData_tag_size
{
  int32_T x[2];
  int32_T y[2];
  int32_T orientation[2];
  int32_T color[2];
};

#endif                                 /*struct_PlayerData_tag_size*/

#ifndef typedef_c3_PlayerData_size
#define typedef_c3_PlayerData_size

typedef struct PlayerData_tag_size c3_PlayerData_size;

#endif                                 /*typedef_c3_PlayerData_size*/

#ifndef struct_BallData_tag
#define struct_BallData_tag

struct BallData_tag
{
  int8_T x[2];
  int8_T y[2];
};

#endif                                 /*struct_BallData_tag*/

#ifndef typedef_c3_BallData
#define typedef_c3_BallData

typedef struct BallData_tag c3_BallData;

#endif                                 /*typedef_c3_BallData*/

#ifndef struct_BallData_tag_size
#define struct_BallData_tag_size

struct BallData_tag_size
{
  int32_T x;
  int32_T y;
};

#endif                                 /*struct_BallData_tag_size*/

#ifndef typedef_c3_BallData_size
#define typedef_c3_BallData_size

typedef struct BallData_tag_size c3_BallData_size;

#endif                                 /*typedef_c3_BallData_size*/

#ifndef typedef_SFc3_LessonIII_startInstanceStruct
#define typedef_SFc3_LessonIII_startInstanceStruct

typedef struct {
  SimStruct *S;
  ChartInfoStruct chartInfo;
  uint32_T chartNumber;
  uint32_T instanceNumber;
  int32_T c3_sfEvent;
  boolean_T c3_isStable;
  boolean_T c3_doneDoubleBufferReInit;
  uint8_T c3_is_active_c3_LessonIII_start;
  c3_BlobData *c3_blobs_data;
  c3_BlobData_size *c3_blobs_elems_sizes;
  c3_PlayerData *c3_players_data;
  c3_PlayerData_size *c3_players_elems_sizes;
  real_T (*c3_camRes)[2];
  real_T *c3_height;
  c3_BallData *c3_Ball_data;
  c3_BallData_size *c3_Ball_elems_sizes;
} SFc3_LessonIII_startInstanceStruct;

#endif                                 /*typedef_SFc3_LessonIII_startInstanceStruct*/

/* Named Constants */

/* Variable Declarations */
extern struct SfDebugInstanceStruct *sfGlobalDebugInstanceStruct;

/* Variable Definitions */

/* Function Declarations */
extern const mxArray *sf_c3_LessonIII_start_get_eml_resolved_functions_info(void);

/* Function Definitions */
extern void sf_c3_LessonIII_start_get_check_sum(mxArray *plhs[]);
extern void c3_LessonIII_start_method_dispatcher(SimStruct *S, int_T method,
  void *data);

#endif

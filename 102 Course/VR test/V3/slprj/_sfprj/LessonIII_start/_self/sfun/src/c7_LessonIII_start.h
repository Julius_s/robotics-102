#ifndef __c7_LessonIII_start_h__
#define __c7_LessonIII_start_h__

/* Include files */
#include "sf_runtime/sfc_sf.h"
#include "sf_runtime/sfc_mex.h"
#include "rtwtypes.h"
#include "multiword_types.h"

/* Type Definitions */
#ifndef struct_Ball_tag
#define struct_Ball_tag

struct Ball_tag
{
  int8_T x;
  int8_T y;
  uint8_T valid;
};

#endif                                 /*struct_Ball_tag*/

#ifndef typedef_c7_Ball
#define typedef_c7_Ball

typedef struct Ball_tag c7_Ball;

#endif                                 /*typedef_c7_Ball*/

#ifndef struct_Player_tag
#define struct_Player_tag

struct Player_tag
{
  int8_T x;
  int8_T y;
  int16_T orientation;
  uint8_T color;
  uint8_T position;
  uint8_T valid;
};

#endif                                 /*struct_Player_tag*/

#ifndef typedef_c7_Player
#define typedef_c7_Player

typedef struct Player_tag c7_Player;

#endif                                 /*typedef_c7_Player*/

#include <stddef.h>
#ifndef typedef_SFc7_LessonIII_startInstanceStruct
#define typedef_SFc7_LessonIII_startInstanceStruct

typedef struct {
  SimStruct *S;
  ChartInfoStruct chartInfo;
  uint32_T chartNumber;
  uint32_T instanceNumber;
  int32_T c7_sfEvent;
  boolean_T c7_isStable;
  boolean_T c7_doneDoubleBufferReInit;
  uint8_T c7_is_active_c7_LessonIII_start;
  uint8_T (*c7_message)[31];
  c7_Ball *c7_b_Ball;
  c7_Player (*c7_players)[6];
  uint8_T *c7_gameOn;
} SFc7_LessonIII_startInstanceStruct;

#endif                                 /*typedef_SFc7_LessonIII_startInstanceStruct*/

/* Named Constants */

/* Variable Declarations */
extern struct SfDebugInstanceStruct *sfGlobalDebugInstanceStruct;

/* Variable Definitions */

/* Function Declarations */
extern const mxArray *sf_c7_LessonIII_start_get_eml_resolved_functions_info(void);

/* Function Definitions */
extern void sf_c7_LessonIII_start_get_check_sum(mxArray *plhs[]);
extern void c7_LessonIII_start_method_dispatcher(SimStruct *S, int_T method,
  void *data);

#endif

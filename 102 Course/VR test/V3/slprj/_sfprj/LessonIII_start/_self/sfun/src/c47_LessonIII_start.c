/* Include files */

#include <stddef.h>
#include "blas.h"
#include "LessonIII_start_sfun.h"
#include "c47_LessonIII_start.h"
#include "mwmathutil.h"
#define CHARTINSTANCE_CHARTNUMBER      (chartInstance->chartNumber)
#define CHARTINSTANCE_INSTANCENUMBER   (chartInstance->instanceNumber)
#include "LessonIII_start_sfun_debug_macros.h"
#define _SF_MEX_LISTEN_FOR_CTRL_C(S)   sf_mex_listen_for_ctrl_c(sfGlobalDebugInstanceStruct,S);

/* Type Definitions */

/* Named Constants */
#define CALL_EVENT                     (-1)

/* Variable Declarations */

/* Variable Definitions */
static real_T _sfTime_;
static const char * c47_debug_family_names[8] = { "T", "M", "nargin", "nargout",
  "targetO", "myO", "realDorien", "direc" };

/* Function Declarations */
static void initialize_c47_LessonIII_start(SFc47_LessonIII_startInstanceStruct
  *chartInstance);
static void initialize_params_c47_LessonIII_start
  (SFc47_LessonIII_startInstanceStruct *chartInstance);
static void enable_c47_LessonIII_start(SFc47_LessonIII_startInstanceStruct
  *chartInstance);
static void disable_c47_LessonIII_start(SFc47_LessonIII_startInstanceStruct
  *chartInstance);
static void c47_update_debugger_state_c47_LessonIII_start
  (SFc47_LessonIII_startInstanceStruct *chartInstance);
static const mxArray *get_sim_state_c47_LessonIII_start
  (SFc47_LessonIII_startInstanceStruct *chartInstance);
static void set_sim_state_c47_LessonIII_start
  (SFc47_LessonIII_startInstanceStruct *chartInstance, const mxArray *c47_st);
static void finalize_c47_LessonIII_start(SFc47_LessonIII_startInstanceStruct
  *chartInstance);
static void sf_gateway_c47_LessonIII_start(SFc47_LessonIII_startInstanceStruct
  *chartInstance);
static void mdl_start_c47_LessonIII_start(SFc47_LessonIII_startInstanceStruct
  *chartInstance);
static void initSimStructsc47_LessonIII_start
  (SFc47_LessonIII_startInstanceStruct *chartInstance);
static void init_script_number_translation(uint32_T c47_machineNumber, uint32_T
  c47_chartNumber, uint32_T c47_instanceNumber);
static const mxArray *c47_sf_marshallOut(void *chartInstanceVoid, void
  *c47_inData);
static int16_T c47_emlrt_marshallIn(SFc47_LessonIII_startInstanceStruct
  *chartInstance, const mxArray *c47_b_direc, const char_T *c47_identifier);
static int16_T c47_b_emlrt_marshallIn(SFc47_LessonIII_startInstanceStruct
  *chartInstance, const mxArray *c47_u, const emlrtMsgIdentifier *c47_parentId);
static void c47_sf_marshallIn(void *chartInstanceVoid, const mxArray
  *c47_mxArrayInData, const char_T *c47_varName, void *c47_outData);
static const mxArray *c47_b_sf_marshallOut(void *chartInstanceVoid, void
  *c47_inData);
static real_T c47_c_emlrt_marshallIn(SFc47_LessonIII_startInstanceStruct
  *chartInstance, const mxArray *c47_u, const emlrtMsgIdentifier *c47_parentId);
static void c47_b_sf_marshallIn(void *chartInstanceVoid, const mxArray
  *c47_mxArrayInData, const char_T *c47_varName, void *c47_outData);
static void c47_info_helper(const mxArray **c47_info);
static const mxArray *c47_emlrt_marshallOut(const char * c47_u);
static const mxArray *c47_b_emlrt_marshallOut(const uint32_T c47_u);
static int16_T c47_mod(SFc47_LessonIII_startInstanceStruct *chartInstance,
  int16_T c47_x);
static void c47_eml_scalar_eg(SFc47_LessonIII_startInstanceStruct *chartInstance);
static const mxArray *c47_c_sf_marshallOut(void *chartInstanceVoid, void
  *c47_inData);
static int32_T c47_d_emlrt_marshallIn(SFc47_LessonIII_startInstanceStruct
  *chartInstance, const mxArray *c47_u, const emlrtMsgIdentifier *c47_parentId);
static void c47_c_sf_marshallIn(void *chartInstanceVoid, const mxArray
  *c47_mxArrayInData, const char_T *c47_varName, void *c47_outData);
static uint8_T c47_e_emlrt_marshallIn(SFc47_LessonIII_startInstanceStruct
  *chartInstance, const mxArray *c47_b_is_active_c47_LessonIII_start, const
  char_T *c47_identifier);
static uint8_T c47_f_emlrt_marshallIn(SFc47_LessonIII_startInstanceStruct
  *chartInstance, const mxArray *c47_u, const emlrtMsgIdentifier *c47_parentId);
static int16_T c47_div_s16s32_floor(SFc47_LessonIII_startInstanceStruct
  *chartInstance, int32_T c47_numerator, int32_T c47_denominator);
static void init_dsm_address_info(SFc47_LessonIII_startInstanceStruct
  *chartInstance);
static void init_simulink_io_address(SFc47_LessonIII_startInstanceStruct
  *chartInstance);

/* Function Definitions */
static void initialize_c47_LessonIII_start(SFc47_LessonIII_startInstanceStruct
  *chartInstance)
{
  chartInstance->c47_sfEvent = CALL_EVENT;
  _sfTime_ = sf_get_time(chartInstance->S);
  chartInstance->c47_is_active_c47_LessonIII_start = 0U;
}

static void initialize_params_c47_LessonIII_start
  (SFc47_LessonIII_startInstanceStruct *chartInstance)
{
  (void)chartInstance;
}

static void enable_c47_LessonIII_start(SFc47_LessonIII_startInstanceStruct
  *chartInstance)
{
  _sfTime_ = sf_get_time(chartInstance->S);
}

static void disable_c47_LessonIII_start(SFc47_LessonIII_startInstanceStruct
  *chartInstance)
{
  _sfTime_ = sf_get_time(chartInstance->S);
}

static void c47_update_debugger_state_c47_LessonIII_start
  (SFc47_LessonIII_startInstanceStruct *chartInstance)
{
  (void)chartInstance;
}

static const mxArray *get_sim_state_c47_LessonIII_start
  (SFc47_LessonIII_startInstanceStruct *chartInstance)
{
  const mxArray *c47_st;
  const mxArray *c47_y = NULL;
  int16_T c47_hoistedGlobal;
  int16_T c47_u;
  const mxArray *c47_b_y = NULL;
  int16_T c47_b_hoistedGlobal;
  int16_T c47_b_u;
  const mxArray *c47_c_y = NULL;
  uint8_T c47_c_hoistedGlobal;
  uint8_T c47_c_u;
  const mxArray *c47_d_y = NULL;
  c47_st = NULL;
  c47_st = NULL;
  c47_y = NULL;
  sf_mex_assign(&c47_y, sf_mex_createcellmatrix(3, 1), false);
  c47_hoistedGlobal = *chartInstance->c47_direc;
  c47_u = c47_hoistedGlobal;
  c47_b_y = NULL;
  sf_mex_assign(&c47_b_y, sf_mex_create("y", &c47_u, 4, 0U, 0U, 0U, 0), false);
  sf_mex_setcell(c47_y, 0, c47_b_y);
  c47_b_hoistedGlobal = *chartInstance->c47_realDorien;
  c47_b_u = c47_b_hoistedGlobal;
  c47_c_y = NULL;
  sf_mex_assign(&c47_c_y, sf_mex_create("y", &c47_b_u, 4, 0U, 0U, 0U, 0), false);
  sf_mex_setcell(c47_y, 1, c47_c_y);
  c47_c_hoistedGlobal = chartInstance->c47_is_active_c47_LessonIII_start;
  c47_c_u = c47_c_hoistedGlobal;
  c47_d_y = NULL;
  sf_mex_assign(&c47_d_y, sf_mex_create("y", &c47_c_u, 3, 0U, 0U, 0U, 0), false);
  sf_mex_setcell(c47_y, 2, c47_d_y);
  sf_mex_assign(&c47_st, c47_y, false);
  return c47_st;
}

static void set_sim_state_c47_LessonIII_start
  (SFc47_LessonIII_startInstanceStruct *chartInstance, const mxArray *c47_st)
{
  const mxArray *c47_u;
  chartInstance->c47_doneDoubleBufferReInit = true;
  c47_u = sf_mex_dup(c47_st);
  *chartInstance->c47_direc = c47_emlrt_marshallIn(chartInstance, sf_mex_dup
    (sf_mex_getcell(c47_u, 0)), "direc");
  *chartInstance->c47_realDorien = c47_emlrt_marshallIn(chartInstance,
    sf_mex_dup(sf_mex_getcell(c47_u, 1)), "realDorien");
  chartInstance->c47_is_active_c47_LessonIII_start = c47_e_emlrt_marshallIn
    (chartInstance, sf_mex_dup(sf_mex_getcell(c47_u, 2)),
     "is_active_c47_LessonIII_start");
  sf_mex_destroy(&c47_u);
  c47_update_debugger_state_c47_LessonIII_start(chartInstance);
  sf_mex_destroy(&c47_st);
}

static void finalize_c47_LessonIII_start(SFc47_LessonIII_startInstanceStruct
  *chartInstance)
{
  (void)chartInstance;
}

static void sf_gateway_c47_LessonIII_start(SFc47_LessonIII_startInstanceStruct
  *chartInstance)
{
  int16_T c47_hoistedGlobal;
  int16_T c47_b_hoistedGlobal;
  int16_T c47_b_targetO;
  int16_T c47_b_myO;
  uint32_T c47_debug_family_var_map[8];
  int16_T c47_T;
  int16_T c47_M;
  real_T c47_nargin = 2.0;
  real_T c47_nargout = 2.0;
  int16_T c47_b_realDorien;
  int16_T c47_b_direc;
  int32_T c47_i0;
  int32_T c47_i1;
  int32_T c47_i2;
  int16_T c47_x;
  int16_T c47_b_x;
  int32_T c47_i3;
  int16_T c47_y;
  int32_T c47_i4;
  int16_T c47_c_x;
  int16_T c47_d_x;
  int32_T c47_i5;
  int16_T c47_b_y;
  int16_T c47_varargin_1;
  int16_T c47_varargin_2;
  int16_T c47_b_varargin_2;
  int16_T c47_varargin_3;
  int16_T c47_e_x;
  int16_T c47_c_y;
  int16_T c47_f_x;
  int16_T c47_d_y;
  int16_T c47_xk;
  int16_T c47_yk;
  int16_T c47_g_x;
  int16_T c47_e_y;
  int32_T c47_i6;
  int16_T c47_h_x;
  int16_T c47_i_x;
  int32_T c47_i7;
  int16_T c47_i8;
  int32_T c47_i9;
  int16_T c47_j_x;
  int16_T c47_k_x;
  int32_T c47_i10;
  int16_T c47_i11;
  int32_T c47_i12;
  int16_T c47_l_x;
  int16_T c47_m_x;
  int32_T c47_i13;
  int16_T c47_n_x;
  int16_T c47_o_x;
  _SFD_SYMBOL_SCOPE_PUSH(0U, 0U);
  _sfTime_ = sf_get_time(chartInstance->S);
  _SFD_CC_CALL(CHART_ENTER_SFUNCTION_TAG, 34U, chartInstance->c47_sfEvent);
  _SFD_DATA_RANGE_CHECK((real_T)*chartInstance->c47_targetO, 0U);
  chartInstance->c47_sfEvent = CALL_EVENT;
  _SFD_CC_CALL(CHART_ENTER_DURING_FUNCTION_TAG, 34U, chartInstance->c47_sfEvent);
  c47_hoistedGlobal = *chartInstance->c47_targetO;
  c47_b_hoistedGlobal = *chartInstance->c47_myO;
  c47_b_targetO = c47_hoistedGlobal;
  c47_b_myO = c47_b_hoistedGlobal;
  _SFD_SYMBOL_SCOPE_PUSH_EML(0U, 8U, 8U, c47_debug_family_names,
    c47_debug_family_var_map);
  _SFD_SYMBOL_SCOPE_ADD_EML_IMPORTABLE(&c47_T, 0U, c47_sf_marshallOut,
    c47_sf_marshallIn);
  _SFD_SYMBOL_SCOPE_ADD_EML_IMPORTABLE(&c47_M, 1U, c47_sf_marshallOut,
    c47_sf_marshallIn);
  _SFD_SYMBOL_SCOPE_ADD_EML_IMPORTABLE(&c47_nargin, 2U, c47_b_sf_marshallOut,
    c47_b_sf_marshallIn);
  _SFD_SYMBOL_SCOPE_ADD_EML_IMPORTABLE(&c47_nargout, 3U, c47_b_sf_marshallOut,
    c47_b_sf_marshallIn);
  _SFD_SYMBOL_SCOPE_ADD_EML(&c47_b_targetO, 4U, c47_sf_marshallOut);
  _SFD_SYMBOL_SCOPE_ADD_EML(&c47_b_myO, 5U, c47_sf_marshallOut);
  _SFD_SYMBOL_SCOPE_ADD_EML_IMPORTABLE(&c47_b_realDorien, 6U, c47_sf_marshallOut,
    c47_sf_marshallIn);
  _SFD_SYMBOL_SCOPE_ADD_EML_IMPORTABLE(&c47_b_direc, 7U, c47_sf_marshallOut,
    c47_sf_marshallIn);
  CV_EML_FCN(0, 0);
  _SFD_EML_CALL(0U, chartInstance->c47_sfEvent, 2);
  c47_i0 = c47_b_targetO + 360;
  if (c47_i0 > 32767) {
    CV_SATURATION_EVAL(4, 0, 10, 0, 1);
    c47_i0 = 32767;
  } else {
    if (CV_SATURATION_EVAL(4, 0, 10, 0, c47_i0 < -32768)) {
      c47_i0 = -32768;
    }
  }

  c47_T = c47_mod(chartInstance, (int16_T)c47_i0);
  _SFD_EML_CALL(0U, chartInstance->c47_sfEvent, 3);
  c47_i1 = c47_b_myO + 360;
  if (c47_i1 > 32767) {
    CV_SATURATION_EVAL(4, 0, 11, 0, 1);
    c47_i1 = 32767;
  } else {
    if (CV_SATURATION_EVAL(4, 0, 11, 0, c47_i1 < -32768)) {
      c47_i1 = -32768;
    }
  }

  c47_M = c47_mod(chartInstance, (int16_T)c47_i1);
  _SFD_EML_CALL(0U, chartInstance->c47_sfEvent, 4);
  c47_i2 = c47_T - c47_M;
  if (c47_i2 > 32767) {
    CV_SATURATION_EVAL(4, 0, 1, 0, 1);
    c47_i2 = 32767;
  } else {
    if (CV_SATURATION_EVAL(4, 0, 1, 0, c47_i2 < -32768)) {
      c47_i2 = -32768;
    }
  }

  c47_x = (int16_T)c47_i2;
  c47_b_x = c47_x;
  c47_i3 = -c47_b_x;
  if (c47_i3 > 32767) {
    CV_SATURATION_EVAL(4, 0, 0, 0, 1);
    c47_i3 = 32767;
  } else {
    if (CV_SATURATION_EVAL(4, 0, 0, 0, c47_i3 < -32768)) {
      c47_i3 = -32768;
    }
  }

  if ((real_T)c47_b_x < 0.0) {
    c47_y = (int16_T)c47_i3;
  } else {
    c47_y = c47_b_x;
  }

  c47_i4 = c47_b_targetO - c47_b_myO;
  if (c47_i4 > 32767) {
    CV_SATURATION_EVAL(4, 0, 3, 0, 1);
    c47_i4 = 32767;
  } else {
    if (CV_SATURATION_EVAL(4, 0, 3, 0, c47_i4 < -32768)) {
      c47_i4 = -32768;
    }
  }

  c47_c_x = (int16_T)c47_i4;
  c47_d_x = c47_c_x;
  c47_i5 = -c47_d_x;
  if (c47_i5 > 32767) {
    CV_SATURATION_EVAL(4, 0, 2, 0, 1);
    c47_i5 = 32767;
  } else {
    if (CV_SATURATION_EVAL(4, 0, 2, 0, c47_i5 < -32768)) {
      c47_i5 = -32768;
    }
  }

  if ((real_T)c47_d_x < 0.0) {
    c47_b_y = (int16_T)c47_i5;
  } else {
    c47_b_y = c47_d_x;
  }

  c47_varargin_1 = c47_y;
  c47_varargin_2 = c47_b_y;
  c47_b_varargin_2 = c47_varargin_1;
  c47_varargin_3 = c47_varargin_2;
  c47_e_x = c47_b_varargin_2;
  c47_c_y = c47_varargin_3;
  c47_f_x = c47_e_x;
  c47_d_y = c47_c_y;
  c47_eml_scalar_eg(chartInstance);
  c47_xk = c47_f_x;
  c47_yk = c47_d_y;
  c47_g_x = c47_xk;
  c47_e_y = c47_yk;
  c47_eml_scalar_eg(chartInstance);
  c47_b_realDorien = muIntScalarMin_sint16(c47_g_x, c47_e_y);
  _SFD_EML_CALL(0U, chartInstance->c47_sfEvent, 5);
  c47_i6 = c47_T - c47_M;
  if (c47_i6 > 32767) {
    CV_SATURATION_EVAL(4, 0, 5, 0, 1);
    c47_i6 = 32767;
  } else {
    if (CV_SATURATION_EVAL(4, 0, 5, 0, c47_i6 < -32768)) {
      c47_i6 = -32768;
    }
  }

  c47_h_x = (int16_T)c47_i6;
  c47_i_x = c47_h_x;
  c47_i7 = -c47_i_x;
  if (c47_i7 > 32767) {
    CV_SATURATION_EVAL(4, 0, 4, 0, 1);
    c47_i7 = 32767;
  } else {
    if (CV_SATURATION_EVAL(4, 0, 4, 0, c47_i7 < -32768)) {
      c47_i7 = -32768;
    }
  }

  if ((real_T)c47_i_x < 0.0) {
    c47_i8 = (int16_T)c47_i7;
  } else {
    c47_i8 = c47_i_x;
  }

  c47_i9 = c47_b_targetO - c47_b_myO;
  if (c47_i9 > 32767) {
    CV_SATURATION_EVAL(4, 0, 7, 0, 1);
    c47_i9 = 32767;
  } else {
    if (CV_SATURATION_EVAL(4, 0, 7, 0, c47_i9 < -32768)) {
      c47_i9 = -32768;
    }
  }

  c47_j_x = (int16_T)c47_i9;
  c47_k_x = c47_j_x;
  c47_i10 = -c47_k_x;
  if (c47_i10 > 32767) {
    CV_SATURATION_EVAL(4, 0, 6, 0, 1);
    c47_i10 = 32767;
  } else {
    if (CV_SATURATION_EVAL(4, 0, 6, 0, c47_i10 < -32768)) {
      c47_i10 = -32768;
    }
  }

  if ((real_T)c47_k_x < 0.0) {
    c47_i11 = (int16_T)c47_i10;
  } else {
    c47_i11 = c47_k_x;
  }

  if (CV_EML_IF(0, 1, 0, CV_RELATIONAL_EVAL(4U, 0U, 0, (real_T)c47_i8, (real_T)
        c47_i11, 0, 2U, c47_i8 < c47_i11))) {
    _SFD_EML_CALL(0U, chartInstance->c47_sfEvent, 6);
    c47_i12 = c47_T - c47_M;
    if (c47_i12 > 32767) {
      CV_SATURATION_EVAL(4, 0, 8, 0, 1);
      c47_i12 = 32767;
    } else {
      if (CV_SATURATION_EVAL(4, 0, 8, 0, c47_i12 < -32768)) {
        c47_i12 = -32768;
      }
    }

    c47_l_x = (int16_T)c47_i12;
    c47_b_direc = c47_l_x;
    c47_m_x = c47_b_direc;
    c47_b_direc = c47_m_x;
    if ((real_T)c47_b_direc > 0.0) {
      c47_b_direc = 1;
    } else if ((real_T)c47_b_direc < 0.0) {
      c47_b_direc = -1;
    } else {
      c47_b_direc = 0;
    }
  } else {
    _SFD_EML_CALL(0U, chartInstance->c47_sfEvent, 8);
    c47_i13 = c47_b_targetO - c47_b_myO;
    if (c47_i13 > 32767) {
      CV_SATURATION_EVAL(4, 0, 9, 0, 1);
      c47_i13 = 32767;
    } else {
      if (CV_SATURATION_EVAL(4, 0, 9, 0, c47_i13 < -32768)) {
        c47_i13 = -32768;
      }
    }

    c47_n_x = (int16_T)c47_i13;
    c47_b_direc = c47_n_x;
    c47_o_x = c47_b_direc;
    c47_b_direc = c47_o_x;
    if ((real_T)c47_b_direc > 0.0) {
      c47_b_direc = 1;
    } else if ((real_T)c47_b_direc < 0.0) {
      c47_b_direc = -1;
    } else {
      c47_b_direc = 0;
    }
  }

  _SFD_EML_CALL(0U, chartInstance->c47_sfEvent, 10);
  if (CV_EML_IF(0, 1, 1, CV_RELATIONAL_EVAL(4U, 0U, 1, (real_T)c47_b_realDorien,
        3.0, -1, 2U, (real_T)c47_b_realDorien < 3.0))) {
    _SFD_EML_CALL(0U, chartInstance->c47_sfEvent, 11);
    c47_b_realDorien = 0;
  }

  _SFD_EML_CALL(0U, chartInstance->c47_sfEvent, -11);
  _SFD_SYMBOL_SCOPE_POP();
  *chartInstance->c47_realDorien = c47_b_realDorien;
  *chartInstance->c47_direc = c47_b_direc;
  _SFD_CC_CALL(EXIT_OUT_OF_FUNCTION_TAG, 34U, chartInstance->c47_sfEvent);
  _SFD_SYMBOL_SCOPE_POP();
  _SFD_CHECK_FOR_STATE_INCONSISTENCY(_LessonIII_startMachineNumber_,
    chartInstance->chartNumber, chartInstance->instanceNumber);
  _SFD_DATA_RANGE_CHECK((real_T)*chartInstance->c47_realDorien, 1U);
  _SFD_DATA_RANGE_CHECK((real_T)*chartInstance->c47_direc, 2U);
  _SFD_DATA_RANGE_CHECK((real_T)*chartInstance->c47_myO, 3U);
}

static void mdl_start_c47_LessonIII_start(SFc47_LessonIII_startInstanceStruct
  *chartInstance)
{
  (void)chartInstance;
}

static void initSimStructsc47_LessonIII_start
  (SFc47_LessonIII_startInstanceStruct *chartInstance)
{
  (void)chartInstance;
}

static void init_script_number_translation(uint32_T c47_machineNumber, uint32_T
  c47_chartNumber, uint32_T c47_instanceNumber)
{
  (void)c47_machineNumber;
  (void)c47_chartNumber;
  (void)c47_instanceNumber;
}

static const mxArray *c47_sf_marshallOut(void *chartInstanceVoid, void
  *c47_inData)
{
  const mxArray *c47_mxArrayOutData = NULL;
  int16_T c47_u;
  const mxArray *c47_y = NULL;
  SFc47_LessonIII_startInstanceStruct *chartInstance;
  chartInstance = (SFc47_LessonIII_startInstanceStruct *)chartInstanceVoid;
  c47_mxArrayOutData = NULL;
  c47_u = *(int16_T *)c47_inData;
  c47_y = NULL;
  sf_mex_assign(&c47_y, sf_mex_create("y", &c47_u, 4, 0U, 0U, 0U, 0), false);
  sf_mex_assign(&c47_mxArrayOutData, c47_y, false);
  return c47_mxArrayOutData;
}

static int16_T c47_emlrt_marshallIn(SFc47_LessonIII_startInstanceStruct
  *chartInstance, const mxArray *c47_b_direc, const char_T *c47_identifier)
{
  int16_T c47_y;
  emlrtMsgIdentifier c47_thisId;
  c47_thisId.fIdentifier = c47_identifier;
  c47_thisId.fParent = NULL;
  c47_y = c47_b_emlrt_marshallIn(chartInstance, sf_mex_dup(c47_b_direc),
    &c47_thisId);
  sf_mex_destroy(&c47_b_direc);
  return c47_y;
}

static int16_T c47_b_emlrt_marshallIn(SFc47_LessonIII_startInstanceStruct
  *chartInstance, const mxArray *c47_u, const emlrtMsgIdentifier *c47_parentId)
{
  int16_T c47_y;
  int16_T c47_i14;
  (void)chartInstance;
  sf_mex_import(c47_parentId, sf_mex_dup(c47_u), &c47_i14, 1, 4, 0U, 0, 0U, 0);
  c47_y = c47_i14;
  sf_mex_destroy(&c47_u);
  return c47_y;
}

static void c47_sf_marshallIn(void *chartInstanceVoid, const mxArray
  *c47_mxArrayInData, const char_T *c47_varName, void *c47_outData)
{
  const mxArray *c47_b_direc;
  const char_T *c47_identifier;
  emlrtMsgIdentifier c47_thisId;
  int16_T c47_y;
  SFc47_LessonIII_startInstanceStruct *chartInstance;
  chartInstance = (SFc47_LessonIII_startInstanceStruct *)chartInstanceVoid;
  c47_b_direc = sf_mex_dup(c47_mxArrayInData);
  c47_identifier = c47_varName;
  c47_thisId.fIdentifier = c47_identifier;
  c47_thisId.fParent = NULL;
  c47_y = c47_b_emlrt_marshallIn(chartInstance, sf_mex_dup(c47_b_direc),
    &c47_thisId);
  sf_mex_destroy(&c47_b_direc);
  *(int16_T *)c47_outData = c47_y;
  sf_mex_destroy(&c47_mxArrayInData);
}

static const mxArray *c47_b_sf_marshallOut(void *chartInstanceVoid, void
  *c47_inData)
{
  const mxArray *c47_mxArrayOutData = NULL;
  real_T c47_u;
  const mxArray *c47_y = NULL;
  SFc47_LessonIII_startInstanceStruct *chartInstance;
  chartInstance = (SFc47_LessonIII_startInstanceStruct *)chartInstanceVoid;
  c47_mxArrayOutData = NULL;
  c47_u = *(real_T *)c47_inData;
  c47_y = NULL;
  sf_mex_assign(&c47_y, sf_mex_create("y", &c47_u, 0, 0U, 0U, 0U, 0), false);
  sf_mex_assign(&c47_mxArrayOutData, c47_y, false);
  return c47_mxArrayOutData;
}

static real_T c47_c_emlrt_marshallIn(SFc47_LessonIII_startInstanceStruct
  *chartInstance, const mxArray *c47_u, const emlrtMsgIdentifier *c47_parentId)
{
  real_T c47_y;
  real_T c47_d0;
  (void)chartInstance;
  sf_mex_import(c47_parentId, sf_mex_dup(c47_u), &c47_d0, 1, 0, 0U, 0, 0U, 0);
  c47_y = c47_d0;
  sf_mex_destroy(&c47_u);
  return c47_y;
}

static void c47_b_sf_marshallIn(void *chartInstanceVoid, const mxArray
  *c47_mxArrayInData, const char_T *c47_varName, void *c47_outData)
{
  const mxArray *c47_nargout;
  const char_T *c47_identifier;
  emlrtMsgIdentifier c47_thisId;
  real_T c47_y;
  SFc47_LessonIII_startInstanceStruct *chartInstance;
  chartInstance = (SFc47_LessonIII_startInstanceStruct *)chartInstanceVoid;
  c47_nargout = sf_mex_dup(c47_mxArrayInData);
  c47_identifier = c47_varName;
  c47_thisId.fIdentifier = c47_identifier;
  c47_thisId.fParent = NULL;
  c47_y = c47_c_emlrt_marshallIn(chartInstance, sf_mex_dup(c47_nargout),
    &c47_thisId);
  sf_mex_destroy(&c47_nargout);
  *(real_T *)c47_outData = c47_y;
  sf_mex_destroy(&c47_mxArrayInData);
}

const mxArray *sf_c47_LessonIII_start_get_eml_resolved_functions_info(void)
{
  const mxArray *c47_nameCaptureInfo = NULL;
  c47_nameCaptureInfo = NULL;
  sf_mex_assign(&c47_nameCaptureInfo, sf_mex_createstruct("structure", 2, 22, 1),
                false);
  c47_info_helper(&c47_nameCaptureInfo);
  sf_mex_emlrtNameCapturePostProcessR2012a(&c47_nameCaptureInfo);
  return c47_nameCaptureInfo;
}

static void c47_info_helper(const mxArray **c47_info)
{
  const mxArray *c47_rhs0 = NULL;
  const mxArray *c47_lhs0 = NULL;
  const mxArray *c47_rhs1 = NULL;
  const mxArray *c47_lhs1 = NULL;
  const mxArray *c47_rhs2 = NULL;
  const mxArray *c47_lhs2 = NULL;
  const mxArray *c47_rhs3 = NULL;
  const mxArray *c47_lhs3 = NULL;
  const mxArray *c47_rhs4 = NULL;
  const mxArray *c47_lhs4 = NULL;
  const mxArray *c47_rhs5 = NULL;
  const mxArray *c47_lhs5 = NULL;
  const mxArray *c47_rhs6 = NULL;
  const mxArray *c47_lhs6 = NULL;
  const mxArray *c47_rhs7 = NULL;
  const mxArray *c47_lhs7 = NULL;
  const mxArray *c47_rhs8 = NULL;
  const mxArray *c47_lhs8 = NULL;
  const mxArray *c47_rhs9 = NULL;
  const mxArray *c47_lhs9 = NULL;
  const mxArray *c47_rhs10 = NULL;
  const mxArray *c47_lhs10 = NULL;
  const mxArray *c47_rhs11 = NULL;
  const mxArray *c47_lhs11 = NULL;
  const mxArray *c47_rhs12 = NULL;
  const mxArray *c47_lhs12 = NULL;
  const mxArray *c47_rhs13 = NULL;
  const mxArray *c47_lhs13 = NULL;
  const mxArray *c47_rhs14 = NULL;
  const mxArray *c47_lhs14 = NULL;
  const mxArray *c47_rhs15 = NULL;
  const mxArray *c47_lhs15 = NULL;
  const mxArray *c47_rhs16 = NULL;
  const mxArray *c47_lhs16 = NULL;
  const mxArray *c47_rhs17 = NULL;
  const mxArray *c47_lhs17 = NULL;
  const mxArray *c47_rhs18 = NULL;
  const mxArray *c47_lhs18 = NULL;
  const mxArray *c47_rhs19 = NULL;
  const mxArray *c47_lhs19 = NULL;
  const mxArray *c47_rhs20 = NULL;
  const mxArray *c47_lhs20 = NULL;
  const mxArray *c47_rhs21 = NULL;
  const mxArray *c47_lhs21 = NULL;
  sf_mex_addfield(*c47_info, c47_emlrt_marshallOut(""), "context", "context", 0);
  sf_mex_addfield(*c47_info, c47_emlrt_marshallOut("mod"), "name", "name", 0);
  sf_mex_addfield(*c47_info, c47_emlrt_marshallOut("int16"), "dominantType",
                  "dominantType", 0);
  sf_mex_addfield(*c47_info, c47_emlrt_marshallOut(
    "[ILXE]$matlabroot$/toolbox/eml/lib/matlab/elfun/mod.m"), "resolved",
                  "resolved", 0);
  sf_mex_addfield(*c47_info, c47_b_emlrt_marshallOut(1363717454U), "fileTimeLo",
                  "fileTimeLo", 0);
  sf_mex_addfield(*c47_info, c47_b_emlrt_marshallOut(0U), "fileTimeHi",
                  "fileTimeHi", 0);
  sf_mex_addfield(*c47_info, c47_b_emlrt_marshallOut(0U), "mFileTimeLo",
                  "mFileTimeLo", 0);
  sf_mex_addfield(*c47_info, c47_b_emlrt_marshallOut(0U), "mFileTimeHi",
                  "mFileTimeHi", 0);
  sf_mex_assign(&c47_rhs0, sf_mex_createcellmatrix(0, 1), false);
  sf_mex_assign(&c47_lhs0, sf_mex_createcellmatrix(0, 1), false);
  sf_mex_addfield(*c47_info, sf_mex_duplicatearraysafe(&c47_rhs0), "rhs", "rhs",
                  0);
  sf_mex_addfield(*c47_info, sf_mex_duplicatearraysafe(&c47_lhs0), "lhs", "lhs",
                  0);
  sf_mex_addfield(*c47_info, c47_emlrt_marshallOut(
    "[ILXE]$matlabroot$/toolbox/eml/lib/matlab/elfun/mod.m"), "context",
                  "context", 1);
  sf_mex_addfield(*c47_info, c47_emlrt_marshallOut(
    "coder.internal.isBuiltInNumeric"), "name", "name", 1);
  sf_mex_addfield(*c47_info, c47_emlrt_marshallOut("int16"), "dominantType",
                  "dominantType", 1);
  sf_mex_addfield(*c47_info, c47_emlrt_marshallOut(
    "[ILXE]$matlabroot$/toolbox/shared/coder/coder/+coder/+internal/isBuiltInNumeric.m"),
                  "resolved", "resolved", 1);
  sf_mex_addfield(*c47_info, c47_b_emlrt_marshallOut(1395935456U), "fileTimeLo",
                  "fileTimeLo", 1);
  sf_mex_addfield(*c47_info, c47_b_emlrt_marshallOut(0U), "fileTimeHi",
                  "fileTimeHi", 1);
  sf_mex_addfield(*c47_info, c47_b_emlrt_marshallOut(0U), "mFileTimeLo",
                  "mFileTimeLo", 1);
  sf_mex_addfield(*c47_info, c47_b_emlrt_marshallOut(0U), "mFileTimeHi",
                  "mFileTimeHi", 1);
  sf_mex_assign(&c47_rhs1, sf_mex_createcellmatrix(0, 1), false);
  sf_mex_assign(&c47_lhs1, sf_mex_createcellmatrix(0, 1), false);
  sf_mex_addfield(*c47_info, sf_mex_duplicatearraysafe(&c47_rhs1), "rhs", "rhs",
                  1);
  sf_mex_addfield(*c47_info, sf_mex_duplicatearraysafe(&c47_lhs1), "lhs", "lhs",
                  1);
  sf_mex_addfield(*c47_info, c47_emlrt_marshallOut(
    "[ILXE]$matlabroot$/toolbox/eml/lib/matlab/elfun/mod.m"), "context",
                  "context", 2);
  sf_mex_addfield(*c47_info, c47_emlrt_marshallOut(
    "coder.internal.isBuiltInNumeric"), "name", "name", 2);
  sf_mex_addfield(*c47_info, c47_emlrt_marshallOut("double"), "dominantType",
                  "dominantType", 2);
  sf_mex_addfield(*c47_info, c47_emlrt_marshallOut(
    "[ILXE]$matlabroot$/toolbox/shared/coder/coder/+coder/+internal/isBuiltInNumeric.m"),
                  "resolved", "resolved", 2);
  sf_mex_addfield(*c47_info, c47_b_emlrt_marshallOut(1395935456U), "fileTimeLo",
                  "fileTimeLo", 2);
  sf_mex_addfield(*c47_info, c47_b_emlrt_marshallOut(0U), "fileTimeHi",
                  "fileTimeHi", 2);
  sf_mex_addfield(*c47_info, c47_b_emlrt_marshallOut(0U), "mFileTimeLo",
                  "mFileTimeLo", 2);
  sf_mex_addfield(*c47_info, c47_b_emlrt_marshallOut(0U), "mFileTimeHi",
                  "mFileTimeHi", 2);
  sf_mex_assign(&c47_rhs2, sf_mex_createcellmatrix(0, 1), false);
  sf_mex_assign(&c47_lhs2, sf_mex_createcellmatrix(0, 1), false);
  sf_mex_addfield(*c47_info, sf_mex_duplicatearraysafe(&c47_rhs2), "rhs", "rhs",
                  2);
  sf_mex_addfield(*c47_info, sf_mex_duplicatearraysafe(&c47_lhs2), "lhs", "lhs",
                  2);
  sf_mex_addfield(*c47_info, c47_emlrt_marshallOut(
    "[ILXE]$matlabroot$/toolbox/eml/lib/matlab/elfun/mod.m"), "context",
                  "context", 3);
  sf_mex_addfield(*c47_info, c47_emlrt_marshallOut("coder.internal.assert"),
                  "name", "name", 3);
  sf_mex_addfield(*c47_info, c47_emlrt_marshallOut("char"), "dominantType",
                  "dominantType", 3);
  sf_mex_addfield(*c47_info, c47_emlrt_marshallOut(
    "[ILXE]$matlabroot$/toolbox/shared/coder/coder/+coder/+internal/assert.m"),
                  "resolved", "resolved", 3);
  sf_mex_addfield(*c47_info, c47_b_emlrt_marshallOut(1389721374U), "fileTimeLo",
                  "fileTimeLo", 3);
  sf_mex_addfield(*c47_info, c47_b_emlrt_marshallOut(0U), "fileTimeHi",
                  "fileTimeHi", 3);
  sf_mex_addfield(*c47_info, c47_b_emlrt_marshallOut(0U), "mFileTimeLo",
                  "mFileTimeLo", 3);
  sf_mex_addfield(*c47_info, c47_b_emlrt_marshallOut(0U), "mFileTimeHi",
                  "mFileTimeHi", 3);
  sf_mex_assign(&c47_rhs3, sf_mex_createcellmatrix(0, 1), false);
  sf_mex_assign(&c47_lhs3, sf_mex_createcellmatrix(0, 1), false);
  sf_mex_addfield(*c47_info, sf_mex_duplicatearraysafe(&c47_rhs3), "rhs", "rhs",
                  3);
  sf_mex_addfield(*c47_info, sf_mex_duplicatearraysafe(&c47_lhs3), "lhs", "lhs",
                  3);
  sf_mex_addfield(*c47_info, c47_emlrt_marshallOut(
    "[ILXE]$matlabroot$/toolbox/eml/lib/matlab/elfun/mod.m"), "context",
                  "context", 4);
  sf_mex_addfield(*c47_info, c47_emlrt_marshallOut("eml_scalar_eg"), "name",
                  "name", 4);
  sf_mex_addfield(*c47_info, c47_emlrt_marshallOut("int16"), "dominantType",
                  "dominantType", 4);
  sf_mex_addfield(*c47_info, c47_emlrt_marshallOut(
    "[ILXE]$matlabroot$/toolbox/eml/lib/matlab/eml/eml_scalar_eg.m"), "resolved",
                  "resolved", 4);
  sf_mex_addfield(*c47_info, c47_b_emlrt_marshallOut(1375984288U), "fileTimeLo",
                  "fileTimeLo", 4);
  sf_mex_addfield(*c47_info, c47_b_emlrt_marshallOut(0U), "fileTimeHi",
                  "fileTimeHi", 4);
  sf_mex_addfield(*c47_info, c47_b_emlrt_marshallOut(0U), "mFileTimeLo",
                  "mFileTimeLo", 4);
  sf_mex_addfield(*c47_info, c47_b_emlrt_marshallOut(0U), "mFileTimeHi",
                  "mFileTimeHi", 4);
  sf_mex_assign(&c47_rhs4, sf_mex_createcellmatrix(0, 1), false);
  sf_mex_assign(&c47_lhs4, sf_mex_createcellmatrix(0, 1), false);
  sf_mex_addfield(*c47_info, sf_mex_duplicatearraysafe(&c47_rhs4), "rhs", "rhs",
                  4);
  sf_mex_addfield(*c47_info, sf_mex_duplicatearraysafe(&c47_lhs4), "lhs", "lhs",
                  4);
  sf_mex_addfield(*c47_info, c47_emlrt_marshallOut(
    "[ILXE]$matlabroot$/toolbox/eml/lib/matlab/eml/eml_scalar_eg.m"), "context",
                  "context", 5);
  sf_mex_addfield(*c47_info, c47_emlrt_marshallOut("coder.internal.scalarEg"),
                  "name", "name", 5);
  sf_mex_addfield(*c47_info, c47_emlrt_marshallOut("int16"), "dominantType",
                  "dominantType", 5);
  sf_mex_addfield(*c47_info, c47_emlrt_marshallOut(
    "[ILXE]$matlabroot$/toolbox/coder/coder/+coder/+internal/scalarEg.p"),
                  "resolved", "resolved", 5);
  sf_mex_addfield(*c47_info, c47_b_emlrt_marshallOut(1410811370U), "fileTimeLo",
                  "fileTimeLo", 5);
  sf_mex_addfield(*c47_info, c47_b_emlrt_marshallOut(0U), "fileTimeHi",
                  "fileTimeHi", 5);
  sf_mex_addfield(*c47_info, c47_b_emlrt_marshallOut(0U), "mFileTimeLo",
                  "mFileTimeLo", 5);
  sf_mex_addfield(*c47_info, c47_b_emlrt_marshallOut(0U), "mFileTimeHi",
                  "mFileTimeHi", 5);
  sf_mex_assign(&c47_rhs5, sf_mex_createcellmatrix(0, 1), false);
  sf_mex_assign(&c47_lhs5, sf_mex_createcellmatrix(0, 1), false);
  sf_mex_addfield(*c47_info, sf_mex_duplicatearraysafe(&c47_rhs5), "rhs", "rhs",
                  5);
  sf_mex_addfield(*c47_info, sf_mex_duplicatearraysafe(&c47_lhs5), "lhs", "lhs",
                  5);
  sf_mex_addfield(*c47_info, c47_emlrt_marshallOut(
    "[ILXE]$matlabroot$/toolbox/eml/lib/matlab/elfun/mod.m"), "context",
                  "context", 6);
  sf_mex_addfield(*c47_info, c47_emlrt_marshallOut("eml_scalexp_alloc"), "name",
                  "name", 6);
  sf_mex_addfield(*c47_info, c47_emlrt_marshallOut("int16"), "dominantType",
                  "dominantType", 6);
  sf_mex_addfield(*c47_info, c47_emlrt_marshallOut(
    "[ILXE]$matlabroot$/toolbox/eml/lib/matlab/eml/eml_scalexp_alloc.m"),
                  "resolved", "resolved", 6);
  sf_mex_addfield(*c47_info, c47_b_emlrt_marshallOut(1375984288U), "fileTimeLo",
                  "fileTimeLo", 6);
  sf_mex_addfield(*c47_info, c47_b_emlrt_marshallOut(0U), "fileTimeHi",
                  "fileTimeHi", 6);
  sf_mex_addfield(*c47_info, c47_b_emlrt_marshallOut(0U), "mFileTimeLo",
                  "mFileTimeLo", 6);
  sf_mex_addfield(*c47_info, c47_b_emlrt_marshallOut(0U), "mFileTimeHi",
                  "mFileTimeHi", 6);
  sf_mex_assign(&c47_rhs6, sf_mex_createcellmatrix(0, 1), false);
  sf_mex_assign(&c47_lhs6, sf_mex_createcellmatrix(0, 1), false);
  sf_mex_addfield(*c47_info, sf_mex_duplicatearraysafe(&c47_rhs6), "rhs", "rhs",
                  6);
  sf_mex_addfield(*c47_info, sf_mex_duplicatearraysafe(&c47_lhs6), "lhs", "lhs",
                  6);
  sf_mex_addfield(*c47_info, c47_emlrt_marshallOut(
    "[ILXE]$matlabroot$/toolbox/eml/lib/matlab/eml/eml_scalexp_alloc.m"),
                  "context", "context", 7);
  sf_mex_addfield(*c47_info, c47_emlrt_marshallOut("coder.internal.scalexpAlloc"),
                  "name", "name", 7);
  sf_mex_addfield(*c47_info, c47_emlrt_marshallOut("int16"), "dominantType",
                  "dominantType", 7);
  sf_mex_addfield(*c47_info, c47_emlrt_marshallOut(
    "[ILXE]$matlabroot$/toolbox/coder/coder/+coder/+internal/scalexpAlloc.p"),
                  "resolved", "resolved", 7);
  sf_mex_addfield(*c47_info, c47_b_emlrt_marshallOut(1410811370U), "fileTimeLo",
                  "fileTimeLo", 7);
  sf_mex_addfield(*c47_info, c47_b_emlrt_marshallOut(0U), "fileTimeHi",
                  "fileTimeHi", 7);
  sf_mex_addfield(*c47_info, c47_b_emlrt_marshallOut(0U), "mFileTimeLo",
                  "mFileTimeLo", 7);
  sf_mex_addfield(*c47_info, c47_b_emlrt_marshallOut(0U), "mFileTimeHi",
                  "mFileTimeHi", 7);
  sf_mex_assign(&c47_rhs7, sf_mex_createcellmatrix(0, 1), false);
  sf_mex_assign(&c47_lhs7, sf_mex_createcellmatrix(0, 1), false);
  sf_mex_addfield(*c47_info, sf_mex_duplicatearraysafe(&c47_rhs7), "rhs", "rhs",
                  7);
  sf_mex_addfield(*c47_info, sf_mex_duplicatearraysafe(&c47_lhs7), "lhs", "lhs",
                  7);
  sf_mex_addfield(*c47_info, c47_emlrt_marshallOut(
    "[ILXE]$matlabroot$/toolbox/eml/lib/matlab/elfun/mod.m!intmod"), "context",
                  "context", 8);
  sf_mex_addfield(*c47_info, c47_emlrt_marshallOut("eml_scalar_eg"), "name",
                  "name", 8);
  sf_mex_addfield(*c47_info, c47_emlrt_marshallOut("int16"), "dominantType",
                  "dominantType", 8);
  sf_mex_addfield(*c47_info, c47_emlrt_marshallOut(
    "[ILXE]$matlabroot$/toolbox/eml/lib/matlab/eml/eml_scalar_eg.m"), "resolved",
                  "resolved", 8);
  sf_mex_addfield(*c47_info, c47_b_emlrt_marshallOut(1375984288U), "fileTimeLo",
                  "fileTimeLo", 8);
  sf_mex_addfield(*c47_info, c47_b_emlrt_marshallOut(0U), "fileTimeHi",
                  "fileTimeHi", 8);
  sf_mex_addfield(*c47_info, c47_b_emlrt_marshallOut(0U), "mFileTimeLo",
                  "mFileTimeLo", 8);
  sf_mex_addfield(*c47_info, c47_b_emlrt_marshallOut(0U), "mFileTimeHi",
                  "mFileTimeHi", 8);
  sf_mex_assign(&c47_rhs8, sf_mex_createcellmatrix(0, 1), false);
  sf_mex_assign(&c47_lhs8, sf_mex_createcellmatrix(0, 1), false);
  sf_mex_addfield(*c47_info, sf_mex_duplicatearraysafe(&c47_rhs8), "rhs", "rhs",
                  8);
  sf_mex_addfield(*c47_info, sf_mex_duplicatearraysafe(&c47_lhs8), "lhs", "lhs",
                  8);
  sf_mex_addfield(*c47_info, c47_emlrt_marshallOut(""), "context", "context", 9);
  sf_mex_addfield(*c47_info, c47_emlrt_marshallOut("abs"), "name", "name", 9);
  sf_mex_addfield(*c47_info, c47_emlrt_marshallOut("int16"), "dominantType",
                  "dominantType", 9);
  sf_mex_addfield(*c47_info, c47_emlrt_marshallOut(
    "[ILXE]$matlabroot$/toolbox/eml/lib/matlab/elfun/abs.m"), "resolved",
                  "resolved", 9);
  sf_mex_addfield(*c47_info, c47_b_emlrt_marshallOut(1363717452U), "fileTimeLo",
                  "fileTimeLo", 9);
  sf_mex_addfield(*c47_info, c47_b_emlrt_marshallOut(0U), "fileTimeHi",
                  "fileTimeHi", 9);
  sf_mex_addfield(*c47_info, c47_b_emlrt_marshallOut(0U), "mFileTimeLo",
                  "mFileTimeLo", 9);
  sf_mex_addfield(*c47_info, c47_b_emlrt_marshallOut(0U), "mFileTimeHi",
                  "mFileTimeHi", 9);
  sf_mex_assign(&c47_rhs9, sf_mex_createcellmatrix(0, 1), false);
  sf_mex_assign(&c47_lhs9, sf_mex_createcellmatrix(0, 1), false);
  sf_mex_addfield(*c47_info, sf_mex_duplicatearraysafe(&c47_rhs9), "rhs", "rhs",
                  9);
  sf_mex_addfield(*c47_info, sf_mex_duplicatearraysafe(&c47_lhs9), "lhs", "lhs",
                  9);
  sf_mex_addfield(*c47_info, c47_emlrt_marshallOut(
    "[ILXE]$matlabroot$/toolbox/eml/lib/matlab/elfun/abs.m"), "context",
                  "context", 10);
  sf_mex_addfield(*c47_info, c47_emlrt_marshallOut(
    "coder.internal.isBuiltInNumeric"), "name", "name", 10);
  sf_mex_addfield(*c47_info, c47_emlrt_marshallOut("int16"), "dominantType",
                  "dominantType", 10);
  sf_mex_addfield(*c47_info, c47_emlrt_marshallOut(
    "[ILXE]$matlabroot$/toolbox/shared/coder/coder/+coder/+internal/isBuiltInNumeric.m"),
                  "resolved", "resolved", 10);
  sf_mex_addfield(*c47_info, c47_b_emlrt_marshallOut(1395935456U), "fileTimeLo",
                  "fileTimeLo", 10);
  sf_mex_addfield(*c47_info, c47_b_emlrt_marshallOut(0U), "fileTimeHi",
                  "fileTimeHi", 10);
  sf_mex_addfield(*c47_info, c47_b_emlrt_marshallOut(0U), "mFileTimeLo",
                  "mFileTimeLo", 10);
  sf_mex_addfield(*c47_info, c47_b_emlrt_marshallOut(0U), "mFileTimeHi",
                  "mFileTimeHi", 10);
  sf_mex_assign(&c47_rhs10, sf_mex_createcellmatrix(0, 1), false);
  sf_mex_assign(&c47_lhs10, sf_mex_createcellmatrix(0, 1), false);
  sf_mex_addfield(*c47_info, sf_mex_duplicatearraysafe(&c47_rhs10), "rhs", "rhs",
                  10);
  sf_mex_addfield(*c47_info, sf_mex_duplicatearraysafe(&c47_lhs10), "lhs", "lhs",
                  10);
  sf_mex_addfield(*c47_info, c47_emlrt_marshallOut(
    "[ILXE]$matlabroot$/toolbox/eml/lib/matlab/elfun/abs.m"), "context",
                  "context", 11);
  sf_mex_addfield(*c47_info, c47_emlrt_marshallOut("eml_scalar_abs"), "name",
                  "name", 11);
  sf_mex_addfield(*c47_info, c47_emlrt_marshallOut("int16"), "dominantType",
                  "dominantType", 11);
  sf_mex_addfield(*c47_info, c47_emlrt_marshallOut(
    "[ILXE]$matlabroot$/toolbox/eml/lib/matlab/elfun/eml_scalar_abs.m"),
                  "resolved", "resolved", 11);
  sf_mex_addfield(*c47_info, c47_b_emlrt_marshallOut(1286822312U), "fileTimeLo",
                  "fileTimeLo", 11);
  sf_mex_addfield(*c47_info, c47_b_emlrt_marshallOut(0U), "fileTimeHi",
                  "fileTimeHi", 11);
  sf_mex_addfield(*c47_info, c47_b_emlrt_marshallOut(0U), "mFileTimeLo",
                  "mFileTimeLo", 11);
  sf_mex_addfield(*c47_info, c47_b_emlrt_marshallOut(0U), "mFileTimeHi",
                  "mFileTimeHi", 11);
  sf_mex_assign(&c47_rhs11, sf_mex_createcellmatrix(0, 1), false);
  sf_mex_assign(&c47_lhs11, sf_mex_createcellmatrix(0, 1), false);
  sf_mex_addfield(*c47_info, sf_mex_duplicatearraysafe(&c47_rhs11), "rhs", "rhs",
                  11);
  sf_mex_addfield(*c47_info, sf_mex_duplicatearraysafe(&c47_lhs11), "lhs", "lhs",
                  11);
  sf_mex_addfield(*c47_info, c47_emlrt_marshallOut(""), "context", "context", 12);
  sf_mex_addfield(*c47_info, c47_emlrt_marshallOut("min"), "name", "name", 12);
  sf_mex_addfield(*c47_info, c47_emlrt_marshallOut("int16"), "dominantType",
                  "dominantType", 12);
  sf_mex_addfield(*c47_info, c47_emlrt_marshallOut(
    "[ILXE]$matlabroot$/toolbox/eml/lib/matlab/datafun/min.m"), "resolved",
                  "resolved", 12);
  sf_mex_addfield(*c47_info, c47_b_emlrt_marshallOut(1311258918U), "fileTimeLo",
                  "fileTimeLo", 12);
  sf_mex_addfield(*c47_info, c47_b_emlrt_marshallOut(0U), "fileTimeHi",
                  "fileTimeHi", 12);
  sf_mex_addfield(*c47_info, c47_b_emlrt_marshallOut(0U), "mFileTimeLo",
                  "mFileTimeLo", 12);
  sf_mex_addfield(*c47_info, c47_b_emlrt_marshallOut(0U), "mFileTimeHi",
                  "mFileTimeHi", 12);
  sf_mex_assign(&c47_rhs12, sf_mex_createcellmatrix(0, 1), false);
  sf_mex_assign(&c47_lhs12, sf_mex_createcellmatrix(0, 1), false);
  sf_mex_addfield(*c47_info, sf_mex_duplicatearraysafe(&c47_rhs12), "rhs", "rhs",
                  12);
  sf_mex_addfield(*c47_info, sf_mex_duplicatearraysafe(&c47_lhs12), "lhs", "lhs",
                  12);
  sf_mex_addfield(*c47_info, c47_emlrt_marshallOut(
    "[ILXE]$matlabroot$/toolbox/eml/lib/matlab/datafun/min.m"), "context",
                  "context", 13);
  sf_mex_addfield(*c47_info, c47_emlrt_marshallOut("eml_min_or_max"), "name",
                  "name", 13);
  sf_mex_addfield(*c47_info, c47_emlrt_marshallOut("int16"), "dominantType",
                  "dominantType", 13);
  sf_mex_addfield(*c47_info, c47_emlrt_marshallOut(
    "[ILXE]$matlabroot$/toolbox/eml/lib/matlab/eml/eml_min_or_max.m"),
                  "resolved", "resolved", 13);
  sf_mex_addfield(*c47_info, c47_b_emlrt_marshallOut(1378299584U), "fileTimeLo",
                  "fileTimeLo", 13);
  sf_mex_addfield(*c47_info, c47_b_emlrt_marshallOut(0U), "fileTimeHi",
                  "fileTimeHi", 13);
  sf_mex_addfield(*c47_info, c47_b_emlrt_marshallOut(0U), "mFileTimeLo",
                  "mFileTimeLo", 13);
  sf_mex_addfield(*c47_info, c47_b_emlrt_marshallOut(0U), "mFileTimeHi",
                  "mFileTimeHi", 13);
  sf_mex_assign(&c47_rhs13, sf_mex_createcellmatrix(0, 1), false);
  sf_mex_assign(&c47_lhs13, sf_mex_createcellmatrix(0, 1), false);
  sf_mex_addfield(*c47_info, sf_mex_duplicatearraysafe(&c47_rhs13), "rhs", "rhs",
                  13);
  sf_mex_addfield(*c47_info, sf_mex_duplicatearraysafe(&c47_lhs13), "lhs", "lhs",
                  13);
  sf_mex_addfield(*c47_info, c47_emlrt_marshallOut(
    "[ILXE]$matlabroot$/toolbox/eml/lib/matlab/eml/eml_min_or_max.m!eml_bin_extremum"),
                  "context", "context", 14);
  sf_mex_addfield(*c47_info, c47_emlrt_marshallOut("eml_scalar_eg"), "name",
                  "name", 14);
  sf_mex_addfield(*c47_info, c47_emlrt_marshallOut("int16"), "dominantType",
                  "dominantType", 14);
  sf_mex_addfield(*c47_info, c47_emlrt_marshallOut(
    "[ILXE]$matlabroot$/toolbox/eml/lib/matlab/eml/eml_scalar_eg.m"), "resolved",
                  "resolved", 14);
  sf_mex_addfield(*c47_info, c47_b_emlrt_marshallOut(1375984288U), "fileTimeLo",
                  "fileTimeLo", 14);
  sf_mex_addfield(*c47_info, c47_b_emlrt_marshallOut(0U), "fileTimeHi",
                  "fileTimeHi", 14);
  sf_mex_addfield(*c47_info, c47_b_emlrt_marshallOut(0U), "mFileTimeLo",
                  "mFileTimeLo", 14);
  sf_mex_addfield(*c47_info, c47_b_emlrt_marshallOut(0U), "mFileTimeHi",
                  "mFileTimeHi", 14);
  sf_mex_assign(&c47_rhs14, sf_mex_createcellmatrix(0, 1), false);
  sf_mex_assign(&c47_lhs14, sf_mex_createcellmatrix(0, 1), false);
  sf_mex_addfield(*c47_info, sf_mex_duplicatearraysafe(&c47_rhs14), "rhs", "rhs",
                  14);
  sf_mex_addfield(*c47_info, sf_mex_duplicatearraysafe(&c47_lhs14), "lhs", "lhs",
                  14);
  sf_mex_addfield(*c47_info, c47_emlrt_marshallOut(
    "[ILXE]$matlabroot$/toolbox/eml/lib/matlab/eml/eml_min_or_max.m!eml_bin_extremum"),
                  "context", "context", 15);
  sf_mex_addfield(*c47_info, c47_emlrt_marshallOut("eml_scalexp_alloc"), "name",
                  "name", 15);
  sf_mex_addfield(*c47_info, c47_emlrt_marshallOut("int16"), "dominantType",
                  "dominantType", 15);
  sf_mex_addfield(*c47_info, c47_emlrt_marshallOut(
    "[ILXE]$matlabroot$/toolbox/eml/lib/matlab/eml/eml_scalexp_alloc.m"),
                  "resolved", "resolved", 15);
  sf_mex_addfield(*c47_info, c47_b_emlrt_marshallOut(1375984288U), "fileTimeLo",
                  "fileTimeLo", 15);
  sf_mex_addfield(*c47_info, c47_b_emlrt_marshallOut(0U), "fileTimeHi",
                  "fileTimeHi", 15);
  sf_mex_addfield(*c47_info, c47_b_emlrt_marshallOut(0U), "mFileTimeLo",
                  "mFileTimeLo", 15);
  sf_mex_addfield(*c47_info, c47_b_emlrt_marshallOut(0U), "mFileTimeHi",
                  "mFileTimeHi", 15);
  sf_mex_assign(&c47_rhs15, sf_mex_createcellmatrix(0, 1), false);
  sf_mex_assign(&c47_lhs15, sf_mex_createcellmatrix(0, 1), false);
  sf_mex_addfield(*c47_info, sf_mex_duplicatearraysafe(&c47_rhs15), "rhs", "rhs",
                  15);
  sf_mex_addfield(*c47_info, sf_mex_duplicatearraysafe(&c47_lhs15), "lhs", "lhs",
                  15);
  sf_mex_addfield(*c47_info, c47_emlrt_marshallOut(
    "[ILXE]$matlabroot$/toolbox/eml/lib/matlab/eml/eml_min_or_max.m!eml_bin_extremum"),
                  "context", "context", 16);
  sf_mex_addfield(*c47_info, c47_emlrt_marshallOut("eml_index_class"), "name",
                  "name", 16);
  sf_mex_addfield(*c47_info, c47_emlrt_marshallOut(""), "dominantType",
                  "dominantType", 16);
  sf_mex_addfield(*c47_info, c47_emlrt_marshallOut(
    "[ILXE]$matlabroot$/toolbox/eml/lib/matlab/eml/eml_index_class.m"),
                  "resolved", "resolved", 16);
  sf_mex_addfield(*c47_info, c47_b_emlrt_marshallOut(1323174178U), "fileTimeLo",
                  "fileTimeLo", 16);
  sf_mex_addfield(*c47_info, c47_b_emlrt_marshallOut(0U), "fileTimeHi",
                  "fileTimeHi", 16);
  sf_mex_addfield(*c47_info, c47_b_emlrt_marshallOut(0U), "mFileTimeLo",
                  "mFileTimeLo", 16);
  sf_mex_addfield(*c47_info, c47_b_emlrt_marshallOut(0U), "mFileTimeHi",
                  "mFileTimeHi", 16);
  sf_mex_assign(&c47_rhs16, sf_mex_createcellmatrix(0, 1), false);
  sf_mex_assign(&c47_lhs16, sf_mex_createcellmatrix(0, 1), false);
  sf_mex_addfield(*c47_info, sf_mex_duplicatearraysafe(&c47_rhs16), "rhs", "rhs",
                  16);
  sf_mex_addfield(*c47_info, sf_mex_duplicatearraysafe(&c47_lhs16), "lhs", "lhs",
                  16);
  sf_mex_addfield(*c47_info, c47_emlrt_marshallOut(
    "[ILXE]$matlabroot$/toolbox/eml/lib/matlab/eml/eml_min_or_max.m!eml_scalar_bin_extremum"),
                  "context", "context", 17);
  sf_mex_addfield(*c47_info, c47_emlrt_marshallOut("eml_scalar_eg"), "name",
                  "name", 17);
  sf_mex_addfield(*c47_info, c47_emlrt_marshallOut("int16"), "dominantType",
                  "dominantType", 17);
  sf_mex_addfield(*c47_info, c47_emlrt_marshallOut(
    "[ILXE]$matlabroot$/toolbox/eml/lib/matlab/eml/eml_scalar_eg.m"), "resolved",
                  "resolved", 17);
  sf_mex_addfield(*c47_info, c47_b_emlrt_marshallOut(1375984288U), "fileTimeLo",
                  "fileTimeLo", 17);
  sf_mex_addfield(*c47_info, c47_b_emlrt_marshallOut(0U), "fileTimeHi",
                  "fileTimeHi", 17);
  sf_mex_addfield(*c47_info, c47_b_emlrt_marshallOut(0U), "mFileTimeLo",
                  "mFileTimeLo", 17);
  sf_mex_addfield(*c47_info, c47_b_emlrt_marshallOut(0U), "mFileTimeHi",
                  "mFileTimeHi", 17);
  sf_mex_assign(&c47_rhs17, sf_mex_createcellmatrix(0, 1), false);
  sf_mex_assign(&c47_lhs17, sf_mex_createcellmatrix(0, 1), false);
  sf_mex_addfield(*c47_info, sf_mex_duplicatearraysafe(&c47_rhs17), "rhs", "rhs",
                  17);
  sf_mex_addfield(*c47_info, sf_mex_duplicatearraysafe(&c47_lhs17), "lhs", "lhs",
                  17);
  sf_mex_addfield(*c47_info, c47_emlrt_marshallOut(
    "[ILXE]$matlabroot$/toolbox/eml/lib/matlab/eml/eml_min_or_max.m!eml_scalar_bin_extremum"),
                  "context", "context", 18);
  sf_mex_addfield(*c47_info, c47_emlrt_marshallOut(
    "coder.internal.isBuiltInNumeric"), "name", "name", 18);
  sf_mex_addfield(*c47_info, c47_emlrt_marshallOut("int16"), "dominantType",
                  "dominantType", 18);
  sf_mex_addfield(*c47_info, c47_emlrt_marshallOut(
    "[ILXE]$matlabroot$/toolbox/shared/coder/coder/+coder/+internal/isBuiltInNumeric.m"),
                  "resolved", "resolved", 18);
  sf_mex_addfield(*c47_info, c47_b_emlrt_marshallOut(1395935456U), "fileTimeLo",
                  "fileTimeLo", 18);
  sf_mex_addfield(*c47_info, c47_b_emlrt_marshallOut(0U), "fileTimeHi",
                  "fileTimeHi", 18);
  sf_mex_addfield(*c47_info, c47_b_emlrt_marshallOut(0U), "mFileTimeLo",
                  "mFileTimeLo", 18);
  sf_mex_addfield(*c47_info, c47_b_emlrt_marshallOut(0U), "mFileTimeHi",
                  "mFileTimeHi", 18);
  sf_mex_assign(&c47_rhs18, sf_mex_createcellmatrix(0, 1), false);
  sf_mex_assign(&c47_lhs18, sf_mex_createcellmatrix(0, 1), false);
  sf_mex_addfield(*c47_info, sf_mex_duplicatearraysafe(&c47_rhs18), "rhs", "rhs",
                  18);
  sf_mex_addfield(*c47_info, sf_mex_duplicatearraysafe(&c47_lhs18), "lhs", "lhs",
                  18);
  sf_mex_addfield(*c47_info, c47_emlrt_marshallOut(""), "context", "context", 19);
  sf_mex_addfield(*c47_info, c47_emlrt_marshallOut("sign"), "name", "name", 19);
  sf_mex_addfield(*c47_info, c47_emlrt_marshallOut("int16"), "dominantType",
                  "dominantType", 19);
  sf_mex_addfield(*c47_info, c47_emlrt_marshallOut(
    "[ILXE]$matlabroot$/toolbox/eml/lib/matlab/elfun/sign.m"), "resolved",
                  "resolved", 19);
  sf_mex_addfield(*c47_info, c47_b_emlrt_marshallOut(1363717456U), "fileTimeLo",
                  "fileTimeLo", 19);
  sf_mex_addfield(*c47_info, c47_b_emlrt_marshallOut(0U), "fileTimeHi",
                  "fileTimeHi", 19);
  sf_mex_addfield(*c47_info, c47_b_emlrt_marshallOut(0U), "mFileTimeLo",
                  "mFileTimeLo", 19);
  sf_mex_addfield(*c47_info, c47_b_emlrt_marshallOut(0U), "mFileTimeHi",
                  "mFileTimeHi", 19);
  sf_mex_assign(&c47_rhs19, sf_mex_createcellmatrix(0, 1), false);
  sf_mex_assign(&c47_lhs19, sf_mex_createcellmatrix(0, 1), false);
  sf_mex_addfield(*c47_info, sf_mex_duplicatearraysafe(&c47_rhs19), "rhs", "rhs",
                  19);
  sf_mex_addfield(*c47_info, sf_mex_duplicatearraysafe(&c47_lhs19), "lhs", "lhs",
                  19);
  sf_mex_addfield(*c47_info, c47_emlrt_marshallOut(
    "[ILXE]$matlabroot$/toolbox/eml/lib/matlab/elfun/sign.m"), "context",
                  "context", 20);
  sf_mex_addfield(*c47_info, c47_emlrt_marshallOut(
    "coder.internal.isBuiltInNumeric"), "name", "name", 20);
  sf_mex_addfield(*c47_info, c47_emlrt_marshallOut("int16"), "dominantType",
                  "dominantType", 20);
  sf_mex_addfield(*c47_info, c47_emlrt_marshallOut(
    "[ILXE]$matlabroot$/toolbox/shared/coder/coder/+coder/+internal/isBuiltInNumeric.m"),
                  "resolved", "resolved", 20);
  sf_mex_addfield(*c47_info, c47_b_emlrt_marshallOut(1395935456U), "fileTimeLo",
                  "fileTimeLo", 20);
  sf_mex_addfield(*c47_info, c47_b_emlrt_marshallOut(0U), "fileTimeHi",
                  "fileTimeHi", 20);
  sf_mex_addfield(*c47_info, c47_b_emlrt_marshallOut(0U), "mFileTimeLo",
                  "mFileTimeLo", 20);
  sf_mex_addfield(*c47_info, c47_b_emlrt_marshallOut(0U), "mFileTimeHi",
                  "mFileTimeHi", 20);
  sf_mex_assign(&c47_rhs20, sf_mex_createcellmatrix(0, 1), false);
  sf_mex_assign(&c47_lhs20, sf_mex_createcellmatrix(0, 1), false);
  sf_mex_addfield(*c47_info, sf_mex_duplicatearraysafe(&c47_rhs20), "rhs", "rhs",
                  20);
  sf_mex_addfield(*c47_info, sf_mex_duplicatearraysafe(&c47_lhs20), "lhs", "lhs",
                  20);
  sf_mex_addfield(*c47_info, c47_emlrt_marshallOut(
    "[ILXE]$matlabroot$/toolbox/eml/lib/matlab/elfun/sign.m"), "context",
                  "context", 21);
  sf_mex_addfield(*c47_info, c47_emlrt_marshallOut("eml_scalar_sign"), "name",
                  "name", 21);
  sf_mex_addfield(*c47_info, c47_emlrt_marshallOut("int16"), "dominantType",
                  "dominantType", 21);
  sf_mex_addfield(*c47_info, c47_emlrt_marshallOut(
    "[ILXE]$matlabroot$/toolbox/eml/lib/matlab/elfun/eml_scalar_sign.m"),
                  "resolved", "resolved", 21);
  sf_mex_addfield(*c47_info, c47_b_emlrt_marshallOut(1356545094U), "fileTimeLo",
                  "fileTimeLo", 21);
  sf_mex_addfield(*c47_info, c47_b_emlrt_marshallOut(0U), "fileTimeHi",
                  "fileTimeHi", 21);
  sf_mex_addfield(*c47_info, c47_b_emlrt_marshallOut(0U), "mFileTimeLo",
                  "mFileTimeLo", 21);
  sf_mex_addfield(*c47_info, c47_b_emlrt_marshallOut(0U), "mFileTimeHi",
                  "mFileTimeHi", 21);
  sf_mex_assign(&c47_rhs21, sf_mex_createcellmatrix(0, 1), false);
  sf_mex_assign(&c47_lhs21, sf_mex_createcellmatrix(0, 1), false);
  sf_mex_addfield(*c47_info, sf_mex_duplicatearraysafe(&c47_rhs21), "rhs", "rhs",
                  21);
  sf_mex_addfield(*c47_info, sf_mex_duplicatearraysafe(&c47_lhs21), "lhs", "lhs",
                  21);
  sf_mex_destroy(&c47_rhs0);
  sf_mex_destroy(&c47_lhs0);
  sf_mex_destroy(&c47_rhs1);
  sf_mex_destroy(&c47_lhs1);
  sf_mex_destroy(&c47_rhs2);
  sf_mex_destroy(&c47_lhs2);
  sf_mex_destroy(&c47_rhs3);
  sf_mex_destroy(&c47_lhs3);
  sf_mex_destroy(&c47_rhs4);
  sf_mex_destroy(&c47_lhs4);
  sf_mex_destroy(&c47_rhs5);
  sf_mex_destroy(&c47_lhs5);
  sf_mex_destroy(&c47_rhs6);
  sf_mex_destroy(&c47_lhs6);
  sf_mex_destroy(&c47_rhs7);
  sf_mex_destroy(&c47_lhs7);
  sf_mex_destroy(&c47_rhs8);
  sf_mex_destroy(&c47_lhs8);
  sf_mex_destroy(&c47_rhs9);
  sf_mex_destroy(&c47_lhs9);
  sf_mex_destroy(&c47_rhs10);
  sf_mex_destroy(&c47_lhs10);
  sf_mex_destroy(&c47_rhs11);
  sf_mex_destroy(&c47_lhs11);
  sf_mex_destroy(&c47_rhs12);
  sf_mex_destroy(&c47_lhs12);
  sf_mex_destroy(&c47_rhs13);
  sf_mex_destroy(&c47_lhs13);
  sf_mex_destroy(&c47_rhs14);
  sf_mex_destroy(&c47_lhs14);
  sf_mex_destroy(&c47_rhs15);
  sf_mex_destroy(&c47_lhs15);
  sf_mex_destroy(&c47_rhs16);
  sf_mex_destroy(&c47_lhs16);
  sf_mex_destroy(&c47_rhs17);
  sf_mex_destroy(&c47_lhs17);
  sf_mex_destroy(&c47_rhs18);
  sf_mex_destroy(&c47_lhs18);
  sf_mex_destroy(&c47_rhs19);
  sf_mex_destroy(&c47_lhs19);
  sf_mex_destroy(&c47_rhs20);
  sf_mex_destroy(&c47_lhs20);
  sf_mex_destroy(&c47_rhs21);
  sf_mex_destroy(&c47_lhs21);
}

static const mxArray *c47_emlrt_marshallOut(const char * c47_u)
{
  const mxArray *c47_y = NULL;
  c47_y = NULL;
  sf_mex_assign(&c47_y, sf_mex_create("y", c47_u, 15, 0U, 0U, 0U, 2, 1, strlen
    (c47_u)), false);
  return c47_y;
}

static const mxArray *c47_b_emlrt_marshallOut(const uint32_T c47_u)
{
  const mxArray *c47_y = NULL;
  c47_y = NULL;
  sf_mex_assign(&c47_y, sf_mex_create("y", &c47_u, 7, 0U, 0U, 0U, 0), false);
  return c47_y;
}

static int16_T c47_mod(SFc47_LessonIII_startInstanceStruct *chartInstance,
  int16_T c47_x)
{
  int16_T c47_b_x;
  int16_T c47_t;
  c47_b_x = c47_x;
  c47_eml_scalar_eg(chartInstance);
  c47_t = c47_div_s16s32_floor(chartInstance, (int32_T)c47_b_x, 360);
  c47_t = (int16_T)(c47_t * 360);
  return (int16_T)(c47_b_x - c47_t);
}

static void c47_eml_scalar_eg(SFc47_LessonIII_startInstanceStruct *chartInstance)
{
  (void)chartInstance;
}

static const mxArray *c47_c_sf_marshallOut(void *chartInstanceVoid, void
  *c47_inData)
{
  const mxArray *c47_mxArrayOutData = NULL;
  int32_T c47_u;
  const mxArray *c47_y = NULL;
  SFc47_LessonIII_startInstanceStruct *chartInstance;
  chartInstance = (SFc47_LessonIII_startInstanceStruct *)chartInstanceVoid;
  c47_mxArrayOutData = NULL;
  c47_u = *(int32_T *)c47_inData;
  c47_y = NULL;
  sf_mex_assign(&c47_y, sf_mex_create("y", &c47_u, 6, 0U, 0U, 0U, 0), false);
  sf_mex_assign(&c47_mxArrayOutData, c47_y, false);
  return c47_mxArrayOutData;
}

static int32_T c47_d_emlrt_marshallIn(SFc47_LessonIII_startInstanceStruct
  *chartInstance, const mxArray *c47_u, const emlrtMsgIdentifier *c47_parentId)
{
  int32_T c47_y;
  int32_T c47_i15;
  (void)chartInstance;
  sf_mex_import(c47_parentId, sf_mex_dup(c47_u), &c47_i15, 1, 6, 0U, 0, 0U, 0);
  c47_y = c47_i15;
  sf_mex_destroy(&c47_u);
  return c47_y;
}

static void c47_c_sf_marshallIn(void *chartInstanceVoid, const mxArray
  *c47_mxArrayInData, const char_T *c47_varName, void *c47_outData)
{
  const mxArray *c47_b_sfEvent;
  const char_T *c47_identifier;
  emlrtMsgIdentifier c47_thisId;
  int32_T c47_y;
  SFc47_LessonIII_startInstanceStruct *chartInstance;
  chartInstance = (SFc47_LessonIII_startInstanceStruct *)chartInstanceVoid;
  c47_b_sfEvent = sf_mex_dup(c47_mxArrayInData);
  c47_identifier = c47_varName;
  c47_thisId.fIdentifier = c47_identifier;
  c47_thisId.fParent = NULL;
  c47_y = c47_d_emlrt_marshallIn(chartInstance, sf_mex_dup(c47_b_sfEvent),
    &c47_thisId);
  sf_mex_destroy(&c47_b_sfEvent);
  *(int32_T *)c47_outData = c47_y;
  sf_mex_destroy(&c47_mxArrayInData);
}

static uint8_T c47_e_emlrt_marshallIn(SFc47_LessonIII_startInstanceStruct
  *chartInstance, const mxArray *c47_b_is_active_c47_LessonIII_start, const
  char_T *c47_identifier)
{
  uint8_T c47_y;
  emlrtMsgIdentifier c47_thisId;
  c47_thisId.fIdentifier = c47_identifier;
  c47_thisId.fParent = NULL;
  c47_y = c47_f_emlrt_marshallIn(chartInstance, sf_mex_dup
    (c47_b_is_active_c47_LessonIII_start), &c47_thisId);
  sf_mex_destroy(&c47_b_is_active_c47_LessonIII_start);
  return c47_y;
}

static uint8_T c47_f_emlrt_marshallIn(SFc47_LessonIII_startInstanceStruct
  *chartInstance, const mxArray *c47_u, const emlrtMsgIdentifier *c47_parentId)
{
  uint8_T c47_y;
  uint8_T c47_u0;
  (void)chartInstance;
  sf_mex_import(c47_parentId, sf_mex_dup(c47_u), &c47_u0, 1, 3, 0U, 0, 0U, 0);
  c47_y = c47_u0;
  sf_mex_destroy(&c47_u);
  return c47_y;
}

static int16_T c47_div_s16s32_floor(SFc47_LessonIII_startInstanceStruct
  *chartInstance, int32_T c47_numerator, int32_T c47_denominator)
{
  int16_T c47_quotient;
  uint32_T c47_absNumerator;
  uint32_T c47_absDenominator;
  boolean_T c47_quotientNeedsNegation;
  uint32_T c47_tempAbsQuotient;
  (void)chartInstance;
  if (c47_denominator == 0) {
    if (c47_numerator >= 0) {
      c47_quotient = MAX_int16_T;
    } else {
      c47_quotient = MIN_int16_T;
    }

    _SFD_OVERFLOW_DETECTION(SFDB_DIVIDE_BY_ZERO);
  } else {
    if (c47_numerator >= 0) {
      c47_absNumerator = (uint32_T)c47_numerator;
    } else {
      c47_absNumerator = (uint32_T)-c47_numerator;
    }

    if (c47_denominator >= 0) {
      c47_absDenominator = (uint32_T)c47_denominator;
    } else {
      c47_absDenominator = (uint32_T)-c47_denominator;
    }

    c47_quotientNeedsNegation = (c47_numerator < 0 != c47_denominator < 0);
    c47_tempAbsQuotient = c47_absNumerator / c47_absDenominator;
    if (c47_quotientNeedsNegation) {
      c47_absNumerator %= c47_absDenominator;
      if (c47_absNumerator > 0U) {
        c47_tempAbsQuotient++;
      }
    }

    if (c47_quotientNeedsNegation) {
      c47_quotient = (int16_T)-(int32_T)c47_tempAbsQuotient;
    } else {
      c47_quotient = (int16_T)c47_tempAbsQuotient;
    }
  }

  return c47_quotient;
}

static void init_dsm_address_info(SFc47_LessonIII_startInstanceStruct
  *chartInstance)
{
  (void)chartInstance;
}

static void init_simulink_io_address(SFc47_LessonIII_startInstanceStruct
  *chartInstance)
{
  chartInstance->c47_targetO = (int16_T *)ssGetInputPortSignal_wrapper
    (chartInstance->S, 0);
  chartInstance->c47_realDorien = (int16_T *)ssGetOutputPortSignal_wrapper
    (chartInstance->S, 1);
  chartInstance->c47_direc = (int16_T *)ssGetOutputPortSignal_wrapper
    (chartInstance->S, 2);
  chartInstance->c47_myO = (int16_T *)ssGetInputPortSignal_wrapper
    (chartInstance->S, 1);
}

/* SFunction Glue Code */
#ifdef utFree
#undef utFree
#endif

#ifdef utMalloc
#undef utMalloc
#endif

#ifdef __cplusplus

extern "C" void *utMalloc(size_t size);
extern "C" void utFree(void*);

#else

extern void *utMalloc(size_t size);
extern void utFree(void*);

#endif

void sf_c47_LessonIII_start_get_check_sum(mxArray *plhs[])
{
  ((real_T *)mxGetPr((plhs[0])))[0] = (real_T)(453741273U);
  ((real_T *)mxGetPr((plhs[0])))[1] = (real_T)(510639413U);
  ((real_T *)mxGetPr((plhs[0])))[2] = (real_T)(2490578805U);
  ((real_T *)mxGetPr((plhs[0])))[3] = (real_T)(3463910138U);
}

mxArray* sf_c47_LessonIII_start_get_post_codegen_info(void);
mxArray *sf_c47_LessonIII_start_get_autoinheritance_info(void)
{
  const char *autoinheritanceFields[] = { "checksum", "inputs", "parameters",
    "outputs", "locals", "postCodegenInfo" };

  mxArray *mxAutoinheritanceInfo = mxCreateStructMatrix(1, 1, sizeof
    (autoinheritanceFields)/sizeof(autoinheritanceFields[0]),
    autoinheritanceFields);

  {
    mxArray *mxChecksum = mxCreateString("7ivw4cCNB5MBh41uVb3SPB");
    mxSetField(mxAutoinheritanceInfo,0,"checksum",mxChecksum);
  }

  {
    const char *dataFields[] = { "size", "type", "complexity" };

    mxArray *mxData = mxCreateStructMatrix(1,2,3,dataFields);

    {
      mxArray *mxSize = mxCreateDoubleMatrix(1,2,mxREAL);
      double *pr = mxGetPr(mxSize);
      pr[0] = (double)(1);
      pr[1] = (double)(1);
      mxSetField(mxData,0,"size",mxSize);
    }

    {
      const char *typeFields[] = { "base", "fixpt" };

      mxArray *mxType = mxCreateStructMatrix(1,1,2,typeFields);
      mxSetField(mxType,0,"base",mxCreateDoubleScalar(6));
      mxSetField(mxType,0,"fixpt",mxCreateDoubleMatrix(0,0,mxREAL));
      mxSetField(mxData,0,"type",mxType);
    }

    mxSetField(mxData,0,"complexity",mxCreateDoubleScalar(0));

    {
      mxArray *mxSize = mxCreateDoubleMatrix(1,2,mxREAL);
      double *pr = mxGetPr(mxSize);
      pr[0] = (double)(1);
      pr[1] = (double)(1);
      mxSetField(mxData,1,"size",mxSize);
    }

    {
      const char *typeFields[] = { "base", "fixpt" };

      mxArray *mxType = mxCreateStructMatrix(1,1,2,typeFields);
      mxSetField(mxType,0,"base",mxCreateDoubleScalar(6));
      mxSetField(mxType,0,"fixpt",mxCreateDoubleMatrix(0,0,mxREAL));
      mxSetField(mxData,1,"type",mxType);
    }

    mxSetField(mxData,1,"complexity",mxCreateDoubleScalar(0));
    mxSetField(mxAutoinheritanceInfo,0,"inputs",mxData);
  }

  {
    mxSetField(mxAutoinheritanceInfo,0,"parameters",mxCreateDoubleMatrix(0,0,
                mxREAL));
  }

  {
    const char *dataFields[] = { "size", "type", "complexity" };

    mxArray *mxData = mxCreateStructMatrix(1,2,3,dataFields);

    {
      mxArray *mxSize = mxCreateDoubleMatrix(1,2,mxREAL);
      double *pr = mxGetPr(mxSize);
      pr[0] = (double)(1);
      pr[1] = (double)(1);
      mxSetField(mxData,0,"size",mxSize);
    }

    {
      const char *typeFields[] = { "base", "fixpt" };

      mxArray *mxType = mxCreateStructMatrix(1,1,2,typeFields);
      mxSetField(mxType,0,"base",mxCreateDoubleScalar(6));
      mxSetField(mxType,0,"fixpt",mxCreateDoubleMatrix(0,0,mxREAL));
      mxSetField(mxData,0,"type",mxType);
    }

    mxSetField(mxData,0,"complexity",mxCreateDoubleScalar(0));

    {
      mxArray *mxSize = mxCreateDoubleMatrix(1,2,mxREAL);
      double *pr = mxGetPr(mxSize);
      pr[0] = (double)(1);
      pr[1] = (double)(1);
      mxSetField(mxData,1,"size",mxSize);
    }

    {
      const char *typeFields[] = { "base", "fixpt" };

      mxArray *mxType = mxCreateStructMatrix(1,1,2,typeFields);
      mxSetField(mxType,0,"base",mxCreateDoubleScalar(6));
      mxSetField(mxType,0,"fixpt",mxCreateDoubleMatrix(0,0,mxREAL));
      mxSetField(mxData,1,"type",mxType);
    }

    mxSetField(mxData,1,"complexity",mxCreateDoubleScalar(0));
    mxSetField(mxAutoinheritanceInfo,0,"outputs",mxData);
  }

  {
    mxSetField(mxAutoinheritanceInfo,0,"locals",mxCreateDoubleMatrix(0,0,mxREAL));
  }

  {
    mxArray* mxPostCodegenInfo = sf_c47_LessonIII_start_get_post_codegen_info();
    mxSetField(mxAutoinheritanceInfo,0,"postCodegenInfo",mxPostCodegenInfo);
  }

  return(mxAutoinheritanceInfo);
}

mxArray *sf_c47_LessonIII_start_third_party_uses_info(void)
{
  mxArray * mxcell3p = mxCreateCellMatrix(1,0);
  return(mxcell3p);
}

mxArray *sf_c47_LessonIII_start_jit_fallback_info(void)
{
  const char *infoFields[] = { "fallbackType", "fallbackReason",
    "incompatibleSymbol", };

  mxArray *mxInfo = mxCreateStructMatrix(1, 1, 3, infoFields);
  mxArray *fallbackReason = mxCreateString("feature_off");
  mxArray *incompatibleSymbol = mxCreateString("");
  mxArray *fallbackType = mxCreateString("early");
  mxSetField(mxInfo, 0, infoFields[0], fallbackType);
  mxSetField(mxInfo, 0, infoFields[1], fallbackReason);
  mxSetField(mxInfo, 0, infoFields[2], incompatibleSymbol);
  return mxInfo;
}

mxArray *sf_c47_LessonIII_start_updateBuildInfo_args_info(void)
{
  mxArray *mxBIArgs = mxCreateCellMatrix(1,0);
  return mxBIArgs;
}

mxArray* sf_c47_LessonIII_start_get_post_codegen_info(void)
{
  const char* fieldNames[] = { "exportedFunctionsUsedByThisChart",
    "exportedFunctionsChecksum" };

  mwSize dims[2] = { 1, 1 };

  mxArray* mxPostCodegenInfo = mxCreateStructArray(2, dims, sizeof(fieldNames)/
    sizeof(fieldNames[0]), fieldNames);

  {
    mxArray* mxExportedFunctionsChecksum = mxCreateString("");
    mwSize exp_dims[2] = { 0, 1 };

    mxArray* mxExportedFunctionsUsedByThisChart = mxCreateCellArray(2, exp_dims);
    mxSetField(mxPostCodegenInfo, 0, "exportedFunctionsUsedByThisChart",
               mxExportedFunctionsUsedByThisChart);
    mxSetField(mxPostCodegenInfo, 0, "exportedFunctionsChecksum",
               mxExportedFunctionsChecksum);
  }

  return mxPostCodegenInfo;
}

static const mxArray *sf_get_sim_state_info_c47_LessonIII_start(void)
{
  const char *infoFields[] = { "chartChecksum", "varInfo" };

  mxArray *mxInfo = mxCreateStructMatrix(1, 1, 2, infoFields);
  const char *infoEncStr[] = {
    "100 S1x3'type','srcId','name','auxInfo'{{M[1],M[6],T\"direc\",},{M[1],M[5],T\"realDorien\",},{M[8],M[0],T\"is_active_c47_LessonIII_start\",}}"
  };

  mxArray *mxVarInfo = sf_mex_decode_encoded_mx_struct_array(infoEncStr, 3, 10);
  mxArray *mxChecksum = mxCreateDoubleMatrix(1, 4, mxREAL);
  sf_c47_LessonIII_start_get_check_sum(&mxChecksum);
  mxSetField(mxInfo, 0, infoFields[0], mxChecksum);
  mxSetField(mxInfo, 0, infoFields[1], mxVarInfo);
  return mxInfo;
}

static void chart_debug_initialization(SimStruct *S, unsigned int
  fullDebuggerInitialization)
{
  if (!sim_mode_is_rtw_gen(S)) {
    SFc47_LessonIII_startInstanceStruct *chartInstance;
    ChartRunTimeInfo * crtInfo = (ChartRunTimeInfo *)(ssGetUserData(S));
    ChartInfoStruct * chartInfo = (ChartInfoStruct *)(crtInfo->instanceInfo);
    chartInstance = (SFc47_LessonIII_startInstanceStruct *)
      chartInfo->chartInstance;
    if (ssIsFirstInitCond(S) && fullDebuggerInitialization==1) {
      /* do this only if simulation is starting */
      {
        unsigned int chartAlreadyPresent;
        chartAlreadyPresent = sf_debug_initialize_chart
          (sfGlobalDebugInstanceStruct,
           _LessonIII_startMachineNumber_,
           47,
           1,
           1,
           0,
           4,
           0,
           0,
           0,
           0,
           0,
           &(chartInstance->chartNumber),
           &(chartInstance->instanceNumber),
           (void *)S);

        /* Each instance must initialize its own list of scripts */
        init_script_number_translation(_LessonIII_startMachineNumber_,
          chartInstance->chartNumber,chartInstance->instanceNumber);
        if (chartAlreadyPresent==0) {
          /* this is the first instance */
          sf_debug_set_chart_disable_implicit_casting
            (sfGlobalDebugInstanceStruct,_LessonIII_startMachineNumber_,
             chartInstance->chartNumber,1);
          sf_debug_set_chart_event_thresholds(sfGlobalDebugInstanceStruct,
            _LessonIII_startMachineNumber_,
            chartInstance->chartNumber,
            0,
            0,
            0);
          _SFD_SET_DATA_PROPS(0,1,1,0,"targetO");
          _SFD_SET_DATA_PROPS(1,2,0,1,"realDorien");
          _SFD_SET_DATA_PROPS(2,2,0,1,"direc");
          _SFD_SET_DATA_PROPS(3,1,1,0,"myO");
          _SFD_STATE_INFO(0,0,2);
          _SFD_CH_SUBSTATE_COUNT(0);
          _SFD_CH_SUBSTATE_DECOMP(0);
        }

        _SFD_CV_INIT_CHART(0,0,0,0);

        {
          _SFD_CV_INIT_STATE(0,0,0,0,0,0,NULL,NULL);
        }

        _SFD_CV_INIT_TRANS(0,0,NULL,NULL,0,NULL);

        /* Initialization of MATLAB Function Model Coverage */
        _SFD_CV_INIT_EML(0,1,1,2,0,12,0,0,0,0,0);
        _SFD_CV_INIT_EML_FCN(0,0,"eML_blk_kernel",0,-1,272);
        _SFD_CV_INIT_EML_SATURATION(0,1,0,110,-1,118);
        _SFD_CV_INIT_EML_SATURATION(0,1,1,114,-1,117);
        _SFD_CV_INIT_EML_SATURATION(0,1,2,119,-1,135);
        _SFD_CV_INIT_EML_SATURATION(0,1,3,123,-1,134);
        _SFD_CV_INIT_EML_SATURATION(0,1,4,141,-1,149);
        _SFD_CV_INIT_EML_SATURATION(0,1,5,145,-1,148);
        _SFD_CV_INIT_EML_SATURATION(0,1,6,150,-1,166);
        _SFD_CV_INIT_EML_SATURATION(0,1,7,154,-1,165);
        _SFD_CV_INIT_EML_SATURATION(0,1,8,182,-1,185);
        _SFD_CV_INIT_EML_SATURATION(0,1,9,208,-1,219);
        _SFD_CV_INIT_EML_SATURATION(0,1,10,57,-1,68);
        _SFD_CV_INIT_EML_SATURATION(0,1,11,81,-1,88);
        _SFD_CV_INIT_EML_IF(0,1,0,138,166,188,225);
        _SFD_CV_INIT_EML_IF(0,1,1,226,242,-1,271);
        _SFD_CV_INIT_EML_RELATIONAL(0,1,0,141,166,0,2);
        _SFD_CV_INIT_EML_RELATIONAL(0,1,1,229,242,-1,2);
        _SFD_SET_DATA_COMPILED_PROPS(0,SF_INT16,0,NULL,0,0,0,0.0,1.0,0,0,
          (MexFcnForType)c47_sf_marshallOut,(MexInFcnForType)NULL);
        _SFD_SET_DATA_COMPILED_PROPS(1,SF_INT16,0,NULL,0,0,0,0.0,1.0,0,0,
          (MexFcnForType)c47_sf_marshallOut,(MexInFcnForType)c47_sf_marshallIn);
        _SFD_SET_DATA_COMPILED_PROPS(2,SF_INT16,0,NULL,0,0,0,0.0,1.0,0,0,
          (MexFcnForType)c47_sf_marshallOut,(MexInFcnForType)c47_sf_marshallIn);
        _SFD_SET_DATA_COMPILED_PROPS(3,SF_INT16,0,NULL,0,0,0,0.0,1.0,0,0,
          (MexFcnForType)c47_sf_marshallOut,(MexInFcnForType)NULL);
        _SFD_SET_DATA_VALUE_PTR(0U, chartInstance->c47_targetO);
        _SFD_SET_DATA_VALUE_PTR(1U, chartInstance->c47_realDorien);
        _SFD_SET_DATA_VALUE_PTR(2U, chartInstance->c47_direc);
        _SFD_SET_DATA_VALUE_PTR(3U, chartInstance->c47_myO);
      }
    } else {
      sf_debug_reset_current_state_configuration(sfGlobalDebugInstanceStruct,
        _LessonIII_startMachineNumber_,chartInstance->chartNumber,
        chartInstance->instanceNumber);
    }
  }
}

static const char* sf_get_instance_specialization(void)
{
  return "iLKkTZfgeUWbCHGkvxHUyF";
}

static void sf_opaque_initialize_c47_LessonIII_start(void *chartInstanceVar)
{
  chart_debug_initialization(((SFc47_LessonIII_startInstanceStruct*)
    chartInstanceVar)->S,0);
  initialize_params_c47_LessonIII_start((SFc47_LessonIII_startInstanceStruct*)
    chartInstanceVar);
  initialize_c47_LessonIII_start((SFc47_LessonIII_startInstanceStruct*)
    chartInstanceVar);
}

static void sf_opaque_enable_c47_LessonIII_start(void *chartInstanceVar)
{
  enable_c47_LessonIII_start((SFc47_LessonIII_startInstanceStruct*)
    chartInstanceVar);
}

static void sf_opaque_disable_c47_LessonIII_start(void *chartInstanceVar)
{
  disable_c47_LessonIII_start((SFc47_LessonIII_startInstanceStruct*)
    chartInstanceVar);
}

static void sf_opaque_gateway_c47_LessonIII_start(void *chartInstanceVar)
{
  sf_gateway_c47_LessonIII_start((SFc47_LessonIII_startInstanceStruct*)
    chartInstanceVar);
}

static const mxArray* sf_opaque_get_sim_state_c47_LessonIII_start(SimStruct* S)
{
  ChartRunTimeInfo * crtInfo = (ChartRunTimeInfo *)(ssGetUserData(S));
  ChartInfoStruct * chartInfo = (ChartInfoStruct *)(crtInfo->instanceInfo);
  return get_sim_state_c47_LessonIII_start((SFc47_LessonIII_startInstanceStruct*)
    chartInfo->chartInstance);         /* raw sim ctx */
}

static void sf_opaque_set_sim_state_c47_LessonIII_start(SimStruct* S, const
  mxArray *st)
{
  ChartRunTimeInfo * crtInfo = (ChartRunTimeInfo *)(ssGetUserData(S));
  ChartInfoStruct * chartInfo = (ChartInfoStruct *)(crtInfo->instanceInfo);
  set_sim_state_c47_LessonIII_start((SFc47_LessonIII_startInstanceStruct*)
    chartInfo->chartInstance, st);
}

static void sf_opaque_terminate_c47_LessonIII_start(void *chartInstanceVar)
{
  if (chartInstanceVar!=NULL) {
    SimStruct *S = ((SFc47_LessonIII_startInstanceStruct*) chartInstanceVar)->S;
    ChartRunTimeInfo * crtInfo = (ChartRunTimeInfo *)(ssGetUserData(S));
    if (sim_mode_is_rtw_gen(S) || sim_mode_is_external(S)) {
      sf_clear_rtw_identifier(S);
      unload_LessonIII_start_optimization_info();
    }

    finalize_c47_LessonIII_start((SFc47_LessonIII_startInstanceStruct*)
      chartInstanceVar);
    utFree(chartInstanceVar);
    if (crtInfo != NULL) {
      utFree(crtInfo);
    }

    ssSetUserData(S,NULL);
  }
}

static void sf_opaque_init_subchart_simstructs(void *chartInstanceVar)
{
  initSimStructsc47_LessonIII_start((SFc47_LessonIII_startInstanceStruct*)
    chartInstanceVar);
}

extern unsigned int sf_machine_global_initializer_called(void);
static void mdlProcessParameters_c47_LessonIII_start(SimStruct *S)
{
  int i;
  for (i=0;i<ssGetNumRunTimeParams(S);i++) {
    if (ssGetSFcnParamTunable(S,i)) {
      ssUpdateDlgParamAsRunTimeParam(S,i);
    }
  }

  if (sf_machine_global_initializer_called()) {
    ChartRunTimeInfo * crtInfo = (ChartRunTimeInfo *)(ssGetUserData(S));
    ChartInfoStruct * chartInfo = (ChartInfoStruct *)(crtInfo->instanceInfo);
    initialize_params_c47_LessonIII_start((SFc47_LessonIII_startInstanceStruct*)
      (chartInfo->chartInstance));
  }
}

static void mdlSetWorkWidths_c47_LessonIII_start(SimStruct *S)
{
  if (sim_mode_is_rtw_gen(S) || sim_mode_is_external(S)) {
    mxArray *infoStruct = load_LessonIII_start_optimization_info();
    int_T chartIsInlinable =
      (int_T)sf_is_chart_inlinable(sf_get_instance_specialization(),infoStruct,
      47);
    ssSetStateflowIsInlinable(S,chartIsInlinable);
    ssSetRTWCG(S,sf_rtw_info_uint_prop(sf_get_instance_specialization(),
                infoStruct,47,"RTWCG"));
    ssSetEnableFcnIsTrivial(S,1);
    ssSetDisableFcnIsTrivial(S,1);
    ssSetNotMultipleInlinable(S,sf_rtw_info_uint_prop
      (sf_get_instance_specialization(),infoStruct,47,
       "gatewayCannotBeInlinedMultipleTimes"));
    sf_update_buildInfo(sf_get_instance_specialization(),infoStruct,47);
    if (chartIsInlinable) {
      ssSetInputPortOptimOpts(S, 0, SS_REUSABLE_AND_LOCAL);
      ssSetInputPortOptimOpts(S, 1, SS_REUSABLE_AND_LOCAL);
      sf_mark_chart_expressionable_inputs(S,sf_get_instance_specialization(),
        infoStruct,47,2);
      sf_mark_chart_reusable_outputs(S,sf_get_instance_specialization(),
        infoStruct,47,2);
    }

    {
      unsigned int outPortIdx;
      for (outPortIdx=1; outPortIdx<=2; ++outPortIdx) {
        ssSetOutputPortOptimizeInIR(S, outPortIdx, 1U);
      }
    }

    {
      unsigned int inPortIdx;
      for (inPortIdx=0; inPortIdx < 2; ++inPortIdx) {
        ssSetInputPortOptimizeInIR(S, inPortIdx, 1U);
      }
    }

    sf_set_rtw_dwork_info(S,sf_get_instance_specialization(),infoStruct,47);
    ssSetHasSubFunctions(S,!(chartIsInlinable));
  } else {
  }

  ssSetOptions(S,ssGetOptions(S)|SS_OPTION_WORKS_WITH_CODE_REUSE);
  ssSetChecksum0(S,(3228832367U));
  ssSetChecksum1(S,(103000363U));
  ssSetChecksum2(S,(4047245248U));
  ssSetChecksum3(S,(2457819482U));
  ssSetmdlDerivatives(S, NULL);
  ssSetExplicitFCSSCtrl(S,1);
  ssSupportsMultipleExecInstances(S,1);
}

static void mdlRTW_c47_LessonIII_start(SimStruct *S)
{
  if (sim_mode_is_rtw_gen(S)) {
    ssWriteRTWStrParam(S, "StateflowChartType", "Embedded MATLAB");
  }
}

static void mdlStart_c47_LessonIII_start(SimStruct *S)
{
  SFc47_LessonIII_startInstanceStruct *chartInstance;
  ChartRunTimeInfo * crtInfo = (ChartRunTimeInfo *)utMalloc(sizeof
    (ChartRunTimeInfo));
  chartInstance = (SFc47_LessonIII_startInstanceStruct *)utMalloc(sizeof
    (SFc47_LessonIII_startInstanceStruct));
  memset(chartInstance, 0, sizeof(SFc47_LessonIII_startInstanceStruct));
  if (chartInstance==NULL) {
    sf_mex_error_message("Could not allocate memory for chart instance.");
  }

  chartInstance->chartInfo.chartInstance = chartInstance;
  chartInstance->chartInfo.isEMLChart = 1;
  chartInstance->chartInfo.chartInitialized = 0;
  chartInstance->chartInfo.sFunctionGateway =
    sf_opaque_gateway_c47_LessonIII_start;
  chartInstance->chartInfo.initializeChart =
    sf_opaque_initialize_c47_LessonIII_start;
  chartInstance->chartInfo.terminateChart =
    sf_opaque_terminate_c47_LessonIII_start;
  chartInstance->chartInfo.enableChart = sf_opaque_enable_c47_LessonIII_start;
  chartInstance->chartInfo.disableChart = sf_opaque_disable_c47_LessonIII_start;
  chartInstance->chartInfo.getSimState =
    sf_opaque_get_sim_state_c47_LessonIII_start;
  chartInstance->chartInfo.setSimState =
    sf_opaque_set_sim_state_c47_LessonIII_start;
  chartInstance->chartInfo.getSimStateInfo =
    sf_get_sim_state_info_c47_LessonIII_start;
  chartInstance->chartInfo.zeroCrossings = NULL;
  chartInstance->chartInfo.outputs = NULL;
  chartInstance->chartInfo.derivatives = NULL;
  chartInstance->chartInfo.mdlRTW = mdlRTW_c47_LessonIII_start;
  chartInstance->chartInfo.mdlStart = mdlStart_c47_LessonIII_start;
  chartInstance->chartInfo.mdlSetWorkWidths =
    mdlSetWorkWidths_c47_LessonIII_start;
  chartInstance->chartInfo.extModeExec = NULL;
  chartInstance->chartInfo.restoreLastMajorStepConfiguration = NULL;
  chartInstance->chartInfo.restoreBeforeLastMajorStepConfiguration = NULL;
  chartInstance->chartInfo.storeCurrentConfiguration = NULL;
  chartInstance->chartInfo.callAtomicSubchartUserFcn = NULL;
  chartInstance->chartInfo.callAtomicSubchartAutoFcn = NULL;
  chartInstance->chartInfo.debugInstance = sfGlobalDebugInstanceStruct;
  chartInstance->S = S;
  crtInfo->checksum = SF_RUNTIME_INFO_CHECKSUM;
  crtInfo->instanceInfo = (&(chartInstance->chartInfo));
  crtInfo->isJITEnabled = false;
  crtInfo->compiledInfo = NULL;
  ssSetUserData(S,(void *)(crtInfo));  /* register the chart instance with simstruct */
  init_dsm_address_info(chartInstance);
  init_simulink_io_address(chartInstance);
  if (!sim_mode_is_rtw_gen(S)) {
  }

  sf_opaque_init_subchart_simstructs(chartInstance->chartInfo.chartInstance);
  chart_debug_initialization(S,1);
}

void c47_LessonIII_start_method_dispatcher(SimStruct *S, int_T method, void
  *data)
{
  switch (method) {
   case SS_CALL_MDL_START:
    mdlStart_c47_LessonIII_start(S);
    break;

   case SS_CALL_MDL_SET_WORK_WIDTHS:
    mdlSetWorkWidths_c47_LessonIII_start(S);
    break;

   case SS_CALL_MDL_PROCESS_PARAMETERS:
    mdlProcessParameters_c47_LessonIII_start(S);
    break;

   default:
    /* Unhandled method */
    sf_mex_error_message("Stateflow Internal Error:\n"
                         "Error calling c47_LessonIII_start_method_dispatcher.\n"
                         "Can't handle method %d.\n", method);
    break;
  }
}

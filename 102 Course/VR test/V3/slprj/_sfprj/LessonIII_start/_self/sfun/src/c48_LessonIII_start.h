#ifndef __c48_LessonIII_start_h__
#define __c48_LessonIII_start_h__

/* Include files */
#include "sf_runtime/sfc_sf.h"
#include "sf_runtime/sfc_mex.h"
#include "rtwtypes.h"
#include "multiword_types.h"

/* Type Definitions */
#ifndef typedef_SFc48_LessonIII_startInstanceStruct
#define typedef_SFc48_LessonIII_startInstanceStruct

typedef struct {
  SimStruct *S;
  ChartInfoStruct chartInfo;
  uint32_T chartNumber;
  uint32_T instanceNumber;
  int32_T c48_sfEvent;
  boolean_T c48_isStable;
  boolean_T c48_doneDoubleBufferReInit;
  uint8_T c48_is_active_c48_LessonIII_start;
  int8_T (*c48_targetPos)[2];
  int16_T *c48_targetOrientation;
  int8_T (*c48_myPos)[2];
} SFc48_LessonIII_startInstanceStruct;

#endif                                 /*typedef_SFc48_LessonIII_startInstanceStruct*/

/* Named Constants */

/* Variable Declarations */
extern struct SfDebugInstanceStruct *sfGlobalDebugInstanceStruct;

/* Variable Definitions */

/* Function Declarations */
extern const mxArray *sf_c48_LessonIII_start_get_eml_resolved_functions_info
  (void);

/* Function Definitions */
extern void sf_c48_LessonIII_start_get_check_sum(mxArray *plhs[]);
extern void c48_LessonIII_start_method_dispatcher(SimStruct *S, int_T method,
  void *data);

#endif

#include "__cf_LessonII.h"
#include <math.h>
#include "LessonII_acc.h"
#include "LessonII_acc_private.h"
#include <stdio.h>
#include "simstruc.h"
#include "fixedpoint.h"
#define CodeFormat S-Function
#define AccDefine1 Accelerator_S-Function
int32_T div_s32s64_floor ( int64_T numerator , int64_T denominator ) {
int32_T quotient ; uint64_T absNumerator ; uint64_T absDenominator ; uint64_T
tempAbsQuotient ; boolean_T quotientNeedsNegation ; if ( denominator == 0LL )
{ quotient = numerator >= 0LL ? MAX_int32_T : MIN_int32_T ; } else {
absNumerator = ( uint64_T ) ( numerator >= 0LL ? numerator : - numerator ) ;
absDenominator = ( uint64_T ) ( denominator >= 0LL ? denominator : -
denominator ) ; quotientNeedsNegation = ( ( numerator < 0LL ) != (
denominator < 0LL ) ) ; tempAbsQuotient = absNumerator / absDenominator ; if
( quotientNeedsNegation ) { absNumerator %= absDenominator ; if (
absNumerator > 0ULL ) { tempAbsQuotient ++ ; } } quotient =
quotientNeedsNegation ? ( int32_T ) - ( int64_T ) tempAbsQuotient : ( int32_T
) tempAbsQuotient ; } return quotient ; } static void mdlOutputs ( SimStruct
* S , int_T tid ) { uint8_T ktvl41htdx ; int32_T i ; int32_T idxMaxVal ;
int32_T maxVal ; uint8_T threshold ; boolean_T maxNumBlobsReached ; int32_T
idx ; int32_T n ; uint32_T stackIdx ; uint32_T pixIdx ; uint32_T walkerIdx ;
uint32_T numBlobs ; int32_T j ; real_T i1tzopfgbq ; int8_T csbwex0o23_idx_1 ;
int8_T csbwex0o23_idx_0 ; int64_T tmp ; real_T maxNumBlobsReached_p ;
f3ep0xylfb * _rtB ; diaowut2vy * _rtP ; oysdyzllkj * _rtDW ; _rtDW = ( (
oysdyzllkj * ) ssGetRootDWork ( S ) ) ; _rtP = ( ( diaowut2vy * )
ssGetDefaultParam ( S ) ) ; _rtB = ( ( f3ep0xylfb * ) _ssGetBlockIO ( S ) ) ;
_rtB -> kh514ecbye [ 0 ] = _rtP -> P_57 [ 0 ] ; _rtB -> kh514ecbye [ 1 ] =
_rtP -> P_57 [ 1 ] ; _rtB -> kh514ecbye [ 2 ] = _rtP -> P_57 [ 2 ] ;
ktvl41htdx = _rtP -> P_86 ; ssCallAccelRunBlock ( S , 0 , 0 ,
SS_CALL_MDL_OUTPUTS ) ; memcpy ( & _rtB -> jwbsnctla4 [ 0 ] , & _rtDW ->
fmeikhvspm [ 0 ] , 230400U * sizeof ( uint8_T ) ) ; ssCallAccelRunBlock ( S ,
43 , 0 , SS_CALL_MDL_OUTPUTS ) ; _rtDW -> hzzyqq3mgx = 1832519379 ; memset (
& _rtDW -> hm4n4rxevh [ 0 ] , 0 , sizeof ( int32_T ) << 8U ) ; for ( i = 0 ;
i < 76800 ; i ++ ) { _rtDW -> hm4n4rxevh [ _rtB -> ljm4djbuc4 [ i ] ] ++ ; }
for ( i = 0 ; i < 256 ; i ++ ) { _rtDW -> hu0hyclw5o [ i ] = ( int32_T ) ( (
int64_T ) _rtDW -> hzzyqq3mgx * _rtDW -> hm4n4rxevh [ i ] >> 17 ) ; } _rtDW
-> ogiz4wkto3 [ 0 ] = _rtDW -> hu0hyclw5o [ 0 ] >> 8 ; for ( i = 0 ; i < 255
; i ++ ) { _rtDW -> ogiz4wkto3 [ i + 1 ] = ( i + 2 ) << 22 ; _rtDW ->
ogiz4wkto3 [ i + 1 ] = ( int32_T ) ( ( int64_T ) _rtDW -> ogiz4wkto3 [ i + 1
] * _rtDW -> hu0hyclw5o [ i + 1 ] >> 30 ) ; _rtDW -> ogiz4wkto3 [ i + 1 ] +=
_rtDW -> ogiz4wkto3 [ i ] ; } for ( i = 0 ; i < 254 ; i ++ ) { _rtDW ->
hu0hyclw5o [ i + 1 ] += _rtDW -> hu0hyclw5o [ i ] ; } idxMaxVal = 0 ; maxVal
= 0 ; for ( i = 0 ; i < 255 ; i ++ ) { idx = ( int32_T ) ( ( int64_T ) _rtDW
-> hu0hyclw5o [ i ] * _rtDW -> ogiz4wkto3 [ 255 ] >> 30 ) - _rtDW ->
ogiz4wkto3 [ i ] ; n = ( int32_T ) ( ( int64_T ) ( 1073741824 - _rtDW ->
hu0hyclw5o [ i ] ) * _rtDW -> hu0hyclw5o [ i ] >> 30 ) ; if ( n == 0 ) { idx
= 0 ; } else { idx = div_s32s64_floor ( ( int64_T ) ( ( int32_T ) ( ( int64_T
) idx * idx >> 30 ) << 2 ) << 30 , n ) ; } if ( idx > maxVal ) { maxVal = idx
; idxMaxVal = i ; } } idx = ( int32_T ) ( 4210752 * idxMaxVal * 255ULL >> 15
) ; threshold = ( uint8_T ) ( ( ( idx & 16384 ) != 0 ) + ( idx >> 15 ) ) ;
for ( i = 0 ; i < 76800 ; i ++ ) { _rtB -> f0fr0kqurg [ i ] = ( _rtB ->
ljm4djbuc4 [ i ] > threshold ) ; } _rtDW -> hzzyqq3mgx = 1832519379 ; memset
( & _rtDW -> hm4n4rxevh [ 0 ] , 0 , sizeof ( int32_T ) << 8U ) ; for ( i = 0
; i < 76800 ; i ++ ) { _rtDW -> hm4n4rxevh [ _rtB -> m32l5sjsd3 [ i ] ] ++ ;
} for ( i = 0 ; i < 256 ; i ++ ) { _rtDW -> hu0hyclw5o [ i ] = ( int32_T ) (
( int64_T ) _rtDW -> hzzyqq3mgx * _rtDW -> hm4n4rxevh [ i ] >> 17 ) ; } _rtDW
-> ogiz4wkto3 [ 0 ] = _rtDW -> hu0hyclw5o [ 0 ] >> 8 ; for ( i = 0 ; i < 255
; i ++ ) { _rtDW -> ogiz4wkto3 [ i + 1 ] = ( i + 2 ) << 22 ; _rtDW ->
ogiz4wkto3 [ i + 1 ] = ( int32_T ) ( ( int64_T ) _rtDW -> ogiz4wkto3 [ i + 1
] * _rtDW -> hu0hyclw5o [ i + 1 ] >> 30 ) ; _rtDW -> ogiz4wkto3 [ i + 1 ] +=
_rtDW -> ogiz4wkto3 [ i ] ; } for ( i = 0 ; i < 254 ; i ++ ) { _rtDW ->
hu0hyclw5o [ i + 1 ] += _rtDW -> hu0hyclw5o [ i ] ; } idxMaxVal = 0 ; maxVal
= 0 ; for ( i = 0 ; i < 255 ; i ++ ) { idx = ( int32_T ) ( ( int64_T ) _rtDW
-> hu0hyclw5o [ i ] * _rtDW -> ogiz4wkto3 [ 255 ] >> 30 ) - _rtDW ->
ogiz4wkto3 [ i ] ; n = ( int32_T ) ( ( int64_T ) ( 1073741824 - _rtDW ->
hu0hyclw5o [ i ] ) * _rtDW -> hu0hyclw5o [ i ] >> 30 ) ; if ( n == 0 ) { idx
= 0 ; } else { idx = div_s32s64_floor ( ( int64_T ) ( ( int32_T ) ( ( int64_T
) idx * idx >> 30 ) << 2 ) << 30 , n ) ; } if ( idx > maxVal ) { maxVal = idx
; idxMaxVal = i ; } } idx = ( int32_T ) ( 4210752 * idxMaxVal * 255ULL >> 15
) ; threshold = ( uint8_T ) ( ( ( idx & 16384 ) != 0 ) + ( idx >> 15 ) ) ;
for ( i = 0 ; i < 76800 ; i ++ ) { _rtB -> ahxxaxfzui [ i ] = ( _rtB ->
m32l5sjsd3 [ i ] > threshold ) ; } _rtDW -> hzzyqq3mgx = 1832519379 ; memset
( & _rtDW -> hm4n4rxevh [ 0 ] , 0 , sizeof ( int32_T ) << 8U ) ; for ( i = 0
; i < 76800 ; i ++ ) { _rtDW -> hm4n4rxevh [ _rtB -> j33crokx52 [ i ] ] ++ ;
} for ( i = 0 ; i < 256 ; i ++ ) { _rtDW -> hu0hyclw5o [ i ] = ( int32_T ) (
( int64_T ) _rtDW -> hzzyqq3mgx * _rtDW -> hm4n4rxevh [ i ] >> 17 ) ; } _rtDW
-> ogiz4wkto3 [ 0 ] = _rtDW -> hu0hyclw5o [ 0 ] >> 8 ; for ( i = 0 ; i < 255
; i ++ ) { _rtDW -> ogiz4wkto3 [ i + 1 ] = ( i + 2 ) << 22 ; _rtDW ->
ogiz4wkto3 [ i + 1 ] = ( int32_T ) ( ( int64_T ) _rtDW -> ogiz4wkto3 [ i + 1
] * _rtDW -> hu0hyclw5o [ i + 1 ] >> 30 ) ; _rtDW -> ogiz4wkto3 [ i + 1 ] +=
_rtDW -> ogiz4wkto3 [ i ] ; } for ( i = 0 ; i < 254 ; i ++ ) { _rtDW ->
hu0hyclw5o [ i + 1 ] += _rtDW -> hu0hyclw5o [ i ] ; } idxMaxVal = 0 ; maxVal
= 0 ; for ( i = 0 ; i < 255 ; i ++ ) { idx = ( int32_T ) ( ( int64_T ) _rtDW
-> hu0hyclw5o [ i ] * _rtDW -> ogiz4wkto3 [ 255 ] >> 30 ) - _rtDW ->
ogiz4wkto3 [ i ] ; n = ( int32_T ) ( ( int64_T ) ( 1073741824 - _rtDW ->
hu0hyclw5o [ i ] ) * _rtDW -> hu0hyclw5o [ i ] >> 30 ) ; if ( n == 0 ) { idx
= 0 ; } else { idx = div_s32s64_floor ( ( int64_T ) ( ( int32_T ) ( ( int64_T
) idx * idx >> 30 ) << 2 ) << 30 , n ) ; } if ( idx > maxVal ) { maxVal = idx
; idxMaxVal = i ; } } idx = ( int32_T ) ( 4210752 * idxMaxVal * 255ULL >> 15
) ; threshold = ( uint8_T ) ( ( ( idx & 16384 ) != 0 ) + ( idx >> 15 ) ) ;
for ( i = 0 ; i < 76800 ; i ++ ) { _rtB -> hpn2cs0fjc [ i ] = ( _rtB ->
j33crokx52 [ i ] > threshold ) ; _rtB -> bfnb4bvikl [ i ] = ( uint8_T ) ( ( (
uint32_T ) _rtB -> f0fr0kqurg [ i ] + _rtB -> ahxxaxfzui [ i ] ) + _rtB ->
hpn2cs0fjc [ i ] ) ; _rtB -> pbb4t4fmph [ i ] = ( _rtB -> bfnb4bvikl [ i ] >
jpf4is5jzw ( S ) -> gugylkeuew ) ; } maxNumBlobsReached = false ; memset ( &
_rtDW -> mnjf24ynlg [ 0 ] , 0 , 243U * sizeof ( uint8_T ) ) ; threshold = 1U
; i = 0 ; idx = 243 ; for ( n = 0 ; n < 320 ; n ++ ) { for ( j = 0 ; j < 240
; j ++ ) { _rtDW -> mnjf24ynlg [ idx ] = ( uint8_T ) ( _rtB -> pbb4t4fmph [ i
] ? 255 : 0 ) ; i ++ ; idx ++ ; } _rtDW -> mnjf24ynlg [ idx ] = 0U ; _rtDW ->
mnjf24ynlg [ idx + 1 ] = 0U ; idx += 2 ; } memset ( & _rtDW -> mnjf24ynlg [
idx ] , 0 , 241U * sizeof ( uint8_T ) ) ; idxMaxVal = 0 ; pixIdx = 0U ; n = 0
; while ( n < 320 ) { idx = 0 ; maxVal = ( idxMaxVal + 1 ) * 242 ; j = 0 ;
while ( j < 240 ) { numBlobs = ( uint32_T ) ( ( maxVal + idx ) + 1 ) ; if (
_rtDW -> mnjf24ynlg [ numBlobs ] == 255 ) { _rtDW -> mnjf24ynlg [ numBlobs ]
= threshold ; _rtDW -> gddwfu4tlr [ pixIdx ] = ( int16_T ) idxMaxVal ; _rtDW
-> is4jmyk3p3 [ pixIdx ] = ( int16_T ) idx ; pixIdx ++ ; _rtDW -> cd1picjivu
[ threshold - 1 ] = 1U ; _rtDW -> arkq12bzd2 [ 0U ] = numBlobs ; stackIdx =
1U ; while ( stackIdx != 0U ) { stackIdx -- ; numBlobs = _rtDW -> arkq12bzd2
[ stackIdx ] ; for ( i = 0 ; i < 8 ; i ++ ) { walkerIdx = numBlobs +
axzuzwdj5b . dypu2qoqhd [ i ] ; if ( _rtDW -> mnjf24ynlg [ walkerIdx ] == 255
) { _rtDW -> mnjf24ynlg [ walkerIdx ] = threshold ; _rtDW -> gddwfu4tlr [
pixIdx ] = ( int16_T ) ( ( int16_T ) ( walkerIdx / 242U ) - 1 ) ; _rtDW ->
is4jmyk3p3 [ pixIdx ] = ( int16_T ) ( walkerIdx % 242U - 1U ) ; pixIdx ++ ;
_rtDW -> cd1picjivu [ threshold - 1 ] ++ ; _rtDW -> arkq12bzd2 [ stackIdx ] =
walkerIdx ; stackIdx ++ ; } } } if ( threshold == 16 ) { maxNumBlobsReached =
true ; n = 320 ; j = 240 ; } if ( j < 240 ) { threshold ++ ; } } idx ++ ; j
++ ; } idxMaxVal ++ ; n ++ ; } numBlobs = ( uint32_T ) ( maxNumBlobsReached ?
( int32_T ) threshold : ( int32_T ) ( uint8_T ) ( threshold - 1U ) ) ;
idxMaxVal = 0 ; idx = 0 ; for ( i = 0 ; i < ( int32_T ) numBlobs ; i ++ ) {
_rtB -> fb1lff2sdv [ i ] = ( int32_T ) _rtDW -> cd1picjivu [ i ] ; maxVal = 0
; n = 0 ; for ( j = 0 ; j < ( int32_T ) _rtDW -> cd1picjivu [ i ] ; j ++ ) {
maxVal += _rtDW -> gddwfu4tlr [ j + idx ] ; n += _rtDW -> is4jmyk3p3 [ j +
idxMaxVal ] ; } _rtB -> g3snywqqsd [ i ] = ( real32_T ) maxVal / ( real32_T )
_rtDW -> cd1picjivu [ i ] + 1.0F ; _rtB -> g3snywqqsd [ numBlobs + i ] = (
real32_T ) n / ( real32_T ) _rtDW -> cd1picjivu [ i ] + 1.0F ; idxMaxVal += (
int32_T ) _rtDW -> cd1picjivu [ i ] ; idx += ( int32_T ) _rtDW -> cd1picjivu
[ i ] ; } _rtDW -> lkmrwk3l1k [ 0 ] = ( int32_T ) numBlobs ; _rtDW ->
lkmrwk3l1k [ 1 ] = 1 ; _rtDW -> mojtovgd2n [ 0 ] = ( int32_T ) numBlobs ;
_rtDW -> mojtovgd2n [ 1 ] = 2 ; ssCallAccelRunBlock ( S , 41 , 0 ,
SS_CALL_MDL_OUTPUTS ) ; _rtB -> jfmah2lk3w [ 0 ] = _rtP -> P_5 [ 0 ] ; _rtB
-> jfmah2lk3w [ 1 ] = _rtP -> P_5 [ 1 ] ; _rtB -> lzr2dxafya = _rtP -> P_6 ;
ssCallAccelRunBlock ( S , 47 , 0 , SS_CALL_MDL_OUTPUTS ) ;
ssCallAccelRunBlock ( S , 45 , 0 , SS_CALL_MDL_OUTPUTS ) ;
ssCallAccelRunBlock ( S , 44 , 0 , SS_CALL_MDL_OUTPUTS ) ; _rtB -> hkayboxns4
= _rtP -> P_92 ; _rtB -> ezeyauz3sj = _rtP -> P_93 ; ssCallAccelRunBlock ( S
, 49 , 20 , SS_CALL_MDL_OUTPUTS ) ; _rtB -> d2n5bsxvci = ( uint8_T ) ( (
uint32_T ) _rtB -> dma5cbx1yd * _rtB -> llyfe3o5am ) ; ssCallAccelRunBlock (
S , 46 , 0 , SS_CALL_MDL_OUTPUTS ) ; ssCallAccelRunBlock ( S , 3 , 0 ,
SS_CALL_MDL_OUTPUTS ) ; ssCallAccelRunBlock ( S , 2 , 0 , SS_CALL_MDL_OUTPUTS
) ; _rtB -> ibg5wmkoqa = _rtB -> o41r3srig0 [ ktvl41htdx - 1 ] ;
ssCallAccelRunBlock ( S , 1 , 0 , SS_CALL_MDL_OUTPUTS ) ; _rtB -> pelmwxp33e
[ 0 ] = _rtB -> ohv02zkswk . x ; _rtB -> pelmwxp33e [ 1 ] = _rtB ->
ohv02zkswk . y ; _rtB -> kacoh1z4rv [ 0 ] = _rtB -> ibg5wmkoqa . x ; _rtB ->
kacoh1z4rv [ 1 ] = _rtB -> ibg5wmkoqa . y ; csbwex0o23_idx_0 = ( int8_T ) (
_rtB -> pelmwxp33e [ 0 ] - _rtB -> kacoh1z4rv [ 0 ] ) ; csbwex0o23_idx_1 = (
int8_T ) ( _rtB -> pelmwxp33e [ 1 ] - _rtB -> kacoh1z4rv [ 1 ] ) ; idx =
csbwex0o23_idx_0 * csbwex0o23_idx_0 + csbwex0o23_idx_1 * csbwex0o23_idx_1 ;
if ( ( idx < 0.0 ) && ( _rtP -> P_7 > muDoubleScalarFloor ( _rtP -> P_7 ) ) )
{ maxNumBlobsReached_p = - muDoubleScalarPower ( - ( real_T ) idx , _rtP ->
P_7 ) ; } else { maxNumBlobsReached_p = muDoubleScalarPower ( idx , _rtP ->
P_7 ) ; } i1tzopfgbq = _rtP -> P_8 * maxNumBlobsReached_p ; if ( i1tzopfgbq >
_rtP -> P_9 ) { _rtB -> hyg0vphneq = _rtP -> P_9 ; } else if ( i1tzopfgbq <
_rtP -> P_10 ) { _rtB -> hyg0vphneq = _rtP -> P_10 ; } else { _rtB ->
hyg0vphneq = i1tzopfgbq ; } ssCallAccelRunBlock ( S , 49 , 39 ,
SS_CALL_MDL_OUTPUTS ) ; maxNumBlobsReached = ( _rtB -> ohv02zkswk .
orientation != _rtP -> P_71 ) ; ssCallAccelRunBlock ( S , 6 , 0 ,
SS_CALL_MDL_OUTPUTS ) ; if ( maxNumBlobsReached ) { _rtB -> laabiylw5i = _rtB
-> ohv02zkswk . orientation ; } else { _rtB -> laabiylw5i = _rtB ->
mu0mwzwfai ; } ssCallAccelRunBlock ( S , 49 , 44 , SS_CALL_MDL_OUTPUTS ) ;
_rtB -> e1wwmdelp2 = ( int16_T ) ( _rtB -> mu0mwzwfai - _rtB -> ibg5wmkoqa .
orientation ) ; _rtB -> hszxpjmt5l = _rtP -> P_95 ; ssCallAccelRunBlock ( S ,
7 , 0 , SS_CALL_MDL_OUTPUTS ) ; if ( maxNumBlobsReached ) {
maxNumBlobsReached_p = _rtP -> P_0 ; } else { maxNumBlobsReached_p = _rtB ->
grkruxpoqc ; } i1tzopfgbq = _rtB -> hyg0vphneq * maxNumBlobsReached_p ;
ssCallAccelRunBlock ( S , 4 , 0 , SS_CALL_MDL_OUTPUTS ) ; idxMaxVal = _rtP ->
P_76 * _rtB -> bfngcbzr03 ; maxNumBlobsReached_p = ( real_T ) ( ( int64_T ) (
_rtP -> P_81 * _rtB -> c3dgzbflta ) * idxMaxVal ) * 1.4551915228366852E-11 +
i1tzopfgbq ; if ( maxNumBlobsReached_p > _rtP -> P_11 ) { _rtB -> j5dzrih1ud
[ 5 ] = _rtP -> P_11 ; } else if ( maxNumBlobsReached_p < _rtP -> P_12 ) {
_rtB -> j5dzrih1ud [ 5 ] = _rtP -> P_12 ; } else { _rtB -> j5dzrih1ud [ 5 ] =
maxNumBlobsReached_p ; } tmp = ( int64_T ) idxMaxVal * _rtB -> c3dgzbflta ;
maxNumBlobsReached_p = ( real_T ) ( ( tmp & 140737488355328LL ) != 0LL ? tmp
| - 140737488355328LL : tmp & 140737488355327LL ) * 4.76837158203125E-7 +
i1tzopfgbq ; if ( maxNumBlobsReached_p > _rtP -> P_13 ) { _rtB -> kgnz1ldmye
[ 5 ] = _rtP -> P_13 ; } else if ( maxNumBlobsReached_p < _rtP -> P_14 ) {
_rtB -> kgnz1ldmye [ 5 ] = _rtP -> P_14 ; } else { _rtB -> kgnz1ldmye [ 5 ] =
maxNumBlobsReached_p ; } _rtB -> omx4digdjr [ 0 ] = _rtP -> P_58 [ 0 ] ; _rtB
-> omx4digdjr [ 1 ] = _rtP -> P_58 [ 1 ] ; _rtB -> omx4digdjr [ 2 ] = _rtP ->
P_58 [ 2 ] ; ssCallAccelRunBlock ( S , 8 , 0 , SS_CALL_MDL_OUTPUTS ) ;
ssCallAccelRunBlock ( S , 11 , 0 , SS_CALL_MDL_OUTPUTS ) ;
ssCallAccelRunBlock ( S , 10 , 0 , SS_CALL_MDL_OUTPUTS ) ; _rtB -> kc3teyvigg
= _rtB -> ixqzozv4si [ _rtP -> P_96 - 1 ] ; ssCallAccelRunBlock ( S , 9 , 0 ,
SS_CALL_MDL_OUTPUTS ) ; _rtB -> kgzsvc10sv [ 0 ] = _rtB -> hvmnrwdold . x ;
_rtB -> kgzsvc10sv [ 1 ] = _rtB -> hvmnrwdold . y ; _rtB -> osmfr34c2i [ 0 ]
= _rtB -> kc3teyvigg . x ; _rtB -> osmfr34c2i [ 1 ] = _rtB -> kc3teyvigg . y
; csbwex0o23_idx_0 = ( int8_T ) ( _rtB -> kgzsvc10sv [ 0 ] - _rtB ->
osmfr34c2i [ 0 ] ) ; csbwex0o23_idx_1 = ( int8_T ) ( _rtB -> kgzsvc10sv [ 1 ]
- _rtB -> osmfr34c2i [ 1 ] ) ; idx = csbwex0o23_idx_0 * csbwex0o23_idx_0 +
csbwex0o23_idx_1 * csbwex0o23_idx_1 ; if ( ( idx < 0.0 ) && ( _rtP -> P_15 >
muDoubleScalarFloor ( _rtP -> P_15 ) ) ) { maxNumBlobsReached_p = -
muDoubleScalarPower ( - ( real_T ) idx , _rtP -> P_15 ) ; } else {
maxNumBlobsReached_p = muDoubleScalarPower ( idx , _rtP -> P_15 ) ; }
i1tzopfgbq = _rtP -> P_16 * maxNumBlobsReached_p ; if ( i1tzopfgbq > _rtP ->
P_17 ) { _rtB -> hyg0vphneq = _rtP -> P_17 ; } else if ( i1tzopfgbq < _rtP ->
P_18 ) { _rtB -> hyg0vphneq = _rtP -> P_18 ; } else { _rtB -> hyg0vphneq =
i1tzopfgbq ; } ssCallAccelRunBlock ( S , 49 , 79 , SS_CALL_MDL_OUTPUTS ) ;
maxNumBlobsReached = ( _rtB -> hvmnrwdold . orientation != _rtP -> P_72 ) ;
ssCallAccelRunBlock ( S , 14 , 0 , SS_CALL_MDL_OUTPUTS ) ; if (
maxNumBlobsReached ) { _rtB -> ed0xfhwloe = _rtB -> hvmnrwdold . orientation
; } else { _rtB -> ed0xfhwloe = _rtB -> l2a4umeg2l ; } ssCallAccelRunBlock (
S , 49 , 84 , SS_CALL_MDL_OUTPUTS ) ; _rtB -> hthvvvsznx = ( int16_T ) ( _rtB
-> l2a4umeg2l - _rtB -> kc3teyvigg . orientation ) ; _rtB -> pv2uiz55u5 =
_rtP -> P_97 ; ssCallAccelRunBlock ( S , 15 , 0 , SS_CALL_MDL_OUTPUTS ) ; if
( maxNumBlobsReached ) { maxNumBlobsReached_p = _rtP -> P_1 ; } else {
maxNumBlobsReached_p = _rtB -> gxmra5ku2e ; } i1tzopfgbq = _rtB -> hyg0vphneq
* maxNumBlobsReached_p ; ssCallAccelRunBlock ( S , 12 , 0 ,
SS_CALL_MDL_OUTPUTS ) ; idxMaxVal = _rtP -> P_77 * _rtB -> nomuvhangv ;
maxNumBlobsReached_p = ( real_T ) ( ( int64_T ) ( _rtP -> P_82 * _rtB ->
i2j2u2ea0j ) * idxMaxVal ) * 1.4551915228366852E-11 + i1tzopfgbq ; if (
maxNumBlobsReached_p > _rtP -> P_19 ) { _rtB -> j5dzrih1ud [ 3 ] = _rtP ->
P_19 ; } else if ( maxNumBlobsReached_p < _rtP -> P_20 ) { _rtB -> j5dzrih1ud
[ 3 ] = _rtP -> P_20 ; } else { _rtB -> j5dzrih1ud [ 3 ] =
maxNumBlobsReached_p ; } tmp = ( int64_T ) idxMaxVal * _rtB -> i2j2u2ea0j ;
maxNumBlobsReached_p = ( real_T ) ( ( tmp & 140737488355328LL ) != 0LL ? tmp
| - 140737488355328LL : tmp & 140737488355327LL ) * 4.76837158203125E-7 +
i1tzopfgbq ; if ( maxNumBlobsReached_p > _rtP -> P_21 ) { _rtB -> kgnz1ldmye
[ 3 ] = _rtP -> P_21 ; } else if ( maxNumBlobsReached_p < _rtP -> P_22 ) {
_rtB -> kgnz1ldmye [ 3 ] = _rtP -> P_22 ; } else { _rtB -> kgnz1ldmye [ 3 ] =
maxNumBlobsReached_p ; } _rtB -> kgnz1ldmye [ 4 ] = _rtP -> P_23 ; _rtB ->
j5dzrih1ud [ 4 ] = _rtP -> P_24 ; _rtB -> kgnz1ldmye [ 1 ] = _rtP -> P_25 ;
_rtB -> j5dzrih1ud [ 1 ] = _rtP -> P_26 ; _rtB -> g5qlkichlv [ 0 ] = _rtP ->
P_59 [ 0 ] ; _rtB -> g5qlkichlv [ 1 ] = _rtP -> P_59 [ 1 ] ; _rtB ->
g5qlkichlv [ 2 ] = _rtP -> P_59 [ 2 ] ; ssCallAccelRunBlock ( S , 16 , 0 ,
SS_CALL_MDL_OUTPUTS ) ; ssCallAccelRunBlock ( S , 19 , 0 ,
SS_CALL_MDL_OUTPUTS ) ; ssCallAccelRunBlock ( S , 18 , 0 ,
SS_CALL_MDL_OUTPUTS ) ; _rtB -> jaepky1crd = _rtB -> bni0i3ip43 [ _rtP ->
P_98 - 1 ] ; ssCallAccelRunBlock ( S , 17 , 0 , SS_CALL_MDL_OUTPUTS ) ; _rtB
-> lukjbjjip3 [ 0 ] = _rtB -> o4n0fbfpvr . x ; _rtB -> lukjbjjip3 [ 1 ] =
_rtB -> o4n0fbfpvr . y ; _rtB -> kzma3vtafj [ 0 ] = _rtB -> jaepky1crd . x ;
_rtB -> kzma3vtafj [ 1 ] = _rtB -> jaepky1crd . y ; csbwex0o23_idx_0 = (
int8_T ) ( _rtB -> lukjbjjip3 [ 0 ] - _rtB -> kzma3vtafj [ 0 ] ) ;
csbwex0o23_idx_1 = ( int8_T ) ( _rtB -> lukjbjjip3 [ 1 ] - _rtB -> kzma3vtafj
[ 1 ] ) ; idx = csbwex0o23_idx_0 * csbwex0o23_idx_0 + csbwex0o23_idx_1 *
csbwex0o23_idx_1 ; if ( ( idx < 0.0 ) && ( _rtP -> P_27 > muDoubleScalarFloor
( _rtP -> P_27 ) ) ) { maxNumBlobsReached_p = - muDoubleScalarPower ( - (
real_T ) idx , _rtP -> P_27 ) ; } else { maxNumBlobsReached_p =
muDoubleScalarPower ( idx , _rtP -> P_27 ) ; } i1tzopfgbq = _rtP -> P_28 *
maxNumBlobsReached_p ; maxNumBlobsReached = ( _rtB -> o4n0fbfpvr .
orientation != _rtP -> P_73 ) ; ssCallAccelRunBlock ( S , 22 , 0 ,
SS_CALL_MDL_OUTPUTS ) ; _rtB -> hk2q23kn3d = ( int16_T ) ( _rtB -> ffewisavg2
- _rtB -> jaepky1crd . orientation ) ; _rtB -> kgevnince5 = _rtP -> P_99 ;
ssCallAccelRunBlock ( S , 23 , 0 , SS_CALL_MDL_OUTPUTS ) ; if ( i1tzopfgbq >
_rtP -> P_29 ) { i1tzopfgbq = _rtP -> P_29 ; } else { if ( i1tzopfgbq < _rtP
-> P_30 ) { i1tzopfgbq = _rtP -> P_30 ; } } if ( maxNumBlobsReached ) {
maxNumBlobsReached_p = _rtP -> P_2 ; } else { maxNumBlobsReached_p = _rtB ->
askjqbwul2 ; } i1tzopfgbq *= maxNumBlobsReached_p ; if ( maxNumBlobsReached )
{ _rtB -> kv3uxjzw5r = _rtB -> o4n0fbfpvr . orientation ; } else { _rtB ->
kv3uxjzw5r = _rtB -> ffewisavg2 ; } ssCallAccelRunBlock ( S , 20 , 0 ,
SS_CALL_MDL_OUTPUTS ) ; idxMaxVal = _rtP -> P_78 * _rtB -> dxjnfukhua ;
maxNumBlobsReached_p = ( real_T ) ( ( int64_T ) ( _rtP -> P_83 * _rtB ->
iuzzt5ohk2 ) * idxMaxVal ) * 1.4551915228366852E-11 + i1tzopfgbq ; if (
maxNumBlobsReached_p > _rtP -> P_31 ) { _rtB -> j5dzrih1ud [ 2 ] = _rtP ->
P_31 ; } else if ( maxNumBlobsReached_p < _rtP -> P_32 ) { _rtB -> j5dzrih1ud
[ 2 ] = _rtP -> P_32 ; } else { _rtB -> j5dzrih1ud [ 2 ] =
maxNumBlobsReached_p ; } tmp = ( int64_T ) idxMaxVal * _rtB -> iuzzt5ohk2 ;
maxNumBlobsReached_p = ( real_T ) ( ( tmp & 140737488355328LL ) != 0LL ? tmp
| - 140737488355328LL : tmp & 140737488355327LL ) * 4.76837158203125E-7 +
i1tzopfgbq ; if ( maxNumBlobsReached_p > _rtP -> P_33 ) { _rtB -> kgnz1ldmye
[ 2 ] = _rtP -> P_33 ; } else if ( maxNumBlobsReached_p < _rtP -> P_34 ) {
_rtB -> kgnz1ldmye [ 2 ] = _rtP -> P_34 ; } else { _rtB -> kgnz1ldmye [ 2 ] =
maxNumBlobsReached_p ; } _rtB -> edfvufvrrx [ 0 ] = _rtP -> P_60 [ 0 ] ; _rtB
-> edfvufvrrx [ 1 ] = _rtP -> P_60 [ 1 ] ; _rtB -> edfvufvrrx [ 2 ] = _rtP ->
P_60 [ 2 ] ; ssCallAccelRunBlock ( S , 24 , 0 , SS_CALL_MDL_OUTPUTS ) ;
ssCallAccelRunBlock ( S , 27 , 0 , SS_CALL_MDL_OUTPUTS ) ;
ssCallAccelRunBlock ( S , 26 , 0 , SS_CALL_MDL_OUTPUTS ) ; _rtB -> frf33h2oo3
= _rtB -> it2ho1drrd [ _rtP -> P_100 - 1 ] ; ssCallAccelRunBlock ( S , 25 , 0
, SS_CALL_MDL_OUTPUTS ) ; _rtB -> f4hc1212gs [ 0 ] = _rtB -> m3eijgp1ov . x ;
_rtB -> f4hc1212gs [ 1 ] = _rtB -> m3eijgp1ov . y ; _rtB -> k5rjzba5s2 [ 0 ]
= _rtB -> frf33h2oo3 . x ; _rtB -> k5rjzba5s2 [ 1 ] = _rtB -> frf33h2oo3 . y
; csbwex0o23_idx_0 = ( int8_T ) ( _rtB -> f4hc1212gs [ 0 ] - _rtB ->
k5rjzba5s2 [ 0 ] ) ; csbwex0o23_idx_1 = ( int8_T ) ( _rtB -> f4hc1212gs [ 1 ]
- _rtB -> k5rjzba5s2 [ 1 ] ) ; idx = csbwex0o23_idx_0 * csbwex0o23_idx_0 +
csbwex0o23_idx_1 * csbwex0o23_idx_1 ; if ( ( idx < 0.0 ) && ( _rtP -> P_35 >
muDoubleScalarFloor ( _rtP -> P_35 ) ) ) { maxNumBlobsReached_p = -
muDoubleScalarPower ( - ( real_T ) idx , _rtP -> P_35 ) ; } else {
maxNumBlobsReached_p = muDoubleScalarPower ( idx , _rtP -> P_35 ) ; }
i1tzopfgbq = _rtP -> P_36 * maxNumBlobsReached_p ; if ( i1tzopfgbq > _rtP ->
P_37 ) { _rtB -> hyg0vphneq = _rtP -> P_37 ; } else if ( i1tzopfgbq < _rtP ->
P_38 ) { _rtB -> hyg0vphneq = _rtP -> P_38 ; } else { _rtB -> hyg0vphneq =
i1tzopfgbq ; } ssCallAccelRunBlock ( S , 49 , 161 , SS_CALL_MDL_OUTPUTS ) ;
maxNumBlobsReached = ( _rtB -> m3eijgp1ov . orientation != _rtP -> P_74 ) ;
ssCallAccelRunBlock ( S , 30 , 0 , SS_CALL_MDL_OUTPUTS ) ; if (
maxNumBlobsReached ) { _rtB -> f3ri0wya2h = _rtB -> m3eijgp1ov . orientation
; } else { _rtB -> f3ri0wya2h = _rtB -> bulsyk12sw ; } ssCallAccelRunBlock (
S , 49 , 166 , SS_CALL_MDL_OUTPUTS ) ; _rtB -> aria1gwzsn = ( int16_T ) (
_rtB -> bulsyk12sw - _rtB -> frf33h2oo3 . orientation ) ; _rtB -> kaxsw44guk
= _rtP -> P_101 ; ssCallAccelRunBlock ( S , 31 , 0 , SS_CALL_MDL_OUTPUTS ) ;
if ( maxNumBlobsReached ) { maxNumBlobsReached_p = _rtP -> P_3 ; } else {
maxNumBlobsReached_p = _rtB -> b3csq5ksrg ; } i1tzopfgbq = _rtB -> hyg0vphneq
* maxNumBlobsReached_p ; ssCallAccelRunBlock ( S , 28 , 0 ,
SS_CALL_MDL_OUTPUTS ) ; idxMaxVal = _rtP -> P_79 * _rtB -> anba0cqdma ;
maxNumBlobsReached_p = ( real_T ) ( ( int64_T ) ( _rtP -> P_84 * _rtB ->
ewprsyuo3p ) * idxMaxVal ) * 1.4551915228366852E-11 + i1tzopfgbq ; if (
maxNumBlobsReached_p > _rtP -> P_39 ) { _rtB -> j5dzrih1ud [ 0 ] = _rtP ->
P_39 ; } else if ( maxNumBlobsReached_p < _rtP -> P_40 ) { _rtB -> j5dzrih1ud
[ 0 ] = _rtP -> P_40 ; } else { _rtB -> j5dzrih1ud [ 0 ] =
maxNumBlobsReached_p ; } tmp = ( int64_T ) idxMaxVal * _rtB -> ewprsyuo3p ;
maxNumBlobsReached_p = ( real_T ) ( ( tmp & 140737488355328LL ) != 0LL ? tmp
| - 140737488355328LL : tmp & 140737488355327LL ) * 4.76837158203125E-7 +
i1tzopfgbq ; if ( maxNumBlobsReached_p > _rtP -> P_41 ) { _rtB -> kgnz1ldmye
[ 0 ] = _rtP -> P_41 ; } else if ( maxNumBlobsReached_p < _rtP -> P_42 ) {
_rtB -> kgnz1ldmye [ 0 ] = _rtP -> P_42 ; } else { _rtB -> kgnz1ldmye [ 0 ] =
maxNumBlobsReached_p ; } for ( i = 0 ; i < 6 ; i ++ ) { _rtB -> imbzpzg52y [
i ] = _rtP -> P_61 [ i ] ; } _rtB -> njwi5hrqjx [ 0 ] = _rtP -> P_62 [ 0 ] ;
_rtB -> njwi5hrqjx [ 1 ] = _rtP -> P_62 [ 1 ] ; _rtB -> njwi5hrqjx [ 2 ] =
_rtP -> P_62 [ 2 ] ; ssCallAccelRunBlock ( S , 32 , 0 , SS_CALL_MDL_OUTPUTS )
; ssCallAccelRunBlock ( S , 34 , 0 , SS_CALL_MDL_OUTPUTS ) ;
ssCallAccelRunBlock ( S , 33 , 0 , SS_CALL_MDL_OUTPUTS ) ; _rtB -> jnus0nczag
= _rtB -> eqtawkgatx [ _rtP -> P_102 - 1 ] ; _rtB -> lbgezj4mg0 [ 0 ] = _rtB
-> l00rfvjnwz . x ; _rtB -> lbgezj4mg0 [ 1 ] = _rtB -> l00rfvjnwz . y ; _rtB
-> izhhi3yrk4 [ 0 ] = _rtB -> jnus0nczag . x ; _rtB -> izhhi3yrk4 [ 1 ] =
_rtB -> jnus0nczag . y ; csbwex0o23_idx_0 = ( int8_T ) ( _rtB -> lbgezj4mg0 [
0 ] - _rtB -> izhhi3yrk4 [ 0 ] ) ; csbwex0o23_idx_1 = ( int8_T ) ( _rtB ->
lbgezj4mg0 [ 1 ] - _rtB -> izhhi3yrk4 [ 1 ] ) ; idx = csbwex0o23_idx_0 *
csbwex0o23_idx_0 + csbwex0o23_idx_1 * csbwex0o23_idx_1 ; if ( ( idx < 0.0 )
&& ( _rtP -> P_43 > muDoubleScalarFloor ( _rtP -> P_43 ) ) ) {
maxNumBlobsReached_p = - muDoubleScalarPower ( - ( real_T ) idx , _rtP ->
P_43 ) ; } else { maxNumBlobsReached_p = muDoubleScalarPower ( idx , _rtP ->
P_43 ) ; } i1tzopfgbq = _rtP -> P_44 * maxNumBlobsReached_p ; if ( i1tzopfgbq
> _rtP -> P_45 ) { _rtB -> hyg0vphneq = _rtP -> P_45 ; } else if ( i1tzopfgbq
< _rtP -> P_46 ) { _rtB -> hyg0vphneq = _rtP -> P_46 ; } else { _rtB ->
hyg0vphneq = i1tzopfgbq ; } ssCallAccelRunBlock ( S , 49 , 203 ,
SS_CALL_MDL_OUTPUTS ) ; maxNumBlobsReached = ( _rtB -> l00rfvjnwz .
orientation != _rtP -> P_75 ) ; ssCallAccelRunBlock ( S , 37 , 0 ,
SS_CALL_MDL_OUTPUTS ) ; if ( maxNumBlobsReached ) { _rtB -> m4h3bsuszy = _rtB
-> l00rfvjnwz . orientation ; } else { _rtB -> m4h3bsuszy = _rtB ->
ibyts1da5c ; } ssCallAccelRunBlock ( S , 49 , 208 , SS_CALL_MDL_OUTPUTS ) ;
_rtB -> capjelp0cz = ( int16_T ) ( _rtB -> ibyts1da5c - _rtB -> jnus0nczag .
orientation ) ; _rtB -> fokhixpacr = _rtP -> P_103 ; ssCallAccelRunBlock ( S
, 38 , 0 , SS_CALL_MDL_OUTPUTS ) ; if ( maxNumBlobsReached ) {
maxNumBlobsReached_p = _rtP -> P_4 ; } else { maxNumBlobsReached_p = _rtB ->
c0zblc31z1 ; } i1tzopfgbq = _rtB -> hyg0vphneq * maxNumBlobsReached_p ;
ssCallAccelRunBlock ( S , 35 , 0 , SS_CALL_MDL_OUTPUTS ) ; idxMaxVal = _rtP
-> P_80 * _rtB -> jgubdh3j14 ; maxNumBlobsReached_p = ( real_T ) ( ( int64_T
) ( _rtP -> P_85 * _rtB -> lsjyf3qu3e ) * idxMaxVal ) *
1.4551915228366852E-11 + i1tzopfgbq ; if ( maxNumBlobsReached_p > _rtP ->
P_47 ) { _rtB -> ec3bs5npq0 = _rtP -> P_47 ; } else if ( maxNumBlobsReached_p
< _rtP -> P_48 ) { _rtB -> ec3bs5npq0 = _rtP -> P_48 ; } else { _rtB ->
ec3bs5npq0 = maxNumBlobsReached_p ; } tmp = ( int64_T ) idxMaxVal * _rtB ->
lsjyf3qu3e ; maxNumBlobsReached_p = ( real_T ) ( ( tmp & 140737488355328LL )
!= 0LL ? tmp | - 140737488355328LL : tmp & 140737488355327LL ) *
4.76837158203125E-7 + i1tzopfgbq ; if ( maxNumBlobsReached_p > _rtP -> P_49 )
{ _rtB -> c0gyrnqbgw = _rtP -> P_49 ; } else if ( maxNumBlobsReached_p < _rtP
-> P_50 ) { _rtB -> c0gyrnqbgw = _rtP -> P_50 ; } else { _rtB -> c0gyrnqbgw =
maxNumBlobsReached_p ; } _rtB -> mdkwizee2r = _rtP -> P_51 ; _rtB ->
mtlmhgvt4k = _rtP -> P_52 ; memcpy ( & _rtB -> aavl5ejkfx [ 0 ] , & _rtP ->
P_63 [ 0 ] , 18U * sizeof ( real32_T ) ) ; _rtB -> hfoblv4ro0 = _rtP -> P_53
; _rtB -> om4hmm0icv = _rtP -> P_104 ; ssCallAccelRunBlock ( S , 40 , 0 ,
SS_CALL_MDL_OUTPUTS ) ; ssCallAccelRunBlock ( S , 39 , 0 ,
SS_CALL_MDL_OUTPUTS ) ; ssCallAccelRunBlock ( S , 49 , 231 ,
SS_CALL_MDL_OUTPUTS ) ; ssCallAccelRunBlock ( S , 42 , 0 ,
SS_CALL_MDL_OUTPUTS ) ; ssCallAccelRunBlock ( S , 49 , 233 ,
SS_CALL_MDL_OUTPUTS ) ; _rtB -> b3lhchb1bp [ 0 ] = _rtP -> P_64 [ 0 ] * _rtB
-> fwy2buoicd [ 5 ] ; _rtB -> b3lhchb1bp [ 1 ] = _rtP -> P_64 [ 1 ] * _rtB ->
fwy2buoicd [ 11 ] ; _rtB -> b3lhchb1bp [ 2 ] = _rtP -> P_64 [ 2 ] * _rtB ->
fwy2buoicd [ 17 ] ; _rtB -> plikmyrwvw [ 0 ] = _rtP -> P_65 [ 0 ] * _rtB ->
fwy2buoicd [ 4 ] ; _rtB -> plikmyrwvw [ 1 ] = _rtP -> P_65 [ 1 ] * _rtB ->
fwy2buoicd [ 10 ] ; _rtB -> plikmyrwvw [ 2 ] = _rtP -> P_65 [ 2 ] * _rtB ->
fwy2buoicd [ 16 ] ; _rtB -> enudydd52w [ 0 ] = _rtP -> P_66 [ 0 ] * _rtB ->
fwy2buoicd [ 3 ] ; _rtB -> enudydd52w [ 1 ] = _rtP -> P_66 [ 1 ] * _rtB ->
fwy2buoicd [ 9 ] ; _rtB -> enudydd52w [ 2 ] = _rtP -> P_66 [ 2 ] * _rtB ->
fwy2buoicd [ 15 ] ; _rtB -> bh1tldl3jb [ 0 ] = _rtP -> P_67 [ 0 ] * _rtB ->
fwy2buoicd [ 2 ] ; _rtB -> bh1tldl3jb [ 1 ] = _rtP -> P_67 [ 1 ] * _rtB ->
fwy2buoicd [ 8 ] ; _rtB -> bh1tldl3jb [ 2 ] = _rtP -> P_67 [ 2 ] * _rtB ->
fwy2buoicd [ 14 ] ; _rtB -> i1mvvv5xlb [ 0 ] = _rtP -> P_68 [ 0 ] * _rtB ->
fwy2buoicd [ 0 ] ; _rtB -> i1mvvv5xlb [ 1 ] = _rtP -> P_68 [ 1 ] * _rtB ->
fwy2buoicd [ 6 ] ; _rtB -> i1mvvv5xlb [ 2 ] = _rtP -> P_68 [ 2 ] * _rtB ->
fwy2buoicd [ 12 ] ; _rtB -> pkml4zlbz4 [ 0 ] = _rtP -> P_69 [ 0 ] * _rtB ->
fwy2buoicd [ 1 ] ; _rtB -> pkml4zlbz4 [ 1 ] = _rtP -> P_69 [ 1 ] * _rtB ->
fwy2buoicd [ 7 ] ; _rtB -> pkml4zlbz4 [ 2 ] = _rtP -> P_69 [ 2 ] * _rtB ->
fwy2buoicd [ 13 ] ; _rtB -> akoreyjgfo = _rtP -> P_54 ; _rtB -> oq04zq4div =
_rtP -> P_55 ; _rtB -> fxurcz2zn2 = _rtP -> P_56 ; ssCallAccelRunBlock ( S ,
48 , 0 , SS_CALL_MDL_OUTPUTS ) ; _rtB -> ogve5ic4fw [ 0 ] = _rtP -> P_70 [ 0
] * _rtB -> h3azju3rbu [ 0 ] ; _rtB -> ogve5ic4fw [ 1 ] = _rtP -> P_70 [ 1 ]
* _rtB -> h3azju3rbu [ 1 ] ; _rtB -> ogve5ic4fw [ 2 ] = _rtP -> P_70 [ 2 ] *
_rtB -> h3azju3rbu [ 2 ] ; _rtB -> pwoi4hytbl [ 0 ] = _rtB -> hxyzsiww0g [ 3
] ; _rtB -> pwoi4hytbl [ 1 ] = _rtB -> hxyzsiww0g [ 9 ] ; _rtB -> pwoi4hytbl
[ 2 ] = _rtB -> hxyzsiww0g [ 15 ] ; _rtB -> pwoi4hytbl [ 3 ] = _rtB ->
hxyzsiww0g [ 21 ] ; _rtB -> k5tfvybxjm [ 0 ] = _rtB -> hxyzsiww0g [ 4 ] ;
_rtB -> k5tfvybxjm [ 1 ] = _rtB -> hxyzsiww0g [ 10 ] ; _rtB -> k5tfvybxjm [ 2
] = _rtB -> hxyzsiww0g [ 16 ] ; _rtB -> k5tfvybxjm [ 3 ] = _rtB -> hxyzsiww0g
[ 22 ] ; _rtB -> nepgel10e5 [ 0 ] = _rtB -> hxyzsiww0g [ 5 ] ; _rtB ->
nepgel10e5 [ 1 ] = _rtB -> hxyzsiww0g [ 11 ] ; _rtB -> nepgel10e5 [ 2 ] =
_rtB -> hxyzsiww0g [ 17 ] ; _rtB -> nepgel10e5 [ 3 ] = _rtB -> hxyzsiww0g [
23 ] ; _rtB -> bty031phsv [ 0 ] = _rtB -> hxyzsiww0g [ 0 ] ; _rtB ->
bty031phsv [ 1 ] = _rtB -> hxyzsiww0g [ 6 ] ; _rtB -> bty031phsv [ 2 ] = _rtB
-> hxyzsiww0g [ 12 ] ; _rtB -> bty031phsv [ 3 ] = _rtB -> hxyzsiww0g [ 18 ] ;
_rtB -> egm0n35pvq [ 0 ] = _rtB -> hxyzsiww0g [ 1 ] ; _rtB -> egm0n35pvq [ 1
] = _rtB -> hxyzsiww0g [ 7 ] ; _rtB -> egm0n35pvq [ 2 ] = _rtB -> hxyzsiww0g
[ 13 ] ; _rtB -> egm0n35pvq [ 3 ] = _rtB -> hxyzsiww0g [ 19 ] ; _rtB ->
frctfibrqz [ 0 ] = _rtB -> hxyzsiww0g [ 2 ] ; _rtB -> frctfibrqz [ 1 ] = _rtB
-> hxyzsiww0g [ 8 ] ; _rtB -> frctfibrqz [ 2 ] = _rtB -> hxyzsiww0g [ 14 ] ;
_rtB -> frctfibrqz [ 3 ] = _rtB -> hxyzsiww0g [ 20 ] ; ssCallAccelRunBlock (
S , 49 , 257 , SS_CALL_MDL_OUTPUTS ) ; UNUSED_PARAMETER ( tid ) ; }
#define MDL_UPDATE
static void mdlUpdate ( SimStruct * S , int_T tid ) { f3ep0xylfb * _rtB ;
oysdyzllkj * _rtDW ; _rtDW = ( ( oysdyzllkj * ) ssGetRootDWork ( S ) ) ; _rtB
= ( ( f3ep0xylfb * ) _ssGetBlockIO ( S ) ) ; memcpy ( & _rtDW -> fmeikhvspm [
0 ] , & _rtB -> biowo4bths [ 0 ] , 230400U * sizeof ( uint8_T ) ) ;
UNUSED_PARAMETER ( tid ) ; } static void mdlInitializeSizes ( SimStruct * S )
{ ssSetChecksumVal ( S , 0 , 1622885693U ) ; ssSetChecksumVal ( S , 1 ,
1762629004U ) ; ssSetChecksumVal ( S , 2 , 3620492619U ) ; ssSetChecksumVal (
S , 3 , 596880067U ) ; { mxArray * slVerStructMat = NULL ; mxArray * slStrMat
= mxCreateString ( "simulink" ) ; char slVerChar [ 10 ] ; int status =
mexCallMATLAB ( 1 , & slVerStructMat , 1 , & slStrMat , "ver" ) ; if ( status
== 0 ) { mxArray * slVerMat = mxGetField ( slVerStructMat , 0 , "Version" ) ;
if ( slVerMat == NULL ) { status = 1 ; } else { status = mxGetString (
slVerMat , slVerChar , 10 ) ; } } mxDestroyArray ( slStrMat ) ;
mxDestroyArray ( slVerStructMat ) ; if ( ( status == 1 ) || ( strcmp (
slVerChar , "8.4" ) != 0 ) ) { return ; } } ssSetOptions ( S ,
SS_OPTION_EXCEPTION_FREE_CODE ) ; if ( ssGetSizeofDWork ( S ) != sizeof (
oysdyzllkj ) ) { ssSetErrorStatus ( S ,
"Unexpected error: Internal DWork sizes do "
"not match for accelerator mex file." ) ; } if ( ssGetSizeofGlobalBlockIO ( S
) != sizeof ( f3ep0xylfb ) ) { ssSetErrorStatus ( S ,
"Unexpected error: Internal BlockIO sizes do "
"not match for accelerator mex file." ) ; } { int ssSizeofParams ;
ssGetSizeofParams ( S , & ssSizeofParams ) ; if ( ssSizeofParams != sizeof (
diaowut2vy ) ) { static char msg [ 256 ] ; sprintf ( msg ,
"Unexpected error: Internal Parameters sizes do "
"not match for accelerator mex file." ) ; } } _ssSetDefaultParam ( S , (
real_T * ) & kyxyccx1mn ) ; _ssSetConstBlockIO ( S , & f0eruxjaz3 ) ;
rt_InitInfAndNaN ( sizeof ( real_T ) ) ; } static void
mdlInitializeSampleTimes ( SimStruct * S ) { { SimStruct * childS ;
SysOutputFcn * callSysFcns ; childS = ssGetSFunction ( S , 0 ) ; callSysFcns
= ssGetCallSystemOutputFcnList ( childS ) ; callSysFcns [ 3 + 0 ] = (
SysOutputFcn ) ( NULL ) ; childS = ssGetSFunction ( S , 1 ) ; callSysFcns =
ssGetCallSystemOutputFcnList ( childS ) ; callSysFcns [ 3 + 0 ] = (
SysOutputFcn ) ( NULL ) ; childS = ssGetSFunction ( S , 2 ) ; callSysFcns =
ssGetCallSystemOutputFcnList ( childS ) ; callSysFcns [ 3 + 0 ] = (
SysOutputFcn ) ( NULL ) ; childS = ssGetSFunction ( S , 3 ) ; callSysFcns =
ssGetCallSystemOutputFcnList ( childS ) ; callSysFcns [ 3 + 0 ] = (
SysOutputFcn ) ( NULL ) ; childS = ssGetSFunction ( S , 4 ) ; callSysFcns =
ssGetCallSystemOutputFcnList ( childS ) ; callSysFcns [ 3 + 0 ] = (
SysOutputFcn ) ( NULL ) ; childS = ssGetSFunction ( S , 5 ) ; callSysFcns =
ssGetCallSystemOutputFcnList ( childS ) ; callSysFcns [ 3 + 0 ] = (
SysOutputFcn ) ( NULL ) ; childS = ssGetSFunction ( S , 6 ) ; callSysFcns =
ssGetCallSystemOutputFcnList ( childS ) ; callSysFcns [ 3 + 0 ] = (
SysOutputFcn ) ( NULL ) ; childS = ssGetSFunction ( S , 7 ) ; callSysFcns =
ssGetCallSystemOutputFcnList ( childS ) ; callSysFcns [ 3 + 0 ] = (
SysOutputFcn ) ( NULL ) ; childS = ssGetSFunction ( S , 8 ) ; callSysFcns =
ssGetCallSystemOutputFcnList ( childS ) ; callSysFcns [ 3 + 0 ] = (
SysOutputFcn ) ( NULL ) ; childS = ssGetSFunction ( S , 9 ) ; callSysFcns =
ssGetCallSystemOutputFcnList ( childS ) ; callSysFcns [ 3 + 0 ] = (
SysOutputFcn ) ( NULL ) ; childS = ssGetSFunction ( S , 10 ) ; callSysFcns =
ssGetCallSystemOutputFcnList ( childS ) ; callSysFcns [ 3 + 0 ] = (
SysOutputFcn ) ( NULL ) ; childS = ssGetSFunction ( S , 11 ) ; callSysFcns =
ssGetCallSystemOutputFcnList ( childS ) ; callSysFcns [ 3 + 0 ] = (
SysOutputFcn ) ( NULL ) ; childS = ssGetSFunction ( S , 12 ) ; callSysFcns =
ssGetCallSystemOutputFcnList ( childS ) ; callSysFcns [ 3 + 0 ] = (
SysOutputFcn ) ( NULL ) ; childS = ssGetSFunction ( S , 13 ) ; callSysFcns =
ssGetCallSystemOutputFcnList ( childS ) ; callSysFcns [ 3 + 0 ] = (
SysOutputFcn ) ( NULL ) ; childS = ssGetSFunction ( S , 14 ) ; callSysFcns =
ssGetCallSystemOutputFcnList ( childS ) ; callSysFcns [ 3 + 0 ] = (
SysOutputFcn ) ( NULL ) ; childS = ssGetSFunction ( S , 15 ) ; callSysFcns =
ssGetCallSystemOutputFcnList ( childS ) ; callSysFcns [ 3 + 0 ] = (
SysOutputFcn ) ( NULL ) ; childS = ssGetSFunction ( S , 16 ) ; callSysFcns =
ssGetCallSystemOutputFcnList ( childS ) ; callSysFcns [ 3 + 0 ] = (
SysOutputFcn ) ( NULL ) ; childS = ssGetSFunction ( S , 17 ) ; callSysFcns =
ssGetCallSystemOutputFcnList ( childS ) ; callSysFcns [ 3 + 0 ] = (
SysOutputFcn ) ( NULL ) ; childS = ssGetSFunction ( S , 18 ) ; callSysFcns =
ssGetCallSystemOutputFcnList ( childS ) ; callSysFcns [ 3 + 0 ] = (
SysOutputFcn ) ( NULL ) ; childS = ssGetSFunction ( S , 19 ) ; callSysFcns =
ssGetCallSystemOutputFcnList ( childS ) ; callSysFcns [ 3 + 0 ] = (
SysOutputFcn ) ( NULL ) ; childS = ssGetSFunction ( S , 20 ) ; callSysFcns =
ssGetCallSystemOutputFcnList ( childS ) ; callSysFcns [ 3 + 0 ] = (
SysOutputFcn ) ( NULL ) ; childS = ssGetSFunction ( S , 21 ) ; callSysFcns =
ssGetCallSystemOutputFcnList ( childS ) ; callSysFcns [ 3 + 0 ] = (
SysOutputFcn ) ( NULL ) ; childS = ssGetSFunction ( S , 22 ) ; callSysFcns =
ssGetCallSystemOutputFcnList ( childS ) ; callSysFcns [ 3 + 0 ] = (
SysOutputFcn ) ( NULL ) ; childS = ssGetSFunction ( S , 23 ) ; callSysFcns =
ssGetCallSystemOutputFcnList ( childS ) ; callSysFcns [ 3 + 0 ] = (
SysOutputFcn ) ( NULL ) ; childS = ssGetSFunction ( S , 24 ) ; callSysFcns =
ssGetCallSystemOutputFcnList ( childS ) ; callSysFcns [ 3 + 0 ] = (
SysOutputFcn ) ( NULL ) ; childS = ssGetSFunction ( S , 25 ) ; callSysFcns =
ssGetCallSystemOutputFcnList ( childS ) ; callSysFcns [ 3 + 0 ] = (
SysOutputFcn ) ( NULL ) ; childS = ssGetSFunction ( S , 26 ) ; callSysFcns =
ssGetCallSystemOutputFcnList ( childS ) ; callSysFcns [ 3 + 0 ] = (
SysOutputFcn ) ( NULL ) ; childS = ssGetSFunction ( S , 27 ) ; callSysFcns =
ssGetCallSystemOutputFcnList ( childS ) ; callSysFcns [ 3 + 0 ] = (
SysOutputFcn ) ( NULL ) ; childS = ssGetSFunction ( S , 28 ) ; callSysFcns =
ssGetCallSystemOutputFcnList ( childS ) ; callSysFcns [ 3 + 0 ] = (
SysOutputFcn ) ( NULL ) ; childS = ssGetSFunction ( S , 29 ) ; callSysFcns =
ssGetCallSystemOutputFcnList ( childS ) ; callSysFcns [ 3 + 0 ] = (
SysOutputFcn ) ( NULL ) ; childS = ssGetSFunction ( S , 30 ) ; callSysFcns =
ssGetCallSystemOutputFcnList ( childS ) ; callSysFcns [ 3 + 0 ] = (
SysOutputFcn ) ( NULL ) ; childS = ssGetSFunction ( S , 31 ) ; callSysFcns =
ssGetCallSystemOutputFcnList ( childS ) ; callSysFcns [ 3 + 0 ] = (
SysOutputFcn ) ( NULL ) ; childS = ssGetSFunction ( S , 32 ) ; callSysFcns =
ssGetCallSystemOutputFcnList ( childS ) ; callSysFcns [ 3 + 0 ] = (
SysOutputFcn ) ( NULL ) ; childS = ssGetSFunction ( S , 33 ) ; callSysFcns =
ssGetCallSystemOutputFcnList ( childS ) ; callSysFcns [ 3 + 0 ] = (
SysOutputFcn ) ( NULL ) ; childS = ssGetSFunction ( S , 34 ) ; callSysFcns =
ssGetCallSystemOutputFcnList ( childS ) ; callSysFcns [ 3 + 0 ] = (
SysOutputFcn ) ( NULL ) ; childS = ssGetSFunction ( S , 35 ) ; callSysFcns =
ssGetCallSystemOutputFcnList ( childS ) ; callSysFcns [ 3 + 0 ] = (
SysOutputFcn ) ( NULL ) ; childS = ssGetSFunction ( S , 36 ) ; callSysFcns =
ssGetCallSystemOutputFcnList ( childS ) ; callSysFcns [ 3 + 0 ] = (
SysOutputFcn ) ( NULL ) ; childS = ssGetSFunction ( S , 37 ) ; callSysFcns =
ssGetCallSystemOutputFcnList ( childS ) ; callSysFcns [ 3 + 0 ] = (
SysOutputFcn ) ( NULL ) ; childS = ssGetSFunction ( S , 38 ) ; callSysFcns =
ssGetCallSystemOutputFcnList ( childS ) ; callSysFcns [ 3 + 0 ] = (
SysOutputFcn ) ( NULL ) ; childS = ssGetSFunction ( S , 39 ) ; callSysFcns =
ssGetCallSystemOutputFcnList ( childS ) ; callSysFcns [ 3 + 0 ] = (
SysOutputFcn ) ( NULL ) ; childS = ssGetSFunction ( S , 40 ) ; callSysFcns =
ssGetCallSystemOutputFcnList ( childS ) ; callSysFcns [ 3 + 0 ] = (
SysOutputFcn ) ( NULL ) ; childS = ssGetSFunction ( S , 41 ) ; callSysFcns =
ssGetCallSystemOutputFcnList ( childS ) ; callSysFcns [ 3 + 0 ] = (
SysOutputFcn ) ( NULL ) ; childS = ssGetSFunction ( S , 42 ) ; callSysFcns =
ssGetCallSystemOutputFcnList ( childS ) ; callSysFcns [ 3 + 0 ] = (
SysOutputFcn ) ( NULL ) ; childS = ssGetSFunction ( S , 43 ) ; callSysFcns =
ssGetCallSystemOutputFcnList ( childS ) ; callSysFcns [ 3 + 0 ] = (
SysOutputFcn ) ( NULL ) ; } } static void mdlTerminate ( SimStruct * S ) { }
#include "simulink.c"

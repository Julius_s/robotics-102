#include "__cf_LessonII.h"
#ifndef RTW_HEADER_LessonII_acc_types_h_
#define RTW_HEADER_LessonII_acc_types_h_
#include "rtwtypes.h"
#include "multiword_types.h"
#ifndef SS_INT64
#define SS_INT64  36
#endif
#ifndef SS_UINT64
#define SS_UINT64  37
#endif
#ifndef _DEFINED_TYPEDEF_FOR_BlobData_size_
#define _DEFINED_TYPEDEF_FOR_BlobData_size_
typedef struct { int32_T centroid [ 2 ] ; int32_T area ; int32_T color ; }
BlobData_size ;
#endif
#ifndef _DEFINED_TYPEDEF_FOR_PlayerData_size_
#define _DEFINED_TYPEDEF_FOR_PlayerData_size_
typedef struct { int32_T x [ 2 ] ; int32_T y [ 2 ] ; int32_T orientation [ 2
] ; int32_T color [ 2 ] ; } PlayerData_size ;
#endif
#ifndef _DEFINED_TYPEDEF_FOR_BallData_size_
#define _DEFINED_TYPEDEF_FOR_BallData_size_
typedef struct { int32_T x ; int32_T y ; } BallData_size ;
#endif
#ifndef _DEFINED_TYPEDEF_FOR_Waypoint_
#define _DEFINED_TYPEDEF_FOR_Waypoint_
typedef struct { int8_T x ; int8_T y ; int16_T orientation ; uint8_T
sl_padding0 [ 4 ] ; } Waypoint ;
#endif
#ifndef _DEFINED_TYPEDEF_FOR_Player_
#define _DEFINED_TYPEDEF_FOR_Player_
typedef struct { int8_T x ; int8_T y ; int16_T orientation ; uint8_T color ;
uint8_T position ; uint8_T valid ; uint8_T sl_padding0 ; } Player ;
#endif
#ifndef _DEFINED_TYPEDEF_FOR_Ball_
#define _DEFINED_TYPEDEF_FOR_Ball_
typedef struct { int8_T x ; int8_T y ; uint8_T valid ; uint8_T sl_padding0 [
5 ] ; } Ball ;
#endif
#ifndef _DEFINED_TYPEDEF_FOR_BlobData_
#define _DEFINED_TYPEDEF_FOR_BlobData_
typedef struct { real32_T centroid [ 32 ] ; int32_T area [ 16 ] ; uint8_T
color [ 16 ] ; } BlobData ;
#endif
#ifndef _DEFINED_TYPEDEF_FOR_PlayerData_
#define _DEFINED_TYPEDEF_FOR_PlayerData_
typedef struct { int8_T x [ 6 ] ; int8_T y [ 6 ] ; int16_T orientation [ 6 ]
; uint8_T color [ 6 ] ; uint8_T sl_padding0 [ 2 ] ; } PlayerData ;
#endif
#ifndef _DEFINED_TYPEDEF_FOR_BallData_
#define _DEFINED_TYPEDEF_FOR_BallData_
typedef struct { int8_T x [ 2 ] ; int8_T y [ 2 ] ; uint8_T sl_padding0 [ 4 ]
; } BallData ;
#endif
typedef struct diaowut2vy_ diaowut2vy ;
#endif

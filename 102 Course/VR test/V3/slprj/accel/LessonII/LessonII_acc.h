#include "__cf_LessonII.h"
#ifndef RTW_HEADER_LessonII_acc_h_
#define RTW_HEADER_LessonII_acc_h_
#include <stddef.h>
#include <string.h>
#ifndef LessonII_acc_COMMON_INCLUDES_
#define LessonII_acc_COMMON_INCLUDES_
#include <stdlib.h>
#define S_FUNCTION_NAME simulink_only_sfcn 
#define S_FUNCTION_LEVEL 2
#define RTW_GENERATED_S_FUNCTION
#include "rtwtypes.h"
#include "simstruc.h"
#include "fixedpoint.h"
#endif
#include "LessonII_acc_types.h"
#include "multiword_types.h"
#include "mwmathutil.h"
#include "rt_defines.h"
#include "rt_nonfinite.h"
typedef struct { BlobData md032svwj1 ; PlayerData jxo1xntrph ; real_T
jfmah2lk3w [ 2 ] ; real_T lzr2dxafya ; real_T j5dzrih1ud [ 6 ] ; real_T
kgnz1ldmye [ 6 ] ; real_T ec3bs5npq0 ; real_T c0gyrnqbgw ; real_T mdkwizee2r
; real_T mtlmhgvt4k ; real_T hfoblv4ro0 ; real_T akoreyjgfo ; real_T
oq04zq4div ; real_T fxurcz2zn2 ; real_T fytypqmc0k ; real_T ik2syjztup [ 6 ]
; real_T i1nmneh3lh [ 6 ] ; real_T c0zblc31z1 ; real_T b3csq5ksrg ; real_T
askjqbwul2 ; real_T gxmra5ku2e ; real_T grkruxpoqc ; real_T hyg0vphneq ;
Waypoint l00rfvjnwz ; Waypoint m3eijgp1ov ; Waypoint ilcffv3ndo ; Waypoint
o4n0fbfpvr ; Waypoint a2rcfjgglm ; Waypoint hvmnrwdold ; Waypoint hzlclzb2p0
; Waypoint ohv02zkswk ; Waypoint mov1k5m11v ; Player ibg5wmkoqa ; Player
kc3teyvigg ; Player jaepky1crd ; Player frf33h2oo3 ; Player jnus0nczag ;
Player iuspssxa4x [ 6 ] ; Player eqtawkgatx [ 6 ] ; Player it2ho1drrd [ 6 ] ;
Player bni0i3ip43 [ 6 ] ; Player ixqzozv4si [ 6 ] ; Player o41r3srig0 [ 6 ] ;
Ball oujpkv2efr ; Ball hi0mq23b1e ; Ball gktpiwywhl ; Ball lsvkobs22z ; Ball
oqykijepkd ; Ball djssfybdfo ; BallData e2d4arigen ; real32_T kh514ecbye [ 3
] ; real32_T g3snywqqsd [ 32 ] ; real32_T omx4digdjr [ 3 ] ; real32_T
g5qlkichlv [ 3 ] ; real32_T edfvufvrrx [ 3 ] ; real32_T imbzpzg52y [ 6 ] ;
real32_T njwi5hrqjx [ 3 ] ; real32_T aavl5ejkfx [ 18 ] ; real32_T b3lhchb1bp
[ 3 ] ; real32_T plikmyrwvw [ 3 ] ; real32_T enudydd52w [ 3 ] ; real32_T
bh1tldl3jb [ 3 ] ; real32_T i1mvvv5xlb [ 3 ] ; real32_T pkml4zlbz4 [ 3 ] ;
real32_T ogve5ic4fw [ 3 ] ; real32_T pwoi4hytbl [ 4 ] ; real32_T k5tfvybxjm [
4 ] ; real32_T nepgel10e5 [ 4 ] ; real32_T bty031phsv [ 4 ] ; real32_T
egm0n35pvq [ 4 ] ; real32_T frctfibrqz [ 4 ] ; real32_T h3azju3rbu [ 3 ] ;
real32_T hxyzsiww0g [ 24 ] ; real32_T fwy2buoicd [ 18 ] ; int32_T fb1lff2sdv
[ 16 ] ; int16_T laabiylw5i ; int16_T e1wwmdelp2 ; int16_T ed0xfhwloe ;
int16_T hthvvvsznx ; int16_T hk2q23kn3d ; int16_T kv3uxjzw5r ; int16_T
f3ri0wya2h ; int16_T aria1gwzsn ; int16_T m4h3bsuszy ; int16_T capjelp0cz ;
int16_T ibyts1da5c ; int16_T jgubdh3j14 ; int16_T lsjyf3qu3e ; int16_T
bulsyk12sw ; int16_T anba0cqdma ; int16_T ewprsyuo3p ; int16_T ffewisavg2 ;
int16_T dxjnfukhua ; int16_T iuzzt5ohk2 ; int16_T l2a4umeg2l ; int16_T
nomuvhangv ; int16_T i2j2u2ea0j ; int16_T mu0mwzwfai ; int16_T bfngcbzr03 ;
int16_T c3dgzbflta ; int8_T pelmwxp33e [ 2 ] ; int8_T kacoh1z4rv [ 2 ] ;
int8_T kgzsvc10sv [ 2 ] ; int8_T osmfr34c2i [ 2 ] ; int8_T lukjbjjip3 [ 2 ] ;
int8_T kzma3vtafj [ 2 ] ; int8_T f4hc1212gs [ 2 ] ; int8_T k5rjzba5s2 [ 2 ] ;
int8_T lbgezj4mg0 [ 2 ] ; int8_T izhhi3yrk4 [ 2 ] ; uint8_T jwbsnctla4 [
230400 ] ; uint8_T llyfe3o5am ; uint8_T d2n5bsxvci ; uint8_T hszxpjmt5l ;
uint8_T pv2uiz55u5 ; uint8_T kgevnince5 ; uint8_T kaxsw44guk ; uint8_T
fokhixpacr ; uint8_T om4hmm0icv ; uint8_T biowo4bths [ 230400 ] ; uint8_T
dfgq20gq3j [ 64 ] ; uint8_T dma5cbx1yd ; uint8_T ljm4djbuc4 [ 76800 ] ;
uint8_T m32l5sjsd3 [ 76800 ] ; uint8_T j33crokx52 [ 76800 ] ; uint8_T
lda3tru0pt [ 31 ] ; uint8_T pvvjronwhn ; uint8_T nev5u44ufw [ 31 ] ; uint8_T
jtblliiht0 ; uint8_T lerckhsap0 [ 31 ] ; uint8_T i2fpwlctp0 ; uint8_T
ewza3bqoyp [ 31 ] ; uint8_T bvbuup4nd1 ; uint8_T c4lt5j2wby [ 31 ] ; uint8_T
g0apqksivd ; uint8_T bfnb4bvikl [ 76800 ] ; uint8_T hkayboxns4 ; uint8_T
ezeyauz3sj ; boolean_T f0fr0kqurg [ 76800 ] ; boolean_T ahxxaxfzui [ 76800 ]
; boolean_T hpn2cs0fjc [ 76800 ] ; boolean_T pbb4t4fmph [ 76800 ] ; char
pad_pbb4t4fmph [ 3 ] ; } f3ep0xylfb ; typedef struct { PlayerData_size
fimvma0oob ; BlobData_size o03eark55y ; real_T dr0znjszqf ; BallData_size
ggbbq2dc5v ; struct { void * LoggedData ; } gbfuywyvmu ; struct { void *
LoggedData ; } bogbx3m2dy ; struct { void * LoggedData ; } abon2fobha ;
struct { void * LoggedData ; } h0uu2g2ji2 ; struct { void * LoggedData ; }
e2a31ytbhy ; struct { void * LoggedData ; } nx2kfwgun4 ; struct { void *
LoggedData ; } avyjbwffii ; struct { void * LoggedData ; } jfscl50npw ;
struct { void * LoggedData ; } idbjimia2h ; struct { void * LoggedData ; }
n5h1kfwqcz ; void * dysnxp0b0n [ 29 ] ; int32_T hm4n4rxevh [ 256 ] ; int32_T
lkmrwk3l1k [ 2 ] ; int32_T mojtovgd2n [ 2 ] ; int32_T ebhen41rmh ; int32_T
myflnhbjpy [ 2 ] ; uint32_T cd1picjivu [ 76800 ] ; uint32_T arkq12bzd2 [
76800 ] ; int32_T hu0hyclw5o [ 256 ] ; int32_T ogiz4wkto3 [ 256 ] ; int32_T
hzzyqq3mgx ; int16_T gddwfu4tlr [ 76800 ] ; int16_T is4jmyk3p3 [ 76800 ] ;
uint8_T fmeikhvspm [ 230400 ] ; uint8_T mnjf24ynlg [ 77924 ] ; char
pad_mnjf24ynlg [ 4 ] ; } oysdyzllkj ; typedef struct { const uint8_T
gugylkeuew ; char pad_gugylkeuew [ 7 ] ; } bvfejktfbl ;
#define jpf4is5jzw(S) ((bvfejktfbl *) _ssGetConstBlockIO(S))
typedef struct { int32_T dypu2qoqhd [ 8 ] ; int32_T ffxkz0id24 ; uint8_T
f5wblq4k00 ; uint8_T pgadi0ceb3 ; char pad_pgadi0ceb3 [ 2 ] ; } nyzppbjnau ;
struct diaowut2vy_ { real_T P_0 ; real_T P_1 ; real_T P_2 ; real_T P_3 ;
real_T P_4 ; real_T P_5 [ 2 ] ; real_T P_6 ; real_T P_7 ; real_T P_8 ; real_T
P_9 ; real_T P_10 ; real_T P_11 ; real_T P_12 ; real_T P_13 ; real_T P_14 ;
real_T P_15 ; real_T P_16 ; real_T P_17 ; real_T P_18 ; real_T P_19 ; real_T
P_20 ; real_T P_21 ; real_T P_22 ; real_T P_23 ; real_T P_24 ; real_T P_25 ;
real_T P_26 ; real_T P_27 ; real_T P_28 ; real_T P_29 ; real_T P_30 ; real_T
P_31 ; real_T P_32 ; real_T P_33 ; real_T P_34 ; real_T P_35 ; real_T P_36 ;
real_T P_37 ; real_T P_38 ; real_T P_39 ; real_T P_40 ; real_T P_41 ; real_T
P_42 ; real_T P_43 ; real_T P_44 ; real_T P_45 ; real_T P_46 ; real_T P_47 ;
real_T P_48 ; real_T P_49 ; real_T P_50 ; real_T P_51 ; real_T P_52 ; real_T
P_53 ; real_T P_54 ; real_T P_55 ; real_T P_56 ; real32_T P_57 [ 3 ] ;
real32_T P_58 [ 3 ] ; real32_T P_59 [ 3 ] ; real32_T P_60 [ 3 ] ; real32_T
P_61 [ 6 ] ; real32_T P_62 [ 3 ] ; real32_T P_63 [ 18 ] ; real32_T P_64 [ 3 ]
; real32_T P_65 [ 3 ] ; real32_T P_66 [ 3 ] ; real32_T P_67 [ 3 ] ; real32_T
P_68 [ 3 ] ; real32_T P_69 [ 3 ] ; real32_T P_70 [ 3 ] ; int16_T P_71 ;
int16_T P_72 ; int16_T P_73 ; int16_T P_74 ; int16_T P_75 ; int16_T P_76 ;
int16_T P_77 ; int16_T P_78 ; int16_T P_79 ; int16_T P_80 ; int16_T P_81 ;
int16_T P_82 ; int16_T P_83 ; int16_T P_84 ; int16_T P_85 ; uint8_T P_86 ;
uint8_T P_87 ; uint8_T P_88 [ 256 ] ; uint8_T P_89 [ 256 ] ; uint8_T P_90 [
256 ] ; uint8_T P_91 ; uint8_T P_92 ; uint8_T P_93 ; uint8_T P_94 ; uint8_T
P_95 ; uint8_T P_96 ; uint8_T P_97 ; uint8_T P_98 ; uint8_T P_99 ; uint8_T
P_100 ; uint8_T P_101 ; uint8_T P_102 ; uint8_T P_103 ; uint8_T P_104 ; char
pad_P_104 [ 2 ] ; } ; extern diaowut2vy kyxyccx1mn ; extern const bvfejktfbl
f0eruxjaz3 ; extern const nyzppbjnau axzuzwdj5b ;
#endif

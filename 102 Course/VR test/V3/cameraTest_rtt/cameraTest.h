/*
 * File: cameraTest.h
 *
 * Code generated for Simulink model 'cameraTest'.
 *
 * Model version                  : 1.2
 * Simulink Coder version         : 8.7 (R2014b) 08-Sep-2014
 * TLC version                    : 8.7 (Aug  5 2014)
 * C/C++ source code generated on : Wed Feb 11 17:41:24 2015
 *
 * Target selection: realtime.tlc
 * Embedded hardware selection: ARM Compatible->ARM Cortex
 * Code generation objectives: Unspecified
 * Validation result: Not run
 */

#ifndef RTW_HEADER_cameraTest_h_
#define RTW_HEADER_cameraTest_h_
#include <float.h>
#include <math.h>
#include <string.h>
#include <stddef.h>
#ifndef cameraTest_COMMON_INCLUDES_
# define cameraTest_COMMON_INCLUDES_
#include "rtwtypes.h"
#include "rtw_extmode.h"
#include "sysran_types.h"
#include "rtw_continuous.h"
#include "rtw_solver.h"
#include "dt_info.h"
#include "ext_work.h"
#include "v4l2_capture.h"
#include "sdl_video_display.h"
#endif                                 /* cameraTest_COMMON_INCLUDES_ */

#include "cameraTest_types.h"

/* Shared type includes */
#include "multiword_types.h"
#include "rtGetNaN.h"
#include "rt_nonfinite.h"
#include "rt_defines.h"
#include "rtGetInf.h"

/* Macros for accessing real-time model data structure */
#ifndef rtmGetFinalTime
# define rtmGetFinalTime(rtm)          ((rtm)->Timing.tFinal)
#endif

#ifndef rtmGetErrorStatus
# define rtmGetErrorStatus(rtm)        ((rtm)->errorStatus)
#endif

#ifndef rtmSetErrorStatus
# define rtmSetErrorStatus(rtm, val)   ((rtm)->errorStatus = (val))
#endif

#ifndef rtmGetStopRequested
# define rtmGetStopRequested(rtm)      ((rtm)->Timing.stopRequestedFlag)
#endif

#ifndef rtmSetStopRequested
# define rtmSetStopRequested(rtm, val) ((rtm)->Timing.stopRequestedFlag = (val))
#endif

#ifndef rtmGetStopRequestedPtr
# define rtmGetStopRequestedPtr(rtm)   (&((rtm)->Timing.stopRequestedFlag))
#endif

#ifndef rtmGetT
# define rtmGetT(rtm)                  ((rtm)->Timing.taskTime0)
#endif

#ifndef rtmGetTFinal
# define rtmGetTFinal(rtm)             ((rtm)->Timing.tFinal)
#endif

/* Block signals (auto storage) */
typedef struct {
  uint8_T V4L2VideoCapture_o1[76800];  /* '<Root>/V4L2 Video Capture' */
  uint8_T V4L2VideoCapture_o2[76800];  /* '<Root>/V4L2 Video Capture' */
  uint8_T V4L2VideoCapture_o3[76800];  /* '<Root>/V4L2 Video Capture' */
  uint8_T hG[76800];
  uint8_T BI[76800];                   /* '<S2>/MATLAB Function' */
  uint8_T GI[76800];                   /* '<S2>/MATLAB Function' */
  uint8_T RI[76800];                   /* '<S2>/MATLAB Function' */
  real_T lengthh;                      /* '<S2>/FindBlobs1' */
  Player players[6];                   /* '<S1>/Player Tracker' */
  Ball ball;                           /* '<S1>/Player Tracker' */
  int32_T BlobAnalysis_o1[16];         /* '<S2>/Blob Analysis' */
  uint8_T Autothreshold_o2;            /* '<S2>/Autothreshold' */
  uint8_T Autothreshold1_o2;           /* '<S2>/Autothreshold1' */
  uint8_T Autothreshold2_o2;           /* '<S2>/Autothreshold2' */
  uint8_T gameOn;                      /* '<S1>/Chart' */
  boolean_T Autothreshold_o1[76800];   /* '<S2>/Autothreshold' */
  boolean_T Autothreshold1_o1[76800];  /* '<S2>/Autothreshold1' */
  boolean_T Autothreshold2_o1[76800];  /* '<S2>/Autothreshold2' */
  boolean_T Compare[76800];            /* '<S7>/Compare' */
} B_cameraTest_T;

/* Block states (auto storage) for system '<Root>' */
typedef struct {
  uint32_T BlobAnalysis_STACK_DW[76800];/* '<S2>/Blob Analysis' */
  int16_T BlobAnalysis_N_PIXLIST_DW[76800];/* '<S2>/Blob Analysis' */
  int16_T BlobAnalysis_M_PIXLIST_DW[76800];/* '<S2>/Blob Analysis' */
  uint8_T BlobAnalysis_PAD_DW[77924];  /* '<S2>/Blob Analysis' */
  stETj7QHScyDJroYDDeQcXH_camer_T latestPlayers;/* '<S1>/Player Tracker' */
  PlayerData_size SFunction_DIMS2;     /* '<S1>/parseBlob' */
  BlobData_size SFunction_DIMS2_a;     /* '<S2>/FindBlobs' */
  Ball latestBall;                     /* '<S1>/Player Tracker' */
  BallData_size SFunction_DIMS3;       /* '<S1>/parseBlob' */
  struct {
    void *LoggedData;
  } Scope_PWORK;                       /* '<S2>/Scope' */

  struct {
    void *LoggedData;
  } Scope1_PWORK;                      /* '<S2>/Scope1' */

  struct {
    void *LoggedData;
  } Scope2_PWORK;                      /* '<S2>/Scope2' */

  int32_T BlobAnalysis_DIMS1[2];       /* '<S2>/Blob Analysis' */
  int32_T BlobAnalysis_DIMS2[2];       /* '<S2>/Blob Analysis' */
  int32_T SFunction_DIMS2_b[2];        /* '<S1>/encode' */
  int32_T sfEvent;                     /* '<S1>/Chart' */
  uint8_T is_active_c10_cameraTest;    /* '<S1>/Chart' */
  uint8_T is_c10_cameraTest;           /* '<S1>/Chart' */
  uint8_T idleTicks;                   /* '<S1>/Chart' */
  boolean_T isStable;                  /* '<S1>/Chart' */
} DW_cameraTest_T;

/* Constant parameters (auto storage) */
typedef struct {
  /* Computed Parameter: BlobAnalysis_WALKER_R
   * Referenced by: '<S2>/Blob Analysis'
   */
  int32_T BlobAnalysis_WALKER_R[8];

  /* Expression: devName
   * Referenced by: '<Root>/V4L2 Video Capture'
   */
  uint8_T V4L2VideoCapture_p1[12];
} ConstP_cameraTest_T;

/* Parameters (auto storage) */
struct P_cameraTest_T_ {
  real_T Constant6_Value[2];           /* Expression: [240 320]
                                        * Referenced by: '<S1>/Constant6'
                                        */
  real_T Constant7_Value;              /* Expression: 1.3
                                        * Referenced by: '<S1>/Constant7'
                                        */
  uint8_T Autothreshold_BIN_BOUNDARY_FIXP[256];/* Computed Parameter: Autothreshold_BIN_BOUNDARY_FIXP
                                                * Referenced by: '<S2>/Autothreshold'
                                                */
  uint8_T Autothreshold1_BIN_BOUNDARY_FIX[256];/* Computed Parameter: Autothreshold1_BIN_BOUNDARY_FIX
                                                * Referenced by: '<S2>/Autothreshold1'
                                                */
  uint8_T Autothreshold2_BIN_BOUNDARY_FIX[256];/* Computed Parameter: Autothreshold2_BIN_BOUNDARY_FIX
                                                * Referenced by: '<S2>/Autothreshold2'
                                                */
  uint8_T Constant_Value;              /* Computed Parameter: Constant_Value
                                        * Referenced by: '<S7>/Constant'
                                        */
};

/* Real-time Model Data Structure */
struct tag_RTM_cameraTest_T {
  const char_T *errorStatus;
  RTWExtModeInfo *extModeInfo;

  /*
   * Sizes:
   * The following substructure contains sizes information
   * for many of the model attributes such as inputs, outputs,
   * dwork, sample times, etc.
   */
  struct {
    uint32_T checksums[4];
  } Sizes;

  /*
   * SpecialInfo:
   * The following substructure contains special information
   * related to other components that are dependent on RTW.
   */
  struct {
    const void *mappingInfo;
  } SpecialInfo;

  /*
   * Timing:
   * The following substructure contains information regarding
   * the timing information for the model.
   */
  struct {
    time_T taskTime0;
    uint32_T clockTick0;
    time_T stepSize0;
    time_T tFinal;
    boolean_T stopRequestedFlag;
  } Timing;
};

/* Block parameters (auto storage) */
extern P_cameraTest_T cameraTest_P;

/* Block signals (auto storage) */
extern B_cameraTest_T cameraTest_B;

/* Block states (auto storage) */
extern DW_cameraTest_T cameraTest_DW;

/* Constant parameters (auto storage) */
extern const ConstP_cameraTest_T cameraTest_ConstP;

/* Model entry point functions */
extern void cameraTest_initialize(void);
extern void cameraTest_output(void);
extern void cameraTest_update(void);
extern void cameraTest_terminate(void);

/* Real-time Model object */
extern RT_MODEL_cameraTest_T *const cameraTest_M;

/*-
 * The generated code includes comments that allow you to trace directly
 * back to the appropriate location in the model.  The basic format
 * is <system>/block_name, where system is the system number (uniquely
 * assigned by Simulink) and block_name is the name of the block.
 *
 * Use the MATLAB hilite_system command to trace the generated code back
 * to the model.  For example,
 *
 * hilite_system('<S3>')    - opens system 3
 * hilite_system('<S3>/Kp') - opens and selects block Kp which resides in S3
 *
 * Here is the system hierarchy for this model
 *
 * '<Root>' : 'cameraTest'
 * '<S1>'   : 'cameraTest/Raspi Code'
 * '<S2>'   : 'cameraTest/Raspi Code/Blob extraction '
 * '<S3>'   : 'cameraTest/Raspi Code/Chart'
 * '<S4>'   : 'cameraTest/Raspi Code/Player Tracker'
 * '<S5>'   : 'cameraTest/Raspi Code/encode'
 * '<S6>'   : 'cameraTest/Raspi Code/parseBlob'
 * '<S7>'   : 'cameraTest/Raspi Code/Blob extraction /Compare To Zero'
 * '<S8>'   : 'cameraTest/Raspi Code/Blob extraction /FindBlobs'
 * '<S9>'   : 'cameraTest/Raspi Code/Blob extraction /FindBlobs1'
 * '<S10>'  : 'cameraTest/Raspi Code/Blob extraction /MATLAB Function'
 */
#endif                                 /* RTW_HEADER_cameraTest_h_ */

/*
 * File trailer for generated code.
 *
 * [EOF]
 */

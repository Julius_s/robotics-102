/*
 * File: cameraTest.c
 *
 * Code generated for Simulink model 'cameraTest'.
 *
 * Model version                  : 1.2
 * Simulink Coder version         : 8.7 (R2014b) 08-Sep-2014
 * TLC version                    : 8.7 (Aug  5 2014)
 * C/C++ source code generated on : Wed Feb 11 17:41:24 2015
 *
 * Target selection: realtime.tlc
 * Embedded hardware selection: ARM Compatible->ARM Cortex
 * Code generation objectives: Unspecified
 * Validation result: Not run
 */

#include "cameraTest.h"
#include "cameraTest_private.h"
#include "cameraTest_dt.h"

/* Named constants for Chart: '<S1>/Chart' */
#define cameraTest_IN_NO_ACTIVE_CHILD  ((uint8_T)0U)
#define cameraTest_IN_gameIsOn         ((uint8_T)1U)
#define cameraTest_IN_notOn            ((uint8_T)2U)

/* Block signals (auto storage) */
B_cameraTest_T cameraTest_B;

/* Block states (auto storage) */
DW_cameraTest_T cameraTest_DW;

/* Real-time model */
RT_MODEL_cameraTest_T cameraTest_M_;
RT_MODEL_cameraTest_T *const cameraTest_M = &cameraTest_M_;

/* Forward declaration for local functions */
static void cameraTest_eml_sort(const int32_T x_data[], const int32_T x_sizes,
  int32_T y_data[], int32_T *y_sizes, int32_T idx_data[], int32_T *idx_sizes);
static void cameraTest_eml_li_find(const boolean_T x_data[], const int32_T
  x_sizes[2], int32_T y_data[], int32_T y_sizes[2]);
static uint8_T cameraTest_calcReady(void);
static uint8_T cameraTest_calcGoal(void);
static real32_T cameraTest_norm(const real32_T x[2]);
void mul_wide_u32(uint32_T in0, uint32_T in1, uint32_T *ptrOutBitsHi, uint32_T
                  *ptrOutBitsLo)
{
  uint32_T outBitsLo;
  uint32_T in0Lo;
  uint32_T in0Hi;
  uint32_T in1Lo;
  uint32_T in1Hi;
  uint32_T productHiLo;
  uint32_T productLoHi;
  in0Hi = in0 >> 16U;
  in0Lo = in0 & 65535U;
  in1Hi = in1 >> 16U;
  in1Lo = in1 & 65535U;
  productHiLo = in0Hi * in1Lo;
  productLoHi = in0Lo * in1Hi;
  in0Lo *= in1Lo;
  in1Lo = 0U;
  outBitsLo = (productLoHi << 16U) + in0Lo;
  if (outBitsLo < in0Lo) {
    in1Lo = 1U;
  }

  in0Lo = outBitsLo;
  outBitsLo += productHiLo << 16U;
  if (outBitsLo < in0Lo) {
    in1Lo++;
  }

  *ptrOutBitsHi = (((productLoHi >> 16U) + (productHiLo >> 16U)) + in0Hi * in1Hi)
    + in1Lo;
  *ptrOutBitsLo = outBitsLo;
}

uint32_T mul_u32_u32_u32_sr15(uint32_T a, uint32_T b)
{
  uint32_T result;
  uint32_T u32_chi;
  mul_wide_u32(a, b, &u32_chi, &result);
  return u32_chi << 17U | result >> 15U;
}

void mul_wide_s32(int32_T in0, int32_T in1, uint32_T *ptrOutBitsHi, uint32_T
                  *ptrOutBitsLo)
{
  uint32_T absIn0;
  uint32_T absIn1;
  uint32_T in0Lo;
  uint32_T in0Hi;
  uint32_T in1Hi;
  uint32_T productHiLo;
  uint32_T productLoHi;
  absIn0 = (uint32_T)(in0 < 0 ? -in0 : in0);
  absIn1 = (uint32_T)(in1 < 0 ? -in1 : in1);
  in0Hi = absIn0 >> 16U;
  in0Lo = absIn0 & 65535U;
  in1Hi = absIn1 >> 16U;
  absIn0 = absIn1 & 65535U;
  productHiLo = in0Hi * absIn0;
  productLoHi = in0Lo * in1Hi;
  absIn0 *= in0Lo;
  absIn1 = 0U;
  in0Lo = (productLoHi << 16U) + absIn0;
  if (in0Lo < absIn0) {
    absIn1 = 1U;
  }

  absIn0 = in0Lo;
  in0Lo += productHiLo << 16U;
  if (in0Lo < absIn0) {
    absIn1++;
  }

  absIn0 = (((productLoHi >> 16U) + (productHiLo >> 16U)) + in0Hi * in1Hi) +
    absIn1;
  if (!((in0 == 0) || ((in1 == 0) || ((in0 > 0) == (in1 > 0))))) {
    absIn0 = ~absIn0;
    in0Lo = ~in0Lo;
    in0Lo++;
    if (in0Lo == 0U) {
      absIn0++;
    }
  }

  *ptrOutBitsHi = absIn0;
  *ptrOutBitsLo = in0Lo;
}

int32_T mul_s32_s32_s32_sr17(int32_T a, int32_T b)
{
  uint32_T u32_chi;
  uint32_T u32_clo;
  mul_wide_s32(a, b, &u32_chi, &u32_clo);
  u32_clo = u32_chi << 15U | u32_clo >> 17U;
  return (int32_T)u32_clo;
}

int32_T mul_s32_s32_s32_sr30(int32_T a, int32_T b)
{
  uint32_T u32_chi;
  uint32_T u32_clo;
  mul_wide_s32(a, b, &u32_chi, &u32_clo);
  u32_clo = u32_chi << 2U | u32_clo >> 30U;
  return (int32_T)u32_clo;
}

int32_T div_repeat_s32_floor(int32_T numerator, int32_T denominator, uint32_T
  nRepeatSub)
{
  int32_T quotient;
  uint32_T absNumerator;
  uint32_T absDenominator;
  if (denominator == 0) {
    quotient = numerator >= 0 ? MAX_int32_T : MIN_int32_T;

    /* Divide by zero handler */
  } else {
    absNumerator = (uint32_T)(numerator >= 0 ? numerator : -numerator);
    absDenominator = (uint32_T)(denominator >= 0 ? denominator : -denominator);
    if ((numerator < 0) != (denominator < 0)) {
      quotient = -(int32_T)div_nzp_repeat_u32_ceiling(absNumerator,
        absDenominator, nRepeatSub);
    } else {
      quotient = (int32_T)div_nzp_repeat_u32(absNumerator, absDenominator,
        nRepeatSub);
    }
  }

  return quotient;
}

uint32_T div_nzp_repeat_u32_ceiling(uint32_T numerator, uint32_T denominator,
  uint32_T nRepeatSub)
{
  uint32_T quotient;
  uint32_T iRepeatSub;
  boolean_T numeratorExtraBit;
  quotient = numerator / denominator;
  numerator %= denominator;
  for (iRepeatSub = 0U; iRepeatSub < nRepeatSub; iRepeatSub++) {
    numeratorExtraBit = (numerator >= 2147483648U);
    numerator <<= 1U;
    quotient <<= 1U;
    if (numeratorExtraBit || (numerator >= denominator)) {
      quotient++;
      numerator -= denominator;
    }
  }

  if (numerator > 0U) {
    quotient++;
  }

  return quotient;
}

uint32_T div_nzp_repeat_u32(uint32_T numerator, uint32_T denominator, uint32_T
  nRepeatSub)
{
  uint32_T quotient;
  uint32_T iRepeatSub;
  boolean_T numeratorExtraBit;
  quotient = numerator / denominator;
  numerator %= denominator;
  for (iRepeatSub = 0U; iRepeatSub < nRepeatSub; iRepeatSub++) {
    numeratorExtraBit = (numerator >= 2147483648U);
    numerator <<= 1U;
    quotient <<= 1U;
    if (numeratorExtraBit || (numerator >= denominator)) {
      quotient++;
      numerator -= denominator;
    }
  }

  return quotient;
}

/* Function for MATLAB Function: '<S1>/parseBlob' */
static void cameraTest_eml_sort(const int32_T x_data[], const int32_T x_sizes,
  int32_T y_data[], int32_T *y_sizes, int32_T idx_data[], int32_T *idx_sizes)
{
  int32_T vstride;
  int32_T i1;
  int32_T ix;
  int32_T dim;
  int32_T vlen;
  int32_T j;
  int32_T d_k;
  int32_T i;
  int32_T i2;
  int32_T b_j;
  int32_T q;
  int32_T qEnd;
  int32_T kEnd;
  int32_T vwork_data[16];
  int32_T iidx_data[16];
  int32_T idx0_data[16];
  int32_T b_idx_0;
  dim = 2;
  if (x_sizes != 1) {
    dim = 1;
  }

  if (dim <= 1) {
    vlen = x_sizes - 1;
  } else {
    vlen = 0;
  }

  b_idx_0 = (int8_T)(vlen + 1);
  *y_sizes = x_sizes;
  *idx_sizes = (int8_T)x_sizes;
  vstride = 1;
  ix = 1;
  while (ix <= dim - 1) {
    vstride *= x_sizes;
    ix = 2;
  }

  i1 = -1;
  for (j = 1; j <= vstride; j++) {
    i1++;
    ix = i1;
    for (dim = 0; dim <= vlen; dim++) {
      vwork_data[dim] = x_data[ix];
      ix += vstride;
    }

    if (b_idx_0 == 0) {
      for (dim = 1; dim <= b_idx_0; dim++) {
        iidx_data[dim - 1] = dim;
      }
    } else {
      for (dim = 1; dim <= b_idx_0; dim++) {
        iidx_data[dim - 1] = dim;
      }

      for (dim = 1; dim <= b_idx_0 - 1; dim += 2) {
        if (!(vwork_data[dim - 1] <= vwork_data[dim])) {
          iidx_data[dim - 1] = dim + 1;
          iidx_data[dim] = dim;
        }
      }

      for (dim = 0; dim < b_idx_0; dim++) {
        idx0_data[dim] = 1;
      }

      i = 2;
      while (i < b_idx_0) {
        i2 = i << 1;
        b_j = 1;
        dim = 1 + i;
        while (dim < b_idx_0 + 1) {
          ix = b_j;
          q = dim;
          qEnd = b_j + i2;
          if (qEnd > b_idx_0 + 1) {
            qEnd = b_idx_0 + 1;
          }

          d_k = 0;
          kEnd = qEnd - b_j;
          while (d_k + 1 <= kEnd) {
            if (vwork_data[iidx_data[ix - 1] - 1] <= vwork_data[iidx_data[q - 1]
                - 1]) {
              idx0_data[d_k] = iidx_data[ix - 1];
              ix++;
              if (ix == dim) {
                while (q < qEnd) {
                  d_k++;
                  idx0_data[d_k] = iidx_data[q - 1];
                  q++;
                }
              }
            } else {
              idx0_data[d_k] = iidx_data[q - 1];
              q++;
              if (q == qEnd) {
                while (ix < dim) {
                  d_k++;
                  idx0_data[d_k] = iidx_data[ix - 1];
                  ix++;
                }
              }
            }

            d_k++;
          }

          for (dim = 0; dim + 1 <= kEnd; dim++) {
            iidx_data[(b_j + dim) - 1] = idx0_data[dim];
          }

          b_j = qEnd;
          dim = qEnd + i;
        }

        i = i2;
      }
    }

    ix = i1;
    for (dim = 0; dim <= vlen; dim++) {
      y_data[ix] = vwork_data[iidx_data[dim] - 1];
      idx_data[ix] = iidx_data[dim];
      ix += vstride;
    }
  }
}

real32_T rt_roundf_snf(real32_T u)
{
  real32_T y;
  if ((real32_T)fabs(u) < 8.388608E+6F) {
    if (u >= 0.5F) {
      y = (real32_T)floor(u + 0.5F);
    } else if (u > -0.5F) {
      y = u * 0.0F;
    } else {
      y = (real32_T)ceil(u - 0.5F);
    }
  } else {
    y = u;
  }

  return y;
}

/* Function for MATLAB Function: '<S1>/parseBlob' */
static void cameraTest_eml_li_find(const boolean_T x_data[], const int32_T
  x_sizes[2], int32_T y_data[], int32_T y_sizes[2])
{
  int32_T n;
  int32_T k;
  int32_T b_i;
  n = x_sizes[1];
  k = 0;
  for (b_i = 1; b_i <= n; b_i++) {
    if (x_data[b_i - 1]) {
      k++;
    }
  }

  y_sizes[0] = 1;
  y_sizes[1] = k;
  k = 0;
  for (b_i = 1; b_i <= n; b_i++) {
    if (x_data[b_i - 1]) {
      y_data[k] = b_i;
      k++;
    }
  }
}

/* Function for Chart: '<S1>/Chart' */
static uint8_T cameraTest_calcReady(void)
{
  uint8_T ready;
  int32_T ii;
  static const int8_T b[6] = { -10, -30, -70, 10, 30, 70 };

  boolean_T exitg1;
  boolean_T guard1;
  int8_T x;
  int32_T tmp;

  /* MATLAB Function 'calcReady': '<S3>:9' */
  /* '<S3>:9:2' */
  ready = 1U;

  /* '<S3>:9:3' */
  /* '<S3>:9:4' */
  ii = 0;
  exitg1 = false;
  while ((!exitg1) && (ii < 6)) {
    /* '<S3>:9:4' */
    tmp = cameraTest_B.players[ii].x - b[ii];
    if (tmp > 127) {
      tmp = 127;
    } else {
      if (tmp < -128) {
        tmp = -128;
      }
    }

    guard1 = false;
    if ((int8_T)tmp < 0) {
      tmp = -(int8_T)tmp;
      if (tmp > 127) {
        tmp = 127;
      }

      x = (int8_T)tmp;
    } else {
      x = (int8_T)tmp;
    }

    if (x > 3) {
      /* '<S3>:9:5' */
      if (cameraTest_B.players[ii].y < 0) {
        tmp = -cameraTest_B.players[ii].y;
        if (tmp > 127) {
          tmp = 127;
        }

        x = (int8_T)tmp;
      } else {
        x = cameraTest_B.players[ii].y;
      }

      if (x > 3) {
        /* '<S3>:9:5' */
        /* '<S3>:9:6' */
        ready = 0U;
        exitg1 = true;
      } else {
        guard1 = true;
      }
    } else {
      guard1 = true;
    }

    if (guard1) {
      /* '<S3>:9:4' */
      ii++;
    }
  }

  return ready;
}

/* Function for Chart: '<S1>/Chart' */
static uint8_T cameraTest_calcGoal(void)
{
  uint8_T goal;
  int8_T tmp;
  int32_T tmp_0;

  /* MATLAB Function 'calcGoal': '<S3>:6' */
  /* '<S3>:6:2' */
  goal = 0U;
  if (cameraTest_B.ball.x < 0) {
    tmp_0 = -cameraTest_B.ball.x;
    if (tmp_0 > 127) {
      tmp_0 = 127;
    }

    tmp = (int8_T)tmp_0;
  } else {
    tmp = cameraTest_B.ball.x;
  }

  if (tmp > 75) {
    /* '<S3>:6:3' */
    /* '<S3>:6:4' */
    goal = 1U;
  }

  return goal;
}

real32_T rt_atan2f_snf(real32_T u0, real32_T u1)
{
  real32_T y;
  int32_T u0_0;
  int32_T u1_0;
  if (rtIsNaNF(u0) || rtIsNaNF(u1)) {
    y = (rtNaNF);
  } else if (rtIsInfF(u0) && rtIsInfF(u1)) {
    if (u0 > 0.0F) {
      u0_0 = 1;
    } else {
      u0_0 = -1;
    }

    if (u1 > 0.0F) {
      u1_0 = 1;
    } else {
      u1_0 = -1;
    }

    y = (real32_T)atan2((real32_T)u0_0, (real32_T)u1_0);
  } else if (u1 == 0.0F) {
    if (u0 > 0.0F) {
      y = RT_PIF / 2.0F;
    } else if (u0 < 0.0F) {
      y = -(RT_PIF / 2.0F);
    } else {
      y = 0.0F;
    }
  } else {
    y = (real32_T)atan2(u0, u1);
  }

  return y;
}

/* Function for MATLAB Function: '<S1>/parseBlob' */
static real32_T cameraTest_norm(const real32_T x[2])
{
  real32_T y;
  real32_T scale;
  real32_T absxk;
  real32_T t;
  scale = 1.17549435E-38F;
  absxk = (real32_T)fabs(x[0]);
  if (absxk > 1.17549435E-38F) {
    y = 1.0F;
    scale = absxk;
  } else {
    t = absxk / 1.17549435E-38F;
    y = t * t;
  }

  absxk = (real32_T)fabs(x[1]);
  if (absxk > scale) {
    t = scale / absxk;
    y = y * t * t + 1.0F;
    scale = absxk;
  } else {
    t = absxk / scale;
    y += t * t;
  }

  return scale * (real32_T)sqrt(y);
}

real32_T rt_remf_snf(real32_T u0, real32_T u1)
{
  real32_T y;
  real32_T u1_0;
  if (!((!rtIsNaNF(u0)) && (!rtIsInfF(u0)) && ((!rtIsNaNF(u1)) && (!rtIsInfF(u1)))))
  {
    y = (rtNaNF);
  } else {
    if (u1 < 0.0F) {
      u1_0 = (real32_T)ceil(u1);
    } else {
      u1_0 = (real32_T)floor(u1);
    }

    if ((u1 != 0.0F) && (u1 != u1_0)) {
      u1_0 = u0 / u1;
      if ((real32_T)fabs(u1_0 - rt_roundf_snf(u1_0)) <= FLT_EPSILON * (real32_T)
          fabs(u1_0)) {
        y = 0.0F;
      } else {
        y = (real32_T)fmod(u0, u1);
      }
    } else {
      y = (real32_T)fmod(u0, u1);
    }
  }

  return y;
}

real_T rt_roundd_snf(real_T u)
{
  real_T y;
  if (fabs(u) < 4.503599627370496E+15) {
    if (u >= 0.5) {
      y = floor(u + 0.5);
    } else if (u > -0.5) {
      y = u * 0.0;
    } else {
      y = ceil(u - 0.5);
    }
  } else {
    y = u;
  }

  return y;
}

/* Model output function */
void cameraTest_output(void)
{
  boolean_T colorVec[3];
  boolean_T guard1;
  int32_T lenRed;
  real32_T dist;
  real32_T scale;
  real32_T t;
  int8_T x;
  uint8_T e_y[2];
  int32_T i;
  int32_T Autothreshold_HIST_FIXPT_DW[256];
  int16_T int16Obj;
  int32_T maxVal;
  uint8_T threshold;
  boolean_T maxNumBlobsReached;
  int32_T idx;
  int32_T n;
  uint32_T stackIdx;
  uint32_T pixIdx;
  uint32_T BlobAnalysis_NUM_PIX_DW[16];
  uint32_T walkerIdx;
  uint32_T numBlobs;
  int32_T pixListNinc;
  int32_T Autothreshold_NORMF_FIXPT_DW;
  int32_T Autothreshold_P_FIXPT_DW[256];
  int32_T Autothreshold_MU_FIXPT_DW[256];
  int32_T Autothreshold1_MU_FIXPT_DW[256];
  int32_T Autothreshold2_MU_FIXPT_DW[256];
  real32_T rtb_BlobAnalysis_o2[32];
  BlobData rtb_blobs;
  int8_T rtb_players_x[6];
  int8_T rtb_players_y[6];
  int16_T rtb_players_orientation[6];
  uint8_T rtb_players_color[6];
  real32_T blues[2];
  real32_T greens[2];
  int32_T i_0;
  int32_T loop_ub;
  real32_T tmp[2];
  real32_T centroidsInt_data[32];
  int8_T color_data[3];
  int8_T b_ii_data[3];
  BlobData_size blobs_elems_sizes;
  real32_T allLights_data[93];
  real32_T reds_data[62];
  real32_T greens_data[62];
  real32_T blues_data[62];
  int32_T blobIndices_data[16];
  int8_T f_data[7];
  int16_T g_data[7];
  uint8_T h_data[7];
  int32_T c_y_data[16];
  int32_T iidx_data[16];
  real32_T tmp_data[32];
  uint8_T tmp_data_0[16];
  int32_T allLights_sizes[2];
  int32_T allLights_sizes_0[2];
  int32_T tmp_sizes[2];
  int32_T allLights_sizes_1[2];
  int32_T allLights_sizes_2[2];
  int32_T allLights_sizes_3[2];
  int32_T allLights_sizes_4[2];
  boolean_T allLights_data_0[31];
  int32_T allLights_sizes_5[2];
  int32_T tmp_data_1[31];
  int32_T tmp_sizes_0[2];
  real32_T allLights_data_1[93];
  real32_T blobs_data[2];
  real32_T s;
  real32_T s_idx_0;
  int8_T rtb_Ball_y_idx_0;
  int8_T rtb_Ball_x_idx_0;

  /* S-Function (v4l2_video_capture_sfcn): '<Root>/V4L2 Video Capture' */
  MW_videoCaptureOutput(cameraTest_ConstP.V4L2VideoCapture_p1,
                        cameraTest_B.V4L2VideoCapture_o1,
                        cameraTest_B.V4L2VideoCapture_o2,
                        cameraTest_B.V4L2VideoCapture_o3);

  /* MATLAB Function: '<S2>/MATLAB Function' */
  /* MATLAB Function 'Raspi Code/Blob extraction /MATLAB Function': '<S10>:1' */
  /*  R=Image(:,:,1); */
  /*  G=Image(:,:,2); */
  /*  B=Image(:,:,3); */
  /* '<S10>:1:5' */
  for (pixListNinc = 0; pixListNinc < 76800; pixListNinc++) {
    i_0 = (int32_T)rt_roundd_snf((real_T)
      cameraTest_B.V4L2VideoCapture_o1[pixListNinc] / 2.0);
    cameraTest_B.BI[pixListNinc] = (uint8_T)i_0;
  }

  /* '<S10>:1:6' */
  for (Autothreshold_NORMF_FIXPT_DW = 0; Autothreshold_NORMF_FIXPT_DW < 76800;
       Autothreshold_NORMF_FIXPT_DW++) {
    i_0 = (int32_T)rt_roundd_snf((real_T)
      cameraTest_B.V4L2VideoCapture_o2[Autothreshold_NORMF_FIXPT_DW] / 2.0);
    cameraTest_B.hG[Autothreshold_NORMF_FIXPT_DW] = (uint8_T)i_0;
  }

  /* '<S10>:1:7' */
  for (Autothreshold_NORMF_FIXPT_DW = 0; Autothreshold_NORMF_FIXPT_DW < 76800;
       Autothreshold_NORMF_FIXPT_DW++) {
    i_0 = (int32_T)rt_roundd_snf((real_T)
      cameraTest_B.V4L2VideoCapture_o3[Autothreshold_NORMF_FIXPT_DW] / 2.0);
    cameraTest_B.GI[Autothreshold_NORMF_FIXPT_DW] = (uint8_T)i_0;
  }

  /* '<S10>:1:8' */
  for (i_0 = 0; i_0 < 76800; i_0++) {
    n = cameraTest_B.V4L2VideoCapture_o1[i_0];
    pixIdx = (uint32_T)n - cameraTest_B.hG[i_0];
    if (pixIdx > (uint32_T)n) {
      pixIdx = 0U;
    }

    numBlobs = pixIdx - cameraTest_B.GI[i_0];
    if (numBlobs > pixIdx) {
      numBlobs = 0U;
    }

    cameraTest_B.RI[i_0] = (uint8_T)numBlobs;
  }

  /* '<S10>:1:9' */
  for (i_0 = 0; i_0 < 76800; i_0++) {
    n = cameraTest_B.V4L2VideoCapture_o2[i_0];
    pixIdx = (uint32_T)n - cameraTest_B.BI[i_0];
    if (pixIdx > (uint32_T)n) {
      pixIdx = 0U;
    }

    numBlobs = pixIdx - cameraTest_B.GI[i_0];
    if (numBlobs > pixIdx) {
      numBlobs = 0U;
    }

    cameraTest_B.GI[i_0] = (uint8_T)numBlobs;
  }

  /* '<S10>:1:10' */
  for (i_0 = 0; i_0 < 76800; i_0++) {
    n = cameraTest_B.V4L2VideoCapture_o3[i_0];
    pixIdx = (uint32_T)n - cameraTest_B.BI[i_0];
    if (pixIdx > (uint32_T)n) {
      pixIdx = 0U;
    }

    numBlobs = pixIdx - cameraTest_B.hG[i_0];
    if (numBlobs > pixIdx) {
      numBlobs = 0U;
    }

    cameraTest_B.BI[i_0] = (uint8_T)numBlobs;
  }

  /* End of MATLAB Function: '<S2>/MATLAB Function' */

  /* S-Function (svipgraythresh): '<S2>/Autothreshold' */
  memset(&Autothreshold_HIST_FIXPT_DW[0], 0, sizeof(int32_T) << 8U);
  for (i = 0; i < 76800; i++) {
    Autothreshold_HIST_FIXPT_DW[cameraTest_B.RI[i]]++;
  }

  for (i = 0; i < 256; i++) {
    Autothreshold_P_FIXPT_DW[i] = mul_s32_s32_s32_sr17(1832519379,
      Autothreshold_HIST_FIXPT_DW[i]);
  }

  Autothreshold_MU_FIXPT_DW[0] = Autothreshold_P_FIXPT_DW[0] >> 8;
  for (i = 0; i < 255; i++) {
    Autothreshold_MU_FIXPT_DW[i + 1] = (i + 2) << 22;
    Autothreshold_MU_FIXPT_DW[i + 1] = mul_s32_s32_s32_sr30
      (Autothreshold_MU_FIXPT_DW[i + 1], Autothreshold_P_FIXPT_DW[i + 1]);
    Autothreshold_MU_FIXPT_DW[i + 1] += Autothreshold_MU_FIXPT_DW[i];
  }

  for (i = 0; i < 254; i++) {
    Autothreshold_P_FIXPT_DW[i + 1] += Autothreshold_P_FIXPT_DW[i];
  }

  Autothreshold_NORMF_FIXPT_DW = 0;
  maxVal = 0;
  for (i = 0; i < 255; i++) {
    n = mul_s32_s32_s32_sr30(Autothreshold_P_FIXPT_DW[i],
      Autothreshold_MU_FIXPT_DW[255]) - Autothreshold_MU_FIXPT_DW[i];
    idx = mul_s32_s32_s32_sr30(Autothreshold_P_FIXPT_DW[i], 1073741824 -
      Autothreshold_P_FIXPT_DW[i]);
    if (idx == 0) {
      n = 0;
    } else {
      n = div_repeat_s32_floor(mul_s32_s32_s32_sr30(n, n) << 2, idx, 30U);
    }

    if (n > maxVal) {
      maxVal = n;
      Autothreshold_NORMF_FIXPT_DW = i;
    }
  }

  i_0 = (int32_T)mul_u32_u32_u32_sr15(255U, (uint32_T)(4210752 *
    Autothreshold_NORMF_FIXPT_DW));
  threshold = (uint8_T)(((i_0 & 16384) != 0) + (i_0 >> 15));
  cameraTest_B.Autothreshold_o2 = threshold;
  for (i = 0; i < 76800; i++) {
    cameraTest_B.Autothreshold_o1[i] = (cameraTest_B.RI[i] > threshold);
  }

  /* End of S-Function (svipgraythresh): '<S2>/Autothreshold' */

  /* S-Function (svipgraythresh): '<S2>/Autothreshold1' */
  memset(&Autothreshold_HIST_FIXPT_DW[0], 0, sizeof(int32_T) << 8U);
  for (i = 0; i < 76800; i++) {
    Autothreshold_HIST_FIXPT_DW[cameraTest_B.GI[i]]++;
  }

  for (i = 0; i < 256; i++) {
    Autothreshold_P_FIXPT_DW[i] = mul_s32_s32_s32_sr17(1832519379,
      Autothreshold_HIST_FIXPT_DW[i]);
  }

  Autothreshold1_MU_FIXPT_DW[0] = Autothreshold_P_FIXPT_DW[0] >> 8;
  for (i = 0; i < 255; i++) {
    Autothreshold1_MU_FIXPT_DW[i + 1] = (i + 2) << 22;
    Autothreshold1_MU_FIXPT_DW[i + 1] = mul_s32_s32_s32_sr30
      (Autothreshold1_MU_FIXPT_DW[i + 1], Autothreshold_P_FIXPT_DW[i + 1]);
    Autothreshold1_MU_FIXPT_DW[i + 1] += Autothreshold1_MU_FIXPT_DW[i];
  }

  for (i = 0; i < 254; i++) {
    Autothreshold_P_FIXPT_DW[i + 1] += Autothreshold_P_FIXPT_DW[i];
  }

  Autothreshold_NORMF_FIXPT_DW = 0;
  maxVal = 0;
  for (i = 0; i < 255; i++) {
    n = mul_s32_s32_s32_sr30(Autothreshold_P_FIXPT_DW[i],
      Autothreshold1_MU_FIXPT_DW[255]) - Autothreshold1_MU_FIXPT_DW[i];
    idx = mul_s32_s32_s32_sr30(Autothreshold_P_FIXPT_DW[i], 1073741824 -
      Autothreshold_P_FIXPT_DW[i]);
    if (idx == 0) {
      n = 0;
    } else {
      n = div_repeat_s32_floor(mul_s32_s32_s32_sr30(n, n) << 2, idx, 30U);
    }

    if (n > maxVal) {
      maxVal = n;
      Autothreshold_NORMF_FIXPT_DW = i;
    }
  }

  i_0 = (int32_T)mul_u32_u32_u32_sr15(255U, (uint32_T)(4210752 *
    Autothreshold_NORMF_FIXPT_DW));
  threshold = (uint8_T)(((i_0 & 16384) != 0) + (i_0 >> 15));
  cameraTest_B.Autothreshold1_o2 = threshold;
  for (i = 0; i < 76800; i++) {
    cameraTest_B.Autothreshold1_o1[i] = (cameraTest_B.GI[i] > threshold);
  }

  /* End of S-Function (svipgraythresh): '<S2>/Autothreshold1' */

  /* S-Function (svipgraythresh): '<S2>/Autothreshold2' */
  memset(&Autothreshold_HIST_FIXPT_DW[0], 0, sizeof(int32_T) << 8U);
  for (i = 0; i < 76800; i++) {
    Autothreshold_HIST_FIXPT_DW[cameraTest_B.BI[i]]++;
  }

  for (i = 0; i < 256; i++) {
    Autothreshold_P_FIXPT_DW[i] = mul_s32_s32_s32_sr17(1832519379,
      Autothreshold_HIST_FIXPT_DW[i]);
  }

  Autothreshold2_MU_FIXPT_DW[0] = Autothreshold_P_FIXPT_DW[0] >> 8;
  for (i = 0; i < 255; i++) {
    Autothreshold2_MU_FIXPT_DW[i + 1] = (i + 2) << 22;
    Autothreshold2_MU_FIXPT_DW[i + 1] = mul_s32_s32_s32_sr30
      (Autothreshold2_MU_FIXPT_DW[i + 1], Autothreshold_P_FIXPT_DW[i + 1]);
    Autothreshold2_MU_FIXPT_DW[i + 1] += Autothreshold2_MU_FIXPT_DW[i];
  }

  for (i = 0; i < 254; i++) {
    Autothreshold_P_FIXPT_DW[i + 1] += Autothreshold_P_FIXPT_DW[i];
  }

  Autothreshold_NORMF_FIXPT_DW = 0;
  maxVal = 0;
  for (i = 0; i < 255; i++) {
    n = mul_s32_s32_s32_sr30(Autothreshold_P_FIXPT_DW[i],
      Autothreshold2_MU_FIXPT_DW[255]) - Autothreshold2_MU_FIXPT_DW[i];
    idx = mul_s32_s32_s32_sr30(Autothreshold_P_FIXPT_DW[i], 1073741824 -
      Autothreshold_P_FIXPT_DW[i]);
    if (idx == 0) {
      n = 0;
    } else {
      n = div_repeat_s32_floor(mul_s32_s32_s32_sr30(n, n) << 2, idx, 30U);
    }

    if (n > maxVal) {
      maxVal = n;
      Autothreshold_NORMF_FIXPT_DW = i;
    }
  }

  i_0 = (int32_T)mul_u32_u32_u32_sr15(255U, (uint32_T)(4210752 *
    Autothreshold_NORMF_FIXPT_DW));
  threshold = (uint8_T)(((i_0 & 16384) != 0) + (i_0 >> 15));
  cameraTest_B.Autothreshold2_o2 = threshold;
  for (i = 0; i < 76800; i++) {
    /* S-Function (svipgraythresh): '<S2>/Autothreshold2' */
    cameraTest_B.Autothreshold2_o1[i] = (cameraTest_B.BI[i] > threshold);

    /* RelationalOperator: '<S7>/Compare' incorporates:
     *  Constant: '<S7>/Constant'
     *  Sum: '<S2>/Add'
     */
    cameraTest_B.Compare[i] = ((int32_T)(((uint32_T)
      cameraTest_B.Autothreshold_o1[i] + cameraTest_B.Autothreshold1_o1[i]) +
      cameraTest_B.Autothreshold2_o1[i]) > cameraTest_P.Constant_Value);
  }

  /* S-Function (sdl_video_display_sfcn): '<S2>/SDL Video Display' */
  memcpy(&cameraTest_B.BI[0], &cameraTest_B.V4L2VideoCapture_o1[0], 76800U *
         sizeof(uint8_T));
  memcpy(&cameraTest_B.hG[0], &cameraTest_B.V4L2VideoCapture_o2[0], 76800U *
         sizeof(uint8_T));
  memcpy(&cameraTest_B.GI[0], &cameraTest_B.V4L2VideoCapture_o3[0], 76800U *
         sizeof(uint8_T));
  MW_SDL_videoDisplayOutput(cameraTest_B.BI, cameraTest_B.hG, cameraTest_B.GI);

  /* S-Function (svipblob): '<S2>/Blob Analysis' */
  maxNumBlobsReached = false;
  memset(&cameraTest_DW.BlobAnalysis_PAD_DW[0], 0, 323U * sizeof(uint8_T));
  threshold = 1U;
  i = 0;
  idx = 323;
  for (n = 0; n < 240; n++) {
    for (lenRed = 0; lenRed < 320; lenRed++) {
      cameraTest_DW.BlobAnalysis_PAD_DW[idx] = (uint8_T)(cameraTest_B.Compare[i]
        ? 255 : 0);
      i++;
      idx++;
    }

    cameraTest_DW.BlobAnalysis_PAD_DW[idx] = 0U;
    cameraTest_DW.BlobAnalysis_PAD_DW[idx + 1] = 0U;
    idx += 2;
  }

  memset(&cameraTest_DW.BlobAnalysis_PAD_DW[idx], 0, 321U * sizeof(uint8_T));
  Autothreshold_NORMF_FIXPT_DW = 0;
  pixIdx = 0U;
  n = 0;
  while (n < 240) {
    pixListNinc = 0;
    idx = (Autothreshold_NORMF_FIXPT_DW + 1) * 322;
    lenRed = 0;
    while (lenRed < 320) {
      numBlobs = (uint32_T)((idx + pixListNinc) + 1);
      if (cameraTest_DW.BlobAnalysis_PAD_DW[numBlobs] == 255) {
        cameraTest_DW.BlobAnalysis_PAD_DW[numBlobs] = threshold;
        cameraTest_DW.BlobAnalysis_N_PIXLIST_DW[pixIdx] = (int16_T)
          Autothreshold_NORMF_FIXPT_DW;
        cameraTest_DW.BlobAnalysis_M_PIXLIST_DW[pixIdx] = (int16_T)pixListNinc;
        pixIdx++;
        BlobAnalysis_NUM_PIX_DW[threshold - 1] = 1U;
        cameraTest_DW.BlobAnalysis_STACK_DW[0U] = numBlobs;
        stackIdx = 1U;
        while (stackIdx != 0U) {
          stackIdx--;
          numBlobs = cameraTest_DW.BlobAnalysis_STACK_DW[stackIdx];
          for (i = 0; i < 8; i++) {
            walkerIdx = numBlobs + cameraTest_ConstP.BlobAnalysis_WALKER_R[i];
            if (cameraTest_DW.BlobAnalysis_PAD_DW[walkerIdx] == 255) {
              cameraTest_DW.BlobAnalysis_PAD_DW[walkerIdx] = threshold;
              cameraTest_DW.BlobAnalysis_N_PIXLIST_DW[pixIdx] = (int16_T)
                ((int16_T)(walkerIdx / 322U) - 1);
              cameraTest_DW.BlobAnalysis_M_PIXLIST_DW[pixIdx] = (int16_T)
                (walkerIdx % 322U - 1U);
              pixIdx++;
              BlobAnalysis_NUM_PIX_DW[threshold - 1]++;
              cameraTest_DW.BlobAnalysis_STACK_DW[stackIdx] = walkerIdx;
              stackIdx++;
            }
          }
        }

        if (threshold == 16) {
          maxNumBlobsReached = true;
          n = 240;
          lenRed = 320;
        }

        if (lenRed < 320) {
          threshold++;
        }
      }

      pixListNinc++;
      lenRed++;
    }

    Autothreshold_NORMF_FIXPT_DW++;
    n++;
  }

  numBlobs = (uint32_T)(maxNumBlobsReached ? (int32_T)threshold : (int32_T)
                        (uint8_T)(threshold - 1U));
  Autothreshold_NORMF_FIXPT_DW = 0;
  pixListNinc = 0;
  for (i = 0; i < (int32_T)numBlobs; i++) {
    cameraTest_B.BlobAnalysis_o1[i] = (int32_T)BlobAnalysis_NUM_PIX_DW[i];
    idx = 0;
    lenRed = 0;
    for (n = 0; n < (int32_T)BlobAnalysis_NUM_PIX_DW[i]; n++) {
      idx += cameraTest_DW.BlobAnalysis_N_PIXLIST_DW[n + pixListNinc];
      lenRed += cameraTest_DW.BlobAnalysis_M_PIXLIST_DW[n +
        Autothreshold_NORMF_FIXPT_DW];
    }

    rtb_BlobAnalysis_o2[i] = (real32_T)idx / (real32_T)BlobAnalysis_NUM_PIX_DW[i]
      + 1.0F;
    rtb_BlobAnalysis_o2[numBlobs + i] = (real32_T)lenRed / (real32_T)
      BlobAnalysis_NUM_PIX_DW[i] + 1.0F;
    Autothreshold_NORMF_FIXPT_DW += (int32_T)BlobAnalysis_NUM_PIX_DW[i];
    pixListNinc += (int32_T)BlobAnalysis_NUM_PIX_DW[i];
  }

  cameraTest_DW.BlobAnalysis_DIMS1[0] = (int32_T)numBlobs;
  cameraTest_DW.BlobAnalysis_DIMS1[1] = 1;
  cameraTest_DW.BlobAnalysis_DIMS2[0] = (int32_T)numBlobs;
  cameraTest_DW.BlobAnalysis_DIMS2[1] = 2;

  /* End of S-Function (svipblob): '<S2>/Blob Analysis' */

  /* MATLAB Function: '<S2>/FindBlobs1' */
  /* MATLAB Function 'Raspi Code/Blob extraction /FindBlobs1': '<S9>:1' */
  /*  area */
  /* '<S9>:1:4' */
  cameraTest_B.lengthh = cameraTest_DW.BlobAnalysis_DIMS1[0];

  /* MATLAB Function: '<S2>/FindBlobs' */
  /* MATLAB Function 'Raspi Code/Blob extraction /FindBlobs': '<S8>:1' */
  /*  area */
  /* '<S8>:1:4' */
  /* '<S8>:1:5' */
  i = cameraTest_DW.BlobAnalysis_DIMS2[0];
  loop_ub = cameraTest_DW.BlobAnalysis_DIMS2[0] *
    cameraTest_DW.BlobAnalysis_DIMS2[1];
  for (i_0 = 0; i_0 < loop_ub; i_0++) {
    centroidsInt_data[i_0] = rtb_BlobAnalysis_o2[i_0];
  }

  Autothreshold_NORMF_FIXPT_DW = cameraTest_DW.BlobAnalysis_DIMS2[0] *
    cameraTest_DW.BlobAnalysis_DIMS2[1];
  for (pixListNinc = 0; pixListNinc < Autothreshold_NORMF_FIXPT_DW; pixListNinc
       ++) {
    centroidsInt_data[pixListNinc] = (real32_T)floor
      (centroidsInt_data[pixListNinc]);
  }

  /* '<S8>:1:6' */
  cameraTest_DW.SFunction_DIMS2_a.centroid[0] = 0;
  cameraTest_DW.SFunction_DIMS2_a.centroid[1] = 2;

  /* '<S8>:1:7' */
  cameraTest_DW.SFunction_DIMS2_a.area = 0;

  /* '<S8>:1:8' */
  cameraTest_DW.SFunction_DIMS2_a.color = 0;

  /*  check for color inconsistencies */
  /* '<S8>:1:13' */
  for (lenRed = 0; lenRed < cameraTest_DW.BlobAnalysis_DIMS1[0]; lenRed++) {
    /* '<S8>:1:13' */
    /* '<S8>:1:14' */
    /* '<S8>:1:15' */
    /* '<S8>:1:16' */
    /* '<S8>:1:17' */
    colorVec[0] = cameraTest_B.Autothreshold_o1[(((int32_T)
      centroidsInt_data[lenRed] - 1) * 320 + (int32_T)centroidsInt_data[lenRed +
      i]) - 1];
    colorVec[1] = cameraTest_B.Autothreshold1_o1[(((int32_T)
      centroidsInt_data[lenRed] - 1) * 320 + (int32_T)centroidsInt_data[lenRed +
      i]) - 1];
    colorVec[2] = cameraTest_B.Autothreshold2_o1[(((int32_T)
      centroidsInt_data[lenRed] - 1) * 320 + (int32_T)centroidsInt_data[lenRed +
      i]) - 1];

    /* '<S8>:1:18' */
    idx = 0;
    pixListNinc = 1;
    maxNumBlobsReached = false;
    while ((!maxNumBlobsReached) && (pixListNinc < 4)) {
      guard1 = false;
      if (colorVec[pixListNinc - 1]) {
        idx++;
        b_ii_data[idx - 1] = (int8_T)pixListNinc;
        if (idx >= 3) {
          maxNumBlobsReached = true;
        } else {
          guard1 = true;
        }
      } else {
        guard1 = true;
      }

      if (guard1) {
        pixListNinc++;
      }
    }

    if (1 > idx) {
      idx = 0;
    }

    for (i_0 = 0; i_0 < idx; i_0++) {
      color_data[i_0] = b_ii_data[i_0];
    }

    if (idx == 1) {
      /* '<S8>:1:19' */
      /* '<S8>:1:20' */
      idx = cameraTest_DW.SFunction_DIMS2_a.centroid[0];
      maxVal = cameraTest_DW.SFunction_DIMS2_a.centroid[1];
      loop_ub = cameraTest_DW.BlobAnalysis_DIMS2[1] - 1;
      n = cameraTest_DW.SFunction_DIMS2_a.centroid[0] + 1;
      for (i_0 = 0; i_0 < maxVal; i_0++) {
        for (pixListNinc = 0; pixListNinc < idx; pixListNinc++) {
          tmp_data[pixListNinc + n * i_0] = rtb_blobs.centroid[idx * i_0 +
            pixListNinc];
        }
      }

      for (i_0 = 0; i_0 <= loop_ub; i_0++) {
        tmp_data[idx + n * i_0] =
          rtb_BlobAnalysis_o2[cameraTest_DW.BlobAnalysis_DIMS2[0] * i_0 + lenRed];
      }

      cameraTest_DW.SFunction_DIMS2_a.centroid[0] = n;
      cameraTest_DW.SFunction_DIMS2_a.centroid[1] = maxVal;
      loop_ub = n * maxVal;
      for (i_0 = 0; i_0 < loop_ub; i_0++) {
        rtb_blobs.centroid[i_0] = tmp_data[i_0];
      }

      /* '<S8>:1:21' */
      idx = cameraTest_DW.SFunction_DIMS2_a.area + 1;
      loop_ub = cameraTest_DW.SFunction_DIMS2_a.area;
      for (i_0 = 0; i_0 < loop_ub; i_0++) {
        c_y_data[i_0] = rtb_blobs.area[i_0];
      }

      c_y_data[cameraTest_DW.SFunction_DIMS2_a.area] =
        cameraTest_B.BlobAnalysis_o1[lenRed];
      cameraTest_DW.SFunction_DIMS2_a.area = idx;
      for (i_0 = 0; i_0 < idx; i_0++) {
        rtb_blobs.area[i_0] = c_y_data[i_0];
      }

      /* '<S8>:1:22' */
      n = cameraTest_DW.SFunction_DIMS2_a.color + 1;
      loop_ub = cameraTest_DW.SFunction_DIMS2_a.color;
      for (i_0 = 0; i_0 < loop_ub; i_0++) {
        tmp_data_0[i_0] = rtb_blobs.color[i_0];
      }

      tmp_data_0[cameraTest_DW.SFunction_DIMS2_a.color] = (uint8_T)(int32_T)
        rt_roundd_snf((real_T)color_data[0]);
      cameraTest_DW.SFunction_DIMS2_a.color = n;
      for (i_0 = 0; i_0 < n; i_0++) {
        rtb_blobs.color[i_0] = tmp_data_0[i_0];
      }
    }

    /* '<S8>:1:13' */
  }

  /* End of MATLAB Function: '<S2>/FindBlobs' */

  /* MATLAB Function: '<S1>/parseBlob' incorporates:
   *  Constant: '<S1>/Constant6'
   *  Constant: '<S1>/Constant7'
   */
  blobs_elems_sizes = cameraTest_DW.SFunction_DIMS2_a;

  /* MATLAB Function 'Raspi Code/parseBlob': '<S6>:1' */
  cameraTest_eml_sort(rtb_blobs.area, blobs_elems_sizes.area, c_y_data, &idx,
                      iidx_data, &n);
  maxVal = n - 1;
  for (i_0 = 0; i_0 < n; i_0++) {
    blobIndices_data[i_0] = iidx_data[i_0];
  }

  /*  blobs.area */
  /*  blobIndices */
  /*  blobs.centroid(:,1) */
  /* '<S6>:1:6' */
  /* '<S6>:1:7' */
  cameraTest_DW.SFunction_DIMS3.x = 0;

  /* '<S6>:1:8' */
  cameraTest_DW.SFunction_DIMS3.y = 0;

  /* % Coefficients from curve fitting */
  /* % calc position */
  /* '<S6>:1:16' */
  idx = maxVal + 1;
  loop_ub = (maxVal + 1) * 3;
  for (i_0 = 0; i_0 < loop_ub; i_0++) {
    allLights_data[i_0] = 0.0F;
  }

  /*  blobIndices */
  if (!(maxVal + 1 == 0)) {
    /* '<S6>:1:18' */
    /* '<S6>:1:19' */
    for (lenRed = 0; lenRed <= maxVal; lenRed++) {
      /* '<S6>:1:19' */
      /* '<S6>:1:20' */
      loop_ub = blobs_elems_sizes.centroid[1];
      for (i_0 = 0; i_0 < loop_ub; i_0++) {
        tmp_sizes[i_0] = i_0;
      }

      /* '<S6>:1:21' */
      tmp[0] = (real32_T)cameraTest_P.Constant6_Value[1] -
        rtb_blobs.centroid[blobIndices_data[lenRed] - 1];
      tmp[1] = rtb_blobs.centroid[(blobIndices_data[lenRed] +
        blobs_elems_sizes.centroid[0]) - 1];
      for (i_0 = 0; i_0 < loop_ub; i_0++) {
        rtb_blobs.centroid[(blobIndices_data[lenRed] +
                            blobs_elems_sizes.centroid[0] * tmp_sizes[i_0]) - 1]
          = tmp[i_0];
      }

      /* '<S6>:1:22' */
      loop_ub = blobs_elems_sizes.centroid[1] - 1;
      for (i_0 = 0; i_0 <= loop_ub; i_0++) {
        blobs_data[i_0] = -rtb_blobs.centroid[(blobs_elems_sizes.centroid[0] *
          i_0 + blobIndices_data[lenRed]) - 1];
      }

      t = (real32_T)(cameraTest_P.Constant6_Value[1] / 2.0) + blobs_data[0];
      scale = (real32_T)(cameraTest_P.Constant6_Value[0] / 2.0) + blobs_data[1];

      /* '<S6>:1:23' */
      if (t < 0.0F) {
        s_idx_0 = -1.0F;
      } else if (t > 0.0F) {
        s_idx_0 = 1.0F;
      } else if (t == 0.0F) {
        s_idx_0 = 0.0F;
      } else {
        s_idx_0 = t;
      }

      if (scale < 0.0F) {
        s = -1.0F;
      } else if (scale > 0.0F) {
        s = 1.0F;
      } else if (scale == 0.0F) {
        s = 0.0F;
      } else {
        s = scale;
      }

      /* '<S6>:1:24' */
      t = (real32_T)fabs(t);
      scale = (real32_T)fabs(scale);

      /* '<S6>:1:25' */
      /* '<S6>:1:26' */
      /* '<S6>:1:27' */
      s_idx_0 *= (t * t * -0.0002273F + 0.2549F * t) + -0.04137F;
      s *= (scale * scale * -0.0002273F + 0.2549F * scale) + -0.04137F;
      if (!((!rtIsInfF(s_idx_0)) && (!rtIsNaNF(s_idx_0)))) {
        dist = (rtNaNF);
      } else {
        dist = rt_remf_snf(s_idx_0, 360.0F);
        scale = (real32_T)fabs(dist);
        if (scale > 180.0F) {
          if (dist > 0.0F) {
            dist -= 360.0F;
          } else {
            dist += 360.0F;
          }

          scale = (real32_T)fabs(dist);
        }

        if (scale <= 45.0F) {
          dist *= 0.0174532924F;
          x = 0;
        } else if (scale <= 135.0F) {
          if (dist > 0.0F) {
            dist = (dist - 90.0F) * 0.0174532924F;
            x = 1;
          } else {
            dist = (dist + 90.0F) * 0.0174532924F;
            x = -1;
          }
        } else if (dist > 0.0F) {
          dist = (dist - 180.0F) * 0.0174532924F;
          x = 2;
        } else {
          dist = (dist + 180.0F) * 0.0174532924F;
          x = -2;
        }

        dist = (real32_T)tan(dist);
        if ((x == 1) || (x == -1)) {
          dist = -1.0F / dist;
        }
      }

      s_idx_0 = dist;
      if (!((!rtIsInfF(s)) && (!rtIsNaNF(s)))) {
        dist = (rtNaNF);
      } else {
        dist = rt_remf_snf(s, 360.0F);
        scale = (real32_T)fabs(dist);
        if (scale > 180.0F) {
          if (dist > 0.0F) {
            dist -= 360.0F;
          } else {
            dist += 360.0F;
          }

          scale = (real32_T)fabs(dist);
        }

        if (scale <= 45.0F) {
          dist *= 0.0174532924F;
          x = 0;
        } else if (scale <= 135.0F) {
          if (dist > 0.0F) {
            dist = (dist - 90.0F) * 0.0174532924F;
            x = 1;
          } else {
            dist = (dist + 90.0F) * 0.0174532924F;
            x = -1;
          }
        } else if (dist > 0.0F) {
          dist = (dist - 180.0F) * 0.0174532924F;
          x = 2;
        } else {
          dist = (dist + 180.0F) * 0.0174532924F;
          x = -2;
        }

        dist = (real32_T)tan(dist);
        if ((x == 1) || (x == -1)) {
          dist = -1.0F / dist;
        }
      }

      s_idx_0 *= (real32_T)cameraTest_P.Constant7_Value;

      /* '<S6>:1:28' */
      n = blobIndices_data[lenRed] - 1;
      allLights_data[3 * n] = s_idx_0;
      allLights_data[1 + 3 * n] = (real32_T)cameraTest_P.Constant7_Value * dist;
      allLights_data[2 + 3 * n] = rtb_blobs.color[blobIndices_data[lenRed] - 1];

      /* '<S6>:1:19' */
    }

    if (rtb_blobs.area[blobIndices_data[maxVal] - 1] > 50) {
      /* '<S6>:1:30' */
      /* '<S6>:1:31' */
      cameraTest_DW.SFunction_DIMS3.x = 1;
      t = rt_roundf_snf(allLights_data[(blobIndices_data[maxVal] - 1) * 3] *
                        100.0F);
      if (t < 128.0F) {
        if (t >= -128.0F) {
          rtb_Ball_x_idx_0 = (int8_T)t;
        } else {
          rtb_Ball_x_idx_0 = MIN_int8_T;
        }
      } else {
        rtb_Ball_x_idx_0 = MAX_int8_T;
      }

      /* '<S6>:1:32' */
      cameraTest_DW.SFunction_DIMS3.y = 1;
      t = rt_roundf_snf(allLights_data[(blobIndices_data[maxVal] - 1) * 3 + 1] *
                        100.0F);
      if (t < 128.0F) {
        if (t >= -128.0F) {
          rtb_Ball_y_idx_0 = (int8_T)t;
        } else {
          rtb_Ball_y_idx_0 = MIN_int8_T;
        }
      } else {
        rtb_Ball_y_idx_0 = MAX_int8_T;
      }

      if ((real_T)blobIndices_data[maxVal] + 1.0 > idx) {
        Autothreshold_NORMF_FIXPT_DW = 0;
        pixListNinc = 0;
      } else {
        Autothreshold_NORMF_FIXPT_DW = (blobIndices_data[maxVal] + 1) - 1;
        pixListNinc = idx;
      }

      /* '<S6>:1:33' */
      if (1.0 > (real_T)blobIndices_data[maxVal] - 1.0) {
        loop_ub = 0;
      } else {
        loop_ub = (int32_T)((real_T)blobIndices_data[maxVal] - 1.0);
      }

      idx = (loop_ub + pixListNinc) - Autothreshold_NORMF_FIXPT_DW;
      for (i_0 = 0; i_0 < loop_ub; i_0++) {
        allLights_data_1[3 * i_0] = allLights_data[3 * i_0];
        allLights_data_1[1 + 3 * i_0] = allLights_data[3 * i_0 + 1];
        allLights_data_1[2 + 3 * i_0] = allLights_data[3 * i_0 + 2];
      }

      n = pixListNinc - Autothreshold_NORMF_FIXPT_DW;
      for (i_0 = 0; i_0 < n; i_0++) {
        allLights_data_1[3 * (i_0 + loop_ub)] = allLights_data
          [(Autothreshold_NORMF_FIXPT_DW + i_0) * 3];
        allLights_data_1[1 + 3 * (i_0 + loop_ub)] = allLights_data
          [(Autothreshold_NORMF_FIXPT_DW + i_0) * 3 + 1];
        allLights_data_1[2 + 3 * (i_0 + loop_ub)] = allLights_data
          [(Autothreshold_NORMF_FIXPT_DW + i_0) * 3 + 2];
      }

      for (i_0 = 0; i_0 < idx; i_0++) {
        allLights_data[3 * i_0] = allLights_data_1[3 * i_0];
        allLights_data[1 + 3 * i_0] = allLights_data_1[3 * i_0 + 1];
        allLights_data[2 + 3 * i_0] = allLights_data_1[3 * i_0 + 2];
      }
    }
  }

  /*  allLights */
  /*  create light arrays */
  /* '<S6>:1:39' */
  allLights_sizes_5[0] = 1;
  allLights_sizes_5[1] = idx;
  for (i_0 = 0; i_0 < idx; i_0++) {
    allLights_data_0[i_0] = (allLights_data[3 * i_0 + 2] == 1.0F);
  }

  cameraTest_eml_li_find(allLights_data_0, allLights_sizes_5, tmp_data_1,
    tmp_sizes_0);
  loop_ub = tmp_sizes_0[1];
  for (i_0 = 0; i_0 < loop_ub; i_0++) {
    reds_data[i_0 << 1] = allLights_data[(tmp_data_1[tmp_sizes_0[0] * i_0] - 1) *
      3];
    reds_data[1 + (i_0 << 1)] = allLights_data[(tmp_data_1[tmp_sizes_0[0] * i_0]
      - 1) * 3 + 1];
  }

  /* '<S6>:1:40' */
  allLights_sizes_4[0] = 1;
  allLights_sizes_4[1] = idx;
  for (i_0 = 0; i_0 < idx; i_0++) {
    allLights_data_0[i_0] = (allLights_data[3 * i_0 + 2] == 2.0F);
  }

  cameraTest_eml_li_find(allLights_data_0, allLights_sizes_4, tmp_data_1,
    tmp_sizes_0);
  loop_ub = tmp_sizes_0[1];
  for (i_0 = 0; i_0 < loop_ub; i_0++) {
    greens_data[i_0 << 1] = allLights_data[(tmp_data_1[tmp_sizes_0[0] * i_0] - 1)
      * 3];
    greens_data[1 + (i_0 << 1)] = allLights_data[(tmp_data_1[tmp_sizes_0[0] *
      i_0] - 1) * 3 + 1];
  }

  /* '<S6>:1:41' */
  allLights_sizes_3[0] = 1;
  allLights_sizes_3[1] = idx;
  for (i_0 = 0; i_0 < idx; i_0++) {
    allLights_data_0[i_0] = (allLights_data[3 * i_0 + 2] == 3.0F);
  }

  cameraTest_eml_li_find(allLights_data_0, allLights_sizes_3, tmp_data_1,
    allLights_sizes_5);
  loop_ub = allLights_sizes_5[1];
  for (i_0 = 0; i_0 < loop_ub; i_0++) {
    blues_data[i_0 << 1] = allLights_data[(tmp_data_1[allLights_sizes_5[0] * i_0]
      - 1) * 3];
    blues_data[1 + (i_0 << 1)] = allLights_data[(tmp_data_1[allLights_sizes_5[0]
      * i_0] - 1) * 3 + 1];
  }

  /*  allLights */
  /* '<S6>:1:44' */
  allLights_sizes_2[0] = 1;
  allLights_sizes_2[1] = idx;
  for (i_0 = 0; i_0 < idx; i_0++) {
    allLights_data_0[i_0] = (allLights_data[3 * i_0 + 2] == 1.0F);
  }

  cameraTest_eml_li_find(allLights_data_0, allLights_sizes_2, tmp_data_1,
    tmp_sizes_0);
  lenRed = tmp_sizes_0[1] - 1;

  /* '<S6>:1:45' */
  allLights_sizes_1[0] = 1;
  allLights_sizes_1[1] = idx;
  for (i_0 = 0; i_0 < idx; i_0++) {
    allLights_data_0[i_0] = (allLights_data[3 * i_0 + 2] == 2.0F);
  }

  cameraTest_eml_li_find(allLights_data_0, allLights_sizes_1, tmp_data_1,
    tmp_sizes_0);

  /* '<S6>:1:46' */
  /* '<S6>:1:48' */
  cameraTest_DW.SFunction_DIMS2.x[0] = 0;
  cameraTest_DW.SFunction_DIMS2.x[1] = 1;

  /* '<S6>:1:49' */
  cameraTest_DW.SFunction_DIMS2.y[0] = 0;
  cameraTest_DW.SFunction_DIMS2.y[1] = 1;

  /* '<S6>:1:50' */
  cameraTest_DW.SFunction_DIMS2.orientation[0] = 0;
  cameraTest_DW.SFunction_DIMS2.orientation[1] = 1;

  /* '<S6>:1:51' */
  cameraTest_DW.SFunction_DIMS2.color[0] = 0;
  cameraTest_DW.SFunction_DIMS2.color[1] = 1;

  /*  coder.varsize('players.x',[6 1]); */
  /*  coder.varsize('players.y',[6 1]); */
  /*  coder.varsize('players.orientation',[6 1]); */
  /*  coder.varsize('players.color',[6 1]); */
  allLights_sizes_0[0] = 1;
  allLights_sizes_0[1] = idx;
  for (i_0 = 0; i_0 < idx; i_0++) {
    allLights_data_0[i_0] = (allLights_data[3 * i_0 + 2] == 1.0F);
  }

  cameraTest_eml_li_find(allLights_data_0, allLights_sizes_0, tmp_data_1,
    tmp_sizes);
  if (tmp_sizes[1] > 0) {
    /* '<S6>:1:58' */
    if (allLights_sizes_5[1] > 0) {
      /* '<S6>:1:59' */
      /* '<S6>:1:60' */
      for (pixListNinc = 0; pixListNinc < allLights_sizes_5[1]; pixListNinc++) {
        /* '<S6>:1:60' */
        /* '<S6>:1:61' */
        /* '<S6>:1:62' */
        dist = 2.14748365E+9F;

        /* '<S6>:1:63' */
        threshold = 0U;

        /* '<S6>:1:64' */
        for (i = 0; i <= lenRed; i++) {
          /* '<S6>:1:64' */
          /* '<S6>:1:65' */
          /* '<S6>:1:66' */
          blues[0] = blues_data[pixListNinc << 1] - reds_data[i << 1];
          blues[1] = blues_data[(pixListNinc << 1) + 1] - reds_data[(i << 1) + 1];
          scale = cameraTest_norm(blues);
          if ((scale < 0.1) && (scale < dist)) {
            /* '<S6>:1:67' */
            /* '<S6>:1:68' */
            dist = scale;

            /* '<S6>:1:69' */
            threshold = (uint8_T)(1 + i);
          }

          /* '<S6>:1:64' */
        }

        if (threshold != 0) {
          /* '<S6>:1:72' */
          /* '<S6>:1:73' */
          /* '<S6>:1:74' */
          /* '<S6>:1:75' */
          n = cameraTest_DW.SFunction_DIMS2.x[0] + 1;
          loop_ub = cameraTest_DW.SFunction_DIMS2.x[0];
          for (i_0 = 0; i_0 < loop_ub; i_0++) {
            f_data[i_0] = rtb_players_x[i_0];
          }

          t = rt_roundf_snf(reds_data[(threshold - 1) << 1] * 100.0F);
          if (t < 128.0F) {
            if (t >= -128.0F) {
              f_data[cameraTest_DW.SFunction_DIMS2.x[0]] = (int8_T)t;
            } else {
              f_data[cameraTest_DW.SFunction_DIMS2.x[0]] = MIN_int8_T;
            }
          } else {
            f_data[cameraTest_DW.SFunction_DIMS2.x[0]] = MAX_int8_T;
          }

          /* '<S6>:1:76' */
          cameraTest_DW.SFunction_DIMS2.x[0] = n;
          cameraTest_DW.SFunction_DIMS2.x[1] = 1;
          for (i_0 = 0; i_0 < n; i_0++) {
            rtb_players_x[i_0] = f_data[i_0];
          }

          n = cameraTest_DW.SFunction_DIMS2.y[0] + 1;
          loop_ub = cameraTest_DW.SFunction_DIMS2.y[0];
          for (i_0 = 0; i_0 < loop_ub; i_0++) {
            f_data[i_0] = rtb_players_y[i_0];
          }

          t = rt_roundf_snf(reds_data[((threshold - 1) << 1) + 1] * 100.0F);
          if (t < 128.0F) {
            if (t >= -128.0F) {
              f_data[cameraTest_DW.SFunction_DIMS2.y[0]] = (int8_T)t;
            } else {
              f_data[cameraTest_DW.SFunction_DIMS2.y[0]] = MIN_int8_T;
            }
          } else {
            f_data[cameraTest_DW.SFunction_DIMS2.y[0]] = MAX_int8_T;
          }

          /* '<S6>:1:77' */
          cameraTest_DW.SFunction_DIMS2.y[0] = n;
          cameraTest_DW.SFunction_DIMS2.y[1] = 1;
          for (i_0 = 0; i_0 < n; i_0++) {
            rtb_players_y[i_0] = f_data[i_0];
          }

          n = cameraTest_DW.SFunction_DIMS2.orientation[0] + 1;
          loop_ub = cameraTest_DW.SFunction_DIMS2.orientation[0];
          for (i_0 = 0; i_0 < loop_ub; i_0++) {
            g_data[i_0] = rtb_players_orientation[i_0];
          }

          t = rt_roundf_snf(rt_atan2f_snf(blues_data[(pixListNinc << 1) + 1] -
            reds_data[((threshold - 1) << 1) + 1], blues_data[pixListNinc << 1]
            - reds_data[(threshold - 1) << 1]) * 57.2957802F);
          if (t < 32768.0F) {
            if (t >= -32768.0F) {
              g_data[cameraTest_DW.SFunction_DIMS2.orientation[0]] = (int16_T)t;
            } else {
              g_data[cameraTest_DW.SFunction_DIMS2.orientation[0]] = MIN_int16_T;
            }
          } else {
            g_data[cameraTest_DW.SFunction_DIMS2.orientation[0]] = MAX_int16_T;
          }

          /* '<S6>:1:78' */
          cameraTest_DW.SFunction_DIMS2.orientation[0] = n;
          cameraTest_DW.SFunction_DIMS2.orientation[1] = 1;
          for (i_0 = 0; i_0 < n; i_0++) {
            rtb_players_orientation[i_0] = g_data[i_0];
          }

          n = cameraTest_DW.SFunction_DIMS2.color[0] + 1;
          loop_ub = cameraTest_DW.SFunction_DIMS2.color[0];
          for (i_0 = 0; i_0 < loop_ub; i_0++) {
            h_data[i_0] = 98U;
          }

          h_data[cameraTest_DW.SFunction_DIMS2.color[0]] = 98U;

          /* '<S6>:1:79' */
          cameraTest_DW.SFunction_DIMS2.color[0] = n;
          cameraTest_DW.SFunction_DIMS2.color[1] = 1;
          for (i_0 = 0; i_0 < n; i_0++) {
            rtb_players_color[i_0] = 98U;
          }
        }

        /* '<S6>:1:60' */
      }
    }

    allLights_sizes[0] = 1;
    allLights_sizes[1] = idx;
    for (i_0 = 0; i_0 < idx; i_0++) {
      allLights_data_0[i_0] = (allLights_data[3 * i_0 + 2] == 2.0F);
    }

    cameraTest_eml_li_find(allLights_data_0, allLights_sizes, tmp_data_1,
      tmp_sizes);
    if (tmp_sizes[1] > 0) {
      /* '<S6>:1:83' */
      /* '<S6>:1:84' */
      for (pixListNinc = 0; pixListNinc < tmp_sizes_0[1]; pixListNinc++) {
        /* '<S6>:1:84' */
        /* '<S6>:1:85' */
        /* '<S6>:1:86' */
        dist = 2.14748365E+9F;

        /* '<S6>:1:87' */
        threshold = 0U;

        /* '<S6>:1:88' */
        for (Autothreshold_NORMF_FIXPT_DW = 0; Autothreshold_NORMF_FIXPT_DW <=
             lenRed; Autothreshold_NORMF_FIXPT_DW++) {
          /* '<S6>:1:88' */
          /* '<S6>:1:89' */
          /* '<S6>:1:90' */
          greens[0] = greens_data[pixListNinc << 1] -
            reds_data[Autothreshold_NORMF_FIXPT_DW << 1];
          greens[1] = greens_data[(pixListNinc << 1) + 1] - reds_data
            [(Autothreshold_NORMF_FIXPT_DW << 1) + 1];
          scale = cameraTest_norm(greens);
          if ((scale < 0.1) && (scale < dist)) {
            /* '<S6>:1:91' */
            /* '<S6>:1:92' */
            dist = scale;

            /* '<S6>:1:93' */
            threshold = (uint8_T)(1 + Autothreshold_NORMF_FIXPT_DW);
          }

          /* '<S6>:1:88' */
        }

        if (threshold != 0) {
          /* '<S6>:1:96' */
          /* '<S6>:1:97' */
          /* '<S6>:1:98' */
          /* '<S6>:1:99' */
          n = cameraTest_DW.SFunction_DIMS2.x[0] + 1;
          loop_ub = cameraTest_DW.SFunction_DIMS2.x[0];
          for (i_0 = 0; i_0 < loop_ub; i_0++) {
            f_data[i_0] = rtb_players_x[i_0];
          }

          t = rt_roundf_snf(reds_data[(threshold - 1) << 1] * 100.0F);
          if (t < 128.0F) {
            if (t >= -128.0F) {
              f_data[cameraTest_DW.SFunction_DIMS2.x[0]] = (int8_T)t;
            } else {
              f_data[cameraTest_DW.SFunction_DIMS2.x[0]] = MIN_int8_T;
            }
          } else {
            f_data[cameraTest_DW.SFunction_DIMS2.x[0]] = MAX_int8_T;
          }

          /* '<S6>:1:100' */
          cameraTest_DW.SFunction_DIMS2.x[0] = n;
          cameraTest_DW.SFunction_DIMS2.x[1] = 1;
          for (i_0 = 0; i_0 < n; i_0++) {
            rtb_players_x[i_0] = f_data[i_0];
          }

          n = cameraTest_DW.SFunction_DIMS2.y[0] + 1;
          loop_ub = cameraTest_DW.SFunction_DIMS2.y[0];
          for (i_0 = 0; i_0 < loop_ub; i_0++) {
            f_data[i_0] = rtb_players_y[i_0];
          }

          t = rt_roundf_snf(reds_data[((threshold - 1) << 1) + 1] * 100.0F);
          if (t < 128.0F) {
            if (t >= -128.0F) {
              f_data[cameraTest_DW.SFunction_DIMS2.y[0]] = (int8_T)t;
            } else {
              f_data[cameraTest_DW.SFunction_DIMS2.y[0]] = MIN_int8_T;
            }
          } else {
            f_data[cameraTest_DW.SFunction_DIMS2.y[0]] = MAX_int8_T;
          }

          /* '<S6>:1:101' */
          cameraTest_DW.SFunction_DIMS2.y[0] = n;
          cameraTest_DW.SFunction_DIMS2.y[1] = 1;
          for (i_0 = 0; i_0 < n; i_0++) {
            rtb_players_y[i_0] = f_data[i_0];
          }

          n = cameraTest_DW.SFunction_DIMS2.orientation[0] + 1;
          loop_ub = cameraTest_DW.SFunction_DIMS2.orientation[0];
          for (i_0 = 0; i_0 < loop_ub; i_0++) {
            g_data[i_0] = rtb_players_orientation[i_0];
          }

          t = rt_roundf_snf(rt_atan2f_snf(greens_data[(pixListNinc << 1) + 1] -
            reds_data[((threshold - 1) << 1) + 1], greens_data[pixListNinc << 1]
            - reds_data[(threshold - 1) << 1]) * 57.2957802F);
          if (t < 32768.0F) {
            if (t >= -32768.0F) {
              g_data[cameraTest_DW.SFunction_DIMS2.orientation[0]] = (int16_T)t;
            } else {
              g_data[cameraTest_DW.SFunction_DIMS2.orientation[0]] = MIN_int16_T;
            }
          } else {
            g_data[cameraTest_DW.SFunction_DIMS2.orientation[0]] = MAX_int16_T;
          }

          /* '<S6>:1:102' */
          cameraTest_DW.SFunction_DIMS2.orientation[0] = n;
          cameraTest_DW.SFunction_DIMS2.orientation[1] = 1;
          for (i_0 = 0; i_0 < n; i_0++) {
            rtb_players_orientation[i_0] = g_data[i_0];
          }

          n = cameraTest_DW.SFunction_DIMS2.color[0] + 1;
          loop_ub = cameraTest_DW.SFunction_DIMS2.color[0];
          for (i_0 = 0; i_0 < loop_ub; i_0++) {
            h_data[i_0] = rtb_players_color[i_0];
          }

          h_data[cameraTest_DW.SFunction_DIMS2.color[0]] = 103U;

          /* '<S6>:1:103' */
          cameraTest_DW.SFunction_DIMS2.color[0] = n;
          cameraTest_DW.SFunction_DIMS2.color[1] = 1;
          for (i_0 = 0; i_0 < n; i_0++) {
            rtb_players_color[i_0] = h_data[i_0];
          }
        }

        /* '<S6>:1:84' */
      }
    }
  }

  /* End of MATLAB Function: '<S1>/parseBlob' */

  /* MATLAB Function: '<S1>/Player Tracker' */
  /* MATLAB Function 'Raspi Code/Player Tracker': '<S4>:1' */
  /*  fGol, fDef, fOff, eGol, eDef, eOff */
  /* Green */
  /* Ball */
  /* % set all valids to false */
  /* '<S4>:1:24' */
  for (i_0 = 0; i_0 < 6; i_0++) {
    cameraTest_DW.latestPlayers.valid[i_0] = 0U;
  }

  /* '<S4>:1:25' */
  cameraTest_DW.latestBall.valid = 0U;
  if (!(cameraTest_DW.SFunction_DIMS3.x == 0)) {
    /* '<S4>:1:27' */
    /* '<S4>:1:28' */
    cameraTest_DW.latestBall.x = rtb_Ball_x_idx_0;

    /* '<S4>:1:29' */
    cameraTest_DW.latestBall.y = rtb_Ball_y_idx_0;

    /* '<S4>:1:30' */
    cameraTest_DW.latestBall.valid = 1U;
  }

  /* '<S4>:1:33' */
  if ((0 == cameraTest_DW.SFunction_DIMS2.x[0]) || (0 ==
       cameraTest_DW.SFunction_DIMS2.x[1])) {
    i_0 = 0;
  } else if (cameraTest_DW.SFunction_DIMS2.x[0] >=
             cameraTest_DW.SFunction_DIMS2.x[1]) {
    i_0 = cameraTest_DW.SFunction_DIMS2.x[0];
  } else {
    i_0 = cameraTest_DW.SFunction_DIMS2.x[1];
  }

  n = i_0 - 1;

  /* '<S4>:1:33' */
  for (lenRed = 0; lenRed <= n; lenRed++) {
    /* '<S4>:1:33' */
    /* '<S4>:1:34' */
    /* '<S4>:1:35' */
    idx = MAX_int32_T;

    /* '<S4>:1:36' */
    threshold = 0U;

    /* '<S4>:1:37' */
    for (i = 0; i < 6; i++) {
      /* '<S4>:1:37' */
      if (rtb_players_color[lenRed] == cameraTest_DW.latestPlayers.color[i]) {
        /* '<S4>:1:38' */
        /* '<S4>:1:39' */
        i_0 = rtb_players_x[lenRed] - cameraTest_DW.latestPlayers.x[i];
        if (i_0 > 127) {
          i_0 = 127;
        } else {
          if (i_0 < -128) {
            i_0 = -128;
          }
        }

        pixListNinc = rtb_players_y[lenRed] - cameraTest_DW.latestPlayers.y[i];
        if (pixListNinc > 127) {
          pixListNinc = 127;
        } else {
          if (pixListNinc < -128) {
            pixListNinc = -128;
          }
        }

        scale = 1.17549435E-38F;
        Autothreshold_NORMF_FIXPT_DW = (int32_T)(real32_T)fabs((int8_T)i_0);
        if (Autothreshold_NORMF_FIXPT_DW > 1.17549435E-38F) {
          t = 1.17549435E-38F / (real32_T)Autothreshold_NORMF_FIXPT_DW;
          dist = 0.0F * t * t + 1.0F;
          scale = (real32_T)Autothreshold_NORMF_FIXPT_DW;
        } else {
          t = (real32_T)Autothreshold_NORMF_FIXPT_DW / 1.17549435E-38F;
          dist = t * t;
        }

        Autothreshold_NORMF_FIXPT_DW = (int32_T)(real32_T)fabs((int8_T)
          pixListNinc);
        if (Autothreshold_NORMF_FIXPT_DW > scale) {
          t = scale / (real32_T)Autothreshold_NORMF_FIXPT_DW;
          dist = dist * t * t + 1.0F;
          scale = (real32_T)Autothreshold_NORMF_FIXPT_DW;
        } else {
          t = (real32_T)Autothreshold_NORMF_FIXPT_DW / scale;
          dist += t * t;
        }

        dist = scale * (real32_T)sqrt(dist);
        t = rt_roundf_snf(dist);
        if (t < 2.14748365E+9F) {
          if (t >= -2.14748365E+9F) {
            i_0 = (int32_T)t;
          } else {
            i_0 = MIN_int32_T;
          }
        } else {
          i_0 = MAX_int32_T;
        }

        if ((i_0 < idx) && (i_0 < 10)) {
          /* '<S4>:1:40' */
          /* '<S4>:1:41' */
          idx = i_0;

          /* '<S4>:1:42' */
          threshold = (uint8_T)(1 + i);
        }
      }

      /* '<S4>:1:37' */
    }

    if (threshold != 0) {
      /* '<S4>:1:46' */
      /* '<S4>:1:47' */
      cameraTest_DW.latestPlayers.x[threshold - 1] = rtb_players_x[lenRed];

      /* '<S4>:1:48' */
      cameraTest_DW.latestPlayers.y[threshold - 1] = rtb_players_y[lenRed];

      /* '<S4>:1:49' */
      cameraTest_DW.latestPlayers.orientation[threshold - 1] =
        rtb_players_orientation[lenRed];

      /* '<S4>:1:50' */
      cameraTest_DW.latestPlayers.valid[threshold - 1] = 1U;
    }

    /* '<S4>:1:33' */
  }

  /* '<S4>:1:53' */
  cameraTest_B.ball = cameraTest_DW.latestBall;

  /* '<S4>:1:54' */
  /* '<S4>:1:55' */
  /* '<S4>:1:56' */
  /* '<S4>:1:57' */
  /* '<S4>:1:58' */
  /* '<S4>:1:59' */
  /* '<S4>:1:60' */
  /* '<S4>:1:61' */
  for (pixListNinc = 0; pixListNinc < 6; pixListNinc++) {
    /* '<S4>:1:61' */
    /* '<S4>:1:62' */
    cameraTest_B.players[pixListNinc].x =
      cameraTest_DW.latestPlayers.x[pixListNinc];

    /* '<S4>:1:63' */
    cameraTest_B.players[pixListNinc].y =
      cameraTest_DW.latestPlayers.y[pixListNinc];

    /* '<S4>:1:64' */
    cameraTest_B.players[pixListNinc].orientation =
      cameraTest_DW.latestPlayers.orientation[pixListNinc];

    /* '<S4>:1:65' */
    cameraTest_B.players[pixListNinc].color =
      cameraTest_DW.latestPlayers.color[pixListNinc];

    /* '<S4>:1:66' */
    cameraTest_B.players[pixListNinc].position =
      cameraTest_DW.latestPlayers.position[pixListNinc];

    /* '<S4>:1:67' */
    cameraTest_B.players[pixListNinc].valid =
      cameraTest_DW.latestPlayers.valid[pixListNinc];

    /* '<S4>:1:61' */
  }

  /* End of MATLAB Function: '<S1>/Player Tracker' */

  /* Chart: '<S1>/Chart' */
  /* Gateway: Raspi Code/Chart */
  cameraTest_DW.sfEvent = -1;

  /* During: Raspi Code/Chart */
  if (cameraTest_DW.is_active_c10_cameraTest == 0U) {
    /* Entry: Raspi Code/Chart */
    cameraTest_DW.is_active_c10_cameraTest = 1U;

    /* Entry Internal: Raspi Code/Chart */
    /* Transition: '<S3>:11' */
    cameraTest_DW.is_c10_cameraTest = cameraTest_IN_notOn;

    /* Entry 'notOn': '<S3>:5' */
    cameraTest_B.gameOn = 0U;
    cameraTest_DW.idleTicks = 0U;
  } else if (cameraTest_DW.is_c10_cameraTest == cameraTest_IN_gameIsOn) {
    /* During 'gameIsOn': '<S3>:4' */
    if (cameraTest_calcGoal() == 1) {
      /* Transition: '<S3>:10' */
      cameraTest_DW.is_c10_cameraTest = cameraTest_IN_notOn;

      /* Entry 'notOn': '<S3>:5' */
      cameraTest_B.gameOn = 0U;
      cameraTest_DW.idleTicks = 0U;
    }
  } else {
    /* During 'notOn': '<S3>:5' */
    if (cameraTest_DW.idleTicks > 10) {
      /* Transition: '<S3>:12' */
      cameraTest_DW.is_c10_cameraTest = cameraTest_IN_gameIsOn;

      /* Entry 'gameIsOn': '<S3>:4' */
      cameraTest_B.gameOn = 1U;
    } else {
      i_0 = (int32_T)((uint32_T)cameraTest_DW.idleTicks + cameraTest_calcReady());
      if ((uint32_T)i_0 > 255U) {
        i_0 = 255;
      }

      cameraTest_DW.idleTicks = (uint8_T)i_0;
    }
  }

  /* End of Chart: '<S1>/Chart' */

  /* MATLAB Function: '<S1>/encode' */
  /* MATLAB Function 'Raspi Code/encode': '<S5>:1' */
  /* '<S5>:1:2' */
  x = cameraTest_B.ball.x;
  memcpy(&threshold, &x, (size_t)1 * sizeof(uint8_T));
  x = cameraTest_B.ball.y;
  memcpy(&threshold, &x, (size_t)1 * sizeof(uint8_T));

  /* '<S5>:1:4' */
  cameraTest_DW.SFunction_DIMS2_b[0] = 1;
  cameraTest_DW.SFunction_DIMS2_b[1] = 7;

  /* '<S5>:1:9' */
  for (lenRed = 0; lenRed < 6; lenRed++) {
    /* '<S5>:1:9' */
    x = cameraTest_B.players[lenRed].x;
    memcpy(&threshold, &x, (size_t)1 * sizeof(uint8_T));
    x = cameraTest_B.players[lenRed].y;
    memcpy(&threshold, &x, (size_t)1 * sizeof(uint8_T));
    int16Obj = cameraTest_B.players[lenRed].orientation;
    memcpy(&e_y[0], &int16Obj, (size_t)2 * sizeof(uint8_T));

    /* '<S5>:1:10' */
    n = cameraTest_DW.SFunction_DIMS2_b[1] + 4;
    cameraTest_DW.SFunction_DIMS2_b[0] = 1;
    cameraTest_DW.SFunction_DIMS2_b[1] = n;

    /* '<S5>:1:9' */
  }

  /* End of MATLAB Function: '<S1>/encode' */
  /*  len=length(infoToTransmit) */
}

/* Model update function */
void cameraTest_update(void)
{
  /* signal main to stop simulation */
  {                                    /* Sample time: [0.033333333333333333s, 0.0s] */
    if ((rtmGetTFinal(cameraTest_M)!=-1) &&
        !((rtmGetTFinal(cameraTest_M)-cameraTest_M->Timing.taskTime0) >
          cameraTest_M->Timing.taskTime0 * (DBL_EPSILON))) {
      rtmSetErrorStatus(cameraTest_M, "Simulation finished");
    }

    if (rtmGetStopRequested(cameraTest_M)) {
      rtmSetErrorStatus(cameraTest_M, "Simulation finished");
    }
  }

  /* Update absolute time for base rate */
  /* The "clockTick0" counts the number of times the code of this task has
   * been executed. The absolute time is the multiplication of "clockTick0"
   * and "Timing.stepSize0". Size of "clockTick0" ensures timer will not
   * overflow during the application lifespan selected.
   */
  cameraTest_M->Timing.taskTime0 =
    (++cameraTest_M->Timing.clockTick0) * cameraTest_M->Timing.stepSize0;
}

/* Model initialize function */
void cameraTest_initialize(void)
{
  /* Registration code */

  /* initialize non-finites */
  rt_InitInfAndNaN(sizeof(real_T));

  /* initialize real-time model */
  (void) memset((void *)cameraTest_M, 0,
                sizeof(RT_MODEL_cameraTest_T));
  rtmSetTFinal(cameraTest_M, -1);
  cameraTest_M->Timing.stepSize0 = 0.033333333333333333;

  /* External mode info */
  cameraTest_M->Sizes.checksums[0] = (2999848203U);
  cameraTest_M->Sizes.checksums[1] = (1338021763U);
  cameraTest_M->Sizes.checksums[2] = (1878381878U);
  cameraTest_M->Sizes.checksums[3] = (1154249998U);

  {
    static const sysRanDType rtAlwaysEnabled = SUBSYS_RAN_BC_ENABLE;
    static RTWExtModeInfo rt_ExtModeInfo;
    static const sysRanDType *systemRan[8];
    cameraTest_M->extModeInfo = (&rt_ExtModeInfo);
    rteiSetSubSystemActiveVectorAddresses(&rt_ExtModeInfo, systemRan);
    systemRan[0] = &rtAlwaysEnabled;
    systemRan[1] = &rtAlwaysEnabled;
    systemRan[2] = &rtAlwaysEnabled;
    systemRan[3] = &rtAlwaysEnabled;
    systemRan[4] = &rtAlwaysEnabled;
    systemRan[5] = &rtAlwaysEnabled;
    systemRan[6] = &rtAlwaysEnabled;
    systemRan[7] = &rtAlwaysEnabled;
    rteiSetModelMappingInfoPtr(cameraTest_M->extModeInfo,
      &cameraTest_M->SpecialInfo.mappingInfo);
    rteiSetChecksumsPtr(cameraTest_M->extModeInfo, cameraTest_M->Sizes.checksums);
    rteiSetTPtr(cameraTest_M->extModeInfo, rtmGetTPtr(cameraTest_M));
  }

  /* block I/O */
  (void) memset(((void *) &cameraTest_B), 0,
                sizeof(B_cameraTest_T));

  /* states (dwork) */
  (void) memset((void *)&cameraTest_DW, 0,
                sizeof(DW_cameraTest_T));

  /* data type transition information */
  {
    static DataTypeTransInfo dtInfo;
    (void) memset((char_T *) &dtInfo, 0,
                  sizeof(dtInfo));
    cameraTest_M->SpecialInfo.mappingInfo = (&dtInfo);
    dtInfo.numDataTypes = 31;
    dtInfo.dataTypeSizes = &rtDataTypeSizes[0];
    dtInfo.dataTypeNames = &rtDataTypeNames[0];

    /* Block I/O transition table */
    dtInfo.B = &rtBTransTable;

    /* Parameters transition table */
    dtInfo.P = &rtPTransTable;
  }

  {
    static const int8_T b[6] = { -10, -30, -70, 10, 30, 70 };

    static const uint8_T c[6] = { 0U, 0U, 0U, 180U, 180U, 180U };

    static const uint8_T d[6] = { 103U, 103U, 103U, 98U, 98U, 98U };

    static const uint8_T e[6] = { 111U, 100U, 103U, 111U, 100U, 103U };

    int32_T i;

    /* Start for S-Function (v4l2_video_capture_sfcn): '<Root>/V4L2 Video Capture' */
    MW_videoCaptureInit(cameraTest_ConstP.V4L2VideoCapture_p1, 0, 0, 0, 0, 320U,
                        240U, 2U, 2U, 1U, 0.033333333333333333);

    /* Start for S-Function (sdl_video_display_sfcn): '<S2>/SDL Video Display' */
    MW_SDL_videoDisplayInit(1, 1, 1, 320, 240);

    /* InitializeConditions for MATLAB Function: '<S1>/Player Tracker' */
    for (i = 0; i < 6; i++) {
      cameraTest_DW.latestPlayers.x[i] = b[i];
      cameraTest_DW.latestPlayers.y[i] = 0;
      cameraTest_DW.latestPlayers.orientation[i] = c[i];
      cameraTest_DW.latestPlayers.color[i] = d[i];
      cameraTest_DW.latestPlayers.position[i] = e[i];
      cameraTest_DW.latestPlayers.valid[i] = 0U;
    }

    cameraTest_DW.latestBall.x = 0;
    cameraTest_DW.latestBall.y = 0;
    cameraTest_DW.latestBall.valid = 1U;

    /* End of InitializeConditions for MATLAB Function: '<S1>/Player Tracker' */

    /* InitializeConditions for Chart: '<S1>/Chart' */
    cameraTest_DW.sfEvent = -1;
    cameraTest_DW.is_active_c10_cameraTest = 0U;
    cameraTest_DW.is_c10_cameraTest = cameraTest_IN_NO_ACTIVE_CHILD;
  }
}

/* Model terminate function */
void cameraTest_terminate(void)
{
  /* Terminate for S-Function (v4l2_video_capture_sfcn): '<Root>/V4L2 Video Capture' */
  MW_videoCaptureTerminate(cameraTest_ConstP.V4L2VideoCapture_p1);

  /* Terminate for S-Function (sdl_video_display_sfcn): '<S2>/SDL Video Display' */
  MW_SDL_videoDisplayTerminate(320, 240);
}

/*
 * File trailer for generated code.
 *
 * [EOF]
 */

/*
 * cameraTest_dt.h
 *
 * Code generation for model "cameraTest".
 *
 * Model version              : 1.2
 * Simulink Coder version : 8.7 (R2014b) 08-Sep-2014
 * C source code generated on : Wed Feb 11 17:41:24 2015
 *
 * Target selection: realtime.tlc
 * Embedded hardware selection: ARM Compatible->ARM Cortex
 * Code generation objectives: Unspecified
 * Validation result: Not run
 */

#include "ext_types.h"

/* data type size table */
static uint_T rtDataTypeSizes[] = {
  sizeof(real_T),
  sizeof(real32_T),
  sizeof(int8_T),
  sizeof(uint8_T),
  sizeof(int16_T),
  sizeof(uint16_T),
  sizeof(int32_T),
  sizeof(uint32_T),
  sizeof(boolean_T),
  sizeof(fcn_call_T),
  sizeof(int_T),
  sizeof(pointer_T),
  sizeof(action_T),
  2*sizeof(uint32_T),
  sizeof(BlobData),
  sizeof(Player),
  sizeof(Ball),
  sizeof(PlayerData),
  sizeof(BallData),
  sizeof(BlobData_size),
  sizeof(int32_T),
  sizeof(int32_T),
  sizeof(int32_T),
  sizeof(PlayerData_size),
  sizeof(BallData_size),
  sizeof(stETj7QHScyDJroYDDeQcXH_camer_T),
  sizeof(int32_T),
  sizeof(uint32_T),
  sizeof(uint32_T),
  sizeof(int32_T),
  sizeof(int32_T)
};

/* data type name table */
static const char_T * rtDataTypeNames[] = {
  "real_T",
  "real32_T",
  "int8_T",
  "uint8_T",
  "int16_T",
  "uint16_T",
  "int32_T",
  "uint32_T",
  "boolean_T",
  "fcn_call_T",
  "int_T",
  "pointer_T",
  "action_T",
  "timer_uint32_pair_T",
  "BlobData",
  "Player",
  "Ball",
  "PlayerData",
  "BallData",
  "BlobData_size",
  "int32_T",
  "int32_T",
  "int32_T",
  "PlayerData_size",
  "BallData_size",
  "stETj7QHScyDJroYDDeQcXH_camer_T",
  "int32_T",
  "uint32_T",
  "uint32_T",
  "int32_T",
  "int32_T"
};

/* data type transitions for block I/O structure */
static DataTypeTransition rtBTransitions[] = {
  { (char_T *)(&cameraTest_B.V4L2VideoCapture_o1[0]), 3, 0, 230400 },

  { (char_T *)(&cameraTest_B.lengthh), 0, 0, 1 },

  { (char_T *)(&cameraTest_B.players[0]), 15, 0, 6 },

  { (char_T *)(&cameraTest_B.ball), 16, 0, 1 },

  { (char_T *)(&cameraTest_B.BlobAnalysis_o1[0]), 6, 0, 16 },

  { (char_T *)(&cameraTest_B.Autothreshold_o2), 3, 0, 4 },

  { (char_T *)(&cameraTest_B.Autothreshold_o1[0]), 8, 0, 307200 }
  ,

  { (char_T *)(&cameraTest_DW.latestPlayers), 25, 0, 1 },

  { (char_T *)(&cameraTest_DW.SFunction_DIMS2), 23, 0, 1 },

  { (char_T *)(&cameraTest_DW.SFunction_DIMS2_a), 19, 0, 1 },

  { (char_T *)(&cameraTest_DW.latestBall), 16, 0, 1 },

  { (char_T *)(&cameraTest_DW.SFunction_DIMS3), 24, 0, 1 },

  { (char_T *)(&cameraTest_DW.Scope_PWORK.LoggedData), 11, 0, 3 },

  { (char_T *)(&cameraTest_DW.BlobAnalysis_DIMS1[0]), 6, 0, 7 },

  { (char_T *)(&cameraTest_DW.is_active_c10_cameraTest), 3, 0, 3 },

  { (char_T *)(&cameraTest_DW.isStable), 8, 0, 1 }
};

/* data type transition table for block I/O structure */
static DataTypeTransitionTable rtBTransTable = {
  16U,
  rtBTransitions
};

/* data type transitions for Parameters structure */
static DataTypeTransition rtPTransitions[] = {
  { (char_T *)(&cameraTest_P.Constant6_Value[0]), 0, 0, 3 },

  { (char_T *)(&cameraTest_P.Autothreshold_BIN_BOUNDARY_FIXP[0]), 3, 0, 769 }
};

/* data type transition table for Parameters structure */
static DataTypeTransitionTable rtPTransTable = {
  2U,
  rtPTransitions
};

/* [EOF] cameraTest_dt.h */

/*
 * File: cameraTest_data.c
 *
 * Code generated for Simulink model 'cameraTest'.
 *
 * Model version                  : 1.2
 * Simulink Coder version         : 8.7 (R2014b) 08-Sep-2014
 * TLC version                    : 8.7 (Aug  5 2014)
 * C/C++ source code generated on : Wed Feb 11 17:41:24 2015
 *
 * Target selection: realtime.tlc
 * Embedded hardware selection: ARM Compatible->ARM Cortex
 * Code generation objectives: Unspecified
 * Validation result: Not run
 */

#include "cameraTest.h"
#include "cameraTest_private.h"

/* Block parameters (auto storage) */
P_cameraTest_T cameraTest_P = {
  /*  Expression: [240 320]
   * Referenced by: '<S1>/Constant6'
   */
  { 240.0, 320.0 },
  1.3,                                 /* Expression: 1.3
                                        * Referenced by: '<S1>/Constant7'
                                        */

  /*  Computed Parameter: Autothreshold_BIN_BOUNDARY_FIXP
   * Referenced by: '<S2>/Autothreshold'
   */
  { 0U, 1U, 2U, 3U, 4U, 5U, 6U, 7U, 8U, 9U, 10U, 11U, 12U, 13U, 14U, 15U, 16U,
    17U, 18U, 19U, 20U, 21U, 22U, 23U, 24U, 25U, 26U, 27U, 28U, 29U, 30U, 31U,
    32U, 33U, 34U, 35U, 36U, 37U, 38U, 39U, 40U, 41U, 42U, 43U, 44U, 45U, 46U,
    47U, 48U, 49U, 50U, 51U, 52U, 53U, 54U, 55U, 56U, 57U, 58U, 59U, 60U, 61U,
    62U, 63U, 64U, 65U, 66U, 67U, 68U, 69U, 70U, 71U, 72U, 73U, 74U, 75U, 76U,
    77U, 78U, 79U, 80U, 81U, 82U, 83U, 84U, 85U, 86U, 87U, 88U, 89U, 90U, 91U,
    92U, 93U, 94U, 95U, 96U, 97U, 98U, 99U, 100U, 101U, 102U, 103U, 104U, 105U,
    106U, 107U, 108U, 109U, 110U, 111U, 112U, 113U, 114U, 115U, 116U, 117U, 118U,
    119U, 120U, 121U, 122U, 123U, 124U, 125U, 126U, 127U, 128U, 129U, 130U, 131U,
    132U, 133U, 134U, 135U, 136U, 137U, 138U, 139U, 140U, 141U, 142U, 143U, 144U,
    145U, 146U, 147U, 148U, 149U, 150U, 151U, 152U, 153U, 154U, 155U, 156U, 157U,
    158U, 159U, 160U, 161U, 162U, 163U, 164U, 165U, 166U, 167U, 168U, 169U, 170U,
    171U, 172U, 173U, 174U, 175U, 176U, 177U, 178U, 179U, 180U, 181U, 182U, 183U,
    184U, 185U, 186U, 187U, 188U, 189U, 190U, 191U, 192U, 193U, 194U, 195U, 196U,
    197U, 198U, 199U, 200U, 201U, 202U, 203U, 204U, 205U, 206U, 207U, 208U, 209U,
    210U, 211U, 212U, 213U, 214U, 215U, 216U, 217U, 218U, 219U, 220U, 221U, 222U,
    223U, 224U, 225U, 226U, 227U, 228U, 229U, 230U, 231U, 232U, 233U, 234U, 235U,
    236U, 237U, 238U, 239U, 240U, 241U, 242U, 243U, 244U, 245U, 246U, 247U, 248U,
    249U, 250U, 251U, 252U, 253U, 254U, 255U },

  /*  Computed Parameter: Autothreshold1_BIN_BOUNDARY_FIX
   * Referenced by: '<S2>/Autothreshold1'
   */
  { 0U, 1U, 2U, 3U, 4U, 5U, 6U, 7U, 8U, 9U, 10U, 11U, 12U, 13U, 14U, 15U, 16U,
    17U, 18U, 19U, 20U, 21U, 22U, 23U, 24U, 25U, 26U, 27U, 28U, 29U, 30U, 31U,
    32U, 33U, 34U, 35U, 36U, 37U, 38U, 39U, 40U, 41U, 42U, 43U, 44U, 45U, 46U,
    47U, 48U, 49U, 50U, 51U, 52U, 53U, 54U, 55U, 56U, 57U, 58U, 59U, 60U, 61U,
    62U, 63U, 64U, 65U, 66U, 67U, 68U, 69U, 70U, 71U, 72U, 73U, 74U, 75U, 76U,
    77U, 78U, 79U, 80U, 81U, 82U, 83U, 84U, 85U, 86U, 87U, 88U, 89U, 90U, 91U,
    92U, 93U, 94U, 95U, 96U, 97U, 98U, 99U, 100U, 101U, 102U, 103U, 104U, 105U,
    106U, 107U, 108U, 109U, 110U, 111U, 112U, 113U, 114U, 115U, 116U, 117U, 118U,
    119U, 120U, 121U, 122U, 123U, 124U, 125U, 126U, 127U, 128U, 129U, 130U, 131U,
    132U, 133U, 134U, 135U, 136U, 137U, 138U, 139U, 140U, 141U, 142U, 143U, 144U,
    145U, 146U, 147U, 148U, 149U, 150U, 151U, 152U, 153U, 154U, 155U, 156U, 157U,
    158U, 159U, 160U, 161U, 162U, 163U, 164U, 165U, 166U, 167U, 168U, 169U, 170U,
    171U, 172U, 173U, 174U, 175U, 176U, 177U, 178U, 179U, 180U, 181U, 182U, 183U,
    184U, 185U, 186U, 187U, 188U, 189U, 190U, 191U, 192U, 193U, 194U, 195U, 196U,
    197U, 198U, 199U, 200U, 201U, 202U, 203U, 204U, 205U, 206U, 207U, 208U, 209U,
    210U, 211U, 212U, 213U, 214U, 215U, 216U, 217U, 218U, 219U, 220U, 221U, 222U,
    223U, 224U, 225U, 226U, 227U, 228U, 229U, 230U, 231U, 232U, 233U, 234U, 235U,
    236U, 237U, 238U, 239U, 240U, 241U, 242U, 243U, 244U, 245U, 246U, 247U, 248U,
    249U, 250U, 251U, 252U, 253U, 254U, 255U },

  /*  Computed Parameter: Autothreshold2_BIN_BOUNDARY_FIX
   * Referenced by: '<S2>/Autothreshold2'
   */
  { 0U, 1U, 2U, 3U, 4U, 5U, 6U, 7U, 8U, 9U, 10U, 11U, 12U, 13U, 14U, 15U, 16U,
    17U, 18U, 19U, 20U, 21U, 22U, 23U, 24U, 25U, 26U, 27U, 28U, 29U, 30U, 31U,
    32U, 33U, 34U, 35U, 36U, 37U, 38U, 39U, 40U, 41U, 42U, 43U, 44U, 45U, 46U,
    47U, 48U, 49U, 50U, 51U, 52U, 53U, 54U, 55U, 56U, 57U, 58U, 59U, 60U, 61U,
    62U, 63U, 64U, 65U, 66U, 67U, 68U, 69U, 70U, 71U, 72U, 73U, 74U, 75U, 76U,
    77U, 78U, 79U, 80U, 81U, 82U, 83U, 84U, 85U, 86U, 87U, 88U, 89U, 90U, 91U,
    92U, 93U, 94U, 95U, 96U, 97U, 98U, 99U, 100U, 101U, 102U, 103U, 104U, 105U,
    106U, 107U, 108U, 109U, 110U, 111U, 112U, 113U, 114U, 115U, 116U, 117U, 118U,
    119U, 120U, 121U, 122U, 123U, 124U, 125U, 126U, 127U, 128U, 129U, 130U, 131U,
    132U, 133U, 134U, 135U, 136U, 137U, 138U, 139U, 140U, 141U, 142U, 143U, 144U,
    145U, 146U, 147U, 148U, 149U, 150U, 151U, 152U, 153U, 154U, 155U, 156U, 157U,
    158U, 159U, 160U, 161U, 162U, 163U, 164U, 165U, 166U, 167U, 168U, 169U, 170U,
    171U, 172U, 173U, 174U, 175U, 176U, 177U, 178U, 179U, 180U, 181U, 182U, 183U,
    184U, 185U, 186U, 187U, 188U, 189U, 190U, 191U, 192U, 193U, 194U, 195U, 196U,
    197U, 198U, 199U, 200U, 201U, 202U, 203U, 204U, 205U, 206U, 207U, 208U, 209U,
    210U, 211U, 212U, 213U, 214U, 215U, 216U, 217U, 218U, 219U, 220U, 221U, 222U,
    223U, 224U, 225U, 226U, 227U, 228U, 229U, 230U, 231U, 232U, 233U, 234U, 235U,
    236U, 237U, 238U, 239U, 240U, 241U, 242U, 243U, 244U, 245U, 246U, 247U, 248U,
    249U, 250U, 251U, 252U, 253U, 254U, 255U },
  0U                                   /* Computed Parameter: Constant_Value
                                        * Referenced by: '<S7>/Constant'
                                        */
};

/* Constant parameters (auto storage) */
const ConstP_cameraTest_T cameraTest_ConstP = {
  /* Computed Parameter: BlobAnalysis_WALKER_R
   * Referenced by: '<S2>/Blob Analysis'
   */
  { -1, 321, 322, 323, 1, -321, -322, -323 },

  /* Expression: devName
   * Referenced by: '<Root>/V4L2 Video Capture'
   */
  { 47U, 100U, 101U, 118U, 47U, 118U, 105U, 100U, 101U, 111U, 48U, 0U }
};

/*
 * File trailer for generated code.
 *
 * [EOF]
 */

/*
 * File: cameraTest.h
 *
 * Code generated for Simulink model 'cameraTest'.
 *
 * Model version                  : 1.126
 * Simulink Coder version         : 8.7 (R2014b) 08-Sep-2014
 * TLC version                    : 8.7 (Aug  5 2014)
 * C/C++ source code generated on : Sat Feb 21 22:17:55 2015
 *
 * Target selection: realtime.tlc
 * Embedded hardware selection: ARM Compatible->ARM Cortex
 * Code generation objectives: Unspecified
 * Validation result: Not run
 */

#ifndef RTW_HEADER_cameraTest_h_
#define RTW_HEADER_cameraTest_h_
#include <float.h>
#include <math.h>
#include <string.h>
#include <stddef.h>
#ifndef cameraTest_COMMON_INCLUDES_
# define cameraTest_COMMON_INCLUDES_
#include "rtwtypes.h"
#include "rtw_extmode.h"
#include "sysran_types.h"
#include "rtw_continuous.h"
#include "rtw_solver.h"
#include "dt_info.h"
#include "ext_work.h"
#include "sdl_video_display.h"
#include "v4l2_capture.h"
#endif                                 /* cameraTest_COMMON_INCLUDES_ */

#include "cameraTest_types.h"

/* Shared type includes */
#include "multiword_types.h"
#include "rtGetNaN.h"
#include "rt_nonfinite.h"
#include "rtGetInf.h"

/* Macros for accessing real-time model data structure */
#ifndef rtmGetFinalTime
# define rtmGetFinalTime(rtm)          ((rtm)->Timing.tFinal)
#endif

#ifndef rtmGetErrorStatus
# define rtmGetErrorStatus(rtm)        ((rtm)->errorStatus)
#endif

#ifndef rtmSetErrorStatus
# define rtmSetErrorStatus(rtm, val)   ((rtm)->errorStatus = (val))
#endif

#ifndef rtmGetStopRequested
# define rtmGetStopRequested(rtm)      ((rtm)->Timing.stopRequestedFlag)
#endif

#ifndef rtmSetStopRequested
# define rtmSetStopRequested(rtm, val) ((rtm)->Timing.stopRequestedFlag = (val))
#endif

#ifndef rtmGetStopRequestedPtr
# define rtmGetStopRequestedPtr(rtm)   (&((rtm)->Timing.stopRequestedFlag))
#endif

#ifndef rtmGetT
# define rtmGetT(rtm)                  ((rtm)->Timing.taskTime0)
#endif

#ifndef rtmGetTFinal
# define rtmGetTFinal(rtm)             ((rtm)->Timing.tFinal)
#endif

/* Block states (auto storage) for system '<S9>/Subsystem' */
typedef struct {
  int8_T Subsystem_SubsysRanBC;        /* '<S9>/Subsystem' */
  boolean_T Subsystem_MODE;            /* '<S9>/Subsystem' */
} DW_Subsystem_cameraTest_T;

/* Block signals (auto storage) */
typedef struct {
  real32_T Gain3[76800];               /* '<S9>/Gain3' */
  real32_T Gain2[76800];               /* '<S9>/Gain2' */
  real32_T Gain1[76800];               /* '<S9>/Gain1' */
  uint8_T uv0[76800];
  uint8_T uv1[76800];
  uint8_T uv2[76800];
  real_T ManualSwitch;                 /* '<S9>/Manual Switch' */
  real_T ManualSwitch1;                /* '<S9>/Manual Switch1' */
  real_T leee;                         /* '<S9>/FindBlobs1' */
  real32_T ColorSpaceConversion_o1[76800];/* '<S9>/Color Space  Conversion' */
  real32_T ColorSpaceConversion_o2[76800];/* '<S9>/Color Space  Conversion' */
  real32_T ColorSpaceConversion_o3[76800];/* '<S9>/Color Space  Conversion' */
  real32_T Autothreshold3_o2;          /* '<S9>/Autothreshold3' */
  real32_T Saturation;                 /* '<S9>/Saturation' */
  int16_T orientation;
  int16_T thing;                       /* '<S9>/FindBlobs1' */
  int8_T x;
  int8_T y;
  uint8_T V4L2VideoCapture_o1[76800];  /* '<Root>/V4L2 Video Capture' */
  uint8_T V4L2VideoCapture_o2[76800];  /* '<Root>/V4L2 Video Capture' */
  uint8_T V4L2VideoCapture_o3[76800];  /* '<Root>/V4L2 Video Capture' */
  boolean_T Autothreshold3_o1[76800];  /* '<S9>/Autothreshold3' */
  boolean_T Erosion[76800];            /* '<S9>/Erosion' */
  boolean_T Erosion1[76800];           /* '<S9>/Erosion1' */
  boolean_T Compare[76800];            /* '<S15>/Compare' */
  boolean_T Compare_m[76800];          /* '<S14>/Compare' */
} B_cameraTest_T;

/* Block states (auto storage) for system '<Root>' */
typedef struct {
  uint32_T BlobAnalysis1_STACK_DW[76800];/* '<S9>/Blob Analysis1' */
  int16_T BlobAnalysis1_M_PIXLIST_DW[76800];/* '<S9>/Blob Analysis1' */
  int16_T BlobAnalysis1_N_PIXLIST_DW[76800];/* '<S9>/Blob Analysis1' */
  boolean_T Erosion_TWO_PAD_IMG_DW[83081];/* '<S9>/Erosion' */
  boolean_T Erosion_ONE_PAD_IMG_DW[83081];/* '<S9>/Erosion' */
  boolean_T Dilation_ONE_PAD_IMG_DW[79625];/* '<S9>/Dilation' */
  uint8_T BlobAnalysis1_PAD_DW[77924]; /* '<S9>/Blob Analysis1' */
  boolean_T Erosion1_ONE_PAD_IMG_DW[77361];/* '<S9>/Erosion1' */
  stETj7QHScyDJroYDDeQcXH_camer_T latestPlayers;/* '<S2>/Player Tracker' */
  PlayerData_size SFunction_DIMS2;     /* '<S2>/parseBlob' */
  BlobData_size SFunction_DIMS2_o;     /* '<S9>/FindBlobs1' */
  Ball latestBall;                     /* '<S2>/Player Tracker' */
  BallData_size SFunction_DIMS3;       /* '<S2>/parseBlob' */
  struct {
    void *LoggedData;
  } Scope_PWORK;                       /* '<S2>/Scope' */

  struct {
    void *LoggedData;
  } Scope_PWORK_c;                     /* '<S9>/Scope' */

  struct {
    void *LoggedData;
  } Scope1_PWORK;                      /* '<S9>/Scope1' */

  struct {
    void *LoggedData;
  } Scope3_PWORK;                      /* '<S9>/Scope3' */

  real32_T ColorSpaceConversion_DWORK1[76800];/* '<S9>/Color Space  Conversion' */
  int32_T Erosion_NUMNONZ_DW[2];       /* '<S9>/Erosion' */
  int32_T Erosion_STREL_DW[2];         /* '<S9>/Erosion' */
  int32_T Erosion_ERODE_OFF_DW[12];    /* '<S9>/Erosion' */
  int32_T Dilation_NUMNONZ_DW;         /* '<S9>/Dilation' */
  int32_T Dilation_STREL_DW;           /* '<S9>/Dilation' */
  int32_T Dilation_DILATE_OFF_DW[9];   /* '<S9>/Dilation' */
  int32_T Erosion1_NUMNONZ_DW;         /* '<S9>/Erosion1' */
  int32_T Erosion1_STREL_DW;           /* '<S9>/Erosion1' */
  int32_T Erosion1_ERODE_OFF_DW;       /* '<S9>/Erosion1' */
  int32_T BlobAnalysis1_DIMS1[2];      /* '<S9>/Blob Analysis1' */
  int32_T BlobAnalysis1_DIMS2[2];      /* '<S9>/Blob Analysis1' */
  int32_T latestRawOrientation_sizes[2];/* '<S2>/Player Tracker' */
  int32_T spHandle;                    /* '<S4>/Data Store Memory' */
  int32_T SFunction_DIMS2_k;           /* '<S4>/SerialRead' */
  int16_T latestRawOrientation_data[6];/* '<S2>/Player Tracker' */
  int8_T FunctionCallSubsystem_SubsysRan;/* '<Root>/Function-Call Subsystem' */
  int8_T Subsystem4_SubsysRanBC;       /* '<S9>/Subsystem4' */
  uint8_T Psend;                       /* '<S1>/MATLAB Function' */
  boolean_T latestPlayers_not_empty;   /* '<S2>/Player Tracker' */
  boolean_T latestBall_not_empty;      /* '<S2>/Player Tracker' */
  boolean_T latestRawOrientation_not_empty;/* '<S2>/Player Tracker' */
  DW_Subsystem_cameraTest_T Subsystem1;/* '<S9>/Subsystem1' */
  DW_Subsystem_cameraTest_T Subsystem; /* '<S9>/Subsystem' */
} DW_cameraTest_T;

/* Constant parameters (auto storage) */
typedef struct {
  /* Computed Parameter: BlobAnalysis1_WALKER_
   * Referenced by: '<S9>/Blob Analysis1'
   */
  int32_T BlobAnalysis1_WALKER_[8];

  /* Expression: devName
   * Referenced by: '<Root>/V4L2 Video Capture'
   */
  uint8_T V4L2VideoCapture_p1[12];
} ConstP_cameraTest_T;

/* Parameters (auto storage) */
struct P_cameraTest_T_ {
  uint32_T BlobAnalysis1_minArea;      /* Mask Parameter: BlobAnalysis1_minArea
                                        * Referenced by: '<S9>/Blob Analysis1'
                                        */
  real_T Constant1_Value;              /* Expression: 0
                                        * Referenced by: '<S1>/Constant1'
                                        */
  real_T Constant_Value;               /* Expression: 1
                                        * Referenced by: '<S1>/Constant'
                                        */
  real_T Constant7_Value;              /* Expression: 0
                                        * Referenced by: '<S9>/Constant7'
                                        */
  real_T Constant3_Value;              /* Expression: 1
                                        * Referenced by: '<S9>/Constant3'
                                        */
  real_T Constant2_Value;              /* Expression: 0
                                        * Referenced by: '<S9>/Constant2'
                                        */
  real_T Constant1_Value_b;            /* Expression: 1
                                        * Referenced by: '<S9>/Constant1'
                                        */
  real_T Constant6_Value[2];           /* Expression: [240 320]
                                        * Referenced by: '<S2>/Constant6'
                                        */
  real_T Constant7_Value_e;            /* Expression: 1.3
                                        * Referenced by: '<S2>/Constant7'
                                        */
  real_T Constant4_Value;              /* Expression: 0
                                        * Referenced by: '<S9>/Constant4'
                                        */
  real32_T Gain1_Gain;                 /* Computed Parameter: Gain1_Gain
                                        * Referenced by: '<S9>/Gain1'
                                        */
  real32_T Gain2_Gain;                 /* Computed Parameter: Gain2_Gain
                                        * Referenced by: '<S9>/Gain2'
                                        */
  real32_T Gain3_Gain;                 /* Computed Parameter: Gain3_Gain
                                        * Referenced by: '<S9>/Gain3'
                                        */
  real32_T Saturation_UpperSat;        /* Computed Parameter: Saturation_UpperSat
                                        * Referenced by: '<S9>/Saturation'
                                        */
  real32_T Saturation_LowerSat;        /* Computed Parameter: Saturation_LowerSat
                                        * Referenced by: '<S9>/Saturation'
                                        */
  real32_T Constant_Value_f;           /* Computed Parameter: Constant_Value_f
                                        * Referenced by: '<S14>/Constant'
                                        */
  int32_T DataStoreMemory_InitialValue;/* Computed Parameter: DataStoreMemory_InitialValue
                                        * Referenced by: '<S4>/Data Store Memory'
                                        */
  uint8_T ManualSwitch_CurrentSetting; /* Computed Parameter: ManualSwitch_CurrentSetting
                                        * Referenced by: '<S1>/Manual Switch'
                                        */
  uint8_T Constant_Value_a;            /* Computed Parameter: Constant_Value_a
                                        * Referenced by: '<S15>/Constant'
                                        */
  uint8_T ManualSwitch_CurrentSetting_o;/* Computed Parameter: ManualSwitch_CurrentSetting_o
                                         * Referenced by: '<S9>/Manual Switch'
                                         */
  uint8_T ManualSwitch1_CurrentSetting;/* Computed Parameter: ManualSwitch1_CurrentSetting
                                        * Referenced by: '<S9>/Manual Switch1'
                                        */
  boolean_T Constant5_Value[9];        /* Computed Parameter: Constant5_Value
                                        * Referenced by: '<S9>/Constant5'
                                        */
  boolean_T Constant6_Value_o;         /* Computed Parameter: Constant6_Value_o
                                        * Referenced by: '<S9>/Constant6'
                                        */
};

/* Real-time Model Data Structure */
struct tag_RTM_cameraTest_T {
  const char_T *errorStatus;
  RTWExtModeInfo *extModeInfo;

  /*
   * Sizes:
   * The following substructure contains sizes information
   * for many of the model attributes such as inputs, outputs,
   * dwork, sample times, etc.
   */
  struct {
    uint32_T checksums[4];
  } Sizes;

  /*
   * SpecialInfo:
   * The following substructure contains special information
   * related to other components that are dependent on RTW.
   */
  struct {
    const void *mappingInfo;
  } SpecialInfo;

  /*
   * Timing:
   * The following substructure contains information regarding
   * the timing information for the model.
   */
  struct {
    time_T taskTime0;
    uint32_T clockTick0;
    time_T stepSize0;
    time_T tFinal;
    boolean_T stopRequestedFlag;
  } Timing;
};

/* Block parameters (auto storage) */
extern P_cameraTest_T cameraTest_P;

/* Block signals (auto storage) */
extern B_cameraTest_T cameraTest_B;

/* Block states (auto storage) */
extern DW_cameraTest_T cameraTest_DW;

/* Constant parameters (auto storage) */
extern const ConstP_cameraTest_T cameraTest_ConstP;

/* Model entry point functions */
extern void cameraTest_initialize(void);
extern void cameraTest_output(void);
extern void cameraTest_update(void);
extern void cameraTest_terminate(void);

/* Real-time Model object */
extern RT_MODEL_cameraTest_T *const cameraTest_M;

/*-
 * The generated code includes comments that allow you to trace directly
 * back to the appropriate location in the model.  The basic format
 * is <system>/block_name, where system is the system number (uniquely
 * assigned by Simulink) and block_name is the name of the block.
 *
 * Use the MATLAB hilite_system command to trace the generated code back
 * to the model.  For example,
 *
 * hilite_system('<S3>')    - opens system 3
 * hilite_system('<S3>/Kp') - opens and selects block Kp which resides in S3
 *
 * Here is the system hierarchy for this model
 *
 * '<Root>' : 'cameraTest'
 * '<S1>'   : 'cameraTest/Function-Call Subsystem'
 * '<S2>'   : 'cameraTest/Raspi Code'
 * '<S3>'   : 'cameraTest/Function-Call Subsystem/MATLAB Function'
 * '<S4>'   : 'cameraTest/Function-Call Subsystem/SerialCom'
 * '<S5>'   : 'cameraTest/Function-Call Subsystem/SerialCom/Function-Call Subsystem'
 * '<S6>'   : 'cameraTest/Function-Call Subsystem/SerialCom/SerialInitilization'
 * '<S7>'   : 'cameraTest/Function-Call Subsystem/SerialCom/SerialRead'
 * '<S8>'   : 'cameraTest/Function-Call Subsystem/SerialCom/Function-Call Subsystem/SerialWrite'
 * '<S9>'   : 'cameraTest/Raspi Code/Blob extraction '
 * '<S10>'  : 'cameraTest/Raspi Code/Chart'
 * '<S11>'  : 'cameraTest/Raspi Code/Player Tracker'
 * '<S12>'  : 'cameraTest/Raspi Code/encode'
 * '<S13>'  : 'cameraTest/Raspi Code/parseBlob'
 * '<S14>'  : 'cameraTest/Raspi Code/Blob extraction /Compare To Zero1'
 * '<S15>'  : 'cameraTest/Raspi Code/Blob extraction /Compare To Zero2'
 * '<S16>'  : 'cameraTest/Raspi Code/Blob extraction /FindBlobs1'
 * '<S17>'  : 'cameraTest/Raspi Code/Blob extraction /MATLAB Function2'
 * '<S18>'  : 'cameraTest/Raspi Code/Blob extraction /Subsystem'
 * '<S19>'  : 'cameraTest/Raspi Code/Blob extraction /Subsystem1'
 * '<S20>'  : 'cameraTest/Raspi Code/Blob extraction /Subsystem4'
 */
#endif                                 /* RTW_HEADER_cameraTest_h_ */

/*
 * File trailer for generated code.
 *
 * [EOF]
 */

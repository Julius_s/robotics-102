/*
 * cameraTest_dt.h
 *
 * Code generation for model "cameraTest".
 *
 * Model version              : 1.126
 * Simulink Coder version : 8.7 (R2014b) 08-Sep-2014
 * C source code generated on : Sat Feb 21 22:17:55 2015
 *
 * Target selection: realtime.tlc
 * Embedded hardware selection: ARM Compatible->ARM Cortex
 * Code generation objectives: Unspecified
 * Validation result: Not run
 */

#include "ext_types.h"

/* data type size table */
static uint_T rtDataTypeSizes[] = {
  sizeof(real_T),
  sizeof(real32_T),
  sizeof(int8_T),
  sizeof(uint8_T),
  sizeof(int16_T),
  sizeof(uint16_T),
  sizeof(int32_T),
  sizeof(uint32_T),
  sizeof(boolean_T),
  sizeof(fcn_call_T),
  sizeof(int_T),
  sizeof(pointer_T),
  sizeof(action_T),
  2*sizeof(uint32_T),
  sizeof(BlobData),
  sizeof(PlayerData),
  sizeof(Player),
  sizeof(Ball),
  sizeof(BallData),
  sizeof(BlobData_size),
  sizeof(PlayerData_size),
  sizeof(BallData_size),
  sizeof(stETj7QHScyDJroYDDeQcXH_camer_T)
};

/* data type name table */
static const char_T * rtDataTypeNames[] = {
  "real_T",
  "real32_T",
  "int8_T",
  "uint8_T",
  "int16_T",
  "uint16_T",
  "int32_T",
  "uint32_T",
  "boolean_T",
  "fcn_call_T",
  "int_T",
  "pointer_T",
  "action_T",
  "timer_uint32_pair_T",
  "BlobData",
  "PlayerData",
  "Player",
  "Ball",
  "BallData",
  "BlobData_size",
  "PlayerData_size",
  "BallData_size",
  "stETj7QHScyDJroYDDeQcXH_camer_T"
};

/* data type transitions for block I/O structure */
static DataTypeTransition rtBTransitions[] = {
  { (char_T *)(&cameraTest_B.ManualSwitch), 0, 0, 3 },

  { (char_T *)(&cameraTest_B.ColorSpaceConversion_o1[0]), 1, 0, 230402 },

  { (char_T *)(&cameraTest_B.orientation), 4, 0, 2 },

  { (char_T *)(&cameraTest_B.x), 2, 0, 2 },

  { (char_T *)(&cameraTest_B.V4L2VideoCapture_o1[0]), 3, 0, 230400 },

  { (char_T *)(&cameraTest_B.Autothreshold3_o1[0]), 8, 0, 230400 }
  ,

  { (char_T *)(&cameraTest_DW.latestPlayers), 22, 0, 1 },

  { (char_T *)(&cameraTest_DW.SFunction_DIMS2), 20, 0, 1 },

  { (char_T *)(&cameraTest_DW.SFunction_DIMS2_o), 19, 0, 1 },

  { (char_T *)(&cameraTest_DW.latestBall), 17, 0, 1 },

  { (char_T *)(&cameraTest_DW.SFunction_DIMS3), 21, 0, 1 },

  { (char_T *)(&cameraTest_DW.Scope_PWORK.LoggedData), 11, 0, 4 },

  { (char_T *)(&cameraTest_DW.ColorSpaceConversion_DWORK1[0]), 1, 0, 76800 },

  { (char_T *)(&cameraTest_DW.Erosion_NUMNONZ_DW[0]), 6, 0, 38 },

  { (char_T *)(&cameraTest_DW.latestRawOrientation_data[0]), 4, 0, 6 },

  { (char_T *)(&cameraTest_DW.FunctionCallSubsystem_SubsysRan), 2, 0, 2 },

  { (char_T *)(&cameraTest_DW.Psend), 3, 0, 1 },

  { (char_T *)(&cameraTest_DW.latestPlayers_not_empty), 8, 0, 3 },

  { (char_T *)(&cameraTest_DW.Subsystem1.Subsystem_SubsysRanBC), 2, 0, 1 },

  { (char_T *)(&cameraTest_DW.Subsystem1.Subsystem_MODE), 8, 0, 1 },

  { (char_T *)(&cameraTest_DW.Subsystem.Subsystem_SubsysRanBC), 2, 0, 1 },

  { (char_T *)(&cameraTest_DW.Subsystem.Subsystem_MODE), 8, 0, 1 }
};

/* data type transition table for block I/O structure */
static DataTypeTransitionTable rtBTransTable = {
  22U,
  rtBTransitions
};

/* data type transitions for Parameters structure */
static DataTypeTransition rtPTransitions[] = {
  { (char_T *)(&cameraTest_P.BlobAnalysis1_minArea), 7, 0, 1 },

  { (char_T *)(&cameraTest_P.Constant1_Value), 0, 0, 10 },

  { (char_T *)(&cameraTest_P.Gain1_Gain), 1, 0, 6 },

  { (char_T *)(&cameraTest_P.DataStoreMemory_InitialValue), 6, 0, 1 },

  { (char_T *)(&cameraTest_P.ManualSwitch_CurrentSetting), 3, 0, 4 },

  { (char_T *)(&cameraTest_P.Constant5_Value[0]), 8, 0, 10 }
};

/* data type transition table for Parameters structure */
static DataTypeTransitionTable rtPTransTable = {
  6U,
  rtPTransitions
};

/* [EOF] cameraTest_dt.h */

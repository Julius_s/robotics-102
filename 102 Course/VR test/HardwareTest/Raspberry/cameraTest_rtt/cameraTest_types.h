/*
 * File: cameraTest_types.h
 *
 * Code generated for Simulink model 'cameraTest'.
 *
 * Model version                  : 1.126
 * Simulink Coder version         : 8.7 (R2014b) 08-Sep-2014
 * TLC version                    : 8.7 (Aug  5 2014)
 * C/C++ source code generated on : Sat Feb 21 22:17:55 2015
 *
 * Target selection: realtime.tlc
 * Embedded hardware selection: ARM Compatible->ARM Cortex
 * Code generation objectives: Unspecified
 * Validation result: Not run
 */

#ifndef RTW_HEADER_cameraTest_types_h_
#define RTW_HEADER_cameraTest_types_h_
#include "rtwtypes.h"
#include "multiword_types.h"
#ifndef _DEFINED_TYPEDEF_FOR_BlobData_size_
#define _DEFINED_TYPEDEF_FOR_BlobData_size_

typedef struct {
  int32_T centroid[2];
  int32_T orientation;
} BlobData_size;

#endif

#ifndef _DEFINED_TYPEDEF_FOR_PlayerData_size_
#define _DEFINED_TYPEDEF_FOR_PlayerData_size_

typedef struct {
  int32_T x[2];
  int32_T y[2];
  int32_T orientation[2];
  int32_T color[2];
} PlayerData_size;

#endif

#ifndef _DEFINED_TYPEDEF_FOR_BallData_size_
#define _DEFINED_TYPEDEF_FOR_BallData_size_

typedef struct {
  int32_T x;
  int32_T y;
} BallData_size;

#endif

#ifndef _DEFINED_TYPEDEF_FOR_BlobData_
#define _DEFINED_TYPEDEF_FOR_BlobData_

typedef struct {
  real32_T centroid[32];
  int16_T orientation[16];
} BlobData;

#endif

#ifndef _DEFINED_TYPEDEF_FOR_PlayerData_
#define _DEFINED_TYPEDEF_FOR_PlayerData_

typedef struct {
  int8_T x[6];
  int8_T y[6];
  int16_T orientation[6];
  uint8_T color[6];
} PlayerData;

#endif

#ifndef _DEFINED_TYPEDEF_FOR_Player_
#define _DEFINED_TYPEDEF_FOR_Player_

typedef struct {
  int8_T x;
  int8_T y;
  int16_T orientation;
  uint8_T color;
  uint8_T position;
  uint8_T valid;
} Player;

#endif

#ifndef _DEFINED_TYPEDEF_FOR_Ball_
#define _DEFINED_TYPEDEF_FOR_Ball_

typedef struct {
  int8_T x;
  int8_T y;
  uint8_T valid;
} Ball;

#endif

#ifndef _DEFINED_TYPEDEF_FOR_BallData_
#define _DEFINED_TYPEDEF_FOR_BallData_

typedef struct {
  int8_T x[2];
  int8_T y[2];
} BallData;

#endif

/* Custom Type definition for MATLAB Function: '<S2>/Player Tracker' */
#ifndef struct_stETj7QHScyDJroYDDeQcXH
#define struct_stETj7QHScyDJroYDDeQcXH

struct stETj7QHScyDJroYDDeQcXH
{
  int8_T x[6];
  int8_T y[6];
  int16_T orientation[6];
  uint8_T color[6];
  uint8_T position[6];
  uint8_T valid[6];
};

#endif                                 /*struct_stETj7QHScyDJroYDDeQcXH*/

#ifndef typedef_stETj7QHScyDJroYDDeQcXH_camer_T
#define typedef_stETj7QHScyDJroYDDeQcXH_camer_T

typedef struct stETj7QHScyDJroYDDeQcXH stETj7QHScyDJroYDDeQcXH_camer_T;

#endif                                 /*typedef_stETj7QHScyDJroYDDeQcXH_camer_T*/

/* Parameters (auto storage) */
typedef struct P_cameraTest_T_ P_cameraTest_T;

/* Forward declaration for rtModel */
typedef struct tag_RTM_cameraTest_T RT_MODEL_cameraTest_T;

#endif                                 /* RTW_HEADER_cameraTest_types_h_ */

/*
 * File trailer for generated code.
 *
 * [EOF]
 */

/*
 * File: cameraTest_private.h
 *
 * Code generated for Simulink model 'cameraTest'.
 *
 * Model version                  : 1.126
 * Simulink Coder version         : 8.7 (R2014b) 08-Sep-2014
 * TLC version                    : 8.7 (Aug  5 2014)
 * C/C++ source code generated on : Sat Feb 21 22:17:55 2015
 *
 * Target selection: realtime.tlc
 * Embedded hardware selection: ARM Compatible->ARM Cortex
 * Code generation objectives: Unspecified
 * Validation result: Not run
 */

#ifndef RTW_HEADER_cameraTest_private_h_
#define RTW_HEADER_cameraTest_private_h_
#include "rtwtypes.h"
#include "multiword_types.h"

/* Private macros used by the generated code to access rtModel */
#ifndef rtmSetTFinal
# define rtmSetTFinal(rtm, val)        ((rtm)->Timing.tFinal = (val))
#endif

#ifndef rtmGetTPtr
# define rtmGetTPtr(rtm)               (&(rtm)->Timing.taskTime0)
#endif

extern real32_T rt_roundf_snf(real32_T u);
extern real32_T rt_remf_snf(real32_T u0, real32_T u1);
extern int32_T div_s32(int32_T numerator, int32_T denominator);
extern int16_T div_s16s32_floor(int32_T numerator, int32_T denominator);
extern void cameraTest_Subsystem_Disable(DW_Subsystem_cameraTest_T *localDW);
extern void cameraTest_Subsystem_Update(DW_Subsystem_cameraTest_T *localDW);
extern void cameraTest_Subsystem(real_T rtu_Enable, DW_Subsystem_cameraTest_T
  *localDW);

#endif                                 /* RTW_HEADER_cameraTest_private_h_ */

/*
 * File trailer for generated code.
 *
 * [EOF]
 */

/*
 * File: cameraTest_data.c
 *
 * Code generated for Simulink model 'cameraTest'.
 *
 * Model version                  : 1.126
 * Simulink Coder version         : 8.7 (R2014b) 08-Sep-2014
 * TLC version                    : 8.7 (Aug  5 2014)
 * C/C++ source code generated on : Sat Feb 21 22:17:55 2015
 *
 * Target selection: realtime.tlc
 * Embedded hardware selection: ARM Compatible->ARM Cortex
 * Code generation objectives: Unspecified
 * Validation result: Not run
 */

#include "cameraTest.h"
#include "cameraTest_private.h"

/* Block parameters (auto storage) */
P_cameraTest_T cameraTest_P = {
  100U,                                /* Mask Parameter: BlobAnalysis1_minArea
                                        * Referenced by: '<S9>/Blob Analysis1'
                                        */
  0.0,                                 /* Expression: 0
                                        * Referenced by: '<S1>/Constant1'
                                        */
  1.0,                                 /* Expression: 1
                                        * Referenced by: '<S1>/Constant'
                                        */
  0.0,                                 /* Expression: 0
                                        * Referenced by: '<S9>/Constant7'
                                        */
  1.0,                                 /* Expression: 1
                                        * Referenced by: '<S9>/Constant3'
                                        */
  0.0,                                 /* Expression: 0
                                        * Referenced by: '<S9>/Constant2'
                                        */
  1.0,                                 /* Expression: 1
                                        * Referenced by: '<S9>/Constant1'
                                        */

  /*  Expression: [240 320]
   * Referenced by: '<S2>/Constant6'
   */
  { 240.0, 320.0 },
  1.3,                                 /* Expression: 1.3
                                        * Referenced by: '<S2>/Constant7'
                                        */
  0.0,                                 /* Expression: 0
                                        * Referenced by: '<S9>/Constant4'
                                        */
  0.00392156886F,                      /* Computed Parameter: Gain1_Gain
                                        * Referenced by: '<S9>/Gain1'
                                        */
  0.00392156886F,                      /* Computed Parameter: Gain2_Gain
                                        * Referenced by: '<S9>/Gain2'
                                        */
  0.00392156886F,                      /* Computed Parameter: Gain3_Gain
                                        * Referenced by: '<S9>/Gain3'
                                        */
  1.0F,                                /* Computed Parameter: Saturation_UpperSat
                                        * Referenced by: '<S9>/Saturation'
                                        */
  0.8F,                                /* Computed Parameter: Saturation_LowerSat
                                        * Referenced by: '<S9>/Saturation'
                                        */
  0.0F,                                /* Computed Parameter: Constant_Value_f
                                        * Referenced by: '<S14>/Constant'
                                        */
  -1,                                  /* Computed Parameter: DataStoreMemory_InitialValue
                                        * Referenced by: '<S4>/Data Store Memory'
                                        */
  1U,                                  /* Computed Parameter: ManualSwitch_CurrentSetting
                                        * Referenced by: '<S1>/Manual Switch'
                                        */
  0U,                                  /* Computed Parameter: Constant_Value_a
                                        * Referenced by: '<S15>/Constant'
                                        */
  0U,                                  /* Computed Parameter: ManualSwitch_CurrentSetting_o
                                        * Referenced by: '<S9>/Manual Switch'
                                        */
  0U,                                  /* Computed Parameter: ManualSwitch1_CurrentSetting
                                        * Referenced by: '<S9>/Manual Switch1'
                                        */

  /*  Computed Parameter: Constant5_Value
   * Referenced by: '<S9>/Constant5'
   */
  { 1, 1, 1, 1, 1, 1, 1, 1, 1 },
  1                                    /* Computed Parameter: Constant6_Value_o
                                        * Referenced by: '<S9>/Constant6'
                                        */
};

/* Constant parameters (auto storage) */
const ConstP_cameraTest_T cameraTest_ConstP = {
  /* Computed Parameter: BlobAnalysis1_WALKER_
   * Referenced by: '<S9>/Blob Analysis1'
   */
  { -1, 321, 322, 323, 1, -321, -322, -323 },

  /* Expression: devName
   * Referenced by: '<Root>/V4L2 Video Capture'
   */
  { 47U, 100U, 101U, 118U, 47U, 118U, 105U, 100U, 101U, 111U, 48U, 0U }
};

/*
 * File trailer for generated code.
 *
 * [EOF]
 */

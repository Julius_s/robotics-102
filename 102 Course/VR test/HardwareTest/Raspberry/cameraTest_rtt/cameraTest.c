/*
 * File: cameraTest.c
 *
 * Code generated for Simulink model 'cameraTest'.
 *
 * Model version                  : 1.126
 * Simulink Coder version         : 8.7 (R2014b) 08-Sep-2014
 * TLC version                    : 8.7 (Aug  5 2014)
 * C/C++ source code generated on : Sat Feb 21 22:17:55 2015
 *
 * Target selection: realtime.tlc
 * Embedded hardware selection: ARM Compatible->ARM Cortex
 * Code generation objectives: Unspecified
 * Validation result: Not run
 */

#include "cameraTest.h"
#include "cameraTest_private.h"
#include "cameraTest_dt.h"

/* Block signals (auto storage) */
B_cameraTest_T cameraTest_B;

/* Block states (auto storage) */
DW_cameraTest_T cameraTest_DW;

/* Real-time model */
RT_MODEL_cameraTest_T cameraTest_M_;
RT_MODEL_cameraTest_T *const cameraTest_M = &cameraTest_M_;
int32_T div_s32(int32_T numerator, int32_T denominator)
{
  int32_T quotient;
  uint32_T tempAbsQuotient;
  if (denominator == 0) {
    quotient = numerator >= 0 ? MAX_int32_T : MIN_int32_T;

    /* Divide by zero handler */
  } else {
    tempAbsQuotient = (uint32_T)(numerator >= 0 ? numerator : -numerator) /
      (denominator >= 0 ? denominator : -denominator);
    quotient = (numerator < 0) != (denominator < 0) ? -(int32_T)tempAbsQuotient :
      (int32_T)tempAbsQuotient;
  }

  return quotient;
}

int16_T div_s16s32_floor(int32_T numerator, int32_T denominator)
{
  int16_T quotient;
  uint32_T absNumerator;
  uint32_T absDenominator;
  uint32_T tempAbsQuotient;
  boolean_T quotientNeedsNegation;
  if (denominator == 0) {
    quotient = numerator >= 0 ? (int32_T)MAX_int16_T : (int32_T)MIN_int16_T;

    /* Divide by zero handler */
  } else {
    absNumerator = (uint32_T)(numerator >= 0 ? numerator : -numerator);
    absDenominator = (uint32_T)(denominator >= 0 ? denominator : -denominator);
    quotientNeedsNegation = ((numerator < 0) != (denominator < 0));
    tempAbsQuotient = absNumerator / absDenominator;
    if (quotientNeedsNegation) {
      absNumerator %= absDenominator;
      if (absNumerator > 0U) {
        tempAbsQuotient++;
      }
    }

    quotient = quotientNeedsNegation ? (int32_T)(int16_T)-(int32_T)
      tempAbsQuotient : (int32_T)(int16_T)tempAbsQuotient;
  }

  return quotient;
}

/*
 * Disable for enable system:
 *    '<S9>/Subsystem'
 *    '<S9>/Subsystem1'
 */
void cameraTest_Subsystem_Disable(DW_Subsystem_cameraTest_T *localDW)
{
  localDW->Subsystem_MODE = false;
}

/*
 * Outputs for enable system:
 *    '<S9>/Subsystem'
 *    '<S9>/Subsystem1'
 */
void cameraTest_Subsystem(real_T rtu_Enable, DW_Subsystem_cameraTest_T *localDW)
{
  /* Outputs for Enabled SubSystem: '<S9>/Subsystem' incorporates:
   *  EnablePort: '<S18>/Enable'
   */
  if (rtu_Enable > 0.0) {
    if (!localDW->Subsystem_MODE) {
      localDW->Subsystem_MODE = true;
    }

    srUpdateBC(localDW->Subsystem_SubsysRanBC);
  } else {
    if (localDW->Subsystem_MODE) {
      cameraTest_Subsystem_Disable(localDW);
    }
  }

  /* End of Outputs for SubSystem: '<S9>/Subsystem' */
}

/*
 * Update for enable system:
 *    '<S9>/Subsystem'
 *    '<S9>/Subsystem1'
 */
void cameraTest_Subsystem_Update(DW_Subsystem_cameraTest_T *localDW)
{
  /* Update for Enabled SubSystem: '<S9>/Subsystem' incorporates:
   *  Update for EnablePort: '<S18>/Enable'
   */
  if (localDW->Subsystem_MODE) {
  }

  /* End of Update for SubSystem: '<S9>/Subsystem' */
}

real32_T rt_roundf_snf(real32_T u)
{
  real32_T y;
  if ((real32_T)fabs(u) < 8.388608E+6F) {
    if (u >= 0.5F) {
      y = (real32_T)floor(u + 0.5F);
    } else if (u > -0.5F) {
      y = u * 0.0F;
    } else {
      y = (real32_T)ceil(u - 0.5F);
    }
  } else {
    y = u;
  }

  return y;
}

real32_T rt_remf_snf(real32_T u0, real32_T u1)
{
  real32_T y;
  real32_T u1_0;
  if (!((!rtIsNaNF(u0)) && (!rtIsInfF(u0)) && ((!rtIsNaNF(u1)) && (!rtIsInfF(u1)))))
  {
    y = (rtNaNF);
  } else {
    if (u1 < 0.0F) {
      u1_0 = (real32_T)ceil(u1);
    } else {
      u1_0 = (real32_T)floor(u1);
    }

    if ((u1 != 0.0F) && (u1 != u1_0)) {
      u1_0 = u0 / u1;
      if ((real32_T)fabs(u1_0 - rt_roundf_snf(u1_0)) <= FLT_EPSILON * (real32_T)
          fabs(u1_0)) {
        y = 0.0F;
      } else {
        y = (real32_T)fmod(u0, u1);
      }
    } else {
      y = (real32_T)fmod(u0, u1);
    }
  }

  return y;
}

/* Model output function */
void cameraTest_output(void)
{
  real32_T t;
  static const int8_T b[6] = { -10, -30, -70, 10, 30, 70 };

  static const uint8_T c[6] = { 0U, 0U, 0U, 180U, 180U, 180U };

  static const uint8_T d[6] = { 103U, 103U, 103U, 98U, 98U, 98U };

  static const uint8_T e[6] = { 111U, 100U, 103U, 111U, 100U, 103U };

  static const uint8_T b_0[8] = { 85U, 85U, 85U, 85U, 104U, 101U, 97U, 100U };

  static const uint8_T d_0[24] = { 120U, 121U, 111U, 111U, 120U, 121U, 111U,
    111U, 120U, 121U, 111U, 111U, 120U, 121U, 111U, 111U, 120U, 121U, 111U, 111U,
    120U, 121U, 111U, 111U };

  int32_T bytesRead;
  uint8_T dataRead[512];
  int32_T dataLength;
  static const char_T b_1[13] = { '/', 'd', 'e', 'v', '/', 't', 't', 'y', 'A',
    'M', 'A', '0', '\x00' };

  uint8_T dataOut[35];
  Player rtb_players_g[6];
  uint8_T rtb_dataOut[35];
  int8_T rtb_players_x[6];
  int8_T rtb_players_y[6];
  int16_T rtb_players_orientation[6];
  int16_T rtb_robotBlobs_orientation[16];
  real32_T rtb_BlobAnalysis1_o2[16];
  real32_T rtb_BlobAnalysis1_o1[32];
  real_T rtb_Product;
  uint32_T numBlobs;
  uint32_T BlobAnalysis1_NUM_PIX_DW[16];
  real32_T xys;
  real32_T ys;
  uint32_T k;
  boolean_T maxNumBlobsReached;
  uint8_T currentLabel;
  uint32_T pixIdx;
  uint32_T start_pixIdx;
  uint32_T stackIdx;
  int32_T n;
  int32_T inIdx;
  int32_T m;
  int32_T bufIdx;
  real32_T scale;
  real32_T Autothreshold3_P_DW[256];
  real32_T Autothreshold3_MU_DW[256];
  real32_T cnt;
  int32_T i;
  char_T b_2[13];
  int32_T i_0;
  real32_T centroidsInt_data[32];
  real32_T blobs_centroid_data[32];
  int16_T blobs_orientation_data[16];
  int8_T allBots_data[32];
  real32_T b_data[18];
  int16_T c_data[17];
  real32_T tmp_data[32];
  int8_T allBots_data_0[32];
  real32_T blobs_centroid_data_0;
  int16_T c_x;
  real32_T s_idx_0;
  real32_T rtb_ballVec_idx_1;
  real32_T rtb_ballVec_idx_0;
  int8_T rtb_Ball_y_idx_0;
  int8_T rtb_Ball_x_idx_0;
  int32_T i_1;
  int16_T u1;
  int16_T tmp;

  /* Reset subsysRan breadcrumbs */
  srClearBC(cameraTest_DW.FunctionCallSubsystem_SubsysRan);

  /* Reset subsysRan breadcrumbs */
  srClearBC(cameraTest_DW.Subsystem.Subsystem_SubsysRanBC);

  /* Reset subsysRan breadcrumbs */
  srClearBC(cameraTest_DW.Subsystem4_SubsysRanBC);

  /* S-Function (fcncallgen): '<Root>/Function-Call Generator' incorporates:
   *  SubSystem: '<Root>/Function-Call Subsystem'
   */
  /* MATLAB Function: '<S1>/MATLAB Function' */
  /* MATLAB Function 'Function-Call Subsystem/MATLAB Function': '<S3>:1' */
  /* '<S3>:1:11' */
  /* '<S3>:1:8' */
  /* '<S3>:1:7' */
  for (i = 0; i < 35; i++) {
    rtb_dataOut[i] = 0U;
  }

  /* '<S3>:1:8' */
  for (i_0 = 0; i_0 < 8; i_0++) {
    rtb_dataOut[i_0] = b_0[i_0];
  }

  /* '<S3>:1:9' */
  rtb_dataOut[8] = 85U;
  rtb_dataOut[9] = 98U;
  rtb_dataOut[10] = 98U;

  /* '<S3>:1:11' */
  for (i_0 = 0; i_0 < 24; i_0++) {
    rtb_dataOut[11 + i_0] = d_0[i_0];
  }

  /* '<S3>:1:12' */
  if (cameraTest_DW.Psend == 0) {
    /* '<S3>:1:13' */
    /* '<S3>:1:14' */
    cameraTest_DW.Psend = 1U;
  } else {
    /* '<S3>:1:16' */
    cameraTest_DW.Psend = 0U;
  }

  /* ManualSwitch: '<S1>/Manual Switch' incorporates:
   *  Constant: '<S1>/Constant'
   *  Constant: '<S1>/Constant1'
   */
  /* '<S3>:1:18' */
  if (cameraTest_P.ManualSwitch_CurrentSetting == 1) {
    rtb_Product = cameraTest_P.Constant_Value;
  } else {
    rtb_Product = cameraTest_P.Constant1_Value;
  }

  /* End of ManualSwitch: '<S1>/Manual Switch' */

  /* Product: '<S1>/Product' incorporates:
   *  MATLAB Function: '<S1>/MATLAB Function'
   */
  rtb_Product *= (real_T)cameraTest_DW.Psend;

  /* MATLAB Function: '<S4>/SerialRead' */
  /* MATLAB Function 'Function-Call Subsystem/SerialCom/SerialRead': '<S7>:1' */
  /* '<S7>:1:4' */
  memset(&dataRead[0], 0, sizeof(uint8_T) << 9U);

  /* '<S7>:1:5' */
  dataLength = 0;
  if (cameraTest_DW.spHandle > 0) {
    /* '<S7>:1:7' */
    /* '<S7>:1:8' */
    bytesRead = read(cameraTest_DW.spHandle, dataRead, 512U);
    if (bytesRead > 0) {
      /* '<S7>:1:9' */
      /* '<S7>:1:10' */
      dataLength = bytesRead;
    }
  }

  /* '<S7>:1:14' */
  if (1 > dataLength) {
    cameraTest_DW.SFunction_DIMS2_k = 0;
  } else {
    cameraTest_DW.SFunction_DIMS2_k = dataLength;
  }

  /* End of MATLAB Function: '<S4>/SerialRead' */

  /* Outputs for Atomic SubSystem: '<S4>/Function-Call Subsystem' */
  /* MATLAB Function: '<S5>/SerialWrite' */
  for (i_0 = 0; i_0 < 35; i_0++) {
    dataOut[i_0] = rtb_dataOut[i_0];
  }

  /* MATLAB Function 'Function-Call Subsystem/SerialCom/Function-Call Subsystem/SerialWrite': '<S8>:1' */
  /* '<S8>:1:3' */
  if (rtb_Product > 0.0) {
    /* '<S8>:1:4' */
    write(cameraTest_DW.spHandle, dataOut, 35U);
  }

  /* End of MATLAB Function: '<S5>/SerialWrite' */
  /* End of Outputs for SubSystem: '<S4>/Function-Call Subsystem' */

  /* MATLAB Function: '<S4>/SerialInitilization' */
  /* MATLAB Function 'Function-Call Subsystem/SerialCom/SerialInitilization': '<S6>:1' */
  /* '<S6>:1:6' */
  if (cameraTest_DW.spHandle == -1) {
    /* '<S6>:1:4' */
    /* '<S6>:1:5' */
    cameraTest_DW.spHandle = 0;

    /* 57600 */
    /* '<S6>:1:6' */
    for (i_0 = 0; i_0 < 13; i_0++) {
      b_2[i_0] = b_1[i_0];
    }

    cameraTest_DW.spHandle = setupSerialPort(b_2, 9600U);
  }

  /* End of MATLAB Function: '<S4>/SerialInitilization' */
  cameraTest_DW.FunctionCallSubsystem_SubsysRan = 4;

  /* S-Function (v4l2_video_capture_sfcn): '<Root>/V4L2 Video Capture' */
  MW_videoCaptureOutput(cameraTest_ConstP.V4L2VideoCapture_p1,
                        cameraTest_B.V4L2VideoCapture_o1,
                        cameraTest_B.V4L2VideoCapture_o2,
                        cameraTest_B.V4L2VideoCapture_o3);

  /* DataTypeConversion: '<S9>/Data Type Conversion' */
  for (i = 0; i < 76800; i++) {
    cameraTest_B.Gain1[i] = cameraTest_B.V4L2VideoCapture_o1[i];
  }

  /* End of DataTypeConversion: '<S9>/Data Type Conversion' */

  /* Gain: '<S9>/Gain1' */
  for (i_0 = 0; i_0 < 76800; i_0++) {
    cameraTest_B.Gain1[i_0] *= cameraTest_P.Gain1_Gain;
  }

  /* End of Gain: '<S9>/Gain1' */

  /* DataTypeConversion: '<S9>/Data Type Conversion1' */
  for (i = 0; i < 76800; i++) {
    cameraTest_B.Gain2[i] = cameraTest_B.V4L2VideoCapture_o2[i];
  }

  /* End of DataTypeConversion: '<S9>/Data Type Conversion1' */

  /* Gain: '<S9>/Gain2' */
  for (i_0 = 0; i_0 < 76800; i_0++) {
    cameraTest_B.Gain2[i_0] *= cameraTest_P.Gain2_Gain;
  }

  /* End of Gain: '<S9>/Gain2' */

  /* DataTypeConversion: '<S9>/Data Type Conversion2' */
  for (i = 0; i < 76800; i++) {
    cameraTest_B.Gain3[i] = cameraTest_B.V4L2VideoCapture_o3[i];
  }

  /* End of DataTypeConversion: '<S9>/Data Type Conversion2' */

  /* Gain: '<S9>/Gain3' */
  for (i_0 = 0; i_0 < 76800; i_0++) {
    cameraTest_B.Gain3[i_0] *= cameraTest_P.Gain3_Gain;
  }

  /* End of Gain: '<S9>/Gain3' */

  /* S-Function (svipcolorconv): '<S9>/Color Space  Conversion' */
  /* temporary variables for in-place operation */
  for (i = 0; i < 76800; i++) {
    /* First get the min and max of the RGB triplet */
    if (cameraTest_B.Gain1[i] > cameraTest_B.Gain2[i]) {
      if ((cameraTest_B.Gain2[i] <= cameraTest_B.Gain3[i]) || rtIsNaNF
          (cameraTest_B.Gain3[i])) {
        cnt = cameraTest_B.Gain2[i];
      } else {
        cnt = cameraTest_B.Gain3[i];
      }

      if ((cameraTest_B.Gain1[i] >= cameraTest_B.Gain3[i]) || rtIsNaNF
          (cameraTest_B.Gain3[i])) {
        scale = cameraTest_B.Gain1[i];
      } else {
        scale = cameraTest_B.Gain3[i];
      }
    } else {
      if ((cameraTest_B.Gain1[i] <= cameraTest_B.Gain3[i]) || rtIsNaNF
          (cameraTest_B.Gain3[i])) {
        cnt = cameraTest_B.Gain1[i];
      } else {
        cnt = cameraTest_B.Gain3[i];
      }

      if ((cameraTest_B.Gain2[i] >= cameraTest_B.Gain3[i]) || rtIsNaNF
          (cameraTest_B.Gain3[i])) {
        scale = cameraTest_B.Gain2[i];
      } else {
        scale = cameraTest_B.Gain3[i];
      }
    }

    cnt = scale - cnt;
    if (scale != 0.0F) {
      t = cnt / scale;
    } else {
      t = 0.0F;
    }

    if (cnt != 0.0F) {
      if (cameraTest_B.Gain1[i] == scale) {
        cnt = (cameraTest_B.Gain2[i] - cameraTest_B.Gain3[i]) / cnt;
      } else if (cameraTest_B.Gain2[i] == scale) {
        cnt = (cameraTest_B.Gain3[i] - cameraTest_B.Gain1[i]) / cnt + 2.0F;
      } else {
        cnt = (cameraTest_B.Gain1[i] - cameraTest_B.Gain2[i]) / cnt + 4.0F;
      }

      cnt /= 6.0F;
      if (cnt < 0.0F) {
        cnt++;
      }
    } else {
      cnt = 0.0F;
    }

    /* assign the results */
    cameraTest_B.ColorSpaceConversion_o1[i] = cnt;
    cameraTest_B.ColorSpaceConversion_o2[i] = t;
    cameraTest_B.ColorSpaceConversion_o3[i] = scale;
  }

  /* End of S-Function (svipcolorconv): '<S9>/Color Space  Conversion' */

  /* S-Function (svipgraythresh): '<S9>/Autothreshold3' */
  for (i = 0; i < 256; i++) {
    Autothreshold3_P_DW[i] = 0.0F;
    Autothreshold3_MU_DW[i] = 0.0F;
  }

  for (i = 0; i < 76800; i++) {
    if (cameraTest_B.ColorSpaceConversion_o3[i] < 0.0F) {
      Autothreshold3_P_DW[0]++;
    } else if (cameraTest_B.ColorSpaceConversion_o3[i] > 1.0F) {
      Autothreshold3_P_DW[255]++;
    } else {
      Autothreshold3_P_DW[(uint8_T)(cameraTest_B.ColorSpaceConversion_o3[i] *
        255.0F + 0.5F)]++;
    }
  }

  for (i = 0; i < 256; i++) {
    Autothreshold3_P_DW[i] /= 76800.0F;
  }

  Autothreshold3_MU_DW[0] = Autothreshold3_P_DW[0];
  cnt = 2.0F;
  for (i = 0; i < 255; i++) {
    Autothreshold3_MU_DW[i + 1] = Autothreshold3_P_DW[i + 1] * cnt +
      Autothreshold3_MU_DW[i];
    cnt++;
  }

  for (i = 0; i < 255; i++) {
    Autothreshold3_P_DW[i + 1] += Autothreshold3_P_DW[i];
  }

  for (i = 0; i < 256; i++) {
    s_idx_0 = Autothreshold3_P_DW[i] * Autothreshold3_MU_DW[255] -
      Autothreshold3_MU_DW[i];
    s_idx_0 *= s_idx_0;
    Autothreshold3_P_DW[i] = s_idx_0 / ((1.0F - Autothreshold3_P_DW[i]) *
      Autothreshold3_P_DW[i]);
  }

  dataLength = 0;
  cnt = 0.0F;
  for (i = 0; i < 256; i++) {
    if (Autothreshold3_P_DW[i] > cnt) {
      cnt = Autothreshold3_P_DW[i];
      dataLength = i;
    }
  }

  cnt = (real32_T)dataLength / 255.0F;
  cameraTest_B.Autothreshold3_o2 = cnt;
  for (i = 0; i < 76800; i++) {
    cameraTest_B.Autothreshold3_o1[i] = (cameraTest_B.ColorSpaceConversion_o3[i]
      > cnt);
  }

  /* End of S-Function (svipgraythresh): '<S9>/Autothreshold3' */

  /* Saturate: '<S9>/Saturation' */
  if (cameraTest_B.Autothreshold3_o2 > cameraTest_P.Saturation_UpperSat) {
    cameraTest_B.Saturation = cameraTest_P.Saturation_UpperSat;
  } else if (cameraTest_B.Autothreshold3_o2 < cameraTest_P.Saturation_LowerSat)
  {
    cameraTest_B.Saturation = cameraTest_P.Saturation_LowerSat;
  } else {
    cameraTest_B.Saturation = cameraTest_B.Autothreshold3_o2;
  }

  /* End of Saturate: '<S9>/Saturation' */

  /* RelationalOperator: '<S14>/Compare' incorporates:
   *  Constant: '<S14>/Constant'
   *  Sum: '<S9>/Add2'
   */
  for (i = 0; i < 76800; i++) {
    cameraTest_B.Compare_m[i] = (cameraTest_B.ColorSpaceConversion_o3[i] -
      cameraTest_B.Saturation > cameraTest_P.Constant_Value_f);
  }

  /* End of RelationalOperator: '<S14>/Compare' */

  /* S-Function (svipmorphop): '<S9>/Erosion' */
  dataLength = 0;
  bytesRead = 0;
  for (i = 0; i < 2; i++) {
    for (m = 0; m < 331; m++) {
      cameraTest_DW.Erosion_ONE_PAD_IMG_DW[dataLength] = true;
      dataLength++;
    }
  }

  for (i = 0; i < 240; i++) {
    cameraTest_DW.Erosion_ONE_PAD_IMG_DW[dataLength] = true;
    dataLength++;
    cameraTest_DW.Erosion_ONE_PAD_IMG_DW[dataLength] = true;
    dataLength++;
    memcpy(&cameraTest_DW.Erosion_ONE_PAD_IMG_DW[dataLength],
           &cameraTest_B.Compare_m[bytesRead], 320U * sizeof(boolean_T));
    dataLength += 320;
    bytesRead += 320;
    for (m = 0; m < 9; m++) {
      cameraTest_DW.Erosion_ONE_PAD_IMG_DW[dataLength] = true;
      dataLength++;
    }
  }

  for (i = 0; i < 9; i++) {
    for (m = 0; m < 331; m++) {
      cameraTest_DW.Erosion_ONE_PAD_IMG_DW[dataLength] = true;
      dataLength++;
    }
  }

  for (i = 0; i < 83081; i++) {
    cameraTest_DW.Erosion_TWO_PAD_IMG_DW[i] = true;
  }

  bufIdx = 0;
  for (n = 0; n < 251; n++) {
    for (m = 0; m < 326; m++) {
      cameraTest_DW.Erosion_TWO_PAD_IMG_DW[2 + bufIdx] = true;
      i = 0;
      while (i < cameraTest_DW.Erosion_NUMNONZ_DW[0]) {
        if (!cameraTest_DW.Erosion_ONE_PAD_IMG_DW[bufIdx +
            cameraTest_DW.Erosion_ERODE_OFF_DW[i]]) {
          cameraTest_DW.Erosion_TWO_PAD_IMG_DW[2 + bufIdx] = false;
          i = cameraTest_DW.Erosion_NUMNONZ_DW[0];
        }

        i++;
      }

      bufIdx++;
    }

    bufIdx += 5;
  }

  bytesRead = 0;
  inIdx = 2;
  bufIdx = 0;
  for (n = 0; n < 240; n++) {
    for (m = 2; m < 322; m++) {
      cameraTest_B.Erosion[bufIdx] = true;
      i = 0;
      while (i < cameraTest_DW.Erosion_NUMNONZ_DW[1]) {
        if (!cameraTest_DW.Erosion_TWO_PAD_IMG_DW[cameraTest_DW.Erosion_ERODE_OFF_DW
            [i + cameraTest_DW.Erosion_NUMNONZ_DW[0]] + inIdx]) {
          cameraTest_B.Erosion[bufIdx] = false;
          i = cameraTest_DW.Erosion_NUMNONZ_DW[1];
        }

        i++;
      }

      inIdx++;
      bufIdx++;
    }

    inIdx += 11;
  }

  /* End of S-Function (svipmorphop): '<S9>/Erosion' */

  /* MATLAB Function: '<S9>/MATLAB Function2' */
  /* MATLAB Function 'Raspi Code/Blob extraction /MATLAB Function2': '<S17>:1' */
  /* % ball vals */
  /* '<S17>:1:4' */
  /* '<S17>:1:5' */
  /*  ballBool=zeros(320,240,'uint8'); */
  /*   */
  /* '<S17>:1:8' */
  for (i = 0; i < 76800; i++) {
    cameraTest_B.Compare_m[i] = ((cameraTest_B.ColorSpaceConversion_o1[i] >=
      0.8F) && (cameraTest_B.ColorSpaceConversion_o2[i] >= 0.401F) &&
      (cameraTest_B.ColorSpaceConversion_o3[i] >= 0.401F));
  }

  /* End of MATLAB Function: '<S9>/MATLAB Function2' */

  /* S-Function (svipmorphop): '<S9>/Dilation' incorporates:
   *  Constant: '<S9>/Constant5'
   */
  dataLength = 0;
  inIdx = 0;
  for (n = 0; n < 3; n++) {
    for (m = 0; m < 3; m++) {
      if (cameraTest_P.Constant5_Value[dataLength]) {
        cameraTest_DW.Dilation_DILATE_OFF_DW[bytesRead] = n * 325 + m;
        inIdx++;
        bytesRead++;
      }

      dataLength++;
    }
  }

  cameraTest_DW.Dilation_NUMNONZ_DW = inIdx;
  for (i = 0; i < 79625; i++) {
    cameraTest_DW.Dilation_ONE_PAD_IMG_DW[i] = false;
  }

  for (inIdx = 0; inIdx < 76800; inIdx++) {
    if (cameraTest_B.Compare_m[inIdx]) {
      dataLength = div_s32(inIdx, 320);
      bufIdx = (inIdx - dataLength * 320) + dataLength * 325;
      for (i = 0; i < cameraTest_DW.Dilation_NUMNONZ_DW; i++) {
        cameraTest_DW.Dilation_ONE_PAD_IMG_DW[bufIdx +
          cameraTest_DW.Dilation_DILATE_OFF_DW[i]] = true;
      }
    }
  }

  inIdx = 326;
  bufIdx = 0;
  for (n = 0; n < 240; n++) {
    for (m = 0; m < 320; m++) {
      cameraTest_B.Compare[bufIdx] = cameraTest_DW.Dilation_ONE_PAD_IMG_DW[inIdx];
      bufIdx++;
      inIdx++;
    }

    inIdx += 5;
  }

  /* End of S-Function (svipmorphop): '<S9>/Dilation' */

  /* S-Function (svipmorphop): '<S9>/Erosion1' incorporates:
   *  Constant: '<S9>/Constant6'
   */
  inIdx = 0;
  n = 0;
  while (n < 1) {
    m = 0;
    while (m < 1) {
      if (cameraTest_P.Constant6_Value_o) {
        cameraTest_DW.Erosion1_ERODE_OFF_DW = 0;
        inIdx++;
      }

      m = 1;
    }

    n = 1;
  }

  cameraTest_DW.Erosion1_NUMNONZ_DW = inIdx;
  dataLength = 0;
  bytesRead = 0;
  for (i = 0; i < 240; i++) {
    memcpy(&cameraTest_DW.Erosion1_ONE_PAD_IMG_DW[dataLength],
           &cameraTest_B.Compare[bytesRead], 320U * sizeof(boolean_T));
    bytesRead += 320;
    cameraTest_DW.Erosion1_ONE_PAD_IMG_DW[dataLength + 320] = true;
    dataLength += 321;
  }

  for (m = 0; m < 321; m++) {
    cameraTest_DW.Erosion1_ONE_PAD_IMG_DW[dataLength] = true;
    dataLength++;
  }

  inIdx = 0;
  bufIdx = 0;
  for (n = 0; n < 240; n++) {
    for (m = 0; m < 320; m++) {
      cameraTest_B.Erosion1[bufIdx] = true;
      i = 0;
      while (i < cameraTest_DW.Erosion1_NUMNONZ_DW) {
        if (!cameraTest_DW.Erosion1_ONE_PAD_IMG_DW[inIdx +
            cameraTest_DW.Erosion1_ERODE_OFF_DW]) {
          cameraTest_B.Erosion1[bufIdx] = false;
          i = cameraTest_DW.Erosion1_NUMNONZ_DW;
        }

        i++;
      }

      inIdx++;
      bufIdx++;
    }

    inIdx++;
  }

  /* End of S-Function (svipmorphop): '<S9>/Erosion1' */

  /* RelationalOperator: '<S15>/Compare' incorporates:
   *  Constant: '<S15>/Constant'
   *  Sum: '<S9>/Add'
   */
  for (i = 0; i < 76800; i++) {
    cameraTest_B.Compare[i] = ((int32_T)((uint32_T)cameraTest_B.Erosion[i] +
      cameraTest_B.Erosion1[i]) > cameraTest_P.Constant_Value_a);
  }

  /* End of RelationalOperator: '<S15>/Compare' */

  /* S-Function (svipblob): '<S9>/Blob Analysis1' */
  maxNumBlobsReached = false;
  memset(&cameraTest_DW.BlobAnalysis1_PAD_DW[0], 0, 323U * sizeof(uint8_T));
  currentLabel = 1U;
  i = 0;
  bytesRead = 323;
  for (n = 0; n < 240; n++) {
    for (m = 0; m < 320; m++) {
      cameraTest_DW.BlobAnalysis1_PAD_DW[bytesRead] = (uint8_T)
        (cameraTest_B.Compare[i] ? 255 : 0);
      i++;
      bytesRead++;
    }

    cameraTest_DW.BlobAnalysis1_PAD_DW[bytesRead] = 0U;
    cameraTest_DW.BlobAnalysis1_PAD_DW[bytesRead + 1] = 0U;
    bytesRead += 2;
  }

  memset(&cameraTest_DW.BlobAnalysis1_PAD_DW[bytesRead], 0, 321U * sizeof
         (uint8_T));
  dataLength = 0;
  pixIdx = 0U;
  n = 0;
  while (n < 240) {
    bytesRead = 0;
    bufIdx = (dataLength + 1) * 322;
    m = 0;
    while (m < 320) {
      numBlobs = (uint32_T)((bufIdx + bytesRead) + 1);
      start_pixIdx = pixIdx;
      if (cameraTest_DW.BlobAnalysis1_PAD_DW[numBlobs] == 255) {
        cameraTest_DW.BlobAnalysis1_PAD_DW[numBlobs] = currentLabel;
        cameraTest_DW.BlobAnalysis1_N_PIXLIST_DW[pixIdx] = (int16_T)dataLength;
        cameraTest_DW.BlobAnalysis1_M_PIXLIST_DW[pixIdx] = (int16_T)bytesRead;
        pixIdx++;
        BlobAnalysis1_NUM_PIX_DW[currentLabel - 1] = 1U;
        cameraTest_DW.BlobAnalysis1_STACK_DW[0U] = numBlobs;
        stackIdx = 1U;
        while (stackIdx != 0U) {
          stackIdx--;
          numBlobs = cameraTest_DW.BlobAnalysis1_STACK_DW[stackIdx];
          for (i = 0; i < 8; i++) {
            k = numBlobs + cameraTest_ConstP.BlobAnalysis1_WALKER_[i];
            if (cameraTest_DW.BlobAnalysis1_PAD_DW[k] == 255) {
              cameraTest_DW.BlobAnalysis1_PAD_DW[k] = currentLabel;
              cameraTest_DW.BlobAnalysis1_N_PIXLIST_DW[pixIdx] = (int16_T)
                ((int16_T)(k / 322U) - 1);
              cameraTest_DW.BlobAnalysis1_M_PIXLIST_DW[pixIdx] = (int16_T)(k %
                322U - 1U);
              pixIdx++;
              BlobAnalysis1_NUM_PIX_DW[currentLabel - 1]++;
              cameraTest_DW.BlobAnalysis1_STACK_DW[stackIdx] = k;
              stackIdx++;
            }
          }
        }

        if (BlobAnalysis1_NUM_PIX_DW[currentLabel - 1] <
            cameraTest_P.BlobAnalysis1_minArea) {
          currentLabel--;
          pixIdx = start_pixIdx;
        }

        if (currentLabel == 16) {
          maxNumBlobsReached = true;
          n = 240;
          m = 320;
        }

        if (m < 320) {
          currentLabel++;
        }
      }

      bytesRead++;
      m++;
    }

    dataLength++;
    n++;
  }

  numBlobs = (uint32_T)(maxNumBlobsReached ? (int32_T)currentLabel : (int32_T)
                        (uint8_T)(currentLabel - 1U));
  dataLength = 0;
  bytesRead = 0;
  for (i = 0; i < (int32_T)numBlobs; i++) {
    bufIdx = 0;
    inIdx = 0;
    for (m = 0; m < (int32_T)BlobAnalysis1_NUM_PIX_DW[i]; m++) {
      bufIdx += cameraTest_DW.BlobAnalysis1_N_PIXLIST_DW[m + bytesRead];
      inIdx += cameraTest_DW.BlobAnalysis1_M_PIXLIST_DW[m + dataLength];
    }

    rtb_ballVec_idx_0 = (real32_T)inIdx / (real32_T)BlobAnalysis1_NUM_PIX_DW[i];
    rtb_ballVec_idx_1 = (real32_T)bufIdx / (real32_T)BlobAnalysis1_NUM_PIX_DW[i];
    rtb_BlobAnalysis1_o1[i] = rtb_ballVec_idx_1 + 1.0F;
    rtb_BlobAnalysis1_o1[numBlobs + i] = rtb_ballVec_idx_0 + 1.0F;
    cnt = 0.0F;
    ys = 0.0F;
    xys = 0.0F;
    for (k = 0U; k < BlobAnalysis1_NUM_PIX_DW[i]; k++) {
      t = (real32_T)cameraTest_DW.BlobAnalysis1_N_PIXLIST_DW[bytesRead +
        (int32_T)k] - rtb_ballVec_idx_1;
      scale = (real32_T)cameraTest_DW.BlobAnalysis1_M_PIXLIST_DW[dataLength +
        (int32_T)k] - rtb_ballVec_idx_0;
      cnt += t * t;
      ys += scale * scale;
      xys += t * -scale;
    }

    s_idx_0 = cnt / (real32_T)BlobAnalysis1_NUM_PIX_DW[i] + 0.0833333358F;
    scale = ys / (real32_T)BlobAnalysis1_NUM_PIX_DW[i] + 0.0833333358F;
    cnt = xys / (real32_T)BlobAnalysis1_NUM_PIX_DW[i];
    t = (real32_T)sqrt((s_idx_0 - scale) * (s_idx_0 - scale) + cnt * cnt * 4.0F);
    if (scale > s_idx_0) {
      ys = (scale - s_idx_0) + t;
      cnt *= 2.0F;
    } else {
      ys = 2.0F * cnt;
      cnt = (s_idx_0 - scale) + t;
    }

    rtb_BlobAnalysis1_o2[i] = (real32_T)atan(ys / (cnt + 1.1920929E-7F));
    dataLength += (int32_T)BlobAnalysis1_NUM_PIX_DW[i];
    bytesRead += (int32_T)BlobAnalysis1_NUM_PIX_DW[i];
  }

  cameraTest_DW.BlobAnalysis1_DIMS1[0] = (int32_T)numBlobs;
  cameraTest_DW.BlobAnalysis1_DIMS1[1] = 2;
  cameraTest_DW.BlobAnalysis1_DIMS2[0] = (int32_T)numBlobs;
  cameraTest_DW.BlobAnalysis1_DIMS2[1] = 1;

  /* End of S-Function (svipblob): '<S9>/Blob Analysis1' */

  /* MATLAB Function: '<S9>/FindBlobs1' */
  /* MATLAB Function 'Raspi Code/Blob extraction /FindBlobs1': '<S16>:1' */
  /*  area */
  /* '<S16>:1:4' */
  /* '<S16>:1:5' */
  bufIdx = cameraTest_DW.BlobAnalysis1_DIMS1[0];
  inIdx = cameraTest_DW.BlobAnalysis1_DIMS1[0] *
    cameraTest_DW.BlobAnalysis1_DIMS1[1];
  for (i_0 = 0; i_0 < inIdx; i_0++) {
    centroidsInt_data[i_0] = rtb_BlobAnalysis1_o1[i_0];
  }

  dataLength = cameraTest_DW.BlobAnalysis1_DIMS1[0] *
    cameraTest_DW.BlobAnalysis1_DIMS1[1];
  for (bytesRead = 0; bytesRead < dataLength; bytesRead++) {
    centroidsInt_data[bytesRead] = (real32_T)floor(centroidsInt_data[bytesRead]);
  }

  /* '<S16>:1:6' */
  cameraTest_DW.SFunction_DIMS2_o.centroid[0] = 0;
  cameraTest_DW.SFunction_DIMS2_o.centroid[1] = 2;

  /* '<S16>:1:7' */
  cameraTest_DW.SFunction_DIMS2_o.orientation = 0;

  /* '<S16>:1:10' */
  rtb_ballVec_idx_0 = 0.0F;
  rtb_ballVec_idx_1 = 0.0F;

  /*  check for color inconsistencies */
  /* '<S16>:1:13' */
  for (m = 0; m < cameraTest_DW.BlobAnalysis1_DIMS2[0]; m++) {
    /* '<S16>:1:13' */
    /* '<S16>:1:14' */
    if (!cameraTest_B.Erosion[(((int32_T)centroidsInt_data[m] - 1) * 320 +
         (int32_T)centroidsInt_data[m + bufIdx]) - 1]) {
      /* '<S16>:1:15' */
      /* '<S16>:1:16' */
      rtb_ballVec_idx_0 = centroidsInt_data[m];
      rtb_ballVec_idx_1 = centroidsInt_data[m + bufIdx];
    } else {
      /* '<S16>:1:18' */
      i = cameraTest_DW.SFunction_DIMS2_o.centroid[0];
      bytesRead = cameraTest_DW.SFunction_DIMS2_o.centroid[1];
      inIdx = cameraTest_DW.BlobAnalysis1_DIMS1[1] - 1;
      dataLength = cameraTest_DW.SFunction_DIMS2_o.centroid[0] + 1;
      for (i_0 = 0; i_0 < bytesRead; i_0++) {
        for (i_1 = 0; i_1 < i; i_1++) {
          tmp_data[i_1 + dataLength * i_0] = rtb_BlobAnalysis1_o1[i * i_0 + i_1];
        }
      }

      for (i_0 = 0; i_0 <= inIdx; i_0++) {
        tmp_data[i + dataLength * i_0] = centroidsInt_data[bufIdx * i_0 + m];
      }

      cameraTest_DW.SFunction_DIMS2_o.centroid[0] = dataLength;
      cameraTest_DW.SFunction_DIMS2_o.centroid[1] = bytesRead;
      inIdx = dataLength * bytesRead;
      for (i_0 = 0; i_0 < inIdx; i_0++) {
        rtb_BlobAnalysis1_o1[i_0] = tmp_data[i_0];
      }

      /* '<S16>:1:19' */
      i = cameraTest_DW.SFunction_DIMS2_o.orientation + 1;
      inIdx = cameraTest_DW.SFunction_DIMS2_o.orientation;
      for (i_0 = 0; i_0 < inIdx; i_0++) {
        blobs_orientation_data[i_0] = rtb_robotBlobs_orientation[i_0];
      }

      s_idx_0 = rt_roundf_snf((-rtb_BlobAnalysis1_o2[m] + 1.57079637F) *
        57.2957802F);
      if (s_idx_0 < 32768.0F) {
        if (s_idx_0 >= -32768.0F) {
          blobs_orientation_data[cameraTest_DW.SFunction_DIMS2_o.orientation] =
            (int16_T)s_idx_0;
        } else {
          blobs_orientation_data[cameraTest_DW.SFunction_DIMS2_o.orientation] =
            MIN_int16_T;
        }
      } else {
        blobs_orientation_data[cameraTest_DW.SFunction_DIMS2_o.orientation] =
          MAX_int16_T;
      }

      cameraTest_DW.SFunction_DIMS2_o.orientation = i;
      for (i_0 = 0; i_0 < i; i_0++) {
        rtb_robotBlobs_orientation[i_0] = blobs_orientation_data[i_0];
      }
    }

    /* '<S16>:1:13' */
  }

  /* '<S16>:1:23' */
  /* '<S16>:1:24' */
  cameraTest_B.thing = rtb_robotBlobs_orientation[0];
  cameraTest_B.leee = cameraTest_DW.SFunction_DIMS2_o.orientation;

  /* End of MATLAB Function: '<S9>/FindBlobs1' */

  /* MATLAB Function: '<S2>/parseBlob' incorporates:
   *  Constant: '<S2>/Constant6'
   *  Constant: '<S2>/Constant7'
   */
  /* MATLAB Function 'Raspi Code/parseBlob': '<S13>:1' */
  /*  blobs.area */
  /*  blobIndices */
  /*  blobs.centroid(:,1) */
  /* % Coefficients from curve fitting */
  /* % calc position */
  i = cameraTest_DW.SFunction_DIMS2_o.centroid[0] + 2;
  inIdx = cameraTest_DW.SFunction_DIMS2_o.centroid[0];
  for (i_0 = 0; i_0 < inIdx; i_0++) {
    b_data[i_0] = rtb_BlobAnalysis1_o1[i_0];
  }

  b_data[cameraTest_DW.SFunction_DIMS2_o.centroid[0]] = rtb_ballVec_idx_0;
  b_data[1 + cameraTest_DW.SFunction_DIMS2_o.centroid[0]] = rtb_ballVec_idx_1;

  /* '<S13>:1:14' */
  for (i_0 = 0; i_0 < i; i_0++) {
    blobs_centroid_data[i_0] = b_data[i_0];
  }

  i = cameraTest_DW.SFunction_DIMS2_o.orientation + 1;
  inIdx = cameraTest_DW.SFunction_DIMS2_o.orientation;
  for (i_0 = 0; i_0 < inIdx; i_0++) {
    c_data[i_0] = rtb_robotBlobs_orientation[i_0];
  }

  c_data[cameraTest_DW.SFunction_DIMS2_o.orientation] = 0;

  /* '<S13>:1:15' */
  /* '<S13>:1:17' */
  /* '<S13>:1:18' */
  inIdx = i << 1;
  for (i_0 = 0; i_0 < inIdx; i_0++) {
    allBots_data[i_0] = 0;
  }

  /* '<S13>:1:19' */
  for (m = 0; m <= cameraTest_DW.SFunction_DIMS2_o.orientation; m++) {
    /* '<S13>:1:19' */
    /* '<S13>:1:20' */
    blobs_centroid_data[m] = (real32_T)cameraTest_P.Constant6_Value[1] -
      blobs_centroid_data[m];

    /* '<S13>:1:21' */
    rtb_ballVec_idx_0 = (real32_T)(cameraTest_P.Constant6_Value[1] / 2.0) +
      -blobs_centroid_data[m];
    rtb_ballVec_idx_1 = (real32_T)(cameraTest_P.Constant6_Value[0] / 2.0) +
      blobs_centroid_data_0;

    /* '<S13>:1:22' */
    if (rtb_ballVec_idx_0 < 0.0F) {
      s_idx_0 = -1.0F;
    } else if (rtb_ballVec_idx_0 > 0.0F) {
      s_idx_0 = 1.0F;
    } else if (rtb_ballVec_idx_0 == 0.0F) {
      s_idx_0 = 0.0F;
    } else {
      s_idx_0 = rtb_ballVec_idx_0;
    }

    if (rtb_ballVec_idx_1 < 0.0F) {
      scale = -1.0F;
    } else if (rtb_ballVec_idx_1 > 0.0F) {
      scale = 1.0F;
    } else if (rtb_ballVec_idx_1 == 0.0F) {
      scale = 0.0F;
    } else {
      scale = rtb_ballVec_idx_1;
    }

    /* '<S13>:1:23' */
    rtb_ballVec_idx_0 = (real32_T)fabs(rtb_ballVec_idx_0);
    rtb_ballVec_idx_1 = (real32_T)fabs(rtb_ballVec_idx_1);

    /* '<S13>:1:24' */
    /* '<S13>:1:25' */
    /* '<S13>:1:26' */
    s_idx_0 *= (rtb_ballVec_idx_0 * rtb_ballVec_idx_0 * -0.0002273F + 0.2549F *
                rtb_ballVec_idx_0) + -0.04137F;
    scale *= (rtb_ballVec_idx_1 * rtb_ballVec_idx_1 * -0.0002273F + 0.2549F *
              rtb_ballVec_idx_1) + -0.04137F;
    if (!((!rtIsInfF(s_idx_0)) && (!rtIsNaNF(s_idx_0)))) {
      cnt = (rtNaNF);
    } else {
      cnt = rt_remf_snf(s_idx_0, 360.0F);
      t = (real32_T)fabs(cnt);
      if (t > 180.0F) {
        if (cnt > 0.0F) {
          cnt -= 360.0F;
        } else {
          cnt += 360.0F;
        }

        t = (real32_T)fabs(cnt);
      }

      if (t <= 45.0F) {
        cnt *= 0.0174532924F;
        rtb_Ball_x_idx_0 = 0;
      } else if (t <= 135.0F) {
        if (cnt > 0.0F) {
          cnt = (cnt - 90.0F) * 0.0174532924F;
          rtb_Ball_x_idx_0 = 1;
        } else {
          cnt = (cnt + 90.0F) * 0.0174532924F;
          rtb_Ball_x_idx_0 = -1;
        }
      } else if (cnt > 0.0F) {
        cnt = (cnt - 180.0F) * 0.0174532924F;
        rtb_Ball_x_idx_0 = 2;
      } else {
        cnt = (cnt + 180.0F) * 0.0174532924F;
        rtb_Ball_x_idx_0 = -2;
      }

      cnt = (real32_T)tan(cnt);
      if ((rtb_Ball_x_idx_0 == 1) || (rtb_Ball_x_idx_0 == -1)) {
        cnt = -1.0F / cnt;
      }
    }

    s_idx_0 = cnt;
    if (!((!rtIsInfF(scale)) && (!rtIsNaNF(scale)))) {
      cnt = (rtNaNF);
    } else {
      cnt = rt_remf_snf(scale, 360.0F);
      t = (real32_T)fabs(cnt);
      if (t > 180.0F) {
        if (cnt > 0.0F) {
          cnt -= 360.0F;
        } else {
          cnt += 360.0F;
        }

        t = (real32_T)fabs(cnt);
      }

      if (t <= 45.0F) {
        cnt *= 0.0174532924F;
        rtb_Ball_x_idx_0 = 0;
      } else if (t <= 135.0F) {
        if (cnt > 0.0F) {
          cnt = (cnt - 90.0F) * 0.0174532924F;
          rtb_Ball_x_idx_0 = 1;
        } else {
          cnt = (cnt + 90.0F) * 0.0174532924F;
          rtb_Ball_x_idx_0 = -1;
        }
      } else if (cnt > 0.0F) {
        cnt = (cnt - 180.0F) * 0.0174532924F;
        rtb_Ball_x_idx_0 = 2;
      } else {
        cnt = (cnt + 180.0F) * 0.0174532924F;
        rtb_Ball_x_idx_0 = -2;
      }

      cnt = (real32_T)tan(cnt);
      if ((rtb_Ball_x_idx_0 == 1) || (rtb_Ball_x_idx_0 == -1)) {
        cnt = -1.0F / cnt;
      }
    }

    s_idx_0 *= (real32_T)cameraTest_P.Constant7_Value_e;

    /* '<S13>:1:27' */
    s_idx_0 = rt_roundf_snf(s_idx_0);
    if (s_idx_0 < 128.0F) {
      if (s_idx_0 >= -128.0F) {
        allBots_data[m << 1] = (int8_T)s_idx_0;
      } else {
        allBots_data[m << 1] = MIN_int8_T;
      }
    } else {
      allBots_data[m << 1] = MAX_int8_T;
    }

    s_idx_0 = rt_roundf_snf((real32_T)cameraTest_P.Constant7_Value_e * cnt);
    if (s_idx_0 < 128.0F) {
      if (s_idx_0 >= -128.0F) {
        allBots_data[1 + (m << 1)] = (int8_T)s_idx_0;
      } else {
        allBots_data[1 + (m << 1)] = MIN_int8_T;
      }
    } else {
      allBots_data[1 + (m << 1)] = MAX_int8_T;
    }

    /* '<S13>:1:19' */
  }

  /* '<S13>:1:29' */
  cameraTest_DW.SFunction_DIMS3.x = 1;
  rtb_Ball_x_idx_0 = allBots_data[(i - 1) << 1];

  /* '<S13>:1:30' */
  cameraTest_DW.SFunction_DIMS3.y = 1;
  rtb_Ball_y_idx_0 = allBots_data[((i - 1) << 1) + 1];

  /* '<S13>:1:31' */
  if (1 > i - 1) {
    i = 1;
  }

  inIdx = i - 2;
  i = inIdx + 1;
  for (i_0 = 0; i_0 <= inIdx; i_0++) {
    allBots_data_0[i_0 << 1] = allBots_data[i_0 << 1];
    allBots_data_0[1 + (i_0 << 1)] = allBots_data[(i_0 << 1) + 1];
  }

  for (i_0 = 0; i_0 < i; i_0++) {
    allBots_data[i_0 << 1] = allBots_data_0[i_0 << 1];
    allBots_data[1 + (i_0 << 1)] = allBots_data_0[(i_0 << 1) + 1];
  }

  /* '<S13>:1:33' */
  cameraTest_DW.SFunction_DIMS2.x[0] = 0;
  cameraTest_DW.SFunction_DIMS2.x[1] = 1;

  /* '<S13>:1:34' */
  cameraTest_DW.SFunction_DIMS2.y[0] = 0;
  cameraTest_DW.SFunction_DIMS2.y[1] = 1;

  /* '<S13>:1:35' */
  cameraTest_DW.SFunction_DIMS2.orientation[0] = 0;
  cameraTest_DW.SFunction_DIMS2.orientation[1] = 1;

  /* '<S13>:1:36' */
  cameraTest_DW.SFunction_DIMS2.color[0] = 0;
  cameraTest_DW.SFunction_DIMS2.color[1] = 1;

  /* '<S13>:1:38' */
  if (0 == i) {
    n = -1;
  } else if (2 > i) {
    n = 1;
  } else {
    n = i - 1;
  }

  /* '<S13>:1:38' */
  for (dataLength = 0; dataLength <= n; dataLength++) {
    /* '<S13>:1:38' */
    /* '<S13>:1:39' */
    cameraTest_DW.SFunction_DIMS2.x[0] = 1;
    cameraTest_DW.SFunction_DIMS2.x[1] = 1;
    rtb_players_x[0] = allBots_data[dataLength << 1];

    /* '<S13>:1:40' */
    cameraTest_DW.SFunction_DIMS2.y[0] = 1;
    cameraTest_DW.SFunction_DIMS2.y[1] = 1;
    rtb_players_y[0] = allBots_data[(dataLength << 1) + 1];

    /* '<S13>:1:41' */
    cameraTest_DW.SFunction_DIMS2.orientation[0] = 1;
    cameraTest_DW.SFunction_DIMS2.orientation[1] = 1;
    rtb_players_orientation[0] = c_data[dataLength];

    /* '<S13>:1:42' */
    cameraTest_DW.SFunction_DIMS2.color[0] = 1;
    cameraTest_DW.SFunction_DIMS2.color[1] = 1;

    /* '<S13>:1:38' */
  }

  /* End of MATLAB Function: '<S2>/parseBlob' */

  /* MATLAB Function: '<S2>/Player Tracker' */
  /* MATLAB Function 'Raspi Code/Player Tracker': '<S11>:1' */
  /* '<S11>:1:19' */
  /* '<S11>:1:18' */
  /*  fGol, fDef, fOff, eGol, eDef, eOff */
  if (!cameraTest_DW.latestRawOrientation_not_empty) {
    /* '<S11>:1:7' */
    /* '<S11>:1:8' */
    cameraTest_DW.latestRawOrientation_sizes[0] =
      cameraTest_DW.SFunction_DIMS2.orientation[0];
    cameraTest_DW.latestRawOrientation_sizes[1] =
      cameraTest_DW.SFunction_DIMS2.orientation[1];
    inIdx = cameraTest_DW.SFunction_DIMS2.orientation[0] *
      cameraTest_DW.SFunction_DIMS2.orientation[1];
    for (i_0 = 0; i_0 < inIdx; i_0++) {
      cameraTest_DW.latestRawOrientation_data[i_0] = rtb_players_orientation[i_0];
    }

    cameraTest_DW.latestRawOrientation_not_empty =
      !((cameraTest_DW.latestRawOrientation_sizes[0] == 0) ||
        (cameraTest_DW.latestRawOrientation_sizes[1] == 0));
  }

  /* Green */
  if (!cameraTest_DW.latestPlayers_not_empty) {
    /* '<S11>:1:12' */
    /* '<S11>:1:13' */
    cameraTest_DW.latestPlayers_not_empty = true;

    /* '<S11>:1:15' */
    /* '<S11>:1:16' */
    /* '<S11>:1:18' */
    /* '<S11>:1:19' */
    /* '<S11>:1:20' */
    for (i_0 = 0; i_0 < 6; i_0++) {
      cameraTest_DW.latestPlayers.x[i_0] = b[i_0];
      cameraTest_DW.latestPlayers.y[i_0] = 0;
      cameraTest_DW.latestPlayers.orientation[i_0] = c[i_0];
      cameraTest_DW.latestPlayers.color[i_0] = d[i_0];
      cameraTest_DW.latestPlayers.position[i_0] = e[i_0];
      cameraTest_DW.latestPlayers.valid[i_0] = 0U;
    }
  }

  /* Ball */
  if (!cameraTest_DW.latestBall_not_empty) {
    /* '<S11>:1:23' */
    /* '<S11>:1:24' */
    cameraTest_DW.latestBall.x = 0;
    cameraTest_DW.latestBall_not_empty = true;

    /* '<S11>:1:25' */
    cameraTest_DW.latestBall.y = 0;

    /* '<S11>:1:26' */
    cameraTest_DW.latestBall.valid = 1U;
  }

  /* % set all valids to false */
  /* '<S11>:1:30' */
  for (i_0 = 0; i_0 < 6; i_0++) {
    cameraTest_DW.latestPlayers.valid[i_0] = 0U;
  }

  /* '<S11>:1:31' */
  cameraTest_DW.latestBall.valid = 0U;
  if (!(cameraTest_DW.SFunction_DIMS3.x == 0)) {
    /* '<S11>:1:33' */
    /* '<S11>:1:34' */
    cameraTest_DW.latestBall.x = rtb_Ball_x_idx_0;

    /* '<S11>:1:35' */
    cameraTest_DW.latestBall.y = rtb_Ball_y_idx_0;

    /* '<S11>:1:36' */
    cameraTest_DW.latestBall.valid = 1U;
  }

  /* '<S11>:1:39' */
  if ((0 == cameraTest_DW.SFunction_DIMS2.x[0]) || (0 ==
       cameraTest_DW.SFunction_DIMS2.x[1])) {
    i_0 = 0;
  } else if (cameraTest_DW.SFunction_DIMS2.x[0] >=
             cameraTest_DW.SFunction_DIMS2.x[1]) {
    i_0 = cameraTest_DW.SFunction_DIMS2.x[0];
  } else {
    i_0 = cameraTest_DW.SFunction_DIMS2.x[1];
  }

  n = i_0 - 1;

  /* '<S11>:1:39' */
  for (m = 0; m <= n; m++) {
    /* '<S11>:1:39' */
    /* '<S11>:1:40' */
    /* '<S11>:1:41' */
    bufIdx = MAX_int32_T;

    /* '<S11>:1:42' */
    currentLabel = 0U;

    /* '<S11>:1:43' */
    for (inIdx = 0; inIdx < 6; inIdx++) {
      /* '<S11>:1:43' */
      if (0 == cameraTest_DW.latestPlayers.color[inIdx]) {
        /* '<S11>:1:44' */
        /* '<S11>:1:45' */
        i_0 = rtb_players_x[m] - cameraTest_DW.latestPlayers.x[inIdx];
        if (i_0 > 127) {
          i_0 = 127;
        } else {
          if (i_0 < -128) {
            i_0 = -128;
          }
        }

        i_1 = rtb_players_y[m] - cameraTest_DW.latestPlayers.y[inIdx];
        if (i_1 > 127) {
          i_1 = 127;
        } else {
          if (i_1 < -128) {
            i_1 = -128;
          }
        }

        scale = 1.17549435E-38F;
        dataLength = (int32_T)(real32_T)fabs((int8_T)i_0);
        if (dataLength > 1.17549435E-38F) {
          t = 1.17549435E-38F / (real32_T)dataLength;
          cnt = 0.0F * t * t + 1.0F;
          scale = (real32_T)dataLength;
        } else {
          t = (real32_T)dataLength / 1.17549435E-38F;
          cnt = t * t;
        }

        dataLength = (int32_T)(real32_T)fabs((int8_T)i_1);
        if (dataLength > scale) {
          t = scale / (real32_T)dataLength;
          cnt = cnt * t * t + 1.0F;
          scale = (real32_T)dataLength;
        } else {
          t = (real32_T)dataLength / scale;
          cnt += t * t;
        }

        cnt = scale * (real32_T)sqrt(cnt);
        s_idx_0 = rt_roundf_snf(cnt);
        if (s_idx_0 < 2.14748365E+9F) {
          if (s_idx_0 >= -2.14748365E+9F) {
            i_0 = (int32_T)s_idx_0;
          } else {
            i_0 = MIN_int32_T;
          }
        } else {
          i_0 = MAX_int32_T;
        }

        if ((i_0 < bufIdx) && (i_0 < 10)) {
          /* '<S11>:1:46' */
          /* '<S11>:1:47' */
          bufIdx = i_0;

          /* '<S11>:1:48' */
          currentLabel = (uint8_T)(1 + inIdx);
        }
      }

      /* '<S11>:1:43' */
    }

    /*      T=mod(playerData.orientation(ii)+360,360); */
    /*      M=mod(latestPlayers.orientation(index)+360,360); */
    /*      realDorien=min(abs(T-M),abs(latestPlayers.orientation(index-playerData.orientation(ii)))); */
    /*      if abs(T-M)<abs(targetO-myO) */
    /*          direc=sign(T-M); */
    /*      else */
    /*          direc=sign(targetO-myO); */
    /*      end */
    /*      realDorien=realDorien*direc; */
    /* '<S11>:1:66' */
    i_0 = rtb_players_orientation[m] - cameraTest_DW.latestRawOrientation_data[m];
    if (i_0 > 32767) {
      i_0 = 32767;
    } else {
      if (i_0 < -32768) {
        i_0 = -32768;
      }
    }

    /* '<S11>:1:67' */
    i_1 = rtb_players_orientation[m] - cameraTest_DW.latestRawOrientation_data[m];
    if (i_1 > 32767) {
      i_1 = 32767;
    } else {
      if (i_1 < -32768) {
        i_1 = -32768;
      }
    }

    i = rtb_players_orientation[m] - cameraTest_DW.latestRawOrientation_data[m];
    if (i > 32767) {
      i = 32767;
    } else {
      if (i < -32768) {
        i = -32768;
      }
    }

    if ((int16_T)i < 0) {
      i = -(int16_T)i;
      if (i > 32767) {
        i = 32767;
      }

      c_x = (int16_T)i;
    } else {
      c_x = (int16_T)i;
    }

    i = 90 + c_x;
    if (i > 32767) {
      i = 32767;
    }

    /* '<S11>:1:69' */
    cameraTest_DW.latestPlayers.x[currentLabel - 1] = rtb_players_x[m];

    /* '<S11>:1:70' */
    cameraTest_DW.latestPlayers.y[currentLabel - 1] = rtb_players_y[m];

    /* '<S11>:1:71' */
    if ((int16_T)i_1 < 0) {
      i_1 = -(int16_T)i_1;
      if (i_1 > 32767) {
        i_1 = 32767;
      }

      c_x = (int16_T)i_1;
    } else {
      c_x = (int16_T)i_1;
    }

    u1 = (int16_T)((int16_T)i - (int16_T)(div_s16s32_floor((int16_T)i, 90) * 90));
    if ((int16_T)i_0 > 0) {
      tmp = 1;
    } else if ((int16_T)i_0 < 0) {
      tmp = -1;
    } else {
      tmp = 0;
    }

    if (c_x <= u1) {
      u1 = c_x;
    }

    i_0 = tmp * u1;
    if (i_0 > 32767) {
      i_0 = 32767;
    }

    i_0 += cameraTest_DW.latestPlayers.orientation[currentLabel - 1];
    if (i_0 > 32767) {
      i_0 = 32767;
    } else {
      if (i_0 < -32768) {
        i_0 = -32768;
      }
    }

    cameraTest_DW.latestPlayers.orientation[currentLabel - 1] = (int16_T)i_0;

    /* '<S11>:1:72' */
    cameraTest_DW.latestPlayers.valid[currentLabel - 1] = 1U;

    /* '<S11>:1:73' */
    cameraTest_DW.latestRawOrientation_data[m] = rtb_players_orientation[m];

    /* '<S11>:1:39' */
  }

  /* '<S11>:1:76' */
  /* '<S11>:1:77' */
  /* '<S11>:1:78' */
  /* '<S11>:1:79' */
  /* '<S11>:1:80' */
  /* '<S11>:1:81' */
  /* '<S11>:1:82' */
  /* '<S11>:1:83' */
  /* '<S11>:1:84' */
  for (dataLength = 0; dataLength < 6; dataLength++) {
    /* '<S11>:1:84' */
    /* '<S11>:1:85' */
    rtb_players_g[dataLength].x = cameraTest_DW.latestPlayers.x[dataLength];

    /* '<S11>:1:86' */
    rtb_players_g[dataLength].y = cameraTest_DW.latestPlayers.y[dataLength];

    /* '<S11>:1:87' */
    rtb_players_g[dataLength].orientation =
      cameraTest_DW.latestPlayers.orientation[dataLength];

    /* '<S11>:1:88' */
    rtb_players_g[dataLength].color =
      cameraTest_DW.latestPlayers.color[dataLength];

    /* '<S11>:1:89' */
    rtb_players_g[dataLength].position =
      cameraTest_DW.latestPlayers.position[dataLength];

    /* '<S11>:1:90' */
    rtb_players_g[dataLength].valid =
      cameraTest_DW.latestPlayers.valid[dataLength];

    /* '<S11>:1:84' */
  }

  /* End of MATLAB Function: '<S2>/Player Tracker' */

  /* SignalConversion: '<S2>/SigConversion_InsertedFor_Bus Selector_at_outport_0' */
  cameraTest_B.x = rtb_players_g[5].x;

  /* SignalConversion: '<S2>/SigConversion_InsertedFor_Bus Selector_at_outport_1' */
  cameraTest_B.y = rtb_players_g[5].y;

  /* SignalConversion: '<S2>/SigConversion_InsertedFor_Bus Selector_at_outport_2' */
  cameraTest_B.orientation = rtb_players_g[5].orientation;

  /* ManualSwitch: '<S9>/Manual Switch' incorporates:
   *  Constant: '<S9>/Constant1'
   *  Constant: '<S9>/Constant2'
   */
  if (cameraTest_P.ManualSwitch_CurrentSetting_o == 1) {
    cameraTest_B.ManualSwitch = cameraTest_P.Constant1_Value_b;
  } else {
    cameraTest_B.ManualSwitch = cameraTest_P.Constant2_Value;
  }

  /* End of ManualSwitch: '<S9>/Manual Switch' */

  /* Outputs for Enabled SubSystem: '<S9>/Subsystem' */
  cameraTest_Subsystem(cameraTest_B.ManualSwitch, &cameraTest_DW.Subsystem);

  /* End of Outputs for SubSystem: '<S9>/Subsystem' */

  /* ManualSwitch: '<S9>/Manual Switch1' incorporates:
   *  Constant: '<S9>/Constant3'
   *  Constant: '<S9>/Constant7'
   */
  if (cameraTest_P.ManualSwitch1_CurrentSetting == 1) {
    cameraTest_B.ManualSwitch1 = cameraTest_P.Constant3_Value;
  } else {
    cameraTest_B.ManualSwitch1 = cameraTest_P.Constant7_Value;
  }

  /* End of ManualSwitch: '<S9>/Manual Switch1' */

  /* Outputs for Enabled SubSystem: '<S9>/Subsystem1' */
  cameraTest_Subsystem(cameraTest_B.ManualSwitch1, &cameraTest_DW.Subsystem1);

  /* End of Outputs for SubSystem: '<S9>/Subsystem1' */

  /* Outputs for Enabled SubSystem: '<S9>/Subsystem4' incorporates:
   *  EnablePort: '<S20>/Enable'
   */
  /* Constant: '<S9>/Constant4' */
  if (cameraTest_P.Constant4_Value > 0.0) {
    /* S-Function (sdl_video_display_sfcn): '<S20>/SDL Video Display1' */
    memcpy(&cameraTest_B.uv0[0], &cameraTest_B.V4L2VideoCapture_o1[0], 76800U *
           sizeof(uint8_T));
    memcpy(&cameraTest_B.uv1[0], &cameraTest_B.V4L2VideoCapture_o2[0], 76800U *
           sizeof(uint8_T));
    memcpy(&cameraTest_B.uv2[0], &cameraTest_B.V4L2VideoCapture_o3[0], 76800U *
           sizeof(uint8_T));
    MW_SDL_videoDisplayOutput(cameraTest_B.uv0, cameraTest_B.uv1,
      cameraTest_B.uv2);
    srUpdateBC(cameraTest_DW.Subsystem4_SubsysRanBC);
  }

  /* End of Constant: '<S9>/Constant4' */
  /* End of Outputs for SubSystem: '<S9>/Subsystem4' */
}

/* Model update function */
void cameraTest_update(void)
{
  /* Update for Enabled SubSystem: '<S9>/Subsystem' */
  cameraTest_Subsystem_Update(&cameraTest_DW.Subsystem);

  /* End of Update for SubSystem: '<S9>/Subsystem' */

  /* Update for Enabled SubSystem: '<S9>/Subsystem1' */
  cameraTest_Subsystem_Update(&cameraTest_DW.Subsystem1);

  /* End of Update for SubSystem: '<S9>/Subsystem1' */

  /* signal main to stop simulation */
  {                                    /* Sample time: [0.1s, 0.0s] */
    if ((rtmGetTFinal(cameraTest_M)!=-1) &&
        !((rtmGetTFinal(cameraTest_M)-cameraTest_M->Timing.taskTime0) >
          cameraTest_M->Timing.taskTime0 * (DBL_EPSILON))) {
      rtmSetErrorStatus(cameraTest_M, "Simulation finished");
    }

    if (rtmGetStopRequested(cameraTest_M)) {
      rtmSetErrorStatus(cameraTest_M, "Simulation finished");
    }
  }

  /* Update absolute time for base rate */
  /* The "clockTick0" counts the number of times the code of this task has
   * been executed. The absolute time is the multiplication of "clockTick0"
   * and "Timing.stepSize0". Size of "clockTick0" ensures timer will not
   * overflow during the application lifespan selected.
   */
  cameraTest_M->Timing.taskTime0 =
    (++cameraTest_M->Timing.clockTick0) * cameraTest_M->Timing.stepSize0;
}

/* Model initialize function */
void cameraTest_initialize(void)
{
  /* Registration code */

  /* initialize non-finites */
  rt_InitInfAndNaN(sizeof(real_T));

  /* initialize real-time model */
  (void) memset((void *)cameraTest_M, 0,
                sizeof(RT_MODEL_cameraTest_T));
  rtmSetTFinal(cameraTest_M, -1);
  cameraTest_M->Timing.stepSize0 = 0.1;

  /* External mode info */
  cameraTest_M->Sizes.checksums[0] = (3811906598U);
  cameraTest_M->Sizes.checksums[1] = (1404961330U);
  cameraTest_M->Sizes.checksums[2] = (1617336905U);
  cameraTest_M->Sizes.checksums[3] = (1534525246U);

  {
    static const sysRanDType rtAlwaysEnabled = SUBSYS_RAN_BC_ENABLE;
    static RTWExtModeInfo rt_ExtModeInfo;
    static const sysRanDType *systemRan[20];
    cameraTest_M->extModeInfo = (&rt_ExtModeInfo);
    rteiSetSubSystemActiveVectorAddresses(&rt_ExtModeInfo, systemRan);
    systemRan[0] = &rtAlwaysEnabled;
    systemRan[1] = (sysRanDType *)&cameraTest_DW.FunctionCallSubsystem_SubsysRan;
    systemRan[2] = (sysRanDType *)&cameraTest_DW.FunctionCallSubsystem_SubsysRan;
    systemRan[3] = (sysRanDType *)&cameraTest_DW.FunctionCallSubsystem_SubsysRan;
    systemRan[4] = (sysRanDType *)&cameraTest_DW.FunctionCallSubsystem_SubsysRan;
    systemRan[5] = (sysRanDType *)&cameraTest_DW.FunctionCallSubsystem_SubsysRan;
    systemRan[6] = (sysRanDType *)&cameraTest_DW.FunctionCallSubsystem_SubsysRan;
    systemRan[7] = (sysRanDType *)&cameraTest_DW.FunctionCallSubsystem_SubsysRan;
    systemRan[8] = (sysRanDType *)&cameraTest_DW.FunctionCallSubsystem_SubsysRan;
    systemRan[9] = &rtAlwaysEnabled;
    systemRan[10] = &rtAlwaysEnabled;
    systemRan[11] = &rtAlwaysEnabled;
    systemRan[12] = &rtAlwaysEnabled;
    systemRan[13] = &rtAlwaysEnabled;
    systemRan[14] = &rtAlwaysEnabled;
    systemRan[15] = (sysRanDType *)
      &cameraTest_DW.Subsystem.Subsystem_SubsysRanBC;
    systemRan[16] = (sysRanDType *)
      &cameraTest_DW.Subsystem1.Subsystem_SubsysRanBC;
    systemRan[17] = (sysRanDType *)&cameraTest_DW.Subsystem4_SubsysRanBC;
    systemRan[18] = &rtAlwaysEnabled;
    systemRan[19] = &rtAlwaysEnabled;
    rteiSetModelMappingInfoPtr(cameraTest_M->extModeInfo,
      &cameraTest_M->SpecialInfo.mappingInfo);
    rteiSetChecksumsPtr(cameraTest_M->extModeInfo, cameraTest_M->Sizes.checksums);
    rteiSetTPtr(cameraTest_M->extModeInfo, rtmGetTPtr(cameraTest_M));
  }

  /* block I/O */
  (void) memset(((void *) &cameraTest_B), 0,
                sizeof(B_cameraTest_T));

  /* states (dwork) */
  (void) memset((void *)&cameraTest_DW, 0,
                sizeof(DW_cameraTest_T));

  /* data type transition information */
  {
    static DataTypeTransInfo dtInfo;
    (void) memset((char_T *) &dtInfo, 0,
                  sizeof(dtInfo));
    cameraTest_M->SpecialInfo.mappingInfo = (&dtInfo);
    dtInfo.numDataTypes = 23;
    dtInfo.dataTypeSizes = &rtDataTypeSizes[0];
    dtInfo.dataTypeNames = &rtDataTypeNames[0];

    /* Block I/O transition table */
    dtInfo.B = &rtBTransTable;

    /* Parameters transition table */
    dtInfo.P = &rtPTransTable;
  }

  {
    int32_T idxOffsets;
    int32_T curNumNonZ;
    int32_T n;
    int32_T m;

    /* Start for S-Function (fcncallgen): '<Root>/Function-Call Generator' incorporates:
     *  Start for SubSystem: '<Root>/Function-Call Subsystem'
     */
    /* Start for DataStoreMemory: '<S4>/Data Store Memory' */
    cameraTest_DW.spHandle = cameraTest_P.DataStoreMemory_InitialValue;

    /* Start for S-Function (v4l2_video_capture_sfcn): '<Root>/V4L2 Video Capture' */
    MW_videoCaptureInit(cameraTest_ConstP.V4L2VideoCapture_p1, 0, 0, 0, 0, 320U,
                        240U, 2U, 2U, 1U, 0.1);

    /* Start for S-Function (svipmorphop): '<S9>/Erosion' */
    idxOffsets = 0;
    curNumNonZ = 0;
    n = 0;
    while (n < 1) {
      for (m = 0; m < 6; m++) {
        cameraTest_DW.Erosion_ERODE_OFF_DW[idxOffsets] = m;
        curNumNonZ++;
        idxOffsets++;
      }

      n = 1;
    }

    cameraTest_DW.Erosion_NUMNONZ_DW[0] = curNumNonZ;
    curNumNonZ = 0;
    for (n = 0; n < 6; n++) {
      m = 0;
      while (m < 1) {
        cameraTest_DW.Erosion_ERODE_OFF_DW[idxOffsets] = n * 331;
        curNumNonZ++;
        idxOffsets++;
        m = 1;
      }
    }

    cameraTest_DW.Erosion_NUMNONZ_DW[1] = curNumNonZ;

    /* End of Start for S-Function (svipmorphop): '<S9>/Erosion' */

    /* Start for Enabled SubSystem: '<S9>/Subsystem4' */
    /* Start for S-Function (sdl_video_display_sfcn): '<S20>/SDL Video Display1' */
    MW_SDL_videoDisplayInit(1, 1, 1, 320, 240);

    /* End of Start for SubSystem: '<S9>/Subsystem4' */

    /* InitializeConditions for S-Function (fcncallgen): '<Root>/Function-Call Generator' incorporates:
     *  InitializeConditions for SubSystem: '<Root>/Function-Call Subsystem'
     */
    /* InitializeConditions for MATLAB Function: '<S1>/MATLAB Function' */
    cameraTest_DW.Psend = 0U;

    /* InitializeConditions for MATLAB Function: '<S2>/Player Tracker' */
    cameraTest_DW.latestPlayers_not_empty = false;
    cameraTest_DW.latestBall_not_empty = false;
    cameraTest_DW.latestRawOrientation_not_empty = false;
  }
}

/* Model terminate function */
void cameraTest_terminate(void)
{
  /* Terminate for S-Function (v4l2_video_capture_sfcn): '<Root>/V4L2 Video Capture' */
  MW_videoCaptureTerminate(cameraTest_ConstP.V4L2VideoCapture_p1);

  /* Terminate for Enabled SubSystem: '<S9>/Subsystem4' */
  /* Terminate for S-Function (sdl_video_display_sfcn): '<S20>/SDL Video Display1' */
  MW_SDL_videoDisplayTerminate(320, 240);

  /* End of Terminate for SubSystem: '<S9>/Subsystem4' */
}

/*
 * File trailer for generated code.
 *
 * [EOF]
 */

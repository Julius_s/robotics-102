rpi=raspi();
% cam=cameraboard(rpi);
%%
clear cam
clear hBlob
cam=cameraboard(rpi,'Resolution','320x240','FrameRate', 30,'Contrast',80);
t=[];
bb=[];
hBlob = vision.BlobAnalysis;
hBlob.BoundingBoxOutputPort=0;
hBlob.MinimumBlobArea=5;
while(1==1)
    tic;
Im= snapshot(cam);
I=rgb2hsv(Im);
%%common v
channel3Min=0.4;
% channel2Min=0.4;
%% hue vals
channel1MinRed = 0.8;
channel1MaxRed = 0.04;

channel1BlueMin = 0.45;
channel1BlueMax = 0.61;
% channel3BlueMin = 0.5;

%% ball vals
redBool = ((I(:,:,1) >= channel1MinRed ) | I(:,:,1) <= channel1MaxRed) & ...
          I(:,:,3) >= channel3Min ;   
blueBool = (I(:,:,1) >= channel1BlueMin ) & (I(:,:,1)<= channel1BlueMax) & ...
          I(:,:,3) >= channel3Min ;
   
%%
  bool= imdilate((redBool+blueBool)>0,ones(2));
 bool= imerode(bool,strel('square',4));
[area,centroids]=step(hBlob,bool);
%%
blobs.centroid=zeros(0,2,'single');
blobs.area=zeros(0,1,'int32');
blobs.color=zeros(0,1,'uint8');
for ii = 1:length(area)
    cr=RBW(centroidsInt(ii,2),centroidsInt(ii,1));
    cb=BBW(centroidsInt(ii,2),centroidsInt(ii,1));
    colorVec=[cr cb];
    color=find(colorVec);
    if length(color)==1
        blobs.centroid=[blobs.centroid ; centroids(ii,:)];        
        blobs.area=[blobs.area ; area(ii)];
        blobs.color=[ blobs.color ;uint8(color(1,1))] ;
    end
end

% toc
% t=[t toc];
end
/*
 * File: ImageDet_data.c
 *
 * Code generated for Simulink model 'ImageDet'.
 *
 * Model version                  : 1.200
 * Simulink Coder version         : 8.7 (R2014b) 08-Sep-2014
 * TLC version                    : 8.7 (Aug  5 2014)
 * C/C++ source code generated on : Sat Mar 07 16:53:42 2015
 *
 * Target selection: realtime.tlc
 * Embedded hardware selection: ARM Compatible->ARM Cortex
 * Code generation objectives: Unspecified
 * Validation result: Not run
 */

#include "ImageDet.h"
#include "ImageDet_private.h"

/* Block parameters (auto storage) */
P_ImageDet_T ImageDet_P = {
  0.0,                                 /* Expression: 0
                                        * Referenced by: '<S3>/Constant9'
                                        */
  1.0,                                 /* Expression: 1
                                        * Referenced by: '<S3>/Constant8'
                                        */
  0.00392156886F,                      /* Computed Parameter: Gain1_Gain
                                        * Referenced by: '<S3>/Gain1'
                                        */
  0.00392156886F,                      /* Computed Parameter: Gain2_Gain
                                        * Referenced by: '<S3>/Gain2'
                                        */
  0.00392156886F,                      /* Computed Parameter: Gain3_Gain
                                        * Referenced by: '<S3>/Gain3'
                                        */
  0U,                                  /* Computed Parameter: Constant_Value
                                        * Referenced by: '<S8>/Constant'
                                        */
  0U,                                  /* Computed Parameter: ManualSwitch2_CurrentSetting
                                        * Referenced by: '<S3>/Manual Switch2'
                                        */

  /*  Computed Parameter: Constant10_Value
   * Referenced by: '<S3>/Constant10'
   */
  { 1, 1, 1, 1 }
};

/* Constant parameters (auto storage) */
const ConstP_ImageDet_T ImageDet_ConstP = {
  /* Expression: devName
   * Referenced by: '<Root>/V4L2 Video Capture'
   */
  { 47U, 100U, 101U, 118U, 47U, 118U, 105U, 100U, 101U, 111U, 48U, 0U }
};

/*
 * File trailer for generated code.
 *
 * [EOF]
 */

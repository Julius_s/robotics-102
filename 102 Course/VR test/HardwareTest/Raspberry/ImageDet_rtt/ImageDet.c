/*
 * File: ImageDet.c
 *
 * Code generated for Simulink model 'ImageDet'.
 *
 * Model version                  : 1.200
 * Simulink Coder version         : 8.7 (R2014b) 08-Sep-2014
 * TLC version                    : 8.7 (Aug  5 2014)
 * C/C++ source code generated on : Sat Mar 07 16:53:42 2015
 *
 * Target selection: realtime.tlc
 * Embedded hardware selection: ARM Compatible->ARM Cortex
 * Code generation objectives: Unspecified
 * Validation result: Not run
 */

#include "ImageDet.h"
#include "ImageDet_private.h"
#include "ImageDet_dt.h"

/* Block signals (auto storage) */
B_ImageDet_T ImageDet_B;

/* Block states (auto storage) */
DW_ImageDet_T ImageDet_DW;

/* Real-time model */
RT_MODEL_ImageDet_T ImageDet_M_;
RT_MODEL_ImageDet_T *const ImageDet_M = &ImageDet_M_;
static void rate_monotonic_scheduler(void);
int32_T div_s32(int32_T numerator, int32_T denominator)
{
  int32_T quotient;
  uint32_T tempAbsQuotient;
  if (denominator == 0) {
    quotient = numerator >= 0 ? MAX_int32_T : MIN_int32_T;

    /* Divide by zero handler */
  } else {
    tempAbsQuotient = (uint32_T)(numerator >= 0 ? numerator : -numerator) /
      (denominator >= 0 ? denominator : -denominator);
    quotient = (numerator < 0) != (denominator < 0) ? -(int32_T)tempAbsQuotient :
      (int32_T)tempAbsQuotient;
  }

  return quotient;
}

/*
 *   This function updates active task flag for each subrate
 * and rate transition flags for tasks that exchange data.
 * The function assumes rate-monotonic multitasking scheduler.
 * The function must be called at model base rate so that
 * the generated code self-manages all its subrates and rate
 * transition flags.
 */
static void rate_monotonic_scheduler(void)
{
  /* To ensure a deterministic data transfer between two rates,
   * data is transferred at the priority of a fast task and the frequency
   * of the slow task.  The following flags indicate when the data transfer
   * happens.  That is, a rate interaction flag is set true when both rates
   * will run, and false otherwise.
   */

  /* tid 0 shares data with slower tid rate: 1 */
  ImageDet_M->Timing.RateInteraction.TID0_1 =
    (ImageDet_M->Timing.TaskCounters.TID[1] == 0);

  /* Compute which subrates run during the next base time step.  Subrates
   * are an integer multiple of the base rate counter.  Therefore, the subtask
   * counter is reset when it reaches its limit (zero means run).
   */
  (ImageDet_M->Timing.TaskCounters.TID[1])++;
  if ((ImageDet_M->Timing.TaskCounters.TID[1]) > 2) {/* Sample time: [0.1s, 0.0s] */
    ImageDet_M->Timing.TaskCounters.TID[1] = 0;
  }
}

/* Model output function for TID0 */
void ImageDet_output0(void)            /* Sample time: [0.033333333333333333s, 0.0s] */
{
  {                                    /* Sample time: [0.033333333333333333s, 0.0s] */
    rate_monotonic_scheduler();
  }

  /* S-Function (v4l2_video_capture_sfcn): '<Root>/V4L2 Video Capture' */
  MW_videoCaptureOutput(ImageDet_ConstP.V4L2VideoCapture_p1,
                        ImageDet_B.V4L2VideoCapture_o1,
                        ImageDet_B.V4L2VideoCapture_o2,
                        ImageDet_B.V4L2VideoCapture_o3);

  /* RateTransition: '<Root>/Rate Transition' */
  if (ImageDet_M->Timing.RateInteraction.TID0_1) {
    memcpy(&ImageDet_B.RateTransition[0], &ImageDet_B.V4L2VideoCapture_o1[0],
           76800U * sizeof(uint8_T));

    /* RateTransition: '<Root>/Rate Transition1' */
    memcpy(&ImageDet_B.RateTransition1[0], &ImageDet_B.V4L2VideoCapture_o2[0],
           76800U * sizeof(uint8_T));

    /* RateTransition: '<Root>/Rate Transition2' */
    memcpy(&ImageDet_B.RateTransition2[0], &ImageDet_B.V4L2VideoCapture_o3[0],
           76800U * sizeof(uint8_T));
  }

  /* End of RateTransition: '<Root>/Rate Transition' */
}

/* Model update function for TID0 */
void ImageDet_update0(void)            /* Sample time: [0.033333333333333333s, 0.0s] */
{
  /* signal main to stop simulation */
  {                                    /* Sample time: [0.033333333333333333s, 0.0s] */
    if ((rtmGetTFinal(ImageDet_M)!=-1) &&
        !((rtmGetTFinal(ImageDet_M)-ImageDet_M->Timing.taskTime0) >
          ImageDet_M->Timing.taskTime0 * (DBL_EPSILON))) {
      rtmSetErrorStatus(ImageDet_M, "Simulation finished");
    }

    if (rtmGetStopRequested(ImageDet_M)) {
      rtmSetErrorStatus(ImageDet_M, "Simulation finished");
    }
  }

  /* Update absolute time */
  /* The "clockTick0" counts the number of times the code of this task has
   * been executed. The absolute time is the multiplication of "clockTick0"
   * and "Timing.stepSize0". Size of "clockTick0" ensures timer will not
   * overflow during the application lifespan selected.
   */
  ImageDet_M->Timing.taskTime0 =
    (++ImageDet_M->Timing.clockTick0) * ImageDet_M->Timing.stepSize0;
}

/* Model output function for TID1 */
void ImageDet_output1(void)            /* Sample time: [0.1s, 0.0s] */
{
  real32_T min;
  real32_T max;
  int32_T n;
  int32_T inIdx;
  int32_T m;
  int32_T idxNHood;
  int32_T idxOffsets;
  int32_T i;
  real_T tmp;

  /* Reset subsysRan breadcrumbs */
  srClearBC(ImageDet_DW.Subsystem2_SubsysRanBC);
  for (i = 0; i < 76800; i++) {
    /* Gain: '<S3>/Gain1' incorporates:
     *  DataTypeConversion: '<S3>/Data Type Conversion'
     */
    ImageDet_B.Gain1[i] = ImageDet_P.Gain1_Gain * (real32_T)
      ImageDet_B.RateTransition[i];

    /* Gain: '<S3>/Gain2' incorporates:
     *  DataTypeConversion: '<S3>/Data Type Conversion1'
     */
    ImageDet_B.Gain2[i] = ImageDet_P.Gain2_Gain * (real32_T)
      ImageDet_B.RateTransition1[i];

    /* Gain: '<S3>/Gain3' incorporates:
     *  DataTypeConversion: '<S3>/Data Type Conversion2'
     */
    ImageDet_B.Gain3[i] = ImageDet_P.Gain3_Gain * (real32_T)
      ImageDet_B.RateTransition2[i];
  }

  /* S-Function (svipcolorconv): '<S3>/Color Space  Conversion' */
  /* temporary variables for in-place operation */
  for (i = 0; i < 76800; i++) {
    /* First get the min and max of the RGB triplet */
    if (ImageDet_B.Gain1[i] > ImageDet_B.Gain2[i]) {
      if ((ImageDet_B.Gain2[i] <= ImageDet_B.Gain3[i]) || rtIsNaNF
          (ImageDet_B.Gain3[i])) {
        min = ImageDet_B.Gain2[i];
      } else {
        min = ImageDet_B.Gain3[i];
      }

      if ((ImageDet_B.Gain1[i] >= ImageDet_B.Gain3[i]) || rtIsNaNF
          (ImageDet_B.Gain3[i])) {
        max = ImageDet_B.Gain1[i];
      } else {
        max = ImageDet_B.Gain3[i];
      }
    } else {
      if ((ImageDet_B.Gain1[i] <= ImageDet_B.Gain3[i]) || rtIsNaNF
          (ImageDet_B.Gain3[i])) {
        min = ImageDet_B.Gain1[i];
      } else {
        min = ImageDet_B.Gain3[i];
      }

      if ((ImageDet_B.Gain2[i] >= ImageDet_B.Gain3[i]) || rtIsNaNF
          (ImageDet_B.Gain3[i])) {
        max = ImageDet_B.Gain2[i];
      } else {
        max = ImageDet_B.Gain3[i];
      }
    }

    min = max - min;
    if (min != 0.0F) {
      if (ImageDet_B.Gain1[i] == max) {
        min = (ImageDet_B.Gain2[i] - ImageDet_B.Gain3[i]) / min;
      } else if (ImageDet_B.Gain2[i] == max) {
        min = (ImageDet_B.Gain3[i] - ImageDet_B.Gain1[i]) / min + 2.0F;
      } else {
        min = (ImageDet_B.Gain1[i] - ImageDet_B.Gain2[i]) / min + 4.0F;
      }

      min /= 6.0F;
      if (min < 0.0F) {
        min++;
      }
    } else {
      min = 0.0F;
    }

    /* assign the results */
    ImageDet_B.ColorSpaceConversion_o1[i] = min;
    ImageDet_B.ColorSpaceConversion_o3[i] = max;
  }

  /* End of S-Function (svipcolorconv): '<S3>/Color Space  Conversion' */

  /* RelationalOperator: '<S8>/Compare' incorporates:
   *  Constant: '<S8>/Constant'
   *  MATLAB Function: '<S3>/MATLAB Function1'
   *  Sum: '<S3>/Add1'
   */
  /* MATLAB Function 'Raspi Code/Blob extraction /MATLAB Function1': '<S10>:1' */
  /* %common v */
  /* '<S10>:1:3' */
  /*  channel2Min=0.4; */
  /* % hue vals */
  /* '<S10>:1:6' */
  /* '<S10>:1:7' */
  /* '<S10>:1:9' */
  /* '<S10>:1:10' */
  /*  channel3BlueMin = 0.5; */
  /* % ball vals */
  /* '<S10>:1:17' */
  /* '<S10>:1:20' */
  for (i = 0; i < 76800; i++) {
    ImageDet_B.Compare[i] = ((int32_T)((uint32_T)
      (((ImageDet_B.ColorSpaceConversion_o1[i] >= 0.75F) ||
        (ImageDet_B.ColorSpaceConversion_o1[i] <= 0.25F)) &&
       (ImageDet_B.ColorSpaceConversion_o3[i] >= 0.8)) +
      ((ImageDet_B.ColorSpaceConversion_o1[i] >= 0.25F) &&
       (ImageDet_B.ColorSpaceConversion_o1[i] <= 0.75F) &&
       (ImageDet_B.ColorSpaceConversion_o3[i] >= 0.8))) >
      ImageDet_P.Constant_Value);
  }

  /* End of RelationalOperator: '<S8>/Compare' */

  /* S-Function (svipmorphop): '<S3>/Dilation1' incorporates:
   *  Constant: '<S3>/Constant10'
   */
  idxNHood = 0;
  idxOffsets = 0;
  inIdx = 0;
  for (n = 0; n < 2; n++) {
    for (m = 0; m < 2; m++) {
      if (ImageDet_P.Constant10_Value[idxNHood]) {
        ImageDet_DW.Dilation1_DILATE_OFF_DW[idxOffsets] = n * 323 + m;
        inIdx++;
        idxOffsets++;
      }

      idxNHood++;
    }
  }

  ImageDet_DW.Dilation1_NUMNONZ_DW = inIdx;
  for (i = 0; i < 78489; i++) {
    ImageDet_DW.Dilation1_ONE_PAD_IMG_DW[i] = false;
  }

  for (inIdx = 0; inIdx < 76800; inIdx++) {
    if (ImageDet_B.Compare[inIdx]) {
      idxNHood = div_s32(inIdx, 320);
      idxNHood = (inIdx - idxNHood * 320) + idxNHood * 323;
      for (i = 0; i < ImageDet_DW.Dilation1_NUMNONZ_DW; i++) {
        ImageDet_DW.Dilation1_ONE_PAD_IMG_DW[idxNHood +
          ImageDet_DW.Dilation1_DILATE_OFF_DW[i]] = true;
      }
    }
  }

  idxNHood = 0;
  idxOffsets = 0;
  inIdx = 0;
  for (n = 0; n < 240; n++) {
    for (m = 0; m < 320; m++) {
      ImageDet_B.Dilation1[idxNHood] =
        ImageDet_DW.Dilation1_ONE_PAD_IMG_DW[inIdx];
      idxNHood++;
      inIdx++;
    }

    inIdx += 3;
  }

  /* End of S-Function (svipmorphop): '<S3>/Dilation1' */

  /* S-Function (svipmorphop): '<S3>/Erosion3' */
  idxNHood = 0;
  for (inIdx = 0; inIdx < 327; inIdx++) {
    ImageDet_DW.Erosion3_ONE_PAD_IMG_DW[idxNHood] = true;
    idxNHood++;
  }

  for (i = 0; i < 240; i++) {
    ImageDet_DW.Erosion3_ONE_PAD_IMG_DW[idxNHood] = true;
    memcpy(&ImageDet_DW.Erosion3_ONE_PAD_IMG_DW[idxNHood + 1],
           &ImageDet_B.Dilation1[idxOffsets], 320U * sizeof(boolean_T));
    idxNHood += 321;
    idxOffsets += 320;
    for (inIdx = 0; inIdx < 6; inIdx++) {
      ImageDet_DW.Erosion3_ONE_PAD_IMG_DW[idxNHood] = true;
      idxNHood++;
    }
  }

  for (i = 0; i < 6; i++) {
    for (inIdx = 0; inIdx < 327; inIdx++) {
      ImageDet_DW.Erosion3_ONE_PAD_IMG_DW[idxNHood] = true;
      idxNHood++;
    }
  }

  for (i = 0; i < 80769; i++) {
    ImageDet_DW.Erosion3_TWO_PAD_IMG_DW[i] = true;
  }

  inIdx = 0;
  for (n = 0; n < 247; n++) {
    for (m = 0; m < 324; m++) {
      ImageDet_DW.Erosion3_TWO_PAD_IMG_DW[1 + inIdx] = true;
      i = 0;
      while (i < ImageDet_DW.Erosion3_NUMNONZ_DW[0]) {
        if (!ImageDet_DW.Erosion3_ONE_PAD_IMG_DW[inIdx +
            ImageDet_DW.Erosion3_ERODE_OFF_DW[i]]) {
          ImageDet_DW.Erosion3_TWO_PAD_IMG_DW[1 + inIdx] = false;
          i = ImageDet_DW.Erosion3_NUMNONZ_DW[0];
        }

        i++;
      }

      inIdx++;
    }

    inIdx += 3;
  }

  inIdx = 1;
  idxNHood = 0;
  for (n = 0; n < 240; n++) {
    for (m = 1; m < 321; m++) {
      ImageDet_B.Erosion3[idxNHood] = true;
      i = 0;
      while (i < ImageDet_DW.Erosion3_NUMNONZ_DW[1]) {
        if (!ImageDet_DW.Erosion3_TWO_PAD_IMG_DW[ImageDet_DW.Erosion3_ERODE_OFF_DW
            [i + ImageDet_DW.Erosion3_NUMNONZ_DW[0]] + inIdx]) {
          ImageDet_B.Erosion3[idxNHood] = false;
          i = ImageDet_DW.Erosion3_NUMNONZ_DW[1];
        }

        i++;
      }

      inIdx++;
      idxNHood++;
    }

    inIdx += 7;
  }

  /* End of S-Function (svipmorphop): '<S3>/Erosion3' */

  /* ManualSwitch: '<S3>/Manual Switch2' incorporates:
   *  Constant: '<S3>/Constant8'
   *  Constant: '<S3>/Constant9'
   */
  if (ImageDet_P.ManualSwitch2_CurrentSetting == 1) {
    tmp = ImageDet_P.Constant8_Value;
  } else {
    tmp = ImageDet_P.Constant9_Value;
  }

  /* End of ManualSwitch: '<S3>/Manual Switch2' */

  /* Outputs for Enabled SubSystem: '<S3>/Subsystem2' incorporates:
   *  EnablePort: '<S12>/Enable'
   */
  if (tmp > 0.0) {
    srUpdateBC(ImageDet_DW.Subsystem2_SubsysRanBC);
  }

  /* End of Outputs for SubSystem: '<S3>/Subsystem2' */
}

/* Model update function for TID1 */
void ImageDet_update1(void)            /* Sample time: [0.1s, 0.0s] */
{
  /* Update absolute time */
  /* The "clockTick1" counts the number of times the code of this task has
   * been executed. The resolution of this integer timer is 0.1, which is the step size
   * of the task. Size of "clockTick1" ensures timer will not overflow during the
   * application lifespan selected.
   */
  ImageDet_M->Timing.clockTick1++;
}

/* Model initialize function */
void ImageDet_initialize(void)
{
  /* Registration code */

  /* initialize non-finites */
  rt_InitInfAndNaN(sizeof(real_T));

  /* initialize real-time model */
  (void) memset((void *)ImageDet_M, 0,
                sizeof(RT_MODEL_ImageDet_T));
  rtmSetTFinal(ImageDet_M, -1);
  ImageDet_M->Timing.stepSize0 = 0.033333333333333333;

  /* External mode info */
  ImageDet_M->Sizes.checksums[0] = (1518382788U);
  ImageDet_M->Sizes.checksums[1] = (3906709913U);
  ImageDet_M->Sizes.checksums[2] = (94026950U);
  ImageDet_M->Sizes.checksums[3] = (2778874300U);

  {
    static const sysRanDType rtAlwaysEnabled = SUBSYS_RAN_BC_ENABLE;
    static RTWExtModeInfo rt_ExtModeInfo;
    static const sysRanDType *systemRan[5];
    ImageDet_M->extModeInfo = (&rt_ExtModeInfo);
    rteiSetSubSystemActiveVectorAddresses(&rt_ExtModeInfo, systemRan);
    systemRan[0] = &rtAlwaysEnabled;
    systemRan[1] = &rtAlwaysEnabled;
    systemRan[2] = &rtAlwaysEnabled;
    systemRan[3] = &rtAlwaysEnabled;
    systemRan[4] = (sysRanDType *)&ImageDet_DW.Subsystem2_SubsysRanBC;
    rteiSetModelMappingInfoPtr(ImageDet_M->extModeInfo,
      &ImageDet_M->SpecialInfo.mappingInfo);
    rteiSetChecksumsPtr(ImageDet_M->extModeInfo, ImageDet_M->Sizes.checksums);
    rteiSetTPtr(ImageDet_M->extModeInfo, rtmGetTPtr(ImageDet_M));
  }

  /* block I/O */
  (void) memset(((void *) &ImageDet_B), 0,
                sizeof(B_ImageDet_T));

  /* states (dwork) */
  (void) memset((void *)&ImageDet_DW, 0,
                sizeof(DW_ImageDet_T));

  /* data type transition information */
  {
    static DataTypeTransInfo dtInfo;
    (void) memset((char_T *) &dtInfo, 0,
                  sizeof(dtInfo));
    ImageDet_M->SpecialInfo.mappingInfo = (&dtInfo);
    dtInfo.numDataTypes = 14;
    dtInfo.dataTypeSizes = &rtDataTypeSizes[0];
    dtInfo.dataTypeNames = &rtDataTypeNames[0];

    /* Block I/O transition table */
    dtInfo.B = &rtBTransTable;

    /* Parameters transition table */
    dtInfo.P = &rtPTransTable;
  }

  {
    int32_T idxOffsets;
    int32_T curNumNonZ;
    int32_T n;
    int32_T m;

    /* Start for S-Function (v4l2_video_capture_sfcn): '<Root>/V4L2 Video Capture' */
    MW_videoCaptureInit(ImageDet_ConstP.V4L2VideoCapture_p1, 0, 0, 0, 0, 320U,
                        240U, 2U, 2U, 1U, 0.033333333333333333);

    /* Start for S-Function (svipmorphop): '<S3>/Erosion3' */
    idxOffsets = 0;
    curNumNonZ = 0;
    n = 0;
    while (n < 1) {
      for (m = 0; m < 4; m++) {
        ImageDet_DW.Erosion3_ERODE_OFF_DW[idxOffsets] = m;
        curNumNonZ++;
        idxOffsets++;
      }

      n = 1;
    }

    ImageDet_DW.Erosion3_NUMNONZ_DW[0] = curNumNonZ;
    curNumNonZ = 0;
    for (n = 0; n < 4; n++) {
      m = 0;
      while (m < 1) {
        ImageDet_DW.Erosion3_ERODE_OFF_DW[idxOffsets] = n * 327;
        curNumNonZ++;
        idxOffsets++;
        m = 1;
      }
    }

    ImageDet_DW.Erosion3_NUMNONZ_DW[1] = curNumNonZ;

    /* End of Start for S-Function (svipmorphop): '<S3>/Erosion3' */
  }
}

/* Model terminate function */
void ImageDet_terminate(void)
{
  /* Terminate for S-Function (v4l2_video_capture_sfcn): '<Root>/V4L2 Video Capture' */
  MW_videoCaptureTerminate(ImageDet_ConstP.V4L2VideoCapture_p1);
}

/*
 * File trailer for generated code.
 *
 * [EOF]
 */

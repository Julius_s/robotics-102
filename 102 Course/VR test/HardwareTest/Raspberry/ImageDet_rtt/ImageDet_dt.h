/*
 * ImageDet_dt.h
 *
 * Code generation for model "ImageDet".
 *
 * Model version              : 1.200
 * Simulink Coder version : 8.7 (R2014b) 08-Sep-2014
 * C source code generated on : Sat Mar 07 16:53:42 2015
 *
 * Target selection: realtime.tlc
 * Embedded hardware selection: ARM Compatible->ARM Cortex
 * Code generation objectives: Unspecified
 * Validation result: Not run
 */

#include "ext_types.h"

/* data type size table */
static uint_T rtDataTypeSizes[] = {
  sizeof(real_T),
  sizeof(real32_T),
  sizeof(int8_T),
  sizeof(uint8_T),
  sizeof(int16_T),
  sizeof(uint16_T),
  sizeof(int32_T),
  sizeof(uint32_T),
  sizeof(boolean_T),
  sizeof(fcn_call_T),
  sizeof(int_T),
  sizeof(pointer_T),
  sizeof(action_T),
  2*sizeof(uint32_T)
};

/* data type name table */
static const char_T * rtDataTypeNames[] = {
  "real_T",
  "real32_T",
  "int8_T",
  "uint8_T",
  "int16_T",
  "uint16_T",
  "int32_T",
  "uint32_T",
  "boolean_T",
  "fcn_call_T",
  "int_T",
  "pointer_T",
  "action_T",
  "timer_uint32_pair_T"
};

/* data type transitions for block I/O structure */
static DataTypeTransition rtBTransitions[] = {
  { (char_T *)(&ImageDet_B.ColorSpaceConversion_o1[0]), 1, 0, 153600 },

  { (char_T *)(&ImageDet_B.RateTransition[0]), 3, 0, 230400 },

  { (char_T *)(&ImageDet_B.Erosion3[0]), 8, 0, 153600 }
  ,

  { (char_T *)(&ImageDet_DW.ColorSpaceConversion_DWORK1[0]), 1, 0, 76800 },

  { (char_T *)(&ImageDet_DW.ToWorkspace_PWORK.LoggedData), 11, 0, 1 },

  { (char_T *)(&ImageDet_DW.Dilation1_NUMNONZ_DW), 6, 0, 18 },

  { (char_T *)(&ImageDet_DW.Subsystem2_SubsysRanBC), 2, 0, 1 }
};

/* data type transition table for block I/O structure */
static DataTypeTransitionTable rtBTransTable = {
  7U,
  rtBTransitions
};

/* data type transitions for Parameters structure */
static DataTypeTransition rtPTransitions[] = {
  { (char_T *)(&ImageDet_P.Constant9_Value), 0, 0, 2 },

  { (char_T *)(&ImageDet_P.Gain1_Gain), 1, 0, 3 },

  { (char_T *)(&ImageDet_P.Constant_Value), 3, 0, 2 },

  { (char_T *)(&ImageDet_P.Constant10_Value[0]), 8, 0, 4 }
};

/* data type transition table for Parameters structure */
static DataTypeTransitionTable rtPTransTable = {
  4U,
  rtPTransitions
};

/* [EOF] ImageDet_dt.h */

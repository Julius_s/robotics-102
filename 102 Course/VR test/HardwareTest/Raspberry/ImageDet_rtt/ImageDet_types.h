/*
 * File: ImageDet_types.h
 *
 * Code generated for Simulink model 'ImageDet'.
 *
 * Model version                  : 1.200
 * Simulink Coder version         : 8.7 (R2014b) 08-Sep-2014
 * TLC version                    : 8.7 (Aug  5 2014)
 * C/C++ source code generated on : Sat Mar 07 16:53:42 2015
 *
 * Target selection: realtime.tlc
 * Embedded hardware selection: ARM Compatible->ARM Cortex
 * Code generation objectives: Unspecified
 * Validation result: Not run
 */

#ifndef RTW_HEADER_ImageDet_types_h_
#define RTW_HEADER_ImageDet_types_h_
#include "rtwtypes.h"
#include "multiword_types.h"

/* Parameters (auto storage) */
typedef struct P_ImageDet_T_ P_ImageDet_T;

/* Forward declaration for rtModel */
typedef struct tag_RTM_ImageDet_T RT_MODEL_ImageDet_T;

#endif                                 /* RTW_HEADER_ImageDet_types_h_ */

/*
 * File trailer for generated code.
 *
 * [EOF]
 */

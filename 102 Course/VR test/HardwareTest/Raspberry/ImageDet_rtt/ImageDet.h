/*
 * File: ImageDet.h
 *
 * Code generated for Simulink model 'ImageDet'.
 *
 * Model version                  : 1.200
 * Simulink Coder version         : 8.7 (R2014b) 08-Sep-2014
 * TLC version                    : 8.7 (Aug  5 2014)
 * C/C++ source code generated on : Sat Mar 07 16:53:42 2015
 *
 * Target selection: realtime.tlc
 * Embedded hardware selection: ARM Compatible->ARM Cortex
 * Code generation objectives: Unspecified
 * Validation result: Not run
 */

#ifndef RTW_HEADER_ImageDet_h_
#define RTW_HEADER_ImageDet_h_
#include <string.h>
#include <float.h>
#include <stddef.h>
#ifndef ImageDet_COMMON_INCLUDES_
# define ImageDet_COMMON_INCLUDES_
#include "rtwtypes.h"
#include "rtw_extmode.h"
#include "sysran_types.h"
#include "rtw_continuous.h"
#include "rtw_solver.h"
#include "dt_info.h"
#include "ext_work.h"
#include "v4l2_capture.h"
#endif                                 /* ImageDet_COMMON_INCLUDES_ */

#include "ImageDet_types.h"

/* Shared type includes */
#include "multiword_types.h"
#include "rt_nonfinite.h"
#include "rtGetInf.h"

/* Macros for accessing real-time model data structure */
#ifndef rtmGetFinalTime
# define rtmGetFinalTime(rtm)          ((rtm)->Timing.tFinal)
#endif

#ifndef rtmGetErrorStatus
# define rtmGetErrorStatus(rtm)        ((rtm)->errorStatus)
#endif

#ifndef rtmSetErrorStatus
# define rtmSetErrorStatus(rtm, val)   ((rtm)->errorStatus = (val))
#endif

#ifndef rtmStepTask
# define rtmStepTask(rtm, idx)         ((rtm)->Timing.TaskCounters.TID[(idx)] == 0)
#endif

#ifndef rtmGetStopRequested
# define rtmGetStopRequested(rtm)      ((rtm)->Timing.stopRequestedFlag)
#endif

#ifndef rtmSetStopRequested
# define rtmSetStopRequested(rtm, val) ((rtm)->Timing.stopRequestedFlag = (val))
#endif

#ifndef rtmGetStopRequestedPtr
# define rtmGetStopRequestedPtr(rtm)   (&((rtm)->Timing.stopRequestedFlag))
#endif

#ifndef rtmGetT
# define rtmGetT(rtm)                  ((rtm)->Timing.taskTime0)
#endif

#ifndef rtmGetTFinal
# define rtmGetTFinal(rtm)             ((rtm)->Timing.tFinal)
#endif

#ifndef rtmTaskCounter
# define rtmTaskCounter(rtm, idx)      ((rtm)->Timing.TaskCounters.TID[(idx)])
#endif

/* Block signals (auto storage) */
typedef struct {
  real32_T ColorSpaceConversion_o1[76800];/* '<S3>/Color Space  Conversion' */
  real32_T ColorSpaceConversion_o3[76800];/* '<S3>/Color Space  Conversion' */
  real32_T Gain3[76800];               /* '<S3>/Gain3' */
  real32_T Gain2[76800];               /* '<S3>/Gain2' */
  real32_T Gain1[76800];               /* '<S3>/Gain1' */
  uint8_T RateTransition[76800];       /* '<Root>/Rate Transition' */
  uint8_T RateTransition1[76800];      /* '<Root>/Rate Transition1' */
  uint8_T RateTransition2[76800];      /* '<Root>/Rate Transition2' */
  uint8_T V4L2VideoCapture_o1[76800];  /* '<Root>/V4L2 Video Capture' */
  uint8_T V4L2VideoCapture_o2[76800];  /* '<Root>/V4L2 Video Capture' */
  uint8_T V4L2VideoCapture_o3[76800];  /* '<Root>/V4L2 Video Capture' */
  boolean_T Erosion3[76800];           /* '<S3>/Erosion3' */
  boolean_T Dilation1[76800];          /* '<S3>/Dilation1' */
  boolean_T Compare[76800];            /* '<S8>/Compare' */
} B_ImageDet_T;

/* Block states (auto storage) for system '<Root>' */
typedef struct {
  real32_T ColorSpaceConversion_DWORK1[76800];/* '<S3>/Color Space  Conversion' */
  boolean_T Erosion3_TWO_PAD_IMG_DW[80769];/* '<S3>/Erosion3' */
  boolean_T Erosion3_ONE_PAD_IMG_DW[80769];/* '<S3>/Erosion3' */
  boolean_T Dilation1_ONE_PAD_IMG_DW[78489];/* '<S3>/Dilation1' */
  struct {
    void *LoggedData;
  } ToWorkspace_PWORK;                 /* '<S12>/To Workspace' */

  int32_T Dilation1_NUMNONZ_DW;        /* '<S3>/Dilation1' */
  int32_T Dilation1_STREL_DW;          /* '<S3>/Dilation1' */
  int32_T Dilation1_DILATE_OFF_DW[4];  /* '<S3>/Dilation1' */
  int32_T Erosion3_NUMNONZ_DW[2];      /* '<S3>/Erosion3' */
  int32_T Erosion3_STREL_DW[2];        /* '<S3>/Erosion3' */
  int32_T Erosion3_ERODE_OFF_DW[8];    /* '<S3>/Erosion3' */
  int8_T Subsystem2_SubsysRanBC;       /* '<S3>/Subsystem2' */
} DW_ImageDet_T;

/* Constant parameters (auto storage) */
typedef struct {
  /* Expression: devName
   * Referenced by: '<Root>/V4L2 Video Capture'
   */
  uint8_T V4L2VideoCapture_p1[12];
} ConstP_ImageDet_T;

/* Parameters (auto storage) */
struct P_ImageDet_T_ {
  real_T Constant9_Value;              /* Expression: 0
                                        * Referenced by: '<S3>/Constant9'
                                        */
  real_T Constant8_Value;              /* Expression: 1
                                        * Referenced by: '<S3>/Constant8'
                                        */
  real32_T Gain1_Gain;                 /* Computed Parameter: Gain1_Gain
                                        * Referenced by: '<S3>/Gain1'
                                        */
  real32_T Gain2_Gain;                 /* Computed Parameter: Gain2_Gain
                                        * Referenced by: '<S3>/Gain2'
                                        */
  real32_T Gain3_Gain;                 /* Computed Parameter: Gain3_Gain
                                        * Referenced by: '<S3>/Gain3'
                                        */
  uint8_T Constant_Value;              /* Computed Parameter: Constant_Value
                                        * Referenced by: '<S8>/Constant'
                                        */
  uint8_T ManualSwitch2_CurrentSetting;/* Computed Parameter: ManualSwitch2_CurrentSetting
                                        * Referenced by: '<S3>/Manual Switch2'
                                        */
  boolean_T Constant10_Value[4];       /* Computed Parameter: Constant10_Value
                                        * Referenced by: '<S3>/Constant10'
                                        */
};

/* Real-time Model Data Structure */
struct tag_RTM_ImageDet_T {
  const char_T *errorStatus;
  RTWExtModeInfo *extModeInfo;

  /*
   * Sizes:
   * The following substructure contains sizes information
   * for many of the model attributes such as inputs, outputs,
   * dwork, sample times, etc.
   */
  struct {
    uint32_T checksums[4];
  } Sizes;

  /*
   * SpecialInfo:
   * The following substructure contains special information
   * related to other components that are dependent on RTW.
   */
  struct {
    const void *mappingInfo;
  } SpecialInfo;

  /*
   * Timing:
   * The following substructure contains information regarding
   * the timing information for the model.
   */
  struct {
    time_T taskTime0;
    uint32_T clockTick0;
    time_T stepSize0;
    uint32_T clockTick1;
    struct {
      uint8_T TID[2];
    } TaskCounters;

    struct {
      boolean_T TID0_1;
    } RateInteraction;

    time_T tFinal;
    boolean_T stopRequestedFlag;
  } Timing;
};

/* Block parameters (auto storage) */
extern P_ImageDet_T ImageDet_P;

/* Block signals (auto storage) */
extern B_ImageDet_T ImageDet_B;

/* Block states (auto storage) */
extern DW_ImageDet_T ImageDet_DW;

/* Constant parameters (auto storage) */
extern const ConstP_ImageDet_T ImageDet_ConstP;

/* Model entry point functions */
extern void ImageDet_initialize(void);
extern void ImageDet_output0(void);
extern void ImageDet_update0(void);
extern void ImageDet_output1(void);
extern void ImageDet_update1(void);
extern void ImageDet_terminate(void);

/* Real-time Model object */
extern RT_MODEL_ImageDet_T *const ImageDet_M;

/*-
 * The generated code includes comments that allow you to trace directly
 * back to the appropriate location in the model.  The basic format
 * is <system>/block_name, where system is the system number (uniquely
 * assigned by Simulink) and block_name is the name of the block.
 *
 * Use the MATLAB hilite_system command to trace the generated code back
 * to the model.  For example,
 *
 * hilite_system('<S3>')    - opens system 3
 * hilite_system('<S3>/Kp') - opens and selects block Kp which resides in S3
 *
 * Here is the system hierarchy for this model
 *
 * '<Root>' : 'ImageDet'
 * '<S1>'   : 'ImageDet/Raspi Code'
 * '<S2>'   : 'ImageDet/Subsystem'
 * '<S3>'   : 'ImageDet/Raspi Code/Blob extraction '
 * '<S4>'   : 'ImageDet/Raspi Code/Chart'
 * '<S5>'   : 'ImageDet/Raspi Code/Player Tracker'
 * '<S6>'   : 'ImageDet/Raspi Code/encode'
 * '<S7>'   : 'ImageDet/Raspi Code/parseBlob'
 * '<S8>'   : 'ImageDet/Raspi Code/Blob extraction /Compare To Zero3'
 * '<S9>'   : 'ImageDet/Raspi Code/Blob extraction /FindBlobs'
 * '<S10>'  : 'ImageDet/Raspi Code/Blob extraction /MATLAB Function1'
 * '<S11>'  : 'ImageDet/Raspi Code/Blob extraction /Subsystem'
 * '<S12>'  : 'ImageDet/Raspi Code/Blob extraction /Subsystem2'
 * '<S13>'  : 'ImageDet/Raspi Code/Blob extraction /Subsystem4'
 * '<S14>'  : 'ImageDet/Raspi Code/Blob extraction /Subsystem/FindBlobs1'
 * '<S15>'  : 'ImageDet/Subsystem/MATLAB Function'
 * '<S16>'  : 'ImageDet/Subsystem/SerialCom'
 * '<S17>'  : 'ImageDet/Subsystem/SerialCom/Function-Call Subsystem'
 * '<S18>'  : 'ImageDet/Subsystem/SerialCom/Function-Call Subsystem/SerialInitilization'
 * '<S19>'  : 'ImageDet/Subsystem/SerialCom/Function-Call Subsystem/SerialWrite'
 */
#endif                                 /* RTW_HEADER_ImageDet_h_ */

/*
 * File trailer for generated code.
 *
 * [EOF]
 */

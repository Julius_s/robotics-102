/*
 * File: colorCamTest_data.c
 *
 * Code generated for Simulink model 'colorCamTest'.
 *
 * Model version                  : 1.151
 * Simulink Coder version         : 8.7 (R2014b) 08-Sep-2014
 * TLC version                    : 8.7 (Aug  5 2014)
 * C/C++ source code generated on : Sun Mar 01 15:21:18 2015
 *
 * Target selection: realtime.tlc
 * Embedded hardware selection: ARM Compatible->ARM Cortex
 * Code generation objectives: Unspecified
 * Validation result: Not run
 */

#include "colorCamTest.h"
#include "colorCamTest_private.h"

/* Block parameters (auto storage) */
P_colorCamTest_T colorCamTest_P = {
  10U,                                 /* Mask Parameter: BlobAnalysis2_minArea
                                        * Referenced by: '<S9>/Blob Analysis2'
                                        */
  0.0,                                 /* Expression: 0
                                        * Referenced by: '<S1>/Constant1'
                                        */
  1.0,                                 /* Expression: 1
                                        * Referenced by: '<S1>/Constant'
                                        */
  0.0,                                 /* Expression: 0
                                        * Referenced by: '<S9>/Constant9'
                                        */
  1.0,                                 /* Expression: 1
                                        * Referenced by: '<S9>/Constant8'
                                        */
  0.00392156886F,                      /* Computed Parameter: Gain1_Gain
                                        * Referenced by: '<S9>/Gain1'
                                        */
  0.00392156886F,                      /* Computed Parameter: Gain2_Gain
                                        * Referenced by: '<S9>/Gain2'
                                        */
  0.00392156886F,                      /* Computed Parameter: Gain3_Gain
                                        * Referenced by: '<S9>/Gain3'
                                        */
  -1,                                  /* Computed Parameter: DataStoreMemory_InitialValue
                                        * Referenced by: '<S4>/Data Store Memory'
                                        */
  1U,                                  /* Computed Parameter: ManualSwitch_CurrentSetting
                                        * Referenced by: '<S1>/Manual Switch'
                                        */
  0U,                                  /* Computed Parameter: Constant_Value_e
                                        * Referenced by: '<S16>/Constant'
                                        */
  0U,                                  /* Computed Parameter: ManualSwitch2_CurrentSetting
                                        * Referenced by: '<S9>/Manual Switch2'
                                        */

  /*  Computed Parameter: Constant10_Value
   * Referenced by: '<S9>/Constant10'
   */
  { 1, 1, 1, 1 }
};

/* Constant parameters (auto storage) */
const ConstP_colorCamTest_T colorCamTest_ConstP = {
  /* Computed Parameter: BlobAnalysis2_WALKER_
   * Referenced by: '<S9>/Blob Analysis2'
   */
  { -1, 321, 322, 323, 1, -321, -322, -323 },

  /* Expression: devName
   * Referenced by: '<Root>/V4L2 Video Capture'
   */
  { 47U, 100U, 101U, 118U, 47U, 118U, 105U, 100U, 101U, 111U, 48U, 0U }
};

/*
 * File trailer for generated code.
 *
 * [EOF]
 */

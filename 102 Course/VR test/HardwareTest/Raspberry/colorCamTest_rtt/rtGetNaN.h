/*
 * File: rtGetNaN.h
 *
 * Code generated for Simulink model 'colorCamTest'.
 *
 * Model version                  : 1.151
 * Simulink Coder version         : 8.7 (R2014b) 08-Sep-2014
 * TLC version                    : 8.7 (Aug  5 2014)
 * C/C++ source code generated on : Sun Mar 01 15:21:18 2015
 *
 * Target selection: realtime.tlc
 * Embedded hardware selection: ARM Compatible->ARM Cortex
 * Code generation objectives: Unspecified
 * Validation result: Not run
 */

#ifndef RTW_HEADER_rtGetNaN_h_
#define RTW_HEADER_rtGetNaN_h_
#include <stddef.h>
#include "rtwtypes.h"
#include "rt_nonfinite.h"

extern real_T rtGetNaN(void);
extern real32_T rtGetNaNF(void);

#endif                                 /* RTW_HEADER_rtGetNaN_h_ */

/*
 * File trailer for generated code.
 *
 * [EOF]
 */

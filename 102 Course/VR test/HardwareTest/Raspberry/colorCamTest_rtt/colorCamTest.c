/*
 * File: colorCamTest.c
 *
 * Code generated for Simulink model 'colorCamTest'.
 *
 * Model version                  : 1.151
 * Simulink Coder version         : 8.7 (R2014b) 08-Sep-2014
 * TLC version                    : 8.7 (Aug  5 2014)
 * C/C++ source code generated on : Sun Mar 01 15:21:18 2015
 *
 * Target selection: realtime.tlc
 * Embedded hardware selection: ARM Compatible->ARM Cortex
 * Code generation objectives: Unspecified
 * Validation result: Not run
 */

#include "colorCamTest.h"
#include "colorCamTest_private.h"
#include "colorCamTest_dt.h"

/* Block signals (auto storage) */
B_colorCamTest_T colorCamTest_B;

/* Block states (auto storage) */
DW_colorCamTest_T colorCamTest_DW;

/* Real-time model */
RT_MODEL_colorCamTest_T colorCamTest_M_;
RT_MODEL_colorCamTest_T *const colorCamTest_M = &colorCamTest_M_;
int32_T div_s32(int32_T numerator, int32_T denominator)
{
  int32_T quotient;
  uint32_T tempAbsQuotient;
  if (denominator == 0) {
    quotient = numerator >= 0 ? MAX_int32_T : MIN_int32_T;

    /* Divide by zero handler */
  } else {
    tempAbsQuotient = (uint32_T)(numerator >= 0 ? numerator : -numerator) /
      (denominator >= 0 ? denominator : -denominator);
    quotient = (numerator < 0) != (denominator < 0) ? -(int32_T)tempAbsQuotient :
      (int32_T)tempAbsQuotient;
  }

  return quotient;
}

/* Model output function */
void colorCamTest_output(void)
{
  real32_T cc2;
  real32_T min;
  real32_T max;
  static const uint8_T b[8] = { 85U, 85U, 85U, 85U, 104U, 101U, 97U, 100U };

  static const uint8_T d[24] = { 120U, 121U, 111U, 111U, 120U, 121U, 111U, 111U,
    120U, 121U, 111U, 111U, 120U, 121U, 111U, 111U, 120U, 121U, 111U, 111U, 120U,
    121U, 111U, 111U };

  int32_T bytesRead;
  uint8_T dataRead[512];
  int32_T dataLength;
  static const char_T b_0[13] = { '/', 'd', 'e', 'v', '/', 't', 't', 'y', 'A',
    'M', 'A', '0', '\x00' };

  uint8_T dataOut[35];
  uint8_T rtb_dataOut[35];
  real_T rtb_Product;
  uint32_T numBlobs;
  uint32_T BlobAnalysis2_NUM_PIX_DW[16];
  boolean_T maxNumBlobsReached;
  uint8_T currentLabel;
  uint32_T stackIdx;
  uint32_T walkerIdx;
  int32_T n;
  int32_T inIdx;
  int32_T m;
  int32_T i;
  char_T b_1[13];

  /* Reset subsysRan breadcrumbs */
  srClearBC(colorCamTest_DW.FunctionCallSubsystem_SubsysRan);

  /* Reset subsysRan breadcrumbs */
  srClearBC(colorCamTest_DW.Subsystem2_SubsysRanBC);

  /* S-Function (fcncallgen): '<Root>/Function-Call Generator' incorporates:
   *  SubSystem: '<Root>/Function-Call Subsystem'
   */
  /* MATLAB Function: '<S1>/MATLAB Function' */
  /* MATLAB Function 'Function-Call Subsystem/MATLAB Function': '<S3>:1' */
  /* '<S3>:1:11' */
  /* '<S3>:1:8' */
  /* '<S3>:1:7' */
  for (i = 0; i < 35; i++) {
    rtb_dataOut[i] = 0U;
  }

  /* '<S3>:1:8' */
  for (inIdx = 0; inIdx < 8; inIdx++) {
    rtb_dataOut[inIdx] = b[inIdx];
  }

  /* '<S3>:1:9' */
  rtb_dataOut[8] = 85U;
  rtb_dataOut[9] = 98U;
  rtb_dataOut[10] = 98U;

  /* '<S3>:1:11' */
  for (inIdx = 0; inIdx < 24; inIdx++) {
    rtb_dataOut[11 + inIdx] = d[inIdx];
  }

  /* '<S3>:1:12' */
  if (colorCamTest_DW.Psend == 0) {
    /* '<S3>:1:13' */
    /* '<S3>:1:14' */
    colorCamTest_DW.Psend = 1U;
  } else {
    /* '<S3>:1:16' */
    colorCamTest_DW.Psend = 0U;
  }

  /* ManualSwitch: '<S1>/Manual Switch' incorporates:
   *  Constant: '<S1>/Constant'
   *  Constant: '<S1>/Constant1'
   */
  /* '<S3>:1:18' */
  if (colorCamTest_P.ManualSwitch_CurrentSetting == 1) {
    rtb_Product = colorCamTest_P.Constant_Value;
  } else {
    rtb_Product = colorCamTest_P.Constant1_Value;
  }

  /* End of ManualSwitch: '<S1>/Manual Switch' */

  /* Product: '<S1>/Product' incorporates:
   *  MATLAB Function: '<S1>/MATLAB Function'
   */
  rtb_Product *= (real_T)colorCamTest_DW.Psend;

  /* MATLAB Function: '<S4>/SerialRead' */
  /* MATLAB Function 'Function-Call Subsystem/SerialCom/SerialRead': '<S7>:1' */
  /* '<S7>:1:4' */
  memset(&dataRead[0], 0, sizeof(uint8_T) << 9U);

  /* '<S7>:1:5' */
  dataLength = 0;
  if (colorCamTest_DW.spHandle > 0) {
    /* '<S7>:1:7' */
    /* '<S7>:1:8' */
    bytesRead = read(colorCamTest_DW.spHandle, dataRead, 512U);
    if (bytesRead > 0) {
      /* '<S7>:1:9' */
      /* '<S7>:1:10' */
      dataLength = bytesRead;
    }
  }

  /* '<S7>:1:14' */
  if (1 > dataLength) {
    colorCamTest_DW.SFunction_DIMS2 = 0;
  } else {
    colorCamTest_DW.SFunction_DIMS2 = dataLength;
  }

  /* End of MATLAB Function: '<S4>/SerialRead' */

  /* Outputs for Atomic SubSystem: '<S4>/Function-Call Subsystem' */
  /* MATLAB Function: '<S5>/SerialWrite' */
  for (inIdx = 0; inIdx < 35; inIdx++) {
    dataOut[inIdx] = rtb_dataOut[inIdx];
  }

  /* MATLAB Function 'Function-Call Subsystem/SerialCom/Function-Call Subsystem/SerialWrite': '<S8>:1' */
  /* '<S8>:1:3' */
  if (rtb_Product > 0.0) {
    /* '<S8>:1:4' */
    write(colorCamTest_DW.spHandle, dataOut, 35U);
  }

  /* End of MATLAB Function: '<S5>/SerialWrite' */
  /* End of Outputs for SubSystem: '<S4>/Function-Call Subsystem' */

  /* MATLAB Function: '<S4>/SerialInitilization' */
  /* MATLAB Function 'Function-Call Subsystem/SerialCom/SerialInitilization': '<S6>:1' */
  /* '<S6>:1:6' */
  if (colorCamTest_DW.spHandle == -1) {
    /* '<S6>:1:4' */
    /* '<S6>:1:5' */
    colorCamTest_DW.spHandle = 0;

    /* 57600 */
    /* '<S6>:1:6' */
    for (inIdx = 0; inIdx < 13; inIdx++) {
      b_1[inIdx] = b_0[inIdx];
    }

    colorCamTest_DW.spHandle = setupSerialPort(b_1, 9600U);
  }

  /* End of MATLAB Function: '<S4>/SerialInitilization' */
  colorCamTest_DW.FunctionCallSubsystem_SubsysRan = 4;

  /* S-Function (v4l2_video_capture_sfcn): '<Root>/V4L2 Video Capture' */
  MW_videoCaptureOutput(colorCamTest_ConstP.V4L2VideoCapture_p1,
                        colorCamTest_B.V4L2VideoCapture_o1,
                        colorCamTest_B.V4L2VideoCapture_o2,
                        colorCamTest_B.V4L2VideoCapture_o3);

  /* DataTypeConversion: '<S9>/Data Type Conversion' */
  for (i = 0; i < 76800; i++) {
    colorCamTest_B.Gain1[i] = colorCamTest_B.V4L2VideoCapture_o1[i];
  }

  /* End of DataTypeConversion: '<S9>/Data Type Conversion' */

  /* Gain: '<S9>/Gain1' */
  for (inIdx = 0; inIdx < 76800; inIdx++) {
    colorCamTest_B.Gain1[inIdx] *= colorCamTest_P.Gain1_Gain;
  }

  /* End of Gain: '<S9>/Gain1' */

  /* DataTypeConversion: '<S9>/Data Type Conversion1' */
  for (i = 0; i < 76800; i++) {
    colorCamTest_B.Gain2[i] = colorCamTest_B.V4L2VideoCapture_o2[i];
  }

  /* End of DataTypeConversion: '<S9>/Data Type Conversion1' */

  /* Gain: '<S9>/Gain2' */
  for (inIdx = 0; inIdx < 76800; inIdx++) {
    colorCamTest_B.Gain2[inIdx] *= colorCamTest_P.Gain2_Gain;
  }

  /* End of Gain: '<S9>/Gain2' */

  /* DataTypeConversion: '<S9>/Data Type Conversion2' */
  for (i = 0; i < 76800; i++) {
    colorCamTest_B.Gain3[i] = colorCamTest_B.V4L2VideoCapture_o3[i];
  }

  /* End of DataTypeConversion: '<S9>/Data Type Conversion2' */

  /* Gain: '<S9>/Gain3' */
  for (inIdx = 0; inIdx < 76800; inIdx++) {
    colorCamTest_B.Gain3[inIdx] *= colorCamTest_P.Gain3_Gain;
  }

  /* End of Gain: '<S9>/Gain3' */

  /* S-Function (svipcolorconv): '<S9>/Color Space  Conversion' */
  /* temporary variables for in-place operation */
  for (i = 0; i < 76800; i++) {
    /* First get the min and max of the RGB triplet */
    if (colorCamTest_B.Gain1[i] > colorCamTest_B.Gain2[i]) {
      if ((colorCamTest_B.Gain2[i] <= colorCamTest_B.Gain3[i]) || rtIsNaNF
          (colorCamTest_B.Gain3[i])) {
        min = colorCamTest_B.Gain2[i];
      } else {
        min = colorCamTest_B.Gain3[i];
      }

      if ((colorCamTest_B.Gain1[i] >= colorCamTest_B.Gain3[i]) || rtIsNaNF
          (colorCamTest_B.Gain3[i])) {
        max = colorCamTest_B.Gain1[i];
      } else {
        max = colorCamTest_B.Gain3[i];
      }
    } else {
      if ((colorCamTest_B.Gain1[i] <= colorCamTest_B.Gain3[i]) || rtIsNaNF
          (colorCamTest_B.Gain3[i])) {
        min = colorCamTest_B.Gain1[i];
      } else {
        min = colorCamTest_B.Gain3[i];
      }

      if ((colorCamTest_B.Gain2[i] >= colorCamTest_B.Gain3[i]) || rtIsNaNF
          (colorCamTest_B.Gain3[i])) {
        max = colorCamTest_B.Gain2[i];
      } else {
        max = colorCamTest_B.Gain3[i];
      }
    }

    min = max - min;
    if (max != 0.0F) {
      cc2 = min / max;
    } else {
      cc2 = 0.0F;
    }

    if (min != 0.0F) {
      if (colorCamTest_B.Gain1[i] == max) {
        min = (colorCamTest_B.Gain2[i] - colorCamTest_B.Gain3[i]) / min;
      } else if (colorCamTest_B.Gain2[i] == max) {
        min = (colorCamTest_B.Gain3[i] - colorCamTest_B.Gain1[i]) / min + 2.0F;
      } else {
        min = (colorCamTest_B.Gain1[i] - colorCamTest_B.Gain2[i]) / min + 4.0F;
      }

      min /= 6.0F;
      if (min < 0.0F) {
        min++;
      }
    } else {
      min = 0.0F;
    }

    /* assign the results */
    colorCamTest_B.ColorSpaceConversion_o1[i] = min;
    colorCamTest_B.ColorSpaceConversion_o2[i] = cc2;
    colorCamTest_B.ColorSpaceConversion_o3[i] = max;
  }

  /* End of S-Function (svipcolorconv): '<S9>/Color Space  Conversion' */

  /* RelationalOperator: '<S16>/Compare' incorporates:
   *  Constant: '<S16>/Constant'
   *  MATLAB Function: '<S9>/MATLAB Function1'
   *  Sum: '<S9>/Add1'
   */
  /* MATLAB Function 'Raspi Code/Blob extraction /MATLAB Function1': '<S19>:1' */
  /* %common v */
  /* '<S19>:1:3' */
  /*  channel2Min=0.4; */
  /* % hue vals */
  /* '<S19>:1:6' */
  /* '<S19>:1:7' */
  /* '<S19>:1:9' */
  /* '<S19>:1:10' */
  /*  channel3BlueMin = 0.5; */
  /* % ball vals */
  /* '<S19>:1:17' */
  /* '<S19>:1:20' */
  for (i = 0; i < 76800; i++) {
    colorCamTest_B.Compare[i] = ((int32_T)((uint32_T)
      (((colorCamTest_B.ColorSpaceConversion_o1[i] >= 0.75F) ||
        (colorCamTest_B.ColorSpaceConversion_o1[i] <= 0.25F)) &&
       (colorCamTest_B.ColorSpaceConversion_o3[i] >= 0.8)) +
      ((colorCamTest_B.ColorSpaceConversion_o1[i] >= 0.25F) &&
       (colorCamTest_B.ColorSpaceConversion_o1[i] <= 0.75F) &&
       (colorCamTest_B.ColorSpaceConversion_o3[i] >= 0.8))) >
      colorCamTest_P.Constant_Value_e);
  }

  /* End of RelationalOperator: '<S16>/Compare' */

  /* S-Function (svipmorphop): '<S9>/Dilation1' incorporates:
   *  Constant: '<S9>/Constant10'
   */
  dataLength = 0;
  bytesRead = 0;
  inIdx = 0;
  for (n = 0; n < 2; n++) {
    for (m = 0; m < 2; m++) {
      if (colorCamTest_P.Constant10_Value[dataLength]) {
        colorCamTest_DW.Dilation1_DILATE_OFF_DW[bytesRead] = n * 323 + m;
        inIdx++;
        bytesRead++;
      }

      dataLength++;
    }
  }

  colorCamTest_DW.Dilation1_NUMNONZ_DW = inIdx;
  for (i = 0; i < 78489; i++) {
    colorCamTest_DW.Dilation1_ONE_PAD_IMG_DW[i] = false;
  }

  for (inIdx = 0; inIdx < 76800; inIdx++) {
    if (colorCamTest_B.Compare[inIdx]) {
      dataLength = div_s32(inIdx, 320);
      dataLength = (inIdx - dataLength * 320) + dataLength * 323;
      for (i = 0; i < colorCamTest_DW.Dilation1_NUMNONZ_DW; i++) {
        colorCamTest_DW.Dilation1_ONE_PAD_IMG_DW[dataLength +
          colorCamTest_DW.Dilation1_DILATE_OFF_DW[i]] = true;
      }
    }
  }

  dataLength = 0;
  bytesRead = 0;
  inIdx = 0;
  for (n = 0; n < 240; n++) {
    for (m = 0; m < 320; m++) {
      colorCamTest_B.Dilation1[dataLength] =
        colorCamTest_DW.Dilation1_ONE_PAD_IMG_DW[inIdx];
      dataLength++;
      inIdx++;
    }

    inIdx += 3;
  }

  /* End of S-Function (svipmorphop): '<S9>/Dilation1' */

  /* S-Function (svipmorphop): '<S9>/Erosion3' */
  dataLength = 0;
  for (i = 0; i < 2; i++) {
    for (inIdx = 0; inIdx < 329; inIdx++) {
      colorCamTest_DW.Erosion3_ONE_PAD_IMG_DW[dataLength] = true;
      dataLength++;
    }
  }

  for (i = 0; i < 240; i++) {
    colorCamTest_DW.Erosion3_ONE_PAD_IMG_DW[dataLength] = true;
    dataLength++;
    colorCamTest_DW.Erosion3_ONE_PAD_IMG_DW[dataLength] = true;
    dataLength++;
    memcpy(&colorCamTest_DW.Erosion3_ONE_PAD_IMG_DW[dataLength],
           &colorCamTest_B.Dilation1[bytesRead], 320U * sizeof(boolean_T));
    dataLength += 320;
    bytesRead += 320;
    for (inIdx = 0; inIdx < 7; inIdx++) {
      colorCamTest_DW.Erosion3_ONE_PAD_IMG_DW[dataLength] = true;
      dataLength++;
    }
  }

  for (i = 0; i < 7; i++) {
    for (inIdx = 0; inIdx < 329; inIdx++) {
      colorCamTest_DW.Erosion3_ONE_PAD_IMG_DW[dataLength] = true;
      dataLength++;
    }
  }

  for (i = 0; i < 81921; i++) {
    colorCamTest_DW.Erosion3_TWO_PAD_IMG_DW[i] = true;
  }

  inIdx = 0;
  for (n = 0; n < 249; n++) {
    for (m = 0; m < 325; m++) {
      colorCamTest_DW.Erosion3_TWO_PAD_IMG_DW[2 + inIdx] = true;
      i = 0;
      while (i < colorCamTest_DW.Erosion3_NUMNONZ_DW[0]) {
        if (!colorCamTest_DW.Erosion3_ONE_PAD_IMG_DW[inIdx +
            colorCamTest_DW.Erosion3_ERODE_OFF_DW[i]]) {
          colorCamTest_DW.Erosion3_TWO_PAD_IMG_DW[2 + inIdx] = false;
          i = colorCamTest_DW.Erosion3_NUMNONZ_DW[0];
        }

        i++;
      }

      inIdx++;
    }

    inIdx += 4;
  }

  inIdx = 2;
  dataLength = 0;
  for (n = 0; n < 240; n++) {
    for (m = 2; m < 322; m++) {
      colorCamTest_B.Erosion3[dataLength] = true;
      i = 0;
      while (i < colorCamTest_DW.Erosion3_NUMNONZ_DW[1]) {
        if (!colorCamTest_DW.Erosion3_TWO_PAD_IMG_DW[colorCamTest_DW.Erosion3_ERODE_OFF_DW
            [i + colorCamTest_DW.Erosion3_NUMNONZ_DW[0]] + inIdx]) {
          colorCamTest_B.Erosion3[dataLength] = false;
          i = colorCamTest_DW.Erosion3_NUMNONZ_DW[1];
        }

        i++;
      }

      inIdx++;
      dataLength++;
    }

    inIdx += 9;
  }

  /* End of S-Function (svipmorphop): '<S9>/Erosion3' */

  /* S-Function (svipblob): '<S9>/Blob Analysis2' */
  maxNumBlobsReached = false;
  memset(&colorCamTest_DW.BlobAnalysis2_PAD_DW[0], 0, 323U * sizeof(uint8_T));
  currentLabel = 1U;
  i = 0;
  bytesRead = 323;
  for (n = 0; n < 240; n++) {
    for (m = 0; m < 320; m++) {
      colorCamTest_DW.BlobAnalysis2_PAD_DW[bytesRead] = (uint8_T)
        (colorCamTest_B.Erosion3[i] ? 255 : 0);
      i++;
      bytesRead++;
    }

    colorCamTest_DW.BlobAnalysis2_PAD_DW[bytesRead] = 0U;
    colorCamTest_DW.BlobAnalysis2_PAD_DW[bytesRead + 1] = 0U;
    bytesRead += 2;
  }

  memset(&colorCamTest_DW.BlobAnalysis2_PAD_DW[bytesRead], 0, 321U * sizeof
         (uint8_T));
  dataLength = 1;
  n = 0;
  while (n < 240) {
    bytesRead = 1;
    inIdx = dataLength * 322;
    m = 0;
    while (m < 320) {
      numBlobs = (uint32_T)(inIdx + bytesRead);
      if (colorCamTest_DW.BlobAnalysis2_PAD_DW[numBlobs] == 255) {
        colorCamTest_DW.BlobAnalysis2_PAD_DW[numBlobs] = currentLabel;
        BlobAnalysis2_NUM_PIX_DW[currentLabel - 1] = 1U;
        colorCamTest_DW.BlobAnalysis2_STACK_DW[0U] = numBlobs;
        stackIdx = 1U;
        while (stackIdx != 0U) {
          stackIdx--;
          numBlobs = colorCamTest_DW.BlobAnalysis2_STACK_DW[stackIdx];
          for (i = 0; i < 8; i++) {
            walkerIdx = numBlobs + colorCamTest_ConstP.BlobAnalysis2_WALKER_[i];
            if (colorCamTest_DW.BlobAnalysis2_PAD_DW[walkerIdx] == 255) {
              colorCamTest_DW.BlobAnalysis2_PAD_DW[walkerIdx] = currentLabel;
              BlobAnalysis2_NUM_PIX_DW[currentLabel - 1]++;
              colorCamTest_DW.BlobAnalysis2_STACK_DW[stackIdx] = walkerIdx;
              stackIdx++;
            }
          }
        }

        if (BlobAnalysis2_NUM_PIX_DW[currentLabel - 1] <
            colorCamTest_P.BlobAnalysis2_minArea) {
          currentLabel--;
        }

        if (currentLabel == 16) {
          maxNumBlobsReached = true;
          n = 240;
          m = 320;
        }

        if (m < 320) {
          currentLabel++;
        }
      }

      bytesRead++;
      m++;
    }

    dataLength++;
    n++;
  }

  numBlobs = (uint32_T)(maxNumBlobsReached ? (int32_T)currentLabel : (int32_T)
                        (uint8_T)(currentLabel - 1U));
  colorCamTest_DW.BlobAnalysis2_DIMS1[0] = (int32_T)numBlobs;
  colorCamTest_DW.BlobAnalysis2_DIMS1[1] = 1;
  colorCamTest_DW.BlobAnalysis2_DIMS2[0] = (int32_T)numBlobs;
  colorCamTest_DW.BlobAnalysis2_DIMS2[1] = 2;

  /* End of S-Function (svipblob): '<S9>/Blob Analysis2' */

  /* MATLAB Function: '<S9>/FindBlobs3' */
  /* MATLAB Function 'Raspi Code/Blob extraction /FindBlobs3': '<S18>:1' */
  /*  area */
  /* '<S18>:1:4' */
  colorCamTest_B.lengthh = colorCamTest_DW.BlobAnalysis2_DIMS1[0];

  /* ManualSwitch: '<S9>/Manual Switch2' incorporates:
   *  Constant: '<S9>/Constant8'
   *  Constant: '<S9>/Constant9'
   */
  if (colorCamTest_P.ManualSwitch2_CurrentSetting == 1) {
    rtb_Product = colorCamTest_P.Constant8_Value;
  } else {
    rtb_Product = colorCamTest_P.Constant9_Value;
  }

  /* End of ManualSwitch: '<S9>/Manual Switch2' */

  /* Outputs for Enabled SubSystem: '<S9>/Subsystem2' incorporates:
   *  EnablePort: '<S23>/Enable'
   */
  if (rtb_Product > 0.0) {
    if (!colorCamTest_DW.Subsystem2_MODE) {
      colorCamTest_DW.Subsystem2_MODE = true;
    }

    srUpdateBC(colorCamTest_DW.Subsystem2_SubsysRanBC);
  } else {
    if (colorCamTest_DW.Subsystem2_MODE) {
      colorCamTest_DW.Subsystem2_MODE = false;
    }
  }

  /* End of Outputs for SubSystem: '<S9>/Subsystem2' */
}

/* Model update function */
void colorCamTest_update(void)
{
  /* Update for Enabled SubSystem: '<S9>/Subsystem2' incorporates:
   *  Update for EnablePort: '<S23>/Enable'
   */
  if (colorCamTest_DW.Subsystem2_MODE) {
  }

  /* End of Update for SubSystem: '<S9>/Subsystem2' */

  /* signal main to stop simulation */
  {                                    /* Sample time: [0.1s, 0.0s] */
    if ((rtmGetTFinal(colorCamTest_M)!=-1) &&
        !((rtmGetTFinal(colorCamTest_M)-colorCamTest_M->Timing.taskTime0) >
          colorCamTest_M->Timing.taskTime0 * (DBL_EPSILON))) {
      rtmSetErrorStatus(colorCamTest_M, "Simulation finished");
    }

    if (rtmGetStopRequested(colorCamTest_M)) {
      rtmSetErrorStatus(colorCamTest_M, "Simulation finished");
    }
  }

  /* Update absolute time for base rate */
  /* The "clockTick0" counts the number of times the code of this task has
   * been executed. The absolute time is the multiplication of "clockTick0"
   * and "Timing.stepSize0". Size of "clockTick0" ensures timer will not
   * overflow during the application lifespan selected.
   */
  colorCamTest_M->Timing.taskTime0 =
    (++colorCamTest_M->Timing.clockTick0) * colorCamTest_M->Timing.stepSize0;
}

/* Model initialize function */
void colorCamTest_initialize(void)
{
  /* Registration code */

  /* initialize non-finites */
  rt_InitInfAndNaN(sizeof(real_T));

  /* initialize real-time model */
  (void) memset((void *)colorCamTest_M, 0,
                sizeof(RT_MODEL_colorCamTest_T));
  rtmSetTFinal(colorCamTest_M, -1);
  colorCamTest_M->Timing.stepSize0 = 0.1;

  /* External mode info */
  colorCamTest_M->Sizes.checksums[0] = (1273392090U);
  colorCamTest_M->Sizes.checksums[1] = (2971777017U);
  colorCamTest_M->Sizes.checksums[2] = (2184869244U);
  colorCamTest_M->Sizes.checksums[3] = (1965619564U);

  {
    static const sysRanDType rtAlwaysEnabled = SUBSYS_RAN_BC_ENABLE;
    static RTWExtModeInfo rt_ExtModeInfo;
    static const sysRanDType *systemRan[14];
    colorCamTest_M->extModeInfo = (&rt_ExtModeInfo);
    rteiSetSubSystemActiveVectorAddresses(&rt_ExtModeInfo, systemRan);
    systemRan[0] = &rtAlwaysEnabled;
    systemRan[1] = (sysRanDType *)
      &colorCamTest_DW.FunctionCallSubsystem_SubsysRan;
    systemRan[2] = (sysRanDType *)
      &colorCamTest_DW.FunctionCallSubsystem_SubsysRan;
    systemRan[3] = (sysRanDType *)
      &colorCamTest_DW.FunctionCallSubsystem_SubsysRan;
    systemRan[4] = (sysRanDType *)
      &colorCamTest_DW.FunctionCallSubsystem_SubsysRan;
    systemRan[5] = (sysRanDType *)
      &colorCamTest_DW.FunctionCallSubsystem_SubsysRan;
    systemRan[6] = (sysRanDType *)
      &colorCamTest_DW.FunctionCallSubsystem_SubsysRan;
    systemRan[7] = (sysRanDType *)
      &colorCamTest_DW.FunctionCallSubsystem_SubsysRan;
    systemRan[8] = (sysRanDType *)
      &colorCamTest_DW.FunctionCallSubsystem_SubsysRan;
    systemRan[9] = &rtAlwaysEnabled;
    systemRan[10] = &rtAlwaysEnabled;
    systemRan[11] = &rtAlwaysEnabled;
    systemRan[12] = &rtAlwaysEnabled;
    systemRan[13] = (sysRanDType *)&colorCamTest_DW.Subsystem2_SubsysRanBC;
    rteiSetModelMappingInfoPtr(colorCamTest_M->extModeInfo,
      &colorCamTest_M->SpecialInfo.mappingInfo);
    rteiSetChecksumsPtr(colorCamTest_M->extModeInfo,
                        colorCamTest_M->Sizes.checksums);
    rteiSetTPtr(colorCamTest_M->extModeInfo, rtmGetTPtr(colorCamTest_M));
  }

  /* block I/O */
  (void) memset(((void *) &colorCamTest_B), 0,
                sizeof(B_colorCamTest_T));

  /* states (dwork) */
  (void) memset((void *)&colorCamTest_DW, 0,
                sizeof(DW_colorCamTest_T));

  /* data type transition information */
  {
    static DataTypeTransInfo dtInfo;
    (void) memset((char_T *) &dtInfo, 0,
                  sizeof(dtInfo));
    colorCamTest_M->SpecialInfo.mappingInfo = (&dtInfo);
    dtInfo.numDataTypes = 14;
    dtInfo.dataTypeSizes = &rtDataTypeSizes[0];
    dtInfo.dataTypeNames = &rtDataTypeNames[0];

    /* Block I/O transition table */
    dtInfo.B = &rtBTransTable;

    /* Parameters transition table */
    dtInfo.P = &rtPTransTable;
  }

  {
    int32_T idxOffsets;
    int32_T curNumNonZ;
    int32_T n;
    int32_T m;

    /* Start for S-Function (fcncallgen): '<Root>/Function-Call Generator' incorporates:
     *  Start for SubSystem: '<Root>/Function-Call Subsystem'
     */
    /* Start for DataStoreMemory: '<S4>/Data Store Memory' */
    colorCamTest_DW.spHandle = colorCamTest_P.DataStoreMemory_InitialValue;

    /* Start for S-Function (v4l2_video_capture_sfcn): '<Root>/V4L2 Video Capture' */
    MW_videoCaptureInit(colorCamTest_ConstP.V4L2VideoCapture_p1, 0, 0, 0, 0,
                        320U, 240U, 2U, 2U, 1U, 0.1);

    /* Start for S-Function (svipmorphop): '<S9>/Erosion3' */
    idxOffsets = 0;
    curNumNonZ = 0;
    n = 0;
    while (n < 1) {
      for (m = 0; m < 5; m++) {
        colorCamTest_DW.Erosion3_ERODE_OFF_DW[idxOffsets] = m;
        curNumNonZ++;
        idxOffsets++;
      }

      n = 1;
    }

    colorCamTest_DW.Erosion3_NUMNONZ_DW[0] = curNumNonZ;
    curNumNonZ = 0;
    for (n = 0; n < 5; n++) {
      m = 0;
      while (m < 1) {
        colorCamTest_DW.Erosion3_ERODE_OFF_DW[idxOffsets] = n * 329;
        curNumNonZ++;
        idxOffsets++;
        m = 1;
      }
    }

    colorCamTest_DW.Erosion3_NUMNONZ_DW[1] = curNumNonZ;

    /* End of Start for S-Function (svipmorphop): '<S9>/Erosion3' */

    /* InitializeConditions for S-Function (fcncallgen): '<Root>/Function-Call Generator' incorporates:
     *  InitializeConditions for SubSystem: '<Root>/Function-Call Subsystem'
     */
    /* InitializeConditions for MATLAB Function: '<S1>/MATLAB Function' */
    colorCamTest_DW.Psend = 0U;
  }
}

/* Model terminate function */
void colorCamTest_terminate(void)
{
  /* Terminate for S-Function (v4l2_video_capture_sfcn): '<Root>/V4L2 Video Capture' */
  MW_videoCaptureTerminate(colorCamTest_ConstP.V4L2VideoCapture_p1);
}

/*
 * File trailer for generated code.
 *
 * [EOF]
 */

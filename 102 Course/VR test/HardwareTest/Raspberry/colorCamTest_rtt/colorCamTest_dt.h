/*
 * colorCamTest_dt.h
 *
 * Code generation for model "colorCamTest".
 *
 * Model version              : 1.151
 * Simulink Coder version : 8.7 (R2014b) 08-Sep-2014
 * C source code generated on : Sun Mar 01 15:21:18 2015
 *
 * Target selection: realtime.tlc
 * Embedded hardware selection: ARM Compatible->ARM Cortex
 * Code generation objectives: Unspecified
 * Validation result: Not run
 */

#include "ext_types.h"

/* data type size table */
static uint_T rtDataTypeSizes[] = {
  sizeof(real_T),
  sizeof(real32_T),
  sizeof(int8_T),
  sizeof(uint8_T),
  sizeof(int16_T),
  sizeof(uint16_T),
  sizeof(int32_T),
  sizeof(uint32_T),
  sizeof(boolean_T),
  sizeof(fcn_call_T),
  sizeof(int_T),
  sizeof(pointer_T),
  sizeof(action_T),
  2*sizeof(uint32_T)
};

/* data type name table */
static const char_T * rtDataTypeNames[] = {
  "real_T",
  "real32_T",
  "int8_T",
  "uint8_T",
  "int16_T",
  "uint16_T",
  "int32_T",
  "uint32_T",
  "boolean_T",
  "fcn_call_T",
  "int_T",
  "pointer_T",
  "action_T",
  "timer_uint32_pair_T"
};

/* data type transitions for block I/O structure */
static DataTypeTransition rtBTransitions[] = {
  { (char_T *)(&colorCamTest_B.ColorSpaceConversion_o1[0]), 1, 0, 230400 },

  { (char_T *)(&colorCamTest_B.Erosion3[0]), 8, 0, 153600 },

  { (char_T *)(&colorCamTest_B.lengthh), 0, 0, 1 }
  ,

  { (char_T *)(&colorCamTest_DW.Scope2_PWORK.LoggedData), 11, 0, 2 },

  { (char_T *)(&colorCamTest_DW.ColorSpaceConversion_DWORK1[0]), 1, 0, 76800 },

  { (char_T *)(&colorCamTest_DW.Dilation1_NUMNONZ_DW), 6, 0, 26 },

  { (char_T *)(&colorCamTest_DW.FunctionCallSubsystem_SubsysRan), 2, 0, 2 },

  { (char_T *)(&colorCamTest_DW.Psend), 3, 0, 1 },

  { (char_T *)(&colorCamTest_DW.Subsystem2_MODE), 8, 0, 1 }
};

/* data type transition table for block I/O structure */
static DataTypeTransitionTable rtBTransTable = {
  9U,
  rtBTransitions
};

/* data type transitions for Parameters structure */
static DataTypeTransition rtPTransitions[] = {
  { (char_T *)(&colorCamTest_P.BlobAnalysis2_minArea), 7, 0, 1 },

  { (char_T *)(&colorCamTest_P.Constant1_Value), 0, 0, 4 },

  { (char_T *)(&colorCamTest_P.Gain1_Gain), 1, 0, 3 },

  { (char_T *)(&colorCamTest_P.DataStoreMemory_InitialValue), 6, 0, 1 },

  { (char_T *)(&colorCamTest_P.ManualSwitch_CurrentSetting), 3, 0, 3 },

  { (char_T *)(&colorCamTest_P.Constant10_Value[0]), 8, 0, 4 }
};

/* data type transition table for Parameters structure */
static DataTypeTransitionTable rtPTransTable = {
  6U,
  rtPTransitions
};

/* [EOF] colorCamTest_dt.h */

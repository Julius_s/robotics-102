/*
 * File: colorCamTest.h
 *
 * Code generated for Simulink model 'colorCamTest'.
 *
 * Model version                  : 1.151
 * Simulink Coder version         : 8.7 (R2014b) 08-Sep-2014
 * TLC version                    : 8.7 (Aug  5 2014)
 * C/C++ source code generated on : Sun Mar 01 15:21:18 2015
 *
 * Target selection: realtime.tlc
 * Embedded hardware selection: ARM Compatible->ARM Cortex
 * Code generation objectives: Unspecified
 * Validation result: Not run
 */

#ifndef RTW_HEADER_colorCamTest_h_
#define RTW_HEADER_colorCamTest_h_
#include <string.h>
#include <float.h>
#include <stddef.h>
#ifndef colorCamTest_COMMON_INCLUDES_
# define colorCamTest_COMMON_INCLUDES_
#include "rtwtypes.h"
#include "rtw_extmode.h"
#include "sysran_types.h"
#include "rtw_continuous.h"
#include "rtw_solver.h"
#include "dt_info.h"
#include "ext_work.h"
#include "v4l2_capture.h"
#endif                                 /* colorCamTest_COMMON_INCLUDES_ */

#include "colorCamTest_types.h"

/* Shared type includes */
#include "multiword_types.h"
#include "rt_nonfinite.h"
#include "rtGetInf.h"

/* Macros for accessing real-time model data structure */
#ifndef rtmGetFinalTime
# define rtmGetFinalTime(rtm)          ((rtm)->Timing.tFinal)
#endif

#ifndef rtmGetErrorStatus
# define rtmGetErrorStatus(rtm)        ((rtm)->errorStatus)
#endif

#ifndef rtmSetErrorStatus
# define rtmSetErrorStatus(rtm, val)   ((rtm)->errorStatus = (val))
#endif

#ifndef rtmGetStopRequested
# define rtmGetStopRequested(rtm)      ((rtm)->Timing.stopRequestedFlag)
#endif

#ifndef rtmSetStopRequested
# define rtmSetStopRequested(rtm, val) ((rtm)->Timing.stopRequestedFlag = (val))
#endif

#ifndef rtmGetStopRequestedPtr
# define rtmGetStopRequestedPtr(rtm)   (&((rtm)->Timing.stopRequestedFlag))
#endif

#ifndef rtmGetT
# define rtmGetT(rtm)                  ((rtm)->Timing.taskTime0)
#endif

#ifndef rtmGetTFinal
# define rtmGetTFinal(rtm)             ((rtm)->Timing.tFinal)
#endif

/* Block signals (auto storage) */
typedef struct {
  real32_T ColorSpaceConversion_o1[76800];/* '<S9>/Color Space  Conversion' */
  real32_T ColorSpaceConversion_o2[76800];/* '<S9>/Color Space  Conversion' */
  real32_T ColorSpaceConversion_o3[76800];/* '<S9>/Color Space  Conversion' */
  real32_T Gain3[76800];               /* '<S9>/Gain3' */
  real32_T Gain2[76800];               /* '<S9>/Gain2' */
  real32_T Gain1[76800];               /* '<S9>/Gain1' */
  uint8_T V4L2VideoCapture_o3[76800];  /* '<Root>/V4L2 Video Capture' */
  uint8_T V4L2VideoCapture_o2[76800];  /* '<Root>/V4L2 Video Capture' */
  uint8_T V4L2VideoCapture_o1[76800];  /* '<Root>/V4L2 Video Capture' */
  boolean_T Erosion3[76800];           /* '<S9>/Erosion3' */
  boolean_T Dilation1[76800];          /* '<S9>/Dilation1' */
  boolean_T Compare[76800];            /* '<S16>/Compare' */
  real_T lengthh;                      /* '<S9>/FindBlobs3' */
} B_colorCamTest_T;

/* Block states (auto storage) for system '<Root>' */
typedef struct {
  uint32_T BlobAnalysis2_STACK_DW[76800];/* '<S9>/Blob Analysis2' */
  boolean_T Erosion3_TWO_PAD_IMG_DW[81921];/* '<S9>/Erosion3' */
  boolean_T Erosion3_ONE_PAD_IMG_DW[81921];/* '<S9>/Erosion3' */
  boolean_T Dilation1_ONE_PAD_IMG_DW[78489];/* '<S9>/Dilation1' */
  uint8_T BlobAnalysis2_PAD_DW[77924]; /* '<S9>/Blob Analysis2' */
  struct {
    void *LoggedData;
  } Scope2_PWORK;                      /* '<S9>/Scope2' */

  struct {
    void *LoggedData;
  } ToWorkspace3_PWORK;                /* '<S9>/To Workspace3' */

  real32_T ColorSpaceConversion_DWORK1[76800];/* '<S9>/Color Space  Conversion' */
  int32_T Dilation1_NUMNONZ_DW;        /* '<S9>/Dilation1' */
  int32_T Dilation1_STREL_DW;          /* '<S9>/Dilation1' */
  int32_T Dilation1_DILATE_OFF_DW[4];  /* '<S9>/Dilation1' */
  int32_T Erosion3_NUMNONZ_DW[2];      /* '<S9>/Erosion3' */
  int32_T Erosion3_STREL_DW[2];        /* '<S9>/Erosion3' */
  int32_T Erosion3_ERODE_OFF_DW[10];   /* '<S9>/Erosion3' */
  int32_T BlobAnalysis2_DIMS1[2];      /* '<S9>/Blob Analysis2' */
  int32_T BlobAnalysis2_DIMS2[2];      /* '<S9>/Blob Analysis2' */
  int32_T spHandle;                    /* '<S4>/Data Store Memory' */
  int32_T SFunction_DIMS2;             /* '<S4>/SerialRead' */
  int8_T FunctionCallSubsystem_SubsysRan;/* '<Root>/Function-Call Subsystem' */
  int8_T Subsystem2_SubsysRanBC;       /* '<S9>/Subsystem2' */
  uint8_T Psend;                       /* '<S1>/MATLAB Function' */
  boolean_T Subsystem2_MODE;           /* '<S9>/Subsystem2' */
} DW_colorCamTest_T;

/* Constant parameters (auto storage) */
typedef struct {
  /* Computed Parameter: BlobAnalysis2_WALKER_
   * Referenced by: '<S9>/Blob Analysis2'
   */
  int32_T BlobAnalysis2_WALKER_[8];

  /* Expression: devName
   * Referenced by: '<Root>/V4L2 Video Capture'
   */
  uint8_T V4L2VideoCapture_p1[12];
} ConstP_colorCamTest_T;

/* Parameters (auto storage) */
struct P_colorCamTest_T_ {
  uint32_T BlobAnalysis2_minArea;      /* Mask Parameter: BlobAnalysis2_minArea
                                        * Referenced by: '<S9>/Blob Analysis2'
                                        */
  real_T Constant1_Value;              /* Expression: 0
                                        * Referenced by: '<S1>/Constant1'
                                        */
  real_T Constant_Value;               /* Expression: 1
                                        * Referenced by: '<S1>/Constant'
                                        */
  real_T Constant9_Value;              /* Expression: 0
                                        * Referenced by: '<S9>/Constant9'
                                        */
  real_T Constant8_Value;              /* Expression: 1
                                        * Referenced by: '<S9>/Constant8'
                                        */
  real32_T Gain1_Gain;                 /* Computed Parameter: Gain1_Gain
                                        * Referenced by: '<S9>/Gain1'
                                        */
  real32_T Gain2_Gain;                 /* Computed Parameter: Gain2_Gain
                                        * Referenced by: '<S9>/Gain2'
                                        */
  real32_T Gain3_Gain;                 /* Computed Parameter: Gain3_Gain
                                        * Referenced by: '<S9>/Gain3'
                                        */
  int32_T DataStoreMemory_InitialValue;/* Computed Parameter: DataStoreMemory_InitialValue
                                        * Referenced by: '<S4>/Data Store Memory'
                                        */
  uint8_T ManualSwitch_CurrentSetting; /* Computed Parameter: ManualSwitch_CurrentSetting
                                        * Referenced by: '<S1>/Manual Switch'
                                        */
  uint8_T Constant_Value_e;            /* Computed Parameter: Constant_Value_e
                                        * Referenced by: '<S16>/Constant'
                                        */
  uint8_T ManualSwitch2_CurrentSetting;/* Computed Parameter: ManualSwitch2_CurrentSetting
                                        * Referenced by: '<S9>/Manual Switch2'
                                        */
  boolean_T Constant10_Value[4];       /* Computed Parameter: Constant10_Value
                                        * Referenced by: '<S9>/Constant10'
                                        */
};

/* Real-time Model Data Structure */
struct tag_RTM_colorCamTest_T {
  const char_T *errorStatus;
  RTWExtModeInfo *extModeInfo;

  /*
   * Sizes:
   * The following substructure contains sizes information
   * for many of the model attributes such as inputs, outputs,
   * dwork, sample times, etc.
   */
  struct {
    uint32_T checksums[4];
  } Sizes;

  /*
   * SpecialInfo:
   * The following substructure contains special information
   * related to other components that are dependent on RTW.
   */
  struct {
    const void *mappingInfo;
  } SpecialInfo;

  /*
   * Timing:
   * The following substructure contains information regarding
   * the timing information for the model.
   */
  struct {
    time_T taskTime0;
    uint32_T clockTick0;
    time_T stepSize0;
    time_T tFinal;
    boolean_T stopRequestedFlag;
  } Timing;
};

/* Block parameters (auto storage) */
extern P_colorCamTest_T colorCamTest_P;

/* Block signals (auto storage) */
extern B_colorCamTest_T colorCamTest_B;

/* Block states (auto storage) */
extern DW_colorCamTest_T colorCamTest_DW;

/* Constant parameters (auto storage) */
extern const ConstP_colorCamTest_T colorCamTest_ConstP;

/* Model entry point functions */
extern void colorCamTest_initialize(void);
extern void colorCamTest_output(void);
extern void colorCamTest_update(void);
extern void colorCamTest_terminate(void);

/* Real-time Model object */
extern RT_MODEL_colorCamTest_T *const colorCamTest_M;

/*-
 * The generated code includes comments that allow you to trace directly
 * back to the appropriate location in the model.  The basic format
 * is <system>/block_name, where system is the system number (uniquely
 * assigned by Simulink) and block_name is the name of the block.
 *
 * Use the MATLAB hilite_system command to trace the generated code back
 * to the model.  For example,
 *
 * hilite_system('<S3>')    - opens system 3
 * hilite_system('<S3>/Kp') - opens and selects block Kp which resides in S3
 *
 * Here is the system hierarchy for this model
 *
 * '<Root>' : 'colorCamTest'
 * '<S1>'   : 'colorCamTest/Function-Call Subsystem'
 * '<S2>'   : 'colorCamTest/Raspi Code'
 * '<S3>'   : 'colorCamTest/Function-Call Subsystem/MATLAB Function'
 * '<S4>'   : 'colorCamTest/Function-Call Subsystem/SerialCom'
 * '<S5>'   : 'colorCamTest/Function-Call Subsystem/SerialCom/Function-Call Subsystem'
 * '<S6>'   : 'colorCamTest/Function-Call Subsystem/SerialCom/SerialInitilization'
 * '<S7>'   : 'colorCamTest/Function-Call Subsystem/SerialCom/SerialRead'
 * '<S8>'   : 'colorCamTest/Function-Call Subsystem/SerialCom/Function-Call Subsystem/SerialWrite'
 * '<S9>'   : 'colorCamTest/Raspi Code/Blob extraction '
 * '<S10>'  : 'colorCamTest/Raspi Code/Chart'
 * '<S11>'  : 'colorCamTest/Raspi Code/Player Tracker'
 * '<S12>'  : 'colorCamTest/Raspi Code/encode'
 * '<S13>'  : 'colorCamTest/Raspi Code/parseBlob'
 * '<S14>'  : 'colorCamTest/Raspi Code/Blob extraction /Compare To Zero1'
 * '<S15>'  : 'colorCamTest/Raspi Code/Blob extraction /Compare To Zero2'
 * '<S16>'  : 'colorCamTest/Raspi Code/Blob extraction /Compare To Zero3'
 * '<S17>'  : 'colorCamTest/Raspi Code/Blob extraction /FindBlobs1'
 * '<S18>'  : 'colorCamTest/Raspi Code/Blob extraction /FindBlobs3'
 * '<S19>'  : 'colorCamTest/Raspi Code/Blob extraction /MATLAB Function1'
 * '<S20>'  : 'colorCamTest/Raspi Code/Blob extraction /MATLAB Function2'
 * '<S21>'  : 'colorCamTest/Raspi Code/Blob extraction /Subsystem'
 * '<S22>'  : 'colorCamTest/Raspi Code/Blob extraction /Subsystem1'
 * '<S23>'  : 'colorCamTest/Raspi Code/Blob extraction /Subsystem2'
 * '<S24>'  : 'colorCamTest/Raspi Code/Blob extraction /Subsystem4'
 */
#endif                                 /* RTW_HEADER_colorCamTest_h_ */

/*
 * File trailer for generated code.
 *
 * [EOF]
 */

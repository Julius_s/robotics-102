

for ii=1:length(simout.signals.values(1,1,:));

II=cat(3,simout.signals.values(:,:,ii),simout1.signals.values(:,:,ii));
I=cat(3,II,simout2.signals.values(:,:,ii));

%%common v
channel3Min=0.8;
channel2Min=0.4;
%% hue vals
channel1MinRed = 0.75;
channel1MaxRed = 0.25;

channel1BlueMin = 0.25;
channel1BlueMax = 0.75;
channel3BlueMin = 0.5;

%% ball vals

channel1MinBall = 0.8;
channel1MaxBall = 0.15;
channel2MinBall = 0.4;
channel3MinBall = 0.5;
% 
% ballBool = (I(:,:,1) >= channel1MinBall | (I(:,:,1) <= channel1MaxBall)) & ...
%     I(:,:,2) >= channel2MinBall & ...
%     I(:,:,3) >= channel3MinBall ;
%%
redBool = ((I(:,:,1) >= channel1MinRed ) | (I(:,:,1) <= channel1MaxRed) )& ...
         I(:,:,2) >= channel2Min & I(:,:,3) >= channel3Min ;
   
blueBool = (I(:,:,1) >= channel1BlueMin ) & (I(:,:,1) <= channel1BlueMax) & ...
         I(:,:,2) >= channel2Min & I(:,:,3) >= channel3BlueMin ;

   pause(0.1);
image(cat(3,cat(3,(redBool)*255,(blueBool)*255),zeros(320,240)*255));
% I=hsv2rgb(I);
% image(I);
% imshow((blueBool+redBool+greenBool+ballBool)*100);
drawnow;

end
bVals=[158 16;
    159 69;
    157 117;
    154 169;
    156 219;
    56 113;
    107 115;
    209 118;
    258 118];
dists=bVals(:,1);

angles=[60;
   30;
    0;
    30;
    60;
    60 ;
    30;
    30;
    60;]
angles=angles./191;
angles=atand(angles);
for ii=1:9
    bVals(ii,2)=240- bVals(ii,2);
    bVals(ii,:)=bVals(ii,:)-[320 240]./2;
    dists(ii)=norm(bVals(ii,:));
end
yields 
General model:
     f(x) = a*x+b
Coefficients (with 95% confidence bounds):
       a =      0.1746  (0.1621, 0.1871)
       b =     -0.1615  (-1.106, 0.7829)
       %%
       spotsX=zeros(63,1);
          spotsY=zeros(63,1);
       for ii=1:63
           temp=qqq(ii).Position;
           spotsX(ii,1)=temp(1);
             spotsY(ii,1)=temp(2);
       end
       spotsX=flipud(fliplr(reshape(spotsX,[9 7])'));
       spotsY=fliplr(flipud(reshape(spotsY,[9 7])'));            
       x=fliplr([80 60 40 20 0 -20 -40 -60 -80]);
       y=fliplr([-60 -40 -20 0 20 40 60]);
       
      [X,Y]=meshgrid(x,y);
      spotsY=240-spotsY;
      spotsX=spotsX-320/2;
      spotsY=spotsY-240/2;
      %% 192 h
      x:
General model:
     f(x) = a*x+b
Coefficients (with 95% confidence bounds):
       a =       0.587  (0.5841, 0.5899)
       b =     -0.2143  (-0.4692, 0.04056)

Goodness of fit:
  SSE: 62.43
  R-square: 0.9996
  Adjusted R-square: 0.9996
  RMSE: 1.012

y:
General model:
     f(x) = a*x+b
Coefficients (with 95% confidence bounds):
       a =      0.5877  (0.5844, 0.5911)
       b =      0.2332  (0.003278, 0.4632)

Goodness of fit:
  SSE: 50.82
  R-square: 0.9995
  Adjusted R-square: 0.9995
  RMSE: 0.9127


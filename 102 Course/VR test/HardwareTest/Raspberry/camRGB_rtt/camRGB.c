/*
 * Academic License - for use in teaching, academic research, and meeting
 * course requirements at degree granting institutions only.  Not for
 * government, commercial, or other organizational use.
 *
 * File: camRGB.c
 *
 * Code generated for Simulink model 'camRGB'.
 *
 * Model version                  : 1.1869
 * Simulink Coder version         : 8.8 (R2015a) 09-Feb-2015
 * TLC version                    : 8.8 (Jan 20 2015)
 * C/C++ source code generated on : Wed May 27 19:31:05 2015
 *
 * Target selection: realtime.tlc
 * Embedded hardware selection: ARM Compatible->ARM Cortex
 * Code generation objectives: Unspecified
 * Validation result: Not run
 */

#include "camRGB.h"
#include "camRGB_private.h"
#include "camRGB_dt.h"

/* Block signals (auto storage) */
B_camRGB_T camRGB_B;

/* Block states (auto storage) */
DW_camRGB_T camRGB_DW;

/* External inputs (root inport signals with auto storage) */
ExtU_camRGB_T camRGB_U;

/* External outputs (root outports fed by signals with auto storage) */
ExtY_camRGB_T camRGB_Y;

/* Real-time model */
RT_MODEL_camRGB_T camRGB_M_;
RT_MODEL_camRGB_T *const camRGB_M = &camRGB_M_;
void mul_wide_u32(uint32_T in0, uint32_T in1, uint32_T *ptrOutBitsHi, uint32_T
                  *ptrOutBitsLo)
{
  uint32_T outBitsLo;
  uint32_T in0Lo;
  uint32_T in0Hi;
  uint32_T in1Lo;
  uint32_T in1Hi;
  uint32_T productHiLo;
  uint32_T productLoHi;
  in0Hi = in0 >> 16U;
  in0Lo = in0 & 65535U;
  in1Hi = in1 >> 16U;
  in1Lo = in1 & 65535U;
  productHiLo = in0Hi * in1Lo;
  productLoHi = in0Lo * in1Hi;
  in0Lo *= in1Lo;
  in1Lo = 0U;
  outBitsLo = (productLoHi << 16U) + in0Lo;
  if (outBitsLo < in0Lo) {
    in1Lo = 1U;
  }

  in0Lo = outBitsLo;
  outBitsLo += productHiLo << 16U;
  if (outBitsLo < in0Lo) {
    in1Lo++;
  }

  *ptrOutBitsHi = (((productLoHi >> 16U) + (productHiLo >> 16U)) + in0Hi * in1Hi)
    + in1Lo;
  *ptrOutBitsLo = outBitsLo;
}

uint32_T mul_u32_u32_u32_sr15(uint32_T a, uint32_T b)
{
  uint32_T result;
  uint32_T u32_chi;
  mul_wide_u32(a, b, &u32_chi, &result);
  return u32_chi << 17U | result >> 15U;
}

void mul_wide_s32(int32_T in0, int32_T in1, uint32_T *ptrOutBitsHi, uint32_T
                  *ptrOutBitsLo)
{
  uint32_T absIn0;
  uint32_T absIn1;
  uint32_T in0Lo;
  uint32_T in0Hi;
  uint32_T in1Hi;
  uint32_T productHiLo;
  uint32_T productLoHi;
  absIn0 = (uint32_T)(in0 < 0 ? -in0 : in0);
  absIn1 = (uint32_T)(in1 < 0 ? -in1 : in1);
  in0Hi = absIn0 >> 16U;
  in0Lo = absIn0 & 65535U;
  in1Hi = absIn1 >> 16U;
  absIn0 = absIn1 & 65535U;
  productHiLo = in0Hi * absIn0;
  productLoHi = in0Lo * in1Hi;
  absIn0 *= in0Lo;
  absIn1 = 0U;
  in0Lo = (productLoHi << 16U) + absIn0;
  if (in0Lo < absIn0) {
    absIn1 = 1U;
  }

  absIn0 = in0Lo;
  in0Lo += productHiLo << 16U;
  if (in0Lo < absIn0) {
    absIn1++;
  }

  absIn0 = (((productLoHi >> 16U) + (productHiLo >> 16U)) + in0Hi * in1Hi) +
    absIn1;
  if (!((in0 == 0) || ((in1 == 0) || ((in0 > 0) == (in1 > 0))))) {
    absIn0 = ~absIn0;
    in0Lo = ~in0Lo;
    in0Lo++;
    if (in0Lo == 0U) {
      absIn0++;
    }
  }

  *ptrOutBitsHi = absIn0;
  *ptrOutBitsLo = in0Lo;
}

int32_T mul_s32_s32_s32_sr17(int32_T a, int32_T b)
{
  uint32_T u32_chi;
  uint32_T u32_clo;
  mul_wide_s32(a, b, &u32_chi, &u32_clo);
  u32_clo = u32_chi << 15U | u32_clo >> 17U;
  return (int32_T)u32_clo;
}

int32_T mul_s32_s32_s32_sr30(int32_T a, int32_T b)
{
  uint32_T u32_chi;
  uint32_T u32_clo;
  mul_wide_s32(a, b, &u32_chi, &u32_clo);
  u32_clo = u32_chi << 2U | u32_clo >> 30U;
  return (int32_T)u32_clo;
}

int32_T div_repeat_s32_floor(int32_T numerator, int32_T denominator, uint32_T
  nRepeatSub)
{
  int32_T quotient;
  uint32_T absNumerator;
  uint32_T absDenominator;
  if (denominator == 0) {
    quotient = numerator >= 0 ? MAX_int32_T : MIN_int32_T;

    /* Divide by zero handler */
  } else {
    absNumerator = (uint32_T)(numerator >= 0 ? numerator : -numerator);
    absDenominator = (uint32_T)(denominator >= 0 ? denominator : -denominator);
    if ((numerator < 0) != (denominator < 0)) {
      quotient = -(int32_T)div_nzp_repeat_u32_ceiling(absNumerator,
        absDenominator, nRepeatSub);
    } else {
      quotient = (int32_T)div_nzp_repeat_u32(absNumerator, absDenominator,
        nRepeatSub);
    }
  }

  return quotient;
}

uint32_T div_nzp_repeat_u32_ceiling(uint32_T numerator, uint32_T denominator,
  uint32_T nRepeatSub)
{
  uint32_T quotient;
  uint32_T iRepeatSub;
  boolean_T numeratorExtraBit;
  quotient = numerator / denominator;
  numerator %= denominator;
  for (iRepeatSub = 0U; iRepeatSub < nRepeatSub; iRepeatSub++) {
    numeratorExtraBit = (numerator >= 2147483648U);
    numerator <<= 1U;
    quotient <<= 1U;
    if (numeratorExtraBit || (numerator >= denominator)) {
      quotient++;
      numerator -= denominator;
    }
  }

  if (numerator > 0U) {
    quotient++;
  }

  return quotient;
}

uint32_T div_nzp_repeat_u32(uint32_T numerator, uint32_T denominator, uint32_T
  nRepeatSub)
{
  uint32_T quotient;
  uint32_T iRepeatSub;
  boolean_T numeratorExtraBit;
  quotient = numerator / denominator;
  numerator %= denominator;
  for (iRepeatSub = 0U; iRepeatSub < nRepeatSub; iRepeatSub++) {
    numeratorExtraBit = (numerator >= 2147483648U);
    numerator <<= 1U;
    quotient <<= 1U;
    if (numeratorExtraBit || (numerator >= denominator)) {
      quotient++;
      numerator -= denominator;
    }
  }

  return quotient;
}

real_T rt_roundd_snf(real_T u)
{
  real_T y;
  if (fabs(u) < 4.503599627370496E+15) {
    if (u >= 0.5) {
      y = floor(u + 0.5);
    } else if (u > -0.5) {
      y = u * 0.0;
    } else {
      y = ceil(u - 0.5);
    }
  } else {
    y = u;
  }

  return y;
}

/* Model output function */
void camRGB_output(void)
{
  int32_T i;
  int32_T Autothreshold_HIST_FIXPT_DW[256];
  int32_T maxVal;
  uint8_T threshold;
  boolean_T maxNumBlobsReached;
  int32_T idx;
  int32_T n;
  uint32_T stackIdx;
  int32_T m;
  uint32_T BlobAnalysis_NUM_PIX_DW[16];
  uint32_T walkerIdx;
  uint32_T numBlobs;
  int32_T Autothreshold_NORMF_FIXPT_DW;
  int32_T Autothreshold_P_FIXPT_DW[256];
  int32_T Autothreshold_MU_FIXPT_DW[256];
  int32_T Autothreshold2_MU_FIXPT_DW[256];

  /* Outport: '<Root>/Info' */
  camRGB_Y.Info = 0.0;

  /* S-Function (v4l2_video_capture_sfcn): '<Root>/V4L2 Video Capture' */
  MW_videoCaptureOutput(camRGB_ConstP.V4L2VideoCapture_p1,
                        camRGB_B.V4L2VideoCapture_o1,
                        camRGB_B.V4L2VideoCapture_o2,
                        camRGB_B.V4L2VideoCapture_o3);

  /* MATLAB Function: '<S1>/MATLAB Function' */
  /* MATLAB Function 'Blob extraction /MATLAB Function': '<S9>:1' */
  /* '<S9>:1:2' */
  for (Autothreshold_NORMF_FIXPT_DW = 0; Autothreshold_NORMF_FIXPT_DW < 76800;
       Autothreshold_NORMF_FIXPT_DW++) {
    idx = (int32_T)rt_roundd_snf((real_T)
      camRGB_B.V4L2VideoCapture_o1[Autothreshold_NORMF_FIXPT_DW] / 2.0);
    camRGB_B.BI[Autothreshold_NORMF_FIXPT_DW] = (uint8_T)idx;
  }

  /* '<S9>:1:3' */
  for (Autothreshold_NORMF_FIXPT_DW = 0; Autothreshold_NORMF_FIXPT_DW < 76800;
       Autothreshold_NORMF_FIXPT_DW++) {
    idx = (int32_T)rt_roundd_snf((real_T)
      camRGB_B.V4L2VideoCapture_o2[Autothreshold_NORMF_FIXPT_DW] / 2.0);
    camRGB_B.hG[Autothreshold_NORMF_FIXPT_DW] = (uint8_T)idx;
  }

  /* '<S9>:1:4' */
  for (Autothreshold_NORMF_FIXPT_DW = 0; Autothreshold_NORMF_FIXPT_DW < 76800;
       Autothreshold_NORMF_FIXPT_DW++) {
    idx = (int32_T)rt_roundd_snf((real_T)
      camRGB_B.V4L2VideoCapture_o3[Autothreshold_NORMF_FIXPT_DW] / 2.0);
    camRGB_B.GI[Autothreshold_NORMF_FIXPT_DW] = (uint8_T)idx;
  }

  /* '<S9>:1:5' */
  for (idx = 0; idx < 76800; idx++) {
    n = camRGB_B.V4L2VideoCapture_o1[idx];
    numBlobs = (uint32_T)n - camRGB_B.hG[idx];
    if (numBlobs > (uint32_T)n) {
      numBlobs = 0U;
    }

    stackIdx = numBlobs - camRGB_B.GI[idx];
    if (stackIdx > numBlobs) {
      stackIdx = 0U;
    }

    camRGB_B.GI[idx] = (uint8_T)stackIdx;
  }

  /* '<S9>:1:6' */
  /* '<S9>:1:7' */
  for (idx = 0; idx < 76800; idx++) {
    n = camRGB_B.V4L2VideoCapture_o3[idx];
    numBlobs = (uint32_T)n - camRGB_B.BI[idx];
    if (numBlobs > (uint32_T)n) {
      numBlobs = 0U;
    }

    stackIdx = numBlobs - camRGB_B.hG[idx];
    if (stackIdx > numBlobs) {
      stackIdx = 0U;
    }

    camRGB_B.BI[idx] = (uint8_T)stackIdx;
  }

  /* End of MATLAB Function: '<S1>/MATLAB Function' */

  /* S-Function (svipgraythresh): '<S1>/Autothreshold' */
  memset(&Autothreshold_HIST_FIXPT_DW[0], 0, sizeof(int32_T) << 8U);
  for (i = 0; i < 76800; i++) {
    Autothreshold_HIST_FIXPT_DW[camRGB_B.GI[i]]++;
  }

  for (i = 0; i < 256; i++) {
    Autothreshold_P_FIXPT_DW[i] = mul_s32_s32_s32_sr17(1832519379,
      Autothreshold_HIST_FIXPT_DW[i]);
  }

  Autothreshold_MU_FIXPT_DW[0] = Autothreshold_P_FIXPT_DW[0] >> 8;
  for (i = 0; i < 255; i++) {
    Autothreshold_MU_FIXPT_DW[i + 1] = (i + 2) << 22;
    Autothreshold_MU_FIXPT_DW[i + 1] = mul_s32_s32_s32_sr30
      (Autothreshold_MU_FIXPT_DW[i + 1], Autothreshold_P_FIXPT_DW[i + 1]);
    Autothreshold_MU_FIXPT_DW[i + 1] += Autothreshold_MU_FIXPT_DW[i];
  }

  for (i = 0; i < 254; i++) {
    Autothreshold_P_FIXPT_DW[i + 1] += Autothreshold_P_FIXPT_DW[i];
  }

  Autothreshold_NORMF_FIXPT_DW = 0;
  maxVal = 0;
  for (i = 0; i < 255; i++) {
    idx = mul_s32_s32_s32_sr30(Autothreshold_P_FIXPT_DW[i],
      Autothreshold_MU_FIXPT_DW[255]) - Autothreshold_MU_FIXPT_DW[i];
    n = mul_s32_s32_s32_sr30(Autothreshold_P_FIXPT_DW[i], 1073741824 -
      Autothreshold_P_FIXPT_DW[i]);
    if (n == 0) {
      n = 0;
    } else {
      n = div_repeat_s32_floor(mul_s32_s32_s32_sr30(idx, idx) << 2, n, 30U);
    }

    if (n > maxVal) {
      maxVal = n;
      Autothreshold_NORMF_FIXPT_DW = i;
    }
  }

  idx = (int32_T)mul_u32_u32_u32_sr15(255U, (uint32_T)(4210752 *
    Autothreshold_NORMF_FIXPT_DW));
  threshold = (uint8_T)(((idx & 16384) != 0) + (idx >> 15));
  for (i = 0; i < 76800; i++) {
    camRGB_B.Autothreshold_o1[i] = (camRGB_B.GI[i] > threshold);
  }

  /* End of S-Function (svipgraythresh): '<S1>/Autothreshold' */

  /* S-Function (svipgraythresh): '<S1>/Autothreshold2' */
  memset(&Autothreshold_HIST_FIXPT_DW[0], 0, sizeof(int32_T) << 8U);
  for (i = 0; i < 76800; i++) {
    Autothreshold_HIST_FIXPT_DW[camRGB_B.BI[i]]++;
  }

  for (i = 0; i < 256; i++) {
    Autothreshold_P_FIXPT_DW[i] = mul_s32_s32_s32_sr17(1832519379,
      Autothreshold_HIST_FIXPT_DW[i]);
  }

  Autothreshold2_MU_FIXPT_DW[0] = Autothreshold_P_FIXPT_DW[0] >> 8;
  for (i = 0; i < 255; i++) {
    Autothreshold2_MU_FIXPT_DW[i + 1] = (i + 2) << 22;
    Autothreshold2_MU_FIXPT_DW[i + 1] = mul_s32_s32_s32_sr30
      (Autothreshold2_MU_FIXPT_DW[i + 1], Autothreshold_P_FIXPT_DW[i + 1]);
    Autothreshold2_MU_FIXPT_DW[i + 1] += Autothreshold2_MU_FIXPT_DW[i];
  }

  for (i = 0; i < 254; i++) {
    Autothreshold_P_FIXPT_DW[i + 1] += Autothreshold_P_FIXPT_DW[i];
  }

  Autothreshold_NORMF_FIXPT_DW = 0;
  maxVal = 0;
  for (i = 0; i < 255; i++) {
    idx = mul_s32_s32_s32_sr30(Autothreshold_P_FIXPT_DW[i],
      Autothreshold2_MU_FIXPT_DW[255]) - Autothreshold2_MU_FIXPT_DW[i];
    n = mul_s32_s32_s32_sr30(Autothreshold_P_FIXPT_DW[i], 1073741824 -
      Autothreshold_P_FIXPT_DW[i]);
    if (n == 0) {
      n = 0;
    } else {
      n = div_repeat_s32_floor(mul_s32_s32_s32_sr30(idx, idx) << 2, n, 30U);
    }

    if (n > maxVal) {
      maxVal = n;
      Autothreshold_NORMF_FIXPT_DW = i;
    }
  }

  idx = (int32_T)mul_u32_u32_u32_sr15(255U, (uint32_T)(4210752 *
    Autothreshold_NORMF_FIXPT_DW));
  threshold = (uint8_T)(((idx & 16384) != 0) + (idx >> 15));
  for (i = 0; i < 76800; i++) {
    /* S-Function (svipgraythresh): '<S1>/Autothreshold2' */
    camRGB_B.Autothreshold2_o1[i] = (camRGB_B.BI[i] > threshold);

    /* RelationalOperator: '<S6>/Compare' incorporates:
     *  Constant: '<S6>/Constant'
     *  Sum: '<S1>/Add'
     */
    camRGB_B.Compare[i] = ((int32_T)((uint32_T)camRGB_B.Autothreshold_o1[i] +
      camRGB_B.Autothreshold2_o1[i]) > camRGB_P.Constant_Value);
  }

  /* S-Function (svipblob): '<S1>/Blob Analysis' */
  maxNumBlobsReached = false;
  memset(&camRGB_DW.BlobAnalysis_PAD_DW[0], 0, 323U * sizeof(uint8_T));
  threshold = 1U;
  i = 0;
  idx = 323;
  for (n = 0; n < 240; n++) {
    for (m = 0; m < 320; m++) {
      camRGB_DW.BlobAnalysis_PAD_DW[idx] = (uint8_T)(camRGB_B.Compare[i] ? 255 :
        0);
      i++;
      idx++;
    }

    camRGB_DW.BlobAnalysis_PAD_DW[idx] = 0U;
    camRGB_DW.BlobAnalysis_PAD_DW[idx + 1] = 0U;
    idx += 2;
  }

  memset(&camRGB_DW.BlobAnalysis_PAD_DW[idx], 0, 321U * sizeof(uint8_T));
  Autothreshold_NORMF_FIXPT_DW = 1;
  n = 0;
  while (n < 240) {
    idx = 1;
    maxVal = Autothreshold_NORMF_FIXPT_DW * 322;
    m = 0;
    while (m < 320) {
      numBlobs = (uint32_T)(maxVal + idx);
      if (camRGB_DW.BlobAnalysis_PAD_DW[numBlobs] == 255) {
        camRGB_DW.BlobAnalysis_PAD_DW[numBlobs] = threshold;
        BlobAnalysis_NUM_PIX_DW[threshold - 1] = 1U;
        camRGB_DW.BlobAnalysis_STACK_DW[0U] = numBlobs;
        stackIdx = 1U;
        while (stackIdx != 0U) {
          stackIdx--;
          numBlobs = camRGB_DW.BlobAnalysis_STACK_DW[stackIdx];
          for (i = 0; i < 8; i++) {
            walkerIdx = numBlobs + camRGB_ConstP.BlobAnalysis_WALKER_R[i];
            if (camRGB_DW.BlobAnalysis_PAD_DW[walkerIdx] == 255) {
              camRGB_DW.BlobAnalysis_PAD_DW[walkerIdx] = threshold;
              BlobAnalysis_NUM_PIX_DW[threshold - 1]++;
              camRGB_DW.BlobAnalysis_STACK_DW[stackIdx] = walkerIdx;
              stackIdx++;
            }
          }
        }

        if (threshold == 16) {
          maxNumBlobsReached = true;
          n = 240;
          m = 320;
        }

        if (m < 320) {
          threshold++;
        }
      }

      idx++;
      m++;
    }

    Autothreshold_NORMF_FIXPT_DW++;
    n++;
  }

  numBlobs = (uint32_T)(maxNumBlobsReached ? (int32_T)threshold : (int32_T)
                        (uint8_T)(threshold - 1U));
  for (i = 0; i < (int32_T)numBlobs; i++) {
    camRGB_B.BlobAnalysis_o1[i] = (int32_T)BlobAnalysis_NUM_PIX_DW[i];
  }

  camRGB_DW.BlobAnalysis_DIMS1[0] = (int32_T)numBlobs;
  camRGB_DW.BlobAnalysis_DIMS1[1] = 1;
  camRGB_DW.BlobAnalysis_DIMS2[0] = (int32_T)numBlobs;
  camRGB_DW.BlobAnalysis_DIMS2[1] = 2;

  /* End of S-Function (svipblob): '<S1>/Blob Analysis' */

  /* MATLAB Function: '<S1>/FindBlobs1' */
  /* MATLAB Function 'Blob extraction /FindBlobs1': '<S8>:1' */
  /*  area */
  /* '<S8>:1:4' */
  camRGB_B.lengthh = camRGB_DW.BlobAnalysis_DIMS1[0];
}

/* Model update function */
void camRGB_update(void)
{
  /* signal main to stop simulation */
  {                                    /* Sample time: [0.1s, 0.0s] */
    if ((rtmGetTFinal(camRGB_M)!=-1) &&
        !((rtmGetTFinal(camRGB_M)-camRGB_M->Timing.taskTime0) >
          camRGB_M->Timing.taskTime0 * (DBL_EPSILON))) {
      rtmSetErrorStatus(camRGB_M, "Simulation finished");
    }

    if (rtmGetStopRequested(camRGB_M)) {
      rtmSetErrorStatus(camRGB_M, "Simulation finished");
    }
  }

  /* Update absolute time for base rate */
  /* The "clockTick0" counts the number of times the code of this task has
   * been executed. The absolute time is the multiplication of "clockTick0"
   * and "Timing.stepSize0". Size of "clockTick0" ensures timer will not
   * overflow during the application lifespan selected.
   */
  camRGB_M->Timing.taskTime0 =
    (++camRGB_M->Timing.clockTick0) * camRGB_M->Timing.stepSize0;
}

/* Model initialize function */
void camRGB_initialize(void)
{
  /* Registration code */

  /* initialize real-time model */
  (void) memset((void *)camRGB_M, 0,
                sizeof(RT_MODEL_camRGB_T));
  rtmSetTFinal(camRGB_M, -1);
  camRGB_M->Timing.stepSize0 = 0.1;

  /* External mode info */
  camRGB_M->Sizes.checksums[0] = (857854175U);
  camRGB_M->Sizes.checksums[1] = (3600275443U);
  camRGB_M->Sizes.checksums[2] = (2401048486U);
  camRGB_M->Sizes.checksums[3] = (4102994626U);

  {
    static const sysRanDType rtAlwaysEnabled = SUBSYS_RAN_BC_ENABLE;
    static RTWExtModeInfo rt_ExtModeInfo;
    static const sysRanDType *systemRan[3];
    camRGB_M->extModeInfo = (&rt_ExtModeInfo);
    rteiSetSubSystemActiveVectorAddresses(&rt_ExtModeInfo, systemRan);
    systemRan[0] = &rtAlwaysEnabled;
    systemRan[1] = &rtAlwaysEnabled;
    systemRan[2] = &rtAlwaysEnabled;
    rteiSetModelMappingInfoPtr(camRGB_M->extModeInfo,
      &camRGB_M->SpecialInfo.mappingInfo);
    rteiSetChecksumsPtr(camRGB_M->extModeInfo, camRGB_M->Sizes.checksums);
    rteiSetTPtr(camRGB_M->extModeInfo, rtmGetTPtr(camRGB_M));
  }

  /* block I/O */
  (void) memset(((void *) &camRGB_B), 0,
                sizeof(B_camRGB_T));

  /* states (dwork) */
  (void) memset((void *)&camRGB_DW, 0,
                sizeof(DW_camRGB_T));

  /* external inputs */
  camRGB_U.GameOn = 0.0;

  /* external outputs */
  camRGB_Y.Info = 0.0;

  /* data type transition information */
  {
    static DataTypeTransInfo dtInfo;
    (void) memset((char_T *) &dtInfo, 0,
                  sizeof(dtInfo));
    camRGB_M->SpecialInfo.mappingInfo = (&dtInfo);
    dtInfo.numDataTypes = 22;
    dtInfo.dataTypeSizes = &rtDataTypeSizes[0];
    dtInfo.dataTypeNames = &rtDataTypeNames[0];

    /* Block I/O transition table */
    dtInfo.B = &rtBTransTable;

    /* Parameters transition table */
    dtInfo.P = &rtPTransTable;
  }

  /* Start for S-Function (v4l2_video_capture_sfcn): '<Root>/V4L2 Video Capture' */
  MW_videoCaptureInit(camRGB_ConstP.V4L2VideoCapture_p1, 0, 0, 0, 0, 320U, 240U,
                      2U, 2U, 1U, 0.1);
}

/* Model terminate function */
void camRGB_terminate(void)
{
  /* Terminate for S-Function (v4l2_video_capture_sfcn): '<Root>/V4L2 Video Capture' */
  MW_videoCaptureTerminate(camRGB_ConstP.V4L2VideoCapture_p1);
}

/*
 * File trailer for generated code.
 *
 * [EOF]
 */

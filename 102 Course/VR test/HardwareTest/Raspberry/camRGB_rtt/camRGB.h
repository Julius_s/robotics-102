/*
 * Academic License - for use in teaching, academic research, and meeting
 * course requirements at degree granting institutions only.  Not for
 * government, commercial, or other organizational use.
 *
 * File: camRGB.h
 *
 * Code generated for Simulink model 'camRGB'.
 *
 * Model version                  : 1.1869
 * Simulink Coder version         : 8.8 (R2015a) 09-Feb-2015
 * TLC version                    : 8.8 (Jan 20 2015)
 * C/C++ source code generated on : Wed May 27 19:31:05 2015
 *
 * Target selection: realtime.tlc
 * Embedded hardware selection: ARM Compatible->ARM Cortex
 * Code generation objectives: Unspecified
 * Validation result: Not run
 */

#ifndef RTW_HEADER_camRGB_h_
#define RTW_HEADER_camRGB_h_
#include <math.h>
#include <string.h>
#include <float.h>
#include <stddef.h>
#ifndef camRGB_COMMON_INCLUDES_
# define camRGB_COMMON_INCLUDES_
#include "rtwtypes.h"
#include "rtw_extmode.h"
#include "sysran_types.h"
#include "rtw_continuous.h"
#include "rtw_solver.h"
#include "dt_info.h"
#include "ext_work.h"
#include "v4l2_capture.h"
#endif                                 /* camRGB_COMMON_INCLUDES_ */

#include "camRGB_types.h"

/* Shared type includes */
#include "multiword_types.h"

/* Macros for accessing real-time model data structure */
#ifndef rtmGetFinalTime
# define rtmGetFinalTime(rtm)          ((rtm)->Timing.tFinal)
#endif

#ifndef rtmGetErrorStatus
# define rtmGetErrorStatus(rtm)        ((rtm)->errorStatus)
#endif

#ifndef rtmSetErrorStatus
# define rtmSetErrorStatus(rtm, val)   ((rtm)->errorStatus = (val))
#endif

#ifndef rtmGetStopRequested
# define rtmGetStopRequested(rtm)      ((rtm)->Timing.stopRequestedFlag)
#endif

#ifndef rtmSetStopRequested
# define rtmSetStopRequested(rtm, val) ((rtm)->Timing.stopRequestedFlag = (val))
#endif

#ifndef rtmGetStopRequestedPtr
# define rtmGetStopRequestedPtr(rtm)   (&((rtm)->Timing.stopRequestedFlag))
#endif

#ifndef rtmGetT
# define rtmGetT(rtm)                  ((rtm)->Timing.taskTime0)
#endif

#ifndef rtmGetTFinal
# define rtmGetTFinal(rtm)             ((rtm)->Timing.tFinal)
#endif

/* Block signals (auto storage) */
typedef struct {
  uint8_T hG[76800];
  uint8_T BI[76800];                   /* '<S1>/MATLAB Function' */
  uint8_T GI[76800];                   /* '<S1>/MATLAB Function' */
  uint8_T V4L2VideoCapture_o1[76800];  /* '<Root>/V4L2 Video Capture' */
  uint8_T V4L2VideoCapture_o2[76800];  /* '<Root>/V4L2 Video Capture' */
  uint8_T V4L2VideoCapture_o3[76800];  /* '<Root>/V4L2 Video Capture' */
  real_T lengthh;                      /* '<S1>/FindBlobs1' */
  int32_T BlobAnalysis_o1[16];         /* '<S1>/Blob Analysis' */
  boolean_T Compare[76800];            /* '<S6>/Compare' */
  boolean_T Autothreshold_o1[76800];   /* '<S1>/Autothreshold' */
  boolean_T Autothreshold2_o1[76800];  /* '<S1>/Autothreshold2' */
} B_camRGB_T;

/* Block states (auto storage) for system '<Root>' */
typedef struct {
  uint32_T BlobAnalysis_STACK_DW[76800];/* '<S1>/Blob Analysis' */
  uint8_T BlobAnalysis_PAD_DW[77924];  /* '<S1>/Blob Analysis' */
  struct {
    void *LoggedData;
  } Scope_PWORK;                       /* '<S1>/Scope' */

  struct {
    void *LoggedData;
  } Scope1_PWORK;                      /* '<S1>/Scope1' */

  int32_T BlobAnalysis_DIMS1[2];       /* '<S1>/Blob Analysis' */
  int32_T BlobAnalysis_DIMS2[2];       /* '<S1>/Blob Analysis' */
} DW_camRGB_T;

/* Constant parameters (auto storage) */
typedef struct {
  /* Computed Parameter: BlobAnalysis_WALKER_R
   * Referenced by: '<S1>/Blob Analysis'
   */
  int32_T BlobAnalysis_WALKER_R[8];

  /* Expression: devName
   * Referenced by: '<Root>/V4L2 Video Capture'
   */
  uint8_T V4L2VideoCapture_p1[12];
} ConstP_camRGB_T;

/* External inputs (root inport signals with auto storage) */
typedef struct {
  real_T GameOn;                       /* '<Root>/GameOn' */
} ExtU_camRGB_T;

/* External outputs (root outports fed by signals with auto storage) */
typedef struct {
  real_T Info;                         /* '<Root>/Info' */
} ExtY_camRGB_T;

/* Parameters (auto storage) */
struct P_camRGB_T_ {
  uint8_T Autothreshold_BIN_BOUNDARY_FIXP[256];/* Computed Parameter: Autothreshold_BIN_BOUNDARY_FIXP
                                                * Referenced by: '<S1>/Autothreshold'
                                                */
  uint8_T Autothreshold2_BIN_BOUNDARY_FIX[256];/* Computed Parameter: Autothreshold2_BIN_BOUNDARY_FIX
                                                * Referenced by: '<S1>/Autothreshold2'
                                                */
  uint8_T Constant_Value;              /* Computed Parameter: Constant_Value
                                        * Referenced by: '<S6>/Constant'
                                        */
};

/* Real-time Model Data Structure */
struct tag_RTM_camRGB_T {
  const char_T *errorStatus;
  RTWExtModeInfo *extModeInfo;

  /*
   * Sizes:
   * The following substructure contains sizes information
   * for many of the model attributes such as inputs, outputs,
   * dwork, sample times, etc.
   */
  struct {
    uint32_T checksums[4];
  } Sizes;

  /*
   * SpecialInfo:
   * The following substructure contains special information
   * related to other components that are dependent on RTW.
   */
  struct {
    const void *mappingInfo;
  } SpecialInfo;

  /*
   * Timing:
   * The following substructure contains information regarding
   * the timing information for the model.
   */
  struct {
    time_T taskTime0;
    uint32_T clockTick0;
    time_T stepSize0;
    time_T tFinal;
    boolean_T stopRequestedFlag;
  } Timing;
};

/* Block parameters (auto storage) */
extern P_camRGB_T camRGB_P;

/* Block signals (auto storage) */
extern B_camRGB_T camRGB_B;

/* Block states (auto storage) */
extern DW_camRGB_T camRGB_DW;

/* External inputs (root inport signals with auto storage) */
extern ExtU_camRGB_T camRGB_U;

/* External outputs (root outports fed by signals with auto storage) */
extern ExtY_camRGB_T camRGB_Y;

/* Constant parameters (auto storage) */
extern const ConstP_camRGB_T camRGB_ConstP;

/* Model entry point functions */
extern void camRGB_initialize(void);
extern void camRGB_output(void);
extern void camRGB_update(void);
extern void camRGB_terminate(void);

/* Real-time Model object */
extern RT_MODEL_camRGB_T *const camRGB_M;

/*-
 * The generated code includes comments that allow you to trace directly
 * back to the appropriate location in the model.  The basic format
 * is <system>/block_name, where system is the system number (uniquely
 * assigned by Simulink) and block_name is the name of the block.
 *
 * Use the MATLAB hilite_system command to trace the generated code back
 * to the model.  For example,
 *
 * hilite_system('<S3>')    - opens system 3
 * hilite_system('<S3>/Kp') - opens and selects block Kp which resides in S3
 *
 * Here is the system hierarchy for this model
 *
 * '<Root>' : 'camRGB'
 * '<S1>'   : 'camRGB/Blob extraction '
 * '<S2>'   : 'camRGB/Chart'
 * '<S3>'   : 'camRGB/Player Tracker'
 * '<S4>'   : 'camRGB/encode'
 * '<S5>'   : 'camRGB/parseBlob'
 * '<S6>'   : 'camRGB/Blob extraction /Compare To Zero'
 * '<S7>'   : 'camRGB/Blob extraction /FindBlobs'
 * '<S8>'   : 'camRGB/Blob extraction /FindBlobs1'
 * '<S9>'   : 'camRGB/Blob extraction /MATLAB Function'
 */
#endif                                 /* RTW_HEADER_camRGB_h_ */

/*
 * File trailer for generated code.
 *
 * [EOF]
 */

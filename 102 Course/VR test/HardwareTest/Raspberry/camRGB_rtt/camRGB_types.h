/*
 * Academic License - for use in teaching, academic research, and meeting
 * course requirements at degree granting institutions only.  Not for
 * government, commercial, or other organizational use.
 *
 * File: camRGB_types.h
 *
 * Code generated for Simulink model 'camRGB'.
 *
 * Model version                  : 1.1869
 * Simulink Coder version         : 8.8 (R2015a) 09-Feb-2015
 * TLC version                    : 8.8 (Jan 20 2015)
 * C/C++ source code generated on : Wed May 27 19:31:05 2015
 *
 * Target selection: realtime.tlc
 * Embedded hardware selection: ARM Compatible->ARM Cortex
 * Code generation objectives: Unspecified
 * Validation result: Not run
 */

#ifndef RTW_HEADER_camRGB_types_h_
#define RTW_HEADER_camRGB_types_h_
#include "rtwtypes.h"
#include "multiword_types.h"

/* Parameters (auto storage) */
typedef struct P_camRGB_T_ P_camRGB_T;

/* Forward declaration for rtModel */
typedef struct tag_RTM_camRGB_T RT_MODEL_camRGB_T;

#endif                                 /* RTW_HEADER_camRGB_types_h_ */

/*
 * File trailer for generated code.
 *
 * [EOF]
 */

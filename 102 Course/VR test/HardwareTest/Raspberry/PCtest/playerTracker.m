function [players,ball] = playerTracker(playerData,BallData)
% fGol, fDef, fOff, eGol, eDef, eOff
persistent latestPlayers;
persistent latestBall;
%Green
if isempty(latestPlayers)
    latestPlayers.x=ones(1,6,'int8').*int8([-20 -50 -80 20 50 80]);    
    latestPlayers.y=ones(1,6,'int8').*int8(0);
    latestPlayers.orientation=[zeros(1,3,'int16')...
        ones(1,3,'int16')*int16(180) ];
    latestPlayers.color=uint8('gggbbb');
    latestPlayers.position=uint8('odgodg');
    latestPlayers.valid=zeros(1,6,'uint8');
end
%Ball
if isempty(latestBall)
    latestBall.x=int8(0);
    latestBall.y=int8(0);
    latestBall.valid=uint8(1);
end

%% set all valids to false
latestPlayers.valid(:)=zeros(1,6,'uint8');
latestBall.valid=uint8(0);

if ~isempty(BallData.x)
    latestBall.x=BallData.x(1);
    latestBall.y=BallData.y(1);
    latestBall.valid=uint8(1);
end

for ii=1:length(playerData.x)
    pos=int8([playerData.x(ii) playerData.y(ii)]);
    dist=intmax;
    index=uint8(0);
    for jj=1:6  
            temp=int32(norm(single(pos-[latestPlayers.x(jj) latestPlayers.y(jj)])));
            if   temp<dist  
                dist= temp;
                index=uint8(jj);
            end     
    end
    if index~=uint8(0)
        latestPlayers.x(index)=playerData.x(ii);
        latestPlayers.y(index)=playerData.y(ii);
        latestPlayers.orientation(index)=playerData.orientation(ii);
        latestPlayers.valid(index)=uint8(1);
    end
end
ball=latestBall;
tempEmpty.x=int8(0);
tempEmpty.y=int8(0);
tempEmpty.orientation=int16(0);
tempEmpty.color=uint8(0);
tempEmpty.position=uint8(0);
tempEmpty.valid=uint8(0);
players=[tempEmpty tempEmpty tempEmpty tempEmpty tempEmpty tempEmpty];
for ii=1:6
    players(ii).x=latestPlayers.x(ii);
    players(ii).y=latestPlayers.y(ii);
    players(ii).orientation=latestPlayers.orientation(ii);
    players(ii).color=latestPlayers.color(ii);
    players(ii).position=latestPlayers.position(ii);
    players(ii).valid=latestPlayers.valid(ii);
end


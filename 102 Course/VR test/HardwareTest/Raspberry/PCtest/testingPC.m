clear cam
clear rpi
fclose(s)
delete(s)
clear s
 clear all
%%
rpi=raspi();
s = serial('COM6');
set(s,'BaudRate',9600);
set(s,'OutputBufferSize',27);
fopen(s);
% cam=cameraboard(rpi);
%%
njk=1;
clear cam
clear hBlob
cam=cameraboard(rpi,'Resolution','320x240','FrameRate', 30,'Contrast',80);
t=[];
bb=[];
hBlob = vision.BlobAnalysis;
hBlob.BoundingBoxOutputPort=0;
hBlob.MinimumBlobArea=5;
while(1==1)
    tic;
Im= snapshot(cam);
I=rgb2hsv(Im);
%%common v
channel3Min=0.4;
% channel2Min=0.4;
%% hue vals
channel1MinRed = 0.8;
channel1MaxRed = 0.04;

channel1BlueMin = 0.45;
channel1BlueMax = 0.61;
% channel3BlueMin = 0.5;

%% ball vals
redBool = ((I(:,:,1) >= channel1MinRed ) | I(:,:,1) <= channel1MaxRed) & ...
          I(:,:,3) >= channel3Min ;   
blueBool = (I(:,:,1) >= channel1BlueMin ) & (I(:,:,1)<= channel1BlueMax) & ...
          I(:,:,3) >= channel3Min ;
   
%%
bool= imdilate((redBool+blueBool)>0,ones(2));
 bool= imerode(bool,strel('square',4));
%  figure(1);
%  imshow(bool);
 [area,centroids]=step(hBlob,bool);
%%
blobs.centroid=zeros(0,2,'single');
blobs.area=zeros(0,1,'int32');
blobs.color=zeros(0,1,'uint8');
centroidsInt=floor(centroids);
for ii = 1:length(area)
    cr=redBool(centroidsInt(ii,2),centroidsInt(ii,1));
    cb=blueBool(centroidsInt(ii,2),centroidsInt(ii,1));
    colorVec=[cr cb];
    color=find(colorVec);
    if length(color)==1
        blobs.centroid=[blobs.centroid ; centroids(ii,:)];        
        blobs.area=[blobs.area ; area(ii)];
        blobs.color=[ blobs.color ;uint8(color(1,1))] ;
    end
end
% blobs.centroid
[playerData, BallData]=position(blobs,[320;240]);
% xxx=playerData.x
% yyy=playerData.y
[players,ball] = playerTracker(playerData,BallData);
% figure(2);
plot([-100 -100 100 100],[-100 100 100 -100],'.k');
% p1=[players(1).x players(1).y;...
%     players(1).x+int8(cosd(single(players(1).orientation))*10) ...
%     players(1).y+int8(sind(single(players(1).orientation))*10)];
% p2=[players(2).x players(2).y;...
%     players(2).x+int8(cosd(single(players(2).orientation))*10) ...
%     players(2).y+int8(sind(single(players(2).orientation))*10)];
% p3=[players(3).x players(3).y;...
%     players(3).x+int8(cosd(single(players(3).orientation))*10) ...
%     players(3).y+int8(sind(single(players(3).orientation))*10)];
% p4=[players(4).x players(4).y;...
%     players(4).x+int8(cosd(single(players(4).orientation))*10) ...
%     players(4).y+int8(sind(single(players(4).orientation))*10)];
% p5=[players(5).x players(5).y;...
%     players(5).x+int8(cosd(single(players(5).orientation))*10) ...
%     players(5).y+int8(sind(single(players(5).orientation))*10)];
% p6=[players(6).x players(6).y;...
%     players(6).x+int8(cosd(single(players(6).orientation))*10) ...
%     players(6).y+int8(sind(single(players(6).orientation))*10)];
   hold on;
   plot(players(1).x,players(1).y,'.r','MarkerSize',20); %* offence
   plot(players(2).x,players(2).y,'.r','MarkerSize',10); %* offence
   plot(players(3).x,players(3).y,'.r','MarkerSize',5); %* offence
   plot(players(4).x,players(4).y,'.b','MarkerSize',20); %* offence
   plot(players(5).x,players(5).y,'.b','MarkerSize',10); %* offence
   plot(players(6).x,players(6).y,'.b','MarkerSize',5); %* offence
x=[players(1).x;players(2).x;players(3).x;players(4).x;players(5).x;players(6).x];
y=[players(1).y;players(2).y;players(3).y;players(4).y;players(5).y;players(6).y];
o=single([players(1).orientation;players(2).orientation;players(3).orientation;players(4).orientation;players(5).orientation;players(6).orientation]);
u=int8(cosd(o)*1); 
v=int8(sind(o)*1); 
quiver(x,y,u,v,0.1,'AutoScale','off','MaxHeadSize',0.1)
[players(3).x players(3).y]'
         plot(ball.x,ball.y,'.r','MarkerSize',50);
         hold off;
         drawnow; 
        info=encode(players,uint8(1),ball); 
% fwrite(s,uint8('1234567890qwertyuiopasdfghj'));
        fwrite(s,info);
        % toc
% t=[t toc];
t=toc;

% while t<0.1
%   t=toc;  
% end
% t
% njk=njk+1
end
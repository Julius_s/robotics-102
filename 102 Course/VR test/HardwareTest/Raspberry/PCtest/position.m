function [players, Ball]=position(blobs,camRes)
[~, blobIndices] = sort(blobs.area);
lenA=length(blobIndices);
Ball.x=zeros(0,1,'int8');
Ball.y=zeros(0,1,'int8');
%% Coefficients from curve fitting
%x
       ax =       0.587;
       bx =     -0.2143 ;
       %y
       ay =      0.5877 ;
       by =      0.2332 ;
%% calc position
allLights=zeros(3,lenA,'single');
if ~isempty(blobIndices)
    for ii=1:lenA
        jj=blobIndices(ii);
        blobs.centroid(jj,:)=[blobs.centroid(jj,1) camRes(2)-blobs.centroid(jj,2)];
        pxFromCentre= blobs.centroid(jj,:)' -camRes/2;        
        pos=single([ax*pxFromCentre(1)+bx ; ay*pxFromCentre(2)+by]);
        allLights(:,jj)=[pos(1);pos(2);single(blobs.color(jj))];
    end
%    largestThing= blobs.area(blobIndices(lenA))
    if blobs.area(blobIndices(lenA))>50;
        Ball.x=int8(allLights(1,blobIndices(lenA)));
        Ball.y=int8(allLights(2,blobIndices(lenA)));
        allLights=[allLights(:,1:blobIndices(lenA)-1) allLights(:,blobIndices(lenA)+1:end)];
        lenA=lenA-1;
    end
end
% allLights
% create light arrays
reds=allLights(1:2,allLights(3,:)==1);
blues=allLights(1:2,allLights(3,:)==2);
% allLights
lenRed=length(reds(1,:));
lenBlue=length(blues(1,:));
players.x=zeros(0,1,'int8');
players.y=zeros(0,1,'int8');
players.orientation=zeros(0,1,'int16');
players.color=zeros(0,1,'uint8');
% lenRed
% lenBlue
if lenRed>0
    if lenBlue>0
        for ii=1:lenBlue
            lightBlue=blues(:,ii);
            dist=single(intmax);
            index=uint8(0);
            for jj=1:lenRed
                lightRed=reds(:,jj);
                temp=norm(lightBlue-lightRed);
                if  temp<dist
                    dist=temp;
                    index=uint8(jj);
                end
            end
            if index~=uint8(0)
                lightRed=reds(:,index);
                Dpos=lightBlue-lightRed;
                orientation=atan2d(Dpos(2),Dpos(1));
                players.x=[ players.x ;int8(lightRed(1))];
                players.y=[players.y ;int8(lightRed(2))];
                players.orientation=[players.orientation ;int16(orientation)];
                players.color=[players.color ;uint8(0)];
            end
        end
    end
end

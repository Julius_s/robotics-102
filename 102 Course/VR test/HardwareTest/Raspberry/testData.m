ballData=all.signals.values(1,2:3,:);
p1=all.signals.values(1,4:7,:);
p2=all.signals.values(1,8:11,:);
p3=all.signals.values(1,12:15,:);
p4=all.signals.values(1,16:19,:);
p5=all.signals.values(1,20:23,:);
p6=all.signals.values(1,24:27,:);
q11=zeros(2,432);
q1=zeros(3,432);
q2=zeros(3,432);
q3=zeros(3,432);
q4=zeros(3,432);
q5=zeros(3,432);
q6=zeros(3,432);
for ii=1:432
 q11(1,ii)=typecast(ballData(1,1,ii),'int8');   
 q11(2,ii)=typecast(ballData(1,2,ii),'int8');   
   q1(1,ii)=typecast(p1(1,1,ii),'int8');   
 q1(2,ii)=typecast(p1(1,2,ii),'int8'); 
  q2(1,ii)=typecast(p2(1,1,ii),'int8');   
 q2(2,ii)=typecast(p2(1,2,ii),'int8'); 
 
   q3(1,ii)=typecast(p3(1,1,ii),'int8');   
 q3(2,ii)=typecast(p3(1,2,ii),'int8'); 
   q4(1,ii)=typecast(p4(1,1,ii),'int8');   
 q4(2,ii)=typecast(p4(1,2,ii),'int8'); 
   q5(1,ii)=typecast(p5(1,1,ii),'int8');   
 q5(2,ii)=typecast(p5(1,2,ii),'int8'); 
   q6(1,ii)=typecast(p6(1,1,ii),'int8');   
 q6(2,ii)=typecast(p6(1,2,ii),'int8'); 
    
end

%%
for ii=130:150
    plot([-200 -200 200 200 q11(1,ii) q1(1,ii) q2(1,ii) q3(1,ii) q4(1,ii) q5(1,ii) q6(1,ii)],[-200 200 200 -200 q11(2,ii) q1(2,ii) q2(2,ii) q3(2,ii) q4(2,ii) q5(2,ii) q6(2,ii)],'.r');
    drawnow;
    pause(0.1);
    ii
end

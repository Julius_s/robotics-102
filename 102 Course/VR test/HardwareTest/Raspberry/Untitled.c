/*
 * Software serial multple serial test
 *
 * Receives from the two software serial ports,
 * sends to the hardware serial port.
 *
 * In order to listen on a software port, you call port.listen().
 * When using two software serial ports, you have to switch ports
 * by listen()ing on each one in turn. Pick a logical time to switch
 * ports, like the end of an expected transmission, or when the
 * buffer is empty. This example switches ports when there is nothing
 * more to read from a port
 *
 * The circuit:
 * Two devices which communicate serially are needed.
 * First serial device's TX attached to digital pin 2, RX to pin 3
 * Second serial device's TX attached to digital pin 4, RX to pin 5
 *
 * Note:
 * Not all pins on the Mega and Mega 2560 support change interrupts,
 * so only the following can be used for RX:
 * 10, 11, 12, 13, 50, 51, 52, 53, 62, 63, 64, 65, 66, 67, 68, 69
 *
 * Not all pins on the Leonardo support change interrupts,
 * so only the following can be used for RX:
 * 8, 9, 10, 11, 14 (MISO), 15 (SCK), 16 (MOSI).
 *
 * created 18 Apr. 2011
 * modified 25 May 2012
 * by Tom Igoe
 * based on Mikal Hart's twoPortRXExample
 *
 * This example code is in the public domain.
 *
 */

#include <SoftwareSerial.h>
// software serial #1: TX = digital pin 10, RX = digital pin 11
// software serial tx rx are switched"!!!!
SoftwareSerial portOne(10,11);//Listen To raspPi
SoftwareSerial portTwo(8,9);//Write To RF

void setup()
{
    // Start each software serial port
    Serial.begin(9600);
    portOne.begin(9600);
    portTwo.begin(9600);
    pinMode(13, OUTPUT);
    portOne.listen();
}

void loop()
{
    // By default, the last intialized port is listening.
    // when you want to listen on a port, explicitly select it:
    
    //Serial.println("Data from port one:");
    // while there is data coming in, read it
    // and send to the hardware serial port:
    while (portOne.available() > 0) {
        //    digitalWrite(13, HIGH);   // turn the LED on (HIGH is the voltage level)
        //  delay(1000);              // wait for a second
        //  digitalWrite(13, LOW);    // turn the LED off by making the voltage LOW
        //  delay(1000);              // wait for a second
        char inByte = portOne.read();
        if(inByte=='U'){
            while (portOne.available() == 0);
            char inByte = portOne.read();
            if(inByte=='U'){
                while (portOne.available() == 0);
                char inByte = portOne.read();
                if(inByte=='z'){
                    while (portOne.available() == 0);
                    char inByte = portOne.read();
                    if(inByte=='7'){
                        while (portOne.available() < 27);
                        Serial.write("27 received");
                    }
                }
            }
        }
        Serial.write(inByte);
    }
    Serial.write("\n");
    //portTwo.write("noise");
    // Serial.write("noise\n");
    delay(100);
}
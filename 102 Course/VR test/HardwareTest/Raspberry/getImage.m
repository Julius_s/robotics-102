rpi=raspi();
% cam=cameraboard(rpi);
%%
clear cam
cam=cameraboard(rpi,'Resolution','320x240');
% for ii=1:30
  while 1
   Im= snapshot(cam);
   image(Im);
   drawnow;
end

trueIm=Im./30;
for ii=1:29
   Im= snapshot(cam);
   trueIm=trueIm+Im./30;
   image(trueIm);
   drawnow;
end

imwrite(Im,'ballGReenBlueRed2.jpg');

%%
clear cam
cam=cameraboard(rpi,'Resolution','320x240');
while(1==1)
    pause(0.1);
Im= snapshot(cam);
I=rgb2hsv(Im);
%%common v
channel3Min=0.8;
channel2Max=0.3;
%% hue vals
channel1MinRed = 0.85;
channel1MaxRed = 0.15;

channel1BlueMin = 0.35;
channel1BlueMax = 0.65;

%% ball vals

channel1MinBall = 0.8;
channel1MaxBall = 0.15;
channel2MinBall = 0.4;
channel3MinBall = 0.5;

ballBool = (I(:,:,1) >= channel1MinBall | (I(:,:,1) <= channel1MaxBall)) & ...
    I(:,:,2) >= channel2MinBall & ...
    I(:,:,3) >= channel3MinBall ;
%%
redBool = ((I(:,:,1) >= channel1MinRed ) | (I(:,:,1) <= channel1MaxRed) )& ...
         I(:,:,2) <= channel2Max & I(:,:,3) >= channel3Min ;
   
blueBool = (I(:,:,1) >= channel1BlueMin ) & (I(:,:,1) <= channel1BlueMax) & ...
         I(:,:,2) <= channel2Max & I(:,:,3) >= channel3Min ;

   
  
image(cat(3,cat(3,(redBool)*255,(ballBool)*255),(blueBool)*255));
% imshow((blueBool+redBool+greenBool+ballBool)*100);
drawnow;

end
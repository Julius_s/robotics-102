/*
 * File: greenOffence_types.h
 *
 * Code generated for Simulink model 'greenOffence'.
 *
 * Model version                  : 1.14
 * Simulink Coder version         : 8.7 (R2014b) 08-Sep-2014
 * TLC version                    : 8.7 (Aug  5 2014)
 * C/C++ source code generated on : Sun Mar 01 21:32:23 2015
 *
 * Target selection: realtime.tlc
 * Embedded hardware selection: Atmel->AVR
 * Code generation objectives: Unspecified
 * Validation result: Not run
 */

#ifndef RTW_HEADER_greenOffence_types_h_
#define RTW_HEADER_greenOffence_types_h_
#include "rtwtypes.h"
#include "multiword_types.h"
#ifndef _DEFINED_TYPEDEF_FOR_Waypoint_
#define _DEFINED_TYPEDEF_FOR_Waypoint_

typedef struct {
  int8_T x;
  int8_T y;
  int16_T orientation;
} Waypoint;

#endif

#ifndef _DEFINED_TYPEDEF_FOR_Player_
#define _DEFINED_TYPEDEF_FOR_Player_

typedef struct {
  int8_T x;
  int8_T y;
  int16_T orientation;
  uint8_T color;
  uint8_T position;
  uint8_T valid;
} Player;

#endif

#ifndef _DEFINED_TYPEDEF_FOR_Ball_
#define _DEFINED_TYPEDEF_FOR_Ball_

typedef struct {
  int8_T x;
  int8_T y;
  uint8_T valid;
} Ball;

#endif

/* Custom Type definition for MATLAB Function: '<S4>/Parser' */
#include <stddef.h>

/* Parameters (auto storage) */
typedef struct P_greenOffence_T_ P_greenOffence_T;

/* Forward declaration for rtModel */
typedef struct tag_RTM_greenOffence_T RT_MODEL_greenOffence_T;

#endif                                 /* RTW_HEADER_greenOffence_types_h_ */

/*
 * File trailer for generated code.
 *
 * [EOF]
 */

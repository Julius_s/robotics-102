/*
 * File: greenOffence_data.c
 *
 * Code generated for Simulink model 'greenOffence'.
 *
 * Model version                  : 1.14
 * Simulink Coder version         : 8.7 (R2014b) 08-Sep-2014
 * TLC version                    : 8.7 (Aug  5 2014)
 * C/C++ source code generated on : Sun Mar 01 21:32:23 2015
 *
 * Target selection: realtime.tlc
 * Embedded hardware selection: Atmel->AVR
 * Code generation objectives: Unspecified
 * Validation result: Not run
 */

#include "greenOffence.h"
#include "greenOffence_private.h"

/* Block parameters (auto storage) */
P_greenOffence_T greenOffence_P = {
  32767,                               /* Mask Parameter: CompareToConstant_const
                                        * Referenced by: '<S10>/Constant'
                                        */
  0.0,                                 /* Expression: 0
                                        * Referenced by: '<S9>/Constant'
                                        */
  0.5,                                 /* Expression: 1/2
                                        * Referenced by: '<S7>/Constant'
                                        */
  0.1,                                 /* Expression: 1/10
                                        * Referenced by: '<S5>/PGain'
                                        */
  1.0,                                 /* Expression: 1
                                        * Referenced by: '<S5>/Saturation'
                                        */
  0.0,                                 /* Expression: 0
                                        * Referenced by: '<S5>/Saturation'
                                        */
  1.0,                                 /* Expression: 1
                                        * Referenced by: '<S5>/SaturationOrien1'
                                        */
  -1.0,                                /* Expression: -1
                                        * Referenced by: '<S5>/SaturationOrien1'
                                        */
  1.0,                                 /* Expression: 1
                                        * Referenced by: '<S5>/SaturationOrien2'
                                        */
  -1.0,                                /* Expression: -1
                                        * Referenced by: '<S5>/SaturationOrien2'
                                        */

  /*  Computed Parameter: ManualConst_Value
   * Referenced by: '<Root>/ManualConst'
   */
  { 68.0F, -55.0F, 32767.0F },
  23302,                               /* Computed Parameter: PGainOrien_Gain
                                        * Referenced by: '<S5>/PGainOrien'
                                        */
  -32768,                              /* Computed Parameter: Gain1_Gain
                                        * Referenced by: '<S5>/Gain1'
                                        */
  1U,                                  /* Computed Parameter: Constant3_Value
                                        * Referenced by: '<Root>/Constant3'
                                        */
  80U                                  /* Computed Parameter: certainty_Value
                                        * Referenced by: '<S9>/certainty'
                                        */
};

/*
 * File trailer for generated code.
 *
 * [EOF]
 */

/*
 * File: greenOffence.h
 *
 * Code generated for Simulink model 'greenOffence'.
 *
 * Model version                  : 1.14
 * Simulink Coder version         : 8.7 (R2014b) 08-Sep-2014
 * TLC version                    : 8.7 (Aug  5 2014)
 * C/C++ source code generated on : Sun Mar 01 21:32:23 2015
 *
 * Target selection: realtime.tlc
 * Embedded hardware selection: Atmel->AVR
 * Code generation objectives: Unspecified
 * Validation result: Not run
 */

#ifndef RTW_HEADER_greenOffence_h_
#define RTW_HEADER_greenOffence_h_
#include <math.h>
#include <float.h>
#include <string.h>
#include <stddef.h>
#ifndef greenOffence_COMMON_INCLUDES_
# define greenOffence_COMMON_INCLUDES_
#include "rtwtypes.h"
#include "rtw_continuous.h"
#include "rtw_solver.h"
#endif                                 /* greenOffence_COMMON_INCLUDES_ */

#include "greenOffence_types.h"

/* Shared type includes */
#include "multiword_types.h"
#include "rtGetNaN.h"
#include "rt_nonfinite.h"
#include "rt_defines.h"
#include "rtGetInf.h"

/* Macros for accessing real-time model data structure */
#ifndef rtmGetErrorStatus
# define rtmGetErrorStatus(rtm)        ((rtm)->errorStatus)
#endif

#ifndef rtmSetErrorStatus
# define rtmSetErrorStatus(rtm, val)   ((rtm)->errorStatus = (val))
#endif

/* Block signals (auto storage) */
typedef struct {
  Waypoint finalWay;                   /* '<Root>/Player Decisions' */
  Player Selector;                     /* '<Root>/Selector' */
  Player players[6];                   /* '<S4>/Parser' */
  Ball Ball_n;                         /* '<S4>/Parser' */
} B_greenOffence_T;

/* Block states (auto storage) for system '<Root>' */
typedef struct {
  Waypoint shootWay;                   /* '<Root>/Player Decisions' */
  Player enemy[3];                     /* '<Root>/Player Decisions' */
  int8_T startingPos[2];               /* '<Root>/Player Decisions' */
  uint8_T lastMessage[27];             /* '<S4>/Parser' */
  uint8_T is_active_c11_greenOffence;  /* '<Root>/Player Decisions' */
  uint8_T is_c11_greenOffence;         /* '<Root>/Player Decisions' */
  uint8_T is_GameIsOn_Offence;         /* '<Root>/Player Decisions' */
  uint8_T is_waiting;                  /* '<Root>/Player Decisions' */
  uint8_T temporalCounter_i1;          /* '<Root>/Player Decisions' */
  boolean_T lastMessage_not_empty;     /* '<S4>/Parser' */
} DW_greenOffence_T;

/* Parameters (auto storage) */
struct P_greenOffence_T_ {
  int16_T CompareToConstant_const;     /* Mask Parameter: CompareToConstant_const
                                        * Referenced by: '<S10>/Constant'
                                        */
  real_T Constant_Value;               /* Expression: 0
                                        * Referenced by: '<S9>/Constant'
                                        */
  real_T Constant_Value_n;             /* Expression: 1/2
                                        * Referenced by: '<S7>/Constant'
                                        */
  real_T PGain_Gain;                   /* Expression: 1/10
                                        * Referenced by: '<S5>/PGain'
                                        */
  real_T Saturation_UpperSat;          /* Expression: 1
                                        * Referenced by: '<S5>/Saturation'
                                        */
  real_T Saturation_LowerSat;          /* Expression: 0
                                        * Referenced by: '<S5>/Saturation'
                                        */
  real_T SaturationOrien1_UpperSat;    /* Expression: 1
                                        * Referenced by: '<S5>/SaturationOrien1'
                                        */
  real_T SaturationOrien1_LowerSat;    /* Expression: -1
                                        * Referenced by: '<S5>/SaturationOrien1'
                                        */
  real_T SaturationOrien2_UpperSat;    /* Expression: 1
                                        * Referenced by: '<S5>/SaturationOrien2'
                                        */
  real_T SaturationOrien2_LowerSat;    /* Expression: -1
                                        * Referenced by: '<S5>/SaturationOrien2'
                                        */
  real32_T ManualConst_Value[3];       /* Computed Parameter: ManualConst_Value
                                        * Referenced by: '<Root>/ManualConst'
                                        */
  int16_T PGainOrien_Gain;             /* Computed Parameter: PGainOrien_Gain
                                        * Referenced by: '<S5>/PGainOrien'
                                        */
  int16_T Gain1_Gain;                  /* Computed Parameter: Gain1_Gain
                                        * Referenced by: '<S5>/Gain1'
                                        */
  uint8_T Constant3_Value;             /* Computed Parameter: Constant3_Value
                                        * Referenced by: '<Root>/Constant3'
                                        */
  uint8_T certainty_Value;             /* Computed Parameter: certainty_Value
                                        * Referenced by: '<S9>/certainty'
                                        */
};

/* Real-time Model Data Structure */
struct tag_RTM_greenOffence_T {
  const char_T * volatile errorStatus;
};

/* Block parameters (auto storage) */
extern P_greenOffence_T greenOffence_P;

/* Block signals (auto storage) */
extern B_greenOffence_T greenOffence_B;

/* Block states (auto storage) */
extern DW_greenOffence_T greenOffence_DW;

/* Model entry point functions */
extern void greenOffence_initialize(void);
extern void greenOffence_output(void);
extern void greenOffence_update(void);
extern void greenOffence_terminate(void);

/* Real-time Model object */
extern RT_MODEL_greenOffence_T *const greenOffence_M;

/*-
 * The generated code includes comments that allow you to trace directly
 * back to the appropriate location in the model.  The basic format
 * is <system>/block_name, where system is the system number (uniquely
 * assigned by Simulink) and block_name is the name of the block.
 *
 * Use the MATLAB hilite_system command to trace the generated code back
 * to the model.  For example,
 *
 * hilite_system('<S3>')    - opens system 3
 * hilite_system('<S3>/Kp') - opens and selects block Kp which resides in S3
 *
 * Here is the system hierarchy for this model
 *
 * '<Root>' : 'greenOffence'
 * '<S1>'   : 'greenOffence/MATLAB Function'
 * '<S2>'   : 'greenOffence/MATLAB Function1'
 * '<S3>'   : 'greenOffence/Player Decisions'
 * '<S4>'   : 'greenOffence/SerialCom'
 * '<S5>'   : 'greenOffence/control'
 * '<S6>'   : 'greenOffence/SerialCom/Parser'
 * '<S7>'   : 'greenOffence/control/Norm'
 * '<S8>'   : 'greenOffence/control/calc_dO'
 * '<S9>'   : 'greenOffence/control/decisionParser'
 * '<S10>'  : 'greenOffence/control/decisionParser/Compare To Constant'
 * '<S11>'  : 'greenOffence/control/decisionParser/calcTargetOrien'
 * '<S12>'  : 'greenOffence/control/decisionParser/fwdLimit'
 */
#endif                                 /* RTW_HEADER_greenOffence_h_ */

/*
 * File trailer for generated code.
 *
 * [EOF]
 */

/*
 * File: greenOffence.c
 *
 * Code generated for Simulink model 'greenOffence'.
 *
 * Model version                  : 1.14
 * Simulink Coder version         : 8.7 (R2014b) 08-Sep-2014
 * TLC version                    : 8.7 (Aug  5 2014)
 * C/C++ source code generated on : Sun Mar 01 21:32:23 2015
 *
 * Target selection: realtime.tlc
 * Embedded hardware selection: Atmel->AVR
 * Code generation objectives: Unspecified
 * Validation result: Not run
 */

#include "greenOffence.h"
#include "greenOffence_private.h"

/* Named constants for Chart: '<Root>/Player Decisions' */
#define greenOffenc_IN_GameIsOn_Offence ((uint8_T)1U)
#define greenOffence_IN_Aim            ((uint8_T)1U)
#define greenOffence_IN_GetToTheBall   ((uint8_T)2U)
#define greenOffence_IN_Idle           ((uint8_T)3U)
#define greenOffence_IN_Idle1          ((uint8_T)1U)
#define greenOffence_IN_Kick           ((uint8_T)4U)
#define greenOffence_IN_NO_ACTIVE_CHILD ((uint8_T)0U)
#define greenOffence_IN_goToPosition   ((uint8_T)2U)
#define greenOffence_IN_waiting        ((uint8_T)2U)

/* Block signals (auto storage) */
B_greenOffence_T greenOffence_B;

/* Block states (auto storage) */
DW_greenOffence_T greenOffence_DW;

/* Real-time model */
RT_MODEL_greenOffence_T greenOffence_M_;
RT_MODEL_greenOffence_T *const greenOffence_M = &greenOffence_M_;

/* Forward declaration for local functions */
static real32_T greenOffence_norm(const real32_T x[2]);
static uint8_T greenOffence_checkReached_a(const int8_T pos[2], real_T tol);
static void greenOffence_calcShootWay(void);
static uint8_T greenOffence_checkReached(const int8_T pos[2], real_T tol);
static void greenOffence_calcStartPos(void);
static void greenOffence_calcStartTeams(void);
int16_T div_s16_floor(int16_T numerator, int16_T denominator)
{
  int16_T quotient;
  uint16_T absNumerator;
  uint16_T absDenominator;
  uint16_T tempAbsQuotient;
  boolean_T quotientNeedsNegation;
  if (denominator == 0) {
    quotient = numerator >= 0 ? MAX_int16_T : MIN_int16_T;

    /* Divide by zero handler */
  } else {
    absNumerator = (uint16_T)(numerator >= 0 ? numerator : -numerator);
    absDenominator = (uint16_T)(denominator >= 0 ? denominator : -denominator);
    quotientNeedsNegation = ((numerator < 0) != (denominator < 0));
    tempAbsQuotient = absNumerator / absDenominator;
    if (quotientNeedsNegation) {
      absNumerator %= absDenominator;
      if (absNumerator > 0U) {
        tempAbsQuotient++;
      }
    }

    quotient = quotientNeedsNegation ? -(int16_T)tempAbsQuotient : (int16_T)
      tempAbsQuotient;
  }

  return quotient;
}

real_T sMultiWord2Double(const uint32_T u1[], int16_T n1, int16_T e1)
{
  real_T y;
  int16_T i;
  int16_T exp_0;
  uint32_T u1i;
  uint32_T cb;
  y = 0.0;
  exp_0 = e1;
  if ((u1[n1 - 1] & 2147483648UL) != 0UL) {
    cb = 1UL;
    for (i = 0; i < n1; i++) {
      u1i = ~u1[i];
      cb += u1i;
      y -= ldexp(cb, exp_0);
      cb = cb < u1i ? 1UL : 0UL;
      exp_0 += 32;
    }
  } else {
    for (i = 0; i < n1; i++) {
      y += ldexp(u1[i], exp_0);
      exp_0 += 32;
    }
  }

  return y;
}

void sMultiWordMul(const uint32_T u1[], int16_T n1, const uint32_T u2[], int16_T
                   n2, uint32_T y[], int16_T n)
{
  int16_T i;
  int16_T j;
  int16_T k;
  int16_T nj;
  uint32_T u1i;
  uint32_T yk;
  uint32_T a1;
  uint32_T a0;
  uint32_T b1;
  uint32_T w10;
  uint32_T w01;
  uint32_T cb;
  boolean_T isNegative1;
  boolean_T isNegative2;
  uint32_T cb1;
  uint32_T cb2;
  isNegative1 = ((u1[n1 - 1] & 2147483648UL) != 0UL);
  isNegative2 = ((u2[n2 - 1] & 2147483648UL) != 0UL);
  cb1 = 1UL;

  /* Initialize output to zero */
  for (k = 0; k < n; k++) {
    y[k] = 0UL;
  }

  for (i = 0; i < n1; i++) {
    cb = 0UL;
    u1i = u1[i];
    if (isNegative1) {
      u1i = ~u1i + cb1;
      cb1 = u1i < cb1 ? 1UL : 0UL;
    }

    a1 = u1i >> 16U;
    a0 = u1i & 65535UL;
    cb2 = 1UL;
    k = n - i;
    nj = n2 <= k ? n2 : k;
    k = i;
    for (j = 0; j < nj; j++) {
      yk = y[k];
      u1i = u2[j];
      if (isNegative2) {
        u1i = ~u1i + cb2;
        cb2 = u1i < cb2 ? 1UL : 0UL;
      }

      b1 = u1i >> 16U;
      u1i &= 65535UL;
      w10 = a1 * u1i;
      w01 = a0 * b1;
      yk += cb;
      cb = yk < cb ? 1UL : 0UL;
      u1i *= a0;
      yk += u1i;
      cb += yk < u1i ? 1UL : 0UL;
      u1i = w10 << 16U;
      yk += u1i;
      cb += yk < u1i ? 1UL : 0UL;
      u1i = w01 << 16U;
      yk += u1i;
      cb += yk < u1i ? 1UL : 0UL;
      y[k] = yk;
      cb += w10 >> 16U;
      cb += w01 >> 16U;
      cb += a1 * b1;
      k++;
    }

    if (k < n) {
      y[k] = cb;
    }
  }

  /* Apply sign */
  if (isNegative1 != isNegative2) {
    cb = 1UL;
    for (k = 0; k < n; k++) {
      yk = ~y[k] + cb;
      y[k] = yk;
      cb = yk < cb ? 1UL : 0UL;
    }
  }
}

/* Function for Chart: '<Root>/Player Decisions' */
static real32_T greenOffence_norm(const real32_T x[2])
{
  real32_T y;
  real32_T scale;
  real32_T absxk;
  real32_T t;
  scale = 1.17549435E-38F;
  absxk = (real32_T)fabs(x[0]);
  if (absxk > 1.17549435E-38F) {
    y = 1.0F;
    scale = absxk;
  } else {
    t = absxk / 1.17549435E-38F;
    y = t * t;
  }

  absxk = (real32_T)fabs(x[1]);
  if (absxk > scale) {
    t = scale / absxk;
    y = y * t * t + 1.0F;
    scale = absxk;
  } else {
    t = absxk / scale;
    y += t * t;
  }

  return scale * (real32_T)sqrt(y);
}

/* Function for Chart: '<Root>/Player Decisions' */
static uint8_T greenOffence_checkReached_a(const int8_T pos[2], real_T tol)
{
  uint8_T posReached;
  real32_T pos_0[2];
  int16_T tmp;

  /* MATLAB Function 'checkReached': '<S3>:94' */
  /* '<S3>:94:2' */
  posReached = 0U;
  tmp = pos[0] - greenOffence_B.Selector.x;
  if (tmp > 127) {
    tmp = 127;
  } else {
    if (tmp < -128) {
      tmp = -128;
    }
  }

  pos_0[0] = tmp;
  tmp = pos[1] - greenOffence_B.Selector.y;
  if (tmp > 127) {
    tmp = 127;
  } else {
    if (tmp < -128) {
      tmp = -128;
    }
  }

  pos_0[1] = tmp;
  if (greenOffence_norm(pos_0) < tol) {
    /* '<S3>:94:3' */
    /* '<S3>:94:4' */
    posReached = 1U;
  }

  return posReached;
}

real32_T rt_roundf_snf(real32_T u)
{
  real32_T y;
  if ((real32_T)fabs(u) < 8.388608E+6F) {
    if (u >= 0.5F) {
      y = (real32_T)floor(u + 0.5F);
    } else if (u > -0.5F) {
      y = u * 0.0F;
    } else {
      y = (real32_T)ceil(u - 0.5F);
    }
  } else {
    y = u;
  }

  return y;
}

real32_T rt_atan2f_snf(real32_T u0, real32_T u1)
{
  real32_T y;
  int16_T u0_0;
  int16_T u1_0;
  if (rtIsNaNF(u0) || rtIsNaNF(u1)) {
    y = (rtNaNF);
  } else if (rtIsInfF(u0) && rtIsInfF(u1)) {
    if (u0 > 0.0F) {
      u0_0 = 1;
    } else {
      u0_0 = -1;
    }

    if (u1 > 0.0F) {
      u1_0 = 1;
    } else {
      u1_0 = -1;
    }

    y = (real32_T)atan2(u0_0, u1_0);
  } else if (u1 == 0.0F) {
    if (u0 > 0.0F) {
      y = RT_PIF / 2.0F;
    } else if (u0 < 0.0F) {
      y = -(RT_PIF / 2.0F);
    } else {
      y = 0.0F;
    }
  } else {
    y = (real32_T)atan2(u0, u1);
  }

  return y;
}

/* Function for Chart: '<Root>/Player Decisions' */
static void greenOffence_calcShootWay(void)
{
  real32_T y;
  real32_T scale;
  int16_T absxk;
  real32_T t;
  int8_T pos_idx_0;
  int8_T pos_idx_1;
  int16_T tmp;
  int16_T tmp_0;
  int16_T tmp_1;

  /* MATLAB Function 'calcShootWay': '<S3>:126' */
  if (greenOffence_DW.enemy[2].y > 0) {
    /* '<S3>:126:2' */
    /* '<S3>:126:3' */
    pos_idx_0 = 70;
    pos_idx_1 = -60;
  } else {
    /* '<S3>:126:5' */
    pos_idx_0 = 70;
    pos_idx_1 = 60;
  }

  if (greenOffence_B.Selector.color != 103) {
    /* '<S3>:126:7' */
    /* '<S3>:126:8' */
    pos_idx_0 = -70;
  }

  /* '<S3>:126:10' */
  tmp_1 = greenOffence_B.Ball_n.x - pos_idx_0;
  if (tmp_1 > 127) {
    tmp_1 = 127;
  } else {
    if (tmp_1 < -128) {
      tmp_1 = -128;
    }
  }

  tmp_0 = greenOffence_B.Ball_n.y - pos_idx_1;
  if (tmp_0 > 127) {
    tmp_0 = 127;
  } else {
    if (tmp_0 < -128) {
      tmp_0 = -128;
    }
  }

  /* '<S3>:126:11' */
  absxk = greenOffence_B.Ball_n.x - (int8_T)tmp_1;
  if (absxk > 127) {
    absxk = 127;
  } else {
    if (absxk < -128) {
      absxk = -128;
    }
  }

  tmp = greenOffence_B.Ball_n.y - (int8_T)tmp_0;
  if (tmp > 127) {
    tmp = 127;
  } else {
    if (tmp < -128) {
      tmp = -128;
    }
  }

  scale = 1.17549435E-38F;
  absxk = (int16_T)(real32_T)fabs((int8_T)absxk);
  if (absxk > 1.17549435E-38F) {
    t = 1.17549435E-38F / (real32_T)absxk;
    y = 0.0F * t * t + 1.0F;
    scale = absxk;
  } else {
    t = (real32_T)absxk / 1.17549435E-38F;
    y = t * t;
  }

  absxk = (int16_T)(real32_T)fabs((int8_T)tmp);
  if (absxk > scale) {
    t = scale / (real32_T)absxk;
    y = y * t * t + 1.0F;
    scale = absxk;
  } else {
    t = (real32_T)absxk / scale;
    y += t * t;
  }

  y = scale * (real32_T)sqrt(y);
  scale = rt_roundf_snf(20.0F * (real32_T)(int8_T)tmp_1 / y);
  if (scale < 128.0F) {
    if (scale >= -128.0F) {
      pos_idx_0 = (int8_T)scale;
    } else {
      pos_idx_0 = MIN_int8_T;
    }
  } else {
    pos_idx_0 = MAX_int8_T;
  }

  scale = rt_roundf_snf(20.0F * (real32_T)(int8_T)tmp_0 / y);
  if (scale < 128.0F) {
    if (scale >= -128.0F) {
      pos_idx_1 = (int8_T)scale;
    } else {
      pos_idx_1 = MIN_int8_T;
    }
  } else {
    pos_idx_1 = MAX_int8_T;
  }

  /* '<S3>:126:13' */
  tmp_1 = greenOffence_B.Ball_n.x + pos_idx_0;
  if (tmp_1 > 127) {
    tmp_1 = 127;
  } else {
    if (tmp_1 < -128) {
      tmp_1 = -128;
    }
  }

  greenOffence_DW.shootWay.x = (int8_T)tmp_1;

  /* '<S3>:126:14' */
  tmp_1 = greenOffence_B.Ball_n.y + pos_idx_1;
  if (tmp_1 > 127) {
    tmp_1 = 127;
  } else {
    if (tmp_1 < -128) {
      tmp_1 = -128;
    }
  }

  greenOffence_DW.shootWay.y = (int8_T)tmp_1;

  /* '<S3>:126:15' */
  tmp_1 = -pos_idx_1;
  if (tmp_1 > 127) {
    tmp_1 = 127;
  }

  tmp_0 = -pos_idx_0;
  if (tmp_0 > 127) {
    tmp_0 = 127;
  }

  scale = rt_roundf_snf(57.2957802F * rt_atan2f_snf((real32_T)tmp_1, (real32_T)
    tmp_0));
  if (scale < 32768.0F) {
    if (scale >= -32768.0F) {
      greenOffence_DW.shootWay.orientation = (int16_T)scale;
    } else {
      greenOffence_DW.shootWay.orientation = MIN_int16_T;
    }
  } else {
    greenOffence_DW.shootWay.orientation = MAX_int16_T;
  }
}

/* Function for Chart: '<Root>/Player Decisions' */
static uint8_T greenOffence_checkReached(const int8_T pos[2], real_T tol)
{
  uint8_T posReached;
  real32_T pos_0[2];
  int16_T tmp;

  /* MATLAB Function 'checkReached': '<S3>:94' */
  /* '<S3>:94:2' */
  posReached = 0U;
  tmp = pos[0] - greenOffence_B.Selector.x;
  if (tmp > 127) {
    tmp = 127;
  } else {
    if (tmp < -128) {
      tmp = -128;
    }
  }

  pos_0[0] = tmp;
  tmp = pos[1] - greenOffence_B.Selector.y;
  if (tmp > 127) {
    tmp = 127;
  } else {
    if (tmp < -128) {
      tmp = -128;
    }
  }

  pos_0[1] = tmp;
  if (greenOffence_norm(pos_0) < tol) {
    /* '<S3>:94:3' */
    /* '<S3>:94:4' */
    posReached = 1U;
  }

  return posReached;
}

/* Function for Chart: '<Root>/Player Decisions' */
static void greenOffence_calcStartPos(void)
{
  int16_T tmp;

  /* MATLAB Function 'calcStartPos': '<S3>:86' */
  /* '<S3>:86:2' */
  switch (greenOffence_B.Selector.position) {
   case 111U:
    /* '<S3>:86:4' */
    greenOffence_DW.startingPos[0] = 10;
    greenOffence_DW.startingPos[1] = 0;
    break;

   case 100U:
    /* '<S3>:86:6' */
    greenOffence_DW.startingPos[0] = 30;
    greenOffence_DW.startingPos[1] = 0;
    break;

   case 103U:
    /* '<S3>:86:8' */
    greenOffence_DW.startingPos[0] = 70;
    greenOffence_DW.startingPos[1] = 0;
    break;
  }

  if (greenOffence_B.Selector.color == 103) {
    /* '<S3>:86:10' */
    /* '<S3>:86:11' */
    tmp = -greenOffence_DW.startingPos[0];
    if (tmp > 127) {
      tmp = 127;
    }

    greenOffence_DW.startingPos[0] = (int8_T)tmp;
    tmp = -greenOffence_DW.startingPos[1];
    if (tmp > 127) {
      tmp = 127;
    }

    greenOffence_DW.startingPos[1] = (int8_T)tmp;
  }
}

/* Function for Chart: '<Root>/Player Decisions' */
static void greenOffence_calcStartTeams(void)
{
  /* MATLAB Function 'calcStartTeams': '<S3>:112' */
  /* '<S3>:112:2' */
  switch (greenOffence_B.Selector.color) {
   case 103U:
    /* '<S3>:112:4' */
    greenOffence_DW.enemy[0] = greenOffence_B.players[3];
    greenOffence_DW.enemy[1] = greenOffence_B.players[4];
    greenOffence_DW.enemy[2] = greenOffence_B.players[5];

    /* '<S3>:112:5' */
    break;

   case 98U:
    /* '<S3>:112:7' */
    greenOffence_DW.enemy[0] = greenOffence_B.players[0];
    greenOffence_DW.enemy[1] = greenOffence_B.players[1];
    greenOffence_DW.enemy[2] = greenOffence_B.players[2];

    /* '<S3>:112:8' */
    break;
  }
}

real_T rt_powd_snf(real_T u0, real_T u1)
{
  real_T y;
  real_T tmp;
  real_T tmp_0;
  if (rtIsNaN(u0) || rtIsNaN(u1)) {
    y = (rtNaN);
  } else {
    tmp = fabs(u0);
    tmp_0 = fabs(u1);
    if (rtIsInf(u1)) {
      if (tmp == 1.0) {
        y = (rtNaN);
      } else if (tmp > 1.0) {
        if (u1 > 0.0) {
          y = (rtInf);
        } else {
          y = 0.0;
        }
      } else if (u1 > 0.0) {
        y = 0.0;
      } else {
        y = (rtInf);
      }
    } else if (tmp_0 == 0.0) {
      y = 1.0;
    } else if (tmp_0 == 1.0) {
      if (u1 > 0.0) {
        y = u0;
      } else {
        y = 1.0 / u0;
      }
    } else if (u1 == 2.0) {
      y = u0 * u0;
    } else if ((u1 == 0.5) && (u0 >= 0.0)) {
      y = sqrt(u0);
    } else if ((u0 < 0.0) && (u1 > floor(u1))) {
      y = (rtNaN);
    } else {
      y = pow(u0, u1);
    }
  }

  return y;
}

real_T rt_atan2d_snf(real_T u0, real_T u1)
{
  real_T y;
  int16_T u0_0;
  int16_T u1_0;
  if (rtIsNaN(u0) || rtIsNaN(u1)) {
    y = (rtNaN);
  } else if (rtIsInf(u0) && rtIsInf(u1)) {
    if (u0 > 0.0) {
      u0_0 = 1;
    } else {
      u0_0 = -1;
    }

    if (u1 > 0.0) {
      u1_0 = 1;
    } else {
      u1_0 = -1;
    }

    y = atan2(u0_0, u1_0);
  } else if (u1 == 0.0) {
    if (u0 > 0.0) {
      y = RT_PI / 2.0;
    } else if (u0 < 0.0) {
      y = -(RT_PI / 2.0);
    } else {
      y = 0.0;
    }
  } else {
    y = atan2(u0, u1);
  }

  return y;
}

real_T rt_roundd_snf(real_T u)
{
  real_T y;
  if (fabs(u) < 4.503599627370496E+15) {
    if (u >= 0.5) {
      y = floor(u + 0.5);
    } else if (u > -0.5) {
      y = u * 0.0;
    } else {
      y = ceil(u - 0.5);
    }
  } else {
    y = u;
  }

  return y;
}

real_T rt_remd_snf(real_T u0, real_T u1)
{
  real_T y;
  real_T u1_0;
  if (!((!rtIsNaN(u0)) && (!rtIsInf(u0)) && ((!rtIsNaN(u1)) && (!rtIsInf(u1)))))
  {
    y = (rtNaN);
  } else {
    if (u1 < 0.0) {
      u1_0 = ceil(u1);
    } else {
      u1_0 = floor(u1);
    }

    if ((u1 != 0.0) && (u1 != u1_0)) {
      u1_0 = u0 / u1;
      if (fabs(u1_0 - rt_roundd_snf(u1_0)) <= DBL_EPSILON * fabs(u1_0)) {
        y = 0.0;
      } else {
        y = fmod(u0, u1);
      }
    } else {
      y = fmod(u0, u1);
    }
  }

  return y;
}

/* Model output function */
void greenOffence_output(void)
{
  uint8_T newMessage[27];
  Player tempEmpty;
  uint8_T x;
  uint8_T e_x[2];
  static const uint8_T c[6] = { 103U, 103U, 103U, 98U, 98U, 98U };

  static const uint8_T d[6] = { 111U, 100U, 103U, 111U, 100U, 103U };

  boolean_T exitg1;
  real_T sigma;
  int16_T absx;
  real_T t;
  int16_T T;
  int16_T M;
  real_T rtb_Product3;
  boolean_T rtb_Compare;
  real_T rtb_SaturationOrien1;
  int16_T rtb_targetOrientation;
  int16_T rtb_Switch1;
  int32_T rtb_PGainOrien;
  int8_T tmp[2];
  int16_T absx_0;
  int16_T f_x;
  int8_T rtb_Add_idx_0;
  int8_T rtb_Add_idx_1;
  int32_T tmp_0;
  int64m_T tmp_1;
  uint32_T tmp_2;
  uint32_T tmp_3;
  int64m_T tmp_4;
  real_T b_y;

  /* MATLAB Function: '<S4>/Parser' */
  /* MATLAB Function 'SerialCom/Parser': '<S6>:1' */
  /* '<S6>:1:33' */
  /* '<S6>:1:32' */
  /*  expects a standard 30 byte long message */
  if (!greenOffence_DW.lastMessage_not_empty) {
    /* '<S6>:1:4' */
    myInit();
    greenOffence_DW.lastMessage_not_empty = true;
  }

  /* '<S6>:1:10' */
  for (absx = 0; absx < 27; absx++) {
    newMessage[absx] = 0U;
  }

  checkDATA(newMessage);
  rtb_Compare = false;
  rtb_targetOrientation = 0;
  exitg1 = false;
  while ((!exitg1) && (rtb_targetOrientation < 27)) {
    if (!(newMessage[rtb_targetOrientation] == 0)) {
      rtb_Compare = true;
      exitg1 = true;
    } else {
      rtb_targetOrientation++;
    }
  }

  if (!rtb_Compare) {
    /* '<S6>:1:15' */
    /* '<S6>:1:16' */
    for (absx = 0; absx < 27; absx++) {
      newMessage[absx] = greenOffence_DW.lastMessage[absx];
    }
  }

  /* '<S6>:1:19' */
  tempEmpty.x = 0;

  /* '<S6>:1:20' */
  tempEmpty.y = 0;

  /* '<S6>:1:21' */
  tempEmpty.orientation = 0;

  /* '<S6>:1:22' */
  tempEmpty.color = 0U;

  /* '<S6>:1:23' */
  tempEmpty.position = 0U;

  /* '<S6>:1:24' */
  tempEmpty.valid = 0U;

  /* '<S6>:1:25' */
  greenOffence_B.players[0] = tempEmpty;
  greenOffence_B.players[1] = tempEmpty;
  greenOffence_B.players[2] = tempEmpty;
  greenOffence_B.players[3] = tempEmpty;
  greenOffence_B.players[4] = tempEmpty;
  greenOffence_B.players[5] = tempEmpty;

  /* '<S6>:1:27' */
  /* '<S6>:1:28' */
  x = newMessage[1];
  memcpy(&rtb_Add_idx_0, &x, (size_t)1 * sizeof(int8_T));
  greenOffence_B.Ball_n.x = rtb_Add_idx_0;

  /* '<S6>:1:29' */
  x = newMessage[2];
  memcpy(&rtb_Add_idx_0, &x, (size_t)1 * sizeof(int8_T));
  greenOffence_B.Ball_n.y = rtb_Add_idx_0;

  /* '<S6>:1:30' */
  greenOffence_B.Ball_n.valid = 1U;

  /* '<S6>:1:31' */
  /* '<S6>:1:32' */
  /* '<S6>:1:33' */
  /* '<S6>:1:34' */
  for (rtb_targetOrientation = 0; rtb_targetOrientation < 6;
       rtb_targetOrientation++) {
    /* '<S6>:1:34' */
    /* '<S6>:1:35' */
    x = newMessage[(rtb_targetOrientation << 2) + 3];
    memcpy(&rtb_Add_idx_0, &x, (size_t)1 * sizeof(int8_T));
    greenOffence_B.players[rtb_targetOrientation].x = rtb_Add_idx_0;

    /* '<S6>:1:36' */
    x = newMessage[(rtb_targetOrientation << 2) + 4];
    memcpy(&rtb_Add_idx_0, &x, (size_t)1 * sizeof(int8_T));
    greenOffence_B.players[rtb_targetOrientation].y = rtb_Add_idx_0;

    /* '<S6>:1:37' */
    absx_0 = rtb_targetOrientation << 2;
    e_x[0] = newMessage[absx_0 + 5];
    e_x[1] = newMessage[absx_0 + 6];
    memcpy(&absx, &e_x[0], (size_t)1 * sizeof(int16_T));
    greenOffence_B.players[rtb_targetOrientation].orientation = absx;

    /* '<S6>:1:38' */
    greenOffence_B.players[rtb_targetOrientation].color =
      c[rtb_targetOrientation];

    /* '<S6>:1:39' */
    greenOffence_B.players[rtb_targetOrientation].position =
      d[rtb_targetOrientation];

    /* '<S6>:1:40' */
    greenOffence_B.players[rtb_targetOrientation].valid = 1U;

    /* '<S6>:1:34' */
  }

  /* Selector: '<Root>/Selector' incorporates:
   *  Constant: '<Root>/Constant3'
   */
  greenOffence_B.Selector =
    greenOffence_B.players[greenOffence_P.Constant3_Value - 1];

  /* Chart: '<Root>/Player Decisions' incorporates:
   *  MATLAB Function: '<S4>/Parser'
   */
  /* MATLAB Function 'MATLAB Function': '<S1>:1' */
  /* '<S1>:1:2' */
  /* '<S1>:1:3' */
  /* '<S1>:1:4' */
  /* Gateway: Player Decisions */
  if (greenOffence_DW.temporalCounter_i1 < 15U) {
    greenOffence_DW.temporalCounter_i1++;
  }

  /* During: Player Decisions */
  if (greenOffence_DW.is_active_c11_greenOffence == 0U) {
    /* Entry: Player Decisions */
    greenOffence_DW.is_active_c11_greenOffence = 1U;

    /* Entry Internal: Player Decisions */
    /* Transition: '<S3>:97' */
    greenOffence_calcStartPos();
    greenOffence_calcStartTeams();
    greenOffence_DW.is_c11_greenOffence = greenOffence_IN_waiting;

    /* Entry Internal 'waiting': '<S3>:88' */
    /* Transition: '<S3>:93' */
    greenOffence_DW.is_waiting = greenOffence_IN_goToPosition;
  } else if (greenOffence_DW.is_c11_greenOffence ==
             greenOffenc_IN_GameIsOn_Offence) {
    /* During 'GameIsOn_Offence': '<S3>:17' */
    if (newMessage[0] == 0) {
      /* Transition: '<S3>:91' */
      /* Exit Internal 'GameIsOn_Offence': '<S3>:17' */
      greenOffence_DW.is_GameIsOn_Offence = greenOffence_IN_NO_ACTIVE_CHILD;
      greenOffence_DW.is_c11_greenOffence = greenOffence_IN_waiting;

      /* Entry Internal 'waiting': '<S3>:88' */
      /* Transition: '<S3>:93' */
      greenOffence_DW.is_waiting = greenOffence_IN_goToPosition;
    } else {
      switch (greenOffence_DW.is_GameIsOn_Offence) {
       case greenOffence_IN_Aim:
        /* During 'Aim': '<S3>:115' */
        rtb_PGainOrien = (int32_T)greenOffence_B.Selector.orientation -
          greenOffence_B.finalWay.orientation;
        if (rtb_PGainOrien > 32767L) {
          rtb_PGainOrien = 32767L;
        } else {
          if (rtb_PGainOrien < -32768L) {
            rtb_PGainOrien = -32768L;
          }
        }

        if ((int16_T)rtb_PGainOrien < 0) {
          if ((int16_T)rtb_PGainOrien <= MIN_int16_T) {
            rtb_targetOrientation = MAX_int16_T;
          } else {
            rtb_targetOrientation = -(int16_T)rtb_PGainOrien;
          }
        } else {
          rtb_targetOrientation = (int16_T)rtb_PGainOrien;
        }

        if (rtb_targetOrientation < 5) {
          /* Transition: '<S3>:121' */
          greenOffence_DW.is_GameIsOn_Offence = greenOffence_IN_Kick;
          greenOffence_DW.temporalCounter_i1 = 0U;

          /* Entry 'Kick': '<S3>:118' */
          greenOffence_B.finalWay.x = greenOffence_B.Ball_n.x;
          greenOffence_B.finalWay.y = greenOffence_B.Ball_n.y;
          greenOffence_B.finalWay.orientation = MAX_int16_T;
        } else {
          greenOffence_calcShootWay();
          greenOffence_B.finalWay.x = greenOffence_B.Selector.x;
          greenOffence_B.finalWay.y = greenOffence_B.Selector.y;
          greenOffence_B.finalWay.orientation =
            greenOffence_DW.shootWay.orientation;
        }
        break;

       case greenOffence_IN_GetToTheBall:
        /* During 'GetToTheBall': '<S3>:60' */
        tmp[0] = greenOffence_DW.shootWay.x;
        tmp[1] = greenOffence_DW.shootWay.y;
        if (greenOffence_checkReached(tmp, 3.0) == 1) {
          /* Transition: '<S3>:116' */
          greenOffence_DW.is_GameIsOn_Offence = greenOffence_IN_Aim;
        } else {
          greenOffence_calcShootWay();
          greenOffence_B.finalWay.x = greenOffence_DW.shootWay.x;
          greenOffence_B.finalWay.y = greenOffence_DW.shootWay.y;
          greenOffence_B.finalWay.orientation = MAX_int16_T;
        }
        break;

       case greenOffence_IN_Idle:
        /* During 'Idle': '<S3>:105' */
        if (greenOffence_DW.temporalCounter_i1 >= 10) {
          /* Transition: '<S3>:120' */
          greenOffence_DW.is_GameIsOn_Offence = greenOffence_IN_GetToTheBall;

          /* Entry 'GetToTheBall': '<S3>:60' */
          greenOffence_calcShootWay();
        } else {
          greenOffence_B.finalWay.x = greenOffence_B.Selector.x;
          greenOffence_B.finalWay.y = greenOffence_B.Selector.y;
          greenOffence_B.finalWay.orientation =
            greenOffence_B.Selector.orientation;
        }
        break;

       default:
        /* During 'Kick': '<S3>:118' */
        tmp[0] = greenOffence_B.Ball_n.x;
        tmp[1] = greenOffence_B.Ball_n.y;
        rtb_Compare = ((greenOffence_DW.temporalCounter_i1 >= 10) &&
                       (greenOffence_checkReached(tmp, 15.0) == 0));
        if (rtb_Compare) {
          /* Transition: '<S3>:131' */
          greenOffence_DW.is_GameIsOn_Offence = greenOffence_IN_Idle;
          greenOffence_DW.temporalCounter_i1 = 0U;
        }
        break;
      }
    }
  } else {
    /* During 'waiting': '<S3>:88' */
    if (newMessage[0] == 1) {
      /* Transition: '<S3>:90' */
      /* Exit Internal 'waiting': '<S3>:88' */
      greenOffence_DW.is_waiting = greenOffence_IN_NO_ACTIVE_CHILD;
      greenOffence_DW.is_c11_greenOffence = greenOffenc_IN_GameIsOn_Offence;

      /* Entry Internal 'GameIsOn_Offence': '<S3>:17' */
      /* Transition: '<S3>:103' */
      greenOffence_DW.is_GameIsOn_Offence = greenOffence_IN_GetToTheBall;

      /* Entry 'GetToTheBall': '<S3>:60' */
      greenOffence_calcShootWay();
    } else if (greenOffence_DW.is_waiting == greenOffence_IN_Idle1) {
      /* During 'Idle1': '<S3>:100' */
      greenOffence_B.finalWay.x = greenOffence_B.Selector.x;
      greenOffence_B.finalWay.y = greenOffence_B.Selector.y;
      greenOffence_B.finalWay.orientation = greenOffence_B.Selector.orientation;
    } else {
      /* During 'goToPosition': '<S3>:92' */
      if (greenOffence_checkReached_a(greenOffence_DW.startingPos, 3.0) == 1) {
        /* Transition: '<S3>:101' */
        greenOffence_DW.is_waiting = greenOffence_IN_Idle1;
      } else {
        greenOffence_B.finalWay.x = greenOffence_DW.startingPos[0];
        greenOffence_B.finalWay.y = greenOffence_DW.startingPos[1];
        greenOffence_B.finalWay.orientation = MAX_int16_T;
      }
    }
  }

  /* End of Chart: '<Root>/Player Decisions' */

  /* Sum: '<S5>/Add' incorporates:
   *  SignalConversion: '<S5>/ConcatBufferAtVector Concatenate1In1'
   *  SignalConversion: '<S5>/ConcatBufferAtVector Concatenate1In2'
   *  SignalConversion: '<S5>/ConcatBufferAtVector ConcatenateIn1'
   *  SignalConversion: '<S5>/ConcatBufferAtVector ConcatenateIn2'
   */
  rtb_Add_idx_0 = (int8_T)(greenOffence_B.finalWay.x - greenOffence_B.Selector.x);
  rtb_Add_idx_1 = (int8_T)(greenOffence_B.finalWay.y - greenOffence_B.Selector.y);

  /* DotProduct: '<S7>/Dot Product' */
  rtb_PGainOrien = (int32_T)(rtb_Add_idx_0 * rtb_Add_idx_0) + rtb_Add_idx_1 *
    rtb_Add_idx_1;

  /* Math: '<S7>/Math Function' incorporates:
   *  Constant: '<S7>/Constant'
   *  DotProduct: '<S7>/Dot Product'
   */
  if ((rtb_PGainOrien < 0.0) && (greenOffence_P.Constant_Value_n > floor
       (greenOffence_P.Constant_Value_n))) {
    b_y = -rt_powd_snf(-(real_T)rtb_PGainOrien, greenOffence_P.Constant_Value_n);
  } else {
    b_y = rt_powd_snf((real_T)rtb_PGainOrien, greenOffence_P.Constant_Value_n);
  }

  /* End of Math: '<S7>/Math Function' */

  /* Gain: '<S5>/PGain' */
  rtb_Product3 = greenOffence_P.PGain_Gain * b_y;

  /* RelationalOperator: '<S10>/Compare' incorporates:
   *  Constant: '<S10>/Constant'
   */
  rtb_Compare = (greenOffence_B.finalWay.orientation !=
                 greenOffence_P.CompareToConstant_const);

  /* MATLAB Function: '<S9>/calcTargetOrien' incorporates:
   *  SignalConversion: '<S5>/ConcatBufferAtVector Concatenate1In1'
   *  SignalConversion: '<S5>/ConcatBufferAtVector Concatenate1In2'
   *  SignalConversion: '<S5>/ConcatBufferAtVector ConcatenateIn1'
   *  SignalConversion: '<S5>/ConcatBufferAtVector ConcatenateIn2'
   */
  /* MATLAB Function 'control/decisionParser/calcTargetOrien': '<S11>:1' */
  /* '<S11>:1:2' */
  absx = greenOffence_B.finalWay.y - greenOffence_B.Selector.y;
  if (absx > 127) {
    absx = 127;
  } else {
    if (absx < -128) {
      absx = -128;
    }
  }

  absx_0 = greenOffence_B.finalWay.x - greenOffence_B.Selector.x;
  if (absx_0 > 127) {
    absx_0 = 127;
  } else {
    if (absx_0 < -128) {
      absx_0 = -128;
    }
  }

  b_y = rt_roundd_snf(57.295779513082323 * rt_atan2d_snf((real_T)absx, (real_T)
    absx_0));
  if (b_y < 32768.0) {
    if (b_y >= -32768.0) {
      rtb_Switch1 = (int16_T)b_y;
    } else {
      rtb_Switch1 = MIN_int16_T;
    }
  } else {
    rtb_Switch1 = MAX_int16_T;
  }

  /* MATLAB Function: '<S9>/fwdLimit' incorporates:
   *  Constant: '<S9>/certainty'
   *  MATLAB Function: '<S9>/calcTargetOrien'
   *  Sum: '<S9>/Add1'
   */
  /* MATLAB Function 'control/decisionParser/fwdLimit': '<S12>:1' */
  /* '<S12>:1:2' */
  sigma = (real_T)greenOffence_P.certainty_Value / 255.0;

  /* '<S12>:1:3' */
  t = rt_remd_snf((real_T)(rtb_Switch1 - greenOffence_B.Selector.orientation),
                  360.0);
  absx = (int16_T)fabs(t);
  if (absx > 180) {
    if (t > 0.0) {
      t -= 360.0;
    } else {
      t += 360.0;
    }

    absx = (int16_T)fabs(t);
  }

  if (absx <= 45) {
    t *= 0.017453292519943295;
    rtb_Add_idx_0 = 0;
  } else if (absx <= 135) {
    if (t > 0.0) {
      t = (t - 90.0) * 0.017453292519943295;
      rtb_Add_idx_0 = 1;
    } else {
      t = (t + 90.0) * 0.017453292519943295;
      rtb_Add_idx_0 = -1;
    }
  } else if (t > 0.0) {
    t = (t - 180.0) * 0.017453292519943295;
    rtb_Add_idx_0 = 2;
  } else {
    t = (t + 180.0) * 0.017453292519943295;
    rtb_Add_idx_0 = -2;
  }

  /* '<S12>:1:4' */
  if (sigma > 0.0) {
    if (rtb_Add_idx_0 == 0) {
      b_y = cos(t);
    } else if (rtb_Add_idx_0 == 1) {
      b_y = -sin(t);
    } else if (rtb_Add_idx_0 == -1) {
      b_y = sin(t);
    } else {
      b_y = -cos(t);
    }

    t = (1.0 - fabs(b_y)) / sigma;
    rtb_SaturationOrien1 = exp(-0.5 * t * t) / (2.5066282746310002 * sigma);
    t = 0.0 / sigma;
    t = exp(-0.5 * t * t) / (2.5066282746310002 * sigma);
  } else {
    rtb_SaturationOrien1 = (rtNaN);
    t = (rtNaN);
  }

  /* Switch: '<S9>/Switch' incorporates:
   *  Constant: '<S9>/Constant'
   *  MATLAB Function: '<S9>/fwdLimit'
   */
  if (rtb_Compare) {
    b_y = greenOffence_P.Constant_Value;
  } else {
    b_y = rtb_SaturationOrien1 / t;
  }

  /* End of Switch: '<S9>/Switch' */

  /* Saturate: '<S5>/Saturation' */
  if (rtb_Product3 > greenOffence_P.Saturation_UpperSat) {
    rtb_Product3 = greenOffence_P.Saturation_UpperSat;
  } else {
    if (rtb_Product3 < greenOffence_P.Saturation_LowerSat) {
      rtb_Product3 = greenOffence_P.Saturation_LowerSat;
    }
  }

  /* Product: '<S5>/Product3' incorporates:
   *  Saturate: '<S5>/Saturation'
   */
  rtb_Product3 *= b_y;

  /* Switch: '<S9>/Switch1' */
  if (rtb_Compare) {
    rtb_Switch1 = greenOffence_B.finalWay.orientation;
  }

  /* End of Switch: '<S9>/Switch1' */

  /* MATLAB Function: '<S5>/calc_dO' */
  /* MATLAB Function 'control/calc_dO': '<S8>:1' */
  /* '<S8>:1:2' */
  rtb_PGainOrien = rtb_Switch1 + 360L;
  if (rtb_PGainOrien > 32767L) {
    rtb_PGainOrien = 32767L;
  }

  T = (int16_T)rtb_PGainOrien - div_s16_floor((int16_T)rtb_PGainOrien, 360) *
    360;

  /* '<S8>:1:3' */
  rtb_PGainOrien = greenOffence_B.Selector.orientation + 360L;
  if (rtb_PGainOrien > 32767L) {
    rtb_PGainOrien = 32767L;
  }

  M = (int16_T)rtb_PGainOrien - div_s16_floor((int16_T)rtb_PGainOrien, 360) *
    360;

  /* '<S8>:1:4' */
  rtb_PGainOrien = (int32_T)T - M;
  if (rtb_PGainOrien > 32767L) {
    rtb_PGainOrien = 32767L;
  } else {
    if (rtb_PGainOrien < -32768L) {
      rtb_PGainOrien = -32768L;
    }
  }

  tmp_0 = (int32_T)rtb_Switch1 - greenOffence_B.Selector.orientation;
  if (tmp_0 > 32767L) {
    tmp_0 = 32767L;
  } else {
    if (tmp_0 < -32768L) {
      tmp_0 = -32768L;
    }
  }

  if ((int16_T)rtb_PGainOrien < 0) {
    if ((int16_T)rtb_PGainOrien <= MIN_int16_T) {
      absx_0 = MAX_int16_T;
    } else {
      absx_0 = -(int16_T)rtb_PGainOrien;
    }
  } else {
    absx_0 = (int16_T)rtb_PGainOrien;
  }

  if ((int16_T)tmp_0 < 0) {
    if ((int16_T)tmp_0 <= MIN_int16_T) {
      absx = MAX_int16_T;
    } else {
      absx = -(int16_T)tmp_0;
    }
  } else {
    absx = (int16_T)tmp_0;
  }

  if (absx_0 <= absx) {
    absx = absx_0;
  }

  rtb_PGainOrien = (int32_T)T - M;
  if (rtb_PGainOrien > 32767L) {
    rtb_PGainOrien = 32767L;
  } else {
    if (rtb_PGainOrien < -32768L) {
      rtb_PGainOrien = -32768L;
    }
  }

  tmp_0 = (int32_T)rtb_Switch1 - greenOffence_B.Selector.orientation;
  if (tmp_0 > 32767L) {
    tmp_0 = 32767L;
  } else {
    if (tmp_0 < -32768L) {
      tmp_0 = -32768L;
    }
  }

  if ((int16_T)rtb_PGainOrien < 0) {
    if ((int16_T)rtb_PGainOrien <= MIN_int16_T) {
      rtb_targetOrientation = MAX_int16_T;
    } else {
      rtb_targetOrientation = -(int16_T)rtb_PGainOrien;
    }
  } else {
    rtb_targetOrientation = (int16_T)rtb_PGainOrien;
  }

  if ((int16_T)tmp_0 < 0) {
    if ((int16_T)tmp_0 <= MIN_int16_T) {
      f_x = MAX_int16_T;
    } else {
      f_x = -(int16_T)tmp_0;
    }
  } else {
    f_x = (int16_T)tmp_0;
  }

  if (rtb_targetOrientation < f_x) {
    /* '<S8>:1:5' */
    /* '<S8>:1:6' */
    rtb_PGainOrien = (int32_T)T - M;
    if (rtb_PGainOrien > 32767L) {
      rtb_PGainOrien = 32767L;
    } else {
      if (rtb_PGainOrien < -32768L) {
        rtb_PGainOrien = -32768L;
      }
    }

    rtb_targetOrientation = (int16_T)rtb_PGainOrien;
    if ((int16_T)rtb_PGainOrien > 0) {
      rtb_targetOrientation = 1;
    } else {
      if ((int16_T)rtb_PGainOrien < 0) {
        rtb_targetOrientation = -1;
      }
    }
  } else {
    /* '<S8>:1:8' */
    rtb_PGainOrien = (int32_T)rtb_Switch1 - greenOffence_B.Selector.orientation;
    if (rtb_PGainOrien > 32767L) {
      rtb_PGainOrien = 32767L;
    } else {
      if (rtb_PGainOrien < -32768L) {
        rtb_PGainOrien = -32768L;
      }
    }

    rtb_targetOrientation = (int16_T)rtb_PGainOrien;
    if ((int16_T)rtb_PGainOrien > 0) {
      rtb_targetOrientation = 1;
    } else {
      if ((int16_T)rtb_PGainOrien < 0) {
        rtb_targetOrientation = -1;
      }
    }
  }

  if (absx < 3) {
    /* '<S8>:1:10' */
    /* '<S8>:1:11' */
    absx = 0;
  }

  /* Gain: '<S5>/PGainOrien' incorporates:
   *  MATLAB Function: '<S5>/calc_dO'
   */
  rtb_PGainOrien = (int32_T)greenOffence_P.PGainOrien_Gain * absx;

  /* Product: '<S5>/Product1' incorporates:
   *  Gain: '<S5>/Gain1'
   *  MATLAB Function: '<S5>/calc_dO'
   */
  tmp_2 = (uint32_T)rtb_PGainOrien;
  tmp_3 = (uint32_T)((int32_T)greenOffence_P.Gain1_Gain * rtb_targetOrientation);
  sMultiWordMul(&tmp_2, 1, &tmp_3, 1, &tmp_1.chunks[0U], 2);

  /* Sum: '<S5>/Add2' */
  rtb_SaturationOrien1 = sMultiWord2Double(&tmp_1.chunks[0U], 2, 0) *
    1.4551915228366852E-11 + rtb_Product3;

  /* Saturate: '<S5>/SaturationOrien1' */
  if (rtb_SaturationOrien1 > greenOffence_P.SaturationOrien1_UpperSat) {
    rtb_SaturationOrien1 = greenOffence_P.SaturationOrien1_UpperSat;
  } else {
    if (rtb_SaturationOrien1 < greenOffence_P.SaturationOrien1_LowerSat) {
      rtb_SaturationOrien1 = greenOffence_P.SaturationOrien1_LowerSat;
    }
  }

  /* End of Saturate: '<S5>/SaturationOrien1' */

  /* Product: '<S5>/Product2' incorporates:
   *  MATLAB Function: '<S5>/calc_dO'
   */
  tmp_2 = (uint32_T)rtb_PGainOrien;
  tmp_3 = (uint32_T)rtb_targetOrientation;
  sMultiWordMul(&tmp_2, 1, &tmp_3, 1, &tmp_4.chunks[0U], 2);

  /* Sum: '<S5>/Add3' */
  t = sMultiWord2Double(&tmp_4.chunks[0U], 2, 0) * 4.76837158203125E-7 +
    rtb_Product3;

  /* Saturate: '<S5>/SaturationOrien2' */
  if (t > greenOffence_P.SaturationOrien2_UpperSat) {
    t = greenOffence_P.SaturationOrien2_UpperSat;
  } else {
    if (t < greenOffence_P.SaturationOrien2_LowerSat) {
      t = greenOffence_P.SaturationOrien2_LowerSat;
    }
  }

  /* End of Saturate: '<S5>/SaturationOrien2' */

  /* MATLAB Function: '<Root>/MATLAB Function1' */
  /* MATLAB Function 'MATLAB Function1': '<S2>:1' */
  b_y = rt_roundd_snf(rtb_SaturationOrien1);
  if (b_y < 32768.0) {
    if (b_y >= -32768.0) {
      rtb_targetOrientation = (int16_T)b_y;
    } else {
      rtb_targetOrientation = MIN_int16_T;
    }
  } else {
    rtb_targetOrientation = MAX_int16_T;
  }

  b_y = rt_roundd_snf(rtb_SaturationOrien1);
  if (b_y < 32768.0) {
    if (b_y >= -32768.0) {
      absx = (int16_T)b_y;
    } else {
      absx = MIN_int16_T;
    }
  } else {
    absx = MAX_int16_T;
  }

  b_y = rt_roundd_snf(t);
  if (b_y < 32768.0) {
    if (b_y >= -32768.0) {
      absx_0 = (int16_T)b_y;
    } else {
      absx_0 = MIN_int16_T;
    }
  } else {
    absx_0 = MAX_int16_T;
  }

  b_y = rt_roundd_snf(t);
  if (b_y < 32768.0) {
    if (b_y >= -32768.0) {
      f_x = (int16_T)b_y;
    } else {
      f_x = MIN_int16_T;
    }
  } else {
    f_x = MAX_int16_T;
  }

  if (rtb_targetOrientation < 0) {
    if (rtb_targetOrientation <= MIN_int16_T) {
      rtb_targetOrientation = MAX_int16_T;
    } else {
      rtb_targetOrientation = -rtb_targetOrientation;
    }
  }

  if (absx_0 < 0) {
    if (absx_0 <= MIN_int16_T) {
      absx_0 = MAX_int16_T;
    } else {
      absx_0 = -absx_0;
    }
  }

  if (absx > 0) {
    absx = 1;
  } else if (absx < 0) {
    absx = -1;
  } else {
    absx = 0;
  }

  if (f_x > 0) {
    f_x = 1;
  } else if (f_x < 0) {
    f_x = -1;
  } else {
    f_x = 0;
  }

  drive(rtb_targetOrientation, absx, absx_0, f_x);

  /* End of MATLAB Function: '<Root>/MATLAB Function1' */
}

/* Model update function */
void greenOffence_update(void)
{
  /* (no update code required) */
}

/* Model initialize function */
void greenOffence_initialize(void)
{
  /* Registration code */

  /* initialize non-finites */
  rt_InitInfAndNaN(sizeof(real_T));

  /* initialize error status */
  rtmSetErrorStatus(greenOffence_M, (NULL));

  /* block I/O */
  (void) memset(((void *) &greenOffence_B), 0,
                sizeof(B_greenOffence_T));

  /* states (dwork) */
  (void) memset((void *)&greenOffence_DW, 0,
                sizeof(DW_greenOffence_T));

  {
    int16_T i;

    /* InitializeConditions for MATLAB Function: '<S4>/Parser' */
    greenOffence_DW.lastMessage_not_empty = false;
    for (i = 0; i < 27; i++) {
      greenOffence_DW.lastMessage[i] = 0U;
    }

    /* End of InitializeConditions for MATLAB Function: '<S4>/Parser' */

    /* InitializeConditions for Chart: '<Root>/Player Decisions' */
    greenOffence_DW.is_GameIsOn_Offence = greenOffence_IN_NO_ACTIVE_CHILD;
    greenOffence_DW.temporalCounter_i1 = 0U;
    greenOffence_DW.is_waiting = greenOffence_IN_NO_ACTIVE_CHILD;
    greenOffence_DW.is_active_c11_greenOffence = 0U;
    greenOffence_DW.is_c11_greenOffence = greenOffence_IN_NO_ACTIVE_CHILD;
  }
}

/* Model terminate function */
void greenOffence_terminate(void)
{
  /* (no terminate code required) */
}

/*
 * File trailer for generated code.
 *
 * [EOF]
 */

/*
 * File: manualController.h
 *
 * Code generated for Simulink model 'manualController'.
 *
 * Model version                  : 1.75
 * Simulink Coder version         : 8.7 (R2014b) 08-Sep-2014
 * TLC version                    : 8.7 (Aug  5 2014)
 * C/C++ source code generated on : Thu Mar 05 19:00:44 2015
 *
 * Target selection: realtime.tlc
 * Embedded hardware selection: Atmel->AVR
 * Code generation objectives: Unspecified
 * Validation result: Not run
 */

#ifndef RTW_HEADER_manualController_h_
#define RTW_HEADER_manualController_h_
#include <string.h>
#include <stddef.h>
#ifndef manualController_COMMON_INCLUDES_
# define manualController_COMMON_INCLUDES_
#include "rtwtypes.h"
#include "rtw_continuous.h"
#include "rtw_solver.h"
#include "arduino_analogoutput_lct.h"
#endif                                 /* manualController_COMMON_INCLUDES_ */

#include "manualController_types.h"

/* Macros for accessing real-time model data structure */
#ifndef rtmGetErrorStatus
# define rtmGetErrorStatus(rtm)        ((rtm)->errorStatus)
#endif

#ifndef rtmSetErrorStatus
# define rtmSetErrorStatus(rtm, val)   ((rtm)->errorStatus = (val))
#endif

/* Block signals (auto storage) */
typedef struct {
  uint8_T SFunctionBuilder1[27];       /* '<S8>/S-Function Builder1' */
} B_manualController_T;

/* Block states (auto storage) for system '<Root>' */
typedef struct {
  uint8_T oldMes[27];                  /* '<S1>/Parser' */
  uint8_T once;                        /* '<S1>/MATLAB Function1' */
} DW_manualController_T;

/* Parameters (auto storage) */
struct P_manualController_T_ {
  uint32_T PWM_pinNumber;              /* Mask Parameter: PWM_pinNumber
                                        * Referenced by: '<S10>/PWM'
                                        */
  uint32_T PWM_pinNumber_k;            /* Mask Parameter: PWM_pinNumber_k
                                        * Referenced by: '<S11>/PWM'
                                        */
  uint32_T PWM_pinNumber_k5;           /* Mask Parameter: PWM_pinNumber_k5
                                        * Referenced by: '<S12>/PWM'
                                        */
  uint32_T PWM_pinNumber_i;            /* Mask Parameter: PWM_pinNumber_i
                                        * Referenced by: '<S13>/PWM'
                                        */
  uint8_T Constant_Value;              /* Computed Parameter: Constant_Value
                                        * Referenced by: '<S3>/Constant'
                                        */
  uint8_T Constant3_Value;             /* Computed Parameter: Constant3_Value
                                        * Referenced by: '<Root>/Constant3'
                                        */
};

/* Real-time Model Data Structure */
struct tag_RTM_manualController_T {
  const char_T * volatile errorStatus;
};

/* Block parameters (auto storage) */
extern P_manualController_T manualController_P;

/* Block signals (auto storage) */
extern B_manualController_T manualController_B;

/* Block states (auto storage) */
extern DW_manualController_T manualController_DW;

/* Model entry point functions */
extern void manualController_initialize(void);
extern void manualController_output(void);
extern void manualController_update(void);
extern void manualController_terminate(void);

/* Real-time Model object */
extern RT_MODEL_manualController_T *const manualController_M;

/*-
 * The generated code includes comments that allow you to trace directly
 * back to the appropriate location in the model.  The basic format
 * is <system>/block_name, where system is the system number (uniquely
 * assigned by Simulink) and block_name is the name of the block.
 *
 * Use the MATLAB hilite_system command to trace the generated code back
 * to the model.  For example,
 *
 * hilite_system('<S3>')    - opens system 3
 * hilite_system('<S3>/Kp') - opens and selects block Kp which resides in S3
 *
 * Here is the system hierarchy for this model
 *
 * '<Root>' : 'manualController'
 * '<S1>'   : 'manualController/Serial'
 * '<S2>'   : 'manualController/Subsystem'
 * '<S3>'   : 'manualController/Serial/Compare To Zero'
 * '<S4>'   : 'manualController/Serial/LED1'
 * '<S5>'   : 'manualController/Serial/MATLAB Function1'
 * '<S6>'   : 'manualController/Serial/Parser'
 * '<S7>'   : 'manualController/Serial/Parser2'
 * '<S8>'   : 'manualController/Serial/serial'
 * '<S9>'   : 'manualController/Subsystem/MATLAB Function1'
 * '<S10>'  : 'manualController/Subsystem/PWM'
 * '<S11>'  : 'manualController/Subsystem/PWM1'
 * '<S12>'  : 'manualController/Subsystem/PWM2'
 * '<S13>'  : 'manualController/Subsystem/PWM3'
 */
#endif                                 /* RTW_HEADER_manualController_h_ */

/*
 * File trailer for generated code.
 *
 * [EOF]
 */

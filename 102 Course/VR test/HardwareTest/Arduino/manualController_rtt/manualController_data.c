/*
 * File: manualController_data.c
 *
 * Code generated for Simulink model 'manualController'.
 *
 * Model version                  : 1.75
 * Simulink Coder version         : 8.7 (R2014b) 08-Sep-2014
 * TLC version                    : 8.7 (Aug  5 2014)
 * C/C++ source code generated on : Thu Mar 05 19:00:44 2015
 *
 * Target selection: realtime.tlc
 * Embedded hardware selection: Atmel->AVR
 * Code generation objectives: Unspecified
 * Validation result: Not run
 */

#include "manualController.h"
#include "manualController_private.h"

/* Block parameters (auto storage) */
P_manualController_T manualController_P = {
  5U,                                  /* Mask Parameter: PWM_pinNumber
                                        * Referenced by: '<S10>/PWM'
                                        */
  6U,                                  /* Mask Parameter: PWM_pinNumber_k
                                        * Referenced by: '<S11>/PWM'
                                        */
  10U,                                 /* Mask Parameter: PWM_pinNumber_k5
                                        * Referenced by: '<S12>/PWM'
                                        */
  9U,                                  /* Mask Parameter: PWM_pinNumber_i
                                        * Referenced by: '<S13>/PWM'
                                        */
  0U,                                  /* Computed Parameter: Constant_Value
                                        * Referenced by: '<S3>/Constant'
                                        */
  1U                                   /* Computed Parameter: Constant3_Value
                                        * Referenced by: '<Root>/Constant3'
                                        */
};

/*
 * File trailer for generated code.
 *
 * [EOF]
 */

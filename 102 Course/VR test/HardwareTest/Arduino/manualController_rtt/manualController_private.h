/*
 * File: manualController_private.h
 *
 * Code generated for Simulink model 'manualController'.
 *
 * Model version                  : 1.75
 * Simulink Coder version         : 8.7 (R2014b) 08-Sep-2014
 * TLC version                    : 8.7 (Aug  5 2014)
 * C/C++ source code generated on : Thu Mar 05 19:00:44 2015
 *
 * Target selection: realtime.tlc
 * Embedded hardware selection: Atmel->AVR
 * Code generation objectives: Unspecified
 * Validation result: Not run
 */

#ifndef RTW_HEADER_manualController_private_h_
#define RTW_HEADER_manualController_private_h_
#include "rtwtypes.h"

extern void sfFASTLED_Outputs_wrapper();
extern void sfSERIALTEST_Outputs_wrapper(uint8_T *message);

#endif                                 /* RTW_HEADER_manualController_private_h_ */

/*
 * File trailer for generated code.
 *
 * [EOF]
 */

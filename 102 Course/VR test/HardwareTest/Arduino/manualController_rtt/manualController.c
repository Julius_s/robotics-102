/*
 * File: manualController.c
 *
 * Code generated for Simulink model 'manualController'.
 *
 * Model version                  : 1.75
 * Simulink Coder version         : 8.7 (R2014b) 08-Sep-2014
 * TLC version                    : 8.7 (Aug  5 2014)
 * C/C++ source code generated on : Thu Mar 05 19:00:44 2015
 *
 * Target selection: realtime.tlc
 * Embedded hardware selection: Atmel->AVR
 * Code generation objectives: Unspecified
 * Validation result: Not run
 */

#include "manualController.h"
#include "manualController_private.h"

/* Block signals (auto storage) */
B_manualController_T manualController_B;

/* Block states (auto storage) */
DW_manualController_T manualController_DW;

/* Real-time model */
RT_MODEL_manualController_T manualController_M_;
RT_MODEL_manualController_T *const manualController_M = &manualController_M_;

/* Model output function */
void manualController_output(void)
{
  uint8_T newMessage[27];
  boolean_T y;
  int8_T b_y;
  uint8_T e_x[2];
  int16_T k;
  static const uint8_T c[6] = { 103U, 103U, 103U, 98U, 98U, 98U };

  static const uint8_T d[6] = { 111U, 100U, 103U, 111U, 100U, 103U };

  boolean_T exitg1;
  Player rtb_players[6];
  uint8_T rtb_BK_B;
  uint8_T rtb_FWD_B;
  uint8_T rtb_FWD_A;
  uint8_T rtb_BK_A;
  uint8_T on;
  int16_T i;

  /* MATLAB Function: '<S1>/MATLAB Function1' */
  /* MATLAB Function 'Serial/MATLAB Function1': '<S5>:1' */
  /* '<S5>:1:7' */
  on = manualController_DW.once;
  if (manualController_DW.once == 1) {
    /* '<S5>:1:8' */
    /* '<S5>:1:9' */
    manualController_DW.once = 0U;
  }

  /* End of MATLAB Function: '<S1>/MATLAB Function1' */

  /* Outputs for Enabled SubSystem: '<S1>/serial' incorporates:
   *  EnablePort: '<S8>/Enable'
   */
  /* RelationalOperator: '<S3>/Compare' incorporates:
   *  Constant: '<S3>/Constant'
   */
  if (on == manualController_P.Constant_Value) {
    /* S-Function (sfSERIALTEST): '<S8>/S-Function Builder1' */
    sfSERIALTEST_Outputs_wrapper( &manualController_B.SFunctionBuilder1[0] );
  }

  /* End of RelationalOperator: '<S3>/Compare' */
  /* End of Outputs for SubSystem: '<S1>/serial' */

  /* MATLAB Function: '<S1>/Parser' */
  for (i = 0; i < 27; i++) {
    newMessage[i] = manualController_B.SFunctionBuilder1[i];
  }

  /* MATLAB Function 'Serial/Parser': '<S6>:1' */
  /* '<S6>:1:27' */
  /* '<S6>:1:26' */
  /*  expects a standard 30 byte long message */
  y = false;
  k = 0;
  exitg1 = false;
  while ((!exitg1) && (k < 27)) {
    if (!(manualController_B.SFunctionBuilder1[k] == 0)) {
      y = true;
      exitg1 = true;
    } else {
      k++;
    }
  }

  if (!y) {
    /* '<S6>:1:7' */
    /* '<S6>:1:8' */
    for (i = 0; i < 27; i++) {
      newMessage[i] = manualController_DW.oldMes[i];
    }
  } else {
    /* '<S6>:1:10' */
    for (i = 0; i < 27; i++) {
      manualController_DW.oldMes[i] = manualController_B.SFunctionBuilder1[i];
    }
  }

  /* '<S6>:1:13' */
  /* '<S6>:1:14' */
  /* '<S6>:1:15' */
  /* '<S6>:1:16' */
  /* '<S6>:1:17' */
  /* '<S6>:1:18' */
  /* '<S6>:1:19' */
  /* '<S6>:1:21' */
  /* '<S6>:1:22' */
  rtb_BK_A = newMessage[1];
  memcpy(&b_y, &rtb_BK_A, (size_t)1 * sizeof(int8_T));

  /* '<S6>:1:23' */
  rtb_BK_A = newMessage[2];
  memcpy(&b_y, &rtb_BK_A, (size_t)1 * sizeof(int8_T));

  /* '<S6>:1:24' */
  /* '<S6>:1:25' */
  /* '<S6>:1:26' */
  /* '<S6>:1:27' */
  /* '<S6>:1:28' */
  for (k = 0; k < 6; k++) {
    /* '<S6>:1:28' */
    /* '<S6>:1:29' */
    rtb_BK_A = newMessage[(k << 2) + 3];
    memcpy(&b_y, &rtb_BK_A, (size_t)1 * sizeof(int8_T));
    rtb_players[k].x = b_y;

    /* '<S6>:1:30' */
    rtb_BK_A = newMessage[(k << 2) + 4];
    memcpy(&b_y, &rtb_BK_A, (size_t)1 * sizeof(int8_T));
    rtb_players[k].y = b_y;

    /* '<S6>:1:31' */
    i = k << 2;
    e_x[0] = newMessage[i + 5];
    e_x[1] = newMessage[i + 6];
    memcpy(&i, &e_x[0], (size_t)1 * sizeof(int16_T));
    rtb_players[k].orientation = i;

    /* '<S6>:1:32' */
    rtb_players[k].color = c[k];

    /* '<S6>:1:33' */
    rtb_players[k].position = d[k];

    /* '<S6>:1:34' */
    rtb_players[k].valid = 1U;

    /* '<S6>:1:28' */
  }

  /* Outputs for Enabled SubSystem: '<Root>/Subsystem' incorporates:
   *  EnablePort: '<S2>/Enable'
   */
  if (newMessage[0] > 0) {
    /* MATLAB Function: '<S2>/MATLAB Function1' incorporates:
     *  Constant: '<Root>/Constant3'
     *  Selector: '<Root>/Selector'
     */
    /* MATLAB Function 'Subsystem/MATLAB Function1': '<S9>:1' */
    if (rtb_players[manualController_P.Constant3_Value - 1].x > 0) {
      b_y = 1;
    } else if (rtb_players[manualController_P.Constant3_Value - 1].x < 0) {
      b_y = -1;
    } else {
      b_y = 0;
    }

    if (b_y > 0) {
      /* '<S9>:1:2' */
      /* '<S9>:1:3' */
      b_y = rtb_players[manualController_P.Constant3_Value - 1].x;
      if (b_y < 0) {
        b_y = 0;
      }

      rtb_FWD_A = (uint8_T)b_y;

      /* '<S9>:1:4' */
      rtb_BK_A = 0U;
    } else {
      /* '<S9>:1:6' */
      b_y = rtb_players[manualController_P.Constant3_Value - 1].x;
      if (b_y < 0) {
        b_y = 0;
      }

      rtb_BK_A = (uint8_T)b_y;

      /* '<S9>:1:7' */
      rtb_FWD_A = 0U;
    }

    if (rtb_players[manualController_P.Constant3_Value - 1].y > 0) {
      b_y = 1;
    } else if (rtb_players[manualController_P.Constant3_Value - 1].y < 0) {
      b_y = -1;
    } else {
      b_y = 0;
    }

    if (b_y > 0) {
      /* '<S9>:1:9' */
      /* '<S9>:1:10' */
      b_y = rtb_players[manualController_P.Constant3_Value - 1].y;
      if (b_y < 0) {
        b_y = 0;
      }

      rtb_FWD_B = (uint8_T)b_y;

      /* '<S9>:1:11' */
      rtb_BK_B = 0U;
    } else {
      /* '<S9>:1:13' */
      b_y = rtb_players[manualController_P.Constant3_Value - 1].y;
      if (b_y < 0) {
        b_y = 0;
      }

      rtb_BK_B = (uint8_T)b_y;

      /* '<S9>:1:14' */
      rtb_FWD_B = 0U;
    }

    /* End of MATLAB Function: '<S2>/MATLAB Function1' */

    /* S-Function (arduinoanalogoutput_sfcn): '<S10>/PWM' */
    MW_analogWrite(manualController_P.PWM_pinNumber, rtb_BK_B);

    /* S-Function (arduinoanalogoutput_sfcn): '<S11>/PWM' */
    MW_analogWrite(manualController_P.PWM_pinNumber_k, rtb_FWD_B);

    /* S-Function (arduinoanalogoutput_sfcn): '<S12>/PWM' */
    MW_analogWrite(manualController_P.PWM_pinNumber_k5, rtb_FWD_A);

    /* S-Function (arduinoanalogoutput_sfcn): '<S13>/PWM' */
    MW_analogWrite(manualController_P.PWM_pinNumber_i, rtb_BK_A);
  }

  /* End of MATLAB Function: '<S1>/Parser' */
  /* End of Outputs for SubSystem: '<Root>/Subsystem' */

  /* Outputs for Enabled SubSystem: '<S1>/LED1' incorporates:
   *  EnablePort: '<S4>/Enable'
   */
  if (on > 0) {
    /* S-Function (sfFASTLED): '<S4>/S-Function Builder' */
    sfFASTLED_Outputs_wrapper( );
  }

  /* End of Outputs for SubSystem: '<S1>/LED1' */
}

/* Model update function */
void manualController_update(void)
{
  /* (no update code required) */
}

/* Model initialize function */
void manualController_initialize(void)
{
  /* Registration code */

  /* initialize error status */
  rtmSetErrorStatus(manualController_M, (NULL));

  /* block I/O */
  (void) memset(((void *) &manualController_B), 0,
                sizeof(B_manualController_T));

  /* states (dwork) */
  (void) memset((void *)&manualController_DW, 0,
                sizeof(DW_manualController_T));

  {
    int16_T i;

    /* Start for Enabled SubSystem: '<Root>/Subsystem' */
    /* Start for S-Function (arduinoanalogoutput_sfcn): '<S10>/PWM' */
    MW_pinModeOutput(manualController_P.PWM_pinNumber);

    /* Start for S-Function (arduinoanalogoutput_sfcn): '<S11>/PWM' */
    MW_pinModeOutput(manualController_P.PWM_pinNumber_k);

    /* Start for S-Function (arduinoanalogoutput_sfcn): '<S12>/PWM' */
    MW_pinModeOutput(manualController_P.PWM_pinNumber_k5);

    /* Start for S-Function (arduinoanalogoutput_sfcn): '<S13>/PWM' */
    MW_pinModeOutput(manualController_P.PWM_pinNumber_i);

    /* End of Start for SubSystem: '<Root>/Subsystem' */

    /* InitializeConditions for MATLAB Function: '<S1>/MATLAB Function1' */
    manualController_DW.once = 1U;

    /* InitializeConditions for MATLAB Function: '<S1>/Parser' */
    for (i = 0; i < 27; i++) {
      manualController_DW.oldMes[i] = 0U;
    }

    /* End of InitializeConditions for MATLAB Function: '<S1>/Parser' */
  }
}

/* Model terminate function */
void manualController_terminate(void)
{
  /* (no terminate code required) */
}

/*
 * File trailer for generated code.
 *
 * [EOF]
 */

/*
 * File: greenOffence.h
 *
 * Code generated for Simulink model 'greenOffence'.
 *
 * Model version                  : 1.193
 * Simulink Coder version         : 8.7 (R2014b) 08-Sep-2014
 * TLC version                    : 8.7 (Aug  5 2014)
 * C/C++ source code generated on : Wed Mar 18 15:21:09 2015
 *
 * Target selection: realtime.tlc
 * Embedded hardware selection: Atmel->AVR
 * Code generation objectives: Unspecified
 * Validation result: Not run
 */

#ifndef RTW_HEADER_greenOffence_h_
#define RTW_HEADER_greenOffence_h_
#include <float.h>
#include <math.h>
#include <string.h>
#include <stddef.h>
#ifndef greenOffence_COMMON_INCLUDES_
# define greenOffence_COMMON_INCLUDES_
#include "rtwtypes.h"
#include "rtw_continuous.h"
#include "rtw_solver.h"
#include "arduino_digitaloutput_lct.h"
#include "arduino_analogoutput_lct.h"
#endif                                 /* greenOffence_COMMON_INCLUDES_ */

#include "greenOffence_types.h"
#include "rtGetNaN.h"
#include "rt_nonfinite.h"
#include "rt_defines.h"
#include "rtGetInf.h"

/* Macros for accessing real-time model data structure */
#ifndef rtmGetErrorStatus
# define rtmGetErrorStatus(rtm)        ((rtm)->errorStatus)
#endif

#ifndef rtmSetErrorStatus
# define rtmSetErrorStatus(rtm, val)   ((rtm)->errorStatus = (val))
#endif

/* Block signals (auto storage) */
typedef struct {
  Waypoint finalWay;                   /* '<S4>/Player Decisions3' */
  Ball Ball_n;                         /* '<S3>/Parser' */
  Player Selector;                     /* '<Root>/Selector' */
  Player players[6];                   /* '<S3>/Parser' */
  uint8_T shoot;                       /* '<S4>/Player Decisions3' */
  uint8_T SFunctionBuilder1[27];       /* '<S10>/S-Function Builder1' */
} B_greenOffence_T;

/* Block states (auto storage) for system '<Root>' */
typedef struct {
  Waypoint shootWay;                   /* '<S4>/Player Decisions3' */
  Player enemy[3];                     /* '<S4>/Player Decisions3' */
  real32_T itteration;                 /* '<S4>/MATLAB Function1' */
  int8_T startingPos[2];               /* '<S4>/Player Decisions3' */
  int8_T startingPos_b[2];             /* '<S4>/Player Decisions1' */
  uint8_T is_active_c9_greenOffence;   /* '<S4>/Player Decisions3' */
  uint8_T is_c9_greenOffence;          /* '<S4>/Player Decisions3' */
  uint8_T is_GameIsOn_Offence;         /* '<S4>/Player Decisions3' */
  uint8_T is_waiting;                  /* '<S4>/Player Decisions3' */
  uint8_T temporalCounter_i1;          /* '<S4>/Player Decisions3' */
  uint8_T is_active_c11_greenOffence;  /* '<S4>/Player Decisions1' */
  uint8_T is_c11_greenOffence;         /* '<S4>/Player Decisions1' */
  uint8_T is_GameIsOn_Goalie;          /* '<S4>/Player Decisions1' */
  uint8_T is_waiting_g;                /* '<S4>/Player Decisions1' */
  uint8_T k;                           /* '<S4>/MATLAB Function' */
  uint8_T i;                           /* '<S4>/MATLAB Function' */
  uint8_T oldMes[27];                  /* '<S3>/Parser' */
  uint8_T once;                        /* '<S3>/MATLAB Function1' */
} DW_greenOffence_T;

/* Parameters (auto storage) */
struct P_greenOffence_T_ {
  uint32_T DigitalOutput_pinNumber;    /* Mask Parameter: DigitalOutput_pinNumber
                                        * Referenced by: '<S11>/Digital Output'
                                        */
  uint32_T PWM_pinNumber;              /* Mask Parameter: PWM_pinNumber
                                        * Referenced by: '<S14>/PWM'
                                        */
  uint32_T PWM_pinNumber_k;            /* Mask Parameter: PWM_pinNumber_k
                                        * Referenced by: '<S15>/PWM'
                                        */
  uint32_T PWM_pinNumber_k5;           /* Mask Parameter: PWM_pinNumber_k5
                                        * Referenced by: '<S16>/PWM'
                                        */
  uint32_T PWM_pinNumber_i;            /* Mask Parameter: PWM_pinNumber_i
                                        * Referenced by: '<S17>/PWM'
                                        */
  int16_T CompareToConstant_const;     /* Mask Parameter: CompareToConstant_const
                                        * Referenced by: '<S26>/Constant'
                                        */
  real_T Constant_Value;               /* Expression: 0
                                        * Referenced by: '<S25>/Constant'
                                        */
  real_T Constant_Value_n;             /* Expression: 1/2
                                        * Referenced by: '<S21>/Constant'
                                        */
  real_T Gain8_Gain;                   /* Expression: 1/80
                                        * Referenced by: '<S20>/Gain8'
                                        */
  real_T Bias_Bias;                    /* Expression: 0.15
                                        * Referenced by: '<S20>/Bias'
                                        */
  real_T Saturation_UpperSat;          /* Expression: 0.5
                                        * Referenced by: '<S20>/Saturation'
                                        */
  real_T Saturation_LowerSat;          /* Expression: 0
                                        * Referenced by: '<S20>/Saturation'
                                        */
  real_T Gain9_Gain;                   /* Expression: -1
                                        * Referenced by: '<S20>/Gain9'
                                        */
  real32_T Bias2_Bias;                 /* Computed Parameter: Bias2_Bias
                                        * Referenced by: '<S20>/Bias2'
                                        */
  real32_T Bias1_Bias;                 /* Computed Parameter: Bias1_Bias
                                        * Referenced by: '<S20>/Bias1'
                                        */
  real32_T Saturation1_UpperSat;       /* Computed Parameter: Saturation1_UpperSat
                                        * Referenced by: '<S20>/Saturation1'
                                        */
  real32_T Saturation1_LowerSat;       /* Computed Parameter: Saturation1_LowerSat
                                        * Referenced by: '<S20>/Saturation1'
                                        */
  real32_T SaturationOrien3_UpperSat;  /* Computed Parameter: SaturationOrien3_UpperSat
                                        * Referenced by: '<S20>/SaturationOrien3'
                                        */
  real32_T SaturationOrien3_LowerSat;  /* Computed Parameter: SaturationOrien3_LowerSat
                                        * Referenced by: '<S20>/SaturationOrien3'
                                        */
  real32_T SaturationOrien4_UpperSat;  /* Computed Parameter: SaturationOrien4_UpperSat
                                        * Referenced by: '<S20>/SaturationOrien4'
                                        */
  real32_T SaturationOrien4_LowerSat;  /* Computed Parameter: SaturationOrien4_LowerSat
                                        * Referenced by: '<S20>/SaturationOrien4'
                                        */
  real32_T ManualConst_Value[3];       /* Computed Parameter: ManualConst_Value
                                        * Referenced by: '<Root>/ManualConst'
                                        */
  int16_T Gain7_Gain;                  /* Computed Parameter: Gain7_Gain
                                        * Referenced by: '<S20>/Gain7'
                                        */
  int16_T Gain3_Gain;                  /* Computed Parameter: Gain3_Gain
                                        * Referenced by: '<S20>/Gain3'
                                        */
  uint8_T certainty_Value;             /* Computed Parameter: certainty_Value
                                        * Referenced by: '<S25>/certainty'
                                        */
  uint8_T Constant_Value_i;            /* Computed Parameter: Constant_Value_i
                                        * Referenced by: '<S5>/Constant'
                                        */
  uint8_T Constant3_Value;             /* Computed Parameter: Constant3_Value
                                        * Referenced by: '<Root>/Constant3'
                                        */
};

/* Real-time Model Data Structure */
struct tag_RTM_greenOffence_T {
  const char_T * volatile errorStatus;
};

/* Block parameters (auto storage) */
extern P_greenOffence_T greenOffence_P;

/* Block signals (auto storage) */
extern B_greenOffence_T greenOffence_B;

/* Block states (auto storage) */
extern DW_greenOffence_T greenOffence_DW;

/* Model entry point functions */
extern void greenOffence_initialize(void);
extern void greenOffence_output(void);
extern void greenOffence_update(void);
extern void greenOffence_terminate(void);

/* Real-time Model object */
extern RT_MODEL_greenOffence_T *const greenOffence_M;

/*-
 * The generated code includes comments that allow you to trace directly
 * back to the appropriate location in the model.  The basic format
 * is <system>/block_name, where system is the system number (uniquely
 * assigned by Simulink) and block_name is the name of the block.
 *
 * Use the MATLAB hilite_system command to trace the generated code back
 * to the model.  For example,
 *
 * hilite_system('<S3>')    - opens system 3
 * hilite_system('<S3>/Kp') - opens and selects block Kp which resides in S3
 *
 * Here is the system hierarchy for this model
 *
 * '<Root>' : 'greenOffence'
 * '<S1>'   : 'greenOffence/MATLAB Function'
 * '<S2>'   : 'greenOffence/MATLAB Function1'
 * '<S3>'   : 'greenOffence/Serial'
 * '<S4>'   : 'greenOffence/Subsystem'
 * '<S5>'   : 'greenOffence/Serial/Compare To Zero'
 * '<S6>'   : 'greenOffence/Serial/LED1'
 * '<S7>'   : 'greenOffence/Serial/MATLAB Function1'
 * '<S8>'   : 'greenOffence/Serial/Parser'
 * '<S9>'   : 'greenOffence/Serial/Parser2'
 * '<S10>'  : 'greenOffence/Serial/serial'
 * '<S11>'  : 'greenOffence/Subsystem/Digital Output'
 * '<S12>'  : 'greenOffence/Subsystem/MATLAB Function'
 * '<S13>'  : 'greenOffence/Subsystem/MATLAB Function1'
 * '<S14>'  : 'greenOffence/Subsystem/PWM'
 * '<S15>'  : 'greenOffence/Subsystem/PWM1'
 * '<S16>'  : 'greenOffence/Subsystem/PWM2'
 * '<S17>'  : 'greenOffence/Subsystem/PWM3'
 * '<S18>'  : 'greenOffence/Subsystem/Player Decisions1'
 * '<S19>'  : 'greenOffence/Subsystem/Player Decisions3'
 * '<S20>'  : 'greenOffence/Subsystem/control'
 * '<S21>'  : 'greenOffence/Subsystem/control/Norm'
 * '<S22>'  : 'greenOffence/Subsystem/control/calc_dO'
 * '<S23>'  : 'greenOffence/Subsystem/control/calc_dO1'
 * '<S24>'  : 'greenOffence/Subsystem/control/calc_dO3'
 * '<S25>'  : 'greenOffence/Subsystem/control/decisionParser'
 * '<S26>'  : 'greenOffence/Subsystem/control/decisionParser/Compare To Constant'
 * '<S27>'  : 'greenOffence/Subsystem/control/decisionParser/calcTargetOrien'
 * '<S28>'  : 'greenOffence/Subsystem/control/decisionParser/fwdLimit'
 */
#endif                                 /* RTW_HEADER_greenOffence_h_ */

/*
 * File trailer for generated code.
 *
 * [EOF]
 */

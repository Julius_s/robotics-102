/*
 * File: ert_main.c
 *
 * Code generated for Simulink model 'greenOffence'.
 *
 * Model version                  : 1.193
 * Simulink Coder version         : 8.7 (R2014b) 08-Sep-2014
 * TLC version                    : 8.7 (Aug  5 2014)
 * C/C++ source code generated on : Wed Mar 18 15:21:09 2015
 *
 * Target selection: realtime.tlc
 * Embedded hardware selection: Atmel->AVR
 * Code generation objectives: Unspecified
 * Validation result: Not run
 */

#include "greenOffence.h"
#include "Arduino.h"
#include "io_wrappers.h"
#define STEP_SIZE                      (unsigned long) (100000)

int_T main(void)
{
  unsigned long oldtime = 0L;
  unsigned long actualtime;
  init();
  greenOffence_initialize();

#ifdef _RTT_USE_SERIAL0_

  Serial_begin(0, 9600);
  Serial_write(0, "***starting the model***", 26);

#endif

  /* The main step loop */
  while (rtmGetErrorStatus(greenOffence_M) == (NULL)) {
    actualtime = micros();
    if ((unsigned long) (actualtime - oldtime) >= STEP_SIZE) {
      oldtime = actualtime;
      greenOffence_output();

      /* Get model outputs here */
      greenOffence_update();
    }
  }

  greenOffence_terminate();
  return 0;
}

/*
 * File trailer for generated code.
 *
 * [EOF]
 */

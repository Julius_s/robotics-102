/*
 * File: greenOffence.c
 *
 * Code generated for Simulink model 'greenOffence'.
 *
 * Model version                  : 1.193
 * Simulink Coder version         : 8.7 (R2014b) 08-Sep-2014
 * TLC version                    : 8.7 (Aug  5 2014)
 * C/C++ source code generated on : Wed Mar 18 15:21:09 2015
 *
 * Target selection: realtime.tlc
 * Embedded hardware selection: Atmel->AVR
 * Code generation objectives: Unspecified
 * Validation result: Not run
 */

#include "greenOffence.h"
#include "greenOffence_private.h"

/* Named constants for Chart: '<S4>/Player Decisions1' */
#define greenOffence_IN_GameIsOn_Goalie ((uint8_T)1U)
#define greenOffence_IN_Idle1          ((uint8_T)1U)
#define greenOffence_IN_NO_ACTIVE_CHILD ((uint8_T)0U)
#define greenOffence_IN_waiting        ((uint8_T)2U)

/* Named constants for Chart: '<S4>/Player Decisions3' */
#define greenOffenc_IN_GameIsOn_Offence ((uint8_T)1U)
#define greenOffence_IN_Aim            ((uint8_T)1U)
#define greenOffence_IN_GetToTheBall   ((uint8_T)2U)
#define greenOffence_IN_Idle_p         ((uint8_T)3U)
#define greenOffence_IN_Kick           ((uint8_T)4U)

/* Block signals (auto storage) */
B_greenOffence_T greenOffence_B;

/* Block states (auto storage) */
DW_greenOffence_T greenOffence_DW;

/* Real-time model */
RT_MODEL_greenOffence_T greenOffence_M_;
RT_MODEL_greenOffence_T *const greenOffence_M = &greenOffence_M_;

/* Forward declaration for local functions */
static void greenOffence_calcShootWay(void);
static uint8_T greenOffence_checkReached_g(const int8_T pos[2], real_T tol);
static void greenOffence_calcStartPos_k(void);
static void greenOffence_calcStartTeams(void);
int16_T div_s16_floor(int16_T numerator, int16_T denominator)
{
  int16_T quotient;
  uint16_T absNumerator;
  uint16_T absDenominator;
  uint16_T tempAbsQuotient;
  boolean_T quotientNeedsNegation;
  if (denominator == 0) {
    quotient = numerator >= 0 ? MAX_int16_T : MIN_int16_T;

    /* Divide by zero handler */
  } else {
    absNumerator = (uint16_T)(numerator >= 0 ? numerator : -numerator);
    absDenominator = (uint16_T)(denominator >= 0 ? denominator : -denominator);
    quotientNeedsNegation = ((numerator < 0) != (denominator < 0));
    tempAbsQuotient = absNumerator / absDenominator;
    if (quotientNeedsNegation) {
      absNumerator %= absDenominator;
      if (absNumerator > 0U) {
        tempAbsQuotient++;
      }
    }

    quotient = quotientNeedsNegation ? -(int16_T)tempAbsQuotient : (int16_T)
      tempAbsQuotient;
  }

  return quotient;
}

real32_T rt_roundf_snf(real32_T u)
{
  real32_T y;
  if ((real32_T)fabs(u) < 8.388608E+6F) {
    if (u >= 0.5F) {
      y = (real32_T)floor(u + 0.5F);
    } else if (u > -0.5F) {
      y = u * 0.0F;
    } else {
      y = (real32_T)ceil(u - 0.5F);
    }
  } else {
    y = u;
  }

  return y;
}

real32_T rt_atan2f_snf(real32_T u0, real32_T u1)
{
  real32_T y;
  int16_T u0_0;
  int16_T u1_0;
  if (rtIsNaNF(u0) || rtIsNaNF(u1)) {
    y = (rtNaNF);
  } else if (rtIsInfF(u0) && rtIsInfF(u1)) {
    if (u0 > 0.0F) {
      u0_0 = 1;
    } else {
      u0_0 = -1;
    }

    if (u1 > 0.0F) {
      u1_0 = 1;
    } else {
      u1_0 = -1;
    }

    y = (real32_T)atan2(u0_0, u1_0);
  } else if (u1 == 0.0F) {
    if (u0 > 0.0F) {
      y = RT_PIF / 2.0F;
    } else if (u0 < 0.0F) {
      y = -(RT_PIF / 2.0F);
    } else {
      y = 0.0F;
    }
  } else {
    y = (real32_T)atan2(u0, u1);
  }

  return y;
}

/* Function for Chart: '<S4>/Player Decisions3' */
static void greenOffence_calcShootWay(void)
{
  real32_T y;
  real32_T scale;
  int16_T absxk;
  real32_T t;
  int8_T pos_idx_0;
  int8_T pos_idx_1;
  int16_T tmp;
  int16_T tmp_0;
  int16_T tmp_1;

  /* MATLAB Function 'calcShootWay': '<S19>:126' */
  if (greenOffence_DW.enemy[2].y > 0) {
    /* '<S19>:126:2' */
    /* '<S19>:126:3' */
    pos_idx_0 = 70;
    pos_idx_1 = -60;
  } else {
    /* '<S19>:126:5' */
    pos_idx_0 = 70;
    pos_idx_1 = 60;
  }

  if (greenOffence_B.Selector.color != 103) {
    /* '<S19>:126:7' */
    /* '<S19>:126:8' */
    pos_idx_0 = -70;
  }

  /* '<S19>:126:10' */
  tmp_1 = greenOffence_B.Ball_n.x - pos_idx_0;
  if (tmp_1 > 127) {
    tmp_1 = 127;
  } else {
    if (tmp_1 < -128) {
      tmp_1 = -128;
    }
  }

  tmp_0 = greenOffence_B.Ball_n.y - pos_idx_1;
  if (tmp_0 > 127) {
    tmp_0 = 127;
  } else {
    if (tmp_0 < -128) {
      tmp_0 = -128;
    }
  }

  /* '<S19>:126:11' */
  absxk = greenOffence_B.Ball_n.x - (int8_T)tmp_1;
  if (absxk > 127) {
    absxk = 127;
  } else {
    if (absxk < -128) {
      absxk = -128;
    }
  }

  tmp = greenOffence_B.Ball_n.y - (int8_T)tmp_0;
  if (tmp > 127) {
    tmp = 127;
  } else {
    if (tmp < -128) {
      tmp = -128;
    }
  }

  scale = 1.17549435E-38F;
  absxk = (int16_T)(real32_T)fabs((int8_T)absxk);
  if (absxk > 1.17549435E-38F) {
    t = 1.17549435E-38F / (real32_T)absxk;
    y = 0.0F * t * t + 1.0F;
    scale = absxk;
  } else {
    t = (real32_T)absxk / 1.17549435E-38F;
    y = t * t;
  }

  absxk = (int16_T)(real32_T)fabs((int8_T)tmp);
  if (absxk > scale) {
    t = scale / (real32_T)absxk;
    y = y * t * t + 1.0F;
    scale = absxk;
  } else {
    t = (real32_T)absxk / scale;
    y += t * t;
  }

  y = scale * (real32_T)sqrt(y);
  scale = rt_roundf_snf(30.0F * (real32_T)(int8_T)tmp_1 / y);
  if (scale < 128.0F) {
    if (scale >= -128.0F) {
      pos_idx_0 = (int8_T)scale;
    } else {
      pos_idx_0 = MIN_int8_T;
    }
  } else {
    pos_idx_0 = MAX_int8_T;
  }

  scale = rt_roundf_snf(30.0F * (real32_T)(int8_T)tmp_0 / y);
  if (scale < 128.0F) {
    if (scale >= -128.0F) {
      pos_idx_1 = (int8_T)scale;
    } else {
      pos_idx_1 = MIN_int8_T;
    }
  } else {
    pos_idx_1 = MAX_int8_T;
  }

  /* '<S19>:126:13' */
  tmp_1 = greenOffence_B.Ball_n.x + pos_idx_0;
  if (tmp_1 > 127) {
    tmp_1 = 127;
  } else {
    if (tmp_1 < -128) {
      tmp_1 = -128;
    }
  }

  greenOffence_DW.shootWay.x = (int8_T)tmp_1;

  /* '<S19>:126:14' */
  tmp_1 = greenOffence_B.Ball_n.y + pos_idx_1;
  if (tmp_1 > 127) {
    tmp_1 = 127;
  } else {
    if (tmp_1 < -128) {
      tmp_1 = -128;
    }
  }

  greenOffence_DW.shootWay.y = (int8_T)tmp_1;

  /* '<S19>:126:15' */
  tmp_1 = -pos_idx_1;
  if (tmp_1 > 127) {
    tmp_1 = 127;
  }

  tmp_0 = -pos_idx_0;
  if (tmp_0 > 127) {
    tmp_0 = 127;
  }

  scale = rt_roundf_snf(57.2957802F * rt_atan2f_snf((real32_T)tmp_1, (real32_T)
    tmp_0));
  if (scale < 32768.0F) {
    if (scale >= -32768.0F) {
      greenOffence_DW.shootWay.orientation = (int16_T)scale;
    } else {
      greenOffence_DW.shootWay.orientation = MIN_int16_T;
    }
  } else {
    greenOffence_DW.shootWay.orientation = MAX_int16_T;
  }
}

/* Function for Chart: '<S4>/Player Decisions3' */
static uint8_T greenOffence_checkReached_g(const int8_T pos[2], real_T tol)
{
  uint8_T posReached;
  real32_T y;
  real32_T scale;
  int16_T absxk;
  real32_T t;
  int16_T tmp;

  /* MATLAB Function 'checkReached': '<S19>:94' */
  /* '<S19>:94:2' */
  posReached = 0U;
  absxk = pos[0] - greenOffence_B.Selector.x;
  if (absxk > 127) {
    absxk = 127;
  } else {
    if (absxk < -128) {
      absxk = -128;
    }
  }

  tmp = pos[1] - greenOffence_B.Selector.y;
  if (tmp > 127) {
    tmp = 127;
  } else {
    if (tmp < -128) {
      tmp = -128;
    }
  }

  scale = 1.17549435E-38F;
  absxk = (int16_T)(real32_T)fabs((int8_T)absxk);
  if (absxk > 1.17549435E-38F) {
    t = 1.17549435E-38F / (real32_T)absxk;
    y = 0.0F * t * t + 1.0F;
    scale = absxk;
  } else {
    t = (real32_T)absxk / 1.17549435E-38F;
    y = t * t;
  }

  absxk = (int16_T)(real32_T)fabs((int8_T)tmp);
  if (absxk > scale) {
    t = scale / (real32_T)absxk;
    y = y * t * t + 1.0F;
    scale = absxk;
  } else {
    t = (real32_T)absxk / scale;
    y += t * t;
  }

  y = scale * (real32_T)sqrt(y);
  if (y < tol) {
    /* '<S19>:94:3' */
    /* '<S19>:94:4' */
    posReached = 1U;
  }

  return posReached;
}

/* Function for Chart: '<S4>/Player Decisions3' */
static void greenOffence_calcStartPos_k(void)
{
  int16_T tmp;

  /* MATLAB Function 'calcStartPos': '<S19>:86' */
  /* '<S19>:86:2' */
  switch (greenOffence_B.Selector.position) {
   case 111U:
    /* '<S19>:86:4' */
    greenOffence_DW.startingPos[0] = 10;
    greenOffence_DW.startingPos[1] = 0;
    break;

   case 100U:
    /* '<S19>:86:6' */
    greenOffence_DW.startingPos[0] = 30;
    greenOffence_DW.startingPos[1] = 0;
    break;

   case 103U:
    /* '<S19>:86:8' */
    greenOffence_DW.startingPos[0] = 70;
    greenOffence_DW.startingPos[1] = 0;
    break;
  }

  if (greenOffence_B.Selector.color == 103) {
    /* '<S19>:86:10' */
    /* '<S19>:86:11' */
    tmp = -greenOffence_DW.startingPos[0];
    if (tmp > 127) {
      tmp = 127;
    }

    greenOffence_DW.startingPos[0] = (int8_T)tmp;
    tmp = -greenOffence_DW.startingPos[1];
    if (tmp > 127) {
      tmp = 127;
    }

    greenOffence_DW.startingPos[1] = (int8_T)tmp;
  }
}

/* Function for Chart: '<S4>/Player Decisions3' */
static void greenOffence_calcStartTeams(void)
{
  /* MATLAB Function 'calcStartTeams': '<S19>:112' */
  /* '<S19>:112:2' */
  switch (greenOffence_B.Selector.color) {
   case 103U:
    /* '<S19>:112:4' */
    greenOffence_DW.enemy[0] = greenOffence_B.players[3];
    greenOffence_DW.enemy[1] = greenOffence_B.players[4];
    greenOffence_DW.enemy[2] = greenOffence_B.players[5];

    /* '<S19>:112:5' */
    break;

   case 98U:
    /* '<S19>:112:7' */
    greenOffence_DW.enemy[0] = greenOffence_B.players[0];
    greenOffence_DW.enemy[1] = greenOffence_B.players[1];
    greenOffence_DW.enemy[2] = greenOffence_B.players[2];

    /* '<S19>:112:8' */
    break;
  }
}

real_T rt_atan2d_snf(real_T u0, real_T u1)
{
  real_T y;
  int16_T u0_0;
  int16_T u1_0;
  if (rtIsNaN(u0) || rtIsNaN(u1)) {
    y = (rtNaN);
  } else if (rtIsInf(u0) && rtIsInf(u1)) {
    if (u0 > 0.0) {
      u0_0 = 1;
    } else {
      u0_0 = -1;
    }

    if (u1 > 0.0) {
      u1_0 = 1;
    } else {
      u1_0 = -1;
    }

    y = atan2(u0_0, u1_0);
  } else if (u1 == 0.0) {
    if (u0 > 0.0) {
      y = RT_PI / 2.0;
    } else if (u0 < 0.0) {
      y = -(RT_PI / 2.0);
    } else {
      y = 0.0;
    }
  } else {
    y = atan2(u0, u1);
  }

  return y;
}

real_T rt_roundd_snf(real_T u)
{
  real_T y;
  if (fabs(u) < 4.503599627370496E+15) {
    if (u >= 0.5) {
      y = floor(u + 0.5);
    } else if (u > -0.5) {
      y = u * 0.0;
    } else {
      y = ceil(u - 0.5);
    }
  } else {
    y = u;
  }

  return y;
}

real_T rt_remd_snf(real_T u0, real_T u1)
{
  real_T y;
  real_T u1_0;
  if (!((!rtIsNaN(u0)) && (!rtIsInf(u0)) && ((!rtIsNaN(u1)) && (!rtIsInf(u1)))))
  {
    y = (rtNaN);
  } else {
    if (u1 < 0.0) {
      u1_0 = ceil(u1);
    } else {
      u1_0 = floor(u1);
    }

    if ((u1 != 0.0) && (u1 != u1_0)) {
      u1_0 = u0 / u1;
      if (fabs(u1_0 - rt_roundd_snf(u1_0)) <= DBL_EPSILON * fabs(u1_0)) {
        y = 0.0;
      } else {
        y = fmod(u0, u1);
      }
    } else {
      y = fmod(u0, u1);
    }
  }

  return y;
}

real_T rt_powd_snf(real_T u0, real_T u1)
{
  real_T y;
  real_T tmp;
  real_T tmp_0;
  if (rtIsNaN(u0) || rtIsNaN(u1)) {
    y = (rtNaN);
  } else {
    tmp = fabs(u0);
    tmp_0 = fabs(u1);
    if (rtIsInf(u1)) {
      if (tmp == 1.0) {
        y = (rtNaN);
      } else if (tmp > 1.0) {
        if (u1 > 0.0) {
          y = (rtInf);
        } else {
          y = 0.0;
        }
      } else if (u1 > 0.0) {
        y = 0.0;
      } else {
        y = (rtInf);
      }
    } else if (tmp_0 == 0.0) {
      y = 1.0;
    } else if (tmp_0 == 1.0) {
      if (u1 > 0.0) {
        y = u0;
      } else {
        y = 1.0 / u0;
      }
    } else if (u1 == 2.0) {
      y = u0 * u0;
    } else if ((u1 == 0.5) && (u0 >= 0.0)) {
      y = sqrt(u0);
    } else if ((u0 < 0.0) && (u1 > floor(u1))) {
      y = (rtNaN);
    } else {
      y = pow(u0, u1);
    }
  }

  return y;
}

/* Model output function */
void greenOffence_output(void)
{
  uint8_T newMessage[27];
  Player tempEmpty;
  boolean_T y;
  uint8_T e_x[2];
  static const uint8_T c[6] = { 103U, 103U, 103U, 98U, 98U, 98U };

  static const uint8_T d[6] = { 111U, 100U, 103U, 111U, 100U, 103U };

  boolean_T exitg1;
  real_T sigma;
  real_T y_0;
  real_T t;
  int16_T T;
  int16_T M;
  real32_T rtb_Add4;
  real32_T rtb_Saturation1;
  real32_T rtb_SaturationOrien3;
  uint8_T rtb_y;
  int16_T rtb_targetOrientation;
  int16_T rtb_Switch1;
  uint8_T rtb_BK_B;
  uint8_T rtb_FWD_B;
  uint8_T rtb_FWD_A;
  uint8_T on;
  boolean_T Compare;
  int16_T i;
  int8_T tmp[2];
  int16_T absx;
  int8_T rtb_Add_idx_1;
  int8_T rtb_Add_idx_0;
  int32_T tmp_0;
  int32_T tmp_1;
  uint16_T tmp_2;

  /* MATLAB Function: '<S3>/MATLAB Function1' */
  /* MATLAB Function 'Serial/MATLAB Function1': '<S7>:1' */
  /* '<S7>:1:7' */
  on = greenOffence_DW.once;
  if (greenOffence_DW.once == 1) {
    /* '<S7>:1:8' */
    /* '<S7>:1:9' */
    greenOffence_DW.once = 0U;
  }

  /* End of MATLAB Function: '<S3>/MATLAB Function1' */

  /* RelationalOperator: '<S5>/Compare' incorporates:
   *  Constant: '<S5>/Constant'
   */
  Compare = (on == greenOffence_P.Constant_Value_i);

  /* Outputs for Enabled SubSystem: '<S3>/serial' incorporates:
   *  EnablePort: '<S10>/Enable'
   */
  if (Compare) {
    /* S-Function (sfSERIALTEST): '<S10>/S-Function Builder1' */
    sfSERIALTEST_Outputs_wrapper( &greenOffence_B.SFunctionBuilder1[0] );
  }

  /* End of Outputs for SubSystem: '<S3>/serial' */

  /* MATLAB Function: '<S3>/Parser' */
  for (i = 0; i < 27; i++) {
    newMessage[i] = greenOffence_B.SFunctionBuilder1[i];
  }

  /* MATLAB Function 'Serial/Parser': '<S8>:1' */
  /* '<S8>:1:27' */
  /* '<S8>:1:26' */
  /*  expects a standard 30 byte long message */
  y = false;
  rtb_targetOrientation = 0;
  exitg1 = false;
  while ((!exitg1) && (rtb_targetOrientation < 27)) {
    if (!(greenOffence_B.SFunctionBuilder1[rtb_targetOrientation] == 0)) {
      y = true;
      exitg1 = true;
    } else {
      rtb_targetOrientation++;
    }
  }

  if (!y) {
    /* '<S8>:1:7' */
    /* '<S8>:1:8' */
    for (i = 0; i < 27; i++) {
      newMessage[i] = greenOffence_DW.oldMes[i];
    }
  } else {
    /* '<S8>:1:10' */
    for (i = 0; i < 27; i++) {
      greenOffence_DW.oldMes[i] = greenOffence_B.SFunctionBuilder1[i];
    }
  }

  /* '<S8>:1:13' */
  tempEmpty.x = 0;

  /* '<S8>:1:14' */
  tempEmpty.y = 0;

  /* '<S8>:1:15' */
  tempEmpty.orientation = 0;

  /* '<S8>:1:16' */
  tempEmpty.color = 0U;

  /* '<S8>:1:17' */
  tempEmpty.position = 0U;

  /* '<S8>:1:18' */
  tempEmpty.valid = 0U;

  /* '<S8>:1:19' */
  greenOffence_B.players[0] = tempEmpty;
  greenOffence_B.players[1] = tempEmpty;
  greenOffence_B.players[2] = tempEmpty;
  greenOffence_B.players[3] = tempEmpty;
  greenOffence_B.players[4] = tempEmpty;
  greenOffence_B.players[5] = tempEmpty;

  /* '<S8>:1:21' */
  /* '<S8>:1:22' */
  rtb_y = newMessage[1];
  memcpy(&rtb_Add_idx_0, &rtb_y, (size_t)1 * sizeof(int8_T));
  greenOffence_B.Ball_n.x = rtb_Add_idx_0;

  /* '<S8>:1:23' */
  rtb_y = newMessage[2];
  memcpy(&rtb_Add_idx_0, &rtb_y, (size_t)1 * sizeof(int8_T));
  greenOffence_B.Ball_n.y = rtb_Add_idx_0;

  /* '<S8>:1:24' */
  greenOffence_B.Ball_n.valid = 1U;

  /* '<S8>:1:25' */
  /* '<S8>:1:26' */
  /* '<S8>:1:27' */
  /* '<S8>:1:28' */
  for (rtb_targetOrientation = 0; rtb_targetOrientation < 6;
       rtb_targetOrientation++) {
    /* '<S8>:1:28' */
    /* '<S8>:1:29' */
    rtb_y = newMessage[(rtb_targetOrientation << 2) + 3];
    memcpy(&rtb_Add_idx_0, &rtb_y, (size_t)1 * sizeof(int8_T));
    greenOffence_B.players[rtb_targetOrientation].x = rtb_Add_idx_0;

    /* '<S8>:1:30' */
    rtb_y = newMessage[(rtb_targetOrientation << 2) + 4];
    memcpy(&rtb_Add_idx_0, &rtb_y, (size_t)1 * sizeof(int8_T));
    greenOffence_B.players[rtb_targetOrientation].y = rtb_Add_idx_0;

    /* '<S8>:1:31' */
    i = rtb_targetOrientation << 2;
    e_x[0] = newMessage[i + 5];
    e_x[1] = newMessage[i + 6];
    memcpy(&i, &e_x[0], (size_t)1 * sizeof(int16_T));
    greenOffence_B.players[rtb_targetOrientation].orientation = i;

    /* '<S8>:1:32' */
    greenOffence_B.players[rtb_targetOrientation].color =
      c[rtb_targetOrientation];

    /* '<S8>:1:33' */
    greenOffence_B.players[rtb_targetOrientation].position =
      d[rtb_targetOrientation];

    /* '<S8>:1:34' */
    greenOffence_B.players[rtb_targetOrientation].valid = 1U;

    /* '<S8>:1:28' */
  }

  /* Selector: '<Root>/Selector' incorporates:
   *  Constant: '<Root>/Constant3'
   */
  greenOffence_B.Selector =
    greenOffence_B.players[greenOffence_P.Constant3_Value - 1];

  /* Outputs for Enabled SubSystem: '<Root>/Subsystem' incorporates:
   *  EnablePort: '<S4>/Enable'
   */
  /* MATLAB Function 'MATLAB Function': '<S1>:1' */
  /* '<S1>:1:2' */
  /* '<S1>:1:3' */
  /* '<S1>:1:4' */
  if (Compare) {
    /* MATLAB Function: '<S4>/MATLAB Function' */
    /* MATLAB Function 'Subsystem/MATLAB Function': '<S12>:1' */
    /* '<S12>:1:10' */
    rtb_y = greenOffence_DW.k;
    if ((greenOffence_DW.k == 1) && ((uint8_T)((uint16_T)greenOffence_DW.i -
          (uint8_T)(greenOffence_DW.i / 10U * 10U)) == 0)) {
      /* '<S12>:1:11' */
      /* '<S12>:1:12' */
      greenOffence_DW.k = 0U;
    } else {
      if ((greenOffence_DW.k == 0) && ((uint8_T)((uint16_T)greenOffence_DW.i -
            (uint8_T)(greenOffence_DW.i / 10U * 10U)) == 0)) {
        /* '<S12>:1:13' */
        /* '<S12>:1:14' */
        greenOffence_DW.k = 1U;
      }
    }

    if (greenOffence_DW.i == 200) {
      /* '<S12>:1:16' */
      /* '<S12>:1:17' */
      greenOffence_DW.i = 1U;
    }

    /* '<S12>:1:19' */
    tmp_2 = greenOffence_DW.i + 1U;
    if (tmp_2 > 255U) {
      tmp_2 = 255U;
    }

    greenOffence_DW.i = (uint8_T)tmp_2;

    /* End of MATLAB Function: '<S4>/MATLAB Function' */

    /* S-Function (arduinodigitaloutput_sfcn): '<S11>/Digital Output' */
    MW_digitalWrite(greenOffence_P.DigitalOutput_pinNumber, rtb_y);

    /* Chart: '<S4>/Player Decisions3' incorporates:
     *  MATLAB Function: '<S3>/Parser'
     */
    /* Gateway: Subsystem/Player Decisions3 */
    if (greenOffence_DW.temporalCounter_i1 < 31U) {
      greenOffence_DW.temporalCounter_i1++;
    }

    /* During: Subsystem/Player Decisions3 */
    if (greenOffence_DW.is_active_c9_greenOffence == 0U) {
      /* Entry: Subsystem/Player Decisions3 */
      greenOffence_DW.is_active_c9_greenOffence = 1U;

      /* Entry Internal: Subsystem/Player Decisions3 */
      /* Transition: '<S19>:97' */
      greenOffence_calcStartPos_k();
      greenOffence_calcStartTeams();
      greenOffence_B.shoot = 0U;
      greenOffence_DW.is_c9_greenOffence = greenOffence_IN_waiting;

      /* Entry Internal 'waiting': '<S19>:88' */
      /* Transition: '<S19>:93' */
      greenOffence_DW.is_waiting = greenOffence_IN_Idle1;
    } else if (greenOffence_DW.is_c9_greenOffence ==
               greenOffenc_IN_GameIsOn_Offence) {
      /* During 'GameIsOn_Offence': '<S19>:17' */
      if (newMessage[0] == 0) {
        /* Transition: '<S19>:91' */
        /* Exit Internal 'GameIsOn_Offence': '<S19>:17' */
        greenOffence_DW.is_GameIsOn_Offence = greenOffence_IN_NO_ACTIVE_CHILD;
        greenOffence_DW.is_c9_greenOffence = greenOffence_IN_waiting;

        /* Entry Internal 'waiting': '<S19>:88' */
        /* Transition: '<S19>:93' */
        greenOffence_DW.is_waiting = greenOffence_IN_Idle1;
      } else {
        greenOffence_B.shoot = 0U;
        switch (greenOffence_DW.is_GameIsOn_Offence) {
         case greenOffence_IN_Aim:
          /* During 'Aim': '<S19>:115' */
          tmp_1 = (int32_T)greenOffence_B.Selector.orientation -
            greenOffence_B.finalWay.orientation;
          if (tmp_1 > 32767L) {
            tmp_1 = 32767L;
          } else {
            if (tmp_1 < -32768L) {
              tmp_1 = -32768L;
            }
          }

          if ((int16_T)tmp_1 < 0) {
            if ((int16_T)tmp_1 <= MIN_int16_T) {
              rtb_targetOrientation = MAX_int16_T;
            } else {
              rtb_targetOrientation = -(int16_T)tmp_1;
            }
          } else {
            rtb_targetOrientation = (int16_T)tmp_1;
          }

          if (rtb_targetOrientation < 30) {
            /* Transition: '<S19>:121' */
            greenOffence_DW.is_GameIsOn_Offence = greenOffence_IN_Kick;
            greenOffence_DW.temporalCounter_i1 = 0U;

            /* Entry 'Kick': '<S19>:118' */
            greenOffence_B.finalWay.x = greenOffence_B.Ball_n.x;
            greenOffence_B.finalWay.y = greenOffence_B.Ball_n.y;
            greenOffence_B.finalWay.orientation = MAX_int16_T;
            greenOffence_B.shoot = 1U;
          } else {
            greenOffence_calcShootWay();
            greenOffence_B.finalWay.x = greenOffence_B.Selector.x;
            greenOffence_B.finalWay.y = greenOffence_B.Selector.y;
            greenOffence_B.finalWay.orientation =
              greenOffence_DW.shootWay.orientation;
          }
          break;

         case greenOffence_IN_GetToTheBall:
          /* During 'GetToTheBall': '<S19>:60' */
          tmp[0] = greenOffence_DW.shootWay.x;
          tmp[1] = greenOffence_DW.shootWay.y;
          if (greenOffence_checkReached_g(tmp, 20.0) == 1) {
            /* Transition: '<S19>:116' */
            greenOffence_DW.is_GameIsOn_Offence = greenOffence_IN_Aim;
          } else {
            greenOffence_calcShootWay();
            greenOffence_B.finalWay.x = greenOffence_DW.shootWay.x;
            greenOffence_B.finalWay.y = greenOffence_DW.shootWay.y;
            greenOffence_B.finalWay.orientation = MAX_int16_T;
          }
          break;

         case greenOffence_IN_Idle_p:
          /* During 'Idle': '<S19>:105' */
          if (greenOffence_DW.temporalCounter_i1 >= 10) {
            /* Transition: '<S19>:120' */
            greenOffence_DW.is_GameIsOn_Offence = greenOffence_IN_GetToTheBall;

            /* Entry 'GetToTheBall': '<S19>:60' */
            greenOffence_calcShootWay();
          } else {
            greenOffence_B.finalWay.x = greenOffence_B.Selector.x;
            greenOffence_B.finalWay.y = greenOffence_B.Selector.y;
            greenOffence_B.finalWay.orientation =
              greenOffence_B.Selector.orientation;
          }
          break;

         default:
          /* During 'Kick': '<S19>:118' */
          if (greenOffence_DW.temporalCounter_i1 >= 30) {
            /* Transition: '<S19>:131' */
            greenOffence_DW.is_GameIsOn_Offence = greenOffence_IN_Idle_p;
            greenOffence_DW.temporalCounter_i1 = 0U;
          }
          break;
        }
      }
    } else {
      /* During 'waiting': '<S19>:88' */
      if (newMessage[0] == 1) {
        /* Transition: '<S19>:90' */
        /* Exit Internal 'waiting': '<S19>:88' */
        greenOffence_DW.is_waiting = greenOffence_IN_NO_ACTIVE_CHILD;
        greenOffence_DW.is_c9_greenOffence = greenOffenc_IN_GameIsOn_Offence;

        /* Entry Internal 'GameIsOn_Offence': '<S19>:17' */
        /* Transition: '<S19>:103' */
        greenOffence_DW.is_GameIsOn_Offence = greenOffence_IN_GetToTheBall;

        /* Entry 'GetToTheBall': '<S19>:60' */
        greenOffence_calcShootWay();
      } else {
        /* During 'Idle1': '<S19>:100' */
        greenOffence_B.finalWay.x = greenOffence_B.Selector.x;
        greenOffence_B.finalWay.y = greenOffence_B.Selector.y;
        greenOffence_B.finalWay.orientation =
          greenOffence_B.Selector.orientation;
      }
    }

    /* End of Chart: '<S4>/Player Decisions3' */

    /* Sum: '<S20>/Add' incorporates:
     *  SignalConversion: '<S20>/ConcatBufferAtVector Concatenate1In1'
     *  SignalConversion: '<S20>/ConcatBufferAtVector Concatenate1In2'
     *  SignalConversion: '<S20>/ConcatBufferAtVector ConcatenateIn1'
     *  SignalConversion: '<S20>/ConcatBufferAtVector ConcatenateIn2'
     */
    rtb_Add_idx_0 = (int8_T)(greenOffence_B.finalWay.x -
      greenOffence_B.Selector.x);
    rtb_Add_idx_1 = (int8_T)(greenOffence_B.finalWay.y -
      greenOffence_B.Selector.y);

    /* DotProduct: '<S21>/Dot Product' */
    tmp_1 = (int32_T)(rtb_Add_idx_0 * rtb_Add_idx_0) + rtb_Add_idx_1 *
      rtb_Add_idx_1;

    /* RelationalOperator: '<S26>/Compare' incorporates:
     *  Constant: '<S26>/Constant'
     */
    Compare = (greenOffence_B.finalWay.orientation !=
               greenOffence_P.CompareToConstant_const);

    /* MATLAB Function: '<S25>/calcTargetOrien' incorporates:
     *  SignalConversion: '<S20>/ConcatBufferAtVector Concatenate1In1'
     *  SignalConversion: '<S20>/ConcatBufferAtVector Concatenate1In2'
     *  SignalConversion: '<S20>/ConcatBufferAtVector ConcatenateIn1'
     *  SignalConversion: '<S20>/ConcatBufferAtVector ConcatenateIn2'
     */
    /* MATLAB Function 'Subsystem/control/decisionParser/calcTargetOrien': '<S27>:1' */
    /* '<S27>:1:2' */
    i = greenOffence_B.finalWay.y - greenOffence_B.Selector.y;
    if (i > 127) {
      i = 127;
    } else {
      if (i < -128) {
        i = -128;
      }
    }

    rtb_Switch1 = greenOffence_B.finalWay.x - greenOffence_B.Selector.x;
    if (rtb_Switch1 > 127) {
      rtb_Switch1 = 127;
    } else {
      if (rtb_Switch1 < -128) {
        rtb_Switch1 = -128;
      }
    }

    y_0 = rt_roundd_snf(57.295779513082323 * rt_atan2d_snf((real_T)i, (real_T)
      rtb_Switch1));
    if (y_0 < 32768.0) {
      if (y_0 >= -32768.0) {
        rtb_Switch1 = (int16_T)y_0;
      } else {
        rtb_Switch1 = MIN_int16_T;
      }
    } else {
      rtb_Switch1 = MAX_int16_T;
    }

    /* MATLAB Function: '<S25>/fwdLimit' incorporates:
     *  Constant: '<S25>/certainty'
     *  MATLAB Function: '<S25>/calcTargetOrien'
     *  Sum: '<S25>/Add1'
     */
    /* MATLAB Function 'Subsystem/control/decisionParser/fwdLimit': '<S28>:1' */
    /* '<S28>:1:2' */
    sigma = (real_T)greenOffence_P.certainty_Value / 255.0;

    /* '<S28>:1:3' */
    t = rt_remd_snf((real_T)(rtb_Switch1 - greenOffence_B.Selector.orientation),
                    360.0);
    i = (int16_T)fabs(t);
    if (i > 180) {
      if (t > 0.0) {
        t -= 360.0;
      } else {
        t += 360.0;
      }

      i = (int16_T)fabs(t);
    }

    if (i <= 45) {
      t *= 0.017453292519943295;
      rtb_Add_idx_0 = 0;
    } else if (i <= 135) {
      if (t > 0.0) {
        t = (t - 90.0) * 0.017453292519943295;
        rtb_Add_idx_0 = 1;
      } else {
        t = (t + 90.0) * 0.017453292519943295;
        rtb_Add_idx_0 = -1;
      }
    } else if (t > 0.0) {
      t = (t - 180.0) * 0.017453292519943295;
      rtb_Add_idx_0 = 2;
    } else {
      t = (t + 180.0) * 0.017453292519943295;
      rtb_Add_idx_0 = -2;
    }

    /* '<S28>:1:4' */
    if (sigma > 0.0) {
      if (rtb_Add_idx_0 == 0) {
        y_0 = cos(t);
      } else if (rtb_Add_idx_0 == 1) {
        y_0 = -sin(t);
      } else if (rtb_Add_idx_0 == -1) {
        y_0 = sin(t);
      } else {
        y_0 = -cos(t);
      }

      t = (1.0 - fabs(y_0)) / sigma;
      y_0 = exp(-0.5 * t * t) / (2.5066282746310002 * sigma);
      t = 0.0 / sigma;
      t = exp(-0.5 * t * t) / (2.5066282746310002 * sigma);
    } else {
      y_0 = (rtNaN);
      t = (rtNaN);
    }

    /* Switch: '<S25>/Switch' incorporates:
     *  Constant: '<S25>/Constant'
     *  MATLAB Function: '<S25>/fwdLimit'
     */
    if (Compare) {
      t = greenOffence_P.Constant_Value;
    } else {
      t = y_0 / t;
    }

    /* End of Switch: '<S25>/Switch' */

    /* Math: '<S21>/Math Function' incorporates:
     *  Constant: '<S21>/Constant'
     *  DotProduct: '<S21>/Dot Product'
     */
    if ((tmp_1 < 0.0) && (greenOffence_P.Constant_Value_n > floor
                          (greenOffence_P.Constant_Value_n))) {
      y_0 = -rt_powd_snf(-(real_T)tmp_1, greenOffence_P.Constant_Value_n);
    } else {
      y_0 = rt_powd_snf((real_T)tmp_1, greenOffence_P.Constant_Value_n);
    }

    /* End of Math: '<S21>/Math Function' */

    /* Bias: '<S20>/Bias' incorporates:
     *  Gain: '<S20>/Gain8'
     */
    y_0 = greenOffence_P.Gain8_Gain * y_0 + greenOffence_P.Bias_Bias;

    /* Saturate: '<S20>/Saturation' */
    if (y_0 > greenOffence_P.Saturation_UpperSat) {
      y_0 = greenOffence_P.Saturation_UpperSat;
    } else {
      if (y_0 < greenOffence_P.Saturation_LowerSat) {
        y_0 = greenOffence_P.Saturation_LowerSat;
      }
    }

    /* Product: '<S20>/Product3' incorporates:
     *  Saturate: '<S20>/Saturation'
     */
    rtb_Add4 = (real32_T)(y_0 * t);

    /* Switch: '<S25>/Switch1' */
    if (Compare) {
      rtb_Switch1 = greenOffence_B.finalWay.orientation;
    }

    /* End of Switch: '<S25>/Switch1' */

    /* MATLAB Function: '<S20>/calc_dO' */
    /* MATLAB Function 'Subsystem/control/calc_dO': '<S22>:1' */
    /* '<S22>:1:2' */
    tmp_1 = rtb_Switch1 + 360L;
    if (tmp_1 > 32767L) {
      tmp_1 = 32767L;
    }

    T = (int16_T)tmp_1 - div_s16_floor((int16_T)tmp_1, 360) * 360;

    /* '<S22>:1:3' */
    tmp_1 = greenOffence_B.Selector.orientation + 360L;
    if (tmp_1 > 32767L) {
      tmp_1 = 32767L;
    }

    M = (int16_T)tmp_1 - div_s16_floor((int16_T)tmp_1, 360) * 360;

    /* '<S22>:1:4' */
    tmp_1 = (int32_T)T - M;
    if (tmp_1 > 32767L) {
      tmp_1 = 32767L;
    } else {
      if (tmp_1 < -32768L) {
        tmp_1 = -32768L;
      }
    }

    tmp_0 = (int32_T)rtb_Switch1 - greenOffence_B.Selector.orientation;
    if (tmp_0 > 32767L) {
      tmp_0 = 32767L;
    } else {
      if (tmp_0 < -32768L) {
        tmp_0 = -32768L;
      }
    }

    if ((int16_T)tmp_1 < 0) {
      if ((int16_T)tmp_1 <= MIN_int16_T) {
        rtb_targetOrientation = MAX_int16_T;
      } else {
        rtb_targetOrientation = -(int16_T)tmp_1;
      }
    } else {
      rtb_targetOrientation = (int16_T)tmp_1;
    }

    if ((int16_T)tmp_0 < 0) {
      if ((int16_T)tmp_0 <= MIN_int16_T) {
        i = MAX_int16_T;
      } else {
        i = -(int16_T)tmp_0;
      }
    } else {
      i = (int16_T)tmp_0;
    }

    if (rtb_targetOrientation <= i) {
      i = rtb_targetOrientation;
    }

    tmp_1 = (int32_T)T - M;
    if (tmp_1 > 32767L) {
      tmp_1 = 32767L;
    } else {
      if (tmp_1 < -32768L) {
        tmp_1 = -32768L;
      }
    }

    tmp_0 = (int32_T)rtb_Switch1 - greenOffence_B.Selector.orientation;
    if (tmp_0 > 32767L) {
      tmp_0 = 32767L;
    } else {
      if (tmp_0 < -32768L) {
        tmp_0 = -32768L;
      }
    }

    if ((int16_T)tmp_1 < 0) {
      if ((int16_T)tmp_1 <= MIN_int16_T) {
        rtb_targetOrientation = MAX_int16_T;
      } else {
        rtb_targetOrientation = -(int16_T)tmp_1;
      }
    } else {
      rtb_targetOrientation = (int16_T)tmp_1;
    }

    if ((int16_T)tmp_0 < 0) {
      if ((int16_T)tmp_0 <= MIN_int16_T) {
        absx = MAX_int16_T;
      } else {
        absx = -(int16_T)tmp_0;
      }
    } else {
      absx = (int16_T)tmp_0;
    }

    if (rtb_targetOrientation < absx) {
      /* '<S22>:1:5' */
      /* '<S22>:1:6' */
      tmp_1 = (int32_T)T - M;
      if (tmp_1 > 32767L) {
        tmp_1 = 32767L;
      } else {
        if (tmp_1 < -32768L) {
          tmp_1 = -32768L;
        }
      }

      rtb_targetOrientation = (int16_T)tmp_1;
      if ((int16_T)tmp_1 > 0) {
        rtb_targetOrientation = 1;
      } else {
        if ((int16_T)tmp_1 < 0) {
          rtb_targetOrientation = -1;
        }
      }
    } else {
      /* '<S22>:1:8' */
      tmp_1 = (int32_T)rtb_Switch1 - greenOffence_B.Selector.orientation;
      if (tmp_1 > 32767L) {
        tmp_1 = 32767L;
      } else {
        if (tmp_1 < -32768L) {
          tmp_1 = -32768L;
        }
      }

      rtb_targetOrientation = (int16_T)tmp_1;
      if ((int16_T)tmp_1 > 0) {
        rtb_targetOrientation = 1;
      } else {
        if ((int16_T)tmp_1 < 0) {
          rtb_targetOrientation = -1;
        }
      }
    }

    if (i < 3) {
      /* '<S22>:1:10' */
      /* '<S22>:1:11' */
      i = 0;
    }

    /* Product: '<S20>/Product1' incorporates:
     *  Bias: '<S20>/Bias1'
     *  Bias: '<S20>/Bias2'
     *  Gain: '<S20>/Gain7'
     *  Gain: '<S20>/Gain9'
     *  MATLAB Function: '<S20>/calc_dO'
     */
    rtb_Saturation1 = ((real32_T)greenOffence_P.Gain7_Gain * 9.53674316E-7F *
                       (real32_T)i + greenOffence_P.Bias1_Bias) * ((real32_T)
      (greenOffence_P.Gain9_Gain * t) + greenOffence_P.Bias2_Bias);

    /* Saturate: '<S20>/Saturation1' */
    if (rtb_Saturation1 > greenOffence_P.Saturation1_UpperSat) {
      rtb_Saturation1 = greenOffence_P.Saturation1_UpperSat;
    } else {
      if (rtb_Saturation1 < greenOffence_P.Saturation1_LowerSat) {
        rtb_Saturation1 = greenOffence_P.Saturation1_LowerSat;
      }
    }

    /* End of Saturate: '<S20>/Saturation1' */

    /* Sum: '<S20>/Add1' incorporates:
     *  Gain: '<S20>/Gain3'
     *  MATLAB Function: '<S20>/calc_dO'
     *  Product: '<S20>/Product4'
     */
    rtb_SaturationOrien3 = (real32_T)greenOffence_P.Gain3_Gain * 3.05175781E-5F *
      (real32_T)rtb_targetOrientation * rtb_Saturation1 + rtb_Add4;

    /* Saturate: '<S20>/SaturationOrien3' */
    if (rtb_SaturationOrien3 > greenOffence_P.SaturationOrien3_UpperSat) {
      rtb_SaturationOrien3 = greenOffence_P.SaturationOrien3_UpperSat;
    } else {
      if (rtb_SaturationOrien3 < greenOffence_P.SaturationOrien3_LowerSat) {
        rtb_SaturationOrien3 = greenOffence_P.SaturationOrien3_LowerSat;
      }
    }

    /* End of Saturate: '<S20>/SaturationOrien3' */

    /* Sum: '<S20>/Add4' incorporates:
     *  MATLAB Function: '<S20>/calc_dO'
     *  Product: '<S20>/Product5'
     */
    rtb_Add4 += rtb_Saturation1 * (real32_T)rtb_targetOrientation;

    /* Saturate: '<S20>/SaturationOrien4' */
    if (rtb_Add4 > greenOffence_P.SaturationOrien4_UpperSat) {
      /* MATLAB Function: '<S4>/MATLAB Function1' */
      rtb_Add4 = greenOffence_P.SaturationOrien4_UpperSat;
    } else {
      if (rtb_Add4 < greenOffence_P.SaturationOrien4_LowerSat) {
        /* MATLAB Function: '<S4>/MATLAB Function1' */
        rtb_Add4 = greenOffence_P.SaturationOrien4_LowerSat;
      }
    }

    /* End of Saturate: '<S20>/SaturationOrien4' */

    /* MATLAB Function: '<S4>/MATLAB Function1' */
    /* MATLAB Function 'Subsystem/MATLAB Function1': '<S13>:1' */
    /* '<S13>:1:2' */
    rtb_targetOrientation = 0;

    /*  shoot=uint8(0); */
    if (greenOffence_DW.itteration - (real32_T)floor(greenOffence_DW.itteration /
         4.0F) * 4.0F == 0.0F) {
      /* '<S13>:1:17' */
      /* '<S13>:1:18' */
      rtb_targetOrientation = 200;
    }

    if (greenOffence_B.shoot == 1) {
      /* '<S13>:1:21' */
      /* '<S13>:1:22' */
      rtb_SaturationOrien3 = 1.0F;

      /* '<S13>:1:23' */
      rtb_Add4 = 1.0F;

      /* '<S13>:1:24' */
      rtb_targetOrientation = 200;
    }

    if (rtb_SaturationOrien3 < 0.0F) {
      rtb_Saturation1 = -1.0F;
    } else if (rtb_SaturationOrien3 > 0.0F) {
      rtb_Saturation1 = 1.0F;
    } else if (rtb_SaturationOrien3 == 0.0F) {
      rtb_Saturation1 = 0.0F;
    } else {
      rtb_Saturation1 = rtb_SaturationOrien3;
    }

    if (rtb_Saturation1 > 0.0F) {
      /* '<S13>:1:27' */
      /* '<S13>:1:28' */
      rtb_Saturation1 = rt_roundf_snf((real32_T)fabs(rtb_SaturationOrien3) *
        (real32_T)rtb_targetOrientation);
      if (rtb_Saturation1 < 256.0F) {
        rtb_FWD_A = (uint8_T)rtb_Saturation1;
      } else {
        rtb_FWD_A = MAX_uint8_T;
      }

      /* '<S13>:1:29' */
      rtb_y = 0U;
    } else {
      /* '<S13>:1:31' */
      rtb_Saturation1 = rt_roundf_snf((real32_T)fabs(rtb_SaturationOrien3) *
        (real32_T)rtb_targetOrientation);
      if (rtb_Saturation1 < 256.0F) {
        rtb_y = (uint8_T)rtb_Saturation1;
      } else {
        rtb_y = MAX_uint8_T;
      }

      /* '<S13>:1:32' */
      rtb_FWD_A = 0U;
    }

    if (rtb_Add4 < 0.0F) {
      rtb_Saturation1 = -1.0F;
    } else if (rtb_Add4 > 0.0F) {
      rtb_Saturation1 = 1.0F;
    } else if (rtb_Add4 == 0.0F) {
      rtb_Saturation1 = 0.0F;
    } else {
      rtb_Saturation1 = rtb_Add4;
    }

    if (rtb_Saturation1 > 0.0F) {
      /* '<S13>:1:34' */
      /* '<S13>:1:35' */
      rtb_Saturation1 = rt_roundf_snf((real32_T)fabs(rtb_Add4) * (real32_T)
        rtb_targetOrientation);
      if (rtb_Saturation1 < 256.0F) {
        rtb_FWD_B = (uint8_T)rtb_Saturation1;
      } else {
        rtb_FWD_B = MAX_uint8_T;
      }

      /* '<S13>:1:36' */
      rtb_BK_B = 0U;
    } else {
      /* '<S13>:1:38' */
      rtb_Saturation1 = rt_roundf_snf((real32_T)fabs(rtb_Add4) * (real32_T)
        rtb_targetOrientation);
      if (rtb_Saturation1 < 256.0F) {
        rtb_BK_B = (uint8_T)rtb_Saturation1;
      } else {
        rtb_BK_B = MAX_uint8_T;
      }

      /* '<S13>:1:39' */
      rtb_FWD_B = 0U;
    }

    if ((real32_T)fabs(rtb_SaturationOrien3) < 0.05) {
      /* '<S13>:1:42' */
      /* '<S13>:1:43' */
      rtb_FWD_A = 0U;

      /* '<S13>:1:44' */
      rtb_y = 0U;
    }

    if ((real32_T)fabs(rtb_Add4) < 0.05) {
      /* '<S13>:1:46' */
      /* '<S13>:1:47' */
      rtb_FWD_B = 0U;

      /* '<S13>:1:48' */
      rtb_BK_B = 0U;
    }

    /* '<S13>:1:51' */
    /* '<S13>:1:52' */
    if (greenOffence_DW.itteration - (real32_T)floor(greenOffence_DW.itteration /
         16.0F) * 16.0F == 0.0F) {
      /* '<S13>:1:54' */
      /* '<S13>:1:55' */
      greenOffence_DW.itteration = 0.0F;
    }

    /* '<S13>:1:57' */
    greenOffence_DW.itteration++;

    /* S-Function (arduinoanalogoutput_sfcn): '<S14>/PWM' */
    MW_analogWrite(greenOffence_P.PWM_pinNumber, rtb_BK_B);

    /* S-Function (arduinoanalogoutput_sfcn): '<S15>/PWM' */
    MW_analogWrite(greenOffence_P.PWM_pinNumber_k, rtb_FWD_B);

    /* S-Function (arduinoanalogoutput_sfcn): '<S16>/PWM' */
    MW_analogWrite(greenOffence_P.PWM_pinNumber_k5, rtb_FWD_A);

    /* S-Function (arduinoanalogoutput_sfcn): '<S17>/PWM' */
    MW_analogWrite(greenOffence_P.PWM_pinNumber_i, rtb_y);

    /* Chart: '<S4>/Player Decisions1' */
    /* Gateway: Subsystem/Player Decisions1 */
    /* During: Subsystem/Player Decisions1 */
    if (greenOffence_DW.is_active_c11_greenOffence == 0U) {
      /* Entry: Subsystem/Player Decisions1 */
      greenOffence_DW.is_active_c11_greenOffence = 1U;

      /* Entry Internal: Subsystem/Player Decisions1 */
      /* Transition: '<S18>:97' */
      greenOffence_DW.is_c11_greenOffence = greenOffence_IN_waiting;

      /* Entry Internal 'waiting': '<S18>:88' */
      /* Transition: '<S18>:93' */
      greenOffence_DW.is_waiting_g = greenOffence_IN_Idle1;

      /* Entry 'Idle1': '<S18>:100' */
    } else if (greenOffence_DW.is_c11_greenOffence ==
               greenOffence_IN_GameIsOn_Goalie) {
      /* During 'GameIsOn_Goalie': '<S18>:17' */
      /* Transition: '<S18>:91' */
      /* Exit Internal 'GameIsOn_Goalie': '<S18>:17' */
      greenOffence_DW.is_GameIsOn_Goalie = greenOffence_IN_NO_ACTIVE_CHILD;
      greenOffence_DW.is_c11_greenOffence = greenOffence_IN_waiting;

      /* Entry Internal 'waiting': '<S18>:88' */
      /* Transition: '<S18>:93' */
      greenOffence_DW.is_waiting_g = greenOffence_IN_Idle1;

      /* Entry 'Idle1': '<S18>:100' */
    } else {
      /* During 'waiting': '<S18>:88' */
      /* During 'Idle1': '<S18>:100' */
      /* finalWay.x=int8(0); */
      /* finalWay.y=int8(-40); */
      /* finalWay.orientation=int16(30); */
    }

    /* End of Chart: '<S4>/Player Decisions1' */
  }

  /* End of Outputs for SubSystem: '<Root>/Subsystem' */

  /* Outputs for Enabled SubSystem: '<S3>/LED1' incorporates:
   *  EnablePort: '<S6>/Enable'
   */
  if (on > 0) {
    /* S-Function (sfFASTLED): '<S6>/S-Function Builder' */
    sfFASTLED_Outputs_wrapper( );
  }

  /* End of Outputs for SubSystem: '<S3>/LED1' */
}

/* Model update function */
void greenOffence_update(void)
{
  /* (no update code required) */
}

/* Model initialize function */
void greenOffence_initialize(void)
{
  /* Registration code */

  /* initialize non-finites */
  rt_InitInfAndNaN(sizeof(real_T));

  /* initialize error status */
  rtmSetErrorStatus(greenOffence_M, (NULL));

  /* block I/O */
  (void) memset(((void *) &greenOffence_B), 0,
                sizeof(B_greenOffence_T));

  /* states (dwork) */
  (void) memset((void *)&greenOffence_DW, 0,
                sizeof(DW_greenOffence_T));

  {
    int16_T i;

    /* Start for Enabled SubSystem: '<Root>/Subsystem' */
    /* Start for S-Function (arduinodigitaloutput_sfcn): '<S11>/Digital Output' */
    MW_pinModeOutput(greenOffence_P.DigitalOutput_pinNumber);

    /* Start for S-Function (arduinoanalogoutput_sfcn): '<S14>/PWM' */
    MW_pinModeOutput(greenOffence_P.PWM_pinNumber);

    /* Start for S-Function (arduinoanalogoutput_sfcn): '<S15>/PWM' */
    MW_pinModeOutput(greenOffence_P.PWM_pinNumber_k);

    /* Start for S-Function (arduinoanalogoutput_sfcn): '<S16>/PWM' */
    MW_pinModeOutput(greenOffence_P.PWM_pinNumber_k5);

    /* Start for S-Function (arduinoanalogoutput_sfcn): '<S17>/PWM' */
    MW_pinModeOutput(greenOffence_P.PWM_pinNumber_i);

    /* End of Start for SubSystem: '<Root>/Subsystem' */

    /* InitializeConditions for Enabled SubSystem: '<Root>/Subsystem' */
    /* InitializeConditions for MATLAB Function: '<S4>/MATLAB Function' */
    greenOffence_DW.k = 1U;
    greenOffence_DW.i = 1U;

    /* InitializeConditions for Chart: '<S4>/Player Decisions3' */
    greenOffence_DW.is_GameIsOn_Offence = greenOffence_IN_NO_ACTIVE_CHILD;
    greenOffence_DW.temporalCounter_i1 = 0U;
    greenOffence_DW.is_waiting = greenOffence_IN_NO_ACTIVE_CHILD;
    greenOffence_DW.is_active_c9_greenOffence = 0U;
    greenOffence_DW.is_c9_greenOffence = greenOffence_IN_NO_ACTIVE_CHILD;

    /* InitializeConditions for MATLAB Function: '<S4>/MATLAB Function1' */
    greenOffence_DW.itteration = 0.0F;

    /* InitializeConditions for Chart: '<S4>/Player Decisions1' */
    greenOffence_DW.is_GameIsOn_Goalie = greenOffence_IN_NO_ACTIVE_CHILD;
    greenOffence_DW.is_waiting_g = greenOffence_IN_NO_ACTIVE_CHILD;
    greenOffence_DW.is_active_c11_greenOffence = 0U;
    greenOffence_DW.is_c11_greenOffence = greenOffence_IN_NO_ACTIVE_CHILD;

    /* End of InitializeConditions for SubSystem: '<Root>/Subsystem' */

    /* InitializeConditions for MATLAB Function: '<S3>/MATLAB Function1' */
    greenOffence_DW.once = 1U;

    /* InitializeConditions for MATLAB Function: '<S3>/Parser' */
    for (i = 0; i < 27; i++) {
      greenOffence_DW.oldMes[i] = 0U;
    }

    /* End of InitializeConditions for MATLAB Function: '<S3>/Parser' */
  }
}

/* Model terminate function */
void greenOffence_terminate(void)
{
  /* (no terminate code required) */
}

/*
 * File trailer for generated code.
 *
 * [EOF]
 */

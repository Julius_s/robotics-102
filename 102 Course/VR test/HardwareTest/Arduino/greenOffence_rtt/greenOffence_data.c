/*
 * File: greenOffence_data.c
 *
 * Code generated for Simulink model 'greenOffence'.
 *
 * Model version                  : 1.193
 * Simulink Coder version         : 8.7 (R2014b) 08-Sep-2014
 * TLC version                    : 8.7 (Aug  5 2014)
 * C/C++ source code generated on : Wed Mar 18 15:21:09 2015
 *
 * Target selection: realtime.tlc
 * Embedded hardware selection: Atmel->AVR
 * Code generation objectives: Unspecified
 * Validation result: Not run
 */

#include "greenOffence.h"
#include "greenOffence_private.h"

/* Block parameters (auto storage) */
P_greenOffence_T greenOffence_P = {
  13U,                                 /* Mask Parameter: DigitalOutput_pinNumber
                                        * Referenced by: '<S11>/Digital Output'
                                        */
  5U,                                  /* Mask Parameter: PWM_pinNumber
                                        * Referenced by: '<S14>/PWM'
                                        */
  6U,                                  /* Mask Parameter: PWM_pinNumber_k
                                        * Referenced by: '<S15>/PWM'
                                        */
  11U,                                 /* Mask Parameter: PWM_pinNumber_k5
                                        * Referenced by: '<S16>/PWM'
                                        */
  10U,                                 /* Mask Parameter: PWM_pinNumber_i
                                        * Referenced by: '<S17>/PWM'
                                        */
  32767,                               /* Mask Parameter: CompareToConstant_const
                                        * Referenced by: '<S26>/Constant'
                                        */
  0.0,                                 /* Expression: 0
                                        * Referenced by: '<S25>/Constant'
                                        */
  0.5,                                 /* Expression: 1/2
                                        * Referenced by: '<S21>/Constant'
                                        */
  0.0125,                              /* Expression: 1/80
                                        * Referenced by: '<S20>/Gain8'
                                        */
  0.15,                                /* Expression: 0.15
                                        * Referenced by: '<S20>/Bias'
                                        */
  0.5,                                 /* Expression: 0.5
                                        * Referenced by: '<S20>/Saturation'
                                        */
  0.0,                                 /* Expression: 0
                                        * Referenced by: '<S20>/Saturation'
                                        */
  -1.0,                                /* Expression: -1
                                        * Referenced by: '<S20>/Gain9'
                                        */
  1.0F,                                /* Computed Parameter: Bias2_Bias
                                        * Referenced by: '<S20>/Bias2'
                                        */
  0.1F,                                /* Computed Parameter: Bias1_Bias
                                        * Referenced by: '<S20>/Bias1'
                                        */
  0.5F,                                /* Computed Parameter: Saturation1_UpperSat
                                        * Referenced by: '<S20>/Saturation1'
                                        */
  0.0F,                                /* Computed Parameter: Saturation1_LowerSat
                                        * Referenced by: '<S20>/Saturation1'
                                        */
  1.0F,                                /* Computed Parameter: SaturationOrien3_UpperSat
                                        * Referenced by: '<S20>/SaturationOrien3'
                                        */
  -1.0F,                               /* Computed Parameter: SaturationOrien3_LowerSat
                                        * Referenced by: '<S20>/SaturationOrien3'
                                        */
  1.0F,                                /* Computed Parameter: SaturationOrien4_UpperSat
                                        * Referenced by: '<S20>/SaturationOrien4'
                                        */
  -1.0F,                               /* Computed Parameter: SaturationOrien4_LowerSat
                                        * Referenced by: '<S20>/SaturationOrien4'
                                        */

  /*  Computed Parameter: ManualConst_Value
   * Referenced by: '<Root>/ManualConst'
   */
  { 0.0F, -50.0F, 32767.0F },
  20972,                               /* Computed Parameter: Gain7_Gain
                                        * Referenced by: '<S20>/Gain7'
                                        */
  -32768,                              /* Computed Parameter: Gain3_Gain
                                        * Referenced by: '<S20>/Gain3'
                                        */
  80U,                                 /* Computed Parameter: certainty_Value
                                        * Referenced by: '<S25>/certainty'
                                        */
  0U,                                  /* Computed Parameter: Constant_Value_i
                                        * Referenced by: '<S5>/Constant'
                                        */
  3U                                   /* Computed Parameter: Constant3_Value
                                        * Referenced by: '<Root>/Constant3'
                                        */
};

/*
 * File trailer for generated code.
 *
 * [EOF]
 */

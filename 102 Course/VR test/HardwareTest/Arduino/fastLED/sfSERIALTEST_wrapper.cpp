

/*
 * Include Files
 *
 */
#if defined(MATLAB_MEX_FILE)
#include "tmwtypes.h"
#include "simstruc_types.h"
#else
#include "rtwtypes.h"
#endif

/* %%%-SFUNWIZ_wrapper_includes_Changes_BEGIN --- EDIT HERE TO _END */
#ifndef MATLAB_MEX_FILE
#define ARDUINO 100
#include <Arduino.h>



#endif
/* %%%-SFUNWIZ_wrapper_includes_Changes_END --- EDIT HERE TO _BEGIN */
#define u_width 
#define y_width 1
/*
 * Create external references here.  
 *
 */
/* %%%-SFUNWIZ_wrapper_externs_Changes_BEGIN --- EDIT HERE TO _END */
/* extern double func(double a); */
/* %%%-SFUNWIZ_wrapper_externs_Changes_END --- EDIT HERE TO _BEGIN */

/*
 * Output functions
 *
 */
extern "C" void sfSERIALTEST_Outputs_wrapper(uint8_T *message)
{
/* %%%-SFUNWIZ_wrapper_Outputs_Changes_BEGIN --- EDIT HERE TO _END */
/* This sample sets the output equal to the input
      y0[0] = u0[0]; 
 For complex signals use: y0[0].re = u0[0].re; 
      y0[0].im = u0[0].im;
      y1[0].re = u1[0].re;
      y1[0].im = u1[0].im;
*/
#ifndef MATLAB_MEX_FILE
   Serial.write( "heartBeat\n");  
for (int i = 0; i < 27; i++){
              message[i] = (uint8_t)0;              
            } 

  while (Serial.available() > 0) {   
    uint8_t inByte = Serial.read();
    if(inByte=='h'){
      while (Serial.available() == 0);
      uint8_t inByte = Serial.read();
      if(inByte=='e'){
        while (Serial.available() == 0);
        uint8_t inByte = Serial.read();
        if(inByte=='a'){
          while (Serial.available() == 0);
          uint8_t inByte = Serial.read();
          if(inByte=='d'){
            while (Serial.available() < 27);          
            for (int i = 0; i < 27; i++){
              message[i] = (uint8_t) Serial.read(); 
            } 
          }
        }
      }
    }
  }
 for (int i = 0; i < 27; i++){
             Serial.write( (uint8_t) message[i]);             
            } 

#endif
/* %%%-SFUNWIZ_wrapper_Outputs_Changes_END --- EDIT HERE TO _BEGIN */
}

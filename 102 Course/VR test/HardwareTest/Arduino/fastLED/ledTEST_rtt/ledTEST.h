/*
 * File: ledTEST.h
 *
 * Code generated for Simulink model 'ledTEST'.
 *
 * Model version                  : 1.20
 * Simulink Coder version         : 8.7 (R2014b) 08-Sep-2014
 * TLC version                    : 8.7 (Aug  5 2014)
 * C/C++ source code generated on : Wed Mar 04 18:34:22 2015
 *
 * Target selection: realtime.tlc
 * Embedded hardware selection: Atmel->AVR
 * Code generation objectives: Unspecified
 * Validation result: Not run
 */

#ifndef RTW_HEADER_ledTEST_h_
#define RTW_HEADER_ledTEST_h_
#include <stddef.h>
#include <string.h>
#ifndef ledTEST_COMMON_INCLUDES_
# define ledTEST_COMMON_INCLUDES_
#include "rtwtypes.h"
#include "rtw_continuous.h"
#include "rtw_solver.h"
#endif                                 /* ledTEST_COMMON_INCLUDES_ */

#include "ledTEST_types.h"

/* Macros for accessing real-time model data structure */
#ifndef rtmGetErrorStatus
# define rtmGetErrorStatus(rtm)        ((rtm)->errorStatus)
#endif

#ifndef rtmSetErrorStatus
# define rtmSetErrorStatus(rtm, val)   ((rtm)->errorStatus = (val))
#endif

/* Block signals (auto storage) */
typedef struct {
  uint8_T SFunctionBuilder1[27];       /* '<S4>/S-Function Builder1' */
} B_ledTEST_T;

/* Block states (auto storage) for system '<Root>' */
typedef struct {
  uint8_T once;                        /* '<Root>/MATLAB Function2' */
} DW_ledTEST_T;

/* Parameters (auto storage) */
struct P_ledTEST_T_ {
  uint8_T Constant_Value;              /* Computed Parameter: Constant_Value
                                        * Referenced by: '<S1>/Constant'
                                        */
};

/* Real-time Model Data Structure */
struct tag_RTM_ledTEST_T {
  const char_T * volatile errorStatus;
};

/* Block parameters (auto storage) */
extern P_ledTEST_T ledTEST_P;

/* Block signals (auto storage) */
extern B_ledTEST_T ledTEST_B;

/* Block states (auto storage) */
extern DW_ledTEST_T ledTEST_DW;

/* Model entry point functions */
extern void ledTEST_initialize(void);
extern void ledTEST_output(void);
extern void ledTEST_update(void);
extern void ledTEST_terminate(void);

/* Real-time Model object */
extern RT_MODEL_ledTEST_T *const ledTEST_M;

/*-
 * The generated code includes comments that allow you to trace directly
 * back to the appropriate location in the model.  The basic format
 * is <system>/block_name, where system is the system number (uniquely
 * assigned by Simulink) and block_name is the name of the block.
 *
 * Use the MATLAB hilite_system command to trace the generated code back
 * to the model.  For example,
 *
 * hilite_system('<S3>')    - opens system 3
 * hilite_system('<S3>/Kp') - opens and selects block Kp which resides in S3
 *
 * Here is the system hierarchy for this model
 *
 * '<Root>' : 'ledTEST'
 * '<S1>'   : 'ledTEST/Compare To Zero'
 * '<S2>'   : 'ledTEST/LED'
 * '<S3>'   : 'ledTEST/MATLAB Function2'
 * '<S4>'   : 'ledTEST/serial'
 */
#endif                                 /* RTW_HEADER_ledTEST_h_ */

/*
 * File trailer for generated code.
 *
 * [EOF]
 */

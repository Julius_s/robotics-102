/*
 * File: ledTEST_types.h
 *
 * Code generated for Simulink model 'ledTEST'.
 *
 * Model version                  : 1.20
 * Simulink Coder version         : 8.7 (R2014b) 08-Sep-2014
 * TLC version                    : 8.7 (Aug  5 2014)
 * C/C++ source code generated on : Wed Mar 04 18:34:22 2015
 *
 * Target selection: realtime.tlc
 * Embedded hardware selection: Atmel->AVR
 * Code generation objectives: Unspecified
 * Validation result: Not run
 */

#ifndef RTW_HEADER_ledTEST_types_h_
#define RTW_HEADER_ledTEST_types_h_
#include "rtwtypes.h"

/* Parameters (auto storage) */
typedef struct P_ledTEST_T_ P_ledTEST_T;

/* Forward declaration for rtModel */
typedef struct tag_RTM_ledTEST_T RT_MODEL_ledTEST_T;

#endif                                 /* RTW_HEADER_ledTEST_types_h_ */

/*
 * File trailer for generated code.
 *
 * [EOF]
 */

/*
 * File: ledTEST.c
 *
 * Code generated for Simulink model 'ledTEST'.
 *
 * Model version                  : 1.20
 * Simulink Coder version         : 8.7 (R2014b) 08-Sep-2014
 * TLC version                    : 8.7 (Aug  5 2014)
 * C/C++ source code generated on : Wed Mar 04 18:34:22 2015
 *
 * Target selection: realtime.tlc
 * Embedded hardware selection: Atmel->AVR
 * Code generation objectives: Unspecified
 * Validation result: Not run
 */

#include "ledTEST.h"
#include "ledTEST_private.h"

/* Block signals (auto storage) */
B_ledTEST_T ledTEST_B;

/* Block states (auto storage) */
DW_ledTEST_T ledTEST_DW;

/* Real-time model */
RT_MODEL_ledTEST_T ledTEST_M_;
RT_MODEL_ledTEST_T *const ledTEST_M = &ledTEST_M_;

/* Model output function */
void ledTEST_output(void)
{
  uint8_T on;

  /* MATLAB Function: '<Root>/MATLAB Function2' */
  /* MATLAB Function 'MATLAB Function2': '<S3>:1' */
  /* '<S3>:1:7' */
  on = ledTEST_DW.once;
  if (ledTEST_DW.once == 1) {
    /* '<S3>:1:8' */
    /* '<S3>:1:9' */
    ledTEST_DW.once = 0U;
  }

  /* End of MATLAB Function: '<Root>/MATLAB Function2' */

  /* Outputs for Enabled SubSystem: '<Root>/LED' incorporates:
   *  EnablePort: '<S2>/Enable'
   */
  if (on > 0) {
    /* S-Function (sfFASTLED): '<S2>/S-Function Builder' */
    sfFASTLED_Outputs_wrapper( );
  }

  /* End of Outputs for SubSystem: '<Root>/LED' */

  /* Outputs for Enabled SubSystem: '<Root>/serial' incorporates:
   *  EnablePort: '<S4>/Enable'
   */
  /* RelationalOperator: '<S1>/Compare' incorporates:
   *  Constant: '<S1>/Constant'
   */
  if (on == ledTEST_P.Constant_Value) {
    /* S-Function (sfSERIALTEST): '<S4>/S-Function Builder1' */
    sfSERIALTEST_Outputs_wrapper( &ledTEST_B.SFunctionBuilder1[0] );
  }

  /* End of RelationalOperator: '<S1>/Compare' */
  /* End of Outputs for SubSystem: '<Root>/serial' */
}

/* Model update function */
void ledTEST_update(void)
{
  /* (no update code required) */
}

/* Model initialize function */
void ledTEST_initialize(void)
{
  /* Registration code */

  /* initialize error status */
  rtmSetErrorStatus(ledTEST_M, (NULL));

  /* block I/O */
  (void) memset(((void *) &ledTEST_B), 0,
                sizeof(B_ledTEST_T));

  /* states (dwork) */
  (void) memset((void *)&ledTEST_DW, 0,
                sizeof(DW_ledTEST_T));

  /* InitializeConditions for MATLAB Function: '<Root>/MATLAB Function2' */
  ledTEST_DW.once = 1U;
}

/* Model terminate function */
void ledTEST_terminate(void)
{
  /* (no terminate code required) */
}

/*
 * File trailer for generated code.
 *
 * [EOF]
 */

/*
 * File: ImageDet_data.c
 *
 * Code generated for Simulink model 'ImageDet'.
 *
 * Model version                  : 1.191
 * Simulink Coder version         : 8.7 (R2014b) 08-Sep-2014
 * TLC version                    : 8.7 (Aug  5 2014)
 * C/C++ source code generated on : Thu Mar 05 19:35:17 2015
 *
 * Target selection: realtime.tlc
 * Embedded hardware selection: ARM Compatible->ARM Cortex
 * Code generation objectives: Unspecified
 * Validation result: Not run
 */

#include "ImageDet.h"
#include "ImageDet_private.h"

/* Block parameters (auto storage) */
P_ImageDet_T ImageDet_P = {
  10U,                                 /* Mask Parameter: BlobAnalysis2_minArea
                                        * Referenced by: '<S3>/Blob Analysis2'
                                        */
  0.0,                                 /* Expression: 0
                                        * Referenced by: '<S3>/Constant9'
                                        */
  1.0,                                 /* Expression: 1
                                        * Referenced by: '<S3>/Constant8'
                                        */
  0.0,                                 /* Expression: 0
                                        * Referenced by: '<S3>/Constant2'
                                        */
  1.0,                                 /* Expression: 1
                                        * Referenced by: '<S3>/Constant1'
                                        */
  0.0,                                 /* Expression: 0
                                        * Referenced by: '<S2>/Constant1'
                                        */
  1.0,                                 /* Expression: 1
                                        * Referenced by: '<S2>/Constant'
                                        */

  /*  Expression: [320 240]
   * Referenced by: '<S1>/Constant6'
   */
  { 320.0, 240.0 },
  1.91,                                /* Expression: 1.91
                                        * Referenced by: '<S1>/Constant7'
                                        */
  0.00392156886F,                      /* Computed Parameter: Gain1_Gain
                                        * Referenced by: '<S3>/Gain1'
                                        */
  0.00392156886F,                      /* Computed Parameter: Gain2_Gain
                                        * Referenced by: '<S3>/Gain2'
                                        */
  0.00392156886F,                      /* Computed Parameter: Gain3_Gain
                                        * Referenced by: '<S3>/Gain3'
                                        */
  -1,                                  /* Computed Parameter: DataStoreMemory_InitialValue
                                        * Referenced by: '<S16>/Data Store Memory'
                                        */
  0U,                                  /* Computed Parameter: Constant1_Value_d
                                        * Referenced by: '<Root>/Constant1'
                                        */
  1U,                                  /* Computed Parameter: Constant_Value_a
                                        * Referenced by: '<Root>/Constant'
                                        */
  0U,                                  /* Computed Parameter: Constant_Value_e
                                        * Referenced by: '<S8>/Constant'
                                        */
  0U,                                  /* Computed Parameter: ManualSwitch_CurrentSetting
                                        * Referenced by: '<Root>/Manual Switch'
                                        */
  1U,                                  /* Computed Parameter: ManualSwitch_CurrentSetting_e
                                        * Referenced by: '<S2>/Manual Switch'
                                        */
  0U,                                  /* Computed Parameter: ManualSwitch1_CurrentSetting
                                        * Referenced by: '<S3>/Manual Switch1'
                                        */
  0U,                                  /* Computed Parameter: ManualSwitch2_CurrentSetting
                                        * Referenced by: '<S3>/Manual Switch2'
                                        */

  /*  Computed Parameter: Constant10_Value
   * Referenced by: '<S3>/Constant10'
   */
  { 1, 1, 1, 1 }
};

/* Constant parameters (auto storage) */
const ConstP_ImageDet_T ImageDet_ConstP = {
  /* Computed Parameter: BlobAnalysis2_WALKER_
   * Referenced by: '<S3>/Blob Analysis2'
   */
  { -1, 321, 322, 323, 1, -321, -322, -323 },

  /* Expression: devName
   * Referenced by: '<Root>/V4L2 Video Capture'
   */
  { 47U, 100U, 101U, 118U, 47U, 118U, 105U, 100U, 101U, 111U, 48U, 0U }
};

/*
 * File trailer for generated code.
 *
 * [EOF]
 */

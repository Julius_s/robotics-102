/*
 * ImageDet_dt.h
 *
 * Code generation for model "ImageDet".
 *
 * Model version              : 1.191
 * Simulink Coder version : 8.7 (R2014b) 08-Sep-2014
 * C source code generated on : Thu Mar 05 19:35:17 2015
 *
 * Target selection: realtime.tlc
 * Embedded hardware selection: ARM Compatible->ARM Cortex
 * Code generation objectives: Unspecified
 * Validation result: Not run
 */

#include "ext_types.h"

/* data type size table */
static uint_T rtDataTypeSizes[] = {
  sizeof(real_T),
  sizeof(real32_T),
  sizeof(int8_T),
  sizeof(uint8_T),
  sizeof(int16_T),
  sizeof(uint16_T),
  sizeof(int32_T),
  sizeof(uint32_T),
  sizeof(boolean_T),
  sizeof(fcn_call_T),
  sizeof(int_T),
  sizeof(pointer_T),
  sizeof(action_T),
  2*sizeof(uint32_T),
  sizeof(BlobData),
  sizeof(Player),
  sizeof(Ball),
  sizeof(PlayerData),
  sizeof(BallData),
  sizeof(BlobData_size),
  sizeof(PlayerData_size),
  sizeof(BallData_size),
  sizeof(stETj7QHScyDJroYDDeQcXH_Image_T)
};

/* data type name table */
static const char_T * rtDataTypeNames[] = {
  "real_T",
  "real32_T",
  "int8_T",
  "uint8_T",
  "int16_T",
  "uint16_T",
  "int32_T",
  "uint32_T",
  "boolean_T",
  "fcn_call_T",
  "int_T",
  "pointer_T",
  "action_T",
  "timer_uint32_pair_T",
  "BlobData",
  "Player",
  "Ball",
  "PlayerData",
  "BallData",
  "BlobData_size",
  "PlayerData_size",
  "BallData_size",
  "stETj7QHScyDJroYDDeQcXH_Image_T"
};

/* data type transitions for block I/O structure */
static DataTypeTransition rtBTransitions[] = {
  { (char_T *)(&ImageDet_B.ColorSpaceConversion_o1[0]), 1, 0, 230400 },

  { (char_T *)(&ImageDet_B.lengthh), 0, 0, 1 },

  { (char_T *)(&ImageDet_B.players[0]), 15, 0, 6 },

  { (char_T *)(&ImageDet_B.ball), 16, 0, 1 },

  { (char_T *)(&ImageDet_B.BlobAnalysis2_o1[0]), 6, 0, 16 },

  { (char_T *)(&ImageDet_B.x[0]), 2, 0, 4 },

  { (char_T *)(&ImageDet_B.infoToTransmit[0]), 3, 0, 27 },

  { (char_T *)(&ImageDet_B.Erosion3[0]), 8, 0, 153600 }
  ,

  { (char_T *)(&ImageDet_DW.ColorSpaceConversion_DWORK1[0]), 1, 0, 76800 },

  { (char_T *)(&ImageDet_DW.latestPlayers), 22, 0, 1 },

  { (char_T *)(&ImageDet_DW.SFunction_DIMS2), 20, 0, 1 },

  { (char_T *)(&ImageDet_DW.SFunction_DIMS2_f), 19, 0, 1 },

  { (char_T *)(&ImageDet_DW.latestBall), 16, 0, 1 },

  { (char_T *)(&ImageDet_DW.SFunction_DIMS3), 21, 0, 1 },

  { (char_T *)(&ImageDet_DW.Scope2_PWORK.LoggedData), 11, 0, 4 },

  { (char_T *)(&ImageDet_DW.Dilation1_NUMNONZ_DW), 6, 0, 28 },

  { (char_T *)(&ImageDet_DW.Subsystem_SubsysRanBC), 2, 0, 2 },

  { (char_T *)(&ImageDet_DW.Psend), 3, 0, 4 },

  { (char_T *)(&ImageDet_DW.isStable), 8, 0, 3 }
};

/* data type transition table for block I/O structure */
static DataTypeTransitionTable rtBTransTable = {
  19U,
  rtBTransitions
};

/* data type transitions for Parameters structure */
static DataTypeTransition rtPTransitions[] = {
  { (char_T *)(&ImageDet_P.BlobAnalysis2_minArea), 7, 0, 1 },

  { (char_T *)(&ImageDet_P.Constant9_Value), 0, 0, 9 },

  { (char_T *)(&ImageDet_P.Gain1_Gain), 1, 0, 3 },

  { (char_T *)(&ImageDet_P.DataStoreMemory_InitialValue), 6, 0, 1 },

  { (char_T *)(&ImageDet_P.Constant1_Value_d), 3, 0, 7 },

  { (char_T *)(&ImageDet_P.Constant10_Value[0]), 8, 0, 4 }
};

/* data type transition table for Parameters structure */
static DataTypeTransitionTable rtPTransTable = {
  6U,
  rtPTransitions
};

/* [EOF] ImageDet_dt.h */

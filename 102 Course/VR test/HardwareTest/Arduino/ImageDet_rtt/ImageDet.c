/*
 * File: ImageDet.c
 *
 * Code generated for Simulink model 'ImageDet'.
 *
 * Model version                  : 1.191
 * Simulink Coder version         : 8.7 (R2014b) 08-Sep-2014
 * TLC version                    : 8.7 (Aug  5 2014)
 * C/C++ source code generated on : Thu Mar 05 19:35:17 2015
 *
 * Target selection: realtime.tlc
 * Embedded hardware selection: ARM Compatible->ARM Cortex
 * Code generation objectives: Unspecified
 * Validation result: Not run
 */

#include "ImageDet.h"
#include "ImageDet_private.h"
#include "ImageDet_dt.h"

/* Named constants for Chart: '<S1>/Chart' */
#define ImageDet_IN_NO_ACTIVE_CHILD    ((uint8_T)0U)
#define ImageDet_IN_gameIsOn           ((uint8_T)1U)
#define ImageDet_IN_notOn              ((uint8_T)2U)

/* Block signals (auto storage) */
B_ImageDet_T ImageDet_B;

/* Block states (auto storage) */
DW_ImageDet_T ImageDet_DW;

/* Real-time model */
RT_MODEL_ImageDet_T ImageDet_M_;
RT_MODEL_ImageDet_T *const ImageDet_M = &ImageDet_M_;

/* Forward declaration for local functions */
static void ImageDet_eml_sort(const int32_T x_data[], const int32_T x_sizes,
  int32_T y_data[], int32_T *y_sizes, int32_T idx_data[], int32_T *idx_sizes);
static void ImageDet_eml_li_find(const boolean_T x_data[], const int32_T
  x_sizes[2], int32_T y_data[], int32_T y_sizes[2]);
static uint8_T ImageDet_calcReady(void);
static uint8_T ImageDet_calcGoal(void);
int32_T div_s32(int32_T numerator, int32_T denominator)
{
  int32_T quotient;
  uint32_T tempAbsQuotient;
  if (denominator == 0) {
    quotient = numerator >= 0 ? MAX_int32_T : MIN_int32_T;

    /* Divide by zero handler */
  } else {
    tempAbsQuotient = (uint32_T)(numerator >= 0 ? numerator : -numerator) /
      (denominator >= 0 ? denominator : -denominator);
    quotient = (numerator < 0) != (denominator < 0) ? -(int32_T)tempAbsQuotient :
      (int32_T)tempAbsQuotient;
  }

  return quotient;
}

/* Function for MATLAB Function: '<S1>/parseBlob' */
static void ImageDet_eml_sort(const int32_T x_data[], const int32_T x_sizes,
  int32_T y_data[], int32_T *y_sizes, int32_T idx_data[], int32_T *idx_sizes)
{
  int32_T vstride;
  int32_T i1;
  int32_T ix;
  int32_T dim;
  int32_T vlen;
  int32_T j;
  int32_T d_k;
  int32_T i;
  int32_T i2;
  int32_T b_j;
  int32_T q;
  int32_T qEnd;
  int32_T kEnd;
  int32_T vwork_data[16];
  int32_T iidx_data[16];
  int32_T idx0_data[16];
  int32_T b_idx_0;
  dim = 2;
  if (x_sizes != 1) {
    dim = 1;
  }

  if (dim <= 1) {
    vlen = x_sizes - 1;
  } else {
    vlen = 0;
  }

  b_idx_0 = (int8_T)(vlen + 1);
  *y_sizes = x_sizes;
  *idx_sizes = (int8_T)x_sizes;
  vstride = 1;
  ix = 1;
  while (ix <= dim - 1) {
    vstride *= x_sizes;
    ix = 2;
  }

  i1 = -1;
  for (j = 1; j <= vstride; j++) {
    i1++;
    ix = i1;
    for (dim = 0; dim <= vlen; dim++) {
      vwork_data[dim] = x_data[ix];
      ix += vstride;
    }

    if (b_idx_0 == 0) {
      for (dim = 1; dim <= b_idx_0; dim++) {
        iidx_data[dim - 1] = dim;
      }
    } else {
      for (dim = 1; dim <= b_idx_0; dim++) {
        iidx_data[dim - 1] = dim;
      }

      for (dim = 1; dim <= b_idx_0 - 1; dim += 2) {
        if (!(vwork_data[dim - 1] <= vwork_data[dim])) {
          iidx_data[dim - 1] = dim + 1;
          iidx_data[dim] = dim;
        }
      }

      for (dim = 0; dim < b_idx_0; dim++) {
        idx0_data[dim] = 1;
      }

      i = 2;
      while (i < b_idx_0) {
        i2 = i << 1;
        b_j = 1;
        dim = 1 + i;
        while (dim < b_idx_0 + 1) {
          ix = b_j;
          q = dim;
          qEnd = b_j + i2;
          if (qEnd > b_idx_0 + 1) {
            qEnd = b_idx_0 + 1;
          }

          d_k = 0;
          kEnd = qEnd - b_j;
          while (d_k + 1 <= kEnd) {
            if (vwork_data[iidx_data[ix - 1] - 1] <= vwork_data[iidx_data[q - 1]
                - 1]) {
              idx0_data[d_k] = iidx_data[ix - 1];
              ix++;
              if (ix == dim) {
                while (q < qEnd) {
                  d_k++;
                  idx0_data[d_k] = iidx_data[q - 1];
                  q++;
                }
              }
            } else {
              idx0_data[d_k] = iidx_data[q - 1];
              q++;
              if (q == qEnd) {
                while (ix < dim) {
                  d_k++;
                  idx0_data[d_k] = iidx_data[ix - 1];
                  ix++;
                }
              }
            }

            d_k++;
          }

          for (dim = 0; dim + 1 <= kEnd; dim++) {
            iidx_data[(b_j + dim) - 1] = idx0_data[dim];
          }

          b_j = qEnd;
          dim = qEnd + i;
        }

        i = i2;
      }
    }

    ix = i1;
    for (dim = 0; dim <= vlen; dim++) {
      y_data[ix] = vwork_data[iidx_data[dim] - 1];
      idx_data[ix] = iidx_data[dim];
      ix += vstride;
    }
  }
}

real32_T rt_roundf_snf(real32_T u)
{
  real32_T y;
  if ((real32_T)fabs(u) < 8.388608E+6F) {
    if (u >= 0.5F) {
      y = (real32_T)floor(u + 0.5F);
    } else if (u > -0.5F) {
      y = u * 0.0F;
    } else {
      y = (real32_T)ceil(u - 0.5F);
    }
  } else {
    y = u;
  }

  return y;
}

/* Function for MATLAB Function: '<S1>/parseBlob' */
static void ImageDet_eml_li_find(const boolean_T x_data[], const int32_T
  x_sizes[2], int32_T y_data[], int32_T y_sizes[2])
{
  int32_T n;
  int32_T k;
  int32_T b_i;
  n = x_sizes[1];
  k = 0;
  for (b_i = 1; b_i <= n; b_i++) {
    if (x_data[b_i - 1]) {
      k++;
    }
  }

  y_sizes[0] = 1;
  y_sizes[1] = k;
  k = 0;
  for (b_i = 1; b_i <= n; b_i++) {
    if (x_data[b_i - 1]) {
      y_data[k] = b_i;
      k++;
    }
  }
}

/* Function for Chart: '<S1>/Chart' */
static uint8_T ImageDet_calcReady(void)
{
  uint8_T ready;
  int32_T ii;
  static const int8_T b[6] = { -10, -30, -70, 10, 30, 70 };

  boolean_T exitg1;
  boolean_T guard1;
  int8_T x;
  int32_T tmp;

  /* MATLAB Function 'calcReady': '<S4>:9' */
  /* '<S4>:9:2' */
  ready = 1U;

  /* '<S4>:9:3' */
  /* '<S4>:9:4' */
  ii = 0;
  exitg1 = false;
  while ((!exitg1) && (ii < 6)) {
    /* '<S4>:9:4' */
    tmp = ImageDet_B.players[ii].x - b[ii];
    if (tmp > 127) {
      tmp = 127;
    } else {
      if (tmp < -128) {
        tmp = -128;
      }
    }

    guard1 = false;
    if ((int8_T)tmp < 0) {
      tmp = -(int8_T)tmp;
      if (tmp > 127) {
        tmp = 127;
      }

      x = (int8_T)tmp;
    } else {
      x = (int8_T)tmp;
    }

    if (x > 3) {
      /* '<S4>:9:5' */
      if (ImageDet_B.players[ii].y < 0) {
        tmp = -ImageDet_B.players[ii].y;
        if (tmp > 127) {
          tmp = 127;
        }

        x = (int8_T)tmp;
      } else {
        x = ImageDet_B.players[ii].y;
      }

      if (x > 3) {
        /* '<S4>:9:5' */
        /* '<S4>:9:6' */
        ready = 0U;
        exitg1 = true;
      } else {
        guard1 = true;
      }
    } else {
      guard1 = true;
    }

    if (guard1) {
      /* '<S4>:9:4' */
      ii++;
    }
  }

  return ready;
}

/* Function for Chart: '<S1>/Chart' */
static uint8_T ImageDet_calcGoal(void)
{
  uint8_T goal;
  int8_T tmp;
  int32_T tmp_0;

  /* MATLAB Function 'calcGoal': '<S4>:6' */
  /* '<S4>:6:2' */
  goal = 0U;
  if (ImageDet_B.ball.x < 0) {
    tmp_0 = -ImageDet_B.ball.x;
    if (tmp_0 > 127) {
      tmp_0 = 127;
    }

    tmp = (int8_T)tmp_0;
  } else {
    tmp = ImageDet_B.ball.x;
  }

  if (tmp > 75) {
    /* '<S4>:6:3' */
    /* '<S4>:6:4' */
    goal = 1U;
  }

  return goal;
}

real32_T rt_atan2f_snf(real32_T u0, real32_T u1)
{
  real32_T y;
  int32_T u0_0;
  int32_T u1_0;
  if (rtIsNaNF(u0) || rtIsNaNF(u1)) {
    y = (rtNaNF);
  } else if (rtIsInfF(u0) && rtIsInfF(u1)) {
    if (u0 > 0.0F) {
      u0_0 = 1;
    } else {
      u0_0 = -1;
    }

    if (u1 > 0.0F) {
      u1_0 = 1;
    } else {
      u1_0 = -1;
    }

    y = (real32_T)atan2((real32_T)u0_0, (real32_T)u1_0);
  } else if (u1 == 0.0F) {
    if (u0 > 0.0F) {
      y = RT_PIF / 2.0F;
    } else if (u0 < 0.0F) {
      y = -(RT_PIF / 2.0F);
    } else {
      y = 0.0F;
    }
  } else {
    y = (real32_T)atan2(u0, u1);
  }

  return y;
}

real32_T rt_remf_snf(real32_T u0, real32_T u1)
{
  real32_T y;
  real32_T u1_0;
  if (!((!rtIsNaNF(u0)) && (!rtIsInfF(u0)) && ((!rtIsNaNF(u1)) && (!rtIsInfF(u1)))))
  {
    y = (rtNaNF);
  } else {
    if (u1 < 0.0F) {
      u1_0 = (real32_T)ceil(u1);
    } else {
      u1_0 = (real32_T)floor(u1);
    }

    if ((u1 != 0.0F) && (u1 != u1_0)) {
      u1_0 = u0 / u1;
      if ((real32_T)fabs(u1_0 - rt_roundf_snf(u1_0)) <= FLT_EPSILON * (real32_T)
          fabs(u1_0)) {
        y = 0.0F;
      } else {
        y = (real32_T)fmod(u0, u1);
      }
    } else {
      y = (real32_T)fmod(u0, u1);
    }
  }

  return y;
}

real_T rt_roundd_snf(real_T u)
{
  real_T y;
  if (fabs(u) < 4.503599627370496E+15) {
    if (u >= 0.5) {
      y = floor(u + 0.5);
    } else if (u > -0.5) {
      y = u * 0.0;
    } else {
      y = ceil(u - 0.5);
    }
  } else {
    y = u;
  }

  return y;
}

/* Model output function */
void ImageDet_output(void)
{
  boolean_T colorVec[2];
  boolean_T guard1;
  real32_T dist;
  real32_T temp;
  real32_T absxk;
  real32_T scale;
  real32_T t;
  uint8_T y;
  int8_T x;
  uint8_T b_y;
  uint8_T e_y[2];
  int16_T e_x;
  static const char_T b[13] = { '/', 'd', 'e', 'v', '/', 't', 't', 'y', 'A', 'M',
    'A', '0', '\x00' };

  uint8_T dataOut[27];
  BlobData rtb_blobs;
  int8_T rtb_players_x[6];
  int8_T rtb_players_y[6];
  int16_T rtb_players_orientation[6];
  real32_T rtb_BlobAnalysis2_o2[32];
  int8_T rtb_y[2];
  int8_T rtb_x[2];
  uint32_T numBlobs;
  uint32_T BlobAnalysis2_NUM_PIX_DW[16];
  boolean_T maxNumBlobsReached;
  uint8_T currentLabel;
  uint32_T pixIdx;
  uint32_T start_pixIdx;
  uint32_T stackIdx;
  uint32_T walkerIdx;
  int32_T n;
  int32_T inIdx;
  int32_T m;
  int32_T nhIdx;
  int32_T idxNHood;
  int32_T i;
  char_T b_0[13];
  int32_T i_0;
  int32_T loop_ub;
  real32_T tmp[2];
  real32_T centroidsInt_data[32];
  int8_T color_data[2];
  int8_T b_ii_data[2];
  BlobData_size blobs_elems_sizes;
  real32_T allLights_data[93];
  real32_T reds_data[62];
  real32_T blues_data[62];
  int32_T blobIndices_data[16];
  int8_T f_data[7];
  int16_T g_data[7];
  int32_T c_y_data[16];
  int32_T iidx_data[16];
  real32_T tmp_data[32];
  uint8_T tmp_data_0[16];
  int32_T allLights_sizes[2];
  int32_T tmp_sizes[2];
  int32_T allLights_sizes_0[2];
  int32_T allLights_sizes_1[2];
  boolean_T allLights_data_0[31];
  int32_T allLights_sizes_2[2];
  int32_T tmp_data_1[31];
  int32_T tmp_sizes_0[2];
  real32_T allLights_data_1[93];
  real32_T blobs_data[2];
  real32_T s;
  int32_T allLights_sizes_idx_1;
  int32_T i_1;
  real_T tmp_0;

  /* Reset subsysRan breadcrumbs */
  srClearBC(ImageDet_DW.Subsystem_SubsysRanBC);

  /* Reset subsysRan breadcrumbs */
  srClearBC(ImageDet_DW.Subsystem2_SubsysRanBC);

  /* S-Function (v4l2_video_capture_sfcn): '<Root>/V4L2 Video Capture' */
  MW_videoCaptureOutput(ImageDet_ConstP.V4L2VideoCapture_p1,
                        ImageDet_B.V4L2VideoCapture_o1,
                        ImageDet_B.V4L2VideoCapture_o2,
                        ImageDet_B.V4L2VideoCapture_o3);

  /* DataTypeConversion: '<S3>/Data Type Conversion' */
  for (i = 0; i < 76800; i++) {
    ImageDet_B.Gain1[i] = ImageDet_B.V4L2VideoCapture_o1[i];
  }

  /* End of DataTypeConversion: '<S3>/Data Type Conversion' */

  /* Gain: '<S3>/Gain1' */
  for (i_0 = 0; i_0 < 76800; i_0++) {
    ImageDet_B.Gain1[i_0] *= ImageDet_P.Gain1_Gain;
  }

  /* End of Gain: '<S3>/Gain1' */

  /* DataTypeConversion: '<S3>/Data Type Conversion1' */
  for (i = 0; i < 76800; i++) {
    ImageDet_B.Gain2[i] = ImageDet_B.V4L2VideoCapture_o2[i];
  }

  /* End of DataTypeConversion: '<S3>/Data Type Conversion1' */

  /* Gain: '<S3>/Gain2' */
  for (i_0 = 0; i_0 < 76800; i_0++) {
    ImageDet_B.Gain2[i_0] *= ImageDet_P.Gain2_Gain;
  }

  /* End of Gain: '<S3>/Gain2' */

  /* DataTypeConversion: '<S3>/Data Type Conversion2' */
  for (i = 0; i < 76800; i++) {
    ImageDet_B.Gain3[i] = ImageDet_B.V4L2VideoCapture_o3[i];
  }

  /* End of DataTypeConversion: '<S3>/Data Type Conversion2' */

  /* Gain: '<S3>/Gain3' */
  for (i_0 = 0; i_0 < 76800; i_0++) {
    ImageDet_B.Gain3[i_0] *= ImageDet_P.Gain3_Gain;
  }

  /* End of Gain: '<S3>/Gain3' */

  /* S-Function (svipcolorconv): '<S3>/Color Space  Conversion' */
  /* temporary variables for in-place operation */
  for (i = 0; i < 76800; i++) {
    /* First get the min and max of the RGB triplet */
    if (ImageDet_B.Gain1[i] > ImageDet_B.Gain2[i]) {
      if ((ImageDet_B.Gain2[i] <= ImageDet_B.Gain3[i]) || rtIsNaNF
          (ImageDet_B.Gain3[i])) {
        temp = ImageDet_B.Gain2[i];
      } else {
        temp = ImageDet_B.Gain3[i];
      }

      if ((ImageDet_B.Gain1[i] >= ImageDet_B.Gain3[i]) || rtIsNaNF
          (ImageDet_B.Gain3[i])) {
        absxk = ImageDet_B.Gain1[i];
      } else {
        absxk = ImageDet_B.Gain3[i];
      }
    } else {
      if ((ImageDet_B.Gain1[i] <= ImageDet_B.Gain3[i]) || rtIsNaNF
          (ImageDet_B.Gain3[i])) {
        temp = ImageDet_B.Gain1[i];
      } else {
        temp = ImageDet_B.Gain3[i];
      }

      if ((ImageDet_B.Gain2[i] >= ImageDet_B.Gain3[i]) || rtIsNaNF
          (ImageDet_B.Gain3[i])) {
        absxk = ImageDet_B.Gain2[i];
      } else {
        absxk = ImageDet_B.Gain3[i];
      }
    }

    dist = absxk - temp;
    if (absxk != 0.0F) {
      temp = dist / absxk;
    } else {
      temp = 0.0F;
    }

    if (dist != 0.0F) {
      if (ImageDet_B.Gain1[i] == absxk) {
        dist = (ImageDet_B.Gain2[i] - ImageDet_B.Gain3[i]) / dist;
      } else if (ImageDet_B.Gain2[i] == absxk) {
        dist = (ImageDet_B.Gain3[i] - ImageDet_B.Gain1[i]) / dist + 2.0F;
      } else {
        dist = (ImageDet_B.Gain1[i] - ImageDet_B.Gain2[i]) / dist + 4.0F;
      }

      dist /= 6.0F;
      if (dist < 0.0F) {
        dist++;
      }
    } else {
      dist = 0.0F;
    }

    /* assign the results */
    ImageDet_B.ColorSpaceConversion_o1[i] = dist;
    ImageDet_B.ColorSpaceConversion_o2[i] = temp;
    ImageDet_B.ColorSpaceConversion_o3[i] = absxk;
  }

  /* MATLAB Function 'Raspi Code/Blob extraction /MATLAB Function1': '<S10>:1' */
  /* %common v */
  /* '<S10>:1:3' */
  /*  channel2Min=0.4; */
  /* % hue vals */
  /* '<S10>:1:6' */
  /* '<S10>:1:7' */
  /* '<S10>:1:9' */
  /* '<S10>:1:10' */
  /*  channel3BlueMin = 0.5; */
  /* % ball vals */
  /* '<S10>:1:17' */
  /* '<S10>:1:20' */
  for (i = 0; i < 76800; i++) {
    /* MATLAB Function: '<S3>/MATLAB Function1' */
    maxNumBlobsReached = (((ImageDet_B.ColorSpaceConversion_o1[i] >= 0.75F) ||
      (ImageDet_B.ColorSpaceConversion_o1[i] <= 0.25F)) &&
                          (ImageDet_B.ColorSpaceConversion_o3[i] >= 0.8));
    guard1 = ((ImageDet_B.ColorSpaceConversion_o1[i] >= 0.25F) &&
              (ImageDet_B.ColorSpaceConversion_o1[i] <= 0.75F) &&
              (ImageDet_B.ColorSpaceConversion_o3[i] >= 0.8));

    /* RelationalOperator: '<S8>/Compare' incorporates:
     *  Constant: '<S8>/Constant'
     *  Sum: '<S3>/Add1'
     */
    ImageDet_B.Compare[i] = ((int32_T)((uint32_T)maxNumBlobsReached + guard1) >
      ImageDet_P.Constant_Value_e);

    /* MATLAB Function: '<S3>/MATLAB Function1' */
    ImageDet_B.redBool[i] = maxNumBlobsReached;
    ImageDet_B.blueBool[i] = guard1;
  }

  /* End of S-Function (svipcolorconv): '<S3>/Color Space  Conversion' */

  /* S-Function (svipmorphop): '<S3>/Dilation1' incorporates:
   *  Constant: '<S3>/Constant10'
   */
  idxNHood = 0;
  allLights_sizes_idx_1 = 0;
  inIdx = 0;
  for (n = 0; n < 2; n++) {
    for (m = 0; m < 2; m++) {
      if (ImageDet_P.Constant10_Value[idxNHood]) {
        ImageDet_DW.Dilation1_DILATE_OFF_DW[allLights_sizes_idx_1] = n * 323 + m;
        inIdx++;
        allLights_sizes_idx_1++;
      }

      idxNHood++;
    }
  }

  ImageDet_DW.Dilation1_NUMNONZ_DW = inIdx;
  for (i = 0; i < 78489; i++) {
    ImageDet_DW.Dilation1_ONE_PAD_IMG_DW[i] = false;
  }

  for (inIdx = 0; inIdx < 76800; inIdx++) {
    if (ImageDet_B.Compare[inIdx]) {
      idxNHood = div_s32(inIdx, 320);
      idxNHood = (inIdx - idxNHood * 320) + idxNHood * 323;
      for (i = 0; i < ImageDet_DW.Dilation1_NUMNONZ_DW; i++) {
        ImageDet_DW.Dilation1_ONE_PAD_IMG_DW[idxNHood +
          ImageDet_DW.Dilation1_DILATE_OFF_DW[i]] = true;
      }
    }
  }

  idxNHood = 0;
  allLights_sizes_idx_1 = 0;
  inIdx = 0;
  for (n = 0; n < 240; n++) {
    for (m = 0; m < 320; m++) {
      ImageDet_B.Dilation1[idxNHood] =
        ImageDet_DW.Dilation1_ONE_PAD_IMG_DW[inIdx];
      idxNHood++;
      inIdx++;
    }

    inIdx += 3;
  }

  /* End of S-Function (svipmorphop): '<S3>/Dilation1' */

  /* S-Function (svipmorphop): '<S3>/Erosion3' */
  idxNHood = 0;
  for (m = 0; m < 327; m++) {
    ImageDet_DW.Erosion3_ONE_PAD_IMG_DW[idxNHood] = true;
    idxNHood++;
  }

  for (i = 0; i < 240; i++) {
    ImageDet_DW.Erosion3_ONE_PAD_IMG_DW[idxNHood] = true;
    memcpy(&ImageDet_DW.Erosion3_ONE_PAD_IMG_DW[idxNHood + 1],
           &ImageDet_B.Dilation1[allLights_sizes_idx_1], 320U * sizeof(boolean_T));
    idxNHood += 321;
    allLights_sizes_idx_1 += 320;
    for (m = 0; m < 6; m++) {
      ImageDet_DW.Erosion3_ONE_PAD_IMG_DW[idxNHood] = true;
      idxNHood++;
    }
  }

  for (i = 0; i < 6; i++) {
    for (m = 0; m < 327; m++) {
      ImageDet_DW.Erosion3_ONE_PAD_IMG_DW[idxNHood] = true;
      idxNHood++;
    }
  }

  for (i = 0; i < 80769; i++) {
    ImageDet_DW.Erosion3_TWO_PAD_IMG_DW[i] = true;
  }

  nhIdx = 0;
  for (n = 0; n < 247; n++) {
    for (m = 0; m < 324; m++) {
      ImageDet_DW.Erosion3_TWO_PAD_IMG_DW[1 + nhIdx] = true;
      i = 0;
      while (i < ImageDet_DW.Erosion3_NUMNONZ_DW[0]) {
        if (!ImageDet_DW.Erosion3_ONE_PAD_IMG_DW[nhIdx +
            ImageDet_DW.Erosion3_ERODE_OFF_DW[i]]) {
          ImageDet_DW.Erosion3_TWO_PAD_IMG_DW[1 + nhIdx] = false;
          i = ImageDet_DW.Erosion3_NUMNONZ_DW[0];
        }

        i++;
      }

      nhIdx++;
    }

    nhIdx += 3;
  }

  inIdx = 1;
  idxNHood = 0;
  for (n = 0; n < 240; n++) {
    for (m = 1; m < 321; m++) {
      ImageDet_B.Erosion3[idxNHood] = true;
      i = 0;
      while (i < ImageDet_DW.Erosion3_NUMNONZ_DW[1]) {
        if (!ImageDet_DW.Erosion3_TWO_PAD_IMG_DW[ImageDet_DW.Erosion3_ERODE_OFF_DW
            [i + ImageDet_DW.Erosion3_NUMNONZ_DW[0]] + inIdx]) {
          ImageDet_B.Erosion3[idxNHood] = false;
          i = ImageDet_DW.Erosion3_NUMNONZ_DW[1];
        }

        i++;
      }

      inIdx++;
      idxNHood++;
    }

    inIdx += 7;
  }

  /* End of S-Function (svipmorphop): '<S3>/Erosion3' */

  /* S-Function (svipblob): '<S3>/Blob Analysis2' */
  maxNumBlobsReached = false;
  memset(&ImageDet_DW.BlobAnalysis2_PAD_DW[0], 0, 323U * sizeof(uint8_T));
  currentLabel = 1U;
  i = 0;
  allLights_sizes_idx_1 = 323;
  for (n = 0; n < 240; n++) {
    for (m = 0; m < 320; m++) {
      ImageDet_DW.BlobAnalysis2_PAD_DW[allLights_sizes_idx_1] = (uint8_T)
        (ImageDet_B.Erosion3[i] ? 255 : 0);
      i++;
      allLights_sizes_idx_1++;
    }

    ImageDet_DW.BlobAnalysis2_PAD_DW[allLights_sizes_idx_1] = 0U;
    ImageDet_DW.BlobAnalysis2_PAD_DW[allLights_sizes_idx_1 + 1] = 0U;
    allLights_sizes_idx_1 += 2;
  }

  memset(&ImageDet_DW.BlobAnalysis2_PAD_DW[allLights_sizes_idx_1], 0, 321U *
         sizeof(uint8_T));
  idxNHood = 0;
  pixIdx = 0U;
  n = 0;
  while (n < 240) {
    allLights_sizes_idx_1 = 0;
    nhIdx = (idxNHood + 1) * 322;
    m = 0;
    while (m < 320) {
      numBlobs = (uint32_T)((nhIdx + allLights_sizes_idx_1) + 1);
      start_pixIdx = pixIdx;
      if (ImageDet_DW.BlobAnalysis2_PAD_DW[numBlobs] == 255) {
        ImageDet_DW.BlobAnalysis2_PAD_DW[numBlobs] = currentLabel;
        ImageDet_DW.BlobAnalysis2_N_PIXLIST_DW[pixIdx] = (int16_T)idxNHood;
        ImageDet_DW.BlobAnalysis2_M_PIXLIST_DW[pixIdx] = (int16_T)
          allLights_sizes_idx_1;
        pixIdx++;
        BlobAnalysis2_NUM_PIX_DW[currentLabel - 1] = 1U;
        ImageDet_DW.BlobAnalysis2_STACK_DW[0U] = numBlobs;
        stackIdx = 1U;
        while (stackIdx != 0U) {
          stackIdx--;
          numBlobs = ImageDet_DW.BlobAnalysis2_STACK_DW[stackIdx];
          for (i = 0; i < 8; i++) {
            walkerIdx = numBlobs + ImageDet_ConstP.BlobAnalysis2_WALKER_[i];
            if (ImageDet_DW.BlobAnalysis2_PAD_DW[walkerIdx] == 255) {
              ImageDet_DW.BlobAnalysis2_PAD_DW[walkerIdx] = currentLabel;
              ImageDet_DW.BlobAnalysis2_N_PIXLIST_DW[pixIdx] = (int16_T)
                ((int16_T)(walkerIdx / 322U) - 1);
              ImageDet_DW.BlobAnalysis2_M_PIXLIST_DW[pixIdx] = (int16_T)
                (walkerIdx % 322U - 1U);
              pixIdx++;
              BlobAnalysis2_NUM_PIX_DW[currentLabel - 1]++;
              ImageDet_DW.BlobAnalysis2_STACK_DW[stackIdx] = walkerIdx;
              stackIdx++;
            }
          }
        }

        if (BlobAnalysis2_NUM_PIX_DW[currentLabel - 1] <
            ImageDet_P.BlobAnalysis2_minArea) {
          currentLabel--;
          pixIdx = start_pixIdx;
        }

        if (currentLabel == 16) {
          maxNumBlobsReached = true;
          n = 240;
          m = 320;
        }

        if (m < 320) {
          currentLabel++;
        }
      }

      allLights_sizes_idx_1++;
      m++;
    }

    idxNHood++;
    n++;
  }

  numBlobs = (uint32_T)(maxNumBlobsReached ? (int32_T)currentLabel : (int32_T)
                        (uint8_T)(currentLabel - 1U));
  idxNHood = 0;
  allLights_sizes_idx_1 = 0;
  for (i = 0; i < (int32_T)numBlobs; i++) {
    ImageDet_B.BlobAnalysis2_o1[i] = (int32_T)BlobAnalysis2_NUM_PIX_DW[i];
    nhIdx = 0;
    inIdx = 0;
    for (m = 0; m < (int32_T)BlobAnalysis2_NUM_PIX_DW[i]; m++) {
      nhIdx += ImageDet_DW.BlobAnalysis2_N_PIXLIST_DW[m + allLights_sizes_idx_1];
      inIdx += ImageDet_DW.BlobAnalysis2_M_PIXLIST_DW[m + idxNHood];
    }

    rtb_BlobAnalysis2_o2[i] = (real32_T)nhIdx / (real32_T)
      BlobAnalysis2_NUM_PIX_DW[i] + 1.0F;
    rtb_BlobAnalysis2_o2[numBlobs + i] = (real32_T)inIdx / (real32_T)
      BlobAnalysis2_NUM_PIX_DW[i] + 1.0F;
    idxNHood += (int32_T)BlobAnalysis2_NUM_PIX_DW[i];
    allLights_sizes_idx_1 += (int32_T)BlobAnalysis2_NUM_PIX_DW[i];
  }

  ImageDet_DW.BlobAnalysis2_DIMS1[0] = (int32_T)numBlobs;
  ImageDet_DW.BlobAnalysis2_DIMS1[1] = 1;
  ImageDet_DW.BlobAnalysis2_DIMS2[0] = (int32_T)numBlobs;
  ImageDet_DW.BlobAnalysis2_DIMS2[1] = 2;

  /* End of S-Function (svipblob): '<S3>/Blob Analysis2' */

  /* MATLAB Function: '<S3>/FindBlobs' */
  /* MATLAB Function 'Raspi Code/Blob extraction /FindBlobs': '<S9>:1' */
  /*  area */
  /* '<S9>:1:4' */
  /* '<S9>:1:5' */
  m = ImageDet_DW.BlobAnalysis2_DIMS2[0];
  loop_ub = ImageDet_DW.BlobAnalysis2_DIMS2[0] *
    ImageDet_DW.BlobAnalysis2_DIMS2[1];
  for (i_0 = 0; i_0 < loop_ub; i_0++) {
    centroidsInt_data[i_0] = rtb_BlobAnalysis2_o2[i_0];
  }

  idxNHood = ImageDet_DW.BlobAnalysis2_DIMS2[0] *
    ImageDet_DW.BlobAnalysis2_DIMS2[1];
  for (inIdx = 0; inIdx < idxNHood; inIdx++) {
    centroidsInt_data[inIdx] = (real32_T)floor(centroidsInt_data[inIdx]);
  }

  /* '<S9>:1:7' */
  ImageDet_DW.SFunction_DIMS2_f.centroid[0] = 0;
  ImageDet_DW.SFunction_DIMS2_f.centroid[1] = 2;

  /* '<S9>:1:8' */
  ImageDet_DW.SFunction_DIMS2_f.area = 0;

  /* '<S9>:1:9' */
  ImageDet_DW.SFunction_DIMS2_f.color = 0;

  /*  check for color inconsistencies */
  /* '<S9>:1:15' */
  for (nhIdx = 0; nhIdx < ImageDet_DW.BlobAnalysis2_DIMS1[0]; nhIdx++) {
    /* '<S9>:1:15' */
    /* '<S9>:1:16' */
    /* '<S9>:1:17' */
    /* '<S9>:1:18' */
    colorVec[0] = ImageDet_B.redBool[(((int32_T)centroidsInt_data[nhIdx] - 1) *
      320 + (int32_T)centroidsInt_data[nhIdx + m]) - 1];
    colorVec[1] = ImageDet_B.blueBool[(((int32_T)centroidsInt_data[nhIdx] - 1) *
      320 + (int32_T)centroidsInt_data[nhIdx + m]) - 1];

    /* '<S9>:1:19' */
    allLights_sizes_idx_1 = 0;
    idxNHood = 1;
    maxNumBlobsReached = false;
    while ((!maxNumBlobsReached) && (idxNHood < 3)) {
      guard1 = false;
      if (colorVec[idxNHood - 1]) {
        allLights_sizes_idx_1++;
        b_ii_data[allLights_sizes_idx_1 - 1] = (int8_T)idxNHood;
        if (allLights_sizes_idx_1 >= 2) {
          maxNumBlobsReached = true;
        } else {
          guard1 = true;
        }
      } else {
        guard1 = true;
      }

      if (guard1) {
        idxNHood++;
      }
    }

    if (1 > allLights_sizes_idx_1) {
      allLights_sizes_idx_1 = 0;
    }

    for (i_0 = 0; i_0 < allLights_sizes_idx_1; i_0++) {
      color_data[i_0] = b_ii_data[i_0];
    }

    if (allLights_sizes_idx_1 == 1) {
      /* '<S9>:1:20' */
      /* '<S9>:1:21' */
      i = ImageDet_DW.SFunction_DIMS2_f.centroid[0];
      inIdx = ImageDet_DW.SFunction_DIMS2_f.centroid[1];
      loop_ub = ImageDet_DW.BlobAnalysis2_DIMS2[1] - 1;
      n = ImageDet_DW.SFunction_DIMS2_f.centroid[0] + 1;
      for (i_0 = 0; i_0 < inIdx; i_0++) {
        for (i_1 = 0; i_1 < i; i_1++) {
          tmp_data[i_1 + n * i_0] = rtb_blobs.centroid[i * i_0 + i_1];
        }
      }

      for (i_0 = 0; i_0 <= loop_ub; i_0++) {
        tmp_data[i + n * i_0] =
          rtb_BlobAnalysis2_o2[ImageDet_DW.BlobAnalysis2_DIMS2[0] * i_0 + nhIdx];
      }

      ImageDet_DW.SFunction_DIMS2_f.centroid[0] = n;
      ImageDet_DW.SFunction_DIMS2_f.centroid[1] = inIdx;
      loop_ub = n * inIdx;
      for (i_0 = 0; i_0 < loop_ub; i_0++) {
        rtb_blobs.centroid[i_0] = tmp_data[i_0];
      }

      /* '<S9>:1:22' */
      inIdx = ImageDet_DW.SFunction_DIMS2_f.area + 1;
      loop_ub = ImageDet_DW.SFunction_DIMS2_f.area;
      for (i_0 = 0; i_0 < loop_ub; i_0++) {
        c_y_data[i_0] = rtb_blobs.area[i_0];
      }

      c_y_data[ImageDet_DW.SFunction_DIMS2_f.area] =
        ImageDet_B.BlobAnalysis2_o1[nhIdx];
      ImageDet_DW.SFunction_DIMS2_f.area = inIdx;
      for (i_0 = 0; i_0 < inIdx; i_0++) {
        rtb_blobs.area[i_0] = c_y_data[i_0];
      }

      /* '<S9>:1:23' */
      i = ImageDet_DW.SFunction_DIMS2_f.color + 1;
      loop_ub = ImageDet_DW.SFunction_DIMS2_f.color;
      for (i_0 = 0; i_0 < loop_ub; i_0++) {
        tmp_data_0[i_0] = rtb_blobs.color[i_0];
      }

      tmp_data_0[ImageDet_DW.SFunction_DIMS2_f.color] = (uint8_T)(int32_T)
        rt_roundd_snf((real_T)color_data[0]);
      ImageDet_DW.SFunction_DIMS2_f.color = i;
      for (i_0 = 0; i_0 < i; i_0++) {
        rtb_blobs.color[i_0] = tmp_data_0[i_0];
      }
    }

    /* '<S9>:1:15' */
  }

  /* End of MATLAB Function: '<S3>/FindBlobs' */

  /* MATLAB Function: '<S1>/parseBlob' incorporates:
   *  Constant: '<S1>/Constant6'
   *  Constant: '<S1>/Constant7'
   */
  /*  assert(length(blobs.centroid(:,1))==length(blobs.color)); */
  blobs_elems_sizes = ImageDet_DW.SFunction_DIMS2_f;

  /* MATLAB Function 'Raspi Code/parseBlob': '<S7>:1' */
  ImageDet_eml_sort(rtb_blobs.area, blobs_elems_sizes.area, c_y_data, &inIdx,
                    iidx_data, &i);
  inIdx = i - 1;
  for (i_0 = 0; i_0 < i; i_0++) {
    blobIndices_data[i_0] = iidx_data[i_0];
  }

  /*  blobs.area */
  /*  blobIndices */
  /*  blobs.centroid(:,1) */
  /* '<S7>:1:6' */
  /* '<S7>:1:7' */
  ImageDet_DW.SFunction_DIMS3.x = 0;

  /* '<S7>:1:8' */
  ImageDet_DW.SFunction_DIMS3.y = 0;

  /* % Coefficients from curve fitting */
  /* % calc position */
  /* '<S7>:1:15' */
  allLights_sizes_idx_1 = inIdx + 1;
  loop_ub = (inIdx + 1) * 3;
  for (i_0 = 0; i_0 < loop_ub; i_0++) {
    allLights_data[i_0] = 0.0F;
  }

  /*  blobIndices */
  if (!(inIdx + 1 == 0)) {
    /* '<S7>:1:17' */
    /* '<S7>:1:18' */
    for (nhIdx = 0; nhIdx <= inIdx; nhIdx++) {
      /* '<S7>:1:18' */
      /* '<S7>:1:19' */
      loop_ub = blobs_elems_sizes.centroid[1];
      for (i_0 = 0; i_0 < loop_ub; i_0++) {
        tmp_sizes[i_0] = i_0;
      }

      /* '<S7>:1:20' */
      tmp[0] = (real32_T)ImageDet_P.Constant6_Value[1] -
        rtb_blobs.centroid[blobIndices_data[nhIdx] - 1];
      tmp[1] = rtb_blobs.centroid[(blobIndices_data[nhIdx] +
        blobs_elems_sizes.centroid[0]) - 1];
      for (i_0 = 0; i_0 < loop_ub; i_0++) {
        rtb_blobs.centroid[(blobIndices_data[nhIdx] +
                            blobs_elems_sizes.centroid[0] * tmp_sizes[i_0]) - 1]
          = tmp[i_0];
      }

      /* '<S7>:1:21' */
      loop_ub = blobs_elems_sizes.centroid[1];
      for (i_0 = 0; i_0 < loop_ub; i_0++) {
        blobs_data[i_0] = rtb_blobs.centroid[(blobs_elems_sizes.centroid[0] *
          i_0 + blobIndices_data[nhIdx]) - 1];
      }

      scale = blobs_data[0] - (real32_T)(ImageDet_P.Constant6_Value[1] / 2.0);
      t = blobs_data[1] - (real32_T)(ImageDet_P.Constant6_Value[0] / 2.0);

      /* '<S7>:1:22' */
      if (scale < 0.0F) {
        absxk = -1.0F;
      } else if (scale > 0.0F) {
        absxk = 1.0F;
      } else if (scale == 0.0F) {
        absxk = 0.0F;
      } else {
        absxk = scale;
      }

      if (t < 0.0F) {
        s = -1.0F;
      } else if (t > 0.0F) {
        s = 1.0F;
      } else if (t == 0.0F) {
        s = 0.0F;
      } else {
        s = t;
      }

      /* '<S7>:1:23' */
      scale = (real32_T)fabs(scale);
      t = (real32_T)fabs(t);

      /* '<S7>:1:24' */
      /* '<S7>:1:25' */
      /* '<S7>:1:26' */
      absxk *= 0.1746F * scale + -0.1615F;
      s *= 0.1746F * t + -0.1615F;
      if (!((!rtIsInfF(absxk)) && (!rtIsNaNF(absxk)))) {
        temp = (rtNaNF);
      } else {
        temp = rt_remf_snf(absxk, 360.0F);
        dist = (real32_T)fabs(temp);
        if (dist > 180.0F) {
          if (temp > 0.0F) {
            temp -= 360.0F;
          } else {
            temp += 360.0F;
          }

          dist = (real32_T)fabs(temp);
        }

        if (dist <= 45.0F) {
          temp *= 0.0174532924F;
          x = 0;
        } else if (dist <= 135.0F) {
          if (temp > 0.0F) {
            temp = (temp - 90.0F) * 0.0174532924F;
            x = 1;
          } else {
            temp = (temp + 90.0F) * 0.0174532924F;
            x = -1;
          }
        } else if (temp > 0.0F) {
          temp = (temp - 180.0F) * 0.0174532924F;
          x = 2;
        } else {
          temp = (temp + 180.0F) * 0.0174532924F;
          x = -2;
        }

        temp = (real32_T)tan(temp);
        if ((x == 1) || (x == -1)) {
          temp = -1.0F / temp;
        }
      }

      absxk = temp;
      if (!((!rtIsInfF(s)) && (!rtIsNaNF(s)))) {
        temp = (rtNaNF);
      } else {
        temp = rt_remf_snf(s, 360.0F);
        dist = (real32_T)fabs(temp);
        if (dist > 180.0F) {
          if (temp > 0.0F) {
            temp -= 360.0F;
          } else {
            temp += 360.0F;
          }

          dist = (real32_T)fabs(temp);
        }

        if (dist <= 45.0F) {
          temp *= 0.0174532924F;
          x = 0;
        } else if (dist <= 135.0F) {
          if (temp > 0.0F) {
            temp = (temp - 90.0F) * 0.0174532924F;
            x = 1;
          } else {
            temp = (temp + 90.0F) * 0.0174532924F;
            x = -1;
          }
        } else if (temp > 0.0F) {
          temp = (temp - 180.0F) * 0.0174532924F;
          x = 2;
        } else {
          temp = (temp + 180.0F) * 0.0174532924F;
          x = -2;
        }

        temp = (real32_T)tan(temp);
        if ((x == 1) || (x == -1)) {
          temp = -1.0F / temp;
        }
      }

      absxk *= (real32_T)ImageDet_P.Constant7_Value;

      /* '<S7>:1:27' */
      i = blobIndices_data[nhIdx] - 1;
      allLights_data[3 * i] = (real32_T)ImageDet_P.Constant7_Value * temp;
      allLights_data[1 + 3 * i] = absxk;
      allLights_data[2 + 3 * i] = rtb_blobs.color[blobIndices_data[nhIdx] - 1];

      /* '<S7>:1:18' */
    }

    if (rtb_blobs.area[blobIndices_data[inIdx] - 1] > 50) {
      /* '<S7>:1:29' */
      /* '<S7>:1:30' */
      ImageDet_DW.SFunction_DIMS3.x = 1;
      scale = rt_roundf_snf(allLights_data[(blobIndices_data[inIdx] - 1) * 3] *
                            100.0F);
      if (scale < 128.0F) {
        if (scale >= -128.0F) {
          rtb_x[0] = (int8_T)scale;
        } else {
          rtb_x[0] = MIN_int8_T;
        }
      } else {
        rtb_x[0] = MAX_int8_T;
      }

      /* '<S7>:1:31' */
      ImageDet_DW.SFunction_DIMS3.y = 1;
      scale = rt_roundf_snf(allLights_data[(blobIndices_data[inIdx] - 1) * 3 + 1]
                            * 100.0F);
      if (scale < 128.0F) {
        if (scale >= -128.0F) {
          rtb_y[0] = (int8_T)scale;
        } else {
          rtb_y[0] = MIN_int8_T;
        }
      } else {
        rtb_y[0] = MAX_int8_T;
      }

      if ((real_T)blobIndices_data[inIdx] + 1.0 > allLights_sizes_idx_1) {
        idxNHood = 0;
        allLights_sizes_idx_1 = 0;
      } else {
        idxNHood = (blobIndices_data[inIdx] + 1) - 1;
      }

      /* '<S7>:1:32' */
      if (1.0 > (real_T)blobIndices_data[inIdx] - 1.0) {
        loop_ub = 0;
      } else {
        loop_ub = (int32_T)((real_T)blobIndices_data[inIdx] - 1.0);
      }

      inIdx = (loop_ub + allLights_sizes_idx_1) - idxNHood;
      for (i_0 = 0; i_0 < loop_ub; i_0++) {
        allLights_data_1[3 * i_0] = allLights_data[3 * i_0];
        allLights_data_1[1 + 3 * i_0] = allLights_data[3 * i_0 + 1];
        allLights_data_1[2 + 3 * i_0] = allLights_data[3 * i_0 + 2];
      }

      i = allLights_sizes_idx_1 - idxNHood;
      for (i_0 = 0; i_0 < i; i_0++) {
        allLights_data_1[3 * (i_0 + loop_ub)] = allLights_data[(idxNHood + i_0) *
          3];
        allLights_data_1[1 + 3 * (i_0 + loop_ub)] = allLights_data[(idxNHood +
          i_0) * 3 + 1];
        allLights_data_1[2 + 3 * (i_0 + loop_ub)] = allLights_data[(idxNHood +
          i_0) * 3 + 2];
      }

      allLights_sizes_idx_1 = inIdx;
      for (i_0 = 0; i_0 < inIdx; i_0++) {
        allLights_data[3 * i_0] = allLights_data_1[3 * i_0];
        allLights_data[1 + 3 * i_0] = allLights_data_1[3 * i_0 + 1];
        allLights_data[2 + 3 * i_0] = allLights_data_1[3 * i_0 + 2];
      }
    }
  }

  /*  allLights */
  /*  create light arrays */
  /* '<S7>:1:38' */
  allLights_sizes_2[0] = 1;
  allLights_sizes_2[1] = allLights_sizes_idx_1;
  for (i_0 = 0; i_0 < allLights_sizes_idx_1; i_0++) {
    allLights_data_0[i_0] = (allLights_data[3 * i_0 + 2] == 1.0F);
  }

  ImageDet_eml_li_find(allLights_data_0, allLights_sizes_2, tmp_data_1,
                       tmp_sizes_0);
  loop_ub = tmp_sizes_0[1];
  for (i_0 = 0; i_0 < loop_ub; i_0++) {
    reds_data[i_0 << 1] = allLights_data[(tmp_data_1[tmp_sizes_0[0] * i_0] - 1) *
      3];
    reds_data[1 + (i_0 << 1)] = allLights_data[(tmp_data_1[tmp_sizes_0[0] * i_0]
      - 1) * 3 + 1];
  }

  /* '<S7>:1:39' */
  allLights_sizes_1[0] = 1;
  allLights_sizes_1[1] = allLights_sizes_idx_1;
  for (i_0 = 0; i_0 < allLights_sizes_idx_1; i_0++) {
    allLights_data_0[i_0] = (allLights_data[3 * i_0 + 2] == 2.0F);
  }

  ImageDet_eml_li_find(allLights_data_0, allLights_sizes_1, tmp_data_1,
                       allLights_sizes_2);
  loop_ub = allLights_sizes_2[1];
  for (i_0 = 0; i_0 < loop_ub; i_0++) {
    blues_data[i_0 << 1] = allLights_data[(tmp_data_1[allLights_sizes_2[0] * i_0]
      - 1) * 3];
    blues_data[1 + (i_0 << 1)] = allLights_data[(tmp_data_1[allLights_sizes_2[0]
      * i_0] - 1) * 3 + 1];
  }

  /*  allLights */
  /* '<S7>:1:41' */
  allLights_sizes_0[0] = 1;
  allLights_sizes_0[1] = allLights_sizes_idx_1;
  for (i_0 = 0; i_0 < allLights_sizes_idx_1; i_0++) {
    allLights_data_0[i_0] = (allLights_data[3 * i_0 + 2] == 1.0F);
  }

  ImageDet_eml_li_find(allLights_data_0, allLights_sizes_0, tmp_data_1,
                       tmp_sizes_0);

  /* '<S7>:1:42' */
  /* '<S7>:1:44' */
  ImageDet_DW.SFunction_DIMS2.x[0] = 0;
  ImageDet_DW.SFunction_DIMS2.x[1] = 1;

  /* '<S7>:1:45' */
  ImageDet_DW.SFunction_DIMS2.y[0] = 0;
  ImageDet_DW.SFunction_DIMS2.y[1] = 1;

  /* '<S7>:1:46' */
  ImageDet_DW.SFunction_DIMS2.orientation[0] = 0;
  ImageDet_DW.SFunction_DIMS2.orientation[1] = 1;

  /* '<S7>:1:47' */
  ImageDet_DW.SFunction_DIMS2.color[0] = 0;
  ImageDet_DW.SFunction_DIMS2.color[1] = 1;

  /*  coder.varsize('players.x',[6 1]); */
  /*  coder.varsize('players.y',[6 1]); */
  /*  coder.varsize('players.orientation',[6 1]); */
  /*  coder.varsize('players.color',[6 1]); */
  allLights_sizes[0] = 1;
  allLights_sizes[1] = allLights_sizes_idx_1;
  for (i_0 = 0; i_0 < allLights_sizes_idx_1; i_0++) {
    allLights_data_0[i_0] = (allLights_data[3 * i_0 + 2] == 1.0F);
  }

  ImageDet_eml_li_find(allLights_data_0, allLights_sizes, tmp_data_1, tmp_sizes);
  if ((tmp_sizes[1] > 0) && (allLights_sizes_2[1] > 0)) {
    /* '<S7>:1:54' */
    /* '<S7>:1:55' */
    /* '<S7>:1:56' */
    for (nhIdx = 0; nhIdx < allLights_sizes_2[1]; nhIdx++) {
      /* '<S7>:1:56' */
      /* '<S7>:1:57' */
      /* '<S7>:1:58' */
      dist = 2.14748365E+9F;

      /* '<S7>:1:59' */
      currentLabel = 0U;

      /* '<S7>:1:60' */
      for (m = 0; m < tmp_sizes_0[1]; m++) {
        /* '<S7>:1:60' */
        /* '<S7>:1:61' */
        /* '<S7>:1:62' */
        scale = 1.17549435E-38F;
        absxk = (real32_T)fabs(blues_data[nhIdx << 1] - reds_data[m << 1]);
        if (absxk > 1.17549435E-38F) {
          temp = 1.0F;
          scale = absxk;
        } else {
          t = absxk / 1.17549435E-38F;
          temp = t * t;
        }

        absxk = (real32_T)fabs(blues_data[(nhIdx << 1) + 1] - reds_data[(m << 1)
          + 1]);
        if (absxk > scale) {
          t = scale / absxk;
          temp = temp * t * t + 1.0F;
          scale = absxk;
        } else {
          t = absxk / scale;
          temp += t * t;
        }

        temp = scale * (real32_T)sqrt(temp);
        if (((real32_T)fabs(temp - 0.04F) < 0.05) && (temp < dist)) {
          /* '<S7>:1:63' */
          /* '<S7>:1:64' */
          dist = temp;

          /* '<S7>:1:65' */
          currentLabel = (uint8_T)(1 + m);
        }

        /* '<S7>:1:60' */
      }

      if (currentLabel != 0) {
        /* '<S7>:1:68' */
        /* '<S7>:1:69' */
        /* '<S7>:1:70' */
        /* '<S7>:1:71' */
        i = ImageDet_DW.SFunction_DIMS2.x[0] + 1;
        loop_ub = ImageDet_DW.SFunction_DIMS2.x[0];
        for (i_0 = 0; i_0 < loop_ub; i_0++) {
          f_data[i_0] = rtb_players_x[i_0];
        }

        scale = rt_roundf_snf(reds_data[(currentLabel - 1) << 1] * 100.0F);
        if (scale < 128.0F) {
          if (scale >= -128.0F) {
            f_data[ImageDet_DW.SFunction_DIMS2.x[0]] = (int8_T)scale;
          } else {
            f_data[ImageDet_DW.SFunction_DIMS2.x[0]] = MIN_int8_T;
          }
        } else {
          f_data[ImageDet_DW.SFunction_DIMS2.x[0]] = MAX_int8_T;
        }

        /* '<S7>:1:72' */
        ImageDet_DW.SFunction_DIMS2.x[0] = i;
        ImageDet_DW.SFunction_DIMS2.x[1] = 1;
        for (i_0 = 0; i_0 < i; i_0++) {
          rtb_players_x[i_0] = f_data[i_0];
        }

        i = ImageDet_DW.SFunction_DIMS2.y[0] + 1;
        loop_ub = ImageDet_DW.SFunction_DIMS2.y[0];
        for (i_0 = 0; i_0 < loop_ub; i_0++) {
          f_data[i_0] = rtb_players_y[i_0];
        }

        scale = rt_roundf_snf(reds_data[((currentLabel - 1) << 1) + 1] * 100.0F);
        if (scale < 128.0F) {
          if (scale >= -128.0F) {
            f_data[ImageDet_DW.SFunction_DIMS2.y[0]] = (int8_T)scale;
          } else {
            f_data[ImageDet_DW.SFunction_DIMS2.y[0]] = MIN_int8_T;
          }
        } else {
          f_data[ImageDet_DW.SFunction_DIMS2.y[0]] = MAX_int8_T;
        }

        /* '<S7>:1:73' */
        ImageDet_DW.SFunction_DIMS2.y[0] = i;
        ImageDet_DW.SFunction_DIMS2.y[1] = 1;
        for (i_0 = 0; i_0 < i; i_0++) {
          rtb_players_y[i_0] = f_data[i_0];
        }

        i = ImageDet_DW.SFunction_DIMS2.orientation[0] + 1;
        loop_ub = ImageDet_DW.SFunction_DIMS2.orientation[0];
        for (i_0 = 0; i_0 < loop_ub; i_0++) {
          g_data[i_0] = rtb_players_orientation[i_0];
        }

        scale = rt_roundf_snf(rt_atan2f_snf(blues_data[(nhIdx << 1) + 1] -
          reds_data[((currentLabel - 1) << 1) + 1], blues_data[nhIdx << 1] -
          reds_data[(currentLabel - 1) << 1]) * 57.2957802F);
        if (scale < 32768.0F) {
          if (scale >= -32768.0F) {
            g_data[ImageDet_DW.SFunction_DIMS2.orientation[0]] = (int16_T)scale;
          } else {
            g_data[ImageDet_DW.SFunction_DIMS2.orientation[0]] = MIN_int16_T;
          }
        } else {
          g_data[ImageDet_DW.SFunction_DIMS2.orientation[0]] = MAX_int16_T;
        }

        /* '<S7>:1:74' */
        ImageDet_DW.SFunction_DIMS2.orientation[0] = i;
        ImageDet_DW.SFunction_DIMS2.orientation[1] = 1;
        for (i_0 = 0; i_0 < i; i_0++) {
          rtb_players_orientation[i_0] = g_data[i_0];
        }

        /* '<S7>:1:75' */
        ImageDet_DW.SFunction_DIMS2.color[0]++;
        ImageDet_DW.SFunction_DIMS2.color[1] = 1;
      }

      /* '<S7>:1:56' */
    }
  }

  /* End of MATLAB Function: '<S1>/parseBlob' */

  /* MATLAB Function: '<S1>/Player Tracker' */
  /* MATLAB Function 'Raspi Code/Player Tracker': '<S5>:1' */
  /*  fGol, fDef, fOff, eGol, eDef, eOff */
  /* Green */
  /* Ball */
  /* % set all valids to false */
  /* '<S5>:1:24' */
  for (i_0 = 0; i_0 < 6; i_0++) {
    ImageDet_DW.latestPlayers.valid[i_0] = 0U;
  }

  /* '<S5>:1:25' */
  ImageDet_DW.latestBall.valid = 0U;
  if (!(ImageDet_DW.SFunction_DIMS3.x == 0)) {
    /* '<S5>:1:27' */
    /* '<S5>:1:28' */
    ImageDet_DW.latestBall.x = rtb_x[0];

    /* '<S5>:1:29' */
    ImageDet_DW.latestBall.y = rtb_y[0];

    /* '<S5>:1:30' */
    ImageDet_DW.latestBall.valid = 1U;
  }

  /* '<S5>:1:33' */
  if ((0 == ImageDet_DW.SFunction_DIMS2.x[0]) || (0 ==
       ImageDet_DW.SFunction_DIMS2.x[1])) {
    i_0 = 0;
  } else if (ImageDet_DW.SFunction_DIMS2.x[0] >= ImageDet_DW.SFunction_DIMS2.x[1])
  {
    i_0 = ImageDet_DW.SFunction_DIMS2.x[0];
  } else {
    i_0 = ImageDet_DW.SFunction_DIMS2.x[1];
  }

  n = i_0 - 1;

  /* '<S5>:1:33' */
  for (nhIdx = 0; nhIdx <= n; nhIdx++) {
    /* '<S5>:1:33' */
    /* '<S5>:1:34' */
    /* '<S5>:1:35' */
    allLights_sizes_idx_1 = MAX_int32_T;

    /* '<S5>:1:36' */
    currentLabel = 0U;

    /* '<S5>:1:37' */
    for (m = 0; m < 6; m++) {
      /* '<S5>:1:37' */
      /* '<S5>:1:38' */
      i_0 = rtb_players_x[nhIdx] - ImageDet_DW.latestPlayers.x[m];
      if (i_0 > 127) {
        i_0 = 127;
      } else {
        if (i_0 < -128) {
          i_0 = -128;
        }
      }

      i_1 = rtb_players_y[nhIdx] - ImageDet_DW.latestPlayers.y[m];
      if (i_1 > 127) {
        i_1 = 127;
      } else {
        if (i_1 < -128) {
          i_1 = -128;
        }
      }

      scale = 1.17549435E-38F;
      idxNHood = (int32_T)(real32_T)fabs((int8_T)i_0);
      if (idxNHood > 1.17549435E-38F) {
        t = 1.17549435E-38F / (real32_T)idxNHood;
        temp = 0.0F * t * t + 1.0F;
        scale = (real32_T)idxNHood;
      } else {
        t = (real32_T)idxNHood / 1.17549435E-38F;
        temp = t * t;
      }

      idxNHood = (int32_T)(real32_T)fabs((int8_T)i_1);
      if (idxNHood > scale) {
        t = scale / (real32_T)idxNHood;
        temp = temp * t * t + 1.0F;
        scale = (real32_T)idxNHood;
      } else {
        t = (real32_T)idxNHood / scale;
        temp += t * t;
      }

      temp = scale * (real32_T)sqrt(temp);
      scale = rt_roundf_snf(temp);
      if (scale < 2.14748365E+9F) {
        if (scale >= -2.14748365E+9F) {
          idxNHood = (int32_T)scale;
        } else {
          idxNHood = MIN_int32_T;
        }
      } else {
        idxNHood = MAX_int32_T;
      }

      if ((idxNHood < 10) && (idxNHood < allLights_sizes_idx_1)) {
        /* '<S5>:1:39' */
        /* '<S5>:1:40' */
        allLights_sizes_idx_1 = idxNHood;

        /* '<S5>:1:41' */
        currentLabel = (uint8_T)(1 + m);
      }

      /* '<S5>:1:37' */
    }

    if (currentLabel != 0) {
      /* '<S5>:1:44' */
      /* '<S5>:1:45' */
      ImageDet_DW.latestPlayers.x[currentLabel - 1] = rtb_players_x[nhIdx];

      /* '<S5>:1:46' */
      ImageDet_DW.latestPlayers.y[currentLabel - 1] = rtb_players_y[nhIdx];

      /* '<S5>:1:47' */
      ImageDet_DW.latestPlayers.orientation[currentLabel - 1] =
        rtb_players_orientation[nhIdx];

      /* '<S5>:1:48' */
      ImageDet_DW.latestPlayers.valid[currentLabel - 1] = 1U;
    }

    /* '<S5>:1:33' */
  }

  /* '<S5>:1:51' */
  ImageDet_B.ball = ImageDet_DW.latestBall;

  /* '<S5>:1:52' */
  /* '<S5>:1:53' */
  /* '<S5>:1:54' */
  /* '<S5>:1:55' */
  /* '<S5>:1:56' */
  /* '<S5>:1:57' */
  /* '<S5>:1:58' */
  /* '<S5>:1:59' */
  for (nhIdx = 0; nhIdx < 6; nhIdx++) {
    /* '<S5>:1:59' */
    /* '<S5>:1:60' */
    ImageDet_B.players[nhIdx].x = ImageDet_DW.latestPlayers.x[nhIdx];

    /* '<S5>:1:61' */
    ImageDet_B.players[nhIdx].y = ImageDet_DW.latestPlayers.y[nhIdx];

    /* '<S5>:1:62' */
    ImageDet_B.players[nhIdx].orientation =
      ImageDet_DW.latestPlayers.orientation[nhIdx];

    /* '<S5>:1:63' */
    ImageDet_B.players[nhIdx].color = ImageDet_DW.latestPlayers.color[nhIdx];

    /* '<S5>:1:64' */
    ImageDet_B.players[nhIdx].position =
      ImageDet_DW.latestPlayers.position[nhIdx];

    /* '<S5>:1:65' */
    ImageDet_B.players[nhIdx].valid = ImageDet_DW.latestPlayers.valid[nhIdx];

    /* '<S5>:1:59' */
  }

  /* End of MATLAB Function: '<S1>/Player Tracker' */

  /* ManualSwitch: '<Root>/Manual Switch' incorporates:
   *  Constant: '<Root>/Constant'
   *  Constant: '<Root>/Constant1'
   */
  if (ImageDet_P.ManualSwitch_CurrentSetting == 1) {
    currentLabel = ImageDet_P.Constant_Value_a;
  } else {
    currentLabel = ImageDet_P.Constant1_Value_d;
  }

  /* End of ManualSwitch: '<Root>/Manual Switch' */

  /* MATLAB Function: '<S1>/encode' */
  /* MATLAB Function 'Raspi Code/encode': '<S6>:1' */
  /* '<S6>:1:2' */
  for (i = 0; i < 27; i++) {
    ImageDet_B.infoToTransmit[i] = 0U;
  }

  /* '<S6>:1:4' */
  x = ImageDet_B.ball.x;
  memcpy(&y, &x, (size_t)1 * sizeof(uint8_T));
  x = ImageDet_B.ball.y;
  memcpy(&b_y, &x, (size_t)1 * sizeof(uint8_T));
  ImageDet_B.infoToTransmit[0] = currentLabel;
  ImageDet_B.infoToTransmit[1] = y;
  ImageDet_B.infoToTransmit[2] = b_y;

  /* '<S6>:1:8' */
  for (nhIdx = 0; nhIdx < 6; nhIdx++) {
    /* '<S6>:1:8' */
    /* '<S6>:1:9' */
    x = ImageDet_B.players[nhIdx].x;
    memcpy(&currentLabel, &x, (size_t)1 * sizeof(uint8_T));
    x = ImageDet_B.players[nhIdx].y;
    memcpy(&y, &x, (size_t)1 * sizeof(uint8_T));
    e_x = ImageDet_B.players[nhIdx].orientation;
    memcpy(&e_y[0], &e_x, (size_t)2 * sizeof(uint8_T));
    i_0 = ((1 + nhIdx) << 2) - 1;
    ImageDet_B.infoToTransmit[i_0] = currentLabel;
    ImageDet_B.infoToTransmit[1 + i_0] = y;
    ImageDet_B.infoToTransmit[2 + i_0] = e_y[0];
    ImageDet_B.infoToTransmit[3 + i_0] = e_y[1];

    /* '<S6>:1:8' */
  }

  /* End of MATLAB Function: '<S1>/encode' */

  /* MATLAB Function: '<S2>/MATLAB Function' */
  /*  len=length(infoToTransmit) */
  /* MATLAB Function 'Subsystem/MATLAB Function': '<S15>:1' */
  /*  infoToTransmit=uint8('1234567890qwertyuiopasdfghj'); */
  /* '<S15>:1:10' */
  if (ImageDet_DW.Psend == 0) {
    /* '<S15>:1:11' */
    /* '<S15>:1:12' */
    ImageDet_DW.Psend = 1U;
  } else {
    /* '<S15>:1:14' */
    ImageDet_DW.Psend = 0U;
  }

  /* Outputs for Atomic SubSystem: '<S16>/Function-Call Subsystem' */
  /* MATLAB Function: '<S17>/SerialWrite' incorporates:
   *  MATLAB Function: '<S2>/MATLAB Function'
   */
  /* '<S15>:1:16' */
  for (i_0 = 0; i_0 < 27; i_0++) {
    dataOut[i_0] = ImageDet_B.infoToTransmit[i_0];
  }

  /* End of Outputs for SubSystem: '<S16>/Function-Call Subsystem' */

  /* ManualSwitch: '<S2>/Manual Switch' incorporates:
   *  Constant: '<S2>/Constant'
   *  Constant: '<S2>/Constant1'
   */
  /* MATLAB Function 'Subsystem/SerialCom/Function-Call Subsystem/SerialWrite': '<S19>:1' */
  /* '<S19>:1:3' */
  if (ImageDet_P.ManualSwitch_CurrentSetting_e == 1) {
    tmp_0 = ImageDet_P.Constant_Value;
  } else {
    tmp_0 = ImageDet_P.Constant1_Value_j;
  }

  /* End of ManualSwitch: '<S2>/Manual Switch' */

  /* Outputs for Atomic SubSystem: '<S16>/Function-Call Subsystem' */
  /* MATLAB Function: '<S17>/SerialWrite' incorporates:
   *  MATLAB Function: '<S2>/MATLAB Function'
   *  Product: '<S2>/Product'
   */
  if ((real_T)ImageDet_DW.Psend * tmp_0 > 0.0) {
    /* '<S19>:1:4' */
    write(ImageDet_DW.spHandle, dataOut, 27U);
  }

  /* End of Outputs for SubSystem: '<S16>/Function-Call Subsystem' */

  /* MATLAB Function: '<S16>/SerialInitilization' */
  /* MATLAB Function 'Subsystem/SerialCom/SerialInitilization': '<S18>:1' */
  /* '<S18>:1:6' */
  if (ImageDet_DW.spHandle == -1) {
    /* '<S18>:1:4' */
    /* '<S18>:1:5' */
    ImageDet_DW.spHandle = 0;

    /* 57600 */
    /* '<S18>:1:6' */
    for (i_0 = 0; i_0 < 13; i_0++) {
      b_0[i_0] = b[i_0];
    }

    ImageDet_DW.spHandle = setupSerialPort(b_0, 9600U);
  }

  /* End of MATLAB Function: '<S16>/SerialInitilization' */

  /* BusSelector: '<S1>/Bus Selector2' */
  ImageDet_DW.BusSelector2_DIMS1 = ImageDet_DW.SFunction_DIMS3.x;
  ImageDet_DW.BusSelector2_DIMS2 = ImageDet_DW.SFunction_DIMS3.y;

  /* SignalConversion: '<S1>/SigConversion_InsertedFor_Bus Selector2_at_outport_0' */
  ImageDet_DW.SigConversion_InsertedFor_BusSe = ImageDet_DW.BusSelector2_DIMS1;
  for (i_0 = 0; i_0 < ImageDet_DW.BusSelector2_DIMS1; i_0++) {
    ImageDet_B.x[i_0] = rtb_x[i_0];
  }

  /* End of SignalConversion: '<S1>/SigConversion_InsertedFor_Bus Selector2_at_outport_0' */

  /* SignalConversion: '<S1>/SigConversion_InsertedFor_Bus Selector2_at_outport_1' */
  ImageDet_DW.SigConversion_InsertedFor_Bus_k = ImageDet_DW.BusSelector2_DIMS2;
  for (i_0 = 0; i_0 < ImageDet_DW.BusSelector2_DIMS2; i_0++) {
    ImageDet_B.y[i_0] = rtb_y[i_0];
  }

  /* End of SignalConversion: '<S1>/SigConversion_InsertedFor_Bus Selector2_at_outport_1' */

  /* ManualSwitch: '<S3>/Manual Switch1' incorporates:
   *  Constant: '<S3>/Constant1'
   *  Constant: '<S3>/Constant2'
   */
  if (ImageDet_P.ManualSwitch1_CurrentSetting == 1) {
    tmp_0 = ImageDet_P.Constant1_Value;
  } else {
    tmp_0 = ImageDet_P.Constant2_Value;
  }

  /* End of ManualSwitch: '<S3>/Manual Switch1' */

  /* Outputs for Enabled SubSystem: '<S3>/Subsystem' incorporates:
   *  EnablePort: '<S11>/Enable'
   */
  if (tmp_0 > 0.0) {
    if (!ImageDet_DW.Subsystem_MODE) {
      ImageDet_DW.Subsystem_MODE = true;
    }

    /* MATLAB Function: '<S11>/FindBlobs1' */
    /* MATLAB Function 'Raspi Code/Blob extraction /Subsystem/FindBlobs1': '<S14>:1' */
    /*  area */
    /* '<S14>:1:4' */
    ImageDet_B.lengthh = ImageDet_DW.BlobAnalysis2_DIMS1[0];
    srUpdateBC(ImageDet_DW.Subsystem_SubsysRanBC);
  } else {
    if (ImageDet_DW.Subsystem_MODE) {
      ImageDet_DW.Subsystem_MODE = false;
    }
  }

  /* End of Outputs for SubSystem: '<S3>/Subsystem' */

  /* ManualSwitch: '<S3>/Manual Switch2' incorporates:
   *  Constant: '<S3>/Constant8'
   *  Constant: '<S3>/Constant9'
   */
  if (ImageDet_P.ManualSwitch2_CurrentSetting == 1) {
    tmp_0 = ImageDet_P.Constant8_Value;
  } else {
    tmp_0 = ImageDet_P.Constant9_Value;
  }

  /* End of ManualSwitch: '<S3>/Manual Switch2' */

  /* Outputs for Enabled SubSystem: '<S3>/Subsystem2' incorporates:
   *  EnablePort: '<S12>/Enable'
   */
  if (tmp_0 > 0.0) {
    if (!ImageDet_DW.Subsystem2_MODE) {
      ImageDet_DW.Subsystem2_MODE = true;
    }

    srUpdateBC(ImageDet_DW.Subsystem2_SubsysRanBC);
  } else {
    if (ImageDet_DW.Subsystem2_MODE) {
      ImageDet_DW.Subsystem2_MODE = false;
    }
  }

  /* End of Outputs for SubSystem: '<S3>/Subsystem2' */

  /* Chart: '<S1>/Chart' */
  /* Gateway: Raspi Code/Chart */
  ImageDet_DW.sfEvent = -1;

  /* During: Raspi Code/Chart */
  if (ImageDet_DW.is_active_c10_ImageDet == 0U) {
    /* Entry: Raspi Code/Chart */
    ImageDet_DW.is_active_c10_ImageDet = 1U;

    /* Entry Internal: Raspi Code/Chart */
    /* Transition: '<S4>:11' */
    ImageDet_DW.is_c10_ImageDet = ImageDet_IN_notOn;

    /* Entry 'notOn': '<S4>:5' */
    ImageDet_DW.idleTicks = 0U;
  } else if (ImageDet_DW.is_c10_ImageDet == ImageDet_IN_gameIsOn) {
    /* During 'gameIsOn': '<S4>:4' */
    if (ImageDet_calcGoal() == 1) {
      /* Transition: '<S4>:10' */
      ImageDet_DW.is_c10_ImageDet = ImageDet_IN_notOn;

      /* Entry 'notOn': '<S4>:5' */
      ImageDet_DW.idleTicks = 0U;
    }
  } else {
    /* During 'notOn': '<S4>:5' */
    if (ImageDet_DW.idleTicks > 10) {
      /* Transition: '<S4>:12' */
      ImageDet_DW.is_c10_ImageDet = ImageDet_IN_gameIsOn;

      /* Entry 'gameIsOn': '<S4>:4' */
    } else {
      i_0 = (int32_T)((uint32_T)ImageDet_DW.idleTicks + ImageDet_calcReady());
      if ((uint32_T)i_0 > 255U) {
        i_0 = 255;
      }

      ImageDet_DW.idleTicks = (uint8_T)i_0;
    }
  }

  /* End of Chart: '<S1>/Chart' */
}

/* Model update function */
void ImageDet_update(void)
{
  /* Update for Enabled SubSystem: '<S3>/Subsystem2' incorporates:
   *  Update for EnablePort: '<S12>/Enable'
   */
  if (ImageDet_DW.Subsystem2_MODE) {
  }

  /* End of Update for SubSystem: '<S3>/Subsystem2' */

  /* signal main to stop simulation */
  {                                    /* Sample time: [0.1s, 0.0s] */
    if ((rtmGetTFinal(ImageDet_M)!=-1) &&
        !((rtmGetTFinal(ImageDet_M)-ImageDet_M->Timing.taskTime0) >
          ImageDet_M->Timing.taskTime0 * (DBL_EPSILON))) {
      rtmSetErrorStatus(ImageDet_M, "Simulation finished");
    }

    if (rtmGetStopRequested(ImageDet_M)) {
      rtmSetErrorStatus(ImageDet_M, "Simulation finished");
    }
  }

  /* Update absolute time for base rate */
  /* The "clockTick0" counts the number of times the code of this task has
   * been executed. The absolute time is the multiplication of "clockTick0"
   * and "Timing.stepSize0". Size of "clockTick0" ensures timer will not
   * overflow during the application lifespan selected.
   */
  ImageDet_M->Timing.taskTime0 =
    (++ImageDet_M->Timing.clockTick0) * ImageDet_M->Timing.stepSize0;
}

/* Model initialize function */
void ImageDet_initialize(void)
{
  /* Registration code */

  /* initialize non-finites */
  rt_InitInfAndNaN(sizeof(real_T));

  /* initialize real-time model */
  (void) memset((void *)ImageDet_M, 0,
                sizeof(RT_MODEL_ImageDet_T));
  rtmSetTFinal(ImageDet_M, -1);
  ImageDet_M->Timing.stepSize0 = 0.1;

  /* External mode info */
  ImageDet_M->Sizes.checksums[0] = (2267689008U);
  ImageDet_M->Sizes.checksums[1] = (784342539U);
  ImageDet_M->Sizes.checksums[2] = (1985804768U);
  ImageDet_M->Sizes.checksums[3] = (3752731953U);

  {
    static const sysRanDType rtAlwaysEnabled = SUBSYS_RAN_BC_ENABLE;
    static RTWExtModeInfo rt_ExtModeInfo;
    static const sysRanDType *systemRan[22];
    ImageDet_M->extModeInfo = (&rt_ExtModeInfo);
    rteiSetSubSystemActiveVectorAddresses(&rt_ExtModeInfo, systemRan);
    systemRan[0] = &rtAlwaysEnabled;
    systemRan[1] = &rtAlwaysEnabled;
    systemRan[2] = &rtAlwaysEnabled;
    systemRan[3] = &rtAlwaysEnabled;
    systemRan[4] = &rtAlwaysEnabled;
    systemRan[5] = &rtAlwaysEnabled;
    systemRan[6] = &rtAlwaysEnabled;
    systemRan[7] = &rtAlwaysEnabled;
    systemRan[8] = &rtAlwaysEnabled;
    systemRan[9] = (sysRanDType *)&ImageDet_DW.Subsystem_SubsysRanBC;
    systemRan[10] = (sysRanDType *)&ImageDet_DW.Subsystem_SubsysRanBC;
    systemRan[11] = (sysRanDType *)&ImageDet_DW.Subsystem2_SubsysRanBC;
    systemRan[12] = &rtAlwaysEnabled;
    systemRan[13] = &rtAlwaysEnabled;
    systemRan[14] = &rtAlwaysEnabled;
    systemRan[15] = &rtAlwaysEnabled;
    systemRan[16] = &rtAlwaysEnabled;
    systemRan[17] = &rtAlwaysEnabled;
    systemRan[18] = &rtAlwaysEnabled;
    systemRan[19] = &rtAlwaysEnabled;
    systemRan[20] = &rtAlwaysEnabled;
    systemRan[21] = &rtAlwaysEnabled;
    rteiSetModelMappingInfoPtr(ImageDet_M->extModeInfo,
      &ImageDet_M->SpecialInfo.mappingInfo);
    rteiSetChecksumsPtr(ImageDet_M->extModeInfo, ImageDet_M->Sizes.checksums);
    rteiSetTPtr(ImageDet_M->extModeInfo, rtmGetTPtr(ImageDet_M));
  }

  /* block I/O */
  (void) memset(((void *) &ImageDet_B), 0,
                sizeof(B_ImageDet_T));

  /* states (dwork) */
  (void) memset((void *)&ImageDet_DW, 0,
                sizeof(DW_ImageDet_T));

  /* data type transition information */
  {
    static DataTypeTransInfo dtInfo;
    (void) memset((char_T *) &dtInfo, 0,
                  sizeof(dtInfo));
    ImageDet_M->SpecialInfo.mappingInfo = (&dtInfo);
    dtInfo.numDataTypes = 23;
    dtInfo.dataTypeSizes = &rtDataTypeSizes[0];
    dtInfo.dataTypeNames = &rtDataTypeNames[0];

    /* Block I/O transition table */
    dtInfo.B = &rtBTransTable;

    /* Parameters transition table */
    dtInfo.P = &rtPTransTable;
  }

  {
    static const int8_T b[6] = { -20, -50, -80, 20, 50, 80 };

    static const uint8_T c[6] = { 0U, 0U, 0U, 180U, 180U, 180U };

    static const uint8_T d[6] = { 103U, 103U, 103U, 98U, 98U, 98U };

    static const uint8_T e[6] = { 111U, 100U, 103U, 111U, 100U, 103U };

    int32_T idxOffsets;
    int32_T curNumNonZ;
    int32_T n;
    int32_T m;

    /* Start for S-Function (v4l2_video_capture_sfcn): '<Root>/V4L2 Video Capture' */
    MW_videoCaptureInit(ImageDet_ConstP.V4L2VideoCapture_p1, 0, 0, 0, 0, 320U,
                        240U, 2U, 2U, 1U, 0.1);

    /* Start for S-Function (svipmorphop): '<S3>/Erosion3' */
    idxOffsets = 0;
    curNumNonZ = 0;
    n = 0;
    while (n < 1) {
      for (m = 0; m < 4; m++) {
        ImageDet_DW.Erosion3_ERODE_OFF_DW[idxOffsets] = m;
        curNumNonZ++;
        idxOffsets++;
      }

      n = 1;
    }

    ImageDet_DW.Erosion3_NUMNONZ_DW[0] = curNumNonZ;
    curNumNonZ = 0;
    for (n = 0; n < 4; n++) {
      m = 0;
      while (m < 1) {
        ImageDet_DW.Erosion3_ERODE_OFF_DW[idxOffsets] = n * 327;
        curNumNonZ++;
        idxOffsets++;
        m = 1;
      }
    }

    ImageDet_DW.Erosion3_NUMNONZ_DW[1] = curNumNonZ;

    /* End of Start for S-Function (svipmorphop): '<S3>/Erosion3' */

    /* Start for DataStoreMemory: '<S16>/Data Store Memory' */
    ImageDet_DW.spHandle = ImageDet_P.DataStoreMemory_InitialValue;

    /* InitializeConditions for MATLAB Function: '<S1>/Player Tracker' */
    for (idxOffsets = 0; idxOffsets < 6; idxOffsets++) {
      ImageDet_DW.latestPlayers.x[idxOffsets] = b[idxOffsets];
      ImageDet_DW.latestPlayers.y[idxOffsets] = 0;
      ImageDet_DW.latestPlayers.orientation[idxOffsets] = c[idxOffsets];
      ImageDet_DW.latestPlayers.color[idxOffsets] = d[idxOffsets];
      ImageDet_DW.latestPlayers.position[idxOffsets] = e[idxOffsets];
      ImageDet_DW.latestPlayers.valid[idxOffsets] = 0U;
    }

    ImageDet_DW.latestBall.x = 0;
    ImageDet_DW.latestBall.y = 0;
    ImageDet_DW.latestBall.valid = 1U;

    /* End of InitializeConditions for MATLAB Function: '<S1>/Player Tracker' */

    /* InitializeConditions for MATLAB Function: '<S2>/MATLAB Function' */
    ImageDet_DW.Psend = 0U;

    /* InitializeConditions for Chart: '<S1>/Chart' */
    ImageDet_DW.sfEvent = -1;
    ImageDet_DW.is_active_c10_ImageDet = 0U;
    ImageDet_DW.is_c10_ImageDet = ImageDet_IN_NO_ACTIVE_CHILD;
  }
}

/* Model terminate function */
void ImageDet_terminate(void)
{
  /* Terminate for S-Function (v4l2_video_capture_sfcn): '<Root>/V4L2 Video Capture' */
  MW_videoCaptureTerminate(ImageDet_ConstP.V4L2VideoCapture_p1);
}

/*
 * File trailer for generated code.
 *
 * [EOF]
 */

/*
 * File: ledTEST_data.c
 *
 * Code generated for Simulink model 'ledTEST'.
 *
 * Model version                  : 1.14
 * Simulink Coder version         : 8.7 (R2014b) 08-Sep-2014
 * TLC version                    : 8.7 (Aug  5 2014)
 * C/C++ source code generated on : Wed Mar 04 18:23:35 2015
 *
 * Target selection: realtime.tlc
 * Embedded hardware selection: Atmel->AVR
 * Code generation objectives: Unspecified
 * Validation result: Not run
 */

#include "ledTEST.h"
#include "ledTEST_private.h"

/* Block parameters (auto storage) */
P_ledTEST_T ledTEST_P = {
  0U                                   /* Computed Parameter: Constant_Value
                                        * Referenced by: '<S1>/Constant'
                                        */
};

/*
 * File trailer for generated code.
 *
 * [EOF]
 */

/*#include "FastLED.h"

CRGB leds[2];*/
int FW_A=10;
int BK_A=9;

int FW_B=6;
int BK_B=5;

void myInit() {
FastLED.addLeds<NEOPIXEL, 3, RGB>(leds, 2);
 /* leds[1].red=0;
  leds[1].blue=128;
  leds[1].green=128;  
  leds[0].red=255;
  leds[0].blue=0;
  leds[0].green=0; 
  FastLED.show(); */
 /*Serial.begin(9600);*/
}
int checkDATA(char *data)
{ 
  while (Serial.available() > 0) {  
    char inByte = Serial.read();
    if(inByte=='h'){
      while (Serial.available() == 0);
      char inByte = Serial.read();
      if(inByte=='e'){
        while (Serial.available() == 0);
        char inByte = Serial.read();
        if(inByte=='a'){
          while (Serial.available() == 0);
          char inByte = Serial.read();
          if(inByte=='d'){
            while (Serial.available() < 27);
            for (int i = 0; i < 27; i++){
              data[i] = Serial.read();           
            } 
            return(1);
          }
        }
      }
    }
  }
return(0);
}

void drive(int left, int leftSign,int right, int rightSign){   
    if(leftSign>0){
        analogWrite(FW_A,left);
        analogWrite(BK_A,0);
    }else{
        analogWrite(FW_A,0);
        analogWrite(BK_A,left);
    }
    
    if(rightSign>0){
        analogWrite(FW_A,right);
        analogWrite(BK_A,0);
    }else{
        analogWrite(FW_A,0);
        analogWrite(BK_A,right);
    }    
}
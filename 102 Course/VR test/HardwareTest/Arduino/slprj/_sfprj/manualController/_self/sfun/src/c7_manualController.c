/* Include files */

#include <stddef.h>
#include "blas.h"
#include "manualController_sfun.h"
#include "c7_manualController.h"
#include <string.h>
#define CHARTINSTANCE_CHARTNUMBER      (chartInstance->chartNumber)
#define CHARTINSTANCE_INSTANCENUMBER   (chartInstance->instanceNumber)
#include "manualController_sfun_debug_macros.h"
#define _SF_MEX_LISTEN_FOR_CTRL_C(S)   sf_mex_listen_for_ctrl_c(sfGlobalDebugInstanceStruct,S);

/* Type Definitions */

/* Named Constants */
#define CALL_EVENT                     (-1)

/* Variable Declarations */

/* Variable Definitions */
static real_T _sfTime_;
static const char * c7_debug_family_names[12] = { "tempEmpty", "playerInfo",
  "colors", "positions", "ii", "nargin", "nargout", "newMessage", "Ball",
  "players", "gameOn", "oldMes" };

/* Function Declarations */
static void initialize_c7_manualController(SFc7_manualControllerInstanceStruct
  *chartInstance);
static void initialize_params_c7_manualController
  (SFc7_manualControllerInstanceStruct *chartInstance);
static void enable_c7_manualController(SFc7_manualControllerInstanceStruct
  *chartInstance);
static void disable_c7_manualController(SFc7_manualControllerInstanceStruct
  *chartInstance);
static void c7_update_debugger_state_c7_manualController
  (SFc7_manualControllerInstanceStruct *chartInstance);
static const mxArray *get_sim_state_c7_manualController
  (SFc7_manualControllerInstanceStruct *chartInstance);
static void set_sim_state_c7_manualController
  (SFc7_manualControllerInstanceStruct *chartInstance, const mxArray *c7_st);
static void finalize_c7_manualController(SFc7_manualControllerInstanceStruct
  *chartInstance);
static void sf_gateway_c7_manualController(SFc7_manualControllerInstanceStruct
  *chartInstance);
static void mdl_start_c7_manualController(SFc7_manualControllerInstanceStruct
  *chartInstance);
static void c7_chartstep_c7_manualController(SFc7_manualControllerInstanceStruct
  *chartInstance);
static void initSimStructsc7_manualController
  (SFc7_manualControllerInstanceStruct *chartInstance);
static void init_script_number_translation(uint32_T c7_machineNumber, uint32_T
  c7_chartNumber, uint32_T c7_instanceNumber);
static const mxArray *c7_sf_marshallOut(void *chartInstanceVoid, void *c7_inData);
static void c7_emlrt_marshallIn(SFc7_manualControllerInstanceStruct
  *chartInstance, const mxArray *c7_b_oldMes, const char_T *c7_identifier,
  uint8_T c7_y[27]);
static void c7_b_emlrt_marshallIn(SFc7_manualControllerInstanceStruct
  *chartInstance, const mxArray *c7_u, const emlrtMsgIdentifier *c7_parentId,
  uint8_T c7_y[27]);
static void c7_sf_marshallIn(void *chartInstanceVoid, const mxArray
  *c7_mxArrayInData, const char_T *c7_varName, void *c7_outData);
static const mxArray *c7_b_sf_marshallOut(void *chartInstanceVoid, void
  *c7_inData);
static uint8_T c7_c_emlrt_marshallIn(SFc7_manualControllerInstanceStruct
  *chartInstance, const mxArray *c7_b_gameOn, const char_T *c7_identifier);
static uint8_T c7_d_emlrt_marshallIn(SFc7_manualControllerInstanceStruct
  *chartInstance, const mxArray *c7_u, const emlrtMsgIdentifier *c7_parentId);
static void c7_b_sf_marshallIn(void *chartInstanceVoid, const mxArray
  *c7_mxArrayInData, const char_T *c7_varName, void *c7_outData);
static const mxArray *c7_emlrt_marshallOut(SFc7_manualControllerInstanceStruct
  *chartInstance, const c7_Player c7_u[6]);
static const mxArray *c7_c_sf_marshallOut(void *chartInstanceVoid, void
  *c7_inData);
static void c7_e_emlrt_marshallIn(SFc7_manualControllerInstanceStruct
  *chartInstance, const mxArray *c7_b_players, const char_T *c7_identifier,
  c7_Player c7_y[6]);
static void c7_f_emlrt_marshallIn(SFc7_manualControllerInstanceStruct
  *chartInstance, const mxArray *c7_u, const emlrtMsgIdentifier *c7_parentId,
  c7_Player c7_y[6]);
static int8_T c7_g_emlrt_marshallIn(SFc7_manualControllerInstanceStruct
  *chartInstance, const mxArray *c7_u, const emlrtMsgIdentifier *c7_parentId);
static int16_T c7_h_emlrt_marshallIn(SFc7_manualControllerInstanceStruct
  *chartInstance, const mxArray *c7_u, const emlrtMsgIdentifier *c7_parentId);
static void c7_c_sf_marshallIn(void *chartInstanceVoid, const mxArray
  *c7_mxArrayInData, const char_T *c7_varName, void *c7_outData);
static const mxArray *c7_d_sf_marshallOut(void *chartInstanceVoid, void
  *c7_inData);
static c7_Ball c7_i_emlrt_marshallIn(SFc7_manualControllerInstanceStruct
  *chartInstance, const mxArray *c7_c_Ball, const char_T *c7_identifier);
static c7_Ball c7_j_emlrt_marshallIn(SFc7_manualControllerInstanceStruct
  *chartInstance, const mxArray *c7_u, const emlrtMsgIdentifier *c7_parentId);
static void c7_d_sf_marshallIn(void *chartInstanceVoid, const mxArray
  *c7_mxArrayInData, const char_T *c7_varName, void *c7_outData);
static const mxArray *c7_e_sf_marshallOut(void *chartInstanceVoid, void
  *c7_inData);
static const mxArray *c7_f_sf_marshallOut(void *chartInstanceVoid, void
  *c7_inData);
static real_T c7_k_emlrt_marshallIn(SFc7_manualControllerInstanceStruct
  *chartInstance, const mxArray *c7_u, const emlrtMsgIdentifier *c7_parentId);
static void c7_e_sf_marshallIn(void *chartInstanceVoid, const mxArray
  *c7_mxArrayInData, const char_T *c7_varName, void *c7_outData);
static const mxArray *c7_g_sf_marshallOut(void *chartInstanceVoid, void
  *c7_inData);
static void c7_l_emlrt_marshallIn(SFc7_manualControllerInstanceStruct
  *chartInstance, const mxArray *c7_u, const emlrtMsgIdentifier *c7_parentId,
  uint8_T c7_y[6]);
static void c7_f_sf_marshallIn(void *chartInstanceVoid, const mxArray
  *c7_mxArrayInData, const char_T *c7_varName, void *c7_outData);
static const mxArray *c7_h_sf_marshallOut(void *chartInstanceVoid, void
  *c7_inData);
static void c7_m_emlrt_marshallIn(SFc7_manualControllerInstanceStruct
  *chartInstance, const mxArray *c7_u, const emlrtMsgIdentifier *c7_parentId,
  uint8_T c7_y[24]);
static void c7_g_sf_marshallIn(void *chartInstanceVoid, const mxArray
  *c7_mxArrayInData, const char_T *c7_varName, void *c7_outData);
static const mxArray *c7_i_sf_marshallOut(void *chartInstanceVoid, void
  *c7_inData);
static c7_Player c7_n_emlrt_marshallIn(SFc7_manualControllerInstanceStruct
  *chartInstance, const mxArray *c7_u, const emlrtMsgIdentifier *c7_parentId);
static void c7_h_sf_marshallIn(void *chartInstanceVoid, const mxArray
  *c7_mxArrayInData, const char_T *c7_varName, void *c7_outData);
static void c7_info_helper(const mxArray **c7_info);
static const mxArray *c7_b_emlrt_marshallOut(const char * c7_u);
static const mxArray *c7_c_emlrt_marshallOut(const uint32_T c7_u);
static int8_T c7_typecast(SFc7_manualControllerInstanceStruct *chartInstance,
  uint8_T c7_x);
static const mxArray *c7_j_sf_marshallOut(void *chartInstanceVoid, void
  *c7_inData);
static int32_T c7_o_emlrt_marshallIn(SFc7_manualControllerInstanceStruct
  *chartInstance, const mxArray *c7_u, const emlrtMsgIdentifier *c7_parentId);
static void c7_i_sf_marshallIn(void *chartInstanceVoid, const mxArray
  *c7_mxArrayInData, const char_T *c7_varName, void *c7_outData);
static const mxArray *c7_Ball_bus_io(void *chartInstanceVoid, void *c7_pData);
static const mxArray *c7_players_bus_io(void *chartInstanceVoid, void *c7_pData);
static void init_dsm_address_info(SFc7_manualControllerInstanceStruct
  *chartInstance);
static void init_simulink_io_address(SFc7_manualControllerInstanceStruct
  *chartInstance);

/* Function Definitions */
static void initialize_c7_manualController(SFc7_manualControllerInstanceStruct
  *chartInstance)
{
  chartInstance->c7_sfEvent = CALL_EVENT;
  _sfTime_ = sf_get_time(chartInstance->S);
  chartInstance->c7_oldMes_not_empty = false;
  chartInstance->c7_is_active_c7_manualController = 0U;
}

static void initialize_params_c7_manualController
  (SFc7_manualControllerInstanceStruct *chartInstance)
{
  (void)chartInstance;
}

static void enable_c7_manualController(SFc7_manualControllerInstanceStruct
  *chartInstance)
{
  _sfTime_ = sf_get_time(chartInstance->S);
}

static void disable_c7_manualController(SFc7_manualControllerInstanceStruct
  *chartInstance)
{
  _sfTime_ = sf_get_time(chartInstance->S);
}

static void c7_update_debugger_state_c7_manualController
  (SFc7_manualControllerInstanceStruct *chartInstance)
{
  (void)chartInstance;
}

static const mxArray *get_sim_state_c7_manualController
  (SFc7_manualControllerInstanceStruct *chartInstance)
{
  const mxArray *c7_st;
  const mxArray *c7_y = NULL;
  const mxArray *c7_b_y = NULL;
  int8_T c7_u;
  const mxArray *c7_c_y = NULL;
  int8_T c7_b_u;
  const mxArray *c7_d_y = NULL;
  uint8_T c7_c_u;
  const mxArray *c7_e_y = NULL;
  uint8_T c7_hoistedGlobal;
  uint8_T c7_d_u;
  const mxArray *c7_f_y = NULL;
  int32_T c7_i0;
  c7_Player c7_rv0[6];
  int32_T c7_i1;
  uint8_T c7_e_u[27];
  const mxArray *c7_g_y = NULL;
  uint8_T c7_b_hoistedGlobal;
  uint8_T c7_f_u;
  const mxArray *c7_h_y = NULL;
  c7_st = NULL;
  c7_st = NULL;
  c7_y = NULL;
  sf_mex_assign(&c7_y, sf_mex_createcellmatrix(5, 1), false);
  c7_b_y = NULL;
  sf_mex_assign(&c7_b_y, sf_mex_createstruct("structure", 2, 1, 1), false);
  c7_u = *(int8_T *)&((char_T *)chartInstance->c7_b_Ball)[0];
  c7_c_y = NULL;
  sf_mex_assign(&c7_c_y, sf_mex_create("y", &c7_u, 2, 0U, 0U, 0U, 0), false);
  sf_mex_addfield(c7_b_y, c7_c_y, "x", "x", 0);
  c7_b_u = *(int8_T *)&((char_T *)chartInstance->c7_b_Ball)[1];
  c7_d_y = NULL;
  sf_mex_assign(&c7_d_y, sf_mex_create("y", &c7_b_u, 2, 0U, 0U, 0U, 0), false);
  sf_mex_addfield(c7_b_y, c7_d_y, "y", "y", 0);
  c7_c_u = *(uint8_T *)&((char_T *)chartInstance->c7_b_Ball)[2];
  c7_e_y = NULL;
  sf_mex_assign(&c7_e_y, sf_mex_create("y", &c7_c_u, 3, 0U, 0U, 0U, 0), false);
  sf_mex_addfield(c7_b_y, c7_e_y, "valid", "valid", 0);
  sf_mex_setcell(c7_y, 0, c7_b_y);
  c7_hoistedGlobal = *chartInstance->c7_gameOn;
  c7_d_u = c7_hoistedGlobal;
  c7_f_y = NULL;
  sf_mex_assign(&c7_f_y, sf_mex_create("y", &c7_d_u, 3, 0U, 0U, 0U, 0), false);
  sf_mex_setcell(c7_y, 1, c7_f_y);
  for (c7_i0 = 0; c7_i0 < 6; c7_i0++) {
    c7_rv0[c7_i0].x = *(int8_T *)&((char_T *)(c7_Player *)&((char_T *)
      chartInstance->c7_players)[8 * c7_i0])[0];
    c7_rv0[c7_i0].y = *(int8_T *)&((char_T *)(c7_Player *)&((char_T *)
      chartInstance->c7_players)[8 * c7_i0])[1];
    c7_rv0[c7_i0].orientation = *(int16_T *)&((char_T *)(c7_Player *)&((char_T *)
      chartInstance->c7_players)[8 * c7_i0])[2];
    c7_rv0[c7_i0].color = *(uint8_T *)&((char_T *)(c7_Player *)&((char_T *)
      chartInstance->c7_players)[8 * c7_i0])[4];
    c7_rv0[c7_i0].position = *(uint8_T *)&((char_T *)(c7_Player *)&((char_T *)
      chartInstance->c7_players)[8 * c7_i0])[5];
    c7_rv0[c7_i0].valid = *(uint8_T *)&((char_T *)(c7_Player *)&((char_T *)
      chartInstance->c7_players)[8 * c7_i0])[6];
  }

  sf_mex_setcell(c7_y, 2, c7_emlrt_marshallOut(chartInstance, c7_rv0));
  for (c7_i1 = 0; c7_i1 < 27; c7_i1++) {
    c7_e_u[c7_i1] = chartInstance->c7_oldMes[c7_i1];
  }

  c7_g_y = NULL;
  if (!chartInstance->c7_oldMes_not_empty) {
    sf_mex_assign(&c7_g_y, sf_mex_create("y", NULL, 0, 0U, 1U, 0U, 2, 0, 0),
                  false);
  } else {
    sf_mex_assign(&c7_g_y, sf_mex_create("y", c7_e_u, 3, 0U, 1U, 0U, 2, 1, 27),
                  false);
  }

  sf_mex_setcell(c7_y, 3, c7_g_y);
  c7_b_hoistedGlobal = chartInstance->c7_is_active_c7_manualController;
  c7_f_u = c7_b_hoistedGlobal;
  c7_h_y = NULL;
  sf_mex_assign(&c7_h_y, sf_mex_create("y", &c7_f_u, 3, 0U, 0U, 0U, 0), false);
  sf_mex_setcell(c7_y, 4, c7_h_y);
  sf_mex_assign(&c7_st, c7_y, false);
  return c7_st;
}

static void set_sim_state_c7_manualController
  (SFc7_manualControllerInstanceStruct *chartInstance, const mxArray *c7_st)
{
  const mxArray *c7_u;
  c7_Ball c7_r0;
  c7_Player c7_rv1[6];
  int32_T c7_i2;
  uint8_T c7_uv0[27];
  int32_T c7_i3;
  chartInstance->c7_doneDoubleBufferReInit = true;
  c7_u = sf_mex_dup(c7_st);
  c7_r0 = c7_i_emlrt_marshallIn(chartInstance, sf_mex_dup(sf_mex_getcell(c7_u, 0)),
    "Ball");
  *(int8_T *)&((char_T *)chartInstance->c7_b_Ball)[0] = c7_r0.x;
  *(int8_T *)&((char_T *)chartInstance->c7_b_Ball)[1] = c7_r0.y;
  *(uint8_T *)&((char_T *)chartInstance->c7_b_Ball)[2] = c7_r0.valid;
  *chartInstance->c7_gameOn = c7_c_emlrt_marshallIn(chartInstance, sf_mex_dup
    (sf_mex_getcell(c7_u, 1)), "gameOn");
  c7_e_emlrt_marshallIn(chartInstance, sf_mex_dup(sf_mex_getcell(c7_u, 2)),
                        "players", c7_rv1);
  for (c7_i2 = 0; c7_i2 < 6; c7_i2++) {
    *(int8_T *)&((char_T *)(c7_Player *)&((char_T *)chartInstance->c7_players)[8
                 * c7_i2])[0] = c7_rv1[c7_i2].x;
    *(int8_T *)&((char_T *)(c7_Player *)&((char_T *)chartInstance->c7_players)[8
                 * c7_i2])[1] = c7_rv1[c7_i2].y;
    *(int16_T *)&((char_T *)(c7_Player *)&((char_T *)chartInstance->c7_players)
                  [8 * c7_i2])[2] = c7_rv1[c7_i2].orientation;
    *(uint8_T *)&((char_T *)(c7_Player *)&((char_T *)chartInstance->c7_players)
                  [8 * c7_i2])[4] = c7_rv1[c7_i2].color;
    *(uint8_T *)&((char_T *)(c7_Player *)&((char_T *)chartInstance->c7_players)
                  [8 * c7_i2])[5] = c7_rv1[c7_i2].position;
    *(uint8_T *)&((char_T *)(c7_Player *)&((char_T *)chartInstance->c7_players)
                  [8 * c7_i2])[6] = c7_rv1[c7_i2].valid;
  }

  c7_emlrt_marshallIn(chartInstance, sf_mex_dup(sf_mex_getcell(c7_u, 3)),
                      "oldMes", c7_uv0);
  for (c7_i3 = 0; c7_i3 < 27; c7_i3++) {
    chartInstance->c7_oldMes[c7_i3] = c7_uv0[c7_i3];
  }

  chartInstance->c7_is_active_c7_manualController = c7_c_emlrt_marshallIn
    (chartInstance, sf_mex_dup(sf_mex_getcell(c7_u, 4)),
     "is_active_c7_manualController");
  sf_mex_destroy(&c7_u);
  c7_update_debugger_state_c7_manualController(chartInstance);
  sf_mex_destroy(&c7_st);
}

static void finalize_c7_manualController(SFc7_manualControllerInstanceStruct
  *chartInstance)
{
  (void)chartInstance;
}

static void sf_gateway_c7_manualController(SFc7_manualControllerInstanceStruct
  *chartInstance)
{
  int32_T c7_i4;
  _SFD_SYMBOL_SCOPE_PUSH(0U, 0U);
  _sfTime_ = sf_get_time(chartInstance->S);
  _SFD_CC_CALL(CHART_ENTER_SFUNCTION_TAG, 2U, chartInstance->c7_sfEvent);
  chartInstance->c7_sfEvent = CALL_EVENT;
  c7_chartstep_c7_manualController(chartInstance);
  _SFD_SYMBOL_SCOPE_POP();
  _SFD_CHECK_FOR_STATE_INCONSISTENCY(_manualControllerMachineNumber_,
    chartInstance->chartNumber, chartInstance->instanceNumber);
  _SFD_DATA_RANGE_CHECK((real_T)*chartInstance->c7_gameOn, 2U);
  for (c7_i4 = 0; c7_i4 < 27; c7_i4++) {
    _SFD_DATA_RANGE_CHECK((real_T)(*chartInstance->c7_newMessage)[c7_i4], 3U);
  }
}

static void mdl_start_c7_manualController(SFc7_manualControllerInstanceStruct
  *chartInstance)
{
  (void)chartInstance;
}

static void c7_chartstep_c7_manualController(SFc7_manualControllerInstanceStruct
  *chartInstance)
{
  int32_T c7_i5;
  uint8_T c7_b_newMessage[27];
  uint32_T c7_debug_family_var_map[12];
  c7_Player c7_tempEmpty;
  uint8_T c7_playerInfo[24];
  uint8_T c7_colors[6];
  uint8_T c7_positions[6];
  real_T c7_ii;
  real_T c7_nargin = 1.0;
  real_T c7_nargout = 3.0;
  c7_Ball c7_c_Ball;
  c7_Player c7_b_players[6];
  uint8_T c7_b_gameOn;
  int32_T c7_i6;
  int32_T c7_i7;
  uint8_T c7_x[27];
  boolean_T c7_y;
  int32_T c7_k;
  real_T c7_b_k;
  boolean_T c7_b0;
  int32_T c7_i8;
  int32_T c7_i9;
  c7_Player c7_b_tempEmpty[6];
  int32_T c7_i10;
  int32_T c7_i11;
  int32_T c7_i12;
  static uint8_T c7_uv1[6] = { 103U, 103U, 103U, 98U, 98U, 98U };

  int32_T c7_i13;
  static uint8_T c7_uv2[6] = { 111U, 100U, 103U, 111U, 100U, 103U };

  int32_T c7_b_ii;
  real_T c7_c_ii;
  int32_T c7_i14;
  uint8_T c7_b_x[2];
  int16_T c7_b_y;
  int32_T c7_i15;
  boolean_T exitg1;
  _SFD_CC_CALL(CHART_ENTER_DURING_FUNCTION_TAG, 2U, chartInstance->c7_sfEvent);
  for (c7_i5 = 0; c7_i5 < 27; c7_i5++) {
    c7_b_newMessage[c7_i5] = (*chartInstance->c7_newMessage)[c7_i5];
  }

  _SFD_SYMBOL_SCOPE_PUSH_EML(0U, 12U, 12U, c7_debug_family_names,
    c7_debug_family_var_map);
  _SFD_SYMBOL_SCOPE_ADD_EML_IMPORTABLE(&c7_tempEmpty, 0U, c7_i_sf_marshallOut,
    c7_h_sf_marshallIn);
  _SFD_SYMBOL_SCOPE_ADD_EML_IMPORTABLE(c7_playerInfo, 1U, c7_h_sf_marshallOut,
    c7_g_sf_marshallIn);
  _SFD_SYMBOL_SCOPE_ADD_EML_IMPORTABLE(c7_colors, 2U, c7_g_sf_marshallOut,
    c7_f_sf_marshallIn);
  _SFD_SYMBOL_SCOPE_ADD_EML_IMPORTABLE(c7_positions, 3U, c7_g_sf_marshallOut,
    c7_f_sf_marshallIn);
  _SFD_SYMBOL_SCOPE_ADD_EML_IMPORTABLE(&c7_ii, 4U, c7_f_sf_marshallOut,
    c7_e_sf_marshallIn);
  _SFD_SYMBOL_SCOPE_ADD_EML_IMPORTABLE(&c7_nargin, 5U, c7_f_sf_marshallOut,
    c7_e_sf_marshallIn);
  _SFD_SYMBOL_SCOPE_ADD_EML_IMPORTABLE(&c7_nargout, 6U, c7_f_sf_marshallOut,
    c7_e_sf_marshallIn);
  _SFD_SYMBOL_SCOPE_ADD_EML(c7_b_newMessage, 7U, c7_e_sf_marshallOut);
  _SFD_SYMBOL_SCOPE_ADD_EML_IMPORTABLE(&c7_c_Ball, 8U, c7_d_sf_marshallOut,
    c7_d_sf_marshallIn);
  _SFD_SYMBOL_SCOPE_ADD_EML_IMPORTABLE(c7_b_players, 9U, c7_c_sf_marshallOut,
    c7_c_sf_marshallIn);
  _SFD_SYMBOL_SCOPE_ADD_EML_IMPORTABLE(&c7_b_gameOn, 10U, c7_b_sf_marshallOut,
    c7_b_sf_marshallIn);
  _SFD_SYMBOL_SCOPE_ADD_EML_IMPORTABLE(chartInstance->c7_oldMes, 11U,
    c7_sf_marshallOut, c7_sf_marshallIn);
  CV_EML_FCN(0, 0);
  _SFD_EML_CALL(0U, chartInstance->c7_sfEvent, 3);
  _SFD_EML_CALL(0U, chartInstance->c7_sfEvent, 4);
  if (CV_EML_IF(0, 1, 0, !chartInstance->c7_oldMes_not_empty)) {
    _SFD_EML_CALL(0U, chartInstance->c7_sfEvent, 5);
    for (c7_i6 = 0; c7_i6 < 27; c7_i6++) {
      chartInstance->c7_oldMes[c7_i6] = 0U;
    }

    chartInstance->c7_oldMes_not_empty = true;
  }

  _SFD_EML_CALL(0U, chartInstance->c7_sfEvent, 7);
  for (c7_i7 = 0; c7_i7 < 27; c7_i7++) {
    c7_x[c7_i7] = c7_b_newMessage[c7_i7];
  }

  c7_y = false;
  c7_k = 0;
  exitg1 = false;
  while ((exitg1 == false) && (c7_k < 27)) {
    c7_b_k = 1.0 + (real_T)c7_k;
    if ((real_T)c7_x[_SFD_EML_ARRAY_BOUNDS_CHECK("", (int32_T)_SFD_INTEGER_CHECK
         ("", c7_b_k), 1, 27, 1, 0) - 1] == 0.0) {
      c7_b0 = true;
    } else {
      (real_T)_SFD_EML_ARRAY_BOUNDS_CHECK("", (int32_T)_SFD_INTEGER_CHECK("",
        c7_b_k), 1, 27, 1, 0);
      c7_b0 = false;
    }

    if (!c7_b0) {
      c7_y = true;
      exitg1 = true;
    } else {
      c7_k++;
    }
  }

  if (CV_EML_IF(0, 1, 1, CV_EML_MCDC(0, 1, 0, !CV_EML_COND(0, 1, 0, c7_y)))) {
    _SFD_EML_CALL(0U, chartInstance->c7_sfEvent, 8);
    for (c7_i8 = 0; c7_i8 < 27; c7_i8++) {
      c7_b_newMessage[c7_i8] = chartInstance->c7_oldMes[c7_i8];
    }
  } else {
    _SFD_EML_CALL(0U, chartInstance->c7_sfEvent, 10);
    for (c7_i9 = 0; c7_i9 < 27; c7_i9++) {
      chartInstance->c7_oldMes[c7_i9] = c7_b_newMessage[c7_i9];
    }
  }

  _SFD_EML_CALL(0U, chartInstance->c7_sfEvent, 13);
  c7_tempEmpty.x = 0;
  _SFD_EML_CALL(0U, chartInstance->c7_sfEvent, 14);
  c7_tempEmpty.y = 0;
  _SFD_EML_CALL(0U, chartInstance->c7_sfEvent, 15);
  c7_tempEmpty.orientation = 0;
  _SFD_EML_CALL(0U, chartInstance->c7_sfEvent, 16);
  c7_tempEmpty.color = 0U;
  _SFD_EML_CALL(0U, chartInstance->c7_sfEvent, 17);
  c7_tempEmpty.position = 0U;
  _SFD_EML_CALL(0U, chartInstance->c7_sfEvent, 18);
  c7_tempEmpty.valid = 0U;
  _SFD_EML_CALL(0U, chartInstance->c7_sfEvent, 19);
  c7_b_tempEmpty[0] = c7_tempEmpty;
  c7_b_tempEmpty[1] = c7_tempEmpty;
  c7_b_tempEmpty[2] = c7_tempEmpty;
  c7_b_tempEmpty[3] = c7_tempEmpty;
  c7_b_tempEmpty[4] = c7_tempEmpty;
  c7_b_tempEmpty[5] = c7_tempEmpty;
  for (c7_i10 = 0; c7_i10 < 6; c7_i10++) {
    c7_b_players[c7_i10] = c7_b_tempEmpty[c7_i10];
  }

  _SFD_EML_CALL(0U, chartInstance->c7_sfEvent, 21);
  c7_b_gameOn = c7_b_newMessage[0];
  _SFD_EML_CALL(0U, chartInstance->c7_sfEvent, 22);
  c7_c_Ball.x = c7_typecast(chartInstance, c7_b_newMessage[1]);
  _SFD_EML_CALL(0U, chartInstance->c7_sfEvent, 23);
  c7_c_Ball.y = c7_typecast(chartInstance, c7_b_newMessage[2]);
  _SFD_EML_CALL(0U, chartInstance->c7_sfEvent, 24);
  c7_c_Ball.valid = 1U;
  _SFD_EML_CALL(0U, chartInstance->c7_sfEvent, 25);
  for (c7_i11 = 0; c7_i11 < 24; c7_i11++) {
    c7_playerInfo[c7_i11] = c7_b_newMessage[c7_i11 + 3];
  }

  _SFD_EML_CALL(0U, chartInstance->c7_sfEvent, 26);
  for (c7_i12 = 0; c7_i12 < 6; c7_i12++) {
    c7_colors[c7_i12] = c7_uv1[c7_i12];
  }

  _SFD_EML_CALL(0U, chartInstance->c7_sfEvent, 27);
  for (c7_i13 = 0; c7_i13 < 6; c7_i13++) {
    c7_positions[c7_i13] = c7_uv2[c7_i13];
  }

  _SFD_EML_CALL(0U, chartInstance->c7_sfEvent, 28);
  c7_ii = 1.0;
  c7_b_ii = 0;
  while (c7_b_ii < 6) {
    c7_ii = 1.0 + (real_T)c7_b_ii;
    CV_EML_FOR(0, 1, 0, 1);
    _SFD_EML_CALL(0U, chartInstance->c7_sfEvent, 29);
    c7_b_players[_SFD_EML_ARRAY_BOUNDS_CHECK("players", (int32_T)
      _SFD_INTEGER_CHECK("ii", c7_ii), 1, 6, 1, 0) - 1].x = c7_typecast
      (chartInstance, c7_playerInfo[_SFD_EML_ARRAY_BOUNDS_CHECK("playerInfo",
        (int32_T)_SFD_INTEGER_CHECK("(ii-1)*4+1", (c7_ii - 1.0) * 4.0 + 1.0), 1,
        24, 1, 0) - 1]);
    _SFD_EML_CALL(0U, chartInstance->c7_sfEvent, 30);
    c7_b_players[_SFD_EML_ARRAY_BOUNDS_CHECK("players", (int32_T)
      _SFD_INTEGER_CHECK("ii", c7_ii), 1, 6, 1, 0) - 1].y = c7_typecast
      (chartInstance, c7_playerInfo[_SFD_EML_ARRAY_BOUNDS_CHECK("playerInfo",
        (int32_T)_SFD_INTEGER_CHECK("(ii-1)*4+2", (c7_ii - 1.0) * 4.0 + 2.0), 1,
        24, 1, 0) - 1]);
    _SFD_EML_CALL(0U, chartInstance->c7_sfEvent, 31);
    c7_c_ii = (c7_ii - 1.0) * 4.0;
    for (c7_i14 = 0; c7_i14 < 2; c7_i14++) {
      c7_b_x[c7_i14] = c7_playerInfo[_SFD_EML_ARRAY_BOUNDS_CHECK("playerInfo",
        (int32_T)_SFD_INTEGER_CHECK("(ii-1)*4+3:(ii-1)*4+4", c7_c_ii + (3.0 +
        (real_T)c7_i14)), 1, 24, 1, 0) - 1];
    }

    memcpy(&c7_b_y, &c7_b_x[0], (size_t)1 * sizeof(int16_T));
    c7_b_players[_SFD_EML_ARRAY_BOUNDS_CHECK("players", (int32_T)
      _SFD_INTEGER_CHECK("ii", c7_ii), 1, 6, 1, 0) - 1].orientation = c7_b_y;
    _SFD_EML_CALL(0U, chartInstance->c7_sfEvent, 32);
    c7_b_players[_SFD_EML_ARRAY_BOUNDS_CHECK("players", (int32_T)
      _SFD_INTEGER_CHECK("ii", c7_ii), 1, 6, 1, 0) - 1].color =
      c7_colors[_SFD_EML_ARRAY_BOUNDS_CHECK("colors", (int32_T)
      _SFD_INTEGER_CHECK("ii", c7_ii), 1, 6, 1, 0) - 1];
    _SFD_EML_CALL(0U, chartInstance->c7_sfEvent, 33);
    c7_b_players[_SFD_EML_ARRAY_BOUNDS_CHECK("players", (int32_T)
      _SFD_INTEGER_CHECK("ii", c7_ii), 1, 6, 1, 0) - 1].position =
      c7_positions[_SFD_EML_ARRAY_BOUNDS_CHECK("positions", (int32_T)
      _SFD_INTEGER_CHECK("ii", c7_ii), 1, 6, 1, 0) - 1];
    _SFD_EML_CALL(0U, chartInstance->c7_sfEvent, 34);
    c7_b_players[_SFD_EML_ARRAY_BOUNDS_CHECK("players", (int32_T)
      _SFD_INTEGER_CHECK("ii", c7_ii), 1, 6, 1, 0) - 1].valid = 1U;
    c7_b_ii++;
    _SF_MEX_LISTEN_FOR_CTRL_C(chartInstance->S);
  }

  CV_EML_FOR(0, 1, 0, 0);
  _SFD_EML_CALL(0U, chartInstance->c7_sfEvent, -34);
  _SFD_SYMBOL_SCOPE_POP();
  *(int8_T *)&((char_T *)chartInstance->c7_b_Ball)[0] = c7_c_Ball.x;
  *(int8_T *)&((char_T *)chartInstance->c7_b_Ball)[1] = c7_c_Ball.y;
  *(uint8_T *)&((char_T *)chartInstance->c7_b_Ball)[2] = c7_c_Ball.valid;
  for (c7_i15 = 0; c7_i15 < 6; c7_i15++) {
    *(int8_T *)&((char_T *)(c7_Player *)&((char_T *)chartInstance->c7_players)[8
                 * c7_i15])[0] = c7_b_players[c7_i15].x;
    *(int8_T *)&((char_T *)(c7_Player *)&((char_T *)chartInstance->c7_players)[8
                 * c7_i15])[1] = c7_b_players[c7_i15].y;
    *(int16_T *)&((char_T *)(c7_Player *)&((char_T *)chartInstance->c7_players)
                  [8 * c7_i15])[2] = c7_b_players[c7_i15].orientation;
    *(uint8_T *)&((char_T *)(c7_Player *)&((char_T *)chartInstance->c7_players)
                  [8 * c7_i15])[4] = c7_b_players[c7_i15].color;
    *(uint8_T *)&((char_T *)(c7_Player *)&((char_T *)chartInstance->c7_players)
                  [8 * c7_i15])[5] = c7_b_players[c7_i15].position;
    *(uint8_T *)&((char_T *)(c7_Player *)&((char_T *)chartInstance->c7_players)
                  [8 * c7_i15])[6] = c7_b_players[c7_i15].valid;
  }

  *chartInstance->c7_gameOn = c7_b_gameOn;
  _SFD_CC_CALL(EXIT_OUT_OF_FUNCTION_TAG, 2U, chartInstance->c7_sfEvent);
}

static void initSimStructsc7_manualController
  (SFc7_manualControllerInstanceStruct *chartInstance)
{
  (void)chartInstance;
}

static void init_script_number_translation(uint32_T c7_machineNumber, uint32_T
  c7_chartNumber, uint32_T c7_instanceNumber)
{
  (void)c7_machineNumber;
  (void)c7_chartNumber;
  (void)c7_instanceNumber;
}

static const mxArray *c7_sf_marshallOut(void *chartInstanceVoid, void *c7_inData)
{
  const mxArray *c7_mxArrayOutData = NULL;
  int32_T c7_i16;
  uint8_T c7_b_inData[27];
  int32_T c7_i17;
  uint8_T c7_u[27];
  const mxArray *c7_y = NULL;
  SFc7_manualControllerInstanceStruct *chartInstance;
  chartInstance = (SFc7_manualControllerInstanceStruct *)chartInstanceVoid;
  c7_mxArrayOutData = NULL;
  for (c7_i16 = 0; c7_i16 < 27; c7_i16++) {
    c7_b_inData[c7_i16] = (*(uint8_T (*)[27])c7_inData)[c7_i16];
  }

  for (c7_i17 = 0; c7_i17 < 27; c7_i17++) {
    c7_u[c7_i17] = c7_b_inData[c7_i17];
  }

  c7_y = NULL;
  if (!chartInstance->c7_oldMes_not_empty) {
    sf_mex_assign(&c7_y, sf_mex_create("y", NULL, 0, 0U, 1U, 0U, 2, 0, 0), false);
  } else {
    sf_mex_assign(&c7_y, sf_mex_create("y", c7_u, 3, 0U, 1U, 0U, 2, 1, 27),
                  false);
  }

  sf_mex_assign(&c7_mxArrayOutData, c7_y, false);
  return c7_mxArrayOutData;
}

static void c7_emlrt_marshallIn(SFc7_manualControllerInstanceStruct
  *chartInstance, const mxArray *c7_b_oldMes, const char_T *c7_identifier,
  uint8_T c7_y[27])
{
  emlrtMsgIdentifier c7_thisId;
  c7_thisId.fIdentifier = c7_identifier;
  c7_thisId.fParent = NULL;
  c7_b_emlrt_marshallIn(chartInstance, sf_mex_dup(c7_b_oldMes), &c7_thisId, c7_y);
  sf_mex_destroy(&c7_b_oldMes);
}

static void c7_b_emlrt_marshallIn(SFc7_manualControllerInstanceStruct
  *chartInstance, const mxArray *c7_u, const emlrtMsgIdentifier *c7_parentId,
  uint8_T c7_y[27])
{
  uint8_T c7_uv3[27];
  int32_T c7_i18;
  if (mxIsEmpty(c7_u)) {
    chartInstance->c7_oldMes_not_empty = false;
  } else {
    chartInstance->c7_oldMes_not_empty = true;
    sf_mex_import(c7_parentId, sf_mex_dup(c7_u), c7_uv3, 1, 3, 0U, 1, 0U, 2, 1,
                  27);
    for (c7_i18 = 0; c7_i18 < 27; c7_i18++) {
      c7_y[c7_i18] = c7_uv3[c7_i18];
    }
  }

  sf_mex_destroy(&c7_u);
}

static void c7_sf_marshallIn(void *chartInstanceVoid, const mxArray
  *c7_mxArrayInData, const char_T *c7_varName, void *c7_outData)
{
  const mxArray *c7_b_oldMes;
  const char_T *c7_identifier;
  emlrtMsgIdentifier c7_thisId;
  uint8_T c7_y[27];
  int32_T c7_i19;
  SFc7_manualControllerInstanceStruct *chartInstance;
  chartInstance = (SFc7_manualControllerInstanceStruct *)chartInstanceVoid;
  c7_b_oldMes = sf_mex_dup(c7_mxArrayInData);
  c7_identifier = c7_varName;
  c7_thisId.fIdentifier = c7_identifier;
  c7_thisId.fParent = NULL;
  c7_b_emlrt_marshallIn(chartInstance, sf_mex_dup(c7_b_oldMes), &c7_thisId, c7_y);
  sf_mex_destroy(&c7_b_oldMes);
  for (c7_i19 = 0; c7_i19 < 27; c7_i19++) {
    (*(uint8_T (*)[27])c7_outData)[c7_i19] = c7_y[c7_i19];
  }

  sf_mex_destroy(&c7_mxArrayInData);
}

static const mxArray *c7_b_sf_marshallOut(void *chartInstanceVoid, void
  *c7_inData)
{
  const mxArray *c7_mxArrayOutData = NULL;
  uint8_T c7_u;
  const mxArray *c7_y = NULL;
  SFc7_manualControllerInstanceStruct *chartInstance;
  chartInstance = (SFc7_manualControllerInstanceStruct *)chartInstanceVoid;
  c7_mxArrayOutData = NULL;
  c7_u = *(uint8_T *)c7_inData;
  c7_y = NULL;
  sf_mex_assign(&c7_y, sf_mex_create("y", &c7_u, 3, 0U, 0U, 0U, 0), false);
  sf_mex_assign(&c7_mxArrayOutData, c7_y, false);
  return c7_mxArrayOutData;
}

static uint8_T c7_c_emlrt_marshallIn(SFc7_manualControllerInstanceStruct
  *chartInstance, const mxArray *c7_b_gameOn, const char_T *c7_identifier)
{
  uint8_T c7_y;
  emlrtMsgIdentifier c7_thisId;
  c7_thisId.fIdentifier = c7_identifier;
  c7_thisId.fParent = NULL;
  c7_y = c7_d_emlrt_marshallIn(chartInstance, sf_mex_dup(c7_b_gameOn),
    &c7_thisId);
  sf_mex_destroy(&c7_b_gameOn);
  return c7_y;
}

static uint8_T c7_d_emlrt_marshallIn(SFc7_manualControllerInstanceStruct
  *chartInstance, const mxArray *c7_u, const emlrtMsgIdentifier *c7_parentId)
{
  uint8_T c7_y;
  uint8_T c7_u0;
  (void)chartInstance;
  sf_mex_import(c7_parentId, sf_mex_dup(c7_u), &c7_u0, 1, 3, 0U, 0, 0U, 0);
  c7_y = c7_u0;
  sf_mex_destroy(&c7_u);
  return c7_y;
}

static void c7_b_sf_marshallIn(void *chartInstanceVoid, const mxArray
  *c7_mxArrayInData, const char_T *c7_varName, void *c7_outData)
{
  const mxArray *c7_b_gameOn;
  const char_T *c7_identifier;
  emlrtMsgIdentifier c7_thisId;
  uint8_T c7_y;
  SFc7_manualControllerInstanceStruct *chartInstance;
  chartInstance = (SFc7_manualControllerInstanceStruct *)chartInstanceVoid;
  c7_b_gameOn = sf_mex_dup(c7_mxArrayInData);
  c7_identifier = c7_varName;
  c7_thisId.fIdentifier = c7_identifier;
  c7_thisId.fParent = NULL;
  c7_y = c7_d_emlrt_marshallIn(chartInstance, sf_mex_dup(c7_b_gameOn),
    &c7_thisId);
  sf_mex_destroy(&c7_b_gameOn);
  *(uint8_T *)c7_outData = c7_y;
  sf_mex_destroy(&c7_mxArrayInData);
}

static const mxArray *c7_emlrt_marshallOut(SFc7_manualControllerInstanceStruct
  *chartInstance, const c7_Player c7_u[6])
{
  const mxArray *c7_y;
  static int32_T c7_iv0[1] = { 6 };

  int32_T c7_iv1[1];
  int32_T c7_i20;
  const c7_Player *c7_r1;
  int8_T c7_b_u;
  const mxArray *c7_b_y = NULL;
  int8_T c7_c_u;
  const mxArray *c7_c_y = NULL;
  int16_T c7_d_u;
  const mxArray *c7_d_y = NULL;
  uint8_T c7_e_u;
  const mxArray *c7_e_y = NULL;
  uint8_T c7_f_u;
  const mxArray *c7_f_y = NULL;
  uint8_T c7_g_u;
  const mxArray *c7_g_y = NULL;
  (void)chartInstance;
  c7_y = NULL;
  c7_y = NULL;
  c7_iv1[0] = c7_iv0[0];
  sf_mex_assign(&c7_y, sf_mex_createstructarray("structure", 1, c7_iv1), false);
  for (c7_i20 = 0; c7_i20 < 6; c7_i20++) {
    c7_r1 = &c7_u[c7_i20];
    c7_b_u = c7_r1->x;
    c7_b_y = NULL;
    sf_mex_assign(&c7_b_y, sf_mex_create("y", &c7_b_u, 2, 0U, 0U, 0U, 0), false);
    sf_mex_addfield(c7_y, c7_b_y, "x", "x", c7_i20);
    c7_c_u = c7_r1->y;
    c7_c_y = NULL;
    sf_mex_assign(&c7_c_y, sf_mex_create("y", &c7_c_u, 2, 0U, 0U, 0U, 0), false);
    sf_mex_addfield(c7_y, c7_c_y, "y", "y", c7_i20);
    c7_d_u = c7_r1->orientation;
    c7_d_y = NULL;
    sf_mex_assign(&c7_d_y, sf_mex_create("y", &c7_d_u, 4, 0U, 0U, 0U, 0), false);
    sf_mex_addfield(c7_y, c7_d_y, "orientation", "orientation", c7_i20);
    c7_e_u = c7_r1->color;
    c7_e_y = NULL;
    sf_mex_assign(&c7_e_y, sf_mex_create("y", &c7_e_u, 3, 0U, 0U, 0U, 0), false);
    sf_mex_addfield(c7_y, c7_e_y, "color", "color", c7_i20);
    c7_f_u = c7_r1->position;
    c7_f_y = NULL;
    sf_mex_assign(&c7_f_y, sf_mex_create("y", &c7_f_u, 3, 0U, 0U, 0U, 0), false);
    sf_mex_addfield(c7_y, c7_f_y, "position", "position", c7_i20);
    c7_g_u = c7_r1->valid;
    c7_g_y = NULL;
    sf_mex_assign(&c7_g_y, sf_mex_create("y", &c7_g_u, 3, 0U, 0U, 0U, 0), false);
    sf_mex_addfield(c7_y, c7_g_y, "valid", "valid", c7_i20);
  }

  return c7_y;
}

static const mxArray *c7_c_sf_marshallOut(void *chartInstanceVoid, void
  *c7_inData)
{
  const mxArray *c7_mxArrayOutData = NULL;
  SFc7_manualControllerInstanceStruct *chartInstance;
  chartInstance = (SFc7_manualControllerInstanceStruct *)chartInstanceVoid;
  c7_mxArrayOutData = NULL;
  sf_mex_assign(&c7_mxArrayOutData, c7_emlrt_marshallOut(chartInstance,
    *(c7_Player (*)[6])c7_inData), false);
  return c7_mxArrayOutData;
}

static void c7_e_emlrt_marshallIn(SFc7_manualControllerInstanceStruct
  *chartInstance, const mxArray *c7_b_players, const char_T *c7_identifier,
  c7_Player c7_y[6])
{
  emlrtMsgIdentifier c7_thisId;
  c7_thisId.fIdentifier = c7_identifier;
  c7_thisId.fParent = NULL;
  c7_f_emlrt_marshallIn(chartInstance, sf_mex_dup(c7_b_players), &c7_thisId,
                        c7_y);
  sf_mex_destroy(&c7_b_players);
}

static void c7_f_emlrt_marshallIn(SFc7_manualControllerInstanceStruct
  *chartInstance, const mxArray *c7_u, const emlrtMsgIdentifier *c7_parentId,
  c7_Player c7_y[6])
{
  static uint32_T c7_uv4[1] = { 6U };

  uint32_T c7_uv5[1];
  emlrtMsgIdentifier c7_thisId;
  static const char * c7_fieldNames[6] = { "x", "y", "orientation", "color",
    "position", "valid" };

  c7_Player (*c7_r2)[6];
  int32_T c7_i21;
  c7_uv5[0] = c7_uv4[0];
  c7_thisId.fParent = c7_parentId;
  sf_mex_check_struct(c7_parentId, c7_u, 6, c7_fieldNames, 1U, c7_uv5);
  c7_r2 = (c7_Player (*)[6])c7_y;
  for (c7_i21 = 0; c7_i21 < 6; c7_i21++) {
    c7_thisId.fIdentifier = "x";
    (*c7_r2)[c7_i21].x = c7_g_emlrt_marshallIn(chartInstance, sf_mex_dup
      (sf_mex_getfield(c7_u, "x", "x", c7_i21)), &c7_thisId);
    c7_thisId.fIdentifier = "y";
    (*c7_r2)[c7_i21].y = c7_g_emlrt_marshallIn(chartInstance, sf_mex_dup
      (sf_mex_getfield(c7_u, "y", "y", c7_i21)), &c7_thisId);
    c7_thisId.fIdentifier = "orientation";
    (*c7_r2)[c7_i21].orientation = c7_h_emlrt_marshallIn(chartInstance,
      sf_mex_dup(sf_mex_getfield(c7_u, "orientation", "orientation", c7_i21)),
      &c7_thisId);
    c7_thisId.fIdentifier = "color";
    (*c7_r2)[c7_i21].color = c7_d_emlrt_marshallIn(chartInstance, sf_mex_dup
      (sf_mex_getfield(c7_u, "color", "color", c7_i21)), &c7_thisId);
    c7_thisId.fIdentifier = "position";
    (*c7_r2)[c7_i21].position = c7_d_emlrt_marshallIn(chartInstance, sf_mex_dup
      (sf_mex_getfield(c7_u, "position", "position", c7_i21)), &c7_thisId);
    c7_thisId.fIdentifier = "valid";
    (*c7_r2)[c7_i21].valid = c7_d_emlrt_marshallIn(chartInstance, sf_mex_dup
      (sf_mex_getfield(c7_u, "valid", "valid", c7_i21)), &c7_thisId);
  }

  sf_mex_destroy(&c7_u);
}

static int8_T c7_g_emlrt_marshallIn(SFc7_manualControllerInstanceStruct
  *chartInstance, const mxArray *c7_u, const emlrtMsgIdentifier *c7_parentId)
{
  int8_T c7_y;
  int8_T c7_i22;
  (void)chartInstance;
  sf_mex_import(c7_parentId, sf_mex_dup(c7_u), &c7_i22, 1, 2, 0U, 0, 0U, 0);
  c7_y = c7_i22;
  sf_mex_destroy(&c7_u);
  return c7_y;
}

static int16_T c7_h_emlrt_marshallIn(SFc7_manualControllerInstanceStruct
  *chartInstance, const mxArray *c7_u, const emlrtMsgIdentifier *c7_parentId)
{
  int16_T c7_y;
  int16_T c7_i23;
  (void)chartInstance;
  sf_mex_import(c7_parentId, sf_mex_dup(c7_u), &c7_i23, 1, 4, 0U, 0, 0U, 0);
  c7_y = c7_i23;
  sf_mex_destroy(&c7_u);
  return c7_y;
}

static void c7_c_sf_marshallIn(void *chartInstanceVoid, const mxArray
  *c7_mxArrayInData, const char_T *c7_varName, void *c7_outData)
{
  const mxArray *c7_b_players;
  const char_T *c7_identifier;
  emlrtMsgIdentifier c7_thisId;
  c7_Player c7_y[6];
  int32_T c7_i24;
  SFc7_manualControllerInstanceStruct *chartInstance;
  chartInstance = (SFc7_manualControllerInstanceStruct *)chartInstanceVoid;
  c7_b_players = sf_mex_dup(c7_mxArrayInData);
  c7_identifier = c7_varName;
  c7_thisId.fIdentifier = c7_identifier;
  c7_thisId.fParent = NULL;
  c7_f_emlrt_marshallIn(chartInstance, sf_mex_dup(c7_b_players), &c7_thisId,
                        c7_y);
  sf_mex_destroy(&c7_b_players);
  for (c7_i24 = 0; c7_i24 < 6; c7_i24++) {
    (*(c7_Player (*)[6])c7_outData)[c7_i24] = c7_y[c7_i24];
  }

  sf_mex_destroy(&c7_mxArrayInData);
}

static const mxArray *c7_d_sf_marshallOut(void *chartInstanceVoid, void
  *c7_inData)
{
  const mxArray *c7_mxArrayOutData = NULL;
  c7_Ball c7_u;
  const mxArray *c7_y = NULL;
  int8_T c7_b_u;
  const mxArray *c7_b_y = NULL;
  int8_T c7_c_u;
  const mxArray *c7_c_y = NULL;
  uint8_T c7_d_u;
  const mxArray *c7_d_y = NULL;
  SFc7_manualControllerInstanceStruct *chartInstance;
  chartInstance = (SFc7_manualControllerInstanceStruct *)chartInstanceVoid;
  c7_mxArrayOutData = NULL;
  c7_u = *(c7_Ball *)c7_inData;
  c7_y = NULL;
  sf_mex_assign(&c7_y, sf_mex_createstruct("structure", 2, 1, 1), false);
  c7_b_u = c7_u.x;
  c7_b_y = NULL;
  sf_mex_assign(&c7_b_y, sf_mex_create("y", &c7_b_u, 2, 0U, 0U, 0U, 0), false);
  sf_mex_addfield(c7_y, c7_b_y, "x", "x", 0);
  c7_c_u = c7_u.y;
  c7_c_y = NULL;
  sf_mex_assign(&c7_c_y, sf_mex_create("y", &c7_c_u, 2, 0U, 0U, 0U, 0), false);
  sf_mex_addfield(c7_y, c7_c_y, "y", "y", 0);
  c7_d_u = c7_u.valid;
  c7_d_y = NULL;
  sf_mex_assign(&c7_d_y, sf_mex_create("y", &c7_d_u, 3, 0U, 0U, 0U, 0), false);
  sf_mex_addfield(c7_y, c7_d_y, "valid", "valid", 0);
  sf_mex_assign(&c7_mxArrayOutData, c7_y, false);
  return c7_mxArrayOutData;
}

static c7_Ball c7_i_emlrt_marshallIn(SFc7_manualControllerInstanceStruct
  *chartInstance, const mxArray *c7_c_Ball, const char_T *c7_identifier)
{
  c7_Ball c7_y;
  emlrtMsgIdentifier c7_thisId;
  c7_thisId.fIdentifier = c7_identifier;
  c7_thisId.fParent = NULL;
  c7_y = c7_j_emlrt_marshallIn(chartInstance, sf_mex_dup(c7_c_Ball), &c7_thisId);
  sf_mex_destroy(&c7_c_Ball);
  return c7_y;
}

static c7_Ball c7_j_emlrt_marshallIn(SFc7_manualControllerInstanceStruct
  *chartInstance, const mxArray *c7_u, const emlrtMsgIdentifier *c7_parentId)
{
  c7_Ball c7_y;
  emlrtMsgIdentifier c7_thisId;
  static const char * c7_fieldNames[3] = { "x", "y", "valid" };

  c7_thisId.fParent = c7_parentId;
  sf_mex_check_struct(c7_parentId, c7_u, 3, c7_fieldNames, 0U, NULL);
  c7_thisId.fIdentifier = "x";
  c7_y.x = c7_g_emlrt_marshallIn(chartInstance, sf_mex_dup(sf_mex_getfield(c7_u,
    "x", "x", 0)), &c7_thisId);
  c7_thisId.fIdentifier = "y";
  c7_y.y = c7_g_emlrt_marshallIn(chartInstance, sf_mex_dup(sf_mex_getfield(c7_u,
    "y", "y", 0)), &c7_thisId);
  c7_thisId.fIdentifier = "valid";
  c7_y.valid = c7_d_emlrt_marshallIn(chartInstance, sf_mex_dup(sf_mex_getfield
    (c7_u, "valid", "valid", 0)), &c7_thisId);
  sf_mex_destroy(&c7_u);
  return c7_y;
}

static void c7_d_sf_marshallIn(void *chartInstanceVoid, const mxArray
  *c7_mxArrayInData, const char_T *c7_varName, void *c7_outData)
{
  const mxArray *c7_c_Ball;
  const char_T *c7_identifier;
  emlrtMsgIdentifier c7_thisId;
  c7_Ball c7_y;
  SFc7_manualControllerInstanceStruct *chartInstance;
  chartInstance = (SFc7_manualControllerInstanceStruct *)chartInstanceVoid;
  c7_c_Ball = sf_mex_dup(c7_mxArrayInData);
  c7_identifier = c7_varName;
  c7_thisId.fIdentifier = c7_identifier;
  c7_thisId.fParent = NULL;
  c7_y = c7_j_emlrt_marshallIn(chartInstance, sf_mex_dup(c7_c_Ball), &c7_thisId);
  sf_mex_destroy(&c7_c_Ball);
  *(c7_Ball *)c7_outData = c7_y;
  sf_mex_destroy(&c7_mxArrayInData);
}

static const mxArray *c7_e_sf_marshallOut(void *chartInstanceVoid, void
  *c7_inData)
{
  const mxArray *c7_mxArrayOutData = NULL;
  int32_T c7_i25;
  uint8_T c7_b_inData[27];
  int32_T c7_i26;
  uint8_T c7_u[27];
  const mxArray *c7_y = NULL;
  SFc7_manualControllerInstanceStruct *chartInstance;
  chartInstance = (SFc7_manualControllerInstanceStruct *)chartInstanceVoid;
  c7_mxArrayOutData = NULL;
  for (c7_i25 = 0; c7_i25 < 27; c7_i25++) {
    c7_b_inData[c7_i25] = (*(uint8_T (*)[27])c7_inData)[c7_i25];
  }

  for (c7_i26 = 0; c7_i26 < 27; c7_i26++) {
    c7_u[c7_i26] = c7_b_inData[c7_i26];
  }

  c7_y = NULL;
  sf_mex_assign(&c7_y, sf_mex_create("y", c7_u, 3, 0U, 1U, 0U, 2, 1, 27), false);
  sf_mex_assign(&c7_mxArrayOutData, c7_y, false);
  return c7_mxArrayOutData;
}

static const mxArray *c7_f_sf_marshallOut(void *chartInstanceVoid, void
  *c7_inData)
{
  const mxArray *c7_mxArrayOutData = NULL;
  real_T c7_u;
  const mxArray *c7_y = NULL;
  SFc7_manualControllerInstanceStruct *chartInstance;
  chartInstance = (SFc7_manualControllerInstanceStruct *)chartInstanceVoid;
  c7_mxArrayOutData = NULL;
  c7_u = *(real_T *)c7_inData;
  c7_y = NULL;
  sf_mex_assign(&c7_y, sf_mex_create("y", &c7_u, 0, 0U, 0U, 0U, 0), false);
  sf_mex_assign(&c7_mxArrayOutData, c7_y, false);
  return c7_mxArrayOutData;
}

static real_T c7_k_emlrt_marshallIn(SFc7_manualControllerInstanceStruct
  *chartInstance, const mxArray *c7_u, const emlrtMsgIdentifier *c7_parentId)
{
  real_T c7_y;
  real_T c7_d0;
  (void)chartInstance;
  sf_mex_import(c7_parentId, sf_mex_dup(c7_u), &c7_d0, 1, 0, 0U, 0, 0U, 0);
  c7_y = c7_d0;
  sf_mex_destroy(&c7_u);
  return c7_y;
}

static void c7_e_sf_marshallIn(void *chartInstanceVoid, const mxArray
  *c7_mxArrayInData, const char_T *c7_varName, void *c7_outData)
{
  const mxArray *c7_nargout;
  const char_T *c7_identifier;
  emlrtMsgIdentifier c7_thisId;
  real_T c7_y;
  SFc7_manualControllerInstanceStruct *chartInstance;
  chartInstance = (SFc7_manualControllerInstanceStruct *)chartInstanceVoid;
  c7_nargout = sf_mex_dup(c7_mxArrayInData);
  c7_identifier = c7_varName;
  c7_thisId.fIdentifier = c7_identifier;
  c7_thisId.fParent = NULL;
  c7_y = c7_k_emlrt_marshallIn(chartInstance, sf_mex_dup(c7_nargout), &c7_thisId);
  sf_mex_destroy(&c7_nargout);
  *(real_T *)c7_outData = c7_y;
  sf_mex_destroy(&c7_mxArrayInData);
}

static const mxArray *c7_g_sf_marshallOut(void *chartInstanceVoid, void
  *c7_inData)
{
  const mxArray *c7_mxArrayOutData = NULL;
  int32_T c7_i27;
  uint8_T c7_b_inData[6];
  int32_T c7_i28;
  uint8_T c7_u[6];
  const mxArray *c7_y = NULL;
  SFc7_manualControllerInstanceStruct *chartInstance;
  chartInstance = (SFc7_manualControllerInstanceStruct *)chartInstanceVoid;
  c7_mxArrayOutData = NULL;
  for (c7_i27 = 0; c7_i27 < 6; c7_i27++) {
    c7_b_inData[c7_i27] = (*(uint8_T (*)[6])c7_inData)[c7_i27];
  }

  for (c7_i28 = 0; c7_i28 < 6; c7_i28++) {
    c7_u[c7_i28] = c7_b_inData[c7_i28];
  }

  c7_y = NULL;
  sf_mex_assign(&c7_y, sf_mex_create("y", c7_u, 3, 0U, 1U, 0U, 2, 1, 6), false);
  sf_mex_assign(&c7_mxArrayOutData, c7_y, false);
  return c7_mxArrayOutData;
}

static void c7_l_emlrt_marshallIn(SFc7_manualControllerInstanceStruct
  *chartInstance, const mxArray *c7_u, const emlrtMsgIdentifier *c7_parentId,
  uint8_T c7_y[6])
{
  uint8_T c7_uv6[6];
  int32_T c7_i29;
  (void)chartInstance;
  sf_mex_import(c7_parentId, sf_mex_dup(c7_u), c7_uv6, 1, 3, 0U, 1, 0U, 2, 1, 6);
  for (c7_i29 = 0; c7_i29 < 6; c7_i29++) {
    c7_y[c7_i29] = c7_uv6[c7_i29];
  }

  sf_mex_destroy(&c7_u);
}

static void c7_f_sf_marshallIn(void *chartInstanceVoid, const mxArray
  *c7_mxArrayInData, const char_T *c7_varName, void *c7_outData)
{
  const mxArray *c7_positions;
  const char_T *c7_identifier;
  emlrtMsgIdentifier c7_thisId;
  uint8_T c7_y[6];
  int32_T c7_i30;
  SFc7_manualControllerInstanceStruct *chartInstance;
  chartInstance = (SFc7_manualControllerInstanceStruct *)chartInstanceVoid;
  c7_positions = sf_mex_dup(c7_mxArrayInData);
  c7_identifier = c7_varName;
  c7_thisId.fIdentifier = c7_identifier;
  c7_thisId.fParent = NULL;
  c7_l_emlrt_marshallIn(chartInstance, sf_mex_dup(c7_positions), &c7_thisId,
                        c7_y);
  sf_mex_destroy(&c7_positions);
  for (c7_i30 = 0; c7_i30 < 6; c7_i30++) {
    (*(uint8_T (*)[6])c7_outData)[c7_i30] = c7_y[c7_i30];
  }

  sf_mex_destroy(&c7_mxArrayInData);
}

static const mxArray *c7_h_sf_marshallOut(void *chartInstanceVoid, void
  *c7_inData)
{
  const mxArray *c7_mxArrayOutData = NULL;
  int32_T c7_i31;
  uint8_T c7_b_inData[24];
  int32_T c7_i32;
  uint8_T c7_u[24];
  const mxArray *c7_y = NULL;
  SFc7_manualControllerInstanceStruct *chartInstance;
  chartInstance = (SFc7_manualControllerInstanceStruct *)chartInstanceVoid;
  c7_mxArrayOutData = NULL;
  for (c7_i31 = 0; c7_i31 < 24; c7_i31++) {
    c7_b_inData[c7_i31] = (*(uint8_T (*)[24])c7_inData)[c7_i31];
  }

  for (c7_i32 = 0; c7_i32 < 24; c7_i32++) {
    c7_u[c7_i32] = c7_b_inData[c7_i32];
  }

  c7_y = NULL;
  sf_mex_assign(&c7_y, sf_mex_create("y", c7_u, 3, 0U, 1U, 0U, 2, 1, 24), false);
  sf_mex_assign(&c7_mxArrayOutData, c7_y, false);
  return c7_mxArrayOutData;
}

static void c7_m_emlrt_marshallIn(SFc7_manualControllerInstanceStruct
  *chartInstance, const mxArray *c7_u, const emlrtMsgIdentifier *c7_parentId,
  uint8_T c7_y[24])
{
  uint8_T c7_uv7[24];
  int32_T c7_i33;
  (void)chartInstance;
  sf_mex_import(c7_parentId, sf_mex_dup(c7_u), c7_uv7, 1, 3, 0U, 1, 0U, 2, 1, 24);
  for (c7_i33 = 0; c7_i33 < 24; c7_i33++) {
    c7_y[c7_i33] = c7_uv7[c7_i33];
  }

  sf_mex_destroy(&c7_u);
}

static void c7_g_sf_marshallIn(void *chartInstanceVoid, const mxArray
  *c7_mxArrayInData, const char_T *c7_varName, void *c7_outData)
{
  const mxArray *c7_playerInfo;
  const char_T *c7_identifier;
  emlrtMsgIdentifier c7_thisId;
  uint8_T c7_y[24];
  int32_T c7_i34;
  SFc7_manualControllerInstanceStruct *chartInstance;
  chartInstance = (SFc7_manualControllerInstanceStruct *)chartInstanceVoid;
  c7_playerInfo = sf_mex_dup(c7_mxArrayInData);
  c7_identifier = c7_varName;
  c7_thisId.fIdentifier = c7_identifier;
  c7_thisId.fParent = NULL;
  c7_m_emlrt_marshallIn(chartInstance, sf_mex_dup(c7_playerInfo), &c7_thisId,
                        c7_y);
  sf_mex_destroy(&c7_playerInfo);
  for (c7_i34 = 0; c7_i34 < 24; c7_i34++) {
    (*(uint8_T (*)[24])c7_outData)[c7_i34] = c7_y[c7_i34];
  }

  sf_mex_destroy(&c7_mxArrayInData);
}

static const mxArray *c7_i_sf_marshallOut(void *chartInstanceVoid, void
  *c7_inData)
{
  const mxArray *c7_mxArrayOutData = NULL;
  c7_Player c7_u;
  const mxArray *c7_y = NULL;
  int8_T c7_b_u;
  const mxArray *c7_b_y = NULL;
  int8_T c7_c_u;
  const mxArray *c7_c_y = NULL;
  int16_T c7_d_u;
  const mxArray *c7_d_y = NULL;
  uint8_T c7_e_u;
  const mxArray *c7_e_y = NULL;
  uint8_T c7_f_u;
  const mxArray *c7_f_y = NULL;
  uint8_T c7_g_u;
  const mxArray *c7_g_y = NULL;
  SFc7_manualControllerInstanceStruct *chartInstance;
  chartInstance = (SFc7_manualControllerInstanceStruct *)chartInstanceVoid;
  c7_mxArrayOutData = NULL;
  c7_u = *(c7_Player *)c7_inData;
  c7_y = NULL;
  sf_mex_assign(&c7_y, sf_mex_createstruct("structure", 2, 1, 1), false);
  c7_b_u = c7_u.x;
  c7_b_y = NULL;
  sf_mex_assign(&c7_b_y, sf_mex_create("y", &c7_b_u, 2, 0U, 0U, 0U, 0), false);
  sf_mex_addfield(c7_y, c7_b_y, "x", "x", 0);
  c7_c_u = c7_u.y;
  c7_c_y = NULL;
  sf_mex_assign(&c7_c_y, sf_mex_create("y", &c7_c_u, 2, 0U, 0U, 0U, 0), false);
  sf_mex_addfield(c7_y, c7_c_y, "y", "y", 0);
  c7_d_u = c7_u.orientation;
  c7_d_y = NULL;
  sf_mex_assign(&c7_d_y, sf_mex_create("y", &c7_d_u, 4, 0U, 0U, 0U, 0), false);
  sf_mex_addfield(c7_y, c7_d_y, "orientation", "orientation", 0);
  c7_e_u = c7_u.color;
  c7_e_y = NULL;
  sf_mex_assign(&c7_e_y, sf_mex_create("y", &c7_e_u, 3, 0U, 0U, 0U, 0), false);
  sf_mex_addfield(c7_y, c7_e_y, "color", "color", 0);
  c7_f_u = c7_u.position;
  c7_f_y = NULL;
  sf_mex_assign(&c7_f_y, sf_mex_create("y", &c7_f_u, 3, 0U, 0U, 0U, 0), false);
  sf_mex_addfield(c7_y, c7_f_y, "position", "position", 0);
  c7_g_u = c7_u.valid;
  c7_g_y = NULL;
  sf_mex_assign(&c7_g_y, sf_mex_create("y", &c7_g_u, 3, 0U, 0U, 0U, 0), false);
  sf_mex_addfield(c7_y, c7_g_y, "valid", "valid", 0);
  sf_mex_assign(&c7_mxArrayOutData, c7_y, false);
  return c7_mxArrayOutData;
}

static c7_Player c7_n_emlrt_marshallIn(SFc7_manualControllerInstanceStruct
  *chartInstance, const mxArray *c7_u, const emlrtMsgIdentifier *c7_parentId)
{
  c7_Player c7_y;
  emlrtMsgIdentifier c7_thisId;
  static const char * c7_fieldNames[6] = { "x", "y", "orientation", "color",
    "position", "valid" };

  c7_thisId.fParent = c7_parentId;
  sf_mex_check_struct(c7_parentId, c7_u, 6, c7_fieldNames, 0U, NULL);
  c7_thisId.fIdentifier = "x";
  c7_y.x = c7_g_emlrt_marshallIn(chartInstance, sf_mex_dup(sf_mex_getfield(c7_u,
    "x", "x", 0)), &c7_thisId);
  c7_thisId.fIdentifier = "y";
  c7_y.y = c7_g_emlrt_marshallIn(chartInstance, sf_mex_dup(sf_mex_getfield(c7_u,
    "y", "y", 0)), &c7_thisId);
  c7_thisId.fIdentifier = "orientation";
  c7_y.orientation = c7_h_emlrt_marshallIn(chartInstance, sf_mex_dup
    (sf_mex_getfield(c7_u, "orientation", "orientation", 0)), &c7_thisId);
  c7_thisId.fIdentifier = "color";
  c7_y.color = c7_d_emlrt_marshallIn(chartInstance, sf_mex_dup(sf_mex_getfield
    (c7_u, "color", "color", 0)), &c7_thisId);
  c7_thisId.fIdentifier = "position";
  c7_y.position = c7_d_emlrt_marshallIn(chartInstance, sf_mex_dup
    (sf_mex_getfield(c7_u, "position", "position", 0)), &c7_thisId);
  c7_thisId.fIdentifier = "valid";
  c7_y.valid = c7_d_emlrt_marshallIn(chartInstance, sf_mex_dup(sf_mex_getfield
    (c7_u, "valid", "valid", 0)), &c7_thisId);
  sf_mex_destroy(&c7_u);
  return c7_y;
}

static void c7_h_sf_marshallIn(void *chartInstanceVoid, const mxArray
  *c7_mxArrayInData, const char_T *c7_varName, void *c7_outData)
{
  const mxArray *c7_tempEmpty;
  const char_T *c7_identifier;
  emlrtMsgIdentifier c7_thisId;
  c7_Player c7_y;
  SFc7_manualControllerInstanceStruct *chartInstance;
  chartInstance = (SFc7_manualControllerInstanceStruct *)chartInstanceVoid;
  c7_tempEmpty = sf_mex_dup(c7_mxArrayInData);
  c7_identifier = c7_varName;
  c7_thisId.fIdentifier = c7_identifier;
  c7_thisId.fParent = NULL;
  c7_y = c7_n_emlrt_marshallIn(chartInstance, sf_mex_dup(c7_tempEmpty),
    &c7_thisId);
  sf_mex_destroy(&c7_tempEmpty);
  *(c7_Player *)c7_outData = c7_y;
  sf_mex_destroy(&c7_mxArrayInData);
}

const mxArray *sf_c7_manualController_get_eml_resolved_functions_info(void)
{
  const mxArray *c7_nameCaptureInfo = NULL;
  c7_nameCaptureInfo = NULL;
  sf_mex_assign(&c7_nameCaptureInfo, sf_mex_createstruct("structure", 2, 62, 1),
                false);
  c7_info_helper(&c7_nameCaptureInfo);
  sf_mex_emlrtNameCapturePostProcessR2012a(&c7_nameCaptureInfo);
  return c7_nameCaptureInfo;
}

static void c7_info_helper(const mxArray **c7_info)
{
  const mxArray *c7_rhs0 = NULL;
  const mxArray *c7_lhs0 = NULL;
  const mxArray *c7_rhs1 = NULL;
  const mxArray *c7_lhs1 = NULL;
  const mxArray *c7_rhs2 = NULL;
  const mxArray *c7_lhs2 = NULL;
  const mxArray *c7_rhs3 = NULL;
  const mxArray *c7_lhs3 = NULL;
  const mxArray *c7_rhs4 = NULL;
  const mxArray *c7_lhs4 = NULL;
  const mxArray *c7_rhs5 = NULL;
  const mxArray *c7_lhs5 = NULL;
  const mxArray *c7_rhs6 = NULL;
  const mxArray *c7_lhs6 = NULL;
  const mxArray *c7_rhs7 = NULL;
  const mxArray *c7_lhs7 = NULL;
  const mxArray *c7_rhs8 = NULL;
  const mxArray *c7_lhs8 = NULL;
  const mxArray *c7_rhs9 = NULL;
  const mxArray *c7_lhs9 = NULL;
  const mxArray *c7_rhs10 = NULL;
  const mxArray *c7_lhs10 = NULL;
  const mxArray *c7_rhs11 = NULL;
  const mxArray *c7_lhs11 = NULL;
  const mxArray *c7_rhs12 = NULL;
  const mxArray *c7_lhs12 = NULL;
  const mxArray *c7_rhs13 = NULL;
  const mxArray *c7_lhs13 = NULL;
  const mxArray *c7_rhs14 = NULL;
  const mxArray *c7_lhs14 = NULL;
  const mxArray *c7_rhs15 = NULL;
  const mxArray *c7_lhs15 = NULL;
  const mxArray *c7_rhs16 = NULL;
  const mxArray *c7_lhs16 = NULL;
  const mxArray *c7_rhs17 = NULL;
  const mxArray *c7_lhs17 = NULL;
  const mxArray *c7_rhs18 = NULL;
  const mxArray *c7_lhs18 = NULL;
  const mxArray *c7_rhs19 = NULL;
  const mxArray *c7_lhs19 = NULL;
  const mxArray *c7_rhs20 = NULL;
  const mxArray *c7_lhs20 = NULL;
  const mxArray *c7_rhs21 = NULL;
  const mxArray *c7_lhs21 = NULL;
  const mxArray *c7_rhs22 = NULL;
  const mxArray *c7_lhs22 = NULL;
  const mxArray *c7_rhs23 = NULL;
  const mxArray *c7_lhs23 = NULL;
  const mxArray *c7_rhs24 = NULL;
  const mxArray *c7_lhs24 = NULL;
  const mxArray *c7_rhs25 = NULL;
  const mxArray *c7_lhs25 = NULL;
  const mxArray *c7_rhs26 = NULL;
  const mxArray *c7_lhs26 = NULL;
  const mxArray *c7_rhs27 = NULL;
  const mxArray *c7_lhs27 = NULL;
  const mxArray *c7_rhs28 = NULL;
  const mxArray *c7_lhs28 = NULL;
  const mxArray *c7_rhs29 = NULL;
  const mxArray *c7_lhs29 = NULL;
  const mxArray *c7_rhs30 = NULL;
  const mxArray *c7_lhs30 = NULL;
  const mxArray *c7_rhs31 = NULL;
  const mxArray *c7_lhs31 = NULL;
  const mxArray *c7_rhs32 = NULL;
  const mxArray *c7_lhs32 = NULL;
  const mxArray *c7_rhs33 = NULL;
  const mxArray *c7_lhs33 = NULL;
  const mxArray *c7_rhs34 = NULL;
  const mxArray *c7_lhs34 = NULL;
  const mxArray *c7_rhs35 = NULL;
  const mxArray *c7_lhs35 = NULL;
  const mxArray *c7_rhs36 = NULL;
  const mxArray *c7_lhs36 = NULL;
  const mxArray *c7_rhs37 = NULL;
  const mxArray *c7_lhs37 = NULL;
  const mxArray *c7_rhs38 = NULL;
  const mxArray *c7_lhs38 = NULL;
  const mxArray *c7_rhs39 = NULL;
  const mxArray *c7_lhs39 = NULL;
  const mxArray *c7_rhs40 = NULL;
  const mxArray *c7_lhs40 = NULL;
  const mxArray *c7_rhs41 = NULL;
  const mxArray *c7_lhs41 = NULL;
  const mxArray *c7_rhs42 = NULL;
  const mxArray *c7_lhs42 = NULL;
  const mxArray *c7_rhs43 = NULL;
  const mxArray *c7_lhs43 = NULL;
  const mxArray *c7_rhs44 = NULL;
  const mxArray *c7_lhs44 = NULL;
  const mxArray *c7_rhs45 = NULL;
  const mxArray *c7_lhs45 = NULL;
  const mxArray *c7_rhs46 = NULL;
  const mxArray *c7_lhs46 = NULL;
  const mxArray *c7_rhs47 = NULL;
  const mxArray *c7_lhs47 = NULL;
  const mxArray *c7_rhs48 = NULL;
  const mxArray *c7_lhs48 = NULL;
  const mxArray *c7_rhs49 = NULL;
  const mxArray *c7_lhs49 = NULL;
  const mxArray *c7_rhs50 = NULL;
  const mxArray *c7_lhs50 = NULL;
  const mxArray *c7_rhs51 = NULL;
  const mxArray *c7_lhs51 = NULL;
  const mxArray *c7_rhs52 = NULL;
  const mxArray *c7_lhs52 = NULL;
  const mxArray *c7_rhs53 = NULL;
  const mxArray *c7_lhs53 = NULL;
  const mxArray *c7_rhs54 = NULL;
  const mxArray *c7_lhs54 = NULL;
  const mxArray *c7_rhs55 = NULL;
  const mxArray *c7_lhs55 = NULL;
  const mxArray *c7_rhs56 = NULL;
  const mxArray *c7_lhs56 = NULL;
  const mxArray *c7_rhs57 = NULL;
  const mxArray *c7_lhs57 = NULL;
  const mxArray *c7_rhs58 = NULL;
  const mxArray *c7_lhs58 = NULL;
  const mxArray *c7_rhs59 = NULL;
  const mxArray *c7_lhs59 = NULL;
  const mxArray *c7_rhs60 = NULL;
  const mxArray *c7_lhs60 = NULL;
  const mxArray *c7_rhs61 = NULL;
  const mxArray *c7_lhs61 = NULL;
  sf_mex_addfield(*c7_info, c7_b_emlrt_marshallOut(""), "context", "context", 0);
  sf_mex_addfield(*c7_info, c7_b_emlrt_marshallOut("any"), "name", "name", 0);
  sf_mex_addfield(*c7_info, c7_b_emlrt_marshallOut("uint8"), "dominantType",
                  "dominantType", 0);
  sf_mex_addfield(*c7_info, c7_b_emlrt_marshallOut(
    "[ILXE]$matlabroot$/toolbox/eml/lib/matlab/ops/any.m"), "resolved",
                  "resolved", 0);
  sf_mex_addfield(*c7_info, c7_c_emlrt_marshallOut(1372586016U), "fileTimeLo",
                  "fileTimeLo", 0);
  sf_mex_addfield(*c7_info, c7_c_emlrt_marshallOut(0U), "fileTimeHi",
                  "fileTimeHi", 0);
  sf_mex_addfield(*c7_info, c7_c_emlrt_marshallOut(0U), "mFileTimeLo",
                  "mFileTimeLo", 0);
  sf_mex_addfield(*c7_info, c7_c_emlrt_marshallOut(0U), "mFileTimeHi",
                  "mFileTimeHi", 0);
  sf_mex_assign(&c7_rhs0, sf_mex_createcellmatrix(0, 1), false);
  sf_mex_assign(&c7_lhs0, sf_mex_createcellmatrix(0, 1), false);
  sf_mex_addfield(*c7_info, sf_mex_duplicatearraysafe(&c7_rhs0), "rhs", "rhs", 0);
  sf_mex_addfield(*c7_info, sf_mex_duplicatearraysafe(&c7_lhs0), "lhs", "lhs", 0);
  sf_mex_addfield(*c7_info, c7_b_emlrt_marshallOut(
    "[ILXE]$matlabroot$/toolbox/eml/lib/matlab/ops/any.m"), "context", "context",
                  1);
  sf_mex_addfield(*c7_info, c7_b_emlrt_marshallOut("coder.internal.assert"),
                  "name", "name", 1);
  sf_mex_addfield(*c7_info, c7_b_emlrt_marshallOut("char"), "dominantType",
                  "dominantType", 1);
  sf_mex_addfield(*c7_info, c7_b_emlrt_marshallOut(
    "[ILXE]$matlabroot$/toolbox/shared/coder/coder/+coder/+internal/assert.m"),
                  "resolved", "resolved", 1);
  sf_mex_addfield(*c7_info, c7_c_emlrt_marshallOut(1389721374U), "fileTimeLo",
                  "fileTimeLo", 1);
  sf_mex_addfield(*c7_info, c7_c_emlrt_marshallOut(0U), "fileTimeHi",
                  "fileTimeHi", 1);
  sf_mex_addfield(*c7_info, c7_c_emlrt_marshallOut(0U), "mFileTimeLo",
                  "mFileTimeLo", 1);
  sf_mex_addfield(*c7_info, c7_c_emlrt_marshallOut(0U), "mFileTimeHi",
                  "mFileTimeHi", 1);
  sf_mex_assign(&c7_rhs1, sf_mex_createcellmatrix(0, 1), false);
  sf_mex_assign(&c7_lhs1, sf_mex_createcellmatrix(0, 1), false);
  sf_mex_addfield(*c7_info, sf_mex_duplicatearraysafe(&c7_rhs1), "rhs", "rhs", 1);
  sf_mex_addfield(*c7_info, sf_mex_duplicatearraysafe(&c7_lhs1), "lhs", "lhs", 1);
  sf_mex_addfield(*c7_info, c7_b_emlrt_marshallOut(
    "[ILXE]$matlabroot$/toolbox/eml/lib/matlab/ops/any.m"), "context", "context",
                  2);
  sf_mex_addfield(*c7_info, c7_b_emlrt_marshallOut(
    "coder.internal.isBuiltInNumeric"), "name", "name", 2);
  sf_mex_addfield(*c7_info, c7_b_emlrt_marshallOut("uint8"), "dominantType",
                  "dominantType", 2);
  sf_mex_addfield(*c7_info, c7_b_emlrt_marshallOut(
    "[ILXE]$matlabroot$/toolbox/shared/coder/coder/+coder/+internal/isBuiltInNumeric.m"),
                  "resolved", "resolved", 2);
  sf_mex_addfield(*c7_info, c7_c_emlrt_marshallOut(1395935456U), "fileTimeLo",
                  "fileTimeLo", 2);
  sf_mex_addfield(*c7_info, c7_c_emlrt_marshallOut(0U), "fileTimeHi",
                  "fileTimeHi", 2);
  sf_mex_addfield(*c7_info, c7_c_emlrt_marshallOut(0U), "mFileTimeLo",
                  "mFileTimeLo", 2);
  sf_mex_addfield(*c7_info, c7_c_emlrt_marshallOut(0U), "mFileTimeHi",
                  "mFileTimeHi", 2);
  sf_mex_assign(&c7_rhs2, sf_mex_createcellmatrix(0, 1), false);
  sf_mex_assign(&c7_lhs2, sf_mex_createcellmatrix(0, 1), false);
  sf_mex_addfield(*c7_info, sf_mex_duplicatearraysafe(&c7_rhs2), "rhs", "rhs", 2);
  sf_mex_addfield(*c7_info, sf_mex_duplicatearraysafe(&c7_lhs2), "lhs", "lhs", 2);
  sf_mex_addfield(*c7_info, c7_b_emlrt_marshallOut(
    "[ILXE]$matlabroot$/toolbox/eml/lib/matlab/ops/any.m"), "context", "context",
                  3);
  sf_mex_addfield(*c7_info, c7_b_emlrt_marshallOut("coder.internal.allOrAny"),
                  "name", "name", 3);
  sf_mex_addfield(*c7_info, c7_b_emlrt_marshallOut("uint8"), "dominantType",
                  "dominantType", 3);
  sf_mex_addfield(*c7_info, c7_b_emlrt_marshallOut(
    "[ILXE]$matlabroot$/toolbox/shared/coder/coder/+coder/+internal/allOrAny.m"),
                  "resolved", "resolved", 3);
  sf_mex_addfield(*c7_info, c7_c_emlrt_marshallOut(1372586758U), "fileTimeLo",
                  "fileTimeLo", 3);
  sf_mex_addfield(*c7_info, c7_c_emlrt_marshallOut(0U), "fileTimeHi",
                  "fileTimeHi", 3);
  sf_mex_addfield(*c7_info, c7_c_emlrt_marshallOut(0U), "mFileTimeLo",
                  "mFileTimeLo", 3);
  sf_mex_addfield(*c7_info, c7_c_emlrt_marshallOut(0U), "mFileTimeHi",
                  "mFileTimeHi", 3);
  sf_mex_assign(&c7_rhs3, sf_mex_createcellmatrix(0, 1), false);
  sf_mex_assign(&c7_lhs3, sf_mex_createcellmatrix(0, 1), false);
  sf_mex_addfield(*c7_info, sf_mex_duplicatearraysafe(&c7_rhs3), "rhs", "rhs", 3);
  sf_mex_addfield(*c7_info, sf_mex_duplicatearraysafe(&c7_lhs3), "lhs", "lhs", 3);
  sf_mex_addfield(*c7_info, c7_b_emlrt_marshallOut(
    "[ILXE]$matlabroot$/toolbox/shared/coder/coder/+coder/+internal/allOrAny.m"),
                  "context", "context", 4);
  sf_mex_addfield(*c7_info, c7_b_emlrt_marshallOut("coder.internal.assert"),
                  "name", "name", 4);
  sf_mex_addfield(*c7_info, c7_b_emlrt_marshallOut("char"), "dominantType",
                  "dominantType", 4);
  sf_mex_addfield(*c7_info, c7_b_emlrt_marshallOut(
    "[ILXE]$matlabroot$/toolbox/shared/coder/coder/+coder/+internal/assert.m"),
                  "resolved", "resolved", 4);
  sf_mex_addfield(*c7_info, c7_c_emlrt_marshallOut(1389721374U), "fileTimeLo",
                  "fileTimeLo", 4);
  sf_mex_addfield(*c7_info, c7_c_emlrt_marshallOut(0U), "fileTimeHi",
                  "fileTimeHi", 4);
  sf_mex_addfield(*c7_info, c7_c_emlrt_marshallOut(0U), "mFileTimeLo",
                  "mFileTimeLo", 4);
  sf_mex_addfield(*c7_info, c7_c_emlrt_marshallOut(0U), "mFileTimeHi",
                  "mFileTimeHi", 4);
  sf_mex_assign(&c7_rhs4, sf_mex_createcellmatrix(0, 1), false);
  sf_mex_assign(&c7_lhs4, sf_mex_createcellmatrix(0, 1), false);
  sf_mex_addfield(*c7_info, sf_mex_duplicatearraysafe(&c7_rhs4), "rhs", "rhs", 4);
  sf_mex_addfield(*c7_info, sf_mex_duplicatearraysafe(&c7_lhs4), "lhs", "lhs", 4);
  sf_mex_addfield(*c7_info, c7_b_emlrt_marshallOut(
    "[ILXE]$matlabroot$/toolbox/shared/coder/coder/+coder/+internal/allOrAny.m"),
                  "context", "context", 5);
  sf_mex_addfield(*c7_info, c7_b_emlrt_marshallOut("isequal"), "name", "name", 5);
  sf_mex_addfield(*c7_info, c7_b_emlrt_marshallOut("uint8"), "dominantType",
                  "dominantType", 5);
  sf_mex_addfield(*c7_info, c7_b_emlrt_marshallOut(
    "[ILXE]$matlabroot$/toolbox/eml/lib/matlab/elmat/isequal.m"), "resolved",
                  "resolved", 5);
  sf_mex_addfield(*c7_info, c7_c_emlrt_marshallOut(1286822358U), "fileTimeLo",
                  "fileTimeLo", 5);
  sf_mex_addfield(*c7_info, c7_c_emlrt_marshallOut(0U), "fileTimeHi",
                  "fileTimeHi", 5);
  sf_mex_addfield(*c7_info, c7_c_emlrt_marshallOut(0U), "mFileTimeLo",
                  "mFileTimeLo", 5);
  sf_mex_addfield(*c7_info, c7_c_emlrt_marshallOut(0U), "mFileTimeHi",
                  "mFileTimeHi", 5);
  sf_mex_assign(&c7_rhs5, sf_mex_createcellmatrix(0, 1), false);
  sf_mex_assign(&c7_lhs5, sf_mex_createcellmatrix(0, 1), false);
  sf_mex_addfield(*c7_info, sf_mex_duplicatearraysafe(&c7_rhs5), "rhs", "rhs", 5);
  sf_mex_addfield(*c7_info, sf_mex_duplicatearraysafe(&c7_lhs5), "lhs", "lhs", 5);
  sf_mex_addfield(*c7_info, c7_b_emlrt_marshallOut(
    "[ILXE]$matlabroot$/toolbox/eml/lib/matlab/elmat/isequal.m"), "context",
                  "context", 6);
  sf_mex_addfield(*c7_info, c7_b_emlrt_marshallOut("eml_isequal_core"), "name",
                  "name", 6);
  sf_mex_addfield(*c7_info, c7_b_emlrt_marshallOut("uint8"), "dominantType",
                  "dominantType", 6);
  sf_mex_addfield(*c7_info, c7_b_emlrt_marshallOut(
    "[ILXE]$matlabroot$/toolbox/eml/lib/matlab/eml/eml_isequal_core.m"),
                  "resolved", "resolved", 6);
  sf_mex_addfield(*c7_info, c7_c_emlrt_marshallOut(1286822386U), "fileTimeLo",
                  "fileTimeLo", 6);
  sf_mex_addfield(*c7_info, c7_c_emlrt_marshallOut(0U), "fileTimeHi",
                  "fileTimeHi", 6);
  sf_mex_addfield(*c7_info, c7_c_emlrt_marshallOut(0U), "mFileTimeLo",
                  "mFileTimeLo", 6);
  sf_mex_addfield(*c7_info, c7_c_emlrt_marshallOut(0U), "mFileTimeHi",
                  "mFileTimeHi", 6);
  sf_mex_assign(&c7_rhs6, sf_mex_createcellmatrix(0, 1), false);
  sf_mex_assign(&c7_lhs6, sf_mex_createcellmatrix(0, 1), false);
  sf_mex_addfield(*c7_info, sf_mex_duplicatearraysafe(&c7_rhs6), "rhs", "rhs", 6);
  sf_mex_addfield(*c7_info, sf_mex_duplicatearraysafe(&c7_lhs6), "lhs", "lhs", 6);
  sf_mex_addfield(*c7_info, c7_b_emlrt_marshallOut(
    "[ILXE]$matlabroot$/toolbox/shared/coder/coder/+coder/+internal/allOrAny.m"),
                  "context", "context", 7);
  sf_mex_addfield(*c7_info, c7_b_emlrt_marshallOut(
    "coder.internal.constNonSingletonDim"), "name", "name", 7);
  sf_mex_addfield(*c7_info, c7_b_emlrt_marshallOut("uint8"), "dominantType",
                  "dominantType", 7);
  sf_mex_addfield(*c7_info, c7_b_emlrt_marshallOut(
    "[ILXE]$matlabroot$/toolbox/shared/coder/coder/+coder/+internal/constNonSingletonDim.m"),
                  "resolved", "resolved", 7);
  sf_mex_addfield(*c7_info, c7_c_emlrt_marshallOut(1372586760U), "fileTimeLo",
                  "fileTimeLo", 7);
  sf_mex_addfield(*c7_info, c7_c_emlrt_marshallOut(0U), "fileTimeHi",
                  "fileTimeHi", 7);
  sf_mex_addfield(*c7_info, c7_c_emlrt_marshallOut(0U), "mFileTimeLo",
                  "mFileTimeLo", 7);
  sf_mex_addfield(*c7_info, c7_c_emlrt_marshallOut(0U), "mFileTimeHi",
                  "mFileTimeHi", 7);
  sf_mex_assign(&c7_rhs7, sf_mex_createcellmatrix(0, 1), false);
  sf_mex_assign(&c7_lhs7, sf_mex_createcellmatrix(0, 1), false);
  sf_mex_addfield(*c7_info, sf_mex_duplicatearraysafe(&c7_rhs7), "rhs", "rhs", 7);
  sf_mex_addfield(*c7_info, sf_mex_duplicatearraysafe(&c7_lhs7), "lhs", "lhs", 7);
  sf_mex_addfield(*c7_info, c7_b_emlrt_marshallOut(
    "[ILXE]$matlabroot$/toolbox/shared/coder/coder/+coder/+internal/allOrAny.m"),
                  "context", "context", 8);
  sf_mex_addfield(*c7_info, c7_b_emlrt_marshallOut("isnan"), "name", "name", 8);
  sf_mex_addfield(*c7_info, c7_b_emlrt_marshallOut("uint8"), "dominantType",
                  "dominantType", 8);
  sf_mex_addfield(*c7_info, c7_b_emlrt_marshallOut(
    "[ILXE]$matlabroot$/toolbox/eml/lib/matlab/elmat/isnan.m"), "resolved",
                  "resolved", 8);
  sf_mex_addfield(*c7_info, c7_c_emlrt_marshallOut(1363717458U), "fileTimeLo",
                  "fileTimeLo", 8);
  sf_mex_addfield(*c7_info, c7_c_emlrt_marshallOut(0U), "fileTimeHi",
                  "fileTimeHi", 8);
  sf_mex_addfield(*c7_info, c7_c_emlrt_marshallOut(0U), "mFileTimeLo",
                  "mFileTimeLo", 8);
  sf_mex_addfield(*c7_info, c7_c_emlrt_marshallOut(0U), "mFileTimeHi",
                  "mFileTimeHi", 8);
  sf_mex_assign(&c7_rhs8, sf_mex_createcellmatrix(0, 1), false);
  sf_mex_assign(&c7_lhs8, sf_mex_createcellmatrix(0, 1), false);
  sf_mex_addfield(*c7_info, sf_mex_duplicatearraysafe(&c7_rhs8), "rhs", "rhs", 8);
  sf_mex_addfield(*c7_info, sf_mex_duplicatearraysafe(&c7_lhs8), "lhs", "lhs", 8);
  sf_mex_addfield(*c7_info, c7_b_emlrt_marshallOut(
    "[ILXE]$matlabroot$/toolbox/eml/lib/matlab/elmat/isnan.m"), "context",
                  "context", 9);
  sf_mex_addfield(*c7_info, c7_b_emlrt_marshallOut(
    "coder.internal.isBuiltInNumeric"), "name", "name", 9);
  sf_mex_addfield(*c7_info, c7_b_emlrt_marshallOut("uint8"), "dominantType",
                  "dominantType", 9);
  sf_mex_addfield(*c7_info, c7_b_emlrt_marshallOut(
    "[ILXE]$matlabroot$/toolbox/shared/coder/coder/+coder/+internal/isBuiltInNumeric.m"),
                  "resolved", "resolved", 9);
  sf_mex_addfield(*c7_info, c7_c_emlrt_marshallOut(1395935456U), "fileTimeLo",
                  "fileTimeLo", 9);
  sf_mex_addfield(*c7_info, c7_c_emlrt_marshallOut(0U), "fileTimeHi",
                  "fileTimeHi", 9);
  sf_mex_addfield(*c7_info, c7_c_emlrt_marshallOut(0U), "mFileTimeLo",
                  "mFileTimeLo", 9);
  sf_mex_addfield(*c7_info, c7_c_emlrt_marshallOut(0U), "mFileTimeHi",
                  "mFileTimeHi", 9);
  sf_mex_assign(&c7_rhs9, sf_mex_createcellmatrix(0, 1), false);
  sf_mex_assign(&c7_lhs9, sf_mex_createcellmatrix(0, 1), false);
  sf_mex_addfield(*c7_info, sf_mex_duplicatearraysafe(&c7_rhs9), "rhs", "rhs", 9);
  sf_mex_addfield(*c7_info, sf_mex_duplicatearraysafe(&c7_lhs9), "lhs", "lhs", 9);
  sf_mex_addfield(*c7_info, c7_b_emlrt_marshallOut(""), "context", "context", 10);
  sf_mex_addfield(*c7_info, c7_b_emlrt_marshallOut("typecast"), "name", "name",
                  10);
  sf_mex_addfield(*c7_info, c7_b_emlrt_marshallOut("uint8"), "dominantType",
                  "dominantType", 10);
  sf_mex_addfield(*c7_info, c7_b_emlrt_marshallOut(
    "[ILXE]$matlabroot$/toolbox/eml/lib/matlab/datatypes/typecast.m"),
                  "resolved", "resolved", 10);
  sf_mex_addfield(*c7_info, c7_c_emlrt_marshallOut(1407168096U), "fileTimeLo",
                  "fileTimeLo", 10);
  sf_mex_addfield(*c7_info, c7_c_emlrt_marshallOut(0U), "fileTimeHi",
                  "fileTimeHi", 10);
  sf_mex_addfield(*c7_info, c7_c_emlrt_marshallOut(0U), "mFileTimeLo",
                  "mFileTimeLo", 10);
  sf_mex_addfield(*c7_info, c7_c_emlrt_marshallOut(0U), "mFileTimeHi",
                  "mFileTimeHi", 10);
  sf_mex_assign(&c7_rhs10, sf_mex_createcellmatrix(0, 1), false);
  sf_mex_assign(&c7_lhs10, sf_mex_createcellmatrix(0, 1), false);
  sf_mex_addfield(*c7_info, sf_mex_duplicatearraysafe(&c7_rhs10), "rhs", "rhs",
                  10);
  sf_mex_addfield(*c7_info, sf_mex_duplicatearraysafe(&c7_lhs10), "lhs", "lhs",
                  10);
  sf_mex_addfield(*c7_info, c7_b_emlrt_marshallOut(
    "[ILXE]$matlabroot$/toolbox/eml/lib/matlab/datatypes/typecast.m"), "context",
                  "context", 11);
  sf_mex_addfield(*c7_info, c7_b_emlrt_marshallOut(
    "coder.internal.isBuiltInNumeric"), "name", "name", 11);
  sf_mex_addfield(*c7_info, c7_b_emlrt_marshallOut("uint8"), "dominantType",
                  "dominantType", 11);
  sf_mex_addfield(*c7_info, c7_b_emlrt_marshallOut(
    "[ILXE]$matlabroot$/toolbox/shared/coder/coder/+coder/+internal/isBuiltInNumeric.m"),
                  "resolved", "resolved", 11);
  sf_mex_addfield(*c7_info, c7_c_emlrt_marshallOut(1395935456U), "fileTimeLo",
                  "fileTimeLo", 11);
  sf_mex_addfield(*c7_info, c7_c_emlrt_marshallOut(0U), "fileTimeHi",
                  "fileTimeHi", 11);
  sf_mex_addfield(*c7_info, c7_c_emlrt_marshallOut(0U), "mFileTimeLo",
                  "mFileTimeLo", 11);
  sf_mex_addfield(*c7_info, c7_c_emlrt_marshallOut(0U), "mFileTimeHi",
                  "mFileTimeHi", 11);
  sf_mex_assign(&c7_rhs11, sf_mex_createcellmatrix(0, 1), false);
  sf_mex_assign(&c7_lhs11, sf_mex_createcellmatrix(0, 1), false);
  sf_mex_addfield(*c7_info, sf_mex_duplicatearraysafe(&c7_rhs11), "rhs", "rhs",
                  11);
  sf_mex_addfield(*c7_info, sf_mex_duplicatearraysafe(&c7_lhs11), "lhs", "lhs",
                  11);
  sf_mex_addfield(*c7_info, c7_b_emlrt_marshallOut(
    "[ILXE]$matlabroot$/toolbox/eml/lib/matlab/datatypes/typecast.m"), "context",
                  "context", 12);
  sf_mex_addfield(*c7_info, c7_b_emlrt_marshallOut("deblank"), "name", "name",
                  12);
  sf_mex_addfield(*c7_info, c7_b_emlrt_marshallOut("char"), "dominantType",
                  "dominantType", 12);
  sf_mex_addfield(*c7_info, c7_b_emlrt_marshallOut(
    "[ILXE]$matlabroot$/toolbox/eml/lib/matlab/strfun/deblank.m"), "resolved",
                  "resolved", 12);
  sf_mex_addfield(*c7_info, c7_c_emlrt_marshallOut(1331308488U), "fileTimeLo",
                  "fileTimeLo", 12);
  sf_mex_addfield(*c7_info, c7_c_emlrt_marshallOut(0U), "fileTimeHi",
                  "fileTimeHi", 12);
  sf_mex_addfield(*c7_info, c7_c_emlrt_marshallOut(0U), "mFileTimeLo",
                  "mFileTimeLo", 12);
  sf_mex_addfield(*c7_info, c7_c_emlrt_marshallOut(0U), "mFileTimeHi",
                  "mFileTimeHi", 12);
  sf_mex_assign(&c7_rhs12, sf_mex_createcellmatrix(0, 1), false);
  sf_mex_assign(&c7_lhs12, sf_mex_createcellmatrix(0, 1), false);
  sf_mex_addfield(*c7_info, sf_mex_duplicatearraysafe(&c7_rhs12), "rhs", "rhs",
                  12);
  sf_mex_addfield(*c7_info, sf_mex_duplicatearraysafe(&c7_lhs12), "lhs", "lhs",
                  12);
  sf_mex_addfield(*c7_info, c7_b_emlrt_marshallOut(
    "[ILXE]$matlabroot$/toolbox/eml/lib/matlab/strfun/deblank.m"), "context",
                  "context", 13);
  sf_mex_addfield(*c7_info, c7_b_emlrt_marshallOut("ismatrix"), "name", "name",
                  13);
  sf_mex_addfield(*c7_info, c7_b_emlrt_marshallOut("char"), "dominantType",
                  "dominantType", 13);
  sf_mex_addfield(*c7_info, c7_b_emlrt_marshallOut(
    "[ILXE]$matlabroot$/toolbox/eml/lib/matlab/elmat/ismatrix.m"), "resolved",
                  "resolved", 13);
  sf_mex_addfield(*c7_info, c7_c_emlrt_marshallOut(1331308458U), "fileTimeLo",
                  "fileTimeLo", 13);
  sf_mex_addfield(*c7_info, c7_c_emlrt_marshallOut(0U), "fileTimeHi",
                  "fileTimeHi", 13);
  sf_mex_addfield(*c7_info, c7_c_emlrt_marshallOut(0U), "mFileTimeLo",
                  "mFileTimeLo", 13);
  sf_mex_addfield(*c7_info, c7_c_emlrt_marshallOut(0U), "mFileTimeHi",
                  "mFileTimeHi", 13);
  sf_mex_assign(&c7_rhs13, sf_mex_createcellmatrix(0, 1), false);
  sf_mex_assign(&c7_lhs13, sf_mex_createcellmatrix(0, 1), false);
  sf_mex_addfield(*c7_info, sf_mex_duplicatearraysafe(&c7_rhs13), "rhs", "rhs",
                  13);
  sf_mex_addfield(*c7_info, sf_mex_duplicatearraysafe(&c7_lhs13), "lhs", "lhs",
                  13);
  sf_mex_addfield(*c7_info, c7_b_emlrt_marshallOut(
    "[ILXE]$matlabroot$/toolbox/eml/lib/matlab/strfun/deblank.m!allwspace"),
                  "context", "context", 14);
  sf_mex_addfield(*c7_info, c7_b_emlrt_marshallOut("isstrprop"), "name", "name",
                  14);
  sf_mex_addfield(*c7_info, c7_b_emlrt_marshallOut("char"), "dominantType",
                  "dominantType", 14);
  sf_mex_addfield(*c7_info, c7_b_emlrt_marshallOut(
    "[ILXE]$matlabroot$/toolbox/eml/lib/matlab/strfun/isstrprop.m"), "resolved",
                  "resolved", 14);
  sf_mex_addfield(*c7_info, c7_c_emlrt_marshallOut(1375984294U), "fileTimeLo",
                  "fileTimeLo", 14);
  sf_mex_addfield(*c7_info, c7_c_emlrt_marshallOut(0U), "fileTimeHi",
                  "fileTimeHi", 14);
  sf_mex_addfield(*c7_info, c7_c_emlrt_marshallOut(0U), "mFileTimeLo",
                  "mFileTimeLo", 14);
  sf_mex_addfield(*c7_info, c7_c_emlrt_marshallOut(0U), "mFileTimeHi",
                  "mFileTimeHi", 14);
  sf_mex_assign(&c7_rhs14, sf_mex_createcellmatrix(0, 1), false);
  sf_mex_assign(&c7_lhs14, sf_mex_createcellmatrix(0, 1), false);
  sf_mex_addfield(*c7_info, sf_mex_duplicatearraysafe(&c7_rhs14), "rhs", "rhs",
                  14);
  sf_mex_addfield(*c7_info, sf_mex_duplicatearraysafe(&c7_lhs14), "lhs", "lhs",
                  14);
  sf_mex_addfield(*c7_info, c7_b_emlrt_marshallOut(
    "[ILXE]$matlabroot$/toolbox/eml/lib/matlab/strfun/isstrprop.m!apply_property_predicate"),
                  "context", "context", 15);
  sf_mex_addfield(*c7_info, c7_b_emlrt_marshallOut("eml_assert_supported_string"),
                  "name", "name", 15);
  sf_mex_addfield(*c7_info, c7_b_emlrt_marshallOut("char"), "dominantType",
                  "dominantType", 15);
  sf_mex_addfield(*c7_info, c7_b_emlrt_marshallOut(
    "[ILXE]$matlabroot$/toolbox/eml/lib/matlab/strfun/eml_assert_supported_string.m"),
                  "resolved", "resolved", 15);
  sf_mex_addfield(*c7_info, c7_c_emlrt_marshallOut(1327422710U), "fileTimeLo",
                  "fileTimeLo", 15);
  sf_mex_addfield(*c7_info, c7_c_emlrt_marshallOut(0U), "fileTimeHi",
                  "fileTimeHi", 15);
  sf_mex_addfield(*c7_info, c7_c_emlrt_marshallOut(0U), "mFileTimeLo",
                  "mFileTimeLo", 15);
  sf_mex_addfield(*c7_info, c7_c_emlrt_marshallOut(0U), "mFileTimeHi",
                  "mFileTimeHi", 15);
  sf_mex_assign(&c7_rhs15, sf_mex_createcellmatrix(0, 1), false);
  sf_mex_assign(&c7_lhs15, sf_mex_createcellmatrix(0, 1), false);
  sf_mex_addfield(*c7_info, sf_mex_duplicatearraysafe(&c7_rhs15), "rhs", "rhs",
                  15);
  sf_mex_addfield(*c7_info, sf_mex_duplicatearraysafe(&c7_lhs15), "lhs", "lhs",
                  15);
  sf_mex_addfield(*c7_info, c7_b_emlrt_marshallOut(
    "[ILXE]$matlabroot$/toolbox/eml/lib/matlab/strfun/eml_assert_supported_string.m!inrange"),
                  "context", "context", 16);
  sf_mex_addfield(*c7_info, c7_b_emlrt_marshallOut("eml_charmax"), "name",
                  "name", 16);
  sf_mex_addfield(*c7_info, c7_b_emlrt_marshallOut(""), "dominantType",
                  "dominantType", 16);
  sf_mex_addfield(*c7_info, c7_b_emlrt_marshallOut(
    "[ILXE]$matlabroot$/toolbox/eml/lib/matlab/strfun/eml_charmax.m"),
                  "resolved", "resolved", 16);
  sf_mex_addfield(*c7_info, c7_c_emlrt_marshallOut(1327422710U), "fileTimeLo",
                  "fileTimeLo", 16);
  sf_mex_addfield(*c7_info, c7_c_emlrt_marshallOut(0U), "fileTimeHi",
                  "fileTimeHi", 16);
  sf_mex_addfield(*c7_info, c7_c_emlrt_marshallOut(0U), "mFileTimeLo",
                  "mFileTimeLo", 16);
  sf_mex_addfield(*c7_info, c7_c_emlrt_marshallOut(0U), "mFileTimeHi",
                  "mFileTimeHi", 16);
  sf_mex_assign(&c7_rhs16, sf_mex_createcellmatrix(0, 1), false);
  sf_mex_assign(&c7_lhs16, sf_mex_createcellmatrix(0, 1), false);
  sf_mex_addfield(*c7_info, sf_mex_duplicatearraysafe(&c7_rhs16), "rhs", "rhs",
                  16);
  sf_mex_addfield(*c7_info, sf_mex_duplicatearraysafe(&c7_lhs16), "lhs", "lhs",
                  16);
  sf_mex_addfield(*c7_info, c7_b_emlrt_marshallOut(
    "[ILXE]$matlabroot$/toolbox/eml/lib/matlab/strfun/eml_charmax.m"), "context",
                  "context", 17);
  sf_mex_addfield(*c7_info, c7_b_emlrt_marshallOut("intmax"), "name", "name", 17);
  sf_mex_addfield(*c7_info, c7_b_emlrt_marshallOut("char"), "dominantType",
                  "dominantType", 17);
  sf_mex_addfield(*c7_info, c7_b_emlrt_marshallOut(
    "[ILXE]$matlabroot$/toolbox/eml/lib/matlab/elmat/intmax.m"), "resolved",
                  "resolved", 17);
  sf_mex_addfield(*c7_info, c7_c_emlrt_marshallOut(1362265482U), "fileTimeLo",
                  "fileTimeLo", 17);
  sf_mex_addfield(*c7_info, c7_c_emlrt_marshallOut(0U), "fileTimeHi",
                  "fileTimeHi", 17);
  sf_mex_addfield(*c7_info, c7_c_emlrt_marshallOut(0U), "mFileTimeLo",
                  "mFileTimeLo", 17);
  sf_mex_addfield(*c7_info, c7_c_emlrt_marshallOut(0U), "mFileTimeHi",
                  "mFileTimeHi", 17);
  sf_mex_assign(&c7_rhs17, sf_mex_createcellmatrix(0, 1), false);
  sf_mex_assign(&c7_lhs17, sf_mex_createcellmatrix(0, 1), false);
  sf_mex_addfield(*c7_info, sf_mex_duplicatearraysafe(&c7_rhs17), "rhs", "rhs",
                  17);
  sf_mex_addfield(*c7_info, sf_mex_duplicatearraysafe(&c7_lhs17), "lhs", "lhs",
                  17);
  sf_mex_addfield(*c7_info, c7_b_emlrt_marshallOut(
    "[ILXE]$matlabroot$/toolbox/eml/lib/matlab/elmat/intmax.m"), "context",
                  "context", 18);
  sf_mex_addfield(*c7_info, c7_b_emlrt_marshallOut("eml_switch_helper"), "name",
                  "name", 18);
  sf_mex_addfield(*c7_info, c7_b_emlrt_marshallOut(""), "dominantType",
                  "dominantType", 18);
  sf_mex_addfield(*c7_info, c7_b_emlrt_marshallOut(
    "[ILXE]$matlabroot$/toolbox/eml/lib/matlab/eml/eml_switch_helper.m"),
                  "resolved", "resolved", 18);
  sf_mex_addfield(*c7_info, c7_c_emlrt_marshallOut(1393334458U), "fileTimeLo",
                  "fileTimeLo", 18);
  sf_mex_addfield(*c7_info, c7_c_emlrt_marshallOut(0U), "fileTimeHi",
                  "fileTimeHi", 18);
  sf_mex_addfield(*c7_info, c7_c_emlrt_marshallOut(0U), "mFileTimeLo",
                  "mFileTimeLo", 18);
  sf_mex_addfield(*c7_info, c7_c_emlrt_marshallOut(0U), "mFileTimeHi",
                  "mFileTimeHi", 18);
  sf_mex_assign(&c7_rhs18, sf_mex_createcellmatrix(0, 1), false);
  sf_mex_assign(&c7_lhs18, sf_mex_createcellmatrix(0, 1), false);
  sf_mex_addfield(*c7_info, sf_mex_duplicatearraysafe(&c7_rhs18), "rhs", "rhs",
                  18);
  sf_mex_addfield(*c7_info, sf_mex_duplicatearraysafe(&c7_lhs18), "lhs", "lhs",
                  18);
  sf_mex_addfield(*c7_info, c7_b_emlrt_marshallOut(
    "[ILXE]$matlabroot$/toolbox/eml/lib/matlab/strfun/eml_assert_supported_string.m"),
                  "context", "context", 19);
  sf_mex_addfield(*c7_info, c7_b_emlrt_marshallOut("eml_charmax"), "name",
                  "name", 19);
  sf_mex_addfield(*c7_info, c7_b_emlrt_marshallOut(""), "dominantType",
                  "dominantType", 19);
  sf_mex_addfield(*c7_info, c7_b_emlrt_marshallOut(
    "[ILXE]$matlabroot$/toolbox/eml/lib/matlab/strfun/eml_charmax.m"),
                  "resolved", "resolved", 19);
  sf_mex_addfield(*c7_info, c7_c_emlrt_marshallOut(1327422710U), "fileTimeLo",
                  "fileTimeLo", 19);
  sf_mex_addfield(*c7_info, c7_c_emlrt_marshallOut(0U), "fileTimeHi",
                  "fileTimeHi", 19);
  sf_mex_addfield(*c7_info, c7_c_emlrt_marshallOut(0U), "mFileTimeLo",
                  "mFileTimeLo", 19);
  sf_mex_addfield(*c7_info, c7_c_emlrt_marshallOut(0U), "mFileTimeHi",
                  "mFileTimeHi", 19);
  sf_mex_assign(&c7_rhs19, sf_mex_createcellmatrix(0, 1), false);
  sf_mex_assign(&c7_lhs19, sf_mex_createcellmatrix(0, 1), false);
  sf_mex_addfield(*c7_info, sf_mex_duplicatearraysafe(&c7_rhs19), "rhs", "rhs",
                  19);
  sf_mex_addfield(*c7_info, sf_mex_duplicatearraysafe(&c7_lhs19), "lhs", "lhs",
                  19);
  sf_mex_addfield(*c7_info, c7_b_emlrt_marshallOut(
    "[ILXE]$matlabroot$/toolbox/eml/lib/matlab/strfun/isstrprop.m!apply_property_predicate"),
                  "context", "context", 20);
  sf_mex_addfield(*c7_info, c7_b_emlrt_marshallOut("eml_charmax"), "name",
                  "name", 20);
  sf_mex_addfield(*c7_info, c7_b_emlrt_marshallOut(""), "dominantType",
                  "dominantType", 20);
  sf_mex_addfield(*c7_info, c7_b_emlrt_marshallOut(
    "[ILXE]$matlabroot$/toolbox/eml/lib/matlab/strfun/eml_charmax.m"),
                  "resolved", "resolved", 20);
  sf_mex_addfield(*c7_info, c7_c_emlrt_marshallOut(1327422710U), "fileTimeLo",
                  "fileTimeLo", 20);
  sf_mex_addfield(*c7_info, c7_c_emlrt_marshallOut(0U), "fileTimeHi",
                  "fileTimeHi", 20);
  sf_mex_addfield(*c7_info, c7_c_emlrt_marshallOut(0U), "mFileTimeLo",
                  "mFileTimeLo", 20);
  sf_mex_addfield(*c7_info, c7_c_emlrt_marshallOut(0U), "mFileTimeHi",
                  "mFileTimeHi", 20);
  sf_mex_assign(&c7_rhs20, sf_mex_createcellmatrix(0, 1), false);
  sf_mex_assign(&c7_lhs20, sf_mex_createcellmatrix(0, 1), false);
  sf_mex_addfield(*c7_info, sf_mex_duplicatearraysafe(&c7_rhs20), "rhs", "rhs",
                  20);
  sf_mex_addfield(*c7_info, sf_mex_duplicatearraysafe(&c7_lhs20), "lhs", "lhs",
                  20);
  sf_mex_addfield(*c7_info, c7_b_emlrt_marshallOut(
    "[ILXE]$matlabroot$/toolbox/eml/lib/matlab/strfun/isstrprop.m!apply_property_predicate"),
                  "context", "context", 21);
  sf_mex_addfield(*c7_info, c7_b_emlrt_marshallOut("colon"), "name", "name", 21);
  sf_mex_addfield(*c7_info, c7_b_emlrt_marshallOut("int8"), "dominantType",
                  "dominantType", 21);
  sf_mex_addfield(*c7_info, c7_b_emlrt_marshallOut(
    "[ILXE]$matlabroot$/toolbox/eml/lib/matlab/ops/colon.m"), "resolved",
                  "resolved", 21);
  sf_mex_addfield(*c7_info, c7_c_emlrt_marshallOut(1378299588U), "fileTimeLo",
                  "fileTimeLo", 21);
  sf_mex_addfield(*c7_info, c7_c_emlrt_marshallOut(0U), "fileTimeHi",
                  "fileTimeHi", 21);
  sf_mex_addfield(*c7_info, c7_c_emlrt_marshallOut(0U), "mFileTimeLo",
                  "mFileTimeLo", 21);
  sf_mex_addfield(*c7_info, c7_c_emlrt_marshallOut(0U), "mFileTimeHi",
                  "mFileTimeHi", 21);
  sf_mex_assign(&c7_rhs21, sf_mex_createcellmatrix(0, 1), false);
  sf_mex_assign(&c7_lhs21, sf_mex_createcellmatrix(0, 1), false);
  sf_mex_addfield(*c7_info, sf_mex_duplicatearraysafe(&c7_rhs21), "rhs", "rhs",
                  21);
  sf_mex_addfield(*c7_info, sf_mex_duplicatearraysafe(&c7_lhs21), "lhs", "lhs",
                  21);
  sf_mex_addfield(*c7_info, c7_b_emlrt_marshallOut(
    "[ILXE]$matlabroot$/toolbox/eml/lib/matlab/ops/colon.m"), "context",
                  "context", 22);
  sf_mex_addfield(*c7_info, c7_b_emlrt_marshallOut("colon"), "name", "name", 22);
  sf_mex_addfield(*c7_info, c7_b_emlrt_marshallOut("int8"), "dominantType",
                  "dominantType", 22);
  sf_mex_addfield(*c7_info, c7_b_emlrt_marshallOut(
    "[ILXE]$matlabroot$/toolbox/eml/lib/matlab/ops/colon.m"), "resolved",
                  "resolved", 22);
  sf_mex_addfield(*c7_info, c7_c_emlrt_marshallOut(1378299588U), "fileTimeLo",
                  "fileTimeLo", 22);
  sf_mex_addfield(*c7_info, c7_c_emlrt_marshallOut(0U), "fileTimeHi",
                  "fileTimeHi", 22);
  sf_mex_addfield(*c7_info, c7_c_emlrt_marshallOut(0U), "mFileTimeLo",
                  "mFileTimeLo", 22);
  sf_mex_addfield(*c7_info, c7_c_emlrt_marshallOut(0U), "mFileTimeHi",
                  "mFileTimeHi", 22);
  sf_mex_assign(&c7_rhs22, sf_mex_createcellmatrix(0, 1), false);
  sf_mex_assign(&c7_lhs22, sf_mex_createcellmatrix(0, 1), false);
  sf_mex_addfield(*c7_info, sf_mex_duplicatearraysafe(&c7_rhs22), "rhs", "rhs",
                  22);
  sf_mex_addfield(*c7_info, sf_mex_duplicatearraysafe(&c7_lhs22), "lhs", "lhs",
                  22);
  sf_mex_addfield(*c7_info, c7_b_emlrt_marshallOut(
    "[ILXE]$matlabroot$/toolbox/eml/lib/matlab/ops/colon.m"), "context",
                  "context", 23);
  sf_mex_addfield(*c7_info, c7_b_emlrt_marshallOut(
    "coder.internal.isBuiltInNumeric"), "name", "name", 23);
  sf_mex_addfield(*c7_info, c7_b_emlrt_marshallOut("double"), "dominantType",
                  "dominantType", 23);
  sf_mex_addfield(*c7_info, c7_b_emlrt_marshallOut(
    "[ILXE]$matlabroot$/toolbox/shared/coder/coder/+coder/+internal/isBuiltInNumeric.m"),
                  "resolved", "resolved", 23);
  sf_mex_addfield(*c7_info, c7_c_emlrt_marshallOut(1395935456U), "fileTimeLo",
                  "fileTimeLo", 23);
  sf_mex_addfield(*c7_info, c7_c_emlrt_marshallOut(0U), "fileTimeHi",
                  "fileTimeHi", 23);
  sf_mex_addfield(*c7_info, c7_c_emlrt_marshallOut(0U), "mFileTimeLo",
                  "mFileTimeLo", 23);
  sf_mex_addfield(*c7_info, c7_c_emlrt_marshallOut(0U), "mFileTimeHi",
                  "mFileTimeHi", 23);
  sf_mex_assign(&c7_rhs23, sf_mex_createcellmatrix(0, 1), false);
  sf_mex_assign(&c7_lhs23, sf_mex_createcellmatrix(0, 1), false);
  sf_mex_addfield(*c7_info, sf_mex_duplicatearraysafe(&c7_rhs23), "rhs", "rhs",
                  23);
  sf_mex_addfield(*c7_info, sf_mex_duplicatearraysafe(&c7_lhs23), "lhs", "lhs",
                  23);
  sf_mex_addfield(*c7_info, c7_b_emlrt_marshallOut(
    "[ILXE]$matlabroot$/toolbox/eml/lib/matlab/ops/colon.m"), "context",
                  "context", 24);
  sf_mex_addfield(*c7_info, c7_b_emlrt_marshallOut(
    "coder.internal.isBuiltInNumeric"), "name", "name", 24);
  sf_mex_addfield(*c7_info, c7_b_emlrt_marshallOut("int8"), "dominantType",
                  "dominantType", 24);
  sf_mex_addfield(*c7_info, c7_b_emlrt_marshallOut(
    "[ILXE]$matlabroot$/toolbox/shared/coder/coder/+coder/+internal/isBuiltInNumeric.m"),
                  "resolved", "resolved", 24);
  sf_mex_addfield(*c7_info, c7_c_emlrt_marshallOut(1395935456U), "fileTimeLo",
                  "fileTimeLo", 24);
  sf_mex_addfield(*c7_info, c7_c_emlrt_marshallOut(0U), "fileTimeHi",
                  "fileTimeHi", 24);
  sf_mex_addfield(*c7_info, c7_c_emlrt_marshallOut(0U), "mFileTimeLo",
                  "mFileTimeLo", 24);
  sf_mex_addfield(*c7_info, c7_c_emlrt_marshallOut(0U), "mFileTimeHi",
                  "mFileTimeHi", 24);
  sf_mex_assign(&c7_rhs24, sf_mex_createcellmatrix(0, 1), false);
  sf_mex_assign(&c7_lhs24, sf_mex_createcellmatrix(0, 1), false);
  sf_mex_addfield(*c7_info, sf_mex_duplicatearraysafe(&c7_rhs24), "rhs", "rhs",
                  24);
  sf_mex_addfield(*c7_info, sf_mex_duplicatearraysafe(&c7_lhs24), "lhs", "lhs",
                  24);
  sf_mex_addfield(*c7_info, c7_b_emlrt_marshallOut(
    "[ILXE]$matlabroot$/toolbox/eml/lib/matlab/ops/colon.m"), "context",
                  "context", 25);
  sf_mex_addfield(*c7_info, c7_b_emlrt_marshallOut("floor"), "name", "name", 25);
  sf_mex_addfield(*c7_info, c7_b_emlrt_marshallOut("double"), "dominantType",
                  "dominantType", 25);
  sf_mex_addfield(*c7_info, c7_b_emlrt_marshallOut(
    "[ILXE]$matlabroot$/toolbox/eml/lib/matlab/elfun/floor.m"), "resolved",
                  "resolved", 25);
  sf_mex_addfield(*c7_info, c7_c_emlrt_marshallOut(1363717454U), "fileTimeLo",
                  "fileTimeLo", 25);
  sf_mex_addfield(*c7_info, c7_c_emlrt_marshallOut(0U), "fileTimeHi",
                  "fileTimeHi", 25);
  sf_mex_addfield(*c7_info, c7_c_emlrt_marshallOut(0U), "mFileTimeLo",
                  "mFileTimeLo", 25);
  sf_mex_addfield(*c7_info, c7_c_emlrt_marshallOut(0U), "mFileTimeHi",
                  "mFileTimeHi", 25);
  sf_mex_assign(&c7_rhs25, sf_mex_createcellmatrix(0, 1), false);
  sf_mex_assign(&c7_lhs25, sf_mex_createcellmatrix(0, 1), false);
  sf_mex_addfield(*c7_info, sf_mex_duplicatearraysafe(&c7_rhs25), "rhs", "rhs",
                  25);
  sf_mex_addfield(*c7_info, sf_mex_duplicatearraysafe(&c7_lhs25), "lhs", "lhs",
                  25);
  sf_mex_addfield(*c7_info, c7_b_emlrt_marshallOut(
    "[ILXE]$matlabroot$/toolbox/eml/lib/matlab/elfun/floor.m"), "context",
                  "context", 26);
  sf_mex_addfield(*c7_info, c7_b_emlrt_marshallOut(
    "coder.internal.isBuiltInNumeric"), "name", "name", 26);
  sf_mex_addfield(*c7_info, c7_b_emlrt_marshallOut("double"), "dominantType",
                  "dominantType", 26);
  sf_mex_addfield(*c7_info, c7_b_emlrt_marshallOut(
    "[ILXE]$matlabroot$/toolbox/shared/coder/coder/+coder/+internal/isBuiltInNumeric.m"),
                  "resolved", "resolved", 26);
  sf_mex_addfield(*c7_info, c7_c_emlrt_marshallOut(1395935456U), "fileTimeLo",
                  "fileTimeLo", 26);
  sf_mex_addfield(*c7_info, c7_c_emlrt_marshallOut(0U), "fileTimeHi",
                  "fileTimeHi", 26);
  sf_mex_addfield(*c7_info, c7_c_emlrt_marshallOut(0U), "mFileTimeLo",
                  "mFileTimeLo", 26);
  sf_mex_addfield(*c7_info, c7_c_emlrt_marshallOut(0U), "mFileTimeHi",
                  "mFileTimeHi", 26);
  sf_mex_assign(&c7_rhs26, sf_mex_createcellmatrix(0, 1), false);
  sf_mex_assign(&c7_lhs26, sf_mex_createcellmatrix(0, 1), false);
  sf_mex_addfield(*c7_info, sf_mex_duplicatearraysafe(&c7_rhs26), "rhs", "rhs",
                  26);
  sf_mex_addfield(*c7_info, sf_mex_duplicatearraysafe(&c7_lhs26), "lhs", "lhs",
                  26);
  sf_mex_addfield(*c7_info, c7_b_emlrt_marshallOut(
    "[ILXE]$matlabroot$/toolbox/eml/lib/matlab/elfun/floor.m"), "context",
                  "context", 27);
  sf_mex_addfield(*c7_info, c7_b_emlrt_marshallOut("eml_scalar_floor"), "name",
                  "name", 27);
  sf_mex_addfield(*c7_info, c7_b_emlrt_marshallOut("double"), "dominantType",
                  "dominantType", 27);
  sf_mex_addfield(*c7_info, c7_b_emlrt_marshallOut(
    "[ILXE]$matlabroot$/toolbox/eml/lib/matlab/elfun/eml_scalar_floor.m"),
                  "resolved", "resolved", 27);
  sf_mex_addfield(*c7_info, c7_c_emlrt_marshallOut(1286822326U), "fileTimeLo",
                  "fileTimeLo", 27);
  sf_mex_addfield(*c7_info, c7_c_emlrt_marshallOut(0U), "fileTimeHi",
                  "fileTimeHi", 27);
  sf_mex_addfield(*c7_info, c7_c_emlrt_marshallOut(0U), "mFileTimeLo",
                  "mFileTimeLo", 27);
  sf_mex_addfield(*c7_info, c7_c_emlrt_marshallOut(0U), "mFileTimeHi",
                  "mFileTimeHi", 27);
  sf_mex_assign(&c7_rhs27, sf_mex_createcellmatrix(0, 1), false);
  sf_mex_assign(&c7_lhs27, sf_mex_createcellmatrix(0, 1), false);
  sf_mex_addfield(*c7_info, sf_mex_duplicatearraysafe(&c7_rhs27), "rhs", "rhs",
                  27);
  sf_mex_addfield(*c7_info, sf_mex_duplicatearraysafe(&c7_lhs27), "lhs", "lhs",
                  27);
  sf_mex_addfield(*c7_info, c7_b_emlrt_marshallOut(
    "[ILXE]$matlabroot$/toolbox/eml/lib/matlab/ops/colon.m!checkrange"),
                  "context", "context", 28);
  sf_mex_addfield(*c7_info, c7_b_emlrt_marshallOut("intmin"), "name", "name", 28);
  sf_mex_addfield(*c7_info, c7_b_emlrt_marshallOut("char"), "dominantType",
                  "dominantType", 28);
  sf_mex_addfield(*c7_info, c7_b_emlrt_marshallOut(
    "[ILXE]$matlabroot$/toolbox/eml/lib/matlab/elmat/intmin.m"), "resolved",
                  "resolved", 28);
  sf_mex_addfield(*c7_info, c7_c_emlrt_marshallOut(1362265482U), "fileTimeLo",
                  "fileTimeLo", 28);
  sf_mex_addfield(*c7_info, c7_c_emlrt_marshallOut(0U), "fileTimeHi",
                  "fileTimeHi", 28);
  sf_mex_addfield(*c7_info, c7_c_emlrt_marshallOut(0U), "mFileTimeLo",
                  "mFileTimeLo", 28);
  sf_mex_addfield(*c7_info, c7_c_emlrt_marshallOut(0U), "mFileTimeHi",
                  "mFileTimeHi", 28);
  sf_mex_assign(&c7_rhs28, sf_mex_createcellmatrix(0, 1), false);
  sf_mex_assign(&c7_lhs28, sf_mex_createcellmatrix(0, 1), false);
  sf_mex_addfield(*c7_info, sf_mex_duplicatearraysafe(&c7_rhs28), "rhs", "rhs",
                  28);
  sf_mex_addfield(*c7_info, sf_mex_duplicatearraysafe(&c7_lhs28), "lhs", "lhs",
                  28);
  sf_mex_addfield(*c7_info, c7_b_emlrt_marshallOut(
    "[ILXE]$matlabroot$/toolbox/eml/lib/matlab/elmat/intmin.m"), "context",
                  "context", 29);
  sf_mex_addfield(*c7_info, c7_b_emlrt_marshallOut("eml_switch_helper"), "name",
                  "name", 29);
  sf_mex_addfield(*c7_info, c7_b_emlrt_marshallOut(""), "dominantType",
                  "dominantType", 29);
  sf_mex_addfield(*c7_info, c7_b_emlrt_marshallOut(
    "[ILXE]$matlabroot$/toolbox/eml/lib/matlab/eml/eml_switch_helper.m"),
                  "resolved", "resolved", 29);
  sf_mex_addfield(*c7_info, c7_c_emlrt_marshallOut(1393334458U), "fileTimeLo",
                  "fileTimeLo", 29);
  sf_mex_addfield(*c7_info, c7_c_emlrt_marshallOut(0U), "fileTimeHi",
                  "fileTimeHi", 29);
  sf_mex_addfield(*c7_info, c7_c_emlrt_marshallOut(0U), "mFileTimeLo",
                  "mFileTimeLo", 29);
  sf_mex_addfield(*c7_info, c7_c_emlrt_marshallOut(0U), "mFileTimeHi",
                  "mFileTimeHi", 29);
  sf_mex_assign(&c7_rhs29, sf_mex_createcellmatrix(0, 1), false);
  sf_mex_assign(&c7_lhs29, sf_mex_createcellmatrix(0, 1), false);
  sf_mex_addfield(*c7_info, sf_mex_duplicatearraysafe(&c7_rhs29), "rhs", "rhs",
                  29);
  sf_mex_addfield(*c7_info, sf_mex_duplicatearraysafe(&c7_lhs29), "lhs", "lhs",
                  29);
  sf_mex_addfield(*c7_info, c7_b_emlrt_marshallOut(
    "[ILXE]$matlabroot$/toolbox/eml/lib/matlab/ops/colon.m!checkrange"),
                  "context", "context", 30);
  sf_mex_addfield(*c7_info, c7_b_emlrt_marshallOut("intmax"), "name", "name", 30);
  sf_mex_addfield(*c7_info, c7_b_emlrt_marshallOut("char"), "dominantType",
                  "dominantType", 30);
  sf_mex_addfield(*c7_info, c7_b_emlrt_marshallOut(
    "[ILXE]$matlabroot$/toolbox/eml/lib/matlab/elmat/intmax.m"), "resolved",
                  "resolved", 30);
  sf_mex_addfield(*c7_info, c7_c_emlrt_marshallOut(1362265482U), "fileTimeLo",
                  "fileTimeLo", 30);
  sf_mex_addfield(*c7_info, c7_c_emlrt_marshallOut(0U), "fileTimeHi",
                  "fileTimeHi", 30);
  sf_mex_addfield(*c7_info, c7_c_emlrt_marshallOut(0U), "mFileTimeLo",
                  "mFileTimeLo", 30);
  sf_mex_addfield(*c7_info, c7_c_emlrt_marshallOut(0U), "mFileTimeHi",
                  "mFileTimeHi", 30);
  sf_mex_assign(&c7_rhs30, sf_mex_createcellmatrix(0, 1), false);
  sf_mex_assign(&c7_lhs30, sf_mex_createcellmatrix(0, 1), false);
  sf_mex_addfield(*c7_info, sf_mex_duplicatearraysafe(&c7_rhs30), "rhs", "rhs",
                  30);
  sf_mex_addfield(*c7_info, sf_mex_duplicatearraysafe(&c7_lhs30), "lhs", "lhs",
                  30);
  sf_mex_addfield(*c7_info, c7_b_emlrt_marshallOut(
    "[ILXE]$matlabroot$/toolbox/eml/lib/matlab/ops/colon.m!eml_integer_colon_dispatcher"),
                  "context", "context", 31);
  sf_mex_addfield(*c7_info, c7_b_emlrt_marshallOut("intmin"), "name", "name", 31);
  sf_mex_addfield(*c7_info, c7_b_emlrt_marshallOut("char"), "dominantType",
                  "dominantType", 31);
  sf_mex_addfield(*c7_info, c7_b_emlrt_marshallOut(
    "[ILXE]$matlabroot$/toolbox/eml/lib/matlab/elmat/intmin.m"), "resolved",
                  "resolved", 31);
  sf_mex_addfield(*c7_info, c7_c_emlrt_marshallOut(1362265482U), "fileTimeLo",
                  "fileTimeLo", 31);
  sf_mex_addfield(*c7_info, c7_c_emlrt_marshallOut(0U), "fileTimeHi",
                  "fileTimeHi", 31);
  sf_mex_addfield(*c7_info, c7_c_emlrt_marshallOut(0U), "mFileTimeLo",
                  "mFileTimeLo", 31);
  sf_mex_addfield(*c7_info, c7_c_emlrt_marshallOut(0U), "mFileTimeHi",
                  "mFileTimeHi", 31);
  sf_mex_assign(&c7_rhs31, sf_mex_createcellmatrix(0, 1), false);
  sf_mex_assign(&c7_lhs31, sf_mex_createcellmatrix(0, 1), false);
  sf_mex_addfield(*c7_info, sf_mex_duplicatearraysafe(&c7_rhs31), "rhs", "rhs",
                  31);
  sf_mex_addfield(*c7_info, sf_mex_duplicatearraysafe(&c7_lhs31), "lhs", "lhs",
                  31);
  sf_mex_addfield(*c7_info, c7_b_emlrt_marshallOut(
    "[ILXE]$matlabroot$/toolbox/eml/lib/matlab/ops/colon.m!eml_integer_colon_dispatcher"),
                  "context", "context", 32);
  sf_mex_addfield(*c7_info, c7_b_emlrt_marshallOut("intmax"), "name", "name", 32);
  sf_mex_addfield(*c7_info, c7_b_emlrt_marshallOut("char"), "dominantType",
                  "dominantType", 32);
  sf_mex_addfield(*c7_info, c7_b_emlrt_marshallOut(
    "[ILXE]$matlabroot$/toolbox/eml/lib/matlab/elmat/intmax.m"), "resolved",
                  "resolved", 32);
  sf_mex_addfield(*c7_info, c7_c_emlrt_marshallOut(1362265482U), "fileTimeLo",
                  "fileTimeLo", 32);
  sf_mex_addfield(*c7_info, c7_c_emlrt_marshallOut(0U), "fileTimeHi",
                  "fileTimeHi", 32);
  sf_mex_addfield(*c7_info, c7_c_emlrt_marshallOut(0U), "mFileTimeLo",
                  "mFileTimeLo", 32);
  sf_mex_addfield(*c7_info, c7_c_emlrt_marshallOut(0U), "mFileTimeHi",
                  "mFileTimeHi", 32);
  sf_mex_assign(&c7_rhs32, sf_mex_createcellmatrix(0, 1), false);
  sf_mex_assign(&c7_lhs32, sf_mex_createcellmatrix(0, 1), false);
  sf_mex_addfield(*c7_info, sf_mex_duplicatearraysafe(&c7_rhs32), "rhs", "rhs",
                  32);
  sf_mex_addfield(*c7_info, sf_mex_duplicatearraysafe(&c7_lhs32), "lhs", "lhs",
                  32);
  sf_mex_addfield(*c7_info, c7_b_emlrt_marshallOut(
    "[ILXE]$matlabroot$/toolbox/eml/lib/matlab/ops/colon.m!eml_integer_colon_dispatcher"),
                  "context", "context", 33);
  sf_mex_addfield(*c7_info, c7_b_emlrt_marshallOut("eml_isa_uint"), "name",
                  "name", 33);
  sf_mex_addfield(*c7_info, c7_b_emlrt_marshallOut("int8"), "dominantType",
                  "dominantType", 33);
  sf_mex_addfield(*c7_info, c7_b_emlrt_marshallOut(
    "[ILXE]$matlabroot$/toolbox/eml/lib/matlab/eml/eml_isa_uint.m"), "resolved",
                  "resolved", 33);
  sf_mex_addfield(*c7_info, c7_c_emlrt_marshallOut(1375984288U), "fileTimeLo",
                  "fileTimeLo", 33);
  sf_mex_addfield(*c7_info, c7_c_emlrt_marshallOut(0U), "fileTimeHi",
                  "fileTimeHi", 33);
  sf_mex_addfield(*c7_info, c7_c_emlrt_marshallOut(0U), "mFileTimeLo",
                  "mFileTimeLo", 33);
  sf_mex_addfield(*c7_info, c7_c_emlrt_marshallOut(0U), "mFileTimeHi",
                  "mFileTimeHi", 33);
  sf_mex_assign(&c7_rhs33, sf_mex_createcellmatrix(0, 1), false);
  sf_mex_assign(&c7_lhs33, sf_mex_createcellmatrix(0, 1), false);
  sf_mex_addfield(*c7_info, sf_mex_duplicatearraysafe(&c7_rhs33), "rhs", "rhs",
                  33);
  sf_mex_addfield(*c7_info, sf_mex_duplicatearraysafe(&c7_lhs33), "lhs", "lhs",
                  33);
  sf_mex_addfield(*c7_info, c7_b_emlrt_marshallOut(
    "[ILXE]$matlabroot$/toolbox/eml/lib/matlab/eml/eml_isa_uint.m"), "context",
                  "context", 34);
  sf_mex_addfield(*c7_info, c7_b_emlrt_marshallOut("coder.internal.isaUint"),
                  "name", "name", 34);
  sf_mex_addfield(*c7_info, c7_b_emlrt_marshallOut("int8"), "dominantType",
                  "dominantType", 34);
  sf_mex_addfield(*c7_info, c7_b_emlrt_marshallOut(
    "[ILXE]$matlabroot$/toolbox/coder/coder/+coder/+internal/isaUint.p"),
                  "resolved", "resolved", 34);
  sf_mex_addfield(*c7_info, c7_c_emlrt_marshallOut(1410811370U), "fileTimeLo",
                  "fileTimeLo", 34);
  sf_mex_addfield(*c7_info, c7_c_emlrt_marshallOut(0U), "fileTimeHi",
                  "fileTimeHi", 34);
  sf_mex_addfield(*c7_info, c7_c_emlrt_marshallOut(0U), "mFileTimeLo",
                  "mFileTimeLo", 34);
  sf_mex_addfield(*c7_info, c7_c_emlrt_marshallOut(0U), "mFileTimeHi",
                  "mFileTimeHi", 34);
  sf_mex_assign(&c7_rhs34, sf_mex_createcellmatrix(0, 1), false);
  sf_mex_assign(&c7_lhs34, sf_mex_createcellmatrix(0, 1), false);
  sf_mex_addfield(*c7_info, sf_mex_duplicatearraysafe(&c7_rhs34), "rhs", "rhs",
                  34);
  sf_mex_addfield(*c7_info, sf_mex_duplicatearraysafe(&c7_lhs34), "lhs", "lhs",
                  34);
  sf_mex_addfield(*c7_info, c7_b_emlrt_marshallOut(
    "[ILXE]$matlabroot$/toolbox/eml/lib/matlab/ops/colon.m!integer_colon_length_nonnegd"),
                  "context", "context", 35);
  sf_mex_addfield(*c7_info, c7_b_emlrt_marshallOut("eml_unsigned_class"), "name",
                  "name", 35);
  sf_mex_addfield(*c7_info, c7_b_emlrt_marshallOut("char"), "dominantType",
                  "dominantType", 35);
  sf_mex_addfield(*c7_info, c7_b_emlrt_marshallOut(
    "[ILXE]$matlabroot$/toolbox/eml/lib/matlab/eml/eml_unsigned_class.m"),
                  "resolved", "resolved", 35);
  sf_mex_addfield(*c7_info, c7_c_emlrt_marshallOut(1375984288U), "fileTimeLo",
                  "fileTimeLo", 35);
  sf_mex_addfield(*c7_info, c7_c_emlrt_marshallOut(0U), "fileTimeHi",
                  "fileTimeHi", 35);
  sf_mex_addfield(*c7_info, c7_c_emlrt_marshallOut(0U), "mFileTimeLo",
                  "mFileTimeLo", 35);
  sf_mex_addfield(*c7_info, c7_c_emlrt_marshallOut(0U), "mFileTimeHi",
                  "mFileTimeHi", 35);
  sf_mex_assign(&c7_rhs35, sf_mex_createcellmatrix(0, 1), false);
  sf_mex_assign(&c7_lhs35, sf_mex_createcellmatrix(0, 1), false);
  sf_mex_addfield(*c7_info, sf_mex_duplicatearraysafe(&c7_rhs35), "rhs", "rhs",
                  35);
  sf_mex_addfield(*c7_info, sf_mex_duplicatearraysafe(&c7_lhs35), "lhs", "lhs",
                  35);
  sf_mex_addfield(*c7_info, c7_b_emlrt_marshallOut(
    "[ILXE]$matlabroot$/toolbox/eml/lib/matlab/eml/eml_unsigned_class.m"),
                  "context", "context", 36);
  sf_mex_addfield(*c7_info, c7_b_emlrt_marshallOut(
    "coder.internal.unsignedClass"), "name", "name", 36);
  sf_mex_addfield(*c7_info, c7_b_emlrt_marshallOut("char"), "dominantType",
                  "dominantType", 36);
  sf_mex_addfield(*c7_info, c7_b_emlrt_marshallOut(
    "[ILXE]$matlabroot$/toolbox/coder/coder/+coder/+internal/unsignedClass.p"),
                  "resolved", "resolved", 36);
  sf_mex_addfield(*c7_info, c7_c_emlrt_marshallOut(1410811370U), "fileTimeLo",
                  "fileTimeLo", 36);
  sf_mex_addfield(*c7_info, c7_c_emlrt_marshallOut(0U), "fileTimeHi",
                  "fileTimeHi", 36);
  sf_mex_addfield(*c7_info, c7_c_emlrt_marshallOut(0U), "mFileTimeLo",
                  "mFileTimeLo", 36);
  sf_mex_addfield(*c7_info, c7_c_emlrt_marshallOut(0U), "mFileTimeHi",
                  "mFileTimeHi", 36);
  sf_mex_assign(&c7_rhs36, sf_mex_createcellmatrix(0, 1), false);
  sf_mex_assign(&c7_lhs36, sf_mex_createcellmatrix(0, 1), false);
  sf_mex_addfield(*c7_info, sf_mex_duplicatearraysafe(&c7_rhs36), "rhs", "rhs",
                  36);
  sf_mex_addfield(*c7_info, sf_mex_duplicatearraysafe(&c7_lhs36), "lhs", "lhs",
                  36);
  sf_mex_addfield(*c7_info, c7_b_emlrt_marshallOut(
    "[ILXE]$matlabroot$/toolbox/coder/coder/+coder/+internal/unsignedClass.p"),
                  "context", "context", 37);
  sf_mex_addfield(*c7_info, c7_b_emlrt_marshallOut("eml_switch_helper"), "name",
                  "name", 37);
  sf_mex_addfield(*c7_info, c7_b_emlrt_marshallOut(""), "dominantType",
                  "dominantType", 37);
  sf_mex_addfield(*c7_info, c7_b_emlrt_marshallOut(
    "[ILXE]$matlabroot$/toolbox/eml/lib/matlab/eml/eml_switch_helper.m"),
                  "resolved", "resolved", 37);
  sf_mex_addfield(*c7_info, c7_c_emlrt_marshallOut(1393334458U), "fileTimeLo",
                  "fileTimeLo", 37);
  sf_mex_addfield(*c7_info, c7_c_emlrt_marshallOut(0U), "fileTimeHi",
                  "fileTimeHi", 37);
  sf_mex_addfield(*c7_info, c7_c_emlrt_marshallOut(0U), "mFileTimeLo",
                  "mFileTimeLo", 37);
  sf_mex_addfield(*c7_info, c7_c_emlrt_marshallOut(0U), "mFileTimeHi",
                  "mFileTimeHi", 37);
  sf_mex_assign(&c7_rhs37, sf_mex_createcellmatrix(0, 1), false);
  sf_mex_assign(&c7_lhs37, sf_mex_createcellmatrix(0, 1), false);
  sf_mex_addfield(*c7_info, sf_mex_duplicatearraysafe(&c7_rhs37), "rhs", "rhs",
                  37);
  sf_mex_addfield(*c7_info, sf_mex_duplicatearraysafe(&c7_lhs37), "lhs", "lhs",
                  37);
  sf_mex_addfield(*c7_info, c7_b_emlrt_marshallOut(
    "[ILXE]$matlabroot$/toolbox/eml/lib/matlab/ops/colon.m!integer_colon_length_nonnegd"),
                  "context", "context", 38);
  sf_mex_addfield(*c7_info, c7_b_emlrt_marshallOut("eml_index_class"), "name",
                  "name", 38);
  sf_mex_addfield(*c7_info, c7_b_emlrt_marshallOut(""), "dominantType",
                  "dominantType", 38);
  sf_mex_addfield(*c7_info, c7_b_emlrt_marshallOut(
    "[ILXE]$matlabroot$/toolbox/eml/lib/matlab/eml/eml_index_class.m"),
                  "resolved", "resolved", 38);
  sf_mex_addfield(*c7_info, c7_c_emlrt_marshallOut(1323174178U), "fileTimeLo",
                  "fileTimeLo", 38);
  sf_mex_addfield(*c7_info, c7_c_emlrt_marshallOut(0U), "fileTimeHi",
                  "fileTimeHi", 38);
  sf_mex_addfield(*c7_info, c7_c_emlrt_marshallOut(0U), "mFileTimeLo",
                  "mFileTimeLo", 38);
  sf_mex_addfield(*c7_info, c7_c_emlrt_marshallOut(0U), "mFileTimeHi",
                  "mFileTimeHi", 38);
  sf_mex_assign(&c7_rhs38, sf_mex_createcellmatrix(0, 1), false);
  sf_mex_assign(&c7_lhs38, sf_mex_createcellmatrix(0, 1), false);
  sf_mex_addfield(*c7_info, sf_mex_duplicatearraysafe(&c7_rhs38), "rhs", "rhs",
                  38);
  sf_mex_addfield(*c7_info, sf_mex_duplicatearraysafe(&c7_lhs38), "lhs", "lhs",
                  38);
  sf_mex_addfield(*c7_info, c7_b_emlrt_marshallOut(
    "[ILXE]$matlabroot$/toolbox/eml/lib/matlab/ops/colon.m!integer_colon_length_nonnegd"),
                  "context", "context", 39);
  sf_mex_addfield(*c7_info, c7_b_emlrt_marshallOut("intmax"), "name", "name", 39);
  sf_mex_addfield(*c7_info, c7_b_emlrt_marshallOut("char"), "dominantType",
                  "dominantType", 39);
  sf_mex_addfield(*c7_info, c7_b_emlrt_marshallOut(
    "[ILXE]$matlabroot$/toolbox/eml/lib/matlab/elmat/intmax.m"), "resolved",
                  "resolved", 39);
  sf_mex_addfield(*c7_info, c7_c_emlrt_marshallOut(1362265482U), "fileTimeLo",
                  "fileTimeLo", 39);
  sf_mex_addfield(*c7_info, c7_c_emlrt_marshallOut(0U), "fileTimeHi",
                  "fileTimeHi", 39);
  sf_mex_addfield(*c7_info, c7_c_emlrt_marshallOut(0U), "mFileTimeLo",
                  "mFileTimeLo", 39);
  sf_mex_addfield(*c7_info, c7_c_emlrt_marshallOut(0U), "mFileTimeHi",
                  "mFileTimeHi", 39);
  sf_mex_assign(&c7_rhs39, sf_mex_createcellmatrix(0, 1), false);
  sf_mex_assign(&c7_lhs39, sf_mex_createcellmatrix(0, 1), false);
  sf_mex_addfield(*c7_info, sf_mex_duplicatearraysafe(&c7_rhs39), "rhs", "rhs",
                  39);
  sf_mex_addfield(*c7_info, sf_mex_duplicatearraysafe(&c7_lhs39), "lhs", "lhs",
                  39);
  sf_mex_addfield(*c7_info, c7_b_emlrt_marshallOut(
    "[ILXE]$matlabroot$/toolbox/eml/lib/matlab/ops/colon.m!integer_colon_length_nonnegd"),
                  "context", "context", 40);
  sf_mex_addfield(*c7_info, c7_b_emlrt_marshallOut("eml_isa_uint"), "name",
                  "name", 40);
  sf_mex_addfield(*c7_info, c7_b_emlrt_marshallOut("int8"), "dominantType",
                  "dominantType", 40);
  sf_mex_addfield(*c7_info, c7_b_emlrt_marshallOut(
    "[ILXE]$matlabroot$/toolbox/eml/lib/matlab/eml/eml_isa_uint.m"), "resolved",
                  "resolved", 40);
  sf_mex_addfield(*c7_info, c7_c_emlrt_marshallOut(1375984288U), "fileTimeLo",
                  "fileTimeLo", 40);
  sf_mex_addfield(*c7_info, c7_c_emlrt_marshallOut(0U), "fileTimeHi",
                  "fileTimeHi", 40);
  sf_mex_addfield(*c7_info, c7_c_emlrt_marshallOut(0U), "mFileTimeLo",
                  "mFileTimeLo", 40);
  sf_mex_addfield(*c7_info, c7_c_emlrt_marshallOut(0U), "mFileTimeHi",
                  "mFileTimeHi", 40);
  sf_mex_assign(&c7_rhs40, sf_mex_createcellmatrix(0, 1), false);
  sf_mex_assign(&c7_lhs40, sf_mex_createcellmatrix(0, 1), false);
  sf_mex_addfield(*c7_info, sf_mex_duplicatearraysafe(&c7_rhs40), "rhs", "rhs",
                  40);
  sf_mex_addfield(*c7_info, sf_mex_duplicatearraysafe(&c7_lhs40), "lhs", "lhs",
                  40);
  sf_mex_addfield(*c7_info, c7_b_emlrt_marshallOut(
    "[ILXE]$matlabroot$/toolbox/eml/lib/matlab/ops/colon.m!integer_colon_length_nonnegd"),
                  "context", "context", 41);
  sf_mex_addfield(*c7_info, c7_b_emlrt_marshallOut("eml_index_plus"), "name",
                  "name", 41);
  sf_mex_addfield(*c7_info, c7_b_emlrt_marshallOut("double"), "dominantType",
                  "dominantType", 41);
  sf_mex_addfield(*c7_info, c7_b_emlrt_marshallOut(
    "[ILXE]$matlabroot$/toolbox/eml/lib/matlab/eml/eml_index_plus.m"),
                  "resolved", "resolved", 41);
  sf_mex_addfield(*c7_info, c7_c_emlrt_marshallOut(1372586016U), "fileTimeLo",
                  "fileTimeLo", 41);
  sf_mex_addfield(*c7_info, c7_c_emlrt_marshallOut(0U), "fileTimeHi",
                  "fileTimeHi", 41);
  sf_mex_addfield(*c7_info, c7_c_emlrt_marshallOut(0U), "mFileTimeLo",
                  "mFileTimeLo", 41);
  sf_mex_addfield(*c7_info, c7_c_emlrt_marshallOut(0U), "mFileTimeHi",
                  "mFileTimeHi", 41);
  sf_mex_assign(&c7_rhs41, sf_mex_createcellmatrix(0, 1), false);
  sf_mex_assign(&c7_lhs41, sf_mex_createcellmatrix(0, 1), false);
  sf_mex_addfield(*c7_info, sf_mex_duplicatearraysafe(&c7_rhs41), "rhs", "rhs",
                  41);
  sf_mex_addfield(*c7_info, sf_mex_duplicatearraysafe(&c7_lhs41), "lhs", "lhs",
                  41);
  sf_mex_addfield(*c7_info, c7_b_emlrt_marshallOut(
    "[ILXE]$matlabroot$/toolbox/eml/lib/matlab/eml/eml_index_plus.m"), "context",
                  "context", 42);
  sf_mex_addfield(*c7_info, c7_b_emlrt_marshallOut("coder.internal.indexPlus"),
                  "name", "name", 42);
  sf_mex_addfield(*c7_info, c7_b_emlrt_marshallOut("double"), "dominantType",
                  "dominantType", 42);
  sf_mex_addfield(*c7_info, c7_b_emlrt_marshallOut(
    "[ILXE]$matlabroot$/toolbox/shared/coder/coder/+coder/+internal/indexPlus.m"),
                  "resolved", "resolved", 42);
  sf_mex_addfield(*c7_info, c7_c_emlrt_marshallOut(1372586760U), "fileTimeLo",
                  "fileTimeLo", 42);
  sf_mex_addfield(*c7_info, c7_c_emlrt_marshallOut(0U), "fileTimeHi",
                  "fileTimeHi", 42);
  sf_mex_addfield(*c7_info, c7_c_emlrt_marshallOut(0U), "mFileTimeLo",
                  "mFileTimeLo", 42);
  sf_mex_addfield(*c7_info, c7_c_emlrt_marshallOut(0U), "mFileTimeHi",
                  "mFileTimeHi", 42);
  sf_mex_assign(&c7_rhs42, sf_mex_createcellmatrix(0, 1), false);
  sf_mex_assign(&c7_lhs42, sf_mex_createcellmatrix(0, 1), false);
  sf_mex_addfield(*c7_info, sf_mex_duplicatearraysafe(&c7_rhs42), "rhs", "rhs",
                  42);
  sf_mex_addfield(*c7_info, sf_mex_duplicatearraysafe(&c7_lhs42), "lhs", "lhs",
                  42);
  sf_mex_addfield(*c7_info, c7_b_emlrt_marshallOut(
    "[ILXE]$matlabroot$/toolbox/eml/lib/matlab/ops/colon.m!eml_signed_integer_colon"),
                  "context", "context", 43);
  sf_mex_addfield(*c7_info, c7_b_emlrt_marshallOut(
    "eml_int_forloop_overflow_check"), "name", "name", 43);
  sf_mex_addfield(*c7_info, c7_b_emlrt_marshallOut(""), "dominantType",
                  "dominantType", 43);
  sf_mex_addfield(*c7_info, c7_b_emlrt_marshallOut(
    "[ILXE]$matlabroot$/toolbox/eml/lib/matlab/eml/eml_int_forloop_overflow_check.m"),
                  "resolved", "resolved", 43);
  sf_mex_addfield(*c7_info, c7_c_emlrt_marshallOut(1397261022U), "fileTimeLo",
                  "fileTimeLo", 43);
  sf_mex_addfield(*c7_info, c7_c_emlrt_marshallOut(0U), "fileTimeHi",
                  "fileTimeHi", 43);
  sf_mex_addfield(*c7_info, c7_c_emlrt_marshallOut(0U), "mFileTimeLo",
                  "mFileTimeLo", 43);
  sf_mex_addfield(*c7_info, c7_c_emlrt_marshallOut(0U), "mFileTimeHi",
                  "mFileTimeHi", 43);
  sf_mex_assign(&c7_rhs43, sf_mex_createcellmatrix(0, 1), false);
  sf_mex_assign(&c7_lhs43, sf_mex_createcellmatrix(0, 1), false);
  sf_mex_addfield(*c7_info, sf_mex_duplicatearraysafe(&c7_rhs43), "rhs", "rhs",
                  43);
  sf_mex_addfield(*c7_info, sf_mex_duplicatearraysafe(&c7_lhs43), "lhs", "lhs",
                  43);
  sf_mex_addfield(*c7_info, c7_b_emlrt_marshallOut(
    "[ILXE]$matlabroot$/toolbox/eml/lib/matlab/eml/eml_int_forloop_overflow_check.m!eml_int_forloop_overflow_check_helper"),
                  "context", "context", 44);
  sf_mex_addfield(*c7_info, c7_b_emlrt_marshallOut("isfi"), "name", "name", 44);
  sf_mex_addfield(*c7_info, c7_b_emlrt_marshallOut("coder.internal.indexInt"),
                  "dominantType", "dominantType", 44);
  sf_mex_addfield(*c7_info, c7_b_emlrt_marshallOut(
    "[ILXE]$matlabroot$/toolbox/eml/lib/fixedpoint/isfi.m"), "resolved",
                  "resolved", 44);
  sf_mex_addfield(*c7_info, c7_c_emlrt_marshallOut(1346513958U), "fileTimeLo",
                  "fileTimeLo", 44);
  sf_mex_addfield(*c7_info, c7_c_emlrt_marshallOut(0U), "fileTimeHi",
                  "fileTimeHi", 44);
  sf_mex_addfield(*c7_info, c7_c_emlrt_marshallOut(0U), "mFileTimeLo",
                  "mFileTimeLo", 44);
  sf_mex_addfield(*c7_info, c7_c_emlrt_marshallOut(0U), "mFileTimeHi",
                  "mFileTimeHi", 44);
  sf_mex_assign(&c7_rhs44, sf_mex_createcellmatrix(0, 1), false);
  sf_mex_assign(&c7_lhs44, sf_mex_createcellmatrix(0, 1), false);
  sf_mex_addfield(*c7_info, sf_mex_duplicatearraysafe(&c7_rhs44), "rhs", "rhs",
                  44);
  sf_mex_addfield(*c7_info, sf_mex_duplicatearraysafe(&c7_lhs44), "lhs", "lhs",
                  44);
  sf_mex_addfield(*c7_info, c7_b_emlrt_marshallOut(
    "[ILXE]$matlabroot$/toolbox/eml/lib/fixedpoint/isfi.m"), "context",
                  "context", 45);
  sf_mex_addfield(*c7_info, c7_b_emlrt_marshallOut("isnumerictype"), "name",
                  "name", 45);
  sf_mex_addfield(*c7_info, c7_b_emlrt_marshallOut("char"), "dominantType",
                  "dominantType", 45);
  sf_mex_addfield(*c7_info, c7_b_emlrt_marshallOut(
    "[ILXE]$matlabroot$/toolbox/eml/lib/fixedpoint/isnumerictype.m"), "resolved",
                  "resolved", 45);
  sf_mex_addfield(*c7_info, c7_c_emlrt_marshallOut(1398879198U), "fileTimeLo",
                  "fileTimeLo", 45);
  sf_mex_addfield(*c7_info, c7_c_emlrt_marshallOut(0U), "fileTimeHi",
                  "fileTimeHi", 45);
  sf_mex_addfield(*c7_info, c7_c_emlrt_marshallOut(0U), "mFileTimeLo",
                  "mFileTimeLo", 45);
  sf_mex_addfield(*c7_info, c7_c_emlrt_marshallOut(0U), "mFileTimeHi",
                  "mFileTimeHi", 45);
  sf_mex_assign(&c7_rhs45, sf_mex_createcellmatrix(0, 1), false);
  sf_mex_assign(&c7_lhs45, sf_mex_createcellmatrix(0, 1), false);
  sf_mex_addfield(*c7_info, sf_mex_duplicatearraysafe(&c7_rhs45), "rhs", "rhs",
                  45);
  sf_mex_addfield(*c7_info, sf_mex_duplicatearraysafe(&c7_lhs45), "lhs", "lhs",
                  45);
  sf_mex_addfield(*c7_info, c7_b_emlrt_marshallOut(
    "[ILXE]$matlabroot$/toolbox/eml/lib/matlab/eml/eml_int_forloop_overflow_check.m!eml_int_forloop_overflow_check_helper"),
                  "context", "context", 46);
  sf_mex_addfield(*c7_info, c7_b_emlrt_marshallOut("intmax"), "name", "name", 46);
  sf_mex_addfield(*c7_info, c7_b_emlrt_marshallOut("char"), "dominantType",
                  "dominantType", 46);
  sf_mex_addfield(*c7_info, c7_b_emlrt_marshallOut(
    "[ILXE]$matlabroot$/toolbox/eml/lib/matlab/elmat/intmax.m"), "resolved",
                  "resolved", 46);
  sf_mex_addfield(*c7_info, c7_c_emlrt_marshallOut(1362265482U), "fileTimeLo",
                  "fileTimeLo", 46);
  sf_mex_addfield(*c7_info, c7_c_emlrt_marshallOut(0U), "fileTimeHi",
                  "fileTimeHi", 46);
  sf_mex_addfield(*c7_info, c7_c_emlrt_marshallOut(0U), "mFileTimeLo",
                  "mFileTimeLo", 46);
  sf_mex_addfield(*c7_info, c7_c_emlrt_marshallOut(0U), "mFileTimeHi",
                  "mFileTimeHi", 46);
  sf_mex_assign(&c7_rhs46, sf_mex_createcellmatrix(0, 1), false);
  sf_mex_assign(&c7_lhs46, sf_mex_createcellmatrix(0, 1), false);
  sf_mex_addfield(*c7_info, sf_mex_duplicatearraysafe(&c7_rhs46), "rhs", "rhs",
                  46);
  sf_mex_addfield(*c7_info, sf_mex_duplicatearraysafe(&c7_lhs46), "lhs", "lhs",
                  46);
  sf_mex_addfield(*c7_info, c7_b_emlrt_marshallOut(
    "[ILXE]$matlabroot$/toolbox/eml/lib/matlab/eml/eml_int_forloop_overflow_check.m!eml_int_forloop_overflow_check_helper"),
                  "context", "context", 47);
  sf_mex_addfield(*c7_info, c7_b_emlrt_marshallOut("intmin"), "name", "name", 47);
  sf_mex_addfield(*c7_info, c7_b_emlrt_marshallOut("char"), "dominantType",
                  "dominantType", 47);
  sf_mex_addfield(*c7_info, c7_b_emlrt_marshallOut(
    "[ILXE]$matlabroot$/toolbox/eml/lib/matlab/elmat/intmin.m"), "resolved",
                  "resolved", 47);
  sf_mex_addfield(*c7_info, c7_c_emlrt_marshallOut(1362265482U), "fileTimeLo",
                  "fileTimeLo", 47);
  sf_mex_addfield(*c7_info, c7_c_emlrt_marshallOut(0U), "fileTimeHi",
                  "fileTimeHi", 47);
  sf_mex_addfield(*c7_info, c7_c_emlrt_marshallOut(0U), "mFileTimeLo",
                  "mFileTimeLo", 47);
  sf_mex_addfield(*c7_info, c7_c_emlrt_marshallOut(0U), "mFileTimeHi",
                  "mFileTimeHi", 47);
  sf_mex_assign(&c7_rhs47, sf_mex_createcellmatrix(0, 1), false);
  sf_mex_assign(&c7_lhs47, sf_mex_createcellmatrix(0, 1), false);
  sf_mex_addfield(*c7_info, sf_mex_duplicatearraysafe(&c7_rhs47), "rhs", "rhs",
                  47);
  sf_mex_addfield(*c7_info, sf_mex_duplicatearraysafe(&c7_lhs47), "lhs", "lhs",
                  47);
  sf_mex_addfield(*c7_info, c7_b_emlrt_marshallOut(
    "[ILXE]$matlabroot$/toolbox/eml/lib/matlab/strfun/isstrprop.m!apply_property_predicate"),
                  "context", "context", 48);
  sf_mex_addfield(*c7_info, c7_b_emlrt_marshallOut("char"), "name", "name", 48);
  sf_mex_addfield(*c7_info, c7_b_emlrt_marshallOut("int8"), "dominantType",
                  "dominantType", 48);
  sf_mex_addfield(*c7_info, c7_b_emlrt_marshallOut(
    "[ILXE]$matlabroot$/toolbox/eml/lib/matlab/strfun/char.m"), "resolved",
                  "resolved", 48);
  sf_mex_addfield(*c7_info, c7_c_emlrt_marshallOut(1319733568U), "fileTimeLo",
                  "fileTimeLo", 48);
  sf_mex_addfield(*c7_info, c7_c_emlrt_marshallOut(0U), "fileTimeHi",
                  "fileTimeHi", 48);
  sf_mex_addfield(*c7_info, c7_c_emlrt_marshallOut(0U), "mFileTimeLo",
                  "mFileTimeLo", 48);
  sf_mex_addfield(*c7_info, c7_c_emlrt_marshallOut(0U), "mFileTimeHi",
                  "mFileTimeHi", 48);
  sf_mex_assign(&c7_rhs48, sf_mex_createcellmatrix(0, 1), false);
  sf_mex_assign(&c7_lhs48, sf_mex_createcellmatrix(0, 1), false);
  sf_mex_addfield(*c7_info, sf_mex_duplicatearraysafe(&c7_rhs48), "rhs", "rhs",
                  48);
  sf_mex_addfield(*c7_info, sf_mex_duplicatearraysafe(&c7_lhs48), "lhs", "lhs",
                  48);
  sf_mex_addfield(*c7_info, c7_b_emlrt_marshallOut(
    "[ILXE]$matlabroot$/toolbox/eml/lib/matlab/datatypes/typecast.m!bytes_per_element"),
                  "context", "context", 49);
  sf_mex_addfield(*c7_info, c7_b_emlrt_marshallOut("eml_switch_helper"), "name",
                  "name", 49);
  sf_mex_addfield(*c7_info, c7_b_emlrt_marshallOut(""), "dominantType",
                  "dominantType", 49);
  sf_mex_addfield(*c7_info, c7_b_emlrt_marshallOut(
    "[ILXE]$matlabroot$/toolbox/eml/lib/matlab/eml/eml_switch_helper.m"),
                  "resolved", "resolved", 49);
  sf_mex_addfield(*c7_info, c7_c_emlrt_marshallOut(1393334458U), "fileTimeLo",
                  "fileTimeLo", 49);
  sf_mex_addfield(*c7_info, c7_c_emlrt_marshallOut(0U), "fileTimeHi",
                  "fileTimeHi", 49);
  sf_mex_addfield(*c7_info, c7_c_emlrt_marshallOut(0U), "mFileTimeLo",
                  "mFileTimeLo", 49);
  sf_mex_addfield(*c7_info, c7_c_emlrt_marshallOut(0U), "mFileTimeHi",
                  "mFileTimeHi", 49);
  sf_mex_assign(&c7_rhs49, sf_mex_createcellmatrix(0, 1), false);
  sf_mex_assign(&c7_lhs49, sf_mex_createcellmatrix(0, 1), false);
  sf_mex_addfield(*c7_info, sf_mex_duplicatearraysafe(&c7_rhs49), "rhs", "rhs",
                  49);
  sf_mex_addfield(*c7_info, sf_mex_duplicatearraysafe(&c7_lhs49), "lhs", "lhs",
                  49);
  sf_mex_addfield(*c7_info, c7_b_emlrt_marshallOut(
    "[ILXE]$matlabroot$/toolbox/eml/lib/matlab/datatypes/typecast.m!bytes_per_element"),
                  "context", "context", 50);
  sf_mex_addfield(*c7_info, c7_b_emlrt_marshallOut("eml_int_nbits"), "name",
                  "name", 50);
  sf_mex_addfield(*c7_info, c7_b_emlrt_marshallOut("char"), "dominantType",
                  "dominantType", 50);
  sf_mex_addfield(*c7_info, c7_b_emlrt_marshallOut(
    "[ILXE]$matlabroot$/toolbox/eml/lib/matlab/eml/eml_int_nbits.m"), "resolved",
                  "resolved", 50);
  sf_mex_addfield(*c7_info, c7_c_emlrt_marshallOut(1323174178U), "fileTimeLo",
                  "fileTimeLo", 50);
  sf_mex_addfield(*c7_info, c7_c_emlrt_marshallOut(0U), "fileTimeHi",
                  "fileTimeHi", 50);
  sf_mex_addfield(*c7_info, c7_c_emlrt_marshallOut(0U), "mFileTimeLo",
                  "mFileTimeLo", 50);
  sf_mex_addfield(*c7_info, c7_c_emlrt_marshallOut(0U), "mFileTimeHi",
                  "mFileTimeHi", 50);
  sf_mex_assign(&c7_rhs50, sf_mex_createcellmatrix(0, 1), false);
  sf_mex_assign(&c7_lhs50, sf_mex_createcellmatrix(0, 1), false);
  sf_mex_addfield(*c7_info, sf_mex_duplicatearraysafe(&c7_rhs50), "rhs", "rhs",
                  50);
  sf_mex_addfield(*c7_info, sf_mex_duplicatearraysafe(&c7_lhs50), "lhs", "lhs",
                  50);
  sf_mex_addfield(*c7_info, c7_b_emlrt_marshallOut(
    "[ILXE]$matlabroot$/toolbox/eml/lib/matlab/eml/eml_int_nbits.m"), "context",
                  "context", 51);
  sf_mex_addfield(*c7_info, c7_b_emlrt_marshallOut("eml_switch_helper"), "name",
                  "name", 51);
  sf_mex_addfield(*c7_info, c7_b_emlrt_marshallOut(""), "dominantType",
                  "dominantType", 51);
  sf_mex_addfield(*c7_info, c7_b_emlrt_marshallOut(
    "[ILXE]$matlabroot$/toolbox/eml/lib/matlab/eml/eml_switch_helper.m"),
                  "resolved", "resolved", 51);
  sf_mex_addfield(*c7_info, c7_c_emlrt_marshallOut(1393334458U), "fileTimeLo",
                  "fileTimeLo", 51);
  sf_mex_addfield(*c7_info, c7_c_emlrt_marshallOut(0U), "fileTimeHi",
                  "fileTimeHi", 51);
  sf_mex_addfield(*c7_info, c7_c_emlrt_marshallOut(0U), "mFileTimeLo",
                  "mFileTimeLo", 51);
  sf_mex_addfield(*c7_info, c7_c_emlrt_marshallOut(0U), "mFileTimeHi",
                  "mFileTimeHi", 51);
  sf_mex_assign(&c7_rhs51, sf_mex_createcellmatrix(0, 1), false);
  sf_mex_assign(&c7_lhs51, sf_mex_createcellmatrix(0, 1), false);
  sf_mex_addfield(*c7_info, sf_mex_duplicatearraysafe(&c7_rhs51), "rhs", "rhs",
                  51);
  sf_mex_addfield(*c7_info, sf_mex_duplicatearraysafe(&c7_lhs51), "lhs", "lhs",
                  51);
  sf_mex_addfield(*c7_info, c7_b_emlrt_marshallOut(
    "[ILXE]$matlabroot$/toolbox/eml/lib/matlab/datatypes/typecast.m!bytes_per_element"),
                  "context", "context", 52);
  sf_mex_addfield(*c7_info, c7_b_emlrt_marshallOut("eml_index_rdivide"), "name",
                  "name", 52);
  sf_mex_addfield(*c7_info, c7_b_emlrt_marshallOut("uint8"), "dominantType",
                  "dominantType", 52);
  sf_mex_addfield(*c7_info, c7_b_emlrt_marshallOut(
    "[ILXE]$matlabroot$/toolbox/eml/lib/matlab/eml/eml_index_rdivide.m"),
                  "resolved", "resolved", 52);
  sf_mex_addfield(*c7_info, c7_c_emlrt_marshallOut(1372586016U), "fileTimeLo",
                  "fileTimeLo", 52);
  sf_mex_addfield(*c7_info, c7_c_emlrt_marshallOut(0U), "fileTimeHi",
                  "fileTimeHi", 52);
  sf_mex_addfield(*c7_info, c7_c_emlrt_marshallOut(0U), "mFileTimeLo",
                  "mFileTimeLo", 52);
  sf_mex_addfield(*c7_info, c7_c_emlrt_marshallOut(0U), "mFileTimeHi",
                  "mFileTimeHi", 52);
  sf_mex_assign(&c7_rhs52, sf_mex_createcellmatrix(0, 1), false);
  sf_mex_assign(&c7_lhs52, sf_mex_createcellmatrix(0, 1), false);
  sf_mex_addfield(*c7_info, sf_mex_duplicatearraysafe(&c7_rhs52), "rhs", "rhs",
                  52);
  sf_mex_addfield(*c7_info, sf_mex_duplicatearraysafe(&c7_lhs52), "lhs", "lhs",
                  52);
  sf_mex_addfield(*c7_info, c7_b_emlrt_marshallOut(
    "[ILXE]$matlabroot$/toolbox/eml/lib/matlab/eml/eml_index_rdivide.m"),
                  "context", "context", 53);
  sf_mex_addfield(*c7_info, c7_b_emlrt_marshallOut("coder.internal.indexDivide"),
                  "name", "name", 53);
  sf_mex_addfield(*c7_info, c7_b_emlrt_marshallOut("uint8"), "dominantType",
                  "dominantType", 53);
  sf_mex_addfield(*c7_info, c7_b_emlrt_marshallOut(
    "[ILXE]$matlabroot$/toolbox/shared/coder/coder/+coder/+internal/indexDivide.m"),
                  "resolved", "resolved", 53);
  sf_mex_addfield(*c7_info, c7_c_emlrt_marshallOut(1372586760U), "fileTimeLo",
                  "fileTimeLo", 53);
  sf_mex_addfield(*c7_info, c7_c_emlrt_marshallOut(0U), "fileTimeHi",
                  "fileTimeHi", 53);
  sf_mex_addfield(*c7_info, c7_c_emlrt_marshallOut(0U), "mFileTimeLo",
                  "mFileTimeLo", 53);
  sf_mex_addfield(*c7_info, c7_c_emlrt_marshallOut(0U), "mFileTimeHi",
                  "mFileTimeHi", 53);
  sf_mex_assign(&c7_rhs53, sf_mex_createcellmatrix(0, 1), false);
  sf_mex_assign(&c7_lhs53, sf_mex_createcellmatrix(0, 1), false);
  sf_mex_addfield(*c7_info, sf_mex_duplicatearraysafe(&c7_rhs53), "rhs", "rhs",
                  53);
  sf_mex_addfield(*c7_info, sf_mex_duplicatearraysafe(&c7_lhs53), "lhs", "lhs",
                  53);
  sf_mex_addfield(*c7_info, c7_b_emlrt_marshallOut(
    "[ILXE]$matlabroot$/toolbox/eml/lib/matlab/datatypes/typecast.m"), "context",
                  "context", 54);
  sf_mex_addfield(*c7_info, c7_b_emlrt_marshallOut("eml_index_times"), "name",
                  "name", 54);
  sf_mex_addfield(*c7_info, c7_b_emlrt_marshallOut("double"), "dominantType",
                  "dominantType", 54);
  sf_mex_addfield(*c7_info, c7_b_emlrt_marshallOut(
    "[ILXE]$matlabroot$/toolbox/eml/lib/matlab/eml/eml_index_times.m"),
                  "resolved", "resolved", 54);
  sf_mex_addfield(*c7_info, c7_c_emlrt_marshallOut(1372586016U), "fileTimeLo",
                  "fileTimeLo", 54);
  sf_mex_addfield(*c7_info, c7_c_emlrt_marshallOut(0U), "fileTimeHi",
                  "fileTimeHi", 54);
  sf_mex_addfield(*c7_info, c7_c_emlrt_marshallOut(0U), "mFileTimeLo",
                  "mFileTimeLo", 54);
  sf_mex_addfield(*c7_info, c7_c_emlrt_marshallOut(0U), "mFileTimeHi",
                  "mFileTimeHi", 54);
  sf_mex_assign(&c7_rhs54, sf_mex_createcellmatrix(0, 1), false);
  sf_mex_assign(&c7_lhs54, sf_mex_createcellmatrix(0, 1), false);
  sf_mex_addfield(*c7_info, sf_mex_duplicatearraysafe(&c7_rhs54), "rhs", "rhs",
                  54);
  sf_mex_addfield(*c7_info, sf_mex_duplicatearraysafe(&c7_lhs54), "lhs", "lhs",
                  54);
  sf_mex_addfield(*c7_info, c7_b_emlrt_marshallOut(
    "[ILXE]$matlabroot$/toolbox/eml/lib/matlab/eml/eml_index_times.m"),
                  "context", "context", 55);
  sf_mex_addfield(*c7_info, c7_b_emlrt_marshallOut("coder.internal.indexTimes"),
                  "name", "name", 55);
  sf_mex_addfield(*c7_info, c7_b_emlrt_marshallOut("double"), "dominantType",
                  "dominantType", 55);
  sf_mex_addfield(*c7_info, c7_b_emlrt_marshallOut(
    "[ILXE]$matlabroot$/toolbox/shared/coder/coder/+coder/+internal/indexTimes.m"),
                  "resolved", "resolved", 55);
  sf_mex_addfield(*c7_info, c7_c_emlrt_marshallOut(1372586760U), "fileTimeLo",
                  "fileTimeLo", 55);
  sf_mex_addfield(*c7_info, c7_c_emlrt_marshallOut(0U), "fileTimeHi",
                  "fileTimeHi", 55);
  sf_mex_addfield(*c7_info, c7_c_emlrt_marshallOut(0U), "mFileTimeLo",
                  "mFileTimeLo", 55);
  sf_mex_addfield(*c7_info, c7_c_emlrt_marshallOut(0U), "mFileTimeHi",
                  "mFileTimeHi", 55);
  sf_mex_assign(&c7_rhs55, sf_mex_createcellmatrix(0, 1), false);
  sf_mex_assign(&c7_lhs55, sf_mex_createcellmatrix(0, 1), false);
  sf_mex_addfield(*c7_info, sf_mex_duplicatearraysafe(&c7_rhs55), "rhs", "rhs",
                  55);
  sf_mex_addfield(*c7_info, sf_mex_duplicatearraysafe(&c7_lhs55), "lhs", "lhs",
                  55);
  sf_mex_addfield(*c7_info, c7_b_emlrt_marshallOut(
    "[ILXE]$matlabroot$/toolbox/eml/lib/matlab/datatypes/typecast.m"), "context",
                  "context", 56);
  sf_mex_addfield(*c7_info, c7_b_emlrt_marshallOut("eml_index_rdivide"), "name",
                  "name", 56);
  sf_mex_addfield(*c7_info, c7_b_emlrt_marshallOut("coder.internal.indexInt"),
                  "dominantType", "dominantType", 56);
  sf_mex_addfield(*c7_info, c7_b_emlrt_marshallOut(
    "[ILXE]$matlabroot$/toolbox/eml/lib/matlab/eml/eml_index_rdivide.m"),
                  "resolved", "resolved", 56);
  sf_mex_addfield(*c7_info, c7_c_emlrt_marshallOut(1372586016U), "fileTimeLo",
                  "fileTimeLo", 56);
  sf_mex_addfield(*c7_info, c7_c_emlrt_marshallOut(0U), "fileTimeHi",
                  "fileTimeHi", 56);
  sf_mex_addfield(*c7_info, c7_c_emlrt_marshallOut(0U), "mFileTimeLo",
                  "mFileTimeLo", 56);
  sf_mex_addfield(*c7_info, c7_c_emlrt_marshallOut(0U), "mFileTimeHi",
                  "mFileTimeHi", 56);
  sf_mex_assign(&c7_rhs56, sf_mex_createcellmatrix(0, 1), false);
  sf_mex_assign(&c7_lhs56, sf_mex_createcellmatrix(0, 1), false);
  sf_mex_addfield(*c7_info, sf_mex_duplicatearraysafe(&c7_rhs56), "rhs", "rhs",
                  56);
  sf_mex_addfield(*c7_info, sf_mex_duplicatearraysafe(&c7_lhs56), "lhs", "lhs",
                  56);
  sf_mex_addfield(*c7_info, c7_b_emlrt_marshallOut(
    "[ILXE]$matlabroot$/toolbox/eml/lib/matlab/eml/eml_index_rdivide.m"),
                  "context", "context", 57);
  sf_mex_addfield(*c7_info, c7_b_emlrt_marshallOut("coder.internal.indexDivide"),
                  "name", "name", 57);
  sf_mex_addfield(*c7_info, c7_b_emlrt_marshallOut("coder.internal.indexInt"),
                  "dominantType", "dominantType", 57);
  sf_mex_addfield(*c7_info, c7_b_emlrt_marshallOut(
    "[ILXE]$matlabroot$/toolbox/shared/coder/coder/+coder/+internal/indexDivide.m"),
                  "resolved", "resolved", 57);
  sf_mex_addfield(*c7_info, c7_c_emlrt_marshallOut(1372586760U), "fileTimeLo",
                  "fileTimeLo", 57);
  sf_mex_addfield(*c7_info, c7_c_emlrt_marshallOut(0U), "fileTimeHi",
                  "fileTimeHi", 57);
  sf_mex_addfield(*c7_info, c7_c_emlrt_marshallOut(0U), "mFileTimeLo",
                  "mFileTimeLo", 57);
  sf_mex_addfield(*c7_info, c7_c_emlrt_marshallOut(0U), "mFileTimeHi",
                  "mFileTimeHi", 57);
  sf_mex_assign(&c7_rhs57, sf_mex_createcellmatrix(0, 1), false);
  sf_mex_assign(&c7_lhs57, sf_mex_createcellmatrix(0, 1), false);
  sf_mex_addfield(*c7_info, sf_mex_duplicatearraysafe(&c7_rhs57), "rhs", "rhs",
                  57);
  sf_mex_addfield(*c7_info, sf_mex_duplicatearraysafe(&c7_lhs57), "lhs", "lhs",
                  57);
  sf_mex_addfield(*c7_info, c7_b_emlrt_marshallOut(
    "[ILXE]$matlabroot$/toolbox/eml/lib/matlab/datatypes/typecast.m"), "context",
                  "context", 58);
  sf_mex_addfield(*c7_info, c7_b_emlrt_marshallOut("eml_index_times"), "name",
                  "name", 58);
  sf_mex_addfield(*c7_info, c7_b_emlrt_marshallOut("coder.internal.indexInt"),
                  "dominantType", "dominantType", 58);
  sf_mex_addfield(*c7_info, c7_b_emlrt_marshallOut(
    "[ILXE]$matlabroot$/toolbox/eml/lib/matlab/eml/eml_index_times.m"),
                  "resolved", "resolved", 58);
  sf_mex_addfield(*c7_info, c7_c_emlrt_marshallOut(1372586016U), "fileTimeLo",
                  "fileTimeLo", 58);
  sf_mex_addfield(*c7_info, c7_c_emlrt_marshallOut(0U), "fileTimeHi",
                  "fileTimeHi", 58);
  sf_mex_addfield(*c7_info, c7_c_emlrt_marshallOut(0U), "mFileTimeLo",
                  "mFileTimeLo", 58);
  sf_mex_addfield(*c7_info, c7_c_emlrt_marshallOut(0U), "mFileTimeHi",
                  "mFileTimeHi", 58);
  sf_mex_assign(&c7_rhs58, sf_mex_createcellmatrix(0, 1), false);
  sf_mex_assign(&c7_lhs58, sf_mex_createcellmatrix(0, 1), false);
  sf_mex_addfield(*c7_info, sf_mex_duplicatearraysafe(&c7_rhs58), "rhs", "rhs",
                  58);
  sf_mex_addfield(*c7_info, sf_mex_duplicatearraysafe(&c7_lhs58), "lhs", "lhs",
                  58);
  sf_mex_addfield(*c7_info, c7_b_emlrt_marshallOut(
    "[ILXE]$matlabroot$/toolbox/eml/lib/matlab/eml/eml_index_times.m"),
                  "context", "context", 59);
  sf_mex_addfield(*c7_info, c7_b_emlrt_marshallOut("coder.internal.indexTimes"),
                  "name", "name", 59);
  sf_mex_addfield(*c7_info, c7_b_emlrt_marshallOut("coder.internal.indexInt"),
                  "dominantType", "dominantType", 59);
  sf_mex_addfield(*c7_info, c7_b_emlrt_marshallOut(
    "[ILXE]$matlabroot$/toolbox/shared/coder/coder/+coder/+internal/indexTimes.m"),
                  "resolved", "resolved", 59);
  sf_mex_addfield(*c7_info, c7_c_emlrt_marshallOut(1372586760U), "fileTimeLo",
                  "fileTimeLo", 59);
  sf_mex_addfield(*c7_info, c7_c_emlrt_marshallOut(0U), "fileTimeHi",
                  "fileTimeHi", 59);
  sf_mex_addfield(*c7_info, c7_c_emlrt_marshallOut(0U), "mFileTimeLo",
                  "mFileTimeLo", 59);
  sf_mex_addfield(*c7_info, c7_c_emlrt_marshallOut(0U), "mFileTimeHi",
                  "mFileTimeHi", 59);
  sf_mex_assign(&c7_rhs59, sf_mex_createcellmatrix(0, 1), false);
  sf_mex_assign(&c7_lhs59, sf_mex_createcellmatrix(0, 1), false);
  sf_mex_addfield(*c7_info, sf_mex_duplicatearraysafe(&c7_rhs59), "rhs", "rhs",
                  59);
  sf_mex_addfield(*c7_info, sf_mex_duplicatearraysafe(&c7_lhs59), "lhs", "lhs",
                  59);
  sf_mex_addfield(*c7_info, c7_b_emlrt_marshallOut(
    "[ILXE]$matlabroot$/toolbox/eml/lib/matlab/datatypes/typecast.m"), "context",
                  "context", 60);
  sf_mex_addfield(*c7_info, c7_b_emlrt_marshallOut("coder.internal.scalarEg"),
                  "name", "name", 60);
  sf_mex_addfield(*c7_info, c7_b_emlrt_marshallOut("int8"), "dominantType",
                  "dominantType", 60);
  sf_mex_addfield(*c7_info, c7_b_emlrt_marshallOut(
    "[ILXE]$matlabroot$/toolbox/coder/coder/+coder/+internal/scalarEg.p"),
                  "resolved", "resolved", 60);
  sf_mex_addfield(*c7_info, c7_c_emlrt_marshallOut(1410811370U), "fileTimeLo",
                  "fileTimeLo", 60);
  sf_mex_addfield(*c7_info, c7_c_emlrt_marshallOut(0U), "fileTimeHi",
                  "fileTimeHi", 60);
  sf_mex_addfield(*c7_info, c7_c_emlrt_marshallOut(0U), "mFileTimeLo",
                  "mFileTimeLo", 60);
  sf_mex_addfield(*c7_info, c7_c_emlrt_marshallOut(0U), "mFileTimeHi",
                  "mFileTimeHi", 60);
  sf_mex_assign(&c7_rhs60, sf_mex_createcellmatrix(0, 1), false);
  sf_mex_assign(&c7_lhs60, sf_mex_createcellmatrix(0, 1), false);
  sf_mex_addfield(*c7_info, sf_mex_duplicatearraysafe(&c7_rhs60), "rhs", "rhs",
                  60);
  sf_mex_addfield(*c7_info, sf_mex_duplicatearraysafe(&c7_lhs60), "lhs", "lhs",
                  60);
  sf_mex_addfield(*c7_info, c7_b_emlrt_marshallOut(
    "[ILXE]$matlabroot$/toolbox/eml/lib/matlab/datatypes/typecast.m"), "context",
                  "context", 61);
  sf_mex_addfield(*c7_info, c7_b_emlrt_marshallOut("coder.internal.scalarEg"),
                  "name", "name", 61);
  sf_mex_addfield(*c7_info, c7_b_emlrt_marshallOut("int16"), "dominantType",
                  "dominantType", 61);
  sf_mex_addfield(*c7_info, c7_b_emlrt_marshallOut(
    "[ILXE]$matlabroot$/toolbox/coder/coder/+coder/+internal/scalarEg.p"),
                  "resolved", "resolved", 61);
  sf_mex_addfield(*c7_info, c7_c_emlrt_marshallOut(1410811370U), "fileTimeLo",
                  "fileTimeLo", 61);
  sf_mex_addfield(*c7_info, c7_c_emlrt_marshallOut(0U), "fileTimeHi",
                  "fileTimeHi", 61);
  sf_mex_addfield(*c7_info, c7_c_emlrt_marshallOut(0U), "mFileTimeLo",
                  "mFileTimeLo", 61);
  sf_mex_addfield(*c7_info, c7_c_emlrt_marshallOut(0U), "mFileTimeHi",
                  "mFileTimeHi", 61);
  sf_mex_assign(&c7_rhs61, sf_mex_createcellmatrix(0, 1), false);
  sf_mex_assign(&c7_lhs61, sf_mex_createcellmatrix(0, 1), false);
  sf_mex_addfield(*c7_info, sf_mex_duplicatearraysafe(&c7_rhs61), "rhs", "rhs",
                  61);
  sf_mex_addfield(*c7_info, sf_mex_duplicatearraysafe(&c7_lhs61), "lhs", "lhs",
                  61);
  sf_mex_destroy(&c7_rhs0);
  sf_mex_destroy(&c7_lhs0);
  sf_mex_destroy(&c7_rhs1);
  sf_mex_destroy(&c7_lhs1);
  sf_mex_destroy(&c7_rhs2);
  sf_mex_destroy(&c7_lhs2);
  sf_mex_destroy(&c7_rhs3);
  sf_mex_destroy(&c7_lhs3);
  sf_mex_destroy(&c7_rhs4);
  sf_mex_destroy(&c7_lhs4);
  sf_mex_destroy(&c7_rhs5);
  sf_mex_destroy(&c7_lhs5);
  sf_mex_destroy(&c7_rhs6);
  sf_mex_destroy(&c7_lhs6);
  sf_mex_destroy(&c7_rhs7);
  sf_mex_destroy(&c7_lhs7);
  sf_mex_destroy(&c7_rhs8);
  sf_mex_destroy(&c7_lhs8);
  sf_mex_destroy(&c7_rhs9);
  sf_mex_destroy(&c7_lhs9);
  sf_mex_destroy(&c7_rhs10);
  sf_mex_destroy(&c7_lhs10);
  sf_mex_destroy(&c7_rhs11);
  sf_mex_destroy(&c7_lhs11);
  sf_mex_destroy(&c7_rhs12);
  sf_mex_destroy(&c7_lhs12);
  sf_mex_destroy(&c7_rhs13);
  sf_mex_destroy(&c7_lhs13);
  sf_mex_destroy(&c7_rhs14);
  sf_mex_destroy(&c7_lhs14);
  sf_mex_destroy(&c7_rhs15);
  sf_mex_destroy(&c7_lhs15);
  sf_mex_destroy(&c7_rhs16);
  sf_mex_destroy(&c7_lhs16);
  sf_mex_destroy(&c7_rhs17);
  sf_mex_destroy(&c7_lhs17);
  sf_mex_destroy(&c7_rhs18);
  sf_mex_destroy(&c7_lhs18);
  sf_mex_destroy(&c7_rhs19);
  sf_mex_destroy(&c7_lhs19);
  sf_mex_destroy(&c7_rhs20);
  sf_mex_destroy(&c7_lhs20);
  sf_mex_destroy(&c7_rhs21);
  sf_mex_destroy(&c7_lhs21);
  sf_mex_destroy(&c7_rhs22);
  sf_mex_destroy(&c7_lhs22);
  sf_mex_destroy(&c7_rhs23);
  sf_mex_destroy(&c7_lhs23);
  sf_mex_destroy(&c7_rhs24);
  sf_mex_destroy(&c7_lhs24);
  sf_mex_destroy(&c7_rhs25);
  sf_mex_destroy(&c7_lhs25);
  sf_mex_destroy(&c7_rhs26);
  sf_mex_destroy(&c7_lhs26);
  sf_mex_destroy(&c7_rhs27);
  sf_mex_destroy(&c7_lhs27);
  sf_mex_destroy(&c7_rhs28);
  sf_mex_destroy(&c7_lhs28);
  sf_mex_destroy(&c7_rhs29);
  sf_mex_destroy(&c7_lhs29);
  sf_mex_destroy(&c7_rhs30);
  sf_mex_destroy(&c7_lhs30);
  sf_mex_destroy(&c7_rhs31);
  sf_mex_destroy(&c7_lhs31);
  sf_mex_destroy(&c7_rhs32);
  sf_mex_destroy(&c7_lhs32);
  sf_mex_destroy(&c7_rhs33);
  sf_mex_destroy(&c7_lhs33);
  sf_mex_destroy(&c7_rhs34);
  sf_mex_destroy(&c7_lhs34);
  sf_mex_destroy(&c7_rhs35);
  sf_mex_destroy(&c7_lhs35);
  sf_mex_destroy(&c7_rhs36);
  sf_mex_destroy(&c7_lhs36);
  sf_mex_destroy(&c7_rhs37);
  sf_mex_destroy(&c7_lhs37);
  sf_mex_destroy(&c7_rhs38);
  sf_mex_destroy(&c7_lhs38);
  sf_mex_destroy(&c7_rhs39);
  sf_mex_destroy(&c7_lhs39);
  sf_mex_destroy(&c7_rhs40);
  sf_mex_destroy(&c7_lhs40);
  sf_mex_destroy(&c7_rhs41);
  sf_mex_destroy(&c7_lhs41);
  sf_mex_destroy(&c7_rhs42);
  sf_mex_destroy(&c7_lhs42);
  sf_mex_destroy(&c7_rhs43);
  sf_mex_destroy(&c7_lhs43);
  sf_mex_destroy(&c7_rhs44);
  sf_mex_destroy(&c7_lhs44);
  sf_mex_destroy(&c7_rhs45);
  sf_mex_destroy(&c7_lhs45);
  sf_mex_destroy(&c7_rhs46);
  sf_mex_destroy(&c7_lhs46);
  sf_mex_destroy(&c7_rhs47);
  sf_mex_destroy(&c7_lhs47);
  sf_mex_destroy(&c7_rhs48);
  sf_mex_destroy(&c7_lhs48);
  sf_mex_destroy(&c7_rhs49);
  sf_mex_destroy(&c7_lhs49);
  sf_mex_destroy(&c7_rhs50);
  sf_mex_destroy(&c7_lhs50);
  sf_mex_destroy(&c7_rhs51);
  sf_mex_destroy(&c7_lhs51);
  sf_mex_destroy(&c7_rhs52);
  sf_mex_destroy(&c7_lhs52);
  sf_mex_destroy(&c7_rhs53);
  sf_mex_destroy(&c7_lhs53);
  sf_mex_destroy(&c7_rhs54);
  sf_mex_destroy(&c7_lhs54);
  sf_mex_destroy(&c7_rhs55);
  sf_mex_destroy(&c7_lhs55);
  sf_mex_destroy(&c7_rhs56);
  sf_mex_destroy(&c7_lhs56);
  sf_mex_destroy(&c7_rhs57);
  sf_mex_destroy(&c7_lhs57);
  sf_mex_destroy(&c7_rhs58);
  sf_mex_destroy(&c7_lhs58);
  sf_mex_destroy(&c7_rhs59);
  sf_mex_destroy(&c7_lhs59);
  sf_mex_destroy(&c7_rhs60);
  sf_mex_destroy(&c7_lhs60);
  sf_mex_destroy(&c7_rhs61);
  sf_mex_destroy(&c7_lhs61);
}

static const mxArray *c7_b_emlrt_marshallOut(const char * c7_u)
{
  const mxArray *c7_y = NULL;
  c7_y = NULL;
  sf_mex_assign(&c7_y, sf_mex_create("y", c7_u, 15, 0U, 0U, 0U, 2, 1, strlen
    (c7_u)), false);
  return c7_y;
}

static const mxArray *c7_c_emlrt_marshallOut(const uint32_T c7_u)
{
  const mxArray *c7_y = NULL;
  c7_y = NULL;
  sf_mex_assign(&c7_y, sf_mex_create("y", &c7_u, 7, 0U, 0U, 0U, 0), false);
  return c7_y;
}

static int8_T c7_typecast(SFc7_manualControllerInstanceStruct *chartInstance,
  uint8_T c7_x)
{
  int8_T c7_y;
  (void)chartInstance;
  memcpy(&c7_y, &c7_x, (size_t)1 * sizeof(int8_T));
  return c7_y;
}

static const mxArray *c7_j_sf_marshallOut(void *chartInstanceVoid, void
  *c7_inData)
{
  const mxArray *c7_mxArrayOutData = NULL;
  int32_T c7_u;
  const mxArray *c7_y = NULL;
  SFc7_manualControllerInstanceStruct *chartInstance;
  chartInstance = (SFc7_manualControllerInstanceStruct *)chartInstanceVoid;
  c7_mxArrayOutData = NULL;
  c7_u = *(int32_T *)c7_inData;
  c7_y = NULL;
  sf_mex_assign(&c7_y, sf_mex_create("y", &c7_u, 6, 0U, 0U, 0U, 0), false);
  sf_mex_assign(&c7_mxArrayOutData, c7_y, false);
  return c7_mxArrayOutData;
}

static int32_T c7_o_emlrt_marshallIn(SFc7_manualControllerInstanceStruct
  *chartInstance, const mxArray *c7_u, const emlrtMsgIdentifier *c7_parentId)
{
  int32_T c7_y;
  int32_T c7_i35;
  (void)chartInstance;
  sf_mex_import(c7_parentId, sf_mex_dup(c7_u), &c7_i35, 1, 6, 0U, 0, 0U, 0);
  c7_y = c7_i35;
  sf_mex_destroy(&c7_u);
  return c7_y;
}

static void c7_i_sf_marshallIn(void *chartInstanceVoid, const mxArray
  *c7_mxArrayInData, const char_T *c7_varName, void *c7_outData)
{
  const mxArray *c7_b_sfEvent;
  const char_T *c7_identifier;
  emlrtMsgIdentifier c7_thisId;
  int32_T c7_y;
  SFc7_manualControllerInstanceStruct *chartInstance;
  chartInstance = (SFc7_manualControllerInstanceStruct *)chartInstanceVoid;
  c7_b_sfEvent = sf_mex_dup(c7_mxArrayInData);
  c7_identifier = c7_varName;
  c7_thisId.fIdentifier = c7_identifier;
  c7_thisId.fParent = NULL;
  c7_y = c7_o_emlrt_marshallIn(chartInstance, sf_mex_dup(c7_b_sfEvent),
    &c7_thisId);
  sf_mex_destroy(&c7_b_sfEvent);
  *(int32_T *)c7_outData = c7_y;
  sf_mex_destroy(&c7_mxArrayInData);
}

static const mxArray *c7_Ball_bus_io(void *chartInstanceVoid, void *c7_pData)
{
  const mxArray *c7_mxVal = NULL;
  c7_Ball c7_tmp;
  SFc7_manualControllerInstanceStruct *chartInstance;
  chartInstance = (SFc7_manualControllerInstanceStruct *)chartInstanceVoid;
  c7_mxVal = NULL;
  c7_tmp.x = *(int8_T *)&((char_T *)(c7_Ball *)c7_pData)[0];
  c7_tmp.y = *(int8_T *)&((char_T *)(c7_Ball *)c7_pData)[1];
  c7_tmp.valid = *(uint8_T *)&((char_T *)(c7_Ball *)c7_pData)[2];
  sf_mex_assign(&c7_mxVal, c7_d_sf_marshallOut(chartInstance, &c7_tmp), false);
  return c7_mxVal;
}

static const mxArray *c7_players_bus_io(void *chartInstanceVoid, void *c7_pData)
{
  const mxArray *c7_mxVal = NULL;
  int32_T c7_i36;
  c7_Player c7_tmp[6];
  SFc7_manualControllerInstanceStruct *chartInstance;
  chartInstance = (SFc7_manualControllerInstanceStruct *)chartInstanceVoid;
  c7_mxVal = NULL;
  for (c7_i36 = 0; c7_i36 < 6; c7_i36++) {
    c7_tmp[c7_i36].x = *(int8_T *)&((char_T *)(c7_Player *)&((char_T *)
      (c7_Player (*)[6])c7_pData)[8 * c7_i36])[0];
    c7_tmp[c7_i36].y = *(int8_T *)&((char_T *)(c7_Player *)&((char_T *)
      (c7_Player (*)[6])c7_pData)[8 * c7_i36])[1];
    c7_tmp[c7_i36].orientation = *(int16_T *)&((char_T *)(c7_Player *)&((char_T *)
      (c7_Player (*)[6])c7_pData)[8 * c7_i36])[2];
    c7_tmp[c7_i36].color = *(uint8_T *)&((char_T *)(c7_Player *)&((char_T *)
      (c7_Player (*)[6])c7_pData)[8 * c7_i36])[4];
    c7_tmp[c7_i36].position = *(uint8_T *)&((char_T *)(c7_Player *)&((char_T *)
      (c7_Player (*)[6])c7_pData)[8 * c7_i36])[5];
    c7_tmp[c7_i36].valid = *(uint8_T *)&((char_T *)(c7_Player *)&((char_T *)
      (c7_Player (*)[6])c7_pData)[8 * c7_i36])[6];
  }

  sf_mex_assign(&c7_mxVal, c7_c_sf_marshallOut(chartInstance, c7_tmp), false);
  return c7_mxVal;
}

static void init_dsm_address_info(SFc7_manualControllerInstanceStruct
  *chartInstance)
{
  (void)chartInstance;
}

static void init_simulink_io_address(SFc7_manualControllerInstanceStruct
  *chartInstance)
{
  chartInstance->c7_b_Ball = (c7_Ball *)ssGetOutputPortSignal_wrapper
    (chartInstance->S, 1);
  chartInstance->c7_players = (c7_Player (*)[6])ssGetOutputPortSignal_wrapper
    (chartInstance->S, 2);
  chartInstance->c7_gameOn = (uint8_T *)ssGetOutputPortSignal_wrapper
    (chartInstance->S, 3);
  chartInstance->c7_newMessage = (uint8_T (*)[27])ssGetInputPortSignal_wrapper
    (chartInstance->S, 0);
}

/* SFunction Glue Code */
#ifdef utFree
#undef utFree
#endif

#ifdef utMalloc
#undef utMalloc
#endif

#ifdef __cplusplus

extern "C" void *utMalloc(size_t size);
extern "C" void utFree(void*);

#else

extern void *utMalloc(size_t size);
extern void utFree(void*);

#endif

void sf_c7_manualController_get_check_sum(mxArray *plhs[])
{
  ((real_T *)mxGetPr((plhs[0])))[0] = (real_T)(4129904324U);
  ((real_T *)mxGetPr((plhs[0])))[1] = (real_T)(54153943U);
  ((real_T *)mxGetPr((plhs[0])))[2] = (real_T)(866825271U);
  ((real_T *)mxGetPr((plhs[0])))[3] = (real_T)(3705070447U);
}

mxArray* sf_c7_manualController_get_post_codegen_info(void);
mxArray *sf_c7_manualController_get_autoinheritance_info(void)
{
  const char *autoinheritanceFields[] = { "checksum", "inputs", "parameters",
    "outputs", "locals", "postCodegenInfo" };

  mxArray *mxAutoinheritanceInfo = mxCreateStructMatrix(1, 1, sizeof
    (autoinheritanceFields)/sizeof(autoinheritanceFields[0]),
    autoinheritanceFields);

  {
    mxArray *mxChecksum = mxCreateString("PGnCf0kevaRCvDCyQOz4kE");
    mxSetField(mxAutoinheritanceInfo,0,"checksum",mxChecksum);
  }

  {
    const char *dataFields[] = { "size", "type", "complexity" };

    mxArray *mxData = mxCreateStructMatrix(1,1,3,dataFields);

    {
      mxArray *mxSize = mxCreateDoubleMatrix(1,2,mxREAL);
      double *pr = mxGetPr(mxSize);
      pr[0] = (double)(1);
      pr[1] = (double)(27);
      mxSetField(mxData,0,"size",mxSize);
    }

    {
      const char *typeFields[] = { "base", "fixpt" };

      mxArray *mxType = mxCreateStructMatrix(1,1,2,typeFields);
      mxSetField(mxType,0,"base",mxCreateDoubleScalar(3));
      mxSetField(mxType,0,"fixpt",mxCreateDoubleMatrix(0,0,mxREAL));
      mxSetField(mxData,0,"type",mxType);
    }

    mxSetField(mxData,0,"complexity",mxCreateDoubleScalar(0));
    mxSetField(mxAutoinheritanceInfo,0,"inputs",mxData);
  }

  {
    mxSetField(mxAutoinheritanceInfo,0,"parameters",mxCreateDoubleMatrix(0,0,
                mxREAL));
  }

  {
    const char *dataFields[] = { "size", "type", "complexity" };

    mxArray *mxData = mxCreateStructMatrix(1,3,3,dataFields);

    {
      mxArray *mxSize = mxCreateDoubleMatrix(1,2,mxREAL);
      double *pr = mxGetPr(mxSize);
      pr[0] = (double)(1);
      pr[1] = (double)(1);
      mxSetField(mxData,0,"size",mxSize);
    }

    {
      const char *typeFields[] = { "base", "fixpt" };

      mxArray *mxType = mxCreateStructMatrix(1,1,2,typeFields);
      mxSetField(mxType,0,"base",mxCreateDoubleScalar(13));
      mxSetField(mxType,0,"fixpt",mxCreateDoubleMatrix(0,0,mxREAL));
      mxSetField(mxData,0,"type",mxType);
    }

    mxSetField(mxData,0,"complexity",mxCreateDoubleScalar(0));

    {
      mxArray *mxSize = mxCreateDoubleMatrix(1,2,mxREAL);
      double *pr = mxGetPr(mxSize);
      pr[0] = (double)(6);
      pr[1] = (double)(1);
      mxSetField(mxData,1,"size",mxSize);
    }

    {
      const char *typeFields[] = { "base", "fixpt" };

      mxArray *mxType = mxCreateStructMatrix(1,1,2,typeFields);
      mxSetField(mxType,0,"base",mxCreateDoubleScalar(13));
      mxSetField(mxType,0,"fixpt",mxCreateDoubleMatrix(0,0,mxREAL));
      mxSetField(mxData,1,"type",mxType);
    }

    mxSetField(mxData,1,"complexity",mxCreateDoubleScalar(0));

    {
      mxArray *mxSize = mxCreateDoubleMatrix(1,2,mxREAL);
      double *pr = mxGetPr(mxSize);
      pr[0] = (double)(1);
      pr[1] = (double)(1);
      mxSetField(mxData,2,"size",mxSize);
    }

    {
      const char *typeFields[] = { "base", "fixpt" };

      mxArray *mxType = mxCreateStructMatrix(1,1,2,typeFields);
      mxSetField(mxType,0,"base",mxCreateDoubleScalar(3));
      mxSetField(mxType,0,"fixpt",mxCreateDoubleMatrix(0,0,mxREAL));
      mxSetField(mxData,2,"type",mxType);
    }

    mxSetField(mxData,2,"complexity",mxCreateDoubleScalar(0));
    mxSetField(mxAutoinheritanceInfo,0,"outputs",mxData);
  }

  {
    mxSetField(mxAutoinheritanceInfo,0,"locals",mxCreateDoubleMatrix(0,0,mxREAL));
  }

  {
    mxArray* mxPostCodegenInfo = sf_c7_manualController_get_post_codegen_info();
    mxSetField(mxAutoinheritanceInfo,0,"postCodegenInfo",mxPostCodegenInfo);
  }

  return(mxAutoinheritanceInfo);
}

mxArray *sf_c7_manualController_third_party_uses_info(void)
{
  mxArray * mxcell3p = mxCreateCellMatrix(1,0);
  return(mxcell3p);
}

mxArray *sf_c7_manualController_jit_fallback_info(void)
{
  const char *infoFields[] = { "fallbackType", "fallbackReason",
    "incompatibleSymbol", };

  mxArray *mxInfo = mxCreateStructMatrix(1, 1, 3, infoFields);
  mxArray *fallbackReason = mxCreateString("feature_off");
  mxArray *incompatibleSymbol = mxCreateString("");
  mxArray *fallbackType = mxCreateString("early");
  mxSetField(mxInfo, 0, infoFields[0], fallbackType);
  mxSetField(mxInfo, 0, infoFields[1], fallbackReason);
  mxSetField(mxInfo, 0, infoFields[2], incompatibleSymbol);
  return mxInfo;
}

mxArray *sf_c7_manualController_updateBuildInfo_args_info(void)
{
  mxArray *mxBIArgs = mxCreateCellMatrix(1,0);
  return mxBIArgs;
}

mxArray* sf_c7_manualController_get_post_codegen_info(void)
{
  const char* fieldNames[] = { "exportedFunctionsUsedByThisChart",
    "exportedFunctionsChecksum" };

  mwSize dims[2] = { 1, 1 };

  mxArray* mxPostCodegenInfo = mxCreateStructArray(2, dims, sizeof(fieldNames)/
    sizeof(fieldNames[0]), fieldNames);

  {
    mxArray* mxExportedFunctionsChecksum = mxCreateString("");
    mwSize exp_dims[2] = { 0, 1 };

    mxArray* mxExportedFunctionsUsedByThisChart = mxCreateCellArray(2, exp_dims);
    mxSetField(mxPostCodegenInfo, 0, "exportedFunctionsUsedByThisChart",
               mxExportedFunctionsUsedByThisChart);
    mxSetField(mxPostCodegenInfo, 0, "exportedFunctionsChecksum",
               mxExportedFunctionsChecksum);
  }

  return mxPostCodegenInfo;
}

static const mxArray *sf_get_sim_state_info_c7_manualController(void)
{
  const char *infoFields[] = { "chartChecksum", "varInfo" };

  mxArray *mxInfo = mxCreateStructMatrix(1, 1, 2, infoFields);
  const char *infoEncStr[] = {
    "100 S1x5'type','srcId','name','auxInfo'{{M[1],M[5],T\"Ball\",},{M[1],M[9],T\"gameOn\",},{M[1],M[8],T\"players\",},{M[4],M[0],T\"oldMes\",S'l','i','p'{{M1x2[122 128],M[0],}}},{M[8],M[0],T\"is_active_c7_manualController\",}}"
  };

  mxArray *mxVarInfo = sf_mex_decode_encoded_mx_struct_array(infoEncStr, 5, 10);
  mxArray *mxChecksum = mxCreateDoubleMatrix(1, 4, mxREAL);
  sf_c7_manualController_get_check_sum(&mxChecksum);
  mxSetField(mxInfo, 0, infoFields[0], mxChecksum);
  mxSetField(mxInfo, 0, infoFields[1], mxVarInfo);
  return mxInfo;
}

static void chart_debug_initialization(SimStruct *S, unsigned int
  fullDebuggerInitialization)
{
  if (!sim_mode_is_rtw_gen(S)) {
    SFc7_manualControllerInstanceStruct *chartInstance;
    ChartRunTimeInfo * crtInfo = (ChartRunTimeInfo *)(ssGetUserData(S));
    ChartInfoStruct * chartInfo = (ChartInfoStruct *)(crtInfo->instanceInfo);
    chartInstance = (SFc7_manualControllerInstanceStruct *)
      chartInfo->chartInstance;
    if (ssIsFirstInitCond(S) && fullDebuggerInitialization==1) {
      /* do this only if simulation is starting */
      {
        unsigned int chartAlreadyPresent;
        chartAlreadyPresent = sf_debug_initialize_chart
          (sfGlobalDebugInstanceStruct,
           _manualControllerMachineNumber_,
           7,
           1,
           1,
           0,
           4,
           0,
           0,
           0,
           0,
           0,
           &(chartInstance->chartNumber),
           &(chartInstance->instanceNumber),
           (void *)S);

        /* Each instance must initialize its own list of scripts */
        init_script_number_translation(_manualControllerMachineNumber_,
          chartInstance->chartNumber,chartInstance->instanceNumber);
        if (chartAlreadyPresent==0) {
          /* this is the first instance */
          sf_debug_set_chart_disable_implicit_casting
            (sfGlobalDebugInstanceStruct,_manualControllerMachineNumber_,
             chartInstance->chartNumber,1);
          sf_debug_set_chart_event_thresholds(sfGlobalDebugInstanceStruct,
            _manualControllerMachineNumber_,
            chartInstance->chartNumber,
            0,
            0,
            0);
          _SFD_SET_DATA_PROPS(0,2,0,1,"Ball");
          _SFD_SET_DATA_PROPS(1,2,0,1,"players");
          _SFD_SET_DATA_PROPS(2,2,0,1,"gameOn");
          _SFD_SET_DATA_PROPS(3,1,1,0,"newMessage");
          _SFD_STATE_INFO(0,0,2);
          _SFD_CH_SUBSTATE_COUNT(0);
          _SFD_CH_SUBSTATE_DECOMP(0);
        }

        _SFD_CV_INIT_CHART(0,0,0,0);

        {
          _SFD_CV_INIT_STATE(0,0,0,0,0,0,NULL,NULL);
        }

        _SFD_CV_INIT_TRANS(0,0,NULL,NULL,0,NULL);

        /* Initialization of MATLAB Function Model Coverage */
        _SFD_CV_INIT_EML(0,1,1,2,0,0,0,1,0,1,1);
        _SFD_CV_INIT_EML_FCN(0,0,"eML_blk_kernel",0,-1,1015);
        _SFD_CV_INIT_EML_IF(0,1,0,130,148,-1,184);
        _SFD_CV_INIT_EML_IF(0,1,1,185,204,228,259);
        _SFD_CV_INIT_EML_FOR(0,1,0,693,704,1014);

        {
          static int condStart[] = { 189 };

          static int condEnd[] = { 204 };

          static int pfixExpr[] = { 0, -1 };

          _SFD_CV_INIT_EML_MCDC(0,1,0,188,204,1,0,&(condStart[0]),&(condEnd[0]),
                                2,&(pfixExpr[0]));
        }

        _SFD_SET_DATA_COMPILED_PROPS(0,SF_STRUCT,0,NULL,0,0,0,0.0,1.0,0,0,
          (MexFcnForType)c7_Ball_bus_io,(MexInFcnForType)NULL);

        {
          unsigned int dimVector[1];
          dimVector[0]= 6;
          _SFD_SET_DATA_COMPILED_PROPS(1,SF_STRUCT,1,&(dimVector[0]),0,0,0,0.0,
            1.0,0,0,(MexFcnForType)c7_players_bus_io,(MexInFcnForType)NULL);
        }

        _SFD_SET_DATA_COMPILED_PROPS(2,SF_UINT8,0,NULL,0,0,0,0.0,1.0,0,0,
          (MexFcnForType)c7_b_sf_marshallOut,(MexInFcnForType)c7_b_sf_marshallIn);

        {
          unsigned int dimVector[2];
          dimVector[0]= 1;
          dimVector[1]= 27;
          _SFD_SET_DATA_COMPILED_PROPS(3,SF_UINT8,2,&(dimVector[0]),0,0,0,0.0,
            1.0,0,0,(MexFcnForType)c7_e_sf_marshallOut,(MexInFcnForType)NULL);
        }

        _SFD_SET_DATA_VALUE_PTR(0U, chartInstance->c7_b_Ball);
        _SFD_SET_DATA_VALUE_PTR(1U, *chartInstance->c7_players);
        _SFD_SET_DATA_VALUE_PTR(2U, chartInstance->c7_gameOn);
        _SFD_SET_DATA_VALUE_PTR(3U, *chartInstance->c7_newMessage);
      }
    } else {
      sf_debug_reset_current_state_configuration(sfGlobalDebugInstanceStruct,
        _manualControllerMachineNumber_,chartInstance->chartNumber,
        chartInstance->instanceNumber);
    }
  }
}

static const char* sf_get_instance_specialization(void)
{
  return "d1cYeVpPJg74IwzYxoZF7C";
}

static void sf_opaque_initialize_c7_manualController(void *chartInstanceVar)
{
  chart_debug_initialization(((SFc7_manualControllerInstanceStruct*)
    chartInstanceVar)->S,0);
  initialize_params_c7_manualController((SFc7_manualControllerInstanceStruct*)
    chartInstanceVar);
  initialize_c7_manualController((SFc7_manualControllerInstanceStruct*)
    chartInstanceVar);
}

static void sf_opaque_enable_c7_manualController(void *chartInstanceVar)
{
  enable_c7_manualController((SFc7_manualControllerInstanceStruct*)
    chartInstanceVar);
}

static void sf_opaque_disable_c7_manualController(void *chartInstanceVar)
{
  disable_c7_manualController((SFc7_manualControllerInstanceStruct*)
    chartInstanceVar);
}

static void sf_opaque_gateway_c7_manualController(void *chartInstanceVar)
{
  sf_gateway_c7_manualController((SFc7_manualControllerInstanceStruct*)
    chartInstanceVar);
}

static const mxArray* sf_opaque_get_sim_state_c7_manualController(SimStruct* S)
{
  ChartRunTimeInfo * crtInfo = (ChartRunTimeInfo *)(ssGetUserData(S));
  ChartInfoStruct * chartInfo = (ChartInfoStruct *)(crtInfo->instanceInfo);
  return get_sim_state_c7_manualController((SFc7_manualControllerInstanceStruct*)
    chartInfo->chartInstance);         /* raw sim ctx */
}

static void sf_opaque_set_sim_state_c7_manualController(SimStruct* S, const
  mxArray *st)
{
  ChartRunTimeInfo * crtInfo = (ChartRunTimeInfo *)(ssGetUserData(S));
  ChartInfoStruct * chartInfo = (ChartInfoStruct *)(crtInfo->instanceInfo);
  set_sim_state_c7_manualController((SFc7_manualControllerInstanceStruct*)
    chartInfo->chartInstance, st);
}

static void sf_opaque_terminate_c7_manualController(void *chartInstanceVar)
{
  if (chartInstanceVar!=NULL) {
    SimStruct *S = ((SFc7_manualControllerInstanceStruct*) chartInstanceVar)->S;
    ChartRunTimeInfo * crtInfo = (ChartRunTimeInfo *)(ssGetUserData(S));
    if (sim_mode_is_rtw_gen(S) || sim_mode_is_external(S)) {
      sf_clear_rtw_identifier(S);
      unload_manualController_optimization_info();
    }

    finalize_c7_manualController((SFc7_manualControllerInstanceStruct*)
      chartInstanceVar);
    utFree(chartInstanceVar);
    if (crtInfo != NULL) {
      utFree(crtInfo);
    }

    ssSetUserData(S,NULL);
  }
}

static void sf_opaque_init_subchart_simstructs(void *chartInstanceVar)
{
  initSimStructsc7_manualController((SFc7_manualControllerInstanceStruct*)
    chartInstanceVar);
}

extern unsigned int sf_machine_global_initializer_called(void);
static void mdlProcessParameters_c7_manualController(SimStruct *S)
{
  int i;
  for (i=0;i<ssGetNumRunTimeParams(S);i++) {
    if (ssGetSFcnParamTunable(S,i)) {
      ssUpdateDlgParamAsRunTimeParam(S,i);
    }
  }

  if (sf_machine_global_initializer_called()) {
    ChartRunTimeInfo * crtInfo = (ChartRunTimeInfo *)(ssGetUserData(S));
    ChartInfoStruct * chartInfo = (ChartInfoStruct *)(crtInfo->instanceInfo);
    initialize_params_c7_manualController((SFc7_manualControllerInstanceStruct*)
      (chartInfo->chartInstance));
  }
}

static void mdlSetWorkWidths_c7_manualController(SimStruct *S)
{
  if (sim_mode_is_rtw_gen(S) || sim_mode_is_external(S)) {
    mxArray *infoStruct = load_manualController_optimization_info();
    int_T chartIsInlinable =
      (int_T)sf_is_chart_inlinable(sf_get_instance_specialization(),infoStruct,7);
    ssSetStateflowIsInlinable(S,chartIsInlinable);
    ssSetRTWCG(S,sf_rtw_info_uint_prop(sf_get_instance_specialization(),
                infoStruct,7,"RTWCG"));
    ssSetEnableFcnIsTrivial(S,1);
    ssSetDisableFcnIsTrivial(S,1);
    ssSetNotMultipleInlinable(S,sf_rtw_info_uint_prop
      (sf_get_instance_specialization(),infoStruct,7,
       "gatewayCannotBeInlinedMultipleTimes"));
    sf_update_buildInfo(sf_get_instance_specialization(),infoStruct,7);
    if (chartIsInlinable) {
      ssSetInputPortOptimOpts(S, 0, SS_REUSABLE_AND_LOCAL);
      sf_mark_chart_expressionable_inputs(S,sf_get_instance_specialization(),
        infoStruct,7,1);
      sf_mark_chart_reusable_outputs(S,sf_get_instance_specialization(),
        infoStruct,7,3);
    }

    {
      unsigned int outPortIdx;
      for (outPortIdx=1; outPortIdx<=3; ++outPortIdx) {
        ssSetOutputPortOptimizeInIR(S, outPortIdx, 1U);
      }
    }

    {
      unsigned int inPortIdx;
      for (inPortIdx=0; inPortIdx < 1; ++inPortIdx) {
        ssSetInputPortOptimizeInIR(S, inPortIdx, 1U);
      }
    }

    sf_set_rtw_dwork_info(S,sf_get_instance_specialization(),infoStruct,7);
    ssSetHasSubFunctions(S,!(chartIsInlinable));
  } else {
  }

  ssSetOptions(S,ssGetOptions(S)|SS_OPTION_WORKS_WITH_CODE_REUSE);
  ssSetChecksum0(S,(14439357U));
  ssSetChecksum1(S,(2578614704U));
  ssSetChecksum2(S,(1382349549U));
  ssSetChecksum3(S,(2717272831U));
  ssSetmdlDerivatives(S, NULL);
  ssSetExplicitFCSSCtrl(S,1);
  ssSupportsMultipleExecInstances(S,1);
}

static void mdlRTW_c7_manualController(SimStruct *S)
{
  if (sim_mode_is_rtw_gen(S)) {
    ssWriteRTWStrParam(S, "StateflowChartType", "Embedded MATLAB");
  }
}

static void mdlStart_c7_manualController(SimStruct *S)
{
  SFc7_manualControllerInstanceStruct *chartInstance;
  ChartRunTimeInfo * crtInfo = (ChartRunTimeInfo *)utMalloc(sizeof
    (ChartRunTimeInfo));
  chartInstance = (SFc7_manualControllerInstanceStruct *)utMalloc(sizeof
    (SFc7_manualControllerInstanceStruct));
  memset(chartInstance, 0, sizeof(SFc7_manualControllerInstanceStruct));
  if (chartInstance==NULL) {
    sf_mex_error_message("Could not allocate memory for chart instance.");
  }

  chartInstance->chartInfo.chartInstance = chartInstance;
  chartInstance->chartInfo.isEMLChart = 1;
  chartInstance->chartInfo.chartInitialized = 0;
  chartInstance->chartInfo.sFunctionGateway =
    sf_opaque_gateway_c7_manualController;
  chartInstance->chartInfo.initializeChart =
    sf_opaque_initialize_c7_manualController;
  chartInstance->chartInfo.terminateChart =
    sf_opaque_terminate_c7_manualController;
  chartInstance->chartInfo.enableChart = sf_opaque_enable_c7_manualController;
  chartInstance->chartInfo.disableChart = sf_opaque_disable_c7_manualController;
  chartInstance->chartInfo.getSimState =
    sf_opaque_get_sim_state_c7_manualController;
  chartInstance->chartInfo.setSimState =
    sf_opaque_set_sim_state_c7_manualController;
  chartInstance->chartInfo.getSimStateInfo =
    sf_get_sim_state_info_c7_manualController;
  chartInstance->chartInfo.zeroCrossings = NULL;
  chartInstance->chartInfo.outputs = NULL;
  chartInstance->chartInfo.derivatives = NULL;
  chartInstance->chartInfo.mdlRTW = mdlRTW_c7_manualController;
  chartInstance->chartInfo.mdlStart = mdlStart_c7_manualController;
  chartInstance->chartInfo.mdlSetWorkWidths =
    mdlSetWorkWidths_c7_manualController;
  chartInstance->chartInfo.extModeExec = NULL;
  chartInstance->chartInfo.restoreLastMajorStepConfiguration = NULL;
  chartInstance->chartInfo.restoreBeforeLastMajorStepConfiguration = NULL;
  chartInstance->chartInfo.storeCurrentConfiguration = NULL;
  chartInstance->chartInfo.callAtomicSubchartUserFcn = NULL;
  chartInstance->chartInfo.callAtomicSubchartAutoFcn = NULL;
  chartInstance->chartInfo.debugInstance = sfGlobalDebugInstanceStruct;
  chartInstance->S = S;
  crtInfo->checksum = SF_RUNTIME_INFO_CHECKSUM;
  crtInfo->instanceInfo = (&(chartInstance->chartInfo));
  crtInfo->isJITEnabled = false;
  crtInfo->compiledInfo = NULL;
  ssSetUserData(S,(void *)(crtInfo));  /* register the chart instance with simstruct */
  init_dsm_address_info(chartInstance);
  init_simulink_io_address(chartInstance);
  if (!sim_mode_is_rtw_gen(S)) {
  }

  sf_opaque_init_subchart_simstructs(chartInstance->chartInfo.chartInstance);
  chart_debug_initialization(S,1);
}

void c7_manualController_method_dispatcher(SimStruct *S, int_T method, void
  *data)
{
  switch (method) {
   case SS_CALL_MDL_START:
    mdlStart_c7_manualController(S);
    break;

   case SS_CALL_MDL_SET_WORK_WIDTHS:
    mdlSetWorkWidths_c7_manualController(S);
    break;

   case SS_CALL_MDL_PROCESS_PARAMETERS:
    mdlProcessParameters_c7_manualController(S);
    break;

   default:
    /* Unhandled method */
    sf_mex_error_message("Stateflow Internal Error:\n"
                         "Error calling c7_manualController_method_dispatcher.\n"
                         "Can't handle method %d.\n", method);
    break;
  }
}

/* Include files */

#include "greenOffence_sfun.h"
#include "greenOffence_sfun_debug_macros.h"
#include "c1_greenOffence.h"
#include "c3_greenOffence.h"
#include "c4_greenOffence.h"
#include "c7_greenOffence.h"
#include "c9_greenOffence.h"
#include "c11_greenOffence.h"
#include "c13_greenOffence.h"
#include "c20_greenOffence.h"
#include "c21_greenOffence.h"
#include "c22_greenOffence.h"

/* Type Definitions */

/* Named Constants */

/* Variable Declarations */

/* Variable Definitions */
uint32_T _greenOffenceMachineNumber_;

/* Function Declarations */

/* Function Definitions */
void greenOffence_initializer(void)
{
}

void greenOffence_terminator(void)
{
}

/* SFunction Glue Code */
unsigned int sf_greenOffence_method_dispatcher(SimStruct *simstructPtr, unsigned
  int chartFileNumber, const char* specsCksum, int_T method, void *data)
{
  if (chartFileNumber==1) {
    c1_greenOffence_method_dispatcher(simstructPtr, method, data);
    return 1;
  }

  if (chartFileNumber==3) {
    c3_greenOffence_method_dispatcher(simstructPtr, method, data);
    return 1;
  }

  if (chartFileNumber==4) {
    c4_greenOffence_method_dispatcher(simstructPtr, method, data);
    return 1;
  }

  if (chartFileNumber==7) {
    c7_greenOffence_method_dispatcher(simstructPtr, method, data);
    return 1;
  }

  if (chartFileNumber==9) {
    c9_greenOffence_method_dispatcher(simstructPtr, method, data);
    return 1;
  }

  if (chartFileNumber==11) {
    c11_greenOffence_method_dispatcher(simstructPtr, method, data);
    return 1;
  }

  if (chartFileNumber==13) {
    c13_greenOffence_method_dispatcher(simstructPtr, method, data);
    return 1;
  }

  if (chartFileNumber==20) {
    c20_greenOffence_method_dispatcher(simstructPtr, method, data);
    return 1;
  }

  if (chartFileNumber==21) {
    c21_greenOffence_method_dispatcher(simstructPtr, method, data);
    return 1;
  }

  if (chartFileNumber==22) {
    c22_greenOffence_method_dispatcher(simstructPtr, method, data);
    return 1;
  }

  return 0;
}

extern void sf_greenOffence_uses_exported_functions(int nlhs, mxArray * plhs[],
  int nrhs, const mxArray * prhs[])
{
  plhs[0] = mxCreateLogicalScalar(0);
}

unsigned int sf_greenOffence_process_check_sum_call( int nlhs, mxArray * plhs[],
  int nrhs, const mxArray * prhs[] )
{

#ifdef MATLAB_MEX_FILE

  char commandName[20];
  if (nrhs<1 || !mxIsChar(prhs[0]) )
    return 0;

  /* Possible call to get the checksum */
  mxGetString(prhs[0], commandName,sizeof(commandName)/sizeof(char));
  commandName[(sizeof(commandName)/sizeof(char)-1)] = '\0';
  if (strcmp(commandName,"sf_get_check_sum"))
    return 0;
  plhs[0] = mxCreateDoubleMatrix( 1,4,mxREAL);
  if (nrhs>1 && mxIsChar(prhs[1])) {
    mxGetString(prhs[1], commandName,sizeof(commandName)/sizeof(char));
    commandName[(sizeof(commandName)/sizeof(char)-1)] = '\0';
    if (!strcmp(commandName,"machine")) {
      ((real_T *)mxGetPr((plhs[0])))[0] = (real_T)(405107593U);
      ((real_T *)mxGetPr((plhs[0])))[1] = (real_T)(2221119640U);
      ((real_T *)mxGetPr((plhs[0])))[2] = (real_T)(2312444883U);
      ((real_T *)mxGetPr((plhs[0])))[3] = (real_T)(371249780U);
    } else if (!strcmp(commandName,"exportedFcn")) {
      ((real_T *)mxGetPr((plhs[0])))[0] = (real_T)(0U);
      ((real_T *)mxGetPr((plhs[0])))[1] = (real_T)(0U);
      ((real_T *)mxGetPr((plhs[0])))[2] = (real_T)(0U);
      ((real_T *)mxGetPr((plhs[0])))[3] = (real_T)(0U);
    } else if (!strcmp(commandName,"makefile")) {
      ((real_T *)mxGetPr((plhs[0])))[0] = (real_T)(1091112704U);
      ((real_T *)mxGetPr((plhs[0])))[1] = (real_T)(2700164705U);
      ((real_T *)mxGetPr((plhs[0])))[2] = (real_T)(768033957U);
      ((real_T *)mxGetPr((plhs[0])))[3] = (real_T)(2231167886U);
    } else if (nrhs==3 && !strcmp(commandName,"chart")) {
      unsigned int chartFileNumber;
      chartFileNumber = (unsigned int)mxGetScalar(prhs[2]);
      switch (chartFileNumber) {
       case 1:
        {
          extern void sf_c1_greenOffence_get_check_sum(mxArray *plhs[]);
          sf_c1_greenOffence_get_check_sum(plhs);
          break;
        }

       case 3:
        {
          extern void sf_c3_greenOffence_get_check_sum(mxArray *plhs[]);
          sf_c3_greenOffence_get_check_sum(plhs);
          break;
        }

       case 4:
        {
          extern void sf_c4_greenOffence_get_check_sum(mxArray *plhs[]);
          sf_c4_greenOffence_get_check_sum(plhs);
          break;
        }

       case 7:
        {
          extern void sf_c7_greenOffence_get_check_sum(mxArray *plhs[]);
          sf_c7_greenOffence_get_check_sum(plhs);
          break;
        }

       case 9:
        {
          extern void sf_c9_greenOffence_get_check_sum(mxArray *plhs[]);
          sf_c9_greenOffence_get_check_sum(plhs);
          break;
        }

       case 11:
        {
          extern void sf_c11_greenOffence_get_check_sum(mxArray *plhs[]);
          sf_c11_greenOffence_get_check_sum(plhs);
          break;
        }

       case 13:
        {
          extern void sf_c13_greenOffence_get_check_sum(mxArray *plhs[]);
          sf_c13_greenOffence_get_check_sum(plhs);
          break;
        }

       case 20:
        {
          extern void sf_c20_greenOffence_get_check_sum(mxArray *plhs[]);
          sf_c20_greenOffence_get_check_sum(plhs);
          break;
        }

       case 21:
        {
          extern void sf_c21_greenOffence_get_check_sum(mxArray *plhs[]);
          sf_c21_greenOffence_get_check_sum(plhs);
          break;
        }

       case 22:
        {
          extern void sf_c22_greenOffence_get_check_sum(mxArray *plhs[]);
          sf_c22_greenOffence_get_check_sum(plhs);
          break;
        }

       default:
        ((real_T *)mxGetPr((plhs[0])))[0] = (real_T)(0.0);
        ((real_T *)mxGetPr((plhs[0])))[1] = (real_T)(0.0);
        ((real_T *)mxGetPr((plhs[0])))[2] = (real_T)(0.0);
        ((real_T *)mxGetPr((plhs[0])))[3] = (real_T)(0.0);
      }
    } else if (!strcmp(commandName,"target")) {
      ((real_T *)mxGetPr((plhs[0])))[0] = (real_T)(3156344859U);
      ((real_T *)mxGetPr((plhs[0])))[1] = (real_T)(1654663603U);
      ((real_T *)mxGetPr((plhs[0])))[2] = (real_T)(3752591294U);
      ((real_T *)mxGetPr((plhs[0])))[3] = (real_T)(3933792561U);
    } else {
      return 0;
    }
  } else {
    ((real_T *)mxGetPr((plhs[0])))[0] = (real_T)(2324025314U);
    ((real_T *)mxGetPr((plhs[0])))[1] = (real_T)(3387807537U);
    ((real_T *)mxGetPr((plhs[0])))[2] = (real_T)(378642313U);
    ((real_T *)mxGetPr((plhs[0])))[3] = (real_T)(957450610U);
  }

  return 1;

#else

  return 0;

#endif

}

unsigned int sf_greenOffence_autoinheritance_info( int nlhs, mxArray * plhs[],
  int nrhs, const mxArray * prhs[] )
{

#ifdef MATLAB_MEX_FILE

  char commandName[32];
  char aiChksum[64];
  if (nrhs<3 || !mxIsChar(prhs[0]) )
    return 0;

  /* Possible call to get the autoinheritance_info */
  mxGetString(prhs[0], commandName,sizeof(commandName)/sizeof(char));
  commandName[(sizeof(commandName)/sizeof(char)-1)] = '\0';
  if (strcmp(commandName,"get_autoinheritance_info"))
    return 0;
  mxGetString(prhs[2], aiChksum,sizeof(aiChksum)/sizeof(char));
  aiChksum[(sizeof(aiChksum)/sizeof(char)-1)] = '\0';

  {
    unsigned int chartFileNumber;
    chartFileNumber = (unsigned int)mxGetScalar(prhs[1]);
    switch (chartFileNumber) {
     case 1:
      {
        if (strcmp(aiChksum, "2QjzhirzPtnQRWlR1Ra4nD") == 0) {
          extern mxArray *sf_c1_greenOffence_get_autoinheritance_info(void);
          plhs[0] = sf_c1_greenOffence_get_autoinheritance_info();
          break;
        }

        plhs[0] = mxCreateDoubleMatrix(0,0,mxREAL);
        break;
      }

     case 3:
      {
        if (strcmp(aiChksum, "m8KCX8tJiLFcYQ41JS7Q3F") == 0) {
          extern mxArray *sf_c3_greenOffence_get_autoinheritance_info(void);
          plhs[0] = sf_c3_greenOffence_get_autoinheritance_info();
          break;
        }

        plhs[0] = mxCreateDoubleMatrix(0,0,mxREAL);
        break;
      }

     case 4:
      {
        if (strcmp(aiChksum, "SUDIisq8Y9RxqzTeE2bRVH") == 0) {
          extern mxArray *sf_c4_greenOffence_get_autoinheritance_info(void);
          plhs[0] = sf_c4_greenOffence_get_autoinheritance_info();
          break;
        }

        plhs[0] = mxCreateDoubleMatrix(0,0,mxREAL);
        break;
      }

     case 7:
      {
        if (strcmp(aiChksum, "PGnCf0kevaRCvDCyQOz4kE") == 0) {
          extern mxArray *sf_c7_greenOffence_get_autoinheritance_info(void);
          plhs[0] = sf_c7_greenOffence_get_autoinheritance_info();
          break;
        }

        plhs[0] = mxCreateDoubleMatrix(0,0,mxREAL);
        break;
      }

     case 9:
      {
        if (strcmp(aiChksum, "GBuTtYKxyARBCmkimosN") == 0) {
          extern mxArray *sf_c9_greenOffence_get_autoinheritance_info(void);
          plhs[0] = sf_c9_greenOffence_get_autoinheritance_info();
          break;
        }

        plhs[0] = mxCreateDoubleMatrix(0,0,mxREAL);
        break;
      }

     case 11:
      {
        if (strcmp(aiChksum, "76eNygp4jEVTHJw31gSFmF") == 0) {
          extern mxArray *sf_c11_greenOffence_get_autoinheritance_info(void);
          plhs[0] = sf_c11_greenOffence_get_autoinheritance_info();
          break;
        }

        plhs[0] = mxCreateDoubleMatrix(0,0,mxREAL);
        break;
      }

     case 13:
      {
        if (strcmp(aiChksum, "avMqIanoQY6XoECCReQu1D") == 0) {
          extern mxArray *sf_c13_greenOffence_get_autoinheritance_info(void);
          plhs[0] = sf_c13_greenOffence_get_autoinheritance_info();
          break;
        }

        plhs[0] = mxCreateDoubleMatrix(0,0,mxREAL);
        break;
      }

     case 20:
      {
        if (strcmp(aiChksum, "7ivw4cCNB5MBh41uVb3SPB") == 0) {
          extern mxArray *sf_c20_greenOffence_get_autoinheritance_info(void);
          plhs[0] = sf_c20_greenOffence_get_autoinheritance_info();
          break;
        }

        plhs[0] = mxCreateDoubleMatrix(0,0,mxREAL);
        break;
      }

     case 21:
      {
        if (strcmp(aiChksum, "aeIe7Vb6eDndSoISjztQnH") == 0) {
          extern mxArray *sf_c21_greenOffence_get_autoinheritance_info(void);
          plhs[0] = sf_c21_greenOffence_get_autoinheritance_info();
          break;
        }

        plhs[0] = mxCreateDoubleMatrix(0,0,mxREAL);
        break;
      }

     case 22:
      {
        if (strcmp(aiChksum, "xTDZbH0g8R2TeJ7jn9aNrF") == 0) {
          extern mxArray *sf_c22_greenOffence_get_autoinheritance_info(void);
          plhs[0] = sf_c22_greenOffence_get_autoinheritance_info();
          break;
        }

        plhs[0] = mxCreateDoubleMatrix(0,0,mxREAL);
        break;
      }

     default:
      plhs[0] = mxCreateDoubleMatrix(0,0,mxREAL);
    }
  }

  return 1;

#else

  return 0;

#endif

}

unsigned int sf_greenOffence_get_eml_resolved_functions_info( int nlhs, mxArray *
  plhs[], int nrhs, const mxArray * prhs[] )
{

#ifdef MATLAB_MEX_FILE

  char commandName[64];
  if (nrhs<2 || !mxIsChar(prhs[0]))
    return 0;

  /* Possible call to get the get_eml_resolved_functions_info */
  mxGetString(prhs[0], commandName,sizeof(commandName)/sizeof(char));
  commandName[(sizeof(commandName)/sizeof(char)-1)] = '\0';
  if (strcmp(commandName,"get_eml_resolved_functions_info"))
    return 0;

  {
    unsigned int chartFileNumber;
    chartFileNumber = (unsigned int)mxGetScalar(prhs[1]);
    switch (chartFileNumber) {
     case 1:
      {
        extern const mxArray *sf_c1_greenOffence_get_eml_resolved_functions_info
          (void);
        mxArray *persistentMxArray = (mxArray *)
          sf_c1_greenOffence_get_eml_resolved_functions_info();
        plhs[0] = mxDuplicateArray(persistentMxArray);
        mxDestroyArray(persistentMxArray);
        break;
      }

     case 3:
      {
        extern const mxArray *sf_c3_greenOffence_get_eml_resolved_functions_info
          (void);
        mxArray *persistentMxArray = (mxArray *)
          sf_c3_greenOffence_get_eml_resolved_functions_info();
        plhs[0] = mxDuplicateArray(persistentMxArray);
        mxDestroyArray(persistentMxArray);
        break;
      }

     case 4:
      {
        extern const mxArray *sf_c4_greenOffence_get_eml_resolved_functions_info
          (void);
        mxArray *persistentMxArray = (mxArray *)
          sf_c4_greenOffence_get_eml_resolved_functions_info();
        plhs[0] = mxDuplicateArray(persistentMxArray);
        mxDestroyArray(persistentMxArray);
        break;
      }

     case 7:
      {
        extern const mxArray *sf_c7_greenOffence_get_eml_resolved_functions_info
          (void);
        mxArray *persistentMxArray = (mxArray *)
          sf_c7_greenOffence_get_eml_resolved_functions_info();
        plhs[0] = mxDuplicateArray(persistentMxArray);
        mxDestroyArray(persistentMxArray);
        break;
      }

     case 9:
      {
        extern const mxArray *sf_c9_greenOffence_get_eml_resolved_functions_info
          (void);
        mxArray *persistentMxArray = (mxArray *)
          sf_c9_greenOffence_get_eml_resolved_functions_info();
        plhs[0] = mxDuplicateArray(persistentMxArray);
        mxDestroyArray(persistentMxArray);
        break;
      }

     case 11:
      {
        extern const mxArray
          *sf_c11_greenOffence_get_eml_resolved_functions_info(void);
        mxArray *persistentMxArray = (mxArray *)
          sf_c11_greenOffence_get_eml_resolved_functions_info();
        plhs[0] = mxDuplicateArray(persistentMxArray);
        mxDestroyArray(persistentMxArray);
        break;
      }

     case 13:
      {
        extern const mxArray
          *sf_c13_greenOffence_get_eml_resolved_functions_info(void);
        mxArray *persistentMxArray = (mxArray *)
          sf_c13_greenOffence_get_eml_resolved_functions_info();
        plhs[0] = mxDuplicateArray(persistentMxArray);
        mxDestroyArray(persistentMxArray);
        break;
      }

     case 20:
      {
        extern const mxArray
          *sf_c20_greenOffence_get_eml_resolved_functions_info(void);
        mxArray *persistentMxArray = (mxArray *)
          sf_c20_greenOffence_get_eml_resolved_functions_info();
        plhs[0] = mxDuplicateArray(persistentMxArray);
        mxDestroyArray(persistentMxArray);
        break;
      }

     case 21:
      {
        extern const mxArray
          *sf_c21_greenOffence_get_eml_resolved_functions_info(void);
        mxArray *persistentMxArray = (mxArray *)
          sf_c21_greenOffence_get_eml_resolved_functions_info();
        plhs[0] = mxDuplicateArray(persistentMxArray);
        mxDestroyArray(persistentMxArray);
        break;
      }

     case 22:
      {
        extern const mxArray
          *sf_c22_greenOffence_get_eml_resolved_functions_info(void);
        mxArray *persistentMxArray = (mxArray *)
          sf_c22_greenOffence_get_eml_resolved_functions_info();
        plhs[0] = mxDuplicateArray(persistentMxArray);
        mxDestroyArray(persistentMxArray);
        break;
      }

     default:
      plhs[0] = mxCreateDoubleMatrix(0,0,mxREAL);
    }
  }

  return 1;

#else

  return 0;

#endif

}

unsigned int sf_greenOffence_third_party_uses_info( int nlhs, mxArray * plhs[],
  int nrhs, const mxArray * prhs[] )
{
  char commandName[64];
  char tpChksum[64];
  if (nrhs<3 || !mxIsChar(prhs[0]))
    return 0;

  /* Possible call to get the third_party_uses_info */
  mxGetString(prhs[0], commandName,sizeof(commandName)/sizeof(char));
  commandName[(sizeof(commandName)/sizeof(char)-1)] = '\0';
  mxGetString(prhs[2], tpChksum,sizeof(tpChksum)/sizeof(char));
  tpChksum[(sizeof(tpChksum)/sizeof(char)-1)] = '\0';
  if (strcmp(commandName,"get_third_party_uses_info"))
    return 0;

  {
    unsigned int chartFileNumber;
    chartFileNumber = (unsigned int)mxGetScalar(prhs[1]);
    switch (chartFileNumber) {
     case 1:
      {
        if (strcmp(tpChksum, "vd7fjcbJSEMNbYnkMPt13F") == 0) {
          extern mxArray *sf_c1_greenOffence_third_party_uses_info(void);
          plhs[0] = sf_c1_greenOffence_third_party_uses_info();
          break;
        }
      }

     case 3:
      {
        if (strcmp(tpChksum, "Gi1CIpFuvJp5IiBMn2aNOF") == 0) {
          extern mxArray *sf_c3_greenOffence_third_party_uses_info(void);
          plhs[0] = sf_c3_greenOffence_third_party_uses_info();
          break;
        }
      }

     case 4:
      {
        if (strcmp(tpChksum, "EDablARDq1v2FyRCqcESNE") == 0) {
          extern mxArray *sf_c4_greenOffence_third_party_uses_info(void);
          plhs[0] = sf_c4_greenOffence_third_party_uses_info();
          break;
        }
      }

     case 7:
      {
        if (strcmp(tpChksum, "d1cYeVpPJg74IwzYxoZF7C") == 0) {
          extern mxArray *sf_c7_greenOffence_third_party_uses_info(void);
          plhs[0] = sf_c7_greenOffence_third_party_uses_info();
          break;
        }
      }

     case 9:
      {
        if (strcmp(tpChksum, "yqxI9GpGlXtwNXzunjTnYF") == 0) {
          extern mxArray *sf_c9_greenOffence_third_party_uses_info(void);
          plhs[0] = sf_c9_greenOffence_third_party_uses_info();
          break;
        }
      }

     case 11:
      {
        if (strcmp(tpChksum, "UeEZVXP7ULvtMMsjsCISDC") == 0) {
          extern mxArray *sf_c11_greenOffence_third_party_uses_info(void);
          plhs[0] = sf_c11_greenOffence_third_party_uses_info();
          break;
        }
      }

     case 13:
      {
        if (strcmp(tpChksum, "ALspH8dkusiL2EbZYOGTgC") == 0) {
          extern mxArray *sf_c13_greenOffence_third_party_uses_info(void);
          plhs[0] = sf_c13_greenOffence_third_party_uses_info();
          break;
        }
      }

     case 20:
      {
        if (strcmp(tpChksum, "LeLWtRVKLBNMKWUDzAAoEG") == 0) {
          extern mxArray *sf_c20_greenOffence_third_party_uses_info(void);
          plhs[0] = sf_c20_greenOffence_third_party_uses_info();
          break;
        }
      }

     case 21:
      {
        if (strcmp(tpChksum, "PNWzkAtCPHS0tn5KDU2ID") == 0) {
          extern mxArray *sf_c21_greenOffence_third_party_uses_info(void);
          plhs[0] = sf_c21_greenOffence_third_party_uses_info();
          break;
        }
      }

     case 22:
      {
        if (strcmp(tpChksum, "eBok9WE1AMgYXkMTR8HnkB") == 0) {
          extern mxArray *sf_c22_greenOffence_third_party_uses_info(void);
          plhs[0] = sf_c22_greenOffence_third_party_uses_info();
          break;
        }
      }

     default:
      plhs[0] = mxCreateDoubleMatrix(0,0,mxREAL);
    }
  }

  return 1;
}

unsigned int sf_greenOffence_jit_fallback_info( int nlhs, mxArray * plhs[], int
  nrhs, const mxArray * prhs[] )
{
  char commandName[64];
  char tpChksum[64];
  if (nrhs<3 || !mxIsChar(prhs[0]))
    return 0;

  /* Possible call to get the jit_fallback_info */
  mxGetString(prhs[0], commandName,sizeof(commandName)/sizeof(char));
  commandName[(sizeof(commandName)/sizeof(char)-1)] = '\0';
  mxGetString(prhs[2], tpChksum,sizeof(tpChksum)/sizeof(char));
  tpChksum[(sizeof(tpChksum)/sizeof(char)-1)] = '\0';
  if (strcmp(commandName,"get_jit_fallback_info"))
    return 0;

  {
    unsigned int chartFileNumber;
    chartFileNumber = (unsigned int)mxGetScalar(prhs[1]);
    switch (chartFileNumber) {
     case 1:
      {
        if (strcmp(tpChksum, "vd7fjcbJSEMNbYnkMPt13F") == 0) {
          extern mxArray *sf_c1_greenOffence_jit_fallback_info(void);
          plhs[0] = sf_c1_greenOffence_jit_fallback_info();
          break;
        }
      }

     case 3:
      {
        if (strcmp(tpChksum, "Gi1CIpFuvJp5IiBMn2aNOF") == 0) {
          extern mxArray *sf_c3_greenOffence_jit_fallback_info(void);
          plhs[0] = sf_c3_greenOffence_jit_fallback_info();
          break;
        }
      }

     case 4:
      {
        if (strcmp(tpChksum, "EDablARDq1v2FyRCqcESNE") == 0) {
          extern mxArray *sf_c4_greenOffence_jit_fallback_info(void);
          plhs[0] = sf_c4_greenOffence_jit_fallback_info();
          break;
        }
      }

     case 7:
      {
        if (strcmp(tpChksum, "d1cYeVpPJg74IwzYxoZF7C") == 0) {
          extern mxArray *sf_c7_greenOffence_jit_fallback_info(void);
          plhs[0] = sf_c7_greenOffence_jit_fallback_info();
          break;
        }
      }

     case 9:
      {
        if (strcmp(tpChksum, "yqxI9GpGlXtwNXzunjTnYF") == 0) {
          extern mxArray *sf_c9_greenOffence_jit_fallback_info(void);
          plhs[0] = sf_c9_greenOffence_jit_fallback_info();
          break;
        }
      }

     case 11:
      {
        if (strcmp(tpChksum, "UeEZVXP7ULvtMMsjsCISDC") == 0) {
          extern mxArray *sf_c11_greenOffence_jit_fallback_info(void);
          plhs[0] = sf_c11_greenOffence_jit_fallback_info();
          break;
        }
      }

     case 13:
      {
        if (strcmp(tpChksum, "ALspH8dkusiL2EbZYOGTgC") == 0) {
          extern mxArray *sf_c13_greenOffence_jit_fallback_info(void);
          plhs[0] = sf_c13_greenOffence_jit_fallback_info();
          break;
        }
      }

     case 20:
      {
        if (strcmp(tpChksum, "LeLWtRVKLBNMKWUDzAAoEG") == 0) {
          extern mxArray *sf_c20_greenOffence_jit_fallback_info(void);
          plhs[0] = sf_c20_greenOffence_jit_fallback_info();
          break;
        }
      }

     case 21:
      {
        if (strcmp(tpChksum, "PNWzkAtCPHS0tn5KDU2ID") == 0) {
          extern mxArray *sf_c21_greenOffence_jit_fallback_info(void);
          plhs[0] = sf_c21_greenOffence_jit_fallback_info();
          break;
        }
      }

     case 22:
      {
        if (strcmp(tpChksum, "eBok9WE1AMgYXkMTR8HnkB") == 0) {
          extern mxArray *sf_c22_greenOffence_jit_fallback_info(void);
          plhs[0] = sf_c22_greenOffence_jit_fallback_info();
          break;
        }
      }

     default:
      plhs[0] = mxCreateDoubleMatrix(0,0,mxREAL);
    }
  }

  return 1;
}

unsigned int sf_greenOffence_updateBuildInfo_args_info( int nlhs, mxArray *
  plhs[], int nrhs, const mxArray * prhs[] )
{
  char commandName[64];
  char tpChksum[64];
  if (nrhs<3 || !mxIsChar(prhs[0]))
    return 0;

  /* Possible call to get the updateBuildInfo_args_info */
  mxGetString(prhs[0], commandName,sizeof(commandName)/sizeof(char));
  commandName[(sizeof(commandName)/sizeof(char)-1)] = '\0';
  mxGetString(prhs[2], tpChksum,sizeof(tpChksum)/sizeof(char));
  tpChksum[(sizeof(tpChksum)/sizeof(char)-1)] = '\0';
  if (strcmp(commandName,"get_updateBuildInfo_args_info"))
    return 0;

  {
    unsigned int chartFileNumber;
    chartFileNumber = (unsigned int)mxGetScalar(prhs[1]);
    switch (chartFileNumber) {
     case 1:
      {
        if (strcmp(tpChksum, "vd7fjcbJSEMNbYnkMPt13F") == 0) {
          extern mxArray *sf_c1_greenOffence_updateBuildInfo_args_info(void);
          plhs[0] = sf_c1_greenOffence_updateBuildInfo_args_info();
          break;
        }
      }

     case 3:
      {
        if (strcmp(tpChksum, "Gi1CIpFuvJp5IiBMn2aNOF") == 0) {
          extern mxArray *sf_c3_greenOffence_updateBuildInfo_args_info(void);
          plhs[0] = sf_c3_greenOffence_updateBuildInfo_args_info();
          break;
        }
      }

     case 4:
      {
        if (strcmp(tpChksum, "EDablARDq1v2FyRCqcESNE") == 0) {
          extern mxArray *sf_c4_greenOffence_updateBuildInfo_args_info(void);
          plhs[0] = sf_c4_greenOffence_updateBuildInfo_args_info();
          break;
        }
      }

     case 7:
      {
        if (strcmp(tpChksum, "d1cYeVpPJg74IwzYxoZF7C") == 0) {
          extern mxArray *sf_c7_greenOffence_updateBuildInfo_args_info(void);
          plhs[0] = sf_c7_greenOffence_updateBuildInfo_args_info();
          break;
        }
      }

     case 9:
      {
        if (strcmp(tpChksum, "yqxI9GpGlXtwNXzunjTnYF") == 0) {
          extern mxArray *sf_c9_greenOffence_updateBuildInfo_args_info(void);
          plhs[0] = sf_c9_greenOffence_updateBuildInfo_args_info();
          break;
        }
      }

     case 11:
      {
        if (strcmp(tpChksum, "UeEZVXP7ULvtMMsjsCISDC") == 0) {
          extern mxArray *sf_c11_greenOffence_updateBuildInfo_args_info(void);
          plhs[0] = sf_c11_greenOffence_updateBuildInfo_args_info();
          break;
        }
      }

     case 13:
      {
        if (strcmp(tpChksum, "ALspH8dkusiL2EbZYOGTgC") == 0) {
          extern mxArray *sf_c13_greenOffence_updateBuildInfo_args_info(void);
          plhs[0] = sf_c13_greenOffence_updateBuildInfo_args_info();
          break;
        }
      }

     case 20:
      {
        if (strcmp(tpChksum, "LeLWtRVKLBNMKWUDzAAoEG") == 0) {
          extern mxArray *sf_c20_greenOffence_updateBuildInfo_args_info(void);
          plhs[0] = sf_c20_greenOffence_updateBuildInfo_args_info();
          break;
        }
      }

     case 21:
      {
        if (strcmp(tpChksum, "PNWzkAtCPHS0tn5KDU2ID") == 0) {
          extern mxArray *sf_c21_greenOffence_updateBuildInfo_args_info(void);
          plhs[0] = sf_c21_greenOffence_updateBuildInfo_args_info();
          break;
        }
      }

     case 22:
      {
        if (strcmp(tpChksum, "eBok9WE1AMgYXkMTR8HnkB") == 0) {
          extern mxArray *sf_c22_greenOffence_updateBuildInfo_args_info(void);
          plhs[0] = sf_c22_greenOffence_updateBuildInfo_args_info();
          break;
        }
      }

     default:
      plhs[0] = mxCreateDoubleMatrix(0,0,mxREAL);
    }
  }

  return 1;
}

void sf_greenOffence_get_post_codegen_info( int nlhs, mxArray * plhs[], int nrhs,
  const mxArray * prhs[] )
{
  unsigned int chartFileNumber = (unsigned int) mxGetScalar(prhs[0]);
  char tpChksum[64];
  mxGetString(prhs[1], tpChksum,sizeof(tpChksum)/sizeof(char));
  tpChksum[(sizeof(tpChksum)/sizeof(char)-1)] = '\0';
  switch (chartFileNumber) {
   case 1:
    {
      if (strcmp(tpChksum, "vd7fjcbJSEMNbYnkMPt13F") == 0) {
        extern mxArray *sf_c1_greenOffence_get_post_codegen_info(void);
        plhs[0] = sf_c1_greenOffence_get_post_codegen_info();
        return;
      }
    }
    break;

   case 3:
    {
      if (strcmp(tpChksum, "Gi1CIpFuvJp5IiBMn2aNOF") == 0) {
        extern mxArray *sf_c3_greenOffence_get_post_codegen_info(void);
        plhs[0] = sf_c3_greenOffence_get_post_codegen_info();
        return;
      }
    }
    break;

   case 4:
    {
      if (strcmp(tpChksum, "EDablARDq1v2FyRCqcESNE") == 0) {
        extern mxArray *sf_c4_greenOffence_get_post_codegen_info(void);
        plhs[0] = sf_c4_greenOffence_get_post_codegen_info();
        return;
      }
    }
    break;

   case 7:
    {
      if (strcmp(tpChksum, "d1cYeVpPJg74IwzYxoZF7C") == 0) {
        extern mxArray *sf_c7_greenOffence_get_post_codegen_info(void);
        plhs[0] = sf_c7_greenOffence_get_post_codegen_info();
        return;
      }
    }
    break;

   case 9:
    {
      if (strcmp(tpChksum, "yqxI9GpGlXtwNXzunjTnYF") == 0) {
        extern mxArray *sf_c9_greenOffence_get_post_codegen_info(void);
        plhs[0] = sf_c9_greenOffence_get_post_codegen_info();
        return;
      }
    }
    break;

   case 11:
    {
      if (strcmp(tpChksum, "UeEZVXP7ULvtMMsjsCISDC") == 0) {
        extern mxArray *sf_c11_greenOffence_get_post_codegen_info(void);
        plhs[0] = sf_c11_greenOffence_get_post_codegen_info();
        return;
      }
    }
    break;

   case 13:
    {
      if (strcmp(tpChksum, "ALspH8dkusiL2EbZYOGTgC") == 0) {
        extern mxArray *sf_c13_greenOffence_get_post_codegen_info(void);
        plhs[0] = sf_c13_greenOffence_get_post_codegen_info();
        return;
      }
    }
    break;

   case 20:
    {
      if (strcmp(tpChksum, "LeLWtRVKLBNMKWUDzAAoEG") == 0) {
        extern mxArray *sf_c20_greenOffence_get_post_codegen_info(void);
        plhs[0] = sf_c20_greenOffence_get_post_codegen_info();
        return;
      }
    }
    break;

   case 21:
    {
      if (strcmp(tpChksum, "PNWzkAtCPHS0tn5KDU2ID") == 0) {
        extern mxArray *sf_c21_greenOffence_get_post_codegen_info(void);
        plhs[0] = sf_c21_greenOffence_get_post_codegen_info();
        return;
      }
    }
    break;

   case 22:
    {
      if (strcmp(tpChksum, "eBok9WE1AMgYXkMTR8HnkB") == 0) {
        extern mxArray *sf_c22_greenOffence_get_post_codegen_info(void);
        plhs[0] = sf_c22_greenOffence_get_post_codegen_info();
        return;
      }
    }
    break;

   default:
    break;
  }

  plhs[0] = mxCreateDoubleMatrix(0,0,mxREAL);
}

void greenOffence_debug_initialize(struct SfDebugInstanceStruct* debugInstance)
{
  _greenOffenceMachineNumber_ = sf_debug_initialize_machine(debugInstance,
    "greenOffence","sfun",0,10,0,0,0);
  sf_debug_set_machine_event_thresholds(debugInstance,
    _greenOffenceMachineNumber_,0,0);
  sf_debug_set_machine_data_thresholds(debugInstance,_greenOffenceMachineNumber_,
    0);
}

void greenOffence_register_exported_symbols(SimStruct* S)
{
}

static mxArray* sRtwOptimizationInfoStruct= NULL;
mxArray* load_greenOffence_optimization_info(void)
{
  if (sRtwOptimizationInfoStruct==NULL) {
    sRtwOptimizationInfoStruct = sf_load_rtw_optimization_info("greenOffence",
      "greenOffence");
    mexMakeArrayPersistent(sRtwOptimizationInfoStruct);
  }

  return(sRtwOptimizationInfoStruct);
}

void unload_greenOffence_optimization_info(void)
{
  if (sRtwOptimizationInfoStruct!=NULL) {
    mxDestroyArray(sRtwOptimizationInfoStruct);
    sRtwOptimizationInfoStruct = NULL;
  }
}

/* Include files */

#include <stddef.h>
#include "blas.h"
#include "greenOffence_sfun.h"
#include "c11_greenOffence.h"
#include "mwmathutil.h"
#define CHARTINSTANCE_CHARTNUMBER      (chartInstance->chartNumber)
#define CHARTINSTANCE_INSTANCENUMBER   (chartInstance->instanceNumber)
#include "greenOffence_sfun_debug_macros.h"
#define _SF_MEX_LISTEN_FOR_CTRL_C(S)   sf_mex_listen_for_ctrl_c(sfGlobalDebugInstanceStruct,S);

/* Type Definitions */

/* Named Constants */
#define CALL_EVENT                     (-1)
#define c11_IN_NO_ACTIVE_CHILD         ((uint8_T)0U)
#define c11_IN_GameIsOn_Goalie         ((uint8_T)1U)
#define c11_IN_waiting                 ((uint8_T)2U)
#define c11_IN_Idle                    ((uint8_T)1U)
#define c11_IN_goalie                  ((uint8_T)2U)
#define c11_IN_Idle1                   ((uint8_T)1U)

/* Variable Declarations */

/* Variable Definitions */
static real_T _sfTime_;
static const char * c11_debug_family_names[2] = { "nargin", "nargout" };

static const char * c11_b_debug_family_names[2] = { "nargin", "nargout" };

static const char * c11_c_debug_family_names[2] = { "nargin", "nargout" };

static const char * c11_d_debug_family_names[2] = { "nargin", "nargout" };

static const char * c11_e_debug_family_names[2] = { "nargin", "nargout" };

static const char * c11_f_debug_family_names[3] = { "nargin", "nargout",
  "sf_internal_predicateOutput" };

static const char * c11_g_debug_family_names[5] = { "nargin", "nargout", "pos",
  "tol", "posReached" };

static const char * c11_h_debug_family_names[3] = { "nargin", "nargout",
  "sf_internal_predicateOutput" };

static const char * c11_i_debug_family_names[2] = { "nargin", "nargout" };

static const char * c11_j_debug_family_names[3] = { "nargin", "nargout",
  "sf_internal_predicateOutput" };

static const char * c11_k_debug_family_names[3] = { "nargin", "nargout",
  "sf_internal_predicateOutput" };

static const char * c11_l_debug_family_names[3] = { "nargin", "nargout",
  "sf_internal_predicateOutput" };

static const char * c11_m_debug_family_names[3] = { "nargin", "nargout",
  "sf_internal_predicateOutput" };

static const char * c11_n_debug_family_names[3] = { "nargin", "nargout",
  "sf_internal_predicateOutput" };

static const char * c11_o_debug_family_names[3] = { "nargin", "nargout",
  "sf_internal_predicateOutput" };

/* Function Declarations */
static void initialize_c11_greenOffence(SFc11_greenOffenceInstanceStruct
  *chartInstance);
static void initialize_params_c11_greenOffence(SFc11_greenOffenceInstanceStruct *
  chartInstance);
static void enable_c11_greenOffence(SFc11_greenOffenceInstanceStruct
  *chartInstance);
static void disable_c11_greenOffence(SFc11_greenOffenceInstanceStruct
  *chartInstance);
static void c11_update_debugger_state_c11_greenOffence
  (SFc11_greenOffenceInstanceStruct *chartInstance);
static const mxArray *get_sim_state_c11_greenOffence
  (SFc11_greenOffenceInstanceStruct *chartInstance);
static void set_sim_state_c11_greenOffence(SFc11_greenOffenceInstanceStruct
  *chartInstance, const mxArray *c11_st);
static void c11_set_sim_state_side_effects_c11_greenOffence
  (SFc11_greenOffenceInstanceStruct *chartInstance);
static void finalize_c11_greenOffence(SFc11_greenOffenceInstanceStruct
  *chartInstance);
static void sf_gateway_c11_greenOffence(SFc11_greenOffenceInstanceStruct
  *chartInstance);
static void mdl_start_c11_greenOffence(SFc11_greenOffenceInstanceStruct
  *chartInstance);
static void c11_chartstep_c11_greenOffence(SFc11_greenOffenceInstanceStruct
  *chartInstance);
static void initSimStructsc11_greenOffence(SFc11_greenOffenceInstanceStruct
  *chartInstance);
static void c11_exit_internal_GameIsOn_Goalie(SFc11_greenOffenceInstanceStruct
  *chartInstance);
static int16_T c11_intmax(SFc11_greenOffenceInstanceStruct *chartInstance);
static real32_T c11_eml_xnrm2(SFc11_greenOffenceInstanceStruct *chartInstance,
  real32_T c11_x[2]);
static void c11_below_threshold(SFc11_greenOffenceInstanceStruct *chartInstance);
static void init_script_number_translation(uint32_T c11_machineNumber, uint32_T
  c11_chartNumber, uint32_T c11_instanceNumber);
static const mxArray *c11_sf_marshallOut(void *chartInstanceVoid, void
  *c11_inData);
static real_T c11_emlrt_marshallIn(SFc11_greenOffenceInstanceStruct
  *chartInstance, const mxArray *c11_u, const emlrtMsgIdentifier *c11_parentId);
static void c11_sf_marshallIn(void *chartInstanceVoid, const mxArray
  *c11_mxArrayInData, const char_T *c11_varName, void *c11_outData);
static const mxArray *c11_b_sf_marshallOut(void *chartInstanceVoid, void
  *c11_inData);
static boolean_T c11_b_emlrt_marshallIn(SFc11_greenOffenceInstanceStruct
  *chartInstance, const mxArray *c11_u, const emlrtMsgIdentifier *c11_parentId);
static void c11_b_sf_marshallIn(void *chartInstanceVoid, const mxArray
  *c11_mxArrayInData, const char_T *c11_varName, void *c11_outData);
static const mxArray *c11_c_sf_marshallOut(void *chartInstanceVoid, void
  *c11_inData);
static uint8_T c11_c_emlrt_marshallIn(SFc11_greenOffenceInstanceStruct
  *chartInstance, const mxArray *c11_posReached, const char_T *c11_identifier);
static uint8_T c11_d_emlrt_marshallIn(SFc11_greenOffenceInstanceStruct
  *chartInstance, const mxArray *c11_u, const emlrtMsgIdentifier *c11_parentId);
static void c11_c_sf_marshallIn(void *chartInstanceVoid, const mxArray
  *c11_mxArrayInData, const char_T *c11_varName, void *c11_outData);
static const mxArray *c11_d_sf_marshallOut(void *chartInstanceVoid, void
  *c11_inData);
static void c11_e_emlrt_marshallIn(SFc11_greenOffenceInstanceStruct
  *chartInstance, const mxArray *c11_u, const emlrtMsgIdentifier *c11_parentId,
  int8_T c11_y[2]);
static void c11_d_sf_marshallIn(void *chartInstanceVoid, const mxArray
  *c11_mxArrayInData, const char_T *c11_varName, void *c11_outData);
static void c11_info_helper(const mxArray **c11_info);
static const mxArray *c11_emlrt_marshallOut(const char * c11_u);
static const mxArray *c11_b_emlrt_marshallOut(const uint32_T c11_u);
static void c11_calcStartPos(SFc11_greenOffenceInstanceStruct *chartInstance);
static uint8_T c11_checkReached(SFc11_greenOffenceInstanceStruct *chartInstance,
  int8_T c11_pos[2], real_T c11_tol);
static const mxArray *c11_e_sf_marshallOut(void *chartInstanceVoid, void
  *c11_inData);
static int32_T c11_f_emlrt_marshallIn(SFc11_greenOffenceInstanceStruct
  *chartInstance, const mxArray *c11_u, const emlrtMsgIdentifier *c11_parentId);
static void c11_e_sf_marshallIn(void *chartInstanceVoid, const mxArray
  *c11_mxArrayInData, const char_T *c11_varName, void *c11_outData);
static const mxArray *c11_me_bus_io(void *chartInstanceVoid, void *c11_pData);
static const mxArray *c11_f_sf_marshallOut(void *chartInstanceVoid, void
  *c11_inData);
static const mxArray *c11_players_bus_io(void *chartInstanceVoid, void
  *c11_pData);
static const mxArray *c11_g_sf_marshallOut(void *chartInstanceVoid, void
  *c11_inData);
static const mxArray *c11_ball_bus_io(void *chartInstanceVoid, void *c11_pData);
static const mxArray *c11_h_sf_marshallOut(void *chartInstanceVoid, void
  *c11_inData);
static const mxArray *c11_finalWay_bus_io(void *chartInstanceVoid, void
  *c11_pData);
static const mxArray *c11_i_sf_marshallOut(void *chartInstanceVoid, void
  *c11_inData);
static c11_Waypoint c11_g_emlrt_marshallIn(SFc11_greenOffenceInstanceStruct
  *chartInstance, const mxArray *c11_b_finalWay, const char_T *c11_identifier);
static c11_Waypoint c11_h_emlrt_marshallIn(SFc11_greenOffenceInstanceStruct
  *chartInstance, const mxArray *c11_u, const emlrtMsgIdentifier *c11_parentId);
static int8_T c11_i_emlrt_marshallIn(SFc11_greenOffenceInstanceStruct
  *chartInstance, const mxArray *c11_u, const emlrtMsgIdentifier *c11_parentId);
static int16_T c11_j_emlrt_marshallIn(SFc11_greenOffenceInstanceStruct
  *chartInstance, const mxArray *c11_u, const emlrtMsgIdentifier *c11_parentId);
static void c11_f_sf_marshallIn(void *chartInstanceVoid, const mxArray
  *c11_mxArrayInData, const char_T *c11_varName, void *c11_outData);
static const mxArray *c11_j_sf_marshallOut(void *chartInstanceVoid, void
  *c11_inData);
static void c11_k_emlrt_marshallIn(SFc11_greenOffenceInstanceStruct
  *chartInstance, const mxArray *c11_b_startingPos, const char_T *c11_identifier,
  int8_T c11_y[2]);
static void c11_l_emlrt_marshallIn(SFc11_greenOffenceInstanceStruct
  *chartInstance, const mxArray *c11_u, const emlrtMsgIdentifier *c11_parentId,
  int8_T c11_y[2]);
static void c11_g_sf_marshallIn(void *chartInstanceVoid, const mxArray
  *c11_mxArrayInData, const char_T *c11_varName, void *c11_outData);
static void c11_m_emlrt_marshallIn(SFc11_greenOffenceInstanceStruct
  *chartInstance, const mxArray *c11_b_dataWrittenToVector, const char_T
  *c11_identifier, boolean_T c11_y[3]);
static void c11_n_emlrt_marshallIn(SFc11_greenOffenceInstanceStruct
  *chartInstance, const mxArray *c11_u, const emlrtMsgIdentifier *c11_parentId,
  boolean_T c11_y[3]);
static const mxArray *c11_o_emlrt_marshallIn(SFc11_greenOffenceInstanceStruct
  *chartInstance, const mxArray *c11_b_setSimStateSideEffectsInfo, const char_T *
  c11_identifier);
static const mxArray *c11_p_emlrt_marshallIn(SFc11_greenOffenceInstanceStruct
  *chartInstance, const mxArray *c11_u, const emlrtMsgIdentifier *c11_parentId);
static void c11_updateDataWrittenToVector(SFc11_greenOffenceInstanceStruct
  *chartInstance, uint32_T c11_vectorIndex);
static void c11_errorIfDataNotWrittenToFcn(SFc11_greenOffenceInstanceStruct
  *chartInstance, uint32_T c11_vectorIndex, uint32_T c11_dataNumber, uint32_T
  c11_ssIdOfSourceObject, int32_T c11_offsetInSourceObject, int32_T
  c11_lengthInSourceObject);
static void init_dsm_address_info(SFc11_greenOffenceInstanceStruct
  *chartInstance);
static void init_simulink_io_address(SFc11_greenOffenceInstanceStruct
  *chartInstance);

/* Function Definitions */
static void initialize_c11_greenOffence(SFc11_greenOffenceInstanceStruct
  *chartInstance)
{
  chartInstance->c11_sfEvent = CALL_EVENT;
  _sfTime_ = sf_get_time(chartInstance->S);
  chartInstance->c11_doSetSimStateSideEffects = 0U;
  chartInstance->c11_setSimStateSideEffectsInfo = NULL;
  chartInstance->c11_is_GameIsOn_Goalie = c11_IN_NO_ACTIVE_CHILD;
  chartInstance->c11_tp_GameIsOn_Goalie = 0U;
  chartInstance->c11_tp_Idle = 0U;
  chartInstance->c11_tp_goalie = 0U;
  chartInstance->c11_is_waiting = c11_IN_NO_ACTIVE_CHILD;
  chartInstance->c11_tp_waiting = 0U;
  chartInstance->c11_tp_Idle1 = 0U;
  chartInstance->c11_is_active_c11_greenOffence = 0U;
  chartInstance->c11_is_c11_greenOffence = c11_IN_NO_ACTIVE_CHILD;
}

static void initialize_params_c11_greenOffence(SFc11_greenOffenceInstanceStruct *
  chartInstance)
{
  (void)chartInstance;
}

static void enable_c11_greenOffence(SFc11_greenOffenceInstanceStruct
  *chartInstance)
{
  _sfTime_ = sf_get_time(chartInstance->S);
}

static void disable_c11_greenOffence(SFc11_greenOffenceInstanceStruct
  *chartInstance)
{
  _sfTime_ = sf_get_time(chartInstance->S);
}

static void c11_update_debugger_state_c11_greenOffence
  (SFc11_greenOffenceInstanceStruct *chartInstance)
{
  uint32_T c11_prevAniVal;
  c11_prevAniVal = _SFD_GET_ANIMATION();
  _SFD_SET_ANIMATION(0U);
  _SFD_SET_HONOR_BREAKPOINTS(0U);
  if (chartInstance->c11_is_active_c11_greenOffence == 1U) {
    _SFD_CC_CALL(CHART_ACTIVE_TAG, 5U, chartInstance->c11_sfEvent);
  }

  if (chartInstance->c11_is_c11_greenOffence == c11_IN_GameIsOn_Goalie) {
    _SFD_CS_CALL(STATE_ACTIVE_TAG, 0U, chartInstance->c11_sfEvent);
  } else {
    _SFD_CS_CALL(STATE_INACTIVE_TAG, 0U, chartInstance->c11_sfEvent);
  }

  if (chartInstance->c11_is_GameIsOn_Goalie == c11_IN_goalie) {
    _SFD_CS_CALL(STATE_ACTIVE_TAG, 2U, chartInstance->c11_sfEvent);
  } else {
    _SFD_CS_CALL(STATE_INACTIVE_TAG, 2U, chartInstance->c11_sfEvent);
  }

  if (chartInstance->c11_is_GameIsOn_Goalie == c11_IN_Idle) {
    _SFD_CS_CALL(STATE_ACTIVE_TAG, 1U, chartInstance->c11_sfEvent);
  } else {
    _SFD_CS_CALL(STATE_INACTIVE_TAG, 1U, chartInstance->c11_sfEvent);
  }

  if (chartInstance->c11_is_c11_greenOffence == c11_IN_waiting) {
    _SFD_CS_CALL(STATE_ACTIVE_TAG, 5U, chartInstance->c11_sfEvent);
  } else {
    _SFD_CS_CALL(STATE_INACTIVE_TAG, 5U, chartInstance->c11_sfEvent);
  }

  if (chartInstance->c11_is_waiting == c11_IN_Idle1) {
    _SFD_CS_CALL(STATE_ACTIVE_TAG, 6U, chartInstance->c11_sfEvent);
  } else {
    _SFD_CS_CALL(STATE_INACTIVE_TAG, 6U, chartInstance->c11_sfEvent);
  }

  _SFD_SET_ANIMATION(c11_prevAniVal);
  _SFD_SET_HONOR_BREAKPOINTS(1U);
  _SFD_ANIMATE();
}

static const mxArray *get_sim_state_c11_greenOffence
  (SFc11_greenOffenceInstanceStruct *chartInstance)
{
  const mxArray *c11_st;
  const mxArray *c11_y = NULL;
  const mxArray *c11_b_y = NULL;
  int8_T c11_u;
  const mxArray *c11_c_y = NULL;
  int8_T c11_b_u;
  const mxArray *c11_d_y = NULL;
  int16_T c11_c_u;
  const mxArray *c11_e_y = NULL;
  int32_T c11_i0;
  int8_T c11_d_u[2];
  const mxArray *c11_f_y = NULL;
  uint8_T c11_hoistedGlobal;
  uint8_T c11_e_u;
  const mxArray *c11_g_y = NULL;
  uint8_T c11_b_hoistedGlobal;
  uint8_T c11_f_u;
  const mxArray *c11_h_y = NULL;
  uint8_T c11_c_hoistedGlobal;
  uint8_T c11_g_u;
  const mxArray *c11_i_y = NULL;
  uint8_T c11_d_hoistedGlobal;
  uint8_T c11_h_u;
  const mxArray *c11_j_y = NULL;
  int32_T c11_i1;
  boolean_T c11_i_u[3];
  const mxArray *c11_k_y = NULL;
  c11_st = NULL;
  c11_st = NULL;
  c11_y = NULL;
  sf_mex_assign(&c11_y, sf_mex_createcellmatrix(7, 1), false);
  c11_b_y = NULL;
  sf_mex_assign(&c11_b_y, sf_mex_createstruct("structure", 2, 1, 1), false);
  c11_u = *(int8_T *)&((char_T *)chartInstance->c11_finalWay)[0];
  c11_c_y = NULL;
  sf_mex_assign(&c11_c_y, sf_mex_create("y", &c11_u, 2, 0U, 0U, 0U, 0), false);
  sf_mex_addfield(c11_b_y, c11_c_y, "x", "x", 0);
  c11_b_u = *(int8_T *)&((char_T *)chartInstance->c11_finalWay)[1];
  c11_d_y = NULL;
  sf_mex_assign(&c11_d_y, sf_mex_create("y", &c11_b_u, 2, 0U, 0U, 0U, 0), false);
  sf_mex_addfield(c11_b_y, c11_d_y, "y", "y", 0);
  c11_c_u = *(int16_T *)&((char_T *)chartInstance->c11_finalWay)[2];
  c11_e_y = NULL;
  sf_mex_assign(&c11_e_y, sf_mex_create("y", &c11_c_u, 4, 0U, 0U, 0U, 0), false);
  sf_mex_addfield(c11_b_y, c11_e_y, "orientation", "orientation", 0);
  sf_mex_setcell(c11_y, 0, c11_b_y);
  for (c11_i0 = 0; c11_i0 < 2; c11_i0++) {
    c11_d_u[c11_i0] = chartInstance->c11_startingPos[c11_i0];
  }

  c11_f_y = NULL;
  sf_mex_assign(&c11_f_y, sf_mex_create("y", c11_d_u, 2, 0U, 1U, 0U, 2, 2, 1),
                false);
  sf_mex_setcell(c11_y, 1, c11_f_y);
  c11_hoistedGlobal = chartInstance->c11_is_active_c11_greenOffence;
  c11_e_u = c11_hoistedGlobal;
  c11_g_y = NULL;
  sf_mex_assign(&c11_g_y, sf_mex_create("y", &c11_e_u, 3, 0U, 0U, 0U, 0), false);
  sf_mex_setcell(c11_y, 2, c11_g_y);
  c11_b_hoistedGlobal = chartInstance->c11_is_c11_greenOffence;
  c11_f_u = c11_b_hoistedGlobal;
  c11_h_y = NULL;
  sf_mex_assign(&c11_h_y, sf_mex_create("y", &c11_f_u, 3, 0U, 0U, 0U, 0), false);
  sf_mex_setcell(c11_y, 3, c11_h_y);
  c11_c_hoistedGlobal = chartInstance->c11_is_GameIsOn_Goalie;
  c11_g_u = c11_c_hoistedGlobal;
  c11_i_y = NULL;
  sf_mex_assign(&c11_i_y, sf_mex_create("y", &c11_g_u, 3, 0U, 0U, 0U, 0), false);
  sf_mex_setcell(c11_y, 4, c11_i_y);
  c11_d_hoistedGlobal = chartInstance->c11_is_waiting;
  c11_h_u = c11_d_hoistedGlobal;
  c11_j_y = NULL;
  sf_mex_assign(&c11_j_y, sf_mex_create("y", &c11_h_u, 3, 0U, 0U, 0U, 0), false);
  sf_mex_setcell(c11_y, 5, c11_j_y);
  for (c11_i1 = 0; c11_i1 < 3; c11_i1++) {
    c11_i_u[c11_i1] = chartInstance->c11_dataWrittenToVector[c11_i1];
  }

  c11_k_y = NULL;
  sf_mex_assign(&c11_k_y, sf_mex_create("y", c11_i_u, 11, 0U, 1U, 0U, 1, 3),
                false);
  sf_mex_setcell(c11_y, 6, c11_k_y);
  sf_mex_assign(&c11_st, c11_y, false);
  return c11_st;
}

static void set_sim_state_c11_greenOffence(SFc11_greenOffenceInstanceStruct
  *chartInstance, const mxArray *c11_st)
{
  const mxArray *c11_u;
  c11_Waypoint c11_r0;
  int8_T c11_iv0[2];
  int32_T c11_i2;
  boolean_T c11_bv0[3];
  int32_T c11_i3;
  c11_u = sf_mex_dup(c11_st);
  c11_r0 = c11_g_emlrt_marshallIn(chartInstance, sf_mex_dup(sf_mex_getcell(c11_u,
    0)), "finalWay");
  *(int8_T *)&((char_T *)chartInstance->c11_finalWay)[0] = c11_r0.x;
  *(int8_T *)&((char_T *)chartInstance->c11_finalWay)[1] = c11_r0.y;
  *(int16_T *)&((char_T *)chartInstance->c11_finalWay)[2] = c11_r0.orientation;
  c11_k_emlrt_marshallIn(chartInstance, sf_mex_dup(sf_mex_getcell(c11_u, 1)),
    "startingPos", c11_iv0);
  for (c11_i2 = 0; c11_i2 < 2; c11_i2++) {
    chartInstance->c11_startingPos[c11_i2] = c11_iv0[c11_i2];
  }

  chartInstance->c11_is_active_c11_greenOffence = c11_c_emlrt_marshallIn
    (chartInstance, sf_mex_dup(sf_mex_getcell(c11_u, 2)),
     "is_active_c11_greenOffence");
  chartInstance->c11_is_c11_greenOffence = c11_c_emlrt_marshallIn(chartInstance,
    sf_mex_dup(sf_mex_getcell(c11_u, 3)), "is_c11_greenOffence");
  chartInstance->c11_is_GameIsOn_Goalie = c11_c_emlrt_marshallIn(chartInstance,
    sf_mex_dup(sf_mex_getcell(c11_u, 4)), "is_GameIsOn_Goalie");
  chartInstance->c11_is_waiting = c11_c_emlrt_marshallIn(chartInstance,
    sf_mex_dup(sf_mex_getcell(c11_u, 5)), "is_waiting");
  c11_m_emlrt_marshallIn(chartInstance, sf_mex_dup(sf_mex_getcell(c11_u, 6)),
    "dataWrittenToVector", c11_bv0);
  for (c11_i3 = 0; c11_i3 < 3; c11_i3++) {
    chartInstance->c11_dataWrittenToVector[c11_i3] = c11_bv0[c11_i3];
  }

  sf_mex_assign(&chartInstance->c11_setSimStateSideEffectsInfo,
                c11_o_emlrt_marshallIn(chartInstance, sf_mex_dup(sf_mex_getcell
    (c11_u, 7)), "setSimStateSideEffectsInfo"), true);
  sf_mex_destroy(&c11_u);
  chartInstance->c11_doSetSimStateSideEffects = 1U;
  c11_update_debugger_state_c11_greenOffence(chartInstance);
  sf_mex_destroy(&c11_st);
}

static void c11_set_sim_state_side_effects_c11_greenOffence
  (SFc11_greenOffenceInstanceStruct *chartInstance)
{
  if (chartInstance->c11_doSetSimStateSideEffects != 0) {
    if (chartInstance->c11_is_c11_greenOffence == c11_IN_GameIsOn_Goalie) {
      chartInstance->c11_tp_GameIsOn_Goalie = 1U;
    } else {
      chartInstance->c11_tp_GameIsOn_Goalie = 0U;
    }

    if (chartInstance->c11_is_GameIsOn_Goalie == c11_IN_Idle) {
      chartInstance->c11_tp_Idle = 1U;
    } else {
      chartInstance->c11_tp_Idle = 0U;
    }

    if (chartInstance->c11_is_GameIsOn_Goalie == c11_IN_goalie) {
      chartInstance->c11_tp_goalie = 1U;
    } else {
      chartInstance->c11_tp_goalie = 0U;
    }

    if (chartInstance->c11_is_c11_greenOffence == c11_IN_waiting) {
      chartInstance->c11_tp_waiting = 1U;
    } else {
      chartInstance->c11_tp_waiting = 0U;
    }

    if (chartInstance->c11_is_waiting == c11_IN_Idle1) {
      chartInstance->c11_tp_Idle1 = 1U;
    } else {
      chartInstance->c11_tp_Idle1 = 0U;
    }

    chartInstance->c11_doSetSimStateSideEffects = 0U;
  }
}

static void finalize_c11_greenOffence(SFc11_greenOffenceInstanceStruct
  *chartInstance)
{
  sf_mex_destroy(&chartInstance->c11_setSimStateSideEffectsInfo);
}

static void sf_gateway_c11_greenOffence(SFc11_greenOffenceInstanceStruct
  *chartInstance)
{
  int32_T c11_i4;
  c11_set_sim_state_side_effects_c11_greenOffence(chartInstance);
  _SFD_SYMBOL_SCOPE_PUSH(0U, 0U);
  _sfTime_ = sf_get_time(chartInstance->S);
  _SFD_CC_CALL(CHART_ENTER_SFUNCTION_TAG, 5U, chartInstance->c11_sfEvent);
  for (c11_i4 = 0; c11_i4 < 2; c11_i4++) {
    _SFD_DATA_RANGE_CHECK((real_T)chartInstance->c11_startingPos[c11_i4], 5U);
  }

  _SFD_DATA_RANGE_CHECK(*chartInstance->c11_GameOn, 6U);
  chartInstance->c11_sfEvent = CALL_EVENT;
  c11_chartstep_c11_greenOffence(chartInstance);
  _SFD_SYMBOL_SCOPE_POP();
  _SFD_CHECK_FOR_STATE_INCONSISTENCY(_greenOffenceMachineNumber_,
    chartInstance->chartNumber, chartInstance->instanceNumber);
}

static void mdl_start_c11_greenOffence(SFc11_greenOffenceInstanceStruct
  *chartInstance)
{
  (void)chartInstance;
}

static void c11_chartstep_c11_greenOffence(SFc11_greenOffenceInstanceStruct
  *chartInstance)
{
  uint32_T c11_debug_family_var_map[2];
  real_T c11_nargin = 0.0;
  real_T c11_nargout = 0.0;
  real_T c11_b_nargin = 0.0;
  real_T c11_b_nargout = 0.0;
  uint32_T c11_b_debug_family_var_map[3];
  real_T c11_c_nargin = 0.0;
  real_T c11_c_nargout = 1.0;
  boolean_T c11_out;
  real_T c11_d_nargin = 0.0;
  real_T c11_d_nargout = 0.0;
  real_T c11_e_nargin = 0.0;
  real_T c11_e_nargout = 1.0;
  boolean_T c11_b_out;
  int8_T c11_iv1[2];
  uint8_T c11_u0;
  real_T c11_f_nargin = 0.0;
  real_T c11_f_nargout = 0.0;
  real_T c11_g_nargin = 0.0;
  real_T c11_g_nargout = 1.0;
  boolean_T c11_c_out;
  int8_T c11_iv2[2];
  uint8_T c11_u1;
  real_T c11_h_nargin = 0.0;
  real_T c11_h_nargout = 0.0;
  real_T c11_i_nargin = 0.0;
  real_T c11_i_nargout = 1.0;
  boolean_T c11_d_out;
  real_T c11_j_nargin = 0.0;
  real_T c11_j_nargout = 0.0;
  _SFD_CC_CALL(CHART_ENTER_DURING_FUNCTION_TAG, 5U, chartInstance->c11_sfEvent);
  if (chartInstance->c11_is_active_c11_greenOffence == 0U) {
    _SFD_CC_CALL(CHART_ENTER_ENTRY_FUNCTION_TAG, 5U, chartInstance->c11_sfEvent);
    chartInstance->c11_is_active_c11_greenOffence = 1U;
    _SFD_CC_CALL(EXIT_OUT_OF_FUNCTION_TAG, 5U, chartInstance->c11_sfEvent);
    _SFD_CT_CALL(TRANSITION_ACTIVE_TAG, 3U, chartInstance->c11_sfEvent);
    _SFD_SYMBOL_SCOPE_PUSH_EML(0U, 2U, 2U, c11_i_debug_family_names,
      c11_debug_family_var_map);
    _SFD_SYMBOL_SCOPE_ADD_EML_IMPORTABLE(&c11_nargin, 0U, c11_sf_marshallOut,
      c11_sf_marshallIn);
    _SFD_SYMBOL_SCOPE_ADD_EML_IMPORTABLE(&c11_nargout, 1U, c11_sf_marshallOut,
      c11_sf_marshallIn);
    c11_calcStartPos(chartInstance);
    _SFD_SYMBOL_SCOPE_POP();
    chartInstance->c11_is_c11_greenOffence = c11_IN_waiting;
    _SFD_CS_CALL(STATE_ACTIVE_TAG, 5U, chartInstance->c11_sfEvent);
    chartInstance->c11_tp_waiting = 1U;
    _SFD_CT_CALL(TRANSITION_ACTIVE_TAG, 2U, chartInstance->c11_sfEvent);
    chartInstance->c11_is_waiting = c11_IN_Idle1;
    _SFD_CS_CALL(STATE_ACTIVE_TAG, 6U, chartInstance->c11_sfEvent);
    chartInstance->c11_tp_Idle1 = 1U;
    _SFD_SYMBOL_SCOPE_PUSH_EML(0U, 2U, 2U, c11_d_debug_family_names,
      c11_debug_family_var_map);
    _SFD_SYMBOL_SCOPE_ADD_EML_IMPORTABLE(&c11_b_nargin, 0U, c11_sf_marshallOut,
      c11_sf_marshallIn);
    _SFD_SYMBOL_SCOPE_ADD_EML_IMPORTABLE(&c11_b_nargout, 1U, c11_sf_marshallOut,
      c11_sf_marshallIn);
    *(int8_T *)&((char_T *)chartInstance->c11_finalWay)[0] = *(int8_T *)
      &((char_T *)chartInstance->c11_me)[0];
    c11_updateDataWrittenToVector(chartInstance, 0U);
    *(int8_T *)&((char_T *)chartInstance->c11_finalWay)[1] = *(int8_T *)
      &((char_T *)chartInstance->c11_me)[1];
    c11_updateDataWrittenToVector(chartInstance, 0U);
    *(int16_T *)&((char_T *)chartInstance->c11_finalWay)[2] = c11_intmax
      (chartInstance);
    c11_updateDataWrittenToVector(chartInstance, 0U);
    _SFD_SYMBOL_SCOPE_POP();
  } else {
    switch (chartInstance->c11_is_c11_greenOffence) {
     case c11_IN_GameIsOn_Goalie:
      CV_CHART_EVAL(5, 0, 1);
      _SFD_CT_CALL(TRANSITION_BEFORE_PROCESSING_TAG, 1U,
                   chartInstance->c11_sfEvent);
      _SFD_SYMBOL_SCOPE_PUSH_EML(0U, 3U, 3U, c11_k_debug_family_names,
        c11_b_debug_family_var_map);
      _SFD_SYMBOL_SCOPE_ADD_EML_IMPORTABLE(&c11_c_nargin, 0U, c11_sf_marshallOut,
        c11_sf_marshallIn);
      _SFD_SYMBOL_SCOPE_ADD_EML_IMPORTABLE(&c11_c_nargout, 1U,
        c11_sf_marshallOut, c11_sf_marshallIn);
      _SFD_SYMBOL_SCOPE_ADD_EML_IMPORTABLE(&c11_out, 2U, c11_b_sf_marshallOut,
        c11_b_sf_marshallIn);
      c11_out = CV_EML_IF(1, 0, 0, CV_RELATIONAL_EVAL(5U, 1U, 0,
        *chartInstance->c11_GameOn, 0.0, -1, 0U, *chartInstance->c11_GameOn ==
        0.0));
      _SFD_SYMBOL_SCOPE_POP();
      if (c11_out) {
        _SFD_CT_CALL(TRANSITION_ACTIVE_TAG, 1U, chartInstance->c11_sfEvent);
        c11_exit_internal_GameIsOn_Goalie(chartInstance);
        chartInstance->c11_tp_GameIsOn_Goalie = 0U;
        _SFD_CS_CALL(STATE_INACTIVE_TAG, 0U, chartInstance->c11_sfEvent);
        chartInstance->c11_is_c11_greenOffence = c11_IN_waiting;
        _SFD_CS_CALL(STATE_ACTIVE_TAG, 5U, chartInstance->c11_sfEvent);
        chartInstance->c11_tp_waiting = 1U;
        _SFD_CT_CALL(TRANSITION_ACTIVE_TAG, 2U, chartInstance->c11_sfEvent);
        chartInstance->c11_is_waiting = c11_IN_Idle1;
        _SFD_CS_CALL(STATE_ACTIVE_TAG, 6U, chartInstance->c11_sfEvent);
        chartInstance->c11_tp_Idle1 = 1U;
        _SFD_SYMBOL_SCOPE_PUSH_EML(0U, 2U, 2U, c11_d_debug_family_names,
          c11_debug_family_var_map);
        _SFD_SYMBOL_SCOPE_ADD_EML_IMPORTABLE(&c11_d_nargin, 0U,
          c11_sf_marshallOut, c11_sf_marshallIn);
        _SFD_SYMBOL_SCOPE_ADD_EML_IMPORTABLE(&c11_d_nargout, 1U,
          c11_sf_marshallOut, c11_sf_marshallIn);
        *(int8_T *)&((char_T *)chartInstance->c11_finalWay)[0] = *(int8_T *)
          &((char_T *)chartInstance->c11_me)[0];
        c11_updateDataWrittenToVector(chartInstance, 0U);
        *(int8_T *)&((char_T *)chartInstance->c11_finalWay)[1] = *(int8_T *)
          &((char_T *)chartInstance->c11_me)[1];
        c11_updateDataWrittenToVector(chartInstance, 0U);
        *(int16_T *)&((char_T *)chartInstance->c11_finalWay)[2] = c11_intmax
          (chartInstance);
        c11_updateDataWrittenToVector(chartInstance, 0U);
        _SFD_SYMBOL_SCOPE_POP();
      } else {
        _SFD_CS_CALL(STATE_ENTER_DURING_FUNCTION_TAG, 0U,
                     chartInstance->c11_sfEvent);
        switch (chartInstance->c11_is_GameIsOn_Goalie) {
         case c11_IN_Idle:
          CV_STATE_EVAL(0, 0, 1);
          _SFD_CT_CALL(TRANSITION_BEFORE_PROCESSING_TAG, 6U,
                       chartInstance->c11_sfEvent);
          _SFD_SYMBOL_SCOPE_PUSH_EML(0U, 3U, 3U, c11_h_debug_family_names,
            c11_b_debug_family_var_map);
          _SFD_SYMBOL_SCOPE_ADD_EML_IMPORTABLE(&c11_e_nargin, 0U,
            c11_sf_marshallOut, c11_sf_marshallIn);
          _SFD_SYMBOL_SCOPE_ADD_EML_IMPORTABLE(&c11_e_nargout, 1U,
            c11_sf_marshallOut, c11_sf_marshallIn);
          _SFD_SYMBOL_SCOPE_ADD_EML_IMPORTABLE(&c11_b_out, 2U,
            c11_b_sf_marshallOut, c11_b_sf_marshallIn);
          c11_iv1[0] = -80;
          c11_iv1[1] = *(int8_T *)&((char_T *)chartInstance->c11_ball)[1];
          c11_u0 = c11_checkReached(chartInstance, c11_iv1, 15.0);
          c11_b_out = CV_EML_IF(6, 0, 0, CV_RELATIONAL_EVAL(5U, 6U, 0, (real_T)
            c11_u0, 0.0, 0, 0U, c11_u0 == 0));
          _SFD_SYMBOL_SCOPE_POP();
          if (c11_b_out) {
            _SFD_CT_CALL(TRANSITION_ACTIVE_TAG, 6U, chartInstance->c11_sfEvent);
            chartInstance->c11_tp_Idle = 0U;
            _SFD_CS_CALL(STATE_INACTIVE_TAG, 1U, chartInstance->c11_sfEvent);
            chartInstance->c11_is_GameIsOn_Goalie = c11_IN_goalie;
            _SFD_CS_CALL(STATE_ACTIVE_TAG, 2U, chartInstance->c11_sfEvent);
            chartInstance->c11_tp_goalie = 1U;
          } else {
            _SFD_CS_CALL(STATE_ENTER_DURING_FUNCTION_TAG, 1U,
                         chartInstance->c11_sfEvent);
            _SFD_SYMBOL_SCOPE_PUSH_EML(0U, 2U, 2U, c11_c_debug_family_names,
              c11_debug_family_var_map);
            _SFD_SYMBOL_SCOPE_ADD_EML_IMPORTABLE(&c11_f_nargin, 0U,
              c11_sf_marshallOut, c11_sf_marshallIn);
            _SFD_SYMBOL_SCOPE_ADD_EML_IMPORTABLE(&c11_f_nargout, 1U,
              c11_sf_marshallOut, c11_sf_marshallIn);
            *(int8_T *)&((char_T *)chartInstance->c11_finalWay)[0] = *(int8_T *)
              &((char_T *)chartInstance->c11_me)[0];
            c11_updateDataWrittenToVector(chartInstance, 0U);
            *(int8_T *)&((char_T *)chartInstance->c11_finalWay)[1] = *(int8_T *)
              &((char_T *)chartInstance->c11_me)[1];
            c11_updateDataWrittenToVector(chartInstance, 0U);
            *(int16_T *)&((char_T *)chartInstance->c11_finalWay)[2] = *(int16_T *)
              &((char_T *)chartInstance->c11_me)[2];
            c11_updateDataWrittenToVector(chartInstance, 0U);
            _SFD_SYMBOL_SCOPE_POP();
          }

          _SFD_CS_CALL(EXIT_OUT_OF_FUNCTION_TAG, 1U, chartInstance->c11_sfEvent);
          break;

         case c11_IN_goalie:
          CV_STATE_EVAL(0, 0, 2);
          _SFD_CT_CALL(TRANSITION_BEFORE_PROCESSING_TAG, 5U,
                       chartInstance->c11_sfEvent);
          _SFD_SYMBOL_SCOPE_PUSH_EML(0U, 3U, 3U, c11_f_debug_family_names,
            c11_b_debug_family_var_map);
          _SFD_SYMBOL_SCOPE_ADD_EML_IMPORTABLE(&c11_g_nargin, 0U,
            c11_sf_marshallOut, c11_sf_marshallIn);
          _SFD_SYMBOL_SCOPE_ADD_EML_IMPORTABLE(&c11_g_nargout, 1U,
            c11_sf_marshallOut, c11_sf_marshallIn);
          _SFD_SYMBOL_SCOPE_ADD_EML_IMPORTABLE(&c11_c_out, 2U,
            c11_b_sf_marshallOut, c11_b_sf_marshallIn);
          c11_iv2[0] = -80;
          c11_iv2[1] = *(int8_T *)&((char_T *)chartInstance->c11_ball)[1];
          c11_u1 = c11_checkReached(chartInstance, c11_iv2, 10.0);
          c11_c_out = CV_EML_IF(5, 0, 0, CV_RELATIONAL_EVAL(5U, 5U, 0, (real_T)
            c11_u1, 1.0, 0, 0U, c11_u1 == 1));
          _SFD_SYMBOL_SCOPE_POP();
          if (c11_c_out) {
            _SFD_CT_CALL(TRANSITION_ACTIVE_TAG, 5U, chartInstance->c11_sfEvent);
            chartInstance->c11_tp_goalie = 0U;
            _SFD_CS_CALL(STATE_INACTIVE_TAG, 2U, chartInstance->c11_sfEvent);
            chartInstance->c11_is_GameIsOn_Goalie = c11_IN_Idle;
            _SFD_CS_CALL(STATE_ACTIVE_TAG, 1U, chartInstance->c11_sfEvent);
            chartInstance->c11_tp_Idle = 1U;
          } else {
            _SFD_CS_CALL(STATE_ENTER_DURING_FUNCTION_TAG, 2U,
                         chartInstance->c11_sfEvent);
            _SFD_SYMBOL_SCOPE_PUSH_EML(0U, 2U, 2U, c11_b_debug_family_names,
              c11_debug_family_var_map);
            _SFD_SYMBOL_SCOPE_ADD_EML_IMPORTABLE(&c11_h_nargin, 0U,
              c11_sf_marshallOut, c11_sf_marshallIn);
            _SFD_SYMBOL_SCOPE_ADD_EML_IMPORTABLE(&c11_h_nargout, 1U,
              c11_sf_marshallOut, c11_sf_marshallIn);
            *(int8_T *)&((char_T *)chartInstance->c11_finalWay)[0] = -70;
            c11_updateDataWrittenToVector(chartInstance, 0U);
            *(int8_T *)&((char_T *)chartInstance->c11_finalWay)[1] = *(int8_T *)
              &((char_T *)chartInstance->c11_ball)[1];
            c11_updateDataWrittenToVector(chartInstance, 0U);
            *(int16_T *)&((char_T *)chartInstance->c11_finalWay)[2] = c11_intmax
              (chartInstance);
            c11_updateDataWrittenToVector(chartInstance, 0U);
            _SFD_SYMBOL_SCOPE_POP();
          }

          _SFD_CS_CALL(EXIT_OUT_OF_FUNCTION_TAG, 2U, chartInstance->c11_sfEvent);
          break;

         default:
          CV_STATE_EVAL(0, 0, 0);
          chartInstance->c11_is_GameIsOn_Goalie = c11_IN_NO_ACTIVE_CHILD;
          _SFD_CS_CALL(STATE_INACTIVE_TAG, 1U, chartInstance->c11_sfEvent);
          break;
        }
      }

      _SFD_CS_CALL(EXIT_OUT_OF_FUNCTION_TAG, 0U, chartInstance->c11_sfEvent);
      break;

     case c11_IN_waiting:
      CV_CHART_EVAL(5, 0, 2);
      _SFD_CT_CALL(TRANSITION_BEFORE_PROCESSING_TAG, 0U,
                   chartInstance->c11_sfEvent);
      _SFD_SYMBOL_SCOPE_PUSH_EML(0U, 3U, 3U, c11_j_debug_family_names,
        c11_b_debug_family_var_map);
      _SFD_SYMBOL_SCOPE_ADD_EML_IMPORTABLE(&c11_i_nargin, 0U, c11_sf_marshallOut,
        c11_sf_marshallIn);
      _SFD_SYMBOL_SCOPE_ADD_EML_IMPORTABLE(&c11_i_nargout, 1U,
        c11_sf_marshallOut, c11_sf_marshallIn);
      _SFD_SYMBOL_SCOPE_ADD_EML_IMPORTABLE(&c11_d_out, 2U, c11_b_sf_marshallOut,
        c11_b_sf_marshallIn);
      c11_d_out = CV_EML_IF(0, 0, 0, CV_RELATIONAL_EVAL(5U, 0U, 0,
        *chartInstance->c11_GameOn, 1.0, -1, 0U, *chartInstance->c11_GameOn ==
        1.0));
      _SFD_SYMBOL_SCOPE_POP();
      if (c11_d_out) {
        _SFD_CT_CALL(TRANSITION_ACTIVE_TAG, 0U, chartInstance->c11_sfEvent);
        chartInstance->c11_tp_Idle1 = 0U;
        chartInstance->c11_is_waiting = c11_IN_NO_ACTIVE_CHILD;
        _SFD_CS_CALL(STATE_INACTIVE_TAG, 6U, chartInstance->c11_sfEvent);
        chartInstance->c11_tp_waiting = 0U;
        _SFD_CS_CALL(STATE_INACTIVE_TAG, 5U, chartInstance->c11_sfEvent);
        chartInstance->c11_is_c11_greenOffence = c11_IN_GameIsOn_Goalie;
        _SFD_CS_CALL(STATE_ACTIVE_TAG, 0U, chartInstance->c11_sfEvent);
        chartInstance->c11_tp_GameIsOn_Goalie = 1U;
        _SFD_CT_CALL(TRANSITION_ACTIVE_TAG, 4U, chartInstance->c11_sfEvent);
        chartInstance->c11_is_GameIsOn_Goalie = c11_IN_goalie;
        _SFD_CS_CALL(STATE_ACTIVE_TAG, 2U, chartInstance->c11_sfEvent);
        chartInstance->c11_tp_goalie = 1U;
      } else {
        _SFD_CS_CALL(STATE_ENTER_DURING_FUNCTION_TAG, 5U,
                     chartInstance->c11_sfEvent);
        _SFD_CS_CALL(STATE_ENTER_DURING_FUNCTION_TAG, 6U,
                     chartInstance->c11_sfEvent);
        _SFD_SYMBOL_SCOPE_PUSH_EML(0U, 2U, 2U, c11_e_debug_family_names,
          c11_debug_family_var_map);
        _SFD_SYMBOL_SCOPE_ADD_EML_IMPORTABLE(&c11_j_nargin, 0U,
          c11_sf_marshallOut, c11_sf_marshallIn);
        _SFD_SYMBOL_SCOPE_ADD_EML_IMPORTABLE(&c11_j_nargout, 1U,
          c11_sf_marshallOut, c11_sf_marshallIn);
        *(int8_T *)&((char_T *)chartInstance->c11_finalWay)[0] = *(int8_T *)
          &((char_T *)chartInstance->c11_me)[0];
        c11_updateDataWrittenToVector(chartInstance, 0U);
        *(int8_T *)&((char_T *)chartInstance->c11_finalWay)[1] = *(int8_T *)
          &((char_T *)chartInstance->c11_me)[1];
        c11_updateDataWrittenToVector(chartInstance, 0U);
        *(int16_T *)&((char_T *)chartInstance->c11_finalWay)[2] = c11_intmax
          (chartInstance);
        c11_updateDataWrittenToVector(chartInstance, 0U);
        _SFD_SYMBOL_SCOPE_POP();
        _SFD_CS_CALL(EXIT_OUT_OF_FUNCTION_TAG, 6U, chartInstance->c11_sfEvent);
      }

      _SFD_CS_CALL(EXIT_OUT_OF_FUNCTION_TAG, 5U, chartInstance->c11_sfEvent);
      break;

     default:
      CV_CHART_EVAL(5, 0, 0);
      chartInstance->c11_is_c11_greenOffence = c11_IN_NO_ACTIVE_CHILD;
      _SFD_CS_CALL(STATE_INACTIVE_TAG, 0U, chartInstance->c11_sfEvent);
      break;
    }
  }

  _SFD_CC_CALL(EXIT_OUT_OF_FUNCTION_TAG, 5U, chartInstance->c11_sfEvent);
}

static void initSimStructsc11_greenOffence(SFc11_greenOffenceInstanceStruct
  *chartInstance)
{
  (void)chartInstance;
}

static void c11_exit_internal_GameIsOn_Goalie(SFc11_greenOffenceInstanceStruct
  *chartInstance)
{
  switch (chartInstance->c11_is_GameIsOn_Goalie) {
   case c11_IN_Idle:
    CV_STATE_EVAL(0, 1, 1);
    chartInstance->c11_tp_Idle = 0U;
    chartInstance->c11_is_GameIsOn_Goalie = c11_IN_NO_ACTIVE_CHILD;
    _SFD_CS_CALL(STATE_INACTIVE_TAG, 1U, chartInstance->c11_sfEvent);
    break;

   case c11_IN_goalie:
    CV_STATE_EVAL(0, 1, 2);
    chartInstance->c11_tp_goalie = 0U;
    chartInstance->c11_is_GameIsOn_Goalie = c11_IN_NO_ACTIVE_CHILD;
    _SFD_CS_CALL(STATE_INACTIVE_TAG, 2U, chartInstance->c11_sfEvent);
    break;

   default:
    CV_STATE_EVAL(0, 1, 0);
    chartInstance->c11_is_GameIsOn_Goalie = c11_IN_NO_ACTIVE_CHILD;
    _SFD_CS_CALL(STATE_INACTIVE_TAG, 1U, chartInstance->c11_sfEvent);
    break;
  }
}

static int16_T c11_intmax(SFc11_greenOffenceInstanceStruct *chartInstance)
{
  (void)chartInstance;
  return MAX_int16_T;
}

static real32_T c11_eml_xnrm2(SFc11_greenOffenceInstanceStruct *chartInstance,
  real32_T c11_x[2])
{
  real32_T c11_y;
  real32_T c11_scale;
  int32_T c11_k;
  int32_T c11_b_k;
  real32_T c11_b_x;
  real32_T c11_c_x;
  real32_T c11_absxk;
  real32_T c11_t;
  c11_below_threshold(chartInstance);
  c11_y = 0.0F;
  c11_scale = 1.17549435E-38F;
  for (c11_k = 1; c11_k < 3; c11_k++) {
    c11_b_k = c11_k;
    c11_b_x = c11_x[_SFD_EML_ARRAY_BOUNDS_CHECK("", (int32_T)_SFD_INTEGER_CHECK(
      "", (real_T)c11_b_k), 1, 2, 1, 0) - 1];
    c11_c_x = c11_b_x;
    c11_absxk = muSingleScalarAbs(c11_c_x);
    if (c11_absxk > c11_scale) {
      c11_t = c11_scale / c11_absxk;
      c11_y = 1.0F + c11_y * c11_t * c11_t;
      c11_scale = c11_absxk;
    } else {
      c11_t = c11_absxk / c11_scale;
      c11_y += c11_t * c11_t;
    }
  }

  return c11_scale * muSingleScalarSqrt(c11_y);
}

static void c11_below_threshold(SFc11_greenOffenceInstanceStruct *chartInstance)
{
  (void)chartInstance;
}

static void init_script_number_translation(uint32_T c11_machineNumber, uint32_T
  c11_chartNumber, uint32_T c11_instanceNumber)
{
  (void)c11_machineNumber;
  (void)c11_chartNumber;
  (void)c11_instanceNumber;
}

static const mxArray *c11_sf_marshallOut(void *chartInstanceVoid, void
  *c11_inData)
{
  const mxArray *c11_mxArrayOutData = NULL;
  real_T c11_u;
  const mxArray *c11_y = NULL;
  SFc11_greenOffenceInstanceStruct *chartInstance;
  chartInstance = (SFc11_greenOffenceInstanceStruct *)chartInstanceVoid;
  c11_mxArrayOutData = NULL;
  c11_u = *(real_T *)c11_inData;
  c11_y = NULL;
  sf_mex_assign(&c11_y, sf_mex_create("y", &c11_u, 0, 0U, 0U, 0U, 0), false);
  sf_mex_assign(&c11_mxArrayOutData, c11_y, false);
  return c11_mxArrayOutData;
}

static real_T c11_emlrt_marshallIn(SFc11_greenOffenceInstanceStruct
  *chartInstance, const mxArray *c11_u, const emlrtMsgIdentifier *c11_parentId)
{
  real_T c11_y;
  real_T c11_d0;
  (void)chartInstance;
  sf_mex_import(c11_parentId, sf_mex_dup(c11_u), &c11_d0, 1, 0, 0U, 0, 0U, 0);
  c11_y = c11_d0;
  sf_mex_destroy(&c11_u);
  return c11_y;
}

static void c11_sf_marshallIn(void *chartInstanceVoid, const mxArray
  *c11_mxArrayInData, const char_T *c11_varName, void *c11_outData)
{
  const mxArray *c11_nargout;
  const char_T *c11_identifier;
  emlrtMsgIdentifier c11_thisId;
  real_T c11_y;
  SFc11_greenOffenceInstanceStruct *chartInstance;
  chartInstance = (SFc11_greenOffenceInstanceStruct *)chartInstanceVoid;
  c11_nargout = sf_mex_dup(c11_mxArrayInData);
  c11_identifier = c11_varName;
  c11_thisId.fIdentifier = c11_identifier;
  c11_thisId.fParent = NULL;
  c11_y = c11_emlrt_marshallIn(chartInstance, sf_mex_dup(c11_nargout),
    &c11_thisId);
  sf_mex_destroy(&c11_nargout);
  *(real_T *)c11_outData = c11_y;
  sf_mex_destroy(&c11_mxArrayInData);
}

static const mxArray *c11_b_sf_marshallOut(void *chartInstanceVoid, void
  *c11_inData)
{
  const mxArray *c11_mxArrayOutData = NULL;
  boolean_T c11_u;
  const mxArray *c11_y = NULL;
  SFc11_greenOffenceInstanceStruct *chartInstance;
  chartInstance = (SFc11_greenOffenceInstanceStruct *)chartInstanceVoid;
  c11_mxArrayOutData = NULL;
  c11_u = *(boolean_T *)c11_inData;
  c11_y = NULL;
  sf_mex_assign(&c11_y, sf_mex_create("y", &c11_u, 11, 0U, 0U, 0U, 0), false);
  sf_mex_assign(&c11_mxArrayOutData, c11_y, false);
  return c11_mxArrayOutData;
}

static boolean_T c11_b_emlrt_marshallIn(SFc11_greenOffenceInstanceStruct
  *chartInstance, const mxArray *c11_u, const emlrtMsgIdentifier *c11_parentId)
{
  boolean_T c11_y;
  boolean_T c11_b0;
  (void)chartInstance;
  sf_mex_import(c11_parentId, sf_mex_dup(c11_u), &c11_b0, 1, 11, 0U, 0, 0U, 0);
  c11_y = c11_b0;
  sf_mex_destroy(&c11_u);
  return c11_y;
}

static void c11_b_sf_marshallIn(void *chartInstanceVoid, const mxArray
  *c11_mxArrayInData, const char_T *c11_varName, void *c11_outData)
{
  const mxArray *c11_sf_internal_predicateOutput;
  const char_T *c11_identifier;
  emlrtMsgIdentifier c11_thisId;
  boolean_T c11_y;
  SFc11_greenOffenceInstanceStruct *chartInstance;
  chartInstance = (SFc11_greenOffenceInstanceStruct *)chartInstanceVoid;
  c11_sf_internal_predicateOutput = sf_mex_dup(c11_mxArrayInData);
  c11_identifier = c11_varName;
  c11_thisId.fIdentifier = c11_identifier;
  c11_thisId.fParent = NULL;
  c11_y = c11_b_emlrt_marshallIn(chartInstance, sf_mex_dup
    (c11_sf_internal_predicateOutput), &c11_thisId);
  sf_mex_destroy(&c11_sf_internal_predicateOutput);
  *(boolean_T *)c11_outData = c11_y;
  sf_mex_destroy(&c11_mxArrayInData);
}

static const mxArray *c11_c_sf_marshallOut(void *chartInstanceVoid, void
  *c11_inData)
{
  const mxArray *c11_mxArrayOutData = NULL;
  uint8_T c11_u;
  const mxArray *c11_y = NULL;
  SFc11_greenOffenceInstanceStruct *chartInstance;
  chartInstance = (SFc11_greenOffenceInstanceStruct *)chartInstanceVoid;
  c11_mxArrayOutData = NULL;
  c11_u = *(uint8_T *)c11_inData;
  c11_y = NULL;
  sf_mex_assign(&c11_y, sf_mex_create("y", &c11_u, 3, 0U, 0U, 0U, 0), false);
  sf_mex_assign(&c11_mxArrayOutData, c11_y, false);
  return c11_mxArrayOutData;
}

static uint8_T c11_c_emlrt_marshallIn(SFc11_greenOffenceInstanceStruct
  *chartInstance, const mxArray *c11_posReached, const char_T *c11_identifier)
{
  uint8_T c11_y;
  emlrtMsgIdentifier c11_thisId;
  c11_thisId.fIdentifier = c11_identifier;
  c11_thisId.fParent = NULL;
  c11_y = c11_d_emlrt_marshallIn(chartInstance, sf_mex_dup(c11_posReached),
    &c11_thisId);
  sf_mex_destroy(&c11_posReached);
  return c11_y;
}

static uint8_T c11_d_emlrt_marshallIn(SFc11_greenOffenceInstanceStruct
  *chartInstance, const mxArray *c11_u, const emlrtMsgIdentifier *c11_parentId)
{
  uint8_T c11_y;
  uint8_T c11_u2;
  (void)chartInstance;
  sf_mex_import(c11_parentId, sf_mex_dup(c11_u), &c11_u2, 1, 3, 0U, 0, 0U, 0);
  c11_y = c11_u2;
  sf_mex_destroy(&c11_u);
  return c11_y;
}

static void c11_c_sf_marshallIn(void *chartInstanceVoid, const mxArray
  *c11_mxArrayInData, const char_T *c11_varName, void *c11_outData)
{
  const mxArray *c11_posReached;
  const char_T *c11_identifier;
  emlrtMsgIdentifier c11_thisId;
  uint8_T c11_y;
  SFc11_greenOffenceInstanceStruct *chartInstance;
  chartInstance = (SFc11_greenOffenceInstanceStruct *)chartInstanceVoid;
  c11_posReached = sf_mex_dup(c11_mxArrayInData);
  c11_identifier = c11_varName;
  c11_thisId.fIdentifier = c11_identifier;
  c11_thisId.fParent = NULL;
  c11_y = c11_d_emlrt_marshallIn(chartInstance, sf_mex_dup(c11_posReached),
    &c11_thisId);
  sf_mex_destroy(&c11_posReached);
  *(uint8_T *)c11_outData = c11_y;
  sf_mex_destroy(&c11_mxArrayInData);
}

static const mxArray *c11_d_sf_marshallOut(void *chartInstanceVoid, void
  *c11_inData)
{
  const mxArray *c11_mxArrayOutData = NULL;
  int32_T c11_i5;
  int8_T c11_b_inData[2];
  int32_T c11_i6;
  int8_T c11_u[2];
  const mxArray *c11_y = NULL;
  SFc11_greenOffenceInstanceStruct *chartInstance;
  chartInstance = (SFc11_greenOffenceInstanceStruct *)chartInstanceVoid;
  c11_mxArrayOutData = NULL;
  for (c11_i5 = 0; c11_i5 < 2; c11_i5++) {
    c11_b_inData[c11_i5] = (*(int8_T (*)[2])c11_inData)[c11_i5];
  }

  for (c11_i6 = 0; c11_i6 < 2; c11_i6++) {
    c11_u[c11_i6] = c11_b_inData[c11_i6];
  }

  c11_y = NULL;
  sf_mex_assign(&c11_y, sf_mex_create("y", c11_u, 2, 0U, 1U, 0U, 2, 1, 2), false);
  sf_mex_assign(&c11_mxArrayOutData, c11_y, false);
  return c11_mxArrayOutData;
}

static void c11_e_emlrt_marshallIn(SFc11_greenOffenceInstanceStruct
  *chartInstance, const mxArray *c11_u, const emlrtMsgIdentifier *c11_parentId,
  int8_T c11_y[2])
{
  int8_T c11_iv3[2];
  int32_T c11_i7;
  (void)chartInstance;
  sf_mex_import(c11_parentId, sf_mex_dup(c11_u), c11_iv3, 1, 2, 0U, 1, 0U, 2, 1,
                2);
  for (c11_i7 = 0; c11_i7 < 2; c11_i7++) {
    c11_y[c11_i7] = c11_iv3[c11_i7];
  }

  sf_mex_destroy(&c11_u);
}

static void c11_d_sf_marshallIn(void *chartInstanceVoid, const mxArray
  *c11_mxArrayInData, const char_T *c11_varName, void *c11_outData)
{
  const mxArray *c11_pos;
  const char_T *c11_identifier;
  emlrtMsgIdentifier c11_thisId;
  int8_T c11_y[2];
  int32_T c11_i8;
  SFc11_greenOffenceInstanceStruct *chartInstance;
  chartInstance = (SFc11_greenOffenceInstanceStruct *)chartInstanceVoid;
  c11_pos = sf_mex_dup(c11_mxArrayInData);
  c11_identifier = c11_varName;
  c11_thisId.fIdentifier = c11_identifier;
  c11_thisId.fParent = NULL;
  c11_e_emlrt_marshallIn(chartInstance, sf_mex_dup(c11_pos), &c11_thisId, c11_y);
  sf_mex_destroy(&c11_pos);
  for (c11_i8 = 0; c11_i8 < 2; c11_i8++) {
    (*(int8_T (*)[2])c11_outData)[c11_i8] = c11_y[c11_i8];
  }

  sf_mex_destroy(&c11_mxArrayInData);
}

const mxArray *sf_c11_greenOffence_get_eml_resolved_functions_info(void)
{
  const mxArray *c11_nameCaptureInfo = NULL;
  c11_nameCaptureInfo = NULL;
  sf_mex_assign(&c11_nameCaptureInfo, sf_mex_createstruct("structure", 2, 27, 1),
                false);
  c11_info_helper(&c11_nameCaptureInfo);
  sf_mex_emlrtNameCapturePostProcessR2012a(&c11_nameCaptureInfo);
  return c11_nameCaptureInfo;
}

static void c11_info_helper(const mxArray **c11_info)
{
  const mxArray *c11_rhs0 = NULL;
  const mxArray *c11_lhs0 = NULL;
  const mxArray *c11_rhs1 = NULL;
  const mxArray *c11_lhs1 = NULL;
  const mxArray *c11_rhs2 = NULL;
  const mxArray *c11_lhs2 = NULL;
  const mxArray *c11_rhs3 = NULL;
  const mxArray *c11_lhs3 = NULL;
  const mxArray *c11_rhs4 = NULL;
  const mxArray *c11_lhs4 = NULL;
  const mxArray *c11_rhs5 = NULL;
  const mxArray *c11_lhs5 = NULL;
  const mxArray *c11_rhs6 = NULL;
  const mxArray *c11_lhs6 = NULL;
  const mxArray *c11_rhs7 = NULL;
  const mxArray *c11_lhs7 = NULL;
  const mxArray *c11_rhs8 = NULL;
  const mxArray *c11_lhs8 = NULL;
  const mxArray *c11_rhs9 = NULL;
  const mxArray *c11_lhs9 = NULL;
  const mxArray *c11_rhs10 = NULL;
  const mxArray *c11_lhs10 = NULL;
  const mxArray *c11_rhs11 = NULL;
  const mxArray *c11_lhs11 = NULL;
  const mxArray *c11_rhs12 = NULL;
  const mxArray *c11_lhs12 = NULL;
  const mxArray *c11_rhs13 = NULL;
  const mxArray *c11_lhs13 = NULL;
  const mxArray *c11_rhs14 = NULL;
  const mxArray *c11_lhs14 = NULL;
  const mxArray *c11_rhs15 = NULL;
  const mxArray *c11_lhs15 = NULL;
  const mxArray *c11_rhs16 = NULL;
  const mxArray *c11_lhs16 = NULL;
  const mxArray *c11_rhs17 = NULL;
  const mxArray *c11_lhs17 = NULL;
  const mxArray *c11_rhs18 = NULL;
  const mxArray *c11_lhs18 = NULL;
  const mxArray *c11_rhs19 = NULL;
  const mxArray *c11_lhs19 = NULL;
  const mxArray *c11_rhs20 = NULL;
  const mxArray *c11_lhs20 = NULL;
  const mxArray *c11_rhs21 = NULL;
  const mxArray *c11_lhs21 = NULL;
  const mxArray *c11_rhs22 = NULL;
  const mxArray *c11_lhs22 = NULL;
  const mxArray *c11_rhs23 = NULL;
  const mxArray *c11_lhs23 = NULL;
  const mxArray *c11_rhs24 = NULL;
  const mxArray *c11_lhs24 = NULL;
  const mxArray *c11_rhs25 = NULL;
  const mxArray *c11_lhs25 = NULL;
  const mxArray *c11_rhs26 = NULL;
  const mxArray *c11_lhs26 = NULL;
  sf_mex_addfield(*c11_info, c11_emlrt_marshallOut(""), "context", "context", 0);
  sf_mex_addfield(*c11_info, c11_emlrt_marshallOut("intmax"), "name", "name", 0);
  sf_mex_addfield(*c11_info, c11_emlrt_marshallOut("char"), "dominantType",
                  "dominantType", 0);
  sf_mex_addfield(*c11_info, c11_emlrt_marshallOut(
    "[ILXE]$matlabroot$/toolbox/eml/lib/matlab/elmat/intmax.m"), "resolved",
                  "resolved", 0);
  sf_mex_addfield(*c11_info, c11_b_emlrt_marshallOut(1362265482U), "fileTimeLo",
                  "fileTimeLo", 0);
  sf_mex_addfield(*c11_info, c11_b_emlrt_marshallOut(0U), "fileTimeHi",
                  "fileTimeHi", 0);
  sf_mex_addfield(*c11_info, c11_b_emlrt_marshallOut(0U), "mFileTimeLo",
                  "mFileTimeLo", 0);
  sf_mex_addfield(*c11_info, c11_b_emlrt_marshallOut(0U), "mFileTimeHi",
                  "mFileTimeHi", 0);
  sf_mex_assign(&c11_rhs0, sf_mex_createcellmatrix(0, 1), false);
  sf_mex_assign(&c11_lhs0, sf_mex_createcellmatrix(0, 1), false);
  sf_mex_addfield(*c11_info, sf_mex_duplicatearraysafe(&c11_rhs0), "rhs", "rhs",
                  0);
  sf_mex_addfield(*c11_info, sf_mex_duplicatearraysafe(&c11_lhs0), "lhs", "lhs",
                  0);
  sf_mex_addfield(*c11_info, c11_emlrt_marshallOut(
    "[ILXE]$matlabroot$/toolbox/eml/lib/matlab/elmat/intmax.m"), "context",
                  "context", 1);
  sf_mex_addfield(*c11_info, c11_emlrt_marshallOut("eml_switch_helper"), "name",
                  "name", 1);
  sf_mex_addfield(*c11_info, c11_emlrt_marshallOut(""), "dominantType",
                  "dominantType", 1);
  sf_mex_addfield(*c11_info, c11_emlrt_marshallOut(
    "[ILXE]$matlabroot$/toolbox/eml/lib/matlab/eml/eml_switch_helper.m"),
                  "resolved", "resolved", 1);
  sf_mex_addfield(*c11_info, c11_b_emlrt_marshallOut(1393334458U), "fileTimeLo",
                  "fileTimeLo", 1);
  sf_mex_addfield(*c11_info, c11_b_emlrt_marshallOut(0U), "fileTimeHi",
                  "fileTimeHi", 1);
  sf_mex_addfield(*c11_info, c11_b_emlrt_marshallOut(0U), "mFileTimeLo",
                  "mFileTimeLo", 1);
  sf_mex_addfield(*c11_info, c11_b_emlrt_marshallOut(0U), "mFileTimeHi",
                  "mFileTimeHi", 1);
  sf_mex_assign(&c11_rhs1, sf_mex_createcellmatrix(0, 1), false);
  sf_mex_assign(&c11_lhs1, sf_mex_createcellmatrix(0, 1), false);
  sf_mex_addfield(*c11_info, sf_mex_duplicatearraysafe(&c11_rhs1), "rhs", "rhs",
                  1);
  sf_mex_addfield(*c11_info, sf_mex_duplicatearraysafe(&c11_lhs1), "lhs", "lhs",
                  1);
  sf_mex_addfield(*c11_info, c11_emlrt_marshallOut(""), "context", "context", 2);
  sf_mex_addfield(*c11_info, c11_emlrt_marshallOut("norm"), "name", "name", 2);
  sf_mex_addfield(*c11_info, c11_emlrt_marshallOut("single"), "dominantType",
                  "dominantType", 2);
  sf_mex_addfield(*c11_info, c11_emlrt_marshallOut(
    "[ILXE]$matlabroot$/toolbox/eml/lib/matlab/matfun/norm.m"), "resolved",
                  "resolved", 2);
  sf_mex_addfield(*c11_info, c11_b_emlrt_marshallOut(1363717468U), "fileTimeLo",
                  "fileTimeLo", 2);
  sf_mex_addfield(*c11_info, c11_b_emlrt_marshallOut(0U), "fileTimeHi",
                  "fileTimeHi", 2);
  sf_mex_addfield(*c11_info, c11_b_emlrt_marshallOut(0U), "mFileTimeLo",
                  "mFileTimeLo", 2);
  sf_mex_addfield(*c11_info, c11_b_emlrt_marshallOut(0U), "mFileTimeHi",
                  "mFileTimeHi", 2);
  sf_mex_assign(&c11_rhs2, sf_mex_createcellmatrix(0, 1), false);
  sf_mex_assign(&c11_lhs2, sf_mex_createcellmatrix(0, 1), false);
  sf_mex_addfield(*c11_info, sf_mex_duplicatearraysafe(&c11_rhs2), "rhs", "rhs",
                  2);
  sf_mex_addfield(*c11_info, sf_mex_duplicatearraysafe(&c11_lhs2), "lhs", "lhs",
                  2);
  sf_mex_addfield(*c11_info, c11_emlrt_marshallOut(
    "[ILXE]$matlabroot$/toolbox/eml/lib/matlab/matfun/norm.m!genpnorm"),
                  "context", "context", 3);
  sf_mex_addfield(*c11_info, c11_emlrt_marshallOut("eml_index_class"), "name",
                  "name", 3);
  sf_mex_addfield(*c11_info, c11_emlrt_marshallOut(""), "dominantType",
                  "dominantType", 3);
  sf_mex_addfield(*c11_info, c11_emlrt_marshallOut(
    "[ILXE]$matlabroot$/toolbox/eml/lib/matlab/eml/eml_index_class.m"),
                  "resolved", "resolved", 3);
  sf_mex_addfield(*c11_info, c11_b_emlrt_marshallOut(1323174178U), "fileTimeLo",
                  "fileTimeLo", 3);
  sf_mex_addfield(*c11_info, c11_b_emlrt_marshallOut(0U), "fileTimeHi",
                  "fileTimeHi", 3);
  sf_mex_addfield(*c11_info, c11_b_emlrt_marshallOut(0U), "mFileTimeLo",
                  "mFileTimeLo", 3);
  sf_mex_addfield(*c11_info, c11_b_emlrt_marshallOut(0U), "mFileTimeHi",
                  "mFileTimeHi", 3);
  sf_mex_assign(&c11_rhs3, sf_mex_createcellmatrix(0, 1), false);
  sf_mex_assign(&c11_lhs3, sf_mex_createcellmatrix(0, 1), false);
  sf_mex_addfield(*c11_info, sf_mex_duplicatearraysafe(&c11_rhs3), "rhs", "rhs",
                  3);
  sf_mex_addfield(*c11_info, sf_mex_duplicatearraysafe(&c11_lhs3), "lhs", "lhs",
                  3);
  sf_mex_addfield(*c11_info, c11_emlrt_marshallOut(
    "[ILXE]$matlabroot$/toolbox/eml/lib/matlab/matfun/norm.m!genpnorm"),
                  "context", "context", 4);
  sf_mex_addfield(*c11_info, c11_emlrt_marshallOut(
    "coder.internal.isBuiltInNumeric"), "name", "name", 4);
  sf_mex_addfield(*c11_info, c11_emlrt_marshallOut("double"), "dominantType",
                  "dominantType", 4);
  sf_mex_addfield(*c11_info, c11_emlrt_marshallOut(
    "[ILXE]$matlabroot$/toolbox/shared/coder/coder/+coder/+internal/isBuiltInNumeric.m"),
                  "resolved", "resolved", 4);
  sf_mex_addfield(*c11_info, c11_b_emlrt_marshallOut(1395935456U), "fileTimeLo",
                  "fileTimeLo", 4);
  sf_mex_addfield(*c11_info, c11_b_emlrt_marshallOut(0U), "fileTimeHi",
                  "fileTimeHi", 4);
  sf_mex_addfield(*c11_info, c11_b_emlrt_marshallOut(0U), "mFileTimeLo",
                  "mFileTimeLo", 4);
  sf_mex_addfield(*c11_info, c11_b_emlrt_marshallOut(0U), "mFileTimeHi",
                  "mFileTimeHi", 4);
  sf_mex_assign(&c11_rhs4, sf_mex_createcellmatrix(0, 1), false);
  sf_mex_assign(&c11_lhs4, sf_mex_createcellmatrix(0, 1), false);
  sf_mex_addfield(*c11_info, sf_mex_duplicatearraysafe(&c11_rhs4), "rhs", "rhs",
                  4);
  sf_mex_addfield(*c11_info, sf_mex_duplicatearraysafe(&c11_lhs4), "lhs", "lhs",
                  4);
  sf_mex_addfield(*c11_info, c11_emlrt_marshallOut(
    "[ILXE]$matlabroot$/toolbox/eml/lib/matlab/matfun/norm.m!genpnorm"),
                  "context", "context", 5);
  sf_mex_addfield(*c11_info, c11_emlrt_marshallOut("eml_xnrm2"), "name", "name",
                  5);
  sf_mex_addfield(*c11_info, c11_emlrt_marshallOut("single"), "dominantType",
                  "dominantType", 5);
  sf_mex_addfield(*c11_info, c11_emlrt_marshallOut(
    "[ILXE]$matlabroot$/toolbox/eml/lib/matlab/eml/blas/eml_xnrm2.m"),
                  "resolved", "resolved", 5);
  sf_mex_addfield(*c11_info, c11_b_emlrt_marshallOut(1375984292U), "fileTimeLo",
                  "fileTimeLo", 5);
  sf_mex_addfield(*c11_info, c11_b_emlrt_marshallOut(0U), "fileTimeHi",
                  "fileTimeHi", 5);
  sf_mex_addfield(*c11_info, c11_b_emlrt_marshallOut(0U), "mFileTimeLo",
                  "mFileTimeLo", 5);
  sf_mex_addfield(*c11_info, c11_b_emlrt_marshallOut(0U), "mFileTimeHi",
                  "mFileTimeHi", 5);
  sf_mex_assign(&c11_rhs5, sf_mex_createcellmatrix(0, 1), false);
  sf_mex_assign(&c11_lhs5, sf_mex_createcellmatrix(0, 1), false);
  sf_mex_addfield(*c11_info, sf_mex_duplicatearraysafe(&c11_rhs5), "rhs", "rhs",
                  5);
  sf_mex_addfield(*c11_info, sf_mex_duplicatearraysafe(&c11_lhs5), "lhs", "lhs",
                  5);
  sf_mex_addfield(*c11_info, c11_emlrt_marshallOut(
    "[ILXE]$matlabroot$/toolbox/eml/lib/matlab/eml/blas/eml_xnrm2.m"), "context",
                  "context", 6);
  sf_mex_addfield(*c11_info, c11_emlrt_marshallOut("coder.internal.blas.inline"),
                  "name", "name", 6);
  sf_mex_addfield(*c11_info, c11_emlrt_marshallOut(""), "dominantType",
                  "dominantType", 6);
  sf_mex_addfield(*c11_info, c11_emlrt_marshallOut(
    "[ILXE]$matlabroot$/toolbox/coder/coder/+coder/+internal/+blas/inline.p"),
                  "resolved", "resolved", 6);
  sf_mex_addfield(*c11_info, c11_b_emlrt_marshallOut(1410811372U), "fileTimeLo",
                  "fileTimeLo", 6);
  sf_mex_addfield(*c11_info, c11_b_emlrt_marshallOut(0U), "fileTimeHi",
                  "fileTimeHi", 6);
  sf_mex_addfield(*c11_info, c11_b_emlrt_marshallOut(0U), "mFileTimeLo",
                  "mFileTimeLo", 6);
  sf_mex_addfield(*c11_info, c11_b_emlrt_marshallOut(0U), "mFileTimeHi",
                  "mFileTimeHi", 6);
  sf_mex_assign(&c11_rhs6, sf_mex_createcellmatrix(0, 1), false);
  sf_mex_assign(&c11_lhs6, sf_mex_createcellmatrix(0, 1), false);
  sf_mex_addfield(*c11_info, sf_mex_duplicatearraysafe(&c11_rhs6), "rhs", "rhs",
                  6);
  sf_mex_addfield(*c11_info, sf_mex_duplicatearraysafe(&c11_lhs6), "lhs", "lhs",
                  6);
  sf_mex_addfield(*c11_info, c11_emlrt_marshallOut(
    "[ILXE]$matlabroot$/toolbox/eml/lib/matlab/eml/blas/eml_xnrm2.m"), "context",
                  "context", 7);
  sf_mex_addfield(*c11_info, c11_emlrt_marshallOut("coder.internal.blas.xnrm2"),
                  "name", "name", 7);
  sf_mex_addfield(*c11_info, c11_emlrt_marshallOut("single"), "dominantType",
                  "dominantType", 7);
  sf_mex_addfield(*c11_info, c11_emlrt_marshallOut(
    "[ILXE]$matlabroot$/toolbox/coder/coder/+coder/+internal/+blas/xnrm2.p"),
                  "resolved", "resolved", 7);
  sf_mex_addfield(*c11_info, c11_b_emlrt_marshallOut(1410811370U), "fileTimeLo",
                  "fileTimeLo", 7);
  sf_mex_addfield(*c11_info, c11_b_emlrt_marshallOut(0U), "fileTimeHi",
                  "fileTimeHi", 7);
  sf_mex_addfield(*c11_info, c11_b_emlrt_marshallOut(0U), "mFileTimeLo",
                  "mFileTimeLo", 7);
  sf_mex_addfield(*c11_info, c11_b_emlrt_marshallOut(0U), "mFileTimeHi",
                  "mFileTimeHi", 7);
  sf_mex_assign(&c11_rhs7, sf_mex_createcellmatrix(0, 1), false);
  sf_mex_assign(&c11_lhs7, sf_mex_createcellmatrix(0, 1), false);
  sf_mex_addfield(*c11_info, sf_mex_duplicatearraysafe(&c11_rhs7), "rhs", "rhs",
                  7);
  sf_mex_addfield(*c11_info, sf_mex_duplicatearraysafe(&c11_lhs7), "lhs", "lhs",
                  7);
  sf_mex_addfield(*c11_info, c11_emlrt_marshallOut(
    "[ILXE]$matlabroot$/toolbox/coder/coder/+coder/+internal/+blas/xnrm2.p"),
                  "context", "context", 8);
  sf_mex_addfield(*c11_info, c11_emlrt_marshallOut(
    "coder.internal.blas.use_refblas"), "name", "name", 8);
  sf_mex_addfield(*c11_info, c11_emlrt_marshallOut(""), "dominantType",
                  "dominantType", 8);
  sf_mex_addfield(*c11_info, c11_emlrt_marshallOut(
    "[ILXE]$matlabroot$/toolbox/coder/coder/+coder/+internal/+blas/use_refblas.p"),
                  "resolved", "resolved", 8);
  sf_mex_addfield(*c11_info, c11_b_emlrt_marshallOut(1410811370U), "fileTimeLo",
                  "fileTimeLo", 8);
  sf_mex_addfield(*c11_info, c11_b_emlrt_marshallOut(0U), "fileTimeHi",
                  "fileTimeHi", 8);
  sf_mex_addfield(*c11_info, c11_b_emlrt_marshallOut(0U), "mFileTimeLo",
                  "mFileTimeLo", 8);
  sf_mex_addfield(*c11_info, c11_b_emlrt_marshallOut(0U), "mFileTimeHi",
                  "mFileTimeHi", 8);
  sf_mex_assign(&c11_rhs8, sf_mex_createcellmatrix(0, 1), false);
  sf_mex_assign(&c11_lhs8, sf_mex_createcellmatrix(0, 1), false);
  sf_mex_addfield(*c11_info, sf_mex_duplicatearraysafe(&c11_rhs8), "rhs", "rhs",
                  8);
  sf_mex_addfield(*c11_info, sf_mex_duplicatearraysafe(&c11_lhs8), "lhs", "lhs",
                  8);
  sf_mex_addfield(*c11_info, c11_emlrt_marshallOut(
    "[ILXE]$matlabroot$/toolbox/coder/coder/+coder/+internal/+blas/xnrm2.p!below_threshold"),
                  "context", "context", 9);
  sf_mex_addfield(*c11_info, c11_emlrt_marshallOut(
    "coder.internal.blas.threshold"), "name", "name", 9);
  sf_mex_addfield(*c11_info, c11_emlrt_marshallOut("char"), "dominantType",
                  "dominantType", 9);
  sf_mex_addfield(*c11_info, c11_emlrt_marshallOut(
    "[ILXE]$matlabroot$/toolbox/coder/coder/+coder/+internal/+blas/threshold.p"),
                  "resolved", "resolved", 9);
  sf_mex_addfield(*c11_info, c11_b_emlrt_marshallOut(1410811372U), "fileTimeLo",
                  "fileTimeLo", 9);
  sf_mex_addfield(*c11_info, c11_b_emlrt_marshallOut(0U), "fileTimeHi",
                  "fileTimeHi", 9);
  sf_mex_addfield(*c11_info, c11_b_emlrt_marshallOut(0U), "mFileTimeLo",
                  "mFileTimeLo", 9);
  sf_mex_addfield(*c11_info, c11_b_emlrt_marshallOut(0U), "mFileTimeHi",
                  "mFileTimeHi", 9);
  sf_mex_assign(&c11_rhs9, sf_mex_createcellmatrix(0, 1), false);
  sf_mex_assign(&c11_lhs9, sf_mex_createcellmatrix(0, 1), false);
  sf_mex_addfield(*c11_info, sf_mex_duplicatearraysafe(&c11_rhs9), "rhs", "rhs",
                  9);
  sf_mex_addfield(*c11_info, sf_mex_duplicatearraysafe(&c11_lhs9), "lhs", "lhs",
                  9);
  sf_mex_addfield(*c11_info, c11_emlrt_marshallOut(
    "[ILXE]$matlabroot$/toolbox/coder/coder/+coder/+internal/+blas/threshold.p"),
                  "context", "context", 10);
  sf_mex_addfield(*c11_info, c11_emlrt_marshallOut("eml_switch_helper"), "name",
                  "name", 10);
  sf_mex_addfield(*c11_info, c11_emlrt_marshallOut(""), "dominantType",
                  "dominantType", 10);
  sf_mex_addfield(*c11_info, c11_emlrt_marshallOut(
    "[ILXE]$matlabroot$/toolbox/eml/lib/matlab/eml/eml_switch_helper.m"),
                  "resolved", "resolved", 10);
  sf_mex_addfield(*c11_info, c11_b_emlrt_marshallOut(1393334458U), "fileTimeLo",
                  "fileTimeLo", 10);
  sf_mex_addfield(*c11_info, c11_b_emlrt_marshallOut(0U), "fileTimeHi",
                  "fileTimeHi", 10);
  sf_mex_addfield(*c11_info, c11_b_emlrt_marshallOut(0U), "mFileTimeLo",
                  "mFileTimeLo", 10);
  sf_mex_addfield(*c11_info, c11_b_emlrt_marshallOut(0U), "mFileTimeHi",
                  "mFileTimeHi", 10);
  sf_mex_assign(&c11_rhs10, sf_mex_createcellmatrix(0, 1), false);
  sf_mex_assign(&c11_lhs10, sf_mex_createcellmatrix(0, 1), false);
  sf_mex_addfield(*c11_info, sf_mex_duplicatearraysafe(&c11_rhs10), "rhs", "rhs",
                  10);
  sf_mex_addfield(*c11_info, sf_mex_duplicatearraysafe(&c11_lhs10), "lhs", "lhs",
                  10);
  sf_mex_addfield(*c11_info, c11_emlrt_marshallOut(
    "[ILXE]$matlabroot$/toolbox/coder/coder/+coder/+internal/+blas/xnrm2.p"),
                  "context", "context", 11);
  sf_mex_addfield(*c11_info, c11_emlrt_marshallOut(
    "coder.internal.refblas.xnrm2"), "name", "name", 11);
  sf_mex_addfield(*c11_info, c11_emlrt_marshallOut("single"), "dominantType",
                  "dominantType", 11);
  sf_mex_addfield(*c11_info, c11_emlrt_marshallOut(
    "[ILXE]$matlabroot$/toolbox/coder/coder/+coder/+internal/+refblas/xnrm2.p"),
                  "resolved", "resolved", 11);
  sf_mex_addfield(*c11_info, c11_b_emlrt_marshallOut(1410811372U), "fileTimeLo",
                  "fileTimeLo", 11);
  sf_mex_addfield(*c11_info, c11_b_emlrt_marshallOut(0U), "fileTimeHi",
                  "fileTimeHi", 11);
  sf_mex_addfield(*c11_info, c11_b_emlrt_marshallOut(0U), "mFileTimeLo",
                  "mFileTimeLo", 11);
  sf_mex_addfield(*c11_info, c11_b_emlrt_marshallOut(0U), "mFileTimeHi",
                  "mFileTimeHi", 11);
  sf_mex_assign(&c11_rhs11, sf_mex_createcellmatrix(0, 1), false);
  sf_mex_assign(&c11_lhs11, sf_mex_createcellmatrix(0, 1), false);
  sf_mex_addfield(*c11_info, sf_mex_duplicatearraysafe(&c11_rhs11), "rhs", "rhs",
                  11);
  sf_mex_addfield(*c11_info, sf_mex_duplicatearraysafe(&c11_lhs11), "lhs", "lhs",
                  11);
  sf_mex_addfield(*c11_info, c11_emlrt_marshallOut(
    "[ILXE]$matlabroot$/toolbox/coder/coder/+coder/+internal/+refblas/xnrm2.p"),
                  "context", "context", 12);
  sf_mex_addfield(*c11_info, c11_emlrt_marshallOut("realmin"), "name", "name",
                  12);
  sf_mex_addfield(*c11_info, c11_emlrt_marshallOut("char"), "dominantType",
                  "dominantType", 12);
  sf_mex_addfield(*c11_info, c11_emlrt_marshallOut(
    "[ILXE]$matlabroot$/toolbox/eml/lib/matlab/elmat/realmin.m"), "resolved",
                  "resolved", 12);
  sf_mex_addfield(*c11_info, c11_b_emlrt_marshallOut(1307654842U), "fileTimeLo",
                  "fileTimeLo", 12);
  sf_mex_addfield(*c11_info, c11_b_emlrt_marshallOut(0U), "fileTimeHi",
                  "fileTimeHi", 12);
  sf_mex_addfield(*c11_info, c11_b_emlrt_marshallOut(0U), "mFileTimeLo",
                  "mFileTimeLo", 12);
  sf_mex_addfield(*c11_info, c11_b_emlrt_marshallOut(0U), "mFileTimeHi",
                  "mFileTimeHi", 12);
  sf_mex_assign(&c11_rhs12, sf_mex_createcellmatrix(0, 1), false);
  sf_mex_assign(&c11_lhs12, sf_mex_createcellmatrix(0, 1), false);
  sf_mex_addfield(*c11_info, sf_mex_duplicatearraysafe(&c11_rhs12), "rhs", "rhs",
                  12);
  sf_mex_addfield(*c11_info, sf_mex_duplicatearraysafe(&c11_lhs12), "lhs", "lhs",
                  12);
  sf_mex_addfield(*c11_info, c11_emlrt_marshallOut(
    "[ILXE]$matlabroot$/toolbox/eml/lib/matlab/elmat/realmin.m"), "context",
                  "context", 13);
  sf_mex_addfield(*c11_info, c11_emlrt_marshallOut("eml_realmin"), "name",
                  "name", 13);
  sf_mex_addfield(*c11_info, c11_emlrt_marshallOut("char"), "dominantType",
                  "dominantType", 13);
  sf_mex_addfield(*c11_info, c11_emlrt_marshallOut(
    "[ILXE]$matlabroot$/toolbox/eml/lib/matlab/eml/eml_realmin.m"), "resolved",
                  "resolved", 13);
  sf_mex_addfield(*c11_info, c11_b_emlrt_marshallOut(1307654844U), "fileTimeLo",
                  "fileTimeLo", 13);
  sf_mex_addfield(*c11_info, c11_b_emlrt_marshallOut(0U), "fileTimeHi",
                  "fileTimeHi", 13);
  sf_mex_addfield(*c11_info, c11_b_emlrt_marshallOut(0U), "mFileTimeLo",
                  "mFileTimeLo", 13);
  sf_mex_addfield(*c11_info, c11_b_emlrt_marshallOut(0U), "mFileTimeHi",
                  "mFileTimeHi", 13);
  sf_mex_assign(&c11_rhs13, sf_mex_createcellmatrix(0, 1), false);
  sf_mex_assign(&c11_lhs13, sf_mex_createcellmatrix(0, 1), false);
  sf_mex_addfield(*c11_info, sf_mex_duplicatearraysafe(&c11_rhs13), "rhs", "rhs",
                  13);
  sf_mex_addfield(*c11_info, sf_mex_duplicatearraysafe(&c11_lhs13), "lhs", "lhs",
                  13);
  sf_mex_addfield(*c11_info, c11_emlrt_marshallOut(
    "[ILXE]$matlabroot$/toolbox/eml/lib/matlab/eml/eml_realmin.m"), "context",
                  "context", 14);
  sf_mex_addfield(*c11_info, c11_emlrt_marshallOut("eml_float_model"), "name",
                  "name", 14);
  sf_mex_addfield(*c11_info, c11_emlrt_marshallOut("char"), "dominantType",
                  "dominantType", 14);
  sf_mex_addfield(*c11_info, c11_emlrt_marshallOut(
    "[ILXE]$matlabroot$/toolbox/eml/lib/matlab/eml/eml_float_model.m"),
                  "resolved", "resolved", 14);
  sf_mex_addfield(*c11_info, c11_b_emlrt_marshallOut(1326731596U), "fileTimeLo",
                  "fileTimeLo", 14);
  sf_mex_addfield(*c11_info, c11_b_emlrt_marshallOut(0U), "fileTimeHi",
                  "fileTimeHi", 14);
  sf_mex_addfield(*c11_info, c11_b_emlrt_marshallOut(0U), "mFileTimeLo",
                  "mFileTimeLo", 14);
  sf_mex_addfield(*c11_info, c11_b_emlrt_marshallOut(0U), "mFileTimeHi",
                  "mFileTimeHi", 14);
  sf_mex_assign(&c11_rhs14, sf_mex_createcellmatrix(0, 1), false);
  sf_mex_assign(&c11_lhs14, sf_mex_createcellmatrix(0, 1), false);
  sf_mex_addfield(*c11_info, sf_mex_duplicatearraysafe(&c11_rhs14), "rhs", "rhs",
                  14);
  sf_mex_addfield(*c11_info, sf_mex_duplicatearraysafe(&c11_lhs14), "lhs", "lhs",
                  14);
  sf_mex_addfield(*c11_info, c11_emlrt_marshallOut(
    "[ILXE]$matlabroot$/toolbox/coder/coder/+coder/+internal/+refblas/xnrm2.p"),
                  "context", "context", 15);
  sf_mex_addfield(*c11_info, c11_emlrt_marshallOut("coder.internal.indexMinus"),
                  "name", "name", 15);
  sf_mex_addfield(*c11_info, c11_emlrt_marshallOut("double"), "dominantType",
                  "dominantType", 15);
  sf_mex_addfield(*c11_info, c11_emlrt_marshallOut(
    "[ILXE]$matlabroot$/toolbox/shared/coder/coder/+coder/+internal/indexMinus.m"),
                  "resolved", "resolved", 15);
  sf_mex_addfield(*c11_info, c11_b_emlrt_marshallOut(1372586760U), "fileTimeLo",
                  "fileTimeLo", 15);
  sf_mex_addfield(*c11_info, c11_b_emlrt_marshallOut(0U), "fileTimeHi",
                  "fileTimeHi", 15);
  sf_mex_addfield(*c11_info, c11_b_emlrt_marshallOut(0U), "mFileTimeLo",
                  "mFileTimeLo", 15);
  sf_mex_addfield(*c11_info, c11_b_emlrt_marshallOut(0U), "mFileTimeHi",
                  "mFileTimeHi", 15);
  sf_mex_assign(&c11_rhs15, sf_mex_createcellmatrix(0, 1), false);
  sf_mex_assign(&c11_lhs15, sf_mex_createcellmatrix(0, 1), false);
  sf_mex_addfield(*c11_info, sf_mex_duplicatearraysafe(&c11_rhs15), "rhs", "rhs",
                  15);
  sf_mex_addfield(*c11_info, sf_mex_duplicatearraysafe(&c11_lhs15), "lhs", "lhs",
                  15);
  sf_mex_addfield(*c11_info, c11_emlrt_marshallOut(
    "[ILXE]$matlabroot$/toolbox/coder/coder/+coder/+internal/+refblas/xnrm2.p"),
                  "context", "context", 16);
  sf_mex_addfield(*c11_info, c11_emlrt_marshallOut("coder.internal.indexTimes"),
                  "name", "name", 16);
  sf_mex_addfield(*c11_info, c11_emlrt_marshallOut("coder.internal.indexInt"),
                  "dominantType", "dominantType", 16);
  sf_mex_addfield(*c11_info, c11_emlrt_marshallOut(
    "[ILXE]$matlabroot$/toolbox/shared/coder/coder/+coder/+internal/indexTimes.m"),
                  "resolved", "resolved", 16);
  sf_mex_addfield(*c11_info, c11_b_emlrt_marshallOut(1372586760U), "fileTimeLo",
                  "fileTimeLo", 16);
  sf_mex_addfield(*c11_info, c11_b_emlrt_marshallOut(0U), "fileTimeHi",
                  "fileTimeHi", 16);
  sf_mex_addfield(*c11_info, c11_b_emlrt_marshallOut(0U), "mFileTimeLo",
                  "mFileTimeLo", 16);
  sf_mex_addfield(*c11_info, c11_b_emlrt_marshallOut(0U), "mFileTimeHi",
                  "mFileTimeHi", 16);
  sf_mex_assign(&c11_rhs16, sf_mex_createcellmatrix(0, 1), false);
  sf_mex_assign(&c11_lhs16, sf_mex_createcellmatrix(0, 1), false);
  sf_mex_addfield(*c11_info, sf_mex_duplicatearraysafe(&c11_rhs16), "rhs", "rhs",
                  16);
  sf_mex_addfield(*c11_info, sf_mex_duplicatearraysafe(&c11_lhs16), "lhs", "lhs",
                  16);
  sf_mex_addfield(*c11_info, c11_emlrt_marshallOut(
    "[ILXE]$matlabroot$/toolbox/coder/coder/+coder/+internal/+refblas/xnrm2.p"),
                  "context", "context", 17);
  sf_mex_addfield(*c11_info, c11_emlrt_marshallOut("coder.internal.indexPlus"),
                  "name", "name", 17);
  sf_mex_addfield(*c11_info, c11_emlrt_marshallOut("coder.internal.indexInt"),
                  "dominantType", "dominantType", 17);
  sf_mex_addfield(*c11_info, c11_emlrt_marshallOut(
    "[ILXE]$matlabroot$/toolbox/shared/coder/coder/+coder/+internal/indexPlus.m"),
                  "resolved", "resolved", 17);
  sf_mex_addfield(*c11_info, c11_b_emlrt_marshallOut(1372586760U), "fileTimeLo",
                  "fileTimeLo", 17);
  sf_mex_addfield(*c11_info, c11_b_emlrt_marshallOut(0U), "fileTimeHi",
                  "fileTimeHi", 17);
  sf_mex_addfield(*c11_info, c11_b_emlrt_marshallOut(0U), "mFileTimeLo",
                  "mFileTimeLo", 17);
  sf_mex_addfield(*c11_info, c11_b_emlrt_marshallOut(0U), "mFileTimeHi",
                  "mFileTimeHi", 17);
  sf_mex_assign(&c11_rhs17, sf_mex_createcellmatrix(0, 1), false);
  sf_mex_assign(&c11_lhs17, sf_mex_createcellmatrix(0, 1), false);
  sf_mex_addfield(*c11_info, sf_mex_duplicatearraysafe(&c11_rhs17), "rhs", "rhs",
                  17);
  sf_mex_addfield(*c11_info, sf_mex_duplicatearraysafe(&c11_lhs17), "lhs", "lhs",
                  17);
  sf_mex_addfield(*c11_info, c11_emlrt_marshallOut(
    "[ILXE]$matlabroot$/toolbox/coder/coder/+coder/+internal/+refblas/xnrm2.p"),
                  "context", "context", 18);
  sf_mex_addfield(*c11_info, c11_emlrt_marshallOut(
    "eml_int_forloop_overflow_check"), "name", "name", 18);
  sf_mex_addfield(*c11_info, c11_emlrt_marshallOut(""), "dominantType",
                  "dominantType", 18);
  sf_mex_addfield(*c11_info, c11_emlrt_marshallOut(
    "[ILXE]$matlabroot$/toolbox/eml/lib/matlab/eml/eml_int_forloop_overflow_check.m"),
                  "resolved", "resolved", 18);
  sf_mex_addfield(*c11_info, c11_b_emlrt_marshallOut(1397261022U), "fileTimeLo",
                  "fileTimeLo", 18);
  sf_mex_addfield(*c11_info, c11_b_emlrt_marshallOut(0U), "fileTimeHi",
                  "fileTimeHi", 18);
  sf_mex_addfield(*c11_info, c11_b_emlrt_marshallOut(0U), "mFileTimeLo",
                  "mFileTimeLo", 18);
  sf_mex_addfield(*c11_info, c11_b_emlrt_marshallOut(0U), "mFileTimeHi",
                  "mFileTimeHi", 18);
  sf_mex_assign(&c11_rhs18, sf_mex_createcellmatrix(0, 1), false);
  sf_mex_assign(&c11_lhs18, sf_mex_createcellmatrix(0, 1), false);
  sf_mex_addfield(*c11_info, sf_mex_duplicatearraysafe(&c11_rhs18), "rhs", "rhs",
                  18);
  sf_mex_addfield(*c11_info, sf_mex_duplicatearraysafe(&c11_lhs18), "lhs", "lhs",
                  18);
  sf_mex_addfield(*c11_info, c11_emlrt_marshallOut(
    "[ILXE]$matlabroot$/toolbox/eml/lib/matlab/eml/eml_int_forloop_overflow_check.m!eml_int_forloop_overflow_check_helper"),
                  "context", "context", 19);
  sf_mex_addfield(*c11_info, c11_emlrt_marshallOut("isfi"), "name", "name", 19);
  sf_mex_addfield(*c11_info, c11_emlrt_marshallOut("coder.internal.indexInt"),
                  "dominantType", "dominantType", 19);
  sf_mex_addfield(*c11_info, c11_emlrt_marshallOut(
    "[ILXE]$matlabroot$/toolbox/eml/lib/fixedpoint/isfi.m"), "resolved",
                  "resolved", 19);
  sf_mex_addfield(*c11_info, c11_b_emlrt_marshallOut(1346513958U), "fileTimeLo",
                  "fileTimeLo", 19);
  sf_mex_addfield(*c11_info, c11_b_emlrt_marshallOut(0U), "fileTimeHi",
                  "fileTimeHi", 19);
  sf_mex_addfield(*c11_info, c11_b_emlrt_marshallOut(0U), "mFileTimeLo",
                  "mFileTimeLo", 19);
  sf_mex_addfield(*c11_info, c11_b_emlrt_marshallOut(0U), "mFileTimeHi",
                  "mFileTimeHi", 19);
  sf_mex_assign(&c11_rhs19, sf_mex_createcellmatrix(0, 1), false);
  sf_mex_assign(&c11_lhs19, sf_mex_createcellmatrix(0, 1), false);
  sf_mex_addfield(*c11_info, sf_mex_duplicatearraysafe(&c11_rhs19), "rhs", "rhs",
                  19);
  sf_mex_addfield(*c11_info, sf_mex_duplicatearraysafe(&c11_lhs19), "lhs", "lhs",
                  19);
  sf_mex_addfield(*c11_info, c11_emlrt_marshallOut(
    "[ILXE]$matlabroot$/toolbox/eml/lib/fixedpoint/isfi.m"), "context",
                  "context", 20);
  sf_mex_addfield(*c11_info, c11_emlrt_marshallOut("isnumerictype"), "name",
                  "name", 20);
  sf_mex_addfield(*c11_info, c11_emlrt_marshallOut("char"), "dominantType",
                  "dominantType", 20);
  sf_mex_addfield(*c11_info, c11_emlrt_marshallOut(
    "[ILXE]$matlabroot$/toolbox/eml/lib/fixedpoint/isnumerictype.m"), "resolved",
                  "resolved", 20);
  sf_mex_addfield(*c11_info, c11_b_emlrt_marshallOut(1398879198U), "fileTimeLo",
                  "fileTimeLo", 20);
  sf_mex_addfield(*c11_info, c11_b_emlrt_marshallOut(0U), "fileTimeHi",
                  "fileTimeHi", 20);
  sf_mex_addfield(*c11_info, c11_b_emlrt_marshallOut(0U), "mFileTimeLo",
                  "mFileTimeLo", 20);
  sf_mex_addfield(*c11_info, c11_b_emlrt_marshallOut(0U), "mFileTimeHi",
                  "mFileTimeHi", 20);
  sf_mex_assign(&c11_rhs20, sf_mex_createcellmatrix(0, 1), false);
  sf_mex_assign(&c11_lhs20, sf_mex_createcellmatrix(0, 1), false);
  sf_mex_addfield(*c11_info, sf_mex_duplicatearraysafe(&c11_rhs20), "rhs", "rhs",
                  20);
  sf_mex_addfield(*c11_info, sf_mex_duplicatearraysafe(&c11_lhs20), "lhs", "lhs",
                  20);
  sf_mex_addfield(*c11_info, c11_emlrt_marshallOut(
    "[ILXE]$matlabroot$/toolbox/eml/lib/matlab/eml/eml_int_forloop_overflow_check.m!eml_int_forloop_overflow_check_helper"),
                  "context", "context", 21);
  sf_mex_addfield(*c11_info, c11_emlrt_marshallOut("intmax"), "name", "name", 21);
  sf_mex_addfield(*c11_info, c11_emlrt_marshallOut("char"), "dominantType",
                  "dominantType", 21);
  sf_mex_addfield(*c11_info, c11_emlrt_marshallOut(
    "[ILXE]$matlabroot$/toolbox/eml/lib/matlab/elmat/intmax.m"), "resolved",
                  "resolved", 21);
  sf_mex_addfield(*c11_info, c11_b_emlrt_marshallOut(1362265482U), "fileTimeLo",
                  "fileTimeLo", 21);
  sf_mex_addfield(*c11_info, c11_b_emlrt_marshallOut(0U), "fileTimeHi",
                  "fileTimeHi", 21);
  sf_mex_addfield(*c11_info, c11_b_emlrt_marshallOut(0U), "mFileTimeLo",
                  "mFileTimeLo", 21);
  sf_mex_addfield(*c11_info, c11_b_emlrt_marshallOut(0U), "mFileTimeHi",
                  "mFileTimeHi", 21);
  sf_mex_assign(&c11_rhs21, sf_mex_createcellmatrix(0, 1), false);
  sf_mex_assign(&c11_lhs21, sf_mex_createcellmatrix(0, 1), false);
  sf_mex_addfield(*c11_info, sf_mex_duplicatearraysafe(&c11_rhs21), "rhs", "rhs",
                  21);
  sf_mex_addfield(*c11_info, sf_mex_duplicatearraysafe(&c11_lhs21), "lhs", "lhs",
                  21);
  sf_mex_addfield(*c11_info, c11_emlrt_marshallOut(
    "[ILXE]$matlabroot$/toolbox/eml/lib/matlab/eml/eml_int_forloop_overflow_check.m!eml_int_forloop_overflow_check_helper"),
                  "context", "context", 22);
  sf_mex_addfield(*c11_info, c11_emlrt_marshallOut("intmin"), "name", "name", 22);
  sf_mex_addfield(*c11_info, c11_emlrt_marshallOut("char"), "dominantType",
                  "dominantType", 22);
  sf_mex_addfield(*c11_info, c11_emlrt_marshallOut(
    "[ILXE]$matlabroot$/toolbox/eml/lib/matlab/elmat/intmin.m"), "resolved",
                  "resolved", 22);
  sf_mex_addfield(*c11_info, c11_b_emlrt_marshallOut(1362265482U), "fileTimeLo",
                  "fileTimeLo", 22);
  sf_mex_addfield(*c11_info, c11_b_emlrt_marshallOut(0U), "fileTimeHi",
                  "fileTimeHi", 22);
  sf_mex_addfield(*c11_info, c11_b_emlrt_marshallOut(0U), "mFileTimeLo",
                  "mFileTimeLo", 22);
  sf_mex_addfield(*c11_info, c11_b_emlrt_marshallOut(0U), "mFileTimeHi",
                  "mFileTimeHi", 22);
  sf_mex_assign(&c11_rhs22, sf_mex_createcellmatrix(0, 1), false);
  sf_mex_assign(&c11_lhs22, sf_mex_createcellmatrix(0, 1), false);
  sf_mex_addfield(*c11_info, sf_mex_duplicatearraysafe(&c11_rhs22), "rhs", "rhs",
                  22);
  sf_mex_addfield(*c11_info, sf_mex_duplicatearraysafe(&c11_lhs22), "lhs", "lhs",
                  22);
  sf_mex_addfield(*c11_info, c11_emlrt_marshallOut(
    "[ILXE]$matlabroot$/toolbox/eml/lib/matlab/elmat/intmin.m"), "context",
                  "context", 23);
  sf_mex_addfield(*c11_info, c11_emlrt_marshallOut("eml_switch_helper"), "name",
                  "name", 23);
  sf_mex_addfield(*c11_info, c11_emlrt_marshallOut(""), "dominantType",
                  "dominantType", 23);
  sf_mex_addfield(*c11_info, c11_emlrt_marshallOut(
    "[ILXE]$matlabroot$/toolbox/eml/lib/matlab/eml/eml_switch_helper.m"),
                  "resolved", "resolved", 23);
  sf_mex_addfield(*c11_info, c11_b_emlrt_marshallOut(1393334458U), "fileTimeLo",
                  "fileTimeLo", 23);
  sf_mex_addfield(*c11_info, c11_b_emlrt_marshallOut(0U), "fileTimeHi",
                  "fileTimeHi", 23);
  sf_mex_addfield(*c11_info, c11_b_emlrt_marshallOut(0U), "mFileTimeLo",
                  "mFileTimeLo", 23);
  sf_mex_addfield(*c11_info, c11_b_emlrt_marshallOut(0U), "mFileTimeHi",
                  "mFileTimeHi", 23);
  sf_mex_assign(&c11_rhs23, sf_mex_createcellmatrix(0, 1), false);
  sf_mex_assign(&c11_lhs23, sf_mex_createcellmatrix(0, 1), false);
  sf_mex_addfield(*c11_info, sf_mex_duplicatearraysafe(&c11_rhs23), "rhs", "rhs",
                  23);
  sf_mex_addfield(*c11_info, sf_mex_duplicatearraysafe(&c11_lhs23), "lhs", "lhs",
                  23);
  sf_mex_addfield(*c11_info, c11_emlrt_marshallOut(
    "[ILXE]$matlabroot$/toolbox/coder/coder/+coder/+internal/+refblas/xnrm2.p"),
                  "context", "context", 24);
  sf_mex_addfield(*c11_info, c11_emlrt_marshallOut("abs"), "name", "name", 24);
  sf_mex_addfield(*c11_info, c11_emlrt_marshallOut("single"), "dominantType",
                  "dominantType", 24);
  sf_mex_addfield(*c11_info, c11_emlrt_marshallOut(
    "[ILXE]$matlabroot$/toolbox/eml/lib/matlab/elfun/abs.m"), "resolved",
                  "resolved", 24);
  sf_mex_addfield(*c11_info, c11_b_emlrt_marshallOut(1363717452U), "fileTimeLo",
                  "fileTimeLo", 24);
  sf_mex_addfield(*c11_info, c11_b_emlrt_marshallOut(0U), "fileTimeHi",
                  "fileTimeHi", 24);
  sf_mex_addfield(*c11_info, c11_b_emlrt_marshallOut(0U), "mFileTimeLo",
                  "mFileTimeLo", 24);
  sf_mex_addfield(*c11_info, c11_b_emlrt_marshallOut(0U), "mFileTimeHi",
                  "mFileTimeHi", 24);
  sf_mex_assign(&c11_rhs24, sf_mex_createcellmatrix(0, 1), false);
  sf_mex_assign(&c11_lhs24, sf_mex_createcellmatrix(0, 1), false);
  sf_mex_addfield(*c11_info, sf_mex_duplicatearraysafe(&c11_rhs24), "rhs", "rhs",
                  24);
  sf_mex_addfield(*c11_info, sf_mex_duplicatearraysafe(&c11_lhs24), "lhs", "lhs",
                  24);
  sf_mex_addfield(*c11_info, c11_emlrt_marshallOut(
    "[ILXE]$matlabroot$/toolbox/eml/lib/matlab/elfun/abs.m"), "context",
                  "context", 25);
  sf_mex_addfield(*c11_info, c11_emlrt_marshallOut(
    "coder.internal.isBuiltInNumeric"), "name", "name", 25);
  sf_mex_addfield(*c11_info, c11_emlrt_marshallOut("single"), "dominantType",
                  "dominantType", 25);
  sf_mex_addfield(*c11_info, c11_emlrt_marshallOut(
    "[ILXE]$matlabroot$/toolbox/shared/coder/coder/+coder/+internal/isBuiltInNumeric.m"),
                  "resolved", "resolved", 25);
  sf_mex_addfield(*c11_info, c11_b_emlrt_marshallOut(1395935456U), "fileTimeLo",
                  "fileTimeLo", 25);
  sf_mex_addfield(*c11_info, c11_b_emlrt_marshallOut(0U), "fileTimeHi",
                  "fileTimeHi", 25);
  sf_mex_addfield(*c11_info, c11_b_emlrt_marshallOut(0U), "mFileTimeLo",
                  "mFileTimeLo", 25);
  sf_mex_addfield(*c11_info, c11_b_emlrt_marshallOut(0U), "mFileTimeHi",
                  "mFileTimeHi", 25);
  sf_mex_assign(&c11_rhs25, sf_mex_createcellmatrix(0, 1), false);
  sf_mex_assign(&c11_lhs25, sf_mex_createcellmatrix(0, 1), false);
  sf_mex_addfield(*c11_info, sf_mex_duplicatearraysafe(&c11_rhs25), "rhs", "rhs",
                  25);
  sf_mex_addfield(*c11_info, sf_mex_duplicatearraysafe(&c11_lhs25), "lhs", "lhs",
                  25);
  sf_mex_addfield(*c11_info, c11_emlrt_marshallOut(
    "[ILXE]$matlabroot$/toolbox/eml/lib/matlab/elfun/abs.m"), "context",
                  "context", 26);
  sf_mex_addfield(*c11_info, c11_emlrt_marshallOut("eml_scalar_abs"), "name",
                  "name", 26);
  sf_mex_addfield(*c11_info, c11_emlrt_marshallOut("single"), "dominantType",
                  "dominantType", 26);
  sf_mex_addfield(*c11_info, c11_emlrt_marshallOut(
    "[ILXE]$matlabroot$/toolbox/eml/lib/matlab/elfun/eml_scalar_abs.m"),
                  "resolved", "resolved", 26);
  sf_mex_addfield(*c11_info, c11_b_emlrt_marshallOut(1286822312U), "fileTimeLo",
                  "fileTimeLo", 26);
  sf_mex_addfield(*c11_info, c11_b_emlrt_marshallOut(0U), "fileTimeHi",
                  "fileTimeHi", 26);
  sf_mex_addfield(*c11_info, c11_b_emlrt_marshallOut(0U), "mFileTimeLo",
                  "mFileTimeLo", 26);
  sf_mex_addfield(*c11_info, c11_b_emlrt_marshallOut(0U), "mFileTimeHi",
                  "mFileTimeHi", 26);
  sf_mex_assign(&c11_rhs26, sf_mex_createcellmatrix(0, 1), false);
  sf_mex_assign(&c11_lhs26, sf_mex_createcellmatrix(0, 1), false);
  sf_mex_addfield(*c11_info, sf_mex_duplicatearraysafe(&c11_rhs26), "rhs", "rhs",
                  26);
  sf_mex_addfield(*c11_info, sf_mex_duplicatearraysafe(&c11_lhs26), "lhs", "lhs",
                  26);
  sf_mex_destroy(&c11_rhs0);
  sf_mex_destroy(&c11_lhs0);
  sf_mex_destroy(&c11_rhs1);
  sf_mex_destroy(&c11_lhs1);
  sf_mex_destroy(&c11_rhs2);
  sf_mex_destroy(&c11_lhs2);
  sf_mex_destroy(&c11_rhs3);
  sf_mex_destroy(&c11_lhs3);
  sf_mex_destroy(&c11_rhs4);
  sf_mex_destroy(&c11_lhs4);
  sf_mex_destroy(&c11_rhs5);
  sf_mex_destroy(&c11_lhs5);
  sf_mex_destroy(&c11_rhs6);
  sf_mex_destroy(&c11_lhs6);
  sf_mex_destroy(&c11_rhs7);
  sf_mex_destroy(&c11_lhs7);
  sf_mex_destroy(&c11_rhs8);
  sf_mex_destroy(&c11_lhs8);
  sf_mex_destroy(&c11_rhs9);
  sf_mex_destroy(&c11_lhs9);
  sf_mex_destroy(&c11_rhs10);
  sf_mex_destroy(&c11_lhs10);
  sf_mex_destroy(&c11_rhs11);
  sf_mex_destroy(&c11_lhs11);
  sf_mex_destroy(&c11_rhs12);
  sf_mex_destroy(&c11_lhs12);
  sf_mex_destroy(&c11_rhs13);
  sf_mex_destroy(&c11_lhs13);
  sf_mex_destroy(&c11_rhs14);
  sf_mex_destroy(&c11_lhs14);
  sf_mex_destroy(&c11_rhs15);
  sf_mex_destroy(&c11_lhs15);
  sf_mex_destroy(&c11_rhs16);
  sf_mex_destroy(&c11_lhs16);
  sf_mex_destroy(&c11_rhs17);
  sf_mex_destroy(&c11_lhs17);
  sf_mex_destroy(&c11_rhs18);
  sf_mex_destroy(&c11_lhs18);
  sf_mex_destroy(&c11_rhs19);
  sf_mex_destroy(&c11_lhs19);
  sf_mex_destroy(&c11_rhs20);
  sf_mex_destroy(&c11_lhs20);
  sf_mex_destroy(&c11_rhs21);
  sf_mex_destroy(&c11_lhs21);
  sf_mex_destroy(&c11_rhs22);
  sf_mex_destroy(&c11_lhs22);
  sf_mex_destroy(&c11_rhs23);
  sf_mex_destroy(&c11_lhs23);
  sf_mex_destroy(&c11_rhs24);
  sf_mex_destroy(&c11_lhs24);
  sf_mex_destroy(&c11_rhs25);
  sf_mex_destroy(&c11_lhs25);
  sf_mex_destroy(&c11_rhs26);
  sf_mex_destroy(&c11_lhs26);
}

static const mxArray *c11_emlrt_marshallOut(const char * c11_u)
{
  const mxArray *c11_y = NULL;
  c11_y = NULL;
  sf_mex_assign(&c11_y, sf_mex_create("y", c11_u, 15, 0U, 0U, 0U, 2, 1, strlen
    (c11_u)), false);
  return c11_y;
}

static const mxArray *c11_b_emlrt_marshallOut(const uint32_T c11_u)
{
  const mxArray *c11_y = NULL;
  c11_y = NULL;
  sf_mex_assign(&c11_y, sf_mex_create("y", &c11_u, 7, 0U, 0U, 0U, 0), false);
  return c11_y;
}

static void c11_calcStartPos(SFc11_greenOffenceInstanceStruct *chartInstance)
{
  uint32_T c11_debug_family_var_map[2];
  real_T c11_nargin = 0.0;
  real_T c11_nargout = 0.0;
  int32_T c11_i9;
  int32_T c11_i10;
  int32_T c11_i11;
  int32_T c11_i12;
  int32_T c11_i13;
  _SFD_SYMBOL_SCOPE_PUSH_EML(0U, 2U, 2U, c11_debug_family_names,
    c11_debug_family_var_map);
  _SFD_SYMBOL_SCOPE_ADD_EML_IMPORTABLE(&c11_nargin, 0U, c11_sf_marshallOut,
    c11_sf_marshallIn);
  _SFD_SYMBOL_SCOPE_ADD_EML_IMPORTABLE(&c11_nargout, 1U, c11_sf_marshallOut,
    c11_sf_marshallIn);
  CV_EML_FCN(3, 0);
  _SFD_EML_CALL(3U, chartInstance->c11_sfEvent, 2);
  switch (*(uint8_T *)&((char_T *)chartInstance->c11_me)[5]) {
   case 111U:
    CV_EML_SWITCH(3, 1, 0, 1);
    _SFD_EML_CALL(3U, chartInstance->c11_sfEvent, 4);
    for (c11_i9 = 0; c11_i9 < 2; c11_i9++) {
      chartInstance->c11_startingPos[c11_i9] = (int8_T)(10 + (int8_T)(-10 *
        (int8_T)c11_i9));
    }

    c11_updateDataWrittenToVector(chartInstance, 1U);
    break;

   case 100U:
    CV_EML_SWITCH(3, 1, 0, 2);
    _SFD_EML_CALL(3U, chartInstance->c11_sfEvent, 6);
    for (c11_i10 = 0; c11_i10 < 2; c11_i10++) {
      chartInstance->c11_startingPos[c11_i10] = (int8_T)(30 + (int8_T)(-30 *
        (int8_T)c11_i10));
    }

    c11_updateDataWrittenToVector(chartInstance, 1U);
    break;

   case 103U:
    CV_EML_SWITCH(3, 1, 0, 3);
    _SFD_EML_CALL(3U, chartInstance->c11_sfEvent, 8);
    for (c11_i11 = 0; c11_i11 < 2; c11_i11++) {
      chartInstance->c11_startingPos[c11_i11] = (int8_T)(70 + (int8_T)(-70 *
        (int8_T)c11_i11));
    }

    c11_updateDataWrittenToVector(chartInstance, 1U);
    break;

   default:
    CV_EML_SWITCH(3, 1, 0, 0);
    break;
  }

  _SFD_EML_CALL(3U, chartInstance->c11_sfEvent, 10);
  if (CV_EML_IF(3, 1, 0, CV_RELATIONAL_EVAL(4U, 3U, 0, (real_T)*(uint8_T *)
        &((char_T *)chartInstance->c11_me)[4], 103.0, 0, 0U, *(uint8_T *)
        &((char_T *)chartInstance->c11_me)[4] == 103))) {
    _SFD_EML_CALL(3U, chartInstance->c11_sfEvent, 11);
    c11_errorIfDataNotWrittenToFcn(chartInstance, 1U, 5U, 86U, 244, 11);
    for (c11_i12 = 0; c11_i12 < 2; c11_i12++) {
      c11_i13 = -chartInstance->c11_startingPos[c11_i12];
      if (c11_i13 > 127) {
        CV_SATURATION_EVAL(4, 3, 0, 0, 1);
        c11_i13 = 127;
      } else {
        if (CV_SATURATION_EVAL(4, 3, 0, 0, c11_i13 < -128)) {
          c11_i13 = -128;
        }
      }

      chartInstance->c11_startingPos[c11_i12] = (int8_T)c11_i13;
    }

    c11_updateDataWrittenToVector(chartInstance, 1U);
  }

  _SFD_EML_CALL(3U, chartInstance->c11_sfEvent, -11);
  _SFD_SYMBOL_SCOPE_POP();
}

static uint8_T c11_checkReached(SFc11_greenOffenceInstanceStruct *chartInstance,
  int8_T c11_pos[2], real_T c11_tol)
{
  uint8_T c11_posReached;
  uint32_T c11_debug_family_var_map[5];
  real_T c11_nargin = 2.0;
  real_T c11_nargout = 1.0;
  int8_T c11_b_pos[2];
  int8_T c11_iv4[2];
  int32_T c11_i14;
  int32_T c11_i15;
  real32_T c11_x[2];
  int32_T c11_i16;
  real32_T c11_b_x[2];
  real32_T c11_y;
  real_T c11_d1;
  _SFD_SYMBOL_SCOPE_PUSH_EML(0U, 5U, 5U, c11_g_debug_family_names,
    c11_debug_family_var_map);
  _SFD_SYMBOL_SCOPE_ADD_EML_IMPORTABLE(&c11_nargin, 0U, c11_sf_marshallOut,
    c11_sf_marshallIn);
  _SFD_SYMBOL_SCOPE_ADD_EML_IMPORTABLE(&c11_nargout, 1U, c11_sf_marshallOut,
    c11_sf_marshallIn);
  _SFD_SYMBOL_SCOPE_ADD_EML_IMPORTABLE(c11_pos, 2U, c11_d_sf_marshallOut,
    c11_d_sf_marshallIn);
  _SFD_SYMBOL_SCOPE_ADD_EML_IMPORTABLE(&c11_tol, 3U, c11_sf_marshallOut,
    c11_sf_marshallIn);
  _SFD_SYMBOL_SCOPE_ADD_EML_IMPORTABLE(&c11_posReached, 4U, c11_c_sf_marshallOut,
    c11_c_sf_marshallIn);
  CV_EML_FCN(4, 0);
  _SFD_EML_CALL(4U, chartInstance->c11_sfEvent, 2);
  c11_posReached = 0U;
  _SFD_EML_CALL(4U, chartInstance->c11_sfEvent, 3);
  c11_b_pos[0] = c11_pos[0];
  c11_b_pos[1] = c11_pos[1];
  c11_iv4[0] = *(int8_T *)&((char_T *)chartInstance->c11_me)[0];
  c11_iv4[1] = *(int8_T *)&((char_T *)chartInstance->c11_me)[1];
  for (c11_i14 = 0; c11_i14 < 2; c11_i14++) {
    c11_i15 = c11_b_pos[c11_i14] - c11_iv4[c11_i14];
    if (c11_i15 > 127) {
      CV_SATURATION_EVAL(4, 4, 0, 0, 1);
      c11_i15 = 127;
    } else {
      if (CV_SATURATION_EVAL(4, 4, 0, 0, c11_i15 < -128)) {
        c11_i15 = -128;
      }
    }

    c11_x[c11_i14] = (real32_T)(int8_T)c11_i15;
  }

  for (c11_i16 = 0; c11_i16 < 2; c11_i16++) {
    c11_b_x[c11_i16] = c11_x[c11_i16];
  }

  c11_y = c11_eml_xnrm2(chartInstance, c11_b_x);
  c11_d1 = c11_y;
  if (CV_EML_IF(4, 1, 0, CV_RELATIONAL_EVAL(4U, 4U, 0, c11_d1, c11_tol, -1, 2U,
        c11_d1 < c11_tol))) {
    _SFD_EML_CALL(4U, chartInstance->c11_sfEvent, 4);
    c11_posReached = 1U;
  }

  _SFD_EML_CALL(4U, chartInstance->c11_sfEvent, -4);
  _SFD_SYMBOL_SCOPE_POP();
  return c11_posReached;
}

static const mxArray *c11_e_sf_marshallOut(void *chartInstanceVoid, void
  *c11_inData)
{
  const mxArray *c11_mxArrayOutData = NULL;
  int32_T c11_u;
  const mxArray *c11_y = NULL;
  SFc11_greenOffenceInstanceStruct *chartInstance;
  chartInstance = (SFc11_greenOffenceInstanceStruct *)chartInstanceVoid;
  c11_mxArrayOutData = NULL;
  c11_u = *(int32_T *)c11_inData;
  c11_y = NULL;
  sf_mex_assign(&c11_y, sf_mex_create("y", &c11_u, 6, 0U, 0U, 0U, 0), false);
  sf_mex_assign(&c11_mxArrayOutData, c11_y, false);
  return c11_mxArrayOutData;
}

static int32_T c11_f_emlrt_marshallIn(SFc11_greenOffenceInstanceStruct
  *chartInstance, const mxArray *c11_u, const emlrtMsgIdentifier *c11_parentId)
{
  int32_T c11_y;
  int32_T c11_i17;
  (void)chartInstance;
  sf_mex_import(c11_parentId, sf_mex_dup(c11_u), &c11_i17, 1, 6, 0U, 0, 0U, 0);
  c11_y = c11_i17;
  sf_mex_destroy(&c11_u);
  return c11_y;
}

static void c11_e_sf_marshallIn(void *chartInstanceVoid, const mxArray
  *c11_mxArrayInData, const char_T *c11_varName, void *c11_outData)
{
  const mxArray *c11_b_sfEvent;
  const char_T *c11_identifier;
  emlrtMsgIdentifier c11_thisId;
  int32_T c11_y;
  SFc11_greenOffenceInstanceStruct *chartInstance;
  chartInstance = (SFc11_greenOffenceInstanceStruct *)chartInstanceVoid;
  c11_b_sfEvent = sf_mex_dup(c11_mxArrayInData);
  c11_identifier = c11_varName;
  c11_thisId.fIdentifier = c11_identifier;
  c11_thisId.fParent = NULL;
  c11_y = c11_f_emlrt_marshallIn(chartInstance, sf_mex_dup(c11_b_sfEvent),
    &c11_thisId);
  sf_mex_destroy(&c11_b_sfEvent);
  *(int32_T *)c11_outData = c11_y;
  sf_mex_destroy(&c11_mxArrayInData);
}

static const mxArray *c11_me_bus_io(void *chartInstanceVoid, void *c11_pData)
{
  const mxArray *c11_mxVal = NULL;
  c11_Player c11_tmp;
  SFc11_greenOffenceInstanceStruct *chartInstance;
  chartInstance = (SFc11_greenOffenceInstanceStruct *)chartInstanceVoid;
  c11_mxVal = NULL;
  c11_tmp.x = *(int8_T *)&((char_T *)(c11_Player *)c11_pData)[0];
  c11_tmp.y = *(int8_T *)&((char_T *)(c11_Player *)c11_pData)[1];
  c11_tmp.orientation = *(int16_T *)&((char_T *)(c11_Player *)c11_pData)[2];
  c11_tmp.color = *(uint8_T *)&((char_T *)(c11_Player *)c11_pData)[4];
  c11_tmp.position = *(uint8_T *)&((char_T *)(c11_Player *)c11_pData)[5];
  c11_tmp.valid = *(uint8_T *)&((char_T *)(c11_Player *)c11_pData)[6];
  sf_mex_assign(&c11_mxVal, c11_f_sf_marshallOut(chartInstance, &c11_tmp), false);
  return c11_mxVal;
}

static const mxArray *c11_f_sf_marshallOut(void *chartInstanceVoid, void
  *c11_inData)
{
  const mxArray *c11_mxArrayOutData = NULL;
  c11_Player c11_u;
  const mxArray *c11_y = NULL;
  int8_T c11_b_u;
  const mxArray *c11_b_y = NULL;
  int8_T c11_c_u;
  const mxArray *c11_c_y = NULL;
  int16_T c11_d_u;
  const mxArray *c11_d_y = NULL;
  uint8_T c11_e_u;
  const mxArray *c11_e_y = NULL;
  uint8_T c11_f_u;
  const mxArray *c11_f_y = NULL;
  uint8_T c11_g_u;
  const mxArray *c11_g_y = NULL;
  SFc11_greenOffenceInstanceStruct *chartInstance;
  chartInstance = (SFc11_greenOffenceInstanceStruct *)chartInstanceVoid;
  c11_mxArrayOutData = NULL;
  c11_u = *(c11_Player *)c11_inData;
  c11_y = NULL;
  sf_mex_assign(&c11_y, sf_mex_createstruct("structure", 2, 1, 1), false);
  c11_b_u = c11_u.x;
  c11_b_y = NULL;
  sf_mex_assign(&c11_b_y, sf_mex_create("y", &c11_b_u, 2, 0U, 0U, 0U, 0), false);
  sf_mex_addfield(c11_y, c11_b_y, "x", "x", 0);
  c11_c_u = c11_u.y;
  c11_c_y = NULL;
  sf_mex_assign(&c11_c_y, sf_mex_create("y", &c11_c_u, 2, 0U, 0U, 0U, 0), false);
  sf_mex_addfield(c11_y, c11_c_y, "y", "y", 0);
  c11_d_u = c11_u.orientation;
  c11_d_y = NULL;
  sf_mex_assign(&c11_d_y, sf_mex_create("y", &c11_d_u, 4, 0U, 0U, 0U, 0), false);
  sf_mex_addfield(c11_y, c11_d_y, "orientation", "orientation", 0);
  c11_e_u = c11_u.color;
  c11_e_y = NULL;
  sf_mex_assign(&c11_e_y, sf_mex_create("y", &c11_e_u, 3, 0U, 0U, 0U, 0), false);
  sf_mex_addfield(c11_y, c11_e_y, "color", "color", 0);
  c11_f_u = c11_u.position;
  c11_f_y = NULL;
  sf_mex_assign(&c11_f_y, sf_mex_create("y", &c11_f_u, 3, 0U, 0U, 0U, 0), false);
  sf_mex_addfield(c11_y, c11_f_y, "position", "position", 0);
  c11_g_u = c11_u.valid;
  c11_g_y = NULL;
  sf_mex_assign(&c11_g_y, sf_mex_create("y", &c11_g_u, 3, 0U, 0U, 0U, 0), false);
  sf_mex_addfield(c11_y, c11_g_y, "valid", "valid", 0);
  sf_mex_assign(&c11_mxArrayOutData, c11_y, false);
  return c11_mxArrayOutData;
}

static const mxArray *c11_players_bus_io(void *chartInstanceVoid, void
  *c11_pData)
{
  const mxArray *c11_mxVal = NULL;
  int32_T c11_i18;
  int32_T c11_i19;
  c11_Player c11_tmp[6];
  SFc11_greenOffenceInstanceStruct *chartInstance;
  chartInstance = (SFc11_greenOffenceInstanceStruct *)chartInstanceVoid;
  c11_mxVal = NULL;
  for (c11_i18 = 0; c11_i18 < 6; c11_i18++) {
    for (c11_i19 = 0; c11_i19 < 1; c11_i19++) {
      c11_tmp[c11_i19 + c11_i18].x = *(int8_T *)&((char_T *)(c11_Player *)
        &((char_T *)(c11_Player (*)[6])c11_pData)[8 * (c11_i19 + c11_i18)])[0];
      c11_tmp[c11_i19 + c11_i18].y = *(int8_T *)&((char_T *)(c11_Player *)
        &((char_T *)(c11_Player (*)[6])c11_pData)[8 * (c11_i19 + c11_i18)])[1];
      c11_tmp[c11_i19 + c11_i18].orientation = *(int16_T *)&((char_T *)
        (c11_Player *)&((char_T *)(c11_Player (*)[6])c11_pData)[8 * (c11_i19 +
        c11_i18)])[2];
      c11_tmp[c11_i19 + c11_i18].color = *(uint8_T *)&((char_T *)(c11_Player *)
        &((char_T *)(c11_Player (*)[6])c11_pData)[8 * (c11_i19 + c11_i18)])[4];
      c11_tmp[c11_i19 + c11_i18].position = *(uint8_T *)&((char_T *)(c11_Player *)
        &((char_T *)(c11_Player (*)[6])c11_pData)[8 * (c11_i19 + c11_i18)])[5];
      c11_tmp[c11_i19 + c11_i18].valid = *(uint8_T *)&((char_T *)(c11_Player *)
        &((char_T *)(c11_Player (*)[6])c11_pData)[8 * (c11_i19 + c11_i18)])[6];
    }
  }

  sf_mex_assign(&c11_mxVal, c11_g_sf_marshallOut(chartInstance, c11_tmp), false);
  return c11_mxVal;
}

static const mxArray *c11_g_sf_marshallOut(void *chartInstanceVoid, void
  *c11_inData)
{
  const mxArray *c11_mxArrayOutData;
  int32_T c11_i20;
  c11_Player c11_b_inData[6];
  int32_T c11_i21;
  c11_Player c11_u[6];
  const mxArray *c11_y = NULL;
  int32_T c11_i22;
  int32_T c11_iv5[2];
  int32_T c11_i23;
  const c11_Player *c11_r1;
  int8_T c11_b_u;
  const mxArray *c11_b_y = NULL;
  int8_T c11_c_u;
  const mxArray *c11_c_y = NULL;
  int16_T c11_d_u;
  const mxArray *c11_d_y = NULL;
  uint8_T c11_e_u;
  const mxArray *c11_e_y = NULL;
  uint8_T c11_f_u;
  const mxArray *c11_f_y = NULL;
  uint8_T c11_g_u;
  const mxArray *c11_g_y = NULL;
  SFc11_greenOffenceInstanceStruct *chartInstance;
  chartInstance = (SFc11_greenOffenceInstanceStruct *)chartInstanceVoid;
  c11_mxArrayOutData = NULL;
  c11_mxArrayOutData = NULL;
  for (c11_i20 = 0; c11_i20 < 6; c11_i20++) {
    c11_b_inData[c11_i20] = (*(c11_Player (*)[6])c11_inData)[c11_i20];
  }

  for (c11_i21 = 0; c11_i21 < 6; c11_i21++) {
    c11_u[c11_i21] = c11_b_inData[c11_i21];
  }

  c11_y = NULL;
  for (c11_i22 = 0; c11_i22 < 2; c11_i22++) {
    c11_iv5[c11_i22] = 1 + 5 * c11_i22;
  }

  sf_mex_assign(&c11_y, sf_mex_createstructarray("structure", 2, c11_iv5), false);
  for (c11_i23 = 0; c11_i23 < 6; c11_i23++) {
    c11_r1 = &c11_u[c11_i23];
    c11_b_u = c11_r1->x;
    c11_b_y = NULL;
    sf_mex_assign(&c11_b_y, sf_mex_create("y", &c11_b_u, 2, 0U, 0U, 0U, 0),
                  false);
    sf_mex_addfield(c11_y, c11_b_y, "x", "x", c11_i23);
    c11_c_u = c11_r1->y;
    c11_c_y = NULL;
    sf_mex_assign(&c11_c_y, sf_mex_create("y", &c11_c_u, 2, 0U, 0U, 0U, 0),
                  false);
    sf_mex_addfield(c11_y, c11_c_y, "y", "y", c11_i23);
    c11_d_u = c11_r1->orientation;
    c11_d_y = NULL;
    sf_mex_assign(&c11_d_y, sf_mex_create("y", &c11_d_u, 4, 0U, 0U, 0U, 0),
                  false);
    sf_mex_addfield(c11_y, c11_d_y, "orientation", "orientation", c11_i23);
    c11_e_u = c11_r1->color;
    c11_e_y = NULL;
    sf_mex_assign(&c11_e_y, sf_mex_create("y", &c11_e_u, 3, 0U, 0U, 0U, 0),
                  false);
    sf_mex_addfield(c11_y, c11_e_y, "color", "color", c11_i23);
    c11_f_u = c11_r1->position;
    c11_f_y = NULL;
    sf_mex_assign(&c11_f_y, sf_mex_create("y", &c11_f_u, 3, 0U, 0U, 0U, 0),
                  false);
    sf_mex_addfield(c11_y, c11_f_y, "position", "position", c11_i23);
    c11_g_u = c11_r1->valid;
    c11_g_y = NULL;
    sf_mex_assign(&c11_g_y, sf_mex_create("y", &c11_g_u, 3, 0U, 0U, 0U, 0),
                  false);
    sf_mex_addfield(c11_y, c11_g_y, "valid", "valid", c11_i23);
  }

  sf_mex_assign(&c11_mxArrayOutData, c11_y, false);
  return c11_mxArrayOutData;
}

static const mxArray *c11_ball_bus_io(void *chartInstanceVoid, void *c11_pData)
{
  const mxArray *c11_mxVal = NULL;
  c11_Ball c11_tmp;
  SFc11_greenOffenceInstanceStruct *chartInstance;
  chartInstance = (SFc11_greenOffenceInstanceStruct *)chartInstanceVoid;
  c11_mxVal = NULL;
  c11_tmp.x = *(int8_T *)&((char_T *)(c11_Ball *)c11_pData)[0];
  c11_tmp.y = *(int8_T *)&((char_T *)(c11_Ball *)c11_pData)[1];
  c11_tmp.valid = *(uint8_T *)&((char_T *)(c11_Ball *)c11_pData)[2];
  sf_mex_assign(&c11_mxVal, c11_h_sf_marshallOut(chartInstance, &c11_tmp), false);
  return c11_mxVal;
}

static const mxArray *c11_h_sf_marshallOut(void *chartInstanceVoid, void
  *c11_inData)
{
  const mxArray *c11_mxArrayOutData = NULL;
  c11_Ball c11_u;
  const mxArray *c11_y = NULL;
  int8_T c11_b_u;
  const mxArray *c11_b_y = NULL;
  int8_T c11_c_u;
  const mxArray *c11_c_y = NULL;
  uint8_T c11_d_u;
  const mxArray *c11_d_y = NULL;
  SFc11_greenOffenceInstanceStruct *chartInstance;
  chartInstance = (SFc11_greenOffenceInstanceStruct *)chartInstanceVoid;
  c11_mxArrayOutData = NULL;
  c11_u = *(c11_Ball *)c11_inData;
  c11_y = NULL;
  sf_mex_assign(&c11_y, sf_mex_createstruct("structure", 2, 1, 1), false);
  c11_b_u = c11_u.x;
  c11_b_y = NULL;
  sf_mex_assign(&c11_b_y, sf_mex_create("y", &c11_b_u, 2, 0U, 0U, 0U, 0), false);
  sf_mex_addfield(c11_y, c11_b_y, "x", "x", 0);
  c11_c_u = c11_u.y;
  c11_c_y = NULL;
  sf_mex_assign(&c11_c_y, sf_mex_create("y", &c11_c_u, 2, 0U, 0U, 0U, 0), false);
  sf_mex_addfield(c11_y, c11_c_y, "y", "y", 0);
  c11_d_u = c11_u.valid;
  c11_d_y = NULL;
  sf_mex_assign(&c11_d_y, sf_mex_create("y", &c11_d_u, 3, 0U, 0U, 0U, 0), false);
  sf_mex_addfield(c11_y, c11_d_y, "valid", "valid", 0);
  sf_mex_assign(&c11_mxArrayOutData, c11_y, false);
  return c11_mxArrayOutData;
}

static const mxArray *c11_finalWay_bus_io(void *chartInstanceVoid, void
  *c11_pData)
{
  const mxArray *c11_mxVal = NULL;
  c11_Waypoint c11_tmp;
  SFc11_greenOffenceInstanceStruct *chartInstance;
  chartInstance = (SFc11_greenOffenceInstanceStruct *)chartInstanceVoid;
  c11_mxVal = NULL;
  c11_tmp.x = *(int8_T *)&((char_T *)(c11_Waypoint *)c11_pData)[0];
  c11_tmp.y = *(int8_T *)&((char_T *)(c11_Waypoint *)c11_pData)[1];
  c11_tmp.orientation = *(int16_T *)&((char_T *)(c11_Waypoint *)c11_pData)[2];
  sf_mex_assign(&c11_mxVal, c11_i_sf_marshallOut(chartInstance, &c11_tmp), false);
  return c11_mxVal;
}

static const mxArray *c11_i_sf_marshallOut(void *chartInstanceVoid, void
  *c11_inData)
{
  const mxArray *c11_mxArrayOutData = NULL;
  c11_Waypoint c11_u;
  const mxArray *c11_y = NULL;
  int8_T c11_b_u;
  const mxArray *c11_b_y = NULL;
  int8_T c11_c_u;
  const mxArray *c11_c_y = NULL;
  int16_T c11_d_u;
  const mxArray *c11_d_y = NULL;
  SFc11_greenOffenceInstanceStruct *chartInstance;
  chartInstance = (SFc11_greenOffenceInstanceStruct *)chartInstanceVoid;
  c11_mxArrayOutData = NULL;
  c11_u = *(c11_Waypoint *)c11_inData;
  c11_y = NULL;
  sf_mex_assign(&c11_y, sf_mex_createstruct("structure", 2, 1, 1), false);
  c11_b_u = c11_u.x;
  c11_b_y = NULL;
  sf_mex_assign(&c11_b_y, sf_mex_create("y", &c11_b_u, 2, 0U, 0U, 0U, 0), false);
  sf_mex_addfield(c11_y, c11_b_y, "x", "x", 0);
  c11_c_u = c11_u.y;
  c11_c_y = NULL;
  sf_mex_assign(&c11_c_y, sf_mex_create("y", &c11_c_u, 2, 0U, 0U, 0U, 0), false);
  sf_mex_addfield(c11_y, c11_c_y, "y", "y", 0);
  c11_d_u = c11_u.orientation;
  c11_d_y = NULL;
  sf_mex_assign(&c11_d_y, sf_mex_create("y", &c11_d_u, 4, 0U, 0U, 0U, 0), false);
  sf_mex_addfield(c11_y, c11_d_y, "orientation", "orientation", 0);
  sf_mex_assign(&c11_mxArrayOutData, c11_y, false);
  return c11_mxArrayOutData;
}

static c11_Waypoint c11_g_emlrt_marshallIn(SFc11_greenOffenceInstanceStruct
  *chartInstance, const mxArray *c11_b_finalWay, const char_T *c11_identifier)
{
  c11_Waypoint c11_y;
  emlrtMsgIdentifier c11_thisId;
  c11_thisId.fIdentifier = c11_identifier;
  c11_thisId.fParent = NULL;
  c11_y = c11_h_emlrt_marshallIn(chartInstance, sf_mex_dup(c11_b_finalWay),
    &c11_thisId);
  sf_mex_destroy(&c11_b_finalWay);
  return c11_y;
}

static c11_Waypoint c11_h_emlrt_marshallIn(SFc11_greenOffenceInstanceStruct
  *chartInstance, const mxArray *c11_u, const emlrtMsgIdentifier *c11_parentId)
{
  c11_Waypoint c11_y;
  emlrtMsgIdentifier c11_thisId;
  static const char * c11_fieldNames[3] = { "x", "y", "orientation" };

  c11_thisId.fParent = c11_parentId;
  sf_mex_check_struct(c11_parentId, c11_u, 3, c11_fieldNames, 0U, NULL);
  c11_thisId.fIdentifier = "x";
  c11_y.x = c11_i_emlrt_marshallIn(chartInstance, sf_mex_dup(sf_mex_getfield
    (c11_u, "x", "x", 0)), &c11_thisId);
  c11_thisId.fIdentifier = "y";
  c11_y.y = c11_i_emlrt_marshallIn(chartInstance, sf_mex_dup(sf_mex_getfield
    (c11_u, "y", "y", 0)), &c11_thisId);
  c11_thisId.fIdentifier = "orientation";
  c11_y.orientation = c11_j_emlrt_marshallIn(chartInstance, sf_mex_dup
    (sf_mex_getfield(c11_u, "orientation", "orientation", 0)), &c11_thisId);
  sf_mex_destroy(&c11_u);
  return c11_y;
}

static int8_T c11_i_emlrt_marshallIn(SFc11_greenOffenceInstanceStruct
  *chartInstance, const mxArray *c11_u, const emlrtMsgIdentifier *c11_parentId)
{
  int8_T c11_y;
  int8_T c11_i24;
  (void)chartInstance;
  sf_mex_import(c11_parentId, sf_mex_dup(c11_u), &c11_i24, 1, 2, 0U, 0, 0U, 0);
  c11_y = c11_i24;
  sf_mex_destroy(&c11_u);
  return c11_y;
}

static int16_T c11_j_emlrt_marshallIn(SFc11_greenOffenceInstanceStruct
  *chartInstance, const mxArray *c11_u, const emlrtMsgIdentifier *c11_parentId)
{
  int16_T c11_y;
  int16_T c11_i25;
  (void)chartInstance;
  sf_mex_import(c11_parentId, sf_mex_dup(c11_u), &c11_i25, 1, 4, 0U, 0, 0U, 0);
  c11_y = c11_i25;
  sf_mex_destroy(&c11_u);
  return c11_y;
}

static void c11_f_sf_marshallIn(void *chartInstanceVoid, const mxArray
  *c11_mxArrayInData, const char_T *c11_varName, void *c11_outData)
{
  const mxArray *c11_b_finalWay;
  const char_T *c11_identifier;
  emlrtMsgIdentifier c11_thisId;
  c11_Waypoint c11_y;
  SFc11_greenOffenceInstanceStruct *chartInstance;
  chartInstance = (SFc11_greenOffenceInstanceStruct *)chartInstanceVoid;
  c11_b_finalWay = sf_mex_dup(c11_mxArrayInData);
  c11_identifier = c11_varName;
  c11_thisId.fIdentifier = c11_identifier;
  c11_thisId.fParent = NULL;
  c11_y = c11_h_emlrt_marshallIn(chartInstance, sf_mex_dup(c11_b_finalWay),
    &c11_thisId);
  sf_mex_destroy(&c11_b_finalWay);
  *(c11_Waypoint *)c11_outData = c11_y;
  sf_mex_destroy(&c11_mxArrayInData);
}

static const mxArray *c11_j_sf_marshallOut(void *chartInstanceVoid, void
  *c11_inData)
{
  const mxArray *c11_mxArrayOutData = NULL;
  int32_T c11_i26;
  int8_T c11_b_inData[2];
  int32_T c11_i27;
  int8_T c11_u[2];
  const mxArray *c11_y = NULL;
  SFc11_greenOffenceInstanceStruct *chartInstance;
  chartInstance = (SFc11_greenOffenceInstanceStruct *)chartInstanceVoid;
  c11_mxArrayOutData = NULL;
  for (c11_i26 = 0; c11_i26 < 2; c11_i26++) {
    c11_b_inData[c11_i26] = (*(int8_T (*)[2])c11_inData)[c11_i26];
  }

  for (c11_i27 = 0; c11_i27 < 2; c11_i27++) {
    c11_u[c11_i27] = c11_b_inData[c11_i27];
  }

  c11_y = NULL;
  sf_mex_assign(&c11_y, sf_mex_create("y", c11_u, 2, 0U, 1U, 0U, 2, 2, 1), false);
  sf_mex_assign(&c11_mxArrayOutData, c11_y, false);
  return c11_mxArrayOutData;
}

static void c11_k_emlrt_marshallIn(SFc11_greenOffenceInstanceStruct
  *chartInstance, const mxArray *c11_b_startingPos, const char_T *c11_identifier,
  int8_T c11_y[2])
{
  emlrtMsgIdentifier c11_thisId;
  c11_thisId.fIdentifier = c11_identifier;
  c11_thisId.fParent = NULL;
  c11_l_emlrt_marshallIn(chartInstance, sf_mex_dup(c11_b_startingPos),
    &c11_thisId, c11_y);
  sf_mex_destroy(&c11_b_startingPos);
}

static void c11_l_emlrt_marshallIn(SFc11_greenOffenceInstanceStruct
  *chartInstance, const mxArray *c11_u, const emlrtMsgIdentifier *c11_parentId,
  int8_T c11_y[2])
{
  int8_T c11_iv6[2];
  int32_T c11_i28;
  (void)chartInstance;
  sf_mex_import(c11_parentId, sf_mex_dup(c11_u), c11_iv6, 1, 2, 0U, 1, 0U, 2, 2,
                1);
  for (c11_i28 = 0; c11_i28 < 2; c11_i28++) {
    c11_y[c11_i28] = c11_iv6[c11_i28];
  }

  sf_mex_destroy(&c11_u);
}

static void c11_g_sf_marshallIn(void *chartInstanceVoid, const mxArray
  *c11_mxArrayInData, const char_T *c11_varName, void *c11_outData)
{
  const mxArray *c11_b_startingPos;
  const char_T *c11_identifier;
  emlrtMsgIdentifier c11_thisId;
  int8_T c11_y[2];
  int32_T c11_i29;
  SFc11_greenOffenceInstanceStruct *chartInstance;
  chartInstance = (SFc11_greenOffenceInstanceStruct *)chartInstanceVoid;
  c11_b_startingPos = sf_mex_dup(c11_mxArrayInData);
  c11_identifier = c11_varName;
  c11_thisId.fIdentifier = c11_identifier;
  c11_thisId.fParent = NULL;
  c11_l_emlrt_marshallIn(chartInstance, sf_mex_dup(c11_b_startingPos),
    &c11_thisId, c11_y);
  sf_mex_destroy(&c11_b_startingPos);
  for (c11_i29 = 0; c11_i29 < 2; c11_i29++) {
    (*(int8_T (*)[2])c11_outData)[c11_i29] = c11_y[c11_i29];
  }

  sf_mex_destroy(&c11_mxArrayInData);
}

static void c11_m_emlrt_marshallIn(SFc11_greenOffenceInstanceStruct
  *chartInstance, const mxArray *c11_b_dataWrittenToVector, const char_T
  *c11_identifier, boolean_T c11_y[3])
{
  emlrtMsgIdentifier c11_thisId;
  c11_thisId.fIdentifier = c11_identifier;
  c11_thisId.fParent = NULL;
  c11_n_emlrt_marshallIn(chartInstance, sf_mex_dup(c11_b_dataWrittenToVector),
    &c11_thisId, c11_y);
  sf_mex_destroy(&c11_b_dataWrittenToVector);
}

static void c11_n_emlrt_marshallIn(SFc11_greenOffenceInstanceStruct
  *chartInstance, const mxArray *c11_u, const emlrtMsgIdentifier *c11_parentId,
  boolean_T c11_y[3])
{
  boolean_T c11_bv1[3];
  int32_T c11_i30;
  (void)chartInstance;
  sf_mex_import(c11_parentId, sf_mex_dup(c11_u), c11_bv1, 1, 11, 0U, 1, 0U, 1, 3);
  for (c11_i30 = 0; c11_i30 < 3; c11_i30++) {
    c11_y[c11_i30] = c11_bv1[c11_i30];
  }

  sf_mex_destroy(&c11_u);
}

static const mxArray *c11_o_emlrt_marshallIn(SFc11_greenOffenceInstanceStruct
  *chartInstance, const mxArray *c11_b_setSimStateSideEffectsInfo, const char_T *
  c11_identifier)
{
  const mxArray *c11_y = NULL;
  emlrtMsgIdentifier c11_thisId;
  c11_y = NULL;
  c11_thisId.fIdentifier = c11_identifier;
  c11_thisId.fParent = NULL;
  sf_mex_assign(&c11_y, c11_p_emlrt_marshallIn(chartInstance, sf_mex_dup
    (c11_b_setSimStateSideEffectsInfo), &c11_thisId), false);
  sf_mex_destroy(&c11_b_setSimStateSideEffectsInfo);
  return c11_y;
}

static const mxArray *c11_p_emlrt_marshallIn(SFc11_greenOffenceInstanceStruct
  *chartInstance, const mxArray *c11_u, const emlrtMsgIdentifier *c11_parentId)
{
  const mxArray *c11_y = NULL;
  (void)chartInstance;
  (void)c11_parentId;
  c11_y = NULL;
  sf_mex_assign(&c11_y, sf_mex_duplicatearraysafe(&c11_u), false);
  sf_mex_destroy(&c11_u);
  return c11_y;
}

static void c11_updateDataWrittenToVector(SFc11_greenOffenceInstanceStruct
  *chartInstance, uint32_T c11_vectorIndex)
{
  chartInstance->c11_dataWrittenToVector[(uint32_T)_SFD_EML_ARRAY_BOUNDS_CHECK
    (0U, (int32_T)c11_vectorIndex, 0, 2, 1, 0)] = true;
}

static void c11_errorIfDataNotWrittenToFcn(SFc11_greenOffenceInstanceStruct
  *chartInstance, uint32_T c11_vectorIndex, uint32_T c11_dataNumber, uint32_T
  c11_ssIdOfSourceObject, int32_T c11_offsetInSourceObject, int32_T
  c11_lengthInSourceObject)
{
  (void)c11_ssIdOfSourceObject;
  (void)c11_offsetInSourceObject;
  (void)c11_lengthInSourceObject;
  if (!chartInstance->c11_dataWrittenToVector[(uint32_T)
      _SFD_EML_ARRAY_BOUNDS_CHECK(0U, (int32_T)c11_vectorIndex, 0, 2, 1, 0)]) {
    _SFD_DATA_READ_BEFORE_WRITE_ERROR(c11_dataNumber);
  }
}

static void init_dsm_address_info(SFc11_greenOffenceInstanceStruct
  *chartInstance)
{
  (void)chartInstance;
}

static void init_simulink_io_address(SFc11_greenOffenceInstanceStruct
  *chartInstance)
{
  chartInstance->c11_me = (c11_Player *)ssGetInputPortSignal_wrapper
    (chartInstance->S, 0);
  chartInstance->c11_players = (c11_Player (*)[6])ssGetInputPortSignal_wrapper
    (chartInstance->S, 1);
  chartInstance->c11_ball = (c11_Ball *)ssGetInputPortSignal_wrapper
    (chartInstance->S, 2);
  chartInstance->c11_finalWay = (c11_Waypoint *)ssGetOutputPortSignal_wrapper
    (chartInstance->S, 1);
  chartInstance->c11_manualWay = (c11_Waypoint *)ssGetInputPortSignal_wrapper
    (chartInstance->S, 3);
  chartInstance->c11_GameOn = (real_T *)ssGetInputPortSignal_wrapper
    (chartInstance->S, 4);
}

/* SFunction Glue Code */
#ifdef utFree
#undef utFree
#endif

#ifdef utMalloc
#undef utMalloc
#endif

#ifdef __cplusplus

extern "C" void *utMalloc(size_t size);
extern "C" void utFree(void*);

#else

extern void *utMalloc(size_t size);
extern void utFree(void*);

#endif

void sf_c11_greenOffence_get_check_sum(mxArray *plhs[])
{
  ((real_T *)mxGetPr((plhs[0])))[0] = (real_T)(4083685083U);
  ((real_T *)mxGetPr((plhs[0])))[1] = (real_T)(77185586U);
  ((real_T *)mxGetPr((plhs[0])))[2] = (real_T)(1505268942U);
  ((real_T *)mxGetPr((plhs[0])))[3] = (real_T)(1728973782U);
}

mxArray* sf_c11_greenOffence_get_post_codegen_info(void);
mxArray *sf_c11_greenOffence_get_autoinheritance_info(void)
{
  const char *autoinheritanceFields[] = { "checksum", "inputs", "parameters",
    "outputs", "locals", "postCodegenInfo" };

  mxArray *mxAutoinheritanceInfo = mxCreateStructMatrix(1, 1, sizeof
    (autoinheritanceFields)/sizeof(autoinheritanceFields[0]),
    autoinheritanceFields);

  {
    mxArray *mxChecksum = mxCreateString("76eNygp4jEVTHJw31gSFmF");
    mxSetField(mxAutoinheritanceInfo,0,"checksum",mxChecksum);
  }

  {
    const char *dataFields[] = { "size", "type", "complexity" };

    mxArray *mxData = mxCreateStructMatrix(1,5,3,dataFields);

    {
      mxArray *mxSize = mxCreateDoubleMatrix(1,2,mxREAL);
      double *pr = mxGetPr(mxSize);
      pr[0] = (double)(1);
      pr[1] = (double)(1);
      mxSetField(mxData,0,"size",mxSize);
    }

    {
      const char *typeFields[] = { "base", "fixpt" };

      mxArray *mxType = mxCreateStructMatrix(1,1,2,typeFields);
      mxSetField(mxType,0,"base",mxCreateDoubleScalar(13));
      mxSetField(mxType,0,"fixpt",mxCreateDoubleMatrix(0,0,mxREAL));
      mxSetField(mxData,0,"type",mxType);
    }

    mxSetField(mxData,0,"complexity",mxCreateDoubleScalar(0));

    {
      mxArray *mxSize = mxCreateDoubleMatrix(1,2,mxREAL);
      double *pr = mxGetPr(mxSize);
      pr[0] = (double)(1);
      pr[1] = (double)(6);
      mxSetField(mxData,1,"size",mxSize);
    }

    {
      const char *typeFields[] = { "base", "fixpt" };

      mxArray *mxType = mxCreateStructMatrix(1,1,2,typeFields);
      mxSetField(mxType,0,"base",mxCreateDoubleScalar(13));
      mxSetField(mxType,0,"fixpt",mxCreateDoubleMatrix(0,0,mxREAL));
      mxSetField(mxData,1,"type",mxType);
    }

    mxSetField(mxData,1,"complexity",mxCreateDoubleScalar(0));

    {
      mxArray *mxSize = mxCreateDoubleMatrix(1,2,mxREAL);
      double *pr = mxGetPr(mxSize);
      pr[0] = (double)(1);
      pr[1] = (double)(1);
      mxSetField(mxData,2,"size",mxSize);
    }

    {
      const char *typeFields[] = { "base", "fixpt" };

      mxArray *mxType = mxCreateStructMatrix(1,1,2,typeFields);
      mxSetField(mxType,0,"base",mxCreateDoubleScalar(13));
      mxSetField(mxType,0,"fixpt",mxCreateDoubleMatrix(0,0,mxREAL));
      mxSetField(mxData,2,"type",mxType);
    }

    mxSetField(mxData,2,"complexity",mxCreateDoubleScalar(0));

    {
      mxArray *mxSize = mxCreateDoubleMatrix(1,2,mxREAL);
      double *pr = mxGetPr(mxSize);
      pr[0] = (double)(1);
      pr[1] = (double)(1);
      mxSetField(mxData,3,"size",mxSize);
    }

    {
      const char *typeFields[] = { "base", "fixpt" };

      mxArray *mxType = mxCreateStructMatrix(1,1,2,typeFields);
      mxSetField(mxType,0,"base",mxCreateDoubleScalar(13));
      mxSetField(mxType,0,"fixpt",mxCreateDoubleMatrix(0,0,mxREAL));
      mxSetField(mxData,3,"type",mxType);
    }

    mxSetField(mxData,3,"complexity",mxCreateDoubleScalar(0));

    {
      mxArray *mxSize = mxCreateDoubleMatrix(1,2,mxREAL);
      double *pr = mxGetPr(mxSize);
      pr[0] = (double)(1);
      pr[1] = (double)(1);
      mxSetField(mxData,4,"size",mxSize);
    }

    {
      const char *typeFields[] = { "base", "fixpt" };

      mxArray *mxType = mxCreateStructMatrix(1,1,2,typeFields);
      mxSetField(mxType,0,"base",mxCreateDoubleScalar(10));
      mxSetField(mxType,0,"fixpt",mxCreateDoubleMatrix(0,0,mxREAL));
      mxSetField(mxData,4,"type",mxType);
    }

    mxSetField(mxData,4,"complexity",mxCreateDoubleScalar(0));
    mxSetField(mxAutoinheritanceInfo,0,"inputs",mxData);
  }

  {
    mxSetField(mxAutoinheritanceInfo,0,"parameters",mxCreateDoubleMatrix(0,0,
                mxREAL));
  }

  {
    const char *dataFields[] = { "size", "type", "complexity" };

    mxArray *mxData = mxCreateStructMatrix(1,1,3,dataFields);

    {
      mxArray *mxSize = mxCreateDoubleMatrix(1,2,mxREAL);
      double *pr = mxGetPr(mxSize);
      pr[0] = (double)(1);
      pr[1] = (double)(1);
      mxSetField(mxData,0,"size",mxSize);
    }

    {
      const char *typeFields[] = { "base", "fixpt" };

      mxArray *mxType = mxCreateStructMatrix(1,1,2,typeFields);
      mxSetField(mxType,0,"base",mxCreateDoubleScalar(13));
      mxSetField(mxType,0,"fixpt",mxCreateDoubleMatrix(0,0,mxREAL));
      mxSetField(mxData,0,"type",mxType);
    }

    mxSetField(mxData,0,"complexity",mxCreateDoubleScalar(0));
    mxSetField(mxAutoinheritanceInfo,0,"outputs",mxData);
  }

  {
    const char *dataFields[] = { "size", "type", "complexity" };

    mxArray *mxData = mxCreateStructMatrix(1,1,3,dataFields);

    {
      mxArray *mxSize = mxCreateDoubleMatrix(1,2,mxREAL);
      double *pr = mxGetPr(mxSize);
      pr[0] = (double)(2);
      pr[1] = (double)(1);
      mxSetField(mxData,0,"size",mxSize);
    }

    {
      const char *typeFields[] = { "base", "fixpt" };

      mxArray *mxType = mxCreateStructMatrix(1,1,2,typeFields);
      mxSetField(mxType,0,"base",mxCreateDoubleScalar(4));
      mxSetField(mxType,0,"fixpt",mxCreateDoubleMatrix(0,0,mxREAL));
      mxSetField(mxData,0,"type",mxType);
    }

    mxSetField(mxData,0,"complexity",mxCreateDoubleScalar(0));
    mxSetField(mxAutoinheritanceInfo,0,"locals",mxData);
  }

  {
    mxArray* mxPostCodegenInfo = sf_c11_greenOffence_get_post_codegen_info();
    mxSetField(mxAutoinheritanceInfo,0,"postCodegenInfo",mxPostCodegenInfo);
  }

  return(mxAutoinheritanceInfo);
}

mxArray *sf_c11_greenOffence_third_party_uses_info(void)
{
  mxArray * mxcell3p = mxCreateCellMatrix(1,0);
  return(mxcell3p);
}

mxArray *sf_c11_greenOffence_jit_fallback_info(void)
{
  const char *infoFields[] = { "fallbackType", "fallbackReason",
    "incompatibleSymbol", };

  mxArray *mxInfo = mxCreateStructMatrix(1, 1, 3, infoFields);
  mxArray *fallbackReason = mxCreateString("feature_off");
  mxArray *incompatibleSymbol = mxCreateString("");
  mxArray *fallbackType = mxCreateString("early");
  mxSetField(mxInfo, 0, infoFields[0], fallbackType);
  mxSetField(mxInfo, 0, infoFields[1], fallbackReason);
  mxSetField(mxInfo, 0, infoFields[2], incompatibleSymbol);
  return mxInfo;
}

mxArray *sf_c11_greenOffence_updateBuildInfo_args_info(void)
{
  mxArray *mxBIArgs = mxCreateCellMatrix(1,0);
  return mxBIArgs;
}

mxArray* sf_c11_greenOffence_get_post_codegen_info(void)
{
  const char* fieldNames[] = { "exportedFunctionsUsedByThisChart",
    "exportedFunctionsChecksum" };

  mwSize dims[2] = { 1, 1 };

  mxArray* mxPostCodegenInfo = mxCreateStructArray(2, dims, sizeof(fieldNames)/
    sizeof(fieldNames[0]), fieldNames);

  {
    mxArray* mxExportedFunctionsChecksum = mxCreateString("");
    mwSize exp_dims[2] = { 0, 1 };

    mxArray* mxExportedFunctionsUsedByThisChart = mxCreateCellArray(2, exp_dims);
    mxSetField(mxPostCodegenInfo, 0, "exportedFunctionsUsedByThisChart",
               mxExportedFunctionsUsedByThisChart);
    mxSetField(mxPostCodegenInfo, 0, "exportedFunctionsChecksum",
               mxExportedFunctionsChecksum);
  }

  return mxPostCodegenInfo;
}

static const mxArray *sf_get_sim_state_info_c11_greenOffence(void)
{
  const char *infoFields[] = { "chartChecksum", "varInfo" };

  mxArray *mxInfo = mxCreateStructMatrix(1, 1, 2, infoFields);
  const char *infoEncStr[] = {
    "100 S1x7'type','srcId','name','auxInfo'{{M[1],M[27],T\"finalWay\",},{M[3],M[98],T\"startingPos\",},{M[8],M[0],T\"is_active_c11_greenOffence\",},{M[9],M[0],T\"is_c11_greenOffence\",},{M[9],M[17],T\"is_GameIsOn_Goalie\",},{M[9],M[88],T\"is_waiting\",},{M[15],M[0],T\"dataWrittenToVector\",}}"
  };

  mxArray *mxVarInfo = sf_mex_decode_encoded_mx_struct_array(infoEncStr, 7, 10);
  mxArray *mxChecksum = mxCreateDoubleMatrix(1, 4, mxREAL);
  sf_c11_greenOffence_get_check_sum(&mxChecksum);
  mxSetField(mxInfo, 0, infoFields[0], mxChecksum);
  mxSetField(mxInfo, 0, infoFields[1], mxVarInfo);
  return mxInfo;
}

static void chart_debug_initialization(SimStruct *S, unsigned int
  fullDebuggerInitialization)
{
  if (!sim_mode_is_rtw_gen(S)) {
    SFc11_greenOffenceInstanceStruct *chartInstance;
    ChartRunTimeInfo * crtInfo = (ChartRunTimeInfo *)(ssGetUserData(S));
    ChartInfoStruct * chartInfo = (ChartInfoStruct *)(crtInfo->instanceInfo);
    chartInstance = (SFc11_greenOffenceInstanceStruct *)
      chartInfo->chartInstance;
    if (ssIsFirstInitCond(S) && fullDebuggerInitialization==1) {
      /* do this only if simulation is starting */
      {
        unsigned int chartAlreadyPresent;
        chartAlreadyPresent = sf_debug_initialize_chart
          (sfGlobalDebugInstanceStruct,
           _greenOffenceMachineNumber_,
           11,
           7,
           7,
           0,
           10,
           0,
           0,
           0,
           0,
           0,
           &(chartInstance->chartNumber),
           &(chartInstance->instanceNumber),
           (void *)S);

        /* Each instance must initialize its own list of scripts */
        init_script_number_translation(_greenOffenceMachineNumber_,
          chartInstance->chartNumber,chartInstance->instanceNumber);
        if (chartAlreadyPresent==0) {
          /* this is the first instance */
          sf_debug_set_chart_disable_implicit_casting
            (sfGlobalDebugInstanceStruct,_greenOffenceMachineNumber_,
             chartInstance->chartNumber,1);
          sf_debug_set_chart_event_thresholds(sfGlobalDebugInstanceStruct,
            _greenOffenceMachineNumber_,
            chartInstance->chartNumber,
            0,
            0,
            0);
          _SFD_SET_DATA_PROPS(0,1,1,0,"me");
          _SFD_SET_DATA_PROPS(1,1,1,0,"players");
          _SFD_SET_DATA_PROPS(2,1,1,0,"ball");
          _SFD_SET_DATA_PROPS(3,2,0,1,"finalWay");
          _SFD_SET_DATA_PROPS(4,1,1,0,"manualWay");
          _SFD_SET_DATA_PROPS(5,0,0,0,"startingPos");
          _SFD_SET_DATA_PROPS(6,1,1,0,"GameOn");
          _SFD_SET_DATA_PROPS(7,8,0,0,"");
          _SFD_SET_DATA_PROPS(8,8,0,0,"");
          _SFD_SET_DATA_PROPS(9,9,0,0,"");
          _SFD_STATE_INFO(0,0,0);
          _SFD_STATE_INFO(1,0,0);
          _SFD_STATE_INFO(2,0,0);
          _SFD_STATE_INFO(5,0,0);
          _SFD_STATE_INFO(6,0,0);
          _SFD_STATE_INFO(3,0,2);
          _SFD_STATE_INFO(4,0,2);
          _SFD_CH_SUBSTATE_COUNT(2);
          _SFD_CH_SUBSTATE_DECOMP(0);
          _SFD_CH_SUBSTATE_INDEX(0,0);
          _SFD_CH_SUBSTATE_INDEX(1,5);
          _SFD_ST_SUBSTATE_COUNT(0,2);
          _SFD_ST_SUBSTATE_INDEX(0,0,1);
          _SFD_ST_SUBSTATE_INDEX(0,1,2);
          _SFD_ST_SUBSTATE_COUNT(1,0);
          _SFD_ST_SUBSTATE_COUNT(2,0);
          _SFD_ST_SUBSTATE_COUNT(5,1);
          _SFD_ST_SUBSTATE_INDEX(5,0,6);
          _SFD_ST_SUBSTATE_COUNT(6,0);
        }

        _SFD_CV_INIT_CHART(2,1,0,0);

        {
          _SFD_CV_INIT_STATE(0,2,1,1,0,0,NULL,NULL);
        }

        {
          _SFD_CV_INIT_STATE(1,0,0,0,0,0,NULL,NULL);
        }

        {
          _SFD_CV_INIT_STATE(2,0,0,0,0,0,NULL,NULL);
        }

        {
          _SFD_CV_INIT_STATE(5,1,0,0,0,0,NULL,NULL);
        }

        {
          _SFD_CV_INIT_STATE(6,0,0,0,0,0,NULL,NULL);
        }

        {
          _SFD_CV_INIT_STATE(3,0,0,0,0,0,NULL,NULL);
        }

        {
          _SFD_CV_INIT_STATE(4,0,0,0,0,0,NULL,NULL);
        }

        _SFD_CV_INIT_TRANS(3,0,NULL,NULL,0,NULL);
        _SFD_CV_INIT_TRANS(0,0,NULL,NULL,0,NULL);
        _SFD_CV_INIT_TRANS(1,0,NULL,NULL,0,NULL);
        _SFD_CV_INIT_TRANS(4,0,NULL,NULL,0,NULL);
        _SFD_CV_INIT_TRANS(5,0,NULL,NULL,0,NULL);
        _SFD_CV_INIT_TRANS(6,0,NULL,NULL,0,NULL);
        _SFD_CV_INIT_TRANS(2,0,NULL,NULL,0,NULL);

        /* Initialization of MATLAB Function Model Coverage */
        _SFD_CV_INIT_EML(3,1,1,1,0,1,1,0,0,0,0);
        _SFD_CV_INIT_EML_FCN(3,0,"calcStartPos",0,-1,262);
        _SFD_CV_INIT_EML_SATURATION(3,1,0,243,-1,255);
        _SFD_CV_INIT_EML_IF(3,1,0,210,226,-1,260);

        {
          static int caseStart[] = { -1, 45, 100, 155 };

          static int caseExprEnd[] = { 8, 60, 115, 170 };

          _SFD_CV_INIT_EML_SWITCH(3,1,0,22,41,209,4,&(caseStart[0]),
            &(caseExprEnd[0]));
        }

        _SFD_CV_INIT_EML_RELATIONAL(3,1,0,213,226,0,0);
        _SFD_CV_INIT_EML(4,1,1,1,0,1,0,0,0,0,0);
        _SFD_CV_INIT_EML_FCN(4,0,"checkReached",0,-1,157);
        _SFD_CV_INIT_EML_SATURATION(4,1,0,78,-1,117);
        _SFD_CV_INIT_EML_IF(4,1,0,63,125,-1,154);
        _SFD_CV_INIT_EML_RELATIONAL(4,1,0,66,125,-1,2);
        _SFD_CV_INIT_EML(2,1,0,0,0,0,0,0,0,0,0);
        _SFD_CV_INIT_EML(1,1,0,0,0,0,0,0,0,0,0);
        _SFD_CV_INIT_EML(6,1,0,0,0,0,0,0,0,0,0);
        _SFD_CV_INIT_EML(5,0,0,1,0,0,0,0,0,0,0);
        _SFD_CV_INIT_EML_IF(5,0,0,1,46,1,46);
        _SFD_CV_INIT_EML_RELATIONAL(5,0,0,1,46,0,0);
        _SFD_CV_INIT_EML(6,0,0,1,0,0,0,0,0,0,0);
        _SFD_CV_INIT_EML_IF(6,0,0,1,46,1,46);
        _SFD_CV_INIT_EML_RELATIONAL(6,0,0,1,46,0,0);
        _SFD_CV_INIT_EML(3,0,0,0,0,0,0,0,0,0,0);
        _SFD_CV_INIT_EML(0,0,0,1,0,0,0,0,0,0,0);
        _SFD_CV_INIT_EML_IF(0,0,0,1,17,1,17);
        _SFD_CV_INIT_EML_RELATIONAL(0,0,0,1,17,-1,0);
        _SFD_CV_INIT_EML(1,0,0,1,0,0,0,0,0,0,0);
        _SFD_CV_INIT_EML_IF(1,0,0,1,17,1,17);
        _SFD_CV_INIT_EML_RELATIONAL(1,0,0,1,17,-1,0);
        _SFD_SET_DATA_COMPILED_PROPS(0,SF_STRUCT,0,NULL,0,0,0,0.0,1.0,0,0,
          (MexFcnForType)c11_me_bus_io,(MexInFcnForType)NULL);

        {
          unsigned int dimVector[2];
          dimVector[0]= 1;
          dimVector[1]= 6;
          _SFD_SET_DATA_COMPILED_PROPS(1,SF_STRUCT,2,&(dimVector[0]),0,0,0,0.0,
            1.0,0,0,(MexFcnForType)c11_players_bus_io,(MexInFcnForType)NULL);
        }

        _SFD_SET_DATA_COMPILED_PROPS(2,SF_STRUCT,0,NULL,0,0,0,0.0,1.0,0,0,
          (MexFcnForType)c11_ball_bus_io,(MexInFcnForType)NULL);
        _SFD_SET_DATA_COMPILED_PROPS(3,SF_STRUCT,0,NULL,0,0,0,0.0,1.0,0,0,
          (MexFcnForType)c11_finalWay_bus_io,(MexInFcnForType)NULL);
        _SFD_SET_DATA_COMPILED_PROPS(4,SF_STRUCT,0,NULL,0,0,0,0.0,1.0,0,0,
          (MexFcnForType)c11_finalWay_bus_io,(MexInFcnForType)NULL);

        {
          unsigned int dimVector[2];
          dimVector[0]= 2;
          dimVector[1]= 1;
          _SFD_SET_DATA_COMPILED_PROPS(5,SF_INT8,2,&(dimVector[0]),0,0,0,0.0,1.0,
            0,0,(MexFcnForType)c11_j_sf_marshallOut,(MexInFcnForType)
            c11_g_sf_marshallIn);
        }

        _SFD_SET_DATA_COMPILED_PROPS(6,SF_DOUBLE,0,NULL,0,0,0,0.0,1.0,0,0,
          (MexFcnForType)c11_sf_marshallOut,(MexInFcnForType)NULL);

        {
          unsigned int dimVector[1];
          dimVector[0]= 4294967295;
          _SFD_SET_DATA_COMPILED_PROPS(7,SF_INT8,1,&(dimVector[0]),0,0,0,0.0,1.0,
            0,0,(MexFcnForType)NULL,(MexInFcnForType)NULL);
        }

        {
          unsigned int dimVector[1];
          dimVector[0]= 4294967295;
          _SFD_SET_DATA_COMPILED_PROPS(8,SF_DOUBLE,1,&(dimVector[0]),0,0,0,0.0,
            1.0,0,0,(MexFcnForType)NULL,(MexInFcnForType)NULL);
        }

        {
          unsigned int dimVector[1];
          dimVector[0]= 4294967295;
          _SFD_SET_DATA_COMPILED_PROPS(9,SF_DOUBLE,1,&(dimVector[0]),0,0,0,0.0,
            1.0,0,0,(MexFcnForType)NULL,(MexInFcnForType)NULL);
        }

        _SFD_SET_DATA_VALUE_PTR(7,(void *)(NULL));
        _SFD_SET_DATA_VALUE_PTR(8,(void *)(NULL));
        _SFD_SET_DATA_VALUE_PTR(9,(void *)(NULL));
        _SFD_SET_DATA_VALUE_PTR(0U, chartInstance->c11_me);
        _SFD_SET_DATA_VALUE_PTR(1U, *chartInstance->c11_players);
        _SFD_SET_DATA_VALUE_PTR(2U, chartInstance->c11_ball);
        _SFD_SET_DATA_VALUE_PTR(3U, chartInstance->c11_finalWay);
        _SFD_SET_DATA_VALUE_PTR(4U, chartInstance->c11_manualWay);
        _SFD_SET_DATA_VALUE_PTR(5U, chartInstance->c11_startingPos);
        _SFD_SET_DATA_VALUE_PTR(6U, chartInstance->c11_GameOn);
      }
    } else {
      sf_debug_reset_current_state_configuration(sfGlobalDebugInstanceStruct,
        _greenOffenceMachineNumber_,chartInstance->chartNumber,
        chartInstance->instanceNumber);
    }
  }
}

static const char* sf_get_instance_specialization(void)
{
  return "UeEZVXP7ULvtMMsjsCISDC";
}

static void sf_opaque_initialize_c11_greenOffence(void *chartInstanceVar)
{
  chart_debug_initialization(((SFc11_greenOffenceInstanceStruct*)
    chartInstanceVar)->S,0);
  initialize_params_c11_greenOffence((SFc11_greenOffenceInstanceStruct*)
    chartInstanceVar);
  initialize_c11_greenOffence((SFc11_greenOffenceInstanceStruct*)
    chartInstanceVar);
}

static void sf_opaque_enable_c11_greenOffence(void *chartInstanceVar)
{
  enable_c11_greenOffence((SFc11_greenOffenceInstanceStruct*) chartInstanceVar);
}

static void sf_opaque_disable_c11_greenOffence(void *chartInstanceVar)
{
  disable_c11_greenOffence((SFc11_greenOffenceInstanceStruct*) chartInstanceVar);
}

static void sf_opaque_gateway_c11_greenOffence(void *chartInstanceVar)
{
  sf_gateway_c11_greenOffence((SFc11_greenOffenceInstanceStruct*)
    chartInstanceVar);
}

static const mxArray* sf_opaque_get_sim_state_c11_greenOffence(SimStruct* S)
{
  ChartRunTimeInfo * crtInfo = (ChartRunTimeInfo *)(ssGetUserData(S));
  ChartInfoStruct * chartInfo = (ChartInfoStruct *)(crtInfo->instanceInfo);
  return get_sim_state_c11_greenOffence((SFc11_greenOffenceInstanceStruct*)
    chartInfo->chartInstance);         /* raw sim ctx */
}

static void sf_opaque_set_sim_state_c11_greenOffence(SimStruct* S, const mxArray
  *st)
{
  ChartRunTimeInfo * crtInfo = (ChartRunTimeInfo *)(ssGetUserData(S));
  ChartInfoStruct * chartInfo = (ChartInfoStruct *)(crtInfo->instanceInfo);
  set_sim_state_c11_greenOffence((SFc11_greenOffenceInstanceStruct*)
    chartInfo->chartInstance, st);
}

static void sf_opaque_terminate_c11_greenOffence(void *chartInstanceVar)
{
  if (chartInstanceVar!=NULL) {
    SimStruct *S = ((SFc11_greenOffenceInstanceStruct*) chartInstanceVar)->S;
    ChartRunTimeInfo * crtInfo = (ChartRunTimeInfo *)(ssGetUserData(S));
    if (sim_mode_is_rtw_gen(S) || sim_mode_is_external(S)) {
      sf_clear_rtw_identifier(S);
      unload_greenOffence_optimization_info();
    }

    finalize_c11_greenOffence((SFc11_greenOffenceInstanceStruct*)
      chartInstanceVar);
    utFree(chartInstanceVar);
    if (crtInfo != NULL) {
      utFree(crtInfo);
    }

    ssSetUserData(S,NULL);
  }
}

static void sf_opaque_init_subchart_simstructs(void *chartInstanceVar)
{
  initSimStructsc11_greenOffence((SFc11_greenOffenceInstanceStruct*)
    chartInstanceVar);
}

extern unsigned int sf_machine_global_initializer_called(void);
static void mdlProcessParameters_c11_greenOffence(SimStruct *S)
{
  int i;
  for (i=0;i<ssGetNumRunTimeParams(S);i++) {
    if (ssGetSFcnParamTunable(S,i)) {
      ssUpdateDlgParamAsRunTimeParam(S,i);
    }
  }

  if (sf_machine_global_initializer_called()) {
    ChartRunTimeInfo * crtInfo = (ChartRunTimeInfo *)(ssGetUserData(S));
    ChartInfoStruct * chartInfo = (ChartInfoStruct *)(crtInfo->instanceInfo);
    initialize_params_c11_greenOffence((SFc11_greenOffenceInstanceStruct*)
      (chartInfo->chartInstance));
  }
}

static void mdlSetWorkWidths_c11_greenOffence(SimStruct *S)
{
  if (sim_mode_is_rtw_gen(S) || sim_mode_is_external(S)) {
    mxArray *infoStruct = load_greenOffence_optimization_info();
    int_T chartIsInlinable =
      (int_T)sf_is_chart_inlinable(sf_get_instance_specialization(),infoStruct,
      11);
    ssSetStateflowIsInlinable(S,chartIsInlinable);
    ssSetRTWCG(S,sf_rtw_info_uint_prop(sf_get_instance_specialization(),
                infoStruct,11,"RTWCG"));
    ssSetEnableFcnIsTrivial(S,1);
    ssSetDisableFcnIsTrivial(S,1);
    ssSetNotMultipleInlinable(S,sf_rtw_info_uint_prop
      (sf_get_instance_specialization(),infoStruct,11,
       "gatewayCannotBeInlinedMultipleTimes"));
    sf_update_buildInfo(sf_get_instance_specialization(),infoStruct,11);
    if (chartIsInlinable) {
      ssSetInputPortOptimOpts(S, 0, SS_REUSABLE_AND_LOCAL);
      ssSetInputPortOptimOpts(S, 1, SS_REUSABLE_AND_LOCAL);
      ssSetInputPortOptimOpts(S, 2, SS_REUSABLE_AND_LOCAL);
      ssSetInputPortOptimOpts(S, 3, SS_REUSABLE_AND_LOCAL);
      ssSetInputPortOptimOpts(S, 4, SS_REUSABLE_AND_LOCAL);
      sf_mark_chart_expressionable_inputs(S,sf_get_instance_specialization(),
        infoStruct,11,5);
      sf_mark_chart_reusable_outputs(S,sf_get_instance_specialization(),
        infoStruct,11,1);
    }

    {
      unsigned int outPortIdx;
      for (outPortIdx=1; outPortIdx<=1; ++outPortIdx) {
        ssSetOutputPortOptimizeInIR(S, outPortIdx, 1U);
      }
    }

    {
      unsigned int inPortIdx;
      for (inPortIdx=0; inPortIdx < 5; ++inPortIdx) {
        ssSetInputPortOptimizeInIR(S, inPortIdx, 1U);
      }
    }

    sf_set_rtw_dwork_info(S,sf_get_instance_specialization(),infoStruct,11);
    ssSetHasSubFunctions(S,!(chartIsInlinable));
  } else {
  }

  ssSetOptions(S,ssGetOptions(S)|SS_OPTION_WORKS_WITH_CODE_REUSE);
  ssSetChecksum0(S,(3061931439U));
  ssSetChecksum1(S,(939913244U));
  ssSetChecksum2(S,(3617723454U));
  ssSetChecksum3(S,(3996521011U));
  ssSetmdlDerivatives(S, NULL);
  ssSetExplicitFCSSCtrl(S,1);
  ssSupportsMultipleExecInstances(S,1);
}

static void mdlRTW_c11_greenOffence(SimStruct *S)
{
  if (sim_mode_is_rtw_gen(S)) {
    ssWriteRTWStrParam(S, "StateflowChartType", "Stateflow");
  }
}

static void mdlStart_c11_greenOffence(SimStruct *S)
{
  SFc11_greenOffenceInstanceStruct *chartInstance;
  ChartRunTimeInfo * crtInfo = (ChartRunTimeInfo *)utMalloc(sizeof
    (ChartRunTimeInfo));
  chartInstance = (SFc11_greenOffenceInstanceStruct *)utMalloc(sizeof
    (SFc11_greenOffenceInstanceStruct));
  memset(chartInstance, 0, sizeof(SFc11_greenOffenceInstanceStruct));
  if (chartInstance==NULL) {
    sf_mex_error_message("Could not allocate memory for chart instance.");
  }

  chartInstance->chartInfo.chartInstance = chartInstance;
  chartInstance->chartInfo.isEMLChart = 0;
  chartInstance->chartInfo.chartInitialized = 0;
  chartInstance->chartInfo.sFunctionGateway = sf_opaque_gateway_c11_greenOffence;
  chartInstance->chartInfo.initializeChart =
    sf_opaque_initialize_c11_greenOffence;
  chartInstance->chartInfo.terminateChart = sf_opaque_terminate_c11_greenOffence;
  chartInstance->chartInfo.enableChart = sf_opaque_enable_c11_greenOffence;
  chartInstance->chartInfo.disableChart = sf_opaque_disable_c11_greenOffence;
  chartInstance->chartInfo.getSimState =
    sf_opaque_get_sim_state_c11_greenOffence;
  chartInstance->chartInfo.setSimState =
    sf_opaque_set_sim_state_c11_greenOffence;
  chartInstance->chartInfo.getSimStateInfo =
    sf_get_sim_state_info_c11_greenOffence;
  chartInstance->chartInfo.zeroCrossings = NULL;
  chartInstance->chartInfo.outputs = NULL;
  chartInstance->chartInfo.derivatives = NULL;
  chartInstance->chartInfo.mdlRTW = mdlRTW_c11_greenOffence;
  chartInstance->chartInfo.mdlStart = mdlStart_c11_greenOffence;
  chartInstance->chartInfo.mdlSetWorkWidths = mdlSetWorkWidths_c11_greenOffence;
  chartInstance->chartInfo.extModeExec = NULL;
  chartInstance->chartInfo.restoreLastMajorStepConfiguration = NULL;
  chartInstance->chartInfo.restoreBeforeLastMajorStepConfiguration = NULL;
  chartInstance->chartInfo.storeCurrentConfiguration = NULL;
  chartInstance->chartInfo.callAtomicSubchartUserFcn = NULL;
  chartInstance->chartInfo.callAtomicSubchartAutoFcn = NULL;
  chartInstance->chartInfo.debugInstance = sfGlobalDebugInstanceStruct;
  chartInstance->S = S;
  crtInfo->checksum = SF_RUNTIME_INFO_CHECKSUM;
  crtInfo->instanceInfo = (&(chartInstance->chartInfo));
  crtInfo->isJITEnabled = false;
  crtInfo->compiledInfo = NULL;
  ssSetUserData(S,(void *)(crtInfo));  /* register the chart instance with simstruct */
  init_dsm_address_info(chartInstance);
  init_simulink_io_address(chartInstance);
  if (!sim_mode_is_rtw_gen(S)) {
  }

  sf_opaque_init_subchart_simstructs(chartInstance->chartInfo.chartInstance);
  chart_debug_initialization(S,1);
}

void c11_greenOffence_method_dispatcher(SimStruct *S, int_T method, void *data)
{
  switch (method) {
   case SS_CALL_MDL_START:
    mdlStart_c11_greenOffence(S);
    break;

   case SS_CALL_MDL_SET_WORK_WIDTHS:
    mdlSetWorkWidths_c11_greenOffence(S);
    break;

   case SS_CALL_MDL_PROCESS_PARAMETERS:
    mdlProcessParameters_c11_greenOffence(S);
    break;

   default:
    /* Unhandled method */
    sf_mex_error_message("Stateflow Internal Error:\n"
                         "Error calling c11_greenOffence_method_dispatcher.\n"
                         "Can't handle method %d.\n", method);
    break;
  }
}

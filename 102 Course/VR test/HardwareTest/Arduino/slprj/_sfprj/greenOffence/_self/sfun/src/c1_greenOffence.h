#ifndef __c1_greenOffence_h__
#define __c1_greenOffence_h__

/* Include files */
#include "sf_runtime/sfc_sf.h"
#include "sf_runtime/sfc_mex.h"
#include "rtwtypes.h"
#include "multiword_types.h"

/* Type Definitions */
#ifndef typedef_SFc1_greenOffenceInstanceStruct
#define typedef_SFc1_greenOffenceInstanceStruct

typedef struct {
  SimStruct *S;
  ChartInfoStruct chartInfo;
  uint32_T chartNumber;
  uint32_T instanceNumber;
  int32_T c1_sfEvent;
  boolean_T c1_isStable;
  boolean_T c1_doneDoubleBufferReInit;
  uint8_T c1_is_active_c1_greenOffence;
  real32_T c1_lastLeft;
  boolean_T c1_lastLeft_not_empty;
  real32_T c1_lastRight;
  boolean_T c1_lastRight_not_empty;
  real32_T c1_itteration;
  boolean_T c1_itteration_not_empty;
  real32_T *c1_leftVol;
  real32_T *c1_rightVol;
  uint8_T *c1_FWD_A;
  uint8_T *c1_BK_A;
  uint8_T *c1_FWD_B;
  uint8_T *c1_BK_B;
  uint8_T *c1_shoot;
} SFc1_greenOffenceInstanceStruct;

#endif                                 /*typedef_SFc1_greenOffenceInstanceStruct*/

/* Named Constants */

/* Variable Declarations */
extern struct SfDebugInstanceStruct *sfGlobalDebugInstanceStruct;

/* Variable Definitions */

/* Function Declarations */
extern const mxArray *sf_c1_greenOffence_get_eml_resolved_functions_info(void);

/* Function Definitions */
extern void sf_c1_greenOffence_get_check_sum(mxArray *plhs[]);
extern void c1_greenOffence_method_dispatcher(SimStruct *S, int_T method, void
  *data);

#endif

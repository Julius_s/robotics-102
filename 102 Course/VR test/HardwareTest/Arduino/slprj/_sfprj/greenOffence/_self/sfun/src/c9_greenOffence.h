#ifndef __c9_greenOffence_h__
#define __c9_greenOffence_h__

/* Include files */
#include "sf_runtime/sfc_sf.h"
#include "sf_runtime/sfc_mex.h"
#include "rtwtypes.h"
#include "multiword_types.h"

/* Type Definitions */
#ifndef struct_Player_tag
#define struct_Player_tag

struct Player_tag
{
  int8_T x;
  int8_T y;
  int16_T orientation;
  uint8_T color;
  uint8_T position;
  uint8_T valid;
};

#endif                                 /*struct_Player_tag*/

#ifndef typedef_c9_Player
#define typedef_c9_Player

typedef struct Player_tag c9_Player;

#endif                                 /*typedef_c9_Player*/

#ifndef struct_Ball_tag
#define struct_Ball_tag

struct Ball_tag
{
  int8_T x;
  int8_T y;
  uint8_T valid;
};

#endif                                 /*struct_Ball_tag*/

#ifndef typedef_c9_Ball
#define typedef_c9_Ball

typedef struct Ball_tag c9_Ball;

#endif                                 /*typedef_c9_Ball*/

#ifndef struct_Waypoint_tag
#define struct_Waypoint_tag

struct Waypoint_tag
{
  int8_T x;
  int8_T y;
  int16_T orientation;
};

#endif                                 /*struct_Waypoint_tag*/

#ifndef typedef_c9_Waypoint
#define typedef_c9_Waypoint

typedef struct Waypoint_tag c9_Waypoint;

#endif                                 /*typedef_c9_Waypoint*/

#ifndef typedef_SFc9_greenOffenceInstanceStruct
#define typedef_SFc9_greenOffenceInstanceStruct

typedef struct {
  SimStruct *S;
  ChartInfoStruct chartInfo;
  uint32_T chartNumber;
  uint32_T instanceNumber;
  int32_T c9_sfEvent;
  uint8_T c9_tp_GameIsOn_Offence;
  uint8_T c9_tp_GetToTheBall;
  uint8_T c9_tp_Idle;
  uint8_T c9_tp_Aim;
  uint8_T c9_tp_Kick;
  uint8_T c9_tp_waiting;
  uint8_T c9_tp_Idle1;
  boolean_T c9_isStable;
  uint8_T c9_is_active_c9_greenOffence;
  uint8_T c9_is_c9_greenOffence;
  uint8_T c9_is_GameIsOn_Offence;
  uint8_T c9_is_waiting;
  int8_T c9_startingPos[2];
  c9_Player c9_enemy[3];
  c9_Player c9_friends[3];
  c9_Waypoint c9_shootWay;
  c9_Waypoint c9_kickWay;
  uint8_T c9_temporalCounter_i1;
  boolean_T c9_dataWrittenToVector[8];
  uint8_T c9_doSetSimStateSideEffects;
  const mxArray *c9_setSimStateSideEffectsInfo;
  c9_Player *c9_me;
  c9_Player (*c9_players)[6];
  c9_Ball *c9_ball;
  c9_Waypoint *c9_finalWay;
  c9_Waypoint *c9_manualWay;
  uint8_T *c9_GameOn;
  uint8_T *c9_shoot;
} SFc9_greenOffenceInstanceStruct;

#endif                                 /*typedef_SFc9_greenOffenceInstanceStruct*/

/* Named Constants */

/* Variable Declarations */
extern struct SfDebugInstanceStruct *sfGlobalDebugInstanceStruct;

/* Variable Definitions */

/* Function Declarations */
extern const mxArray *sf_c9_greenOffence_get_eml_resolved_functions_info(void);

/* Function Definitions */
extern void sf_c9_greenOffence_get_check_sum(mxArray *plhs[]);
extern void c9_greenOffence_method_dispatcher(SimStruct *S, int_T method, void
  *data);

#endif

/* Include files */

#include <stddef.h>
#include "blas.h"
#include "greenOffence_sfun.h"
#include "c9_greenOffence.h"
#include "mwmathutil.h"
#define CHARTINSTANCE_CHARTNUMBER      (chartInstance->chartNumber)
#define CHARTINSTANCE_INSTANCENUMBER   (chartInstance->instanceNumber)
#include "greenOffence_sfun_debug_macros.h"
#define _SF_MEX_LISTEN_FOR_CTRL_C(S)   sf_mex_listen_for_ctrl_c(sfGlobalDebugInstanceStruct,S);

/* Type Definitions */

/* Named Constants */
#define CALL_EVENT                     (-1)
#define c9_IN_NO_ACTIVE_CHILD          ((uint8_T)0U)
#define c9_IN_GameIsOn_Offence         ((uint8_T)1U)
#define c9_IN_waiting                  ((uint8_T)2U)
#define c9_IN_Aim                      ((uint8_T)1U)
#define c9_IN_GetToTheBall             ((uint8_T)2U)
#define c9_IN_Idle                     ((uint8_T)3U)
#define c9_IN_Kick                     ((uint8_T)4U)
#define c9_IN_Idle1                    ((uint8_T)1U)

/* Variable Declarations */

/* Variable Definitions */
static real_T _sfTime_;
static const char * c9_debug_family_names[3] = { "pos", "nargin", "nargout" };

static const char * c9_b_debug_family_names[2] = { "nargin", "nargout" };

static const char * c9_c_debug_family_names[2] = { "nargin", "nargout" };

static const char * c9_d_debug_family_names[2] = { "nargin", "nargout" };

static const char * c9_e_debug_family_names[2] = { "nargin", "nargout" };

static const char * c9_f_debug_family_names[2] = { "nargin", "nargout" };

static const char * c9_g_debug_family_names[2] = { "nargin", "nargout" };

static const char * c9_h_debug_family_names[2] = { "nargin", "nargout" };

static const char * c9_i_debug_family_names[2] = { "nargin", "nargout" };

static const char * c9_j_debug_family_names[2] = { "nargin", "nargout" };

static const char * c9_k_debug_family_names[3] = { "nargin", "nargout",
  "sf_internal_predicateOutput" };

static const char * c9_l_debug_family_names[3] = { "nargin", "nargout",
  "sf_internal_predicateOutput" };

static const char * c9_m_debug_family_names[3] = { "nargin", "nargout",
  "sf_internal_predicateOutput" };

static const char * c9_n_debug_family_names[5] = { "nargin", "nargout", "pos",
  "tol", "posReached" };

static const char * c9_o_debug_family_names[3] = { "nargin", "nargout",
  "sf_internal_predicateOutput" };

static const char * c9_p_debug_family_names[2] = { "nargin", "nargout" };

static const char * c9_q_debug_family_names[3] = { "nargin", "nargout",
  "sf_internal_predicateOutput" };

static const char * c9_r_debug_family_names[3] = { "nargin", "nargout",
  "sf_internal_predicateOutput" };

static const char * c9_s_debug_family_names[3] = { "nargin", "nargout",
  "sf_internal_predicateOutput" };

static const char * c9_t_debug_family_names[3] = { "nargin", "nargout",
  "sf_internal_predicateOutput" };

static const char * c9_u_debug_family_names[3] = { "nargin", "nargout",
  "sf_internal_predicateOutput" };

static const char * c9_v_debug_family_names[3] = { "nargin", "nargout",
  "sf_internal_predicateOutput" };

static const char * c9_w_debug_family_names[3] = { "nargin", "nargout",
  "sf_internal_predicateOutput" };

static const char * c9_x_debug_family_names[3] = { "nargin", "nargout",
  "sf_internal_predicateOutput" };

/* Function Declarations */
static void initialize_c9_greenOffence(SFc9_greenOffenceInstanceStruct
  *chartInstance);
static void initialize_params_c9_greenOffence(SFc9_greenOffenceInstanceStruct
  *chartInstance);
static void enable_c9_greenOffence(SFc9_greenOffenceInstanceStruct
  *chartInstance);
static void disable_c9_greenOffence(SFc9_greenOffenceInstanceStruct
  *chartInstance);
static void c9_update_debugger_state_c9_greenOffence
  (SFc9_greenOffenceInstanceStruct *chartInstance);
static const mxArray *get_sim_state_c9_greenOffence
  (SFc9_greenOffenceInstanceStruct *chartInstance);
static void set_sim_state_c9_greenOffence(SFc9_greenOffenceInstanceStruct
  *chartInstance, const mxArray *c9_st);
static void c9_set_sim_state_side_effects_c9_greenOffence
  (SFc9_greenOffenceInstanceStruct *chartInstance);
static void finalize_c9_greenOffence(SFc9_greenOffenceInstanceStruct
  *chartInstance);
static void sf_gateway_c9_greenOffence(SFc9_greenOffenceInstanceStruct
  *chartInstance);
static void mdl_start_c9_greenOffence(SFc9_greenOffenceInstanceStruct
  *chartInstance);
static void initSimStructsc9_greenOffence(SFc9_greenOffenceInstanceStruct
  *chartInstance);
static void c9_GameIsOn_Offence(SFc9_greenOffenceInstanceStruct *chartInstance);
static void c9_exit_internal_GameIsOn_Offence(SFc9_greenOffenceInstanceStruct
  *chartInstance);
static real32_T c9_eml_xnrm2(SFc9_greenOffenceInstanceStruct *chartInstance,
  real32_T c9_x[2]);
static void init_script_number_translation(uint32_T c9_machineNumber, uint32_T
  c9_chartNumber, uint32_T c9_instanceNumber);
static const mxArray *c9_sf_marshallOut(void *chartInstanceVoid, void *c9_inData);
static real_T c9_emlrt_marshallIn(SFc9_greenOffenceInstanceStruct *chartInstance,
  const mxArray *c9_u, const emlrtMsgIdentifier *c9_parentId);
static void c9_sf_marshallIn(void *chartInstanceVoid, const mxArray
  *c9_mxArrayInData, const char_T *c9_varName, void *c9_outData);
static const mxArray *c9_b_sf_marshallOut(void *chartInstanceVoid, void
  *c9_inData);
static void c9_b_emlrt_marshallIn(SFc9_greenOffenceInstanceStruct *chartInstance,
  const mxArray *c9_u, const emlrtMsgIdentifier *c9_parentId, int8_T c9_y[2]);
static void c9_b_sf_marshallIn(void *chartInstanceVoid, const mxArray
  *c9_mxArrayInData, const char_T *c9_varName, void *c9_outData);
static const mxArray *c9_c_sf_marshallOut(void *chartInstanceVoid, void
  *c9_inData);
static boolean_T c9_c_emlrt_marshallIn(SFc9_greenOffenceInstanceStruct
  *chartInstance, const mxArray *c9_u, const emlrtMsgIdentifier *c9_parentId);
static void c9_c_sf_marshallIn(void *chartInstanceVoid, const mxArray
  *c9_mxArrayInData, const char_T *c9_varName, void *c9_outData);
static const mxArray *c9_d_sf_marshallOut(void *chartInstanceVoid, void
  *c9_inData);
static uint8_T c9_d_emlrt_marshallIn(SFc9_greenOffenceInstanceStruct
  *chartInstance, const mxArray *c9_posReached, const char_T *c9_identifier);
static uint8_T c9_e_emlrt_marshallIn(SFc9_greenOffenceInstanceStruct
  *chartInstance, const mxArray *c9_u, const emlrtMsgIdentifier *c9_parentId);
static void c9_d_sf_marshallIn(void *chartInstanceVoid, const mxArray
  *c9_mxArrayInData, const char_T *c9_varName, void *c9_outData);
static const mxArray *c9_e_sf_marshallOut(void *chartInstanceVoid, void
  *c9_inData);
static void c9_f_emlrt_marshallIn(SFc9_greenOffenceInstanceStruct *chartInstance,
  const mxArray *c9_u, const emlrtMsgIdentifier *c9_parentId, int8_T c9_y[2]);
static void c9_e_sf_marshallIn(void *chartInstanceVoid, const mxArray
  *c9_mxArrayInData, const char_T *c9_varName, void *c9_outData);
static void c9_info_helper(const mxArray **c9_info);
static const mxArray *c9_emlrt_marshallOut(const char * c9_u);
static const mxArray *c9_b_emlrt_marshallOut(const uint32_T c9_u);
static void c9_calcShootWay(SFc9_greenOffenceInstanceStruct *chartInstance);
static void c9_calcStartTeams(SFc9_greenOffenceInstanceStruct *chartInstance);
static void c9_calcStartPos(SFc9_greenOffenceInstanceStruct *chartInstance);
static real32_T c9_b_eml_xnrm2(SFc9_greenOffenceInstanceStruct *chartInstance,
  real32_T c9_x[2]);
static void c9_threshold(SFc9_greenOffenceInstanceStruct *chartInstance);
static void c9_realmin(SFc9_greenOffenceInstanceStruct *chartInstance);
static uint8_T c9_checkReached(SFc9_greenOffenceInstanceStruct *chartInstance,
  int8_T c9_pos[2], real_T c9_tol);
static const mxArray *c9_f_sf_marshallOut(void *chartInstanceVoid, void
  *c9_inData);
static int32_T c9_g_emlrt_marshallIn(SFc9_greenOffenceInstanceStruct
  *chartInstance, const mxArray *c9_u, const emlrtMsgIdentifier *c9_parentId);
static void c9_f_sf_marshallIn(void *chartInstanceVoid, const mxArray
  *c9_mxArrayInData, const char_T *c9_varName, void *c9_outData);
static const mxArray *c9_me_bus_io(void *chartInstanceVoid, void *c9_pData);
static const mxArray *c9_g_sf_marshallOut(void *chartInstanceVoid, void
  *c9_inData);
static const mxArray *c9_players_bus_io(void *chartInstanceVoid, void *c9_pData);
static const mxArray *c9_h_sf_marshallOut(void *chartInstanceVoid, void
  *c9_inData);
static const mxArray *c9_ball_bus_io(void *chartInstanceVoid, void *c9_pData);
static const mxArray *c9_i_sf_marshallOut(void *chartInstanceVoid, void
  *c9_inData);
static const mxArray *c9_finalWay_bus_io(void *chartInstanceVoid, void *c9_pData);
static const mxArray *c9_j_sf_marshallOut(void *chartInstanceVoid, void
  *c9_inData);
static c9_Waypoint c9_h_emlrt_marshallIn(SFc9_greenOffenceInstanceStruct
  *chartInstance, const mxArray *c9_b_finalWay, const char_T *c9_identifier);
static c9_Waypoint c9_i_emlrt_marshallIn(SFc9_greenOffenceInstanceStruct
  *chartInstance, const mxArray *c9_u, const emlrtMsgIdentifier *c9_parentId);
static int8_T c9_j_emlrt_marshallIn(SFc9_greenOffenceInstanceStruct
  *chartInstance, const mxArray *c9_u, const emlrtMsgIdentifier *c9_parentId);
static int16_T c9_k_emlrt_marshallIn(SFc9_greenOffenceInstanceStruct
  *chartInstance, const mxArray *c9_u, const emlrtMsgIdentifier *c9_parentId);
static void c9_g_sf_marshallIn(void *chartInstanceVoid, const mxArray
  *c9_mxArrayInData, const char_T *c9_varName, void *c9_outData);
static const mxArray *c9_k_sf_marshallOut(void *chartInstanceVoid, void
  *c9_inData);
static void c9_l_emlrt_marshallIn(SFc9_greenOffenceInstanceStruct *chartInstance,
  const mxArray *c9_b_startingPos, const char_T *c9_identifier, int8_T c9_y[2]);
static void c9_m_emlrt_marshallIn(SFc9_greenOffenceInstanceStruct *chartInstance,
  const mxArray *c9_u, const emlrtMsgIdentifier *c9_parentId, int8_T c9_y[2]);
static void c9_h_sf_marshallIn(void *chartInstanceVoid, const mxArray
  *c9_mxArrayInData, const char_T *c9_varName, void *c9_outData);
static const mxArray *c9_l_sf_marshallOut(void *chartInstanceVoid, void
  *c9_inData);
static void c9_n_emlrt_marshallIn(SFc9_greenOffenceInstanceStruct *chartInstance,
  const mxArray *c9_b_enemy, const char_T *c9_identifier, c9_Player c9_y[3]);
static void c9_o_emlrt_marshallIn(SFc9_greenOffenceInstanceStruct *chartInstance,
  const mxArray *c9_u, const emlrtMsgIdentifier *c9_parentId, c9_Player c9_y[3]);
static void c9_i_sf_marshallIn(void *chartInstanceVoid, const mxArray
  *c9_mxArrayInData, const char_T *c9_varName, void *c9_outData);
static void c9_p_emlrt_marshallIn(SFc9_greenOffenceInstanceStruct *chartInstance,
  const mxArray *c9_b_dataWrittenToVector, const char_T *c9_identifier,
  boolean_T c9_y[8]);
static void c9_q_emlrt_marshallIn(SFc9_greenOffenceInstanceStruct *chartInstance,
  const mxArray *c9_u, const emlrtMsgIdentifier *c9_parentId, boolean_T c9_y[8]);
static const mxArray *c9_r_emlrt_marshallIn(SFc9_greenOffenceInstanceStruct
  *chartInstance, const mxArray *c9_b_setSimStateSideEffectsInfo, const char_T
  *c9_identifier);
static const mxArray *c9_s_emlrt_marshallIn(SFc9_greenOffenceInstanceStruct
  *chartInstance, const mxArray *c9_u, const emlrtMsgIdentifier *c9_parentId);
static void c9_updateDataWrittenToVector(SFc9_greenOffenceInstanceStruct
  *chartInstance, uint32_T c9_vectorIndex);
static void c9_errorIfDataNotWrittenToFcn(SFc9_greenOffenceInstanceStruct
  *chartInstance, uint32_T c9_vectorIndex, uint32_T c9_dataNumber, uint32_T
  c9_ssIdOfSourceObject, int32_T c9_offsetInSourceObject, int32_T
  c9_lengthInSourceObject);
static void init_dsm_address_info(SFc9_greenOffenceInstanceStruct *chartInstance);
static void init_simulink_io_address(SFc9_greenOffenceInstanceStruct
  *chartInstance);

/* Function Definitions */
static void initialize_c9_greenOffence(SFc9_greenOffenceInstanceStruct
  *chartInstance)
{
  chartInstance->c9_sfEvent = CALL_EVENT;
  _sfTime_ = sf_get_time(chartInstance->S);
  chartInstance->c9_doSetSimStateSideEffects = 0U;
  chartInstance->c9_setSimStateSideEffectsInfo = NULL;
  chartInstance->c9_is_GameIsOn_Offence = c9_IN_NO_ACTIVE_CHILD;
  chartInstance->c9_tp_GameIsOn_Offence = 0U;
  chartInstance->c9_tp_Aim = 0U;
  chartInstance->c9_tp_GetToTheBall = 0U;
  chartInstance->c9_tp_Idle = 0U;
  chartInstance->c9_temporalCounter_i1 = 0U;
  chartInstance->c9_tp_Kick = 0U;
  chartInstance->c9_temporalCounter_i1 = 0U;
  chartInstance->c9_is_waiting = c9_IN_NO_ACTIVE_CHILD;
  chartInstance->c9_tp_waiting = 0U;
  chartInstance->c9_tp_Idle1 = 0U;
  chartInstance->c9_is_active_c9_greenOffence = 0U;
  chartInstance->c9_is_c9_greenOffence = c9_IN_NO_ACTIVE_CHILD;
}

static void initialize_params_c9_greenOffence(SFc9_greenOffenceInstanceStruct
  *chartInstance)
{
  (void)chartInstance;
}

static void enable_c9_greenOffence(SFc9_greenOffenceInstanceStruct
  *chartInstance)
{
  _sfTime_ = sf_get_time(chartInstance->S);
}

static void disable_c9_greenOffence(SFc9_greenOffenceInstanceStruct
  *chartInstance)
{
  _sfTime_ = sf_get_time(chartInstance->S);
}

static void c9_update_debugger_state_c9_greenOffence
  (SFc9_greenOffenceInstanceStruct *chartInstance)
{
  uint32_T c9_prevAniVal;
  c9_prevAniVal = _SFD_GET_ANIMATION();
  _SFD_SET_ANIMATION(0U);
  _SFD_SET_HONOR_BREAKPOINTS(0U);
  if (chartInstance->c9_is_active_c9_greenOffence == 1U) {
    _SFD_CC_CALL(CHART_ACTIVE_TAG, 4U, chartInstance->c9_sfEvent);
  }

  if (chartInstance->c9_is_c9_greenOffence == c9_IN_GameIsOn_Offence) {
    _SFD_CS_CALL(STATE_ACTIVE_TAG, 0U, chartInstance->c9_sfEvent);
  } else {
    _SFD_CS_CALL(STATE_INACTIVE_TAG, 0U, chartInstance->c9_sfEvent);
  }

  if (chartInstance->c9_is_GameIsOn_Offence == c9_IN_GetToTheBall) {
    _SFD_CS_CALL(STATE_ACTIVE_TAG, 2U, chartInstance->c9_sfEvent);
  } else {
    _SFD_CS_CALL(STATE_INACTIVE_TAG, 2U, chartInstance->c9_sfEvent);
  }

  if (chartInstance->c9_is_GameIsOn_Offence == c9_IN_Idle) {
    _SFD_CS_CALL(STATE_ACTIVE_TAG, 3U, chartInstance->c9_sfEvent);
  } else {
    _SFD_CS_CALL(STATE_INACTIVE_TAG, 3U, chartInstance->c9_sfEvent);
  }

  if (chartInstance->c9_is_GameIsOn_Offence == c9_IN_Aim) {
    _SFD_CS_CALL(STATE_ACTIVE_TAG, 1U, chartInstance->c9_sfEvent);
  } else {
    _SFD_CS_CALL(STATE_INACTIVE_TAG, 1U, chartInstance->c9_sfEvent);
  }

  if (chartInstance->c9_is_GameIsOn_Offence == c9_IN_Kick) {
    _SFD_CS_CALL(STATE_ACTIVE_TAG, 4U, chartInstance->c9_sfEvent);
  } else {
    _SFD_CS_CALL(STATE_INACTIVE_TAG, 4U, chartInstance->c9_sfEvent);
  }

  if (chartInstance->c9_is_c9_greenOffence == c9_IN_waiting) {
    _SFD_CS_CALL(STATE_ACTIVE_TAG, 9U, chartInstance->c9_sfEvent);
  } else {
    _SFD_CS_CALL(STATE_INACTIVE_TAG, 9U, chartInstance->c9_sfEvent);
  }

  if (chartInstance->c9_is_waiting == c9_IN_Idle1) {
    _SFD_CS_CALL(STATE_ACTIVE_TAG, 10U, chartInstance->c9_sfEvent);
  } else {
    _SFD_CS_CALL(STATE_INACTIVE_TAG, 10U, chartInstance->c9_sfEvent);
  }

  _SFD_SET_ANIMATION(c9_prevAniVal);
  _SFD_SET_HONOR_BREAKPOINTS(1U);
  _SFD_ANIMATE();
}

static const mxArray *get_sim_state_c9_greenOffence
  (SFc9_greenOffenceInstanceStruct *chartInstance)
{
  const mxArray *c9_st;
  const mxArray *c9_y = NULL;
  const mxArray *c9_b_y = NULL;
  int8_T c9_u;
  const mxArray *c9_c_y = NULL;
  int8_T c9_b_u;
  const mxArray *c9_d_y = NULL;
  int16_T c9_c_u;
  const mxArray *c9_e_y = NULL;
  uint8_T c9_hoistedGlobal;
  uint8_T c9_d_u;
  const mxArray *c9_f_y = NULL;
  int32_T c9_i0;
  c9_Player c9_e_u[3];
  const mxArray *c9_g_y = NULL;
  static int32_T c9_iv0[1] = { 3 };

  int32_T c9_iv1[1];
  int32_T c9_i1;
  const c9_Player *c9_r0;
  int8_T c9_f_u;
  const mxArray *c9_h_y = NULL;
  int8_T c9_g_u;
  const mxArray *c9_i_y = NULL;
  int16_T c9_h_u;
  const mxArray *c9_j_y = NULL;
  uint8_T c9_i_u;
  const mxArray *c9_k_y = NULL;
  uint8_T c9_j_u;
  const mxArray *c9_l_y = NULL;
  uint8_T c9_k_u;
  const mxArray *c9_m_y = NULL;
  int32_T c9_i2;
  c9_Player c9_l_u[3];
  const mxArray *c9_n_y = NULL;
  int32_T c9_iv2[1];
  int32_T c9_i3;
  const c9_Player *c9_r1;
  int8_T c9_m_u;
  const mxArray *c9_o_y = NULL;
  int8_T c9_n_u;
  const mxArray *c9_p_y = NULL;
  int16_T c9_o_u;
  const mxArray *c9_q_y = NULL;
  uint8_T c9_p_u;
  const mxArray *c9_r_y = NULL;
  uint8_T c9_q_u;
  const mxArray *c9_s_y = NULL;
  uint8_T c9_r_u;
  const mxArray *c9_t_y = NULL;
  const mxArray *c9_u_y = NULL;
  int8_T c9_s_u;
  const mxArray *c9_v_y = NULL;
  int8_T c9_t_u;
  const mxArray *c9_w_y = NULL;
  int16_T c9_u_u;
  const mxArray *c9_x_y = NULL;
  const mxArray *c9_y_y = NULL;
  int8_T c9_v_u;
  const mxArray *c9_ab_y = NULL;
  int8_T c9_w_u;
  const mxArray *c9_bb_y = NULL;
  int16_T c9_x_u;
  const mxArray *c9_cb_y = NULL;
  int32_T c9_i4;
  int8_T c9_y_u[2];
  const mxArray *c9_db_y = NULL;
  uint8_T c9_b_hoistedGlobal;
  uint8_T c9_ab_u;
  const mxArray *c9_eb_y = NULL;
  uint8_T c9_c_hoistedGlobal;
  uint8_T c9_bb_u;
  const mxArray *c9_fb_y = NULL;
  uint8_T c9_d_hoistedGlobal;
  uint8_T c9_cb_u;
  const mxArray *c9_gb_y = NULL;
  uint8_T c9_e_hoistedGlobal;
  uint8_T c9_db_u;
  const mxArray *c9_hb_y = NULL;
  uint8_T c9_f_hoistedGlobal;
  uint8_T c9_eb_u;
  const mxArray *c9_ib_y = NULL;
  int32_T c9_i5;
  boolean_T c9_fb_u[8];
  const mxArray *c9_jb_y = NULL;
  c9_st = NULL;
  c9_st = NULL;
  c9_y = NULL;
  sf_mex_assign(&c9_y, sf_mex_createcellmatrix(13, 1), false);
  c9_b_y = NULL;
  sf_mex_assign(&c9_b_y, sf_mex_createstruct("structure", 2, 1, 1), false);
  c9_u = *(int8_T *)&((char_T *)chartInstance->c9_finalWay)[0];
  c9_c_y = NULL;
  sf_mex_assign(&c9_c_y, sf_mex_create("y", &c9_u, 2, 0U, 0U, 0U, 0), false);
  sf_mex_addfield(c9_b_y, c9_c_y, "x", "x", 0);
  c9_b_u = *(int8_T *)&((char_T *)chartInstance->c9_finalWay)[1];
  c9_d_y = NULL;
  sf_mex_assign(&c9_d_y, sf_mex_create("y", &c9_b_u, 2, 0U, 0U, 0U, 0), false);
  sf_mex_addfield(c9_b_y, c9_d_y, "y", "y", 0);
  c9_c_u = *(int16_T *)&((char_T *)chartInstance->c9_finalWay)[2];
  c9_e_y = NULL;
  sf_mex_assign(&c9_e_y, sf_mex_create("y", &c9_c_u, 4, 0U, 0U, 0U, 0), false);
  sf_mex_addfield(c9_b_y, c9_e_y, "orientation", "orientation", 0);
  sf_mex_setcell(c9_y, 0, c9_b_y);
  c9_hoistedGlobal = *chartInstance->c9_shoot;
  c9_d_u = c9_hoistedGlobal;
  c9_f_y = NULL;
  sf_mex_assign(&c9_f_y, sf_mex_create("y", &c9_d_u, 3, 0U, 0U, 0U, 0), false);
  sf_mex_setcell(c9_y, 1, c9_f_y);
  for (c9_i0 = 0; c9_i0 < 3; c9_i0++) {
    c9_e_u[c9_i0] = chartInstance->c9_enemy[c9_i0];
  }

  c9_g_y = NULL;
  c9_iv1[0] = c9_iv0[0];
  sf_mex_assign(&c9_g_y, sf_mex_createstructarray("structure", 1, c9_iv1), false);
  for (c9_i1 = 0; c9_i1 < 3; c9_i1++) {
    c9_r0 = &c9_e_u[c9_i1];
    c9_f_u = c9_r0->x;
    c9_h_y = NULL;
    sf_mex_assign(&c9_h_y, sf_mex_create("y", &c9_f_u, 2, 0U, 0U, 0U, 0), false);
    sf_mex_addfield(c9_g_y, c9_h_y, "x", "x", c9_i1);
    c9_g_u = c9_r0->y;
    c9_i_y = NULL;
    sf_mex_assign(&c9_i_y, sf_mex_create("y", &c9_g_u, 2, 0U, 0U, 0U, 0), false);
    sf_mex_addfield(c9_g_y, c9_i_y, "y", "y", c9_i1);
    c9_h_u = c9_r0->orientation;
    c9_j_y = NULL;
    sf_mex_assign(&c9_j_y, sf_mex_create("y", &c9_h_u, 4, 0U, 0U, 0U, 0), false);
    sf_mex_addfield(c9_g_y, c9_j_y, "orientation", "orientation", c9_i1);
    c9_i_u = c9_r0->color;
    c9_k_y = NULL;
    sf_mex_assign(&c9_k_y, sf_mex_create("y", &c9_i_u, 3, 0U, 0U, 0U, 0), false);
    sf_mex_addfield(c9_g_y, c9_k_y, "color", "color", c9_i1);
    c9_j_u = c9_r0->position;
    c9_l_y = NULL;
    sf_mex_assign(&c9_l_y, sf_mex_create("y", &c9_j_u, 3, 0U, 0U, 0U, 0), false);
    sf_mex_addfield(c9_g_y, c9_l_y, "position", "position", c9_i1);
    c9_k_u = c9_r0->valid;
    c9_m_y = NULL;
    sf_mex_assign(&c9_m_y, sf_mex_create("y", &c9_k_u, 3, 0U, 0U, 0U, 0), false);
    sf_mex_addfield(c9_g_y, c9_m_y, "valid", "valid", c9_i1);
  }

  sf_mex_setcell(c9_y, 2, c9_g_y);
  for (c9_i2 = 0; c9_i2 < 3; c9_i2++) {
    c9_l_u[c9_i2] = chartInstance->c9_friends[c9_i2];
  }

  c9_n_y = NULL;
  c9_iv2[0] = c9_iv0[0];
  sf_mex_assign(&c9_n_y, sf_mex_createstructarray("structure", 1, c9_iv2), false);
  for (c9_i3 = 0; c9_i3 < 3; c9_i3++) {
    c9_r1 = &c9_l_u[c9_i3];
    c9_m_u = c9_r1->x;
    c9_o_y = NULL;
    sf_mex_assign(&c9_o_y, sf_mex_create("y", &c9_m_u, 2, 0U, 0U, 0U, 0), false);
    sf_mex_addfield(c9_n_y, c9_o_y, "x", "x", c9_i3);
    c9_n_u = c9_r1->y;
    c9_p_y = NULL;
    sf_mex_assign(&c9_p_y, sf_mex_create("y", &c9_n_u, 2, 0U, 0U, 0U, 0), false);
    sf_mex_addfield(c9_n_y, c9_p_y, "y", "y", c9_i3);
    c9_o_u = c9_r1->orientation;
    c9_q_y = NULL;
    sf_mex_assign(&c9_q_y, sf_mex_create("y", &c9_o_u, 4, 0U, 0U, 0U, 0), false);
    sf_mex_addfield(c9_n_y, c9_q_y, "orientation", "orientation", c9_i3);
    c9_p_u = c9_r1->color;
    c9_r_y = NULL;
    sf_mex_assign(&c9_r_y, sf_mex_create("y", &c9_p_u, 3, 0U, 0U, 0U, 0), false);
    sf_mex_addfield(c9_n_y, c9_r_y, "color", "color", c9_i3);
    c9_q_u = c9_r1->position;
    c9_s_y = NULL;
    sf_mex_assign(&c9_s_y, sf_mex_create("y", &c9_q_u, 3, 0U, 0U, 0U, 0), false);
    sf_mex_addfield(c9_n_y, c9_s_y, "position", "position", c9_i3);
    c9_r_u = c9_r1->valid;
    c9_t_y = NULL;
    sf_mex_assign(&c9_t_y, sf_mex_create("y", &c9_r_u, 3, 0U, 0U, 0U, 0), false);
    sf_mex_addfield(c9_n_y, c9_t_y, "valid", "valid", c9_i3);
  }

  sf_mex_setcell(c9_y, 3, c9_n_y);
  c9_u_y = NULL;
  sf_mex_assign(&c9_u_y, sf_mex_createstruct("structure", 2, 1, 1), false);
  c9_s_u = chartInstance->c9_kickWay.x;
  c9_v_y = NULL;
  sf_mex_assign(&c9_v_y, sf_mex_create("y", &c9_s_u, 2, 0U, 0U, 0U, 0), false);
  sf_mex_addfield(c9_u_y, c9_v_y, "x", "x", 0);
  c9_t_u = chartInstance->c9_kickWay.y;
  c9_w_y = NULL;
  sf_mex_assign(&c9_w_y, sf_mex_create("y", &c9_t_u, 2, 0U, 0U, 0U, 0), false);
  sf_mex_addfield(c9_u_y, c9_w_y, "y", "y", 0);
  c9_u_u = chartInstance->c9_kickWay.orientation;
  c9_x_y = NULL;
  sf_mex_assign(&c9_x_y, sf_mex_create("y", &c9_u_u, 4, 0U, 0U, 0U, 0), false);
  sf_mex_addfield(c9_u_y, c9_x_y, "orientation", "orientation", 0);
  sf_mex_setcell(c9_y, 4, c9_u_y);
  c9_y_y = NULL;
  sf_mex_assign(&c9_y_y, sf_mex_createstruct("structure", 2, 1, 1), false);
  c9_v_u = chartInstance->c9_shootWay.x;
  c9_ab_y = NULL;
  sf_mex_assign(&c9_ab_y, sf_mex_create("y", &c9_v_u, 2, 0U, 0U, 0U, 0), false);
  sf_mex_addfield(c9_y_y, c9_ab_y, "x", "x", 0);
  c9_w_u = chartInstance->c9_shootWay.y;
  c9_bb_y = NULL;
  sf_mex_assign(&c9_bb_y, sf_mex_create("y", &c9_w_u, 2, 0U, 0U, 0U, 0), false);
  sf_mex_addfield(c9_y_y, c9_bb_y, "y", "y", 0);
  c9_x_u = chartInstance->c9_shootWay.orientation;
  c9_cb_y = NULL;
  sf_mex_assign(&c9_cb_y, sf_mex_create("y", &c9_x_u, 4, 0U, 0U, 0U, 0), false);
  sf_mex_addfield(c9_y_y, c9_cb_y, "orientation", "orientation", 0);
  sf_mex_setcell(c9_y, 5, c9_y_y);
  for (c9_i4 = 0; c9_i4 < 2; c9_i4++) {
    c9_y_u[c9_i4] = chartInstance->c9_startingPos[c9_i4];
  }

  c9_db_y = NULL;
  sf_mex_assign(&c9_db_y, sf_mex_create("y", c9_y_u, 2, 0U, 1U, 0U, 2, 2, 1),
                false);
  sf_mex_setcell(c9_y, 6, c9_db_y);
  c9_b_hoistedGlobal = chartInstance->c9_is_active_c9_greenOffence;
  c9_ab_u = c9_b_hoistedGlobal;
  c9_eb_y = NULL;
  sf_mex_assign(&c9_eb_y, sf_mex_create("y", &c9_ab_u, 3, 0U, 0U, 0U, 0), false);
  sf_mex_setcell(c9_y, 7, c9_eb_y);
  c9_c_hoistedGlobal = chartInstance->c9_is_c9_greenOffence;
  c9_bb_u = c9_c_hoistedGlobal;
  c9_fb_y = NULL;
  sf_mex_assign(&c9_fb_y, sf_mex_create("y", &c9_bb_u, 3, 0U, 0U, 0U, 0), false);
  sf_mex_setcell(c9_y, 8, c9_fb_y);
  c9_d_hoistedGlobal = chartInstance->c9_is_GameIsOn_Offence;
  c9_cb_u = c9_d_hoistedGlobal;
  c9_gb_y = NULL;
  sf_mex_assign(&c9_gb_y, sf_mex_create("y", &c9_cb_u, 3, 0U, 0U, 0U, 0), false);
  sf_mex_setcell(c9_y, 9, c9_gb_y);
  c9_e_hoistedGlobal = chartInstance->c9_is_waiting;
  c9_db_u = c9_e_hoistedGlobal;
  c9_hb_y = NULL;
  sf_mex_assign(&c9_hb_y, sf_mex_create("y", &c9_db_u, 3, 0U, 0U, 0U, 0), false);
  sf_mex_setcell(c9_y, 10, c9_hb_y);
  c9_f_hoistedGlobal = chartInstance->c9_temporalCounter_i1;
  c9_eb_u = c9_f_hoistedGlobal;
  c9_ib_y = NULL;
  sf_mex_assign(&c9_ib_y, sf_mex_create("y", &c9_eb_u, 3, 0U, 0U, 0U, 0), false);
  sf_mex_setcell(c9_y, 11, c9_ib_y);
  for (c9_i5 = 0; c9_i5 < 8; c9_i5++) {
    c9_fb_u[c9_i5] = chartInstance->c9_dataWrittenToVector[c9_i5];
  }

  c9_jb_y = NULL;
  sf_mex_assign(&c9_jb_y, sf_mex_create("y", c9_fb_u, 11, 0U, 1U, 0U, 1, 8),
                false);
  sf_mex_setcell(c9_y, 12, c9_jb_y);
  sf_mex_assign(&c9_st, c9_y, false);
  return c9_st;
}

static void set_sim_state_c9_greenOffence(SFc9_greenOffenceInstanceStruct
  *chartInstance, const mxArray *c9_st)
{
  const mxArray *c9_u;
  c9_Waypoint c9_r2;
  c9_Player c9_rv0[3];
  int32_T c9_i6;
  c9_Player c9_rv1[3];
  int32_T c9_i7;
  int8_T c9_iv3[2];
  int32_T c9_i8;
  boolean_T c9_bv0[8];
  int32_T c9_i9;
  c9_u = sf_mex_dup(c9_st);
  c9_r2 = c9_h_emlrt_marshallIn(chartInstance, sf_mex_dup(sf_mex_getcell(c9_u, 0)),
    "finalWay");
  *(int8_T *)&((char_T *)chartInstance->c9_finalWay)[0] = c9_r2.x;
  *(int8_T *)&((char_T *)chartInstance->c9_finalWay)[1] = c9_r2.y;
  *(int16_T *)&((char_T *)chartInstance->c9_finalWay)[2] = c9_r2.orientation;
  *chartInstance->c9_shoot = c9_d_emlrt_marshallIn(chartInstance, sf_mex_dup
    (sf_mex_getcell(c9_u, 1)), "shoot");
  c9_n_emlrt_marshallIn(chartInstance, sf_mex_dup(sf_mex_getcell(c9_u, 2)),
                        "enemy", c9_rv0);
  for (c9_i6 = 0; c9_i6 < 3; c9_i6++) {
    chartInstance->c9_enemy[c9_i6] = c9_rv0[c9_i6];
  }

  c9_n_emlrt_marshallIn(chartInstance, sf_mex_dup(sf_mex_getcell(c9_u, 3)),
                        "friends", c9_rv1);
  for (c9_i7 = 0; c9_i7 < 3; c9_i7++) {
    chartInstance->c9_friends[c9_i7] = c9_rv1[c9_i7];
  }

  chartInstance->c9_kickWay = c9_h_emlrt_marshallIn(chartInstance, sf_mex_dup
    (sf_mex_getcell(c9_u, 4)), "kickWay");
  chartInstance->c9_shootWay = c9_h_emlrt_marshallIn(chartInstance, sf_mex_dup
    (sf_mex_getcell(c9_u, 5)), "shootWay");
  c9_l_emlrt_marshallIn(chartInstance, sf_mex_dup(sf_mex_getcell(c9_u, 6)),
                        "startingPos", c9_iv3);
  for (c9_i8 = 0; c9_i8 < 2; c9_i8++) {
    chartInstance->c9_startingPos[c9_i8] = c9_iv3[c9_i8];
  }

  chartInstance->c9_is_active_c9_greenOffence = c9_d_emlrt_marshallIn
    (chartInstance, sf_mex_dup(sf_mex_getcell(c9_u, 7)),
     "is_active_c9_greenOffence");
  chartInstance->c9_is_c9_greenOffence = c9_d_emlrt_marshallIn(chartInstance,
    sf_mex_dup(sf_mex_getcell(c9_u, 8)), "is_c9_greenOffence");
  chartInstance->c9_is_GameIsOn_Offence = c9_d_emlrt_marshallIn(chartInstance,
    sf_mex_dup(sf_mex_getcell(c9_u, 9)), "is_GameIsOn_Offence");
  chartInstance->c9_is_waiting = c9_d_emlrt_marshallIn(chartInstance, sf_mex_dup
    (sf_mex_getcell(c9_u, 10)), "is_waiting");
  chartInstance->c9_temporalCounter_i1 = c9_d_emlrt_marshallIn(chartInstance,
    sf_mex_dup(sf_mex_getcell(c9_u, 11)), "temporalCounter_i1");
  c9_p_emlrt_marshallIn(chartInstance, sf_mex_dup(sf_mex_getcell(c9_u, 12)),
                        "dataWrittenToVector", c9_bv0);
  for (c9_i9 = 0; c9_i9 < 8; c9_i9++) {
    chartInstance->c9_dataWrittenToVector[c9_i9] = c9_bv0[c9_i9];
  }

  sf_mex_assign(&chartInstance->c9_setSimStateSideEffectsInfo,
                c9_r_emlrt_marshallIn(chartInstance, sf_mex_dup(sf_mex_getcell
    (c9_u, 13)), "setSimStateSideEffectsInfo"), true);
  sf_mex_destroy(&c9_u);
  chartInstance->c9_doSetSimStateSideEffects = 1U;
  c9_update_debugger_state_c9_greenOffence(chartInstance);
  sf_mex_destroy(&c9_st);
}

static void c9_set_sim_state_side_effects_c9_greenOffence
  (SFc9_greenOffenceInstanceStruct *chartInstance)
{
  if (chartInstance->c9_doSetSimStateSideEffects != 0) {
    if (chartInstance->c9_is_c9_greenOffence == c9_IN_GameIsOn_Offence) {
      chartInstance->c9_tp_GameIsOn_Offence = 1U;
    } else {
      chartInstance->c9_tp_GameIsOn_Offence = 0U;
    }

    if (chartInstance->c9_is_GameIsOn_Offence == c9_IN_Aim) {
      chartInstance->c9_tp_Aim = 1U;
    } else {
      chartInstance->c9_tp_Aim = 0U;
    }

    if (chartInstance->c9_is_GameIsOn_Offence == c9_IN_GetToTheBall) {
      chartInstance->c9_tp_GetToTheBall = 1U;
    } else {
      chartInstance->c9_tp_GetToTheBall = 0U;
    }

    if (chartInstance->c9_is_GameIsOn_Offence == c9_IN_Idle) {
      chartInstance->c9_tp_Idle = 1U;
      if (sf_mex_sub(chartInstance->c9_setSimStateSideEffectsInfo,
                     "setSimStateSideEffectsInfo", 1, 5) == 0.0) {
        chartInstance->c9_temporalCounter_i1 = 0U;
      }
    } else {
      chartInstance->c9_tp_Idle = 0U;
    }

    if (chartInstance->c9_is_GameIsOn_Offence == c9_IN_Kick) {
      chartInstance->c9_tp_Kick = 1U;
      if (sf_mex_sub(chartInstance->c9_setSimStateSideEffectsInfo,
                     "setSimStateSideEffectsInfo", 1, 6) == 0.0) {
        chartInstance->c9_temporalCounter_i1 = 0U;
      }
    } else {
      chartInstance->c9_tp_Kick = 0U;
    }

    if (chartInstance->c9_is_c9_greenOffence == c9_IN_waiting) {
      chartInstance->c9_tp_waiting = 1U;
    } else {
      chartInstance->c9_tp_waiting = 0U;
    }

    if (chartInstance->c9_is_waiting == c9_IN_Idle1) {
      chartInstance->c9_tp_Idle1 = 1U;
    } else {
      chartInstance->c9_tp_Idle1 = 0U;
    }

    chartInstance->c9_doSetSimStateSideEffects = 0U;
  }
}

static void finalize_c9_greenOffence(SFc9_greenOffenceInstanceStruct
  *chartInstance)
{
  sf_mex_destroy(&chartInstance->c9_setSimStateSideEffectsInfo);
}

static void sf_gateway_c9_greenOffence(SFc9_greenOffenceInstanceStruct
  *chartInstance)
{
  int32_T c9_i10;
  uint32_T c9_debug_family_var_map[2];
  real_T c9_nargin = 0.0;
  real_T c9_nargout = 0.0;
  uint32_T c9_b_debug_family_var_map[3];
  real_T c9_b_nargin = 0.0;
  real_T c9_b_nargout = 1.0;
  boolean_T c9_out;
  real_T c9_c_nargin = 0.0;
  real_T c9_c_nargout = 0.0;
  real_T c9_d_nargin = 0.0;
  real_T c9_d_nargout = 0.0;
  c9_set_sim_state_side_effects_c9_greenOffence(chartInstance);
  _SFD_SYMBOL_SCOPE_PUSH(0U, 0U);
  _sfTime_ = sf_get_time(chartInstance->S);
  _SFD_CC_CALL(CHART_ENTER_SFUNCTION_TAG, 4U, chartInstance->c9_sfEvent);
  for (c9_i10 = 0; c9_i10 < 2; c9_i10++) {
    _SFD_DATA_RANGE_CHECK((real_T)chartInstance->c9_startingPos[c9_i10], 5U);
  }

  _SFD_DATA_RANGE_CHECK((real_T)*chartInstance->c9_GameOn, 6U);
  _SFD_DATA_RANGE_CHECK((real_T)*chartInstance->c9_shoot, 11U);
  chartInstance->c9_sfEvent = CALL_EVENT;
  if (chartInstance->c9_temporalCounter_i1 < 31U) {
    chartInstance->c9_temporalCounter_i1++;
  }

  _SFD_CC_CALL(CHART_ENTER_DURING_FUNCTION_TAG, 4U, chartInstance->c9_sfEvent);
  if (chartInstance->c9_is_active_c9_greenOffence == 0U) {
    _SFD_CC_CALL(CHART_ENTER_ENTRY_FUNCTION_TAG, 4U, chartInstance->c9_sfEvent);
    chartInstance->c9_is_active_c9_greenOffence = 1U;
    _SFD_CC_CALL(EXIT_OUT_OF_FUNCTION_TAG, 4U, chartInstance->c9_sfEvent);
    _SFD_CT_CALL(TRANSITION_ACTIVE_TAG, 3U, chartInstance->c9_sfEvent);
    _SFD_SYMBOL_SCOPE_PUSH_EML(0U, 2U, 2U, c9_p_debug_family_names,
      c9_debug_family_var_map);
    _SFD_SYMBOL_SCOPE_ADD_EML_IMPORTABLE(&c9_nargin, 0U, c9_sf_marshallOut,
      c9_sf_marshallIn);
    _SFD_SYMBOL_SCOPE_ADD_EML_IMPORTABLE(&c9_nargout, 1U, c9_sf_marshallOut,
      c9_sf_marshallIn);
    c9_calcStartPos(chartInstance);
    c9_calcStartTeams(chartInstance);
    *chartInstance->c9_shoot = 0U;
    c9_updateDataWrittenToVector(chartInstance, 6U);
    _SFD_DATA_RANGE_CHECK((real_T)*chartInstance->c9_shoot, 11U);
    _SFD_SYMBOL_SCOPE_POP();
    chartInstance->c9_is_c9_greenOffence = c9_IN_waiting;
    _SFD_CS_CALL(STATE_ACTIVE_TAG, 9U, chartInstance->c9_sfEvent);
    chartInstance->c9_tp_waiting = 1U;
    _SFD_CT_CALL(TRANSITION_ACTIVE_TAG, 2U, chartInstance->c9_sfEvent);
    chartInstance->c9_is_waiting = c9_IN_Idle1;
    _SFD_CS_CALL(STATE_ACTIVE_TAG, 10U, chartInstance->c9_sfEvent);
    chartInstance->c9_tp_Idle1 = 1U;
  } else {
    switch (chartInstance->c9_is_c9_greenOffence) {
     case c9_IN_GameIsOn_Offence:
      CV_CHART_EVAL(4, 0, 1);
      c9_GameIsOn_Offence(chartInstance);
      break;

     case c9_IN_waiting:
      CV_CHART_EVAL(4, 0, 2);
      _SFD_CT_CALL(TRANSITION_BEFORE_PROCESSING_TAG, 0U,
                   chartInstance->c9_sfEvent);
      _SFD_SYMBOL_SCOPE_PUSH_EML(0U, 3U, 3U, c9_q_debug_family_names,
        c9_b_debug_family_var_map);
      _SFD_SYMBOL_SCOPE_ADD_EML_IMPORTABLE(&c9_b_nargin, 0U, c9_sf_marshallOut,
        c9_sf_marshallIn);
      _SFD_SYMBOL_SCOPE_ADD_EML_IMPORTABLE(&c9_b_nargout, 1U, c9_sf_marshallOut,
        c9_sf_marshallIn);
      _SFD_SYMBOL_SCOPE_ADD_EML_IMPORTABLE(&c9_out, 2U, c9_c_sf_marshallOut,
        c9_c_sf_marshallIn);
      c9_out = CV_EML_IF(0, 0, 0, CV_RELATIONAL_EVAL(5U, 0U, 0, (real_T)
        *chartInstance->c9_GameOn, 1.0, 0, 0U, *chartInstance->c9_GameOn == 1));
      _SFD_SYMBOL_SCOPE_POP();
      if (c9_out) {
        _SFD_CT_CALL(TRANSITION_ACTIVE_TAG, 0U, chartInstance->c9_sfEvent);
        chartInstance->c9_tp_Idle1 = 0U;
        chartInstance->c9_is_waiting = c9_IN_NO_ACTIVE_CHILD;
        _SFD_CS_CALL(STATE_INACTIVE_TAG, 10U, chartInstance->c9_sfEvent);
        chartInstance->c9_tp_waiting = 0U;
        _SFD_CS_CALL(STATE_INACTIVE_TAG, 9U, chartInstance->c9_sfEvent);
        chartInstance->c9_is_c9_greenOffence = c9_IN_GameIsOn_Offence;
        _SFD_CS_CALL(STATE_ACTIVE_TAG, 0U, chartInstance->c9_sfEvent);
        chartInstance->c9_tp_GameIsOn_Offence = 1U;
        _SFD_CT_CALL(TRANSITION_ACTIVE_TAG, 4U, chartInstance->c9_sfEvent);
        chartInstance->c9_is_GameIsOn_Offence = c9_IN_GetToTheBall;
        _SFD_CS_CALL(STATE_ACTIVE_TAG, 2U, chartInstance->c9_sfEvent);
        chartInstance->c9_tp_GetToTheBall = 1U;
        _SFD_SYMBOL_SCOPE_PUSH_EML(0U, 2U, 2U, c9_e_debug_family_names,
          c9_debug_family_var_map);
        _SFD_SYMBOL_SCOPE_ADD_EML_IMPORTABLE(&c9_c_nargin, 0U, c9_sf_marshallOut,
          c9_sf_marshallIn);
        _SFD_SYMBOL_SCOPE_ADD_EML_IMPORTABLE(&c9_c_nargout, 1U,
          c9_sf_marshallOut, c9_sf_marshallIn);
        c9_calcShootWay(chartInstance);
        _SFD_SYMBOL_SCOPE_POP();
      } else {
        _SFD_CS_CALL(STATE_ENTER_DURING_FUNCTION_TAG, 9U,
                     chartInstance->c9_sfEvent);
        _SFD_CS_CALL(STATE_ENTER_DURING_FUNCTION_TAG, 10U,
                     chartInstance->c9_sfEvent);
        _SFD_SYMBOL_SCOPE_PUSH_EML(0U, 2U, 2U, c9_j_debug_family_names,
          c9_debug_family_var_map);
        _SFD_SYMBOL_SCOPE_ADD_EML_IMPORTABLE(&c9_d_nargin, 0U, c9_sf_marshallOut,
          c9_sf_marshallIn);
        _SFD_SYMBOL_SCOPE_ADD_EML_IMPORTABLE(&c9_d_nargout, 1U,
          c9_sf_marshallOut, c9_sf_marshallIn);
        *(int8_T *)&((char_T *)chartInstance->c9_finalWay)[0] = *(int8_T *)
          &((char_T *)chartInstance->c9_me)[0];
        c9_updateDataWrittenToVector(chartInstance, 0U);
        *(int8_T *)&((char_T *)chartInstance->c9_finalWay)[1] = *(int8_T *)
          &((char_T *)chartInstance->c9_me)[1];
        c9_updateDataWrittenToVector(chartInstance, 0U);
        *(int16_T *)&((char_T *)chartInstance->c9_finalWay)[2] = *(int16_T *)
          &((char_T *)chartInstance->c9_me)[2];
        c9_updateDataWrittenToVector(chartInstance, 0U);
        _SFD_SYMBOL_SCOPE_POP();
        _SFD_CS_CALL(EXIT_OUT_OF_FUNCTION_TAG, 10U, chartInstance->c9_sfEvent);
      }

      _SFD_CS_CALL(EXIT_OUT_OF_FUNCTION_TAG, 9U, chartInstance->c9_sfEvent);
      break;

     default:
      CV_CHART_EVAL(4, 0, 0);
      chartInstance->c9_is_c9_greenOffence = c9_IN_NO_ACTIVE_CHILD;
      _SFD_CS_CALL(STATE_INACTIVE_TAG, 0U, chartInstance->c9_sfEvent);
      break;
    }
  }

  _SFD_CC_CALL(EXIT_OUT_OF_FUNCTION_TAG, 4U, chartInstance->c9_sfEvent);
  _SFD_SYMBOL_SCOPE_POP();
  _SFD_CHECK_FOR_STATE_INCONSISTENCY(_greenOffenceMachineNumber_,
    chartInstance->chartNumber, chartInstance->instanceNumber);
}

static void mdl_start_c9_greenOffence(SFc9_greenOffenceInstanceStruct
  *chartInstance)
{
  (void)chartInstance;
}

static void initSimStructsc9_greenOffence(SFc9_greenOffenceInstanceStruct
  *chartInstance)
{
  (void)chartInstance;
}

static void c9_GameIsOn_Offence(SFc9_greenOffenceInstanceStruct *chartInstance)
{
  uint32_T c9_debug_family_var_map[3];
  real_T c9_nargin = 0.0;
  real_T c9_nargout = 1.0;
  boolean_T c9_out;
  uint32_T c9_b_debug_family_var_map[2];
  real_T c9_b_nargin = 0.0;
  real_T c9_b_nargout = 0.0;
  real_T c9_c_nargin = 0.0;
  real_T c9_c_nargout = 1.0;
  boolean_T c9_b_out;
  int32_T c9_i11;
  int16_T c9_x;
  int16_T c9_b_x;
  int32_T c9_i12;
  int16_T c9_y;
  real_T c9_d0;
  real_T c9_d_nargin = 0.0;
  real_T c9_d_nargout = 0.0;
  real_T c9_e_nargin = 0.0;
  real_T c9_e_nargout = 0.0;
  real_T c9_f_nargin = 0.0;
  real_T c9_f_nargout = 1.0;
  boolean_T c9_c_out;
  int8_T c9_iv4[2];
  uint8_T c9_u0;
  real_T c9_g_nargin = 0.0;
  real_T c9_g_nargout = 0.0;
  real_T c9_h_nargin = 0.0;
  real_T c9_h_nargout = 1.0;
  boolean_T c9_d_out;
  real_T c9_i_nargin = 0.0;
  real_T c9_i_nargout = 0.0;
  real_T c9_j_nargin = 0.0;
  real_T c9_j_nargout = 0.0;
  real_T c9_k_nargin = 0.0;
  real_T c9_k_nargout = 1.0;
  boolean_T c9_e_out;
  _SFD_CT_CALL(TRANSITION_BEFORE_PROCESSING_TAG, 1U, chartInstance->c9_sfEvent);
  _SFD_SYMBOL_SCOPE_PUSH_EML(0U, 3U, 3U, c9_r_debug_family_names,
    c9_debug_family_var_map);
  _SFD_SYMBOL_SCOPE_ADD_EML_IMPORTABLE(&c9_nargin, 0U, c9_sf_marshallOut,
    c9_sf_marshallIn);
  _SFD_SYMBOL_SCOPE_ADD_EML_IMPORTABLE(&c9_nargout, 1U, c9_sf_marshallOut,
    c9_sf_marshallIn);
  _SFD_SYMBOL_SCOPE_ADD_EML_IMPORTABLE(&c9_out, 2U, c9_c_sf_marshallOut,
    c9_c_sf_marshallIn);
  c9_out = CV_EML_IF(1, 0, 0, CV_RELATIONAL_EVAL(5U, 1U, 0, (real_T)
    *chartInstance->c9_GameOn, 0.0, 0, 0U, *chartInstance->c9_GameOn == 0));
  _SFD_SYMBOL_SCOPE_POP();
  if (c9_out) {
    _SFD_CT_CALL(TRANSITION_ACTIVE_TAG, 1U, chartInstance->c9_sfEvent);
    c9_exit_internal_GameIsOn_Offence(chartInstance);
    chartInstance->c9_tp_GameIsOn_Offence = 0U;
    _SFD_CS_CALL(STATE_INACTIVE_TAG, 0U, chartInstance->c9_sfEvent);
    chartInstance->c9_is_c9_greenOffence = c9_IN_waiting;
    _SFD_CS_CALL(STATE_ACTIVE_TAG, 9U, chartInstance->c9_sfEvent);
    chartInstance->c9_tp_waiting = 1U;
    _SFD_CT_CALL(TRANSITION_ACTIVE_TAG, 2U, chartInstance->c9_sfEvent);
    chartInstance->c9_is_waiting = c9_IN_Idle1;
    _SFD_CS_CALL(STATE_ACTIVE_TAG, 10U, chartInstance->c9_sfEvent);
    chartInstance->c9_tp_Idle1 = 1U;
  } else {
    _SFD_CS_CALL(STATE_ENTER_DURING_FUNCTION_TAG, 0U, chartInstance->c9_sfEvent);
    _SFD_SYMBOL_SCOPE_PUSH_EML(0U, 2U, 2U, c9_d_debug_family_names,
      c9_b_debug_family_var_map);
    _SFD_SYMBOL_SCOPE_ADD_EML_IMPORTABLE(&c9_b_nargin, 0U, c9_sf_marshallOut,
      c9_sf_marshallIn);
    _SFD_SYMBOL_SCOPE_ADD_EML_IMPORTABLE(&c9_b_nargout, 1U, c9_sf_marshallOut,
      c9_sf_marshallIn);
    *chartInstance->c9_shoot = 0U;
    c9_updateDataWrittenToVector(chartInstance, 6U);
    _SFD_DATA_RANGE_CHECK((real_T)*chartInstance->c9_shoot, 11U);
    _SFD_SYMBOL_SCOPE_POP();
    switch (chartInstance->c9_is_GameIsOn_Offence) {
     case c9_IN_Aim:
      CV_STATE_EVAL(0, 0, 1);
      _SFD_CT_CALL(TRANSITION_BEFORE_PROCESSING_TAG, 7U,
                   chartInstance->c9_sfEvent);
      _SFD_SYMBOL_SCOPE_PUSH_EML(0U, 3U, 3U, c9_o_debug_family_names,
        c9_debug_family_var_map);
      _SFD_SYMBOL_SCOPE_ADD_EML_IMPORTABLE(&c9_c_nargin, 0U, c9_sf_marshallOut,
        c9_sf_marshallIn);
      _SFD_SYMBOL_SCOPE_ADD_EML_IMPORTABLE(&c9_c_nargout, 1U, c9_sf_marshallOut,
        c9_sf_marshallIn);
      _SFD_SYMBOL_SCOPE_ADD_EML_IMPORTABLE(&c9_b_out, 2U, c9_c_sf_marshallOut,
        c9_c_sf_marshallIn);
      c9_errorIfDataNotWrittenToFcn(chartInstance, 0U, 3U, 121U, 22, 20);
      c9_i11 = *(int16_T *)&((char_T *)chartInstance->c9_me)[2] - *(int16_T *)
        &((char_T *)chartInstance->c9_finalWay)[2];
      if (c9_i11 > 32767) {
        CV_SATURATION_EVAL(5, 7, 1, 0, 1);
        c9_i11 = 32767;
      } else {
        if (CV_SATURATION_EVAL(5, 7, 1, 0, c9_i11 < -32768)) {
          c9_i11 = -32768;
        }
      }

      c9_x = (int16_T)c9_i11;
      c9_b_x = c9_x;
      c9_i12 = -c9_b_x;
      if (c9_i12 > 32767) {
        CV_SATURATION_EVAL(5, 7, 0, 0, 1);
        c9_i12 = 32767;
      } else {
        if (CV_SATURATION_EVAL(5, 7, 0, 0, c9_i12 < -32768)) {
          c9_i12 = -32768;
        }
      }

      if ((real_T)c9_b_x < 0.0) {
        c9_y = (int16_T)c9_i12;
      } else {
        c9_y = c9_b_x;
      }

      c9_d0 = (real_T)c9_y;
      c9_b_out = CV_EML_IF(7, 0, 0, CV_RELATIONAL_EVAL(5U, 7U, 0, c9_d0, 30.0,
        -1, 2U, c9_d0 < 30.0));
      _SFD_SYMBOL_SCOPE_POP();
      if (c9_b_out) {
        _SFD_CT_CALL(TRANSITION_ACTIVE_TAG, 7U, chartInstance->c9_sfEvent);
        chartInstance->c9_tp_Aim = 0U;
        _SFD_CS_CALL(STATE_INACTIVE_TAG, 1U, chartInstance->c9_sfEvent);
        chartInstance->c9_is_GameIsOn_Offence = c9_IN_Kick;
        _SFD_CS_CALL(STATE_ACTIVE_TAG, 4U, chartInstance->c9_sfEvent);
        chartInstance->c9_temporalCounter_i1 = 0U;
        chartInstance->c9_tp_Kick = 1U;
        _SFD_SYMBOL_SCOPE_PUSH_EML(0U, 2U, 2U, c9_i_debug_family_names,
          c9_b_debug_family_var_map);
        _SFD_SYMBOL_SCOPE_ADD_EML_IMPORTABLE(&c9_d_nargin, 0U, c9_sf_marshallOut,
          c9_sf_marshallIn);
        _SFD_SYMBOL_SCOPE_ADD_EML_IMPORTABLE(&c9_d_nargout, 1U,
          c9_sf_marshallOut, c9_sf_marshallIn);
        *(int8_T *)&((char_T *)chartInstance->c9_finalWay)[0] = *(int8_T *)
          &((char_T *)chartInstance->c9_ball)[0];
        c9_updateDataWrittenToVector(chartInstance, 0U);
        *(int8_T *)&((char_T *)chartInstance->c9_finalWay)[1] = *(int8_T *)
          &((char_T *)chartInstance->c9_ball)[1];
        c9_updateDataWrittenToVector(chartInstance, 0U);
        *(int16_T *)&((char_T *)chartInstance->c9_finalWay)[2] = MAX_int16_T;
        c9_updateDataWrittenToVector(chartInstance, 0U);
        *chartInstance->c9_shoot = 1U;
        c9_updateDataWrittenToVector(chartInstance, 6U);
        _SFD_DATA_RANGE_CHECK((real_T)*chartInstance->c9_shoot, 11U);
        _SFD_SYMBOL_SCOPE_POP();
      } else {
        _SFD_CS_CALL(STATE_ENTER_DURING_FUNCTION_TAG, 1U,
                     chartInstance->c9_sfEvent);
        _SFD_SYMBOL_SCOPE_PUSH_EML(0U, 2U, 2U, c9_h_debug_family_names,
          c9_b_debug_family_var_map);
        _SFD_SYMBOL_SCOPE_ADD_EML_IMPORTABLE(&c9_e_nargin, 0U, c9_sf_marshallOut,
          c9_sf_marshallIn);
        _SFD_SYMBOL_SCOPE_ADD_EML_IMPORTABLE(&c9_e_nargout, 1U,
          c9_sf_marshallOut, c9_sf_marshallIn);
        c9_calcShootWay(chartInstance);
        *(int8_T *)&((char_T *)chartInstance->c9_finalWay)[0] = *(int8_T *)
          &((char_T *)chartInstance->c9_me)[0];
        c9_updateDataWrittenToVector(chartInstance, 0U);
        *(int8_T *)&((char_T *)chartInstance->c9_finalWay)[1] = *(int8_T *)
          &((char_T *)chartInstance->c9_me)[1];
        c9_updateDataWrittenToVector(chartInstance, 0U);
        c9_errorIfDataNotWrittenToFcn(chartInstance, 4U, 9U, 115U, 78, 20);
        *(int16_T *)&((char_T *)chartInstance->c9_finalWay)[2] =
          chartInstance->c9_shootWay.orientation;
        c9_updateDataWrittenToVector(chartInstance, 0U);
        _SFD_SYMBOL_SCOPE_POP();
      }

      _SFD_CS_CALL(EXIT_OUT_OF_FUNCTION_TAG, 1U, chartInstance->c9_sfEvent);
      break;

     case c9_IN_GetToTheBall:
      CV_STATE_EVAL(0, 0, 2);
      _SFD_CT_CALL(TRANSITION_BEFORE_PROCESSING_TAG, 5U,
                   chartInstance->c9_sfEvent);
      _SFD_SYMBOL_SCOPE_PUSH_EML(0U, 3U, 3U, c9_m_debug_family_names,
        c9_debug_family_var_map);
      _SFD_SYMBOL_SCOPE_ADD_EML_IMPORTABLE(&c9_f_nargin, 0U, c9_sf_marshallOut,
        c9_sf_marshallIn);
      _SFD_SYMBOL_SCOPE_ADD_EML_IMPORTABLE(&c9_f_nargout, 1U, c9_sf_marshallOut,
        c9_sf_marshallIn);
      _SFD_SYMBOL_SCOPE_ADD_EML_IMPORTABLE(&c9_c_out, 2U, c9_c_sf_marshallOut,
        c9_c_sf_marshallIn);
      c9_errorIfDataNotWrittenToFcn(chartInstance, 4U, 9U, 116U, 15, 10);
      c9_iv4[0] = chartInstance->c9_shootWay.x;
      c9_iv4[1] = chartInstance->c9_shootWay.y;
      c9_u0 = c9_checkReached(chartInstance, c9_iv4, 20.0);
      if (CV_EML_IF(5, 0, 0, CV_RELATIONAL_EVAL(5U, 5U, 0, (real_T)c9_u0, 1.0, 0,
            0U, c9_u0 == 1))) {
        c9_c_out = true;
      } else {
        c9_updateDataWrittenToVector(chartInstance, 4U);
        c9_c_out = false;
      }

      _SFD_SYMBOL_SCOPE_POP();
      if (c9_c_out) {
        _SFD_CT_CALL(TRANSITION_ACTIVE_TAG, 5U, chartInstance->c9_sfEvent);
        chartInstance->c9_tp_GetToTheBall = 0U;
        _SFD_CS_CALL(STATE_INACTIVE_TAG, 2U, chartInstance->c9_sfEvent);
        chartInstance->c9_is_GameIsOn_Offence = c9_IN_Aim;
        _SFD_CS_CALL(STATE_ACTIVE_TAG, 1U, chartInstance->c9_sfEvent);
        chartInstance->c9_tp_Aim = 1U;
      } else {
        _SFD_CS_CALL(STATE_ENTER_DURING_FUNCTION_TAG, 2U,
                     chartInstance->c9_sfEvent);
        _SFD_SYMBOL_SCOPE_PUSH_EML(0U, 2U, 2U, c9_f_debug_family_names,
          c9_b_debug_family_var_map);
        _SFD_SYMBOL_SCOPE_ADD_EML_IMPORTABLE(&c9_g_nargin, 0U, c9_sf_marshallOut,
          c9_sf_marshallIn);
        _SFD_SYMBOL_SCOPE_ADD_EML_IMPORTABLE(&c9_g_nargout, 1U,
          c9_sf_marshallOut, c9_sf_marshallIn);
        c9_calcShootWay(chartInstance);
        c9_errorIfDataNotWrittenToFcn(chartInstance, 4U, 9U, 60U, 61, 10);
        *(int8_T *)&((char_T *)chartInstance->c9_finalWay)[0] =
          chartInstance->c9_shootWay.x;
        c9_updateDataWrittenToVector(chartInstance, 0U);
        c9_errorIfDataNotWrittenToFcn(chartInstance, 4U, 9U, 60U, 84, 10);
        *(int8_T *)&((char_T *)chartInstance->c9_finalWay)[1] =
          chartInstance->c9_shootWay.y;
        c9_updateDataWrittenToVector(chartInstance, 0U);
        *(int16_T *)&((char_T *)chartInstance->c9_finalWay)[2] = MAX_int16_T;
        c9_updateDataWrittenToVector(chartInstance, 0U);
        _SFD_SYMBOL_SCOPE_POP();
      }

      _SFD_CS_CALL(EXIT_OUT_OF_FUNCTION_TAG, 2U, chartInstance->c9_sfEvent);
      break;

     case c9_IN_Idle:
      CV_STATE_EVAL(0, 0, 3);
      _SFD_CT_CALL(TRANSITION_BEFORE_PROCESSING_TAG, 6U,
                   chartInstance->c9_sfEvent);
      _SFD_SYMBOL_SCOPE_PUSH_EML(0U, 3U, 3U, c9_k_debug_family_names,
        c9_debug_family_var_map);
      _SFD_SYMBOL_SCOPE_ADD_EML_IMPORTABLE(&c9_h_nargin, 0U, c9_sf_marshallOut,
        c9_sf_marshallIn);
      _SFD_SYMBOL_SCOPE_ADD_EML_IMPORTABLE(&c9_h_nargout, 1U, c9_sf_marshallOut,
        c9_sf_marshallIn);
      _SFD_SYMBOL_SCOPE_ADD_EML_IMPORTABLE(&c9_d_out, 2U, c9_c_sf_marshallOut,
        c9_c_sf_marshallIn);
      c9_d_out = CV_EML_IF(6, 0, 0, chartInstance->c9_temporalCounter_i1 >= 10);
      _SFD_SYMBOL_SCOPE_POP();
      if (c9_d_out) {
        _SFD_CT_CALL(TRANSITION_ACTIVE_TAG, 6U, chartInstance->c9_sfEvent);
        chartInstance->c9_tp_Idle = 0U;
        _SFD_CS_CALL(STATE_INACTIVE_TAG, 3U, chartInstance->c9_sfEvent);
        chartInstance->c9_is_GameIsOn_Offence = c9_IN_GetToTheBall;
        _SFD_CS_CALL(STATE_ACTIVE_TAG, 2U, chartInstance->c9_sfEvent);
        chartInstance->c9_tp_GetToTheBall = 1U;
        _SFD_SYMBOL_SCOPE_PUSH_EML(0U, 2U, 2U, c9_e_debug_family_names,
          c9_b_debug_family_var_map);
        _SFD_SYMBOL_SCOPE_ADD_EML_IMPORTABLE(&c9_i_nargin, 0U, c9_sf_marshallOut,
          c9_sf_marshallIn);
        _SFD_SYMBOL_SCOPE_ADD_EML_IMPORTABLE(&c9_i_nargout, 1U,
          c9_sf_marshallOut, c9_sf_marshallIn);
        c9_calcShootWay(chartInstance);
        _SFD_SYMBOL_SCOPE_POP();
      } else {
        _SFD_CS_CALL(STATE_ENTER_DURING_FUNCTION_TAG, 3U,
                     chartInstance->c9_sfEvent);
        _SFD_SYMBOL_SCOPE_PUSH_EML(0U, 2U, 2U, c9_g_debug_family_names,
          c9_b_debug_family_var_map);
        _SFD_SYMBOL_SCOPE_ADD_EML_IMPORTABLE(&c9_j_nargin, 0U, c9_sf_marshallOut,
          c9_sf_marshallIn);
        _SFD_SYMBOL_SCOPE_ADD_EML_IMPORTABLE(&c9_j_nargout, 1U,
          c9_sf_marshallOut, c9_sf_marshallIn);
        *(int8_T *)&((char_T *)chartInstance->c9_finalWay)[0] = *(int8_T *)
          &((char_T *)chartInstance->c9_me)[0];
        c9_updateDataWrittenToVector(chartInstance, 0U);
        *(int8_T *)&((char_T *)chartInstance->c9_finalWay)[1] = *(int8_T *)
          &((char_T *)chartInstance->c9_me)[1];
        c9_updateDataWrittenToVector(chartInstance, 0U);
        *(int16_T *)&((char_T *)chartInstance->c9_finalWay)[2] = *(int16_T *)
          &((char_T *)chartInstance->c9_me)[2];
        c9_updateDataWrittenToVector(chartInstance, 0U);
        _SFD_SYMBOL_SCOPE_POP();
      }

      _SFD_CS_CALL(EXIT_OUT_OF_FUNCTION_TAG, 3U, chartInstance->c9_sfEvent);
      break;

     case c9_IN_Kick:
      CV_STATE_EVAL(0, 0, 4);
      _SFD_CT_CALL(TRANSITION_BEFORE_PROCESSING_TAG, 8U,
                   chartInstance->c9_sfEvent);
      _SFD_SYMBOL_SCOPE_PUSH_EML(0U, 3U, 3U, c9_l_debug_family_names,
        c9_debug_family_var_map);
      _SFD_SYMBOL_SCOPE_ADD_EML_IMPORTABLE(&c9_k_nargin, 0U, c9_sf_marshallOut,
        c9_sf_marshallIn);
      _SFD_SYMBOL_SCOPE_ADD_EML_IMPORTABLE(&c9_k_nargout, 1U, c9_sf_marshallOut,
        c9_sf_marshallIn);
      _SFD_SYMBOL_SCOPE_ADD_EML_IMPORTABLE(&c9_e_out, 2U, c9_c_sf_marshallOut,
        c9_c_sf_marshallIn);
      c9_e_out = CV_EML_IF(8, 0, 0, chartInstance->c9_temporalCounter_i1 >= 30);
      _SFD_SYMBOL_SCOPE_POP();
      if (c9_e_out) {
        _SFD_CT_CALL(TRANSITION_ACTIVE_TAG, 8U, chartInstance->c9_sfEvent);
        chartInstance->c9_tp_Kick = 0U;
        _SFD_CS_CALL(STATE_INACTIVE_TAG, 4U, chartInstance->c9_sfEvent);
        chartInstance->c9_is_GameIsOn_Offence = c9_IN_Idle;
        _SFD_CS_CALL(STATE_ACTIVE_TAG, 3U, chartInstance->c9_sfEvent);
        chartInstance->c9_temporalCounter_i1 = 0U;
        chartInstance->c9_tp_Idle = 1U;
      } else {
        _SFD_CS_CALL(STATE_ENTER_DURING_FUNCTION_TAG, 4U,
                     chartInstance->c9_sfEvent);
      }

      _SFD_CS_CALL(EXIT_OUT_OF_FUNCTION_TAG, 4U, chartInstance->c9_sfEvent);
      break;

     default:
      CV_STATE_EVAL(0, 0, 0);
      chartInstance->c9_is_GameIsOn_Offence = c9_IN_NO_ACTIVE_CHILD;
      _SFD_CS_CALL(STATE_INACTIVE_TAG, 1U, chartInstance->c9_sfEvent);
      break;
    }
  }

  _SFD_CS_CALL(EXIT_OUT_OF_FUNCTION_TAG, 0U, chartInstance->c9_sfEvent);
}

static void c9_exit_internal_GameIsOn_Offence(SFc9_greenOffenceInstanceStruct
  *chartInstance)
{
  switch (chartInstance->c9_is_GameIsOn_Offence) {
   case c9_IN_Aim:
    CV_STATE_EVAL(0, 1, 1);
    chartInstance->c9_tp_Aim = 0U;
    chartInstance->c9_is_GameIsOn_Offence = c9_IN_NO_ACTIVE_CHILD;
    _SFD_CS_CALL(STATE_INACTIVE_TAG, 1U, chartInstance->c9_sfEvent);
    break;

   case c9_IN_GetToTheBall:
    CV_STATE_EVAL(0, 1, 2);
    chartInstance->c9_tp_GetToTheBall = 0U;
    chartInstance->c9_is_GameIsOn_Offence = c9_IN_NO_ACTIVE_CHILD;
    _SFD_CS_CALL(STATE_INACTIVE_TAG, 2U, chartInstance->c9_sfEvent);
    break;

   case c9_IN_Idle:
    CV_STATE_EVAL(0, 1, 3);
    chartInstance->c9_tp_Idle = 0U;
    chartInstance->c9_is_GameIsOn_Offence = c9_IN_NO_ACTIVE_CHILD;
    _SFD_CS_CALL(STATE_INACTIVE_TAG, 3U, chartInstance->c9_sfEvent);
    break;

   case c9_IN_Kick:
    CV_STATE_EVAL(0, 1, 4);
    chartInstance->c9_tp_Kick = 0U;
    chartInstance->c9_is_GameIsOn_Offence = c9_IN_NO_ACTIVE_CHILD;
    _SFD_CS_CALL(STATE_INACTIVE_TAG, 4U, chartInstance->c9_sfEvent);
    break;

   default:
    CV_STATE_EVAL(0, 1, 0);
    chartInstance->c9_is_GameIsOn_Offence = c9_IN_NO_ACTIVE_CHILD;
    _SFD_CS_CALL(STATE_INACTIVE_TAG, 1U, chartInstance->c9_sfEvent);
    break;
  }
}

static real32_T c9_eml_xnrm2(SFc9_greenOffenceInstanceStruct *chartInstance,
  real32_T c9_x[2])
{
  real32_T c9_y;
  real32_T c9_scale;
  int32_T c9_k;
  int32_T c9_b_k;
  real32_T c9_b_x;
  real32_T c9_c_x;
  real32_T c9_absxk;
  real32_T c9_t;
  c9_threshold(chartInstance);
  c9_y = 0.0F;
  c9_realmin(chartInstance);
  c9_scale = 1.17549435E-38F;
  for (c9_k = 1; c9_k < 3; c9_k++) {
    c9_b_k = c9_k;
    c9_b_x = c9_x[_SFD_EML_ARRAY_BOUNDS_CHECK("", (int32_T)_SFD_INTEGER_CHECK("",
      (real_T)c9_b_k), 1, 2, 1, 0) - 1];
    c9_c_x = c9_b_x;
    c9_absxk = muSingleScalarAbs(c9_c_x);
    if (c9_absxk > c9_scale) {
      c9_t = c9_scale / c9_absxk;
      c9_y = 1.0F + c9_y * c9_t * c9_t;
      c9_scale = c9_absxk;
    } else {
      c9_t = c9_absxk / c9_scale;
      c9_y += c9_t * c9_t;
    }
  }

  return c9_scale * muSingleScalarSqrt(c9_y);
}

static void init_script_number_translation(uint32_T c9_machineNumber, uint32_T
  c9_chartNumber, uint32_T c9_instanceNumber)
{
  (void)c9_machineNumber;
  (void)c9_chartNumber;
  (void)c9_instanceNumber;
}

static const mxArray *c9_sf_marshallOut(void *chartInstanceVoid, void *c9_inData)
{
  const mxArray *c9_mxArrayOutData = NULL;
  real_T c9_u;
  const mxArray *c9_y = NULL;
  SFc9_greenOffenceInstanceStruct *chartInstance;
  chartInstance = (SFc9_greenOffenceInstanceStruct *)chartInstanceVoid;
  c9_mxArrayOutData = NULL;
  c9_u = *(real_T *)c9_inData;
  c9_y = NULL;
  sf_mex_assign(&c9_y, sf_mex_create("y", &c9_u, 0, 0U, 0U, 0U, 0), false);
  sf_mex_assign(&c9_mxArrayOutData, c9_y, false);
  return c9_mxArrayOutData;
}

static real_T c9_emlrt_marshallIn(SFc9_greenOffenceInstanceStruct *chartInstance,
  const mxArray *c9_u, const emlrtMsgIdentifier *c9_parentId)
{
  real_T c9_y;
  real_T c9_d1;
  (void)chartInstance;
  sf_mex_import(c9_parentId, sf_mex_dup(c9_u), &c9_d1, 1, 0, 0U, 0, 0U, 0);
  c9_y = c9_d1;
  sf_mex_destroy(&c9_u);
  return c9_y;
}

static void c9_sf_marshallIn(void *chartInstanceVoid, const mxArray
  *c9_mxArrayInData, const char_T *c9_varName, void *c9_outData)
{
  const mxArray *c9_nargout;
  const char_T *c9_identifier;
  emlrtMsgIdentifier c9_thisId;
  real_T c9_y;
  SFc9_greenOffenceInstanceStruct *chartInstance;
  chartInstance = (SFc9_greenOffenceInstanceStruct *)chartInstanceVoid;
  c9_nargout = sf_mex_dup(c9_mxArrayInData);
  c9_identifier = c9_varName;
  c9_thisId.fIdentifier = c9_identifier;
  c9_thisId.fParent = NULL;
  c9_y = c9_emlrt_marshallIn(chartInstance, sf_mex_dup(c9_nargout), &c9_thisId);
  sf_mex_destroy(&c9_nargout);
  *(real_T *)c9_outData = c9_y;
  sf_mex_destroy(&c9_mxArrayInData);
}

static const mxArray *c9_b_sf_marshallOut(void *chartInstanceVoid, void
  *c9_inData)
{
  const mxArray *c9_mxArrayOutData = NULL;
  int32_T c9_i13;
  int8_T c9_b_inData[2];
  int32_T c9_i14;
  int8_T c9_u[2];
  const mxArray *c9_y = NULL;
  SFc9_greenOffenceInstanceStruct *chartInstance;
  chartInstance = (SFc9_greenOffenceInstanceStruct *)chartInstanceVoid;
  c9_mxArrayOutData = NULL;
  for (c9_i13 = 0; c9_i13 < 2; c9_i13++) {
    c9_b_inData[c9_i13] = (*(int8_T (*)[2])c9_inData)[c9_i13];
  }

  for (c9_i14 = 0; c9_i14 < 2; c9_i14++) {
    c9_u[c9_i14] = c9_b_inData[c9_i14];
  }

  c9_y = NULL;
  sf_mex_assign(&c9_y, sf_mex_create("y", c9_u, 2, 0U, 1U, 0U, 1, 2), false);
  sf_mex_assign(&c9_mxArrayOutData, c9_y, false);
  return c9_mxArrayOutData;
}

static void c9_b_emlrt_marshallIn(SFc9_greenOffenceInstanceStruct *chartInstance,
  const mxArray *c9_u, const emlrtMsgIdentifier *c9_parentId, int8_T c9_y[2])
{
  int8_T c9_iv5[2];
  int32_T c9_i15;
  (void)chartInstance;
  sf_mex_import(c9_parentId, sf_mex_dup(c9_u), c9_iv5, 1, 2, 0U, 1, 0U, 1, 2);
  for (c9_i15 = 0; c9_i15 < 2; c9_i15++) {
    c9_y[c9_i15] = c9_iv5[c9_i15];
  }

  sf_mex_destroy(&c9_u);
}

static void c9_b_sf_marshallIn(void *chartInstanceVoid, const mxArray
  *c9_mxArrayInData, const char_T *c9_varName, void *c9_outData)
{
  const mxArray *c9_pos;
  const char_T *c9_identifier;
  emlrtMsgIdentifier c9_thisId;
  int8_T c9_y[2];
  int32_T c9_i16;
  SFc9_greenOffenceInstanceStruct *chartInstance;
  chartInstance = (SFc9_greenOffenceInstanceStruct *)chartInstanceVoid;
  c9_pos = sf_mex_dup(c9_mxArrayInData);
  c9_identifier = c9_varName;
  c9_thisId.fIdentifier = c9_identifier;
  c9_thisId.fParent = NULL;
  c9_b_emlrt_marshallIn(chartInstance, sf_mex_dup(c9_pos), &c9_thisId, c9_y);
  sf_mex_destroy(&c9_pos);
  for (c9_i16 = 0; c9_i16 < 2; c9_i16++) {
    (*(int8_T (*)[2])c9_outData)[c9_i16] = c9_y[c9_i16];
  }

  sf_mex_destroy(&c9_mxArrayInData);
}

static const mxArray *c9_c_sf_marshallOut(void *chartInstanceVoid, void
  *c9_inData)
{
  const mxArray *c9_mxArrayOutData = NULL;
  boolean_T c9_u;
  const mxArray *c9_y = NULL;
  SFc9_greenOffenceInstanceStruct *chartInstance;
  chartInstance = (SFc9_greenOffenceInstanceStruct *)chartInstanceVoid;
  c9_mxArrayOutData = NULL;
  c9_u = *(boolean_T *)c9_inData;
  c9_y = NULL;
  sf_mex_assign(&c9_y, sf_mex_create("y", &c9_u, 11, 0U, 0U, 0U, 0), false);
  sf_mex_assign(&c9_mxArrayOutData, c9_y, false);
  return c9_mxArrayOutData;
}

static boolean_T c9_c_emlrt_marshallIn(SFc9_greenOffenceInstanceStruct
  *chartInstance, const mxArray *c9_u, const emlrtMsgIdentifier *c9_parentId)
{
  boolean_T c9_y;
  boolean_T c9_b0;
  (void)chartInstance;
  sf_mex_import(c9_parentId, sf_mex_dup(c9_u), &c9_b0, 1, 11, 0U, 0, 0U, 0);
  c9_y = c9_b0;
  sf_mex_destroy(&c9_u);
  return c9_y;
}

static void c9_c_sf_marshallIn(void *chartInstanceVoid, const mxArray
  *c9_mxArrayInData, const char_T *c9_varName, void *c9_outData)
{
  const mxArray *c9_sf_internal_predicateOutput;
  const char_T *c9_identifier;
  emlrtMsgIdentifier c9_thisId;
  boolean_T c9_y;
  SFc9_greenOffenceInstanceStruct *chartInstance;
  chartInstance = (SFc9_greenOffenceInstanceStruct *)chartInstanceVoid;
  c9_sf_internal_predicateOutput = sf_mex_dup(c9_mxArrayInData);
  c9_identifier = c9_varName;
  c9_thisId.fIdentifier = c9_identifier;
  c9_thisId.fParent = NULL;
  c9_y = c9_c_emlrt_marshallIn(chartInstance, sf_mex_dup
    (c9_sf_internal_predicateOutput), &c9_thisId);
  sf_mex_destroy(&c9_sf_internal_predicateOutput);
  *(boolean_T *)c9_outData = c9_y;
  sf_mex_destroy(&c9_mxArrayInData);
}

static const mxArray *c9_d_sf_marshallOut(void *chartInstanceVoid, void
  *c9_inData)
{
  const mxArray *c9_mxArrayOutData = NULL;
  uint8_T c9_u;
  const mxArray *c9_y = NULL;
  SFc9_greenOffenceInstanceStruct *chartInstance;
  chartInstance = (SFc9_greenOffenceInstanceStruct *)chartInstanceVoid;
  c9_mxArrayOutData = NULL;
  c9_u = *(uint8_T *)c9_inData;
  c9_y = NULL;
  sf_mex_assign(&c9_y, sf_mex_create("y", &c9_u, 3, 0U, 0U, 0U, 0), false);
  sf_mex_assign(&c9_mxArrayOutData, c9_y, false);
  return c9_mxArrayOutData;
}

static uint8_T c9_d_emlrt_marshallIn(SFc9_greenOffenceInstanceStruct
  *chartInstance, const mxArray *c9_posReached, const char_T *c9_identifier)
{
  uint8_T c9_y;
  emlrtMsgIdentifier c9_thisId;
  c9_thisId.fIdentifier = c9_identifier;
  c9_thisId.fParent = NULL;
  c9_y = c9_e_emlrt_marshallIn(chartInstance, sf_mex_dup(c9_posReached),
    &c9_thisId);
  sf_mex_destroy(&c9_posReached);
  return c9_y;
}

static uint8_T c9_e_emlrt_marshallIn(SFc9_greenOffenceInstanceStruct
  *chartInstance, const mxArray *c9_u, const emlrtMsgIdentifier *c9_parentId)
{
  uint8_T c9_y;
  uint8_T c9_u1;
  (void)chartInstance;
  sf_mex_import(c9_parentId, sf_mex_dup(c9_u), &c9_u1, 1, 3, 0U, 0, 0U, 0);
  c9_y = c9_u1;
  sf_mex_destroy(&c9_u);
  return c9_y;
}

static void c9_d_sf_marshallIn(void *chartInstanceVoid, const mxArray
  *c9_mxArrayInData, const char_T *c9_varName, void *c9_outData)
{
  const mxArray *c9_posReached;
  const char_T *c9_identifier;
  emlrtMsgIdentifier c9_thisId;
  uint8_T c9_y;
  SFc9_greenOffenceInstanceStruct *chartInstance;
  chartInstance = (SFc9_greenOffenceInstanceStruct *)chartInstanceVoid;
  c9_posReached = sf_mex_dup(c9_mxArrayInData);
  c9_identifier = c9_varName;
  c9_thisId.fIdentifier = c9_identifier;
  c9_thisId.fParent = NULL;
  c9_y = c9_e_emlrt_marshallIn(chartInstance, sf_mex_dup(c9_posReached),
    &c9_thisId);
  sf_mex_destroy(&c9_posReached);
  *(uint8_T *)c9_outData = c9_y;
  sf_mex_destroy(&c9_mxArrayInData);
}

static const mxArray *c9_e_sf_marshallOut(void *chartInstanceVoid, void
  *c9_inData)
{
  const mxArray *c9_mxArrayOutData = NULL;
  int32_T c9_i17;
  int8_T c9_b_inData[2];
  int32_T c9_i18;
  int8_T c9_u[2];
  const mxArray *c9_y = NULL;
  SFc9_greenOffenceInstanceStruct *chartInstance;
  chartInstance = (SFc9_greenOffenceInstanceStruct *)chartInstanceVoid;
  c9_mxArrayOutData = NULL;
  for (c9_i17 = 0; c9_i17 < 2; c9_i17++) {
    c9_b_inData[c9_i17] = (*(int8_T (*)[2])c9_inData)[c9_i17];
  }

  for (c9_i18 = 0; c9_i18 < 2; c9_i18++) {
    c9_u[c9_i18] = c9_b_inData[c9_i18];
  }

  c9_y = NULL;
  sf_mex_assign(&c9_y, sf_mex_create("y", c9_u, 2, 0U, 1U, 0U, 2, 1, 2), false);
  sf_mex_assign(&c9_mxArrayOutData, c9_y, false);
  return c9_mxArrayOutData;
}

static void c9_f_emlrt_marshallIn(SFc9_greenOffenceInstanceStruct *chartInstance,
  const mxArray *c9_u, const emlrtMsgIdentifier *c9_parentId, int8_T c9_y[2])
{
  int8_T c9_iv6[2];
  int32_T c9_i19;
  (void)chartInstance;
  sf_mex_import(c9_parentId, sf_mex_dup(c9_u), c9_iv6, 1, 2, 0U, 1, 0U, 2, 1, 2);
  for (c9_i19 = 0; c9_i19 < 2; c9_i19++) {
    c9_y[c9_i19] = c9_iv6[c9_i19];
  }

  sf_mex_destroy(&c9_u);
}

static void c9_e_sf_marshallIn(void *chartInstanceVoid, const mxArray
  *c9_mxArrayInData, const char_T *c9_varName, void *c9_outData)
{
  const mxArray *c9_pos;
  const char_T *c9_identifier;
  emlrtMsgIdentifier c9_thisId;
  int8_T c9_y[2];
  int32_T c9_i20;
  SFc9_greenOffenceInstanceStruct *chartInstance;
  chartInstance = (SFc9_greenOffenceInstanceStruct *)chartInstanceVoid;
  c9_pos = sf_mex_dup(c9_mxArrayInData);
  c9_identifier = c9_varName;
  c9_thisId.fIdentifier = c9_identifier;
  c9_thisId.fParent = NULL;
  c9_f_emlrt_marshallIn(chartInstance, sf_mex_dup(c9_pos), &c9_thisId, c9_y);
  sf_mex_destroy(&c9_pos);
  for (c9_i20 = 0; c9_i20 < 2; c9_i20++) {
    (*(int8_T (*)[2])c9_outData)[c9_i20] = c9_y[c9_i20];
  }

  sf_mex_destroy(&c9_mxArrayInData);
}

const mxArray *sf_c9_greenOffence_get_eml_resolved_functions_info(void)
{
  const mxArray *c9_nameCaptureInfo = NULL;
  c9_nameCaptureInfo = NULL;
  sf_mex_assign(&c9_nameCaptureInfo, sf_mex_createstruct("structure", 2, 47, 1),
                false);
  c9_info_helper(&c9_nameCaptureInfo);
  sf_mex_emlrtNameCapturePostProcessR2012a(&c9_nameCaptureInfo);
  return c9_nameCaptureInfo;
}

static void c9_info_helper(const mxArray **c9_info)
{
  const mxArray *c9_rhs0 = NULL;
  const mxArray *c9_lhs0 = NULL;
  const mxArray *c9_rhs1 = NULL;
  const mxArray *c9_lhs1 = NULL;
  const mxArray *c9_rhs2 = NULL;
  const mxArray *c9_lhs2 = NULL;
  const mxArray *c9_rhs3 = NULL;
  const mxArray *c9_lhs3 = NULL;
  const mxArray *c9_rhs4 = NULL;
  const mxArray *c9_lhs4 = NULL;
  const mxArray *c9_rhs5 = NULL;
  const mxArray *c9_lhs5 = NULL;
  const mxArray *c9_rhs6 = NULL;
  const mxArray *c9_lhs6 = NULL;
  const mxArray *c9_rhs7 = NULL;
  const mxArray *c9_lhs7 = NULL;
  const mxArray *c9_rhs8 = NULL;
  const mxArray *c9_lhs8 = NULL;
  const mxArray *c9_rhs9 = NULL;
  const mxArray *c9_lhs9 = NULL;
  const mxArray *c9_rhs10 = NULL;
  const mxArray *c9_lhs10 = NULL;
  const mxArray *c9_rhs11 = NULL;
  const mxArray *c9_lhs11 = NULL;
  const mxArray *c9_rhs12 = NULL;
  const mxArray *c9_lhs12 = NULL;
  const mxArray *c9_rhs13 = NULL;
  const mxArray *c9_lhs13 = NULL;
  const mxArray *c9_rhs14 = NULL;
  const mxArray *c9_lhs14 = NULL;
  const mxArray *c9_rhs15 = NULL;
  const mxArray *c9_lhs15 = NULL;
  const mxArray *c9_rhs16 = NULL;
  const mxArray *c9_lhs16 = NULL;
  const mxArray *c9_rhs17 = NULL;
  const mxArray *c9_lhs17 = NULL;
  const mxArray *c9_rhs18 = NULL;
  const mxArray *c9_lhs18 = NULL;
  const mxArray *c9_rhs19 = NULL;
  const mxArray *c9_lhs19 = NULL;
  const mxArray *c9_rhs20 = NULL;
  const mxArray *c9_lhs20 = NULL;
  const mxArray *c9_rhs21 = NULL;
  const mxArray *c9_lhs21 = NULL;
  const mxArray *c9_rhs22 = NULL;
  const mxArray *c9_lhs22 = NULL;
  const mxArray *c9_rhs23 = NULL;
  const mxArray *c9_lhs23 = NULL;
  const mxArray *c9_rhs24 = NULL;
  const mxArray *c9_lhs24 = NULL;
  const mxArray *c9_rhs25 = NULL;
  const mxArray *c9_lhs25 = NULL;
  const mxArray *c9_rhs26 = NULL;
  const mxArray *c9_lhs26 = NULL;
  const mxArray *c9_rhs27 = NULL;
  const mxArray *c9_lhs27 = NULL;
  const mxArray *c9_rhs28 = NULL;
  const mxArray *c9_lhs28 = NULL;
  const mxArray *c9_rhs29 = NULL;
  const mxArray *c9_lhs29 = NULL;
  const mxArray *c9_rhs30 = NULL;
  const mxArray *c9_lhs30 = NULL;
  const mxArray *c9_rhs31 = NULL;
  const mxArray *c9_lhs31 = NULL;
  const mxArray *c9_rhs32 = NULL;
  const mxArray *c9_lhs32 = NULL;
  const mxArray *c9_rhs33 = NULL;
  const mxArray *c9_lhs33 = NULL;
  const mxArray *c9_rhs34 = NULL;
  const mxArray *c9_lhs34 = NULL;
  const mxArray *c9_rhs35 = NULL;
  const mxArray *c9_lhs35 = NULL;
  const mxArray *c9_rhs36 = NULL;
  const mxArray *c9_lhs36 = NULL;
  const mxArray *c9_rhs37 = NULL;
  const mxArray *c9_lhs37 = NULL;
  const mxArray *c9_rhs38 = NULL;
  const mxArray *c9_lhs38 = NULL;
  const mxArray *c9_rhs39 = NULL;
  const mxArray *c9_lhs39 = NULL;
  const mxArray *c9_rhs40 = NULL;
  const mxArray *c9_lhs40 = NULL;
  const mxArray *c9_rhs41 = NULL;
  const mxArray *c9_lhs41 = NULL;
  const mxArray *c9_rhs42 = NULL;
  const mxArray *c9_lhs42 = NULL;
  const mxArray *c9_rhs43 = NULL;
  const mxArray *c9_lhs43 = NULL;
  const mxArray *c9_rhs44 = NULL;
  const mxArray *c9_lhs44 = NULL;
  const mxArray *c9_rhs45 = NULL;
  const mxArray *c9_lhs45 = NULL;
  const mxArray *c9_rhs46 = NULL;
  const mxArray *c9_lhs46 = NULL;
  sf_mex_addfield(*c9_info, c9_emlrt_marshallOut(""), "context", "context", 0);
  sf_mex_addfield(*c9_info, c9_emlrt_marshallOut("eml_mtimes_helper"), "name",
                  "name", 0);
  sf_mex_addfield(*c9_info, c9_emlrt_marshallOut(""), "dominantType",
                  "dominantType", 0);
  sf_mex_addfield(*c9_info, c9_emlrt_marshallOut(
    "[ILXE]$matlabroot$/toolbox/eml/lib/matlab/ops/eml_mtimes_helper.m"),
                  "resolved", "resolved", 0);
  sf_mex_addfield(*c9_info, c9_b_emlrt_marshallOut(1383880894U), "fileTimeLo",
                  "fileTimeLo", 0);
  sf_mex_addfield(*c9_info, c9_b_emlrt_marshallOut(0U), "fileTimeHi",
                  "fileTimeHi", 0);
  sf_mex_addfield(*c9_info, c9_b_emlrt_marshallOut(0U), "mFileTimeLo",
                  "mFileTimeLo", 0);
  sf_mex_addfield(*c9_info, c9_b_emlrt_marshallOut(0U), "mFileTimeHi",
                  "mFileTimeHi", 0);
  sf_mex_assign(&c9_rhs0, sf_mex_createcellmatrix(0, 1), false);
  sf_mex_assign(&c9_lhs0, sf_mex_createcellmatrix(0, 1), false);
  sf_mex_addfield(*c9_info, sf_mex_duplicatearraysafe(&c9_rhs0), "rhs", "rhs", 0);
  sf_mex_addfield(*c9_info, sf_mex_duplicatearraysafe(&c9_lhs0), "lhs", "lhs", 0);
  sf_mex_addfield(*c9_info, c9_emlrt_marshallOut(
    "[ILXE]$matlabroot$/toolbox/eml/lib/matlab/ops/eml_mtimes_helper.m!common_checks"),
                  "context", "context", 1);
  sf_mex_addfield(*c9_info, c9_emlrt_marshallOut(
    "coder.internal.isBuiltInNumeric"), "name", "name", 1);
  sf_mex_addfield(*c9_info, c9_emlrt_marshallOut("double"), "dominantType",
                  "dominantType", 1);
  sf_mex_addfield(*c9_info, c9_emlrt_marshallOut(
    "[ILXE]$matlabroot$/toolbox/shared/coder/coder/+coder/+internal/isBuiltInNumeric.m"),
                  "resolved", "resolved", 1);
  sf_mex_addfield(*c9_info, c9_b_emlrt_marshallOut(1395935456U), "fileTimeLo",
                  "fileTimeLo", 1);
  sf_mex_addfield(*c9_info, c9_b_emlrt_marshallOut(0U), "fileTimeHi",
                  "fileTimeHi", 1);
  sf_mex_addfield(*c9_info, c9_b_emlrt_marshallOut(0U), "mFileTimeLo",
                  "mFileTimeLo", 1);
  sf_mex_addfield(*c9_info, c9_b_emlrt_marshallOut(0U), "mFileTimeHi",
                  "mFileTimeHi", 1);
  sf_mex_assign(&c9_rhs1, sf_mex_createcellmatrix(0, 1), false);
  sf_mex_assign(&c9_lhs1, sf_mex_createcellmatrix(0, 1), false);
  sf_mex_addfield(*c9_info, sf_mex_duplicatearraysafe(&c9_rhs1), "rhs", "rhs", 1);
  sf_mex_addfield(*c9_info, sf_mex_duplicatearraysafe(&c9_lhs1), "lhs", "lhs", 1);
  sf_mex_addfield(*c9_info, c9_emlrt_marshallOut(
    "[ILXE]$matlabroot$/toolbox/eml/lib/matlab/ops/eml_mtimes_helper.m!common_checks"),
                  "context", "context", 2);
  sf_mex_addfield(*c9_info, c9_emlrt_marshallOut(
    "coder.internal.isBuiltInNumeric"), "name", "name", 2);
  sf_mex_addfield(*c9_info, c9_emlrt_marshallOut("single"), "dominantType",
                  "dominantType", 2);
  sf_mex_addfield(*c9_info, c9_emlrt_marshallOut(
    "[ILXE]$matlabroot$/toolbox/shared/coder/coder/+coder/+internal/isBuiltInNumeric.m"),
                  "resolved", "resolved", 2);
  sf_mex_addfield(*c9_info, c9_b_emlrt_marshallOut(1395935456U), "fileTimeLo",
                  "fileTimeLo", 2);
  sf_mex_addfield(*c9_info, c9_b_emlrt_marshallOut(0U), "fileTimeHi",
                  "fileTimeHi", 2);
  sf_mex_addfield(*c9_info, c9_b_emlrt_marshallOut(0U), "mFileTimeLo",
                  "mFileTimeLo", 2);
  sf_mex_addfield(*c9_info, c9_b_emlrt_marshallOut(0U), "mFileTimeHi",
                  "mFileTimeHi", 2);
  sf_mex_assign(&c9_rhs2, sf_mex_createcellmatrix(0, 1), false);
  sf_mex_assign(&c9_lhs2, sf_mex_createcellmatrix(0, 1), false);
  sf_mex_addfield(*c9_info, sf_mex_duplicatearraysafe(&c9_rhs2), "rhs", "rhs", 2);
  sf_mex_addfield(*c9_info, sf_mex_duplicatearraysafe(&c9_lhs2), "lhs", "lhs", 2);
  sf_mex_addfield(*c9_info, c9_emlrt_marshallOut(""), "context", "context", 3);
  sf_mex_addfield(*c9_info, c9_emlrt_marshallOut("norm"), "name", "name", 3);
  sf_mex_addfield(*c9_info, c9_emlrt_marshallOut("single"), "dominantType",
                  "dominantType", 3);
  sf_mex_addfield(*c9_info, c9_emlrt_marshallOut(
    "[ILXE]$matlabroot$/toolbox/eml/lib/matlab/matfun/norm.m"), "resolved",
                  "resolved", 3);
  sf_mex_addfield(*c9_info, c9_b_emlrt_marshallOut(1363717468U), "fileTimeLo",
                  "fileTimeLo", 3);
  sf_mex_addfield(*c9_info, c9_b_emlrt_marshallOut(0U), "fileTimeHi",
                  "fileTimeHi", 3);
  sf_mex_addfield(*c9_info, c9_b_emlrt_marshallOut(0U), "mFileTimeLo",
                  "mFileTimeLo", 3);
  sf_mex_addfield(*c9_info, c9_b_emlrt_marshallOut(0U), "mFileTimeHi",
                  "mFileTimeHi", 3);
  sf_mex_assign(&c9_rhs3, sf_mex_createcellmatrix(0, 1), false);
  sf_mex_assign(&c9_lhs3, sf_mex_createcellmatrix(0, 1), false);
  sf_mex_addfield(*c9_info, sf_mex_duplicatearraysafe(&c9_rhs3), "rhs", "rhs", 3);
  sf_mex_addfield(*c9_info, sf_mex_duplicatearraysafe(&c9_lhs3), "lhs", "lhs", 3);
  sf_mex_addfield(*c9_info, c9_emlrt_marshallOut(
    "[ILXE]$matlabroot$/toolbox/eml/lib/matlab/matfun/norm.m!genpnorm"),
                  "context", "context", 4);
  sf_mex_addfield(*c9_info, c9_emlrt_marshallOut("eml_index_class"), "name",
                  "name", 4);
  sf_mex_addfield(*c9_info, c9_emlrt_marshallOut(""), "dominantType",
                  "dominantType", 4);
  sf_mex_addfield(*c9_info, c9_emlrt_marshallOut(
    "[ILXE]$matlabroot$/toolbox/eml/lib/matlab/eml/eml_index_class.m"),
                  "resolved", "resolved", 4);
  sf_mex_addfield(*c9_info, c9_b_emlrt_marshallOut(1323174178U), "fileTimeLo",
                  "fileTimeLo", 4);
  sf_mex_addfield(*c9_info, c9_b_emlrt_marshallOut(0U), "fileTimeHi",
                  "fileTimeHi", 4);
  sf_mex_addfield(*c9_info, c9_b_emlrt_marshallOut(0U), "mFileTimeLo",
                  "mFileTimeLo", 4);
  sf_mex_addfield(*c9_info, c9_b_emlrt_marshallOut(0U), "mFileTimeHi",
                  "mFileTimeHi", 4);
  sf_mex_assign(&c9_rhs4, sf_mex_createcellmatrix(0, 1), false);
  sf_mex_assign(&c9_lhs4, sf_mex_createcellmatrix(0, 1), false);
  sf_mex_addfield(*c9_info, sf_mex_duplicatearraysafe(&c9_rhs4), "rhs", "rhs", 4);
  sf_mex_addfield(*c9_info, sf_mex_duplicatearraysafe(&c9_lhs4), "lhs", "lhs", 4);
  sf_mex_addfield(*c9_info, c9_emlrt_marshallOut(
    "[ILXE]$matlabroot$/toolbox/eml/lib/matlab/matfun/norm.m!genpnorm"),
                  "context", "context", 5);
  sf_mex_addfield(*c9_info, c9_emlrt_marshallOut(
    "coder.internal.isBuiltInNumeric"), "name", "name", 5);
  sf_mex_addfield(*c9_info, c9_emlrt_marshallOut("double"), "dominantType",
                  "dominantType", 5);
  sf_mex_addfield(*c9_info, c9_emlrt_marshallOut(
    "[ILXE]$matlabroot$/toolbox/shared/coder/coder/+coder/+internal/isBuiltInNumeric.m"),
                  "resolved", "resolved", 5);
  sf_mex_addfield(*c9_info, c9_b_emlrt_marshallOut(1395935456U), "fileTimeLo",
                  "fileTimeLo", 5);
  sf_mex_addfield(*c9_info, c9_b_emlrt_marshallOut(0U), "fileTimeHi",
                  "fileTimeHi", 5);
  sf_mex_addfield(*c9_info, c9_b_emlrt_marshallOut(0U), "mFileTimeLo",
                  "mFileTimeLo", 5);
  sf_mex_addfield(*c9_info, c9_b_emlrt_marshallOut(0U), "mFileTimeHi",
                  "mFileTimeHi", 5);
  sf_mex_assign(&c9_rhs5, sf_mex_createcellmatrix(0, 1), false);
  sf_mex_assign(&c9_lhs5, sf_mex_createcellmatrix(0, 1), false);
  sf_mex_addfield(*c9_info, sf_mex_duplicatearraysafe(&c9_rhs5), "rhs", "rhs", 5);
  sf_mex_addfield(*c9_info, sf_mex_duplicatearraysafe(&c9_lhs5), "lhs", "lhs", 5);
  sf_mex_addfield(*c9_info, c9_emlrt_marshallOut(
    "[ILXE]$matlabroot$/toolbox/eml/lib/matlab/matfun/norm.m!genpnorm"),
                  "context", "context", 6);
  sf_mex_addfield(*c9_info, c9_emlrt_marshallOut("eml_xnrm2"), "name", "name", 6);
  sf_mex_addfield(*c9_info, c9_emlrt_marshallOut("single"), "dominantType",
                  "dominantType", 6);
  sf_mex_addfield(*c9_info, c9_emlrt_marshallOut(
    "[ILXE]$matlabroot$/toolbox/eml/lib/matlab/eml/blas/eml_xnrm2.m"),
                  "resolved", "resolved", 6);
  sf_mex_addfield(*c9_info, c9_b_emlrt_marshallOut(1375984292U), "fileTimeLo",
                  "fileTimeLo", 6);
  sf_mex_addfield(*c9_info, c9_b_emlrt_marshallOut(0U), "fileTimeHi",
                  "fileTimeHi", 6);
  sf_mex_addfield(*c9_info, c9_b_emlrt_marshallOut(0U), "mFileTimeLo",
                  "mFileTimeLo", 6);
  sf_mex_addfield(*c9_info, c9_b_emlrt_marshallOut(0U), "mFileTimeHi",
                  "mFileTimeHi", 6);
  sf_mex_assign(&c9_rhs6, sf_mex_createcellmatrix(0, 1), false);
  sf_mex_assign(&c9_lhs6, sf_mex_createcellmatrix(0, 1), false);
  sf_mex_addfield(*c9_info, sf_mex_duplicatearraysafe(&c9_rhs6), "rhs", "rhs", 6);
  sf_mex_addfield(*c9_info, sf_mex_duplicatearraysafe(&c9_lhs6), "lhs", "lhs", 6);
  sf_mex_addfield(*c9_info, c9_emlrt_marshallOut(
    "[ILXE]$matlabroot$/toolbox/eml/lib/matlab/eml/blas/eml_xnrm2.m"), "context",
                  "context", 7);
  sf_mex_addfield(*c9_info, c9_emlrt_marshallOut("coder.internal.blas.inline"),
                  "name", "name", 7);
  sf_mex_addfield(*c9_info, c9_emlrt_marshallOut(""), "dominantType",
                  "dominantType", 7);
  sf_mex_addfield(*c9_info, c9_emlrt_marshallOut(
    "[ILXE]$matlabroot$/toolbox/coder/coder/+coder/+internal/+blas/inline.p"),
                  "resolved", "resolved", 7);
  sf_mex_addfield(*c9_info, c9_b_emlrt_marshallOut(1410811372U), "fileTimeLo",
                  "fileTimeLo", 7);
  sf_mex_addfield(*c9_info, c9_b_emlrt_marshallOut(0U), "fileTimeHi",
                  "fileTimeHi", 7);
  sf_mex_addfield(*c9_info, c9_b_emlrt_marshallOut(0U), "mFileTimeLo",
                  "mFileTimeLo", 7);
  sf_mex_addfield(*c9_info, c9_b_emlrt_marshallOut(0U), "mFileTimeHi",
                  "mFileTimeHi", 7);
  sf_mex_assign(&c9_rhs7, sf_mex_createcellmatrix(0, 1), false);
  sf_mex_assign(&c9_lhs7, sf_mex_createcellmatrix(0, 1), false);
  sf_mex_addfield(*c9_info, sf_mex_duplicatearraysafe(&c9_rhs7), "rhs", "rhs", 7);
  sf_mex_addfield(*c9_info, sf_mex_duplicatearraysafe(&c9_lhs7), "lhs", "lhs", 7);
  sf_mex_addfield(*c9_info, c9_emlrt_marshallOut(
    "[ILXE]$matlabroot$/toolbox/eml/lib/matlab/eml/blas/eml_xnrm2.m"), "context",
                  "context", 8);
  sf_mex_addfield(*c9_info, c9_emlrt_marshallOut("coder.internal.blas.xnrm2"),
                  "name", "name", 8);
  sf_mex_addfield(*c9_info, c9_emlrt_marshallOut("single"), "dominantType",
                  "dominantType", 8);
  sf_mex_addfield(*c9_info, c9_emlrt_marshallOut(
    "[ILXE]$matlabroot$/toolbox/coder/coder/+coder/+internal/+blas/xnrm2.p"),
                  "resolved", "resolved", 8);
  sf_mex_addfield(*c9_info, c9_b_emlrt_marshallOut(1410811370U), "fileTimeLo",
                  "fileTimeLo", 8);
  sf_mex_addfield(*c9_info, c9_b_emlrt_marshallOut(0U), "fileTimeHi",
                  "fileTimeHi", 8);
  sf_mex_addfield(*c9_info, c9_b_emlrt_marshallOut(0U), "mFileTimeLo",
                  "mFileTimeLo", 8);
  sf_mex_addfield(*c9_info, c9_b_emlrt_marshallOut(0U), "mFileTimeHi",
                  "mFileTimeHi", 8);
  sf_mex_assign(&c9_rhs8, sf_mex_createcellmatrix(0, 1), false);
  sf_mex_assign(&c9_lhs8, sf_mex_createcellmatrix(0, 1), false);
  sf_mex_addfield(*c9_info, sf_mex_duplicatearraysafe(&c9_rhs8), "rhs", "rhs", 8);
  sf_mex_addfield(*c9_info, sf_mex_duplicatearraysafe(&c9_lhs8), "lhs", "lhs", 8);
  sf_mex_addfield(*c9_info, c9_emlrt_marshallOut(
    "[ILXE]$matlabroot$/toolbox/coder/coder/+coder/+internal/+blas/xnrm2.p"),
                  "context", "context", 9);
  sf_mex_addfield(*c9_info, c9_emlrt_marshallOut(
    "coder.internal.blas.use_refblas"), "name", "name", 9);
  sf_mex_addfield(*c9_info, c9_emlrt_marshallOut(""), "dominantType",
                  "dominantType", 9);
  sf_mex_addfield(*c9_info, c9_emlrt_marshallOut(
    "[ILXE]$matlabroot$/toolbox/coder/coder/+coder/+internal/+blas/use_refblas.p"),
                  "resolved", "resolved", 9);
  sf_mex_addfield(*c9_info, c9_b_emlrt_marshallOut(1410811370U), "fileTimeLo",
                  "fileTimeLo", 9);
  sf_mex_addfield(*c9_info, c9_b_emlrt_marshallOut(0U), "fileTimeHi",
                  "fileTimeHi", 9);
  sf_mex_addfield(*c9_info, c9_b_emlrt_marshallOut(0U), "mFileTimeLo",
                  "mFileTimeLo", 9);
  sf_mex_addfield(*c9_info, c9_b_emlrt_marshallOut(0U), "mFileTimeHi",
                  "mFileTimeHi", 9);
  sf_mex_assign(&c9_rhs9, sf_mex_createcellmatrix(0, 1), false);
  sf_mex_assign(&c9_lhs9, sf_mex_createcellmatrix(0, 1), false);
  sf_mex_addfield(*c9_info, sf_mex_duplicatearraysafe(&c9_rhs9), "rhs", "rhs", 9);
  sf_mex_addfield(*c9_info, sf_mex_duplicatearraysafe(&c9_lhs9), "lhs", "lhs", 9);
  sf_mex_addfield(*c9_info, c9_emlrt_marshallOut(
    "[ILXE]$matlabroot$/toolbox/coder/coder/+coder/+internal/+blas/xnrm2.p!below_threshold"),
                  "context", "context", 10);
  sf_mex_addfield(*c9_info, c9_emlrt_marshallOut("coder.internal.blas.threshold"),
                  "name", "name", 10);
  sf_mex_addfield(*c9_info, c9_emlrt_marshallOut("char"), "dominantType",
                  "dominantType", 10);
  sf_mex_addfield(*c9_info, c9_emlrt_marshallOut(
    "[ILXE]$matlabroot$/toolbox/coder/coder/+coder/+internal/+blas/threshold.p"),
                  "resolved", "resolved", 10);
  sf_mex_addfield(*c9_info, c9_b_emlrt_marshallOut(1410811372U), "fileTimeLo",
                  "fileTimeLo", 10);
  sf_mex_addfield(*c9_info, c9_b_emlrt_marshallOut(0U), "fileTimeHi",
                  "fileTimeHi", 10);
  sf_mex_addfield(*c9_info, c9_b_emlrt_marshallOut(0U), "mFileTimeLo",
                  "mFileTimeLo", 10);
  sf_mex_addfield(*c9_info, c9_b_emlrt_marshallOut(0U), "mFileTimeHi",
                  "mFileTimeHi", 10);
  sf_mex_assign(&c9_rhs10, sf_mex_createcellmatrix(0, 1), false);
  sf_mex_assign(&c9_lhs10, sf_mex_createcellmatrix(0, 1), false);
  sf_mex_addfield(*c9_info, sf_mex_duplicatearraysafe(&c9_rhs10), "rhs", "rhs",
                  10);
  sf_mex_addfield(*c9_info, sf_mex_duplicatearraysafe(&c9_lhs10), "lhs", "lhs",
                  10);
  sf_mex_addfield(*c9_info, c9_emlrt_marshallOut(
    "[ILXE]$matlabroot$/toolbox/coder/coder/+coder/+internal/+blas/threshold.p"),
                  "context", "context", 11);
  sf_mex_addfield(*c9_info, c9_emlrt_marshallOut("eml_switch_helper"), "name",
                  "name", 11);
  sf_mex_addfield(*c9_info, c9_emlrt_marshallOut(""), "dominantType",
                  "dominantType", 11);
  sf_mex_addfield(*c9_info, c9_emlrt_marshallOut(
    "[ILXE]$matlabroot$/toolbox/eml/lib/matlab/eml/eml_switch_helper.m"),
                  "resolved", "resolved", 11);
  sf_mex_addfield(*c9_info, c9_b_emlrt_marshallOut(1393334458U), "fileTimeLo",
                  "fileTimeLo", 11);
  sf_mex_addfield(*c9_info, c9_b_emlrt_marshallOut(0U), "fileTimeHi",
                  "fileTimeHi", 11);
  sf_mex_addfield(*c9_info, c9_b_emlrt_marshallOut(0U), "mFileTimeLo",
                  "mFileTimeLo", 11);
  sf_mex_addfield(*c9_info, c9_b_emlrt_marshallOut(0U), "mFileTimeHi",
                  "mFileTimeHi", 11);
  sf_mex_assign(&c9_rhs11, sf_mex_createcellmatrix(0, 1), false);
  sf_mex_assign(&c9_lhs11, sf_mex_createcellmatrix(0, 1), false);
  sf_mex_addfield(*c9_info, sf_mex_duplicatearraysafe(&c9_rhs11), "rhs", "rhs",
                  11);
  sf_mex_addfield(*c9_info, sf_mex_duplicatearraysafe(&c9_lhs11), "lhs", "lhs",
                  11);
  sf_mex_addfield(*c9_info, c9_emlrt_marshallOut(
    "[ILXE]$matlabroot$/toolbox/coder/coder/+coder/+internal/+blas/xnrm2.p"),
                  "context", "context", 12);
  sf_mex_addfield(*c9_info, c9_emlrt_marshallOut("coder.internal.refblas.xnrm2"),
                  "name", "name", 12);
  sf_mex_addfield(*c9_info, c9_emlrt_marshallOut("single"), "dominantType",
                  "dominantType", 12);
  sf_mex_addfield(*c9_info, c9_emlrt_marshallOut(
    "[ILXE]$matlabroot$/toolbox/coder/coder/+coder/+internal/+refblas/xnrm2.p"),
                  "resolved", "resolved", 12);
  sf_mex_addfield(*c9_info, c9_b_emlrt_marshallOut(1410811372U), "fileTimeLo",
                  "fileTimeLo", 12);
  sf_mex_addfield(*c9_info, c9_b_emlrt_marshallOut(0U), "fileTimeHi",
                  "fileTimeHi", 12);
  sf_mex_addfield(*c9_info, c9_b_emlrt_marshallOut(0U), "mFileTimeLo",
                  "mFileTimeLo", 12);
  sf_mex_addfield(*c9_info, c9_b_emlrt_marshallOut(0U), "mFileTimeHi",
                  "mFileTimeHi", 12);
  sf_mex_assign(&c9_rhs12, sf_mex_createcellmatrix(0, 1), false);
  sf_mex_assign(&c9_lhs12, sf_mex_createcellmatrix(0, 1), false);
  sf_mex_addfield(*c9_info, sf_mex_duplicatearraysafe(&c9_rhs12), "rhs", "rhs",
                  12);
  sf_mex_addfield(*c9_info, sf_mex_duplicatearraysafe(&c9_lhs12), "lhs", "lhs",
                  12);
  sf_mex_addfield(*c9_info, c9_emlrt_marshallOut(
    "[ILXE]$matlabroot$/toolbox/coder/coder/+coder/+internal/+refblas/xnrm2.p"),
                  "context", "context", 13);
  sf_mex_addfield(*c9_info, c9_emlrt_marshallOut("realmin"), "name", "name", 13);
  sf_mex_addfield(*c9_info, c9_emlrt_marshallOut("char"), "dominantType",
                  "dominantType", 13);
  sf_mex_addfield(*c9_info, c9_emlrt_marshallOut(
    "[ILXE]$matlabroot$/toolbox/eml/lib/matlab/elmat/realmin.m"), "resolved",
                  "resolved", 13);
  sf_mex_addfield(*c9_info, c9_b_emlrt_marshallOut(1307654842U), "fileTimeLo",
                  "fileTimeLo", 13);
  sf_mex_addfield(*c9_info, c9_b_emlrt_marshallOut(0U), "fileTimeHi",
                  "fileTimeHi", 13);
  sf_mex_addfield(*c9_info, c9_b_emlrt_marshallOut(0U), "mFileTimeLo",
                  "mFileTimeLo", 13);
  sf_mex_addfield(*c9_info, c9_b_emlrt_marshallOut(0U), "mFileTimeHi",
                  "mFileTimeHi", 13);
  sf_mex_assign(&c9_rhs13, sf_mex_createcellmatrix(0, 1), false);
  sf_mex_assign(&c9_lhs13, sf_mex_createcellmatrix(0, 1), false);
  sf_mex_addfield(*c9_info, sf_mex_duplicatearraysafe(&c9_rhs13), "rhs", "rhs",
                  13);
  sf_mex_addfield(*c9_info, sf_mex_duplicatearraysafe(&c9_lhs13), "lhs", "lhs",
                  13);
  sf_mex_addfield(*c9_info, c9_emlrt_marshallOut(
    "[ILXE]$matlabroot$/toolbox/eml/lib/matlab/elmat/realmin.m"), "context",
                  "context", 14);
  sf_mex_addfield(*c9_info, c9_emlrt_marshallOut("eml_realmin"), "name", "name",
                  14);
  sf_mex_addfield(*c9_info, c9_emlrt_marshallOut("char"), "dominantType",
                  "dominantType", 14);
  sf_mex_addfield(*c9_info, c9_emlrt_marshallOut(
    "[ILXE]$matlabroot$/toolbox/eml/lib/matlab/eml/eml_realmin.m"), "resolved",
                  "resolved", 14);
  sf_mex_addfield(*c9_info, c9_b_emlrt_marshallOut(1307654844U), "fileTimeLo",
                  "fileTimeLo", 14);
  sf_mex_addfield(*c9_info, c9_b_emlrt_marshallOut(0U), "fileTimeHi",
                  "fileTimeHi", 14);
  sf_mex_addfield(*c9_info, c9_b_emlrt_marshallOut(0U), "mFileTimeLo",
                  "mFileTimeLo", 14);
  sf_mex_addfield(*c9_info, c9_b_emlrt_marshallOut(0U), "mFileTimeHi",
                  "mFileTimeHi", 14);
  sf_mex_assign(&c9_rhs14, sf_mex_createcellmatrix(0, 1), false);
  sf_mex_assign(&c9_lhs14, sf_mex_createcellmatrix(0, 1), false);
  sf_mex_addfield(*c9_info, sf_mex_duplicatearraysafe(&c9_rhs14), "rhs", "rhs",
                  14);
  sf_mex_addfield(*c9_info, sf_mex_duplicatearraysafe(&c9_lhs14), "lhs", "lhs",
                  14);
  sf_mex_addfield(*c9_info, c9_emlrt_marshallOut(
    "[ILXE]$matlabroot$/toolbox/eml/lib/matlab/eml/eml_realmin.m"), "context",
                  "context", 15);
  sf_mex_addfield(*c9_info, c9_emlrt_marshallOut("eml_float_model"), "name",
                  "name", 15);
  sf_mex_addfield(*c9_info, c9_emlrt_marshallOut("char"), "dominantType",
                  "dominantType", 15);
  sf_mex_addfield(*c9_info, c9_emlrt_marshallOut(
    "[ILXE]$matlabroot$/toolbox/eml/lib/matlab/eml/eml_float_model.m"),
                  "resolved", "resolved", 15);
  sf_mex_addfield(*c9_info, c9_b_emlrt_marshallOut(1326731596U), "fileTimeLo",
                  "fileTimeLo", 15);
  sf_mex_addfield(*c9_info, c9_b_emlrt_marshallOut(0U), "fileTimeHi",
                  "fileTimeHi", 15);
  sf_mex_addfield(*c9_info, c9_b_emlrt_marshallOut(0U), "mFileTimeLo",
                  "mFileTimeLo", 15);
  sf_mex_addfield(*c9_info, c9_b_emlrt_marshallOut(0U), "mFileTimeHi",
                  "mFileTimeHi", 15);
  sf_mex_assign(&c9_rhs15, sf_mex_createcellmatrix(0, 1), false);
  sf_mex_assign(&c9_lhs15, sf_mex_createcellmatrix(0, 1), false);
  sf_mex_addfield(*c9_info, sf_mex_duplicatearraysafe(&c9_rhs15), "rhs", "rhs",
                  15);
  sf_mex_addfield(*c9_info, sf_mex_duplicatearraysafe(&c9_lhs15), "lhs", "lhs",
                  15);
  sf_mex_addfield(*c9_info, c9_emlrt_marshallOut(
    "[ILXE]$matlabroot$/toolbox/coder/coder/+coder/+internal/+refblas/xnrm2.p"),
                  "context", "context", 16);
  sf_mex_addfield(*c9_info, c9_emlrt_marshallOut("coder.internal.indexMinus"),
                  "name", "name", 16);
  sf_mex_addfield(*c9_info, c9_emlrt_marshallOut("double"), "dominantType",
                  "dominantType", 16);
  sf_mex_addfield(*c9_info, c9_emlrt_marshallOut(
    "[ILXE]$matlabroot$/toolbox/shared/coder/coder/+coder/+internal/indexMinus.m"),
                  "resolved", "resolved", 16);
  sf_mex_addfield(*c9_info, c9_b_emlrt_marshallOut(1372586760U), "fileTimeLo",
                  "fileTimeLo", 16);
  sf_mex_addfield(*c9_info, c9_b_emlrt_marshallOut(0U), "fileTimeHi",
                  "fileTimeHi", 16);
  sf_mex_addfield(*c9_info, c9_b_emlrt_marshallOut(0U), "mFileTimeLo",
                  "mFileTimeLo", 16);
  sf_mex_addfield(*c9_info, c9_b_emlrt_marshallOut(0U), "mFileTimeHi",
                  "mFileTimeHi", 16);
  sf_mex_assign(&c9_rhs16, sf_mex_createcellmatrix(0, 1), false);
  sf_mex_assign(&c9_lhs16, sf_mex_createcellmatrix(0, 1), false);
  sf_mex_addfield(*c9_info, sf_mex_duplicatearraysafe(&c9_rhs16), "rhs", "rhs",
                  16);
  sf_mex_addfield(*c9_info, sf_mex_duplicatearraysafe(&c9_lhs16), "lhs", "lhs",
                  16);
  sf_mex_addfield(*c9_info, c9_emlrt_marshallOut(
    "[ILXE]$matlabroot$/toolbox/coder/coder/+coder/+internal/+refblas/xnrm2.p"),
                  "context", "context", 17);
  sf_mex_addfield(*c9_info, c9_emlrt_marshallOut("coder.internal.indexTimes"),
                  "name", "name", 17);
  sf_mex_addfield(*c9_info, c9_emlrt_marshallOut("coder.internal.indexInt"),
                  "dominantType", "dominantType", 17);
  sf_mex_addfield(*c9_info, c9_emlrt_marshallOut(
    "[ILXE]$matlabroot$/toolbox/shared/coder/coder/+coder/+internal/indexTimes.m"),
                  "resolved", "resolved", 17);
  sf_mex_addfield(*c9_info, c9_b_emlrt_marshallOut(1372586760U), "fileTimeLo",
                  "fileTimeLo", 17);
  sf_mex_addfield(*c9_info, c9_b_emlrt_marshallOut(0U), "fileTimeHi",
                  "fileTimeHi", 17);
  sf_mex_addfield(*c9_info, c9_b_emlrt_marshallOut(0U), "mFileTimeLo",
                  "mFileTimeLo", 17);
  sf_mex_addfield(*c9_info, c9_b_emlrt_marshallOut(0U), "mFileTimeHi",
                  "mFileTimeHi", 17);
  sf_mex_assign(&c9_rhs17, sf_mex_createcellmatrix(0, 1), false);
  sf_mex_assign(&c9_lhs17, sf_mex_createcellmatrix(0, 1), false);
  sf_mex_addfield(*c9_info, sf_mex_duplicatearraysafe(&c9_rhs17), "rhs", "rhs",
                  17);
  sf_mex_addfield(*c9_info, sf_mex_duplicatearraysafe(&c9_lhs17), "lhs", "lhs",
                  17);
  sf_mex_addfield(*c9_info, c9_emlrt_marshallOut(
    "[ILXE]$matlabroot$/toolbox/coder/coder/+coder/+internal/+refblas/xnrm2.p"),
                  "context", "context", 18);
  sf_mex_addfield(*c9_info, c9_emlrt_marshallOut("coder.internal.indexPlus"),
                  "name", "name", 18);
  sf_mex_addfield(*c9_info, c9_emlrt_marshallOut("coder.internal.indexInt"),
                  "dominantType", "dominantType", 18);
  sf_mex_addfield(*c9_info, c9_emlrt_marshallOut(
    "[ILXE]$matlabroot$/toolbox/shared/coder/coder/+coder/+internal/indexPlus.m"),
                  "resolved", "resolved", 18);
  sf_mex_addfield(*c9_info, c9_b_emlrt_marshallOut(1372586760U), "fileTimeLo",
                  "fileTimeLo", 18);
  sf_mex_addfield(*c9_info, c9_b_emlrt_marshallOut(0U), "fileTimeHi",
                  "fileTimeHi", 18);
  sf_mex_addfield(*c9_info, c9_b_emlrt_marshallOut(0U), "mFileTimeLo",
                  "mFileTimeLo", 18);
  sf_mex_addfield(*c9_info, c9_b_emlrt_marshallOut(0U), "mFileTimeHi",
                  "mFileTimeHi", 18);
  sf_mex_assign(&c9_rhs18, sf_mex_createcellmatrix(0, 1), false);
  sf_mex_assign(&c9_lhs18, sf_mex_createcellmatrix(0, 1), false);
  sf_mex_addfield(*c9_info, sf_mex_duplicatearraysafe(&c9_rhs18), "rhs", "rhs",
                  18);
  sf_mex_addfield(*c9_info, sf_mex_duplicatearraysafe(&c9_lhs18), "lhs", "lhs",
                  18);
  sf_mex_addfield(*c9_info, c9_emlrt_marshallOut(
    "[ILXE]$matlabroot$/toolbox/coder/coder/+coder/+internal/+refblas/xnrm2.p"),
                  "context", "context", 19);
  sf_mex_addfield(*c9_info, c9_emlrt_marshallOut(
    "eml_int_forloop_overflow_check"), "name", "name", 19);
  sf_mex_addfield(*c9_info, c9_emlrt_marshallOut(""), "dominantType",
                  "dominantType", 19);
  sf_mex_addfield(*c9_info, c9_emlrt_marshallOut(
    "[ILXE]$matlabroot$/toolbox/eml/lib/matlab/eml/eml_int_forloop_overflow_check.m"),
                  "resolved", "resolved", 19);
  sf_mex_addfield(*c9_info, c9_b_emlrt_marshallOut(1397261022U), "fileTimeLo",
                  "fileTimeLo", 19);
  sf_mex_addfield(*c9_info, c9_b_emlrt_marshallOut(0U), "fileTimeHi",
                  "fileTimeHi", 19);
  sf_mex_addfield(*c9_info, c9_b_emlrt_marshallOut(0U), "mFileTimeLo",
                  "mFileTimeLo", 19);
  sf_mex_addfield(*c9_info, c9_b_emlrt_marshallOut(0U), "mFileTimeHi",
                  "mFileTimeHi", 19);
  sf_mex_assign(&c9_rhs19, sf_mex_createcellmatrix(0, 1), false);
  sf_mex_assign(&c9_lhs19, sf_mex_createcellmatrix(0, 1), false);
  sf_mex_addfield(*c9_info, sf_mex_duplicatearraysafe(&c9_rhs19), "rhs", "rhs",
                  19);
  sf_mex_addfield(*c9_info, sf_mex_duplicatearraysafe(&c9_lhs19), "lhs", "lhs",
                  19);
  sf_mex_addfield(*c9_info, c9_emlrt_marshallOut(
    "[ILXE]$matlabroot$/toolbox/eml/lib/matlab/eml/eml_int_forloop_overflow_check.m!eml_int_forloop_overflow_check_helper"),
                  "context", "context", 20);
  sf_mex_addfield(*c9_info, c9_emlrt_marshallOut("isfi"), "name", "name", 20);
  sf_mex_addfield(*c9_info, c9_emlrt_marshallOut("coder.internal.indexInt"),
                  "dominantType", "dominantType", 20);
  sf_mex_addfield(*c9_info, c9_emlrt_marshallOut(
    "[ILXE]$matlabroot$/toolbox/eml/lib/fixedpoint/isfi.m"), "resolved",
                  "resolved", 20);
  sf_mex_addfield(*c9_info, c9_b_emlrt_marshallOut(1346513958U), "fileTimeLo",
                  "fileTimeLo", 20);
  sf_mex_addfield(*c9_info, c9_b_emlrt_marshallOut(0U), "fileTimeHi",
                  "fileTimeHi", 20);
  sf_mex_addfield(*c9_info, c9_b_emlrt_marshallOut(0U), "mFileTimeLo",
                  "mFileTimeLo", 20);
  sf_mex_addfield(*c9_info, c9_b_emlrt_marshallOut(0U), "mFileTimeHi",
                  "mFileTimeHi", 20);
  sf_mex_assign(&c9_rhs20, sf_mex_createcellmatrix(0, 1), false);
  sf_mex_assign(&c9_lhs20, sf_mex_createcellmatrix(0, 1), false);
  sf_mex_addfield(*c9_info, sf_mex_duplicatearraysafe(&c9_rhs20), "rhs", "rhs",
                  20);
  sf_mex_addfield(*c9_info, sf_mex_duplicatearraysafe(&c9_lhs20), "lhs", "lhs",
                  20);
  sf_mex_addfield(*c9_info, c9_emlrt_marshallOut(
    "[ILXE]$matlabroot$/toolbox/eml/lib/fixedpoint/isfi.m"), "context",
                  "context", 21);
  sf_mex_addfield(*c9_info, c9_emlrt_marshallOut("isnumerictype"), "name",
                  "name", 21);
  sf_mex_addfield(*c9_info, c9_emlrt_marshallOut("char"), "dominantType",
                  "dominantType", 21);
  sf_mex_addfield(*c9_info, c9_emlrt_marshallOut(
    "[ILXE]$matlabroot$/toolbox/eml/lib/fixedpoint/isnumerictype.m"), "resolved",
                  "resolved", 21);
  sf_mex_addfield(*c9_info, c9_b_emlrt_marshallOut(1398879198U), "fileTimeLo",
                  "fileTimeLo", 21);
  sf_mex_addfield(*c9_info, c9_b_emlrt_marshallOut(0U), "fileTimeHi",
                  "fileTimeHi", 21);
  sf_mex_addfield(*c9_info, c9_b_emlrt_marshallOut(0U), "mFileTimeLo",
                  "mFileTimeLo", 21);
  sf_mex_addfield(*c9_info, c9_b_emlrt_marshallOut(0U), "mFileTimeHi",
                  "mFileTimeHi", 21);
  sf_mex_assign(&c9_rhs21, sf_mex_createcellmatrix(0, 1), false);
  sf_mex_assign(&c9_lhs21, sf_mex_createcellmatrix(0, 1), false);
  sf_mex_addfield(*c9_info, sf_mex_duplicatearraysafe(&c9_rhs21), "rhs", "rhs",
                  21);
  sf_mex_addfield(*c9_info, sf_mex_duplicatearraysafe(&c9_lhs21), "lhs", "lhs",
                  21);
  sf_mex_addfield(*c9_info, c9_emlrt_marshallOut(
    "[ILXE]$matlabroot$/toolbox/eml/lib/matlab/eml/eml_int_forloop_overflow_check.m!eml_int_forloop_overflow_check_helper"),
                  "context", "context", 22);
  sf_mex_addfield(*c9_info, c9_emlrt_marshallOut("intmax"), "name", "name", 22);
  sf_mex_addfield(*c9_info, c9_emlrt_marshallOut("char"), "dominantType",
                  "dominantType", 22);
  sf_mex_addfield(*c9_info, c9_emlrt_marshallOut(
    "[ILXE]$matlabroot$/toolbox/eml/lib/matlab/elmat/intmax.m"), "resolved",
                  "resolved", 22);
  sf_mex_addfield(*c9_info, c9_b_emlrt_marshallOut(1362265482U), "fileTimeLo",
                  "fileTimeLo", 22);
  sf_mex_addfield(*c9_info, c9_b_emlrt_marshallOut(0U), "fileTimeHi",
                  "fileTimeHi", 22);
  sf_mex_addfield(*c9_info, c9_b_emlrt_marshallOut(0U), "mFileTimeLo",
                  "mFileTimeLo", 22);
  sf_mex_addfield(*c9_info, c9_b_emlrt_marshallOut(0U), "mFileTimeHi",
                  "mFileTimeHi", 22);
  sf_mex_assign(&c9_rhs22, sf_mex_createcellmatrix(0, 1), false);
  sf_mex_assign(&c9_lhs22, sf_mex_createcellmatrix(0, 1), false);
  sf_mex_addfield(*c9_info, sf_mex_duplicatearraysafe(&c9_rhs22), "rhs", "rhs",
                  22);
  sf_mex_addfield(*c9_info, sf_mex_duplicatearraysafe(&c9_lhs22), "lhs", "lhs",
                  22);
  sf_mex_addfield(*c9_info, c9_emlrt_marshallOut(
    "[ILXE]$matlabroot$/toolbox/eml/lib/matlab/elmat/intmax.m"), "context",
                  "context", 23);
  sf_mex_addfield(*c9_info, c9_emlrt_marshallOut("eml_switch_helper"), "name",
                  "name", 23);
  sf_mex_addfield(*c9_info, c9_emlrt_marshallOut(""), "dominantType",
                  "dominantType", 23);
  sf_mex_addfield(*c9_info, c9_emlrt_marshallOut(
    "[ILXE]$matlabroot$/toolbox/eml/lib/matlab/eml/eml_switch_helper.m"),
                  "resolved", "resolved", 23);
  sf_mex_addfield(*c9_info, c9_b_emlrt_marshallOut(1393334458U), "fileTimeLo",
                  "fileTimeLo", 23);
  sf_mex_addfield(*c9_info, c9_b_emlrt_marshallOut(0U), "fileTimeHi",
                  "fileTimeHi", 23);
  sf_mex_addfield(*c9_info, c9_b_emlrt_marshallOut(0U), "mFileTimeLo",
                  "mFileTimeLo", 23);
  sf_mex_addfield(*c9_info, c9_b_emlrt_marshallOut(0U), "mFileTimeHi",
                  "mFileTimeHi", 23);
  sf_mex_assign(&c9_rhs23, sf_mex_createcellmatrix(0, 1), false);
  sf_mex_assign(&c9_lhs23, sf_mex_createcellmatrix(0, 1), false);
  sf_mex_addfield(*c9_info, sf_mex_duplicatearraysafe(&c9_rhs23), "rhs", "rhs",
                  23);
  sf_mex_addfield(*c9_info, sf_mex_duplicatearraysafe(&c9_lhs23), "lhs", "lhs",
                  23);
  sf_mex_addfield(*c9_info, c9_emlrt_marshallOut(
    "[ILXE]$matlabroot$/toolbox/eml/lib/matlab/eml/eml_int_forloop_overflow_check.m!eml_int_forloop_overflow_check_helper"),
                  "context", "context", 24);
  sf_mex_addfield(*c9_info, c9_emlrt_marshallOut("intmin"), "name", "name", 24);
  sf_mex_addfield(*c9_info, c9_emlrt_marshallOut("char"), "dominantType",
                  "dominantType", 24);
  sf_mex_addfield(*c9_info, c9_emlrt_marshallOut(
    "[ILXE]$matlabroot$/toolbox/eml/lib/matlab/elmat/intmin.m"), "resolved",
                  "resolved", 24);
  sf_mex_addfield(*c9_info, c9_b_emlrt_marshallOut(1362265482U), "fileTimeLo",
                  "fileTimeLo", 24);
  sf_mex_addfield(*c9_info, c9_b_emlrt_marshallOut(0U), "fileTimeHi",
                  "fileTimeHi", 24);
  sf_mex_addfield(*c9_info, c9_b_emlrt_marshallOut(0U), "mFileTimeLo",
                  "mFileTimeLo", 24);
  sf_mex_addfield(*c9_info, c9_b_emlrt_marshallOut(0U), "mFileTimeHi",
                  "mFileTimeHi", 24);
  sf_mex_assign(&c9_rhs24, sf_mex_createcellmatrix(0, 1), false);
  sf_mex_assign(&c9_lhs24, sf_mex_createcellmatrix(0, 1), false);
  sf_mex_addfield(*c9_info, sf_mex_duplicatearraysafe(&c9_rhs24), "rhs", "rhs",
                  24);
  sf_mex_addfield(*c9_info, sf_mex_duplicatearraysafe(&c9_lhs24), "lhs", "lhs",
                  24);
  sf_mex_addfield(*c9_info, c9_emlrt_marshallOut(
    "[ILXE]$matlabroot$/toolbox/eml/lib/matlab/elmat/intmin.m"), "context",
                  "context", 25);
  sf_mex_addfield(*c9_info, c9_emlrt_marshallOut("eml_switch_helper"), "name",
                  "name", 25);
  sf_mex_addfield(*c9_info, c9_emlrt_marshallOut(""), "dominantType",
                  "dominantType", 25);
  sf_mex_addfield(*c9_info, c9_emlrt_marshallOut(
    "[ILXE]$matlabroot$/toolbox/eml/lib/matlab/eml/eml_switch_helper.m"),
                  "resolved", "resolved", 25);
  sf_mex_addfield(*c9_info, c9_b_emlrt_marshallOut(1393334458U), "fileTimeLo",
                  "fileTimeLo", 25);
  sf_mex_addfield(*c9_info, c9_b_emlrt_marshallOut(0U), "fileTimeHi",
                  "fileTimeHi", 25);
  sf_mex_addfield(*c9_info, c9_b_emlrt_marshallOut(0U), "mFileTimeLo",
                  "mFileTimeLo", 25);
  sf_mex_addfield(*c9_info, c9_b_emlrt_marshallOut(0U), "mFileTimeHi",
                  "mFileTimeHi", 25);
  sf_mex_assign(&c9_rhs25, sf_mex_createcellmatrix(0, 1), false);
  sf_mex_assign(&c9_lhs25, sf_mex_createcellmatrix(0, 1), false);
  sf_mex_addfield(*c9_info, sf_mex_duplicatearraysafe(&c9_rhs25), "rhs", "rhs",
                  25);
  sf_mex_addfield(*c9_info, sf_mex_duplicatearraysafe(&c9_lhs25), "lhs", "lhs",
                  25);
  sf_mex_addfield(*c9_info, c9_emlrt_marshallOut(
    "[ILXE]$matlabroot$/toolbox/coder/coder/+coder/+internal/+refblas/xnrm2.p"),
                  "context", "context", 26);
  sf_mex_addfield(*c9_info, c9_emlrt_marshallOut("abs"), "name", "name", 26);
  sf_mex_addfield(*c9_info, c9_emlrt_marshallOut("single"), "dominantType",
                  "dominantType", 26);
  sf_mex_addfield(*c9_info, c9_emlrt_marshallOut(
    "[ILXE]$matlabroot$/toolbox/eml/lib/matlab/elfun/abs.m"), "resolved",
                  "resolved", 26);
  sf_mex_addfield(*c9_info, c9_b_emlrt_marshallOut(1363717452U), "fileTimeLo",
                  "fileTimeLo", 26);
  sf_mex_addfield(*c9_info, c9_b_emlrt_marshallOut(0U), "fileTimeHi",
                  "fileTimeHi", 26);
  sf_mex_addfield(*c9_info, c9_b_emlrt_marshallOut(0U), "mFileTimeLo",
                  "mFileTimeLo", 26);
  sf_mex_addfield(*c9_info, c9_b_emlrt_marshallOut(0U), "mFileTimeHi",
                  "mFileTimeHi", 26);
  sf_mex_assign(&c9_rhs26, sf_mex_createcellmatrix(0, 1), false);
  sf_mex_assign(&c9_lhs26, sf_mex_createcellmatrix(0, 1), false);
  sf_mex_addfield(*c9_info, sf_mex_duplicatearraysafe(&c9_rhs26), "rhs", "rhs",
                  26);
  sf_mex_addfield(*c9_info, sf_mex_duplicatearraysafe(&c9_lhs26), "lhs", "lhs",
                  26);
  sf_mex_addfield(*c9_info, c9_emlrt_marshallOut(
    "[ILXE]$matlabroot$/toolbox/eml/lib/matlab/elfun/abs.m"), "context",
                  "context", 27);
  sf_mex_addfield(*c9_info, c9_emlrt_marshallOut(
    "coder.internal.isBuiltInNumeric"), "name", "name", 27);
  sf_mex_addfield(*c9_info, c9_emlrt_marshallOut("single"), "dominantType",
                  "dominantType", 27);
  sf_mex_addfield(*c9_info, c9_emlrt_marshallOut(
    "[ILXE]$matlabroot$/toolbox/shared/coder/coder/+coder/+internal/isBuiltInNumeric.m"),
                  "resolved", "resolved", 27);
  sf_mex_addfield(*c9_info, c9_b_emlrt_marshallOut(1395935456U), "fileTimeLo",
                  "fileTimeLo", 27);
  sf_mex_addfield(*c9_info, c9_b_emlrt_marshallOut(0U), "fileTimeHi",
                  "fileTimeHi", 27);
  sf_mex_addfield(*c9_info, c9_b_emlrt_marshallOut(0U), "mFileTimeLo",
                  "mFileTimeLo", 27);
  sf_mex_addfield(*c9_info, c9_b_emlrt_marshallOut(0U), "mFileTimeHi",
                  "mFileTimeHi", 27);
  sf_mex_assign(&c9_rhs27, sf_mex_createcellmatrix(0, 1), false);
  sf_mex_assign(&c9_lhs27, sf_mex_createcellmatrix(0, 1), false);
  sf_mex_addfield(*c9_info, sf_mex_duplicatearraysafe(&c9_rhs27), "rhs", "rhs",
                  27);
  sf_mex_addfield(*c9_info, sf_mex_duplicatearraysafe(&c9_lhs27), "lhs", "lhs",
                  27);
  sf_mex_addfield(*c9_info, c9_emlrt_marshallOut(
    "[ILXE]$matlabroot$/toolbox/eml/lib/matlab/elfun/abs.m"), "context",
                  "context", 28);
  sf_mex_addfield(*c9_info, c9_emlrt_marshallOut("eml_scalar_abs"), "name",
                  "name", 28);
  sf_mex_addfield(*c9_info, c9_emlrt_marshallOut("single"), "dominantType",
                  "dominantType", 28);
  sf_mex_addfield(*c9_info, c9_emlrt_marshallOut(
    "[ILXE]$matlabroot$/toolbox/eml/lib/matlab/elfun/eml_scalar_abs.m"),
                  "resolved", "resolved", 28);
  sf_mex_addfield(*c9_info, c9_b_emlrt_marshallOut(1286822312U), "fileTimeLo",
                  "fileTimeLo", 28);
  sf_mex_addfield(*c9_info, c9_b_emlrt_marshallOut(0U), "fileTimeHi",
                  "fileTimeHi", 28);
  sf_mex_addfield(*c9_info, c9_b_emlrt_marshallOut(0U), "mFileTimeLo",
                  "mFileTimeLo", 28);
  sf_mex_addfield(*c9_info, c9_b_emlrt_marshallOut(0U), "mFileTimeHi",
                  "mFileTimeHi", 28);
  sf_mex_assign(&c9_rhs28, sf_mex_createcellmatrix(0, 1), false);
  sf_mex_assign(&c9_lhs28, sf_mex_createcellmatrix(0, 1), false);
  sf_mex_addfield(*c9_info, sf_mex_duplicatearraysafe(&c9_rhs28), "rhs", "rhs",
                  28);
  sf_mex_addfield(*c9_info, sf_mex_duplicatearraysafe(&c9_lhs28), "lhs", "lhs",
                  28);
  sf_mex_addfield(*c9_info, c9_emlrt_marshallOut(""), "context", "context", 29);
  sf_mex_addfield(*c9_info, c9_emlrt_marshallOut("mrdivide"), "name", "name", 29);
  sf_mex_addfield(*c9_info, c9_emlrt_marshallOut("single"), "dominantType",
                  "dominantType", 29);
  sf_mex_addfield(*c9_info, c9_emlrt_marshallOut(
    "[ILXE]$matlabroot$/toolbox/eml/lib/matlab/ops/mrdivide.p"), "resolved",
                  "resolved", 29);
  sf_mex_addfield(*c9_info, c9_b_emlrt_marshallOut(1410811248U), "fileTimeLo",
                  "fileTimeLo", 29);
  sf_mex_addfield(*c9_info, c9_b_emlrt_marshallOut(0U), "fileTimeHi",
                  "fileTimeHi", 29);
  sf_mex_addfield(*c9_info, c9_b_emlrt_marshallOut(1370013486U), "mFileTimeLo",
                  "mFileTimeLo", 29);
  sf_mex_addfield(*c9_info, c9_b_emlrt_marshallOut(0U), "mFileTimeHi",
                  "mFileTimeHi", 29);
  sf_mex_assign(&c9_rhs29, sf_mex_createcellmatrix(0, 1), false);
  sf_mex_assign(&c9_lhs29, sf_mex_createcellmatrix(0, 1), false);
  sf_mex_addfield(*c9_info, sf_mex_duplicatearraysafe(&c9_rhs29), "rhs", "rhs",
                  29);
  sf_mex_addfield(*c9_info, sf_mex_duplicatearraysafe(&c9_lhs29), "lhs", "lhs",
                  29);
  sf_mex_addfield(*c9_info, c9_emlrt_marshallOut(
    "[ILXE]$matlabroot$/toolbox/eml/lib/matlab/ops/mrdivide.p"), "context",
                  "context", 30);
  sf_mex_addfield(*c9_info, c9_emlrt_marshallOut("coder.internal.assert"),
                  "name", "name", 30);
  sf_mex_addfield(*c9_info, c9_emlrt_marshallOut("char"), "dominantType",
                  "dominantType", 30);
  sf_mex_addfield(*c9_info, c9_emlrt_marshallOut(
    "[ILXE]$matlabroot$/toolbox/shared/coder/coder/+coder/+internal/assert.m"),
                  "resolved", "resolved", 30);
  sf_mex_addfield(*c9_info, c9_b_emlrt_marshallOut(1389721374U), "fileTimeLo",
                  "fileTimeLo", 30);
  sf_mex_addfield(*c9_info, c9_b_emlrt_marshallOut(0U), "fileTimeHi",
                  "fileTimeHi", 30);
  sf_mex_addfield(*c9_info, c9_b_emlrt_marshallOut(0U), "mFileTimeLo",
                  "mFileTimeLo", 30);
  sf_mex_addfield(*c9_info, c9_b_emlrt_marshallOut(0U), "mFileTimeHi",
                  "mFileTimeHi", 30);
  sf_mex_assign(&c9_rhs30, sf_mex_createcellmatrix(0, 1), false);
  sf_mex_assign(&c9_lhs30, sf_mex_createcellmatrix(0, 1), false);
  sf_mex_addfield(*c9_info, sf_mex_duplicatearraysafe(&c9_rhs30), "rhs", "rhs",
                  30);
  sf_mex_addfield(*c9_info, sf_mex_duplicatearraysafe(&c9_lhs30), "lhs", "lhs",
                  30);
  sf_mex_addfield(*c9_info, c9_emlrt_marshallOut(
    "[ILXE]$matlabroot$/toolbox/eml/lib/matlab/ops/mrdivide.p"), "context",
                  "context", 31);
  sf_mex_addfield(*c9_info, c9_emlrt_marshallOut("rdivide"), "name", "name", 31);
  sf_mex_addfield(*c9_info, c9_emlrt_marshallOut("single"), "dominantType",
                  "dominantType", 31);
  sf_mex_addfield(*c9_info, c9_emlrt_marshallOut(
    "[ILXE]$matlabroot$/toolbox/eml/lib/matlab/ops/rdivide.m"), "resolved",
                  "resolved", 31);
  sf_mex_addfield(*c9_info, c9_b_emlrt_marshallOut(1363717480U), "fileTimeLo",
                  "fileTimeLo", 31);
  sf_mex_addfield(*c9_info, c9_b_emlrt_marshallOut(0U), "fileTimeHi",
                  "fileTimeHi", 31);
  sf_mex_addfield(*c9_info, c9_b_emlrt_marshallOut(0U), "mFileTimeLo",
                  "mFileTimeLo", 31);
  sf_mex_addfield(*c9_info, c9_b_emlrt_marshallOut(0U), "mFileTimeHi",
                  "mFileTimeHi", 31);
  sf_mex_assign(&c9_rhs31, sf_mex_createcellmatrix(0, 1), false);
  sf_mex_assign(&c9_lhs31, sf_mex_createcellmatrix(0, 1), false);
  sf_mex_addfield(*c9_info, sf_mex_duplicatearraysafe(&c9_rhs31), "rhs", "rhs",
                  31);
  sf_mex_addfield(*c9_info, sf_mex_duplicatearraysafe(&c9_lhs31), "lhs", "lhs",
                  31);
  sf_mex_addfield(*c9_info, c9_emlrt_marshallOut(
    "[ILXE]$matlabroot$/toolbox/eml/lib/matlab/ops/rdivide.m"), "context",
                  "context", 32);
  sf_mex_addfield(*c9_info, c9_emlrt_marshallOut(
    "coder.internal.isBuiltInNumeric"), "name", "name", 32);
  sf_mex_addfield(*c9_info, c9_emlrt_marshallOut("single"), "dominantType",
                  "dominantType", 32);
  sf_mex_addfield(*c9_info, c9_emlrt_marshallOut(
    "[ILXE]$matlabroot$/toolbox/shared/coder/coder/+coder/+internal/isBuiltInNumeric.m"),
                  "resolved", "resolved", 32);
  sf_mex_addfield(*c9_info, c9_b_emlrt_marshallOut(1395935456U), "fileTimeLo",
                  "fileTimeLo", 32);
  sf_mex_addfield(*c9_info, c9_b_emlrt_marshallOut(0U), "fileTimeHi",
                  "fileTimeHi", 32);
  sf_mex_addfield(*c9_info, c9_b_emlrt_marshallOut(0U), "mFileTimeLo",
                  "mFileTimeLo", 32);
  sf_mex_addfield(*c9_info, c9_b_emlrt_marshallOut(0U), "mFileTimeHi",
                  "mFileTimeHi", 32);
  sf_mex_assign(&c9_rhs32, sf_mex_createcellmatrix(0, 1), false);
  sf_mex_assign(&c9_lhs32, sf_mex_createcellmatrix(0, 1), false);
  sf_mex_addfield(*c9_info, sf_mex_duplicatearraysafe(&c9_rhs32), "rhs", "rhs",
                  32);
  sf_mex_addfield(*c9_info, sf_mex_duplicatearraysafe(&c9_lhs32), "lhs", "lhs",
                  32);
  sf_mex_addfield(*c9_info, c9_emlrt_marshallOut(
    "[ILXE]$matlabroot$/toolbox/eml/lib/matlab/ops/rdivide.m"), "context",
                  "context", 33);
  sf_mex_addfield(*c9_info, c9_emlrt_marshallOut("eml_scalexp_compatible"),
                  "name", "name", 33);
  sf_mex_addfield(*c9_info, c9_emlrt_marshallOut("single"), "dominantType",
                  "dominantType", 33);
  sf_mex_addfield(*c9_info, c9_emlrt_marshallOut(
    "[ILXE]$matlabroot$/toolbox/eml/lib/matlab/eml/eml_scalexp_compatible.m"),
                  "resolved", "resolved", 33);
  sf_mex_addfield(*c9_info, c9_b_emlrt_marshallOut(1286822396U), "fileTimeLo",
                  "fileTimeLo", 33);
  sf_mex_addfield(*c9_info, c9_b_emlrt_marshallOut(0U), "fileTimeHi",
                  "fileTimeHi", 33);
  sf_mex_addfield(*c9_info, c9_b_emlrt_marshallOut(0U), "mFileTimeLo",
                  "mFileTimeLo", 33);
  sf_mex_addfield(*c9_info, c9_b_emlrt_marshallOut(0U), "mFileTimeHi",
                  "mFileTimeHi", 33);
  sf_mex_assign(&c9_rhs33, sf_mex_createcellmatrix(0, 1), false);
  sf_mex_assign(&c9_lhs33, sf_mex_createcellmatrix(0, 1), false);
  sf_mex_addfield(*c9_info, sf_mex_duplicatearraysafe(&c9_rhs33), "rhs", "rhs",
                  33);
  sf_mex_addfield(*c9_info, sf_mex_duplicatearraysafe(&c9_lhs33), "lhs", "lhs",
                  33);
  sf_mex_addfield(*c9_info, c9_emlrt_marshallOut(
    "[ILXE]$matlabroot$/toolbox/eml/lib/matlab/ops/rdivide.m"), "context",
                  "context", 34);
  sf_mex_addfield(*c9_info, c9_emlrt_marshallOut("eml_div"), "name", "name", 34);
  sf_mex_addfield(*c9_info, c9_emlrt_marshallOut("single"), "dominantType",
                  "dominantType", 34);
  sf_mex_addfield(*c9_info, c9_emlrt_marshallOut(
    "[ILXE]$matlabroot$/toolbox/eml/lib/matlab/eml/eml_div.m"), "resolved",
                  "resolved", 34);
  sf_mex_addfield(*c9_info, c9_b_emlrt_marshallOut(1386427552U), "fileTimeLo",
                  "fileTimeLo", 34);
  sf_mex_addfield(*c9_info, c9_b_emlrt_marshallOut(0U), "fileTimeHi",
                  "fileTimeHi", 34);
  sf_mex_addfield(*c9_info, c9_b_emlrt_marshallOut(0U), "mFileTimeLo",
                  "mFileTimeLo", 34);
  sf_mex_addfield(*c9_info, c9_b_emlrt_marshallOut(0U), "mFileTimeHi",
                  "mFileTimeHi", 34);
  sf_mex_assign(&c9_rhs34, sf_mex_createcellmatrix(0, 1), false);
  sf_mex_assign(&c9_lhs34, sf_mex_createcellmatrix(0, 1), false);
  sf_mex_addfield(*c9_info, sf_mex_duplicatearraysafe(&c9_rhs34), "rhs", "rhs",
                  34);
  sf_mex_addfield(*c9_info, sf_mex_duplicatearraysafe(&c9_lhs34), "lhs", "lhs",
                  34);
  sf_mex_addfield(*c9_info, c9_emlrt_marshallOut(
    "[ILXE]$matlabroot$/toolbox/eml/lib/matlab/eml/eml_div.m"), "context",
                  "context", 35);
  sf_mex_addfield(*c9_info, c9_emlrt_marshallOut("coder.internal.div"), "name",
                  "name", 35);
  sf_mex_addfield(*c9_info, c9_emlrt_marshallOut("single"), "dominantType",
                  "dominantType", 35);
  sf_mex_addfield(*c9_info, c9_emlrt_marshallOut(
    "[ILXE]$matlabroot$/toolbox/coder/coder/+coder/+internal/div.p"), "resolved",
                  "resolved", 35);
  sf_mex_addfield(*c9_info, c9_b_emlrt_marshallOut(1410811370U), "fileTimeLo",
                  "fileTimeLo", 35);
  sf_mex_addfield(*c9_info, c9_b_emlrt_marshallOut(0U), "fileTimeHi",
                  "fileTimeHi", 35);
  sf_mex_addfield(*c9_info, c9_b_emlrt_marshallOut(0U), "mFileTimeLo",
                  "mFileTimeLo", 35);
  sf_mex_addfield(*c9_info, c9_b_emlrt_marshallOut(0U), "mFileTimeHi",
                  "mFileTimeHi", 35);
  sf_mex_assign(&c9_rhs35, sf_mex_createcellmatrix(0, 1), false);
  sf_mex_assign(&c9_lhs35, sf_mex_createcellmatrix(0, 1), false);
  sf_mex_addfield(*c9_info, sf_mex_duplicatearraysafe(&c9_rhs35), "rhs", "rhs",
                  35);
  sf_mex_addfield(*c9_info, sf_mex_duplicatearraysafe(&c9_lhs35), "lhs", "lhs",
                  35);
  sf_mex_addfield(*c9_info, c9_emlrt_marshallOut(""), "context", "context", 36);
  sf_mex_addfield(*c9_info, c9_emlrt_marshallOut("atan2d"), "name", "name", 36);
  sf_mex_addfield(*c9_info, c9_emlrt_marshallOut("single"), "dominantType",
                  "dominantType", 36);
  sf_mex_addfield(*c9_info, c9_emlrt_marshallOut(
    "[ILXE]$matlabroot$/toolbox/eml/lib/matlab/elfun/atan2d.m"), "resolved",
                  "resolved", 36);
  sf_mex_addfield(*c9_info, c9_b_emlrt_marshallOut(1395332096U), "fileTimeLo",
                  "fileTimeLo", 36);
  sf_mex_addfield(*c9_info, c9_b_emlrt_marshallOut(0U), "fileTimeHi",
                  "fileTimeHi", 36);
  sf_mex_addfield(*c9_info, c9_b_emlrt_marshallOut(0U), "mFileTimeLo",
                  "mFileTimeLo", 36);
  sf_mex_addfield(*c9_info, c9_b_emlrt_marshallOut(0U), "mFileTimeHi",
                  "mFileTimeHi", 36);
  sf_mex_assign(&c9_rhs36, sf_mex_createcellmatrix(0, 1), false);
  sf_mex_assign(&c9_lhs36, sf_mex_createcellmatrix(0, 1), false);
  sf_mex_addfield(*c9_info, sf_mex_duplicatearraysafe(&c9_rhs36), "rhs", "rhs",
                  36);
  sf_mex_addfield(*c9_info, sf_mex_duplicatearraysafe(&c9_lhs36), "lhs", "lhs",
                  36);
  sf_mex_addfield(*c9_info, c9_emlrt_marshallOut(
    "[ILXE]$matlabroot$/toolbox/eml/lib/matlab/elfun/atan2d.m"), "context",
                  "context", 37);
  sf_mex_addfield(*c9_info, c9_emlrt_marshallOut("eml_scalar_eg"), "name",
                  "name", 37);
  sf_mex_addfield(*c9_info, c9_emlrt_marshallOut("single"), "dominantType",
                  "dominantType", 37);
  sf_mex_addfield(*c9_info, c9_emlrt_marshallOut(
    "[ILXE]$matlabroot$/toolbox/eml/lib/matlab/eml/eml_scalar_eg.m"), "resolved",
                  "resolved", 37);
  sf_mex_addfield(*c9_info, c9_b_emlrt_marshallOut(1375984288U), "fileTimeLo",
                  "fileTimeLo", 37);
  sf_mex_addfield(*c9_info, c9_b_emlrt_marshallOut(0U), "fileTimeHi",
                  "fileTimeHi", 37);
  sf_mex_addfield(*c9_info, c9_b_emlrt_marshallOut(0U), "mFileTimeLo",
                  "mFileTimeLo", 37);
  sf_mex_addfield(*c9_info, c9_b_emlrt_marshallOut(0U), "mFileTimeHi",
                  "mFileTimeHi", 37);
  sf_mex_assign(&c9_rhs37, sf_mex_createcellmatrix(0, 1), false);
  sf_mex_assign(&c9_lhs37, sf_mex_createcellmatrix(0, 1), false);
  sf_mex_addfield(*c9_info, sf_mex_duplicatearraysafe(&c9_rhs37), "rhs", "rhs",
                  37);
  sf_mex_addfield(*c9_info, sf_mex_duplicatearraysafe(&c9_lhs37), "lhs", "lhs",
                  37);
  sf_mex_addfield(*c9_info, c9_emlrt_marshallOut(
    "[ILXE]$matlabroot$/toolbox/eml/lib/matlab/eml/eml_scalar_eg.m"), "context",
                  "context", 38);
  sf_mex_addfield(*c9_info, c9_emlrt_marshallOut("coder.internal.scalarEg"),
                  "name", "name", 38);
  sf_mex_addfield(*c9_info, c9_emlrt_marshallOut("single"), "dominantType",
                  "dominantType", 38);
  sf_mex_addfield(*c9_info, c9_emlrt_marshallOut(
    "[ILXE]$matlabroot$/toolbox/coder/coder/+coder/+internal/scalarEg.p"),
                  "resolved", "resolved", 38);
  sf_mex_addfield(*c9_info, c9_b_emlrt_marshallOut(1410811370U), "fileTimeLo",
                  "fileTimeLo", 38);
  sf_mex_addfield(*c9_info, c9_b_emlrt_marshallOut(0U), "fileTimeHi",
                  "fileTimeHi", 38);
  sf_mex_addfield(*c9_info, c9_b_emlrt_marshallOut(0U), "mFileTimeLo",
                  "mFileTimeLo", 38);
  sf_mex_addfield(*c9_info, c9_b_emlrt_marshallOut(0U), "mFileTimeHi",
                  "mFileTimeHi", 38);
  sf_mex_assign(&c9_rhs38, sf_mex_createcellmatrix(0, 1), false);
  sf_mex_assign(&c9_lhs38, sf_mex_createcellmatrix(0, 1), false);
  sf_mex_addfield(*c9_info, sf_mex_duplicatearraysafe(&c9_rhs38), "rhs", "rhs",
                  38);
  sf_mex_addfield(*c9_info, sf_mex_duplicatearraysafe(&c9_lhs38), "lhs", "lhs",
                  38);
  sf_mex_addfield(*c9_info, c9_emlrt_marshallOut(
    "[ILXE]$matlabroot$/toolbox/eml/lib/matlab/elfun/atan2d.m"), "context",
                  "context", 39);
  sf_mex_addfield(*c9_info, c9_emlrt_marshallOut("eml_scalexp_alloc"), "name",
                  "name", 39);
  sf_mex_addfield(*c9_info, c9_emlrt_marshallOut("single"), "dominantType",
                  "dominantType", 39);
  sf_mex_addfield(*c9_info, c9_emlrt_marshallOut(
    "[ILXE]$matlabroot$/toolbox/eml/lib/matlab/eml/eml_scalexp_alloc.m"),
                  "resolved", "resolved", 39);
  sf_mex_addfield(*c9_info, c9_b_emlrt_marshallOut(1375984288U), "fileTimeLo",
                  "fileTimeLo", 39);
  sf_mex_addfield(*c9_info, c9_b_emlrt_marshallOut(0U), "fileTimeHi",
                  "fileTimeHi", 39);
  sf_mex_addfield(*c9_info, c9_b_emlrt_marshallOut(0U), "mFileTimeLo",
                  "mFileTimeLo", 39);
  sf_mex_addfield(*c9_info, c9_b_emlrt_marshallOut(0U), "mFileTimeHi",
                  "mFileTimeHi", 39);
  sf_mex_assign(&c9_rhs39, sf_mex_createcellmatrix(0, 1), false);
  sf_mex_assign(&c9_lhs39, sf_mex_createcellmatrix(0, 1), false);
  sf_mex_addfield(*c9_info, sf_mex_duplicatearraysafe(&c9_rhs39), "rhs", "rhs",
                  39);
  sf_mex_addfield(*c9_info, sf_mex_duplicatearraysafe(&c9_lhs39), "lhs", "lhs",
                  39);
  sf_mex_addfield(*c9_info, c9_emlrt_marshallOut(
    "[ILXE]$matlabroot$/toolbox/eml/lib/matlab/eml/eml_scalexp_alloc.m"),
                  "context", "context", 40);
  sf_mex_addfield(*c9_info, c9_emlrt_marshallOut("coder.internal.scalexpAlloc"),
                  "name", "name", 40);
  sf_mex_addfield(*c9_info, c9_emlrt_marshallOut("single"), "dominantType",
                  "dominantType", 40);
  sf_mex_addfield(*c9_info, c9_emlrt_marshallOut(
    "[ILXE]$matlabroot$/toolbox/coder/coder/+coder/+internal/scalexpAlloc.p"),
                  "resolved", "resolved", 40);
  sf_mex_addfield(*c9_info, c9_b_emlrt_marshallOut(1410811370U), "fileTimeLo",
                  "fileTimeLo", 40);
  sf_mex_addfield(*c9_info, c9_b_emlrt_marshallOut(0U), "fileTimeHi",
                  "fileTimeHi", 40);
  sf_mex_addfield(*c9_info, c9_b_emlrt_marshallOut(0U), "mFileTimeLo",
                  "mFileTimeLo", 40);
  sf_mex_addfield(*c9_info, c9_b_emlrt_marshallOut(0U), "mFileTimeHi",
                  "mFileTimeHi", 40);
  sf_mex_assign(&c9_rhs40, sf_mex_createcellmatrix(0, 1), false);
  sf_mex_assign(&c9_lhs40, sf_mex_createcellmatrix(0, 1), false);
  sf_mex_addfield(*c9_info, sf_mex_duplicatearraysafe(&c9_rhs40), "rhs", "rhs",
                  40);
  sf_mex_addfield(*c9_info, sf_mex_duplicatearraysafe(&c9_lhs40), "lhs", "lhs",
                  40);
  sf_mex_addfield(*c9_info, c9_emlrt_marshallOut(
    "[ILXE]$matlabroot$/toolbox/eml/lib/matlab/elfun/atan2d.m"), "context",
                  "context", 41);
  sf_mex_addfield(*c9_info, c9_emlrt_marshallOut("eml_scalar_atan2"), "name",
                  "name", 41);
  sf_mex_addfield(*c9_info, c9_emlrt_marshallOut("single"), "dominantType",
                  "dominantType", 41);
  sf_mex_addfield(*c9_info, c9_emlrt_marshallOut(
    "[ILXE]$matlabroot$/toolbox/eml/lib/matlab/elfun/eml_scalar_atan2.m"),
                  "resolved", "resolved", 41);
  sf_mex_addfield(*c9_info, c9_b_emlrt_marshallOut(1286822320U), "fileTimeLo",
                  "fileTimeLo", 41);
  sf_mex_addfield(*c9_info, c9_b_emlrt_marshallOut(0U), "fileTimeHi",
                  "fileTimeHi", 41);
  sf_mex_addfield(*c9_info, c9_b_emlrt_marshallOut(0U), "mFileTimeLo",
                  "mFileTimeLo", 41);
  sf_mex_addfield(*c9_info, c9_b_emlrt_marshallOut(0U), "mFileTimeHi",
                  "mFileTimeHi", 41);
  sf_mex_assign(&c9_rhs41, sf_mex_createcellmatrix(0, 1), false);
  sf_mex_assign(&c9_lhs41, sf_mex_createcellmatrix(0, 1), false);
  sf_mex_addfield(*c9_info, sf_mex_duplicatearraysafe(&c9_rhs41), "rhs", "rhs",
                  41);
  sf_mex_addfield(*c9_info, sf_mex_duplicatearraysafe(&c9_lhs41), "lhs", "lhs",
                  41);
  sf_mex_addfield(*c9_info, c9_emlrt_marshallOut(
    "[ILXE]$matlabroot$/toolbox/eml/lib/matlab/elfun/atan2d.m"), "context",
                  "context", 42);
  sf_mex_addfield(*c9_info, c9_emlrt_marshallOut("eml_mtimes_helper"), "name",
                  "name", 42);
  sf_mex_addfield(*c9_info, c9_emlrt_marshallOut(""), "dominantType",
                  "dominantType", 42);
  sf_mex_addfield(*c9_info, c9_emlrt_marshallOut(
    "[ILXE]$matlabroot$/toolbox/eml/lib/matlab/ops/eml_mtimes_helper.m"),
                  "resolved", "resolved", 42);
  sf_mex_addfield(*c9_info, c9_b_emlrt_marshallOut(1383880894U), "fileTimeLo",
                  "fileTimeLo", 42);
  sf_mex_addfield(*c9_info, c9_b_emlrt_marshallOut(0U), "fileTimeHi",
                  "fileTimeHi", 42);
  sf_mex_addfield(*c9_info, c9_b_emlrt_marshallOut(0U), "mFileTimeLo",
                  "mFileTimeLo", 42);
  sf_mex_addfield(*c9_info, c9_b_emlrt_marshallOut(0U), "mFileTimeHi",
                  "mFileTimeHi", 42);
  sf_mex_assign(&c9_rhs42, sf_mex_createcellmatrix(0, 1), false);
  sf_mex_assign(&c9_lhs42, sf_mex_createcellmatrix(0, 1), false);
  sf_mex_addfield(*c9_info, sf_mex_duplicatearraysafe(&c9_rhs42), "rhs", "rhs",
                  42);
  sf_mex_addfield(*c9_info, sf_mex_duplicatearraysafe(&c9_lhs42), "lhs", "lhs",
                  42);
  sf_mex_addfield(*c9_info, c9_emlrt_marshallOut(""), "context", "context", 43);
  sf_mex_addfield(*c9_info, c9_emlrt_marshallOut("intmax"), "name", "name", 43);
  sf_mex_addfield(*c9_info, c9_emlrt_marshallOut("char"), "dominantType",
                  "dominantType", 43);
  sf_mex_addfield(*c9_info, c9_emlrt_marshallOut(
    "[ILXE]$matlabroot$/toolbox/eml/lib/matlab/elmat/intmax.m"), "resolved",
                  "resolved", 43);
  sf_mex_addfield(*c9_info, c9_b_emlrt_marshallOut(1362265482U), "fileTimeLo",
                  "fileTimeLo", 43);
  sf_mex_addfield(*c9_info, c9_b_emlrt_marshallOut(0U), "fileTimeHi",
                  "fileTimeHi", 43);
  sf_mex_addfield(*c9_info, c9_b_emlrt_marshallOut(0U), "mFileTimeLo",
                  "mFileTimeLo", 43);
  sf_mex_addfield(*c9_info, c9_b_emlrt_marshallOut(0U), "mFileTimeHi",
                  "mFileTimeHi", 43);
  sf_mex_assign(&c9_rhs43, sf_mex_createcellmatrix(0, 1), false);
  sf_mex_assign(&c9_lhs43, sf_mex_createcellmatrix(0, 1), false);
  sf_mex_addfield(*c9_info, sf_mex_duplicatearraysafe(&c9_rhs43), "rhs", "rhs",
                  43);
  sf_mex_addfield(*c9_info, sf_mex_duplicatearraysafe(&c9_lhs43), "lhs", "lhs",
                  43);
  sf_mex_addfield(*c9_info, c9_emlrt_marshallOut(""), "context", "context", 44);
  sf_mex_addfield(*c9_info, c9_emlrt_marshallOut("abs"), "name", "name", 44);
  sf_mex_addfield(*c9_info, c9_emlrt_marshallOut("int16"), "dominantType",
                  "dominantType", 44);
  sf_mex_addfield(*c9_info, c9_emlrt_marshallOut(
    "[ILXE]$matlabroot$/toolbox/eml/lib/matlab/elfun/abs.m"), "resolved",
                  "resolved", 44);
  sf_mex_addfield(*c9_info, c9_b_emlrt_marshallOut(1363717452U), "fileTimeLo",
                  "fileTimeLo", 44);
  sf_mex_addfield(*c9_info, c9_b_emlrt_marshallOut(0U), "fileTimeHi",
                  "fileTimeHi", 44);
  sf_mex_addfield(*c9_info, c9_b_emlrt_marshallOut(0U), "mFileTimeLo",
                  "mFileTimeLo", 44);
  sf_mex_addfield(*c9_info, c9_b_emlrt_marshallOut(0U), "mFileTimeHi",
                  "mFileTimeHi", 44);
  sf_mex_assign(&c9_rhs44, sf_mex_createcellmatrix(0, 1), false);
  sf_mex_assign(&c9_lhs44, sf_mex_createcellmatrix(0, 1), false);
  sf_mex_addfield(*c9_info, sf_mex_duplicatearraysafe(&c9_rhs44), "rhs", "rhs",
                  44);
  sf_mex_addfield(*c9_info, sf_mex_duplicatearraysafe(&c9_lhs44), "lhs", "lhs",
                  44);
  sf_mex_addfield(*c9_info, c9_emlrt_marshallOut(
    "[ILXE]$matlabroot$/toolbox/eml/lib/matlab/elfun/abs.m"), "context",
                  "context", 45);
  sf_mex_addfield(*c9_info, c9_emlrt_marshallOut(
    "coder.internal.isBuiltInNumeric"), "name", "name", 45);
  sf_mex_addfield(*c9_info, c9_emlrt_marshallOut("int16"), "dominantType",
                  "dominantType", 45);
  sf_mex_addfield(*c9_info, c9_emlrt_marshallOut(
    "[ILXE]$matlabroot$/toolbox/shared/coder/coder/+coder/+internal/isBuiltInNumeric.m"),
                  "resolved", "resolved", 45);
  sf_mex_addfield(*c9_info, c9_b_emlrt_marshallOut(1395935456U), "fileTimeLo",
                  "fileTimeLo", 45);
  sf_mex_addfield(*c9_info, c9_b_emlrt_marshallOut(0U), "fileTimeHi",
                  "fileTimeHi", 45);
  sf_mex_addfield(*c9_info, c9_b_emlrt_marshallOut(0U), "mFileTimeLo",
                  "mFileTimeLo", 45);
  sf_mex_addfield(*c9_info, c9_b_emlrt_marshallOut(0U), "mFileTimeHi",
                  "mFileTimeHi", 45);
  sf_mex_assign(&c9_rhs45, sf_mex_createcellmatrix(0, 1), false);
  sf_mex_assign(&c9_lhs45, sf_mex_createcellmatrix(0, 1), false);
  sf_mex_addfield(*c9_info, sf_mex_duplicatearraysafe(&c9_rhs45), "rhs", "rhs",
                  45);
  sf_mex_addfield(*c9_info, sf_mex_duplicatearraysafe(&c9_lhs45), "lhs", "lhs",
                  45);
  sf_mex_addfield(*c9_info, c9_emlrt_marshallOut(
    "[ILXE]$matlabroot$/toolbox/eml/lib/matlab/elfun/abs.m"), "context",
                  "context", 46);
  sf_mex_addfield(*c9_info, c9_emlrt_marshallOut("eml_scalar_abs"), "name",
                  "name", 46);
  sf_mex_addfield(*c9_info, c9_emlrt_marshallOut("int16"), "dominantType",
                  "dominantType", 46);
  sf_mex_addfield(*c9_info, c9_emlrt_marshallOut(
    "[ILXE]$matlabroot$/toolbox/eml/lib/matlab/elfun/eml_scalar_abs.m"),
                  "resolved", "resolved", 46);
  sf_mex_addfield(*c9_info, c9_b_emlrt_marshallOut(1286822312U), "fileTimeLo",
                  "fileTimeLo", 46);
  sf_mex_addfield(*c9_info, c9_b_emlrt_marshallOut(0U), "fileTimeHi",
                  "fileTimeHi", 46);
  sf_mex_addfield(*c9_info, c9_b_emlrt_marshallOut(0U), "mFileTimeLo",
                  "mFileTimeLo", 46);
  sf_mex_addfield(*c9_info, c9_b_emlrt_marshallOut(0U), "mFileTimeHi",
                  "mFileTimeHi", 46);
  sf_mex_assign(&c9_rhs46, sf_mex_createcellmatrix(0, 1), false);
  sf_mex_assign(&c9_lhs46, sf_mex_createcellmatrix(0, 1), false);
  sf_mex_addfield(*c9_info, sf_mex_duplicatearraysafe(&c9_rhs46), "rhs", "rhs",
                  46);
  sf_mex_addfield(*c9_info, sf_mex_duplicatearraysafe(&c9_lhs46), "lhs", "lhs",
                  46);
  sf_mex_destroy(&c9_rhs0);
  sf_mex_destroy(&c9_lhs0);
  sf_mex_destroy(&c9_rhs1);
  sf_mex_destroy(&c9_lhs1);
  sf_mex_destroy(&c9_rhs2);
  sf_mex_destroy(&c9_lhs2);
  sf_mex_destroy(&c9_rhs3);
  sf_mex_destroy(&c9_lhs3);
  sf_mex_destroy(&c9_rhs4);
  sf_mex_destroy(&c9_lhs4);
  sf_mex_destroy(&c9_rhs5);
  sf_mex_destroy(&c9_lhs5);
  sf_mex_destroy(&c9_rhs6);
  sf_mex_destroy(&c9_lhs6);
  sf_mex_destroy(&c9_rhs7);
  sf_mex_destroy(&c9_lhs7);
  sf_mex_destroy(&c9_rhs8);
  sf_mex_destroy(&c9_lhs8);
  sf_mex_destroy(&c9_rhs9);
  sf_mex_destroy(&c9_lhs9);
  sf_mex_destroy(&c9_rhs10);
  sf_mex_destroy(&c9_lhs10);
  sf_mex_destroy(&c9_rhs11);
  sf_mex_destroy(&c9_lhs11);
  sf_mex_destroy(&c9_rhs12);
  sf_mex_destroy(&c9_lhs12);
  sf_mex_destroy(&c9_rhs13);
  sf_mex_destroy(&c9_lhs13);
  sf_mex_destroy(&c9_rhs14);
  sf_mex_destroy(&c9_lhs14);
  sf_mex_destroy(&c9_rhs15);
  sf_mex_destroy(&c9_lhs15);
  sf_mex_destroy(&c9_rhs16);
  sf_mex_destroy(&c9_lhs16);
  sf_mex_destroy(&c9_rhs17);
  sf_mex_destroy(&c9_lhs17);
  sf_mex_destroy(&c9_rhs18);
  sf_mex_destroy(&c9_lhs18);
  sf_mex_destroy(&c9_rhs19);
  sf_mex_destroy(&c9_lhs19);
  sf_mex_destroy(&c9_rhs20);
  sf_mex_destroy(&c9_lhs20);
  sf_mex_destroy(&c9_rhs21);
  sf_mex_destroy(&c9_lhs21);
  sf_mex_destroy(&c9_rhs22);
  sf_mex_destroy(&c9_lhs22);
  sf_mex_destroy(&c9_rhs23);
  sf_mex_destroy(&c9_lhs23);
  sf_mex_destroy(&c9_rhs24);
  sf_mex_destroy(&c9_lhs24);
  sf_mex_destroy(&c9_rhs25);
  sf_mex_destroy(&c9_lhs25);
  sf_mex_destroy(&c9_rhs26);
  sf_mex_destroy(&c9_lhs26);
  sf_mex_destroy(&c9_rhs27);
  sf_mex_destroy(&c9_lhs27);
  sf_mex_destroy(&c9_rhs28);
  sf_mex_destroy(&c9_lhs28);
  sf_mex_destroy(&c9_rhs29);
  sf_mex_destroy(&c9_lhs29);
  sf_mex_destroy(&c9_rhs30);
  sf_mex_destroy(&c9_lhs30);
  sf_mex_destroy(&c9_rhs31);
  sf_mex_destroy(&c9_lhs31);
  sf_mex_destroy(&c9_rhs32);
  sf_mex_destroy(&c9_lhs32);
  sf_mex_destroy(&c9_rhs33);
  sf_mex_destroy(&c9_lhs33);
  sf_mex_destroy(&c9_rhs34);
  sf_mex_destroy(&c9_lhs34);
  sf_mex_destroy(&c9_rhs35);
  sf_mex_destroy(&c9_lhs35);
  sf_mex_destroy(&c9_rhs36);
  sf_mex_destroy(&c9_lhs36);
  sf_mex_destroy(&c9_rhs37);
  sf_mex_destroy(&c9_lhs37);
  sf_mex_destroy(&c9_rhs38);
  sf_mex_destroy(&c9_lhs38);
  sf_mex_destroy(&c9_rhs39);
  sf_mex_destroy(&c9_lhs39);
  sf_mex_destroy(&c9_rhs40);
  sf_mex_destroy(&c9_lhs40);
  sf_mex_destroy(&c9_rhs41);
  sf_mex_destroy(&c9_lhs41);
  sf_mex_destroy(&c9_rhs42);
  sf_mex_destroy(&c9_lhs42);
  sf_mex_destroy(&c9_rhs43);
  sf_mex_destroy(&c9_lhs43);
  sf_mex_destroy(&c9_rhs44);
  sf_mex_destroy(&c9_lhs44);
  sf_mex_destroy(&c9_rhs45);
  sf_mex_destroy(&c9_lhs45);
  sf_mex_destroy(&c9_rhs46);
  sf_mex_destroy(&c9_lhs46);
}

static const mxArray *c9_emlrt_marshallOut(const char * c9_u)
{
  const mxArray *c9_y = NULL;
  c9_y = NULL;
  sf_mex_assign(&c9_y, sf_mex_create("y", c9_u, 15, 0U, 0U, 0U, 2, 1, strlen
    (c9_u)), false);
  return c9_y;
}

static const mxArray *c9_b_emlrt_marshallOut(const uint32_T c9_u)
{
  const mxArray *c9_y = NULL;
  c9_y = NULL;
  sf_mex_assign(&c9_y, sf_mex_create("y", &c9_u, 7, 0U, 0U, 0U, 0), false);
  return c9_y;
}

static void c9_calcShootWay(SFc9_greenOffenceInstanceStruct *chartInstance)
{
  uint32_T c9_debug_family_var_map[3];
  int8_T c9_pos[2];
  real_T c9_nargin = 0.0;
  real_T c9_nargout = 0.0;
  int32_T c9_i21;
  static int8_T c9_iv7[2] = { 70, -60 };

  int32_T c9_i22;
  int32_T c9_i23;
  int8_T c9_b_pos;
  int8_T c9_c_pos;
  int8_T c9_iv8[2];
  int8_T c9_d_pos[2];
  int32_T c9_i24;
  int32_T c9_i25;
  int32_T c9_i26;
  real32_T c9_b[2];
  int32_T c9_i27;
  int8_T c9_iv9[2];
  int32_T c9_i28;
  int32_T c9_i29;
  real32_T c9_x[2];
  int32_T c9_i30;
  real32_T c9_b_x[2];
  real32_T c9_y;
  real32_T c9_B;
  real32_T c9_b_y;
  real32_T c9_c_y;
  real32_T c9_d_y;
  int32_T c9_i31;
  int32_T c9_i32;
  real32_T c9_f0;
  int8_T c9_i33;
  int32_T c9_i34;
  int32_T c9_i35;
  int32_T c9_i36;
  real32_T c9_e_y;
  int32_T c9_i37;
  real32_T c9_c_x;
  real32_T c9_f_y;
  real32_T c9_d_x;
  real32_T c9_r;
  real32_T c9_b_b;
  real32_T c9_b_r;
  real32_T c9_f1;
  int16_T c9_i38;
  _SFD_SYMBOL_SCOPE_PUSH_EML(0U, 3U, 3U, c9_debug_family_names,
    c9_debug_family_var_map);
  _SFD_SYMBOL_SCOPE_ADD_EML_IMPORTABLE(c9_pos, 0U, c9_b_sf_marshallOut,
    c9_b_sf_marshallIn);
  _SFD_SYMBOL_SCOPE_ADD_EML_IMPORTABLE(&c9_nargin, 1U, c9_sf_marshallOut,
    c9_sf_marshallIn);
  _SFD_SYMBOL_SCOPE_ADD_EML_IMPORTABLE(&c9_nargout, 2U, c9_sf_marshallOut,
    c9_sf_marshallIn);
  CV_EML_FCN(5, 0);
  _SFD_EML_CALL(5U, chartInstance->c9_sfEvent, 2);
  c9_errorIfDataNotWrittenToFcn(chartInstance, 2U, 7U, 126U, 25, 8);
  if (CV_EML_IF(5, 1, 0, CV_RELATIONAL_EVAL(4U, 5U, 0, (real_T)
        chartInstance->c9_enemy[2].y, 0.0, -1, 4U, (real_T)
        chartInstance->c9_enemy[2].y > 0.0))) {
    _SFD_EML_CALL(5U, chartInstance->c9_sfEvent, 3);
    for (c9_i21 = 0; c9_i21 < 2; c9_i21++) {
      c9_pos[c9_i21] = c9_iv7[c9_i21];
    }
  } else {
    _SFD_EML_CALL(5U, chartInstance->c9_sfEvent, 5);
    for (c9_i22 = 0; c9_i22 < 2; c9_i22++) {
      c9_pos[c9_i22] = (int8_T)(70 + (int8_T)(-10 * (int8_T)c9_i22));
    }
  }

  _SFD_EML_CALL(5U, chartInstance->c9_sfEvent, 7);
  if (CV_EML_IF(5, 1, 1, CV_RELATIONAL_EVAL(4U, 5U, 1, (real_T)*(uint8_T *)
        &((char_T *)chartInstance->c9_me)[4], 103.0, 0, 1U, *(uint8_T *)
        &((char_T *)chartInstance->c9_me)[4] != 103))) {
    _SFD_EML_CALL(5U, chartInstance->c9_sfEvent, 8);
    c9_i23 = -c9_pos[0];
    if (c9_i23 > 127) {
      CV_SATURATION_EVAL(4, 5, 0, 0, 1);
      c9_i23 = 127;
    } else {
      if (CV_SATURATION_EVAL(4, 5, 0, 0, c9_i23 < -128)) {
        c9_i23 = -128;
      }
    }

    c9_pos[0] = (int8_T)c9_i23;
  }

  _SFD_EML_CALL(5U, chartInstance->c9_sfEvent, 10);
  c9_b_pos = c9_pos[0];
  c9_c_pos = c9_pos[1];
  c9_iv8[0] = *(int8_T *)&((char_T *)chartInstance->c9_ball)[0];
  c9_iv8[1] = *(int8_T *)&((char_T *)chartInstance->c9_ball)[1];
  c9_d_pos[0] = c9_b_pos;
  c9_d_pos[1] = c9_c_pos;
  for (c9_i24 = 0; c9_i24 < 2; c9_i24++) {
    c9_i25 = c9_iv8[c9_i24] - c9_d_pos[c9_i24];
    if (c9_i25 > 127) {
      CV_SATURATION_EVAL(4, 5, 1, 0, 1);
      c9_i25 = 127;
    } else {
      if (CV_SATURATION_EVAL(4, 5, 1, 0, c9_i25 < -128)) {
        c9_i25 = -128;
      }
    }

    c9_pos[c9_i24] = (int8_T)c9_i25;
  }

  _SFD_EML_CALL(5U, chartInstance->c9_sfEvent, 11);
  for (c9_i26 = 0; c9_i26 < 2; c9_i26++) {
    c9_b[c9_i26] = (real32_T)c9_pos[c9_i26];
  }

  for (c9_i27 = 0; c9_i27 < 2; c9_i27++) {
    c9_b[c9_i27] *= 30.0F;
  }

  c9_iv9[0] = *(int8_T *)&((char_T *)chartInstance->c9_ball)[0];
  c9_iv9[1] = *(int8_T *)&((char_T *)chartInstance->c9_ball)[1];
  for (c9_i28 = 0; c9_i28 < 2; c9_i28++) {
    c9_i29 = c9_iv9[c9_i28] - c9_pos[c9_i28];
    if (c9_i29 > 127) {
      CV_SATURATION_EVAL(4, 5, 3, 0, 1);
      c9_i29 = 127;
    } else {
      if (CV_SATURATION_EVAL(4, 5, 3, 0, c9_i29 < -128)) {
        c9_i29 = -128;
      }
    }

    c9_x[c9_i28] = (real32_T)(int8_T)c9_i29;
  }

  for (c9_i30 = 0; c9_i30 < 2; c9_i30++) {
    c9_b_x[c9_i30] = c9_x[c9_i30];
  }

  c9_y = c9_b_eml_xnrm2(chartInstance, c9_b_x);
  c9_B = c9_y;
  c9_b_y = c9_B;
  c9_c_y = c9_b_y;
  c9_d_y = c9_c_y;
  for (c9_i31 = 0; c9_i31 < 2; c9_i31++) {
    c9_b[c9_i31] /= c9_d_y;
  }

  for (c9_i32 = 0; c9_i32 < 2; c9_i32++) {
    c9_f0 = muSingleScalarRound(c9_b[c9_i32]);
    if (c9_f0 < 128.0F) {
      if (CV_SATURATION_EVAL(4, 5, 2, 1, c9_f0 >= -128.0F)) {
        c9_i33 = (int8_T)c9_f0;
      } else {
        c9_i33 = MIN_int8_T;
      }
    } else if (CV_SATURATION_EVAL(4, 5, 2, 0, c9_f0 >= 128.0F)) {
      c9_i33 = MAX_int8_T;
    } else {
      c9_i33 = 0;
    }

    c9_pos[c9_i32] = c9_i33;
  }

  _SFD_EML_CALL(5U, chartInstance->c9_sfEvent, 13);
  c9_i34 = *(int8_T *)&((char_T *)chartInstance->c9_ball)[0] + c9_pos[0];
  if (c9_i34 > 127) {
    CV_SATURATION_EVAL(4, 5, 4, 0, 1);
    c9_i34 = 127;
  } else {
    if (CV_SATURATION_EVAL(4, 5, 4, 0, c9_i34 < -128)) {
      c9_i34 = -128;
    }
  }

  chartInstance->c9_shootWay.x = (int8_T)c9_i34;
  c9_updateDataWrittenToVector(chartInstance, 4U);
  _SFD_EML_CALL(5U, chartInstance->c9_sfEvent, 14);
  c9_i35 = *(int8_T *)&((char_T *)chartInstance->c9_ball)[1] + c9_pos[1];
  if (c9_i35 > 127) {
    CV_SATURATION_EVAL(4, 5, 5, 0, 1);
    c9_i35 = 127;
  } else {
    if (CV_SATURATION_EVAL(4, 5, 5, 0, c9_i35 < -128)) {
      c9_i35 = -128;
    }
  }

  chartInstance->c9_shootWay.y = (int8_T)c9_i35;
  c9_updateDataWrittenToVector(chartInstance, 4U);
  _SFD_EML_CALL(5U, chartInstance->c9_sfEvent, 15);
  c9_i36 = -c9_pos[1];
  if (c9_i36 > 127) {
    CV_SATURATION_EVAL(4, 5, 7, 0, 1);
    c9_i36 = 127;
  } else {
    if (CV_SATURATION_EVAL(4, 5, 7, 0, c9_i36 < -128)) {
      c9_i36 = -128;
    }
  }

  c9_e_y = (real32_T)(int8_T)c9_i36;
  c9_i37 = -c9_pos[0];
  if (c9_i37 > 127) {
    CV_SATURATION_EVAL(4, 5, 8, 0, 1);
    c9_i37 = 127;
  } else {
    if (CV_SATURATION_EVAL(4, 5, 8, 0, c9_i37 < -128)) {
      c9_i37 = -128;
    }
  }

  c9_c_x = (real32_T)(int8_T)c9_i37;
  c9_f_y = c9_e_y;
  c9_d_x = c9_c_x;
  c9_r = muSingleScalarAtan2(c9_f_y, c9_d_x);
  c9_b_b = c9_r;
  c9_b_r = 57.2957802F * c9_b_b;
  c9_f1 = muSingleScalarRound(c9_b_r);
  if (c9_f1 < 32768.0F) {
    if (CV_SATURATION_EVAL(4, 5, 6, 1, c9_f1 >= -32768.0F)) {
      c9_i38 = (int16_T)c9_f1;
    } else {
      c9_i38 = MIN_int16_T;
    }
  } else if (CV_SATURATION_EVAL(4, 5, 6, 0, c9_f1 >= 32768.0F)) {
    c9_i38 = MAX_int16_T;
  } else {
    c9_i38 = 0;
  }

  chartInstance->c9_shootWay.orientation = c9_i38;
  c9_updateDataWrittenToVector(chartInstance, 4U);
  _SFD_EML_CALL(5U, chartInstance->c9_sfEvent, -15);
  _SFD_SYMBOL_SCOPE_POP();
}

static void c9_calcStartTeams(SFc9_greenOffenceInstanceStruct *chartInstance)
{
  uint32_T c9_debug_family_var_map[2];
  real_T c9_nargin = 0.0;
  real_T c9_nargout = 0.0;
  int32_T c9_i39;
  int32_T c9_i40;
  c9_Player c9_rv2[6];
  int32_T c9_i41;
  int32_T c9_i42;
  int32_T c9_i43;
  c9_Player c9_rv3[6];
  int32_T c9_i44;
  int32_T c9_i45;
  int32_T c9_i46;
  c9_Player c9_rv4[6];
  int32_T c9_i47;
  int32_T c9_i48;
  int32_T c9_i49;
  c9_Player c9_rv5[6];
  int32_T c9_i50;
  _SFD_SYMBOL_SCOPE_PUSH_EML(0U, 2U, 2U, c9_b_debug_family_names,
    c9_debug_family_var_map);
  _SFD_SYMBOL_SCOPE_ADD_EML_IMPORTABLE(&c9_nargin, 0U, c9_sf_marshallOut,
    c9_sf_marshallIn);
  _SFD_SYMBOL_SCOPE_ADD_EML_IMPORTABLE(&c9_nargout, 1U, c9_sf_marshallOut,
    c9_sf_marshallIn);
  CV_EML_FCN(7, 0);
  _SFD_EML_CALL(7U, chartInstance->c9_sfEvent, 2);
  switch (*(uint8_T *)&((char_T *)chartInstance->c9_me)[4]) {
   case 103U:
    CV_EML_SWITCH(7, 1, 0, 1);
    _SFD_EML_CALL(7U, chartInstance->c9_sfEvent, 4);
    for (c9_i39 = 0; c9_i39 < 6; c9_i39++) {
      for (c9_i40 = 0; c9_i40 < 1; c9_i40++) {
        c9_rv2[c9_i40 + c9_i39].x = *(int8_T *)&((char_T *)(c9_Player *)
          &((char_T *)chartInstance->c9_players)[8 * (c9_i40 + c9_i39)])[0];
        c9_rv2[c9_i40 + c9_i39].y = *(int8_T *)&((char_T *)(c9_Player *)
          &((char_T *)chartInstance->c9_players)[8 * (c9_i40 + c9_i39)])[1];
        c9_rv2[c9_i40 + c9_i39].orientation = *(int16_T *)&((char_T *)(c9_Player
          *)&((char_T *)chartInstance->c9_players)[8 * (c9_i40 + c9_i39)])[2];
        c9_rv2[c9_i40 + c9_i39].color = *(uint8_T *)&((char_T *)(c9_Player *)
          &((char_T *)chartInstance->c9_players)[8 * (c9_i40 + c9_i39)])[4];
        c9_rv2[c9_i40 + c9_i39].position = *(uint8_T *)&((char_T *)(c9_Player *)
          &((char_T *)chartInstance->c9_players)[8 * (c9_i40 + c9_i39)])[5];
        c9_rv2[c9_i40 + c9_i39].valid = *(uint8_T *)&((char_T *)(c9_Player *)
          &((char_T *)chartInstance->c9_players)[8 * (c9_i40 + c9_i39)])[6];
      }
    }

    for (c9_i41 = 0; c9_i41 < 3; c9_i41++) {
      chartInstance->c9_enemy[c9_i41] = c9_rv2[c9_i41 + 3];
    }

    c9_updateDataWrittenToVector(chartInstance, 2U);
    _SFD_EML_CALL(7U, chartInstance->c9_sfEvent, 5);
    for (c9_i42 = 0; c9_i42 < 6; c9_i42++) {
      for (c9_i43 = 0; c9_i43 < 1; c9_i43++) {
        c9_rv3[c9_i43 + c9_i42].x = *(int8_T *)&((char_T *)(c9_Player *)
          &((char_T *)chartInstance->c9_players)[8 * (c9_i43 + c9_i42)])[0];
        c9_rv3[c9_i43 + c9_i42].y = *(int8_T *)&((char_T *)(c9_Player *)
          &((char_T *)chartInstance->c9_players)[8 * (c9_i43 + c9_i42)])[1];
        c9_rv3[c9_i43 + c9_i42].orientation = *(int16_T *)&((char_T *)(c9_Player
          *)&((char_T *)chartInstance->c9_players)[8 * (c9_i43 + c9_i42)])[2];
        c9_rv3[c9_i43 + c9_i42].color = *(uint8_T *)&((char_T *)(c9_Player *)
          &((char_T *)chartInstance->c9_players)[8 * (c9_i43 + c9_i42)])[4];
        c9_rv3[c9_i43 + c9_i42].position = *(uint8_T *)&((char_T *)(c9_Player *)
          &((char_T *)chartInstance->c9_players)[8 * (c9_i43 + c9_i42)])[5];
        c9_rv3[c9_i43 + c9_i42].valid = *(uint8_T *)&((char_T *)(c9_Player *)
          &((char_T *)chartInstance->c9_players)[8 * (c9_i43 + c9_i42)])[6];
      }
    }

    for (c9_i44 = 0; c9_i44 < 3; c9_i44++) {
      chartInstance->c9_friends[c9_i44] = c9_rv3[c9_i44];
    }

    c9_updateDataWrittenToVector(chartInstance, 3U);
    break;

   case 98U:
    CV_EML_SWITCH(7, 1, 0, 2);
    _SFD_EML_CALL(7U, chartInstance->c9_sfEvent, 7);
    for (c9_i45 = 0; c9_i45 < 6; c9_i45++) {
      for (c9_i46 = 0; c9_i46 < 1; c9_i46++) {
        c9_rv4[c9_i46 + c9_i45].x = *(int8_T *)&((char_T *)(c9_Player *)
          &((char_T *)chartInstance->c9_players)[8 * (c9_i46 + c9_i45)])[0];
        c9_rv4[c9_i46 + c9_i45].y = *(int8_T *)&((char_T *)(c9_Player *)
          &((char_T *)chartInstance->c9_players)[8 * (c9_i46 + c9_i45)])[1];
        c9_rv4[c9_i46 + c9_i45].orientation = *(int16_T *)&((char_T *)(c9_Player
          *)&((char_T *)chartInstance->c9_players)[8 * (c9_i46 + c9_i45)])[2];
        c9_rv4[c9_i46 + c9_i45].color = *(uint8_T *)&((char_T *)(c9_Player *)
          &((char_T *)chartInstance->c9_players)[8 * (c9_i46 + c9_i45)])[4];
        c9_rv4[c9_i46 + c9_i45].position = *(uint8_T *)&((char_T *)(c9_Player *)
          &((char_T *)chartInstance->c9_players)[8 * (c9_i46 + c9_i45)])[5];
        c9_rv4[c9_i46 + c9_i45].valid = *(uint8_T *)&((char_T *)(c9_Player *)
          &((char_T *)chartInstance->c9_players)[8 * (c9_i46 + c9_i45)])[6];
      }
    }

    for (c9_i47 = 0; c9_i47 < 3; c9_i47++) {
      chartInstance->c9_enemy[c9_i47] = c9_rv4[c9_i47];
    }

    c9_updateDataWrittenToVector(chartInstance, 2U);
    _SFD_EML_CALL(7U, chartInstance->c9_sfEvent, 8);
    for (c9_i48 = 0; c9_i48 < 6; c9_i48++) {
      for (c9_i49 = 0; c9_i49 < 1; c9_i49++) {
        c9_rv5[c9_i49 + c9_i48].x = *(int8_T *)&((char_T *)(c9_Player *)
          &((char_T *)chartInstance->c9_players)[8 * (c9_i49 + c9_i48)])[0];
        c9_rv5[c9_i49 + c9_i48].y = *(int8_T *)&((char_T *)(c9_Player *)
          &((char_T *)chartInstance->c9_players)[8 * (c9_i49 + c9_i48)])[1];
        c9_rv5[c9_i49 + c9_i48].orientation = *(int16_T *)&((char_T *)(c9_Player
          *)&((char_T *)chartInstance->c9_players)[8 * (c9_i49 + c9_i48)])[2];
        c9_rv5[c9_i49 + c9_i48].color = *(uint8_T *)&((char_T *)(c9_Player *)
          &((char_T *)chartInstance->c9_players)[8 * (c9_i49 + c9_i48)])[4];
        c9_rv5[c9_i49 + c9_i48].position = *(uint8_T *)&((char_T *)(c9_Player *)
          &((char_T *)chartInstance->c9_players)[8 * (c9_i49 + c9_i48)])[5];
        c9_rv5[c9_i49 + c9_i48].valid = *(uint8_T *)&((char_T *)(c9_Player *)
          &((char_T *)chartInstance->c9_players)[8 * (c9_i49 + c9_i48)])[6];
      }
    }

    for (c9_i50 = 0; c9_i50 < 3; c9_i50++) {
      chartInstance->c9_friends[c9_i50] = c9_rv5[c9_i50 + 3];
    }

    c9_updateDataWrittenToVector(chartInstance, 3U);
    break;

   default:
    CV_EML_SWITCH(7, 1, 0, 0);
    break;
  }

  _SFD_EML_CALL(7U, chartInstance->c9_sfEvent, -8);
  _SFD_SYMBOL_SCOPE_POP();
}

static void c9_calcStartPos(SFc9_greenOffenceInstanceStruct *chartInstance)
{
  uint32_T c9_debug_family_var_map[2];
  real_T c9_nargin = 0.0;
  real_T c9_nargout = 0.0;
  int32_T c9_i51;
  int32_T c9_i52;
  int32_T c9_i53;
  int32_T c9_i54;
  int32_T c9_i55;
  _SFD_SYMBOL_SCOPE_PUSH_EML(0U, 2U, 2U, c9_c_debug_family_names,
    c9_debug_family_var_map);
  _SFD_SYMBOL_SCOPE_ADD_EML_IMPORTABLE(&c9_nargin, 0U, c9_sf_marshallOut,
    c9_sf_marshallIn);
  _SFD_SYMBOL_SCOPE_ADD_EML_IMPORTABLE(&c9_nargout, 1U, c9_sf_marshallOut,
    c9_sf_marshallIn);
  CV_EML_FCN(6, 0);
  _SFD_EML_CALL(6U, chartInstance->c9_sfEvent, 2);
  switch (*(uint8_T *)&((char_T *)chartInstance->c9_me)[5]) {
   case 111U:
    CV_EML_SWITCH(6, 1, 0, 1);
    _SFD_EML_CALL(6U, chartInstance->c9_sfEvent, 4);
    for (c9_i51 = 0; c9_i51 < 2; c9_i51++) {
      chartInstance->c9_startingPos[c9_i51] = (int8_T)(10 + (int8_T)(-10 *
        (int8_T)c9_i51));
    }

    c9_updateDataWrittenToVector(chartInstance, 1U);
    break;

   case 100U:
    CV_EML_SWITCH(6, 1, 0, 2);
    _SFD_EML_CALL(6U, chartInstance->c9_sfEvent, 6);
    for (c9_i52 = 0; c9_i52 < 2; c9_i52++) {
      chartInstance->c9_startingPos[c9_i52] = (int8_T)(30 + (int8_T)(-30 *
        (int8_T)c9_i52));
    }

    c9_updateDataWrittenToVector(chartInstance, 1U);
    break;

   case 103U:
    CV_EML_SWITCH(6, 1, 0, 3);
    _SFD_EML_CALL(6U, chartInstance->c9_sfEvent, 8);
    for (c9_i53 = 0; c9_i53 < 2; c9_i53++) {
      chartInstance->c9_startingPos[c9_i53] = (int8_T)(70 + (int8_T)(-70 *
        (int8_T)c9_i53));
    }

    c9_updateDataWrittenToVector(chartInstance, 1U);
    break;

   default:
    CV_EML_SWITCH(6, 1, 0, 0);
    break;
  }

  _SFD_EML_CALL(6U, chartInstance->c9_sfEvent, 10);
  if (CV_EML_IF(6, 1, 0, CV_RELATIONAL_EVAL(4U, 6U, 0, (real_T)*(uint8_T *)
        &((char_T *)chartInstance->c9_me)[4], 103.0, 0, 0U, *(uint8_T *)
        &((char_T *)chartInstance->c9_me)[4] == 103))) {
    _SFD_EML_CALL(6U, chartInstance->c9_sfEvent, 11);
    c9_errorIfDataNotWrittenToFcn(chartInstance, 1U, 5U, 86U, 244, 11);
    for (c9_i54 = 0; c9_i54 < 2; c9_i54++) {
      c9_i55 = -chartInstance->c9_startingPos[c9_i54];
      if (c9_i55 > 127) {
        CV_SATURATION_EVAL(4, 6, 0, 0, 1);
        c9_i55 = 127;
      } else {
        if (CV_SATURATION_EVAL(4, 6, 0, 0, c9_i55 < -128)) {
          c9_i55 = -128;
        }
      }

      chartInstance->c9_startingPos[c9_i54] = (int8_T)c9_i55;
    }

    c9_updateDataWrittenToVector(chartInstance, 1U);
  }

  _SFD_EML_CALL(6U, chartInstance->c9_sfEvent, -11);
  _SFD_SYMBOL_SCOPE_POP();
}

static real32_T c9_b_eml_xnrm2(SFc9_greenOffenceInstanceStruct *chartInstance,
  real32_T c9_x[2])
{
  real32_T c9_y;
  real32_T c9_scale;
  int32_T c9_k;
  int32_T c9_b_k;
  real32_T c9_b_x;
  real32_T c9_c_x;
  real32_T c9_absxk;
  real32_T c9_t;
  c9_threshold(chartInstance);
  c9_y = 0.0F;
  c9_realmin(chartInstance);
  c9_scale = 1.17549435E-38F;
  for (c9_k = 1; c9_k < 3; c9_k++) {
    c9_b_k = c9_k;
    c9_b_x = c9_x[_SFD_EML_ARRAY_BOUNDS_CHECK("", (int32_T)_SFD_INTEGER_CHECK("",
      (real_T)c9_b_k), 1, 2, 1, 0) - 1];
    c9_c_x = c9_b_x;
    c9_absxk = muSingleScalarAbs(c9_c_x);
    if (c9_absxk > c9_scale) {
      c9_t = c9_scale / c9_absxk;
      c9_y = 1.0F + c9_y * c9_t * c9_t;
      c9_scale = c9_absxk;
    } else {
      c9_t = c9_absxk / c9_scale;
      c9_y += c9_t * c9_t;
    }
  }

  return c9_scale * muSingleScalarSqrt(c9_y);
}

static void c9_threshold(SFc9_greenOffenceInstanceStruct *chartInstance)
{
  (void)chartInstance;
}

static void c9_realmin(SFc9_greenOffenceInstanceStruct *chartInstance)
{
  (void)chartInstance;
}

static uint8_T c9_checkReached(SFc9_greenOffenceInstanceStruct *chartInstance,
  int8_T c9_pos[2], real_T c9_tol)
{
  uint8_T c9_posReached;
  uint32_T c9_debug_family_var_map[5];
  real_T c9_nargin = 2.0;
  real_T c9_nargout = 1.0;
  int8_T c9_b_pos[2];
  int8_T c9_iv10[2];
  int32_T c9_i56;
  int32_T c9_i57;
  real32_T c9_x[2];
  int32_T c9_i58;
  real32_T c9_b_x[2];
  real32_T c9_y;
  real_T c9_d2;
  _SFD_SYMBOL_SCOPE_PUSH_EML(0U, 5U, 5U, c9_n_debug_family_names,
    c9_debug_family_var_map);
  _SFD_SYMBOL_SCOPE_ADD_EML_IMPORTABLE(&c9_nargin, 0U, c9_sf_marshallOut,
    c9_sf_marshallIn);
  _SFD_SYMBOL_SCOPE_ADD_EML_IMPORTABLE(&c9_nargout, 1U, c9_sf_marshallOut,
    c9_sf_marshallIn);
  _SFD_SYMBOL_SCOPE_ADD_EML_IMPORTABLE(c9_pos, 2U, c9_e_sf_marshallOut,
    c9_e_sf_marshallIn);
  _SFD_SYMBOL_SCOPE_ADD_EML_IMPORTABLE(&c9_tol, 3U, c9_sf_marshallOut,
    c9_sf_marshallIn);
  _SFD_SYMBOL_SCOPE_ADD_EML_IMPORTABLE(&c9_posReached, 4U, c9_d_sf_marshallOut,
    c9_d_sf_marshallIn);
  CV_EML_FCN(8, 0);
  _SFD_EML_CALL(8U, chartInstance->c9_sfEvent, 2);
  c9_posReached = 0U;
  _SFD_EML_CALL(8U, chartInstance->c9_sfEvent, 3);
  c9_b_pos[0] = c9_pos[0];
  c9_b_pos[1] = c9_pos[1];
  c9_iv10[0] = *(int8_T *)&((char_T *)chartInstance->c9_me)[0];
  c9_iv10[1] = *(int8_T *)&((char_T *)chartInstance->c9_me)[1];
  for (c9_i56 = 0; c9_i56 < 2; c9_i56++) {
    c9_i57 = c9_b_pos[c9_i56] - c9_iv10[c9_i56];
    if (c9_i57 > 127) {
      CV_SATURATION_EVAL(4, 8, 0, 0, 1);
      c9_i57 = 127;
    } else {
      if (CV_SATURATION_EVAL(4, 8, 0, 0, c9_i57 < -128)) {
        c9_i57 = -128;
      }
    }

    c9_x[c9_i56] = (real32_T)(int8_T)c9_i57;
  }

  for (c9_i58 = 0; c9_i58 < 2; c9_i58++) {
    c9_b_x[c9_i58] = c9_x[c9_i58];
  }

  c9_y = c9_eml_xnrm2(chartInstance, c9_b_x);
  c9_d2 = c9_y;
  if (CV_EML_IF(8, 1, 0, CV_RELATIONAL_EVAL(4U, 8U, 0, c9_d2, c9_tol, -1, 2U,
        c9_d2 < c9_tol))) {
    _SFD_EML_CALL(8U, chartInstance->c9_sfEvent, 4);
    c9_posReached = 1U;
  }

  _SFD_EML_CALL(8U, chartInstance->c9_sfEvent, -4);
  _SFD_SYMBOL_SCOPE_POP();
  return c9_posReached;
}

static const mxArray *c9_f_sf_marshallOut(void *chartInstanceVoid, void
  *c9_inData)
{
  const mxArray *c9_mxArrayOutData = NULL;
  int32_T c9_u;
  const mxArray *c9_y = NULL;
  SFc9_greenOffenceInstanceStruct *chartInstance;
  chartInstance = (SFc9_greenOffenceInstanceStruct *)chartInstanceVoid;
  c9_mxArrayOutData = NULL;
  c9_u = *(int32_T *)c9_inData;
  c9_y = NULL;
  sf_mex_assign(&c9_y, sf_mex_create("y", &c9_u, 6, 0U, 0U, 0U, 0), false);
  sf_mex_assign(&c9_mxArrayOutData, c9_y, false);
  return c9_mxArrayOutData;
}

static int32_T c9_g_emlrt_marshallIn(SFc9_greenOffenceInstanceStruct
  *chartInstance, const mxArray *c9_u, const emlrtMsgIdentifier *c9_parentId)
{
  int32_T c9_y;
  int32_T c9_i59;
  (void)chartInstance;
  sf_mex_import(c9_parentId, sf_mex_dup(c9_u), &c9_i59, 1, 6, 0U, 0, 0U, 0);
  c9_y = c9_i59;
  sf_mex_destroy(&c9_u);
  return c9_y;
}

static void c9_f_sf_marshallIn(void *chartInstanceVoid, const mxArray
  *c9_mxArrayInData, const char_T *c9_varName, void *c9_outData)
{
  const mxArray *c9_b_sfEvent;
  const char_T *c9_identifier;
  emlrtMsgIdentifier c9_thisId;
  int32_T c9_y;
  SFc9_greenOffenceInstanceStruct *chartInstance;
  chartInstance = (SFc9_greenOffenceInstanceStruct *)chartInstanceVoid;
  c9_b_sfEvent = sf_mex_dup(c9_mxArrayInData);
  c9_identifier = c9_varName;
  c9_thisId.fIdentifier = c9_identifier;
  c9_thisId.fParent = NULL;
  c9_y = c9_g_emlrt_marshallIn(chartInstance, sf_mex_dup(c9_b_sfEvent),
    &c9_thisId);
  sf_mex_destroy(&c9_b_sfEvent);
  *(int32_T *)c9_outData = c9_y;
  sf_mex_destroy(&c9_mxArrayInData);
}

static const mxArray *c9_me_bus_io(void *chartInstanceVoid, void *c9_pData)
{
  const mxArray *c9_mxVal = NULL;
  c9_Player c9_tmp;
  SFc9_greenOffenceInstanceStruct *chartInstance;
  chartInstance = (SFc9_greenOffenceInstanceStruct *)chartInstanceVoid;
  c9_mxVal = NULL;
  c9_tmp.x = *(int8_T *)&((char_T *)(c9_Player *)c9_pData)[0];
  c9_tmp.y = *(int8_T *)&((char_T *)(c9_Player *)c9_pData)[1];
  c9_tmp.orientation = *(int16_T *)&((char_T *)(c9_Player *)c9_pData)[2];
  c9_tmp.color = *(uint8_T *)&((char_T *)(c9_Player *)c9_pData)[4];
  c9_tmp.position = *(uint8_T *)&((char_T *)(c9_Player *)c9_pData)[5];
  c9_tmp.valid = *(uint8_T *)&((char_T *)(c9_Player *)c9_pData)[6];
  sf_mex_assign(&c9_mxVal, c9_g_sf_marshallOut(chartInstance, &c9_tmp), false);
  return c9_mxVal;
}

static const mxArray *c9_g_sf_marshallOut(void *chartInstanceVoid, void
  *c9_inData)
{
  const mxArray *c9_mxArrayOutData = NULL;
  c9_Player c9_u;
  const mxArray *c9_y = NULL;
  int8_T c9_b_u;
  const mxArray *c9_b_y = NULL;
  int8_T c9_c_u;
  const mxArray *c9_c_y = NULL;
  int16_T c9_d_u;
  const mxArray *c9_d_y = NULL;
  uint8_T c9_e_u;
  const mxArray *c9_e_y = NULL;
  uint8_T c9_f_u;
  const mxArray *c9_f_y = NULL;
  uint8_T c9_g_u;
  const mxArray *c9_g_y = NULL;
  SFc9_greenOffenceInstanceStruct *chartInstance;
  chartInstance = (SFc9_greenOffenceInstanceStruct *)chartInstanceVoid;
  c9_mxArrayOutData = NULL;
  c9_u = *(c9_Player *)c9_inData;
  c9_y = NULL;
  sf_mex_assign(&c9_y, sf_mex_createstruct("structure", 2, 1, 1), false);
  c9_b_u = c9_u.x;
  c9_b_y = NULL;
  sf_mex_assign(&c9_b_y, sf_mex_create("y", &c9_b_u, 2, 0U, 0U, 0U, 0), false);
  sf_mex_addfield(c9_y, c9_b_y, "x", "x", 0);
  c9_c_u = c9_u.y;
  c9_c_y = NULL;
  sf_mex_assign(&c9_c_y, sf_mex_create("y", &c9_c_u, 2, 0U, 0U, 0U, 0), false);
  sf_mex_addfield(c9_y, c9_c_y, "y", "y", 0);
  c9_d_u = c9_u.orientation;
  c9_d_y = NULL;
  sf_mex_assign(&c9_d_y, sf_mex_create("y", &c9_d_u, 4, 0U, 0U, 0U, 0), false);
  sf_mex_addfield(c9_y, c9_d_y, "orientation", "orientation", 0);
  c9_e_u = c9_u.color;
  c9_e_y = NULL;
  sf_mex_assign(&c9_e_y, sf_mex_create("y", &c9_e_u, 3, 0U, 0U, 0U, 0), false);
  sf_mex_addfield(c9_y, c9_e_y, "color", "color", 0);
  c9_f_u = c9_u.position;
  c9_f_y = NULL;
  sf_mex_assign(&c9_f_y, sf_mex_create("y", &c9_f_u, 3, 0U, 0U, 0U, 0), false);
  sf_mex_addfield(c9_y, c9_f_y, "position", "position", 0);
  c9_g_u = c9_u.valid;
  c9_g_y = NULL;
  sf_mex_assign(&c9_g_y, sf_mex_create("y", &c9_g_u, 3, 0U, 0U, 0U, 0), false);
  sf_mex_addfield(c9_y, c9_g_y, "valid", "valid", 0);
  sf_mex_assign(&c9_mxArrayOutData, c9_y, false);
  return c9_mxArrayOutData;
}

static const mxArray *c9_players_bus_io(void *chartInstanceVoid, void *c9_pData)
{
  const mxArray *c9_mxVal = NULL;
  int32_T c9_i60;
  int32_T c9_i61;
  c9_Player c9_tmp[6];
  SFc9_greenOffenceInstanceStruct *chartInstance;
  chartInstance = (SFc9_greenOffenceInstanceStruct *)chartInstanceVoid;
  c9_mxVal = NULL;
  for (c9_i60 = 0; c9_i60 < 6; c9_i60++) {
    for (c9_i61 = 0; c9_i61 < 1; c9_i61++) {
      c9_tmp[c9_i61 + c9_i60].x = *(int8_T *)&((char_T *)(c9_Player *)&((char_T *)
        (c9_Player (*)[6])c9_pData)[8 * (c9_i61 + c9_i60)])[0];
      c9_tmp[c9_i61 + c9_i60].y = *(int8_T *)&((char_T *)(c9_Player *)&((char_T *)
        (c9_Player (*)[6])c9_pData)[8 * (c9_i61 + c9_i60)])[1];
      c9_tmp[c9_i61 + c9_i60].orientation = *(int16_T *)&((char_T *)(c9_Player *)
        &((char_T *)(c9_Player (*)[6])c9_pData)[8 * (c9_i61 + c9_i60)])[2];
      c9_tmp[c9_i61 + c9_i60].color = *(uint8_T *)&((char_T *)(c9_Player *)
        &((char_T *)(c9_Player (*)[6])c9_pData)[8 * (c9_i61 + c9_i60)])[4];
      c9_tmp[c9_i61 + c9_i60].position = *(uint8_T *)&((char_T *)(c9_Player *)
        &((char_T *)(c9_Player (*)[6])c9_pData)[8 * (c9_i61 + c9_i60)])[5];
      c9_tmp[c9_i61 + c9_i60].valid = *(uint8_T *)&((char_T *)(c9_Player *)
        &((char_T *)(c9_Player (*)[6])c9_pData)[8 * (c9_i61 + c9_i60)])[6];
    }
  }

  sf_mex_assign(&c9_mxVal, c9_h_sf_marshallOut(chartInstance, c9_tmp), false);
  return c9_mxVal;
}

static const mxArray *c9_h_sf_marshallOut(void *chartInstanceVoid, void
  *c9_inData)
{
  const mxArray *c9_mxArrayOutData;
  int32_T c9_i62;
  c9_Player c9_b_inData[6];
  int32_T c9_i63;
  c9_Player c9_u[6];
  const mxArray *c9_y = NULL;
  int32_T c9_i64;
  int32_T c9_iv11[2];
  int32_T c9_i65;
  const c9_Player *c9_r3;
  int8_T c9_b_u;
  const mxArray *c9_b_y = NULL;
  int8_T c9_c_u;
  const mxArray *c9_c_y = NULL;
  int16_T c9_d_u;
  const mxArray *c9_d_y = NULL;
  uint8_T c9_e_u;
  const mxArray *c9_e_y = NULL;
  uint8_T c9_f_u;
  const mxArray *c9_f_y = NULL;
  uint8_T c9_g_u;
  const mxArray *c9_g_y = NULL;
  SFc9_greenOffenceInstanceStruct *chartInstance;
  chartInstance = (SFc9_greenOffenceInstanceStruct *)chartInstanceVoid;
  c9_mxArrayOutData = NULL;
  c9_mxArrayOutData = NULL;
  for (c9_i62 = 0; c9_i62 < 6; c9_i62++) {
    c9_b_inData[c9_i62] = (*(c9_Player (*)[6])c9_inData)[c9_i62];
  }

  for (c9_i63 = 0; c9_i63 < 6; c9_i63++) {
    c9_u[c9_i63] = c9_b_inData[c9_i63];
  }

  c9_y = NULL;
  for (c9_i64 = 0; c9_i64 < 2; c9_i64++) {
    c9_iv11[c9_i64] = 1 + 5 * c9_i64;
  }

  sf_mex_assign(&c9_y, sf_mex_createstructarray("structure", 2, c9_iv11), false);
  for (c9_i65 = 0; c9_i65 < 6; c9_i65++) {
    c9_r3 = &c9_u[c9_i65];
    c9_b_u = c9_r3->x;
    c9_b_y = NULL;
    sf_mex_assign(&c9_b_y, sf_mex_create("y", &c9_b_u, 2, 0U, 0U, 0U, 0), false);
    sf_mex_addfield(c9_y, c9_b_y, "x", "x", c9_i65);
    c9_c_u = c9_r3->y;
    c9_c_y = NULL;
    sf_mex_assign(&c9_c_y, sf_mex_create("y", &c9_c_u, 2, 0U, 0U, 0U, 0), false);
    sf_mex_addfield(c9_y, c9_c_y, "y", "y", c9_i65);
    c9_d_u = c9_r3->orientation;
    c9_d_y = NULL;
    sf_mex_assign(&c9_d_y, sf_mex_create("y", &c9_d_u, 4, 0U, 0U, 0U, 0), false);
    sf_mex_addfield(c9_y, c9_d_y, "orientation", "orientation", c9_i65);
    c9_e_u = c9_r3->color;
    c9_e_y = NULL;
    sf_mex_assign(&c9_e_y, sf_mex_create("y", &c9_e_u, 3, 0U, 0U, 0U, 0), false);
    sf_mex_addfield(c9_y, c9_e_y, "color", "color", c9_i65);
    c9_f_u = c9_r3->position;
    c9_f_y = NULL;
    sf_mex_assign(&c9_f_y, sf_mex_create("y", &c9_f_u, 3, 0U, 0U, 0U, 0), false);
    sf_mex_addfield(c9_y, c9_f_y, "position", "position", c9_i65);
    c9_g_u = c9_r3->valid;
    c9_g_y = NULL;
    sf_mex_assign(&c9_g_y, sf_mex_create("y", &c9_g_u, 3, 0U, 0U, 0U, 0), false);
    sf_mex_addfield(c9_y, c9_g_y, "valid", "valid", c9_i65);
  }

  sf_mex_assign(&c9_mxArrayOutData, c9_y, false);
  return c9_mxArrayOutData;
}

static const mxArray *c9_ball_bus_io(void *chartInstanceVoid, void *c9_pData)
{
  const mxArray *c9_mxVal = NULL;
  c9_Ball c9_tmp;
  SFc9_greenOffenceInstanceStruct *chartInstance;
  chartInstance = (SFc9_greenOffenceInstanceStruct *)chartInstanceVoid;
  c9_mxVal = NULL;
  c9_tmp.x = *(int8_T *)&((char_T *)(c9_Ball *)c9_pData)[0];
  c9_tmp.y = *(int8_T *)&((char_T *)(c9_Ball *)c9_pData)[1];
  c9_tmp.valid = *(uint8_T *)&((char_T *)(c9_Ball *)c9_pData)[2];
  sf_mex_assign(&c9_mxVal, c9_i_sf_marshallOut(chartInstance, &c9_tmp), false);
  return c9_mxVal;
}

static const mxArray *c9_i_sf_marshallOut(void *chartInstanceVoid, void
  *c9_inData)
{
  const mxArray *c9_mxArrayOutData = NULL;
  c9_Ball c9_u;
  const mxArray *c9_y = NULL;
  int8_T c9_b_u;
  const mxArray *c9_b_y = NULL;
  int8_T c9_c_u;
  const mxArray *c9_c_y = NULL;
  uint8_T c9_d_u;
  const mxArray *c9_d_y = NULL;
  SFc9_greenOffenceInstanceStruct *chartInstance;
  chartInstance = (SFc9_greenOffenceInstanceStruct *)chartInstanceVoid;
  c9_mxArrayOutData = NULL;
  c9_u = *(c9_Ball *)c9_inData;
  c9_y = NULL;
  sf_mex_assign(&c9_y, sf_mex_createstruct("structure", 2, 1, 1), false);
  c9_b_u = c9_u.x;
  c9_b_y = NULL;
  sf_mex_assign(&c9_b_y, sf_mex_create("y", &c9_b_u, 2, 0U, 0U, 0U, 0), false);
  sf_mex_addfield(c9_y, c9_b_y, "x", "x", 0);
  c9_c_u = c9_u.y;
  c9_c_y = NULL;
  sf_mex_assign(&c9_c_y, sf_mex_create("y", &c9_c_u, 2, 0U, 0U, 0U, 0), false);
  sf_mex_addfield(c9_y, c9_c_y, "y", "y", 0);
  c9_d_u = c9_u.valid;
  c9_d_y = NULL;
  sf_mex_assign(&c9_d_y, sf_mex_create("y", &c9_d_u, 3, 0U, 0U, 0U, 0), false);
  sf_mex_addfield(c9_y, c9_d_y, "valid", "valid", 0);
  sf_mex_assign(&c9_mxArrayOutData, c9_y, false);
  return c9_mxArrayOutData;
}

static const mxArray *c9_finalWay_bus_io(void *chartInstanceVoid, void *c9_pData)
{
  const mxArray *c9_mxVal = NULL;
  c9_Waypoint c9_tmp;
  SFc9_greenOffenceInstanceStruct *chartInstance;
  chartInstance = (SFc9_greenOffenceInstanceStruct *)chartInstanceVoid;
  c9_mxVal = NULL;
  c9_tmp.x = *(int8_T *)&((char_T *)(c9_Waypoint *)c9_pData)[0];
  c9_tmp.y = *(int8_T *)&((char_T *)(c9_Waypoint *)c9_pData)[1];
  c9_tmp.orientation = *(int16_T *)&((char_T *)(c9_Waypoint *)c9_pData)[2];
  sf_mex_assign(&c9_mxVal, c9_j_sf_marshallOut(chartInstance, &c9_tmp), false);
  return c9_mxVal;
}

static const mxArray *c9_j_sf_marshallOut(void *chartInstanceVoid, void
  *c9_inData)
{
  const mxArray *c9_mxArrayOutData = NULL;
  c9_Waypoint c9_u;
  const mxArray *c9_y = NULL;
  int8_T c9_b_u;
  const mxArray *c9_b_y = NULL;
  int8_T c9_c_u;
  const mxArray *c9_c_y = NULL;
  int16_T c9_d_u;
  const mxArray *c9_d_y = NULL;
  SFc9_greenOffenceInstanceStruct *chartInstance;
  chartInstance = (SFc9_greenOffenceInstanceStruct *)chartInstanceVoid;
  c9_mxArrayOutData = NULL;
  c9_u = *(c9_Waypoint *)c9_inData;
  c9_y = NULL;
  sf_mex_assign(&c9_y, sf_mex_createstruct("structure", 2, 1, 1), false);
  c9_b_u = c9_u.x;
  c9_b_y = NULL;
  sf_mex_assign(&c9_b_y, sf_mex_create("y", &c9_b_u, 2, 0U, 0U, 0U, 0), false);
  sf_mex_addfield(c9_y, c9_b_y, "x", "x", 0);
  c9_c_u = c9_u.y;
  c9_c_y = NULL;
  sf_mex_assign(&c9_c_y, sf_mex_create("y", &c9_c_u, 2, 0U, 0U, 0U, 0), false);
  sf_mex_addfield(c9_y, c9_c_y, "y", "y", 0);
  c9_d_u = c9_u.orientation;
  c9_d_y = NULL;
  sf_mex_assign(&c9_d_y, sf_mex_create("y", &c9_d_u, 4, 0U, 0U, 0U, 0), false);
  sf_mex_addfield(c9_y, c9_d_y, "orientation", "orientation", 0);
  sf_mex_assign(&c9_mxArrayOutData, c9_y, false);
  return c9_mxArrayOutData;
}

static c9_Waypoint c9_h_emlrt_marshallIn(SFc9_greenOffenceInstanceStruct
  *chartInstance, const mxArray *c9_b_finalWay, const char_T *c9_identifier)
{
  c9_Waypoint c9_y;
  emlrtMsgIdentifier c9_thisId;
  c9_thisId.fIdentifier = c9_identifier;
  c9_thisId.fParent = NULL;
  c9_y = c9_i_emlrt_marshallIn(chartInstance, sf_mex_dup(c9_b_finalWay),
    &c9_thisId);
  sf_mex_destroy(&c9_b_finalWay);
  return c9_y;
}

static c9_Waypoint c9_i_emlrt_marshallIn(SFc9_greenOffenceInstanceStruct
  *chartInstance, const mxArray *c9_u, const emlrtMsgIdentifier *c9_parentId)
{
  c9_Waypoint c9_y;
  emlrtMsgIdentifier c9_thisId;
  static const char * c9_fieldNames[3] = { "x", "y", "orientation" };

  c9_thisId.fParent = c9_parentId;
  sf_mex_check_struct(c9_parentId, c9_u, 3, c9_fieldNames, 0U, NULL);
  c9_thisId.fIdentifier = "x";
  c9_y.x = c9_j_emlrt_marshallIn(chartInstance, sf_mex_dup(sf_mex_getfield(c9_u,
    "x", "x", 0)), &c9_thisId);
  c9_thisId.fIdentifier = "y";
  c9_y.y = c9_j_emlrt_marshallIn(chartInstance, sf_mex_dup(sf_mex_getfield(c9_u,
    "y", "y", 0)), &c9_thisId);
  c9_thisId.fIdentifier = "orientation";
  c9_y.orientation = c9_k_emlrt_marshallIn(chartInstance, sf_mex_dup
    (sf_mex_getfield(c9_u, "orientation", "orientation", 0)), &c9_thisId);
  sf_mex_destroy(&c9_u);
  return c9_y;
}

static int8_T c9_j_emlrt_marshallIn(SFc9_greenOffenceInstanceStruct
  *chartInstance, const mxArray *c9_u, const emlrtMsgIdentifier *c9_parentId)
{
  int8_T c9_y;
  int8_T c9_i66;
  (void)chartInstance;
  sf_mex_import(c9_parentId, sf_mex_dup(c9_u), &c9_i66, 1, 2, 0U, 0, 0U, 0);
  c9_y = c9_i66;
  sf_mex_destroy(&c9_u);
  return c9_y;
}

static int16_T c9_k_emlrt_marshallIn(SFc9_greenOffenceInstanceStruct
  *chartInstance, const mxArray *c9_u, const emlrtMsgIdentifier *c9_parentId)
{
  int16_T c9_y;
  int16_T c9_i67;
  (void)chartInstance;
  sf_mex_import(c9_parentId, sf_mex_dup(c9_u), &c9_i67, 1, 4, 0U, 0, 0U, 0);
  c9_y = c9_i67;
  sf_mex_destroy(&c9_u);
  return c9_y;
}

static void c9_g_sf_marshallIn(void *chartInstanceVoid, const mxArray
  *c9_mxArrayInData, const char_T *c9_varName, void *c9_outData)
{
  const mxArray *c9_b_finalWay;
  const char_T *c9_identifier;
  emlrtMsgIdentifier c9_thisId;
  c9_Waypoint c9_y;
  SFc9_greenOffenceInstanceStruct *chartInstance;
  chartInstance = (SFc9_greenOffenceInstanceStruct *)chartInstanceVoid;
  c9_b_finalWay = sf_mex_dup(c9_mxArrayInData);
  c9_identifier = c9_varName;
  c9_thisId.fIdentifier = c9_identifier;
  c9_thisId.fParent = NULL;
  c9_y = c9_i_emlrt_marshallIn(chartInstance, sf_mex_dup(c9_b_finalWay),
    &c9_thisId);
  sf_mex_destroy(&c9_b_finalWay);
  *(c9_Waypoint *)c9_outData = c9_y;
  sf_mex_destroy(&c9_mxArrayInData);
}

static const mxArray *c9_k_sf_marshallOut(void *chartInstanceVoid, void
  *c9_inData)
{
  const mxArray *c9_mxArrayOutData = NULL;
  int32_T c9_i68;
  int8_T c9_b_inData[2];
  int32_T c9_i69;
  int8_T c9_u[2];
  const mxArray *c9_y = NULL;
  SFc9_greenOffenceInstanceStruct *chartInstance;
  chartInstance = (SFc9_greenOffenceInstanceStruct *)chartInstanceVoid;
  c9_mxArrayOutData = NULL;
  for (c9_i68 = 0; c9_i68 < 2; c9_i68++) {
    c9_b_inData[c9_i68] = (*(int8_T (*)[2])c9_inData)[c9_i68];
  }

  for (c9_i69 = 0; c9_i69 < 2; c9_i69++) {
    c9_u[c9_i69] = c9_b_inData[c9_i69];
  }

  c9_y = NULL;
  sf_mex_assign(&c9_y, sf_mex_create("y", c9_u, 2, 0U, 1U, 0U, 2, 2, 1), false);
  sf_mex_assign(&c9_mxArrayOutData, c9_y, false);
  return c9_mxArrayOutData;
}

static void c9_l_emlrt_marshallIn(SFc9_greenOffenceInstanceStruct *chartInstance,
  const mxArray *c9_b_startingPos, const char_T *c9_identifier, int8_T c9_y[2])
{
  emlrtMsgIdentifier c9_thisId;
  c9_thisId.fIdentifier = c9_identifier;
  c9_thisId.fParent = NULL;
  c9_m_emlrt_marshallIn(chartInstance, sf_mex_dup(c9_b_startingPos), &c9_thisId,
                        c9_y);
  sf_mex_destroy(&c9_b_startingPos);
}

static void c9_m_emlrt_marshallIn(SFc9_greenOffenceInstanceStruct *chartInstance,
  const mxArray *c9_u, const emlrtMsgIdentifier *c9_parentId, int8_T c9_y[2])
{
  int8_T c9_iv12[2];
  int32_T c9_i70;
  (void)chartInstance;
  sf_mex_import(c9_parentId, sf_mex_dup(c9_u), c9_iv12, 1, 2, 0U, 1, 0U, 2, 2, 1);
  for (c9_i70 = 0; c9_i70 < 2; c9_i70++) {
    c9_y[c9_i70] = c9_iv12[c9_i70];
  }

  sf_mex_destroy(&c9_u);
}

static void c9_h_sf_marshallIn(void *chartInstanceVoid, const mxArray
  *c9_mxArrayInData, const char_T *c9_varName, void *c9_outData)
{
  const mxArray *c9_b_startingPos;
  const char_T *c9_identifier;
  emlrtMsgIdentifier c9_thisId;
  int8_T c9_y[2];
  int32_T c9_i71;
  SFc9_greenOffenceInstanceStruct *chartInstance;
  chartInstance = (SFc9_greenOffenceInstanceStruct *)chartInstanceVoid;
  c9_b_startingPos = sf_mex_dup(c9_mxArrayInData);
  c9_identifier = c9_varName;
  c9_thisId.fIdentifier = c9_identifier;
  c9_thisId.fParent = NULL;
  c9_m_emlrt_marshallIn(chartInstance, sf_mex_dup(c9_b_startingPos), &c9_thisId,
                        c9_y);
  sf_mex_destroy(&c9_b_startingPos);
  for (c9_i71 = 0; c9_i71 < 2; c9_i71++) {
    (*(int8_T (*)[2])c9_outData)[c9_i71] = c9_y[c9_i71];
  }

  sf_mex_destroy(&c9_mxArrayInData);
}

static const mxArray *c9_l_sf_marshallOut(void *chartInstanceVoid, void
  *c9_inData)
{
  const mxArray *c9_mxArrayOutData;
  int32_T c9_i72;
  c9_Player c9_b_inData[3];
  int32_T c9_i73;
  c9_Player c9_u[3];
  const mxArray *c9_y = NULL;
  static int32_T c9_iv13[1] = { 3 };

  int32_T c9_iv14[1];
  int32_T c9_i74;
  const c9_Player *c9_r4;
  int8_T c9_b_u;
  const mxArray *c9_b_y = NULL;
  int8_T c9_c_u;
  const mxArray *c9_c_y = NULL;
  int16_T c9_d_u;
  const mxArray *c9_d_y = NULL;
  uint8_T c9_e_u;
  const mxArray *c9_e_y = NULL;
  uint8_T c9_f_u;
  const mxArray *c9_f_y = NULL;
  uint8_T c9_g_u;
  const mxArray *c9_g_y = NULL;
  SFc9_greenOffenceInstanceStruct *chartInstance;
  chartInstance = (SFc9_greenOffenceInstanceStruct *)chartInstanceVoid;
  c9_mxArrayOutData = NULL;
  c9_mxArrayOutData = NULL;
  for (c9_i72 = 0; c9_i72 < 3; c9_i72++) {
    c9_b_inData[c9_i72] = (*(c9_Player (*)[3])c9_inData)[c9_i72];
  }

  for (c9_i73 = 0; c9_i73 < 3; c9_i73++) {
    c9_u[c9_i73] = c9_b_inData[c9_i73];
  }

  c9_y = NULL;
  c9_iv14[0] = c9_iv13[0];
  sf_mex_assign(&c9_y, sf_mex_createstructarray("structure", 1, c9_iv14), false);
  for (c9_i74 = 0; c9_i74 < 3; c9_i74++) {
    c9_r4 = &c9_u[c9_i74];
    c9_b_u = c9_r4->x;
    c9_b_y = NULL;
    sf_mex_assign(&c9_b_y, sf_mex_create("y", &c9_b_u, 2, 0U, 0U, 0U, 0), false);
    sf_mex_addfield(c9_y, c9_b_y, "x", "x", c9_i74);
    c9_c_u = c9_r4->y;
    c9_c_y = NULL;
    sf_mex_assign(&c9_c_y, sf_mex_create("y", &c9_c_u, 2, 0U, 0U, 0U, 0), false);
    sf_mex_addfield(c9_y, c9_c_y, "y", "y", c9_i74);
    c9_d_u = c9_r4->orientation;
    c9_d_y = NULL;
    sf_mex_assign(&c9_d_y, sf_mex_create("y", &c9_d_u, 4, 0U, 0U, 0U, 0), false);
    sf_mex_addfield(c9_y, c9_d_y, "orientation", "orientation", c9_i74);
    c9_e_u = c9_r4->color;
    c9_e_y = NULL;
    sf_mex_assign(&c9_e_y, sf_mex_create("y", &c9_e_u, 3, 0U, 0U, 0U, 0), false);
    sf_mex_addfield(c9_y, c9_e_y, "color", "color", c9_i74);
    c9_f_u = c9_r4->position;
    c9_f_y = NULL;
    sf_mex_assign(&c9_f_y, sf_mex_create("y", &c9_f_u, 3, 0U, 0U, 0U, 0), false);
    sf_mex_addfield(c9_y, c9_f_y, "position", "position", c9_i74);
    c9_g_u = c9_r4->valid;
    c9_g_y = NULL;
    sf_mex_assign(&c9_g_y, sf_mex_create("y", &c9_g_u, 3, 0U, 0U, 0U, 0), false);
    sf_mex_addfield(c9_y, c9_g_y, "valid", "valid", c9_i74);
  }

  sf_mex_assign(&c9_mxArrayOutData, c9_y, false);
  return c9_mxArrayOutData;
}

static void c9_n_emlrt_marshallIn(SFc9_greenOffenceInstanceStruct *chartInstance,
  const mxArray *c9_b_enemy, const char_T *c9_identifier, c9_Player c9_y[3])
{
  emlrtMsgIdentifier c9_thisId;
  c9_thisId.fIdentifier = c9_identifier;
  c9_thisId.fParent = NULL;
  c9_o_emlrt_marshallIn(chartInstance, sf_mex_dup(c9_b_enemy), &c9_thisId, c9_y);
  sf_mex_destroy(&c9_b_enemy);
}

static void c9_o_emlrt_marshallIn(SFc9_greenOffenceInstanceStruct *chartInstance,
  const mxArray *c9_u, const emlrtMsgIdentifier *c9_parentId, c9_Player c9_y[3])
{
  static uint32_T c9_uv0[1] = { 3U };

  uint32_T c9_uv1[1];
  emlrtMsgIdentifier c9_thisId;
  static const char * c9_fieldNames[6] = { "x", "y", "orientation", "color",
    "position", "valid" };

  c9_Player (*c9_r5)[3];
  int32_T c9_i75;
  c9_uv1[0] = c9_uv0[0];
  c9_thisId.fParent = c9_parentId;
  sf_mex_check_struct(c9_parentId, c9_u, 6, c9_fieldNames, 1U, c9_uv1);
  c9_r5 = (c9_Player (*)[3])c9_y;
  for (c9_i75 = 0; c9_i75 < 3; c9_i75++) {
    c9_thisId.fIdentifier = "x";
    (*c9_r5)[c9_i75].x = c9_j_emlrt_marshallIn(chartInstance, sf_mex_dup
      (sf_mex_getfield(c9_u, "x", "x", c9_i75)), &c9_thisId);
    c9_thisId.fIdentifier = "y";
    (*c9_r5)[c9_i75].y = c9_j_emlrt_marshallIn(chartInstance, sf_mex_dup
      (sf_mex_getfield(c9_u, "y", "y", c9_i75)), &c9_thisId);
    c9_thisId.fIdentifier = "orientation";
    (*c9_r5)[c9_i75].orientation = c9_k_emlrt_marshallIn(chartInstance,
      sf_mex_dup(sf_mex_getfield(c9_u, "orientation", "orientation", c9_i75)),
      &c9_thisId);
    c9_thisId.fIdentifier = "color";
    (*c9_r5)[c9_i75].color = c9_e_emlrt_marshallIn(chartInstance, sf_mex_dup
      (sf_mex_getfield(c9_u, "color", "color", c9_i75)), &c9_thisId);
    c9_thisId.fIdentifier = "position";
    (*c9_r5)[c9_i75].position = c9_e_emlrt_marshallIn(chartInstance, sf_mex_dup
      (sf_mex_getfield(c9_u, "position", "position", c9_i75)), &c9_thisId);
    c9_thisId.fIdentifier = "valid";
    (*c9_r5)[c9_i75].valid = c9_e_emlrt_marshallIn(chartInstance, sf_mex_dup
      (sf_mex_getfield(c9_u, "valid", "valid", c9_i75)), &c9_thisId);
  }

  sf_mex_destroy(&c9_u);
}

static void c9_i_sf_marshallIn(void *chartInstanceVoid, const mxArray
  *c9_mxArrayInData, const char_T *c9_varName, void *c9_outData)
{
  const mxArray *c9_b_enemy;
  const char_T *c9_identifier;
  emlrtMsgIdentifier c9_thisId;
  c9_Player c9_y[3];
  int32_T c9_i76;
  SFc9_greenOffenceInstanceStruct *chartInstance;
  chartInstance = (SFc9_greenOffenceInstanceStruct *)chartInstanceVoid;
  c9_b_enemy = sf_mex_dup(c9_mxArrayInData);
  c9_identifier = c9_varName;
  c9_thisId.fIdentifier = c9_identifier;
  c9_thisId.fParent = NULL;
  c9_o_emlrt_marshallIn(chartInstance, sf_mex_dup(c9_b_enemy), &c9_thisId, c9_y);
  sf_mex_destroy(&c9_b_enemy);
  for (c9_i76 = 0; c9_i76 < 3; c9_i76++) {
    (*(c9_Player (*)[3])c9_outData)[c9_i76] = c9_y[c9_i76];
  }

  sf_mex_destroy(&c9_mxArrayInData);
}

static void c9_p_emlrt_marshallIn(SFc9_greenOffenceInstanceStruct *chartInstance,
  const mxArray *c9_b_dataWrittenToVector, const char_T *c9_identifier,
  boolean_T c9_y[8])
{
  emlrtMsgIdentifier c9_thisId;
  c9_thisId.fIdentifier = c9_identifier;
  c9_thisId.fParent = NULL;
  c9_q_emlrt_marshallIn(chartInstance, sf_mex_dup(c9_b_dataWrittenToVector),
                        &c9_thisId, c9_y);
  sf_mex_destroy(&c9_b_dataWrittenToVector);
}

static void c9_q_emlrt_marshallIn(SFc9_greenOffenceInstanceStruct *chartInstance,
  const mxArray *c9_u, const emlrtMsgIdentifier *c9_parentId, boolean_T c9_y[8])
{
  boolean_T c9_bv1[8];
  int32_T c9_i77;
  (void)chartInstance;
  sf_mex_import(c9_parentId, sf_mex_dup(c9_u), c9_bv1, 1, 11, 0U, 1, 0U, 1, 8);
  for (c9_i77 = 0; c9_i77 < 8; c9_i77++) {
    c9_y[c9_i77] = c9_bv1[c9_i77];
  }

  sf_mex_destroy(&c9_u);
}

static const mxArray *c9_r_emlrt_marshallIn(SFc9_greenOffenceInstanceStruct
  *chartInstance, const mxArray *c9_b_setSimStateSideEffectsInfo, const char_T
  *c9_identifier)
{
  const mxArray *c9_y = NULL;
  emlrtMsgIdentifier c9_thisId;
  c9_y = NULL;
  c9_thisId.fIdentifier = c9_identifier;
  c9_thisId.fParent = NULL;
  sf_mex_assign(&c9_y, c9_s_emlrt_marshallIn(chartInstance, sf_mex_dup
    (c9_b_setSimStateSideEffectsInfo), &c9_thisId), false);
  sf_mex_destroy(&c9_b_setSimStateSideEffectsInfo);
  return c9_y;
}

static const mxArray *c9_s_emlrt_marshallIn(SFc9_greenOffenceInstanceStruct
  *chartInstance, const mxArray *c9_u, const emlrtMsgIdentifier *c9_parentId)
{
  const mxArray *c9_y = NULL;
  (void)chartInstance;
  (void)c9_parentId;
  c9_y = NULL;
  sf_mex_assign(&c9_y, sf_mex_duplicatearraysafe(&c9_u), false);
  sf_mex_destroy(&c9_u);
  return c9_y;
}

static void c9_updateDataWrittenToVector(SFc9_greenOffenceInstanceStruct
  *chartInstance, uint32_T c9_vectorIndex)
{
  chartInstance->c9_dataWrittenToVector[(uint32_T)_SFD_EML_ARRAY_BOUNDS_CHECK(0U,
    (int32_T)c9_vectorIndex, 0, 7, 1, 0)] = true;
}

static void c9_errorIfDataNotWrittenToFcn(SFc9_greenOffenceInstanceStruct
  *chartInstance, uint32_T c9_vectorIndex, uint32_T c9_dataNumber, uint32_T
  c9_ssIdOfSourceObject, int32_T c9_offsetInSourceObject, int32_T
  c9_lengthInSourceObject)
{
  (void)c9_ssIdOfSourceObject;
  (void)c9_offsetInSourceObject;
  (void)c9_lengthInSourceObject;
  if (!chartInstance->c9_dataWrittenToVector[(uint32_T)
      _SFD_EML_ARRAY_BOUNDS_CHECK(0U, (int32_T)c9_vectorIndex, 0, 7, 1, 0)]) {
    _SFD_DATA_READ_BEFORE_WRITE_ERROR(c9_dataNumber);
  }
}

static void init_dsm_address_info(SFc9_greenOffenceInstanceStruct *chartInstance)
{
  (void)chartInstance;
}

static void init_simulink_io_address(SFc9_greenOffenceInstanceStruct
  *chartInstance)
{
  chartInstance->c9_me = (c9_Player *)ssGetInputPortSignal_wrapper
    (chartInstance->S, 0);
  chartInstance->c9_players = (c9_Player (*)[6])ssGetInputPortSignal_wrapper
    (chartInstance->S, 1);
  chartInstance->c9_ball = (c9_Ball *)ssGetInputPortSignal_wrapper
    (chartInstance->S, 2);
  chartInstance->c9_finalWay = (c9_Waypoint *)ssGetOutputPortSignal_wrapper
    (chartInstance->S, 1);
  chartInstance->c9_manualWay = (c9_Waypoint *)ssGetInputPortSignal_wrapper
    (chartInstance->S, 3);
  chartInstance->c9_GameOn = (uint8_T *)ssGetInputPortSignal_wrapper
    (chartInstance->S, 4);
  chartInstance->c9_shoot = (uint8_T *)ssGetOutputPortSignal_wrapper
    (chartInstance->S, 2);
}

/* SFunction Glue Code */
#ifdef utFree
#undef utFree
#endif

#ifdef utMalloc
#undef utMalloc
#endif

#ifdef __cplusplus

extern "C" void *utMalloc(size_t size);
extern "C" void utFree(void*);

#else

extern void *utMalloc(size_t size);
extern void utFree(void*);

#endif

void sf_c9_greenOffence_get_check_sum(mxArray *plhs[])
{
  ((real_T *)mxGetPr((plhs[0])))[0] = (real_T)(1895348485U);
  ((real_T *)mxGetPr((plhs[0])))[1] = (real_T)(401212989U);
  ((real_T *)mxGetPr((plhs[0])))[2] = (real_T)(2549146016U);
  ((real_T *)mxGetPr((plhs[0])))[3] = (real_T)(621247149U);
}

mxArray* sf_c9_greenOffence_get_post_codegen_info(void);
mxArray *sf_c9_greenOffence_get_autoinheritance_info(void)
{
  const char *autoinheritanceFields[] = { "checksum", "inputs", "parameters",
    "outputs", "locals", "postCodegenInfo" };

  mxArray *mxAutoinheritanceInfo = mxCreateStructMatrix(1, 1, sizeof
    (autoinheritanceFields)/sizeof(autoinheritanceFields[0]),
    autoinheritanceFields);

  {
    mxArray *mxChecksum = mxCreateString("GBuTtYKxyARBCmkimosN");
    mxSetField(mxAutoinheritanceInfo,0,"checksum",mxChecksum);
  }

  {
    const char *dataFields[] = { "size", "type", "complexity" };

    mxArray *mxData = mxCreateStructMatrix(1,5,3,dataFields);

    {
      mxArray *mxSize = mxCreateDoubleMatrix(1,2,mxREAL);
      double *pr = mxGetPr(mxSize);
      pr[0] = (double)(1);
      pr[1] = (double)(1);
      mxSetField(mxData,0,"size",mxSize);
    }

    {
      const char *typeFields[] = { "base", "fixpt" };

      mxArray *mxType = mxCreateStructMatrix(1,1,2,typeFields);
      mxSetField(mxType,0,"base",mxCreateDoubleScalar(13));
      mxSetField(mxType,0,"fixpt",mxCreateDoubleMatrix(0,0,mxREAL));
      mxSetField(mxData,0,"type",mxType);
    }

    mxSetField(mxData,0,"complexity",mxCreateDoubleScalar(0));

    {
      mxArray *mxSize = mxCreateDoubleMatrix(1,2,mxREAL);
      double *pr = mxGetPr(mxSize);
      pr[0] = (double)(1);
      pr[1] = (double)(6);
      mxSetField(mxData,1,"size",mxSize);
    }

    {
      const char *typeFields[] = { "base", "fixpt" };

      mxArray *mxType = mxCreateStructMatrix(1,1,2,typeFields);
      mxSetField(mxType,0,"base",mxCreateDoubleScalar(13));
      mxSetField(mxType,0,"fixpt",mxCreateDoubleMatrix(0,0,mxREAL));
      mxSetField(mxData,1,"type",mxType);
    }

    mxSetField(mxData,1,"complexity",mxCreateDoubleScalar(0));

    {
      mxArray *mxSize = mxCreateDoubleMatrix(1,2,mxREAL);
      double *pr = mxGetPr(mxSize);
      pr[0] = (double)(1);
      pr[1] = (double)(1);
      mxSetField(mxData,2,"size",mxSize);
    }

    {
      const char *typeFields[] = { "base", "fixpt" };

      mxArray *mxType = mxCreateStructMatrix(1,1,2,typeFields);
      mxSetField(mxType,0,"base",mxCreateDoubleScalar(13));
      mxSetField(mxType,0,"fixpt",mxCreateDoubleMatrix(0,0,mxREAL));
      mxSetField(mxData,2,"type",mxType);
    }

    mxSetField(mxData,2,"complexity",mxCreateDoubleScalar(0));

    {
      mxArray *mxSize = mxCreateDoubleMatrix(1,2,mxREAL);
      double *pr = mxGetPr(mxSize);
      pr[0] = (double)(1);
      pr[1] = (double)(1);
      mxSetField(mxData,3,"size",mxSize);
    }

    {
      const char *typeFields[] = { "base", "fixpt" };

      mxArray *mxType = mxCreateStructMatrix(1,1,2,typeFields);
      mxSetField(mxType,0,"base",mxCreateDoubleScalar(13));
      mxSetField(mxType,0,"fixpt",mxCreateDoubleMatrix(0,0,mxREAL));
      mxSetField(mxData,3,"type",mxType);
    }

    mxSetField(mxData,3,"complexity",mxCreateDoubleScalar(0));

    {
      mxArray *mxSize = mxCreateDoubleMatrix(1,2,mxREAL);
      double *pr = mxGetPr(mxSize);
      pr[0] = (double)(1);
      pr[1] = (double)(1);
      mxSetField(mxData,4,"size",mxSize);
    }

    {
      const char *typeFields[] = { "base", "fixpt" };

      mxArray *mxType = mxCreateStructMatrix(1,1,2,typeFields);
      mxSetField(mxType,0,"base",mxCreateDoubleScalar(3));
      mxSetField(mxType,0,"fixpt",mxCreateDoubleMatrix(0,0,mxREAL));
      mxSetField(mxData,4,"type",mxType);
    }

    mxSetField(mxData,4,"complexity",mxCreateDoubleScalar(0));
    mxSetField(mxAutoinheritanceInfo,0,"inputs",mxData);
  }

  {
    mxSetField(mxAutoinheritanceInfo,0,"parameters",mxCreateDoubleMatrix(0,0,
                mxREAL));
  }

  {
    const char *dataFields[] = { "size", "type", "complexity" };

    mxArray *mxData = mxCreateStructMatrix(1,2,3,dataFields);

    {
      mxArray *mxSize = mxCreateDoubleMatrix(1,2,mxREAL);
      double *pr = mxGetPr(mxSize);
      pr[0] = (double)(1);
      pr[1] = (double)(1);
      mxSetField(mxData,0,"size",mxSize);
    }

    {
      const char *typeFields[] = { "base", "fixpt" };

      mxArray *mxType = mxCreateStructMatrix(1,1,2,typeFields);
      mxSetField(mxType,0,"base",mxCreateDoubleScalar(13));
      mxSetField(mxType,0,"fixpt",mxCreateDoubleMatrix(0,0,mxREAL));
      mxSetField(mxData,0,"type",mxType);
    }

    mxSetField(mxData,0,"complexity",mxCreateDoubleScalar(0));

    {
      mxArray *mxSize = mxCreateDoubleMatrix(1,2,mxREAL);
      double *pr = mxGetPr(mxSize);
      pr[0] = (double)(1);
      pr[1] = (double)(1);
      mxSetField(mxData,1,"size",mxSize);
    }

    {
      const char *typeFields[] = { "base", "fixpt" };

      mxArray *mxType = mxCreateStructMatrix(1,1,2,typeFields);
      mxSetField(mxType,0,"base",mxCreateDoubleScalar(3));
      mxSetField(mxType,0,"fixpt",mxCreateDoubleMatrix(0,0,mxREAL));
      mxSetField(mxData,1,"type",mxType);
    }

    mxSetField(mxData,1,"complexity",mxCreateDoubleScalar(0));
    mxSetField(mxAutoinheritanceInfo,0,"outputs",mxData);
  }

  {
    const char *dataFields[] = { "size", "type", "complexity" };

    mxArray *mxData = mxCreateStructMatrix(1,5,3,dataFields);

    {
      mxArray *mxSize = mxCreateDoubleMatrix(1,2,mxREAL);
      double *pr = mxGetPr(mxSize);
      pr[0] = (double)(2);
      pr[1] = (double)(1);
      mxSetField(mxData,0,"size",mxSize);
    }

    {
      const char *typeFields[] = { "base", "fixpt" };

      mxArray *mxType = mxCreateStructMatrix(1,1,2,typeFields);
      mxSetField(mxType,0,"base",mxCreateDoubleScalar(4));
      mxSetField(mxType,0,"fixpt",mxCreateDoubleMatrix(0,0,mxREAL));
      mxSetField(mxData,0,"type",mxType);
    }

    mxSetField(mxData,0,"complexity",mxCreateDoubleScalar(0));

    {
      mxArray *mxSize = mxCreateDoubleMatrix(1,2,mxREAL);
      double *pr = mxGetPr(mxSize);
      pr[0] = (double)(3);
      pr[1] = (double)(1);
      mxSetField(mxData,1,"size",mxSize);
    }

    {
      const char *typeFields[] = { "base", "fixpt" };

      mxArray *mxType = mxCreateStructMatrix(1,1,2,typeFields);
      mxSetField(mxType,0,"base",mxCreateDoubleScalar(13));
      mxSetField(mxType,0,"fixpt",mxCreateDoubleMatrix(0,0,mxREAL));
      mxSetField(mxData,1,"type",mxType);
    }

    mxSetField(mxData,1,"complexity",mxCreateDoubleScalar(0));

    {
      mxArray *mxSize = mxCreateDoubleMatrix(1,2,mxREAL);
      double *pr = mxGetPr(mxSize);
      pr[0] = (double)(3);
      pr[1] = (double)(1);
      mxSetField(mxData,2,"size",mxSize);
    }

    {
      const char *typeFields[] = { "base", "fixpt" };

      mxArray *mxType = mxCreateStructMatrix(1,1,2,typeFields);
      mxSetField(mxType,0,"base",mxCreateDoubleScalar(13));
      mxSetField(mxType,0,"fixpt",mxCreateDoubleMatrix(0,0,mxREAL));
      mxSetField(mxData,2,"type",mxType);
    }

    mxSetField(mxData,2,"complexity",mxCreateDoubleScalar(0));

    {
      mxArray *mxSize = mxCreateDoubleMatrix(1,2,mxREAL);
      double *pr = mxGetPr(mxSize);
      pr[0] = (double)(1);
      pr[1] = (double)(1);
      mxSetField(mxData,3,"size",mxSize);
    }

    {
      const char *typeFields[] = { "base", "fixpt" };

      mxArray *mxType = mxCreateStructMatrix(1,1,2,typeFields);
      mxSetField(mxType,0,"base",mxCreateDoubleScalar(13));
      mxSetField(mxType,0,"fixpt",mxCreateDoubleMatrix(0,0,mxREAL));
      mxSetField(mxData,3,"type",mxType);
    }

    mxSetField(mxData,3,"complexity",mxCreateDoubleScalar(0));

    {
      mxArray *mxSize = mxCreateDoubleMatrix(1,2,mxREAL);
      double *pr = mxGetPr(mxSize);
      pr[0] = (double)(1);
      pr[1] = (double)(1);
      mxSetField(mxData,4,"size",mxSize);
    }

    {
      const char *typeFields[] = { "base", "fixpt" };

      mxArray *mxType = mxCreateStructMatrix(1,1,2,typeFields);
      mxSetField(mxType,0,"base",mxCreateDoubleScalar(13));
      mxSetField(mxType,0,"fixpt",mxCreateDoubleMatrix(0,0,mxREAL));
      mxSetField(mxData,4,"type",mxType);
    }

    mxSetField(mxData,4,"complexity",mxCreateDoubleScalar(0));
    mxSetField(mxAutoinheritanceInfo,0,"locals",mxData);
  }

  {
    mxArray* mxPostCodegenInfo = sf_c9_greenOffence_get_post_codegen_info();
    mxSetField(mxAutoinheritanceInfo,0,"postCodegenInfo",mxPostCodegenInfo);
  }

  return(mxAutoinheritanceInfo);
}

mxArray *sf_c9_greenOffence_third_party_uses_info(void)
{
  mxArray * mxcell3p = mxCreateCellMatrix(1,0);
  return(mxcell3p);
}

mxArray *sf_c9_greenOffence_jit_fallback_info(void)
{
  const char *infoFields[] = { "fallbackType", "fallbackReason",
    "incompatibleSymbol", };

  mxArray *mxInfo = mxCreateStructMatrix(1, 1, 3, infoFields);
  mxArray *fallbackReason = mxCreateString("feature_off");
  mxArray *incompatibleSymbol = mxCreateString("");
  mxArray *fallbackType = mxCreateString("early");
  mxSetField(mxInfo, 0, infoFields[0], fallbackType);
  mxSetField(mxInfo, 0, infoFields[1], fallbackReason);
  mxSetField(mxInfo, 0, infoFields[2], incompatibleSymbol);
  return mxInfo;
}

mxArray *sf_c9_greenOffence_updateBuildInfo_args_info(void)
{
  mxArray *mxBIArgs = mxCreateCellMatrix(1,0);
  return mxBIArgs;
}

mxArray* sf_c9_greenOffence_get_post_codegen_info(void)
{
  const char* fieldNames[] = { "exportedFunctionsUsedByThisChart",
    "exportedFunctionsChecksum" };

  mwSize dims[2] = { 1, 1 };

  mxArray* mxPostCodegenInfo = mxCreateStructArray(2, dims, sizeof(fieldNames)/
    sizeof(fieldNames[0]), fieldNames);

  {
    mxArray* mxExportedFunctionsChecksum = mxCreateString("");
    mwSize exp_dims[2] = { 0, 1 };

    mxArray* mxExportedFunctionsUsedByThisChart = mxCreateCellArray(2, exp_dims);
    mxSetField(mxPostCodegenInfo, 0, "exportedFunctionsUsedByThisChart",
               mxExportedFunctionsUsedByThisChart);
    mxSetField(mxPostCodegenInfo, 0, "exportedFunctionsChecksum",
               mxExportedFunctionsChecksum);
  }

  return mxPostCodegenInfo;
}

static const mxArray *sf_get_sim_state_info_c9_greenOffence(void)
{
  const char *infoFields[] = { "chartChecksum", "varInfo" };

  mxArray *mxInfo = mxCreateStructMatrix(1, 1, 2, infoFields);
  const char *infoEncStr[] = {
    "100 S1x10'type','srcId','name','auxInfo'{{M[1],M[27],T\"finalWay\",},{M[1],M[132],T\"shoot\",},{M[3],M[113],T\"enemy\",},{M[3],M[114],T\"friends\",},{M[3],M[125],T\"kickWay\",},{M[3],M[124],T\"shootWay\",},{M[3],M[98],T\"startingPos\",},{M[8],M[0],T\"is_active_c9_greenOffence\",},{M[9],M[0],T\"is_c9_greenOffence\",},{M[9],M[17],T\"is_GameIsOn_Offence\",}}",
    "100 S1x3'type','srcId','name','auxInfo'{{M[9],M[88],T\"is_waiting\",},{M[11],M[0],T\"temporalCounter_i1\",S'et','os','ct'{{T\"wu\",M1x2[105 118],M[1]}}},{M[15],M[0],T\"dataWrittenToVector\",}}"
  };

  mxArray *mxVarInfo = sf_mex_decode_encoded_mx_struct_array(infoEncStr, 13, 10);
  mxArray *mxChecksum = mxCreateDoubleMatrix(1, 4, mxREAL);
  sf_c9_greenOffence_get_check_sum(&mxChecksum);
  mxSetField(mxInfo, 0, infoFields[0], mxChecksum);
  mxSetField(mxInfo, 0, infoFields[1], mxVarInfo);
  return mxInfo;
}

static void chart_debug_initialization(SimStruct *S, unsigned int
  fullDebuggerInitialization)
{
  if (!sim_mode_is_rtw_gen(S)) {
    SFc9_greenOffenceInstanceStruct *chartInstance;
    ChartRunTimeInfo * crtInfo = (ChartRunTimeInfo *)(ssGetUserData(S));
    ChartInfoStruct * chartInfo = (ChartInfoStruct *)(crtInfo->instanceInfo);
    chartInstance = (SFc9_greenOffenceInstanceStruct *) chartInfo->chartInstance;
    if (ssIsFirstInitCond(S) && fullDebuggerInitialization==1) {
      /* do this only if simulation is starting */
      {
        unsigned int chartAlreadyPresent;
        chartAlreadyPresent = sf_debug_initialize_chart
          (sfGlobalDebugInstanceStruct,
           _greenOffenceMachineNumber_,
           9,
           11,
           9,
           0,
           15,
           0,
           0,
           0,
           0,
           0,
           &(chartInstance->chartNumber),
           &(chartInstance->instanceNumber),
           (void *)S);

        /* Each instance must initialize its own list of scripts */
        init_script_number_translation(_greenOffenceMachineNumber_,
          chartInstance->chartNumber,chartInstance->instanceNumber);
        if (chartAlreadyPresent==0) {
          /* this is the first instance */
          sf_debug_set_chart_disable_implicit_casting
            (sfGlobalDebugInstanceStruct,_greenOffenceMachineNumber_,
             chartInstance->chartNumber,1);
          sf_debug_set_chart_event_thresholds(sfGlobalDebugInstanceStruct,
            _greenOffenceMachineNumber_,
            chartInstance->chartNumber,
            0,
            0,
            0);
          _SFD_SET_DATA_PROPS(0,1,1,0,"me");
          _SFD_SET_DATA_PROPS(1,1,1,0,"players");
          _SFD_SET_DATA_PROPS(2,1,1,0,"ball");
          _SFD_SET_DATA_PROPS(3,2,0,1,"finalWay");
          _SFD_SET_DATA_PROPS(4,1,1,0,"manualWay");
          _SFD_SET_DATA_PROPS(5,0,0,0,"startingPos");
          _SFD_SET_DATA_PROPS(6,1,1,0,"GameOn");
          _SFD_SET_DATA_PROPS(7,0,0,0,"enemy");
          _SFD_SET_DATA_PROPS(8,0,0,0,"friends");
          _SFD_SET_DATA_PROPS(9,0,0,0,"shootWay");
          _SFD_SET_DATA_PROPS(10,0,0,0,"kickWay");
          _SFD_SET_DATA_PROPS(11,2,0,1,"shoot");
          _SFD_SET_DATA_PROPS(12,8,0,0,"");
          _SFD_SET_DATA_PROPS(13,8,0,0,"");
          _SFD_SET_DATA_PROPS(14,9,0,0,"");
          _SFD_STATE_INFO(0,0,0);
          _SFD_STATE_INFO(1,0,0);
          _SFD_STATE_INFO(2,0,0);
          _SFD_STATE_INFO(3,0,0);
          _SFD_STATE_INFO(4,0,0);
          _SFD_STATE_INFO(9,0,0);
          _SFD_STATE_INFO(10,0,0);
          _SFD_STATE_INFO(5,0,2);
          _SFD_STATE_INFO(6,0,2);
          _SFD_STATE_INFO(7,0,2);
          _SFD_STATE_INFO(8,0,2);
          _SFD_CH_SUBSTATE_COUNT(2);
          _SFD_CH_SUBSTATE_DECOMP(0);
          _SFD_CH_SUBSTATE_INDEX(0,0);
          _SFD_CH_SUBSTATE_INDEX(1,9);
          _SFD_ST_SUBSTATE_COUNT(0,4);
          _SFD_ST_SUBSTATE_INDEX(0,0,1);
          _SFD_ST_SUBSTATE_INDEX(0,1,2);
          _SFD_ST_SUBSTATE_INDEX(0,2,3);
          _SFD_ST_SUBSTATE_INDEX(0,3,4);
          _SFD_ST_SUBSTATE_COUNT(1,0);
          _SFD_ST_SUBSTATE_COUNT(2,0);
          _SFD_ST_SUBSTATE_COUNT(3,0);
          _SFD_ST_SUBSTATE_COUNT(4,0);
          _SFD_ST_SUBSTATE_COUNT(9,1);
          _SFD_ST_SUBSTATE_INDEX(9,0,10);
          _SFD_ST_SUBSTATE_COUNT(10,0);
        }

        _SFD_CV_INIT_CHART(2,1,0,0);

        {
          _SFD_CV_INIT_STATE(0,4,1,1,0,0,NULL,NULL);
        }

        {
          _SFD_CV_INIT_STATE(1,0,0,0,0,0,NULL,NULL);
        }

        {
          _SFD_CV_INIT_STATE(2,0,0,0,0,0,NULL,NULL);
        }

        {
          _SFD_CV_INIT_STATE(3,0,0,0,0,0,NULL,NULL);
        }

        {
          _SFD_CV_INIT_STATE(4,0,0,0,0,0,NULL,NULL);
        }

        {
          _SFD_CV_INIT_STATE(9,1,0,0,0,0,NULL,NULL);
        }

        {
          _SFD_CV_INIT_STATE(10,0,0,0,0,0,NULL,NULL);
        }

        {
          _SFD_CV_INIT_STATE(5,0,0,0,0,0,NULL,NULL);
        }

        {
          _SFD_CV_INIT_STATE(6,0,0,0,0,0,NULL,NULL);
        }

        {
          _SFD_CV_INIT_STATE(7,0,0,0,0,0,NULL,NULL);
        }

        {
          _SFD_CV_INIT_STATE(8,0,0,0,0,0,NULL,NULL);
        }

        _SFD_CV_INIT_TRANS(3,0,NULL,NULL,0,NULL);
        _SFD_CV_INIT_TRANS(0,0,NULL,NULL,0,NULL);
        _SFD_CV_INIT_TRANS(1,0,NULL,NULL,0,NULL);
        _SFD_CV_INIT_TRANS(4,0,NULL,NULL,0,NULL);
        _SFD_CV_INIT_TRANS(6,0,NULL,NULL,0,NULL);
        _SFD_CV_INIT_TRANS(8,0,NULL,NULL,0,NULL);
        _SFD_CV_INIT_TRANS(5,0,NULL,NULL,0,NULL);
        _SFD_CV_INIT_TRANS(7,0,NULL,NULL,0,NULL);
        _SFD_CV_INIT_TRANS(2,0,NULL,NULL,0,NULL);

        /* Initialization of MATLAB Function Model Coverage */
        _SFD_CV_INIT_EML(5,1,1,2,0,9,0,0,0,0,0);
        _SFD_CV_INIT_EML_FCN(5,0,"calcShootWay",0,-1,382);
        _SFD_CV_INIT_EML_SATURATION(5,1,0,131,-1,138);
        _SFD_CV_INIT_EML_SATURATION(5,1,1,148,-1,187);
        _SFD_CV_INIT_EML_SATURATION(5,1,2,193,-1,254);
        _SFD_CV_INIT_EML_SATURATION(5,1,3,225,-1,251);
        _SFD_CV_INIT_EML_SATURATION(5,1,4,268,-1,281);
        _SFD_CV_INIT_EML_SATURATION(5,1,5,294,-1,307);
        _SFD_CV_INIT_EML_SATURATION(5,1,6,330,-1,376);
        _SFD_CV_INIT_EML_SATURATION(5,1,7,350,-1,357);
        _SFD_CV_INIT_EML_SATURATION(5,1,8,366,-1,373);
        _SFD_CV_INIT_EML_IF(5,1,0,22,37,63,95);
        _SFD_CV_INIT_EML_IF(5,1,1,96,119,-1,143);
        _SFD_CV_INIT_EML_RELATIONAL(5,1,0,25,37,-1,4);
        _SFD_CV_INIT_EML_RELATIONAL(5,1,1,99,119,0,1);
        _SFD_CV_INIT_EML(7,1,1,0,0,0,1,0,0,0,0);
        _SFD_CV_INIT_EML_FCN(7,0,"calcStartTeams",0,-1,221);

        {
          static int caseStart[] = { -1, 44, 132 };

          static int caseExprEnd[] = { 8, 59, 147 };

          _SFD_CV_INIT_EML_SWITCH(7,1,0,24,40,219,3,&(caseStart[0]),
            &(caseExprEnd[0]));
        }

        _SFD_CV_INIT_EML(6,1,1,1,0,1,1,0,0,0,0);
        _SFD_CV_INIT_EML_FCN(6,0,"calcStartPos",0,-1,262);
        _SFD_CV_INIT_EML_SATURATION(6,1,0,243,-1,255);
        _SFD_CV_INIT_EML_IF(6,1,0,210,226,-1,260);

        {
          static int caseStart[] = { -1, 45, 100, 155 };

          static int caseExprEnd[] = { 8, 60, 115, 170 };

          _SFD_CV_INIT_EML_SWITCH(6,1,0,22,41,209,4,&(caseStart[0]),
            &(caseExprEnd[0]));
        }

        _SFD_CV_INIT_EML_RELATIONAL(6,1,0,213,226,0,0);
        _SFD_CV_INIT_EML(8,1,1,1,0,1,0,0,0,0,0);
        _SFD_CV_INIT_EML_FCN(8,0,"checkReached",0,-1,145);
        _SFD_CV_INIT_EML_SATURATION(8,1,0,78,-1,105);
        _SFD_CV_INIT_EML_IF(8,1,0,63,113,-1,142);
        _SFD_CV_INIT_EML_RELATIONAL(8,1,0,66,113,-1,2);
        _SFD_CV_INIT_EML(0,1,0,0,0,0,0,0,0,0,0);
        _SFD_CV_INIT_EML(2,1,0,0,0,0,0,0,0,0,0);
        _SFD_CV_INIT_EML(3,1,0,0,0,0,0,0,0,0,0);
        _SFD_CV_INIT_EML(1,1,0,0,0,0,0,0,0,0,0);
        _SFD_CV_INIT_EML(4,1,0,0,0,0,0,0,0,0,0);
        _SFD_CV_INIT_EML(10,1,0,0,0,0,0,0,0,0,0);
        _SFD_CV_INIT_EML(6,0,0,1,0,0,0,0,0,0,0);
        _SFD_CV_INIT_EML_IF(6,0,0,0,14,0,14);
        _SFD_CV_INIT_EML(8,0,0,1,0,0,0,0,0,0,0);
        _SFD_CV_INIT_EML_IF(8,0,0,1,15,1,15);
        _SFD_CV_INIT_EML(5,0,0,1,0,0,0,0,0,0,0);
        _SFD_CV_INIT_EML_IF(5,0,0,1,51,1,48);
        _SFD_CV_INIT_EML_RELATIONAL(5,0,0,1,51,0,0);
        _SFD_CV_INIT_EML(7,0,0,1,0,2,0,0,0,0,0);
        _SFD_CV_INIT_EML_SATURATION(7,0,0,1,-1,43);
        _SFD_CV_INIT_EML_SATURATION(7,0,1,5,-1,42);
        _SFD_CV_INIT_EML_IF(7,0,0,1,46,1,46);
        _SFD_CV_INIT_EML_RELATIONAL(7,0,0,1,46,-1,2);
        _SFD_CV_INIT_EML(3,0,0,0,0,0,0,0,0,0,0);
        _SFD_CV_INIT_EML(0,0,0,1,0,0,0,0,0,0,0);
        _SFD_CV_INIT_EML_IF(0,0,0,1,17,1,17);
        _SFD_CV_INIT_EML_RELATIONAL(0,0,0,1,17,0,0);
        _SFD_CV_INIT_EML(1,0,0,1,0,0,0,0,0,0,0);
        _SFD_CV_INIT_EML_IF(1,0,0,1,17,1,17);
        _SFD_CV_INIT_EML_RELATIONAL(1,0,0,1,17,0,0);
        _SFD_SET_DATA_COMPILED_PROPS(0,SF_STRUCT,0,NULL,0,0,0,0.0,1.0,0,0,
          (MexFcnForType)c9_me_bus_io,(MexInFcnForType)NULL);

        {
          unsigned int dimVector[2];
          dimVector[0]= 1;
          dimVector[1]= 6;
          _SFD_SET_DATA_COMPILED_PROPS(1,SF_STRUCT,2,&(dimVector[0]),0,0,0,0.0,
            1.0,0,0,(MexFcnForType)c9_players_bus_io,(MexInFcnForType)NULL);
        }

        _SFD_SET_DATA_COMPILED_PROPS(2,SF_STRUCT,0,NULL,0,0,0,0.0,1.0,0,0,
          (MexFcnForType)c9_ball_bus_io,(MexInFcnForType)NULL);
        _SFD_SET_DATA_COMPILED_PROPS(3,SF_STRUCT,0,NULL,0,0,0,0.0,1.0,0,0,
          (MexFcnForType)c9_finalWay_bus_io,(MexInFcnForType)NULL);
        _SFD_SET_DATA_COMPILED_PROPS(4,SF_STRUCT,0,NULL,0,0,0,0.0,1.0,0,0,
          (MexFcnForType)c9_finalWay_bus_io,(MexInFcnForType)NULL);

        {
          unsigned int dimVector[2];
          dimVector[0]= 2;
          dimVector[1]= 1;
          _SFD_SET_DATA_COMPILED_PROPS(5,SF_INT8,2,&(dimVector[0]),0,0,0,0.0,1.0,
            0,0,(MexFcnForType)c9_k_sf_marshallOut,(MexInFcnForType)
            c9_h_sf_marshallIn);
        }

        _SFD_SET_DATA_COMPILED_PROPS(6,SF_UINT8,0,NULL,0,0,0,0.0,1.0,0,0,
          (MexFcnForType)c9_d_sf_marshallOut,(MexInFcnForType)NULL);

        {
          unsigned int dimVector[1];
          dimVector[0]= 3;
          _SFD_SET_DATA_COMPILED_PROPS(7,SF_STRUCT,1,&(dimVector[0]),0,0,0,0.0,
            1.0,0,0,(MexFcnForType)c9_l_sf_marshallOut,(MexInFcnForType)
            c9_i_sf_marshallIn);
        }

        {
          unsigned int dimVector[1];
          dimVector[0]= 3;
          _SFD_SET_DATA_COMPILED_PROPS(8,SF_STRUCT,1,&(dimVector[0]),0,0,0,0.0,
            1.0,0,0,(MexFcnForType)c9_l_sf_marshallOut,(MexInFcnForType)
            c9_i_sf_marshallIn);
        }

        _SFD_SET_DATA_COMPILED_PROPS(9,SF_STRUCT,0,NULL,0,0,0,0.0,1.0,0,0,
          (MexFcnForType)c9_j_sf_marshallOut,(MexInFcnForType)c9_g_sf_marshallIn);
        _SFD_SET_DATA_COMPILED_PROPS(10,SF_STRUCT,0,NULL,0,0,0,0.0,1.0,0,0,
          (MexFcnForType)c9_j_sf_marshallOut,(MexInFcnForType)c9_g_sf_marshallIn);
        _SFD_SET_DATA_COMPILED_PROPS(11,SF_UINT8,0,NULL,0,0,0,0.0,1.0,0,0,
          (MexFcnForType)c9_d_sf_marshallOut,(MexInFcnForType)c9_d_sf_marshallIn);

        {
          unsigned int dimVector[1];
          dimVector[0]= 4294967295;
          _SFD_SET_DATA_COMPILED_PROPS(12,SF_INT8,1,&(dimVector[0]),0,0,0,0.0,
            1.0,0,0,(MexFcnForType)NULL,(MexInFcnForType)NULL);
        }

        {
          unsigned int dimVector[1];
          dimVector[0]= 4294967295;
          _SFD_SET_DATA_COMPILED_PROPS(13,SF_DOUBLE,1,&(dimVector[0]),0,0,0,0.0,
            1.0,0,0,(MexFcnForType)NULL,(MexInFcnForType)NULL);
        }

        {
          unsigned int dimVector[1];
          dimVector[0]= 4294967295;
          _SFD_SET_DATA_COMPILED_PROPS(14,SF_DOUBLE,1,&(dimVector[0]),0,0,0,0.0,
            1.0,0,0,(MexFcnForType)NULL,(MexInFcnForType)NULL);
        }

        _SFD_SET_DATA_VALUE_PTR(12,(void *)(NULL));
        _SFD_SET_DATA_VALUE_PTR(13,(void *)(NULL));
        _SFD_SET_DATA_VALUE_PTR(14,(void *)(NULL));
        _SFD_SET_DATA_VALUE_PTR(0U, chartInstance->c9_me);
        _SFD_SET_DATA_VALUE_PTR(1U, *chartInstance->c9_players);
        _SFD_SET_DATA_VALUE_PTR(2U, chartInstance->c9_ball);
        _SFD_SET_DATA_VALUE_PTR(3U, chartInstance->c9_finalWay);
        _SFD_SET_DATA_VALUE_PTR(4U, chartInstance->c9_manualWay);
        _SFD_SET_DATA_VALUE_PTR(5U, chartInstance->c9_startingPos);
        _SFD_SET_DATA_VALUE_PTR(6U, chartInstance->c9_GameOn);
        _SFD_SET_DATA_VALUE_PTR(7U, chartInstance->c9_enemy);
        _SFD_SET_DATA_VALUE_PTR(8U, chartInstance->c9_friends);
        _SFD_SET_DATA_VALUE_PTR(9U, &chartInstance->c9_shootWay);
        _SFD_SET_DATA_VALUE_PTR(10U, &chartInstance->c9_kickWay);
        _SFD_SET_DATA_VALUE_PTR(11U, chartInstance->c9_shoot);
      }
    } else {
      sf_debug_reset_current_state_configuration(sfGlobalDebugInstanceStruct,
        _greenOffenceMachineNumber_,chartInstance->chartNumber,
        chartInstance->instanceNumber);
    }
  }
}

static const char* sf_get_instance_specialization(void)
{
  return "yqxI9GpGlXtwNXzunjTnYF";
}

static void sf_opaque_initialize_c9_greenOffence(void *chartInstanceVar)
{
  chart_debug_initialization(((SFc9_greenOffenceInstanceStruct*)
    chartInstanceVar)->S,0);
  initialize_params_c9_greenOffence((SFc9_greenOffenceInstanceStruct*)
    chartInstanceVar);
  initialize_c9_greenOffence((SFc9_greenOffenceInstanceStruct*) chartInstanceVar);
}

static void sf_opaque_enable_c9_greenOffence(void *chartInstanceVar)
{
  enable_c9_greenOffence((SFc9_greenOffenceInstanceStruct*) chartInstanceVar);
}

static void sf_opaque_disable_c9_greenOffence(void *chartInstanceVar)
{
  disable_c9_greenOffence((SFc9_greenOffenceInstanceStruct*) chartInstanceVar);
}

static void sf_opaque_gateway_c9_greenOffence(void *chartInstanceVar)
{
  sf_gateway_c9_greenOffence((SFc9_greenOffenceInstanceStruct*) chartInstanceVar);
}

static const mxArray* sf_opaque_get_sim_state_c9_greenOffence(SimStruct* S)
{
  ChartRunTimeInfo * crtInfo = (ChartRunTimeInfo *)(ssGetUserData(S));
  ChartInfoStruct * chartInfo = (ChartInfoStruct *)(crtInfo->instanceInfo);
  return get_sim_state_c9_greenOffence((SFc9_greenOffenceInstanceStruct*)
    chartInfo->chartInstance);         /* raw sim ctx */
}

static void sf_opaque_set_sim_state_c9_greenOffence(SimStruct* S, const mxArray *
  st)
{
  ChartRunTimeInfo * crtInfo = (ChartRunTimeInfo *)(ssGetUserData(S));
  ChartInfoStruct * chartInfo = (ChartInfoStruct *)(crtInfo->instanceInfo);
  set_sim_state_c9_greenOffence((SFc9_greenOffenceInstanceStruct*)
    chartInfo->chartInstance, st);
}

static void sf_opaque_terminate_c9_greenOffence(void *chartInstanceVar)
{
  if (chartInstanceVar!=NULL) {
    SimStruct *S = ((SFc9_greenOffenceInstanceStruct*) chartInstanceVar)->S;
    ChartRunTimeInfo * crtInfo = (ChartRunTimeInfo *)(ssGetUserData(S));
    if (sim_mode_is_rtw_gen(S) || sim_mode_is_external(S)) {
      sf_clear_rtw_identifier(S);
      unload_greenOffence_optimization_info();
    }

    finalize_c9_greenOffence((SFc9_greenOffenceInstanceStruct*) chartInstanceVar);
    utFree(chartInstanceVar);
    if (crtInfo != NULL) {
      utFree(crtInfo);
    }

    ssSetUserData(S,NULL);
  }
}

static void sf_opaque_init_subchart_simstructs(void *chartInstanceVar)
{
  initSimStructsc9_greenOffence((SFc9_greenOffenceInstanceStruct*)
    chartInstanceVar);
}

extern unsigned int sf_machine_global_initializer_called(void);
static void mdlProcessParameters_c9_greenOffence(SimStruct *S)
{
  int i;
  for (i=0;i<ssGetNumRunTimeParams(S);i++) {
    if (ssGetSFcnParamTunable(S,i)) {
      ssUpdateDlgParamAsRunTimeParam(S,i);
    }
  }

  if (sf_machine_global_initializer_called()) {
    ChartRunTimeInfo * crtInfo = (ChartRunTimeInfo *)(ssGetUserData(S));
    ChartInfoStruct * chartInfo = (ChartInfoStruct *)(crtInfo->instanceInfo);
    initialize_params_c9_greenOffence((SFc9_greenOffenceInstanceStruct*)
      (chartInfo->chartInstance));
  }
}

static void mdlSetWorkWidths_c9_greenOffence(SimStruct *S)
{
  if (sim_mode_is_rtw_gen(S) || sim_mode_is_external(S)) {
    mxArray *infoStruct = load_greenOffence_optimization_info();
    int_T chartIsInlinable =
      (int_T)sf_is_chart_inlinable(sf_get_instance_specialization(),infoStruct,9);
    ssSetStateflowIsInlinable(S,chartIsInlinable);
    ssSetRTWCG(S,sf_rtw_info_uint_prop(sf_get_instance_specialization(),
                infoStruct,9,"RTWCG"));
    ssSetEnableFcnIsTrivial(S,1);
    ssSetDisableFcnIsTrivial(S,1);
    ssSetNotMultipleInlinable(S,sf_rtw_info_uint_prop
      (sf_get_instance_specialization(),infoStruct,9,
       "gatewayCannotBeInlinedMultipleTimes"));
    sf_update_buildInfo(sf_get_instance_specialization(),infoStruct,9);
    if (chartIsInlinable) {
      ssSetInputPortOptimOpts(S, 0, SS_REUSABLE_AND_LOCAL);
      ssSetInputPortOptimOpts(S, 1, SS_REUSABLE_AND_LOCAL);
      ssSetInputPortOptimOpts(S, 2, SS_REUSABLE_AND_LOCAL);
      ssSetInputPortOptimOpts(S, 3, SS_REUSABLE_AND_LOCAL);
      ssSetInputPortOptimOpts(S, 4, SS_REUSABLE_AND_LOCAL);
      sf_mark_chart_expressionable_inputs(S,sf_get_instance_specialization(),
        infoStruct,9,5);
      sf_mark_chart_reusable_outputs(S,sf_get_instance_specialization(),
        infoStruct,9,2);
    }

    {
      unsigned int outPortIdx;
      for (outPortIdx=1; outPortIdx<=2; ++outPortIdx) {
        ssSetOutputPortOptimizeInIR(S, outPortIdx, 1U);
      }
    }

    {
      unsigned int inPortIdx;
      for (inPortIdx=0; inPortIdx < 5; ++inPortIdx) {
        ssSetInputPortOptimizeInIR(S, inPortIdx, 1U);
      }
    }

    sf_set_rtw_dwork_info(S,sf_get_instance_specialization(),infoStruct,9);
    ssSetHasSubFunctions(S,!(chartIsInlinable));
  } else {
  }

  ssSetOptions(S,ssGetOptions(S)|SS_OPTION_WORKS_WITH_CODE_REUSE);
  ssSetChecksum0(S,(3065249425U));
  ssSetChecksum1(S,(1362790545U));
  ssSetChecksum2(S,(2689736412U));
  ssSetChecksum3(S,(4101292776U));
  ssSetmdlDerivatives(S, NULL);
  ssSetExplicitFCSSCtrl(S,1);
  ssSupportsMultipleExecInstances(S,1);
}

static void mdlRTW_c9_greenOffence(SimStruct *S)
{
  if (sim_mode_is_rtw_gen(S)) {
    ssWriteRTWStrParam(S, "StateflowChartType", "Stateflow");
  }
}

static void mdlStart_c9_greenOffence(SimStruct *S)
{
  SFc9_greenOffenceInstanceStruct *chartInstance;
  ChartRunTimeInfo * crtInfo = (ChartRunTimeInfo *)utMalloc(sizeof
    (ChartRunTimeInfo));
  chartInstance = (SFc9_greenOffenceInstanceStruct *)utMalloc(sizeof
    (SFc9_greenOffenceInstanceStruct));
  memset(chartInstance, 0, sizeof(SFc9_greenOffenceInstanceStruct));
  if (chartInstance==NULL) {
    sf_mex_error_message("Could not allocate memory for chart instance.");
  }

  chartInstance->chartInfo.chartInstance = chartInstance;
  chartInstance->chartInfo.isEMLChart = 0;
  chartInstance->chartInfo.chartInitialized = 0;
  chartInstance->chartInfo.sFunctionGateway = sf_opaque_gateway_c9_greenOffence;
  chartInstance->chartInfo.initializeChart =
    sf_opaque_initialize_c9_greenOffence;
  chartInstance->chartInfo.terminateChart = sf_opaque_terminate_c9_greenOffence;
  chartInstance->chartInfo.enableChart = sf_opaque_enable_c9_greenOffence;
  chartInstance->chartInfo.disableChart = sf_opaque_disable_c9_greenOffence;
  chartInstance->chartInfo.getSimState = sf_opaque_get_sim_state_c9_greenOffence;
  chartInstance->chartInfo.setSimState = sf_opaque_set_sim_state_c9_greenOffence;
  chartInstance->chartInfo.getSimStateInfo =
    sf_get_sim_state_info_c9_greenOffence;
  chartInstance->chartInfo.zeroCrossings = NULL;
  chartInstance->chartInfo.outputs = NULL;
  chartInstance->chartInfo.derivatives = NULL;
  chartInstance->chartInfo.mdlRTW = mdlRTW_c9_greenOffence;
  chartInstance->chartInfo.mdlStart = mdlStart_c9_greenOffence;
  chartInstance->chartInfo.mdlSetWorkWidths = mdlSetWorkWidths_c9_greenOffence;
  chartInstance->chartInfo.extModeExec = NULL;
  chartInstance->chartInfo.restoreLastMajorStepConfiguration = NULL;
  chartInstance->chartInfo.restoreBeforeLastMajorStepConfiguration = NULL;
  chartInstance->chartInfo.storeCurrentConfiguration = NULL;
  chartInstance->chartInfo.callAtomicSubchartUserFcn = NULL;
  chartInstance->chartInfo.callAtomicSubchartAutoFcn = NULL;
  chartInstance->chartInfo.debugInstance = sfGlobalDebugInstanceStruct;
  chartInstance->S = S;
  crtInfo->checksum = SF_RUNTIME_INFO_CHECKSUM;
  crtInfo->instanceInfo = (&(chartInstance->chartInfo));
  crtInfo->isJITEnabled = false;
  crtInfo->compiledInfo = NULL;
  ssSetUserData(S,(void *)(crtInfo));  /* register the chart instance with simstruct */
  init_dsm_address_info(chartInstance);
  init_simulink_io_address(chartInstance);
  if (!sim_mode_is_rtw_gen(S)) {
  }

  sf_opaque_init_subchart_simstructs(chartInstance->chartInfo.chartInstance);
  chart_debug_initialization(S,1);
}

void c9_greenOffence_method_dispatcher(SimStruct *S, int_T method, void *data)
{
  switch (method) {
   case SS_CALL_MDL_START:
    mdlStart_c9_greenOffence(S);
    break;

   case SS_CALL_MDL_SET_WORK_WIDTHS:
    mdlSetWorkWidths_c9_greenOffence(S);
    break;

   case SS_CALL_MDL_PROCESS_PARAMETERS:
    mdlProcessParameters_c9_greenOffence(S);
    break;

   default:
    /* Unhandled method */
    sf_mex_error_message("Stateflow Internal Error:\n"
                         "Error calling c9_greenOffence_method_dispatcher.\n"
                         "Can't handle method %d.\n", method);
    break;
  }
}

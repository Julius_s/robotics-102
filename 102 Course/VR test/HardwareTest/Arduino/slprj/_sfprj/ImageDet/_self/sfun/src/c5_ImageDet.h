#ifndef __c5_ImageDet_h__
#define __c5_ImageDet_h__

/* Include files */
#include "sf_runtime/sfc_sf.h"
#include "sf_runtime/sfc_mex.h"
#include "rtwtypes.h"
#include "multiword_types.h"

/* Type Definitions */
#ifndef struct_BlobData_tag
#define struct_BlobData_tag

struct BlobData_tag
{
  real32_T centroid[32];
  int32_T area[16];
  uint8_T color[16];
};

#endif                                 /*struct_BlobData_tag*/

#ifndef typedef_c5_BlobData
#define typedef_c5_BlobData

typedef struct BlobData_tag c5_BlobData;

#endif                                 /*typedef_c5_BlobData*/

#ifndef struct_BlobData_tag_size
#define struct_BlobData_tag_size

struct BlobData_tag_size
{
  int32_T centroid[2];
  int32_T area;
  int32_T color;
};

#endif                                 /*struct_BlobData_tag_size*/

#ifndef typedef_c5_BlobData_size
#define typedef_c5_BlobData_size

typedef struct BlobData_tag_size c5_BlobData_size;

#endif                                 /*typedef_c5_BlobData_size*/

#ifndef typedef_SFc5_ImageDetInstanceStruct
#define typedef_SFc5_ImageDetInstanceStruct

typedef struct {
  SimStruct *S;
  ChartInfoStruct chartInfo;
  uint32_T chartNumber;
  uint32_T instanceNumber;
  int32_T c5_sfEvent;
  boolean_T c5_isStable;
  boolean_T c5_doneDoubleBufferReInit;
  uint8_T c5_is_active_c5_ImageDet;
  real32_T (*c5_centroids_data)[32];
  int32_T (*c5_centroids_sizes)[2];
  int32_T (*c5_area_data)[16];
  int32_T (*c5_area_sizes)[2];
  boolean_T (*c5_RBW)[76800];
  boolean_T (*c5_BBW)[76800];
  c5_BlobData *c5_blobs_data;
  c5_BlobData_size *c5_blobs_elems_sizes;
} SFc5_ImageDetInstanceStruct;

#endif                                 /*typedef_SFc5_ImageDetInstanceStruct*/

/* Named Constants */

/* Variable Declarations */
extern struct SfDebugInstanceStruct *sfGlobalDebugInstanceStruct;

/* Variable Definitions */

/* Function Declarations */
extern const mxArray *sf_c5_ImageDet_get_eml_resolved_functions_info(void);

/* Function Definitions */
extern void sf_c5_ImageDet_get_check_sum(mxArray *plhs[]);
extern void c5_ImageDet_method_dispatcher(SimStruct *S, int_T method, void *data);

#endif

/* Include files */

#include <stddef.h>
#include "blas.h"
#include "ImageDet_sfun.h"
#include "c3_ImageDet.h"
#include "mwmathutil.h"
#define CHARTINSTANCE_CHARTNUMBER      (chartInstance->chartNumber)
#define CHARTINSTANCE_INSTANCENUMBER   (chartInstance->instanceNumber)
#include "ImageDet_sfun_debug_macros.h"
#define _SF_MEX_LISTEN_FOR_CTRL_C(S)   sf_mex_listen_for_ctrl_c(sfGlobalDebugInstanceStruct,S);

/* Type Definitions */

/* Named Constants */
#define CALL_EVENT                     (-1)

/* Variable Declarations */

/* Variable Definitions */
static real_T _sfTime_;
static const char * c3_debug_family_names[29] = { "blobIndices", "lenA", "a",
  "b", "allLights", "ii", "jj", "pxFromCentre", "s", "angles", "pos", "reds",
  "blues", "lenRed", "lenBlue", "lightBlue", "dist", "index", "lightRed", "temp",
  "Dpos", "orientation", "nargin", "nargout", "blobs", "camRes", "height",
  "players", "Ball" };

/* Function Declarations */
static void initialize_c3_ImageDet(SFc3_ImageDetInstanceStruct *chartInstance);
static void initialize_params_c3_ImageDet(SFc3_ImageDetInstanceStruct
  *chartInstance);
static void enable_c3_ImageDet(SFc3_ImageDetInstanceStruct *chartInstance);
static void disable_c3_ImageDet(SFc3_ImageDetInstanceStruct *chartInstance);
static void c3_update_debugger_state_c3_ImageDet(SFc3_ImageDetInstanceStruct
  *chartInstance);
static const mxArray *get_sim_state_c3_ImageDet(SFc3_ImageDetInstanceStruct
  *chartInstance);
static void set_sim_state_c3_ImageDet(SFc3_ImageDetInstanceStruct *chartInstance,
  const mxArray *c3_st);
static void finalize_c3_ImageDet(SFc3_ImageDetInstanceStruct *chartInstance);
static void sf_gateway_c3_ImageDet(SFc3_ImageDetInstanceStruct *chartInstance);
static void mdl_start_c3_ImageDet(SFc3_ImageDetInstanceStruct *chartInstance);
static void c3_chartstep_c3_ImageDet(SFc3_ImageDetInstanceStruct *chartInstance);
static void initSimStructsc3_ImageDet(SFc3_ImageDetInstanceStruct *chartInstance);
static void init_script_number_translation(uint32_T c3_machineNumber, uint32_T
  c3_chartNumber, uint32_T c3_instanceNumber);
static const mxArray *c3_sf_marshallOut(void *chartInstanceVoid, c3_BallData
  *c3_inData_data, c3_BallData_size *c3_inData_elems_sizes);
static void c3_emlrt_marshallIn(SFc3_ImageDetInstanceStruct *chartInstance,
  const mxArray *c3_Ball, const char_T *c3_identifier, c3_BallData *c3_y_data,
  c3_BallData_size *c3_y_elems_sizes);
static void c3_b_emlrt_marshallIn(SFc3_ImageDetInstanceStruct *chartInstance,
  const mxArray *c3_u, const emlrtMsgIdentifier *c3_parentId, c3_BallData
  *c3_y_data, c3_BallData_size *c3_y_elems_sizes);
static void c3_c_emlrt_marshallIn(SFc3_ImageDetInstanceStruct *chartInstance,
  const mxArray *c3_u, const emlrtMsgIdentifier *c3_parentId, int8_T c3_y_data[],
  int32_T *c3_y_sizes);
static void c3_sf_marshallIn(void *chartInstanceVoid, const mxArray
  *c3_mxArrayInData, const char_T *c3_varName, c3_BallData *c3_outData_data,
  c3_BallData_size *c3_outData_elems_sizes);
static const mxArray *c3_b_sf_marshallOut(void *chartInstanceVoid, c3_PlayerData
  *c3_inData_data, c3_PlayerData_size *c3_inData_elems_sizes);
static void c3_d_emlrt_marshallIn(SFc3_ImageDetInstanceStruct *chartInstance,
  const mxArray *c3_players, const char_T *c3_identifier, c3_PlayerData
  *c3_y_data, c3_PlayerData_size *c3_y_elems_sizes);
static void c3_e_emlrt_marshallIn(SFc3_ImageDetInstanceStruct *chartInstance,
  const mxArray *c3_u, const emlrtMsgIdentifier *c3_parentId, c3_PlayerData
  *c3_y_data, c3_PlayerData_size *c3_y_elems_sizes);
static void c3_f_emlrt_marshallIn(SFc3_ImageDetInstanceStruct *chartInstance,
  const mxArray *c3_u, const emlrtMsgIdentifier *c3_parentId, int8_T c3_y_data[],
  int32_T c3_y_sizes[2]);
static void c3_g_emlrt_marshallIn(SFc3_ImageDetInstanceStruct *chartInstance,
  const mxArray *c3_u, const emlrtMsgIdentifier *c3_parentId, int16_T c3_y_data[],
  int32_T c3_y_sizes[2]);
static void c3_h_emlrt_marshallIn(SFc3_ImageDetInstanceStruct *chartInstance,
  const mxArray *c3_u, const emlrtMsgIdentifier *c3_parentId, uint8_T c3_y_data[],
  int32_T c3_y_sizes[2]);
static void c3_b_sf_marshallIn(void *chartInstanceVoid, const mxArray
  *c3_mxArrayInData, const char_T *c3_varName, c3_PlayerData *c3_outData_data,
  c3_PlayerData_size *c3_outData_elems_sizes);
static const mxArray *c3_c_sf_marshallOut(void *chartInstanceVoid, void
  *c3_inData);
static const mxArray *c3_d_sf_marshallOut(void *chartInstanceVoid, void
  *c3_inData);
static const mxArray *c3_e_sf_marshallOut(void *chartInstanceVoid, c3_BlobData
  *c3_inData_data, c3_BlobData_size *c3_inData_elems_sizes);
static real_T c3_i_emlrt_marshallIn(SFc3_ImageDetInstanceStruct *chartInstance,
  const mxArray *c3_u, const emlrtMsgIdentifier *c3_parentId);
static void c3_c_sf_marshallIn(void *chartInstanceVoid, const mxArray
  *c3_mxArrayInData, const char_T *c3_varName, void *c3_outData);
static const mxArray *c3_f_sf_marshallOut(void *chartInstanceVoid, void
  *c3_inData);
static real32_T c3_j_emlrt_marshallIn(SFc3_ImageDetInstanceStruct *chartInstance,
  const mxArray *c3_u, const emlrtMsgIdentifier *c3_parentId);
static void c3_d_sf_marshallIn(void *chartInstanceVoid, const mxArray
  *c3_mxArrayInData, const char_T *c3_varName, void *c3_outData);
static const mxArray *c3_g_sf_marshallOut(void *chartInstanceVoid, void
  *c3_inData);
static void c3_k_emlrt_marshallIn(SFc3_ImageDetInstanceStruct *chartInstance,
  const mxArray *c3_u, const emlrtMsgIdentifier *c3_parentId, real32_T c3_y[2]);
static void c3_e_sf_marshallIn(void *chartInstanceVoid, const mxArray
  *c3_mxArrayInData, const char_T *c3_varName, void *c3_outData);
static const mxArray *c3_h_sf_marshallOut(void *chartInstanceVoid, void
  *c3_inData);
static uint8_T c3_l_emlrt_marshallIn(SFc3_ImageDetInstanceStruct *chartInstance,
  const mxArray *c3_index, const char_T *c3_identifier);
static uint8_T c3_m_emlrt_marshallIn(SFc3_ImageDetInstanceStruct *chartInstance,
  const mxArray *c3_u, const emlrtMsgIdentifier *c3_parentId);
static void c3_f_sf_marshallIn(void *chartInstanceVoid, const mxArray
  *c3_mxArrayInData, const char_T *c3_varName, void *c3_outData);
static const mxArray *c3_i_sf_marshallOut(void *chartInstanceVoid, real32_T
  c3_inData_data[], int32_T c3_inData_sizes[2]);
static void c3_n_emlrt_marshallIn(SFc3_ImageDetInstanceStruct *chartInstance,
  const mxArray *c3_u, const emlrtMsgIdentifier *c3_parentId, real32_T
  c3_y_data[], int32_T c3_y_sizes[2]);
static void c3_g_sf_marshallIn(void *chartInstanceVoid, const mxArray
  *c3_mxArrayInData, const char_T *c3_varName, real32_T c3_outData_data[],
  int32_T c3_outData_sizes[2]);
static const mxArray *c3_j_sf_marshallOut(void *chartInstanceVoid, real32_T
  c3_inData_data[], int32_T c3_inData_sizes[2]);
static void c3_o_emlrt_marshallIn(SFc3_ImageDetInstanceStruct *chartInstance,
  const mxArray *c3_u, const emlrtMsgIdentifier *c3_parentId, real32_T
  c3_y_data[], int32_T c3_y_sizes[2]);
static void c3_h_sf_marshallIn(void *chartInstanceVoid, const mxArray
  *c3_mxArrayInData, const char_T *c3_varName, real32_T c3_outData_data[],
  int32_T c3_outData_sizes[2]);
static const mxArray *c3_k_sf_marshallOut(void *chartInstanceVoid, real_T
  c3_inData_data[], int32_T *c3_inData_sizes);
static void c3_p_emlrt_marshallIn(SFc3_ImageDetInstanceStruct *chartInstance,
  const mxArray *c3_u, const emlrtMsgIdentifier *c3_parentId, real_T c3_y_data[],
  int32_T *c3_y_sizes);
static void c3_i_sf_marshallIn(void *chartInstanceVoid, const mxArray
  *c3_mxArrayInData, const char_T *c3_varName, real_T c3_outData_data[], int32_T
  *c3_outData_sizes);
static void c3_info_helper(const mxArray **c3_info);
static const mxArray *c3_emlrt_marshallOut(const char * c3_u);
static const mxArray *c3_b_emlrt_marshallOut(const uint32_T c3_u);
static void c3_b_info_helper(const mxArray **c3_info);
static void c3_eml_sort(SFc3_ImageDetInstanceStruct *chartInstance, int32_T
  c3_x_data[], int32_T c3_x_sizes, int32_T c3_y_data[], int32_T *c3_y_sizes,
  int32_T c3_idx_data[], int32_T *c3_idx_sizes);
static int32_T c3_eml_nonsingleton_dim(SFc3_ImageDetInstanceStruct
  *chartInstance, int32_T c3_x_data[], int32_T c3_x_sizes);
static void c3_eml_switch_helper(SFc3_ImageDetInstanceStruct *chartInstance);
static void c3_check_forloop_overflow_error(SFc3_ImageDetInstanceStruct
  *chartInstance, boolean_T c3_overflow);
static void c3_eml_sort_idx(SFc3_ImageDetInstanceStruct *chartInstance, int32_T
  c3_x_data[], int32_T c3_x_sizes, int32_T c3_idx_data[], int32_T *c3_idx_sizes);
static void c3_sign(SFc3_ImageDetInstanceStruct *chartInstance, real32_T c3_x[2],
                    real32_T c3_b_x[2]);
static void c3_abs(SFc3_ImageDetInstanceStruct *chartInstance, real32_T c3_x[2],
                   real32_T c3_y[2]);
static void c3_tand(SFc3_ImageDetInstanceStruct *chartInstance, real32_T c3_x[2],
                    real32_T c3_b_x[2]);
static void c3_eml_scalar_eg(SFc3_ImageDetInstanceStruct *chartInstance);
static real32_T c3_b_abs(SFc3_ImageDetInstanceStruct *chartInstance, real32_T
  c3_x);
static void c3_eml_li_find(SFc3_ImageDetInstanceStruct *chartInstance, boolean_T
  c3_x_data[], int32_T c3_x_sizes[2], int32_T c3_y_data[], int32_T c3_y_sizes[2]);
static real32_T c3_norm(SFc3_ImageDetInstanceStruct *chartInstance, real32_T
  c3_x[2]);
static void c3_below_threshold(SFc3_ImageDetInstanceStruct *chartInstance);
static real32_T c3_atan2d(SFc3_ImageDetInstanceStruct *chartInstance, real32_T
  c3_y, real32_T c3_x);
static const mxArray *c3_l_sf_marshallOut(void *chartInstanceVoid, void
  *c3_inData);
static int32_T c3_q_emlrt_marshallIn(SFc3_ImageDetInstanceStruct *chartInstance,
  const mxArray *c3_u, const emlrtMsgIdentifier *c3_parentId);
static void c3_j_sf_marshallIn(void *chartInstanceVoid, const mxArray
  *c3_mxArrayInData, const char_T *c3_varName, void *c3_outData);
static const mxArray *c3_blobs_bus_io(void *chartInstanceVoid, void *c3_pData);
static const mxArray *c3_sf_marshall_unsupported(void *chartInstanceVoid);
static const mxArray *c3_c_emlrt_marshallOut(SFc3_ImageDetInstanceStruct
  *chartInstance, const char * c3_u);
static const mxArray *c3_players_bus_io(void *chartInstanceVoid, void *c3_pData);
static const mxArray *c3_Ball_bus_io(void *chartInstanceVoid, void *c3_pData);
static void c3_b_sign(SFc3_ImageDetInstanceStruct *chartInstance, real32_T c3_x
                      [2]);
static void c3_b_tand(SFc3_ImageDetInstanceStruct *chartInstance, real32_T c3_x
                      [2]);
static void init_dsm_address_info(SFc3_ImageDetInstanceStruct *chartInstance);
static void init_simulink_io_address(SFc3_ImageDetInstanceStruct *chartInstance);

/* Function Definitions */
static void initialize_c3_ImageDet(SFc3_ImageDetInstanceStruct *chartInstance)
{
  chartInstance->c3_sfEvent = CALL_EVENT;
  _sfTime_ = sf_get_time(chartInstance->S);
  chartInstance->c3_is_active_c3_ImageDet = 0U;
}

static void initialize_params_c3_ImageDet(SFc3_ImageDetInstanceStruct
  *chartInstance)
{
  (void)chartInstance;
}

static void enable_c3_ImageDet(SFc3_ImageDetInstanceStruct *chartInstance)
{
  _sfTime_ = sf_get_time(chartInstance->S);
}

static void disable_c3_ImageDet(SFc3_ImageDetInstanceStruct *chartInstance)
{
  _sfTime_ = sf_get_time(chartInstance->S);
}

static void c3_update_debugger_state_c3_ImageDet(SFc3_ImageDetInstanceStruct
  *chartInstance)
{
  (void)chartInstance;
}

static const mxArray *get_sim_state_c3_ImageDet(SFc3_ImageDetInstanceStruct
  *chartInstance)
{
  const mxArray *c3_st;
  const mxArray *c3_y = NULL;
  const mxArray *c3_b_y = NULL;
  int32_T c3_u_sizes;
  int32_T c3_loop_ub;
  int32_T c3_i0;
  int8_T c3_u_data[2];
  const mxArray *c3_c_y = NULL;
  int32_T c3_b_u_sizes;
  int32_T c3_b_loop_ub;
  int32_T c3_i1;
  int8_T c3_b_u_data[2];
  const mxArray *c3_d_y = NULL;
  const mxArray *c3_e_y = NULL;
  int32_T c3_c_u_sizes[2];
  int32_T c3_u;
  int32_T c3_b_u;
  int32_T c3_c_loop_ub;
  int32_T c3_i2;
  int8_T c3_c_u_data[6];
  const mxArray *c3_f_y = NULL;
  int32_T c3_d_u_sizes[2];
  int32_T c3_c_u;
  int32_T c3_d_u;
  int32_T c3_d_loop_ub;
  int32_T c3_i3;
  int8_T c3_d_u_data[6];
  const mxArray *c3_g_y = NULL;
  int32_T c3_e_u_sizes[2];
  int32_T c3_e_u;
  int32_T c3_f_u;
  int32_T c3_e_loop_ub;
  int32_T c3_i4;
  int16_T c3_e_u_data[6];
  const mxArray *c3_h_y = NULL;
  int32_T c3_f_u_sizes[2];
  int32_T c3_g_u;
  int32_T c3_h_u;
  int32_T c3_f_loop_ub;
  int32_T c3_i5;
  uint8_T c3_f_u_data[6];
  const mxArray *c3_i_y = NULL;
  uint8_T c3_hoistedGlobal;
  uint8_T c3_i_u;
  const mxArray *c3_j_y = NULL;
  c3_st = NULL;
  c3_st = NULL;
  c3_y = NULL;
  sf_mex_assign(&c3_y, sf_mex_createcellmatrix(3, 1), false);
  c3_b_y = NULL;
  sf_mex_assign(&c3_b_y, sf_mex_createstruct("structure", 2, 1, 1), false);
  c3_u_sizes = chartInstance->c3_Ball_elems_sizes->x;
  c3_loop_ub = chartInstance->c3_Ball_elems_sizes->x - 1;
  for (c3_i0 = 0; c3_i0 <= c3_loop_ub; c3_i0++) {
    c3_u_data[c3_i0] = ((int8_T *)&((char_T *)chartInstance->c3_Ball_data)[0])
      [c3_i0];
  }

  c3_c_y = NULL;
  sf_mex_assign(&c3_c_y, sf_mex_create("y", c3_u_data, 2, 0U, 1U, 0U, 1,
    c3_u_sizes), false);
  sf_mex_addfield(c3_b_y, c3_c_y, "x", "x", 0);
  c3_b_u_sizes = chartInstance->c3_Ball_elems_sizes->y;
  c3_b_loop_ub = chartInstance->c3_Ball_elems_sizes->y - 1;
  for (c3_i1 = 0; c3_i1 <= c3_b_loop_ub; c3_i1++) {
    c3_b_u_data[c3_i1] = ((int8_T *)&((char_T *)chartInstance->c3_Ball_data)[2])
      [c3_i1];
  }

  c3_d_y = NULL;
  sf_mex_assign(&c3_d_y, sf_mex_create("y", c3_b_u_data, 2, 0U, 1U, 0U, 1,
    c3_b_u_sizes), false);
  sf_mex_addfield(c3_b_y, c3_d_y, "y", "y", 0);
  sf_mex_setcell(c3_y, 0, c3_b_y);
  c3_e_y = NULL;
  sf_mex_assign(&c3_e_y, sf_mex_createstruct("structure", 2, 1, 1), false);
  c3_c_u_sizes[0] = chartInstance->c3_players_elems_sizes->x[0];
  c3_c_u_sizes[1] = chartInstance->c3_players_elems_sizes->x[1];
  c3_u = c3_c_u_sizes[0];
  c3_b_u = c3_c_u_sizes[1];
  c3_c_loop_ub = chartInstance->c3_players_elems_sizes->x[0] *
    chartInstance->c3_players_elems_sizes->x[1] - 1;
  for (c3_i2 = 0; c3_i2 <= c3_c_loop_ub; c3_i2++) {
    c3_c_u_data[c3_i2] = ((int8_T *)&((char_T *)chartInstance->c3_players_data)
                          [0])[c3_i2];
  }

  c3_f_y = NULL;
  sf_mex_assign(&c3_f_y, sf_mex_create("y", c3_c_u_data, 2, 0U, 1U, 0U, 2,
    c3_c_u_sizes[0], c3_c_u_sizes[1]), false);
  sf_mex_addfield(c3_e_y, c3_f_y, "x", "x", 0);
  c3_d_u_sizes[0] = chartInstance->c3_players_elems_sizes->y[0];
  c3_d_u_sizes[1] = chartInstance->c3_players_elems_sizes->y[1];
  c3_c_u = c3_d_u_sizes[0];
  c3_d_u = c3_d_u_sizes[1];
  c3_d_loop_ub = chartInstance->c3_players_elems_sizes->y[0] *
    chartInstance->c3_players_elems_sizes->y[1] - 1;
  for (c3_i3 = 0; c3_i3 <= c3_d_loop_ub; c3_i3++) {
    c3_d_u_data[c3_i3] = ((int8_T *)&((char_T *)chartInstance->c3_players_data)
                          [6])[c3_i3];
  }

  c3_g_y = NULL;
  sf_mex_assign(&c3_g_y, sf_mex_create("y", c3_d_u_data, 2, 0U, 1U, 0U, 2,
    c3_d_u_sizes[0], c3_d_u_sizes[1]), false);
  sf_mex_addfield(c3_e_y, c3_g_y, "y", "y", 0);
  c3_e_u_sizes[0] = chartInstance->c3_players_elems_sizes->orientation[0];
  c3_e_u_sizes[1] = chartInstance->c3_players_elems_sizes->orientation[1];
  c3_e_u = c3_e_u_sizes[0];
  c3_f_u = c3_e_u_sizes[1];
  c3_e_loop_ub = chartInstance->c3_players_elems_sizes->orientation[0] *
    chartInstance->c3_players_elems_sizes->orientation[1] - 1;
  for (c3_i4 = 0; c3_i4 <= c3_e_loop_ub; c3_i4++) {
    c3_e_u_data[c3_i4] = ((int16_T *)&((char_T *)chartInstance->c3_players_data)
                          [12])[c3_i4];
  }

  c3_h_y = NULL;
  sf_mex_assign(&c3_h_y, sf_mex_create("y", c3_e_u_data, 4, 0U, 1U, 0U, 2,
    c3_e_u_sizes[0], c3_e_u_sizes[1]), false);
  sf_mex_addfield(c3_e_y, c3_h_y, "orientation", "orientation", 0);
  c3_f_u_sizes[0] = chartInstance->c3_players_elems_sizes->color[0];
  c3_f_u_sizes[1] = chartInstance->c3_players_elems_sizes->color[1];
  c3_g_u = c3_f_u_sizes[0];
  c3_h_u = c3_f_u_sizes[1];
  c3_f_loop_ub = chartInstance->c3_players_elems_sizes->color[0] *
    chartInstance->c3_players_elems_sizes->color[1] - 1;
  for (c3_i5 = 0; c3_i5 <= c3_f_loop_ub; c3_i5++) {
    c3_f_u_data[c3_i5] = ((uint8_T *)&((char_T *)chartInstance->c3_players_data)
                          [24])[c3_i5];
  }

  c3_i_y = NULL;
  sf_mex_assign(&c3_i_y, sf_mex_create("y", c3_f_u_data, 3, 0U, 1U, 0U, 2,
    c3_f_u_sizes[0], c3_f_u_sizes[1]), false);
  sf_mex_addfield(c3_e_y, c3_i_y, "color", "color", 0);
  sf_mex_setcell(c3_y, 1, c3_e_y);
  c3_hoistedGlobal = chartInstance->c3_is_active_c3_ImageDet;
  c3_i_u = c3_hoistedGlobal;
  c3_j_y = NULL;
  sf_mex_assign(&c3_j_y, sf_mex_create("y", &c3_i_u, 3, 0U, 0U, 0U, 0), false);
  sf_mex_setcell(c3_y, 2, c3_j_y);
  sf_mex_assign(&c3_st, c3_y, false);
  return c3_st;
}

static void set_sim_state_c3_ImageDet(SFc3_ImageDetInstanceStruct *chartInstance,
  const mxArray *c3_st)
{
  const mxArray *c3_u;
  c3_BallData_size c3_tmp_elems_sizes;
  c3_BallData c3_tmp_data;
  int32_T c3_loop_ub;
  int32_T c3_i6;
  int32_T c3_b_loop_ub;
  int32_T c3_i7;
  c3_PlayerData_size c3_b_tmp_elems_sizes;
  c3_PlayerData c3_b_tmp_data;
  int32_T c3_c_loop_ub;
  int32_T c3_i8;
  int32_T c3_d_loop_ub;
  int32_T c3_i9;
  int32_T c3_e_loop_ub;
  int32_T c3_i10;
  int32_T c3_f_loop_ub;
  int32_T c3_i11;
  int32_T c3_g_loop_ub;
  int32_T c3_i12;
  int32_T c3_h_loop_ub;
  int32_T c3_i13;
  int32_T c3_i_loop_ub;
  int32_T c3_i14;
  int32_T c3_j_loop_ub;
  int32_T c3_i15;
  chartInstance->c3_doneDoubleBufferReInit = true;
  c3_u = sf_mex_dup(c3_st);
  c3_emlrt_marshallIn(chartInstance, sf_mex_dup(sf_mex_getcell(c3_u, 0)), "Ball",
                      &c3_tmp_data, &c3_tmp_elems_sizes);
  chartInstance->c3_Ball_elems_sizes->x = c3_tmp_elems_sizes.x;
  c3_loop_ub = c3_tmp_elems_sizes.x - 1;
  for (c3_i6 = 0; c3_i6 <= c3_loop_ub; c3_i6++) {
    ((int8_T *)&((char_T *)chartInstance->c3_Ball_data)[0])[c3_i6] =
      c3_tmp_data.x[c3_i6];
  }

  chartInstance->c3_Ball_elems_sizes->y = c3_tmp_elems_sizes.y;
  c3_b_loop_ub = c3_tmp_elems_sizes.y - 1;
  for (c3_i7 = 0; c3_i7 <= c3_b_loop_ub; c3_i7++) {
    ((int8_T *)&((char_T *)chartInstance->c3_Ball_data)[2])[c3_i7] =
      c3_tmp_data.y[c3_i7];
  }

  c3_d_emlrt_marshallIn(chartInstance, sf_mex_dup(sf_mex_getcell(c3_u, 1)),
                        "players", &c3_b_tmp_data, &c3_b_tmp_elems_sizes);
  chartInstance->c3_players_elems_sizes->x[0] = c3_b_tmp_elems_sizes.x[0];
  chartInstance->c3_players_elems_sizes->x[1] = c3_b_tmp_elems_sizes.x[1];
  c3_c_loop_ub = c3_b_tmp_elems_sizes.x[1] - 1;
  for (c3_i8 = 0; c3_i8 <= c3_c_loop_ub; c3_i8++) {
    c3_d_loop_ub = c3_b_tmp_elems_sizes.x[0] - 1;
    for (c3_i9 = 0; c3_i9 <= c3_d_loop_ub; c3_i9++) {
      ((int8_T *)&((char_T *)chartInstance->c3_players_data)[0])[c3_i9 +
        chartInstance->c3_players_elems_sizes->x[0] * c3_i8] =
        c3_b_tmp_data.x[c3_i9 + c3_b_tmp_elems_sizes.x[0] * c3_i8];
    }
  }

  chartInstance->c3_players_elems_sizes->y[0] = c3_b_tmp_elems_sizes.y[0];
  chartInstance->c3_players_elems_sizes->y[1] = c3_b_tmp_elems_sizes.y[1];
  c3_e_loop_ub = c3_b_tmp_elems_sizes.y[1] - 1;
  for (c3_i10 = 0; c3_i10 <= c3_e_loop_ub; c3_i10++) {
    c3_f_loop_ub = c3_b_tmp_elems_sizes.y[0] - 1;
    for (c3_i11 = 0; c3_i11 <= c3_f_loop_ub; c3_i11++) {
      ((int8_T *)&((char_T *)chartInstance->c3_players_data)[6])[c3_i11 +
        chartInstance->c3_players_elems_sizes->y[0] * c3_i10] =
        c3_b_tmp_data.y[c3_i11 + c3_b_tmp_elems_sizes.y[0] * c3_i10];
    }
  }

  chartInstance->c3_players_elems_sizes->orientation[0] =
    c3_b_tmp_elems_sizes.orientation[0];
  chartInstance->c3_players_elems_sizes->orientation[1] =
    c3_b_tmp_elems_sizes.orientation[1];
  c3_g_loop_ub = c3_b_tmp_elems_sizes.orientation[1] - 1;
  for (c3_i12 = 0; c3_i12 <= c3_g_loop_ub; c3_i12++) {
    c3_h_loop_ub = c3_b_tmp_elems_sizes.orientation[0] - 1;
    for (c3_i13 = 0; c3_i13 <= c3_h_loop_ub; c3_i13++) {
      ((int16_T *)&((char_T *)chartInstance->c3_players_data)[12])[c3_i13 +
        chartInstance->c3_players_elems_sizes->orientation[0] * c3_i12] =
        c3_b_tmp_data.orientation[c3_i13 + c3_b_tmp_elems_sizes.orientation[0] *
        c3_i12];
    }
  }

  chartInstance->c3_players_elems_sizes->color[0] = c3_b_tmp_elems_sizes.color[0];
  chartInstance->c3_players_elems_sizes->color[1] = c3_b_tmp_elems_sizes.color[1];
  c3_i_loop_ub = c3_b_tmp_elems_sizes.color[1] - 1;
  for (c3_i14 = 0; c3_i14 <= c3_i_loop_ub; c3_i14++) {
    c3_j_loop_ub = c3_b_tmp_elems_sizes.color[0] - 1;
    for (c3_i15 = 0; c3_i15 <= c3_j_loop_ub; c3_i15++) {
      ((uint8_T *)&((char_T *)chartInstance->c3_players_data)[24])[c3_i15 +
        chartInstance->c3_players_elems_sizes->color[0] * c3_i14] =
        c3_b_tmp_data.color[c3_i15 + c3_b_tmp_elems_sizes.color[0] * c3_i14];
    }
  }

  chartInstance->c3_is_active_c3_ImageDet = c3_l_emlrt_marshallIn(chartInstance,
    sf_mex_dup(sf_mex_getcell(c3_u, 2)), "is_active_c3_ImageDet");
  sf_mex_destroy(&c3_u);
  c3_update_debugger_state_c3_ImageDet(chartInstance);
  sf_mex_destroy(&c3_st);
}

static void finalize_c3_ImageDet(SFc3_ImageDetInstanceStruct *chartInstance)
{
  (void)chartInstance;
}

static void sf_gateway_c3_ImageDet(SFc3_ImageDetInstanceStruct *chartInstance)
{
  int32_T c3_i16;
  _SFD_SYMBOL_SCOPE_PUSH(0U, 0U);
  _sfTime_ = sf_get_time(chartInstance->S);
  _SFD_CC_CALL(CHART_ENTER_SFUNCTION_TAG, 1U, chartInstance->c3_sfEvent);
  chartInstance->c3_sfEvent = CALL_EVENT;
  c3_chartstep_c3_ImageDet(chartInstance);
  _SFD_SYMBOL_SCOPE_POP();
  _SFD_CHECK_FOR_STATE_INCONSISTENCY(_ImageDetMachineNumber_,
    chartInstance->chartNumber, chartInstance->instanceNumber);
  for (c3_i16 = 0; c3_i16 < 2; c3_i16++) {
    _SFD_DATA_RANGE_CHECK((*chartInstance->c3_camRes)[c3_i16], 2U);
  }

  _SFD_DATA_RANGE_CHECK(*chartInstance->c3_height, 3U);
}

static void mdl_start_c3_ImageDet(SFc3_ImageDetInstanceStruct *chartInstance)
{
  (void)chartInstance;
}

static void c3_chartstep_c3_ImageDet(SFc3_ImageDetInstanceStruct *chartInstance)
{
  boolean_T c3_b0;
  real_T c3_hoistedGlobal;
  c3_BlobData_size c3_b_blobs_elems_sizes;
  int32_T c3_loop_ub;
  int32_T c3_i17;
  int32_T c3_b_loop_ub;
  int32_T c3_i18;
  c3_BlobData c3_b_blobs_data;
  int32_T c3_c_loop_ub;
  int32_T c3_i19;
  int32_T c3_d_loop_ub;
  int32_T c3_i20;
  int32_T c3_i21;
  real_T c3_b_camRes[2];
  real_T c3_b_height;
  uint32_T c3_debug_family_var_map[29];
  int32_T c3_blobIndices_sizes;
  real_T c3_blobIndices_data[16];
  real_T c3_lenA;
  real_T c3_a;
  real_T c3_b;
  int32_T c3_allLights_sizes[2];
  real32_T c3_allLights_data[93];
  real_T c3_ii;
  real_T c3_jj;
  real32_T c3_pxFromCentre[2];
  real32_T c3_s[2];
  real32_T c3_angles[2];
  real32_T c3_pos[2];
  int32_T c3_reds_sizes[2];
  real32_T c3_reds_data[62];
  int32_T c3_blues_sizes[2];
  real32_T c3_blues_data[62];
  real_T c3_lenRed;
  real_T c3_lenBlue;
  real32_T c3_lightBlue[2];
  real32_T c3_dist;
  uint8_T c3_index;
  real32_T c3_lightRed[2];
  real32_T c3_temp;
  real32_T c3_Dpos[2];
  real32_T c3_orientation;
  real_T c3_nargin = 3.0;
  real_T c3_nargout = 2.0;
  c3_PlayerData_size c3_b_players_elems_sizes;
  c3_PlayerData c3_b_players_data;
  c3_BallData_size c3_b_Ball_elems_sizes;
  c3_BallData c3_b_Ball_data;
  int32_T c3_x_sizes;
  int32_T c3_e_loop_ub;
  int32_T c3_i22;
  int32_T c3_x_data[16];
  int32_T c3_b_x_sizes;
  int32_T c3_f_loop_ub;
  int32_T c3_i23;
  int32_T c3_b_x_data[16];
  int32_T c3_iidx_sizes;
  int32_T c3_iidx_data[16];
  int32_T c3_y_sizes;
  int32_T c3_y_data[16];
  int32_T c3_b_blobIndices_sizes;
  int32_T c3_g_loop_ub;
  int32_T c3_i24;
  real_T c3_b_blobIndices_data[16];
  int32_T c3_h_loop_ub;
  int32_T c3_i25;
  int32_T c3_i_loop_ub;
  int32_T c3_i26;
  int32_T c3_iv0[2];
  int32_T c3_allLights;
  int32_T c3_b_allLights;
  int32_T c3_j_loop_ub;
  int32_T c3_i27;
  real_T c3_b_lenA;
  int32_T c3_i28;
  int32_T c3_b_ii;
  int32_T c3_i29;
  int32_T c3_i30;
  int32_T c3_tmp_sizes;
  int32_T c3_k_loop_ub;
  int32_T c3_i31;
  int32_T c3_tmp_data[2];
  int32_T c3_iv1[2];
  int32_T c3_i32;
  int32_T c3_iv2[2];
  real32_T c3_c_camRes[2];
  int32_T c3_i33;
  int32_T c3_iv3[2];
  int32_T c3_l_loop_ub;
  int32_T c3_i34;
  int32_T c3_i35;
  int32_T c3_b_jj;
  int32_T c3_b_tmp_sizes;
  int32_T c3_m_loop_ub;
  int32_T c3_i36;
  real32_T c3_b_tmp_data[2];
  real_T c3_A[2];
  int32_T c3_i37;
  int32_T c3_i38;
  int32_T c3_i39;
  real32_T c3_b_b[2];
  int32_T c3_i40;
  int32_T c3_i41;
  real32_T c3_b_pxFromCentre[2];
  real32_T c3_fv0[2];
  int32_T c3_i42;
  int32_T c3_i43;
  int32_T c3_i44;
  int32_T c3_i45;
  int32_T c3_i46;
  real_T c3_b_a;
  int32_T c3_i47;
  int32_T c3_i48;
  int32_T c3_c_allLights;
  int32_T c3_c_jj;
  real_T c3_d0;
  real32_T c3_c_b;
  real32_T c3_y;
  real32_T c3_f0;
  int8_T c3_i49;
  real32_T c3_d_b;
  real32_T c3_b_y;
  real32_T c3_f1;
  int8_T c3_i50;
  real_T c3_d1;
  boolean_T c3_b1;
  boolean_T c3_b2;
  boolean_T c3_b3;
  int32_T c3_i51;
  real_T c3_d2;
  real_T c3_d3;
  boolean_T c3_b4;
  boolean_T c3_b5;
  boolean_T c3_b6;
  int32_T c3_i52;
  int32_T c3_i53;
  int32_T c3_b_allLights_sizes[2];
  int32_T c3_n_loop_ub;
  int32_T c3_i54;
  int32_T c3_i55;
  real32_T c3_b_allLights_data[93];
  int32_T c3_o_loop_ub;
  int32_T c3_i56;
  int32_T c3_i57;
  int32_T c3_p_loop_ub;
  int32_T c3_i58;
  int32_T c3_i59;
  int32_T c3_i60;
  int32_T c3_c_allLights_sizes[2];
  int32_T c3_q_loop_ub;
  int32_T c3_i61;
  boolean_T c3_c_allLights_data[31];
  int32_T c3_c_tmp_sizes[2];
  int32_T c3_c_tmp_data[31];
  int32_T c3_r_loop_ub;
  int32_T c3_i62;
  int32_T c3_i63;
  int32_T c3_i64;
  int32_T c3_d_allLights_sizes[2];
  int32_T c3_s_loop_ub;
  int32_T c3_i65;
  boolean_T c3_d_allLights_data[31];
  int32_T c3_t_loop_ub;
  int32_T c3_i66;
  int32_T c3_i67;
  int32_T c3_i68;
  int32_T c3_c_x_sizes[2];
  int32_T c3_u_loop_ub;
  int32_T c3_i69;
  real32_T c3_c_x_data[31];
  int32_T c3_i70;
  int32_T c3_v_loop_ub;
  int32_T c3_i71;
  int32_T c3_players;
  int32_T c3_b_players;
  int32_T c3_c_players;
  int32_T c3_d_players;
  int32_T c3_e_players;
  int32_T c3_f_players;
  int32_T c3_g_players;
  int32_T c3_h_players;
  real_T c3_b_lenBlue;
  int32_T c3_i72;
  int32_T c3_c_ii;
  int32_T c3_d_ii;
  int32_T c3_i73;
  real_T c3_b_lenRed;
  int32_T c3_i74;
  int32_T c3_d_jj;
  int32_T c3_e_jj;
  int32_T c3_i75;
  int32_T c3_i76;
  real32_T c3_b_lightBlue[2];
  real_T c3_d4;
  real_T c3_d5;
  uint8_T c3_u0;
  int32_T c3_b_index;
  int32_T c3_i77;
  int32_T c3_i78;
  real32_T c3_c_a;
  real32_T c3_c_y;
  int32_T c3_d_tmp_sizes;
  int32_T c3_w_loop_ub;
  int32_T c3_i79;
  int8_T c3_d_tmp_data[7];
  real32_T c3_f2;
  int8_T c3_i80;
  int32_T c3_iv4[2];
  int32_T c3_iv5[2];
  int32_T c3_i_players;
  int32_T c3_j_players;
  int32_T c3_x_loop_ub;
  int32_T c3_i81;
  real32_T c3_d_a;
  real32_T c3_d_y;
  int32_T c3_y_loop_ub;
  int32_T c3_i82;
  real32_T c3_f3;
  int8_T c3_i83;
  int32_T c3_iv6[2];
  int32_T c3_iv7[2];
  int32_T c3_k_players;
  int32_T c3_l_players;
  int32_T c3_ab_loop_ub;
  int32_T c3_i84;
  int32_T c3_e_tmp_sizes;
  int32_T c3_bb_loop_ub;
  int32_T c3_i85;
  int16_T c3_e_tmp_data[7];
  real32_T c3_f4;
  int16_T c3_i86;
  int32_T c3_iv8[2];
  int32_T c3_iv9[2];
  int32_T c3_m_players;
  int32_T c3_n_players;
  int32_T c3_cb_loop_ub;
  int32_T c3_i87;
  int32_T c3_f_tmp_sizes;
  int32_T c3_db_loop_ub;
  int32_T c3_i88;
  uint8_T c3_f_tmp_data[7];
  int32_T c3_iv10[2];
  int32_T c3_iv11[2];
  int32_T c3_o_players;
  int32_T c3_p_players;
  int32_T c3_eb_loop_ub;
  int32_T c3_i89;
  int32_T c3_fb_loop_ub;
  int32_T c3_i90;
  int32_T c3_gb_loop_ub;
  int32_T c3_i91;
  int32_T c3_hb_loop_ub;
  int32_T c3_i92;
  int32_T c3_ib_loop_ub;
  int32_T c3_i93;
  int32_T c3_jb_loop_ub;
  int32_T c3_i94;
  int32_T c3_kb_loop_ub;
  int32_T c3_i95;
  int32_T c3_lb_loop_ub;
  int32_T c3_i96;
  int32_T c3_mb_loop_ub;
  int32_T c3_i97;
  int32_T c3_nb_loop_ub;
  int32_T c3_i98;
  int32_T c3_ob_loop_ub;
  int32_T c3_i99;
  boolean_T guard1 = false;
  c3_b0 = false;
  _SFD_CC_CALL(CHART_ENTER_DURING_FUNCTION_TAG, 1U, chartInstance->c3_sfEvent);
  c3_hoistedGlobal = *chartInstance->c3_height;
  c3_b_blobs_elems_sizes.centroid[0] = chartInstance->
    c3_blobs_elems_sizes->centroid[0];
  c3_b_blobs_elems_sizes.centroid[1] = chartInstance->
    c3_blobs_elems_sizes->centroid[1];
  c3_loop_ub = chartInstance->c3_blobs_elems_sizes->centroid[1] - 1;
  for (c3_i17 = 0; c3_i17 <= c3_loop_ub; c3_i17++) {
    c3_b_loop_ub = chartInstance->c3_blobs_elems_sizes->centroid[0] - 1;
    for (c3_i18 = 0; c3_i18 <= c3_b_loop_ub; c3_i18++) {
      c3_b_blobs_data.centroid[c3_i18 + c3_b_blobs_elems_sizes.centroid[0] *
        c3_i17] = ((real32_T *)&((char_T *)chartInstance->c3_blobs_data)[0])
        [c3_i18 + chartInstance->c3_blobs_elems_sizes->centroid[0] * c3_i17];
    }
  }

  c3_b_blobs_elems_sizes.area = chartInstance->c3_blobs_elems_sizes->area;
  c3_c_loop_ub = chartInstance->c3_blobs_elems_sizes->area - 1;
  for (c3_i19 = 0; c3_i19 <= c3_c_loop_ub; c3_i19++) {
    c3_b_blobs_data.area[c3_i19] = ((int32_T *)&((char_T *)
      chartInstance->c3_blobs_data)[128])[c3_i19];
  }

  c3_b_blobs_elems_sizes.color = chartInstance->c3_blobs_elems_sizes->color;
  c3_d_loop_ub = chartInstance->c3_blobs_elems_sizes->color - 1;
  for (c3_i20 = 0; c3_i20 <= c3_d_loop_ub; c3_i20++) {
    c3_b_blobs_data.color[c3_i20] = ((uint8_T *)&((char_T *)
      chartInstance->c3_blobs_data)[192])[c3_i20];
  }

  for (c3_i21 = 0; c3_i21 < 2; c3_i21++) {
    c3_b_camRes[c3_i21] = (*chartInstance->c3_camRes)[c3_i21];
  }

  c3_b_height = c3_hoistedGlobal;
  _SFD_SYMBOL_SCOPE_PUSH_EML(0U, 29U, 29U, c3_debug_family_names,
    c3_debug_family_var_map);
  _SFD_SYMBOL_SCOPE_ADD_EML_DYN_IMPORTABLE(c3_blobIndices_data, (const int32_T *)
    &c3_blobIndices_sizes, NULL, 0, 0, (void *)c3_k_sf_marshallOut, (void *)
    c3_i_sf_marshallIn);
  _SFD_SYMBOL_SCOPE_ADD_EML_IMPORTABLE(&c3_lenA, 1U, c3_c_sf_marshallOut,
    c3_c_sf_marshallIn);
  _SFD_SYMBOL_SCOPE_ADD_EML(&c3_a, 2U, c3_c_sf_marshallOut);
  _SFD_SYMBOL_SCOPE_ADD_EML_IMPORTABLE(&c3_b, 3U, c3_c_sf_marshallOut,
    c3_c_sf_marshallIn);
  _SFD_SYMBOL_SCOPE_ADD_EML_DYN_IMPORTABLE(c3_allLights_data, (const int32_T *)
    &c3_allLights_sizes, NULL, 0, 4, (void *)c3_j_sf_marshallOut, (void *)
    c3_h_sf_marshallIn);
  _SFD_SYMBOL_SCOPE_ADD_EML_IMPORTABLE(&c3_ii, 5U, c3_c_sf_marshallOut,
    c3_c_sf_marshallIn);
  _SFD_SYMBOL_SCOPE_ADD_EML_IMPORTABLE(&c3_jj, 6U, c3_c_sf_marshallOut,
    c3_c_sf_marshallIn);
  _SFD_SYMBOL_SCOPE_ADD_EML_IMPORTABLE(c3_pxFromCentre, 7U, c3_g_sf_marshallOut,
    c3_e_sf_marshallIn);
  _SFD_SYMBOL_SCOPE_ADD_EML_IMPORTABLE(c3_s, 8U, c3_g_sf_marshallOut,
    c3_e_sf_marshallIn);
  _SFD_SYMBOL_SCOPE_ADD_EML_IMPORTABLE(c3_angles, 9U, c3_g_sf_marshallOut,
    c3_e_sf_marshallIn);
  _SFD_SYMBOL_SCOPE_ADD_EML_IMPORTABLE(c3_pos, 10U, c3_g_sf_marshallOut,
    c3_e_sf_marshallIn);
  _SFD_SYMBOL_SCOPE_ADD_EML_DYN_IMPORTABLE(c3_reds_data, (const int32_T *)
    &c3_reds_sizes, NULL, 0, 11, (void *)c3_i_sf_marshallOut, (void *)
    c3_g_sf_marshallIn);
  _SFD_SYMBOL_SCOPE_ADD_EML_DYN_IMPORTABLE(c3_blues_data, (const int32_T *)
    &c3_blues_sizes, NULL, 0, 12, (void *)c3_i_sf_marshallOut, (void *)
    c3_g_sf_marshallIn);
  _SFD_SYMBOL_SCOPE_ADD_EML_IMPORTABLE(&c3_lenRed, 13U, c3_c_sf_marshallOut,
    c3_c_sf_marshallIn);
  _SFD_SYMBOL_SCOPE_ADD_EML_IMPORTABLE(&c3_lenBlue, 14U, c3_c_sf_marshallOut,
    c3_c_sf_marshallIn);
  _SFD_SYMBOL_SCOPE_ADD_EML_IMPORTABLE(c3_lightBlue, 15U, c3_g_sf_marshallOut,
    c3_e_sf_marshallIn);
  _SFD_SYMBOL_SCOPE_ADD_EML_IMPORTABLE(&c3_dist, 16U, c3_f_sf_marshallOut,
    c3_d_sf_marshallIn);
  _SFD_SYMBOL_SCOPE_ADD_EML_IMPORTABLE(&c3_index, 17U, c3_h_sf_marshallOut,
    c3_f_sf_marshallIn);
  _SFD_SYMBOL_SCOPE_ADD_EML_IMPORTABLE(c3_lightRed, 18U, c3_g_sf_marshallOut,
    c3_e_sf_marshallIn);
  _SFD_SYMBOL_SCOPE_ADD_EML_IMPORTABLE(&c3_temp, 19U, c3_f_sf_marshallOut,
    c3_d_sf_marshallIn);
  _SFD_SYMBOL_SCOPE_ADD_EML_IMPORTABLE(c3_Dpos, 20U, c3_g_sf_marshallOut,
    c3_e_sf_marshallIn);
  _SFD_SYMBOL_SCOPE_ADD_EML_IMPORTABLE(&c3_orientation, 21U, c3_f_sf_marshallOut,
    c3_d_sf_marshallIn);
  _SFD_SYMBOL_SCOPE_ADD_EML_IMPORTABLE(&c3_nargin, 22U, c3_c_sf_marshallOut,
    c3_c_sf_marshallIn);
  _SFD_SYMBOL_SCOPE_ADD_EML_IMPORTABLE(&c3_nargout, 23U, c3_c_sf_marshallOut,
    c3_c_sf_marshallIn);
  _SFD_SYMBOL_SCOPE_ADD_EML_DYN(&c3_b_blobs_data, NULL, &c3_b_blobs_elems_sizes,
    1, 24, (void *)c3_e_sf_marshallOut);
  _SFD_SYMBOL_SCOPE_ADD_EML(c3_b_camRes, 25U, c3_d_sf_marshallOut);
  _SFD_SYMBOL_SCOPE_ADD_EML(&c3_b_height, 26U, c3_c_sf_marshallOut);
  _SFD_SYMBOL_SCOPE_ADD_EML_DYN_IMPORTABLE(&c3_b_players_data, NULL,
    &c3_b_players_elems_sizes, 0, 27, (void *)c3_b_sf_marshallOut, (void *)
    c3_b_sf_marshallIn);
  _SFD_SYMBOL_SCOPE_ADD_EML_DYN_IMPORTABLE(&c3_b_Ball_data, NULL,
    &c3_b_Ball_elems_sizes, 0, 28, (void *)c3_sf_marshallOut, (void *)
    c3_sf_marshallIn);
  CV_EML_FCN(0, 0);
  _SFD_EML_CALL(0U, chartInstance->c3_sfEvent, 2);
  c3_x_sizes = c3_b_blobs_elems_sizes.area;
  c3_e_loop_ub = c3_b_blobs_elems_sizes.area - 1;
  for (c3_i22 = 0; c3_i22 <= c3_e_loop_ub; c3_i22++) {
    c3_x_data[c3_i22] = c3_b_blobs_data.area[c3_i22];
  }

  c3_b_x_sizes = c3_x_sizes;
  c3_f_loop_ub = c3_x_sizes - 1;
  for (c3_i23 = 0; c3_i23 <= c3_f_loop_ub; c3_i23++) {
    c3_b_x_data[c3_i23] = c3_x_data[c3_i23];
  }

  c3_eml_sort(chartInstance, c3_b_x_data, c3_b_x_sizes, c3_y_data, &c3_y_sizes,
              c3_iidx_data, &c3_iidx_sizes);
  c3_b_blobIndices_sizes = c3_iidx_sizes;
  c3_g_loop_ub = c3_iidx_sizes - 1;
  for (c3_i24 = 0; c3_i24 <= c3_g_loop_ub; c3_i24++) {
    c3_b_blobIndices_data[c3_i24] = (real_T)c3_iidx_data[c3_i24];
  }

  c3_blobIndices_sizes = c3_b_blobIndices_sizes;
  c3_h_loop_ub = c3_b_blobIndices_sizes - 1;
  for (c3_i25 = 0; c3_i25 <= c3_h_loop_ub; c3_i25++) {
    c3_blobIndices_data[c3_i25] = c3_b_blobIndices_data[c3_i25];
  }

  _SFD_EML_CALL(0U, chartInstance->c3_sfEvent, 2);
  _SFD_EML_CALL(0U, chartInstance->c3_sfEvent, 6);
  c3_b_blobIndices_sizes = c3_blobIndices_sizes;
  c3_i_loop_ub = c3_blobIndices_sizes - 1;
  for (c3_i26 = 0; c3_i26 <= c3_i_loop_ub; c3_i26++) {
    c3_b_blobIndices_data[c3_i26] = c3_blobIndices_data[c3_i26];
  }

  c3_lenA = (real_T)c3_b_blobIndices_sizes;
  _SFD_EML_CALL(0U, chartInstance->c3_sfEvent, 7);
  _SFD_DIM_SIZE_GEQ_CHECK(2, 0, 1);
  c3_b_Ball_elems_sizes.x = 0;
  _SFD_EML_CALL(0U, chartInstance->c3_sfEvent, 8);
  _SFD_DIM_SIZE_GEQ_CHECK(2, 0, 1);
  c3_b_Ball_elems_sizes.y = 0;
  _SFD_EML_CALL(0U, chartInstance->c3_sfEvent, 9);
  _SFD_EML_CALL(0U, chartInstance->c3_sfEvent, 10);
  _SFD_EML_CALL(0U, chartInstance->c3_sfEvent, 12);
  c3_a = 0.1746;
  _SFD_EML_CALL(0U, chartInstance->c3_sfEvent, 13);
  c3_b = -0.1615;
  _SFD_EML_CALL(0U, chartInstance->c3_sfEvent, 15);
  c3_allLights_sizes[0] = 3;
  c3_iv0[0] = 3;
  c3_iv0[1] = (int32_T)c3_lenA;
  c3_allLights_sizes[1] = c3_iv0[1];
  c3_allLights = c3_allLights_sizes[0];
  c3_b_allLights = c3_allLights_sizes[1];
  c3_j_loop_ub = 3 * (int32_T)c3_lenA - 1;
  for (c3_i27 = 0; c3_i27 <= c3_j_loop_ub; c3_i27++) {
    c3_allLights_data[c3_i27] = 0.0F;
  }

  _SFD_EML_CALL(0U, chartInstance->c3_sfEvent, 17);
  if (CV_EML_IF(0, 1, 0, CV_EML_MCDC(0, 1, 0, !CV_EML_COND(0, 1, 0,
         c3_blobIndices_sizes == 0)))) {
    _SFD_EML_CALL(0U, chartInstance->c3_sfEvent, 18);
    c3_b_lenA = c3_lenA;
    c3_i28 = (int32_T)c3_b_lenA - 1;
    c3_ii = 1.0;
    c3_b_ii = 0;
    while (c3_b_ii <= c3_i28) {
      c3_ii = 1.0 + (real_T)c3_b_ii;
      CV_EML_FOR(0, 1, 0, 1);
      _SFD_EML_CALL(0U, chartInstance->c3_sfEvent, 19);
      c3_jj = c3_blobIndices_data[_SFD_EML_ARRAY_BOUNDS_CHECK("blobIndices",
        (int32_T)c3_ii, 1, c3_blobIndices_sizes, 1, 0) - 1];
      _SFD_EML_CALL(0U, chartInstance->c3_sfEvent, 20);
      c3_i29 = _SFD_EML_ARRAY_BOUNDS_CHECK("blobs.centroid", (int32_T)c3_jj, 1,
        c3_b_blobs_elems_sizes.centroid[0], 1, 0);
      c3_i30 = c3_b_blobs_elems_sizes.centroid[1];
      c3_tmp_sizes = c3_i30;
      c3_k_loop_ub = c3_i30 - 1;
      for (c3_i31 = 0; c3_i31 <= c3_k_loop_ub; c3_i31++) {
        c3_tmp_data[c3_i31] = c3_i31;
      }

      c3_iv1[0] = 1;
      c3_iv1[1] = c3_tmp_sizes;
      if (!c3_b0) {
        for (c3_i32 = 0; c3_i32 < 2; c3_i32++) {
          c3_iv2[c3_i32] = 1 + c3_i32;
        }

        c3_b0 = true;
      }

      _SFD_SUB_ASSIGN_SIZE_CHECK_ND(c3_iv1, 2, c3_iv2, 2);
      (real_T)_SFD_EML_ARRAY_BOUNDS_CHECK("blobs.centroid", 1, 1,
        c3_b_blobs_elems_sizes.centroid[1], 2, 0);
      (real_T)_SFD_EML_ARRAY_BOUNDS_CHECK("blobs.centroid", 2, 1,
        c3_b_blobs_elems_sizes.centroid[1], 2, 0);
      c3_c_camRes[0] = (real32_T)c3_b_camRes[1] -
        c3_b_blobs_data.centroid[_SFD_EML_ARRAY_BOUNDS_CHECK("blobs.centroid",
        (int32_T)c3_jj, 1, c3_b_blobs_elems_sizes.centroid[0], 1, 0) - 1];
      c3_c_camRes[1] = c3_b_blobs_data.centroid[(_SFD_EML_ARRAY_BOUNDS_CHECK(
        "blobs.centroid", (int32_T)c3_jj, 1, c3_b_blobs_elems_sizes.centroid[0],
        1, 0) + c3_b_blobs_elems_sizes.centroid[0]) - 1];
      c3_i33 = c3_i29 - 1;
      c3_iv3[0] = 1;
      c3_iv3[1] = c3_tmp_sizes;
      c3_l_loop_ub = c3_iv3[1] - 1;
      for (c3_i34 = 0; c3_i34 <= c3_l_loop_ub; c3_i34++) {
        c3_b_blobs_data.centroid[c3_i33 + c3_b_blobs_elems_sizes.centroid[0] *
          c3_tmp_data[c3_i34]] = c3_c_camRes[c3_iv3[0] * c3_i34];
      }

      _SFD_EML_CALL(0U, chartInstance->c3_sfEvent, 21);
      c3_i35 = c3_b_blobs_elems_sizes.centroid[1];
      c3_b_jj = _SFD_EML_ARRAY_BOUNDS_CHECK("blobs.centroid", (int32_T)c3_jj, 1,
        c3_b_blobs_elems_sizes.centroid[0], 1, 0) - 1;
      c3_b_tmp_sizes = c3_i35;
      c3_m_loop_ub = c3_i35 - 1;
      for (c3_i36 = 0; c3_i36 <= c3_m_loop_ub; c3_i36++) {
        c3_b_tmp_data[c3_i36] = c3_b_blobs_data.centroid[c3_b_jj +
          c3_b_blobs_elems_sizes.centroid[0] * c3_i36];
      }

      _SFD_SIZE_EQ_CHECK_1D(c3_b_tmp_sizes, 2);
      c3_A[0] = c3_b_camRes[1];
      c3_A[1] = c3_b_camRes[0];
      for (c3_i37 = 0; c3_i37 < 2; c3_i37++) {
        c3_A[c3_i37] /= 2.0;
      }

      for (c3_i38 = 0; c3_i38 < 2; c3_i38++) {
        c3_pxFromCentre[c3_i38] = c3_b_tmp_data[c3_i38] - (real32_T)c3_A[c3_i38];
      }

      _SFD_EML_CALL(0U, chartInstance->c3_sfEvent, 22);
      for (c3_i39 = 0; c3_i39 < 2; c3_i39++) {
        c3_b_b[c3_i39] = c3_pxFromCentre[c3_i39];
      }

      c3_b_sign(chartInstance, c3_b_b);
      for (c3_i40 = 0; c3_i40 < 2; c3_i40++) {
        c3_s[c3_i40] = c3_b_b[c3_i40];
      }

      _SFD_EML_CALL(0U, chartInstance->c3_sfEvent, 23);
      for (c3_i41 = 0; c3_i41 < 2; c3_i41++) {
        c3_b_pxFromCentre[c3_i41] = c3_pxFromCentre[c3_i41];
      }

      c3_abs(chartInstance, c3_b_pxFromCentre, c3_fv0);
      for (c3_i42 = 0; c3_i42 < 2; c3_i42++) {
        c3_pxFromCentre[c3_i42] = c3_fv0[c3_i42];
      }

      _SFD_EML_CALL(0U, chartInstance->c3_sfEvent, 24);
      for (c3_i43 = 0; c3_i43 < 2; c3_i43++) {
        c3_b_b[c3_i43] = c3_pxFromCentre[c3_i43];
      }

      for (c3_i44 = 0; c3_i44 < 2; c3_i44++) {
        c3_b_b[c3_i44] *= 0.1746F;
      }

      for (c3_i45 = 0; c3_i45 < 2; c3_i45++) {
        c3_angles[c3_i45] = c3_b_b[c3_i45] + -0.1615F;
      }

      _SFD_EML_CALL(0U, chartInstance->c3_sfEvent, 25);
      for (c3_i46 = 0; c3_i46 < 2; c3_i46++) {
        c3_angles[c3_i46] *= c3_s[c3_i46];
      }

      _SFD_EML_CALL(0U, chartInstance->c3_sfEvent, 26);
      c3_b_a = c3_b_height;
      for (c3_i47 = 0; c3_i47 < 2; c3_i47++) {
        c3_b_b[c3_i47] = c3_angles[c3_i47];
      }

      c3_b_tand(chartInstance, c3_b_b);
      for (c3_i48 = 0; c3_i48 < 2; c3_i48++) {
        c3_pos[c3_i48] = (real32_T)c3_b_a * c3_b_b[c3_i48];
      }

      _SFD_EML_CALL(0U, chartInstance->c3_sfEvent, 27);
      c3_c_allLights = c3_allLights_sizes[1];
      c3_c_jj = _SFD_EML_ARRAY_BOUNDS_CHECK("allLights", (int32_T)c3_jj, 1,
        c3_c_allLights, 2, 0) - 1;
      c3_allLights_data[c3_allLights_sizes[0] * c3_c_jj] = c3_pos[1];
      c3_allLights_data[1 + c3_allLights_sizes[0] * c3_c_jj] = c3_pos[0];
      c3_allLights_data[2 + c3_allLights_sizes[0] * c3_c_jj] = (real32_T)
        c3_b_blobs_data.color[_SFD_EML_ARRAY_BOUNDS_CHECK("blobs.color",
        (int32_T)c3_jj, 1, c3_b_blobs_elems_sizes.color, 1, 0) - 1];
      c3_b_ii++;
      _SF_MEX_LISTEN_FOR_CTRL_C(chartInstance->S);
    }

    CV_EML_FOR(0, 1, 0, 0);
    _SFD_EML_CALL(0U, chartInstance->c3_sfEvent, 29);
    c3_d0 = (real_T)c3_b_blobs_data.area[_SFD_EML_ARRAY_BOUNDS_CHECK(
      "blobs.area", (int32_T)c3_blobIndices_data[_SFD_EML_ARRAY_BOUNDS_CHECK(
      "blobIndices", (int32_T)c3_lenA, 1, c3_blobIndices_sizes, 1, 0) - 1], 1,
      c3_b_blobs_elems_sizes.area, 1, 0) - 1];
    if (CV_EML_IF(0, 1, 1, CV_RELATIONAL_EVAL(4U, 0U, 0, c3_d0, 50.0, -1, 4U,
          c3_d0 > 50.0))) {
      _SFD_EML_CALL(0U, chartInstance->c3_sfEvent, 30);
      c3_c_b = c3_allLights_data[c3_allLights_sizes[0] *
        (_SFD_EML_ARRAY_BOUNDS_CHECK("allLights", (int32_T)
          c3_blobIndices_data[_SFD_EML_ARRAY_BOUNDS_CHECK("blobIndices",
           (int32_T)c3_lenA, 1, c3_blobIndices_sizes, 1, 0) - 1], 1,
          c3_allLights_sizes[1], 2, 0) - 1)];
      c3_y = 100.0F * c3_c_b;
      c3_b_Ball_elems_sizes.x = 1;
      c3_f0 = muSingleScalarRound(c3_y);
      if (c3_f0 < 128.0F) {
        if (CV_SATURATION_EVAL(4, 0, 4, 1, c3_f0 >= -128.0F)) {
          c3_i49 = (int8_T)c3_f0;
        } else {
          c3_i49 = MIN_int8_T;
        }
      } else if (CV_SATURATION_EVAL(4, 0, 4, 0, c3_f0 >= 128.0F)) {
        c3_i49 = MAX_int8_T;
      } else {
        c3_i49 = 0;
      }

      c3_b_Ball_data.x[0] = c3_i49;
      _SFD_EML_CALL(0U, chartInstance->c3_sfEvent, 31);
      c3_d_b = c3_allLights_data[1 + c3_allLights_sizes[0] *
        (_SFD_EML_ARRAY_BOUNDS_CHECK("allLights", (int32_T)
          c3_blobIndices_data[_SFD_EML_ARRAY_BOUNDS_CHECK("blobIndices",
           (int32_T)c3_lenA, 1, c3_blobIndices_sizes, 1, 0) - 1], 1,
          c3_allLights_sizes[1], 2, 0) - 1)];
      c3_b_y = 100.0F * c3_d_b;
      c3_b_Ball_elems_sizes.y = 1;
      c3_f1 = muSingleScalarRound(c3_b_y);
      if (c3_f1 < 128.0F) {
        if (CV_SATURATION_EVAL(4, 0, 5, 1, c3_f1 >= -128.0F)) {
          c3_i50 = (int8_T)c3_f1;
        } else {
          c3_i50 = MIN_int8_T;
        }
      } else if (CV_SATURATION_EVAL(4, 0, 5, 0, c3_f1 >= 128.0F)) {
        c3_i50 = MAX_int8_T;
      } else {
        c3_i50 = 0;
      }

      c3_b_Ball_data.y[0] = c3_i50;
      _SFD_EML_CALL(0U, chartInstance->c3_sfEvent, 32);
      c3_d1 = c3_blobIndices_data[_SFD_EML_ARRAY_BOUNDS_CHECK("blobIndices",
        (int32_T)c3_lenA, 1, c3_blobIndices_sizes, 1, 0) - 1] - 1.0;
      c3_b1 = (1.0 > c3_d1);
      c3_b2 = c3_b1;
      c3_b3 = c3_b2;
      if (c3_b3) {
        c3_i51 = 0;
      } else {
        (real_T)_SFD_EML_ARRAY_BOUNDS_CHECK("allLights", 1, 1,
          c3_allLights_sizes[1], 0, 0);
        c3_i51 = _SFD_EML_ARRAY_BOUNDS_CHECK("allLights", (int32_T)c3_d1, 1,
          c3_allLights_sizes[1], 0, 0);
      }

      c3_d2 = c3_blobIndices_data[_SFD_EML_ARRAY_BOUNDS_CHECK("blobIndices",
        (int32_T)c3_lenA, 1, c3_blobIndices_sizes, 1, 0) - 1] + 1.0;
      c3_d3 = (real_T)c3_allLights_sizes[1];
      c3_b4 = (c3_d2 > c3_d3);
      c3_b5 = c3_b4;
      c3_b6 = c3_b5;
      if (c3_b6) {
        c3_i52 = 1;
        c3_i53 = 0;
      } else {
        c3_i52 = _SFD_EML_ARRAY_BOUNDS_CHECK("allLights", (int32_T)c3_d2, 1,
          c3_allLights_sizes[1], 0, 0);
        c3_i53 = _SFD_EML_ARRAY_BOUNDS_CHECK("allLights", (int32_T)c3_d3, 1,
          c3_allLights_sizes[1], 0, 0);
      }

      c3_b_allLights_sizes[0] = 3;
      c3_b_allLights_sizes[1] = ((c3_i51 + c3_i53) - c3_i52) + 1;
      c3_n_loop_ub = c3_i51 - 1;
      for (c3_i54 = 0; c3_i54 <= c3_n_loop_ub; c3_i54++) {
        for (c3_i55 = 0; c3_i55 < 3; c3_i55++) {
          c3_b_allLights_data[c3_i55 + c3_b_allLights_sizes[0] * c3_i54] =
            c3_allLights_data[c3_i55 + c3_allLights_sizes[0] * c3_i54];
        }
      }

      c3_o_loop_ub = c3_i53 - c3_i52;
      for (c3_i56 = 0; c3_i56 <= c3_o_loop_ub; c3_i56++) {
        for (c3_i57 = 0; c3_i57 < 3; c3_i57++) {
          c3_b_allLights_data[c3_i57 + c3_b_allLights_sizes[0] * (c3_i56 +
            c3_i51)] = c3_allLights_data[c3_i57 + c3_allLights_sizes[0] *
            ((c3_i52 + c3_i56) - 1)];
        }
      }

      c3_allLights_sizes[0] = 3;
      c3_allLights_sizes[1] = c3_b_allLights_sizes[1];
      c3_p_loop_ub = c3_b_allLights_sizes[1] - 1;
      for (c3_i58 = 0; c3_i58 <= c3_p_loop_ub; c3_i58++) {
        for (c3_i59 = 0; c3_i59 < 3; c3_i59++) {
          c3_allLights_data[c3_i59 + c3_allLights_sizes[0] * c3_i58] =
            c3_b_allLights_data[c3_i59 + c3_b_allLights_sizes[0] * c3_i58];
        }
      }

      _SFD_EML_CALL(0U, chartInstance->c3_sfEvent, 33);
      c3_lenA--;
    }
  }

  _SFD_EML_CALL(0U, chartInstance->c3_sfEvent, 38);
  c3_i60 = c3_allLights_sizes[1];
  c3_c_allLights_sizes[0] = 1;
  c3_c_allLights_sizes[1] = c3_i60;
  c3_q_loop_ub = c3_i60 - 1;
  for (c3_i61 = 0; c3_i61 <= c3_q_loop_ub; c3_i61++) {
    c3_c_allLights_data[c3_c_allLights_sizes[0] * c3_i61] = (c3_allLights_data[2
      + c3_allLights_sizes[0] * c3_i61] == 1.0F);
  }

  c3_eml_li_find(chartInstance, c3_c_allLights_data, c3_c_allLights_sizes,
                 c3_c_tmp_data, c3_c_tmp_sizes);
  c3_reds_sizes[0] = 2;
  c3_reds_sizes[1] = c3_c_tmp_sizes[1];
  c3_r_loop_ub = c3_c_tmp_sizes[1] - 1;
  for (c3_i62 = 0; c3_i62 <= c3_r_loop_ub; c3_i62++) {
    for (c3_i63 = 0; c3_i63 < 2; c3_i63++) {
      c3_reds_data[c3_i63 + c3_reds_sizes[0] * c3_i62] =
        c3_allLights_data[c3_i63 + c3_allLights_sizes[0] *
        (_SFD_EML_ARRAY_BOUNDS_CHECK("allLights", c3_c_tmp_data[c3_c_tmp_sizes[0]
          * c3_i62], 1, c3_allLights_sizes[1], 2, 0) - 1)];
    }
  }

  _SFD_EML_CALL(0U, chartInstance->c3_sfEvent, 39);
  c3_i64 = c3_allLights_sizes[1];
  c3_d_allLights_sizes[0] = 1;
  c3_d_allLights_sizes[1] = c3_i64;
  c3_s_loop_ub = c3_i64 - 1;
  for (c3_i65 = 0; c3_i65 <= c3_s_loop_ub; c3_i65++) {
    c3_d_allLights_data[c3_d_allLights_sizes[0] * c3_i65] = (c3_allLights_data[2
      + c3_allLights_sizes[0] * c3_i65] == 2.0F);
  }

  c3_eml_li_find(chartInstance, c3_d_allLights_data, c3_d_allLights_sizes,
                 c3_c_tmp_data, c3_c_tmp_sizes);
  c3_blues_sizes[0] = 2;
  c3_blues_sizes[1] = c3_c_tmp_sizes[1];
  c3_t_loop_ub = c3_c_tmp_sizes[1] - 1;
  for (c3_i66 = 0; c3_i66 <= c3_t_loop_ub; c3_i66++) {
    for (c3_i67 = 0; c3_i67 < 2; c3_i67++) {
      c3_blues_data[c3_i67 + c3_blues_sizes[0] * c3_i66] =
        c3_allLights_data[c3_i67 + c3_allLights_sizes[0] *
        (_SFD_EML_ARRAY_BOUNDS_CHECK("allLights", c3_c_tmp_data[c3_c_tmp_sizes[0]
          * c3_i66], 1, c3_allLights_sizes[1], 2, 0) - 1)];
    }
  }

  _SFD_EML_CALL(0U, chartInstance->c3_sfEvent, 41);
  c3_i68 = c3_reds_sizes[1];
  c3_c_x_sizes[0] = 1;
  c3_c_x_sizes[1] = c3_i68;
  c3_u_loop_ub = c3_i68 - 1;
  for (c3_i69 = 0; c3_i69 <= c3_u_loop_ub; c3_i69++) {
    c3_c_x_data[c3_c_x_sizes[0] * c3_i69] = c3_reds_data[c3_reds_sizes[0] *
      c3_i69];
  }

  c3_lenRed = (real_T)c3_c_x_sizes[1];
  _SFD_EML_CALL(0U, chartInstance->c3_sfEvent, 42);
  c3_i70 = c3_blues_sizes[1];
  c3_c_x_sizes[0] = 1;
  c3_c_x_sizes[1] = c3_i70;
  c3_v_loop_ub = c3_i70 - 1;
  for (c3_i71 = 0; c3_i71 <= c3_v_loop_ub; c3_i71++) {
    c3_c_x_data[c3_c_x_sizes[0] * c3_i71] = c3_blues_data[c3_blues_sizes[0] *
      c3_i71];
  }

  c3_lenBlue = (real_T)c3_c_x_sizes[1];
  _SFD_EML_CALL(0U, chartInstance->c3_sfEvent, 44);
  _SFD_DIM_SIZE_GEQ_CHECK(6, 0, 1);
  c3_b_players_elems_sizes.x[0] = 0;
  c3_b_players_elems_sizes.x[1] = 1;
  c3_players = c3_b_players_elems_sizes.x[0];
  c3_b_players = c3_b_players_elems_sizes.x[1];
  _SFD_EML_CALL(0U, chartInstance->c3_sfEvent, 45);
  _SFD_DIM_SIZE_GEQ_CHECK(6, 0, 1);
  c3_b_players_elems_sizes.y[0] = 0;
  c3_b_players_elems_sizes.y[1] = 1;
  c3_c_players = c3_b_players_elems_sizes.y[0];
  c3_d_players = c3_b_players_elems_sizes.y[1];
  _SFD_EML_CALL(0U, chartInstance->c3_sfEvent, 46);
  _SFD_DIM_SIZE_GEQ_CHECK(6, 0, 1);
  c3_b_players_elems_sizes.orientation[0] = 0;
  c3_b_players_elems_sizes.orientation[1] = 1;
  c3_e_players = c3_b_players_elems_sizes.orientation[0];
  c3_f_players = c3_b_players_elems_sizes.orientation[1];
  _SFD_EML_CALL(0U, chartInstance->c3_sfEvent, 47);
  _SFD_DIM_SIZE_GEQ_CHECK(6, 0, 1);
  c3_b_players_elems_sizes.color[0] = 0;
  c3_b_players_elems_sizes.color[1] = 1;
  c3_g_players = c3_b_players_elems_sizes.color[0];
  c3_h_players = c3_b_players_elems_sizes.color[1];
  _SFD_EML_CALL(0U, chartInstance->c3_sfEvent, 54);
  if (CV_EML_IF(0, 1, 2, CV_RELATIONAL_EVAL(4U, 0U, 1, c3_lenRed, 0.0, -1, 4U,
        c3_lenRed > 0.0))) {
    _SFD_EML_CALL(0U, chartInstance->c3_sfEvent, 55);
    if (CV_EML_IF(0, 1, 3, CV_RELATIONAL_EVAL(4U, 0U, 2, c3_lenBlue, 0.0, -1, 4U,
          c3_lenBlue > 0.0))) {
      _SFD_EML_CALL(0U, chartInstance->c3_sfEvent, 56);
      c3_b_lenBlue = c3_lenBlue;
      c3_i72 = (int32_T)c3_b_lenBlue - 1;
      c3_ii = 1.0;
      c3_c_ii = 0;
      while (c3_c_ii <= c3_i72) {
        c3_ii = 1.0 + (real_T)c3_c_ii;
        CV_EML_FOR(0, 1, 1, 1);
        _SFD_EML_CALL(0U, chartInstance->c3_sfEvent, 57);
        c3_d_ii = _SFD_EML_ARRAY_BOUNDS_CHECK("blues", (int32_T)c3_ii, 1,
          c3_blues_sizes[1], 2, 0) - 1;
        for (c3_i73 = 0; c3_i73 < 2; c3_i73++) {
          c3_lightBlue[c3_i73] = c3_blues_data[c3_i73 + c3_blues_sizes[0] *
            c3_d_ii];
        }

        _SFD_EML_CALL(0U, chartInstance->c3_sfEvent, 58);
        c3_dist = 2.14748365E+9F;
        _SFD_EML_CALL(0U, chartInstance->c3_sfEvent, 59);
        c3_index = 0U;
        _SFD_EML_CALL(0U, chartInstance->c3_sfEvent, 60);
        c3_b_lenRed = c3_lenRed;
        c3_i74 = (int32_T)c3_b_lenRed - 1;
        c3_jj = 1.0;
        c3_d_jj = 0;
        while (c3_d_jj <= c3_i74) {
          c3_jj = 1.0 + (real_T)c3_d_jj;
          CV_EML_FOR(0, 1, 2, 1);
          _SFD_EML_CALL(0U, chartInstance->c3_sfEvent, 61);
          c3_e_jj = _SFD_EML_ARRAY_BOUNDS_CHECK("reds", (int32_T)c3_jj, 1,
            c3_reds_sizes[1], 2, 0) - 1;
          for (c3_i75 = 0; c3_i75 < 2; c3_i75++) {
            c3_lightRed[c3_i75] = c3_reds_data[c3_i75 + c3_reds_sizes[0] *
              c3_e_jj];
          }

          _SFD_EML_CALL(0U, chartInstance->c3_sfEvent, 62);
          for (c3_i76 = 0; c3_i76 < 2; c3_i76++) {
            c3_b_lightBlue[c3_i76] = c3_lightBlue[c3_i76] - c3_lightRed[c3_i76];
          }

          c3_temp = c3_norm(chartInstance, c3_b_lightBlue);
          _SFD_EML_CALL(0U, chartInstance->c3_sfEvent, 63);
          c3_d4 = c3_b_abs(chartInstance, c3_temp - 0.04F);
          guard1 = false;
          if (CV_EML_COND(0, 1, 1, CV_RELATIONAL_EVAL(4U, 0U, 3, c3_d4, 0.05, -1,
                2U, c3_d4 < 0.05))) {
            if (CV_EML_COND(0, 1, 2, CV_RELATIONAL_EVAL(4U, 0U, 4, (real_T)
                  c3_temp, (real_T)c3_dist, -3, 2U, c3_temp < c3_dist))) {
              CV_EML_MCDC(0, 1, 1, true);
              CV_EML_IF(0, 1, 4, true);
              _SFD_EML_CALL(0U, chartInstance->c3_sfEvent, 64);
              c3_dist = c3_temp;
              _SFD_EML_CALL(0U, chartInstance->c3_sfEvent, 65);
              c3_d5 = muDoubleScalarRound(c3_jj);
              if (c3_d5 < 256.0) {
                if (CV_SATURATION_EVAL(4, 0, 0, 1, c3_d5 >= 0.0)) {
                  c3_u0 = (uint8_T)c3_d5;
                } else {
                  c3_u0 = 0U;
                }
              } else if (CV_SATURATION_EVAL(4, 0, 0, 0, c3_d5 >= 256.0)) {
                c3_u0 = MAX_uint8_T;
              } else {
                c3_u0 = 0U;
              }

              c3_index = c3_u0;
            } else {
              guard1 = true;
            }
          } else {
            guard1 = true;
          }

          if (guard1 == true) {
            CV_EML_MCDC(0, 1, 1, false);
            CV_EML_IF(0, 1, 4, false);
          }

          c3_d_jj++;
          _SF_MEX_LISTEN_FOR_CTRL_C(chartInstance->S);
        }

        CV_EML_FOR(0, 1, 2, 0);
        _SFD_EML_CALL(0U, chartInstance->c3_sfEvent, 68);
        if (CV_EML_IF(0, 1, 5, CV_RELATIONAL_EVAL(4U, 0U, 5, (real_T)c3_index,
              0.0, 0, 1U, c3_index != 0))) {
          _SFD_EML_CALL(0U, chartInstance->c3_sfEvent, 69);
          c3_b_index = (uint8_T)_SFD_EML_ARRAY_BOUNDS_CHECK("reds", (int32_T)
            c3_index, 1, c3_reds_sizes[1], 2, 0) - 1;
          for (c3_i77 = 0; c3_i77 < 2; c3_i77++) {
            c3_lightRed[c3_i77] = c3_reds_data[c3_i77 + c3_reds_sizes[0] *
              c3_b_index];
          }

          _SFD_EML_CALL(0U, chartInstance->c3_sfEvent, 70);
          for (c3_i78 = 0; c3_i78 < 2; c3_i78++) {
            c3_Dpos[c3_i78] = c3_lightBlue[c3_i78] - c3_lightRed[c3_i78];
          }

          _SFD_EML_CALL(0U, chartInstance->c3_sfEvent, 71);
          c3_orientation = c3_atan2d(chartInstance, c3_Dpos[1], c3_Dpos[0]);
          _SFD_EML_CALL(0U, chartInstance->c3_sfEvent, 72);
          c3_c_a = c3_lightRed[0];
          c3_c_y = c3_c_a * 100.0F;
          c3_d_tmp_sizes = c3_b_players_elems_sizes.x[0] + 1;
          c3_w_loop_ub = c3_b_players_elems_sizes.x[0] - 1;
          for (c3_i79 = 0; c3_i79 <= c3_w_loop_ub; c3_i79++) {
            c3_d_tmp_data[c3_i79] = c3_b_players_data.x[c3_i79];
          }

          c3_f2 = muSingleScalarRound(c3_c_y);
          if (c3_f2 < 128.0F) {
            if (CV_SATURATION_EVAL(4, 0, 1, 1, c3_f2 >= -128.0F)) {
              c3_i80 = (int8_T)c3_f2;
            } else {
              c3_i80 = MIN_int8_T;
            }
          } else if (CV_SATURATION_EVAL(4, 0, 1, 0, c3_f2 >= 128.0F)) {
            c3_i80 = MAX_int8_T;
          } else {
            c3_i80 = 0;
          }

          c3_d_tmp_data[c3_b_players_elems_sizes.x[0]] = c3_i80;
          _SFD_DIM_SIZE_GEQ_CHECK(6, c3_d_tmp_sizes, 1);
          c3_iv4[0] = c3_d_tmp_sizes;
          c3_iv4[1] = 1;
          c3_b_players_elems_sizes.x[0] = c3_iv4[0];
          c3_iv5[0] = c3_d_tmp_sizes;
          c3_iv5[1] = 1;
          c3_b_players_elems_sizes.x[1] = c3_iv5[1];
          c3_i_players = c3_b_players_elems_sizes.x[0];
          c3_j_players = c3_b_players_elems_sizes.x[1];
          c3_x_loop_ub = c3_d_tmp_sizes - 1;
          for (c3_i81 = 0; c3_i81 <= c3_x_loop_ub; c3_i81++) {
            c3_b_players_data.x[c3_i81] = c3_d_tmp_data[c3_i81];
          }

          _SFD_EML_CALL(0U, chartInstance->c3_sfEvent, 73);
          c3_d_a = c3_lightRed[1];
          c3_d_y = c3_d_a * 100.0F;
          c3_d_tmp_sizes = c3_b_players_elems_sizes.y[0] + 1;
          c3_y_loop_ub = c3_b_players_elems_sizes.y[0] - 1;
          for (c3_i82 = 0; c3_i82 <= c3_y_loop_ub; c3_i82++) {
            c3_d_tmp_data[c3_i82] = c3_b_players_data.y[c3_i82];
          }

          c3_f3 = muSingleScalarRound(c3_d_y);
          if (c3_f3 < 128.0F) {
            if (CV_SATURATION_EVAL(4, 0, 2, 1, c3_f3 >= -128.0F)) {
              c3_i83 = (int8_T)c3_f3;
            } else {
              c3_i83 = MIN_int8_T;
            }
          } else if (CV_SATURATION_EVAL(4, 0, 2, 0, c3_f3 >= 128.0F)) {
            c3_i83 = MAX_int8_T;
          } else {
            c3_i83 = 0;
          }

          c3_d_tmp_data[c3_b_players_elems_sizes.y[0]] = c3_i83;
          _SFD_DIM_SIZE_GEQ_CHECK(6, c3_d_tmp_sizes, 1);
          c3_iv6[0] = c3_d_tmp_sizes;
          c3_iv6[1] = 1;
          c3_b_players_elems_sizes.y[0] = c3_iv6[0];
          c3_iv7[0] = c3_d_tmp_sizes;
          c3_iv7[1] = 1;
          c3_b_players_elems_sizes.y[1] = c3_iv7[1];
          c3_k_players = c3_b_players_elems_sizes.y[0];
          c3_l_players = c3_b_players_elems_sizes.y[1];
          c3_ab_loop_ub = c3_d_tmp_sizes - 1;
          for (c3_i84 = 0; c3_i84 <= c3_ab_loop_ub; c3_i84++) {
            c3_b_players_data.y[c3_i84] = c3_d_tmp_data[c3_i84];
          }

          _SFD_EML_CALL(0U, chartInstance->c3_sfEvent, 74);
          c3_e_tmp_sizes = c3_b_players_elems_sizes.orientation[0] + 1;
          c3_bb_loop_ub = c3_b_players_elems_sizes.orientation[0] - 1;
          for (c3_i85 = 0; c3_i85 <= c3_bb_loop_ub; c3_i85++) {
            c3_e_tmp_data[c3_i85] = c3_b_players_data.orientation[c3_i85];
          }

          c3_f4 = muSingleScalarRound(c3_orientation);
          if (c3_f4 < 32768.0F) {
            if (CV_SATURATION_EVAL(4, 0, 3, 1, c3_f4 >= -32768.0F)) {
              c3_i86 = (int16_T)c3_f4;
            } else {
              c3_i86 = MIN_int16_T;
            }
          } else if (CV_SATURATION_EVAL(4, 0, 3, 0, c3_f4 >= 32768.0F)) {
            c3_i86 = MAX_int16_T;
          } else {
            c3_i86 = 0;
          }

          c3_e_tmp_data[c3_b_players_elems_sizes.orientation[0]] = c3_i86;
          _SFD_DIM_SIZE_GEQ_CHECK(6, c3_e_tmp_sizes, 1);
          c3_iv8[0] = c3_e_tmp_sizes;
          c3_iv8[1] = 1;
          c3_b_players_elems_sizes.orientation[0] = c3_iv8[0];
          c3_iv9[0] = c3_e_tmp_sizes;
          c3_iv9[1] = 1;
          c3_b_players_elems_sizes.orientation[1] = c3_iv9[1];
          c3_m_players = c3_b_players_elems_sizes.orientation[0];
          c3_n_players = c3_b_players_elems_sizes.orientation[1];
          c3_cb_loop_ub = c3_e_tmp_sizes - 1;
          for (c3_i87 = 0; c3_i87 <= c3_cb_loop_ub; c3_i87++) {
            c3_b_players_data.orientation[c3_i87] = c3_e_tmp_data[c3_i87];
          }

          _SFD_EML_CALL(0U, chartInstance->c3_sfEvent, 75);
          c3_f_tmp_sizes = c3_b_players_elems_sizes.color[0] + 1;
          c3_db_loop_ub = c3_b_players_elems_sizes.color[0] - 1;
          for (c3_i88 = 0; c3_i88 <= c3_db_loop_ub; c3_i88++) {
            c3_f_tmp_data[c3_i88] = c3_b_players_data.color[c3_i88];
          }

          c3_f_tmp_data[c3_b_players_elems_sizes.color[0]] = 0U;
          _SFD_DIM_SIZE_GEQ_CHECK(6, c3_f_tmp_sizes, 1);
          c3_iv10[0] = c3_f_tmp_sizes;
          c3_iv10[1] = 1;
          c3_b_players_elems_sizes.color[0] = c3_iv10[0];
          c3_iv11[0] = c3_f_tmp_sizes;
          c3_iv11[1] = 1;
          c3_b_players_elems_sizes.color[1] = c3_iv11[1];
          c3_o_players = c3_b_players_elems_sizes.color[0];
          c3_p_players = c3_b_players_elems_sizes.color[1];
          c3_eb_loop_ub = c3_f_tmp_sizes - 1;
          for (c3_i89 = 0; c3_i89 <= c3_eb_loop_ub; c3_i89++) {
            c3_b_players_data.color[c3_i89] = c3_f_tmp_data[c3_i89];
          }
        }

        c3_c_ii++;
        _SF_MEX_LISTEN_FOR_CTRL_C(chartInstance->S);
      }

      CV_EML_FOR(0, 1, 1, 0);
    }
  }

  _SFD_EML_CALL(0U, chartInstance->c3_sfEvent, -75);
  _SFD_SYMBOL_SCOPE_POP();
  chartInstance->c3_players_elems_sizes->x[0] = c3_b_players_elems_sizes.x[0];
  chartInstance->c3_players_elems_sizes->x[1] = c3_b_players_elems_sizes.x[1];
  c3_fb_loop_ub = c3_b_players_elems_sizes.x[1] - 1;
  for (c3_i90 = 0; c3_i90 <= c3_fb_loop_ub; c3_i90++) {
    c3_gb_loop_ub = c3_b_players_elems_sizes.x[0] - 1;
    for (c3_i91 = 0; c3_i91 <= c3_gb_loop_ub; c3_i91++) {
      ((int8_T *)&((char_T *)chartInstance->c3_players_data)[0])[c3_i91 +
        chartInstance->c3_players_elems_sizes->x[0] * c3_i90] =
        c3_b_players_data.x[c3_i91 + c3_b_players_elems_sizes.x[0] * c3_i90];
    }
  }

  chartInstance->c3_players_elems_sizes->y[0] = c3_b_players_elems_sizes.y[0];
  chartInstance->c3_players_elems_sizes->y[1] = c3_b_players_elems_sizes.y[1];
  c3_hb_loop_ub = c3_b_players_elems_sizes.y[1] - 1;
  for (c3_i92 = 0; c3_i92 <= c3_hb_loop_ub; c3_i92++) {
    c3_ib_loop_ub = c3_b_players_elems_sizes.y[0] - 1;
    for (c3_i93 = 0; c3_i93 <= c3_ib_loop_ub; c3_i93++) {
      ((int8_T *)&((char_T *)chartInstance->c3_players_data)[6])[c3_i93 +
        chartInstance->c3_players_elems_sizes->y[0] * c3_i92] =
        c3_b_players_data.y[c3_i93 + c3_b_players_elems_sizes.y[0] * c3_i92];
    }
  }

  chartInstance->c3_players_elems_sizes->orientation[0] =
    c3_b_players_elems_sizes.orientation[0];
  chartInstance->c3_players_elems_sizes->orientation[1] =
    c3_b_players_elems_sizes.orientation[1];
  c3_jb_loop_ub = c3_b_players_elems_sizes.orientation[1] - 1;
  for (c3_i94 = 0; c3_i94 <= c3_jb_loop_ub; c3_i94++) {
    c3_kb_loop_ub = c3_b_players_elems_sizes.orientation[0] - 1;
    for (c3_i95 = 0; c3_i95 <= c3_kb_loop_ub; c3_i95++) {
      ((int16_T *)&((char_T *)chartInstance->c3_players_data)[12])[c3_i95 +
        chartInstance->c3_players_elems_sizes->orientation[0] * c3_i94] =
        c3_b_players_data.orientation[c3_i95 +
        c3_b_players_elems_sizes.orientation[0] * c3_i94];
    }
  }

  chartInstance->c3_players_elems_sizes->color[0] =
    c3_b_players_elems_sizes.color[0];
  chartInstance->c3_players_elems_sizes->color[1] =
    c3_b_players_elems_sizes.color[1];
  c3_lb_loop_ub = c3_b_players_elems_sizes.color[1] - 1;
  for (c3_i96 = 0; c3_i96 <= c3_lb_loop_ub; c3_i96++) {
    c3_mb_loop_ub = c3_b_players_elems_sizes.color[0] - 1;
    for (c3_i97 = 0; c3_i97 <= c3_mb_loop_ub; c3_i97++) {
      ((uint8_T *)&((char_T *)chartInstance->c3_players_data)[24])[c3_i97 +
        chartInstance->c3_players_elems_sizes->color[0] * c3_i96] =
        c3_b_players_data.color[c3_i97 + c3_b_players_elems_sizes.color[0] *
        c3_i96];
    }
  }

  chartInstance->c3_Ball_elems_sizes->x = c3_b_Ball_elems_sizes.x;
  c3_nb_loop_ub = c3_b_Ball_elems_sizes.x - 1;
  for (c3_i98 = 0; c3_i98 <= c3_nb_loop_ub; c3_i98++) {
    ((int8_T *)&((char_T *)chartInstance->c3_Ball_data)[0])[c3_i98] =
      c3_b_Ball_data.x[c3_i98];
  }

  chartInstance->c3_Ball_elems_sizes->y = c3_b_Ball_elems_sizes.y;
  c3_ob_loop_ub = c3_b_Ball_elems_sizes.y - 1;
  for (c3_i99 = 0; c3_i99 <= c3_ob_loop_ub; c3_i99++) {
    ((int8_T *)&((char_T *)chartInstance->c3_Ball_data)[2])[c3_i99] =
      c3_b_Ball_data.y[c3_i99];
  }

  _SFD_CC_CALL(EXIT_OUT_OF_FUNCTION_TAG, 1U, chartInstance->c3_sfEvent);
}

static void initSimStructsc3_ImageDet(SFc3_ImageDetInstanceStruct *chartInstance)
{
  (void)chartInstance;
}

static void init_script_number_translation(uint32_T c3_machineNumber, uint32_T
  c3_chartNumber, uint32_T c3_instanceNumber)
{
  (void)c3_machineNumber;
  (void)c3_chartNumber;
  (void)c3_instanceNumber;
}

static const mxArray *c3_sf_marshallOut(void *chartInstanceVoid, c3_BallData
  *c3_inData_data, c3_BallData_size *c3_inData_elems_sizes)
{
  const mxArray *c3_mxArrayOutData = NULL;
  c3_BallData_size c3_u_elems_sizes;
  c3_BallData c3_u_data;
  const mxArray *c3_y = NULL;
  int32_T c3_u_sizes;
  int32_T c3_loop_ub;
  int32_T c3_i100;
  int8_T c3_b_u_data[2];
  const mxArray *c3_b_y = NULL;
  int32_T c3_b_u_sizes;
  int32_T c3_b_loop_ub;
  int32_T c3_i101;
  int8_T c3_c_u_data[2];
  const mxArray *c3_c_y = NULL;
  SFc3_ImageDetInstanceStruct *chartInstance;
  chartInstance = (SFc3_ImageDetInstanceStruct *)chartInstanceVoid;
  c3_mxArrayOutData = NULL;
  c3_u_elems_sizes = *c3_inData_elems_sizes;
  c3_u_data = *c3_inData_data;
  c3_y = NULL;
  sf_mex_assign(&c3_y, sf_mex_createstruct("structure", 2, 1, 1), false);
  c3_u_sizes = c3_u_elems_sizes.x;
  c3_loop_ub = c3_u_elems_sizes.x - 1;
  for (c3_i100 = 0; c3_i100 <= c3_loop_ub; c3_i100++) {
    c3_b_u_data[c3_i100] = c3_u_data.x[c3_i100];
  }

  c3_b_y = NULL;
  sf_mex_assign(&c3_b_y, sf_mex_create("y", c3_b_u_data, 2, 0U, 1U, 0U, 1,
    c3_u_sizes), false);
  sf_mex_addfield(c3_y, c3_b_y, "x", "x", 0);
  c3_b_u_sizes = c3_u_elems_sizes.y;
  c3_b_loop_ub = c3_u_elems_sizes.y - 1;
  for (c3_i101 = 0; c3_i101 <= c3_b_loop_ub; c3_i101++) {
    c3_c_u_data[c3_i101] = c3_u_data.y[c3_i101];
  }

  c3_c_y = NULL;
  sf_mex_assign(&c3_c_y, sf_mex_create("y", c3_c_u_data, 2, 0U, 1U, 0U, 1,
    c3_b_u_sizes), false);
  sf_mex_addfield(c3_y, c3_c_y, "y", "y", 0);
  sf_mex_assign(&c3_mxArrayOutData, c3_y, false);
  return c3_mxArrayOutData;
}

static void c3_emlrt_marshallIn(SFc3_ImageDetInstanceStruct *chartInstance,
  const mxArray *c3_Ball, const char_T *c3_identifier, c3_BallData *c3_y_data,
  c3_BallData_size *c3_y_elems_sizes)
{
  emlrtMsgIdentifier c3_thisId;
  c3_thisId.fIdentifier = c3_identifier;
  c3_thisId.fParent = NULL;
  c3_b_emlrt_marshallIn(chartInstance, sf_mex_dup(c3_Ball), &c3_thisId,
                        c3_y_data, c3_y_elems_sizes);
  sf_mex_destroy(&c3_Ball);
}

static void c3_b_emlrt_marshallIn(SFc3_ImageDetInstanceStruct *chartInstance,
  const mxArray *c3_u, const emlrtMsgIdentifier *c3_parentId, c3_BallData
  *c3_y_data, c3_BallData_size *c3_y_elems_sizes)
{
  emlrtMsgIdentifier c3_thisId;
  static const char * c3_fieldNames[2] = { "x", "y" };

  c3_thisId.fParent = c3_parentId;
  sf_mex_check_struct(c3_parentId, c3_u, 2, c3_fieldNames, 0U, NULL);
  c3_thisId.fIdentifier = "x";
  c3_c_emlrt_marshallIn(chartInstance, sf_mex_dup(sf_mex_getfield(c3_u, "x", "x",
    0)), &c3_thisId, c3_y_data->x, &c3_y_elems_sizes->x);
  c3_thisId.fIdentifier = "y";
  c3_c_emlrt_marshallIn(chartInstance, sf_mex_dup(sf_mex_getfield(c3_u, "y", "y",
    0)), &c3_thisId, c3_y_data->y, &c3_y_elems_sizes->y);
  sf_mex_destroy(&c3_u);
}

static void c3_c_emlrt_marshallIn(SFc3_ImageDetInstanceStruct *chartInstance,
  const mxArray *c3_u, const emlrtMsgIdentifier *c3_parentId, int8_T c3_y_data[],
  int32_T *c3_y_sizes)
{
  static uint32_T c3_uv0[1] = { 2U };

  uint32_T c3_uv1[1];
  static boolean_T c3_bv0[1] = { true };

  boolean_T c3_bv1[1];
  int32_T c3_tmp_sizes;
  int8_T c3_tmp_data[2];
  int32_T c3_loop_ub;
  int32_T c3_i102;
  (void)chartInstance;
  c3_uv1[0] = c3_uv0[0];
  c3_bv1[0] = c3_bv0[0];
  sf_mex_import_vs(c3_parentId, sf_mex_dup(c3_u), c3_tmp_data, 1, 2, 0U, 1, 0U,
                   1, c3_bv1, c3_uv1, &c3_tmp_sizes);
  *c3_y_sizes = c3_tmp_sizes;
  c3_loop_ub = c3_tmp_sizes - 1;
  for (c3_i102 = 0; c3_i102 <= c3_loop_ub; c3_i102++) {
    c3_y_data[c3_i102] = c3_tmp_data[c3_i102];
  }

  sf_mex_destroy(&c3_u);
}

static void c3_sf_marshallIn(void *chartInstanceVoid, const mxArray
  *c3_mxArrayInData, const char_T *c3_varName, c3_BallData *c3_outData_data,
  c3_BallData_size *c3_outData_elems_sizes)
{
  const mxArray *c3_Ball;
  const char_T *c3_identifier;
  emlrtMsgIdentifier c3_thisId;
  c3_BallData_size c3_y_elems_sizes;
  c3_BallData c3_y_data;
  SFc3_ImageDetInstanceStruct *chartInstance;
  chartInstance = (SFc3_ImageDetInstanceStruct *)chartInstanceVoid;
  c3_Ball = sf_mex_dup(c3_mxArrayInData);
  c3_identifier = c3_varName;
  c3_thisId.fIdentifier = c3_identifier;
  c3_thisId.fParent = NULL;
  c3_b_emlrt_marshallIn(chartInstance, sf_mex_dup(c3_Ball), &c3_thisId,
                        &c3_y_data, &c3_y_elems_sizes);
  sf_mex_destroy(&c3_Ball);
  *c3_outData_elems_sizes = c3_y_elems_sizes;
  *c3_outData_data = c3_y_data;
  sf_mex_destroy(&c3_mxArrayInData);
}

static const mxArray *c3_b_sf_marshallOut(void *chartInstanceVoid, c3_PlayerData
  *c3_inData_data, c3_PlayerData_size *c3_inData_elems_sizes)
{
  const mxArray *c3_mxArrayOutData = NULL;
  c3_PlayerData_size c3_u_elems_sizes;
  c3_PlayerData c3_u_data;
  const mxArray *c3_y = NULL;
  int32_T c3_u_sizes[2];
  int32_T c3_u;
  int32_T c3_b_u;
  int32_T c3_loop_ub;
  int32_T c3_i103;
  int8_T c3_b_u_data[6];
  const mxArray *c3_b_y = NULL;
  int32_T c3_b_u_sizes[2];
  int32_T c3_c_u;
  int32_T c3_d_u;
  int32_T c3_b_loop_ub;
  int32_T c3_i104;
  int8_T c3_c_u_data[6];
  const mxArray *c3_c_y = NULL;
  int32_T c3_c_u_sizes[2];
  int32_T c3_e_u;
  int32_T c3_f_u;
  int32_T c3_c_loop_ub;
  int32_T c3_i105;
  int16_T c3_d_u_data[6];
  const mxArray *c3_d_y = NULL;
  int32_T c3_d_u_sizes[2];
  int32_T c3_g_u;
  int32_T c3_h_u;
  int32_T c3_d_loop_ub;
  int32_T c3_i106;
  uint8_T c3_e_u_data[6];
  const mxArray *c3_e_y = NULL;
  SFc3_ImageDetInstanceStruct *chartInstance;
  chartInstance = (SFc3_ImageDetInstanceStruct *)chartInstanceVoid;
  c3_mxArrayOutData = NULL;
  c3_u_elems_sizes = *c3_inData_elems_sizes;
  c3_u_data = *c3_inData_data;
  c3_y = NULL;
  sf_mex_assign(&c3_y, sf_mex_createstruct("structure", 2, 1, 1), false);
  c3_u_sizes[0] = c3_u_elems_sizes.x[0];
  c3_u_sizes[1] = c3_u_elems_sizes.x[1];
  c3_u = c3_u_sizes[0];
  c3_b_u = c3_u_sizes[1];
  c3_loop_ub = c3_u_elems_sizes.x[0] * c3_u_elems_sizes.x[1] - 1;
  for (c3_i103 = 0; c3_i103 <= c3_loop_ub; c3_i103++) {
    c3_b_u_data[c3_i103] = c3_u_data.x[c3_i103];
  }

  c3_b_y = NULL;
  sf_mex_assign(&c3_b_y, sf_mex_create("y", c3_b_u_data, 2, 0U, 1U, 0U, 2,
    c3_u_sizes[0], c3_u_sizes[1]), false);
  sf_mex_addfield(c3_y, c3_b_y, "x", "x", 0);
  c3_b_u_sizes[0] = c3_u_elems_sizes.y[0];
  c3_b_u_sizes[1] = c3_u_elems_sizes.y[1];
  c3_c_u = c3_b_u_sizes[0];
  c3_d_u = c3_b_u_sizes[1];
  c3_b_loop_ub = c3_u_elems_sizes.y[0] * c3_u_elems_sizes.y[1] - 1;
  for (c3_i104 = 0; c3_i104 <= c3_b_loop_ub; c3_i104++) {
    c3_c_u_data[c3_i104] = c3_u_data.y[c3_i104];
  }

  c3_c_y = NULL;
  sf_mex_assign(&c3_c_y, sf_mex_create("y", c3_c_u_data, 2, 0U, 1U, 0U, 2,
    c3_b_u_sizes[0], c3_b_u_sizes[1]), false);
  sf_mex_addfield(c3_y, c3_c_y, "y", "y", 0);
  c3_c_u_sizes[0] = c3_u_elems_sizes.orientation[0];
  c3_c_u_sizes[1] = c3_u_elems_sizes.orientation[1];
  c3_e_u = c3_c_u_sizes[0];
  c3_f_u = c3_c_u_sizes[1];
  c3_c_loop_ub = c3_u_elems_sizes.orientation[0] * c3_u_elems_sizes.orientation
    [1] - 1;
  for (c3_i105 = 0; c3_i105 <= c3_c_loop_ub; c3_i105++) {
    c3_d_u_data[c3_i105] = c3_u_data.orientation[c3_i105];
  }

  c3_d_y = NULL;
  sf_mex_assign(&c3_d_y, sf_mex_create("y", c3_d_u_data, 4, 0U, 1U, 0U, 2,
    c3_c_u_sizes[0], c3_c_u_sizes[1]), false);
  sf_mex_addfield(c3_y, c3_d_y, "orientation", "orientation", 0);
  c3_d_u_sizes[0] = c3_u_elems_sizes.color[0];
  c3_d_u_sizes[1] = c3_u_elems_sizes.color[1];
  c3_g_u = c3_d_u_sizes[0];
  c3_h_u = c3_d_u_sizes[1];
  c3_d_loop_ub = c3_u_elems_sizes.color[0] * c3_u_elems_sizes.color[1] - 1;
  for (c3_i106 = 0; c3_i106 <= c3_d_loop_ub; c3_i106++) {
    c3_e_u_data[c3_i106] = c3_u_data.color[c3_i106];
  }

  c3_e_y = NULL;
  sf_mex_assign(&c3_e_y, sf_mex_create("y", c3_e_u_data, 3, 0U, 1U, 0U, 2,
    c3_d_u_sizes[0], c3_d_u_sizes[1]), false);
  sf_mex_addfield(c3_y, c3_e_y, "color", "color", 0);
  sf_mex_assign(&c3_mxArrayOutData, c3_y, false);
  return c3_mxArrayOutData;
}

static void c3_d_emlrt_marshallIn(SFc3_ImageDetInstanceStruct *chartInstance,
  const mxArray *c3_players, const char_T *c3_identifier, c3_PlayerData
  *c3_y_data, c3_PlayerData_size *c3_y_elems_sizes)
{
  emlrtMsgIdentifier c3_thisId;
  c3_thisId.fIdentifier = c3_identifier;
  c3_thisId.fParent = NULL;
  c3_e_emlrt_marshallIn(chartInstance, sf_mex_dup(c3_players), &c3_thisId,
                        c3_y_data, c3_y_elems_sizes);
  sf_mex_destroy(&c3_players);
}

static void c3_e_emlrt_marshallIn(SFc3_ImageDetInstanceStruct *chartInstance,
  const mxArray *c3_u, const emlrtMsgIdentifier *c3_parentId, c3_PlayerData
  *c3_y_data, c3_PlayerData_size *c3_y_elems_sizes)
{
  emlrtMsgIdentifier c3_thisId;
  static const char * c3_fieldNames[4] = { "x", "y", "orientation", "color" };

  c3_thisId.fParent = c3_parentId;
  sf_mex_check_struct(c3_parentId, c3_u, 4, c3_fieldNames, 0U, NULL);
  c3_thisId.fIdentifier = "x";
  c3_f_emlrt_marshallIn(chartInstance, sf_mex_dup(sf_mex_getfield(c3_u, "x", "x",
    0)), &c3_thisId, c3_y_data->x, c3_y_elems_sizes->x);
  c3_thisId.fIdentifier = "y";
  c3_f_emlrt_marshallIn(chartInstance, sf_mex_dup(sf_mex_getfield(c3_u, "y", "y",
    0)), &c3_thisId, c3_y_data->y, c3_y_elems_sizes->y);
  c3_thisId.fIdentifier = "orientation";
  c3_g_emlrt_marshallIn(chartInstance, sf_mex_dup(sf_mex_getfield(c3_u,
    "orientation", "orientation", 0)), &c3_thisId, c3_y_data->orientation,
                        c3_y_elems_sizes->orientation);
  c3_thisId.fIdentifier = "color";
  c3_h_emlrt_marshallIn(chartInstance, sf_mex_dup(sf_mex_getfield(c3_u, "color",
    "color", 0)), &c3_thisId, c3_y_data->color, c3_y_elems_sizes->color);
  sf_mex_destroy(&c3_u);
}

static void c3_f_emlrt_marshallIn(SFc3_ImageDetInstanceStruct *chartInstance,
  const mxArray *c3_u, const emlrtMsgIdentifier *c3_parentId, int8_T c3_y_data[],
  int32_T c3_y_sizes[2])
{
  int32_T c3_i107;
  uint32_T c3_uv2[2];
  int32_T c3_i108;
  boolean_T c3_bv2[2];
  int32_T c3_tmp_sizes[2];
  int8_T c3_tmp_data[6];
  int32_T c3_y;
  int32_T c3_b_y;
  int32_T c3_loop_ub;
  int32_T c3_i109;
  (void)chartInstance;
  for (c3_i107 = 0; c3_i107 < 2; c3_i107++) {
    c3_uv2[c3_i107] = 6U + (uint32_T)(-5 * c3_i107);
  }

  for (c3_i108 = 0; c3_i108 < 2; c3_i108++) {
    c3_bv2[c3_i108] = true;
  }

  sf_mex_import_vs(c3_parentId, sf_mex_dup(c3_u), c3_tmp_data, 1, 2, 0U, 1, 0U,
                   2, c3_bv2, c3_uv2, c3_tmp_sizes);
  c3_y_sizes[0] = c3_tmp_sizes[0];
  c3_y_sizes[1] = c3_tmp_sizes[1];
  c3_y = c3_y_sizes[0];
  c3_b_y = c3_y_sizes[1];
  c3_loop_ub = c3_tmp_sizes[0] * c3_tmp_sizes[1] - 1;
  for (c3_i109 = 0; c3_i109 <= c3_loop_ub; c3_i109++) {
    c3_y_data[c3_i109] = c3_tmp_data[c3_i109];
  }

  sf_mex_destroy(&c3_u);
}

static void c3_g_emlrt_marshallIn(SFc3_ImageDetInstanceStruct *chartInstance,
  const mxArray *c3_u, const emlrtMsgIdentifier *c3_parentId, int16_T c3_y_data[],
  int32_T c3_y_sizes[2])
{
  int32_T c3_i110;
  uint32_T c3_uv3[2];
  int32_T c3_i111;
  boolean_T c3_bv3[2];
  int32_T c3_tmp_sizes[2];
  int16_T c3_tmp_data[6];
  int32_T c3_y;
  int32_T c3_b_y;
  int32_T c3_loop_ub;
  int32_T c3_i112;
  (void)chartInstance;
  for (c3_i110 = 0; c3_i110 < 2; c3_i110++) {
    c3_uv3[c3_i110] = 6U + (uint32_T)(-5 * c3_i110);
  }

  for (c3_i111 = 0; c3_i111 < 2; c3_i111++) {
    c3_bv3[c3_i111] = true;
  }

  sf_mex_import_vs(c3_parentId, sf_mex_dup(c3_u), c3_tmp_data, 1, 4, 0U, 1, 0U,
                   2, c3_bv3, c3_uv3, c3_tmp_sizes);
  c3_y_sizes[0] = c3_tmp_sizes[0];
  c3_y_sizes[1] = c3_tmp_sizes[1];
  c3_y = c3_y_sizes[0];
  c3_b_y = c3_y_sizes[1];
  c3_loop_ub = c3_tmp_sizes[0] * c3_tmp_sizes[1] - 1;
  for (c3_i112 = 0; c3_i112 <= c3_loop_ub; c3_i112++) {
    c3_y_data[c3_i112] = c3_tmp_data[c3_i112];
  }

  sf_mex_destroy(&c3_u);
}

static void c3_h_emlrt_marshallIn(SFc3_ImageDetInstanceStruct *chartInstance,
  const mxArray *c3_u, const emlrtMsgIdentifier *c3_parentId, uint8_T c3_y_data[],
  int32_T c3_y_sizes[2])
{
  int32_T c3_i113;
  uint32_T c3_uv4[2];
  int32_T c3_i114;
  boolean_T c3_bv4[2];
  int32_T c3_tmp_sizes[2];
  uint8_T c3_tmp_data[6];
  int32_T c3_y;
  int32_T c3_b_y;
  int32_T c3_loop_ub;
  int32_T c3_i115;
  (void)chartInstance;
  for (c3_i113 = 0; c3_i113 < 2; c3_i113++) {
    c3_uv4[c3_i113] = 6U + (uint32_T)(-5 * c3_i113);
  }

  for (c3_i114 = 0; c3_i114 < 2; c3_i114++) {
    c3_bv4[c3_i114] = true;
  }

  sf_mex_import_vs(c3_parentId, sf_mex_dup(c3_u), c3_tmp_data, 1, 3, 0U, 1, 0U,
                   2, c3_bv4, c3_uv4, c3_tmp_sizes);
  c3_y_sizes[0] = c3_tmp_sizes[0];
  c3_y_sizes[1] = c3_tmp_sizes[1];
  c3_y = c3_y_sizes[0];
  c3_b_y = c3_y_sizes[1];
  c3_loop_ub = c3_tmp_sizes[0] * c3_tmp_sizes[1] - 1;
  for (c3_i115 = 0; c3_i115 <= c3_loop_ub; c3_i115++) {
    c3_y_data[c3_i115] = c3_tmp_data[c3_i115];
  }

  sf_mex_destroy(&c3_u);
}

static void c3_b_sf_marshallIn(void *chartInstanceVoid, const mxArray
  *c3_mxArrayInData, const char_T *c3_varName, c3_PlayerData *c3_outData_data,
  c3_PlayerData_size *c3_outData_elems_sizes)
{
  const mxArray *c3_players;
  const char_T *c3_identifier;
  emlrtMsgIdentifier c3_thisId;
  c3_PlayerData_size c3_y_elems_sizes;
  c3_PlayerData c3_y_data;
  SFc3_ImageDetInstanceStruct *chartInstance;
  chartInstance = (SFc3_ImageDetInstanceStruct *)chartInstanceVoid;
  c3_players = sf_mex_dup(c3_mxArrayInData);
  c3_identifier = c3_varName;
  c3_thisId.fIdentifier = c3_identifier;
  c3_thisId.fParent = NULL;
  c3_e_emlrt_marshallIn(chartInstance, sf_mex_dup(c3_players), &c3_thisId,
                        &c3_y_data, &c3_y_elems_sizes);
  sf_mex_destroy(&c3_players);
  *c3_outData_elems_sizes = c3_y_elems_sizes;
  *c3_outData_data = c3_y_data;
  sf_mex_destroy(&c3_mxArrayInData);
}

static const mxArray *c3_c_sf_marshallOut(void *chartInstanceVoid, void
  *c3_inData)
{
  const mxArray *c3_mxArrayOutData = NULL;
  real_T c3_u;
  const mxArray *c3_y = NULL;
  SFc3_ImageDetInstanceStruct *chartInstance;
  chartInstance = (SFc3_ImageDetInstanceStruct *)chartInstanceVoid;
  c3_mxArrayOutData = NULL;
  c3_u = *(real_T *)c3_inData;
  c3_y = NULL;
  sf_mex_assign(&c3_y, sf_mex_create("y", &c3_u, 0, 0U, 0U, 0U, 0), false);
  sf_mex_assign(&c3_mxArrayOutData, c3_y, false);
  return c3_mxArrayOutData;
}

static const mxArray *c3_d_sf_marshallOut(void *chartInstanceVoid, void
  *c3_inData)
{
  const mxArray *c3_mxArrayOutData = NULL;
  int32_T c3_i116;
  real_T c3_b_inData[2];
  int32_T c3_i117;
  real_T c3_u[2];
  const mxArray *c3_y = NULL;
  SFc3_ImageDetInstanceStruct *chartInstance;
  chartInstance = (SFc3_ImageDetInstanceStruct *)chartInstanceVoid;
  c3_mxArrayOutData = NULL;
  for (c3_i116 = 0; c3_i116 < 2; c3_i116++) {
    c3_b_inData[c3_i116] = (*(real_T (*)[2])c3_inData)[c3_i116];
  }

  for (c3_i117 = 0; c3_i117 < 2; c3_i117++) {
    c3_u[c3_i117] = c3_b_inData[c3_i117];
  }

  c3_y = NULL;
  sf_mex_assign(&c3_y, sf_mex_create("y", c3_u, 0, 0U, 1U, 0U, 1, 2), false);
  sf_mex_assign(&c3_mxArrayOutData, c3_y, false);
  return c3_mxArrayOutData;
}

static const mxArray *c3_e_sf_marshallOut(void *chartInstanceVoid, c3_BlobData
  *c3_inData_data, c3_BlobData_size *c3_inData_elems_sizes)
{
  const mxArray *c3_mxArrayOutData = NULL;
  c3_BlobData_size c3_u_elems_sizes;
  c3_BlobData c3_u_data;
  const mxArray *c3_y = NULL;
  int32_T c3_u_sizes[2];
  int32_T c3_u;
  int32_T c3_b_u;
  int32_T c3_loop_ub;
  int32_T c3_i118;
  real32_T c3_b_u_data[32];
  const mxArray *c3_b_y = NULL;
  int32_T c3_b_u_sizes;
  int32_T c3_b_loop_ub;
  int32_T c3_i119;
  int32_T c3_c_u_data[16];
  const mxArray *c3_c_y = NULL;
  int32_T c3_c_u_sizes;
  int32_T c3_c_loop_ub;
  int32_T c3_i120;
  uint8_T c3_d_u_data[16];
  const mxArray *c3_d_y = NULL;
  SFc3_ImageDetInstanceStruct *chartInstance;
  chartInstance = (SFc3_ImageDetInstanceStruct *)chartInstanceVoid;
  c3_mxArrayOutData = NULL;
  c3_u_elems_sizes = *c3_inData_elems_sizes;
  c3_u_data = *c3_inData_data;
  c3_y = NULL;
  sf_mex_assign(&c3_y, sf_mex_createstruct("structure", 2, 1, 1), false);
  c3_u_sizes[0] = c3_u_elems_sizes.centroid[0];
  c3_u_sizes[1] = c3_u_elems_sizes.centroid[1];
  c3_u = c3_u_sizes[0];
  c3_b_u = c3_u_sizes[1];
  c3_loop_ub = c3_u_elems_sizes.centroid[0] * c3_u_elems_sizes.centroid[1] - 1;
  for (c3_i118 = 0; c3_i118 <= c3_loop_ub; c3_i118++) {
    c3_b_u_data[c3_i118] = c3_u_data.centroid[c3_i118];
  }

  c3_b_y = NULL;
  sf_mex_assign(&c3_b_y, sf_mex_create("y", c3_b_u_data, 1, 0U, 1U, 0U, 2,
    c3_u_sizes[0], c3_u_sizes[1]), false);
  sf_mex_addfield(c3_y, c3_b_y, "centroid", "centroid", 0);
  c3_b_u_sizes = c3_u_elems_sizes.area;
  c3_b_loop_ub = c3_u_elems_sizes.area - 1;
  for (c3_i119 = 0; c3_i119 <= c3_b_loop_ub; c3_i119++) {
    c3_c_u_data[c3_i119] = c3_u_data.area[c3_i119];
  }

  c3_c_y = NULL;
  sf_mex_assign(&c3_c_y, sf_mex_create("y", c3_c_u_data, 6, 0U, 1U, 0U, 1,
    c3_b_u_sizes), false);
  sf_mex_addfield(c3_y, c3_c_y, "area", "area", 0);
  c3_c_u_sizes = c3_u_elems_sizes.color;
  c3_c_loop_ub = c3_u_elems_sizes.color - 1;
  for (c3_i120 = 0; c3_i120 <= c3_c_loop_ub; c3_i120++) {
    c3_d_u_data[c3_i120] = c3_u_data.color[c3_i120];
  }

  c3_d_y = NULL;
  sf_mex_assign(&c3_d_y, sf_mex_create("y", c3_d_u_data, 3, 0U, 1U, 0U, 1,
    c3_c_u_sizes), false);
  sf_mex_addfield(c3_y, c3_d_y, "color", "color", 0);
  sf_mex_assign(&c3_mxArrayOutData, c3_y, false);
  return c3_mxArrayOutData;
}

static real_T c3_i_emlrt_marshallIn(SFc3_ImageDetInstanceStruct *chartInstance,
  const mxArray *c3_u, const emlrtMsgIdentifier *c3_parentId)
{
  real_T c3_y;
  real_T c3_d6;
  (void)chartInstance;
  sf_mex_import(c3_parentId, sf_mex_dup(c3_u), &c3_d6, 1, 0, 0U, 0, 0U, 0);
  c3_y = c3_d6;
  sf_mex_destroy(&c3_u);
  return c3_y;
}

static void c3_c_sf_marshallIn(void *chartInstanceVoid, const mxArray
  *c3_mxArrayInData, const char_T *c3_varName, void *c3_outData)
{
  const mxArray *c3_nargout;
  const char_T *c3_identifier;
  emlrtMsgIdentifier c3_thisId;
  real_T c3_y;
  SFc3_ImageDetInstanceStruct *chartInstance;
  chartInstance = (SFc3_ImageDetInstanceStruct *)chartInstanceVoid;
  c3_nargout = sf_mex_dup(c3_mxArrayInData);
  c3_identifier = c3_varName;
  c3_thisId.fIdentifier = c3_identifier;
  c3_thisId.fParent = NULL;
  c3_y = c3_i_emlrt_marshallIn(chartInstance, sf_mex_dup(c3_nargout), &c3_thisId);
  sf_mex_destroy(&c3_nargout);
  *(real_T *)c3_outData = c3_y;
  sf_mex_destroy(&c3_mxArrayInData);
}

static const mxArray *c3_f_sf_marshallOut(void *chartInstanceVoid, void
  *c3_inData)
{
  const mxArray *c3_mxArrayOutData = NULL;
  real32_T c3_u;
  const mxArray *c3_y = NULL;
  SFc3_ImageDetInstanceStruct *chartInstance;
  chartInstance = (SFc3_ImageDetInstanceStruct *)chartInstanceVoid;
  c3_mxArrayOutData = NULL;
  c3_u = *(real32_T *)c3_inData;
  c3_y = NULL;
  sf_mex_assign(&c3_y, sf_mex_create("y", &c3_u, 1, 0U, 0U, 0U, 0), false);
  sf_mex_assign(&c3_mxArrayOutData, c3_y, false);
  return c3_mxArrayOutData;
}

static real32_T c3_j_emlrt_marshallIn(SFc3_ImageDetInstanceStruct *chartInstance,
  const mxArray *c3_u, const emlrtMsgIdentifier *c3_parentId)
{
  real32_T c3_y;
  real32_T c3_f5;
  (void)chartInstance;
  sf_mex_import(c3_parentId, sf_mex_dup(c3_u), &c3_f5, 1, 1, 0U, 0, 0U, 0);
  c3_y = c3_f5;
  sf_mex_destroy(&c3_u);
  return c3_y;
}

static void c3_d_sf_marshallIn(void *chartInstanceVoid, const mxArray
  *c3_mxArrayInData, const char_T *c3_varName, void *c3_outData)
{
  const mxArray *c3_orientation;
  const char_T *c3_identifier;
  emlrtMsgIdentifier c3_thisId;
  real32_T c3_y;
  SFc3_ImageDetInstanceStruct *chartInstance;
  chartInstance = (SFc3_ImageDetInstanceStruct *)chartInstanceVoid;
  c3_orientation = sf_mex_dup(c3_mxArrayInData);
  c3_identifier = c3_varName;
  c3_thisId.fIdentifier = c3_identifier;
  c3_thisId.fParent = NULL;
  c3_y = c3_j_emlrt_marshallIn(chartInstance, sf_mex_dup(c3_orientation),
    &c3_thisId);
  sf_mex_destroy(&c3_orientation);
  *(real32_T *)c3_outData = c3_y;
  sf_mex_destroy(&c3_mxArrayInData);
}

static const mxArray *c3_g_sf_marshallOut(void *chartInstanceVoid, void
  *c3_inData)
{
  const mxArray *c3_mxArrayOutData = NULL;
  int32_T c3_i121;
  real32_T c3_b_inData[2];
  int32_T c3_i122;
  real32_T c3_u[2];
  const mxArray *c3_y = NULL;
  SFc3_ImageDetInstanceStruct *chartInstance;
  chartInstance = (SFc3_ImageDetInstanceStruct *)chartInstanceVoid;
  c3_mxArrayOutData = NULL;
  for (c3_i121 = 0; c3_i121 < 2; c3_i121++) {
    c3_b_inData[c3_i121] = (*(real32_T (*)[2])c3_inData)[c3_i121];
  }

  for (c3_i122 = 0; c3_i122 < 2; c3_i122++) {
    c3_u[c3_i122] = c3_b_inData[c3_i122];
  }

  c3_y = NULL;
  sf_mex_assign(&c3_y, sf_mex_create("y", c3_u, 1, 0U, 1U, 0U, 1, 2), false);
  sf_mex_assign(&c3_mxArrayOutData, c3_y, false);
  return c3_mxArrayOutData;
}

static void c3_k_emlrt_marshallIn(SFc3_ImageDetInstanceStruct *chartInstance,
  const mxArray *c3_u, const emlrtMsgIdentifier *c3_parentId, real32_T c3_y[2])
{
  real32_T c3_fv1[2];
  int32_T c3_i123;
  (void)chartInstance;
  sf_mex_import(c3_parentId, sf_mex_dup(c3_u), c3_fv1, 1, 1, 0U, 1, 0U, 1, 2);
  for (c3_i123 = 0; c3_i123 < 2; c3_i123++) {
    c3_y[c3_i123] = c3_fv1[c3_i123];
  }

  sf_mex_destroy(&c3_u);
}

static void c3_e_sf_marshallIn(void *chartInstanceVoid, const mxArray
  *c3_mxArrayInData, const char_T *c3_varName, void *c3_outData)
{
  const mxArray *c3_Dpos;
  const char_T *c3_identifier;
  emlrtMsgIdentifier c3_thisId;
  real32_T c3_y[2];
  int32_T c3_i124;
  SFc3_ImageDetInstanceStruct *chartInstance;
  chartInstance = (SFc3_ImageDetInstanceStruct *)chartInstanceVoid;
  c3_Dpos = sf_mex_dup(c3_mxArrayInData);
  c3_identifier = c3_varName;
  c3_thisId.fIdentifier = c3_identifier;
  c3_thisId.fParent = NULL;
  c3_k_emlrt_marshallIn(chartInstance, sf_mex_dup(c3_Dpos), &c3_thisId, c3_y);
  sf_mex_destroy(&c3_Dpos);
  for (c3_i124 = 0; c3_i124 < 2; c3_i124++) {
    (*(real32_T (*)[2])c3_outData)[c3_i124] = c3_y[c3_i124];
  }

  sf_mex_destroy(&c3_mxArrayInData);
}

static const mxArray *c3_h_sf_marshallOut(void *chartInstanceVoid, void
  *c3_inData)
{
  const mxArray *c3_mxArrayOutData = NULL;
  uint8_T c3_u;
  const mxArray *c3_y = NULL;
  SFc3_ImageDetInstanceStruct *chartInstance;
  chartInstance = (SFc3_ImageDetInstanceStruct *)chartInstanceVoid;
  c3_mxArrayOutData = NULL;
  c3_u = *(uint8_T *)c3_inData;
  c3_y = NULL;
  sf_mex_assign(&c3_y, sf_mex_create("y", &c3_u, 3, 0U, 0U, 0U, 0), false);
  sf_mex_assign(&c3_mxArrayOutData, c3_y, false);
  return c3_mxArrayOutData;
}

static uint8_T c3_l_emlrt_marshallIn(SFc3_ImageDetInstanceStruct *chartInstance,
  const mxArray *c3_index, const char_T *c3_identifier)
{
  uint8_T c3_y;
  emlrtMsgIdentifier c3_thisId;
  c3_thisId.fIdentifier = c3_identifier;
  c3_thisId.fParent = NULL;
  c3_y = c3_m_emlrt_marshallIn(chartInstance, sf_mex_dup(c3_index), &c3_thisId);
  sf_mex_destroy(&c3_index);
  return c3_y;
}

static uint8_T c3_m_emlrt_marshallIn(SFc3_ImageDetInstanceStruct *chartInstance,
  const mxArray *c3_u, const emlrtMsgIdentifier *c3_parentId)
{
  uint8_T c3_y;
  uint8_T c3_u1;
  (void)chartInstance;
  sf_mex_import(c3_parentId, sf_mex_dup(c3_u), &c3_u1, 1, 3, 0U, 0, 0U, 0);
  c3_y = c3_u1;
  sf_mex_destroy(&c3_u);
  return c3_y;
}

static void c3_f_sf_marshallIn(void *chartInstanceVoid, const mxArray
  *c3_mxArrayInData, const char_T *c3_varName, void *c3_outData)
{
  const mxArray *c3_index;
  const char_T *c3_identifier;
  emlrtMsgIdentifier c3_thisId;
  uint8_T c3_y;
  SFc3_ImageDetInstanceStruct *chartInstance;
  chartInstance = (SFc3_ImageDetInstanceStruct *)chartInstanceVoid;
  c3_index = sf_mex_dup(c3_mxArrayInData);
  c3_identifier = c3_varName;
  c3_thisId.fIdentifier = c3_identifier;
  c3_thisId.fParent = NULL;
  c3_y = c3_m_emlrt_marshallIn(chartInstance, sf_mex_dup(c3_index), &c3_thisId);
  sf_mex_destroy(&c3_index);
  *(uint8_T *)c3_outData = c3_y;
  sf_mex_destroy(&c3_mxArrayInData);
}

static const mxArray *c3_i_sf_marshallOut(void *chartInstanceVoid, real32_T
  c3_inData_data[], int32_T c3_inData_sizes[2])
{
  const mxArray *c3_mxArrayOutData = NULL;
  int32_T c3_u_sizes[2];
  int32_T c3_u;
  int32_T c3_b_u;
  int32_T c3_inData;
  int32_T c3_b_inData;
  int32_T c3_b_inData_sizes;
  int32_T c3_loop_ub;
  int32_T c3_i125;
  real32_T c3_b_inData_data[62];
  int32_T c3_b_loop_ub;
  int32_T c3_i126;
  real32_T c3_u_data[62];
  const mxArray *c3_y = NULL;
  SFc3_ImageDetInstanceStruct *chartInstance;
  chartInstance = (SFc3_ImageDetInstanceStruct *)chartInstanceVoid;
  c3_mxArrayOutData = NULL;
  c3_u_sizes[0] = 2;
  c3_u_sizes[1] = c3_inData_sizes[1];
  c3_u = c3_u_sizes[0];
  c3_b_u = c3_u_sizes[1];
  c3_inData = c3_inData_sizes[0];
  c3_b_inData = c3_inData_sizes[1];
  c3_b_inData_sizes = c3_inData * c3_b_inData;
  c3_loop_ub = c3_inData * c3_b_inData - 1;
  for (c3_i125 = 0; c3_i125 <= c3_loop_ub; c3_i125++) {
    c3_b_inData_data[c3_i125] = c3_inData_data[c3_i125];
  }

  c3_b_loop_ub = c3_b_inData_sizes - 1;
  for (c3_i126 = 0; c3_i126 <= c3_b_loop_ub; c3_i126++) {
    c3_u_data[c3_i126] = c3_b_inData_data[c3_i126];
  }

  c3_y = NULL;
  sf_mex_assign(&c3_y, sf_mex_create("y", c3_u_data, 1, 0U, 1U, 0U, 2,
    c3_u_sizes[0], c3_u_sizes[1]), false);
  sf_mex_assign(&c3_mxArrayOutData, c3_y, false);
  return c3_mxArrayOutData;
}

static void c3_n_emlrt_marshallIn(SFc3_ImageDetInstanceStruct *chartInstance,
  const mxArray *c3_u, const emlrtMsgIdentifier *c3_parentId, real32_T
  c3_y_data[], int32_T c3_y_sizes[2])
{
  int32_T c3_i127;
  uint32_T c3_uv5[2];
  int32_T c3_i128;
  static boolean_T c3_bv5[2] = { false, true };

  boolean_T c3_bv6[2];
  int32_T c3_tmp_sizes[2];
  real32_T c3_tmp_data[62];
  int32_T c3_y;
  int32_T c3_b_y;
  int32_T c3_loop_ub;
  int32_T c3_i129;
  (void)chartInstance;
  for (c3_i127 = 0; c3_i127 < 2; c3_i127++) {
    c3_uv5[c3_i127] = 2U + 29U * (uint32_T)c3_i127;
  }

  for (c3_i128 = 0; c3_i128 < 2; c3_i128++) {
    c3_bv6[c3_i128] = c3_bv5[c3_i128];
  }

  sf_mex_import_vs(c3_parentId, sf_mex_dup(c3_u), c3_tmp_data, 1, 1, 0U, 1, 0U,
                   2, c3_bv6, c3_uv5, c3_tmp_sizes);
  c3_y_sizes[0] = 2;
  c3_y_sizes[1] = c3_tmp_sizes[1];
  c3_y = c3_y_sizes[0];
  c3_b_y = c3_y_sizes[1];
  c3_loop_ub = c3_tmp_sizes[0] * c3_tmp_sizes[1] - 1;
  for (c3_i129 = 0; c3_i129 <= c3_loop_ub; c3_i129++) {
    c3_y_data[c3_i129] = c3_tmp_data[c3_i129];
  }

  sf_mex_destroy(&c3_u);
}

static void c3_g_sf_marshallIn(void *chartInstanceVoid, const mxArray
  *c3_mxArrayInData, const char_T *c3_varName, real32_T c3_outData_data[],
  int32_T c3_outData_sizes[2])
{
  const mxArray *c3_blues;
  const char_T *c3_identifier;
  emlrtMsgIdentifier c3_thisId;
  int32_T c3_y_sizes[2];
  real32_T c3_y_data[62];
  int32_T c3_loop_ub;
  int32_T c3_i130;
  int32_T c3_i131;
  SFc3_ImageDetInstanceStruct *chartInstance;
  chartInstance = (SFc3_ImageDetInstanceStruct *)chartInstanceVoid;
  c3_blues = sf_mex_dup(c3_mxArrayInData);
  c3_identifier = c3_varName;
  c3_thisId.fIdentifier = c3_identifier;
  c3_thisId.fParent = NULL;
  c3_n_emlrt_marshallIn(chartInstance, sf_mex_dup(c3_blues), &c3_thisId,
                        c3_y_data, c3_y_sizes);
  sf_mex_destroy(&c3_blues);
  c3_outData_sizes[0] = 2;
  c3_outData_sizes[1] = c3_y_sizes[1];
  c3_loop_ub = c3_y_sizes[1] - 1;
  for (c3_i130 = 0; c3_i130 <= c3_loop_ub; c3_i130++) {
    for (c3_i131 = 0; c3_i131 < 2; c3_i131++) {
      c3_outData_data[c3_i131 + c3_outData_sizes[0] * c3_i130] =
        c3_y_data[c3_i131 + c3_y_sizes[0] * c3_i130];
    }
  }

  sf_mex_destroy(&c3_mxArrayInData);
}

static const mxArray *c3_j_sf_marshallOut(void *chartInstanceVoid, real32_T
  c3_inData_data[], int32_T c3_inData_sizes[2])
{
  const mxArray *c3_mxArrayOutData = NULL;
  int32_T c3_u_sizes[2];
  int32_T c3_u;
  int32_T c3_b_u;
  int32_T c3_inData;
  int32_T c3_b_inData;
  int32_T c3_b_inData_sizes;
  int32_T c3_loop_ub;
  int32_T c3_i132;
  real32_T c3_b_inData_data[93];
  int32_T c3_b_loop_ub;
  int32_T c3_i133;
  real32_T c3_u_data[93];
  const mxArray *c3_y = NULL;
  SFc3_ImageDetInstanceStruct *chartInstance;
  chartInstance = (SFc3_ImageDetInstanceStruct *)chartInstanceVoid;
  c3_mxArrayOutData = NULL;
  c3_u_sizes[0] = 3;
  c3_u_sizes[1] = c3_inData_sizes[1];
  c3_u = c3_u_sizes[0];
  c3_b_u = c3_u_sizes[1];
  c3_inData = c3_inData_sizes[0];
  c3_b_inData = c3_inData_sizes[1];
  c3_b_inData_sizes = c3_inData * c3_b_inData;
  c3_loop_ub = c3_inData * c3_b_inData - 1;
  for (c3_i132 = 0; c3_i132 <= c3_loop_ub; c3_i132++) {
    c3_b_inData_data[c3_i132] = c3_inData_data[c3_i132];
  }

  c3_b_loop_ub = c3_b_inData_sizes - 1;
  for (c3_i133 = 0; c3_i133 <= c3_b_loop_ub; c3_i133++) {
    c3_u_data[c3_i133] = c3_b_inData_data[c3_i133];
  }

  c3_y = NULL;
  sf_mex_assign(&c3_y, sf_mex_create("y", c3_u_data, 1, 0U, 1U, 0U, 2,
    c3_u_sizes[0], c3_u_sizes[1]), false);
  sf_mex_assign(&c3_mxArrayOutData, c3_y, false);
  return c3_mxArrayOutData;
}

static void c3_o_emlrt_marshallIn(SFc3_ImageDetInstanceStruct *chartInstance,
  const mxArray *c3_u, const emlrtMsgIdentifier *c3_parentId, real32_T
  c3_y_data[], int32_T c3_y_sizes[2])
{
  int32_T c3_i134;
  uint32_T c3_uv6[2];
  int32_T c3_i135;
  static boolean_T c3_bv7[2] = { false, true };

  boolean_T c3_bv8[2];
  int32_T c3_tmp_sizes[2];
  real32_T c3_tmp_data[93];
  int32_T c3_y;
  int32_T c3_b_y;
  int32_T c3_loop_ub;
  int32_T c3_i136;
  (void)chartInstance;
  for (c3_i134 = 0; c3_i134 < 2; c3_i134++) {
    c3_uv6[c3_i134] = 3U + 28U * (uint32_T)c3_i134;
  }

  for (c3_i135 = 0; c3_i135 < 2; c3_i135++) {
    c3_bv8[c3_i135] = c3_bv7[c3_i135];
  }

  sf_mex_import_vs(c3_parentId, sf_mex_dup(c3_u), c3_tmp_data, 1, 1, 0U, 1, 0U,
                   2, c3_bv8, c3_uv6, c3_tmp_sizes);
  c3_y_sizes[0] = 3;
  c3_y_sizes[1] = c3_tmp_sizes[1];
  c3_y = c3_y_sizes[0];
  c3_b_y = c3_y_sizes[1];
  c3_loop_ub = c3_tmp_sizes[0] * c3_tmp_sizes[1] - 1;
  for (c3_i136 = 0; c3_i136 <= c3_loop_ub; c3_i136++) {
    c3_y_data[c3_i136] = c3_tmp_data[c3_i136];
  }

  sf_mex_destroy(&c3_u);
}

static void c3_h_sf_marshallIn(void *chartInstanceVoid, const mxArray
  *c3_mxArrayInData, const char_T *c3_varName, real32_T c3_outData_data[],
  int32_T c3_outData_sizes[2])
{
  const mxArray *c3_allLights;
  const char_T *c3_identifier;
  emlrtMsgIdentifier c3_thisId;
  int32_T c3_y_sizes[2];
  real32_T c3_y_data[93];
  int32_T c3_loop_ub;
  int32_T c3_i137;
  int32_T c3_i138;
  SFc3_ImageDetInstanceStruct *chartInstance;
  chartInstance = (SFc3_ImageDetInstanceStruct *)chartInstanceVoid;
  c3_allLights = sf_mex_dup(c3_mxArrayInData);
  c3_identifier = c3_varName;
  c3_thisId.fIdentifier = c3_identifier;
  c3_thisId.fParent = NULL;
  c3_o_emlrt_marshallIn(chartInstance, sf_mex_dup(c3_allLights), &c3_thisId,
                        c3_y_data, c3_y_sizes);
  sf_mex_destroy(&c3_allLights);
  c3_outData_sizes[0] = 3;
  c3_outData_sizes[1] = c3_y_sizes[1];
  c3_loop_ub = c3_y_sizes[1] - 1;
  for (c3_i137 = 0; c3_i137 <= c3_loop_ub; c3_i137++) {
    for (c3_i138 = 0; c3_i138 < 3; c3_i138++) {
      c3_outData_data[c3_i138 + c3_outData_sizes[0] * c3_i137] =
        c3_y_data[c3_i138 + c3_y_sizes[0] * c3_i137];
    }
  }

  sf_mex_destroy(&c3_mxArrayInData);
}

static const mxArray *c3_k_sf_marshallOut(void *chartInstanceVoid, real_T
  c3_inData_data[], int32_T *c3_inData_sizes)
{
  const mxArray *c3_mxArrayOutData = NULL;
  int32_T c3_b_inData_sizes;
  int32_T c3_loop_ub;
  int32_T c3_i139;
  real_T c3_b_inData_data[16];
  int32_T c3_u_sizes;
  int32_T c3_b_loop_ub;
  int32_T c3_i140;
  real_T c3_u_data[16];
  const mxArray *c3_y = NULL;
  SFc3_ImageDetInstanceStruct *chartInstance;
  chartInstance = (SFc3_ImageDetInstanceStruct *)chartInstanceVoid;
  c3_mxArrayOutData = NULL;
  c3_b_inData_sizes = *c3_inData_sizes;
  c3_loop_ub = *c3_inData_sizes - 1;
  for (c3_i139 = 0; c3_i139 <= c3_loop_ub; c3_i139++) {
    c3_b_inData_data[c3_i139] = c3_inData_data[c3_i139];
  }

  c3_u_sizes = c3_b_inData_sizes;
  c3_b_loop_ub = c3_b_inData_sizes - 1;
  for (c3_i140 = 0; c3_i140 <= c3_b_loop_ub; c3_i140++) {
    c3_u_data[c3_i140] = c3_b_inData_data[c3_i140];
  }

  c3_y = NULL;
  sf_mex_assign(&c3_y, sf_mex_create("y", c3_u_data, 0, 0U, 1U, 0U, 1,
    c3_u_sizes), false);
  sf_mex_assign(&c3_mxArrayOutData, c3_y, false);
  return c3_mxArrayOutData;
}

static void c3_p_emlrt_marshallIn(SFc3_ImageDetInstanceStruct *chartInstance,
  const mxArray *c3_u, const emlrtMsgIdentifier *c3_parentId, real_T c3_y_data[],
  int32_T *c3_y_sizes)
{
  static uint32_T c3_uv7[1] = { 16U };

  uint32_T c3_uv8[1];
  static boolean_T c3_bv9[1] = { true };

  boolean_T c3_bv10[1];
  int32_T c3_tmp_sizes;
  real_T c3_tmp_data[16];
  int32_T c3_loop_ub;
  int32_T c3_i141;
  (void)chartInstance;
  c3_uv8[0] = c3_uv7[0];
  c3_bv10[0] = c3_bv9[0];
  sf_mex_import_vs(c3_parentId, sf_mex_dup(c3_u), c3_tmp_data, 1, 0, 0U, 1, 0U,
                   1, c3_bv10, c3_uv8, &c3_tmp_sizes);
  *c3_y_sizes = c3_tmp_sizes;
  c3_loop_ub = c3_tmp_sizes - 1;
  for (c3_i141 = 0; c3_i141 <= c3_loop_ub; c3_i141++) {
    c3_y_data[c3_i141] = c3_tmp_data[c3_i141];
  }

  sf_mex_destroy(&c3_u);
}

static void c3_i_sf_marshallIn(void *chartInstanceVoid, const mxArray
  *c3_mxArrayInData, const char_T *c3_varName, real_T c3_outData_data[], int32_T
  *c3_outData_sizes)
{
  const mxArray *c3_blobIndices;
  const char_T *c3_identifier;
  emlrtMsgIdentifier c3_thisId;
  int32_T c3_y_sizes;
  real_T c3_y_data[16];
  int32_T c3_loop_ub;
  int32_T c3_i142;
  SFc3_ImageDetInstanceStruct *chartInstance;
  chartInstance = (SFc3_ImageDetInstanceStruct *)chartInstanceVoid;
  c3_blobIndices = sf_mex_dup(c3_mxArrayInData);
  c3_identifier = c3_varName;
  c3_thisId.fIdentifier = c3_identifier;
  c3_thisId.fParent = NULL;
  c3_p_emlrt_marshallIn(chartInstance, sf_mex_dup(c3_blobIndices), &c3_thisId,
                        c3_y_data, &c3_y_sizes);
  sf_mex_destroy(&c3_blobIndices);
  *c3_outData_sizes = c3_y_sizes;
  c3_loop_ub = c3_y_sizes - 1;
  for (c3_i142 = 0; c3_i142 <= c3_loop_ub; c3_i142++) {
    c3_outData_data[c3_i142] = c3_y_data[c3_i142];
  }

  sf_mex_destroy(&c3_mxArrayInData);
}

const mxArray *sf_c3_ImageDet_get_eml_resolved_functions_info(void)
{
  const mxArray *c3_nameCaptureInfo = NULL;
  c3_nameCaptureInfo = NULL;
  sf_mex_assign(&c3_nameCaptureInfo, sf_mex_createstruct("structure", 2, 123, 1),
                false);
  c3_info_helper(&c3_nameCaptureInfo);
  c3_b_info_helper(&c3_nameCaptureInfo);
  sf_mex_emlrtNameCapturePostProcessR2012a(&c3_nameCaptureInfo);
  return c3_nameCaptureInfo;
}

static void c3_info_helper(const mxArray **c3_info)
{
  const mxArray *c3_rhs0 = NULL;
  const mxArray *c3_lhs0 = NULL;
  const mxArray *c3_rhs1 = NULL;
  const mxArray *c3_lhs1 = NULL;
  const mxArray *c3_rhs2 = NULL;
  const mxArray *c3_lhs2 = NULL;
  const mxArray *c3_rhs3 = NULL;
  const mxArray *c3_lhs3 = NULL;
  const mxArray *c3_rhs4 = NULL;
  const mxArray *c3_lhs4 = NULL;
  const mxArray *c3_rhs5 = NULL;
  const mxArray *c3_lhs5 = NULL;
  const mxArray *c3_rhs6 = NULL;
  const mxArray *c3_lhs6 = NULL;
  const mxArray *c3_rhs7 = NULL;
  const mxArray *c3_lhs7 = NULL;
  const mxArray *c3_rhs8 = NULL;
  const mxArray *c3_lhs8 = NULL;
  const mxArray *c3_rhs9 = NULL;
  const mxArray *c3_lhs9 = NULL;
  const mxArray *c3_rhs10 = NULL;
  const mxArray *c3_lhs10 = NULL;
  const mxArray *c3_rhs11 = NULL;
  const mxArray *c3_lhs11 = NULL;
  const mxArray *c3_rhs12 = NULL;
  const mxArray *c3_lhs12 = NULL;
  const mxArray *c3_rhs13 = NULL;
  const mxArray *c3_lhs13 = NULL;
  const mxArray *c3_rhs14 = NULL;
  const mxArray *c3_lhs14 = NULL;
  const mxArray *c3_rhs15 = NULL;
  const mxArray *c3_lhs15 = NULL;
  const mxArray *c3_rhs16 = NULL;
  const mxArray *c3_lhs16 = NULL;
  const mxArray *c3_rhs17 = NULL;
  const mxArray *c3_lhs17 = NULL;
  const mxArray *c3_rhs18 = NULL;
  const mxArray *c3_lhs18 = NULL;
  const mxArray *c3_rhs19 = NULL;
  const mxArray *c3_lhs19 = NULL;
  const mxArray *c3_rhs20 = NULL;
  const mxArray *c3_lhs20 = NULL;
  const mxArray *c3_rhs21 = NULL;
  const mxArray *c3_lhs21 = NULL;
  const mxArray *c3_rhs22 = NULL;
  const mxArray *c3_lhs22 = NULL;
  const mxArray *c3_rhs23 = NULL;
  const mxArray *c3_lhs23 = NULL;
  const mxArray *c3_rhs24 = NULL;
  const mxArray *c3_lhs24 = NULL;
  const mxArray *c3_rhs25 = NULL;
  const mxArray *c3_lhs25 = NULL;
  const mxArray *c3_rhs26 = NULL;
  const mxArray *c3_lhs26 = NULL;
  const mxArray *c3_rhs27 = NULL;
  const mxArray *c3_lhs27 = NULL;
  const mxArray *c3_rhs28 = NULL;
  const mxArray *c3_lhs28 = NULL;
  const mxArray *c3_rhs29 = NULL;
  const mxArray *c3_lhs29 = NULL;
  const mxArray *c3_rhs30 = NULL;
  const mxArray *c3_lhs30 = NULL;
  const mxArray *c3_rhs31 = NULL;
  const mxArray *c3_lhs31 = NULL;
  const mxArray *c3_rhs32 = NULL;
  const mxArray *c3_lhs32 = NULL;
  const mxArray *c3_rhs33 = NULL;
  const mxArray *c3_lhs33 = NULL;
  const mxArray *c3_rhs34 = NULL;
  const mxArray *c3_lhs34 = NULL;
  const mxArray *c3_rhs35 = NULL;
  const mxArray *c3_lhs35 = NULL;
  const mxArray *c3_rhs36 = NULL;
  const mxArray *c3_lhs36 = NULL;
  const mxArray *c3_rhs37 = NULL;
  const mxArray *c3_lhs37 = NULL;
  const mxArray *c3_rhs38 = NULL;
  const mxArray *c3_lhs38 = NULL;
  const mxArray *c3_rhs39 = NULL;
  const mxArray *c3_lhs39 = NULL;
  const mxArray *c3_rhs40 = NULL;
  const mxArray *c3_lhs40 = NULL;
  const mxArray *c3_rhs41 = NULL;
  const mxArray *c3_lhs41 = NULL;
  const mxArray *c3_rhs42 = NULL;
  const mxArray *c3_lhs42 = NULL;
  const mxArray *c3_rhs43 = NULL;
  const mxArray *c3_lhs43 = NULL;
  const mxArray *c3_rhs44 = NULL;
  const mxArray *c3_lhs44 = NULL;
  const mxArray *c3_rhs45 = NULL;
  const mxArray *c3_lhs45 = NULL;
  const mxArray *c3_rhs46 = NULL;
  const mxArray *c3_lhs46 = NULL;
  const mxArray *c3_rhs47 = NULL;
  const mxArray *c3_lhs47 = NULL;
  const mxArray *c3_rhs48 = NULL;
  const mxArray *c3_lhs48 = NULL;
  const mxArray *c3_rhs49 = NULL;
  const mxArray *c3_lhs49 = NULL;
  const mxArray *c3_rhs50 = NULL;
  const mxArray *c3_lhs50 = NULL;
  const mxArray *c3_rhs51 = NULL;
  const mxArray *c3_lhs51 = NULL;
  const mxArray *c3_rhs52 = NULL;
  const mxArray *c3_lhs52 = NULL;
  const mxArray *c3_rhs53 = NULL;
  const mxArray *c3_lhs53 = NULL;
  const mxArray *c3_rhs54 = NULL;
  const mxArray *c3_lhs54 = NULL;
  const mxArray *c3_rhs55 = NULL;
  const mxArray *c3_lhs55 = NULL;
  const mxArray *c3_rhs56 = NULL;
  const mxArray *c3_lhs56 = NULL;
  const mxArray *c3_rhs57 = NULL;
  const mxArray *c3_lhs57 = NULL;
  const mxArray *c3_rhs58 = NULL;
  const mxArray *c3_lhs58 = NULL;
  const mxArray *c3_rhs59 = NULL;
  const mxArray *c3_lhs59 = NULL;
  const mxArray *c3_rhs60 = NULL;
  const mxArray *c3_lhs60 = NULL;
  const mxArray *c3_rhs61 = NULL;
  const mxArray *c3_lhs61 = NULL;
  const mxArray *c3_rhs62 = NULL;
  const mxArray *c3_lhs62 = NULL;
  const mxArray *c3_rhs63 = NULL;
  const mxArray *c3_lhs63 = NULL;
  sf_mex_addfield(*c3_info, c3_emlrt_marshallOut(""), "context", "context", 0);
  sf_mex_addfield(*c3_info, c3_emlrt_marshallOut("sort"), "name", "name", 0);
  sf_mex_addfield(*c3_info, c3_emlrt_marshallOut("int32"), "dominantType",
                  "dominantType", 0);
  sf_mex_addfield(*c3_info, c3_emlrt_marshallOut(
    "[ILXE]$matlabroot$/toolbox/eml/lib/matlab/datafun/sort.m"), "resolved",
                  "resolved", 0);
  sf_mex_addfield(*c3_info, c3_b_emlrt_marshallOut(1363717456U), "fileTimeLo",
                  "fileTimeLo", 0);
  sf_mex_addfield(*c3_info, c3_b_emlrt_marshallOut(0U), "fileTimeHi",
                  "fileTimeHi", 0);
  sf_mex_addfield(*c3_info, c3_b_emlrt_marshallOut(0U), "mFileTimeLo",
                  "mFileTimeLo", 0);
  sf_mex_addfield(*c3_info, c3_b_emlrt_marshallOut(0U), "mFileTimeHi",
                  "mFileTimeHi", 0);
  sf_mex_assign(&c3_rhs0, sf_mex_createcellmatrix(0, 1), false);
  sf_mex_assign(&c3_lhs0, sf_mex_createcellmatrix(0, 1), false);
  sf_mex_addfield(*c3_info, sf_mex_duplicatearraysafe(&c3_rhs0), "rhs", "rhs", 0);
  sf_mex_addfield(*c3_info, sf_mex_duplicatearraysafe(&c3_lhs0), "lhs", "lhs", 0);
  sf_mex_addfield(*c3_info, c3_emlrt_marshallOut(
    "[ILXE]$matlabroot$/toolbox/eml/lib/matlab/datafun/sort.m"), "context",
                  "context", 1);
  sf_mex_addfield(*c3_info, c3_emlrt_marshallOut(
    "coder.internal.isBuiltInNumeric"), "name", "name", 1);
  sf_mex_addfield(*c3_info, c3_emlrt_marshallOut("int32"), "dominantType",
                  "dominantType", 1);
  sf_mex_addfield(*c3_info, c3_emlrt_marshallOut(
    "[ILXE]$matlabroot$/toolbox/shared/coder/coder/+coder/+internal/isBuiltInNumeric.m"),
                  "resolved", "resolved", 1);
  sf_mex_addfield(*c3_info, c3_b_emlrt_marshallOut(1395935456U), "fileTimeLo",
                  "fileTimeLo", 1);
  sf_mex_addfield(*c3_info, c3_b_emlrt_marshallOut(0U), "fileTimeHi",
                  "fileTimeHi", 1);
  sf_mex_addfield(*c3_info, c3_b_emlrt_marshallOut(0U), "mFileTimeLo",
                  "mFileTimeLo", 1);
  sf_mex_addfield(*c3_info, c3_b_emlrt_marshallOut(0U), "mFileTimeHi",
                  "mFileTimeHi", 1);
  sf_mex_assign(&c3_rhs1, sf_mex_createcellmatrix(0, 1), false);
  sf_mex_assign(&c3_lhs1, sf_mex_createcellmatrix(0, 1), false);
  sf_mex_addfield(*c3_info, sf_mex_duplicatearraysafe(&c3_rhs1), "rhs", "rhs", 1);
  sf_mex_addfield(*c3_info, sf_mex_duplicatearraysafe(&c3_lhs1), "lhs", "lhs", 1);
  sf_mex_addfield(*c3_info, c3_emlrt_marshallOut(
    "[ILXE]$matlabroot$/toolbox/eml/lib/matlab/datafun/sort.m"), "context",
                  "context", 2);
  sf_mex_addfield(*c3_info, c3_emlrt_marshallOut("eml_sort"), "name", "name", 2);
  sf_mex_addfield(*c3_info, c3_emlrt_marshallOut("int32"), "dominantType",
                  "dominantType", 2);
  sf_mex_addfield(*c3_info, c3_emlrt_marshallOut(
    "[ILXE]$matlabroot$/toolbox/eml/lib/matlab/eml/eml_sort.m"), "resolved",
                  "resolved", 2);
  sf_mex_addfield(*c3_info, c3_b_emlrt_marshallOut(1314740212U), "fileTimeLo",
                  "fileTimeLo", 2);
  sf_mex_addfield(*c3_info, c3_b_emlrt_marshallOut(0U), "fileTimeHi",
                  "fileTimeHi", 2);
  sf_mex_addfield(*c3_info, c3_b_emlrt_marshallOut(0U), "mFileTimeLo",
                  "mFileTimeLo", 2);
  sf_mex_addfield(*c3_info, c3_b_emlrt_marshallOut(0U), "mFileTimeHi",
                  "mFileTimeHi", 2);
  sf_mex_assign(&c3_rhs2, sf_mex_createcellmatrix(0, 1), false);
  sf_mex_assign(&c3_lhs2, sf_mex_createcellmatrix(0, 1), false);
  sf_mex_addfield(*c3_info, sf_mex_duplicatearraysafe(&c3_rhs2), "rhs", "rhs", 2);
  sf_mex_addfield(*c3_info, sf_mex_duplicatearraysafe(&c3_lhs2), "lhs", "lhs", 2);
  sf_mex_addfield(*c3_info, c3_emlrt_marshallOut(
    "[ILXE]$matlabroot$/toolbox/eml/lib/matlab/eml/eml_sort.m"), "context",
                  "context", 3);
  sf_mex_addfield(*c3_info, c3_emlrt_marshallOut("eml_nonsingleton_dim"), "name",
                  "name", 3);
  sf_mex_addfield(*c3_info, c3_emlrt_marshallOut("int32"), "dominantType",
                  "dominantType", 3);
  sf_mex_addfield(*c3_info, c3_emlrt_marshallOut(
    "[ILXE]$matlabroot$/toolbox/eml/lib/matlab/eml/eml_nonsingleton_dim.m"),
                  "resolved", "resolved", 3);
  sf_mex_addfield(*c3_info, c3_b_emlrt_marshallOut(1307654842U), "fileTimeLo",
                  "fileTimeLo", 3);
  sf_mex_addfield(*c3_info, c3_b_emlrt_marshallOut(0U), "fileTimeHi",
                  "fileTimeHi", 3);
  sf_mex_addfield(*c3_info, c3_b_emlrt_marshallOut(0U), "mFileTimeLo",
                  "mFileTimeLo", 3);
  sf_mex_addfield(*c3_info, c3_b_emlrt_marshallOut(0U), "mFileTimeHi",
                  "mFileTimeHi", 3);
  sf_mex_assign(&c3_rhs3, sf_mex_createcellmatrix(0, 1), false);
  sf_mex_assign(&c3_lhs3, sf_mex_createcellmatrix(0, 1), false);
  sf_mex_addfield(*c3_info, sf_mex_duplicatearraysafe(&c3_rhs3), "rhs", "rhs", 3);
  sf_mex_addfield(*c3_info, sf_mex_duplicatearraysafe(&c3_lhs3), "lhs", "lhs", 3);
  sf_mex_addfield(*c3_info, c3_emlrt_marshallOut(
    "[ILXE]$matlabroot$/toolbox/eml/lib/matlab/eml/eml_nonsingleton_dim.m"),
                  "context", "context", 4);
  sf_mex_addfield(*c3_info, c3_emlrt_marshallOut("eml_index_class"), "name",
                  "name", 4);
  sf_mex_addfield(*c3_info, c3_emlrt_marshallOut(""), "dominantType",
                  "dominantType", 4);
  sf_mex_addfield(*c3_info, c3_emlrt_marshallOut(
    "[ILXE]$matlabroot$/toolbox/eml/lib/matlab/eml/eml_index_class.m"),
                  "resolved", "resolved", 4);
  sf_mex_addfield(*c3_info, c3_b_emlrt_marshallOut(1323174178U), "fileTimeLo",
                  "fileTimeLo", 4);
  sf_mex_addfield(*c3_info, c3_b_emlrt_marshallOut(0U), "fileTimeHi",
                  "fileTimeHi", 4);
  sf_mex_addfield(*c3_info, c3_b_emlrt_marshallOut(0U), "mFileTimeLo",
                  "mFileTimeLo", 4);
  sf_mex_addfield(*c3_info, c3_b_emlrt_marshallOut(0U), "mFileTimeHi",
                  "mFileTimeHi", 4);
  sf_mex_assign(&c3_rhs4, sf_mex_createcellmatrix(0, 1), false);
  sf_mex_assign(&c3_lhs4, sf_mex_createcellmatrix(0, 1), false);
  sf_mex_addfield(*c3_info, sf_mex_duplicatearraysafe(&c3_rhs4), "rhs", "rhs", 4);
  sf_mex_addfield(*c3_info, sf_mex_duplicatearraysafe(&c3_lhs4), "lhs", "lhs", 4);
  sf_mex_addfield(*c3_info, c3_emlrt_marshallOut(
    "[ILXE]$matlabroot$/toolbox/eml/lib/matlab/eml/eml_nonsingleton_dim.m"),
                  "context", "context", 5);
  sf_mex_addfield(*c3_info, c3_emlrt_marshallOut(
    "eml_int_forloop_overflow_check"), "name", "name", 5);
  sf_mex_addfield(*c3_info, c3_emlrt_marshallOut(""), "dominantType",
                  "dominantType", 5);
  sf_mex_addfield(*c3_info, c3_emlrt_marshallOut(
    "[ILXE]$matlabroot$/toolbox/eml/lib/matlab/eml/eml_int_forloop_overflow_check.m"),
                  "resolved", "resolved", 5);
  sf_mex_addfield(*c3_info, c3_b_emlrt_marshallOut(1397261022U), "fileTimeLo",
                  "fileTimeLo", 5);
  sf_mex_addfield(*c3_info, c3_b_emlrt_marshallOut(0U), "fileTimeHi",
                  "fileTimeHi", 5);
  sf_mex_addfield(*c3_info, c3_b_emlrt_marshallOut(0U), "mFileTimeLo",
                  "mFileTimeLo", 5);
  sf_mex_addfield(*c3_info, c3_b_emlrt_marshallOut(0U), "mFileTimeHi",
                  "mFileTimeHi", 5);
  sf_mex_assign(&c3_rhs5, sf_mex_createcellmatrix(0, 1), false);
  sf_mex_assign(&c3_lhs5, sf_mex_createcellmatrix(0, 1), false);
  sf_mex_addfield(*c3_info, sf_mex_duplicatearraysafe(&c3_rhs5), "rhs", "rhs", 5);
  sf_mex_addfield(*c3_info, sf_mex_duplicatearraysafe(&c3_lhs5), "lhs", "lhs", 5);
  sf_mex_addfield(*c3_info, c3_emlrt_marshallOut(
    "[ILXE]$matlabroot$/toolbox/eml/lib/matlab/eml/eml_int_forloop_overflow_check.m!eml_int_forloop_overflow_check_helper"),
                  "context", "context", 6);
  sf_mex_addfield(*c3_info, c3_emlrt_marshallOut("isfi"), "name", "name", 6);
  sf_mex_addfield(*c3_info, c3_emlrt_marshallOut("coder.internal.indexInt"),
                  "dominantType", "dominantType", 6);
  sf_mex_addfield(*c3_info, c3_emlrt_marshallOut(
    "[ILXE]$matlabroot$/toolbox/eml/lib/fixedpoint/isfi.m"), "resolved",
                  "resolved", 6);
  sf_mex_addfield(*c3_info, c3_b_emlrt_marshallOut(1346513958U), "fileTimeLo",
                  "fileTimeLo", 6);
  sf_mex_addfield(*c3_info, c3_b_emlrt_marshallOut(0U), "fileTimeHi",
                  "fileTimeHi", 6);
  sf_mex_addfield(*c3_info, c3_b_emlrt_marshallOut(0U), "mFileTimeLo",
                  "mFileTimeLo", 6);
  sf_mex_addfield(*c3_info, c3_b_emlrt_marshallOut(0U), "mFileTimeHi",
                  "mFileTimeHi", 6);
  sf_mex_assign(&c3_rhs6, sf_mex_createcellmatrix(0, 1), false);
  sf_mex_assign(&c3_lhs6, sf_mex_createcellmatrix(0, 1), false);
  sf_mex_addfield(*c3_info, sf_mex_duplicatearraysafe(&c3_rhs6), "rhs", "rhs", 6);
  sf_mex_addfield(*c3_info, sf_mex_duplicatearraysafe(&c3_lhs6), "lhs", "lhs", 6);
  sf_mex_addfield(*c3_info, c3_emlrt_marshallOut(
    "[ILXE]$matlabroot$/toolbox/eml/lib/fixedpoint/isfi.m"), "context",
                  "context", 7);
  sf_mex_addfield(*c3_info, c3_emlrt_marshallOut("isnumerictype"), "name",
                  "name", 7);
  sf_mex_addfield(*c3_info, c3_emlrt_marshallOut("char"), "dominantType",
                  "dominantType", 7);
  sf_mex_addfield(*c3_info, c3_emlrt_marshallOut(
    "[ILXE]$matlabroot$/toolbox/eml/lib/fixedpoint/isnumerictype.m"), "resolved",
                  "resolved", 7);
  sf_mex_addfield(*c3_info, c3_b_emlrt_marshallOut(1398879198U), "fileTimeLo",
                  "fileTimeLo", 7);
  sf_mex_addfield(*c3_info, c3_b_emlrt_marshallOut(0U), "fileTimeHi",
                  "fileTimeHi", 7);
  sf_mex_addfield(*c3_info, c3_b_emlrt_marshallOut(0U), "mFileTimeLo",
                  "mFileTimeLo", 7);
  sf_mex_addfield(*c3_info, c3_b_emlrt_marshallOut(0U), "mFileTimeHi",
                  "mFileTimeHi", 7);
  sf_mex_assign(&c3_rhs7, sf_mex_createcellmatrix(0, 1), false);
  sf_mex_assign(&c3_lhs7, sf_mex_createcellmatrix(0, 1), false);
  sf_mex_addfield(*c3_info, sf_mex_duplicatearraysafe(&c3_rhs7), "rhs", "rhs", 7);
  sf_mex_addfield(*c3_info, sf_mex_duplicatearraysafe(&c3_lhs7), "lhs", "lhs", 7);
  sf_mex_addfield(*c3_info, c3_emlrt_marshallOut(
    "[ILXE]$matlabroot$/toolbox/eml/lib/matlab/eml/eml_int_forloop_overflow_check.m!eml_int_forloop_overflow_check_helper"),
                  "context", "context", 8);
  sf_mex_addfield(*c3_info, c3_emlrt_marshallOut("intmax"), "name", "name", 8);
  sf_mex_addfield(*c3_info, c3_emlrt_marshallOut("char"), "dominantType",
                  "dominantType", 8);
  sf_mex_addfield(*c3_info, c3_emlrt_marshallOut(
    "[ILXE]$matlabroot$/toolbox/eml/lib/matlab/elmat/intmax.m"), "resolved",
                  "resolved", 8);
  sf_mex_addfield(*c3_info, c3_b_emlrt_marshallOut(1362265482U), "fileTimeLo",
                  "fileTimeLo", 8);
  sf_mex_addfield(*c3_info, c3_b_emlrt_marshallOut(0U), "fileTimeHi",
                  "fileTimeHi", 8);
  sf_mex_addfield(*c3_info, c3_b_emlrt_marshallOut(0U), "mFileTimeLo",
                  "mFileTimeLo", 8);
  sf_mex_addfield(*c3_info, c3_b_emlrt_marshallOut(0U), "mFileTimeHi",
                  "mFileTimeHi", 8);
  sf_mex_assign(&c3_rhs8, sf_mex_createcellmatrix(0, 1), false);
  sf_mex_assign(&c3_lhs8, sf_mex_createcellmatrix(0, 1), false);
  sf_mex_addfield(*c3_info, sf_mex_duplicatearraysafe(&c3_rhs8), "rhs", "rhs", 8);
  sf_mex_addfield(*c3_info, sf_mex_duplicatearraysafe(&c3_lhs8), "lhs", "lhs", 8);
  sf_mex_addfield(*c3_info, c3_emlrt_marshallOut(
    "[ILXE]$matlabroot$/toolbox/eml/lib/matlab/elmat/intmax.m"), "context",
                  "context", 9);
  sf_mex_addfield(*c3_info, c3_emlrt_marshallOut("eml_switch_helper"), "name",
                  "name", 9);
  sf_mex_addfield(*c3_info, c3_emlrt_marshallOut(""), "dominantType",
                  "dominantType", 9);
  sf_mex_addfield(*c3_info, c3_emlrt_marshallOut(
    "[ILXE]$matlabroot$/toolbox/eml/lib/matlab/eml/eml_switch_helper.m"),
                  "resolved", "resolved", 9);
  sf_mex_addfield(*c3_info, c3_b_emlrt_marshallOut(1393334458U), "fileTimeLo",
                  "fileTimeLo", 9);
  sf_mex_addfield(*c3_info, c3_b_emlrt_marshallOut(0U), "fileTimeHi",
                  "fileTimeHi", 9);
  sf_mex_addfield(*c3_info, c3_b_emlrt_marshallOut(0U), "mFileTimeLo",
                  "mFileTimeLo", 9);
  sf_mex_addfield(*c3_info, c3_b_emlrt_marshallOut(0U), "mFileTimeHi",
                  "mFileTimeHi", 9);
  sf_mex_assign(&c3_rhs9, sf_mex_createcellmatrix(0, 1), false);
  sf_mex_assign(&c3_lhs9, sf_mex_createcellmatrix(0, 1), false);
  sf_mex_addfield(*c3_info, sf_mex_duplicatearraysafe(&c3_rhs9), "rhs", "rhs", 9);
  sf_mex_addfield(*c3_info, sf_mex_duplicatearraysafe(&c3_lhs9), "lhs", "lhs", 9);
  sf_mex_addfield(*c3_info, c3_emlrt_marshallOut(
    "[ILXE]$matlabroot$/toolbox/eml/lib/matlab/eml/eml_int_forloop_overflow_check.m!eml_int_forloop_overflow_check_helper"),
                  "context", "context", 10);
  sf_mex_addfield(*c3_info, c3_emlrt_marshallOut("intmin"), "name", "name", 10);
  sf_mex_addfield(*c3_info, c3_emlrt_marshallOut("char"), "dominantType",
                  "dominantType", 10);
  sf_mex_addfield(*c3_info, c3_emlrt_marshallOut(
    "[ILXE]$matlabroot$/toolbox/eml/lib/matlab/elmat/intmin.m"), "resolved",
                  "resolved", 10);
  sf_mex_addfield(*c3_info, c3_b_emlrt_marshallOut(1362265482U), "fileTimeLo",
                  "fileTimeLo", 10);
  sf_mex_addfield(*c3_info, c3_b_emlrt_marshallOut(0U), "fileTimeHi",
                  "fileTimeHi", 10);
  sf_mex_addfield(*c3_info, c3_b_emlrt_marshallOut(0U), "mFileTimeLo",
                  "mFileTimeLo", 10);
  sf_mex_addfield(*c3_info, c3_b_emlrt_marshallOut(0U), "mFileTimeHi",
                  "mFileTimeHi", 10);
  sf_mex_assign(&c3_rhs10, sf_mex_createcellmatrix(0, 1), false);
  sf_mex_assign(&c3_lhs10, sf_mex_createcellmatrix(0, 1), false);
  sf_mex_addfield(*c3_info, sf_mex_duplicatearraysafe(&c3_rhs10), "rhs", "rhs",
                  10);
  sf_mex_addfield(*c3_info, sf_mex_duplicatearraysafe(&c3_lhs10), "lhs", "lhs",
                  10);
  sf_mex_addfield(*c3_info, c3_emlrt_marshallOut(
    "[ILXE]$matlabroot$/toolbox/eml/lib/matlab/elmat/intmin.m"), "context",
                  "context", 11);
  sf_mex_addfield(*c3_info, c3_emlrt_marshallOut("eml_switch_helper"), "name",
                  "name", 11);
  sf_mex_addfield(*c3_info, c3_emlrt_marshallOut(""), "dominantType",
                  "dominantType", 11);
  sf_mex_addfield(*c3_info, c3_emlrt_marshallOut(
    "[ILXE]$matlabroot$/toolbox/eml/lib/matlab/eml/eml_switch_helper.m"),
                  "resolved", "resolved", 11);
  sf_mex_addfield(*c3_info, c3_b_emlrt_marshallOut(1393334458U), "fileTimeLo",
                  "fileTimeLo", 11);
  sf_mex_addfield(*c3_info, c3_b_emlrt_marshallOut(0U), "fileTimeHi",
                  "fileTimeHi", 11);
  sf_mex_addfield(*c3_info, c3_b_emlrt_marshallOut(0U), "mFileTimeLo",
                  "mFileTimeLo", 11);
  sf_mex_addfield(*c3_info, c3_b_emlrt_marshallOut(0U), "mFileTimeHi",
                  "mFileTimeHi", 11);
  sf_mex_assign(&c3_rhs11, sf_mex_createcellmatrix(0, 1), false);
  sf_mex_assign(&c3_lhs11, sf_mex_createcellmatrix(0, 1), false);
  sf_mex_addfield(*c3_info, sf_mex_duplicatearraysafe(&c3_rhs11), "rhs", "rhs",
                  11);
  sf_mex_addfield(*c3_info, sf_mex_duplicatearraysafe(&c3_lhs11), "lhs", "lhs",
                  11);
  sf_mex_addfield(*c3_info, c3_emlrt_marshallOut(
    "[ILXE]$matlabroot$/toolbox/eml/lib/matlab/eml/eml_sort.m"), "context",
                  "context", 12);
  sf_mex_addfield(*c3_info, c3_emlrt_marshallOut("eml_assert_valid_dim"), "name",
                  "name", 12);
  sf_mex_addfield(*c3_info, c3_emlrt_marshallOut("coder.internal.indexInt"),
                  "dominantType", "dominantType", 12);
  sf_mex_addfield(*c3_info, c3_emlrt_marshallOut(
    "[ILXE]$matlabroot$/toolbox/eml/lib/matlab/eml/eml_assert_valid_dim.m"),
                  "resolved", "resolved", 12);
  sf_mex_addfield(*c3_info, c3_b_emlrt_marshallOut(1372586016U), "fileTimeLo",
                  "fileTimeLo", 12);
  sf_mex_addfield(*c3_info, c3_b_emlrt_marshallOut(0U), "fileTimeHi",
                  "fileTimeHi", 12);
  sf_mex_addfield(*c3_info, c3_b_emlrt_marshallOut(0U), "mFileTimeLo",
                  "mFileTimeLo", 12);
  sf_mex_addfield(*c3_info, c3_b_emlrt_marshallOut(0U), "mFileTimeHi",
                  "mFileTimeHi", 12);
  sf_mex_assign(&c3_rhs12, sf_mex_createcellmatrix(0, 1), false);
  sf_mex_assign(&c3_lhs12, sf_mex_createcellmatrix(0, 1), false);
  sf_mex_addfield(*c3_info, sf_mex_duplicatearraysafe(&c3_rhs12), "rhs", "rhs",
                  12);
  sf_mex_addfield(*c3_info, sf_mex_duplicatearraysafe(&c3_lhs12), "lhs", "lhs",
                  12);
  sf_mex_addfield(*c3_info, c3_emlrt_marshallOut(
    "[ILXE]$matlabroot$/toolbox/eml/lib/matlab/eml/eml_assert_valid_dim.m"),
                  "context", "context", 13);
  sf_mex_addfield(*c3_info, c3_emlrt_marshallOut("coder.internal.assertValidDim"),
                  "name", "name", 13);
  sf_mex_addfield(*c3_info, c3_emlrt_marshallOut("coder.internal.indexInt"),
                  "dominantType", "dominantType", 13);
  sf_mex_addfield(*c3_info, c3_emlrt_marshallOut(
    "[ILXE]$matlabroot$/toolbox/shared/coder/coder/+coder/+internal/assertValidDim.m"),
                  "resolved", "resolved", 13);
  sf_mex_addfield(*c3_info, c3_b_emlrt_marshallOut(1372586760U), "fileTimeLo",
                  "fileTimeLo", 13);
  sf_mex_addfield(*c3_info, c3_b_emlrt_marshallOut(0U), "fileTimeHi",
                  "fileTimeHi", 13);
  sf_mex_addfield(*c3_info, c3_b_emlrt_marshallOut(0U), "mFileTimeLo",
                  "mFileTimeLo", 13);
  sf_mex_addfield(*c3_info, c3_b_emlrt_marshallOut(0U), "mFileTimeHi",
                  "mFileTimeHi", 13);
  sf_mex_assign(&c3_rhs13, sf_mex_createcellmatrix(0, 1), false);
  sf_mex_assign(&c3_lhs13, sf_mex_createcellmatrix(0, 1), false);
  sf_mex_addfield(*c3_info, sf_mex_duplicatearraysafe(&c3_rhs13), "rhs", "rhs",
                  13);
  sf_mex_addfield(*c3_info, sf_mex_duplicatearraysafe(&c3_lhs13), "lhs", "lhs",
                  13);
  sf_mex_addfield(*c3_info, c3_emlrt_marshallOut(
    "[ILXE]$matlabroot$/toolbox/shared/coder/coder/+coder/+internal/assertValidDim.m"),
                  "context", "context", 14);
  sf_mex_addfield(*c3_info, c3_emlrt_marshallOut(
    "coder.internal.isBuiltInNumeric"), "name", "name", 14);
  sf_mex_addfield(*c3_info, c3_emlrt_marshallOut("coder.internal.indexInt"),
                  "dominantType", "dominantType", 14);
  sf_mex_addfield(*c3_info, c3_emlrt_marshallOut(
    "[ILXE]$matlabroot$/toolbox/shared/coder/coder/+coder/+internal/isBuiltInNumeric.m"),
                  "resolved", "resolved", 14);
  sf_mex_addfield(*c3_info, c3_b_emlrt_marshallOut(1395935456U), "fileTimeLo",
                  "fileTimeLo", 14);
  sf_mex_addfield(*c3_info, c3_b_emlrt_marshallOut(0U), "fileTimeHi",
                  "fileTimeHi", 14);
  sf_mex_addfield(*c3_info, c3_b_emlrt_marshallOut(0U), "mFileTimeLo",
                  "mFileTimeLo", 14);
  sf_mex_addfield(*c3_info, c3_b_emlrt_marshallOut(0U), "mFileTimeHi",
                  "mFileTimeHi", 14);
  sf_mex_assign(&c3_rhs14, sf_mex_createcellmatrix(0, 1), false);
  sf_mex_assign(&c3_lhs14, sf_mex_createcellmatrix(0, 1), false);
  sf_mex_addfield(*c3_info, sf_mex_duplicatearraysafe(&c3_rhs14), "rhs", "rhs",
                  14);
  sf_mex_addfield(*c3_info, sf_mex_duplicatearraysafe(&c3_lhs14), "lhs", "lhs",
                  14);
  sf_mex_addfield(*c3_info, c3_emlrt_marshallOut(
    "[ILXE]$matlabroot$/toolbox/shared/coder/coder/+coder/+internal/assertValidDim.m"),
                  "context", "context", 15);
  sf_mex_addfield(*c3_info, c3_emlrt_marshallOut("floor"), "name", "name", 15);
  sf_mex_addfield(*c3_info, c3_emlrt_marshallOut("coder.internal.indexInt"),
                  "dominantType", "dominantType", 15);
  sf_mex_addfield(*c3_info, c3_emlrt_marshallOut(
    "[ILXE]$matlabroot$/toolbox/eml/lib/matlab/elfun/floor.m"), "resolved",
                  "resolved", 15);
  sf_mex_addfield(*c3_info, c3_b_emlrt_marshallOut(1363717454U), "fileTimeLo",
                  "fileTimeLo", 15);
  sf_mex_addfield(*c3_info, c3_b_emlrt_marshallOut(0U), "fileTimeHi",
                  "fileTimeHi", 15);
  sf_mex_addfield(*c3_info, c3_b_emlrt_marshallOut(0U), "mFileTimeLo",
                  "mFileTimeLo", 15);
  sf_mex_addfield(*c3_info, c3_b_emlrt_marshallOut(0U), "mFileTimeHi",
                  "mFileTimeHi", 15);
  sf_mex_assign(&c3_rhs15, sf_mex_createcellmatrix(0, 1), false);
  sf_mex_assign(&c3_lhs15, sf_mex_createcellmatrix(0, 1), false);
  sf_mex_addfield(*c3_info, sf_mex_duplicatearraysafe(&c3_rhs15), "rhs", "rhs",
                  15);
  sf_mex_addfield(*c3_info, sf_mex_duplicatearraysafe(&c3_lhs15), "lhs", "lhs",
                  15);
  sf_mex_addfield(*c3_info, c3_emlrt_marshallOut(
    "[ILXE]$matlabroot$/toolbox/eml/lib/matlab/elfun/floor.m"), "context",
                  "context", 16);
  sf_mex_addfield(*c3_info, c3_emlrt_marshallOut(
    "coder.internal.isBuiltInNumeric"), "name", "name", 16);
  sf_mex_addfield(*c3_info, c3_emlrt_marshallOut("coder.internal.indexInt"),
                  "dominantType", "dominantType", 16);
  sf_mex_addfield(*c3_info, c3_emlrt_marshallOut(
    "[ILXE]$matlabroot$/toolbox/shared/coder/coder/+coder/+internal/isBuiltInNumeric.m"),
                  "resolved", "resolved", 16);
  sf_mex_addfield(*c3_info, c3_b_emlrt_marshallOut(1395935456U), "fileTimeLo",
                  "fileTimeLo", 16);
  sf_mex_addfield(*c3_info, c3_b_emlrt_marshallOut(0U), "fileTimeHi",
                  "fileTimeHi", 16);
  sf_mex_addfield(*c3_info, c3_b_emlrt_marshallOut(0U), "mFileTimeLo",
                  "mFileTimeLo", 16);
  sf_mex_addfield(*c3_info, c3_b_emlrt_marshallOut(0U), "mFileTimeHi",
                  "mFileTimeHi", 16);
  sf_mex_assign(&c3_rhs16, sf_mex_createcellmatrix(0, 1), false);
  sf_mex_assign(&c3_lhs16, sf_mex_createcellmatrix(0, 1), false);
  sf_mex_addfield(*c3_info, sf_mex_duplicatearraysafe(&c3_rhs16), "rhs", "rhs",
                  16);
  sf_mex_addfield(*c3_info, sf_mex_duplicatearraysafe(&c3_lhs16), "lhs", "lhs",
                  16);
  sf_mex_addfield(*c3_info, c3_emlrt_marshallOut(
    "[ILXE]$matlabroot$/toolbox/eml/lib/matlab/elfun/floor.m"), "context",
                  "context", 17);
  sf_mex_addfield(*c3_info, c3_emlrt_marshallOut("eml_scalar_floor"), "name",
                  "name", 17);
  sf_mex_addfield(*c3_info, c3_emlrt_marshallOut("coder.internal.indexInt"),
                  "dominantType", "dominantType", 17);
  sf_mex_addfield(*c3_info, c3_emlrt_marshallOut(
    "[ILXE]$matlabroot$/toolbox/eml/lib/matlab/elfun/eml_scalar_floor.m"),
                  "resolved", "resolved", 17);
  sf_mex_addfield(*c3_info, c3_b_emlrt_marshallOut(1286822326U), "fileTimeLo",
                  "fileTimeLo", 17);
  sf_mex_addfield(*c3_info, c3_b_emlrt_marshallOut(0U), "fileTimeHi",
                  "fileTimeHi", 17);
  sf_mex_addfield(*c3_info, c3_b_emlrt_marshallOut(0U), "mFileTimeLo",
                  "mFileTimeLo", 17);
  sf_mex_addfield(*c3_info, c3_b_emlrt_marshallOut(0U), "mFileTimeHi",
                  "mFileTimeHi", 17);
  sf_mex_assign(&c3_rhs17, sf_mex_createcellmatrix(0, 1), false);
  sf_mex_assign(&c3_lhs17, sf_mex_createcellmatrix(0, 1), false);
  sf_mex_addfield(*c3_info, sf_mex_duplicatearraysafe(&c3_rhs17), "rhs", "rhs",
                  17);
  sf_mex_addfield(*c3_info, sf_mex_duplicatearraysafe(&c3_lhs17), "lhs", "lhs",
                  17);
  sf_mex_addfield(*c3_info, c3_emlrt_marshallOut(
    "[ILXE]$matlabroot$/toolbox/shared/coder/coder/+coder/+internal/assertValidDim.m"),
                  "context", "context", 18);
  sf_mex_addfield(*c3_info, c3_emlrt_marshallOut("intmax"), "name", "name", 18);
  sf_mex_addfield(*c3_info, c3_emlrt_marshallOut("char"), "dominantType",
                  "dominantType", 18);
  sf_mex_addfield(*c3_info, c3_emlrt_marshallOut(
    "[ILXE]$matlabroot$/toolbox/eml/lib/matlab/elmat/intmax.m"), "resolved",
                  "resolved", 18);
  sf_mex_addfield(*c3_info, c3_b_emlrt_marshallOut(1362265482U), "fileTimeLo",
                  "fileTimeLo", 18);
  sf_mex_addfield(*c3_info, c3_b_emlrt_marshallOut(0U), "fileTimeHi",
                  "fileTimeHi", 18);
  sf_mex_addfield(*c3_info, c3_b_emlrt_marshallOut(0U), "mFileTimeLo",
                  "mFileTimeLo", 18);
  sf_mex_addfield(*c3_info, c3_b_emlrt_marshallOut(0U), "mFileTimeHi",
                  "mFileTimeHi", 18);
  sf_mex_assign(&c3_rhs18, sf_mex_createcellmatrix(0, 1), false);
  sf_mex_assign(&c3_lhs18, sf_mex_createcellmatrix(0, 1), false);
  sf_mex_addfield(*c3_info, sf_mex_duplicatearraysafe(&c3_rhs18), "rhs", "rhs",
                  18);
  sf_mex_addfield(*c3_info, sf_mex_duplicatearraysafe(&c3_lhs18), "lhs", "lhs",
                  18);
  sf_mex_addfield(*c3_info, c3_emlrt_marshallOut(
    "[ILXE]$matlabroot$/toolbox/eml/lib/matlab/eml/eml_sort.m"), "context",
                  "context", 19);
  sf_mex_addfield(*c3_info, c3_emlrt_marshallOut("eml_scalar_eg"), "name",
                  "name", 19);
  sf_mex_addfield(*c3_info, c3_emlrt_marshallOut("int32"), "dominantType",
                  "dominantType", 19);
  sf_mex_addfield(*c3_info, c3_emlrt_marshallOut(
    "[ILXE]$matlabroot$/toolbox/eml/lib/matlab/eml/eml_scalar_eg.m"), "resolved",
                  "resolved", 19);
  sf_mex_addfield(*c3_info, c3_b_emlrt_marshallOut(1375984288U), "fileTimeLo",
                  "fileTimeLo", 19);
  sf_mex_addfield(*c3_info, c3_b_emlrt_marshallOut(0U), "fileTimeHi",
                  "fileTimeHi", 19);
  sf_mex_addfield(*c3_info, c3_b_emlrt_marshallOut(0U), "mFileTimeLo",
                  "mFileTimeLo", 19);
  sf_mex_addfield(*c3_info, c3_b_emlrt_marshallOut(0U), "mFileTimeHi",
                  "mFileTimeHi", 19);
  sf_mex_assign(&c3_rhs19, sf_mex_createcellmatrix(0, 1), false);
  sf_mex_assign(&c3_lhs19, sf_mex_createcellmatrix(0, 1), false);
  sf_mex_addfield(*c3_info, sf_mex_duplicatearraysafe(&c3_rhs19), "rhs", "rhs",
                  19);
  sf_mex_addfield(*c3_info, sf_mex_duplicatearraysafe(&c3_lhs19), "lhs", "lhs",
                  19);
  sf_mex_addfield(*c3_info, c3_emlrt_marshallOut(
    "[ILXE]$matlabroot$/toolbox/eml/lib/matlab/eml/eml_scalar_eg.m"), "context",
                  "context", 20);
  sf_mex_addfield(*c3_info, c3_emlrt_marshallOut("coder.internal.scalarEg"),
                  "name", "name", 20);
  sf_mex_addfield(*c3_info, c3_emlrt_marshallOut("int32"), "dominantType",
                  "dominantType", 20);
  sf_mex_addfield(*c3_info, c3_emlrt_marshallOut(
    "[ILXE]$matlabroot$/toolbox/coder/coder/+coder/+internal/scalarEg.p"),
                  "resolved", "resolved", 20);
  sf_mex_addfield(*c3_info, c3_b_emlrt_marshallOut(1410811370U), "fileTimeLo",
                  "fileTimeLo", 20);
  sf_mex_addfield(*c3_info, c3_b_emlrt_marshallOut(0U), "fileTimeHi",
                  "fileTimeHi", 20);
  sf_mex_addfield(*c3_info, c3_b_emlrt_marshallOut(0U), "mFileTimeLo",
                  "mFileTimeLo", 20);
  sf_mex_addfield(*c3_info, c3_b_emlrt_marshallOut(0U), "mFileTimeHi",
                  "mFileTimeHi", 20);
  sf_mex_assign(&c3_rhs20, sf_mex_createcellmatrix(0, 1), false);
  sf_mex_assign(&c3_lhs20, sf_mex_createcellmatrix(0, 1), false);
  sf_mex_addfield(*c3_info, sf_mex_duplicatearraysafe(&c3_rhs20), "rhs", "rhs",
                  20);
  sf_mex_addfield(*c3_info, sf_mex_duplicatearraysafe(&c3_lhs20), "lhs", "lhs",
                  20);
  sf_mex_addfield(*c3_info, c3_emlrt_marshallOut(
    "[ILXE]$matlabroot$/toolbox/eml/lib/matlab/eml/eml_sort.m"), "context",
                  "context", 21);
  sf_mex_addfield(*c3_info, c3_emlrt_marshallOut("eml_index_class"), "name",
                  "name", 21);
  sf_mex_addfield(*c3_info, c3_emlrt_marshallOut(""), "dominantType",
                  "dominantType", 21);
  sf_mex_addfield(*c3_info, c3_emlrt_marshallOut(
    "[ILXE]$matlabroot$/toolbox/eml/lib/matlab/eml/eml_index_class.m"),
                  "resolved", "resolved", 21);
  sf_mex_addfield(*c3_info, c3_b_emlrt_marshallOut(1323174178U), "fileTimeLo",
                  "fileTimeLo", 21);
  sf_mex_addfield(*c3_info, c3_b_emlrt_marshallOut(0U), "fileTimeHi",
                  "fileTimeHi", 21);
  sf_mex_addfield(*c3_info, c3_b_emlrt_marshallOut(0U), "mFileTimeLo",
                  "mFileTimeLo", 21);
  sf_mex_addfield(*c3_info, c3_b_emlrt_marshallOut(0U), "mFileTimeHi",
                  "mFileTimeHi", 21);
  sf_mex_assign(&c3_rhs21, sf_mex_createcellmatrix(0, 1), false);
  sf_mex_assign(&c3_lhs21, sf_mex_createcellmatrix(0, 1), false);
  sf_mex_addfield(*c3_info, sf_mex_duplicatearraysafe(&c3_rhs21), "rhs", "rhs",
                  21);
  sf_mex_addfield(*c3_info, sf_mex_duplicatearraysafe(&c3_lhs21), "lhs", "lhs",
                  21);
  sf_mex_addfield(*c3_info, c3_emlrt_marshallOut(
    "[ILXE]$matlabroot$/toolbox/eml/lib/matlab/eml/eml_sort.m"), "context",
                  "context", 22);
  sf_mex_addfield(*c3_info, c3_emlrt_marshallOut("eml_matrix_vstride"), "name",
                  "name", 22);
  sf_mex_addfield(*c3_info, c3_emlrt_marshallOut("int32"), "dominantType",
                  "dominantType", 22);
  sf_mex_addfield(*c3_info, c3_emlrt_marshallOut(
    "[ILXE]$matlabroot$/toolbox/eml/lib/matlab/eml/eml_matrix_vstride.m"),
                  "resolved", "resolved", 22);
  sf_mex_addfield(*c3_info, c3_b_emlrt_marshallOut(1360285950U), "fileTimeLo",
                  "fileTimeLo", 22);
  sf_mex_addfield(*c3_info, c3_b_emlrt_marshallOut(0U), "fileTimeHi",
                  "fileTimeHi", 22);
  sf_mex_addfield(*c3_info, c3_b_emlrt_marshallOut(0U), "mFileTimeLo",
                  "mFileTimeLo", 22);
  sf_mex_addfield(*c3_info, c3_b_emlrt_marshallOut(0U), "mFileTimeHi",
                  "mFileTimeHi", 22);
  sf_mex_assign(&c3_rhs22, sf_mex_createcellmatrix(0, 1), false);
  sf_mex_assign(&c3_lhs22, sf_mex_createcellmatrix(0, 1), false);
  sf_mex_addfield(*c3_info, sf_mex_duplicatearraysafe(&c3_rhs22), "rhs", "rhs",
                  22);
  sf_mex_addfield(*c3_info, sf_mex_duplicatearraysafe(&c3_lhs22), "lhs", "lhs",
                  22);
  sf_mex_addfield(*c3_info, c3_emlrt_marshallOut(
    "[ILXE]$matlabroot$/toolbox/eml/lib/matlab/eml/eml_matrix_vstride.m"),
                  "context", "context", 23);
  sf_mex_addfield(*c3_info, c3_emlrt_marshallOut("coder.internal.prodsize"),
                  "name", "name", 23);
  sf_mex_addfield(*c3_info, c3_emlrt_marshallOut("int32"), "dominantType",
                  "dominantType", 23);
  sf_mex_addfield(*c3_info, c3_emlrt_marshallOut(
    "[ILXE]$matlabroot$/toolbox/shared/coder/coder/+coder/+internal/prodsize.m"),
                  "resolved", "resolved", 23);
  sf_mex_addfield(*c3_info, c3_b_emlrt_marshallOut(1360286188U), "fileTimeLo",
                  "fileTimeLo", 23);
  sf_mex_addfield(*c3_info, c3_b_emlrt_marshallOut(0U), "fileTimeHi",
                  "fileTimeHi", 23);
  sf_mex_addfield(*c3_info, c3_b_emlrt_marshallOut(0U), "mFileTimeLo",
                  "mFileTimeLo", 23);
  sf_mex_addfield(*c3_info, c3_b_emlrt_marshallOut(0U), "mFileTimeHi",
                  "mFileTimeHi", 23);
  sf_mex_assign(&c3_rhs23, sf_mex_createcellmatrix(0, 1), false);
  sf_mex_assign(&c3_lhs23, sf_mex_createcellmatrix(0, 1), false);
  sf_mex_addfield(*c3_info, sf_mex_duplicatearraysafe(&c3_rhs23), "rhs", "rhs",
                  23);
  sf_mex_addfield(*c3_info, sf_mex_duplicatearraysafe(&c3_lhs23), "lhs", "lhs",
                  23);
  sf_mex_addfield(*c3_info, c3_emlrt_marshallOut(
    "[ILXE]$matlabroot$/toolbox/shared/coder/coder/+coder/+internal/prodsize.m"),
                  "context", "context", 24);
  sf_mex_addfield(*c3_info, c3_emlrt_marshallOut(
    "eml_int_forloop_overflow_check"), "name", "name", 24);
  sf_mex_addfield(*c3_info, c3_emlrt_marshallOut(""), "dominantType",
                  "dominantType", 24);
  sf_mex_addfield(*c3_info, c3_emlrt_marshallOut(
    "[ILXE]$matlabroot$/toolbox/eml/lib/matlab/eml/eml_int_forloop_overflow_check.m"),
                  "resolved", "resolved", 24);
  sf_mex_addfield(*c3_info, c3_b_emlrt_marshallOut(1397261022U), "fileTimeLo",
                  "fileTimeLo", 24);
  sf_mex_addfield(*c3_info, c3_b_emlrt_marshallOut(0U), "fileTimeHi",
                  "fileTimeHi", 24);
  sf_mex_addfield(*c3_info, c3_b_emlrt_marshallOut(0U), "mFileTimeLo",
                  "mFileTimeLo", 24);
  sf_mex_addfield(*c3_info, c3_b_emlrt_marshallOut(0U), "mFileTimeHi",
                  "mFileTimeHi", 24);
  sf_mex_assign(&c3_rhs24, sf_mex_createcellmatrix(0, 1), false);
  sf_mex_assign(&c3_lhs24, sf_mex_createcellmatrix(0, 1), false);
  sf_mex_addfield(*c3_info, sf_mex_duplicatearraysafe(&c3_rhs24), "rhs", "rhs",
                  24);
  sf_mex_addfield(*c3_info, sf_mex_duplicatearraysafe(&c3_lhs24), "lhs", "lhs",
                  24);
  sf_mex_addfield(*c3_info, c3_emlrt_marshallOut(
    "[ILXE]$matlabroot$/toolbox/eml/lib/matlab/eml/eml_sort.m"), "context",
                  "context", 25);
  sf_mex_addfield(*c3_info, c3_emlrt_marshallOut("eml_index_minus"), "name",
                  "name", 25);
  sf_mex_addfield(*c3_info, c3_emlrt_marshallOut("double"), "dominantType",
                  "dominantType", 25);
  sf_mex_addfield(*c3_info, c3_emlrt_marshallOut(
    "[ILXE]$matlabroot$/toolbox/eml/lib/matlab/eml/eml_index_minus.m"),
                  "resolved", "resolved", 25);
  sf_mex_addfield(*c3_info, c3_b_emlrt_marshallOut(1372586016U), "fileTimeLo",
                  "fileTimeLo", 25);
  sf_mex_addfield(*c3_info, c3_b_emlrt_marshallOut(0U), "fileTimeHi",
                  "fileTimeHi", 25);
  sf_mex_addfield(*c3_info, c3_b_emlrt_marshallOut(0U), "mFileTimeLo",
                  "mFileTimeLo", 25);
  sf_mex_addfield(*c3_info, c3_b_emlrt_marshallOut(0U), "mFileTimeHi",
                  "mFileTimeHi", 25);
  sf_mex_assign(&c3_rhs25, sf_mex_createcellmatrix(0, 1), false);
  sf_mex_assign(&c3_lhs25, sf_mex_createcellmatrix(0, 1), false);
  sf_mex_addfield(*c3_info, sf_mex_duplicatearraysafe(&c3_rhs25), "rhs", "rhs",
                  25);
  sf_mex_addfield(*c3_info, sf_mex_duplicatearraysafe(&c3_lhs25), "lhs", "lhs",
                  25);
  sf_mex_addfield(*c3_info, c3_emlrt_marshallOut(
    "[ILXE]$matlabroot$/toolbox/eml/lib/matlab/eml/eml_index_minus.m"),
                  "context", "context", 26);
  sf_mex_addfield(*c3_info, c3_emlrt_marshallOut("coder.internal.indexMinus"),
                  "name", "name", 26);
  sf_mex_addfield(*c3_info, c3_emlrt_marshallOut("double"), "dominantType",
                  "dominantType", 26);
  sf_mex_addfield(*c3_info, c3_emlrt_marshallOut(
    "[ILXE]$matlabroot$/toolbox/shared/coder/coder/+coder/+internal/indexMinus.m"),
                  "resolved", "resolved", 26);
  sf_mex_addfield(*c3_info, c3_b_emlrt_marshallOut(1372586760U), "fileTimeLo",
                  "fileTimeLo", 26);
  sf_mex_addfield(*c3_info, c3_b_emlrt_marshallOut(0U), "fileTimeHi",
                  "fileTimeHi", 26);
  sf_mex_addfield(*c3_info, c3_b_emlrt_marshallOut(0U), "mFileTimeLo",
                  "mFileTimeLo", 26);
  sf_mex_addfield(*c3_info, c3_b_emlrt_marshallOut(0U), "mFileTimeHi",
                  "mFileTimeHi", 26);
  sf_mex_assign(&c3_rhs26, sf_mex_createcellmatrix(0, 1), false);
  sf_mex_assign(&c3_lhs26, sf_mex_createcellmatrix(0, 1), false);
  sf_mex_addfield(*c3_info, sf_mex_duplicatearraysafe(&c3_rhs26), "rhs", "rhs",
                  26);
  sf_mex_addfield(*c3_info, sf_mex_duplicatearraysafe(&c3_lhs26), "lhs", "lhs",
                  26);
  sf_mex_addfield(*c3_info, c3_emlrt_marshallOut(
    "[ILXE]$matlabroot$/toolbox/eml/lib/matlab/eml/eml_sort.m"), "context",
                  "context", 27);
  sf_mex_addfield(*c3_info, c3_emlrt_marshallOut("eml_index_times"), "name",
                  "name", 27);
  sf_mex_addfield(*c3_info, c3_emlrt_marshallOut("coder.internal.indexInt"),
                  "dominantType", "dominantType", 27);
  sf_mex_addfield(*c3_info, c3_emlrt_marshallOut(
    "[ILXE]$matlabroot$/toolbox/eml/lib/matlab/eml/eml_index_times.m"),
                  "resolved", "resolved", 27);
  sf_mex_addfield(*c3_info, c3_b_emlrt_marshallOut(1372586016U), "fileTimeLo",
                  "fileTimeLo", 27);
  sf_mex_addfield(*c3_info, c3_b_emlrt_marshallOut(0U), "fileTimeHi",
                  "fileTimeHi", 27);
  sf_mex_addfield(*c3_info, c3_b_emlrt_marshallOut(0U), "mFileTimeLo",
                  "mFileTimeLo", 27);
  sf_mex_addfield(*c3_info, c3_b_emlrt_marshallOut(0U), "mFileTimeHi",
                  "mFileTimeHi", 27);
  sf_mex_assign(&c3_rhs27, sf_mex_createcellmatrix(0, 1), false);
  sf_mex_assign(&c3_lhs27, sf_mex_createcellmatrix(0, 1), false);
  sf_mex_addfield(*c3_info, sf_mex_duplicatearraysafe(&c3_rhs27), "rhs", "rhs",
                  27);
  sf_mex_addfield(*c3_info, sf_mex_duplicatearraysafe(&c3_lhs27), "lhs", "lhs",
                  27);
  sf_mex_addfield(*c3_info, c3_emlrt_marshallOut(
    "[ILXE]$matlabroot$/toolbox/eml/lib/matlab/eml/eml_index_times.m"),
                  "context", "context", 28);
  sf_mex_addfield(*c3_info, c3_emlrt_marshallOut("coder.internal.indexTimes"),
                  "name", "name", 28);
  sf_mex_addfield(*c3_info, c3_emlrt_marshallOut("coder.internal.indexInt"),
                  "dominantType", "dominantType", 28);
  sf_mex_addfield(*c3_info, c3_emlrt_marshallOut(
    "[ILXE]$matlabroot$/toolbox/shared/coder/coder/+coder/+internal/indexTimes.m"),
                  "resolved", "resolved", 28);
  sf_mex_addfield(*c3_info, c3_b_emlrt_marshallOut(1372586760U), "fileTimeLo",
                  "fileTimeLo", 28);
  sf_mex_addfield(*c3_info, c3_b_emlrt_marshallOut(0U), "fileTimeHi",
                  "fileTimeHi", 28);
  sf_mex_addfield(*c3_info, c3_b_emlrt_marshallOut(0U), "mFileTimeLo",
                  "mFileTimeLo", 28);
  sf_mex_addfield(*c3_info, c3_b_emlrt_marshallOut(0U), "mFileTimeHi",
                  "mFileTimeHi", 28);
  sf_mex_assign(&c3_rhs28, sf_mex_createcellmatrix(0, 1), false);
  sf_mex_assign(&c3_lhs28, sf_mex_createcellmatrix(0, 1), false);
  sf_mex_addfield(*c3_info, sf_mex_duplicatearraysafe(&c3_rhs28), "rhs", "rhs",
                  28);
  sf_mex_addfield(*c3_info, sf_mex_duplicatearraysafe(&c3_lhs28), "lhs", "lhs",
                  28);
  sf_mex_addfield(*c3_info, c3_emlrt_marshallOut(
    "[ILXE]$matlabroot$/toolbox/eml/lib/matlab/eml/eml_sort.m"), "context",
                  "context", 29);
  sf_mex_addfield(*c3_info, c3_emlrt_marshallOut("eml_matrix_npages"), "name",
                  "name", 29);
  sf_mex_addfield(*c3_info, c3_emlrt_marshallOut("int32"), "dominantType",
                  "dominantType", 29);
  sf_mex_addfield(*c3_info, c3_emlrt_marshallOut(
    "[ILXE]$matlabroot$/toolbox/eml/lib/matlab/eml/eml_matrix_npages.m"),
                  "resolved", "resolved", 29);
  sf_mex_addfield(*c3_info, c3_b_emlrt_marshallOut(1360285950U), "fileTimeLo",
                  "fileTimeLo", 29);
  sf_mex_addfield(*c3_info, c3_b_emlrt_marshallOut(0U), "fileTimeHi",
                  "fileTimeHi", 29);
  sf_mex_addfield(*c3_info, c3_b_emlrt_marshallOut(0U), "mFileTimeLo",
                  "mFileTimeLo", 29);
  sf_mex_addfield(*c3_info, c3_b_emlrt_marshallOut(0U), "mFileTimeHi",
                  "mFileTimeHi", 29);
  sf_mex_assign(&c3_rhs29, sf_mex_createcellmatrix(0, 1), false);
  sf_mex_assign(&c3_lhs29, sf_mex_createcellmatrix(0, 1), false);
  sf_mex_addfield(*c3_info, sf_mex_duplicatearraysafe(&c3_rhs29), "rhs", "rhs",
                  29);
  sf_mex_addfield(*c3_info, sf_mex_duplicatearraysafe(&c3_lhs29), "lhs", "lhs",
                  29);
  sf_mex_addfield(*c3_info, c3_emlrt_marshallOut(
    "[ILXE]$matlabroot$/toolbox/eml/lib/matlab/eml/eml_matrix_npages.m"),
                  "context", "context", 30);
  sf_mex_addfield(*c3_info, c3_emlrt_marshallOut("coder.internal.prodsize"),
                  "name", "name", 30);
  sf_mex_addfield(*c3_info, c3_emlrt_marshallOut("int32"), "dominantType",
                  "dominantType", 30);
  sf_mex_addfield(*c3_info, c3_emlrt_marshallOut(
    "[ILXE]$matlabroot$/toolbox/shared/coder/coder/+coder/+internal/prodsize.m"),
                  "resolved", "resolved", 30);
  sf_mex_addfield(*c3_info, c3_b_emlrt_marshallOut(1360286188U), "fileTimeLo",
                  "fileTimeLo", 30);
  sf_mex_addfield(*c3_info, c3_b_emlrt_marshallOut(0U), "fileTimeHi",
                  "fileTimeHi", 30);
  sf_mex_addfield(*c3_info, c3_b_emlrt_marshallOut(0U), "mFileTimeLo",
                  "mFileTimeLo", 30);
  sf_mex_addfield(*c3_info, c3_b_emlrt_marshallOut(0U), "mFileTimeHi",
                  "mFileTimeHi", 30);
  sf_mex_assign(&c3_rhs30, sf_mex_createcellmatrix(0, 1), false);
  sf_mex_assign(&c3_lhs30, sf_mex_createcellmatrix(0, 1), false);
  sf_mex_addfield(*c3_info, sf_mex_duplicatearraysafe(&c3_rhs30), "rhs", "rhs",
                  30);
  sf_mex_addfield(*c3_info, sf_mex_duplicatearraysafe(&c3_lhs30), "lhs", "lhs",
                  30);
  sf_mex_addfield(*c3_info, c3_emlrt_marshallOut(
    "[ILXE]$matlabroot$/toolbox/eml/lib/matlab/eml/eml_sort.m"), "context",
                  "context", 31);
  sf_mex_addfield(*c3_info, c3_emlrt_marshallOut(
    "eml_int_forloop_overflow_check"), "name", "name", 31);
  sf_mex_addfield(*c3_info, c3_emlrt_marshallOut(""), "dominantType",
                  "dominantType", 31);
  sf_mex_addfield(*c3_info, c3_emlrt_marshallOut(
    "[ILXE]$matlabroot$/toolbox/eml/lib/matlab/eml/eml_int_forloop_overflow_check.m"),
                  "resolved", "resolved", 31);
  sf_mex_addfield(*c3_info, c3_b_emlrt_marshallOut(1397261022U), "fileTimeLo",
                  "fileTimeLo", 31);
  sf_mex_addfield(*c3_info, c3_b_emlrt_marshallOut(0U), "fileTimeHi",
                  "fileTimeHi", 31);
  sf_mex_addfield(*c3_info, c3_b_emlrt_marshallOut(0U), "mFileTimeLo",
                  "mFileTimeLo", 31);
  sf_mex_addfield(*c3_info, c3_b_emlrt_marshallOut(0U), "mFileTimeHi",
                  "mFileTimeHi", 31);
  sf_mex_assign(&c3_rhs31, sf_mex_createcellmatrix(0, 1), false);
  sf_mex_assign(&c3_lhs31, sf_mex_createcellmatrix(0, 1), false);
  sf_mex_addfield(*c3_info, sf_mex_duplicatearraysafe(&c3_rhs31), "rhs", "rhs",
                  31);
  sf_mex_addfield(*c3_info, sf_mex_duplicatearraysafe(&c3_lhs31), "lhs", "lhs",
                  31);
  sf_mex_addfield(*c3_info, c3_emlrt_marshallOut(
    "[ILXE]$matlabroot$/toolbox/eml/lib/matlab/eml/eml_sort.m"), "context",
                  "context", 32);
  sf_mex_addfield(*c3_info, c3_emlrt_marshallOut("eml_index_plus"), "name",
                  "name", 32);
  sf_mex_addfield(*c3_info, c3_emlrt_marshallOut("coder.internal.indexInt"),
                  "dominantType", "dominantType", 32);
  sf_mex_addfield(*c3_info, c3_emlrt_marshallOut(
    "[ILXE]$matlabroot$/toolbox/eml/lib/matlab/eml/eml_index_plus.m"),
                  "resolved", "resolved", 32);
  sf_mex_addfield(*c3_info, c3_b_emlrt_marshallOut(1372586016U), "fileTimeLo",
                  "fileTimeLo", 32);
  sf_mex_addfield(*c3_info, c3_b_emlrt_marshallOut(0U), "fileTimeHi",
                  "fileTimeHi", 32);
  sf_mex_addfield(*c3_info, c3_b_emlrt_marshallOut(0U), "mFileTimeLo",
                  "mFileTimeLo", 32);
  sf_mex_addfield(*c3_info, c3_b_emlrt_marshallOut(0U), "mFileTimeHi",
                  "mFileTimeHi", 32);
  sf_mex_assign(&c3_rhs32, sf_mex_createcellmatrix(0, 1), false);
  sf_mex_assign(&c3_lhs32, sf_mex_createcellmatrix(0, 1), false);
  sf_mex_addfield(*c3_info, sf_mex_duplicatearraysafe(&c3_rhs32), "rhs", "rhs",
                  32);
  sf_mex_addfield(*c3_info, sf_mex_duplicatearraysafe(&c3_lhs32), "lhs", "lhs",
                  32);
  sf_mex_addfield(*c3_info, c3_emlrt_marshallOut(
    "[ILXE]$matlabroot$/toolbox/eml/lib/matlab/eml/eml_index_plus.m"), "context",
                  "context", 33);
  sf_mex_addfield(*c3_info, c3_emlrt_marshallOut("coder.internal.indexPlus"),
                  "name", "name", 33);
  sf_mex_addfield(*c3_info, c3_emlrt_marshallOut("coder.internal.indexInt"),
                  "dominantType", "dominantType", 33);
  sf_mex_addfield(*c3_info, c3_emlrt_marshallOut(
    "[ILXE]$matlabroot$/toolbox/shared/coder/coder/+coder/+internal/indexPlus.m"),
                  "resolved", "resolved", 33);
  sf_mex_addfield(*c3_info, c3_b_emlrt_marshallOut(1372586760U), "fileTimeLo",
                  "fileTimeLo", 33);
  sf_mex_addfield(*c3_info, c3_b_emlrt_marshallOut(0U), "fileTimeHi",
                  "fileTimeHi", 33);
  sf_mex_addfield(*c3_info, c3_b_emlrt_marshallOut(0U), "mFileTimeLo",
                  "mFileTimeLo", 33);
  sf_mex_addfield(*c3_info, c3_b_emlrt_marshallOut(0U), "mFileTimeHi",
                  "mFileTimeHi", 33);
  sf_mex_assign(&c3_rhs33, sf_mex_createcellmatrix(0, 1), false);
  sf_mex_assign(&c3_lhs33, sf_mex_createcellmatrix(0, 1), false);
  sf_mex_addfield(*c3_info, sf_mex_duplicatearraysafe(&c3_rhs33), "rhs", "rhs",
                  33);
  sf_mex_addfield(*c3_info, sf_mex_duplicatearraysafe(&c3_lhs33), "lhs", "lhs",
                  33);
  sf_mex_addfield(*c3_info, c3_emlrt_marshallOut(
    "[ILXE]$matlabroot$/toolbox/eml/lib/matlab/eml/eml_sort.m"), "context",
                  "context", 34);
  sf_mex_addfield(*c3_info, c3_emlrt_marshallOut("eml_index_plus"), "name",
                  "name", 34);
  sf_mex_addfield(*c3_info, c3_emlrt_marshallOut("double"), "dominantType",
                  "dominantType", 34);
  sf_mex_addfield(*c3_info, c3_emlrt_marshallOut(
    "[ILXE]$matlabroot$/toolbox/eml/lib/matlab/eml/eml_index_plus.m"),
                  "resolved", "resolved", 34);
  sf_mex_addfield(*c3_info, c3_b_emlrt_marshallOut(1372586016U), "fileTimeLo",
                  "fileTimeLo", 34);
  sf_mex_addfield(*c3_info, c3_b_emlrt_marshallOut(0U), "fileTimeHi",
                  "fileTimeHi", 34);
  sf_mex_addfield(*c3_info, c3_b_emlrt_marshallOut(0U), "mFileTimeLo",
                  "mFileTimeLo", 34);
  sf_mex_addfield(*c3_info, c3_b_emlrt_marshallOut(0U), "mFileTimeHi",
                  "mFileTimeHi", 34);
  sf_mex_assign(&c3_rhs34, sf_mex_createcellmatrix(0, 1), false);
  sf_mex_assign(&c3_lhs34, sf_mex_createcellmatrix(0, 1), false);
  sf_mex_addfield(*c3_info, sf_mex_duplicatearraysafe(&c3_rhs34), "rhs", "rhs",
                  34);
  sf_mex_addfield(*c3_info, sf_mex_duplicatearraysafe(&c3_lhs34), "lhs", "lhs",
                  34);
  sf_mex_addfield(*c3_info, c3_emlrt_marshallOut(
    "[ILXE]$matlabroot$/toolbox/eml/lib/matlab/eml/eml_index_plus.m"), "context",
                  "context", 35);
  sf_mex_addfield(*c3_info, c3_emlrt_marshallOut("coder.internal.indexPlus"),
                  "name", "name", 35);
  sf_mex_addfield(*c3_info, c3_emlrt_marshallOut("double"), "dominantType",
                  "dominantType", 35);
  sf_mex_addfield(*c3_info, c3_emlrt_marshallOut(
    "[ILXE]$matlabroot$/toolbox/shared/coder/coder/+coder/+internal/indexPlus.m"),
                  "resolved", "resolved", 35);
  sf_mex_addfield(*c3_info, c3_b_emlrt_marshallOut(1372586760U), "fileTimeLo",
                  "fileTimeLo", 35);
  sf_mex_addfield(*c3_info, c3_b_emlrt_marshallOut(0U), "fileTimeHi",
                  "fileTimeHi", 35);
  sf_mex_addfield(*c3_info, c3_b_emlrt_marshallOut(0U), "mFileTimeLo",
                  "mFileTimeLo", 35);
  sf_mex_addfield(*c3_info, c3_b_emlrt_marshallOut(0U), "mFileTimeHi",
                  "mFileTimeHi", 35);
  sf_mex_assign(&c3_rhs35, sf_mex_createcellmatrix(0, 1), false);
  sf_mex_assign(&c3_lhs35, sf_mex_createcellmatrix(0, 1), false);
  sf_mex_addfield(*c3_info, sf_mex_duplicatearraysafe(&c3_rhs35), "rhs", "rhs",
                  35);
  sf_mex_addfield(*c3_info, sf_mex_duplicatearraysafe(&c3_lhs35), "lhs", "lhs",
                  35);
  sf_mex_addfield(*c3_info, c3_emlrt_marshallOut(
    "[ILXE]$matlabroot$/toolbox/eml/lib/matlab/eml/eml_sort.m"), "context",
                  "context", 36);
  sf_mex_addfield(*c3_info, c3_emlrt_marshallOut("eml_sort_idx"), "name", "name",
                  36);
  sf_mex_addfield(*c3_info, c3_emlrt_marshallOut("int32"), "dominantType",
                  "dominantType", 36);
  sf_mex_addfield(*c3_info, c3_emlrt_marshallOut(
    "[ILXE]$matlabroot$/toolbox/eml/lib/matlab/eml/eml_sort_idx.m"), "resolved",
                  "resolved", 36);
  sf_mex_addfield(*c3_info, c3_b_emlrt_marshallOut(1305321604U), "fileTimeLo",
                  "fileTimeLo", 36);
  sf_mex_addfield(*c3_info, c3_b_emlrt_marshallOut(0U), "fileTimeHi",
                  "fileTimeHi", 36);
  sf_mex_addfield(*c3_info, c3_b_emlrt_marshallOut(0U), "mFileTimeLo",
                  "mFileTimeLo", 36);
  sf_mex_addfield(*c3_info, c3_b_emlrt_marshallOut(0U), "mFileTimeHi",
                  "mFileTimeHi", 36);
  sf_mex_assign(&c3_rhs36, sf_mex_createcellmatrix(0, 1), false);
  sf_mex_assign(&c3_lhs36, sf_mex_createcellmatrix(0, 1), false);
  sf_mex_addfield(*c3_info, sf_mex_duplicatearraysafe(&c3_rhs36), "rhs", "rhs",
                  36);
  sf_mex_addfield(*c3_info, sf_mex_duplicatearraysafe(&c3_lhs36), "lhs", "lhs",
                  36);
  sf_mex_addfield(*c3_info, c3_emlrt_marshallOut(
    "[ILXE]$matlabroot$/toolbox/eml/lib/matlab/eml/eml_sort_idx.m"), "context",
                  "context", 37);
  sf_mex_addfield(*c3_info, c3_emlrt_marshallOut("eml_index_class"), "name",
                  "name", 37);
  sf_mex_addfield(*c3_info, c3_emlrt_marshallOut(""), "dominantType",
                  "dominantType", 37);
  sf_mex_addfield(*c3_info, c3_emlrt_marshallOut(
    "[ILXE]$matlabroot$/toolbox/eml/lib/matlab/eml/eml_index_class.m"),
                  "resolved", "resolved", 37);
  sf_mex_addfield(*c3_info, c3_b_emlrt_marshallOut(1323174178U), "fileTimeLo",
                  "fileTimeLo", 37);
  sf_mex_addfield(*c3_info, c3_b_emlrt_marshallOut(0U), "fileTimeHi",
                  "fileTimeHi", 37);
  sf_mex_addfield(*c3_info, c3_b_emlrt_marshallOut(0U), "mFileTimeLo",
                  "mFileTimeLo", 37);
  sf_mex_addfield(*c3_info, c3_b_emlrt_marshallOut(0U), "mFileTimeHi",
                  "mFileTimeHi", 37);
  sf_mex_assign(&c3_rhs37, sf_mex_createcellmatrix(0, 1), false);
  sf_mex_assign(&c3_lhs37, sf_mex_createcellmatrix(0, 1), false);
  sf_mex_addfield(*c3_info, sf_mex_duplicatearraysafe(&c3_rhs37), "rhs", "rhs",
                  37);
  sf_mex_addfield(*c3_info, sf_mex_duplicatearraysafe(&c3_lhs37), "lhs", "lhs",
                  37);
  sf_mex_addfield(*c3_info, c3_emlrt_marshallOut(
    "[ILXE]$matlabroot$/toolbox/eml/lib/matlab/eml/eml_sort_idx.m"), "context",
                  "context", 38);
  sf_mex_addfield(*c3_info, c3_emlrt_marshallOut("eml_index_plus"), "name",
                  "name", 38);
  sf_mex_addfield(*c3_info, c3_emlrt_marshallOut("coder.internal.indexInt"),
                  "dominantType", "dominantType", 38);
  sf_mex_addfield(*c3_info, c3_emlrt_marshallOut(
    "[ILXE]$matlabroot$/toolbox/eml/lib/matlab/eml/eml_index_plus.m"),
                  "resolved", "resolved", 38);
  sf_mex_addfield(*c3_info, c3_b_emlrt_marshallOut(1372586016U), "fileTimeLo",
                  "fileTimeLo", 38);
  sf_mex_addfield(*c3_info, c3_b_emlrt_marshallOut(0U), "fileTimeHi",
                  "fileTimeHi", 38);
  sf_mex_addfield(*c3_info, c3_b_emlrt_marshallOut(0U), "mFileTimeLo",
                  "mFileTimeLo", 38);
  sf_mex_addfield(*c3_info, c3_b_emlrt_marshallOut(0U), "mFileTimeHi",
                  "mFileTimeHi", 38);
  sf_mex_assign(&c3_rhs38, sf_mex_createcellmatrix(0, 1), false);
  sf_mex_assign(&c3_lhs38, sf_mex_createcellmatrix(0, 1), false);
  sf_mex_addfield(*c3_info, sf_mex_duplicatearraysafe(&c3_rhs38), "rhs", "rhs",
                  38);
  sf_mex_addfield(*c3_info, sf_mex_duplicatearraysafe(&c3_lhs38), "lhs", "lhs",
                  38);
  sf_mex_addfield(*c3_info, c3_emlrt_marshallOut(
    "[ILXE]$matlabroot$/toolbox/eml/lib/matlab/eml/eml_sort_idx.m"), "context",
                  "context", 39);
  sf_mex_addfield(*c3_info, c3_emlrt_marshallOut(
    "eml_int_forloop_overflow_check"), "name", "name", 39);
  sf_mex_addfield(*c3_info, c3_emlrt_marshallOut(""), "dominantType",
                  "dominantType", 39);
  sf_mex_addfield(*c3_info, c3_emlrt_marshallOut(
    "[ILXE]$matlabroot$/toolbox/eml/lib/matlab/eml/eml_int_forloop_overflow_check.m"),
                  "resolved", "resolved", 39);
  sf_mex_addfield(*c3_info, c3_b_emlrt_marshallOut(1397261022U), "fileTimeLo",
                  "fileTimeLo", 39);
  sf_mex_addfield(*c3_info, c3_b_emlrt_marshallOut(0U), "fileTimeHi",
                  "fileTimeHi", 39);
  sf_mex_addfield(*c3_info, c3_b_emlrt_marshallOut(0U), "mFileTimeLo",
                  "mFileTimeLo", 39);
  sf_mex_addfield(*c3_info, c3_b_emlrt_marshallOut(0U), "mFileTimeHi",
                  "mFileTimeHi", 39);
  sf_mex_assign(&c3_rhs39, sf_mex_createcellmatrix(0, 1), false);
  sf_mex_assign(&c3_lhs39, sf_mex_createcellmatrix(0, 1), false);
  sf_mex_addfield(*c3_info, sf_mex_duplicatearraysafe(&c3_rhs39), "rhs", "rhs",
                  39);
  sf_mex_addfield(*c3_info, sf_mex_duplicatearraysafe(&c3_lhs39), "lhs", "lhs",
                  39);
  sf_mex_addfield(*c3_info, c3_emlrt_marshallOut(
    "[ILXE]$matlabroot$/toolbox/eml/lib/matlab/eml/eml_sort_idx.m"), "context",
                  "context", 40);
  sf_mex_addfield(*c3_info, c3_emlrt_marshallOut("eml_index_minus"), "name",
                  "name", 40);
  sf_mex_addfield(*c3_info, c3_emlrt_marshallOut("double"), "dominantType",
                  "dominantType", 40);
  sf_mex_addfield(*c3_info, c3_emlrt_marshallOut(
    "[ILXE]$matlabroot$/toolbox/eml/lib/matlab/eml/eml_index_minus.m"),
                  "resolved", "resolved", 40);
  sf_mex_addfield(*c3_info, c3_b_emlrt_marshallOut(1372586016U), "fileTimeLo",
                  "fileTimeLo", 40);
  sf_mex_addfield(*c3_info, c3_b_emlrt_marshallOut(0U), "fileTimeHi",
                  "fileTimeHi", 40);
  sf_mex_addfield(*c3_info, c3_b_emlrt_marshallOut(0U), "mFileTimeLo",
                  "mFileTimeLo", 40);
  sf_mex_addfield(*c3_info, c3_b_emlrt_marshallOut(0U), "mFileTimeHi",
                  "mFileTimeHi", 40);
  sf_mex_assign(&c3_rhs40, sf_mex_createcellmatrix(0, 1), false);
  sf_mex_assign(&c3_lhs40, sf_mex_createcellmatrix(0, 1), false);
  sf_mex_addfield(*c3_info, sf_mex_duplicatearraysafe(&c3_rhs40), "rhs", "rhs",
                  40);
  sf_mex_addfield(*c3_info, sf_mex_duplicatearraysafe(&c3_lhs40), "lhs", "lhs",
                  40);
  sf_mex_addfield(*c3_info, c3_emlrt_marshallOut(
    "[ILXE]$matlabroot$/toolbox/eml/lib/matlab/eml/eml_sort_idx.m"), "context",
                  "context", 41);
  sf_mex_addfield(*c3_info, c3_emlrt_marshallOut("eml_index_plus"), "name",
                  "name", 41);
  sf_mex_addfield(*c3_info, c3_emlrt_marshallOut("double"), "dominantType",
                  "dominantType", 41);
  sf_mex_addfield(*c3_info, c3_emlrt_marshallOut(
    "[ILXE]$matlabroot$/toolbox/eml/lib/matlab/eml/eml_index_plus.m"),
                  "resolved", "resolved", 41);
  sf_mex_addfield(*c3_info, c3_b_emlrt_marshallOut(1372586016U), "fileTimeLo",
                  "fileTimeLo", 41);
  sf_mex_addfield(*c3_info, c3_b_emlrt_marshallOut(0U), "fileTimeHi",
                  "fileTimeHi", 41);
  sf_mex_addfield(*c3_info, c3_b_emlrt_marshallOut(0U), "mFileTimeLo",
                  "mFileTimeLo", 41);
  sf_mex_addfield(*c3_info, c3_b_emlrt_marshallOut(0U), "mFileTimeHi",
                  "mFileTimeHi", 41);
  sf_mex_assign(&c3_rhs41, sf_mex_createcellmatrix(0, 1), false);
  sf_mex_assign(&c3_lhs41, sf_mex_createcellmatrix(0, 1), false);
  sf_mex_addfield(*c3_info, sf_mex_duplicatearraysafe(&c3_rhs41), "rhs", "rhs",
                  41);
  sf_mex_addfield(*c3_info, sf_mex_duplicatearraysafe(&c3_lhs41), "lhs", "lhs",
                  41);
  sf_mex_addfield(*c3_info, c3_emlrt_marshallOut(
    "[ILXE]$matlabroot$/toolbox/eml/lib/matlab/eml/eml_sort_idx.m"), "context",
                  "context", 42);
  sf_mex_addfield(*c3_info, c3_emlrt_marshallOut("eml_sort_le"), "name", "name",
                  42);
  sf_mex_addfield(*c3_info, c3_emlrt_marshallOut("int32"), "dominantType",
                  "dominantType", 42);
  sf_mex_addfield(*c3_info, c3_emlrt_marshallOut(
    "[ILXE]$matlabroot$/toolbox/eml/lib/matlab/eml/eml_sort_le.m"), "resolved",
                  "resolved", 42);
  sf_mex_addfield(*c3_info, c3_b_emlrt_marshallOut(1292194110U), "fileTimeLo",
                  "fileTimeLo", 42);
  sf_mex_addfield(*c3_info, c3_b_emlrt_marshallOut(0U), "fileTimeHi",
                  "fileTimeHi", 42);
  sf_mex_addfield(*c3_info, c3_b_emlrt_marshallOut(0U), "mFileTimeLo",
                  "mFileTimeLo", 42);
  sf_mex_addfield(*c3_info, c3_b_emlrt_marshallOut(0U), "mFileTimeHi",
                  "mFileTimeHi", 42);
  sf_mex_assign(&c3_rhs42, sf_mex_createcellmatrix(0, 1), false);
  sf_mex_assign(&c3_lhs42, sf_mex_createcellmatrix(0, 1), false);
  sf_mex_addfield(*c3_info, sf_mex_duplicatearraysafe(&c3_rhs42), "rhs", "rhs",
                  42);
  sf_mex_addfield(*c3_info, sf_mex_duplicatearraysafe(&c3_lhs42), "lhs", "lhs",
                  42);
  sf_mex_addfield(*c3_info, c3_emlrt_marshallOut(
    "[ILXE]$matlabroot$/toolbox/eml/lib/matlab/eml/eml_sort_le.m!eml_sort_ascending_le"),
                  "context", "context", 43);
  sf_mex_addfield(*c3_info, c3_emlrt_marshallOut("eml_relop"), "name", "name",
                  43);
  sf_mex_addfield(*c3_info, c3_emlrt_marshallOut("function_handle"),
                  "dominantType", "dominantType", 43);
  sf_mex_addfield(*c3_info, c3_emlrt_marshallOut(
    "[ILXE]$matlabroot$/toolbox/eml/lib/matlab/eml/eml_relop.m"), "resolved",
                  "resolved", 43);
  sf_mex_addfield(*c3_info, c3_b_emlrt_marshallOut(1342454782U), "fileTimeLo",
                  "fileTimeLo", 43);
  sf_mex_addfield(*c3_info, c3_b_emlrt_marshallOut(0U), "fileTimeHi",
                  "fileTimeHi", 43);
  sf_mex_addfield(*c3_info, c3_b_emlrt_marshallOut(0U), "mFileTimeLo",
                  "mFileTimeLo", 43);
  sf_mex_addfield(*c3_info, c3_b_emlrt_marshallOut(0U), "mFileTimeHi",
                  "mFileTimeHi", 43);
  sf_mex_assign(&c3_rhs43, sf_mex_createcellmatrix(0, 1), false);
  sf_mex_assign(&c3_lhs43, sf_mex_createcellmatrix(0, 1), false);
  sf_mex_addfield(*c3_info, sf_mex_duplicatearraysafe(&c3_rhs43), "rhs", "rhs",
                  43);
  sf_mex_addfield(*c3_info, sf_mex_duplicatearraysafe(&c3_lhs43), "lhs", "lhs",
                  43);
  sf_mex_addfield(*c3_info, c3_emlrt_marshallOut(
    "[ILXE]$matlabroot$/toolbox/eml/lib/matlab/eml/eml_sort_le.m!eml_sort_ascending_le"),
                  "context", "context", 44);
  sf_mex_addfield(*c3_info, c3_emlrt_marshallOut("isnan"), "name", "name", 44);
  sf_mex_addfield(*c3_info, c3_emlrt_marshallOut("int32"), "dominantType",
                  "dominantType", 44);
  sf_mex_addfield(*c3_info, c3_emlrt_marshallOut(
    "[ILXE]$matlabroot$/toolbox/eml/lib/matlab/elmat/isnan.m"), "resolved",
                  "resolved", 44);
  sf_mex_addfield(*c3_info, c3_b_emlrt_marshallOut(1363717458U), "fileTimeLo",
                  "fileTimeLo", 44);
  sf_mex_addfield(*c3_info, c3_b_emlrt_marshallOut(0U), "fileTimeHi",
                  "fileTimeHi", 44);
  sf_mex_addfield(*c3_info, c3_b_emlrt_marshallOut(0U), "mFileTimeLo",
                  "mFileTimeLo", 44);
  sf_mex_addfield(*c3_info, c3_b_emlrt_marshallOut(0U), "mFileTimeHi",
                  "mFileTimeHi", 44);
  sf_mex_assign(&c3_rhs44, sf_mex_createcellmatrix(0, 1), false);
  sf_mex_assign(&c3_lhs44, sf_mex_createcellmatrix(0, 1), false);
  sf_mex_addfield(*c3_info, sf_mex_duplicatearraysafe(&c3_rhs44), "rhs", "rhs",
                  44);
  sf_mex_addfield(*c3_info, sf_mex_duplicatearraysafe(&c3_lhs44), "lhs", "lhs",
                  44);
  sf_mex_addfield(*c3_info, c3_emlrt_marshallOut(
    "[ILXE]$matlabroot$/toolbox/eml/lib/matlab/elmat/isnan.m"), "context",
                  "context", 45);
  sf_mex_addfield(*c3_info, c3_emlrt_marshallOut(
    "coder.internal.isBuiltInNumeric"), "name", "name", 45);
  sf_mex_addfield(*c3_info, c3_emlrt_marshallOut("int32"), "dominantType",
                  "dominantType", 45);
  sf_mex_addfield(*c3_info, c3_emlrt_marshallOut(
    "[ILXE]$matlabroot$/toolbox/shared/coder/coder/+coder/+internal/isBuiltInNumeric.m"),
                  "resolved", "resolved", 45);
  sf_mex_addfield(*c3_info, c3_b_emlrt_marshallOut(1395935456U), "fileTimeLo",
                  "fileTimeLo", 45);
  sf_mex_addfield(*c3_info, c3_b_emlrt_marshallOut(0U), "fileTimeHi",
                  "fileTimeHi", 45);
  sf_mex_addfield(*c3_info, c3_b_emlrt_marshallOut(0U), "mFileTimeLo",
                  "mFileTimeLo", 45);
  sf_mex_addfield(*c3_info, c3_b_emlrt_marshallOut(0U), "mFileTimeHi",
                  "mFileTimeHi", 45);
  sf_mex_assign(&c3_rhs45, sf_mex_createcellmatrix(0, 1), false);
  sf_mex_assign(&c3_lhs45, sf_mex_createcellmatrix(0, 1), false);
  sf_mex_addfield(*c3_info, sf_mex_duplicatearraysafe(&c3_rhs45), "rhs", "rhs",
                  45);
  sf_mex_addfield(*c3_info, sf_mex_duplicatearraysafe(&c3_lhs45), "lhs", "lhs",
                  45);
  sf_mex_addfield(*c3_info, c3_emlrt_marshallOut(
    "[ILXE]$matlabroot$/toolbox/eml/lib/matlab/eml/eml_sort_idx.m"), "context",
                  "context", 46);
  sf_mex_addfield(*c3_info, c3_emlrt_marshallOut("eml_index_times"), "name",
                  "name", 46);
  sf_mex_addfield(*c3_info, c3_emlrt_marshallOut("double"), "dominantType",
                  "dominantType", 46);
  sf_mex_addfield(*c3_info, c3_emlrt_marshallOut(
    "[ILXE]$matlabroot$/toolbox/eml/lib/matlab/eml/eml_index_times.m"),
                  "resolved", "resolved", 46);
  sf_mex_addfield(*c3_info, c3_b_emlrt_marshallOut(1372586016U), "fileTimeLo",
                  "fileTimeLo", 46);
  sf_mex_addfield(*c3_info, c3_b_emlrt_marshallOut(0U), "fileTimeHi",
                  "fileTimeHi", 46);
  sf_mex_addfield(*c3_info, c3_b_emlrt_marshallOut(0U), "mFileTimeLo",
                  "mFileTimeLo", 46);
  sf_mex_addfield(*c3_info, c3_b_emlrt_marshallOut(0U), "mFileTimeHi",
                  "mFileTimeHi", 46);
  sf_mex_assign(&c3_rhs46, sf_mex_createcellmatrix(0, 1), false);
  sf_mex_assign(&c3_lhs46, sf_mex_createcellmatrix(0, 1), false);
  sf_mex_addfield(*c3_info, sf_mex_duplicatearraysafe(&c3_rhs46), "rhs", "rhs",
                  46);
  sf_mex_addfield(*c3_info, sf_mex_duplicatearraysafe(&c3_lhs46), "lhs", "lhs",
                  46);
  sf_mex_addfield(*c3_info, c3_emlrt_marshallOut(
    "[ILXE]$matlabroot$/toolbox/eml/lib/matlab/eml/eml_index_times.m"),
                  "context", "context", 47);
  sf_mex_addfield(*c3_info, c3_emlrt_marshallOut("coder.internal.indexTimes"),
                  "name", "name", 47);
  sf_mex_addfield(*c3_info, c3_emlrt_marshallOut("double"), "dominantType",
                  "dominantType", 47);
  sf_mex_addfield(*c3_info, c3_emlrt_marshallOut(
    "[ILXE]$matlabroot$/toolbox/shared/coder/coder/+coder/+internal/indexTimes.m"),
                  "resolved", "resolved", 47);
  sf_mex_addfield(*c3_info, c3_b_emlrt_marshallOut(1372586760U), "fileTimeLo",
                  "fileTimeLo", 47);
  sf_mex_addfield(*c3_info, c3_b_emlrt_marshallOut(0U), "fileTimeHi",
                  "fileTimeHi", 47);
  sf_mex_addfield(*c3_info, c3_b_emlrt_marshallOut(0U), "mFileTimeLo",
                  "mFileTimeLo", 47);
  sf_mex_addfield(*c3_info, c3_b_emlrt_marshallOut(0U), "mFileTimeHi",
                  "mFileTimeHi", 47);
  sf_mex_assign(&c3_rhs47, sf_mex_createcellmatrix(0, 1), false);
  sf_mex_assign(&c3_lhs47, sf_mex_createcellmatrix(0, 1), false);
  sf_mex_addfield(*c3_info, sf_mex_duplicatearraysafe(&c3_rhs47), "rhs", "rhs",
                  47);
  sf_mex_addfield(*c3_info, sf_mex_duplicatearraysafe(&c3_lhs47), "lhs", "lhs",
                  47);
  sf_mex_addfield(*c3_info, c3_emlrt_marshallOut(
    "[ILXE]$matlabroot$/toolbox/eml/lib/matlab/eml/eml_sort_idx.m"), "context",
                  "context", 48);
  sf_mex_addfield(*c3_info, c3_emlrt_marshallOut("eml_index_minus"), "name",
                  "name", 48);
  sf_mex_addfield(*c3_info, c3_emlrt_marshallOut("coder.internal.indexInt"),
                  "dominantType", "dominantType", 48);
  sf_mex_addfield(*c3_info, c3_emlrt_marshallOut(
    "[ILXE]$matlabroot$/toolbox/eml/lib/matlab/eml/eml_index_minus.m"),
                  "resolved", "resolved", 48);
  sf_mex_addfield(*c3_info, c3_b_emlrt_marshallOut(1372586016U), "fileTimeLo",
                  "fileTimeLo", 48);
  sf_mex_addfield(*c3_info, c3_b_emlrt_marshallOut(0U), "fileTimeHi",
                  "fileTimeHi", 48);
  sf_mex_addfield(*c3_info, c3_b_emlrt_marshallOut(0U), "mFileTimeLo",
                  "mFileTimeLo", 48);
  sf_mex_addfield(*c3_info, c3_b_emlrt_marshallOut(0U), "mFileTimeHi",
                  "mFileTimeHi", 48);
  sf_mex_assign(&c3_rhs48, sf_mex_createcellmatrix(0, 1), false);
  sf_mex_assign(&c3_lhs48, sf_mex_createcellmatrix(0, 1), false);
  sf_mex_addfield(*c3_info, sf_mex_duplicatearraysafe(&c3_rhs48), "rhs", "rhs",
                  48);
  sf_mex_addfield(*c3_info, sf_mex_duplicatearraysafe(&c3_lhs48), "lhs", "lhs",
                  48);
  sf_mex_addfield(*c3_info, c3_emlrt_marshallOut(
    "[ILXE]$matlabroot$/toolbox/eml/lib/matlab/eml/eml_index_minus.m"),
                  "context", "context", 49);
  sf_mex_addfield(*c3_info, c3_emlrt_marshallOut("coder.internal.indexMinus"),
                  "name", "name", 49);
  sf_mex_addfield(*c3_info, c3_emlrt_marshallOut("coder.internal.indexInt"),
                  "dominantType", "dominantType", 49);
  sf_mex_addfield(*c3_info, c3_emlrt_marshallOut(
    "[ILXE]$matlabroot$/toolbox/shared/coder/coder/+coder/+internal/indexMinus.m"),
                  "resolved", "resolved", 49);
  sf_mex_addfield(*c3_info, c3_b_emlrt_marshallOut(1372586760U), "fileTimeLo",
                  "fileTimeLo", 49);
  sf_mex_addfield(*c3_info, c3_b_emlrt_marshallOut(0U), "fileTimeHi",
                  "fileTimeHi", 49);
  sf_mex_addfield(*c3_info, c3_b_emlrt_marshallOut(0U), "mFileTimeLo",
                  "mFileTimeLo", 49);
  sf_mex_addfield(*c3_info, c3_b_emlrt_marshallOut(0U), "mFileTimeHi",
                  "mFileTimeHi", 49);
  sf_mex_assign(&c3_rhs49, sf_mex_createcellmatrix(0, 1), false);
  sf_mex_assign(&c3_lhs49, sf_mex_createcellmatrix(0, 1), false);
  sf_mex_addfield(*c3_info, sf_mex_duplicatearraysafe(&c3_rhs49), "rhs", "rhs",
                  49);
  sf_mex_addfield(*c3_info, sf_mex_duplicatearraysafe(&c3_lhs49), "lhs", "lhs",
                  49);
  sf_mex_addfield(*c3_info, c3_emlrt_marshallOut(""), "context", "context", 50);
  sf_mex_addfield(*c3_info, c3_emlrt_marshallOut("length"), "name", "name", 50);
  sf_mex_addfield(*c3_info, c3_emlrt_marshallOut("double"), "dominantType",
                  "dominantType", 50);
  sf_mex_addfield(*c3_info, c3_emlrt_marshallOut(
    "[ILXE]$matlabroot$/toolbox/eml/lib/matlab/elmat/length.m"), "resolved",
                  "resolved", 50);
  sf_mex_addfield(*c3_info, c3_b_emlrt_marshallOut(1303149806U), "fileTimeLo",
                  "fileTimeLo", 50);
  sf_mex_addfield(*c3_info, c3_b_emlrt_marshallOut(0U), "fileTimeHi",
                  "fileTimeHi", 50);
  sf_mex_addfield(*c3_info, c3_b_emlrt_marshallOut(0U), "mFileTimeLo",
                  "mFileTimeLo", 50);
  sf_mex_addfield(*c3_info, c3_b_emlrt_marshallOut(0U), "mFileTimeHi",
                  "mFileTimeHi", 50);
  sf_mex_assign(&c3_rhs50, sf_mex_createcellmatrix(0, 1), false);
  sf_mex_assign(&c3_lhs50, sf_mex_createcellmatrix(0, 1), false);
  sf_mex_addfield(*c3_info, sf_mex_duplicatearraysafe(&c3_rhs50), "rhs", "rhs",
                  50);
  sf_mex_addfield(*c3_info, sf_mex_duplicatearraysafe(&c3_lhs50), "lhs", "lhs",
                  50);
  sf_mex_addfield(*c3_info, c3_emlrt_marshallOut(""), "context", "context", 51);
  sf_mex_addfield(*c3_info, c3_emlrt_marshallOut("mrdivide"), "name", "name", 51);
  sf_mex_addfield(*c3_info, c3_emlrt_marshallOut("double"), "dominantType",
                  "dominantType", 51);
  sf_mex_addfield(*c3_info, c3_emlrt_marshallOut(
    "[ILXE]$matlabroot$/toolbox/eml/lib/matlab/ops/mrdivide.p"), "resolved",
                  "resolved", 51);
  sf_mex_addfield(*c3_info, c3_b_emlrt_marshallOut(1410811248U), "fileTimeLo",
                  "fileTimeLo", 51);
  sf_mex_addfield(*c3_info, c3_b_emlrt_marshallOut(0U), "fileTimeHi",
                  "fileTimeHi", 51);
  sf_mex_addfield(*c3_info, c3_b_emlrt_marshallOut(1370013486U), "mFileTimeLo",
                  "mFileTimeLo", 51);
  sf_mex_addfield(*c3_info, c3_b_emlrt_marshallOut(0U), "mFileTimeHi",
                  "mFileTimeHi", 51);
  sf_mex_assign(&c3_rhs51, sf_mex_createcellmatrix(0, 1), false);
  sf_mex_assign(&c3_lhs51, sf_mex_createcellmatrix(0, 1), false);
  sf_mex_addfield(*c3_info, sf_mex_duplicatearraysafe(&c3_rhs51), "rhs", "rhs",
                  51);
  sf_mex_addfield(*c3_info, sf_mex_duplicatearraysafe(&c3_lhs51), "lhs", "lhs",
                  51);
  sf_mex_addfield(*c3_info, c3_emlrt_marshallOut(
    "[ILXE]$matlabroot$/toolbox/eml/lib/matlab/ops/mrdivide.p"), "context",
                  "context", 52);
  sf_mex_addfield(*c3_info, c3_emlrt_marshallOut("coder.internal.assert"),
                  "name", "name", 52);
  sf_mex_addfield(*c3_info, c3_emlrt_marshallOut("char"), "dominantType",
                  "dominantType", 52);
  sf_mex_addfield(*c3_info, c3_emlrt_marshallOut(
    "[ILXE]$matlabroot$/toolbox/shared/coder/coder/+coder/+internal/assert.m"),
                  "resolved", "resolved", 52);
  sf_mex_addfield(*c3_info, c3_b_emlrt_marshallOut(1389721374U), "fileTimeLo",
                  "fileTimeLo", 52);
  sf_mex_addfield(*c3_info, c3_b_emlrt_marshallOut(0U), "fileTimeHi",
                  "fileTimeHi", 52);
  sf_mex_addfield(*c3_info, c3_b_emlrt_marshallOut(0U), "mFileTimeLo",
                  "mFileTimeLo", 52);
  sf_mex_addfield(*c3_info, c3_b_emlrt_marshallOut(0U), "mFileTimeHi",
                  "mFileTimeHi", 52);
  sf_mex_assign(&c3_rhs52, sf_mex_createcellmatrix(0, 1), false);
  sf_mex_assign(&c3_lhs52, sf_mex_createcellmatrix(0, 1), false);
  sf_mex_addfield(*c3_info, sf_mex_duplicatearraysafe(&c3_rhs52), "rhs", "rhs",
                  52);
  sf_mex_addfield(*c3_info, sf_mex_duplicatearraysafe(&c3_lhs52), "lhs", "lhs",
                  52);
  sf_mex_addfield(*c3_info, c3_emlrt_marshallOut(
    "[ILXE]$matlabroot$/toolbox/eml/lib/matlab/ops/mrdivide.p"), "context",
                  "context", 53);
  sf_mex_addfield(*c3_info, c3_emlrt_marshallOut("rdivide"), "name", "name", 53);
  sf_mex_addfield(*c3_info, c3_emlrt_marshallOut("double"), "dominantType",
                  "dominantType", 53);
  sf_mex_addfield(*c3_info, c3_emlrt_marshallOut(
    "[ILXE]$matlabroot$/toolbox/eml/lib/matlab/ops/rdivide.m"), "resolved",
                  "resolved", 53);
  sf_mex_addfield(*c3_info, c3_b_emlrt_marshallOut(1363717480U), "fileTimeLo",
                  "fileTimeLo", 53);
  sf_mex_addfield(*c3_info, c3_b_emlrt_marshallOut(0U), "fileTimeHi",
                  "fileTimeHi", 53);
  sf_mex_addfield(*c3_info, c3_b_emlrt_marshallOut(0U), "mFileTimeLo",
                  "mFileTimeLo", 53);
  sf_mex_addfield(*c3_info, c3_b_emlrt_marshallOut(0U), "mFileTimeHi",
                  "mFileTimeHi", 53);
  sf_mex_assign(&c3_rhs53, sf_mex_createcellmatrix(0, 1), false);
  sf_mex_assign(&c3_lhs53, sf_mex_createcellmatrix(0, 1), false);
  sf_mex_addfield(*c3_info, sf_mex_duplicatearraysafe(&c3_rhs53), "rhs", "rhs",
                  53);
  sf_mex_addfield(*c3_info, sf_mex_duplicatearraysafe(&c3_lhs53), "lhs", "lhs",
                  53);
  sf_mex_addfield(*c3_info, c3_emlrt_marshallOut(
    "[ILXE]$matlabroot$/toolbox/eml/lib/matlab/ops/rdivide.m"), "context",
                  "context", 54);
  sf_mex_addfield(*c3_info, c3_emlrt_marshallOut(
    "coder.internal.isBuiltInNumeric"), "name", "name", 54);
  sf_mex_addfield(*c3_info, c3_emlrt_marshallOut("double"), "dominantType",
                  "dominantType", 54);
  sf_mex_addfield(*c3_info, c3_emlrt_marshallOut(
    "[ILXE]$matlabroot$/toolbox/shared/coder/coder/+coder/+internal/isBuiltInNumeric.m"),
                  "resolved", "resolved", 54);
  sf_mex_addfield(*c3_info, c3_b_emlrt_marshallOut(1395935456U), "fileTimeLo",
                  "fileTimeLo", 54);
  sf_mex_addfield(*c3_info, c3_b_emlrt_marshallOut(0U), "fileTimeHi",
                  "fileTimeHi", 54);
  sf_mex_addfield(*c3_info, c3_b_emlrt_marshallOut(0U), "mFileTimeLo",
                  "mFileTimeLo", 54);
  sf_mex_addfield(*c3_info, c3_b_emlrt_marshallOut(0U), "mFileTimeHi",
                  "mFileTimeHi", 54);
  sf_mex_assign(&c3_rhs54, sf_mex_createcellmatrix(0, 1), false);
  sf_mex_assign(&c3_lhs54, sf_mex_createcellmatrix(0, 1), false);
  sf_mex_addfield(*c3_info, sf_mex_duplicatearraysafe(&c3_rhs54), "rhs", "rhs",
                  54);
  sf_mex_addfield(*c3_info, sf_mex_duplicatearraysafe(&c3_lhs54), "lhs", "lhs",
                  54);
  sf_mex_addfield(*c3_info, c3_emlrt_marshallOut(
    "[ILXE]$matlabroot$/toolbox/eml/lib/matlab/ops/rdivide.m"), "context",
                  "context", 55);
  sf_mex_addfield(*c3_info, c3_emlrt_marshallOut("eml_scalexp_compatible"),
                  "name", "name", 55);
  sf_mex_addfield(*c3_info, c3_emlrt_marshallOut("double"), "dominantType",
                  "dominantType", 55);
  sf_mex_addfield(*c3_info, c3_emlrt_marshallOut(
    "[ILXE]$matlabroot$/toolbox/eml/lib/matlab/eml/eml_scalexp_compatible.m"),
                  "resolved", "resolved", 55);
  sf_mex_addfield(*c3_info, c3_b_emlrt_marshallOut(1286822396U), "fileTimeLo",
                  "fileTimeLo", 55);
  sf_mex_addfield(*c3_info, c3_b_emlrt_marshallOut(0U), "fileTimeHi",
                  "fileTimeHi", 55);
  sf_mex_addfield(*c3_info, c3_b_emlrt_marshallOut(0U), "mFileTimeLo",
                  "mFileTimeLo", 55);
  sf_mex_addfield(*c3_info, c3_b_emlrt_marshallOut(0U), "mFileTimeHi",
                  "mFileTimeHi", 55);
  sf_mex_assign(&c3_rhs55, sf_mex_createcellmatrix(0, 1), false);
  sf_mex_assign(&c3_lhs55, sf_mex_createcellmatrix(0, 1), false);
  sf_mex_addfield(*c3_info, sf_mex_duplicatearraysafe(&c3_rhs55), "rhs", "rhs",
                  55);
  sf_mex_addfield(*c3_info, sf_mex_duplicatearraysafe(&c3_lhs55), "lhs", "lhs",
                  55);
  sf_mex_addfield(*c3_info, c3_emlrt_marshallOut(
    "[ILXE]$matlabroot$/toolbox/eml/lib/matlab/ops/rdivide.m"), "context",
                  "context", 56);
  sf_mex_addfield(*c3_info, c3_emlrt_marshallOut("eml_div"), "name", "name", 56);
  sf_mex_addfield(*c3_info, c3_emlrt_marshallOut("double"), "dominantType",
                  "dominantType", 56);
  sf_mex_addfield(*c3_info, c3_emlrt_marshallOut(
    "[ILXE]$matlabroot$/toolbox/eml/lib/matlab/eml/eml_div.m"), "resolved",
                  "resolved", 56);
  sf_mex_addfield(*c3_info, c3_b_emlrt_marshallOut(1386427552U), "fileTimeLo",
                  "fileTimeLo", 56);
  sf_mex_addfield(*c3_info, c3_b_emlrt_marshallOut(0U), "fileTimeHi",
                  "fileTimeHi", 56);
  sf_mex_addfield(*c3_info, c3_b_emlrt_marshallOut(0U), "mFileTimeLo",
                  "mFileTimeLo", 56);
  sf_mex_addfield(*c3_info, c3_b_emlrt_marshallOut(0U), "mFileTimeHi",
                  "mFileTimeHi", 56);
  sf_mex_assign(&c3_rhs56, sf_mex_createcellmatrix(0, 1), false);
  sf_mex_assign(&c3_lhs56, sf_mex_createcellmatrix(0, 1), false);
  sf_mex_addfield(*c3_info, sf_mex_duplicatearraysafe(&c3_rhs56), "rhs", "rhs",
                  56);
  sf_mex_addfield(*c3_info, sf_mex_duplicatearraysafe(&c3_lhs56), "lhs", "lhs",
                  56);
  sf_mex_addfield(*c3_info, c3_emlrt_marshallOut(
    "[ILXE]$matlabroot$/toolbox/eml/lib/matlab/eml/eml_div.m"), "context",
                  "context", 57);
  sf_mex_addfield(*c3_info, c3_emlrt_marshallOut("coder.internal.div"), "name",
                  "name", 57);
  sf_mex_addfield(*c3_info, c3_emlrt_marshallOut("double"), "dominantType",
                  "dominantType", 57);
  sf_mex_addfield(*c3_info, c3_emlrt_marshallOut(
    "[ILXE]$matlabroot$/toolbox/coder/coder/+coder/+internal/div.p"), "resolved",
                  "resolved", 57);
  sf_mex_addfield(*c3_info, c3_b_emlrt_marshallOut(1410811370U), "fileTimeLo",
                  "fileTimeLo", 57);
  sf_mex_addfield(*c3_info, c3_b_emlrt_marshallOut(0U), "fileTimeHi",
                  "fileTimeHi", 57);
  sf_mex_addfield(*c3_info, c3_b_emlrt_marshallOut(0U), "mFileTimeLo",
                  "mFileTimeLo", 57);
  sf_mex_addfield(*c3_info, c3_b_emlrt_marshallOut(0U), "mFileTimeHi",
                  "mFileTimeHi", 57);
  sf_mex_assign(&c3_rhs57, sf_mex_createcellmatrix(0, 1), false);
  sf_mex_assign(&c3_lhs57, sf_mex_createcellmatrix(0, 1), false);
  sf_mex_addfield(*c3_info, sf_mex_duplicatearraysafe(&c3_rhs57), "rhs", "rhs",
                  57);
  sf_mex_addfield(*c3_info, sf_mex_duplicatearraysafe(&c3_lhs57), "lhs", "lhs",
                  57);
  sf_mex_addfield(*c3_info, c3_emlrt_marshallOut(""), "context", "context", 58);
  sf_mex_addfield(*c3_info, c3_emlrt_marshallOut("sign"), "name", "name", 58);
  sf_mex_addfield(*c3_info, c3_emlrt_marshallOut("single"), "dominantType",
                  "dominantType", 58);
  sf_mex_addfield(*c3_info, c3_emlrt_marshallOut(
    "[ILXE]$matlabroot$/toolbox/eml/lib/matlab/elfun/sign.m"), "resolved",
                  "resolved", 58);
  sf_mex_addfield(*c3_info, c3_b_emlrt_marshallOut(1363717456U), "fileTimeLo",
                  "fileTimeLo", 58);
  sf_mex_addfield(*c3_info, c3_b_emlrt_marshallOut(0U), "fileTimeHi",
                  "fileTimeHi", 58);
  sf_mex_addfield(*c3_info, c3_b_emlrt_marshallOut(0U), "mFileTimeLo",
                  "mFileTimeLo", 58);
  sf_mex_addfield(*c3_info, c3_b_emlrt_marshallOut(0U), "mFileTimeHi",
                  "mFileTimeHi", 58);
  sf_mex_assign(&c3_rhs58, sf_mex_createcellmatrix(0, 1), false);
  sf_mex_assign(&c3_lhs58, sf_mex_createcellmatrix(0, 1), false);
  sf_mex_addfield(*c3_info, sf_mex_duplicatearraysafe(&c3_rhs58), "rhs", "rhs",
                  58);
  sf_mex_addfield(*c3_info, sf_mex_duplicatearraysafe(&c3_lhs58), "lhs", "lhs",
                  58);
  sf_mex_addfield(*c3_info, c3_emlrt_marshallOut(
    "[ILXE]$matlabroot$/toolbox/eml/lib/matlab/elfun/sign.m"), "context",
                  "context", 59);
  sf_mex_addfield(*c3_info, c3_emlrt_marshallOut(
    "coder.internal.isBuiltInNumeric"), "name", "name", 59);
  sf_mex_addfield(*c3_info, c3_emlrt_marshallOut("single"), "dominantType",
                  "dominantType", 59);
  sf_mex_addfield(*c3_info, c3_emlrt_marshallOut(
    "[ILXE]$matlabroot$/toolbox/shared/coder/coder/+coder/+internal/isBuiltInNumeric.m"),
                  "resolved", "resolved", 59);
  sf_mex_addfield(*c3_info, c3_b_emlrt_marshallOut(1395935456U), "fileTimeLo",
                  "fileTimeLo", 59);
  sf_mex_addfield(*c3_info, c3_b_emlrt_marshallOut(0U), "fileTimeHi",
                  "fileTimeHi", 59);
  sf_mex_addfield(*c3_info, c3_b_emlrt_marshallOut(0U), "mFileTimeLo",
                  "mFileTimeLo", 59);
  sf_mex_addfield(*c3_info, c3_b_emlrt_marshallOut(0U), "mFileTimeHi",
                  "mFileTimeHi", 59);
  sf_mex_assign(&c3_rhs59, sf_mex_createcellmatrix(0, 1), false);
  sf_mex_assign(&c3_lhs59, sf_mex_createcellmatrix(0, 1), false);
  sf_mex_addfield(*c3_info, sf_mex_duplicatearraysafe(&c3_rhs59), "rhs", "rhs",
                  59);
  sf_mex_addfield(*c3_info, sf_mex_duplicatearraysafe(&c3_lhs59), "lhs", "lhs",
                  59);
  sf_mex_addfield(*c3_info, c3_emlrt_marshallOut(
    "[ILXE]$matlabroot$/toolbox/eml/lib/matlab/elfun/sign.m"), "context",
                  "context", 60);
  sf_mex_addfield(*c3_info, c3_emlrt_marshallOut("eml_scalar_sign"), "name",
                  "name", 60);
  sf_mex_addfield(*c3_info, c3_emlrt_marshallOut("single"), "dominantType",
                  "dominantType", 60);
  sf_mex_addfield(*c3_info, c3_emlrt_marshallOut(
    "[ILXE]$matlabroot$/toolbox/eml/lib/matlab/elfun/eml_scalar_sign.m"),
                  "resolved", "resolved", 60);
  sf_mex_addfield(*c3_info, c3_b_emlrt_marshallOut(1356545094U), "fileTimeLo",
                  "fileTimeLo", 60);
  sf_mex_addfield(*c3_info, c3_b_emlrt_marshallOut(0U), "fileTimeHi",
                  "fileTimeHi", 60);
  sf_mex_addfield(*c3_info, c3_b_emlrt_marshallOut(0U), "mFileTimeLo",
                  "mFileTimeLo", 60);
  sf_mex_addfield(*c3_info, c3_b_emlrt_marshallOut(0U), "mFileTimeHi",
                  "mFileTimeHi", 60);
  sf_mex_assign(&c3_rhs60, sf_mex_createcellmatrix(0, 1), false);
  sf_mex_assign(&c3_lhs60, sf_mex_createcellmatrix(0, 1), false);
  sf_mex_addfield(*c3_info, sf_mex_duplicatearraysafe(&c3_rhs60), "rhs", "rhs",
                  60);
  sf_mex_addfield(*c3_info, sf_mex_duplicatearraysafe(&c3_lhs60), "lhs", "lhs",
                  60);
  sf_mex_addfield(*c3_info, c3_emlrt_marshallOut(""), "context", "context", 61);
  sf_mex_addfield(*c3_info, c3_emlrt_marshallOut("abs"), "name", "name", 61);
  sf_mex_addfield(*c3_info, c3_emlrt_marshallOut("single"), "dominantType",
                  "dominantType", 61);
  sf_mex_addfield(*c3_info, c3_emlrt_marshallOut(
    "[ILXE]$matlabroot$/toolbox/eml/lib/matlab/elfun/abs.m"), "resolved",
                  "resolved", 61);
  sf_mex_addfield(*c3_info, c3_b_emlrt_marshallOut(1363717452U), "fileTimeLo",
                  "fileTimeLo", 61);
  sf_mex_addfield(*c3_info, c3_b_emlrt_marshallOut(0U), "fileTimeHi",
                  "fileTimeHi", 61);
  sf_mex_addfield(*c3_info, c3_b_emlrt_marshallOut(0U), "mFileTimeLo",
                  "mFileTimeLo", 61);
  sf_mex_addfield(*c3_info, c3_b_emlrt_marshallOut(0U), "mFileTimeHi",
                  "mFileTimeHi", 61);
  sf_mex_assign(&c3_rhs61, sf_mex_createcellmatrix(0, 1), false);
  sf_mex_assign(&c3_lhs61, sf_mex_createcellmatrix(0, 1), false);
  sf_mex_addfield(*c3_info, sf_mex_duplicatearraysafe(&c3_rhs61), "rhs", "rhs",
                  61);
  sf_mex_addfield(*c3_info, sf_mex_duplicatearraysafe(&c3_lhs61), "lhs", "lhs",
                  61);
  sf_mex_addfield(*c3_info, c3_emlrt_marshallOut(
    "[ILXE]$matlabroot$/toolbox/eml/lib/matlab/elfun/abs.m"), "context",
                  "context", 62);
  sf_mex_addfield(*c3_info, c3_emlrt_marshallOut(
    "coder.internal.isBuiltInNumeric"), "name", "name", 62);
  sf_mex_addfield(*c3_info, c3_emlrt_marshallOut("single"), "dominantType",
                  "dominantType", 62);
  sf_mex_addfield(*c3_info, c3_emlrt_marshallOut(
    "[ILXE]$matlabroot$/toolbox/shared/coder/coder/+coder/+internal/isBuiltInNumeric.m"),
                  "resolved", "resolved", 62);
  sf_mex_addfield(*c3_info, c3_b_emlrt_marshallOut(1395935456U), "fileTimeLo",
                  "fileTimeLo", 62);
  sf_mex_addfield(*c3_info, c3_b_emlrt_marshallOut(0U), "fileTimeHi",
                  "fileTimeHi", 62);
  sf_mex_addfield(*c3_info, c3_b_emlrt_marshallOut(0U), "mFileTimeLo",
                  "mFileTimeLo", 62);
  sf_mex_addfield(*c3_info, c3_b_emlrt_marshallOut(0U), "mFileTimeHi",
                  "mFileTimeHi", 62);
  sf_mex_assign(&c3_rhs62, sf_mex_createcellmatrix(0, 1), false);
  sf_mex_assign(&c3_lhs62, sf_mex_createcellmatrix(0, 1), false);
  sf_mex_addfield(*c3_info, sf_mex_duplicatearraysafe(&c3_rhs62), "rhs", "rhs",
                  62);
  sf_mex_addfield(*c3_info, sf_mex_duplicatearraysafe(&c3_lhs62), "lhs", "lhs",
                  62);
  sf_mex_addfield(*c3_info, c3_emlrt_marshallOut(
    "[ILXE]$matlabroot$/toolbox/eml/lib/matlab/elfun/abs.m"), "context",
                  "context", 63);
  sf_mex_addfield(*c3_info, c3_emlrt_marshallOut("eml_scalar_abs"), "name",
                  "name", 63);
  sf_mex_addfield(*c3_info, c3_emlrt_marshallOut("single"), "dominantType",
                  "dominantType", 63);
  sf_mex_addfield(*c3_info, c3_emlrt_marshallOut(
    "[ILXE]$matlabroot$/toolbox/eml/lib/matlab/elfun/eml_scalar_abs.m"),
                  "resolved", "resolved", 63);
  sf_mex_addfield(*c3_info, c3_b_emlrt_marshallOut(1286822312U), "fileTimeLo",
                  "fileTimeLo", 63);
  sf_mex_addfield(*c3_info, c3_b_emlrt_marshallOut(0U), "fileTimeHi",
                  "fileTimeHi", 63);
  sf_mex_addfield(*c3_info, c3_b_emlrt_marshallOut(0U), "mFileTimeLo",
                  "mFileTimeLo", 63);
  sf_mex_addfield(*c3_info, c3_b_emlrt_marshallOut(0U), "mFileTimeHi",
                  "mFileTimeHi", 63);
  sf_mex_assign(&c3_rhs63, sf_mex_createcellmatrix(0, 1), false);
  sf_mex_assign(&c3_lhs63, sf_mex_createcellmatrix(0, 1), false);
  sf_mex_addfield(*c3_info, sf_mex_duplicatearraysafe(&c3_rhs63), "rhs", "rhs",
                  63);
  sf_mex_addfield(*c3_info, sf_mex_duplicatearraysafe(&c3_lhs63), "lhs", "lhs",
                  63);
  sf_mex_destroy(&c3_rhs0);
  sf_mex_destroy(&c3_lhs0);
  sf_mex_destroy(&c3_rhs1);
  sf_mex_destroy(&c3_lhs1);
  sf_mex_destroy(&c3_rhs2);
  sf_mex_destroy(&c3_lhs2);
  sf_mex_destroy(&c3_rhs3);
  sf_mex_destroy(&c3_lhs3);
  sf_mex_destroy(&c3_rhs4);
  sf_mex_destroy(&c3_lhs4);
  sf_mex_destroy(&c3_rhs5);
  sf_mex_destroy(&c3_lhs5);
  sf_mex_destroy(&c3_rhs6);
  sf_mex_destroy(&c3_lhs6);
  sf_mex_destroy(&c3_rhs7);
  sf_mex_destroy(&c3_lhs7);
  sf_mex_destroy(&c3_rhs8);
  sf_mex_destroy(&c3_lhs8);
  sf_mex_destroy(&c3_rhs9);
  sf_mex_destroy(&c3_lhs9);
  sf_mex_destroy(&c3_rhs10);
  sf_mex_destroy(&c3_lhs10);
  sf_mex_destroy(&c3_rhs11);
  sf_mex_destroy(&c3_lhs11);
  sf_mex_destroy(&c3_rhs12);
  sf_mex_destroy(&c3_lhs12);
  sf_mex_destroy(&c3_rhs13);
  sf_mex_destroy(&c3_lhs13);
  sf_mex_destroy(&c3_rhs14);
  sf_mex_destroy(&c3_lhs14);
  sf_mex_destroy(&c3_rhs15);
  sf_mex_destroy(&c3_lhs15);
  sf_mex_destroy(&c3_rhs16);
  sf_mex_destroy(&c3_lhs16);
  sf_mex_destroy(&c3_rhs17);
  sf_mex_destroy(&c3_lhs17);
  sf_mex_destroy(&c3_rhs18);
  sf_mex_destroy(&c3_lhs18);
  sf_mex_destroy(&c3_rhs19);
  sf_mex_destroy(&c3_lhs19);
  sf_mex_destroy(&c3_rhs20);
  sf_mex_destroy(&c3_lhs20);
  sf_mex_destroy(&c3_rhs21);
  sf_mex_destroy(&c3_lhs21);
  sf_mex_destroy(&c3_rhs22);
  sf_mex_destroy(&c3_lhs22);
  sf_mex_destroy(&c3_rhs23);
  sf_mex_destroy(&c3_lhs23);
  sf_mex_destroy(&c3_rhs24);
  sf_mex_destroy(&c3_lhs24);
  sf_mex_destroy(&c3_rhs25);
  sf_mex_destroy(&c3_lhs25);
  sf_mex_destroy(&c3_rhs26);
  sf_mex_destroy(&c3_lhs26);
  sf_mex_destroy(&c3_rhs27);
  sf_mex_destroy(&c3_lhs27);
  sf_mex_destroy(&c3_rhs28);
  sf_mex_destroy(&c3_lhs28);
  sf_mex_destroy(&c3_rhs29);
  sf_mex_destroy(&c3_lhs29);
  sf_mex_destroy(&c3_rhs30);
  sf_mex_destroy(&c3_lhs30);
  sf_mex_destroy(&c3_rhs31);
  sf_mex_destroy(&c3_lhs31);
  sf_mex_destroy(&c3_rhs32);
  sf_mex_destroy(&c3_lhs32);
  sf_mex_destroy(&c3_rhs33);
  sf_mex_destroy(&c3_lhs33);
  sf_mex_destroy(&c3_rhs34);
  sf_mex_destroy(&c3_lhs34);
  sf_mex_destroy(&c3_rhs35);
  sf_mex_destroy(&c3_lhs35);
  sf_mex_destroy(&c3_rhs36);
  sf_mex_destroy(&c3_lhs36);
  sf_mex_destroy(&c3_rhs37);
  sf_mex_destroy(&c3_lhs37);
  sf_mex_destroy(&c3_rhs38);
  sf_mex_destroy(&c3_lhs38);
  sf_mex_destroy(&c3_rhs39);
  sf_mex_destroy(&c3_lhs39);
  sf_mex_destroy(&c3_rhs40);
  sf_mex_destroy(&c3_lhs40);
  sf_mex_destroy(&c3_rhs41);
  sf_mex_destroy(&c3_lhs41);
  sf_mex_destroy(&c3_rhs42);
  sf_mex_destroy(&c3_lhs42);
  sf_mex_destroy(&c3_rhs43);
  sf_mex_destroy(&c3_lhs43);
  sf_mex_destroy(&c3_rhs44);
  sf_mex_destroy(&c3_lhs44);
  sf_mex_destroy(&c3_rhs45);
  sf_mex_destroy(&c3_lhs45);
  sf_mex_destroy(&c3_rhs46);
  sf_mex_destroy(&c3_lhs46);
  sf_mex_destroy(&c3_rhs47);
  sf_mex_destroy(&c3_lhs47);
  sf_mex_destroy(&c3_rhs48);
  sf_mex_destroy(&c3_lhs48);
  sf_mex_destroy(&c3_rhs49);
  sf_mex_destroy(&c3_lhs49);
  sf_mex_destroy(&c3_rhs50);
  sf_mex_destroy(&c3_lhs50);
  sf_mex_destroy(&c3_rhs51);
  sf_mex_destroy(&c3_lhs51);
  sf_mex_destroy(&c3_rhs52);
  sf_mex_destroy(&c3_lhs52);
  sf_mex_destroy(&c3_rhs53);
  sf_mex_destroy(&c3_lhs53);
  sf_mex_destroy(&c3_rhs54);
  sf_mex_destroy(&c3_lhs54);
  sf_mex_destroy(&c3_rhs55);
  sf_mex_destroy(&c3_lhs55);
  sf_mex_destroy(&c3_rhs56);
  sf_mex_destroy(&c3_lhs56);
  sf_mex_destroy(&c3_rhs57);
  sf_mex_destroy(&c3_lhs57);
  sf_mex_destroy(&c3_rhs58);
  sf_mex_destroy(&c3_lhs58);
  sf_mex_destroy(&c3_rhs59);
  sf_mex_destroy(&c3_lhs59);
  sf_mex_destroy(&c3_rhs60);
  sf_mex_destroy(&c3_lhs60);
  sf_mex_destroy(&c3_rhs61);
  sf_mex_destroy(&c3_lhs61);
  sf_mex_destroy(&c3_rhs62);
  sf_mex_destroy(&c3_lhs62);
  sf_mex_destroy(&c3_rhs63);
  sf_mex_destroy(&c3_lhs63);
}

static const mxArray *c3_emlrt_marshallOut(const char * c3_u)
{
  const mxArray *c3_y = NULL;
  c3_y = NULL;
  sf_mex_assign(&c3_y, sf_mex_create("y", c3_u, 15, 0U, 0U, 0U, 2, 1, strlen
    (c3_u)), false);
  return c3_y;
}

static const mxArray *c3_b_emlrt_marshallOut(const uint32_T c3_u)
{
  const mxArray *c3_y = NULL;
  c3_y = NULL;
  sf_mex_assign(&c3_y, sf_mex_create("y", &c3_u, 7, 0U, 0U, 0U, 0), false);
  return c3_y;
}

static void c3_b_info_helper(const mxArray **c3_info)
{
  const mxArray *c3_rhs64 = NULL;
  const mxArray *c3_lhs64 = NULL;
  const mxArray *c3_rhs65 = NULL;
  const mxArray *c3_lhs65 = NULL;
  const mxArray *c3_rhs66 = NULL;
  const mxArray *c3_lhs66 = NULL;
  const mxArray *c3_rhs67 = NULL;
  const mxArray *c3_lhs67 = NULL;
  const mxArray *c3_rhs68 = NULL;
  const mxArray *c3_lhs68 = NULL;
  const mxArray *c3_rhs69 = NULL;
  const mxArray *c3_lhs69 = NULL;
  const mxArray *c3_rhs70 = NULL;
  const mxArray *c3_lhs70 = NULL;
  const mxArray *c3_rhs71 = NULL;
  const mxArray *c3_lhs71 = NULL;
  const mxArray *c3_rhs72 = NULL;
  const mxArray *c3_lhs72 = NULL;
  const mxArray *c3_rhs73 = NULL;
  const mxArray *c3_lhs73 = NULL;
  const mxArray *c3_rhs74 = NULL;
  const mxArray *c3_lhs74 = NULL;
  const mxArray *c3_rhs75 = NULL;
  const mxArray *c3_lhs75 = NULL;
  const mxArray *c3_rhs76 = NULL;
  const mxArray *c3_lhs76 = NULL;
  const mxArray *c3_rhs77 = NULL;
  const mxArray *c3_lhs77 = NULL;
  const mxArray *c3_rhs78 = NULL;
  const mxArray *c3_lhs78 = NULL;
  const mxArray *c3_rhs79 = NULL;
  const mxArray *c3_lhs79 = NULL;
  const mxArray *c3_rhs80 = NULL;
  const mxArray *c3_lhs80 = NULL;
  const mxArray *c3_rhs81 = NULL;
  const mxArray *c3_lhs81 = NULL;
  const mxArray *c3_rhs82 = NULL;
  const mxArray *c3_lhs82 = NULL;
  const mxArray *c3_rhs83 = NULL;
  const mxArray *c3_lhs83 = NULL;
  const mxArray *c3_rhs84 = NULL;
  const mxArray *c3_lhs84 = NULL;
  const mxArray *c3_rhs85 = NULL;
  const mxArray *c3_lhs85 = NULL;
  const mxArray *c3_rhs86 = NULL;
  const mxArray *c3_lhs86 = NULL;
  const mxArray *c3_rhs87 = NULL;
  const mxArray *c3_lhs87 = NULL;
  const mxArray *c3_rhs88 = NULL;
  const mxArray *c3_lhs88 = NULL;
  const mxArray *c3_rhs89 = NULL;
  const mxArray *c3_lhs89 = NULL;
  const mxArray *c3_rhs90 = NULL;
  const mxArray *c3_lhs90 = NULL;
  const mxArray *c3_rhs91 = NULL;
  const mxArray *c3_lhs91 = NULL;
  const mxArray *c3_rhs92 = NULL;
  const mxArray *c3_lhs92 = NULL;
  const mxArray *c3_rhs93 = NULL;
  const mxArray *c3_lhs93 = NULL;
  const mxArray *c3_rhs94 = NULL;
  const mxArray *c3_lhs94 = NULL;
  const mxArray *c3_rhs95 = NULL;
  const mxArray *c3_lhs95 = NULL;
  const mxArray *c3_rhs96 = NULL;
  const mxArray *c3_lhs96 = NULL;
  const mxArray *c3_rhs97 = NULL;
  const mxArray *c3_lhs97 = NULL;
  const mxArray *c3_rhs98 = NULL;
  const mxArray *c3_lhs98 = NULL;
  const mxArray *c3_rhs99 = NULL;
  const mxArray *c3_lhs99 = NULL;
  const mxArray *c3_rhs100 = NULL;
  const mxArray *c3_lhs100 = NULL;
  const mxArray *c3_rhs101 = NULL;
  const mxArray *c3_lhs101 = NULL;
  const mxArray *c3_rhs102 = NULL;
  const mxArray *c3_lhs102 = NULL;
  const mxArray *c3_rhs103 = NULL;
  const mxArray *c3_lhs103 = NULL;
  const mxArray *c3_rhs104 = NULL;
  const mxArray *c3_lhs104 = NULL;
  const mxArray *c3_rhs105 = NULL;
  const mxArray *c3_lhs105 = NULL;
  const mxArray *c3_rhs106 = NULL;
  const mxArray *c3_lhs106 = NULL;
  const mxArray *c3_rhs107 = NULL;
  const mxArray *c3_lhs107 = NULL;
  const mxArray *c3_rhs108 = NULL;
  const mxArray *c3_lhs108 = NULL;
  const mxArray *c3_rhs109 = NULL;
  const mxArray *c3_lhs109 = NULL;
  const mxArray *c3_rhs110 = NULL;
  const mxArray *c3_lhs110 = NULL;
  const mxArray *c3_rhs111 = NULL;
  const mxArray *c3_lhs111 = NULL;
  const mxArray *c3_rhs112 = NULL;
  const mxArray *c3_lhs112 = NULL;
  const mxArray *c3_rhs113 = NULL;
  const mxArray *c3_lhs113 = NULL;
  const mxArray *c3_rhs114 = NULL;
  const mxArray *c3_lhs114 = NULL;
  const mxArray *c3_rhs115 = NULL;
  const mxArray *c3_lhs115 = NULL;
  const mxArray *c3_rhs116 = NULL;
  const mxArray *c3_lhs116 = NULL;
  const mxArray *c3_rhs117 = NULL;
  const mxArray *c3_lhs117 = NULL;
  const mxArray *c3_rhs118 = NULL;
  const mxArray *c3_lhs118 = NULL;
  const mxArray *c3_rhs119 = NULL;
  const mxArray *c3_lhs119 = NULL;
  const mxArray *c3_rhs120 = NULL;
  const mxArray *c3_lhs120 = NULL;
  const mxArray *c3_rhs121 = NULL;
  const mxArray *c3_lhs121 = NULL;
  const mxArray *c3_rhs122 = NULL;
  const mxArray *c3_lhs122 = NULL;
  sf_mex_addfield(*c3_info, c3_emlrt_marshallOut(""), "context", "context", 64);
  sf_mex_addfield(*c3_info, c3_emlrt_marshallOut("eml_mtimes_helper"), "name",
                  "name", 64);
  sf_mex_addfield(*c3_info, c3_emlrt_marshallOut(""), "dominantType",
                  "dominantType", 64);
  sf_mex_addfield(*c3_info, c3_emlrt_marshallOut(
    "[ILXE]$matlabroot$/toolbox/eml/lib/matlab/ops/eml_mtimes_helper.m"),
                  "resolved", "resolved", 64);
  sf_mex_addfield(*c3_info, c3_b_emlrt_marshallOut(1383880894U), "fileTimeLo",
                  "fileTimeLo", 64);
  sf_mex_addfield(*c3_info, c3_b_emlrt_marshallOut(0U), "fileTimeHi",
                  "fileTimeHi", 64);
  sf_mex_addfield(*c3_info, c3_b_emlrt_marshallOut(0U), "mFileTimeLo",
                  "mFileTimeLo", 64);
  sf_mex_addfield(*c3_info, c3_b_emlrt_marshallOut(0U), "mFileTimeHi",
                  "mFileTimeHi", 64);
  sf_mex_assign(&c3_rhs64, sf_mex_createcellmatrix(0, 1), false);
  sf_mex_assign(&c3_lhs64, sf_mex_createcellmatrix(0, 1), false);
  sf_mex_addfield(*c3_info, sf_mex_duplicatearraysafe(&c3_rhs64), "rhs", "rhs",
                  64);
  sf_mex_addfield(*c3_info, sf_mex_duplicatearraysafe(&c3_lhs64), "lhs", "lhs",
                  64);
  sf_mex_addfield(*c3_info, c3_emlrt_marshallOut(
    "[ILXE]$matlabroot$/toolbox/eml/lib/matlab/ops/eml_mtimes_helper.m!common_checks"),
                  "context", "context", 65);
  sf_mex_addfield(*c3_info, c3_emlrt_marshallOut(
    "coder.internal.isBuiltInNumeric"), "name", "name", 65);
  sf_mex_addfield(*c3_info, c3_emlrt_marshallOut("double"), "dominantType",
                  "dominantType", 65);
  sf_mex_addfield(*c3_info, c3_emlrt_marshallOut(
    "[ILXE]$matlabroot$/toolbox/shared/coder/coder/+coder/+internal/isBuiltInNumeric.m"),
                  "resolved", "resolved", 65);
  sf_mex_addfield(*c3_info, c3_b_emlrt_marshallOut(1395935456U), "fileTimeLo",
                  "fileTimeLo", 65);
  sf_mex_addfield(*c3_info, c3_b_emlrt_marshallOut(0U), "fileTimeHi",
                  "fileTimeHi", 65);
  sf_mex_addfield(*c3_info, c3_b_emlrt_marshallOut(0U), "mFileTimeLo",
                  "mFileTimeLo", 65);
  sf_mex_addfield(*c3_info, c3_b_emlrt_marshallOut(0U), "mFileTimeHi",
                  "mFileTimeHi", 65);
  sf_mex_assign(&c3_rhs65, sf_mex_createcellmatrix(0, 1), false);
  sf_mex_assign(&c3_lhs65, sf_mex_createcellmatrix(0, 1), false);
  sf_mex_addfield(*c3_info, sf_mex_duplicatearraysafe(&c3_rhs65), "rhs", "rhs",
                  65);
  sf_mex_addfield(*c3_info, sf_mex_duplicatearraysafe(&c3_lhs65), "lhs", "lhs",
                  65);
  sf_mex_addfield(*c3_info, c3_emlrt_marshallOut(
    "[ILXE]$matlabroot$/toolbox/eml/lib/matlab/ops/eml_mtimes_helper.m!common_checks"),
                  "context", "context", 66);
  sf_mex_addfield(*c3_info, c3_emlrt_marshallOut(
    "coder.internal.isBuiltInNumeric"), "name", "name", 66);
  sf_mex_addfield(*c3_info, c3_emlrt_marshallOut("single"), "dominantType",
                  "dominantType", 66);
  sf_mex_addfield(*c3_info, c3_emlrt_marshallOut(
    "[ILXE]$matlabroot$/toolbox/shared/coder/coder/+coder/+internal/isBuiltInNumeric.m"),
                  "resolved", "resolved", 66);
  sf_mex_addfield(*c3_info, c3_b_emlrt_marshallOut(1395935456U), "fileTimeLo",
                  "fileTimeLo", 66);
  sf_mex_addfield(*c3_info, c3_b_emlrt_marshallOut(0U), "fileTimeHi",
                  "fileTimeHi", 66);
  sf_mex_addfield(*c3_info, c3_b_emlrt_marshallOut(0U), "mFileTimeLo",
                  "mFileTimeLo", 66);
  sf_mex_addfield(*c3_info, c3_b_emlrt_marshallOut(0U), "mFileTimeHi",
                  "mFileTimeHi", 66);
  sf_mex_assign(&c3_rhs66, sf_mex_createcellmatrix(0, 1), false);
  sf_mex_assign(&c3_lhs66, sf_mex_createcellmatrix(0, 1), false);
  sf_mex_addfield(*c3_info, sf_mex_duplicatearraysafe(&c3_rhs66), "rhs", "rhs",
                  66);
  sf_mex_addfield(*c3_info, sf_mex_duplicatearraysafe(&c3_lhs66), "lhs", "lhs",
                  66);
  sf_mex_addfield(*c3_info, c3_emlrt_marshallOut(""), "context", "context", 67);
  sf_mex_addfield(*c3_info, c3_emlrt_marshallOut("tand"), "name", "name", 67);
  sf_mex_addfield(*c3_info, c3_emlrt_marshallOut("single"), "dominantType",
                  "dominantType", 67);
  sf_mex_addfield(*c3_info, c3_emlrt_marshallOut(
    "[ILXE]$matlabroot$/toolbox/eml/lib/matlab/elfun/tand.m"), "resolved",
                  "resolved", 67);
  sf_mex_addfield(*c3_info, c3_b_emlrt_marshallOut(1395332104U), "fileTimeLo",
                  "fileTimeLo", 67);
  sf_mex_addfield(*c3_info, c3_b_emlrt_marshallOut(0U), "fileTimeHi",
                  "fileTimeHi", 67);
  sf_mex_addfield(*c3_info, c3_b_emlrt_marshallOut(0U), "mFileTimeLo",
                  "mFileTimeLo", 67);
  sf_mex_addfield(*c3_info, c3_b_emlrt_marshallOut(0U), "mFileTimeHi",
                  "mFileTimeHi", 67);
  sf_mex_assign(&c3_rhs67, sf_mex_createcellmatrix(0, 1), false);
  sf_mex_assign(&c3_lhs67, sf_mex_createcellmatrix(0, 1), false);
  sf_mex_addfield(*c3_info, sf_mex_duplicatearraysafe(&c3_rhs67), "rhs", "rhs",
                  67);
  sf_mex_addfield(*c3_info, sf_mex_duplicatearraysafe(&c3_lhs67), "lhs", "lhs",
                  67);
  sf_mex_addfield(*c3_info, c3_emlrt_marshallOut(
    "[ILXE]$matlabroot$/toolbox/eml/lib/matlab/elfun/tand.m!scalar_real_tand"),
                  "context", "context", 68);
  sf_mex_addfield(*c3_info, c3_emlrt_marshallOut("isfinite"), "name", "name", 68);
  sf_mex_addfield(*c3_info, c3_emlrt_marshallOut("single"), "dominantType",
                  "dominantType", 68);
  sf_mex_addfield(*c3_info, c3_emlrt_marshallOut(
    "[ILXE]$matlabroot$/toolbox/eml/lib/matlab/elmat/isfinite.m"), "resolved",
                  "resolved", 68);
  sf_mex_addfield(*c3_info, c3_b_emlrt_marshallOut(1363717456U), "fileTimeLo",
                  "fileTimeLo", 68);
  sf_mex_addfield(*c3_info, c3_b_emlrt_marshallOut(0U), "fileTimeHi",
                  "fileTimeHi", 68);
  sf_mex_addfield(*c3_info, c3_b_emlrt_marshallOut(0U), "mFileTimeLo",
                  "mFileTimeLo", 68);
  sf_mex_addfield(*c3_info, c3_b_emlrt_marshallOut(0U), "mFileTimeHi",
                  "mFileTimeHi", 68);
  sf_mex_assign(&c3_rhs68, sf_mex_createcellmatrix(0, 1), false);
  sf_mex_assign(&c3_lhs68, sf_mex_createcellmatrix(0, 1), false);
  sf_mex_addfield(*c3_info, sf_mex_duplicatearraysafe(&c3_rhs68), "rhs", "rhs",
                  68);
  sf_mex_addfield(*c3_info, sf_mex_duplicatearraysafe(&c3_lhs68), "lhs", "lhs",
                  68);
  sf_mex_addfield(*c3_info, c3_emlrt_marshallOut(
    "[ILXE]$matlabroot$/toolbox/eml/lib/matlab/elmat/isfinite.m"), "context",
                  "context", 69);
  sf_mex_addfield(*c3_info, c3_emlrt_marshallOut(
    "coder.internal.isBuiltInNumeric"), "name", "name", 69);
  sf_mex_addfield(*c3_info, c3_emlrt_marshallOut("single"), "dominantType",
                  "dominantType", 69);
  sf_mex_addfield(*c3_info, c3_emlrt_marshallOut(
    "[ILXE]$matlabroot$/toolbox/shared/coder/coder/+coder/+internal/isBuiltInNumeric.m"),
                  "resolved", "resolved", 69);
  sf_mex_addfield(*c3_info, c3_b_emlrt_marshallOut(1395935456U), "fileTimeLo",
                  "fileTimeLo", 69);
  sf_mex_addfield(*c3_info, c3_b_emlrt_marshallOut(0U), "fileTimeHi",
                  "fileTimeHi", 69);
  sf_mex_addfield(*c3_info, c3_b_emlrt_marshallOut(0U), "mFileTimeLo",
                  "mFileTimeLo", 69);
  sf_mex_addfield(*c3_info, c3_b_emlrt_marshallOut(0U), "mFileTimeHi",
                  "mFileTimeHi", 69);
  sf_mex_assign(&c3_rhs69, sf_mex_createcellmatrix(0, 1), false);
  sf_mex_assign(&c3_lhs69, sf_mex_createcellmatrix(0, 1), false);
  sf_mex_addfield(*c3_info, sf_mex_duplicatearraysafe(&c3_rhs69), "rhs", "rhs",
                  69);
  sf_mex_addfield(*c3_info, sf_mex_duplicatearraysafe(&c3_lhs69), "lhs", "lhs",
                  69);
  sf_mex_addfield(*c3_info, c3_emlrt_marshallOut(
    "[ILXE]$matlabroot$/toolbox/eml/lib/matlab/elmat/isfinite.m"), "context",
                  "context", 70);
  sf_mex_addfield(*c3_info, c3_emlrt_marshallOut("isinf"), "name", "name", 70);
  sf_mex_addfield(*c3_info, c3_emlrt_marshallOut("single"), "dominantType",
                  "dominantType", 70);
  sf_mex_addfield(*c3_info, c3_emlrt_marshallOut(
    "[ILXE]$matlabroot$/toolbox/eml/lib/matlab/elmat/isinf.m"), "resolved",
                  "resolved", 70);
  sf_mex_addfield(*c3_info, c3_b_emlrt_marshallOut(1363717456U), "fileTimeLo",
                  "fileTimeLo", 70);
  sf_mex_addfield(*c3_info, c3_b_emlrt_marshallOut(0U), "fileTimeHi",
                  "fileTimeHi", 70);
  sf_mex_addfield(*c3_info, c3_b_emlrt_marshallOut(0U), "mFileTimeLo",
                  "mFileTimeLo", 70);
  sf_mex_addfield(*c3_info, c3_b_emlrt_marshallOut(0U), "mFileTimeHi",
                  "mFileTimeHi", 70);
  sf_mex_assign(&c3_rhs70, sf_mex_createcellmatrix(0, 1), false);
  sf_mex_assign(&c3_lhs70, sf_mex_createcellmatrix(0, 1), false);
  sf_mex_addfield(*c3_info, sf_mex_duplicatearraysafe(&c3_rhs70), "rhs", "rhs",
                  70);
  sf_mex_addfield(*c3_info, sf_mex_duplicatearraysafe(&c3_lhs70), "lhs", "lhs",
                  70);
  sf_mex_addfield(*c3_info, c3_emlrt_marshallOut(
    "[ILXE]$matlabroot$/toolbox/eml/lib/matlab/elmat/isinf.m"), "context",
                  "context", 71);
  sf_mex_addfield(*c3_info, c3_emlrt_marshallOut(
    "coder.internal.isBuiltInNumeric"), "name", "name", 71);
  sf_mex_addfield(*c3_info, c3_emlrt_marshallOut("single"), "dominantType",
                  "dominantType", 71);
  sf_mex_addfield(*c3_info, c3_emlrt_marshallOut(
    "[ILXE]$matlabroot$/toolbox/shared/coder/coder/+coder/+internal/isBuiltInNumeric.m"),
                  "resolved", "resolved", 71);
  sf_mex_addfield(*c3_info, c3_b_emlrt_marshallOut(1395935456U), "fileTimeLo",
                  "fileTimeLo", 71);
  sf_mex_addfield(*c3_info, c3_b_emlrt_marshallOut(0U), "fileTimeHi",
                  "fileTimeHi", 71);
  sf_mex_addfield(*c3_info, c3_b_emlrt_marshallOut(0U), "mFileTimeLo",
                  "mFileTimeLo", 71);
  sf_mex_addfield(*c3_info, c3_b_emlrt_marshallOut(0U), "mFileTimeHi",
                  "mFileTimeHi", 71);
  sf_mex_assign(&c3_rhs71, sf_mex_createcellmatrix(0, 1), false);
  sf_mex_assign(&c3_lhs71, sf_mex_createcellmatrix(0, 1), false);
  sf_mex_addfield(*c3_info, sf_mex_duplicatearraysafe(&c3_rhs71), "rhs", "rhs",
                  71);
  sf_mex_addfield(*c3_info, sf_mex_duplicatearraysafe(&c3_lhs71), "lhs", "lhs",
                  71);
  sf_mex_addfield(*c3_info, c3_emlrt_marshallOut(
    "[ILXE]$matlabroot$/toolbox/eml/lib/matlab/elmat/isfinite.m"), "context",
                  "context", 72);
  sf_mex_addfield(*c3_info, c3_emlrt_marshallOut("isnan"), "name", "name", 72);
  sf_mex_addfield(*c3_info, c3_emlrt_marshallOut("single"), "dominantType",
                  "dominantType", 72);
  sf_mex_addfield(*c3_info, c3_emlrt_marshallOut(
    "[ILXE]$matlabroot$/toolbox/eml/lib/matlab/elmat/isnan.m"), "resolved",
                  "resolved", 72);
  sf_mex_addfield(*c3_info, c3_b_emlrt_marshallOut(1363717458U), "fileTimeLo",
                  "fileTimeLo", 72);
  sf_mex_addfield(*c3_info, c3_b_emlrt_marshallOut(0U), "fileTimeHi",
                  "fileTimeHi", 72);
  sf_mex_addfield(*c3_info, c3_b_emlrt_marshallOut(0U), "mFileTimeLo",
                  "mFileTimeLo", 72);
  sf_mex_addfield(*c3_info, c3_b_emlrt_marshallOut(0U), "mFileTimeHi",
                  "mFileTimeHi", 72);
  sf_mex_assign(&c3_rhs72, sf_mex_createcellmatrix(0, 1), false);
  sf_mex_assign(&c3_lhs72, sf_mex_createcellmatrix(0, 1), false);
  sf_mex_addfield(*c3_info, sf_mex_duplicatearraysafe(&c3_rhs72), "rhs", "rhs",
                  72);
  sf_mex_addfield(*c3_info, sf_mex_duplicatearraysafe(&c3_lhs72), "lhs", "lhs",
                  72);
  sf_mex_addfield(*c3_info, c3_emlrt_marshallOut(
    "[ILXE]$matlabroot$/toolbox/eml/lib/matlab/elmat/isnan.m"), "context",
                  "context", 73);
  sf_mex_addfield(*c3_info, c3_emlrt_marshallOut(
    "coder.internal.isBuiltInNumeric"), "name", "name", 73);
  sf_mex_addfield(*c3_info, c3_emlrt_marshallOut("single"), "dominantType",
                  "dominantType", 73);
  sf_mex_addfield(*c3_info, c3_emlrt_marshallOut(
    "[ILXE]$matlabroot$/toolbox/shared/coder/coder/+coder/+internal/isBuiltInNumeric.m"),
                  "resolved", "resolved", 73);
  sf_mex_addfield(*c3_info, c3_b_emlrt_marshallOut(1395935456U), "fileTimeLo",
                  "fileTimeLo", 73);
  sf_mex_addfield(*c3_info, c3_b_emlrt_marshallOut(0U), "fileTimeHi",
                  "fileTimeHi", 73);
  sf_mex_addfield(*c3_info, c3_b_emlrt_marshallOut(0U), "mFileTimeLo",
                  "mFileTimeLo", 73);
  sf_mex_addfield(*c3_info, c3_b_emlrt_marshallOut(0U), "mFileTimeHi",
                  "mFileTimeHi", 73);
  sf_mex_assign(&c3_rhs73, sf_mex_createcellmatrix(0, 1), false);
  sf_mex_assign(&c3_lhs73, sf_mex_createcellmatrix(0, 1), false);
  sf_mex_addfield(*c3_info, sf_mex_duplicatearraysafe(&c3_rhs73), "rhs", "rhs",
                  73);
  sf_mex_addfield(*c3_info, sf_mex_duplicatearraysafe(&c3_lhs73), "lhs", "lhs",
                  73);
  sf_mex_addfield(*c3_info, c3_emlrt_marshallOut(
    "[ILXE]$matlabroot$/toolbox/eml/lib/matlab/elfun/tand.m!scalar_real_tand"),
                  "context", "context", 74);
  sf_mex_addfield(*c3_info, c3_emlrt_marshallOut("eml_guarded_nan"), "name",
                  "name", 74);
  sf_mex_addfield(*c3_info, c3_emlrt_marshallOut("char"), "dominantType",
                  "dominantType", 74);
  sf_mex_addfield(*c3_info, c3_emlrt_marshallOut(
    "[ILXE]$matlabroot$/toolbox/eml/lib/matlab/eml/eml_guarded_nan.m"),
                  "resolved", "resolved", 74);
  sf_mex_addfield(*c3_info, c3_b_emlrt_marshallOut(1286822376U), "fileTimeLo",
                  "fileTimeLo", 74);
  sf_mex_addfield(*c3_info, c3_b_emlrt_marshallOut(0U), "fileTimeHi",
                  "fileTimeHi", 74);
  sf_mex_addfield(*c3_info, c3_b_emlrt_marshallOut(0U), "mFileTimeLo",
                  "mFileTimeLo", 74);
  sf_mex_addfield(*c3_info, c3_b_emlrt_marshallOut(0U), "mFileTimeHi",
                  "mFileTimeHi", 74);
  sf_mex_assign(&c3_rhs74, sf_mex_createcellmatrix(0, 1), false);
  sf_mex_assign(&c3_lhs74, sf_mex_createcellmatrix(0, 1), false);
  sf_mex_addfield(*c3_info, sf_mex_duplicatearraysafe(&c3_rhs74), "rhs", "rhs",
                  74);
  sf_mex_addfield(*c3_info, sf_mex_duplicatearraysafe(&c3_lhs74), "lhs", "lhs",
                  74);
  sf_mex_addfield(*c3_info, c3_emlrt_marshallOut(
    "[ILXE]$matlabroot$/toolbox/eml/lib/matlab/eml/eml_guarded_nan.m"),
                  "context", "context", 75);
  sf_mex_addfield(*c3_info, c3_emlrt_marshallOut("eml_is_float_class"), "name",
                  "name", 75);
  sf_mex_addfield(*c3_info, c3_emlrt_marshallOut("char"), "dominantType",
                  "dominantType", 75);
  sf_mex_addfield(*c3_info, c3_emlrt_marshallOut(
    "[ILXE]$matlabroot$/toolbox/eml/lib/matlab/eml/eml_is_float_class.m"),
                  "resolved", "resolved", 75);
  sf_mex_addfield(*c3_info, c3_b_emlrt_marshallOut(1286822382U), "fileTimeLo",
                  "fileTimeLo", 75);
  sf_mex_addfield(*c3_info, c3_b_emlrt_marshallOut(0U), "fileTimeHi",
                  "fileTimeHi", 75);
  sf_mex_addfield(*c3_info, c3_b_emlrt_marshallOut(0U), "mFileTimeLo",
                  "mFileTimeLo", 75);
  sf_mex_addfield(*c3_info, c3_b_emlrt_marshallOut(0U), "mFileTimeHi",
                  "mFileTimeHi", 75);
  sf_mex_assign(&c3_rhs75, sf_mex_createcellmatrix(0, 1), false);
  sf_mex_assign(&c3_lhs75, sf_mex_createcellmatrix(0, 1), false);
  sf_mex_addfield(*c3_info, sf_mex_duplicatearraysafe(&c3_rhs75), "rhs", "rhs",
                  75);
  sf_mex_addfield(*c3_info, sf_mex_duplicatearraysafe(&c3_lhs75), "lhs", "lhs",
                  75);
  sf_mex_addfield(*c3_info, c3_emlrt_marshallOut(
    "[ILXE]$matlabroot$/toolbox/eml/lib/matlab/elfun/tand.m!scalar_real_tand"),
                  "context", "context", 76);
  sf_mex_addfield(*c3_info, c3_emlrt_marshallOut("eml_scalar_rem90"), "name",
                  "name", 76);
  sf_mex_addfield(*c3_info, c3_emlrt_marshallOut("single"), "dominantType",
                  "dominantType", 76);
  sf_mex_addfield(*c3_info, c3_emlrt_marshallOut(
    "[ILXE]$matlabroot$/toolbox/eml/lib/matlab/elfun/eml_scalar_rem90.m"),
                  "resolved", "resolved", 76);
  sf_mex_addfield(*c3_info, c3_b_emlrt_marshallOut(1343833978U), "fileTimeLo",
                  "fileTimeLo", 76);
  sf_mex_addfield(*c3_info, c3_b_emlrt_marshallOut(0U), "fileTimeHi",
                  "fileTimeHi", 76);
  sf_mex_addfield(*c3_info, c3_b_emlrt_marshallOut(0U), "mFileTimeLo",
                  "mFileTimeLo", 76);
  sf_mex_addfield(*c3_info, c3_b_emlrt_marshallOut(0U), "mFileTimeHi",
                  "mFileTimeHi", 76);
  sf_mex_assign(&c3_rhs76, sf_mex_createcellmatrix(0, 1), false);
  sf_mex_assign(&c3_lhs76, sf_mex_createcellmatrix(0, 1), false);
  sf_mex_addfield(*c3_info, sf_mex_duplicatearraysafe(&c3_rhs76), "rhs", "rhs",
                  76);
  sf_mex_addfield(*c3_info, sf_mex_duplicatearraysafe(&c3_lhs76), "lhs", "lhs",
                  76);
  sf_mex_addfield(*c3_info, c3_emlrt_marshallOut(
    "[ILXE]$matlabroot$/toolbox/eml/lib/matlab/elfun/eml_scalar_rem90.m"),
                  "context", "context", 77);
  sf_mex_addfield(*c3_info, c3_emlrt_marshallOut("mrdivide"), "name", "name", 77);
  sf_mex_addfield(*c3_info, c3_emlrt_marshallOut("double"), "dominantType",
                  "dominantType", 77);
  sf_mex_addfield(*c3_info, c3_emlrt_marshallOut(
    "[ILXE]$matlabroot$/toolbox/eml/lib/matlab/ops/mrdivide.p"), "resolved",
                  "resolved", 77);
  sf_mex_addfield(*c3_info, c3_b_emlrt_marshallOut(1410811248U), "fileTimeLo",
                  "fileTimeLo", 77);
  sf_mex_addfield(*c3_info, c3_b_emlrt_marshallOut(0U), "fileTimeHi",
                  "fileTimeHi", 77);
  sf_mex_addfield(*c3_info, c3_b_emlrt_marshallOut(1370013486U), "mFileTimeLo",
                  "mFileTimeLo", 77);
  sf_mex_addfield(*c3_info, c3_b_emlrt_marshallOut(0U), "mFileTimeHi",
                  "mFileTimeHi", 77);
  sf_mex_assign(&c3_rhs77, sf_mex_createcellmatrix(0, 1), false);
  sf_mex_assign(&c3_lhs77, sf_mex_createcellmatrix(0, 1), false);
  sf_mex_addfield(*c3_info, sf_mex_duplicatearraysafe(&c3_rhs77), "rhs", "rhs",
                  77);
  sf_mex_addfield(*c3_info, sf_mex_duplicatearraysafe(&c3_lhs77), "lhs", "lhs",
                  77);
  sf_mex_addfield(*c3_info, c3_emlrt_marshallOut(
    "[ILXE]$matlabroot$/toolbox/eml/lib/matlab/elfun/eml_scalar_rem90.m"),
                  "context", "context", 78);
  sf_mex_addfield(*c3_info, c3_emlrt_marshallOut("rem"), "name", "name", 78);
  sf_mex_addfield(*c3_info, c3_emlrt_marshallOut("single"), "dominantType",
                  "dominantType", 78);
  sf_mex_addfield(*c3_info, c3_emlrt_marshallOut(
    "[ILXE]$matlabroot$/toolbox/eml/lib/matlab/elfun/rem.m"), "resolved",
                  "resolved", 78);
  sf_mex_addfield(*c3_info, c3_b_emlrt_marshallOut(1363717454U), "fileTimeLo",
                  "fileTimeLo", 78);
  sf_mex_addfield(*c3_info, c3_b_emlrt_marshallOut(0U), "fileTimeHi",
                  "fileTimeHi", 78);
  sf_mex_addfield(*c3_info, c3_b_emlrt_marshallOut(0U), "mFileTimeLo",
                  "mFileTimeLo", 78);
  sf_mex_addfield(*c3_info, c3_b_emlrt_marshallOut(0U), "mFileTimeHi",
                  "mFileTimeHi", 78);
  sf_mex_assign(&c3_rhs78, sf_mex_createcellmatrix(0, 1), false);
  sf_mex_assign(&c3_lhs78, sf_mex_createcellmatrix(0, 1), false);
  sf_mex_addfield(*c3_info, sf_mex_duplicatearraysafe(&c3_rhs78), "rhs", "rhs",
                  78);
  sf_mex_addfield(*c3_info, sf_mex_duplicatearraysafe(&c3_lhs78), "lhs", "lhs",
                  78);
  sf_mex_addfield(*c3_info, c3_emlrt_marshallOut(
    "[ILXE]$matlabroot$/toolbox/eml/lib/matlab/elfun/rem.m"), "context",
                  "context", 79);
  sf_mex_addfield(*c3_info, c3_emlrt_marshallOut(
    "coder.internal.isBuiltInNumeric"), "name", "name", 79);
  sf_mex_addfield(*c3_info, c3_emlrt_marshallOut("single"), "dominantType",
                  "dominantType", 79);
  sf_mex_addfield(*c3_info, c3_emlrt_marshallOut(
    "[ILXE]$matlabroot$/toolbox/shared/coder/coder/+coder/+internal/isBuiltInNumeric.m"),
                  "resolved", "resolved", 79);
  sf_mex_addfield(*c3_info, c3_b_emlrt_marshallOut(1395935456U), "fileTimeLo",
                  "fileTimeLo", 79);
  sf_mex_addfield(*c3_info, c3_b_emlrt_marshallOut(0U), "fileTimeHi",
                  "fileTimeHi", 79);
  sf_mex_addfield(*c3_info, c3_b_emlrt_marshallOut(0U), "mFileTimeLo",
                  "mFileTimeLo", 79);
  sf_mex_addfield(*c3_info, c3_b_emlrt_marshallOut(0U), "mFileTimeHi",
                  "mFileTimeHi", 79);
  sf_mex_assign(&c3_rhs79, sf_mex_createcellmatrix(0, 1), false);
  sf_mex_assign(&c3_lhs79, sf_mex_createcellmatrix(0, 1), false);
  sf_mex_addfield(*c3_info, sf_mex_duplicatearraysafe(&c3_rhs79), "rhs", "rhs",
                  79);
  sf_mex_addfield(*c3_info, sf_mex_duplicatearraysafe(&c3_lhs79), "lhs", "lhs",
                  79);
  sf_mex_addfield(*c3_info, c3_emlrt_marshallOut(
    "[ILXE]$matlabroot$/toolbox/eml/lib/matlab/elfun/rem.m"), "context",
                  "context", 80);
  sf_mex_addfield(*c3_info, c3_emlrt_marshallOut("eml_scalar_eg"), "name",
                  "name", 80);
  sf_mex_addfield(*c3_info, c3_emlrt_marshallOut("single"), "dominantType",
                  "dominantType", 80);
  sf_mex_addfield(*c3_info, c3_emlrt_marshallOut(
    "[ILXE]$matlabroot$/toolbox/eml/lib/matlab/eml/eml_scalar_eg.m"), "resolved",
                  "resolved", 80);
  sf_mex_addfield(*c3_info, c3_b_emlrt_marshallOut(1375984288U), "fileTimeLo",
                  "fileTimeLo", 80);
  sf_mex_addfield(*c3_info, c3_b_emlrt_marshallOut(0U), "fileTimeHi",
                  "fileTimeHi", 80);
  sf_mex_addfield(*c3_info, c3_b_emlrt_marshallOut(0U), "mFileTimeLo",
                  "mFileTimeLo", 80);
  sf_mex_addfield(*c3_info, c3_b_emlrt_marshallOut(0U), "mFileTimeHi",
                  "mFileTimeHi", 80);
  sf_mex_assign(&c3_rhs80, sf_mex_createcellmatrix(0, 1), false);
  sf_mex_assign(&c3_lhs80, sf_mex_createcellmatrix(0, 1), false);
  sf_mex_addfield(*c3_info, sf_mex_duplicatearraysafe(&c3_rhs80), "rhs", "rhs",
                  80);
  sf_mex_addfield(*c3_info, sf_mex_duplicatearraysafe(&c3_lhs80), "lhs", "lhs",
                  80);
  sf_mex_addfield(*c3_info, c3_emlrt_marshallOut(
    "[ILXE]$matlabroot$/toolbox/eml/lib/matlab/eml/eml_scalar_eg.m"), "context",
                  "context", 81);
  sf_mex_addfield(*c3_info, c3_emlrt_marshallOut("coder.internal.scalarEg"),
                  "name", "name", 81);
  sf_mex_addfield(*c3_info, c3_emlrt_marshallOut("single"), "dominantType",
                  "dominantType", 81);
  sf_mex_addfield(*c3_info, c3_emlrt_marshallOut(
    "[ILXE]$matlabroot$/toolbox/coder/coder/+coder/+internal/scalarEg.p"),
                  "resolved", "resolved", 81);
  sf_mex_addfield(*c3_info, c3_b_emlrt_marshallOut(1410811370U), "fileTimeLo",
                  "fileTimeLo", 81);
  sf_mex_addfield(*c3_info, c3_b_emlrt_marshallOut(0U), "fileTimeHi",
                  "fileTimeHi", 81);
  sf_mex_addfield(*c3_info, c3_b_emlrt_marshallOut(0U), "mFileTimeLo",
                  "mFileTimeLo", 81);
  sf_mex_addfield(*c3_info, c3_b_emlrt_marshallOut(0U), "mFileTimeHi",
                  "mFileTimeHi", 81);
  sf_mex_assign(&c3_rhs81, sf_mex_createcellmatrix(0, 1), false);
  sf_mex_assign(&c3_lhs81, sf_mex_createcellmatrix(0, 1), false);
  sf_mex_addfield(*c3_info, sf_mex_duplicatearraysafe(&c3_rhs81), "rhs", "rhs",
                  81);
  sf_mex_addfield(*c3_info, sf_mex_duplicatearraysafe(&c3_lhs81), "lhs", "lhs",
                  81);
  sf_mex_addfield(*c3_info, c3_emlrt_marshallOut(
    "[ILXE]$matlabroot$/toolbox/eml/lib/matlab/elfun/rem.m"), "context",
                  "context", 82);
  sf_mex_addfield(*c3_info, c3_emlrt_marshallOut("eml_scalexp_alloc"), "name",
                  "name", 82);
  sf_mex_addfield(*c3_info, c3_emlrt_marshallOut("single"), "dominantType",
                  "dominantType", 82);
  sf_mex_addfield(*c3_info, c3_emlrt_marshallOut(
    "[ILXE]$matlabroot$/toolbox/eml/lib/matlab/eml/eml_scalexp_alloc.m"),
                  "resolved", "resolved", 82);
  sf_mex_addfield(*c3_info, c3_b_emlrt_marshallOut(1375984288U), "fileTimeLo",
                  "fileTimeLo", 82);
  sf_mex_addfield(*c3_info, c3_b_emlrt_marshallOut(0U), "fileTimeHi",
                  "fileTimeHi", 82);
  sf_mex_addfield(*c3_info, c3_b_emlrt_marshallOut(0U), "mFileTimeLo",
                  "mFileTimeLo", 82);
  sf_mex_addfield(*c3_info, c3_b_emlrt_marshallOut(0U), "mFileTimeHi",
                  "mFileTimeHi", 82);
  sf_mex_assign(&c3_rhs82, sf_mex_createcellmatrix(0, 1), false);
  sf_mex_assign(&c3_lhs82, sf_mex_createcellmatrix(0, 1), false);
  sf_mex_addfield(*c3_info, sf_mex_duplicatearraysafe(&c3_rhs82), "rhs", "rhs",
                  82);
  sf_mex_addfield(*c3_info, sf_mex_duplicatearraysafe(&c3_lhs82), "lhs", "lhs",
                  82);
  sf_mex_addfield(*c3_info, c3_emlrt_marshallOut(
    "[ILXE]$matlabroot$/toolbox/eml/lib/matlab/eml/eml_scalexp_alloc.m"),
                  "context", "context", 83);
  sf_mex_addfield(*c3_info, c3_emlrt_marshallOut("coder.internal.scalexpAlloc"),
                  "name", "name", 83);
  sf_mex_addfield(*c3_info, c3_emlrt_marshallOut("single"), "dominantType",
                  "dominantType", 83);
  sf_mex_addfield(*c3_info, c3_emlrt_marshallOut(
    "[ILXE]$matlabroot$/toolbox/coder/coder/+coder/+internal/scalexpAlloc.p"),
                  "resolved", "resolved", 83);
  sf_mex_addfield(*c3_info, c3_b_emlrt_marshallOut(1410811370U), "fileTimeLo",
                  "fileTimeLo", 83);
  sf_mex_addfield(*c3_info, c3_b_emlrt_marshallOut(0U), "fileTimeHi",
                  "fileTimeHi", 83);
  sf_mex_addfield(*c3_info, c3_b_emlrt_marshallOut(0U), "mFileTimeLo",
                  "mFileTimeLo", 83);
  sf_mex_addfield(*c3_info, c3_b_emlrt_marshallOut(0U), "mFileTimeHi",
                  "mFileTimeHi", 83);
  sf_mex_assign(&c3_rhs83, sf_mex_createcellmatrix(0, 1), false);
  sf_mex_assign(&c3_lhs83, sf_mex_createcellmatrix(0, 1), false);
  sf_mex_addfield(*c3_info, sf_mex_duplicatearraysafe(&c3_rhs83), "rhs", "rhs",
                  83);
  sf_mex_addfield(*c3_info, sf_mex_duplicatearraysafe(&c3_lhs83), "lhs", "lhs",
                  83);
  sf_mex_addfield(*c3_info, c3_emlrt_marshallOut(
    "[ILXE]$matlabroot$/toolbox/eml/lib/matlab/elfun/eml_scalar_rem90.m"),
                  "context", "context", 84);
  sf_mex_addfield(*c3_info, c3_emlrt_marshallOut("abs"), "name", "name", 84);
  sf_mex_addfield(*c3_info, c3_emlrt_marshallOut("single"), "dominantType",
                  "dominantType", 84);
  sf_mex_addfield(*c3_info, c3_emlrt_marshallOut(
    "[ILXE]$matlabroot$/toolbox/eml/lib/matlab/elfun/abs.m"), "resolved",
                  "resolved", 84);
  sf_mex_addfield(*c3_info, c3_b_emlrt_marshallOut(1363717452U), "fileTimeLo",
                  "fileTimeLo", 84);
  sf_mex_addfield(*c3_info, c3_b_emlrt_marshallOut(0U), "fileTimeHi",
                  "fileTimeHi", 84);
  sf_mex_addfield(*c3_info, c3_b_emlrt_marshallOut(0U), "mFileTimeLo",
                  "mFileTimeLo", 84);
  sf_mex_addfield(*c3_info, c3_b_emlrt_marshallOut(0U), "mFileTimeHi",
                  "mFileTimeHi", 84);
  sf_mex_assign(&c3_rhs84, sf_mex_createcellmatrix(0, 1), false);
  sf_mex_assign(&c3_lhs84, sf_mex_createcellmatrix(0, 1), false);
  sf_mex_addfield(*c3_info, sf_mex_duplicatearraysafe(&c3_rhs84), "rhs", "rhs",
                  84);
  sf_mex_addfield(*c3_info, sf_mex_duplicatearraysafe(&c3_lhs84), "lhs", "lhs",
                  84);
  sf_mex_addfield(*c3_info, c3_emlrt_marshallOut(
    "[ILXE]$matlabroot$/toolbox/eml/lib/matlab/elfun/eml_scalar_rem90.m"),
                  "context", "context", 85);
  sf_mex_addfield(*c3_info, c3_emlrt_marshallOut("eml_mtimes_helper"), "name",
                  "name", 85);
  sf_mex_addfield(*c3_info, c3_emlrt_marshallOut(""), "dominantType",
                  "dominantType", 85);
  sf_mex_addfield(*c3_info, c3_emlrt_marshallOut(
    "[ILXE]$matlabroot$/toolbox/eml/lib/matlab/ops/eml_mtimes_helper.m"),
                  "resolved", "resolved", 85);
  sf_mex_addfield(*c3_info, c3_b_emlrt_marshallOut(1383880894U), "fileTimeLo",
                  "fileTimeLo", 85);
  sf_mex_addfield(*c3_info, c3_b_emlrt_marshallOut(0U), "fileTimeHi",
                  "fileTimeHi", 85);
  sf_mex_addfield(*c3_info, c3_b_emlrt_marshallOut(0U), "mFileTimeLo",
                  "mFileTimeLo", 85);
  sf_mex_addfield(*c3_info, c3_b_emlrt_marshallOut(0U), "mFileTimeHi",
                  "mFileTimeHi", 85);
  sf_mex_assign(&c3_rhs85, sf_mex_createcellmatrix(0, 1), false);
  sf_mex_assign(&c3_lhs85, sf_mex_createcellmatrix(0, 1), false);
  sf_mex_addfield(*c3_info, sf_mex_duplicatearraysafe(&c3_rhs85), "rhs", "rhs",
                  85);
  sf_mex_addfield(*c3_info, sf_mex_duplicatearraysafe(&c3_lhs85), "lhs", "lhs",
                  85);
  sf_mex_addfield(*c3_info, c3_emlrt_marshallOut(
    "[ILXE]$matlabroot$/toolbox/eml/lib/matlab/elfun/tand.m!scalar_real_tand"),
                  "context", "context", 86);
  sf_mex_addfield(*c3_info, c3_emlrt_marshallOut("rdivide"), "name", "name", 86);
  sf_mex_addfield(*c3_info, c3_emlrt_marshallOut("single"), "dominantType",
                  "dominantType", 86);
  sf_mex_addfield(*c3_info, c3_emlrt_marshallOut(
    "[ILXE]$matlabroot$/toolbox/eml/lib/matlab/ops/rdivide.m"), "resolved",
                  "resolved", 86);
  sf_mex_addfield(*c3_info, c3_b_emlrt_marshallOut(1363717480U), "fileTimeLo",
                  "fileTimeLo", 86);
  sf_mex_addfield(*c3_info, c3_b_emlrt_marshallOut(0U), "fileTimeHi",
                  "fileTimeHi", 86);
  sf_mex_addfield(*c3_info, c3_b_emlrt_marshallOut(0U), "mFileTimeLo",
                  "mFileTimeLo", 86);
  sf_mex_addfield(*c3_info, c3_b_emlrt_marshallOut(0U), "mFileTimeHi",
                  "mFileTimeHi", 86);
  sf_mex_assign(&c3_rhs86, sf_mex_createcellmatrix(0, 1), false);
  sf_mex_assign(&c3_lhs86, sf_mex_createcellmatrix(0, 1), false);
  sf_mex_addfield(*c3_info, sf_mex_duplicatearraysafe(&c3_rhs86), "rhs", "rhs",
                  86);
  sf_mex_addfield(*c3_info, sf_mex_duplicatearraysafe(&c3_lhs86), "lhs", "lhs",
                  86);
  sf_mex_addfield(*c3_info, c3_emlrt_marshallOut(
    "[ILXE]$matlabroot$/toolbox/eml/lib/matlab/ops/rdivide.m"), "context",
                  "context", 87);
  sf_mex_addfield(*c3_info, c3_emlrt_marshallOut(
    "coder.internal.isBuiltInNumeric"), "name", "name", 87);
  sf_mex_addfield(*c3_info, c3_emlrt_marshallOut("single"), "dominantType",
                  "dominantType", 87);
  sf_mex_addfield(*c3_info, c3_emlrt_marshallOut(
    "[ILXE]$matlabroot$/toolbox/shared/coder/coder/+coder/+internal/isBuiltInNumeric.m"),
                  "resolved", "resolved", 87);
  sf_mex_addfield(*c3_info, c3_b_emlrt_marshallOut(1395935456U), "fileTimeLo",
                  "fileTimeLo", 87);
  sf_mex_addfield(*c3_info, c3_b_emlrt_marshallOut(0U), "fileTimeHi",
                  "fileTimeHi", 87);
  sf_mex_addfield(*c3_info, c3_b_emlrt_marshallOut(0U), "mFileTimeLo",
                  "mFileTimeLo", 87);
  sf_mex_addfield(*c3_info, c3_b_emlrt_marshallOut(0U), "mFileTimeHi",
                  "mFileTimeHi", 87);
  sf_mex_assign(&c3_rhs87, sf_mex_createcellmatrix(0, 1), false);
  sf_mex_assign(&c3_lhs87, sf_mex_createcellmatrix(0, 1), false);
  sf_mex_addfield(*c3_info, sf_mex_duplicatearraysafe(&c3_rhs87), "rhs", "rhs",
                  87);
  sf_mex_addfield(*c3_info, sf_mex_duplicatearraysafe(&c3_lhs87), "lhs", "lhs",
                  87);
  sf_mex_addfield(*c3_info, c3_emlrt_marshallOut(
    "[ILXE]$matlabroot$/toolbox/eml/lib/matlab/ops/rdivide.m"), "context",
                  "context", 88);
  sf_mex_addfield(*c3_info, c3_emlrt_marshallOut("eml_scalexp_compatible"),
                  "name", "name", 88);
  sf_mex_addfield(*c3_info, c3_emlrt_marshallOut("single"), "dominantType",
                  "dominantType", 88);
  sf_mex_addfield(*c3_info, c3_emlrt_marshallOut(
    "[ILXE]$matlabroot$/toolbox/eml/lib/matlab/eml/eml_scalexp_compatible.m"),
                  "resolved", "resolved", 88);
  sf_mex_addfield(*c3_info, c3_b_emlrt_marshallOut(1286822396U), "fileTimeLo",
                  "fileTimeLo", 88);
  sf_mex_addfield(*c3_info, c3_b_emlrt_marshallOut(0U), "fileTimeHi",
                  "fileTimeHi", 88);
  sf_mex_addfield(*c3_info, c3_b_emlrt_marshallOut(0U), "mFileTimeLo",
                  "mFileTimeLo", 88);
  sf_mex_addfield(*c3_info, c3_b_emlrt_marshallOut(0U), "mFileTimeHi",
                  "mFileTimeHi", 88);
  sf_mex_assign(&c3_rhs88, sf_mex_createcellmatrix(0, 1), false);
  sf_mex_assign(&c3_lhs88, sf_mex_createcellmatrix(0, 1), false);
  sf_mex_addfield(*c3_info, sf_mex_duplicatearraysafe(&c3_rhs88), "rhs", "rhs",
                  88);
  sf_mex_addfield(*c3_info, sf_mex_duplicatearraysafe(&c3_lhs88), "lhs", "lhs",
                  88);
  sf_mex_addfield(*c3_info, c3_emlrt_marshallOut(
    "[ILXE]$matlabroot$/toolbox/eml/lib/matlab/ops/rdivide.m"), "context",
                  "context", 89);
  sf_mex_addfield(*c3_info, c3_emlrt_marshallOut("eml_div"), "name", "name", 89);
  sf_mex_addfield(*c3_info, c3_emlrt_marshallOut("single"), "dominantType",
                  "dominantType", 89);
  sf_mex_addfield(*c3_info, c3_emlrt_marshallOut(
    "[ILXE]$matlabroot$/toolbox/eml/lib/matlab/eml/eml_div.m"), "resolved",
                  "resolved", 89);
  sf_mex_addfield(*c3_info, c3_b_emlrt_marshallOut(1386427552U), "fileTimeLo",
                  "fileTimeLo", 89);
  sf_mex_addfield(*c3_info, c3_b_emlrt_marshallOut(0U), "fileTimeHi",
                  "fileTimeHi", 89);
  sf_mex_addfield(*c3_info, c3_b_emlrt_marshallOut(0U), "mFileTimeLo",
                  "mFileTimeLo", 89);
  sf_mex_addfield(*c3_info, c3_b_emlrt_marshallOut(0U), "mFileTimeHi",
                  "mFileTimeHi", 89);
  sf_mex_assign(&c3_rhs89, sf_mex_createcellmatrix(0, 1), false);
  sf_mex_assign(&c3_lhs89, sf_mex_createcellmatrix(0, 1), false);
  sf_mex_addfield(*c3_info, sf_mex_duplicatearraysafe(&c3_rhs89), "rhs", "rhs",
                  89);
  sf_mex_addfield(*c3_info, sf_mex_duplicatearraysafe(&c3_lhs89), "lhs", "lhs",
                  89);
  sf_mex_addfield(*c3_info, c3_emlrt_marshallOut(
    "[ILXE]$matlabroot$/toolbox/eml/lib/matlab/eml/eml_div.m"), "context",
                  "context", 90);
  sf_mex_addfield(*c3_info, c3_emlrt_marshallOut("coder.internal.div"), "name",
                  "name", 90);
  sf_mex_addfield(*c3_info, c3_emlrt_marshallOut("single"), "dominantType",
                  "dominantType", 90);
  sf_mex_addfield(*c3_info, c3_emlrt_marshallOut(
    "[ILXE]$matlabroot$/toolbox/coder/coder/+coder/+internal/div.p"), "resolved",
                  "resolved", 90);
  sf_mex_addfield(*c3_info, c3_b_emlrt_marshallOut(1410811370U), "fileTimeLo",
                  "fileTimeLo", 90);
  sf_mex_addfield(*c3_info, c3_b_emlrt_marshallOut(0U), "fileTimeHi",
                  "fileTimeHi", 90);
  sf_mex_addfield(*c3_info, c3_b_emlrt_marshallOut(0U), "mFileTimeLo",
                  "mFileTimeLo", 90);
  sf_mex_addfield(*c3_info, c3_b_emlrt_marshallOut(0U), "mFileTimeHi",
                  "mFileTimeHi", 90);
  sf_mex_assign(&c3_rhs90, sf_mex_createcellmatrix(0, 1), false);
  sf_mex_assign(&c3_lhs90, sf_mex_createcellmatrix(0, 1), false);
  sf_mex_addfield(*c3_info, sf_mex_duplicatearraysafe(&c3_rhs90), "rhs", "rhs",
                  90);
  sf_mex_addfield(*c3_info, sf_mex_duplicatearraysafe(&c3_lhs90), "lhs", "lhs",
                  90);
  sf_mex_addfield(*c3_info, c3_emlrt_marshallOut(""), "context", "context", 91);
  sf_mex_addfield(*c3_info, c3_emlrt_marshallOut("eml_li_find"), "name", "name",
                  91);
  sf_mex_addfield(*c3_info, c3_emlrt_marshallOut(""), "dominantType",
                  "dominantType", 91);
  sf_mex_addfield(*c3_info, c3_emlrt_marshallOut(
    "[ILXE]$matlabroot$/toolbox/eml/lib/matlab/eml/eml_li_find.m"), "resolved",
                  "resolved", 91);
  sf_mex_addfield(*c3_info, c3_b_emlrt_marshallOut(1286822386U), "fileTimeLo",
                  "fileTimeLo", 91);
  sf_mex_addfield(*c3_info, c3_b_emlrt_marshallOut(0U), "fileTimeHi",
                  "fileTimeHi", 91);
  sf_mex_addfield(*c3_info, c3_b_emlrt_marshallOut(0U), "mFileTimeLo",
                  "mFileTimeLo", 91);
  sf_mex_addfield(*c3_info, c3_b_emlrt_marshallOut(0U), "mFileTimeHi",
                  "mFileTimeHi", 91);
  sf_mex_assign(&c3_rhs91, sf_mex_createcellmatrix(0, 1), false);
  sf_mex_assign(&c3_lhs91, sf_mex_createcellmatrix(0, 1), false);
  sf_mex_addfield(*c3_info, sf_mex_duplicatearraysafe(&c3_rhs91), "rhs", "rhs",
                  91);
  sf_mex_addfield(*c3_info, sf_mex_duplicatearraysafe(&c3_lhs91), "lhs", "lhs",
                  91);
  sf_mex_addfield(*c3_info, c3_emlrt_marshallOut(
    "[ILXE]$matlabroot$/toolbox/eml/lib/matlab/eml/eml_li_find.m"), "context",
                  "context", 92);
  sf_mex_addfield(*c3_info, c3_emlrt_marshallOut("eml_index_class"), "name",
                  "name", 92);
  sf_mex_addfield(*c3_info, c3_emlrt_marshallOut(""), "dominantType",
                  "dominantType", 92);
  sf_mex_addfield(*c3_info, c3_emlrt_marshallOut(
    "[ILXE]$matlabroot$/toolbox/eml/lib/matlab/eml/eml_index_class.m"),
                  "resolved", "resolved", 92);
  sf_mex_addfield(*c3_info, c3_b_emlrt_marshallOut(1323174178U), "fileTimeLo",
                  "fileTimeLo", 92);
  sf_mex_addfield(*c3_info, c3_b_emlrt_marshallOut(0U), "fileTimeHi",
                  "fileTimeHi", 92);
  sf_mex_addfield(*c3_info, c3_b_emlrt_marshallOut(0U), "mFileTimeLo",
                  "mFileTimeLo", 92);
  sf_mex_addfield(*c3_info, c3_b_emlrt_marshallOut(0U), "mFileTimeHi",
                  "mFileTimeHi", 92);
  sf_mex_assign(&c3_rhs92, sf_mex_createcellmatrix(0, 1), false);
  sf_mex_assign(&c3_lhs92, sf_mex_createcellmatrix(0, 1), false);
  sf_mex_addfield(*c3_info, sf_mex_duplicatearraysafe(&c3_rhs92), "rhs", "rhs",
                  92);
  sf_mex_addfield(*c3_info, sf_mex_duplicatearraysafe(&c3_lhs92), "lhs", "lhs",
                  92);
  sf_mex_addfield(*c3_info, c3_emlrt_marshallOut(
    "[ILXE]$matlabroot$/toolbox/eml/lib/matlab/eml/eml_li_find.m!compute_nones"),
                  "context", "context", 93);
  sf_mex_addfield(*c3_info, c3_emlrt_marshallOut("eml_index_class"), "name",
                  "name", 93);
  sf_mex_addfield(*c3_info, c3_emlrt_marshallOut(""), "dominantType",
                  "dominantType", 93);
  sf_mex_addfield(*c3_info, c3_emlrt_marshallOut(
    "[ILXE]$matlabroot$/toolbox/eml/lib/matlab/eml/eml_index_class.m"),
                  "resolved", "resolved", 93);
  sf_mex_addfield(*c3_info, c3_b_emlrt_marshallOut(1323174178U), "fileTimeLo",
                  "fileTimeLo", 93);
  sf_mex_addfield(*c3_info, c3_b_emlrt_marshallOut(0U), "fileTimeHi",
                  "fileTimeHi", 93);
  sf_mex_addfield(*c3_info, c3_b_emlrt_marshallOut(0U), "mFileTimeLo",
                  "mFileTimeLo", 93);
  sf_mex_addfield(*c3_info, c3_b_emlrt_marshallOut(0U), "mFileTimeHi",
                  "mFileTimeHi", 93);
  sf_mex_assign(&c3_rhs93, sf_mex_createcellmatrix(0, 1), false);
  sf_mex_assign(&c3_lhs93, sf_mex_createcellmatrix(0, 1), false);
  sf_mex_addfield(*c3_info, sf_mex_duplicatearraysafe(&c3_rhs93), "rhs", "rhs",
                  93);
  sf_mex_addfield(*c3_info, sf_mex_duplicatearraysafe(&c3_lhs93), "lhs", "lhs",
                  93);
  sf_mex_addfield(*c3_info, c3_emlrt_marshallOut(
    "[ILXE]$matlabroot$/toolbox/eml/lib/matlab/eml/eml_li_find.m!compute_nones"),
                  "context", "context", 94);
  sf_mex_addfield(*c3_info, c3_emlrt_marshallOut(
    "eml_int_forloop_overflow_check"), "name", "name", 94);
  sf_mex_addfield(*c3_info, c3_emlrt_marshallOut(""), "dominantType",
                  "dominantType", 94);
  sf_mex_addfield(*c3_info, c3_emlrt_marshallOut(
    "[ILXE]$matlabroot$/toolbox/eml/lib/matlab/eml/eml_int_forloop_overflow_check.m"),
                  "resolved", "resolved", 94);
  sf_mex_addfield(*c3_info, c3_b_emlrt_marshallOut(1397261022U), "fileTimeLo",
                  "fileTimeLo", 94);
  sf_mex_addfield(*c3_info, c3_b_emlrt_marshallOut(0U), "fileTimeHi",
                  "fileTimeHi", 94);
  sf_mex_addfield(*c3_info, c3_b_emlrt_marshallOut(0U), "mFileTimeLo",
                  "mFileTimeLo", 94);
  sf_mex_addfield(*c3_info, c3_b_emlrt_marshallOut(0U), "mFileTimeHi",
                  "mFileTimeHi", 94);
  sf_mex_assign(&c3_rhs94, sf_mex_createcellmatrix(0, 1), false);
  sf_mex_assign(&c3_lhs94, sf_mex_createcellmatrix(0, 1), false);
  sf_mex_addfield(*c3_info, sf_mex_duplicatearraysafe(&c3_rhs94), "rhs", "rhs",
                  94);
  sf_mex_addfield(*c3_info, sf_mex_duplicatearraysafe(&c3_lhs94), "lhs", "lhs",
                  94);
  sf_mex_addfield(*c3_info, c3_emlrt_marshallOut(
    "[ILXE]$matlabroot$/toolbox/eml/lib/matlab/eml/eml_li_find.m!compute_nones"),
                  "context", "context", 95);
  sf_mex_addfield(*c3_info, c3_emlrt_marshallOut("eml_index_plus"), "name",
                  "name", 95);
  sf_mex_addfield(*c3_info, c3_emlrt_marshallOut("double"), "dominantType",
                  "dominantType", 95);
  sf_mex_addfield(*c3_info, c3_emlrt_marshallOut(
    "[ILXE]$matlabroot$/toolbox/eml/lib/matlab/eml/eml_index_plus.m"),
                  "resolved", "resolved", 95);
  sf_mex_addfield(*c3_info, c3_b_emlrt_marshallOut(1372586016U), "fileTimeLo",
                  "fileTimeLo", 95);
  sf_mex_addfield(*c3_info, c3_b_emlrt_marshallOut(0U), "fileTimeHi",
                  "fileTimeHi", 95);
  sf_mex_addfield(*c3_info, c3_b_emlrt_marshallOut(0U), "mFileTimeLo",
                  "mFileTimeLo", 95);
  sf_mex_addfield(*c3_info, c3_b_emlrt_marshallOut(0U), "mFileTimeHi",
                  "mFileTimeHi", 95);
  sf_mex_assign(&c3_rhs95, sf_mex_createcellmatrix(0, 1), false);
  sf_mex_assign(&c3_lhs95, sf_mex_createcellmatrix(0, 1), false);
  sf_mex_addfield(*c3_info, sf_mex_duplicatearraysafe(&c3_rhs95), "rhs", "rhs",
                  95);
  sf_mex_addfield(*c3_info, sf_mex_duplicatearraysafe(&c3_lhs95), "lhs", "lhs",
                  95);
  sf_mex_addfield(*c3_info, c3_emlrt_marshallOut(
    "[ILXE]$matlabroot$/toolbox/eml/lib/matlab/eml/eml_li_find.m"), "context",
                  "context", 96);
  sf_mex_addfield(*c3_info, c3_emlrt_marshallOut(
    "eml_int_forloop_overflow_check"), "name", "name", 96);
  sf_mex_addfield(*c3_info, c3_emlrt_marshallOut(""), "dominantType",
                  "dominantType", 96);
  sf_mex_addfield(*c3_info, c3_emlrt_marshallOut(
    "[ILXE]$matlabroot$/toolbox/eml/lib/matlab/eml/eml_int_forloop_overflow_check.m"),
                  "resolved", "resolved", 96);
  sf_mex_addfield(*c3_info, c3_b_emlrt_marshallOut(1397261022U), "fileTimeLo",
                  "fileTimeLo", 96);
  sf_mex_addfield(*c3_info, c3_b_emlrt_marshallOut(0U), "fileTimeHi",
                  "fileTimeHi", 96);
  sf_mex_addfield(*c3_info, c3_b_emlrt_marshallOut(0U), "mFileTimeLo",
                  "mFileTimeLo", 96);
  sf_mex_addfield(*c3_info, c3_b_emlrt_marshallOut(0U), "mFileTimeHi",
                  "mFileTimeHi", 96);
  sf_mex_assign(&c3_rhs96, sf_mex_createcellmatrix(0, 1), false);
  sf_mex_assign(&c3_lhs96, sf_mex_createcellmatrix(0, 1), false);
  sf_mex_addfield(*c3_info, sf_mex_duplicatearraysafe(&c3_rhs96), "rhs", "rhs",
                  96);
  sf_mex_addfield(*c3_info, sf_mex_duplicatearraysafe(&c3_lhs96), "lhs", "lhs",
                  96);
  sf_mex_addfield(*c3_info, c3_emlrt_marshallOut(
    "[ILXE]$matlabroot$/toolbox/eml/lib/matlab/eml/eml_li_find.m"), "context",
                  "context", 97);
  sf_mex_addfield(*c3_info, c3_emlrt_marshallOut("eml_index_plus"), "name",
                  "name", 97);
  sf_mex_addfield(*c3_info, c3_emlrt_marshallOut("double"), "dominantType",
                  "dominantType", 97);
  sf_mex_addfield(*c3_info, c3_emlrt_marshallOut(
    "[ILXE]$matlabroot$/toolbox/eml/lib/matlab/eml/eml_index_plus.m"),
                  "resolved", "resolved", 97);
  sf_mex_addfield(*c3_info, c3_b_emlrt_marshallOut(1372586016U), "fileTimeLo",
                  "fileTimeLo", 97);
  sf_mex_addfield(*c3_info, c3_b_emlrt_marshallOut(0U), "fileTimeHi",
                  "fileTimeHi", 97);
  sf_mex_addfield(*c3_info, c3_b_emlrt_marshallOut(0U), "mFileTimeLo",
                  "mFileTimeLo", 97);
  sf_mex_addfield(*c3_info, c3_b_emlrt_marshallOut(0U), "mFileTimeHi",
                  "mFileTimeHi", 97);
  sf_mex_assign(&c3_rhs97, sf_mex_createcellmatrix(0, 1), false);
  sf_mex_assign(&c3_lhs97, sf_mex_createcellmatrix(0, 1), false);
  sf_mex_addfield(*c3_info, sf_mex_duplicatearraysafe(&c3_rhs97), "rhs", "rhs",
                  97);
  sf_mex_addfield(*c3_info, sf_mex_duplicatearraysafe(&c3_lhs97), "lhs", "lhs",
                  97);
  sf_mex_addfield(*c3_info, c3_emlrt_marshallOut(""), "context", "context", 98);
  sf_mex_addfield(*c3_info, c3_emlrt_marshallOut("length"), "name", "name", 98);
  sf_mex_addfield(*c3_info, c3_emlrt_marshallOut("single"), "dominantType",
                  "dominantType", 98);
  sf_mex_addfield(*c3_info, c3_emlrt_marshallOut(
    "[ILXE]$matlabroot$/toolbox/eml/lib/matlab/elmat/length.m"), "resolved",
                  "resolved", 98);
  sf_mex_addfield(*c3_info, c3_b_emlrt_marshallOut(1303149806U), "fileTimeLo",
                  "fileTimeLo", 98);
  sf_mex_addfield(*c3_info, c3_b_emlrt_marshallOut(0U), "fileTimeHi",
                  "fileTimeHi", 98);
  sf_mex_addfield(*c3_info, c3_b_emlrt_marshallOut(0U), "mFileTimeLo",
                  "mFileTimeLo", 98);
  sf_mex_addfield(*c3_info, c3_b_emlrt_marshallOut(0U), "mFileTimeHi",
                  "mFileTimeHi", 98);
  sf_mex_assign(&c3_rhs98, sf_mex_createcellmatrix(0, 1), false);
  sf_mex_assign(&c3_lhs98, sf_mex_createcellmatrix(0, 1), false);
  sf_mex_addfield(*c3_info, sf_mex_duplicatearraysafe(&c3_rhs98), "rhs", "rhs",
                  98);
  sf_mex_addfield(*c3_info, sf_mex_duplicatearraysafe(&c3_lhs98), "lhs", "lhs",
                  98);
  sf_mex_addfield(*c3_info, c3_emlrt_marshallOut(""), "context", "context", 99);
  sf_mex_addfield(*c3_info, c3_emlrt_marshallOut("intmax"), "name", "name", 99);
  sf_mex_addfield(*c3_info, c3_emlrt_marshallOut(""), "dominantType",
                  "dominantType", 99);
  sf_mex_addfield(*c3_info, c3_emlrt_marshallOut(
    "[ILXE]$matlabroot$/toolbox/eml/lib/matlab/elmat/intmax.m"), "resolved",
                  "resolved", 99);
  sf_mex_addfield(*c3_info, c3_b_emlrt_marshallOut(1362265482U), "fileTimeLo",
                  "fileTimeLo", 99);
  sf_mex_addfield(*c3_info, c3_b_emlrt_marshallOut(0U), "fileTimeHi",
                  "fileTimeHi", 99);
  sf_mex_addfield(*c3_info, c3_b_emlrt_marshallOut(0U), "mFileTimeLo",
                  "mFileTimeLo", 99);
  sf_mex_addfield(*c3_info, c3_b_emlrt_marshallOut(0U), "mFileTimeHi",
                  "mFileTimeHi", 99);
  sf_mex_assign(&c3_rhs99, sf_mex_createcellmatrix(0, 1), false);
  sf_mex_assign(&c3_lhs99, sf_mex_createcellmatrix(0, 1), false);
  sf_mex_addfield(*c3_info, sf_mex_duplicatearraysafe(&c3_rhs99), "rhs", "rhs",
                  99);
  sf_mex_addfield(*c3_info, sf_mex_duplicatearraysafe(&c3_lhs99), "lhs", "lhs",
                  99);
  sf_mex_addfield(*c3_info, c3_emlrt_marshallOut(""), "context", "context", 100);
  sf_mex_addfield(*c3_info, c3_emlrt_marshallOut("norm"), "name", "name", 100);
  sf_mex_addfield(*c3_info, c3_emlrt_marshallOut("single"), "dominantType",
                  "dominantType", 100);
  sf_mex_addfield(*c3_info, c3_emlrt_marshallOut(
    "[ILXE]$matlabroot$/toolbox/eml/lib/matlab/matfun/norm.m"), "resolved",
                  "resolved", 100);
  sf_mex_addfield(*c3_info, c3_b_emlrt_marshallOut(1363717468U), "fileTimeLo",
                  "fileTimeLo", 100);
  sf_mex_addfield(*c3_info, c3_b_emlrt_marshallOut(0U), "fileTimeHi",
                  "fileTimeHi", 100);
  sf_mex_addfield(*c3_info, c3_b_emlrt_marshallOut(0U), "mFileTimeLo",
                  "mFileTimeLo", 100);
  sf_mex_addfield(*c3_info, c3_b_emlrt_marshallOut(0U), "mFileTimeHi",
                  "mFileTimeHi", 100);
  sf_mex_assign(&c3_rhs100, sf_mex_createcellmatrix(0, 1), false);
  sf_mex_assign(&c3_lhs100, sf_mex_createcellmatrix(0, 1), false);
  sf_mex_addfield(*c3_info, sf_mex_duplicatearraysafe(&c3_rhs100), "rhs", "rhs",
                  100);
  sf_mex_addfield(*c3_info, sf_mex_duplicatearraysafe(&c3_lhs100), "lhs", "lhs",
                  100);
  sf_mex_addfield(*c3_info, c3_emlrt_marshallOut(
    "[ILXE]$matlabroot$/toolbox/eml/lib/matlab/matfun/norm.m!genpnorm"),
                  "context", "context", 101);
  sf_mex_addfield(*c3_info, c3_emlrt_marshallOut("eml_index_class"), "name",
                  "name", 101);
  sf_mex_addfield(*c3_info, c3_emlrt_marshallOut(""), "dominantType",
                  "dominantType", 101);
  sf_mex_addfield(*c3_info, c3_emlrt_marshallOut(
    "[ILXE]$matlabroot$/toolbox/eml/lib/matlab/eml/eml_index_class.m"),
                  "resolved", "resolved", 101);
  sf_mex_addfield(*c3_info, c3_b_emlrt_marshallOut(1323174178U), "fileTimeLo",
                  "fileTimeLo", 101);
  sf_mex_addfield(*c3_info, c3_b_emlrt_marshallOut(0U), "fileTimeHi",
                  "fileTimeHi", 101);
  sf_mex_addfield(*c3_info, c3_b_emlrt_marshallOut(0U), "mFileTimeLo",
                  "mFileTimeLo", 101);
  sf_mex_addfield(*c3_info, c3_b_emlrt_marshallOut(0U), "mFileTimeHi",
                  "mFileTimeHi", 101);
  sf_mex_assign(&c3_rhs101, sf_mex_createcellmatrix(0, 1), false);
  sf_mex_assign(&c3_lhs101, sf_mex_createcellmatrix(0, 1), false);
  sf_mex_addfield(*c3_info, sf_mex_duplicatearraysafe(&c3_rhs101), "rhs", "rhs",
                  101);
  sf_mex_addfield(*c3_info, sf_mex_duplicatearraysafe(&c3_lhs101), "lhs", "lhs",
                  101);
  sf_mex_addfield(*c3_info, c3_emlrt_marshallOut(
    "[ILXE]$matlabroot$/toolbox/eml/lib/matlab/matfun/norm.m!genpnorm"),
                  "context", "context", 102);
  sf_mex_addfield(*c3_info, c3_emlrt_marshallOut(
    "coder.internal.isBuiltInNumeric"), "name", "name", 102);
  sf_mex_addfield(*c3_info, c3_emlrt_marshallOut("double"), "dominantType",
                  "dominantType", 102);
  sf_mex_addfield(*c3_info, c3_emlrt_marshallOut(
    "[ILXE]$matlabroot$/toolbox/shared/coder/coder/+coder/+internal/isBuiltInNumeric.m"),
                  "resolved", "resolved", 102);
  sf_mex_addfield(*c3_info, c3_b_emlrt_marshallOut(1395935456U), "fileTimeLo",
                  "fileTimeLo", 102);
  sf_mex_addfield(*c3_info, c3_b_emlrt_marshallOut(0U), "fileTimeHi",
                  "fileTimeHi", 102);
  sf_mex_addfield(*c3_info, c3_b_emlrt_marshallOut(0U), "mFileTimeLo",
                  "mFileTimeLo", 102);
  sf_mex_addfield(*c3_info, c3_b_emlrt_marshallOut(0U), "mFileTimeHi",
                  "mFileTimeHi", 102);
  sf_mex_assign(&c3_rhs102, sf_mex_createcellmatrix(0, 1), false);
  sf_mex_assign(&c3_lhs102, sf_mex_createcellmatrix(0, 1), false);
  sf_mex_addfield(*c3_info, sf_mex_duplicatearraysafe(&c3_rhs102), "rhs", "rhs",
                  102);
  sf_mex_addfield(*c3_info, sf_mex_duplicatearraysafe(&c3_lhs102), "lhs", "lhs",
                  102);
  sf_mex_addfield(*c3_info, c3_emlrt_marshallOut(
    "[ILXE]$matlabroot$/toolbox/eml/lib/matlab/matfun/norm.m!genpnorm"),
                  "context", "context", 103);
  sf_mex_addfield(*c3_info, c3_emlrt_marshallOut("eml_xnrm2"), "name", "name",
                  103);
  sf_mex_addfield(*c3_info, c3_emlrt_marshallOut("single"), "dominantType",
                  "dominantType", 103);
  sf_mex_addfield(*c3_info, c3_emlrt_marshallOut(
    "[ILXE]$matlabroot$/toolbox/eml/lib/matlab/eml/blas/eml_xnrm2.m"),
                  "resolved", "resolved", 103);
  sf_mex_addfield(*c3_info, c3_b_emlrt_marshallOut(1375984292U), "fileTimeLo",
                  "fileTimeLo", 103);
  sf_mex_addfield(*c3_info, c3_b_emlrt_marshallOut(0U), "fileTimeHi",
                  "fileTimeHi", 103);
  sf_mex_addfield(*c3_info, c3_b_emlrt_marshallOut(0U), "mFileTimeLo",
                  "mFileTimeLo", 103);
  sf_mex_addfield(*c3_info, c3_b_emlrt_marshallOut(0U), "mFileTimeHi",
                  "mFileTimeHi", 103);
  sf_mex_assign(&c3_rhs103, sf_mex_createcellmatrix(0, 1), false);
  sf_mex_assign(&c3_lhs103, sf_mex_createcellmatrix(0, 1), false);
  sf_mex_addfield(*c3_info, sf_mex_duplicatearraysafe(&c3_rhs103), "rhs", "rhs",
                  103);
  sf_mex_addfield(*c3_info, sf_mex_duplicatearraysafe(&c3_lhs103), "lhs", "lhs",
                  103);
  sf_mex_addfield(*c3_info, c3_emlrt_marshallOut(
    "[ILXE]$matlabroot$/toolbox/eml/lib/matlab/eml/blas/eml_xnrm2.m"), "context",
                  "context", 104);
  sf_mex_addfield(*c3_info, c3_emlrt_marshallOut("coder.internal.blas.inline"),
                  "name", "name", 104);
  sf_mex_addfield(*c3_info, c3_emlrt_marshallOut(""), "dominantType",
                  "dominantType", 104);
  sf_mex_addfield(*c3_info, c3_emlrt_marshallOut(
    "[ILXE]$matlabroot$/toolbox/coder/coder/+coder/+internal/+blas/inline.p"),
                  "resolved", "resolved", 104);
  sf_mex_addfield(*c3_info, c3_b_emlrt_marshallOut(1410811372U), "fileTimeLo",
                  "fileTimeLo", 104);
  sf_mex_addfield(*c3_info, c3_b_emlrt_marshallOut(0U), "fileTimeHi",
                  "fileTimeHi", 104);
  sf_mex_addfield(*c3_info, c3_b_emlrt_marshallOut(0U), "mFileTimeLo",
                  "mFileTimeLo", 104);
  sf_mex_addfield(*c3_info, c3_b_emlrt_marshallOut(0U), "mFileTimeHi",
                  "mFileTimeHi", 104);
  sf_mex_assign(&c3_rhs104, sf_mex_createcellmatrix(0, 1), false);
  sf_mex_assign(&c3_lhs104, sf_mex_createcellmatrix(0, 1), false);
  sf_mex_addfield(*c3_info, sf_mex_duplicatearraysafe(&c3_rhs104), "rhs", "rhs",
                  104);
  sf_mex_addfield(*c3_info, sf_mex_duplicatearraysafe(&c3_lhs104), "lhs", "lhs",
                  104);
  sf_mex_addfield(*c3_info, c3_emlrt_marshallOut(
    "[ILXE]$matlabroot$/toolbox/eml/lib/matlab/eml/blas/eml_xnrm2.m"), "context",
                  "context", 105);
  sf_mex_addfield(*c3_info, c3_emlrt_marshallOut("coder.internal.blas.xnrm2"),
                  "name", "name", 105);
  sf_mex_addfield(*c3_info, c3_emlrt_marshallOut("single"), "dominantType",
                  "dominantType", 105);
  sf_mex_addfield(*c3_info, c3_emlrt_marshallOut(
    "[ILXE]$matlabroot$/toolbox/coder/coder/+coder/+internal/+blas/xnrm2.p"),
                  "resolved", "resolved", 105);
  sf_mex_addfield(*c3_info, c3_b_emlrt_marshallOut(1410811370U), "fileTimeLo",
                  "fileTimeLo", 105);
  sf_mex_addfield(*c3_info, c3_b_emlrt_marshallOut(0U), "fileTimeHi",
                  "fileTimeHi", 105);
  sf_mex_addfield(*c3_info, c3_b_emlrt_marshallOut(0U), "mFileTimeLo",
                  "mFileTimeLo", 105);
  sf_mex_addfield(*c3_info, c3_b_emlrt_marshallOut(0U), "mFileTimeHi",
                  "mFileTimeHi", 105);
  sf_mex_assign(&c3_rhs105, sf_mex_createcellmatrix(0, 1), false);
  sf_mex_assign(&c3_lhs105, sf_mex_createcellmatrix(0, 1), false);
  sf_mex_addfield(*c3_info, sf_mex_duplicatearraysafe(&c3_rhs105), "rhs", "rhs",
                  105);
  sf_mex_addfield(*c3_info, sf_mex_duplicatearraysafe(&c3_lhs105), "lhs", "lhs",
                  105);
  sf_mex_addfield(*c3_info, c3_emlrt_marshallOut(
    "[ILXE]$matlabroot$/toolbox/coder/coder/+coder/+internal/+blas/xnrm2.p"),
                  "context", "context", 106);
  sf_mex_addfield(*c3_info, c3_emlrt_marshallOut(
    "coder.internal.blas.use_refblas"), "name", "name", 106);
  sf_mex_addfield(*c3_info, c3_emlrt_marshallOut(""), "dominantType",
                  "dominantType", 106);
  sf_mex_addfield(*c3_info, c3_emlrt_marshallOut(
    "[ILXE]$matlabroot$/toolbox/coder/coder/+coder/+internal/+blas/use_refblas.p"),
                  "resolved", "resolved", 106);
  sf_mex_addfield(*c3_info, c3_b_emlrt_marshallOut(1410811370U), "fileTimeLo",
                  "fileTimeLo", 106);
  sf_mex_addfield(*c3_info, c3_b_emlrt_marshallOut(0U), "fileTimeHi",
                  "fileTimeHi", 106);
  sf_mex_addfield(*c3_info, c3_b_emlrt_marshallOut(0U), "mFileTimeLo",
                  "mFileTimeLo", 106);
  sf_mex_addfield(*c3_info, c3_b_emlrt_marshallOut(0U), "mFileTimeHi",
                  "mFileTimeHi", 106);
  sf_mex_assign(&c3_rhs106, sf_mex_createcellmatrix(0, 1), false);
  sf_mex_assign(&c3_lhs106, sf_mex_createcellmatrix(0, 1), false);
  sf_mex_addfield(*c3_info, sf_mex_duplicatearraysafe(&c3_rhs106), "rhs", "rhs",
                  106);
  sf_mex_addfield(*c3_info, sf_mex_duplicatearraysafe(&c3_lhs106), "lhs", "lhs",
                  106);
  sf_mex_addfield(*c3_info, c3_emlrt_marshallOut(
    "[ILXE]$matlabroot$/toolbox/coder/coder/+coder/+internal/+blas/xnrm2.p!below_threshold"),
                  "context", "context", 107);
  sf_mex_addfield(*c3_info, c3_emlrt_marshallOut("coder.internal.blas.threshold"),
                  "name", "name", 107);
  sf_mex_addfield(*c3_info, c3_emlrt_marshallOut("char"), "dominantType",
                  "dominantType", 107);
  sf_mex_addfield(*c3_info, c3_emlrt_marshallOut(
    "[ILXE]$matlabroot$/toolbox/coder/coder/+coder/+internal/+blas/threshold.p"),
                  "resolved", "resolved", 107);
  sf_mex_addfield(*c3_info, c3_b_emlrt_marshallOut(1410811372U), "fileTimeLo",
                  "fileTimeLo", 107);
  sf_mex_addfield(*c3_info, c3_b_emlrt_marshallOut(0U), "fileTimeHi",
                  "fileTimeHi", 107);
  sf_mex_addfield(*c3_info, c3_b_emlrt_marshallOut(0U), "mFileTimeLo",
                  "mFileTimeLo", 107);
  sf_mex_addfield(*c3_info, c3_b_emlrt_marshallOut(0U), "mFileTimeHi",
                  "mFileTimeHi", 107);
  sf_mex_assign(&c3_rhs107, sf_mex_createcellmatrix(0, 1), false);
  sf_mex_assign(&c3_lhs107, sf_mex_createcellmatrix(0, 1), false);
  sf_mex_addfield(*c3_info, sf_mex_duplicatearraysafe(&c3_rhs107), "rhs", "rhs",
                  107);
  sf_mex_addfield(*c3_info, sf_mex_duplicatearraysafe(&c3_lhs107), "lhs", "lhs",
                  107);
  sf_mex_addfield(*c3_info, c3_emlrt_marshallOut(
    "[ILXE]$matlabroot$/toolbox/coder/coder/+coder/+internal/+blas/threshold.p"),
                  "context", "context", 108);
  sf_mex_addfield(*c3_info, c3_emlrt_marshallOut("eml_switch_helper"), "name",
                  "name", 108);
  sf_mex_addfield(*c3_info, c3_emlrt_marshallOut(""), "dominantType",
                  "dominantType", 108);
  sf_mex_addfield(*c3_info, c3_emlrt_marshallOut(
    "[ILXE]$matlabroot$/toolbox/eml/lib/matlab/eml/eml_switch_helper.m"),
                  "resolved", "resolved", 108);
  sf_mex_addfield(*c3_info, c3_b_emlrt_marshallOut(1393334458U), "fileTimeLo",
                  "fileTimeLo", 108);
  sf_mex_addfield(*c3_info, c3_b_emlrt_marshallOut(0U), "fileTimeHi",
                  "fileTimeHi", 108);
  sf_mex_addfield(*c3_info, c3_b_emlrt_marshallOut(0U), "mFileTimeLo",
                  "mFileTimeLo", 108);
  sf_mex_addfield(*c3_info, c3_b_emlrt_marshallOut(0U), "mFileTimeHi",
                  "mFileTimeHi", 108);
  sf_mex_assign(&c3_rhs108, sf_mex_createcellmatrix(0, 1), false);
  sf_mex_assign(&c3_lhs108, sf_mex_createcellmatrix(0, 1), false);
  sf_mex_addfield(*c3_info, sf_mex_duplicatearraysafe(&c3_rhs108), "rhs", "rhs",
                  108);
  sf_mex_addfield(*c3_info, sf_mex_duplicatearraysafe(&c3_lhs108), "lhs", "lhs",
                  108);
  sf_mex_addfield(*c3_info, c3_emlrt_marshallOut(
    "[ILXE]$matlabroot$/toolbox/coder/coder/+coder/+internal/+blas/xnrm2.p"),
                  "context", "context", 109);
  sf_mex_addfield(*c3_info, c3_emlrt_marshallOut("coder.internal.refblas.xnrm2"),
                  "name", "name", 109);
  sf_mex_addfield(*c3_info, c3_emlrt_marshallOut("single"), "dominantType",
                  "dominantType", 109);
  sf_mex_addfield(*c3_info, c3_emlrt_marshallOut(
    "[ILXE]$matlabroot$/toolbox/coder/coder/+coder/+internal/+refblas/xnrm2.p"),
                  "resolved", "resolved", 109);
  sf_mex_addfield(*c3_info, c3_b_emlrt_marshallOut(1410811372U), "fileTimeLo",
                  "fileTimeLo", 109);
  sf_mex_addfield(*c3_info, c3_b_emlrt_marshallOut(0U), "fileTimeHi",
                  "fileTimeHi", 109);
  sf_mex_addfield(*c3_info, c3_b_emlrt_marshallOut(0U), "mFileTimeLo",
                  "mFileTimeLo", 109);
  sf_mex_addfield(*c3_info, c3_b_emlrt_marshallOut(0U), "mFileTimeHi",
                  "mFileTimeHi", 109);
  sf_mex_assign(&c3_rhs109, sf_mex_createcellmatrix(0, 1), false);
  sf_mex_assign(&c3_lhs109, sf_mex_createcellmatrix(0, 1), false);
  sf_mex_addfield(*c3_info, sf_mex_duplicatearraysafe(&c3_rhs109), "rhs", "rhs",
                  109);
  sf_mex_addfield(*c3_info, sf_mex_duplicatearraysafe(&c3_lhs109), "lhs", "lhs",
                  109);
  sf_mex_addfield(*c3_info, c3_emlrt_marshallOut(
    "[ILXE]$matlabroot$/toolbox/coder/coder/+coder/+internal/+refblas/xnrm2.p"),
                  "context", "context", 110);
  sf_mex_addfield(*c3_info, c3_emlrt_marshallOut("realmin"), "name", "name", 110);
  sf_mex_addfield(*c3_info, c3_emlrt_marshallOut("char"), "dominantType",
                  "dominantType", 110);
  sf_mex_addfield(*c3_info, c3_emlrt_marshallOut(
    "[ILXE]$matlabroot$/toolbox/eml/lib/matlab/elmat/realmin.m"), "resolved",
                  "resolved", 110);
  sf_mex_addfield(*c3_info, c3_b_emlrt_marshallOut(1307654842U), "fileTimeLo",
                  "fileTimeLo", 110);
  sf_mex_addfield(*c3_info, c3_b_emlrt_marshallOut(0U), "fileTimeHi",
                  "fileTimeHi", 110);
  sf_mex_addfield(*c3_info, c3_b_emlrt_marshallOut(0U), "mFileTimeLo",
                  "mFileTimeLo", 110);
  sf_mex_addfield(*c3_info, c3_b_emlrt_marshallOut(0U), "mFileTimeHi",
                  "mFileTimeHi", 110);
  sf_mex_assign(&c3_rhs110, sf_mex_createcellmatrix(0, 1), false);
  sf_mex_assign(&c3_lhs110, sf_mex_createcellmatrix(0, 1), false);
  sf_mex_addfield(*c3_info, sf_mex_duplicatearraysafe(&c3_rhs110), "rhs", "rhs",
                  110);
  sf_mex_addfield(*c3_info, sf_mex_duplicatearraysafe(&c3_lhs110), "lhs", "lhs",
                  110);
  sf_mex_addfield(*c3_info, c3_emlrt_marshallOut(
    "[ILXE]$matlabroot$/toolbox/eml/lib/matlab/elmat/realmin.m"), "context",
                  "context", 111);
  sf_mex_addfield(*c3_info, c3_emlrt_marshallOut("eml_realmin"), "name", "name",
                  111);
  sf_mex_addfield(*c3_info, c3_emlrt_marshallOut("char"), "dominantType",
                  "dominantType", 111);
  sf_mex_addfield(*c3_info, c3_emlrt_marshallOut(
    "[ILXE]$matlabroot$/toolbox/eml/lib/matlab/eml/eml_realmin.m"), "resolved",
                  "resolved", 111);
  sf_mex_addfield(*c3_info, c3_b_emlrt_marshallOut(1307654844U), "fileTimeLo",
                  "fileTimeLo", 111);
  sf_mex_addfield(*c3_info, c3_b_emlrt_marshallOut(0U), "fileTimeHi",
                  "fileTimeHi", 111);
  sf_mex_addfield(*c3_info, c3_b_emlrt_marshallOut(0U), "mFileTimeLo",
                  "mFileTimeLo", 111);
  sf_mex_addfield(*c3_info, c3_b_emlrt_marshallOut(0U), "mFileTimeHi",
                  "mFileTimeHi", 111);
  sf_mex_assign(&c3_rhs111, sf_mex_createcellmatrix(0, 1), false);
  sf_mex_assign(&c3_lhs111, sf_mex_createcellmatrix(0, 1), false);
  sf_mex_addfield(*c3_info, sf_mex_duplicatearraysafe(&c3_rhs111), "rhs", "rhs",
                  111);
  sf_mex_addfield(*c3_info, sf_mex_duplicatearraysafe(&c3_lhs111), "lhs", "lhs",
                  111);
  sf_mex_addfield(*c3_info, c3_emlrt_marshallOut(
    "[ILXE]$matlabroot$/toolbox/eml/lib/matlab/eml/eml_realmin.m"), "context",
                  "context", 112);
  sf_mex_addfield(*c3_info, c3_emlrt_marshallOut("eml_float_model"), "name",
                  "name", 112);
  sf_mex_addfield(*c3_info, c3_emlrt_marshallOut("char"), "dominantType",
                  "dominantType", 112);
  sf_mex_addfield(*c3_info, c3_emlrt_marshallOut(
    "[ILXE]$matlabroot$/toolbox/eml/lib/matlab/eml/eml_float_model.m"),
                  "resolved", "resolved", 112);
  sf_mex_addfield(*c3_info, c3_b_emlrt_marshallOut(1326731596U), "fileTimeLo",
                  "fileTimeLo", 112);
  sf_mex_addfield(*c3_info, c3_b_emlrt_marshallOut(0U), "fileTimeHi",
                  "fileTimeHi", 112);
  sf_mex_addfield(*c3_info, c3_b_emlrt_marshallOut(0U), "mFileTimeLo",
                  "mFileTimeLo", 112);
  sf_mex_addfield(*c3_info, c3_b_emlrt_marshallOut(0U), "mFileTimeHi",
                  "mFileTimeHi", 112);
  sf_mex_assign(&c3_rhs112, sf_mex_createcellmatrix(0, 1), false);
  sf_mex_assign(&c3_lhs112, sf_mex_createcellmatrix(0, 1), false);
  sf_mex_addfield(*c3_info, sf_mex_duplicatearraysafe(&c3_rhs112), "rhs", "rhs",
                  112);
  sf_mex_addfield(*c3_info, sf_mex_duplicatearraysafe(&c3_lhs112), "lhs", "lhs",
                  112);
  sf_mex_addfield(*c3_info, c3_emlrt_marshallOut(
    "[ILXE]$matlabroot$/toolbox/coder/coder/+coder/+internal/+refblas/xnrm2.p"),
                  "context", "context", 113);
  sf_mex_addfield(*c3_info, c3_emlrt_marshallOut("coder.internal.indexMinus"),
                  "name", "name", 113);
  sf_mex_addfield(*c3_info, c3_emlrt_marshallOut("double"), "dominantType",
                  "dominantType", 113);
  sf_mex_addfield(*c3_info, c3_emlrt_marshallOut(
    "[ILXE]$matlabroot$/toolbox/shared/coder/coder/+coder/+internal/indexMinus.m"),
                  "resolved", "resolved", 113);
  sf_mex_addfield(*c3_info, c3_b_emlrt_marshallOut(1372586760U), "fileTimeLo",
                  "fileTimeLo", 113);
  sf_mex_addfield(*c3_info, c3_b_emlrt_marshallOut(0U), "fileTimeHi",
                  "fileTimeHi", 113);
  sf_mex_addfield(*c3_info, c3_b_emlrt_marshallOut(0U), "mFileTimeLo",
                  "mFileTimeLo", 113);
  sf_mex_addfield(*c3_info, c3_b_emlrt_marshallOut(0U), "mFileTimeHi",
                  "mFileTimeHi", 113);
  sf_mex_assign(&c3_rhs113, sf_mex_createcellmatrix(0, 1), false);
  sf_mex_assign(&c3_lhs113, sf_mex_createcellmatrix(0, 1), false);
  sf_mex_addfield(*c3_info, sf_mex_duplicatearraysafe(&c3_rhs113), "rhs", "rhs",
                  113);
  sf_mex_addfield(*c3_info, sf_mex_duplicatearraysafe(&c3_lhs113), "lhs", "lhs",
                  113);
  sf_mex_addfield(*c3_info, c3_emlrt_marshallOut(
    "[ILXE]$matlabroot$/toolbox/coder/coder/+coder/+internal/+refblas/xnrm2.p"),
                  "context", "context", 114);
  sf_mex_addfield(*c3_info, c3_emlrt_marshallOut("coder.internal.indexTimes"),
                  "name", "name", 114);
  sf_mex_addfield(*c3_info, c3_emlrt_marshallOut("coder.internal.indexInt"),
                  "dominantType", "dominantType", 114);
  sf_mex_addfield(*c3_info, c3_emlrt_marshallOut(
    "[ILXE]$matlabroot$/toolbox/shared/coder/coder/+coder/+internal/indexTimes.m"),
                  "resolved", "resolved", 114);
  sf_mex_addfield(*c3_info, c3_b_emlrt_marshallOut(1372586760U), "fileTimeLo",
                  "fileTimeLo", 114);
  sf_mex_addfield(*c3_info, c3_b_emlrt_marshallOut(0U), "fileTimeHi",
                  "fileTimeHi", 114);
  sf_mex_addfield(*c3_info, c3_b_emlrt_marshallOut(0U), "mFileTimeLo",
                  "mFileTimeLo", 114);
  sf_mex_addfield(*c3_info, c3_b_emlrt_marshallOut(0U), "mFileTimeHi",
                  "mFileTimeHi", 114);
  sf_mex_assign(&c3_rhs114, sf_mex_createcellmatrix(0, 1), false);
  sf_mex_assign(&c3_lhs114, sf_mex_createcellmatrix(0, 1), false);
  sf_mex_addfield(*c3_info, sf_mex_duplicatearraysafe(&c3_rhs114), "rhs", "rhs",
                  114);
  sf_mex_addfield(*c3_info, sf_mex_duplicatearraysafe(&c3_lhs114), "lhs", "lhs",
                  114);
  sf_mex_addfield(*c3_info, c3_emlrt_marshallOut(
    "[ILXE]$matlabroot$/toolbox/coder/coder/+coder/+internal/+refblas/xnrm2.p"),
                  "context", "context", 115);
  sf_mex_addfield(*c3_info, c3_emlrt_marshallOut("coder.internal.indexPlus"),
                  "name", "name", 115);
  sf_mex_addfield(*c3_info, c3_emlrt_marshallOut("coder.internal.indexInt"),
                  "dominantType", "dominantType", 115);
  sf_mex_addfield(*c3_info, c3_emlrt_marshallOut(
    "[ILXE]$matlabroot$/toolbox/shared/coder/coder/+coder/+internal/indexPlus.m"),
                  "resolved", "resolved", 115);
  sf_mex_addfield(*c3_info, c3_b_emlrt_marshallOut(1372586760U), "fileTimeLo",
                  "fileTimeLo", 115);
  sf_mex_addfield(*c3_info, c3_b_emlrt_marshallOut(0U), "fileTimeHi",
                  "fileTimeHi", 115);
  sf_mex_addfield(*c3_info, c3_b_emlrt_marshallOut(0U), "mFileTimeLo",
                  "mFileTimeLo", 115);
  sf_mex_addfield(*c3_info, c3_b_emlrt_marshallOut(0U), "mFileTimeHi",
                  "mFileTimeHi", 115);
  sf_mex_assign(&c3_rhs115, sf_mex_createcellmatrix(0, 1), false);
  sf_mex_assign(&c3_lhs115, sf_mex_createcellmatrix(0, 1), false);
  sf_mex_addfield(*c3_info, sf_mex_duplicatearraysafe(&c3_rhs115), "rhs", "rhs",
                  115);
  sf_mex_addfield(*c3_info, sf_mex_duplicatearraysafe(&c3_lhs115), "lhs", "lhs",
                  115);
  sf_mex_addfield(*c3_info, c3_emlrt_marshallOut(
    "[ILXE]$matlabroot$/toolbox/coder/coder/+coder/+internal/+refblas/xnrm2.p"),
                  "context", "context", 116);
  sf_mex_addfield(*c3_info, c3_emlrt_marshallOut(
    "eml_int_forloop_overflow_check"), "name", "name", 116);
  sf_mex_addfield(*c3_info, c3_emlrt_marshallOut(""), "dominantType",
                  "dominantType", 116);
  sf_mex_addfield(*c3_info, c3_emlrt_marshallOut(
    "[ILXE]$matlabroot$/toolbox/eml/lib/matlab/eml/eml_int_forloop_overflow_check.m"),
                  "resolved", "resolved", 116);
  sf_mex_addfield(*c3_info, c3_b_emlrt_marshallOut(1397261022U), "fileTimeLo",
                  "fileTimeLo", 116);
  sf_mex_addfield(*c3_info, c3_b_emlrt_marshallOut(0U), "fileTimeHi",
                  "fileTimeHi", 116);
  sf_mex_addfield(*c3_info, c3_b_emlrt_marshallOut(0U), "mFileTimeLo",
                  "mFileTimeLo", 116);
  sf_mex_addfield(*c3_info, c3_b_emlrt_marshallOut(0U), "mFileTimeHi",
                  "mFileTimeHi", 116);
  sf_mex_assign(&c3_rhs116, sf_mex_createcellmatrix(0, 1), false);
  sf_mex_assign(&c3_lhs116, sf_mex_createcellmatrix(0, 1), false);
  sf_mex_addfield(*c3_info, sf_mex_duplicatearraysafe(&c3_rhs116), "rhs", "rhs",
                  116);
  sf_mex_addfield(*c3_info, sf_mex_duplicatearraysafe(&c3_lhs116), "lhs", "lhs",
                  116);
  sf_mex_addfield(*c3_info, c3_emlrt_marshallOut(
    "[ILXE]$matlabroot$/toolbox/coder/coder/+coder/+internal/+refblas/xnrm2.p"),
                  "context", "context", 117);
  sf_mex_addfield(*c3_info, c3_emlrt_marshallOut("abs"), "name", "name", 117);
  sf_mex_addfield(*c3_info, c3_emlrt_marshallOut("single"), "dominantType",
                  "dominantType", 117);
  sf_mex_addfield(*c3_info, c3_emlrt_marshallOut(
    "[ILXE]$matlabroot$/toolbox/eml/lib/matlab/elfun/abs.m"), "resolved",
                  "resolved", 117);
  sf_mex_addfield(*c3_info, c3_b_emlrt_marshallOut(1363717452U), "fileTimeLo",
                  "fileTimeLo", 117);
  sf_mex_addfield(*c3_info, c3_b_emlrt_marshallOut(0U), "fileTimeHi",
                  "fileTimeHi", 117);
  sf_mex_addfield(*c3_info, c3_b_emlrt_marshallOut(0U), "mFileTimeLo",
                  "mFileTimeLo", 117);
  sf_mex_addfield(*c3_info, c3_b_emlrt_marshallOut(0U), "mFileTimeHi",
                  "mFileTimeHi", 117);
  sf_mex_assign(&c3_rhs117, sf_mex_createcellmatrix(0, 1), false);
  sf_mex_assign(&c3_lhs117, sf_mex_createcellmatrix(0, 1), false);
  sf_mex_addfield(*c3_info, sf_mex_duplicatearraysafe(&c3_rhs117), "rhs", "rhs",
                  117);
  sf_mex_addfield(*c3_info, sf_mex_duplicatearraysafe(&c3_lhs117), "lhs", "lhs",
                  117);
  sf_mex_addfield(*c3_info, c3_emlrt_marshallOut(""), "context", "context", 118);
  sf_mex_addfield(*c3_info, c3_emlrt_marshallOut("atan2d"), "name", "name", 118);
  sf_mex_addfield(*c3_info, c3_emlrt_marshallOut("single"), "dominantType",
                  "dominantType", 118);
  sf_mex_addfield(*c3_info, c3_emlrt_marshallOut(
    "[ILXE]$matlabroot$/toolbox/eml/lib/matlab/elfun/atan2d.m"), "resolved",
                  "resolved", 118);
  sf_mex_addfield(*c3_info, c3_b_emlrt_marshallOut(1395332096U), "fileTimeLo",
                  "fileTimeLo", 118);
  sf_mex_addfield(*c3_info, c3_b_emlrt_marshallOut(0U), "fileTimeHi",
                  "fileTimeHi", 118);
  sf_mex_addfield(*c3_info, c3_b_emlrt_marshallOut(0U), "mFileTimeLo",
                  "mFileTimeLo", 118);
  sf_mex_addfield(*c3_info, c3_b_emlrt_marshallOut(0U), "mFileTimeHi",
                  "mFileTimeHi", 118);
  sf_mex_assign(&c3_rhs118, sf_mex_createcellmatrix(0, 1), false);
  sf_mex_assign(&c3_lhs118, sf_mex_createcellmatrix(0, 1), false);
  sf_mex_addfield(*c3_info, sf_mex_duplicatearraysafe(&c3_rhs118), "rhs", "rhs",
                  118);
  sf_mex_addfield(*c3_info, sf_mex_duplicatearraysafe(&c3_lhs118), "lhs", "lhs",
                  118);
  sf_mex_addfield(*c3_info, c3_emlrt_marshallOut(
    "[ILXE]$matlabroot$/toolbox/eml/lib/matlab/elfun/atan2d.m"), "context",
                  "context", 119);
  sf_mex_addfield(*c3_info, c3_emlrt_marshallOut("eml_scalar_eg"), "name",
                  "name", 119);
  sf_mex_addfield(*c3_info, c3_emlrt_marshallOut("single"), "dominantType",
                  "dominantType", 119);
  sf_mex_addfield(*c3_info, c3_emlrt_marshallOut(
    "[ILXE]$matlabroot$/toolbox/eml/lib/matlab/eml/eml_scalar_eg.m"), "resolved",
                  "resolved", 119);
  sf_mex_addfield(*c3_info, c3_b_emlrt_marshallOut(1375984288U), "fileTimeLo",
                  "fileTimeLo", 119);
  sf_mex_addfield(*c3_info, c3_b_emlrt_marshallOut(0U), "fileTimeHi",
                  "fileTimeHi", 119);
  sf_mex_addfield(*c3_info, c3_b_emlrt_marshallOut(0U), "mFileTimeLo",
                  "mFileTimeLo", 119);
  sf_mex_addfield(*c3_info, c3_b_emlrt_marshallOut(0U), "mFileTimeHi",
                  "mFileTimeHi", 119);
  sf_mex_assign(&c3_rhs119, sf_mex_createcellmatrix(0, 1), false);
  sf_mex_assign(&c3_lhs119, sf_mex_createcellmatrix(0, 1), false);
  sf_mex_addfield(*c3_info, sf_mex_duplicatearraysafe(&c3_rhs119), "rhs", "rhs",
                  119);
  sf_mex_addfield(*c3_info, sf_mex_duplicatearraysafe(&c3_lhs119), "lhs", "lhs",
                  119);
  sf_mex_addfield(*c3_info, c3_emlrt_marshallOut(
    "[ILXE]$matlabroot$/toolbox/eml/lib/matlab/elfun/atan2d.m"), "context",
                  "context", 120);
  sf_mex_addfield(*c3_info, c3_emlrt_marshallOut("eml_scalexp_alloc"), "name",
                  "name", 120);
  sf_mex_addfield(*c3_info, c3_emlrt_marshallOut("single"), "dominantType",
                  "dominantType", 120);
  sf_mex_addfield(*c3_info, c3_emlrt_marshallOut(
    "[ILXE]$matlabroot$/toolbox/eml/lib/matlab/eml/eml_scalexp_alloc.m"),
                  "resolved", "resolved", 120);
  sf_mex_addfield(*c3_info, c3_b_emlrt_marshallOut(1375984288U), "fileTimeLo",
                  "fileTimeLo", 120);
  sf_mex_addfield(*c3_info, c3_b_emlrt_marshallOut(0U), "fileTimeHi",
                  "fileTimeHi", 120);
  sf_mex_addfield(*c3_info, c3_b_emlrt_marshallOut(0U), "mFileTimeLo",
                  "mFileTimeLo", 120);
  sf_mex_addfield(*c3_info, c3_b_emlrt_marshallOut(0U), "mFileTimeHi",
                  "mFileTimeHi", 120);
  sf_mex_assign(&c3_rhs120, sf_mex_createcellmatrix(0, 1), false);
  sf_mex_assign(&c3_lhs120, sf_mex_createcellmatrix(0, 1), false);
  sf_mex_addfield(*c3_info, sf_mex_duplicatearraysafe(&c3_rhs120), "rhs", "rhs",
                  120);
  sf_mex_addfield(*c3_info, sf_mex_duplicatearraysafe(&c3_lhs120), "lhs", "lhs",
                  120);
  sf_mex_addfield(*c3_info, c3_emlrt_marshallOut(
    "[ILXE]$matlabroot$/toolbox/eml/lib/matlab/elfun/atan2d.m"), "context",
                  "context", 121);
  sf_mex_addfield(*c3_info, c3_emlrt_marshallOut("eml_scalar_atan2"), "name",
                  "name", 121);
  sf_mex_addfield(*c3_info, c3_emlrt_marshallOut("single"), "dominantType",
                  "dominantType", 121);
  sf_mex_addfield(*c3_info, c3_emlrt_marshallOut(
    "[ILXE]$matlabroot$/toolbox/eml/lib/matlab/elfun/eml_scalar_atan2.m"),
                  "resolved", "resolved", 121);
  sf_mex_addfield(*c3_info, c3_b_emlrt_marshallOut(1286822320U), "fileTimeLo",
                  "fileTimeLo", 121);
  sf_mex_addfield(*c3_info, c3_b_emlrt_marshallOut(0U), "fileTimeHi",
                  "fileTimeHi", 121);
  sf_mex_addfield(*c3_info, c3_b_emlrt_marshallOut(0U), "mFileTimeLo",
                  "mFileTimeLo", 121);
  sf_mex_addfield(*c3_info, c3_b_emlrt_marshallOut(0U), "mFileTimeHi",
                  "mFileTimeHi", 121);
  sf_mex_assign(&c3_rhs121, sf_mex_createcellmatrix(0, 1), false);
  sf_mex_assign(&c3_lhs121, sf_mex_createcellmatrix(0, 1), false);
  sf_mex_addfield(*c3_info, sf_mex_duplicatearraysafe(&c3_rhs121), "rhs", "rhs",
                  121);
  sf_mex_addfield(*c3_info, sf_mex_duplicatearraysafe(&c3_lhs121), "lhs", "lhs",
                  121);
  sf_mex_addfield(*c3_info, c3_emlrt_marshallOut(
    "[ILXE]$matlabroot$/toolbox/eml/lib/matlab/elfun/atan2d.m"), "context",
                  "context", 122);
  sf_mex_addfield(*c3_info, c3_emlrt_marshallOut("eml_mtimes_helper"), "name",
                  "name", 122);
  sf_mex_addfield(*c3_info, c3_emlrt_marshallOut(""), "dominantType",
                  "dominantType", 122);
  sf_mex_addfield(*c3_info, c3_emlrt_marshallOut(
    "[ILXE]$matlabroot$/toolbox/eml/lib/matlab/ops/eml_mtimes_helper.m"),
                  "resolved", "resolved", 122);
  sf_mex_addfield(*c3_info, c3_b_emlrt_marshallOut(1383880894U), "fileTimeLo",
                  "fileTimeLo", 122);
  sf_mex_addfield(*c3_info, c3_b_emlrt_marshallOut(0U), "fileTimeHi",
                  "fileTimeHi", 122);
  sf_mex_addfield(*c3_info, c3_b_emlrt_marshallOut(0U), "mFileTimeLo",
                  "mFileTimeLo", 122);
  sf_mex_addfield(*c3_info, c3_b_emlrt_marshallOut(0U), "mFileTimeHi",
                  "mFileTimeHi", 122);
  sf_mex_assign(&c3_rhs122, sf_mex_createcellmatrix(0, 1), false);
  sf_mex_assign(&c3_lhs122, sf_mex_createcellmatrix(0, 1), false);
  sf_mex_addfield(*c3_info, sf_mex_duplicatearraysafe(&c3_rhs122), "rhs", "rhs",
                  122);
  sf_mex_addfield(*c3_info, sf_mex_duplicatearraysafe(&c3_lhs122), "lhs", "lhs",
                  122);
  sf_mex_destroy(&c3_rhs64);
  sf_mex_destroy(&c3_lhs64);
  sf_mex_destroy(&c3_rhs65);
  sf_mex_destroy(&c3_lhs65);
  sf_mex_destroy(&c3_rhs66);
  sf_mex_destroy(&c3_lhs66);
  sf_mex_destroy(&c3_rhs67);
  sf_mex_destroy(&c3_lhs67);
  sf_mex_destroy(&c3_rhs68);
  sf_mex_destroy(&c3_lhs68);
  sf_mex_destroy(&c3_rhs69);
  sf_mex_destroy(&c3_lhs69);
  sf_mex_destroy(&c3_rhs70);
  sf_mex_destroy(&c3_lhs70);
  sf_mex_destroy(&c3_rhs71);
  sf_mex_destroy(&c3_lhs71);
  sf_mex_destroy(&c3_rhs72);
  sf_mex_destroy(&c3_lhs72);
  sf_mex_destroy(&c3_rhs73);
  sf_mex_destroy(&c3_lhs73);
  sf_mex_destroy(&c3_rhs74);
  sf_mex_destroy(&c3_lhs74);
  sf_mex_destroy(&c3_rhs75);
  sf_mex_destroy(&c3_lhs75);
  sf_mex_destroy(&c3_rhs76);
  sf_mex_destroy(&c3_lhs76);
  sf_mex_destroy(&c3_rhs77);
  sf_mex_destroy(&c3_lhs77);
  sf_mex_destroy(&c3_rhs78);
  sf_mex_destroy(&c3_lhs78);
  sf_mex_destroy(&c3_rhs79);
  sf_mex_destroy(&c3_lhs79);
  sf_mex_destroy(&c3_rhs80);
  sf_mex_destroy(&c3_lhs80);
  sf_mex_destroy(&c3_rhs81);
  sf_mex_destroy(&c3_lhs81);
  sf_mex_destroy(&c3_rhs82);
  sf_mex_destroy(&c3_lhs82);
  sf_mex_destroy(&c3_rhs83);
  sf_mex_destroy(&c3_lhs83);
  sf_mex_destroy(&c3_rhs84);
  sf_mex_destroy(&c3_lhs84);
  sf_mex_destroy(&c3_rhs85);
  sf_mex_destroy(&c3_lhs85);
  sf_mex_destroy(&c3_rhs86);
  sf_mex_destroy(&c3_lhs86);
  sf_mex_destroy(&c3_rhs87);
  sf_mex_destroy(&c3_lhs87);
  sf_mex_destroy(&c3_rhs88);
  sf_mex_destroy(&c3_lhs88);
  sf_mex_destroy(&c3_rhs89);
  sf_mex_destroy(&c3_lhs89);
  sf_mex_destroy(&c3_rhs90);
  sf_mex_destroy(&c3_lhs90);
  sf_mex_destroy(&c3_rhs91);
  sf_mex_destroy(&c3_lhs91);
  sf_mex_destroy(&c3_rhs92);
  sf_mex_destroy(&c3_lhs92);
  sf_mex_destroy(&c3_rhs93);
  sf_mex_destroy(&c3_lhs93);
  sf_mex_destroy(&c3_rhs94);
  sf_mex_destroy(&c3_lhs94);
  sf_mex_destroy(&c3_rhs95);
  sf_mex_destroy(&c3_lhs95);
  sf_mex_destroy(&c3_rhs96);
  sf_mex_destroy(&c3_lhs96);
  sf_mex_destroy(&c3_rhs97);
  sf_mex_destroy(&c3_lhs97);
  sf_mex_destroy(&c3_rhs98);
  sf_mex_destroy(&c3_lhs98);
  sf_mex_destroy(&c3_rhs99);
  sf_mex_destroy(&c3_lhs99);
  sf_mex_destroy(&c3_rhs100);
  sf_mex_destroy(&c3_lhs100);
  sf_mex_destroy(&c3_rhs101);
  sf_mex_destroy(&c3_lhs101);
  sf_mex_destroy(&c3_rhs102);
  sf_mex_destroy(&c3_lhs102);
  sf_mex_destroy(&c3_rhs103);
  sf_mex_destroy(&c3_lhs103);
  sf_mex_destroy(&c3_rhs104);
  sf_mex_destroy(&c3_lhs104);
  sf_mex_destroy(&c3_rhs105);
  sf_mex_destroy(&c3_lhs105);
  sf_mex_destroy(&c3_rhs106);
  sf_mex_destroy(&c3_lhs106);
  sf_mex_destroy(&c3_rhs107);
  sf_mex_destroy(&c3_lhs107);
  sf_mex_destroy(&c3_rhs108);
  sf_mex_destroy(&c3_lhs108);
  sf_mex_destroy(&c3_rhs109);
  sf_mex_destroy(&c3_lhs109);
  sf_mex_destroy(&c3_rhs110);
  sf_mex_destroy(&c3_lhs110);
  sf_mex_destroy(&c3_rhs111);
  sf_mex_destroy(&c3_lhs111);
  sf_mex_destroy(&c3_rhs112);
  sf_mex_destroy(&c3_lhs112);
  sf_mex_destroy(&c3_rhs113);
  sf_mex_destroy(&c3_lhs113);
  sf_mex_destroy(&c3_rhs114);
  sf_mex_destroy(&c3_lhs114);
  sf_mex_destroy(&c3_rhs115);
  sf_mex_destroy(&c3_lhs115);
  sf_mex_destroy(&c3_rhs116);
  sf_mex_destroy(&c3_lhs116);
  sf_mex_destroy(&c3_rhs117);
  sf_mex_destroy(&c3_lhs117);
  sf_mex_destroy(&c3_rhs118);
  sf_mex_destroy(&c3_lhs118);
  sf_mex_destroy(&c3_rhs119);
  sf_mex_destroy(&c3_lhs119);
  sf_mex_destroy(&c3_rhs120);
  sf_mex_destroy(&c3_lhs120);
  sf_mex_destroy(&c3_rhs121);
  sf_mex_destroy(&c3_lhs121);
  sf_mex_destroy(&c3_rhs122);
  sf_mex_destroy(&c3_lhs122);
}

static void c3_eml_sort(SFc3_ImageDetInstanceStruct *chartInstance, int32_T
  c3_x_data[], int32_T c3_x_sizes, int32_T c3_y_data[], int32_T *c3_y_sizes,
  int32_T c3_idx_data[], int32_T *c3_idx_sizes)
{
  int32_T c3_b_x_sizes;
  int32_T c3_loop_ub;
  int32_T c3_i143;
  int32_T c3_b_x_data[16];
  int32_T c3_dim;
  int32_T c3_b_dim;
  int32_T c3_c_dim;
  int32_T c3_x;
  int32_T c3_b_x;
  boolean_T c3_b7;
  int32_T c3_i144;
  static char_T c3_cv0[53] = { 'C', 'o', 'd', 'e', 'r', ':', 'M', 'A', 'T', 'L',
    'A', 'B', ':', 'g', 'e', 't', 'd', 'i', 'm', 'a', 'r', 'g', '_', 'd', 'i',
    'm', 'e', 'n', 's', 'i', 'o', 'n', 'M', 'u', 's', 't', 'B', 'e', 'P', 'o',
    's', 'i', 't', 'i', 'v', 'e', 'I', 'n', 't', 'e', 'g', 'e', 'r' };

  char_T c3_u[53];
  const mxArray *c3_y = NULL;
  int32_T c3_i145;
  real_T c3_d7;
  real_T c3_vlen;
  real_T c3_dv0[2];
  int32_T c3_iidx_sizes;
  int32_T c3_b_loop_ub;
  int32_T c3_i146;
  int32_T c3_iidx_data[16];
  int32_T c3_vwork_sizes;
  int32_T c3_c_loop_ub;
  int32_T c3_i147;
  int32_T c3_d_dim;
  int32_T c3_e_dim;
  int32_T c3_vstride;
  int32_T c3_i148;
  int32_T c3_b;
  int32_T c3_b_b;
  boolean_T c3_overflow;
  int32_T c3_k;
  real_T c3_d8;
  real_T c3_a;
  real_T c3_b_a;
  int32_T c3_c;
  int32_T c3_c_a;
  int32_T c3_c_b;
  int32_T c3_d_a;
  int32_T c3_d_b;
  int32_T c3_vspread;
  int32_T c3_f_dim;
  int32_T c3_g_dim;
  int32_T c3_i149;
  int32_T c3_e_a;
  int32_T c3_f_a;
  int32_T c3_i2;
  int32_T c3_i;
  int32_T c3_i1;
  int32_T c3_g_a;
  int32_T c3_e_b;
  int32_T c3_h_a;
  int32_T c3_f_b;
  int32_T c3_b_vstride;
  int32_T c3_g_b;
  int32_T c3_h_b;
  boolean_T c3_b_overflow;
  int32_T c3_j;
  int32_T c3_i_a;
  int32_T c3_j_a;
  int32_T c3_k_a;
  int32_T c3_l_a;
  int32_T c3_ix;
  real_T c3_b_vlen;
  int32_T c3_i150;
  int32_T c3_b_k;
  real_T c3_c_k;
  int32_T c3_vwork_data[16];
  int32_T c3_m_a;
  int32_T c3_i_b;
  int32_T c3_n_a;
  int32_T c3_j_b;
  int32_T c3_b_vwork_sizes;
  int32_T c3_d_loop_ub;
  int32_T c3_i151;
  int32_T c3_b_vwork_data[16];
  real_T c3_c_vlen;
  int32_T c3_i152;
  int32_T c3_d_k;
  int32_T c3_o_a;
  int32_T c3_k_b;
  int32_T c3_p_a;
  int32_T c3_l_b;
  c3_b_x_sizes = c3_x_sizes;
  c3_loop_ub = c3_x_sizes - 1;
  for (c3_i143 = 0; c3_i143 <= c3_loop_ub; c3_i143++) {
    c3_b_x_data[c3_i143] = c3_x_data[c3_i143];
  }

  c3_dim = c3_eml_nonsingleton_dim(chartInstance, c3_b_x_data, c3_b_x_sizes);
  c3_b_dim = c3_dim;
  c3_c_dim = c3_b_dim;
  c3_x = c3_c_dim;
  c3_b_x = c3_x;
  c3_b7 = (c3_c_dim == c3_b_x);
  if (c3_b7) {
  } else {
    for (c3_i144 = 0; c3_i144 < 53; c3_i144++) {
      c3_u[c3_i144] = c3_cv0[c3_i144];
    }

    c3_y = NULL;
    sf_mex_assign(&c3_y, sf_mex_create("y", c3_u, 10, 0U, 1U, 0U, 2, 1, 53),
                  false);
    sf_mex_call_debug(sfGlobalDebugInstanceStruct, "error", 0U, 1U, 14,
                      sf_mex_call_debug(sfGlobalDebugInstanceStruct, "message",
      1U, 1U, 14, c3_y));
  }

  c3_eml_switch_helper(chartInstance);
  c3_i145 = c3_dim;
  if (c3_i145 <= 1) {
    c3_d7 = (real_T)c3_x_sizes;
  } else {
    c3_d7 = 1.0;
  }

  c3_vlen = c3_d7;
  c3_dv0[0] = c3_vlen;
  c3_dv0[1] = 1.0;
  c3_iidx_sizes = (int32_T)c3_dv0[0];
  c3_b_loop_ub = (int32_T)c3_dv0[0] - 1;
  for (c3_i146 = 0; c3_i146 <= c3_b_loop_ub; c3_i146++) {
    c3_iidx_data[c3_i146] = 0;
  }

  c3_vwork_sizes = c3_iidx_sizes;
  *c3_y_sizes = c3_x_sizes;
  c3_dv0[0] = (real_T)c3_x_sizes;
  c3_dv0[1] = 1.0;
  c3_iidx_sizes = (int32_T)c3_dv0[0];
  c3_c_loop_ub = (int32_T)c3_dv0[0] - 1;
  for (c3_i147 = 0; c3_i147 <= c3_c_loop_ub; c3_i147++) {
    c3_iidx_data[c3_i147] = 0;
  }

  *c3_idx_sizes = c3_iidx_sizes;
  c3_d_dim = c3_dim;
  c3_e_dim = c3_d_dim - 1;
  c3_vstride = 1;
  c3_i148 = c3_e_dim;
  c3_b = c3_i148;
  c3_b_b = c3_b;
  if (1 > c3_b_b) {
    c3_overflow = false;
  } else {
    c3_eml_switch_helper(chartInstance);
    c3_eml_switch_helper(chartInstance);
    c3_overflow = (c3_b_b > 2147483646);
  }

  if (c3_overflow) {
    c3_check_forloop_overflow_error(chartInstance, true);
  }

  c3_k = 1;
  while (c3_k <= c3_i148) {
    c3_d8 = (real_T)c3_x_sizes;
    c3_vstride *= (int32_T)c3_d8;
    c3_k = 2;
  }

  c3_a = c3_vlen;
  c3_b_a = c3_a;
  c3_c = (int32_T)c3_b_a;
  c3_c_a = c3_c - 1;
  c3_c_b = c3_vstride;
  c3_d_a = c3_c_a;
  c3_d_b = c3_c_b;
  c3_vspread = c3_d_a * c3_d_b;
  c3_f_dim = c3_dim;
  c3_g_dim = c3_f_dim + 1;
  c3_i149 = c3_g_dim;
  c3_e_a = c3_i149;
  c3_f_a = c3_e_a;
  if (c3_f_a > 2) {
  } else {
    c3_eml_switch_helper(chartInstance);
    c3_eml_switch_helper(chartInstance);
  }

  c3_i2 = 0;
  c3_eml_switch_helper(chartInstance);
  c3_eml_switch_helper(chartInstance);
  c3_i = 1;
  while (c3_i <= 1) {
    c3_i1 = c3_i2;
    c3_g_a = c3_i2;
    c3_e_b = c3_vspread;
    c3_h_a = c3_g_a;
    c3_f_b = c3_e_b;
    c3_i2 = c3_h_a + c3_f_b;
    c3_b_vstride = c3_vstride;
    c3_g_b = c3_b_vstride;
    c3_h_b = c3_g_b;
    if (1 > c3_h_b) {
      c3_b_overflow = false;
    } else {
      c3_eml_switch_helper(chartInstance);
      c3_eml_switch_helper(chartInstance);
      c3_b_overflow = (c3_h_b > 2147483646);
    }

    if (c3_b_overflow) {
      c3_check_forloop_overflow_error(chartInstance, true);
    }

    for (c3_j = 1; c3_j <= c3_b_vstride; c3_j++) {
      c3_i_a = c3_i1;
      c3_j_a = c3_i_a + 1;
      c3_i1 = c3_j_a;
      c3_k_a = c3_i2;
      c3_l_a = c3_k_a + 1;
      c3_i2 = c3_l_a;
      c3_ix = c3_i1;
      c3_b_vlen = c3_vlen;
      c3_i150 = (int32_T)c3_b_vlen - 1;
      for (c3_b_k = 0; c3_b_k <= c3_i150; c3_b_k++) {
        c3_c_k = 1.0 + (real_T)c3_b_k;
        c3_vwork_data[_SFD_EML_ARRAY_BOUNDS_CHECK("", (int32_T)c3_c_k, 1,
          c3_vwork_sizes, 1, 0) - 1] = c3_x_data[_SFD_EML_ARRAY_BOUNDS_CHECK("",
          c3_ix, 1, c3_x_sizes, 1, 0) - 1];
        c3_m_a = c3_ix;
        c3_i_b = c3_vstride;
        c3_n_a = c3_m_a;
        c3_j_b = c3_i_b;
        c3_ix = c3_n_a + c3_j_b;
      }

      c3_b_vwork_sizes = c3_vwork_sizes;
      c3_d_loop_ub = c3_vwork_sizes - 1;
      for (c3_i151 = 0; c3_i151 <= c3_d_loop_ub; c3_i151++) {
        c3_b_vwork_data[c3_i151] = c3_vwork_data[c3_i151];
      }

      c3_eml_sort_idx(chartInstance, c3_b_vwork_data, c3_b_vwork_sizes,
                      c3_iidx_data, &c3_iidx_sizes);
      c3_ix = c3_i1;
      c3_c_vlen = c3_vlen;
      c3_i152 = (int32_T)c3_c_vlen - 1;
      for (c3_d_k = 0; c3_d_k <= c3_i152; c3_d_k++) {
        c3_c_k = 1.0 + (real_T)c3_d_k;
        c3_y_data[_SFD_EML_ARRAY_BOUNDS_CHECK("", c3_ix, 1, *c3_y_sizes, 1, 0) -
          1] = c3_vwork_data[_SFD_EML_ARRAY_BOUNDS_CHECK("",
          c3_iidx_data[_SFD_EML_ARRAY_BOUNDS_CHECK("", (int32_T)c3_c_k, 1,
          c3_iidx_sizes, 1, 0) - 1], 1, c3_vwork_sizes, 1, 0) - 1];
        c3_idx_data[_SFD_EML_ARRAY_BOUNDS_CHECK("", c3_ix, 1, *c3_idx_sizes, 1,
          0) - 1] = c3_iidx_data[_SFD_EML_ARRAY_BOUNDS_CHECK("", (int32_T)c3_c_k,
          1, c3_iidx_sizes, 1, 0) - 1];
        c3_o_a = c3_ix;
        c3_k_b = c3_vstride;
        c3_p_a = c3_o_a;
        c3_l_b = c3_k_b;
        c3_ix = c3_p_a + c3_l_b;
      }
    }

    c3_i = 2;
  }
}

static int32_T c3_eml_nonsingleton_dim(SFc3_ImageDetInstanceStruct
  *chartInstance, int32_T c3_x_data[], int32_T c3_x_sizes)
{
  int32_T c3_dim;
  (void)c3_x_data;
  c3_dim = 2;
  c3_eml_switch_helper(chartInstance);
  if ((real_T)c3_x_sizes != 1.0) {
    c3_dim = 1;
  }

  return c3_dim;
}

static void c3_eml_switch_helper(SFc3_ImageDetInstanceStruct *chartInstance)
{
  (void)chartInstance;
}

static void c3_check_forloop_overflow_error(SFc3_ImageDetInstanceStruct
  *chartInstance, boolean_T c3_overflow)
{
  int32_T c3_i153;
  static char_T c3_cv1[34] = { 'C', 'o', 'd', 'e', 'r', ':', 't', 'o', 'o', 'l',
    'b', 'o', 'x', ':', 'i', 'n', 't', '_', 'f', 'o', 'r', 'l', 'o', 'o', 'p',
    '_', 'o', 'v', 'e', 'r', 'f', 'l', 'o', 'w' };

  char_T c3_u[34];
  const mxArray *c3_y = NULL;
  int32_T c3_i154;
  static char_T c3_cv2[5] = { 'i', 'n', 't', '3', '2' };

  char_T c3_b_u[5];
  const mxArray *c3_b_y = NULL;
  (void)chartInstance;
  (void)c3_overflow;
  for (c3_i153 = 0; c3_i153 < 34; c3_i153++) {
    c3_u[c3_i153] = c3_cv1[c3_i153];
  }

  c3_y = NULL;
  sf_mex_assign(&c3_y, sf_mex_create("y", c3_u, 10, 0U, 1U, 0U, 2, 1, 34), false);
  for (c3_i154 = 0; c3_i154 < 5; c3_i154++) {
    c3_b_u[c3_i154] = c3_cv2[c3_i154];
  }

  c3_b_y = NULL;
  sf_mex_assign(&c3_b_y, sf_mex_create("y", c3_b_u, 10, 0U, 1U, 0U, 2, 1, 5),
                false);
  sf_mex_call_debug(sfGlobalDebugInstanceStruct, "error", 0U, 1U, 14,
                    sf_mex_call_debug(sfGlobalDebugInstanceStruct, "message", 1U,
    2U, 14, c3_y, 14, c3_b_y));
}

static void c3_eml_sort_idx(SFc3_ImageDetInstanceStruct *chartInstance, int32_T
  c3_x_data[], int32_T c3_x_sizes, int32_T c3_idx_data[], int32_T *c3_idx_sizes)
{
  int32_T c3_n;
  real_T c3_dv1[2];
  int32_T c3_idx0_sizes;
  int32_T c3_loop_ub;
  int32_T c3_i155;
  int32_T c3_idx0_data[16];
  int32_T c3_a;
  int32_T c3_b_a;
  int32_T c3_np1;
  int32_T c3_b_n;
  int32_T c3_b;
  int32_T c3_b_b;
  boolean_T c3_overflow;
  int32_T c3_k;
  int32_T c3_b_k;
  int32_T c3_c_n;
  int32_T c3_c_b;
  int32_T c3_d_b;
  boolean_T c3_b_overflow;
  int32_T c3_c_k;
  int32_T c3_c_a;
  int32_T c3_d_a;
  int32_T c3_i156;
  int32_T c3_e_b;
  int32_T c3_f_b;
  int32_T c3_d_k;
  int32_T c3_e_a;
  int32_T c3_f_a;
  int32_T c3_c;
  int32_T c3_irow1;
  int32_T c3_irow2;
  int32_T c3_g_a;
  int32_T c3_g_b;
  int32_T c3_h_a;
  int32_T c3_h_b;
  boolean_T c3_p;
  boolean_T c3_b8;
  boolean_T c3_b_p;
  int32_T c3_i_a;
  int32_T c3_j_a;
  int32_T c3_b_c;
  int32_T c3_k_a;
  int32_T c3_l_a;
  int32_T c3_c_c;
  int32_T c3_b_loop_ub;
  int32_T c3_i157;
  int32_T c3_i;
  int32_T c3_m_a;
  int32_T c3_n_a;
  int32_T c3_i2;
  int32_T c3_j;
  int32_T c3_i_b;
  int32_T c3_j_b;
  int32_T c3_pEnd;
  int32_T c3_c_p;
  int32_T c3_q;
  int32_T c3_o_a;
  int32_T c3_k_b;
  int32_T c3_p_a;
  int32_T c3_l_b;
  int32_T c3_qEnd;
  int32_T c3_q_a;
  int32_T c3_m_b;
  int32_T c3_r_a;
  int32_T c3_n_b;
  int32_T c3_kEnd;
  int32_T c3_b_irow1;
  int32_T c3_b_irow2;
  int32_T c3_s_a;
  int32_T c3_o_b;
  int32_T c3_t_a;
  int32_T c3_p_b;
  boolean_T c3_d_p;
  boolean_T c3_b9;
  boolean_T c3_e_p;
  int32_T c3_u_a;
  int32_T c3_v_a;
  int32_T c3_w_a;
  int32_T c3_x_a;
  int32_T c3_y_a;
  int32_T c3_ab_a;
  int32_T c3_bb_a;
  int32_T c3_cb_a;
  int32_T c3_db_a;
  int32_T c3_eb_a;
  int32_T c3_fb_a;
  int32_T c3_gb_a;
  int32_T c3_hb_a;
  int32_T c3_ib_a;
  int32_T c3_jb_a;
  int32_T c3_kb_a;
  int32_T c3_b_kEnd;
  int32_T c3_q_b;
  int32_T c3_r_b;
  boolean_T c3_c_overflow;
  int32_T c3_e_k;
  int32_T c3_lb_a;
  int32_T c3_s_b;
  int32_T c3_mb_a;
  int32_T c3_t_b;
  int32_T c3_d_c;
  int32_T c3_nb_a;
  int32_T c3_u_b;
  int32_T c3_ob_a;
  int32_T c3_v_b;
  c3_n = c3_x_sizes;
  c3_dv1[0] = (real_T)c3_x_sizes;
  c3_dv1[1] = 1.0;
  c3_idx0_sizes = (int32_T)c3_dv1[0];
  c3_loop_ub = (int32_T)c3_dv1[0] - 1;
  for (c3_i155 = 0; c3_i155 <= c3_loop_ub; c3_i155++) {
    c3_idx0_data[c3_i155] = 0;
  }

  *c3_idx_sizes = c3_idx0_sizes;
  c3_a = c3_n;
  c3_b_a = c3_a + 1;
  c3_np1 = c3_b_a;
  if (c3_x_sizes == 0) {
    c3_b_n = c3_n;
    c3_b = c3_b_n;
    c3_b_b = c3_b;
    if (1 > c3_b_b) {
      c3_overflow = false;
    } else {
      c3_eml_switch_helper(chartInstance);
      c3_eml_switch_helper(chartInstance);
      c3_overflow = (c3_b_b > 2147483646);
    }

    if (c3_overflow) {
      c3_check_forloop_overflow_error(chartInstance, true);
    }

    for (c3_k = 1; c3_k <= c3_b_n; c3_k++) {
      c3_b_k = c3_k;
      c3_idx_data[_SFD_EML_ARRAY_BOUNDS_CHECK("", c3_b_k, 1, *c3_idx_sizes, 1, 0)
        - 1] = c3_b_k;
    }
  } else {
    c3_c_n = c3_n;
    c3_c_b = c3_c_n;
    c3_d_b = c3_c_b;
    if (1 > c3_d_b) {
      c3_b_overflow = false;
    } else {
      c3_eml_switch_helper(chartInstance);
      c3_eml_switch_helper(chartInstance);
      c3_b_overflow = (c3_d_b > 2147483646);
    }

    if (c3_b_overflow) {
      c3_check_forloop_overflow_error(chartInstance, true);
    }

    for (c3_c_k = 1; c3_c_k <= c3_c_n; c3_c_k++) {
      c3_b_k = c3_c_k;
      c3_idx_data[_SFD_EML_ARRAY_BOUNDS_CHECK("", c3_b_k, 1, *c3_idx_sizes, 1, 0)
        - 1] = c3_b_k;
    }

    c3_c_a = c3_n;
    c3_d_a = c3_c_a - 1;
    c3_i156 = c3_d_a;
    c3_e_b = c3_i156;
    c3_f_b = c3_e_b;
    if (1 > c3_f_b) {
    } else {
      c3_eml_switch_helper(chartInstance);
      c3_eml_switch_helper(chartInstance);
    }

    for (c3_d_k = 1; c3_d_k <= c3_i156; c3_d_k += 2) {
      c3_b_k = c3_d_k;
      c3_e_a = c3_b_k;
      c3_f_a = c3_e_a;
      c3_c = c3_f_a;
      c3_irow1 = c3_b_k;
      c3_irow2 = c3_c + 1;
      c3_g_a = c3_x_data[_SFD_EML_ARRAY_BOUNDS_CHECK("", c3_irow1, 1, c3_x_sizes,
        1, 0) - 1];
      c3_g_b = c3_x_data[_SFD_EML_ARRAY_BOUNDS_CHECK("", c3_irow2, 1, c3_x_sizes,
        1, 0) - 1];
      c3_h_a = c3_g_a;
      c3_h_b = c3_g_b;
      c3_p = (c3_h_a <= c3_h_b);
      c3_b8 = c3_p;
      c3_b_p = c3_b8;
      if (c3_b_p) {
      } else {
        c3_i_a = c3_b_k;
        c3_j_a = c3_i_a;
        c3_b_c = c3_j_a;
        c3_idx_data[_SFD_EML_ARRAY_BOUNDS_CHECK("", c3_b_k, 1, *c3_idx_sizes, 1,
          0) - 1] = c3_b_c + 1;
        c3_k_a = c3_b_k;
        c3_l_a = c3_k_a;
        c3_c_c = c3_l_a;
        c3_idx_data[_SFD_EML_ARRAY_BOUNDS_CHECK("", c3_c_c + 1, 1, *c3_idx_sizes,
          1, 0) - 1] = c3_b_k;
      }
    }

    c3_idx0_sizes = c3_n;
    c3_b_loop_ub = c3_n - 1;
    for (c3_i157 = 0; c3_i157 <= c3_b_loop_ub; c3_i157++) {
      c3_idx0_data[c3_i157] = 1;
    }

    c3_i = 2;
    while (c3_i < c3_n) {
      c3_m_a = c3_i;
      c3_n_a = c3_m_a;
      c3_i2 = c3_n_a << 1;
      c3_j = 1;
      c3_i_b = c3_i;
      c3_j_b = c3_i_b + 1;
      for (c3_pEnd = c3_j_b; c3_pEnd < c3_np1; c3_pEnd = c3_ob_a + c3_v_b) {
        c3_c_p = c3_j;
        c3_q = c3_pEnd;
        c3_o_a = c3_j;
        c3_k_b = c3_i2;
        c3_p_a = c3_o_a;
        c3_l_b = c3_k_b;
        c3_qEnd = c3_p_a + c3_l_b;
        if (c3_qEnd > c3_np1) {
          c3_qEnd = c3_np1;
        }

        c3_b_k = 1;
        c3_q_a = c3_qEnd;
        c3_m_b = c3_j;
        c3_r_a = c3_q_a;
        c3_n_b = c3_m_b;
        c3_kEnd = c3_r_a - c3_n_b;
        while (c3_b_k <= c3_kEnd) {
          c3_b_irow1 = c3_idx_data[_SFD_EML_ARRAY_BOUNDS_CHECK("", c3_c_p, 1,
            *c3_idx_sizes, 1, 0) - 1];
          c3_b_irow2 = c3_idx_data[_SFD_EML_ARRAY_BOUNDS_CHECK("", c3_q, 1,
            *c3_idx_sizes, 1, 0) - 1];
          c3_s_a = c3_x_data[_SFD_EML_ARRAY_BOUNDS_CHECK("", c3_b_irow1, 1,
            c3_x_sizes, 1, 0) - 1];
          c3_o_b = c3_x_data[_SFD_EML_ARRAY_BOUNDS_CHECK("", c3_b_irow2, 1,
            c3_x_sizes, 1, 0) - 1];
          c3_t_a = c3_s_a;
          c3_p_b = c3_o_b;
          c3_d_p = (c3_t_a <= c3_p_b);
          c3_b9 = c3_d_p;
          c3_e_p = c3_b9;
          if (c3_e_p) {
            c3_idx0_data[_SFD_EML_ARRAY_BOUNDS_CHECK("", c3_b_k, 1,
              c3_idx0_sizes, 1, 0) - 1] =
              c3_idx_data[_SFD_EML_ARRAY_BOUNDS_CHECK("", c3_c_p, 1,
              *c3_idx_sizes, 1, 0) - 1];
            c3_u_a = c3_c_p;
            c3_v_a = c3_u_a + 1;
            c3_c_p = c3_v_a;
            if (c3_c_p == c3_pEnd) {
              while (c3_q < c3_qEnd) {
                c3_w_a = c3_b_k;
                c3_x_a = c3_w_a + 1;
                c3_b_k = c3_x_a;
                c3_idx0_data[_SFD_EML_ARRAY_BOUNDS_CHECK("", c3_b_k, 1,
                  c3_idx0_sizes, 1, 0) - 1] =
                  c3_idx_data[_SFD_EML_ARRAY_BOUNDS_CHECK("", c3_q, 1,
                  *c3_idx_sizes, 1, 0) - 1];
                c3_y_a = c3_q;
                c3_ab_a = c3_y_a + 1;
                c3_q = c3_ab_a;
              }
            }
          } else {
            c3_idx0_data[_SFD_EML_ARRAY_BOUNDS_CHECK("", c3_b_k, 1,
              c3_idx0_sizes, 1, 0) - 1] =
              c3_idx_data[_SFD_EML_ARRAY_BOUNDS_CHECK("", c3_q, 1, *c3_idx_sizes,
              1, 0) - 1];
            c3_bb_a = c3_q;
            c3_cb_a = c3_bb_a + 1;
            c3_q = c3_cb_a;
            if (c3_q == c3_qEnd) {
              while (c3_c_p < c3_pEnd) {
                c3_db_a = c3_b_k;
                c3_eb_a = c3_db_a + 1;
                c3_b_k = c3_eb_a;
                c3_idx0_data[_SFD_EML_ARRAY_BOUNDS_CHECK("", c3_b_k, 1,
                  c3_idx0_sizes, 1, 0) - 1] =
                  c3_idx_data[_SFD_EML_ARRAY_BOUNDS_CHECK("", c3_c_p, 1,
                  *c3_idx_sizes, 1, 0) - 1];
                c3_fb_a = c3_c_p;
                c3_gb_a = c3_fb_a + 1;
                c3_c_p = c3_gb_a;
              }
            }
          }

          c3_hb_a = c3_b_k;
          c3_ib_a = c3_hb_a + 1;
          c3_b_k = c3_ib_a;
        }

        c3_jb_a = c3_j;
        c3_kb_a = c3_jb_a;
        c3_c_p = c3_kb_a;
        c3_b_kEnd = c3_kEnd;
        c3_q_b = c3_b_kEnd;
        c3_r_b = c3_q_b;
        if (1 > c3_r_b) {
          c3_c_overflow = false;
        } else {
          c3_eml_switch_helper(chartInstance);
          c3_eml_switch_helper(chartInstance);
          c3_c_overflow = (c3_r_b > 2147483646);
        }

        if (c3_c_overflow) {
          c3_check_forloop_overflow_error(chartInstance, true);
        }

        for (c3_e_k = 1; c3_e_k <= c3_b_kEnd; c3_e_k++) {
          c3_b_k = c3_e_k;
          c3_lb_a = c3_c_p - 1;
          c3_s_b = c3_b_k;
          c3_mb_a = c3_lb_a;
          c3_t_b = c3_s_b;
          c3_d_c = c3_mb_a + c3_t_b;
          c3_idx_data[_SFD_EML_ARRAY_BOUNDS_CHECK("", c3_d_c, 1, *c3_idx_sizes,
            1, 0) - 1] = c3_idx0_data[_SFD_EML_ARRAY_BOUNDS_CHECK("", c3_b_k, 1,
            c3_idx0_sizes, 1, 0) - 1];
        }

        c3_j = c3_qEnd;
        c3_nb_a = c3_j;
        c3_u_b = c3_i;
        c3_ob_a = c3_nb_a;
        c3_v_b = c3_u_b;
      }

      c3_i = c3_i2;
    }
  }
}

static void c3_sign(SFc3_ImageDetInstanceStruct *chartInstance, real32_T c3_x[2],
                    real32_T c3_b_x[2])
{
  int32_T c3_i158;
  for (c3_i158 = 0; c3_i158 < 2; c3_i158++) {
    c3_b_x[c3_i158] = c3_x[c3_i158];
  }

  c3_b_sign(chartInstance, c3_b_x);
}

static void c3_abs(SFc3_ImageDetInstanceStruct *chartInstance, real32_T c3_x[2],
                   real32_T c3_y[2])
{
  int32_T c3_k;
  real_T c3_b_k;
  real32_T c3_b_x;
  real32_T c3_b_y;
  (void)chartInstance;
  for (c3_k = 0; c3_k < 2; c3_k++) {
    c3_b_k = 1.0 + (real_T)c3_k;
    c3_b_x = c3_x[(int32_T)c3_b_k - 1];
    c3_b_y = muSingleScalarAbs(c3_b_x);
    c3_y[(int32_T)c3_b_k - 1] = c3_b_y;
  }
}

static void c3_tand(SFc3_ImageDetInstanceStruct *chartInstance, real32_T c3_x[2],
                    real32_T c3_b_x[2])
{
  int32_T c3_i159;
  for (c3_i159 = 0; c3_i159 < 2; c3_i159++) {
    c3_b_x[c3_i159] = c3_x[c3_i159];
  }

  c3_b_tand(chartInstance, c3_b_x);
}

static void c3_eml_scalar_eg(SFc3_ImageDetInstanceStruct *chartInstance)
{
  (void)chartInstance;
}

static real32_T c3_b_abs(SFc3_ImageDetInstanceStruct *chartInstance, real32_T
  c3_x)
{
  real32_T c3_b_x;
  (void)chartInstance;
  c3_b_x = c3_x;
  return muSingleScalarAbs(c3_b_x);
}

static void c3_eml_li_find(SFc3_ImageDetInstanceStruct *chartInstance, boolean_T
  c3_x_data[], int32_T c3_x_sizes[2], int32_T c3_y_data[], int32_T c3_y_sizes[2])
{
  int32_T c3_n;
  int32_T c3_b_n;
  int32_T c3_k;
  int32_T c3_c_n;
  int32_T c3_b;
  int32_T c3_b_b;
  boolean_T c3_overflow;
  int32_T c3_i;
  int32_T c3_b_i;
  int32_T c3_a;
  int32_T c3_b_a;
  const mxArray *c3_y = NULL;
  int32_T c3_tmp_sizes[2];
  int32_T c3_iv12[2];
  int32_T c3_i160;
  int32_T c3_i161;
  int32_T c3_loop_ub;
  int32_T c3_i162;
  int32_T c3_tmp_data[31];
  int32_T c3_i163;
  int32_T c3_j;
  int32_T c3_d_n;
  int32_T c3_c_b;
  int32_T c3_d_b;
  boolean_T c3_b_overflow;
  int32_T c3_c_i;
  int32_T c3_d_i;
  int32_T c3_c_a;
  int32_T c3_d_a;
  c3_n = c3_x_sizes[1];
  c3_b_n = c3_n;
  c3_k = 0;
  c3_c_n = c3_b_n;
  c3_b = c3_c_n;
  c3_b_b = c3_b;
  if (1 > c3_b_b) {
    c3_overflow = false;
  } else {
    c3_eml_switch_helper(chartInstance);
    c3_eml_switch_helper(chartInstance);
    c3_overflow = (c3_b_b > 2147483646);
  }

  if (c3_overflow) {
    c3_check_forloop_overflow_error(chartInstance, true);
  }

  for (c3_i = 1; c3_i <= c3_c_n; c3_i++) {
    c3_b_i = c3_i;
    if (c3_x_data[_SFD_EML_ARRAY_BOUNDS_CHECK("", c3_b_i, 1, c3_x_sizes[1], 1, 0)
        - 1]) {
      c3_a = c3_k;
      c3_b_a = c3_a + 1;
      c3_k = c3_b_a;
    }
  }

  if (c3_k <= c3_n) {
  } else {
    c3_y = NULL;
    sf_mex_assign(&c3_y, sf_mex_create("y", "Assertion failed.", 15, 0U, 0U, 0U,
      2, 1, strlen("Assertion failed.")), false);
    sf_mex_call_debug(sfGlobalDebugInstanceStruct, "error", 0U, 1U, 14, c3_y);
  }

  c3_tmp_sizes[0] = 1;
  c3_iv12[0] = 1;
  c3_iv12[1] = (int32_T)_SFD_NON_NEGATIVE_CHECK("", (real_T)c3_k);
  c3_tmp_sizes[1] = c3_iv12[1];
  c3_i160 = c3_tmp_sizes[0];
  c3_i161 = c3_tmp_sizes[1];
  c3_loop_ub = (int32_T)_SFD_NON_NEGATIVE_CHECK("", (real_T)c3_k) - 1;
  for (c3_i162 = 0; c3_i162 <= c3_loop_ub; c3_i162++) {
    c3_tmp_data[c3_i162] = 0;
  }

  for (c3_i163 = 0; c3_i163 < 2; c3_i163++) {
    c3_y_sizes[c3_i163] = c3_tmp_sizes[c3_i163];
  }

  c3_j = 1;
  c3_d_n = c3_n;
  c3_c_b = c3_d_n;
  c3_d_b = c3_c_b;
  if (1 > c3_d_b) {
    c3_b_overflow = false;
  } else {
    c3_eml_switch_helper(chartInstance);
    c3_eml_switch_helper(chartInstance);
    c3_b_overflow = (c3_d_b > 2147483646);
  }

  if (c3_b_overflow) {
    c3_check_forloop_overflow_error(chartInstance, true);
  }

  for (c3_c_i = 1; c3_c_i <= c3_d_n; c3_c_i++) {
    c3_d_i = c3_c_i;
    if (c3_x_data[_SFD_EML_ARRAY_BOUNDS_CHECK("", c3_d_i, 1, c3_x_sizes[1], 1, 0)
        - 1]) {
      c3_y_data[_SFD_EML_ARRAY_BOUNDS_CHECK("", c3_j, 1, c3_y_sizes[1], 1, 0) -
        1] = c3_d_i;
      c3_c_a = c3_j;
      c3_d_a = c3_c_a + 1;
      c3_j = c3_d_a;
    }
  }
}

static real32_T c3_norm(SFc3_ImageDetInstanceStruct *chartInstance, real32_T
  c3_x[2])
{
  real32_T c3_y;
  real32_T c3_scale;
  int32_T c3_k;
  int32_T c3_b_k;
  real32_T c3_b_x;
  real32_T c3_c_x;
  real32_T c3_absxk;
  real32_T c3_t;
  c3_below_threshold(chartInstance);
  c3_y = 0.0F;
  c3_scale = 1.17549435E-38F;
  c3_eml_switch_helper(chartInstance);
  for (c3_k = 1; c3_k < 3; c3_k++) {
    c3_b_k = c3_k - 1;
    c3_b_x = c3_x[c3_b_k];
    c3_c_x = c3_b_x;
    c3_absxk = muSingleScalarAbs(c3_c_x);
    if (c3_absxk > c3_scale) {
      c3_t = c3_scale / c3_absxk;
      c3_y = 1.0F + c3_y * c3_t * c3_t;
      c3_scale = c3_absxk;
    } else {
      c3_t = c3_absxk / c3_scale;
      c3_y += c3_t * c3_t;
    }
  }

  return c3_scale * muSingleScalarSqrt(c3_y);
}

static void c3_below_threshold(SFc3_ImageDetInstanceStruct *chartInstance)
{
  (void)chartInstance;
}

static real32_T c3_atan2d(SFc3_ImageDetInstanceStruct *chartInstance, real32_T
  c3_y, real32_T c3_x)
{
  real32_T c3_b_y;
  real32_T c3_b_x;
  real32_T c3_b_r;
  real32_T c3_b;
  c3_eml_scalar_eg(chartInstance);
  c3_b_y = c3_y;
  c3_b_x = c3_x;
  c3_b_r = muSingleScalarAtan2(c3_b_y, c3_b_x);
  c3_b = c3_b_r;
  return 57.2957802F * c3_b;
}

static const mxArray *c3_l_sf_marshallOut(void *chartInstanceVoid, void
  *c3_inData)
{
  const mxArray *c3_mxArrayOutData = NULL;
  int32_T c3_u;
  const mxArray *c3_y = NULL;
  SFc3_ImageDetInstanceStruct *chartInstance;
  chartInstance = (SFc3_ImageDetInstanceStruct *)chartInstanceVoid;
  c3_mxArrayOutData = NULL;
  c3_u = *(int32_T *)c3_inData;
  c3_y = NULL;
  sf_mex_assign(&c3_y, sf_mex_create("y", &c3_u, 6, 0U, 0U, 0U, 0), false);
  sf_mex_assign(&c3_mxArrayOutData, c3_y, false);
  return c3_mxArrayOutData;
}

static int32_T c3_q_emlrt_marshallIn(SFc3_ImageDetInstanceStruct *chartInstance,
  const mxArray *c3_u, const emlrtMsgIdentifier *c3_parentId)
{
  int32_T c3_y;
  int32_T c3_i164;
  (void)chartInstance;
  sf_mex_import(c3_parentId, sf_mex_dup(c3_u), &c3_i164, 1, 6, 0U, 0, 0U, 0);
  c3_y = c3_i164;
  sf_mex_destroy(&c3_u);
  return c3_y;
}

static void c3_j_sf_marshallIn(void *chartInstanceVoid, const mxArray
  *c3_mxArrayInData, const char_T *c3_varName, void *c3_outData)
{
  const mxArray *c3_b_sfEvent;
  const char_T *c3_identifier;
  emlrtMsgIdentifier c3_thisId;
  int32_T c3_y;
  SFc3_ImageDetInstanceStruct *chartInstance;
  chartInstance = (SFc3_ImageDetInstanceStruct *)chartInstanceVoid;
  c3_b_sfEvent = sf_mex_dup(c3_mxArrayInData);
  c3_identifier = c3_varName;
  c3_thisId.fIdentifier = c3_identifier;
  c3_thisId.fParent = NULL;
  c3_y = c3_q_emlrt_marshallIn(chartInstance, sf_mex_dup(c3_b_sfEvent),
    &c3_thisId);
  sf_mex_destroy(&c3_b_sfEvent);
  *(int32_T *)c3_outData = c3_y;
  sf_mex_destroy(&c3_mxArrayInData);
}

static const mxArray *c3_blobs_bus_io(void *chartInstanceVoid, void *c3_pData)
{
  const mxArray *c3_mxVal = NULL;
  SFc3_ImageDetInstanceStruct *chartInstance;
  (void)c3_pData;
  chartInstance = (SFc3_ImageDetInstanceStruct *)chartInstanceVoid;
  c3_mxVal = NULL;
  sf_mex_assign(&c3_mxVal, c3_sf_marshall_unsupported(chartInstance), false);
  return c3_mxVal;
}

static const mxArray *c3_sf_marshall_unsupported(void *chartInstanceVoid)
{
  const mxArray *c3_y = NULL;
  SFc3_ImageDetInstanceStruct *chartInstance;
  chartInstance = (SFc3_ImageDetInstanceStruct *)chartInstanceVoid;
  c3_y = NULL;
  sf_mex_assign(&c3_y, c3_c_emlrt_marshallOut(chartInstance,
    "Structures with variable-sized fields unsupported for debugging."), false);
  return c3_y;
}

static const mxArray *c3_c_emlrt_marshallOut(SFc3_ImageDetInstanceStruct
  *chartInstance, const char * c3_u)
{
  const mxArray *c3_y = NULL;
  (void)chartInstance;
  c3_y = NULL;
  sf_mex_assign(&c3_y, sf_mex_create("y", c3_u, 15, 0U, 0U, 0U, 2, 1, strlen
    (c3_u)), false);
  return c3_y;
}

static const mxArray *c3_players_bus_io(void *chartInstanceVoid, void *c3_pData)
{
  const mxArray *c3_mxVal = NULL;
  SFc3_ImageDetInstanceStruct *chartInstance;
  (void)c3_pData;
  chartInstance = (SFc3_ImageDetInstanceStruct *)chartInstanceVoid;
  c3_mxVal = NULL;
  sf_mex_assign(&c3_mxVal, c3_sf_marshall_unsupported(chartInstance), false);
  return c3_mxVal;
}

static const mxArray *c3_Ball_bus_io(void *chartInstanceVoid, void *c3_pData)
{
  const mxArray *c3_mxVal = NULL;
  SFc3_ImageDetInstanceStruct *chartInstance;
  (void)c3_pData;
  chartInstance = (SFc3_ImageDetInstanceStruct *)chartInstanceVoid;
  c3_mxVal = NULL;
  sf_mex_assign(&c3_mxVal, c3_sf_marshall_unsupported(chartInstance), false);
  return c3_mxVal;
}

static void c3_b_sign(SFc3_ImageDetInstanceStruct *chartInstance, real32_T c3_x
                      [2])
{
  int32_T c3_k;
  real_T c3_b_k;
  real32_T c3_b_x;
  real32_T c3_c_x;
  (void)chartInstance;
  for (c3_k = 0; c3_k < 2; c3_k++) {
    c3_b_k = 1.0 + (real_T)c3_k;
    c3_b_x = c3_x[(int32_T)c3_b_k - 1];
    c3_c_x = c3_b_x;
    c3_c_x = muSingleScalarSign(c3_c_x);
    c3_x[(int32_T)c3_b_k - 1] = c3_c_x;
  }
}

static void c3_b_tand(SFc3_ImageDetInstanceStruct *chartInstance, real32_T c3_x
                      [2])
{
  int32_T c3_k;
  real_T c3_b_k;
  real32_T c3_b_x;
  real32_T c3_c_x;
  real32_T c3_d_x;
  real32_T c3_e_x;
  boolean_T c3_b;
  boolean_T c3_b10;
  real32_T c3_f_x;
  boolean_T c3_b_b;
  boolean_T c3_b11;
  boolean_T c3_c_b;
  real32_T c3_g_x;
  real32_T c3_h_x;
  real32_T c3_i_x;
  real32_T c3_xk;
  real32_T c3_j_x;
  real32_T c3_k_x;
  real32_T c3_absx;
  real32_T c3_l_x;
  real32_T c3_m_x;
  real32_T c3_d_b;
  int8_T c3_n;
  real32_T c3_e_b;
  real32_T c3_f_b;
  real32_T c3_g_b;
  real32_T c3_h_b;
  int8_T c3_b_n;
  real32_T c3_y;
  real32_T c3_b_y;
  real32_T c3_c_y;
  boolean_T guard1 = false;
  for (c3_k = 0; c3_k < 2; c3_k++) {
    c3_b_k = 1.0 + (real_T)c3_k;
    c3_b_x = c3_x[(int32_T)c3_b_k - 1];
    c3_c_x = c3_b_x;
    c3_d_x = c3_c_x;
    c3_e_x = c3_d_x;
    c3_b = muSingleScalarIsInf(c3_e_x);
    c3_b10 = !c3_b;
    c3_f_x = c3_d_x;
    c3_b_b = muSingleScalarIsNaN(c3_f_x);
    c3_b11 = !c3_b_b;
    c3_c_b = (c3_b10 && c3_b11);
    if (!c3_c_b) {
      c3_c_x = ((real32_T)rtNaN);
    } else {
      c3_g_x = c3_c_x;
      c3_h_x = c3_g_x;
      c3_i_x = c3_h_x;
      c3_eml_scalar_eg(chartInstance);
      c3_xk = c3_i_x;
      c3_h_x = muSingleScalarRem(c3_xk, 360.0F);
      c3_j_x = c3_h_x;
      c3_k_x = c3_j_x;
      c3_absx = muSingleScalarAbs(c3_k_x);
      if (c3_absx > 180.0F) {
        if (c3_h_x > 0.0F) {
          c3_h_x -= 360.0F;
        } else {
          c3_h_x += 360.0F;
        }

        c3_l_x = c3_h_x;
        c3_m_x = c3_l_x;
        c3_absx = muSingleScalarAbs(c3_m_x);
      }

      if (c3_absx <= 45.0F) {
        c3_d_b = c3_h_x;
        c3_h_x = 0.0174532924F * c3_d_b;
        c3_n = 0;
      } else if (c3_absx <= 135.0F) {
        if (c3_h_x > 0.0F) {
          c3_e_b = c3_h_x - 90.0F;
          c3_h_x = 0.0174532924F * c3_e_b;
          c3_n = 1;
        } else {
          c3_f_b = c3_h_x + 90.0F;
          c3_h_x = 0.0174532924F * c3_f_b;
          c3_n = -1;
        }
      } else if (c3_h_x > 0.0F) {
        c3_g_b = c3_h_x - 180.0F;
        c3_h_x = 0.0174532924F * c3_g_b;
        c3_n = 2;
      } else {
        c3_h_b = c3_h_x + 180.0F;
        c3_h_x = 0.0174532924F * c3_h_b;
        c3_n = -2;
      }

      c3_b_n = c3_n;
      c3_c_x = c3_h_x;
      c3_c_x = muSingleScalarTan(c3_c_x);
      guard1 = false;
      if ((real_T)c3_b_n == 1.0) {
        guard1 = true;
      } else {
        if ((real_T)c3_b_n == -1.0) {
          guard1 = true;
        }
      }

      if (guard1 == true) {
        c3_y = c3_c_x;
        c3_b_y = c3_y;
        c3_c_y = c3_b_y;
        c3_c_x = -1.0F / c3_c_y;
      }
    }

    c3_x[(int32_T)c3_b_k - 1] = c3_c_x;
  }
}

static void init_dsm_address_info(SFc3_ImageDetInstanceStruct *chartInstance)
{
  (void)chartInstance;
}

static void init_simulink_io_address(SFc3_ImageDetInstanceStruct *chartInstance)
{
  chartInstance->c3_blobs_data = (c3_BlobData *)ssGetInputPortSignal_wrapper
    (chartInstance->S, 0);
  chartInstance->c3_blobs_elems_sizes = (c3_BlobData_size *)
    ssGetCurrentInputPortDimensions_wrapper(chartInstance->S, 0);
  chartInstance->c3_players_data = (c3_PlayerData *)
    ssGetOutputPortSignal_wrapper(chartInstance->S, 1);
  chartInstance->c3_players_elems_sizes = (c3_PlayerData_size *)
    ssGetCurrentOutputPortDimensions_wrapper(chartInstance->S, 1);
  chartInstance->c3_camRes = (real_T (*)[2])ssGetInputPortSignal_wrapper
    (chartInstance->S, 1);
  chartInstance->c3_height = (real_T *)ssGetInputPortSignal_wrapper
    (chartInstance->S, 2);
  chartInstance->c3_Ball_data = (c3_BallData *)ssGetOutputPortSignal_wrapper
    (chartInstance->S, 2);
  chartInstance->c3_Ball_elems_sizes = (c3_BallData_size *)
    ssGetCurrentOutputPortDimensions_wrapper(chartInstance->S, 2);
}

/* SFunction Glue Code */
#ifdef utFree
#undef utFree
#endif

#ifdef utMalloc
#undef utMalloc
#endif

#ifdef __cplusplus

extern "C" void *utMalloc(size_t size);
extern "C" void utFree(void*);

#else

extern void *utMalloc(size_t size);
extern void utFree(void*);

#endif

void sf_c3_ImageDet_get_check_sum(mxArray *plhs[])
{
  ((real_T *)mxGetPr((plhs[0])))[0] = (real_T)(2761759055U);
  ((real_T *)mxGetPr((plhs[0])))[1] = (real_T)(3867104996U);
  ((real_T *)mxGetPr((plhs[0])))[2] = (real_T)(3785256766U);
  ((real_T *)mxGetPr((plhs[0])))[3] = (real_T)(4175562722U);
}

mxArray* sf_c3_ImageDet_get_post_codegen_info(void);
mxArray *sf_c3_ImageDet_get_autoinheritance_info(void)
{
  const char *autoinheritanceFields[] = { "checksum", "inputs", "parameters",
    "outputs", "locals", "postCodegenInfo" };

  mxArray *mxAutoinheritanceInfo = mxCreateStructMatrix(1, 1, sizeof
    (autoinheritanceFields)/sizeof(autoinheritanceFields[0]),
    autoinheritanceFields);

  {
    mxArray *mxChecksum = mxCreateString("pAWtn4SYXmZJILo3KvFhzB");
    mxSetField(mxAutoinheritanceInfo,0,"checksum",mxChecksum);
  }

  {
    const char *dataFields[] = { "size", "type", "complexity" };

    mxArray *mxData = mxCreateStructMatrix(1,3,3,dataFields);

    {
      mxArray *mxSize = mxCreateDoubleMatrix(1,2,mxREAL);
      double *pr = mxGetPr(mxSize);
      pr[0] = (double)(1);
      pr[1] = (double)(1);
      mxSetField(mxData,0,"size",mxSize);
    }

    {
      const char *typeFields[] = { "base", "fixpt" };

      mxArray *mxType = mxCreateStructMatrix(1,1,2,typeFields);
      mxSetField(mxType,0,"base",mxCreateDoubleScalar(13));
      mxSetField(mxType,0,"fixpt",mxCreateDoubleMatrix(0,0,mxREAL));
      mxSetField(mxData,0,"type",mxType);
    }

    mxSetField(mxData,0,"complexity",mxCreateDoubleScalar(0));

    {
      mxArray *mxSize = mxCreateDoubleMatrix(1,2,mxREAL);
      double *pr = mxGetPr(mxSize);
      pr[0] = (double)(2);
      pr[1] = (double)(1);
      mxSetField(mxData,1,"size",mxSize);
    }

    {
      const char *typeFields[] = { "base", "fixpt" };

      mxArray *mxType = mxCreateStructMatrix(1,1,2,typeFields);
      mxSetField(mxType,0,"base",mxCreateDoubleScalar(10));
      mxSetField(mxType,0,"fixpt",mxCreateDoubleMatrix(0,0,mxREAL));
      mxSetField(mxData,1,"type",mxType);
    }

    mxSetField(mxData,1,"complexity",mxCreateDoubleScalar(0));

    {
      mxArray *mxSize = mxCreateDoubleMatrix(1,2,mxREAL);
      double *pr = mxGetPr(mxSize);
      pr[0] = (double)(1);
      pr[1] = (double)(1);
      mxSetField(mxData,2,"size",mxSize);
    }

    {
      const char *typeFields[] = { "base", "fixpt" };

      mxArray *mxType = mxCreateStructMatrix(1,1,2,typeFields);
      mxSetField(mxType,0,"base",mxCreateDoubleScalar(10));
      mxSetField(mxType,0,"fixpt",mxCreateDoubleMatrix(0,0,mxREAL));
      mxSetField(mxData,2,"type",mxType);
    }

    mxSetField(mxData,2,"complexity",mxCreateDoubleScalar(0));
    mxSetField(mxAutoinheritanceInfo,0,"inputs",mxData);
  }

  {
    mxSetField(mxAutoinheritanceInfo,0,"parameters",mxCreateDoubleMatrix(0,0,
                mxREAL));
  }

  {
    const char *dataFields[] = { "size", "type", "complexity" };

    mxArray *mxData = mxCreateStructMatrix(1,2,3,dataFields);

    {
      mxArray *mxSize = mxCreateDoubleMatrix(1,2,mxREAL);
      double *pr = mxGetPr(mxSize);
      pr[0] = (double)(1);
      pr[1] = (double)(1);
      mxSetField(mxData,0,"size",mxSize);
    }

    {
      const char *typeFields[] = { "base", "fixpt" };

      mxArray *mxType = mxCreateStructMatrix(1,1,2,typeFields);
      mxSetField(mxType,0,"base",mxCreateDoubleScalar(13));
      mxSetField(mxType,0,"fixpt",mxCreateDoubleMatrix(0,0,mxREAL));
      mxSetField(mxData,0,"type",mxType);
    }

    mxSetField(mxData,0,"complexity",mxCreateDoubleScalar(0));

    {
      mxArray *mxSize = mxCreateDoubleMatrix(1,2,mxREAL);
      double *pr = mxGetPr(mxSize);
      pr[0] = (double)(1);
      pr[1] = (double)(1);
      mxSetField(mxData,1,"size",mxSize);
    }

    {
      const char *typeFields[] = { "base", "fixpt" };

      mxArray *mxType = mxCreateStructMatrix(1,1,2,typeFields);
      mxSetField(mxType,0,"base",mxCreateDoubleScalar(13));
      mxSetField(mxType,0,"fixpt",mxCreateDoubleMatrix(0,0,mxREAL));
      mxSetField(mxData,1,"type",mxType);
    }

    mxSetField(mxData,1,"complexity",mxCreateDoubleScalar(0));
    mxSetField(mxAutoinheritanceInfo,0,"outputs",mxData);
  }

  {
    mxSetField(mxAutoinheritanceInfo,0,"locals",mxCreateDoubleMatrix(0,0,mxREAL));
  }

  {
    mxArray* mxPostCodegenInfo = sf_c3_ImageDet_get_post_codegen_info();
    mxSetField(mxAutoinheritanceInfo,0,"postCodegenInfo",mxPostCodegenInfo);
  }

  return(mxAutoinheritanceInfo);
}

mxArray *sf_c3_ImageDet_third_party_uses_info(void)
{
  mxArray * mxcell3p = mxCreateCellMatrix(1,0);
  return(mxcell3p);
}

mxArray *sf_c3_ImageDet_jit_fallback_info(void)
{
  const char *infoFields[] = { "fallbackType", "fallbackReason",
    "incompatibleSymbol", };

  mxArray *mxInfo = mxCreateStructMatrix(1, 1, 3, infoFields);
  mxArray *fallbackReason = mxCreateString("feature_off");
  mxArray *incompatibleSymbol = mxCreateString("");
  mxArray *fallbackType = mxCreateString("early");
  mxSetField(mxInfo, 0, infoFields[0], fallbackType);
  mxSetField(mxInfo, 0, infoFields[1], fallbackReason);
  mxSetField(mxInfo, 0, infoFields[2], incompatibleSymbol);
  return mxInfo;
}

mxArray *sf_c3_ImageDet_updateBuildInfo_args_info(void)
{
  mxArray *mxBIArgs = mxCreateCellMatrix(1,0);
  return mxBIArgs;
}

mxArray* sf_c3_ImageDet_get_post_codegen_info(void)
{
  const char* fieldNames[] = { "exportedFunctionsUsedByThisChart",
    "exportedFunctionsChecksum" };

  mwSize dims[2] = { 1, 1 };

  mxArray* mxPostCodegenInfo = mxCreateStructArray(2, dims, sizeof(fieldNames)/
    sizeof(fieldNames[0]), fieldNames);

  {
    mxArray* mxExportedFunctionsChecksum = mxCreateString("");
    mwSize exp_dims[2] = { 0, 1 };

    mxArray* mxExportedFunctionsUsedByThisChart = mxCreateCellArray(2, exp_dims);
    mxSetField(mxPostCodegenInfo, 0, "exportedFunctionsUsedByThisChart",
               mxExportedFunctionsUsedByThisChart);
    mxSetField(mxPostCodegenInfo, 0, "exportedFunctionsChecksum",
               mxExportedFunctionsChecksum);
  }

  return mxPostCodegenInfo;
}

static const mxArray *sf_get_sim_state_info_c3_ImageDet(void)
{
  const char *infoFields[] = { "chartChecksum", "varInfo" };

  mxArray *mxInfo = mxCreateStructMatrix(1, 1, 2, infoFields);
  const char *infoEncStr[] = {
    "100 S1x3'type','srcId','name','auxInfo'{{M[1],M[50],T\"Ball\",},{M[1],M[46],T\"players\",},{M[8],M[0],T\"is_active_c3_ImageDet\",}}"
  };

  mxArray *mxVarInfo = sf_mex_decode_encoded_mx_struct_array(infoEncStr, 3, 10);
  mxArray *mxChecksum = mxCreateDoubleMatrix(1, 4, mxREAL);
  sf_c3_ImageDet_get_check_sum(&mxChecksum);
  mxSetField(mxInfo, 0, infoFields[0], mxChecksum);
  mxSetField(mxInfo, 0, infoFields[1], mxVarInfo);
  return mxInfo;
}

static void chart_debug_initialization(SimStruct *S, unsigned int
  fullDebuggerInitialization)
{
  if (!sim_mode_is_rtw_gen(S)) {
    SFc3_ImageDetInstanceStruct *chartInstance;
    ChartRunTimeInfo * crtInfo = (ChartRunTimeInfo *)(ssGetUserData(S));
    ChartInfoStruct * chartInfo = (ChartInfoStruct *)(crtInfo->instanceInfo);
    chartInstance = (SFc3_ImageDetInstanceStruct *) chartInfo->chartInstance;
    if (ssIsFirstInitCond(S) && fullDebuggerInitialization==1) {
      /* do this only if simulation is starting */
      {
        unsigned int chartAlreadyPresent;
        chartAlreadyPresent = sf_debug_initialize_chart
          (sfGlobalDebugInstanceStruct,
           _ImageDetMachineNumber_,
           3,
           1,
           1,
           0,
           5,
           0,
           0,
           0,
           0,
           0,
           &(chartInstance->chartNumber),
           &(chartInstance->instanceNumber),
           (void *)S);

        /* Each instance must initialize its own list of scripts */
        init_script_number_translation(_ImageDetMachineNumber_,
          chartInstance->chartNumber,chartInstance->instanceNumber);
        if (chartAlreadyPresent==0) {
          /* this is the first instance */
          sf_debug_set_chart_disable_implicit_casting
            (sfGlobalDebugInstanceStruct,_ImageDetMachineNumber_,
             chartInstance->chartNumber,1);
          sf_debug_set_chart_event_thresholds(sfGlobalDebugInstanceStruct,
            _ImageDetMachineNumber_,
            chartInstance->chartNumber,
            0,
            0,
            0);
          _SFD_SET_DATA_PROPS(0,1,1,0,"blobs");
          _SFD_SET_DATA_PROPS(1,2,0,1,"players");
          _SFD_SET_DATA_PROPS(2,1,1,0,"camRes");
          _SFD_SET_DATA_PROPS(3,1,1,0,"height");
          _SFD_SET_DATA_PROPS(4,2,0,1,"Ball");
          _SFD_STATE_INFO(0,0,2);
          _SFD_CH_SUBSTATE_COUNT(0);
          _SFD_CH_SUBSTATE_DECOMP(0);
        }

        _SFD_CV_INIT_CHART(0,0,0,0);

        {
          _SFD_CV_INIT_STATE(0,0,0,0,0,0,NULL,NULL);
        }

        _SFD_CV_INIT_TRANS(0,0,NULL,NULL,0,NULL);

        /* Initialization of MATLAB Function Model Coverage */
        _SFD_CV_INIT_EML(0,1,1,6,0,6,0,3,0,3,2);
        _SFD_CV_INIT_EML_FCN(0,0,"eML_blk_kernel",0,-1,2502);
        _SFD_CV_INIT_EML_SATURATION(0,1,0,1989,-1,1998);
        _SFD_CV_INIT_EML_SATURATION(0,1,1,2240,-1,2261);
        _SFD_CV_INIT_EML_SATURATION(0,1,2,2302,-1,2323);
        _SFD_CV_INIT_EML_SATURATION(0,1,3,2384,-1,2402);
        _SFD_CV_INIT_EML_SATURATION(0,1,4,921,-1,961);
        _SFD_CV_INIT_EML_SATURATION(0,1,5,978,-1,1018);
        _SFD_CV_INIT_EML_IF(0,1,0,395,419,-1,1145);
        _SFD_CV_INIT_EML_IF(0,1,1,869,904,-1,1141);
        _SFD_CV_INIT_EML_IF(0,1,2,1618,1629,-1,2501);
        _SFD_CV_INIT_EML_IF(0,1,3,1634,1646,-1,2497);
        _SFD_CV_INIT_EML_IF(0,1,4,1896,1931,-1,2019);
        _SFD_CV_INIT_EML_IF(0,1,5,2048,2066,-1,2477);
        _SFD_CV_INIT_EML_FOR(0,1,0,424,438,864);
        _SFD_CV_INIT_EML_FOR(0,1,1,1655,1672,2489);
        _SFD_CV_INIT_EML_FOR(0,1,2,1780,1796,2035);

        {
          static int condStart[] = { 399 };

          static int condEnd[] = { 419 };

          static int pfixExpr[] = { 0, -1 };

          _SFD_CV_INIT_EML_MCDC(0,1,0,398,419,1,0,&(condStart[0]),&(condEnd[0]),
                                2,&(pfixExpr[0]));
        }

        {
          static int condStart[] = { 1899, 1922 };

          static int condEnd[] = { 1918, 1931 };

          static int pfixExpr[] = { 0, 1, -3 };

          _SFD_CV_INIT_EML_MCDC(0,1,1,1899,1931,2,1,&(condStart[0]),&(condEnd[0]),
                                3,&(pfixExpr[0]));
        }

        _SFD_CV_INIT_EML_RELATIONAL(0,1,0,872,904,-1,4);
        _SFD_CV_INIT_EML_RELATIONAL(0,1,1,1621,1629,-1,4);
        _SFD_CV_INIT_EML_RELATIONAL(0,1,2,1637,1646,-1,4);
        _SFD_CV_INIT_EML_RELATIONAL(0,1,3,1899,1918,-1,2);
        _SFD_CV_INIT_EML_RELATIONAL(0,1,4,1922,1931,-3,2);
        _SFD_CV_INIT_EML_RELATIONAL(0,1,5,2051,2066,0,1);
        _SFD_SET_DATA_COMPILED_PROPS(0,SF_STRUCT,0,NULL,0,0,0,0.0,1.0,0,0,
          (MexFcnForType)c3_blobs_bus_io,(MexInFcnForType)NULL);
        _SFD_SET_DATA_COMPILED_PROPS(1,SF_STRUCT,0,NULL,0,0,0,0.0,1.0,0,0,
          (MexFcnForType)c3_players_bus_io,(MexInFcnForType)NULL);

        {
          unsigned int dimVector[1];
          dimVector[0]= 2;
          _SFD_SET_DATA_COMPILED_PROPS(2,SF_DOUBLE,1,&(dimVector[0]),0,0,0,0.0,
            1.0,0,0,(MexFcnForType)c3_d_sf_marshallOut,(MexInFcnForType)NULL);
        }

        _SFD_SET_DATA_COMPILED_PROPS(3,SF_DOUBLE,0,NULL,0,0,0,0.0,1.0,0,0,
          (MexFcnForType)c3_c_sf_marshallOut,(MexInFcnForType)NULL);
        _SFD_SET_DATA_COMPILED_PROPS(4,SF_STRUCT,0,NULL,0,0,0,0.0,1.0,0,0,
          (MexFcnForType)c3_Ball_bus_io,(MexInFcnForType)NULL);
        _SFD_SET_DATA_VALUE_PTR(0U, chartInstance->c3_blobs_data);
        _SFD_SET_DATA_VALUE_PTR(1U, chartInstance->c3_players_data);
        _SFD_SET_DATA_VALUE_PTR(2U, *chartInstance->c3_camRes);
        _SFD_SET_DATA_VALUE_PTR(3U, chartInstance->c3_height);
        _SFD_SET_DATA_VALUE_PTR(4U, chartInstance->c3_Ball_data);
      }
    } else {
      sf_debug_reset_current_state_configuration(sfGlobalDebugInstanceStruct,
        _ImageDetMachineNumber_,chartInstance->chartNumber,
        chartInstance->instanceNumber);
    }
  }
}

static const char* sf_get_instance_specialization(void)
{
  return "RaqAiVYus4LXUoP8LZzu3F";
}

static void sf_opaque_initialize_c3_ImageDet(void *chartInstanceVar)
{
  chart_debug_initialization(((SFc3_ImageDetInstanceStruct*) chartInstanceVar)
    ->S,0);
  initialize_params_c3_ImageDet((SFc3_ImageDetInstanceStruct*) chartInstanceVar);
  initialize_c3_ImageDet((SFc3_ImageDetInstanceStruct*) chartInstanceVar);
}

static void sf_opaque_enable_c3_ImageDet(void *chartInstanceVar)
{
  enable_c3_ImageDet((SFc3_ImageDetInstanceStruct*) chartInstanceVar);
}

static void sf_opaque_disable_c3_ImageDet(void *chartInstanceVar)
{
  disable_c3_ImageDet((SFc3_ImageDetInstanceStruct*) chartInstanceVar);
}

static void sf_opaque_gateway_c3_ImageDet(void *chartInstanceVar)
{
  sf_gateway_c3_ImageDet((SFc3_ImageDetInstanceStruct*) chartInstanceVar);
}

static const mxArray* sf_opaque_get_sim_state_c3_ImageDet(SimStruct* S)
{
  ChartRunTimeInfo * crtInfo = (ChartRunTimeInfo *)(ssGetUserData(S));
  ChartInfoStruct * chartInfo = (ChartInfoStruct *)(crtInfo->instanceInfo);
  return get_sim_state_c3_ImageDet((SFc3_ImageDetInstanceStruct*)
    chartInfo->chartInstance);         /* raw sim ctx */
}

static void sf_opaque_set_sim_state_c3_ImageDet(SimStruct* S, const mxArray *st)
{
  ChartRunTimeInfo * crtInfo = (ChartRunTimeInfo *)(ssGetUserData(S));
  ChartInfoStruct * chartInfo = (ChartInfoStruct *)(crtInfo->instanceInfo);
  set_sim_state_c3_ImageDet((SFc3_ImageDetInstanceStruct*)
    chartInfo->chartInstance, st);
}

static void sf_opaque_terminate_c3_ImageDet(void *chartInstanceVar)
{
  if (chartInstanceVar!=NULL) {
    SimStruct *S = ((SFc3_ImageDetInstanceStruct*) chartInstanceVar)->S;
    ChartRunTimeInfo * crtInfo = (ChartRunTimeInfo *)(ssGetUserData(S));
    if (sim_mode_is_rtw_gen(S) || sim_mode_is_external(S)) {
      sf_clear_rtw_identifier(S);
      unload_ImageDet_optimization_info();
    }

    finalize_c3_ImageDet((SFc3_ImageDetInstanceStruct*) chartInstanceVar);
    utFree(chartInstanceVar);
    if (crtInfo != NULL) {
      utFree(crtInfo);
    }

    ssSetUserData(S,NULL);
  }
}

static void sf_opaque_init_subchart_simstructs(void *chartInstanceVar)
{
  initSimStructsc3_ImageDet((SFc3_ImageDetInstanceStruct*) chartInstanceVar);
}

extern unsigned int sf_machine_global_initializer_called(void);
static void mdlProcessParameters_c3_ImageDet(SimStruct *S)
{
  int i;
  for (i=0;i<ssGetNumRunTimeParams(S);i++) {
    if (ssGetSFcnParamTunable(S,i)) {
      ssUpdateDlgParamAsRunTimeParam(S,i);
    }
  }

  if (sf_machine_global_initializer_called()) {
    ChartRunTimeInfo * crtInfo = (ChartRunTimeInfo *)(ssGetUserData(S));
    ChartInfoStruct * chartInfo = (ChartInfoStruct *)(crtInfo->instanceInfo);
    initialize_params_c3_ImageDet((SFc3_ImageDetInstanceStruct*)
      (chartInfo->chartInstance));
  }
}

static void mdlSetWorkWidths_c3_ImageDet(SimStruct *S)
{
  if (sim_mode_is_rtw_gen(S) || sim_mode_is_external(S)) {
    mxArray *infoStruct = load_ImageDet_optimization_info();
    int_T chartIsInlinable =
      (int_T)sf_is_chart_inlinable(sf_get_instance_specialization(),infoStruct,3);
    ssSetStateflowIsInlinable(S,chartIsInlinable);
    ssSetRTWCG(S,sf_rtw_info_uint_prop(sf_get_instance_specialization(),
                infoStruct,3,"RTWCG"));
    ssSetEnableFcnIsTrivial(S,1);
    ssSetDisableFcnIsTrivial(S,1);
    ssSetNotMultipleInlinable(S,sf_rtw_info_uint_prop
      (sf_get_instance_specialization(),infoStruct,3,
       "gatewayCannotBeInlinedMultipleTimes"));
    sf_update_buildInfo(sf_get_instance_specialization(),infoStruct,3);
    if (chartIsInlinable) {
      ssSetInputPortOptimOpts(S, 0, SS_REUSABLE_AND_LOCAL);
      ssSetInputPortOptimOpts(S, 1, SS_REUSABLE_AND_LOCAL);
      ssSetInputPortOptimOpts(S, 2, SS_REUSABLE_AND_LOCAL);
      sf_mark_chart_expressionable_inputs(S,sf_get_instance_specialization(),
        infoStruct,3,3);
      sf_mark_chart_reusable_outputs(S,sf_get_instance_specialization(),
        infoStruct,3,2);
    }

    {
      unsigned int outPortIdx;
      for (outPortIdx=1; outPortIdx<=2; ++outPortIdx) {
        ssSetOutputPortOptimizeInIR(S, outPortIdx, 1U);
      }
    }

    {
      unsigned int inPortIdx;
      for (inPortIdx=0; inPortIdx < 3; ++inPortIdx) {
        ssSetInputPortOptimizeInIR(S, inPortIdx, 1U);
      }
    }

    sf_set_rtw_dwork_info(S,sf_get_instance_specialization(),infoStruct,3);
    ssSetHasSubFunctions(S,!(chartIsInlinable));
  } else {
  }

  ssSetOptions(S,ssGetOptions(S)|SS_OPTION_WORKS_WITH_CODE_REUSE);
  ssSetChecksum0(S,(545882118U));
  ssSetChecksum1(S,(2806390254U));
  ssSetChecksum2(S,(1274122198U));
  ssSetChecksum3(S,(748200073U));
  ssSetmdlDerivatives(S, NULL);
  ssSetExplicitFCSSCtrl(S,1);
  ssSupportsMultipleExecInstances(S,1);
}

static void mdlRTW_c3_ImageDet(SimStruct *S)
{
  if (sim_mode_is_rtw_gen(S)) {
    ssWriteRTWStrParam(S, "StateflowChartType", "Embedded MATLAB");
  }
}

static void mdlStart_c3_ImageDet(SimStruct *S)
{
  SFc3_ImageDetInstanceStruct *chartInstance;
  ChartRunTimeInfo * crtInfo = (ChartRunTimeInfo *)utMalloc(sizeof
    (ChartRunTimeInfo));
  chartInstance = (SFc3_ImageDetInstanceStruct *)utMalloc(sizeof
    (SFc3_ImageDetInstanceStruct));
  memset(chartInstance, 0, sizeof(SFc3_ImageDetInstanceStruct));
  if (chartInstance==NULL) {
    sf_mex_error_message("Could not allocate memory for chart instance.");
  }

  chartInstance->chartInfo.chartInstance = chartInstance;
  chartInstance->chartInfo.isEMLChart = 1;
  chartInstance->chartInfo.chartInitialized = 0;
  chartInstance->chartInfo.sFunctionGateway = sf_opaque_gateway_c3_ImageDet;
  chartInstance->chartInfo.initializeChart = sf_opaque_initialize_c3_ImageDet;
  chartInstance->chartInfo.terminateChart = sf_opaque_terminate_c3_ImageDet;
  chartInstance->chartInfo.enableChart = sf_opaque_enable_c3_ImageDet;
  chartInstance->chartInfo.disableChart = sf_opaque_disable_c3_ImageDet;
  chartInstance->chartInfo.getSimState = sf_opaque_get_sim_state_c3_ImageDet;
  chartInstance->chartInfo.setSimState = sf_opaque_set_sim_state_c3_ImageDet;
  chartInstance->chartInfo.getSimStateInfo = sf_get_sim_state_info_c3_ImageDet;
  chartInstance->chartInfo.zeroCrossings = NULL;
  chartInstance->chartInfo.outputs = NULL;
  chartInstance->chartInfo.derivatives = NULL;
  chartInstance->chartInfo.mdlRTW = mdlRTW_c3_ImageDet;
  chartInstance->chartInfo.mdlStart = mdlStart_c3_ImageDet;
  chartInstance->chartInfo.mdlSetWorkWidths = mdlSetWorkWidths_c3_ImageDet;
  chartInstance->chartInfo.extModeExec = NULL;
  chartInstance->chartInfo.restoreLastMajorStepConfiguration = NULL;
  chartInstance->chartInfo.restoreBeforeLastMajorStepConfiguration = NULL;
  chartInstance->chartInfo.storeCurrentConfiguration = NULL;
  chartInstance->chartInfo.callAtomicSubchartUserFcn = NULL;
  chartInstance->chartInfo.callAtomicSubchartAutoFcn = NULL;
  chartInstance->chartInfo.debugInstance = sfGlobalDebugInstanceStruct;
  chartInstance->S = S;
  crtInfo->checksum = SF_RUNTIME_INFO_CHECKSUM;
  crtInfo->instanceInfo = (&(chartInstance->chartInfo));
  crtInfo->isJITEnabled = false;
  crtInfo->compiledInfo = NULL;
  ssSetUserData(S,(void *)(crtInfo));  /* register the chart instance with simstruct */
  init_dsm_address_info(chartInstance);
  init_simulink_io_address(chartInstance);
  if (!sim_mode_is_rtw_gen(S)) {
  }

  sf_opaque_init_subchart_simstructs(chartInstance->chartInfo.chartInstance);
  chart_debug_initialization(S,1);
}

void c3_ImageDet_method_dispatcher(SimStruct *S, int_T method, void *data)
{
  switch (method) {
   case SS_CALL_MDL_START:
    mdlStart_c3_ImageDet(S);
    break;

   case SS_CALL_MDL_SET_WORK_WIDTHS:
    mdlSetWorkWidths_c3_ImageDet(S);
    break;

   case SS_CALL_MDL_PROCESS_PARAMETERS:
    mdlProcessParameters_c3_ImageDet(S);
    break;

   default:
    /* Unhandled method */
    sf_mex_error_message("Stateflow Internal Error:\n"
                         "Error calling c3_ImageDet_method_dispatcher.\n"
                         "Can't handle method %d.\n", method);
    break;
  }
}

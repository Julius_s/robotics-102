/* Include files */

#include "ImageDet_sfun.h"
#include "ImageDet_sfun_debug_macros.h"
#include "c1_ImageDet.h"
#include "c3_ImageDet.h"
#include "c4_ImageDet.h"
#include "c5_ImageDet.h"
#include "c6_ImageDet.h"
#include "c8_ImageDet.h"
#include "c9_ImageDet.h"
#include "c10_ImageDet.h"
#include "c11_ImageDet.h"
#include "c12_ImageDet.h"

/* Type Definitions */

/* Named Constants */

/* Variable Declarations */

/* Variable Definitions */
uint32_T _ImageDetMachineNumber_;

/* Function Declarations */

/* Function Definitions */
void ImageDet_initializer(void)
{
}

void ImageDet_terminator(void)
{
}

/* SFunction Glue Code */
unsigned int sf_ImageDet_method_dispatcher(SimStruct *simstructPtr, unsigned int
  chartFileNumber, const char* specsCksum, int_T method, void *data)
{
  if (chartFileNumber==1) {
    c1_ImageDet_method_dispatcher(simstructPtr, method, data);
    return 1;
  }

  if (chartFileNumber==3) {
    c3_ImageDet_method_dispatcher(simstructPtr, method, data);
    return 1;
  }

  if (chartFileNumber==4) {
    c4_ImageDet_method_dispatcher(simstructPtr, method, data);
    return 1;
  }

  if (chartFileNumber==5) {
    c5_ImageDet_method_dispatcher(simstructPtr, method, data);
    return 1;
  }

  if (chartFileNumber==6) {
    c6_ImageDet_method_dispatcher(simstructPtr, method, data);
    return 1;
  }

  if (chartFileNumber==8) {
    c8_ImageDet_method_dispatcher(simstructPtr, method, data);
    return 1;
  }

  if (chartFileNumber==9) {
    c9_ImageDet_method_dispatcher(simstructPtr, method, data);
    return 1;
  }

  if (chartFileNumber==10) {
    c10_ImageDet_method_dispatcher(simstructPtr, method, data);
    return 1;
  }

  if (chartFileNumber==11) {
    c11_ImageDet_method_dispatcher(simstructPtr, method, data);
    return 1;
  }

  if (chartFileNumber==12) {
    c12_ImageDet_method_dispatcher(simstructPtr, method, data);
    return 1;
  }

  return 0;
}

extern void sf_ImageDet_uses_exported_functions(int nlhs, mxArray * plhs[], int
  nrhs, const mxArray * prhs[])
{
  plhs[0] = mxCreateLogicalScalar(0);
}

unsigned int sf_ImageDet_process_testpoint_info_call( int nlhs, mxArray * plhs[],
  int nrhs, const mxArray * prhs[] )
{

#ifdef MATLAB_MEX_FILE

  char commandName[32];
  char machineName[128];
  if (nrhs < 3 || !mxIsChar(prhs[0]) || !mxIsChar(prhs[1]))
    return 0;

  /* Possible call to get testpoint info. */
  mxGetString(prhs[0], commandName,sizeof(commandName)/sizeof(char));
  commandName[(sizeof(commandName)/sizeof(char)-1)] = '\0';
  if (strcmp(commandName,"get_testpoint_info"))
    return 0;
  mxGetString(prhs[1], machineName, sizeof(machineName)/sizeof(char));
  machineName[(sizeof(machineName)/sizeof(char)-1)] = '\0';
  if (!strcmp(machineName, "ImageDet")) {
    unsigned int chartFileNumber;
    chartFileNumber = (unsigned int)mxGetScalar(prhs[2]);
    switch (chartFileNumber) {
     case 10:
      {
        extern mxArray *sf_c10_ImageDet_get_testpoint_info(void);
        plhs[0] = sf_c10_ImageDet_get_testpoint_info();
        break;
      }

     default:
      plhs[0] = mxCreateDoubleMatrix(0,0,mxREAL);
    }

    return 1;
  }

  return 0;

#else

  return 0;

#endif

}

unsigned int sf_ImageDet_process_check_sum_call( int nlhs, mxArray * plhs[], int
  nrhs, const mxArray * prhs[] )
{

#ifdef MATLAB_MEX_FILE

  char commandName[20];
  if (nrhs<1 || !mxIsChar(prhs[0]) )
    return 0;

  /* Possible call to get the checksum */
  mxGetString(prhs[0], commandName,sizeof(commandName)/sizeof(char));
  commandName[(sizeof(commandName)/sizeof(char)-1)] = '\0';
  if (strcmp(commandName,"sf_get_check_sum"))
    return 0;
  plhs[0] = mxCreateDoubleMatrix( 1,4,mxREAL);
  if (nrhs>1 && mxIsChar(prhs[1])) {
    mxGetString(prhs[1], commandName,sizeof(commandName)/sizeof(char));
    commandName[(sizeof(commandName)/sizeof(char)-1)] = '\0';
    if (!strcmp(commandName,"machine")) {
      ((real_T *)mxGetPr((plhs[0])))[0] = (real_T)(931244039U);
      ((real_T *)mxGetPr((plhs[0])))[1] = (real_T)(2078949664U);
      ((real_T *)mxGetPr((plhs[0])))[2] = (real_T)(4001656157U);
      ((real_T *)mxGetPr((plhs[0])))[3] = (real_T)(1356403870U);
    } else if (!strcmp(commandName,"exportedFcn")) {
      ((real_T *)mxGetPr((plhs[0])))[0] = (real_T)(0U);
      ((real_T *)mxGetPr((plhs[0])))[1] = (real_T)(0U);
      ((real_T *)mxGetPr((plhs[0])))[2] = (real_T)(0U);
      ((real_T *)mxGetPr((plhs[0])))[3] = (real_T)(0U);
    } else if (!strcmp(commandName,"makefile")) {
      ((real_T *)mxGetPr((plhs[0])))[0] = (real_T)(181086964U);
      ((real_T *)mxGetPr((plhs[0])))[1] = (real_T)(3004702921U);
      ((real_T *)mxGetPr((plhs[0])))[2] = (real_T)(2782343253U);
      ((real_T *)mxGetPr((plhs[0])))[3] = (real_T)(257767180U);
    } else if (nrhs==3 && !strcmp(commandName,"chart")) {
      unsigned int chartFileNumber;
      chartFileNumber = (unsigned int)mxGetScalar(prhs[2]);
      switch (chartFileNumber) {
       case 1:
        {
          extern void sf_c1_ImageDet_get_check_sum(mxArray *plhs[]);
          sf_c1_ImageDet_get_check_sum(plhs);
          break;
        }

       case 3:
        {
          extern void sf_c3_ImageDet_get_check_sum(mxArray *plhs[]);
          sf_c3_ImageDet_get_check_sum(plhs);
          break;
        }

       case 4:
        {
          extern void sf_c4_ImageDet_get_check_sum(mxArray *plhs[]);
          sf_c4_ImageDet_get_check_sum(plhs);
          break;
        }

       case 5:
        {
          extern void sf_c5_ImageDet_get_check_sum(mxArray *plhs[]);
          sf_c5_ImageDet_get_check_sum(plhs);
          break;
        }

       case 6:
        {
          extern void sf_c6_ImageDet_get_check_sum(mxArray *plhs[]);
          sf_c6_ImageDet_get_check_sum(plhs);
          break;
        }

       case 8:
        {
          extern void sf_c8_ImageDet_get_check_sum(mxArray *plhs[]);
          sf_c8_ImageDet_get_check_sum(plhs);
          break;
        }

       case 9:
        {
          extern void sf_c9_ImageDet_get_check_sum(mxArray *plhs[]);
          sf_c9_ImageDet_get_check_sum(plhs);
          break;
        }

       case 10:
        {
          extern void sf_c10_ImageDet_get_check_sum(mxArray *plhs[]);
          sf_c10_ImageDet_get_check_sum(plhs);
          break;
        }

       case 11:
        {
          extern void sf_c11_ImageDet_get_check_sum(mxArray *plhs[]);
          sf_c11_ImageDet_get_check_sum(plhs);
          break;
        }

       case 12:
        {
          extern void sf_c12_ImageDet_get_check_sum(mxArray *plhs[]);
          sf_c12_ImageDet_get_check_sum(plhs);
          break;
        }

       default:
        ((real_T *)mxGetPr((plhs[0])))[0] = (real_T)(0.0);
        ((real_T *)mxGetPr((plhs[0])))[1] = (real_T)(0.0);
        ((real_T *)mxGetPr((plhs[0])))[2] = (real_T)(0.0);
        ((real_T *)mxGetPr((plhs[0])))[3] = (real_T)(0.0);
      }
    } else if (!strcmp(commandName,"target")) {
      ((real_T *)mxGetPr((plhs[0])))[0] = (real_T)(1079105835U);
      ((real_T *)mxGetPr((plhs[0])))[1] = (real_T)(4093772408U);
      ((real_T *)mxGetPr((plhs[0])))[2] = (real_T)(525736737U);
      ((real_T *)mxGetPr((plhs[0])))[3] = (real_T)(2157381758U);
    } else {
      return 0;
    }
  } else {
    ((real_T *)mxGetPr((plhs[0])))[0] = (real_T)(1034590669U);
    ((real_T *)mxGetPr((plhs[0])))[1] = (real_T)(3451319387U);
    ((real_T *)mxGetPr((plhs[0])))[2] = (real_T)(516780157U);
    ((real_T *)mxGetPr((plhs[0])))[3] = (real_T)(1652888561U);
  }

  return 1;

#else

  return 0;

#endif

}

unsigned int sf_ImageDet_autoinheritance_info( int nlhs, mxArray * plhs[], int
  nrhs, const mxArray * prhs[] )
{

#ifdef MATLAB_MEX_FILE

  char commandName[32];
  char aiChksum[64];
  if (nrhs<3 || !mxIsChar(prhs[0]) )
    return 0;

  /* Possible call to get the autoinheritance_info */
  mxGetString(prhs[0], commandName,sizeof(commandName)/sizeof(char));
  commandName[(sizeof(commandName)/sizeof(char)-1)] = '\0';
  if (strcmp(commandName,"get_autoinheritance_info"))
    return 0;
  mxGetString(prhs[2], aiChksum,sizeof(aiChksum)/sizeof(char));
  aiChksum[(sizeof(aiChksum)/sizeof(char)-1)] = '\0';

  {
    unsigned int chartFileNumber;
    chartFileNumber = (unsigned int)mxGetScalar(prhs[1]);
    switch (chartFileNumber) {
     case 1:
      {
        if (strcmp(aiChksum, "w2vRmMYgs91tUddE44mv0E") == 0) {
          extern mxArray *sf_c1_ImageDet_get_autoinheritance_info(void);
          plhs[0] = sf_c1_ImageDet_get_autoinheritance_info();
          break;
        }

        plhs[0] = mxCreateDoubleMatrix(0,0,mxREAL);
        break;
      }

     case 3:
      {
        if (strcmp(aiChksum, "pAWtn4SYXmZJILo3KvFhzB") == 0) {
          extern mxArray *sf_c3_ImageDet_get_autoinheritance_info(void);
          plhs[0] = sf_c3_ImageDet_get_autoinheritance_info();
          break;
        }

        plhs[0] = mxCreateDoubleMatrix(0,0,mxREAL);
        break;
      }

     case 4:
      {
        if (strcmp(aiChksum, "M60qDZ5LK98SzGRweJTTPG") == 0) {
          extern mxArray *sf_c4_ImageDet_get_autoinheritance_info(void);
          plhs[0] = sf_c4_ImageDet_get_autoinheritance_info();
          break;
        }

        plhs[0] = mxCreateDoubleMatrix(0,0,mxREAL);
        break;
      }

     case 5:
      {
        if (strcmp(aiChksum, "iDJyvjMVCb74ZpvVxe3sSG") == 0) {
          extern mxArray *sf_c5_ImageDet_get_autoinheritance_info(void);
          plhs[0] = sf_c5_ImageDet_get_autoinheritance_info();
          break;
        }

        plhs[0] = mxCreateDoubleMatrix(0,0,mxREAL);
        break;
      }

     case 6:
      {
        if (strcmp(aiChksum, "h96JBYlffGxFvL85jAnRt") == 0) {
          extern mxArray *sf_c6_ImageDet_get_autoinheritance_info(void);
          plhs[0] = sf_c6_ImageDet_get_autoinheritance_info();
          break;
        }

        plhs[0] = mxCreateDoubleMatrix(0,0,mxREAL);
        break;
      }

     case 8:
      {
        if (strcmp(aiChksum, "VdrxMAer3s7QjCXpfytsgG") == 0) {
          extern mxArray *sf_c8_ImageDet_get_autoinheritance_info(void);
          plhs[0] = sf_c8_ImageDet_get_autoinheritance_info();
          break;
        }

        plhs[0] = mxCreateDoubleMatrix(0,0,mxREAL);
        break;
      }

     case 9:
      {
        if (strcmp(aiChksum, "ArIrdR8KduvZC6FfJiWpp") == 0) {
          extern mxArray *sf_c9_ImageDet_get_autoinheritance_info(void);
          plhs[0] = sf_c9_ImageDet_get_autoinheritance_info();
          break;
        }

        plhs[0] = mxCreateDoubleMatrix(0,0,mxREAL);
        break;
      }

     case 10:
      {
        if (strcmp(aiChksum, "5GCUruyFPNfSoASjim60bB") == 0) {
          extern mxArray *sf_c10_ImageDet_get_autoinheritance_info(void);
          plhs[0] = sf_c10_ImageDet_get_autoinheritance_info();
          break;
        }

        plhs[0] = mxCreateDoubleMatrix(0,0,mxREAL);
        break;
      }

     case 11:
      {
        if (strcmp(aiChksum, "te9YSTCHh6bbb6RnbjN6wG") == 0) {
          extern mxArray *sf_c11_ImageDet_get_autoinheritance_info(void);
          plhs[0] = sf_c11_ImageDet_get_autoinheritance_info();
          break;
        }

        plhs[0] = mxCreateDoubleMatrix(0,0,mxREAL);
        break;
      }

     case 12:
      {
        if (strcmp(aiChksum, "qprmGTYKmcaG38Z6iEUHKE") == 0) {
          extern mxArray *sf_c12_ImageDet_get_autoinheritance_info(void);
          plhs[0] = sf_c12_ImageDet_get_autoinheritance_info();
          break;
        }

        plhs[0] = mxCreateDoubleMatrix(0,0,mxREAL);
        break;
      }

     default:
      plhs[0] = mxCreateDoubleMatrix(0,0,mxREAL);
    }
  }

  return 1;

#else

  return 0;

#endif

}

unsigned int sf_ImageDet_get_eml_resolved_functions_info( int nlhs, mxArray *
  plhs[], int nrhs, const mxArray * prhs[] )
{

#ifdef MATLAB_MEX_FILE

  char commandName[64];
  if (nrhs<2 || !mxIsChar(prhs[0]))
    return 0;

  /* Possible call to get the get_eml_resolved_functions_info */
  mxGetString(prhs[0], commandName,sizeof(commandName)/sizeof(char));
  commandName[(sizeof(commandName)/sizeof(char)-1)] = '\0';
  if (strcmp(commandName,"get_eml_resolved_functions_info"))
    return 0;

  {
    unsigned int chartFileNumber;
    chartFileNumber = (unsigned int)mxGetScalar(prhs[1]);
    switch (chartFileNumber) {
     case 1:
      {
        extern const mxArray *sf_c1_ImageDet_get_eml_resolved_functions_info
          (void);
        mxArray *persistentMxArray = (mxArray *)
          sf_c1_ImageDet_get_eml_resolved_functions_info();
        plhs[0] = mxDuplicateArray(persistentMxArray);
        mxDestroyArray(persistentMxArray);
        break;
      }

     case 3:
      {
        extern const mxArray *sf_c3_ImageDet_get_eml_resolved_functions_info
          (void);
        mxArray *persistentMxArray = (mxArray *)
          sf_c3_ImageDet_get_eml_resolved_functions_info();
        plhs[0] = mxDuplicateArray(persistentMxArray);
        mxDestroyArray(persistentMxArray);
        break;
      }

     case 4:
      {
        extern const mxArray *sf_c4_ImageDet_get_eml_resolved_functions_info
          (void);
        mxArray *persistentMxArray = (mxArray *)
          sf_c4_ImageDet_get_eml_resolved_functions_info();
        plhs[0] = mxDuplicateArray(persistentMxArray);
        mxDestroyArray(persistentMxArray);
        break;
      }

     case 5:
      {
        extern const mxArray *sf_c5_ImageDet_get_eml_resolved_functions_info
          (void);
        mxArray *persistentMxArray = (mxArray *)
          sf_c5_ImageDet_get_eml_resolved_functions_info();
        plhs[0] = mxDuplicateArray(persistentMxArray);
        mxDestroyArray(persistentMxArray);
        break;
      }

     case 6:
      {
        extern const mxArray *sf_c6_ImageDet_get_eml_resolved_functions_info
          (void);
        mxArray *persistentMxArray = (mxArray *)
          sf_c6_ImageDet_get_eml_resolved_functions_info();
        plhs[0] = mxDuplicateArray(persistentMxArray);
        mxDestroyArray(persistentMxArray);
        break;
      }

     case 8:
      {
        extern const mxArray *sf_c8_ImageDet_get_eml_resolved_functions_info
          (void);
        mxArray *persistentMxArray = (mxArray *)
          sf_c8_ImageDet_get_eml_resolved_functions_info();
        plhs[0] = mxDuplicateArray(persistentMxArray);
        mxDestroyArray(persistentMxArray);
        break;
      }

     case 9:
      {
        extern const mxArray *sf_c9_ImageDet_get_eml_resolved_functions_info
          (void);
        mxArray *persistentMxArray = (mxArray *)
          sf_c9_ImageDet_get_eml_resolved_functions_info();
        plhs[0] = mxDuplicateArray(persistentMxArray);
        mxDestroyArray(persistentMxArray);
        break;
      }

     case 10:
      {
        extern const mxArray *sf_c10_ImageDet_get_eml_resolved_functions_info
          (void);
        mxArray *persistentMxArray = (mxArray *)
          sf_c10_ImageDet_get_eml_resolved_functions_info();
        plhs[0] = mxDuplicateArray(persistentMxArray);
        mxDestroyArray(persistentMxArray);
        break;
      }

     case 11:
      {
        extern const mxArray *sf_c11_ImageDet_get_eml_resolved_functions_info
          (void);
        mxArray *persistentMxArray = (mxArray *)
          sf_c11_ImageDet_get_eml_resolved_functions_info();
        plhs[0] = mxDuplicateArray(persistentMxArray);
        mxDestroyArray(persistentMxArray);
        break;
      }

     case 12:
      {
        extern const mxArray *sf_c12_ImageDet_get_eml_resolved_functions_info
          (void);
        mxArray *persistentMxArray = (mxArray *)
          sf_c12_ImageDet_get_eml_resolved_functions_info();
        plhs[0] = mxDuplicateArray(persistentMxArray);
        mxDestroyArray(persistentMxArray);
        break;
      }

     default:
      plhs[0] = mxCreateDoubleMatrix(0,0,mxREAL);
    }
  }

  return 1;

#else

  return 0;

#endif

}

unsigned int sf_ImageDet_third_party_uses_info( int nlhs, mxArray * plhs[], int
  nrhs, const mxArray * prhs[] )
{
  char commandName[64];
  char tpChksum[64];
  if (nrhs<3 || !mxIsChar(prhs[0]))
    return 0;

  /* Possible call to get the third_party_uses_info */
  mxGetString(prhs[0], commandName,sizeof(commandName)/sizeof(char));
  commandName[(sizeof(commandName)/sizeof(char)-1)] = '\0';
  mxGetString(prhs[2], tpChksum,sizeof(tpChksum)/sizeof(char));
  tpChksum[(sizeof(tpChksum)/sizeof(char)-1)] = '\0';
  if (strcmp(commandName,"get_third_party_uses_info"))
    return 0;

  {
    unsigned int chartFileNumber;
    chartFileNumber = (unsigned int)mxGetScalar(prhs[1]);
    switch (chartFileNumber) {
     case 1:
      {
        if (strcmp(tpChksum, "LiNDKp54yE9FUToGpNz85D") == 0) {
          extern mxArray *sf_c1_ImageDet_third_party_uses_info(void);
          plhs[0] = sf_c1_ImageDet_third_party_uses_info();
          break;
        }
      }

     case 3:
      {
        if (strcmp(tpChksum, "RaqAiVYus4LXUoP8LZzu3F") == 0) {
          extern mxArray *sf_c3_ImageDet_third_party_uses_info(void);
          plhs[0] = sf_c3_ImageDet_third_party_uses_info();
          break;
        }
      }

     case 4:
      {
        if (strcmp(tpChksum, "VgPcjGomSjmmrIUeZvuvUB") == 0) {
          extern mxArray *sf_c4_ImageDet_third_party_uses_info(void);
          plhs[0] = sf_c4_ImageDet_third_party_uses_info();
          break;
        }
      }

     case 5:
      {
        if (strcmp(tpChksum, "UAjSElHQfBjUmqpqKewW3") == 0) {
          extern mxArray *sf_c5_ImageDet_third_party_uses_info(void);
          plhs[0] = sf_c5_ImageDet_third_party_uses_info();
          break;
        }
      }

     case 6:
      {
        if (strcmp(tpChksum, "x4XA89MNqPjioxDjYjI5IB") == 0) {
          extern mxArray *sf_c6_ImageDet_third_party_uses_info(void);
          plhs[0] = sf_c6_ImageDet_third_party_uses_info();
          break;
        }
      }

     case 8:
      {
        if (strcmp(tpChksum, "NJqXvmbUOybOCqj85gZKnE") == 0) {
          extern mxArray *sf_c8_ImageDet_third_party_uses_info(void);
          plhs[0] = sf_c8_ImageDet_third_party_uses_info();
          break;
        }
      }

     case 9:
      {
        if (strcmp(tpChksum, "cFtwoRDsiuvx30E55B2oa") == 0) {
          extern mxArray *sf_c9_ImageDet_third_party_uses_info(void);
          plhs[0] = sf_c9_ImageDet_third_party_uses_info();
          break;
        }
      }

     case 10:
      {
        if (strcmp(tpChksum, "3lr4VjdexkDRLGej5QFKzF") == 0) {
          extern mxArray *sf_c10_ImageDet_third_party_uses_info(void);
          plhs[0] = sf_c10_ImageDet_third_party_uses_info();
          break;
        }
      }

     case 11:
      {
        if (strcmp(tpChksum, "XCbTeHJvJHOL3MnzDuHjME") == 0) {
          extern mxArray *sf_c11_ImageDet_third_party_uses_info(void);
          plhs[0] = sf_c11_ImageDet_third_party_uses_info();
          break;
        }
      }

     case 12:
      {
        if (strcmp(tpChksum, "e49UtqwYFsFJgVAtuSPeMF") == 0) {
          extern mxArray *sf_c12_ImageDet_third_party_uses_info(void);
          plhs[0] = sf_c12_ImageDet_third_party_uses_info();
          break;
        }
      }

     default:
      plhs[0] = mxCreateDoubleMatrix(0,0,mxREAL);
    }
  }

  return 1;
}

unsigned int sf_ImageDet_jit_fallback_info( int nlhs, mxArray * plhs[], int nrhs,
  const mxArray * prhs[] )
{
  char commandName[64];
  char tpChksum[64];
  if (nrhs<3 || !mxIsChar(prhs[0]))
    return 0;

  /* Possible call to get the jit_fallback_info */
  mxGetString(prhs[0], commandName,sizeof(commandName)/sizeof(char));
  commandName[(sizeof(commandName)/sizeof(char)-1)] = '\0';
  mxGetString(prhs[2], tpChksum,sizeof(tpChksum)/sizeof(char));
  tpChksum[(sizeof(tpChksum)/sizeof(char)-1)] = '\0';
  if (strcmp(commandName,"get_jit_fallback_info"))
    return 0;

  {
    unsigned int chartFileNumber;
    chartFileNumber = (unsigned int)mxGetScalar(prhs[1]);
    switch (chartFileNumber) {
     case 1:
      {
        if (strcmp(tpChksum, "LiNDKp54yE9FUToGpNz85D") == 0) {
          extern mxArray *sf_c1_ImageDet_jit_fallback_info(void);
          plhs[0] = sf_c1_ImageDet_jit_fallback_info();
          break;
        }
      }

     case 3:
      {
        if (strcmp(tpChksum, "RaqAiVYus4LXUoP8LZzu3F") == 0) {
          extern mxArray *sf_c3_ImageDet_jit_fallback_info(void);
          plhs[0] = sf_c3_ImageDet_jit_fallback_info();
          break;
        }
      }

     case 4:
      {
        if (strcmp(tpChksum, "VgPcjGomSjmmrIUeZvuvUB") == 0) {
          extern mxArray *sf_c4_ImageDet_jit_fallback_info(void);
          plhs[0] = sf_c4_ImageDet_jit_fallback_info();
          break;
        }
      }

     case 5:
      {
        if (strcmp(tpChksum, "UAjSElHQfBjUmqpqKewW3") == 0) {
          extern mxArray *sf_c5_ImageDet_jit_fallback_info(void);
          plhs[0] = sf_c5_ImageDet_jit_fallback_info();
          break;
        }
      }

     case 6:
      {
        if (strcmp(tpChksum, "x4XA89MNqPjioxDjYjI5IB") == 0) {
          extern mxArray *sf_c6_ImageDet_jit_fallback_info(void);
          plhs[0] = sf_c6_ImageDet_jit_fallback_info();
          break;
        }
      }

     case 8:
      {
        if (strcmp(tpChksum, "NJqXvmbUOybOCqj85gZKnE") == 0) {
          extern mxArray *sf_c8_ImageDet_jit_fallback_info(void);
          plhs[0] = sf_c8_ImageDet_jit_fallback_info();
          break;
        }
      }

     case 9:
      {
        if (strcmp(tpChksum, "cFtwoRDsiuvx30E55B2oa") == 0) {
          extern mxArray *sf_c9_ImageDet_jit_fallback_info(void);
          plhs[0] = sf_c9_ImageDet_jit_fallback_info();
          break;
        }
      }

     case 10:
      {
        if (strcmp(tpChksum, "3lr4VjdexkDRLGej5QFKzF") == 0) {
          extern mxArray *sf_c10_ImageDet_jit_fallback_info(void);
          plhs[0] = sf_c10_ImageDet_jit_fallback_info();
          break;
        }
      }

     case 11:
      {
        if (strcmp(tpChksum, "XCbTeHJvJHOL3MnzDuHjME") == 0) {
          extern mxArray *sf_c11_ImageDet_jit_fallback_info(void);
          plhs[0] = sf_c11_ImageDet_jit_fallback_info();
          break;
        }
      }

     case 12:
      {
        if (strcmp(tpChksum, "e49UtqwYFsFJgVAtuSPeMF") == 0) {
          extern mxArray *sf_c12_ImageDet_jit_fallback_info(void);
          plhs[0] = sf_c12_ImageDet_jit_fallback_info();
          break;
        }
      }

     default:
      plhs[0] = mxCreateDoubleMatrix(0,0,mxREAL);
    }
  }

  return 1;
}

unsigned int sf_ImageDet_updateBuildInfo_args_info( int nlhs, mxArray * plhs[],
  int nrhs, const mxArray * prhs[] )
{
  char commandName[64];
  char tpChksum[64];
  if (nrhs<3 || !mxIsChar(prhs[0]))
    return 0;

  /* Possible call to get the updateBuildInfo_args_info */
  mxGetString(prhs[0], commandName,sizeof(commandName)/sizeof(char));
  commandName[(sizeof(commandName)/sizeof(char)-1)] = '\0';
  mxGetString(prhs[2], tpChksum,sizeof(tpChksum)/sizeof(char));
  tpChksum[(sizeof(tpChksum)/sizeof(char)-1)] = '\0';
  if (strcmp(commandName,"get_updateBuildInfo_args_info"))
    return 0;

  {
    unsigned int chartFileNumber;
    chartFileNumber = (unsigned int)mxGetScalar(prhs[1]);
    switch (chartFileNumber) {
     case 1:
      {
        if (strcmp(tpChksum, "LiNDKp54yE9FUToGpNz85D") == 0) {
          extern mxArray *sf_c1_ImageDet_updateBuildInfo_args_info(void);
          plhs[0] = sf_c1_ImageDet_updateBuildInfo_args_info();
          break;
        }
      }

     case 3:
      {
        if (strcmp(tpChksum, "RaqAiVYus4LXUoP8LZzu3F") == 0) {
          extern mxArray *sf_c3_ImageDet_updateBuildInfo_args_info(void);
          plhs[0] = sf_c3_ImageDet_updateBuildInfo_args_info();
          break;
        }
      }

     case 4:
      {
        if (strcmp(tpChksum, "VgPcjGomSjmmrIUeZvuvUB") == 0) {
          extern mxArray *sf_c4_ImageDet_updateBuildInfo_args_info(void);
          plhs[0] = sf_c4_ImageDet_updateBuildInfo_args_info();
          break;
        }
      }

     case 5:
      {
        if (strcmp(tpChksum, "UAjSElHQfBjUmqpqKewW3") == 0) {
          extern mxArray *sf_c5_ImageDet_updateBuildInfo_args_info(void);
          plhs[0] = sf_c5_ImageDet_updateBuildInfo_args_info();
          break;
        }
      }

     case 6:
      {
        if (strcmp(tpChksum, "x4XA89MNqPjioxDjYjI5IB") == 0) {
          extern mxArray *sf_c6_ImageDet_updateBuildInfo_args_info(void);
          plhs[0] = sf_c6_ImageDet_updateBuildInfo_args_info();
          break;
        }
      }

     case 8:
      {
        if (strcmp(tpChksum, "NJqXvmbUOybOCqj85gZKnE") == 0) {
          extern mxArray *sf_c8_ImageDet_updateBuildInfo_args_info(void);
          plhs[0] = sf_c8_ImageDet_updateBuildInfo_args_info();
          break;
        }
      }

     case 9:
      {
        if (strcmp(tpChksum, "cFtwoRDsiuvx30E55B2oa") == 0) {
          extern mxArray *sf_c9_ImageDet_updateBuildInfo_args_info(void);
          plhs[0] = sf_c9_ImageDet_updateBuildInfo_args_info();
          break;
        }
      }

     case 10:
      {
        if (strcmp(tpChksum, "3lr4VjdexkDRLGej5QFKzF") == 0) {
          extern mxArray *sf_c10_ImageDet_updateBuildInfo_args_info(void);
          plhs[0] = sf_c10_ImageDet_updateBuildInfo_args_info();
          break;
        }
      }

     case 11:
      {
        if (strcmp(tpChksum, "XCbTeHJvJHOL3MnzDuHjME") == 0) {
          extern mxArray *sf_c11_ImageDet_updateBuildInfo_args_info(void);
          plhs[0] = sf_c11_ImageDet_updateBuildInfo_args_info();
          break;
        }
      }

     case 12:
      {
        if (strcmp(tpChksum, "e49UtqwYFsFJgVAtuSPeMF") == 0) {
          extern mxArray *sf_c12_ImageDet_updateBuildInfo_args_info(void);
          plhs[0] = sf_c12_ImageDet_updateBuildInfo_args_info();
          break;
        }
      }

     default:
      plhs[0] = mxCreateDoubleMatrix(0,0,mxREAL);
    }
  }

  return 1;
}

void sf_ImageDet_get_post_codegen_info( int nlhs, mxArray * plhs[], int nrhs,
  const mxArray * prhs[] )
{
  unsigned int chartFileNumber = (unsigned int) mxGetScalar(prhs[0]);
  char tpChksum[64];
  mxGetString(prhs[1], tpChksum,sizeof(tpChksum)/sizeof(char));
  tpChksum[(sizeof(tpChksum)/sizeof(char)-1)] = '\0';
  switch (chartFileNumber) {
   case 1:
    {
      if (strcmp(tpChksum, "LiNDKp54yE9FUToGpNz85D") == 0) {
        extern mxArray *sf_c1_ImageDet_get_post_codegen_info(void);
        plhs[0] = sf_c1_ImageDet_get_post_codegen_info();
        return;
      }
    }
    break;

   case 3:
    {
      if (strcmp(tpChksum, "RaqAiVYus4LXUoP8LZzu3F") == 0) {
        extern mxArray *sf_c3_ImageDet_get_post_codegen_info(void);
        plhs[0] = sf_c3_ImageDet_get_post_codegen_info();
        return;
      }
    }
    break;

   case 4:
    {
      if (strcmp(tpChksum, "VgPcjGomSjmmrIUeZvuvUB") == 0) {
        extern mxArray *sf_c4_ImageDet_get_post_codegen_info(void);
        plhs[0] = sf_c4_ImageDet_get_post_codegen_info();
        return;
      }
    }
    break;

   case 5:
    {
      if (strcmp(tpChksum, "UAjSElHQfBjUmqpqKewW3") == 0) {
        extern mxArray *sf_c5_ImageDet_get_post_codegen_info(void);
        plhs[0] = sf_c5_ImageDet_get_post_codegen_info();
        return;
      }
    }
    break;

   case 6:
    {
      if (strcmp(tpChksum, "x4XA89MNqPjioxDjYjI5IB") == 0) {
        extern mxArray *sf_c6_ImageDet_get_post_codegen_info(void);
        plhs[0] = sf_c6_ImageDet_get_post_codegen_info();
        return;
      }
    }
    break;

   case 8:
    {
      if (strcmp(tpChksum, "NJqXvmbUOybOCqj85gZKnE") == 0) {
        extern mxArray *sf_c8_ImageDet_get_post_codegen_info(void);
        plhs[0] = sf_c8_ImageDet_get_post_codegen_info();
        return;
      }
    }
    break;

   case 9:
    {
      if (strcmp(tpChksum, "cFtwoRDsiuvx30E55B2oa") == 0) {
        extern mxArray *sf_c9_ImageDet_get_post_codegen_info(void);
        plhs[0] = sf_c9_ImageDet_get_post_codegen_info();
        return;
      }
    }
    break;

   case 10:
    {
      if (strcmp(tpChksum, "3lr4VjdexkDRLGej5QFKzF") == 0) {
        extern mxArray *sf_c10_ImageDet_get_post_codegen_info(void);
        plhs[0] = sf_c10_ImageDet_get_post_codegen_info();
        return;
      }
    }
    break;

   case 11:
    {
      if (strcmp(tpChksum, "XCbTeHJvJHOL3MnzDuHjME") == 0) {
        extern mxArray *sf_c11_ImageDet_get_post_codegen_info(void);
        plhs[0] = sf_c11_ImageDet_get_post_codegen_info();
        return;
      }
    }
    break;

   case 12:
    {
      if (strcmp(tpChksum, "e49UtqwYFsFJgVAtuSPeMF") == 0) {
        extern mxArray *sf_c12_ImageDet_get_post_codegen_info(void);
        plhs[0] = sf_c12_ImageDet_get_post_codegen_info();
        return;
      }
    }
    break;

   default:
    break;
  }

  plhs[0] = mxCreateDoubleMatrix(0,0,mxREAL);
}

void ImageDet_debug_initialize(struct SfDebugInstanceStruct* debugInstance)
{
  _ImageDetMachineNumber_ = sf_debug_initialize_machine(debugInstance,"ImageDet",
    "sfun",0,10,0,0,0);
  sf_debug_set_machine_event_thresholds(debugInstance,_ImageDetMachineNumber_,0,
    0);
  sf_debug_set_machine_data_thresholds(debugInstance,_ImageDetMachineNumber_,0);
}

void ImageDet_register_exported_symbols(SimStruct* S)
{
}

static mxArray* sRtwOptimizationInfoStruct= NULL;
mxArray* load_ImageDet_optimization_info(void)
{
  if (sRtwOptimizationInfoStruct==NULL) {
    sRtwOptimizationInfoStruct = sf_load_rtw_optimization_info("ImageDet",
      "ImageDet");
    mexMakeArrayPersistent(sRtwOptimizationInfoStruct);
  }

  return(sRtwOptimizationInfoStruct);
}

void unload_ImageDet_optimization_info(void)
{
  if (sRtwOptimizationInfoStruct!=NULL) {
    mxDestroyArray(sRtwOptimizationInfoStruct);
    sRtwOptimizationInfoStruct = NULL;
  }
}

/* Include files */

#include <stddef.h>
#include "blas.h"
#include "ImageDet_sfun.h"
#include "c1_ImageDet.h"
#define CHARTINSTANCE_CHARTNUMBER      (chartInstance->chartNumber)
#define CHARTINSTANCE_INSTANCENUMBER   (chartInstance->instanceNumber)
#include "ImageDet_sfun_debug_macros.h"
#define _SF_MEX_LISTEN_FOR_CTRL_C(S)   sf_mex_listen_for_ctrl_c(sfGlobalDebugInstanceStruct,S);

/* Type Definitions */

/* Named Constants */
#define CALL_EVENT                     (-1)

/* Variable Declarations */

/* Variable Definitions */
static real_T _sfTime_;
static const char * c1_debug_family_names[12] = { "channel3Min",
  "channel1MinRed", "channel1MaxRed", "channel1BlueMin", "channel1BlueMax",
  "nargin", "nargout", "H", "S", "V", "redBool", "blueBool" };

/* Function Declarations */
static void initialize_c1_ImageDet(SFc1_ImageDetInstanceStruct *chartInstance);
static void initialize_params_c1_ImageDet(SFc1_ImageDetInstanceStruct
  *chartInstance);
static void enable_c1_ImageDet(SFc1_ImageDetInstanceStruct *chartInstance);
static void disable_c1_ImageDet(SFc1_ImageDetInstanceStruct *chartInstance);
static void c1_update_debugger_state_c1_ImageDet(SFc1_ImageDetInstanceStruct
  *chartInstance);
static const mxArray *get_sim_state_c1_ImageDet(SFc1_ImageDetInstanceStruct
  *chartInstance);
static void set_sim_state_c1_ImageDet(SFc1_ImageDetInstanceStruct *chartInstance,
  const mxArray *c1_st);
static void finalize_c1_ImageDet(SFc1_ImageDetInstanceStruct *chartInstance);
static void sf_gateway_c1_ImageDet(SFc1_ImageDetInstanceStruct *chartInstance);
static void mdl_start_c1_ImageDet(SFc1_ImageDetInstanceStruct *chartInstance);
static void c1_chartstep_c1_ImageDet(SFc1_ImageDetInstanceStruct *chartInstance);
static void initSimStructsc1_ImageDet(SFc1_ImageDetInstanceStruct *chartInstance);
static void init_script_number_translation(uint32_T c1_machineNumber, uint32_T
  c1_chartNumber, uint32_T c1_instanceNumber);
static const mxArray *c1_sf_marshallOut(void *chartInstanceVoid, void
  *c1_b_inData);
static void c1_emlrt_marshallIn(SFc1_ImageDetInstanceStruct *chartInstance,
  const mxArray *c1_c_blueBool, const char_T *c1_identifier, boolean_T c1_y
  [76800]);
static void c1_b_emlrt_marshallIn(SFc1_ImageDetInstanceStruct *chartInstance,
  const mxArray *c1_b_u, const emlrtMsgIdentifier *c1_parentId, boolean_T c1_y
  [76800]);
static void c1_sf_marshallIn(void *chartInstanceVoid, const mxArray
  *c1_mxArrayInData, const char_T *c1_varName, void *c1_outData);
static const mxArray *c1_b_sf_marshallOut(void *chartInstanceVoid, void
  *c1_b_inData);
static const mxArray *c1_c_sf_marshallOut(void *chartInstanceVoid, void
  *c1_b_inData);
static real_T c1_c_emlrt_marshallIn(SFc1_ImageDetInstanceStruct *chartInstance,
  const mxArray *c1_b_u, const emlrtMsgIdentifier *c1_parentId);
static void c1_b_sf_marshallIn(void *chartInstanceVoid, const mxArray
  *c1_mxArrayInData, const char_T *c1_varName, void *c1_outData);
static const mxArray *c1_d_sf_marshallOut(void *chartInstanceVoid, void
  *c1_b_inData);
static int32_T c1_d_emlrt_marshallIn(SFc1_ImageDetInstanceStruct *chartInstance,
  const mxArray *c1_b_u, const emlrtMsgIdentifier *c1_parentId);
static void c1_c_sf_marshallIn(void *chartInstanceVoid, const mxArray
  *c1_mxArrayInData, const char_T *c1_varName, void *c1_outData);
static uint8_T c1_e_emlrt_marshallIn(SFc1_ImageDetInstanceStruct *chartInstance,
  const mxArray *c1_b_is_active_c1_ImageDet, const char_T *c1_identifier);
static uint8_T c1_f_emlrt_marshallIn(SFc1_ImageDetInstanceStruct *chartInstance,
  const mxArray *c1_b_u, const emlrtMsgIdentifier *c1_parentId);
static void init_dsm_address_info(SFc1_ImageDetInstanceStruct *chartInstance);
static void init_simulink_io_address(SFc1_ImageDetInstanceStruct *chartInstance);

/* Function Definitions */
static void initialize_c1_ImageDet(SFc1_ImageDetInstanceStruct *chartInstance)
{
  chartInstance->c1_sfEvent = CALL_EVENT;
  _sfTime_ = sf_get_time(chartInstance->S);
  chartInstance->c1_is_active_c1_ImageDet = 0U;
}

static void initialize_params_c1_ImageDet(SFc1_ImageDetInstanceStruct
  *chartInstance)
{
  (void)chartInstance;
}

static void enable_c1_ImageDet(SFc1_ImageDetInstanceStruct *chartInstance)
{
  _sfTime_ = sf_get_time(chartInstance->S);
}

static void disable_c1_ImageDet(SFc1_ImageDetInstanceStruct *chartInstance)
{
  _sfTime_ = sf_get_time(chartInstance->S);
}

static void c1_update_debugger_state_c1_ImageDet(SFc1_ImageDetInstanceStruct
  *chartInstance)
{
  (void)chartInstance;
}

static const mxArray *get_sim_state_c1_ImageDet(SFc1_ImageDetInstanceStruct
  *chartInstance)
{
  const mxArray *c1_st;
  const mxArray *c1_y = NULL;
  int32_T c1_i0;
  boolean_T c1_b_u[76800];
  const mxArray *c1_b_y = NULL;
  int32_T c1_i1;
  boolean_T c1_c_u[76800];
  const mxArray *c1_c_y = NULL;
  uint8_T c1_hoistedGlobal;
  uint8_T c1_d_u;
  const mxArray *c1_d_y = NULL;
  c1_st = NULL;
  c1_st = NULL;
  c1_y = NULL;
  sf_mex_assign(&c1_y, sf_mex_createcellmatrix(3, 1), false);
  for (c1_i0 = 0; c1_i0 < 76800; c1_i0++) {
    c1_b_u[c1_i0] = (*chartInstance->c1_b_blueBool)[c1_i0];
  }

  c1_b_y = NULL;
  sf_mex_assign(&c1_b_y, sf_mex_create("y", c1_b_u, 11, 0U, 1U, 0U, 2, 320, 240),
                false);
  sf_mex_setcell(c1_y, 0, c1_b_y);
  for (c1_i1 = 0; c1_i1 < 76800; c1_i1++) {
    c1_c_u[c1_i1] = (*chartInstance->c1_b_redBool)[c1_i1];
  }

  c1_c_y = NULL;
  sf_mex_assign(&c1_c_y, sf_mex_create("y", c1_c_u, 11, 0U, 1U, 0U, 2, 320, 240),
                false);
  sf_mex_setcell(c1_y, 1, c1_c_y);
  c1_hoistedGlobal = chartInstance->c1_is_active_c1_ImageDet;
  c1_d_u = c1_hoistedGlobal;
  c1_d_y = NULL;
  sf_mex_assign(&c1_d_y, sf_mex_create("y", &c1_d_u, 3, 0U, 0U, 0U, 0), false);
  sf_mex_setcell(c1_y, 2, c1_d_y);
  sf_mex_assign(&c1_st, c1_y, false);
  return c1_st;
}

static void set_sim_state_c1_ImageDet(SFc1_ImageDetInstanceStruct *chartInstance,
  const mxArray *c1_st)
{
  const mxArray *c1_b_u;
  int32_T c1_i2;
  boolean_T c1_bv1[76800];
  int32_T c1_i3;
  chartInstance->c1_doneDoubleBufferReInit = true;
  c1_b_u = sf_mex_dup(c1_st);
  c1_emlrt_marshallIn(chartInstance, sf_mex_dup(sf_mex_getcell(c1_b_u, 0)),
                      "blueBool", chartInstance->c1_bv0);
  for (c1_i2 = 0; c1_i2 < 76800; c1_i2++) {
    (*chartInstance->c1_b_blueBool)[c1_i2] = chartInstance->c1_bv0[c1_i2];
  }

  c1_emlrt_marshallIn(chartInstance, sf_mex_dup(sf_mex_getcell(c1_b_u, 1)),
                      "redBool", c1_bv1);
  for (c1_i3 = 0; c1_i3 < 76800; c1_i3++) {
    (*chartInstance->c1_b_redBool)[c1_i3] = c1_bv1[c1_i3];
  }

  chartInstance->c1_is_active_c1_ImageDet = c1_e_emlrt_marshallIn(chartInstance,
    sf_mex_dup(sf_mex_getcell(c1_b_u, 2)), "is_active_c1_ImageDet");
  sf_mex_destroy(&c1_b_u);
  c1_update_debugger_state_c1_ImageDet(chartInstance);
  sf_mex_destroy(&c1_st);
}

static void finalize_c1_ImageDet(SFc1_ImageDetInstanceStruct *chartInstance)
{
  (void)chartInstance;
}

static void sf_gateway_c1_ImageDet(SFc1_ImageDetInstanceStruct *chartInstance)
{
  int32_T c1_i4;
  int32_T c1_i5;
  int32_T c1_i6;
  int32_T c1_i7;
  int32_T c1_i8;
  _SFD_SYMBOL_SCOPE_PUSH(0U, 0U);
  _sfTime_ = sf_get_time(chartInstance->S);
  _SFD_CC_CALL(CHART_ENTER_SFUNCTION_TAG, 0U, chartInstance->c1_sfEvent);
  chartInstance->c1_sfEvent = CALL_EVENT;
  c1_chartstep_c1_ImageDet(chartInstance);
  _SFD_SYMBOL_SCOPE_POP();
  _SFD_CHECK_FOR_STATE_INCONSISTENCY(_ImageDetMachineNumber_,
    chartInstance->chartNumber, chartInstance->instanceNumber);
  for (c1_i4 = 0; c1_i4 < 76800; c1_i4++) {
    _SFD_DATA_RANGE_CHECK((real_T)(*chartInstance->c1_b_redBool)[c1_i4], 0U);
  }

  for (c1_i5 = 0; c1_i5 < 76800; c1_i5++) {
    _SFD_DATA_RANGE_CHECK((real_T)(*chartInstance->c1_b_blueBool)[c1_i5], 1U);
  }

  for (c1_i6 = 0; c1_i6 < 76800; c1_i6++) {
    _SFD_DATA_RANGE_CHECK((real_T)(*chartInstance->c1_b_H)[c1_i6], 2U);
  }

  for (c1_i7 = 0; c1_i7 < 76800; c1_i7++) {
    _SFD_DATA_RANGE_CHECK((real_T)(*chartInstance->c1_b_S)[c1_i7], 3U);
  }

  for (c1_i8 = 0; c1_i8 < 76800; c1_i8++) {
    _SFD_DATA_RANGE_CHECK((real_T)(*chartInstance->c1_b_V)[c1_i8], 4U);
  }
}

static void mdl_start_c1_ImageDet(SFc1_ImageDetInstanceStruct *chartInstance)
{
  (void)chartInstance;
}

static void c1_chartstep_c1_ImageDet(SFc1_ImageDetInstanceStruct *chartInstance)
{
  int32_T c1_i9;
  int32_T c1_i10;
  int32_T c1_i11;
  uint32_T c1_debug_family_var_map[12];
  real_T c1_channel3Min;
  real_T c1_channel1MinRed;
  real_T c1_channel1MaxRed;
  real_T c1_channel1BlueMin;
  real_T c1_channel1BlueMax;
  real_T c1_nargin = 3.0;
  real_T c1_nargout = 2.0;
  int32_T c1_i12;
  boolean_T c1_bv2[76800];
  int32_T c1_i13;
  boolean_T c1_bv3[76800];
  int32_T c1_i14;
  int32_T c1_i15;
  int32_T c1_i16;
  int32_T c1_i17;
  int32_T c1_i18;
  int32_T c1_i19;
  int32_T c1_i20;
  int32_T c1_i21;
  int32_T c1_i22;
  int32_T c1_i23;
  _SFD_CC_CALL(CHART_ENTER_DURING_FUNCTION_TAG, 0U, chartInstance->c1_sfEvent);
  for (c1_i9 = 0; c1_i9 < 76800; c1_i9++) {
    chartInstance->c1_H[c1_i9] = (*chartInstance->c1_b_H)[c1_i9];
  }

  for (c1_i10 = 0; c1_i10 < 76800; c1_i10++) {
    chartInstance->c1_S[c1_i10] = (*chartInstance->c1_b_S)[c1_i10];
  }

  for (c1_i11 = 0; c1_i11 < 76800; c1_i11++) {
    chartInstance->c1_V[c1_i11] = (*chartInstance->c1_b_V)[c1_i11];
  }

  _SFD_SYMBOL_SCOPE_PUSH_EML(0U, 12U, 12U, c1_debug_family_names,
    c1_debug_family_var_map);
  _SFD_SYMBOL_SCOPE_ADD_EML_IMPORTABLE(&c1_channel3Min, 0U, c1_c_sf_marshallOut,
    c1_b_sf_marshallIn);
  _SFD_SYMBOL_SCOPE_ADD_EML_IMPORTABLE(&c1_channel1MinRed, 1U,
    c1_c_sf_marshallOut, c1_b_sf_marshallIn);
  _SFD_SYMBOL_SCOPE_ADD_EML_IMPORTABLE(&c1_channel1MaxRed, 2U,
    c1_c_sf_marshallOut, c1_b_sf_marshallIn);
  _SFD_SYMBOL_SCOPE_ADD_EML_IMPORTABLE(&c1_channel1BlueMin, 3U,
    c1_c_sf_marshallOut, c1_b_sf_marshallIn);
  _SFD_SYMBOL_SCOPE_ADD_EML_IMPORTABLE(&c1_channel1BlueMax, 4U,
    c1_c_sf_marshallOut, c1_b_sf_marshallIn);
  _SFD_SYMBOL_SCOPE_ADD_EML_IMPORTABLE(&c1_nargin, 5U, c1_c_sf_marshallOut,
    c1_b_sf_marshallIn);
  _SFD_SYMBOL_SCOPE_ADD_EML_IMPORTABLE(&c1_nargout, 6U, c1_c_sf_marshallOut,
    c1_b_sf_marshallIn);
  _SFD_SYMBOL_SCOPE_ADD_EML(chartInstance->c1_H, 7U, c1_b_sf_marshallOut);
  _SFD_SYMBOL_SCOPE_ADD_EML(chartInstance->c1_S, 8U, c1_b_sf_marshallOut);
  _SFD_SYMBOL_SCOPE_ADD_EML(chartInstance->c1_V, 9U, c1_b_sf_marshallOut);
  _SFD_SYMBOL_SCOPE_ADD_EML_IMPORTABLE(chartInstance->c1_redBool, 10U,
    c1_sf_marshallOut, c1_sf_marshallIn);
  _SFD_SYMBOL_SCOPE_ADD_EML_IMPORTABLE(chartInstance->c1_blueBool, 11U,
    c1_sf_marshallOut, c1_sf_marshallIn);
  CV_EML_FCN(0, 0);
  _SFD_EML_CALL(0U, chartInstance->c1_sfEvent, 3);
  c1_channel3Min = 0.8;
  _SFD_EML_CALL(0U, chartInstance->c1_sfEvent, 6);
  c1_channel1MinRed = 0.75;
  _SFD_EML_CALL(0U, chartInstance->c1_sfEvent, 7);
  c1_channel1MaxRed = 0.25;
  _SFD_EML_CALL(0U, chartInstance->c1_sfEvent, 9);
  c1_channel1BlueMin = 0.25;
  _SFD_EML_CALL(0U, chartInstance->c1_sfEvent, 10);
  c1_channel1BlueMax = 0.75;
  _SFD_EML_CALL(0U, chartInstance->c1_sfEvent, 17);
  for (c1_i12 = 0; c1_i12 < 76800; c1_i12++) {
    c1_bv2[c1_i12] = ((real_T)chartInstance->c1_H[c1_i12] >= c1_channel1MinRed);
  }

  for (c1_i13 = 0; c1_i13 < 76800; c1_i13++) {
    c1_bv3[c1_i13] = ((real_T)chartInstance->c1_H[c1_i13] <= c1_channel1MaxRed);
  }

  for (c1_i14 = 0; c1_i14 < 76800; c1_i14++) {
    c1_bv2[c1_i14] = (c1_bv2[c1_i14] || c1_bv3[c1_i14]);
  }

  for (c1_i15 = 0; c1_i15 < 76800; c1_i15++) {
    c1_bv3[c1_i15] = ((real_T)chartInstance->c1_V[c1_i15] >= c1_channel3Min);
  }

  for (c1_i16 = 0; c1_i16 < 76800; c1_i16++) {
    chartInstance->c1_redBool[c1_i16] = (c1_bv2[c1_i16] && c1_bv3[c1_i16]);
  }

  _SFD_EML_CALL(0U, chartInstance->c1_sfEvent, 20);
  for (c1_i17 = 0; c1_i17 < 76800; c1_i17++) {
    c1_bv2[c1_i17] = ((real_T)chartInstance->c1_H[c1_i17] >= c1_channel1BlueMin);
  }

  for (c1_i18 = 0; c1_i18 < 76800; c1_i18++) {
    c1_bv3[c1_i18] = ((real_T)chartInstance->c1_H[c1_i18] <= c1_channel1BlueMax);
  }

  for (c1_i19 = 0; c1_i19 < 76800; c1_i19++) {
    c1_bv2[c1_i19] = (c1_bv2[c1_i19] && c1_bv3[c1_i19]);
  }

  for (c1_i20 = 0; c1_i20 < 76800; c1_i20++) {
    c1_bv3[c1_i20] = ((real_T)chartInstance->c1_V[c1_i20] >= c1_channel3Min);
  }

  for (c1_i21 = 0; c1_i21 < 76800; c1_i21++) {
    chartInstance->c1_blueBool[c1_i21] = (c1_bv2[c1_i21] && c1_bv3[c1_i21]);
  }

  _SFD_EML_CALL(0U, chartInstance->c1_sfEvent, -20);
  _SFD_SYMBOL_SCOPE_POP();
  for (c1_i22 = 0; c1_i22 < 76800; c1_i22++) {
    (*chartInstance->c1_b_redBool)[c1_i22] = chartInstance->c1_redBool[c1_i22];
  }

  for (c1_i23 = 0; c1_i23 < 76800; c1_i23++) {
    (*chartInstance->c1_b_blueBool)[c1_i23] = chartInstance->c1_blueBool[c1_i23];
  }

  _SFD_CC_CALL(EXIT_OUT_OF_FUNCTION_TAG, 0U, chartInstance->c1_sfEvent);
}

static void initSimStructsc1_ImageDet(SFc1_ImageDetInstanceStruct *chartInstance)
{
  (void)chartInstance;
}

static void init_script_number_translation(uint32_T c1_machineNumber, uint32_T
  c1_chartNumber, uint32_T c1_instanceNumber)
{
  (void)c1_machineNumber;
  (void)c1_chartNumber;
  (void)c1_instanceNumber;
}

static const mxArray *c1_sf_marshallOut(void *chartInstanceVoid, void
  *c1_b_inData)
{
  const mxArray *c1_mxArrayOutData = NULL;
  int32_T c1_i24;
  int32_T c1_i25;
  int32_T c1_i26;
  boolean_T c1_c_inData[76800];
  int32_T c1_i27;
  int32_T c1_i28;
  int32_T c1_i29;
  boolean_T c1_b_u[76800];
  const mxArray *c1_y = NULL;
  SFc1_ImageDetInstanceStruct *chartInstance;
  chartInstance = (SFc1_ImageDetInstanceStruct *)chartInstanceVoid;
  c1_mxArrayOutData = NULL;
  c1_i24 = 0;
  for (c1_i25 = 0; c1_i25 < 240; c1_i25++) {
    for (c1_i26 = 0; c1_i26 < 320; c1_i26++) {
      c1_c_inData[c1_i26 + c1_i24] = (*(boolean_T (*)[76800])c1_b_inData)[c1_i26
        + c1_i24];
    }

    c1_i24 += 320;
  }

  c1_i27 = 0;
  for (c1_i28 = 0; c1_i28 < 240; c1_i28++) {
    for (c1_i29 = 0; c1_i29 < 320; c1_i29++) {
      c1_b_u[c1_i29 + c1_i27] = c1_c_inData[c1_i29 + c1_i27];
    }

    c1_i27 += 320;
  }

  c1_y = NULL;
  sf_mex_assign(&c1_y, sf_mex_create("y", c1_b_u, 11, 0U, 1U, 0U, 2, 320, 240),
                false);
  sf_mex_assign(&c1_mxArrayOutData, c1_y, false);
  return c1_mxArrayOutData;
}

static void c1_emlrt_marshallIn(SFc1_ImageDetInstanceStruct *chartInstance,
  const mxArray *c1_c_blueBool, const char_T *c1_identifier, boolean_T c1_y
  [76800])
{
  emlrtMsgIdentifier c1_thisId;
  c1_thisId.fIdentifier = c1_identifier;
  c1_thisId.fParent = NULL;
  c1_b_emlrt_marshallIn(chartInstance, sf_mex_dup(c1_c_blueBool), &c1_thisId,
                        c1_y);
  sf_mex_destroy(&c1_c_blueBool);
}

static void c1_b_emlrt_marshallIn(SFc1_ImageDetInstanceStruct *chartInstance,
  const mxArray *c1_b_u, const emlrtMsgIdentifier *c1_parentId, boolean_T c1_y
  [76800])
{
  boolean_T c1_bv4[76800];
  int32_T c1_i30;
  (void)chartInstance;
  sf_mex_import(c1_parentId, sf_mex_dup(c1_b_u), c1_bv4, 1, 11, 0U, 1, 0U, 2,
                320, 240);
  for (c1_i30 = 0; c1_i30 < 76800; c1_i30++) {
    c1_y[c1_i30] = c1_bv4[c1_i30];
  }

  sf_mex_destroy(&c1_b_u);
}

static void c1_sf_marshallIn(void *chartInstanceVoid, const mxArray
  *c1_mxArrayInData, const char_T *c1_varName, void *c1_outData)
{
  const mxArray *c1_c_blueBool;
  const char_T *c1_identifier;
  emlrtMsgIdentifier c1_thisId;
  boolean_T c1_y[76800];
  int32_T c1_i31;
  int32_T c1_i32;
  int32_T c1_i33;
  SFc1_ImageDetInstanceStruct *chartInstance;
  chartInstance = (SFc1_ImageDetInstanceStruct *)chartInstanceVoid;
  c1_c_blueBool = sf_mex_dup(c1_mxArrayInData);
  c1_identifier = c1_varName;
  c1_thisId.fIdentifier = c1_identifier;
  c1_thisId.fParent = NULL;
  c1_b_emlrt_marshallIn(chartInstance, sf_mex_dup(c1_c_blueBool), &c1_thisId,
                        c1_y);
  sf_mex_destroy(&c1_c_blueBool);
  c1_i31 = 0;
  for (c1_i32 = 0; c1_i32 < 240; c1_i32++) {
    for (c1_i33 = 0; c1_i33 < 320; c1_i33++) {
      (*(boolean_T (*)[76800])c1_outData)[c1_i33 + c1_i31] = c1_y[c1_i33 +
        c1_i31];
    }

    c1_i31 += 320;
  }

  sf_mex_destroy(&c1_mxArrayInData);
}

static const mxArray *c1_b_sf_marshallOut(void *chartInstanceVoid, void
  *c1_b_inData)
{
  const mxArray *c1_mxArrayOutData = NULL;
  int32_T c1_i34;
  int32_T c1_i35;
  int32_T c1_i36;
  int32_T c1_i37;
  int32_T c1_i38;
  int32_T c1_i39;
  const mxArray *c1_y = NULL;
  SFc1_ImageDetInstanceStruct *chartInstance;
  chartInstance = (SFc1_ImageDetInstanceStruct *)chartInstanceVoid;
  c1_mxArrayOutData = NULL;
  c1_i34 = 0;
  for (c1_i35 = 0; c1_i35 < 240; c1_i35++) {
    for (c1_i36 = 0; c1_i36 < 320; c1_i36++) {
      chartInstance->c1_inData[c1_i36 + c1_i34] = (*(real32_T (*)[76800])
        c1_b_inData)[c1_i36 + c1_i34];
    }

    c1_i34 += 320;
  }

  c1_i37 = 0;
  for (c1_i38 = 0; c1_i38 < 240; c1_i38++) {
    for (c1_i39 = 0; c1_i39 < 320; c1_i39++) {
      chartInstance->c1_u[c1_i39 + c1_i37] = chartInstance->c1_inData[c1_i39 +
        c1_i37];
    }

    c1_i37 += 320;
  }

  c1_y = NULL;
  sf_mex_assign(&c1_y, sf_mex_create("y", chartInstance->c1_u, 1, 0U, 1U, 0U, 2,
    320, 240), false);
  sf_mex_assign(&c1_mxArrayOutData, c1_y, false);
  return c1_mxArrayOutData;
}

static const mxArray *c1_c_sf_marshallOut(void *chartInstanceVoid, void
  *c1_b_inData)
{
  const mxArray *c1_mxArrayOutData = NULL;
  real_T c1_b_u;
  const mxArray *c1_y = NULL;
  SFc1_ImageDetInstanceStruct *chartInstance;
  chartInstance = (SFc1_ImageDetInstanceStruct *)chartInstanceVoid;
  c1_mxArrayOutData = NULL;
  c1_b_u = *(real_T *)c1_b_inData;
  c1_y = NULL;
  sf_mex_assign(&c1_y, sf_mex_create("y", &c1_b_u, 0, 0U, 0U, 0U, 0), false);
  sf_mex_assign(&c1_mxArrayOutData, c1_y, false);
  return c1_mxArrayOutData;
}

static real_T c1_c_emlrt_marshallIn(SFc1_ImageDetInstanceStruct *chartInstance,
  const mxArray *c1_b_u, const emlrtMsgIdentifier *c1_parentId)
{
  real_T c1_y;
  real_T c1_d0;
  (void)chartInstance;
  sf_mex_import(c1_parentId, sf_mex_dup(c1_b_u), &c1_d0, 1, 0, 0U, 0, 0U, 0);
  c1_y = c1_d0;
  sf_mex_destroy(&c1_b_u);
  return c1_y;
}

static void c1_b_sf_marshallIn(void *chartInstanceVoid, const mxArray
  *c1_mxArrayInData, const char_T *c1_varName, void *c1_outData)
{
  const mxArray *c1_nargout;
  const char_T *c1_identifier;
  emlrtMsgIdentifier c1_thisId;
  real_T c1_y;
  SFc1_ImageDetInstanceStruct *chartInstance;
  chartInstance = (SFc1_ImageDetInstanceStruct *)chartInstanceVoid;
  c1_nargout = sf_mex_dup(c1_mxArrayInData);
  c1_identifier = c1_varName;
  c1_thisId.fIdentifier = c1_identifier;
  c1_thisId.fParent = NULL;
  c1_y = c1_c_emlrt_marshallIn(chartInstance, sf_mex_dup(c1_nargout), &c1_thisId);
  sf_mex_destroy(&c1_nargout);
  *(real_T *)c1_outData = c1_y;
  sf_mex_destroy(&c1_mxArrayInData);
}

const mxArray *sf_c1_ImageDet_get_eml_resolved_functions_info(void)
{
  const mxArray *c1_nameCaptureInfo = NULL;
  c1_nameCaptureInfo = NULL;
  sf_mex_assign(&c1_nameCaptureInfo, sf_mex_create("nameCaptureInfo", NULL, 0,
    0U, 1U, 0U, 2, 0, 1), false);
  return c1_nameCaptureInfo;
}

static const mxArray *c1_d_sf_marshallOut(void *chartInstanceVoid, void
  *c1_b_inData)
{
  const mxArray *c1_mxArrayOutData = NULL;
  int32_T c1_b_u;
  const mxArray *c1_y = NULL;
  SFc1_ImageDetInstanceStruct *chartInstance;
  chartInstance = (SFc1_ImageDetInstanceStruct *)chartInstanceVoid;
  c1_mxArrayOutData = NULL;
  c1_b_u = *(int32_T *)c1_b_inData;
  c1_y = NULL;
  sf_mex_assign(&c1_y, sf_mex_create("y", &c1_b_u, 6, 0U, 0U, 0U, 0), false);
  sf_mex_assign(&c1_mxArrayOutData, c1_y, false);
  return c1_mxArrayOutData;
}

static int32_T c1_d_emlrt_marshallIn(SFc1_ImageDetInstanceStruct *chartInstance,
  const mxArray *c1_b_u, const emlrtMsgIdentifier *c1_parentId)
{
  int32_T c1_y;
  int32_T c1_i40;
  (void)chartInstance;
  sf_mex_import(c1_parentId, sf_mex_dup(c1_b_u), &c1_i40, 1, 6, 0U, 0, 0U, 0);
  c1_y = c1_i40;
  sf_mex_destroy(&c1_b_u);
  return c1_y;
}

static void c1_c_sf_marshallIn(void *chartInstanceVoid, const mxArray
  *c1_mxArrayInData, const char_T *c1_varName, void *c1_outData)
{
  const mxArray *c1_b_sfEvent;
  const char_T *c1_identifier;
  emlrtMsgIdentifier c1_thisId;
  int32_T c1_y;
  SFc1_ImageDetInstanceStruct *chartInstance;
  chartInstance = (SFc1_ImageDetInstanceStruct *)chartInstanceVoid;
  c1_b_sfEvent = sf_mex_dup(c1_mxArrayInData);
  c1_identifier = c1_varName;
  c1_thisId.fIdentifier = c1_identifier;
  c1_thisId.fParent = NULL;
  c1_y = c1_d_emlrt_marshallIn(chartInstance, sf_mex_dup(c1_b_sfEvent),
    &c1_thisId);
  sf_mex_destroy(&c1_b_sfEvent);
  *(int32_T *)c1_outData = c1_y;
  sf_mex_destroy(&c1_mxArrayInData);
}

static uint8_T c1_e_emlrt_marshallIn(SFc1_ImageDetInstanceStruct *chartInstance,
  const mxArray *c1_b_is_active_c1_ImageDet, const char_T *c1_identifier)
{
  uint8_T c1_y;
  emlrtMsgIdentifier c1_thisId;
  c1_thisId.fIdentifier = c1_identifier;
  c1_thisId.fParent = NULL;
  c1_y = c1_f_emlrt_marshallIn(chartInstance, sf_mex_dup
    (c1_b_is_active_c1_ImageDet), &c1_thisId);
  sf_mex_destroy(&c1_b_is_active_c1_ImageDet);
  return c1_y;
}

static uint8_T c1_f_emlrt_marshallIn(SFc1_ImageDetInstanceStruct *chartInstance,
  const mxArray *c1_b_u, const emlrtMsgIdentifier *c1_parentId)
{
  uint8_T c1_y;
  uint8_T c1_u0;
  (void)chartInstance;
  sf_mex_import(c1_parentId, sf_mex_dup(c1_b_u), &c1_u0, 1, 3, 0U, 0, 0U, 0);
  c1_y = c1_u0;
  sf_mex_destroy(&c1_b_u);
  return c1_y;
}

static void init_dsm_address_info(SFc1_ImageDetInstanceStruct *chartInstance)
{
  (void)chartInstance;
}

static void init_simulink_io_address(SFc1_ImageDetInstanceStruct *chartInstance)
{
  chartInstance->c1_b_redBool = (boolean_T (*)[76800])
    ssGetOutputPortSignal_wrapper(chartInstance->S, 1);
  chartInstance->c1_b_blueBool = (boolean_T (*)[76800])
    ssGetOutputPortSignal_wrapper(chartInstance->S, 2);
  chartInstance->c1_b_H = (real32_T (*)[76800])ssGetInputPortSignal_wrapper
    (chartInstance->S, 0);
  chartInstance->c1_b_S = (real32_T (*)[76800])ssGetInputPortSignal_wrapper
    (chartInstance->S, 1);
  chartInstance->c1_b_V = (real32_T (*)[76800])ssGetInputPortSignal_wrapper
    (chartInstance->S, 2);
}

/* SFunction Glue Code */
#ifdef utFree
#undef utFree
#endif

#ifdef utMalloc
#undef utMalloc
#endif

#ifdef __cplusplus

extern "C" void *utMalloc(size_t size);
extern "C" void utFree(void*);

#else

extern void *utMalloc(size_t size);
extern void utFree(void*);

#endif

void sf_c1_ImageDet_get_check_sum(mxArray *plhs[])
{
  ((real_T *)mxGetPr((plhs[0])))[0] = (real_T)(3549138951U);
  ((real_T *)mxGetPr((plhs[0])))[1] = (real_T)(3396771275U);
  ((real_T *)mxGetPr((plhs[0])))[2] = (real_T)(2220436498U);
  ((real_T *)mxGetPr((plhs[0])))[3] = (real_T)(2973983856U);
}

mxArray* sf_c1_ImageDet_get_post_codegen_info(void);
mxArray *sf_c1_ImageDet_get_autoinheritance_info(void)
{
  const char *autoinheritanceFields[] = { "checksum", "inputs", "parameters",
    "outputs", "locals", "postCodegenInfo" };

  mxArray *mxAutoinheritanceInfo = mxCreateStructMatrix(1, 1, sizeof
    (autoinheritanceFields)/sizeof(autoinheritanceFields[0]),
    autoinheritanceFields);

  {
    mxArray *mxChecksum = mxCreateString("w2vRmMYgs91tUddE44mv0E");
    mxSetField(mxAutoinheritanceInfo,0,"checksum",mxChecksum);
  }

  {
    const char *dataFields[] = { "size", "type", "complexity" };

    mxArray *mxData = mxCreateStructMatrix(1,3,3,dataFields);

    {
      mxArray *mxSize = mxCreateDoubleMatrix(1,2,mxREAL);
      double *pr = mxGetPr(mxSize);
      pr[0] = (double)(320);
      pr[1] = (double)(240);
      mxSetField(mxData,0,"size",mxSize);
    }

    {
      const char *typeFields[] = { "base", "fixpt" };

      mxArray *mxType = mxCreateStructMatrix(1,1,2,typeFields);
      mxSetField(mxType,0,"base",mxCreateDoubleScalar(9));
      mxSetField(mxType,0,"fixpt",mxCreateDoubleMatrix(0,0,mxREAL));
      mxSetField(mxData,0,"type",mxType);
    }

    mxSetField(mxData,0,"complexity",mxCreateDoubleScalar(0));

    {
      mxArray *mxSize = mxCreateDoubleMatrix(1,2,mxREAL);
      double *pr = mxGetPr(mxSize);
      pr[0] = (double)(320);
      pr[1] = (double)(240);
      mxSetField(mxData,1,"size",mxSize);
    }

    {
      const char *typeFields[] = { "base", "fixpt" };

      mxArray *mxType = mxCreateStructMatrix(1,1,2,typeFields);
      mxSetField(mxType,0,"base",mxCreateDoubleScalar(9));
      mxSetField(mxType,0,"fixpt",mxCreateDoubleMatrix(0,0,mxREAL));
      mxSetField(mxData,1,"type",mxType);
    }

    mxSetField(mxData,1,"complexity",mxCreateDoubleScalar(0));

    {
      mxArray *mxSize = mxCreateDoubleMatrix(1,2,mxREAL);
      double *pr = mxGetPr(mxSize);
      pr[0] = (double)(320);
      pr[1] = (double)(240);
      mxSetField(mxData,2,"size",mxSize);
    }

    {
      const char *typeFields[] = { "base", "fixpt" };

      mxArray *mxType = mxCreateStructMatrix(1,1,2,typeFields);
      mxSetField(mxType,0,"base",mxCreateDoubleScalar(9));
      mxSetField(mxType,0,"fixpt",mxCreateDoubleMatrix(0,0,mxREAL));
      mxSetField(mxData,2,"type",mxType);
    }

    mxSetField(mxData,2,"complexity",mxCreateDoubleScalar(0));
    mxSetField(mxAutoinheritanceInfo,0,"inputs",mxData);
  }

  {
    mxSetField(mxAutoinheritanceInfo,0,"parameters",mxCreateDoubleMatrix(0,0,
                mxREAL));
  }

  {
    const char *dataFields[] = { "size", "type", "complexity" };

    mxArray *mxData = mxCreateStructMatrix(1,2,3,dataFields);

    {
      mxArray *mxSize = mxCreateDoubleMatrix(1,2,mxREAL);
      double *pr = mxGetPr(mxSize);
      pr[0] = (double)(320);
      pr[1] = (double)(240);
      mxSetField(mxData,0,"size",mxSize);
    }

    {
      const char *typeFields[] = { "base", "fixpt" };

      mxArray *mxType = mxCreateStructMatrix(1,1,2,typeFields);
      mxSetField(mxType,0,"base",mxCreateDoubleScalar(1));
      mxSetField(mxType,0,"fixpt",mxCreateDoubleMatrix(0,0,mxREAL));
      mxSetField(mxData,0,"type",mxType);
    }

    mxSetField(mxData,0,"complexity",mxCreateDoubleScalar(0));

    {
      mxArray *mxSize = mxCreateDoubleMatrix(1,2,mxREAL);
      double *pr = mxGetPr(mxSize);
      pr[0] = (double)(320);
      pr[1] = (double)(240);
      mxSetField(mxData,1,"size",mxSize);
    }

    {
      const char *typeFields[] = { "base", "fixpt" };

      mxArray *mxType = mxCreateStructMatrix(1,1,2,typeFields);
      mxSetField(mxType,0,"base",mxCreateDoubleScalar(1));
      mxSetField(mxType,0,"fixpt",mxCreateDoubleMatrix(0,0,mxREAL));
      mxSetField(mxData,1,"type",mxType);
    }

    mxSetField(mxData,1,"complexity",mxCreateDoubleScalar(0));
    mxSetField(mxAutoinheritanceInfo,0,"outputs",mxData);
  }

  {
    mxSetField(mxAutoinheritanceInfo,0,"locals",mxCreateDoubleMatrix(0,0,mxREAL));
  }

  {
    mxArray* mxPostCodegenInfo = sf_c1_ImageDet_get_post_codegen_info();
    mxSetField(mxAutoinheritanceInfo,0,"postCodegenInfo",mxPostCodegenInfo);
  }

  return(mxAutoinheritanceInfo);
}

mxArray *sf_c1_ImageDet_third_party_uses_info(void)
{
  mxArray * mxcell3p = mxCreateCellMatrix(1,0);
  return(mxcell3p);
}

mxArray *sf_c1_ImageDet_jit_fallback_info(void)
{
  const char *infoFields[] = { "fallbackType", "fallbackReason",
    "incompatibleSymbol", };

  mxArray *mxInfo = mxCreateStructMatrix(1, 1, 3, infoFields);
  mxArray *fallbackReason = mxCreateString("feature_off");
  mxArray *incompatibleSymbol = mxCreateString("");
  mxArray *fallbackType = mxCreateString("early");
  mxSetField(mxInfo, 0, infoFields[0], fallbackType);
  mxSetField(mxInfo, 0, infoFields[1], fallbackReason);
  mxSetField(mxInfo, 0, infoFields[2], incompatibleSymbol);
  return mxInfo;
}

mxArray *sf_c1_ImageDet_updateBuildInfo_args_info(void)
{
  mxArray *mxBIArgs = mxCreateCellMatrix(1,0);
  return mxBIArgs;
}

mxArray* sf_c1_ImageDet_get_post_codegen_info(void)
{
  const char* fieldNames[] = { "exportedFunctionsUsedByThisChart",
    "exportedFunctionsChecksum" };

  mwSize dims[2] = { 1, 1 };

  mxArray* mxPostCodegenInfo = mxCreateStructArray(2, dims, sizeof(fieldNames)/
    sizeof(fieldNames[0]), fieldNames);

  {
    mxArray* mxExportedFunctionsChecksum = mxCreateString("");
    mwSize exp_dims[2] = { 0, 1 };

    mxArray* mxExportedFunctionsUsedByThisChart = mxCreateCellArray(2, exp_dims);
    mxSetField(mxPostCodegenInfo, 0, "exportedFunctionsUsedByThisChart",
               mxExportedFunctionsUsedByThisChart);
    mxSetField(mxPostCodegenInfo, 0, "exportedFunctionsChecksum",
               mxExportedFunctionsChecksum);
  }

  return mxPostCodegenInfo;
}

static const mxArray *sf_get_sim_state_info_c1_ImageDet(void)
{
  const char *infoFields[] = { "chartChecksum", "varInfo" };

  mxArray *mxInfo = mxCreateStructMatrix(1, 1, 2, infoFields);
  const char *infoEncStr[] = {
    "100 S1x3'type','srcId','name','auxInfo'{{M[1],M[14],T\"blueBool\",},{M[1],M[13],T\"redBool\",},{M[8],M[0],T\"is_active_c1_ImageDet\",}}"
  };

  mxArray *mxVarInfo = sf_mex_decode_encoded_mx_struct_array(infoEncStr, 3, 10);
  mxArray *mxChecksum = mxCreateDoubleMatrix(1, 4, mxREAL);
  sf_c1_ImageDet_get_check_sum(&mxChecksum);
  mxSetField(mxInfo, 0, infoFields[0], mxChecksum);
  mxSetField(mxInfo, 0, infoFields[1], mxVarInfo);
  return mxInfo;
}

static void chart_debug_initialization(SimStruct *S, unsigned int
  fullDebuggerInitialization)
{
  if (!sim_mode_is_rtw_gen(S)) {
    SFc1_ImageDetInstanceStruct *chartInstance;
    ChartRunTimeInfo * crtInfo = (ChartRunTimeInfo *)(ssGetUserData(S));
    ChartInfoStruct * chartInfo = (ChartInfoStruct *)(crtInfo->instanceInfo);
    chartInstance = (SFc1_ImageDetInstanceStruct *) chartInfo->chartInstance;
    if (ssIsFirstInitCond(S) && fullDebuggerInitialization==1) {
      /* do this only if simulation is starting */
      {
        unsigned int chartAlreadyPresent;
        chartAlreadyPresent = sf_debug_initialize_chart
          (sfGlobalDebugInstanceStruct,
           _ImageDetMachineNumber_,
           1,
           1,
           1,
           0,
           5,
           0,
           0,
           0,
           0,
           0,
           &(chartInstance->chartNumber),
           &(chartInstance->instanceNumber),
           (void *)S);

        /* Each instance must initialize its own list of scripts */
        init_script_number_translation(_ImageDetMachineNumber_,
          chartInstance->chartNumber,chartInstance->instanceNumber);
        if (chartAlreadyPresent==0) {
          /* this is the first instance */
          sf_debug_set_chart_disable_implicit_casting
            (sfGlobalDebugInstanceStruct,_ImageDetMachineNumber_,
             chartInstance->chartNumber,1);
          sf_debug_set_chart_event_thresholds(sfGlobalDebugInstanceStruct,
            _ImageDetMachineNumber_,
            chartInstance->chartNumber,
            0,
            0,
            0);
          _SFD_SET_DATA_PROPS(0,2,0,1,"redBool");
          _SFD_SET_DATA_PROPS(1,2,0,1,"blueBool");
          _SFD_SET_DATA_PROPS(2,1,1,0,"H");
          _SFD_SET_DATA_PROPS(3,1,1,0,"S");
          _SFD_SET_DATA_PROPS(4,1,1,0,"V");
          _SFD_STATE_INFO(0,0,2);
          _SFD_CH_SUBSTATE_COUNT(0);
          _SFD_CH_SUBSTATE_DECOMP(0);
        }

        _SFD_CV_INIT_CHART(0,0,0,0);

        {
          _SFD_CV_INIT_STATE(0,0,0,0,0,0,NULL,NULL);
        }

        _SFD_CV_INIT_TRANS(0,0,NULL,NULL,0,NULL);

        /* Initialization of MATLAB Function Model Coverage */
        _SFD_CV_INIT_EML(0,1,1,0,0,0,0,0,0,0,0);
        _SFD_CV_INIT_EML_FCN(0,0,"eML_blk_kernel",0,-1,429);

        {
          unsigned int dimVector[2];
          dimVector[0]= 320;
          dimVector[1]= 240;
          _SFD_SET_DATA_COMPILED_PROPS(0,SF_UINT8,2,&(dimVector[0]),0,0,0,0.0,
            1.0,0,0,(MexFcnForType)c1_sf_marshallOut,(MexInFcnForType)
            c1_sf_marshallIn);
        }

        {
          unsigned int dimVector[2];
          dimVector[0]= 320;
          dimVector[1]= 240;
          _SFD_SET_DATA_COMPILED_PROPS(1,SF_UINT8,2,&(dimVector[0]),0,0,0,0.0,
            1.0,0,0,(MexFcnForType)c1_sf_marshallOut,(MexInFcnForType)
            c1_sf_marshallIn);
        }

        {
          unsigned int dimVector[2];
          dimVector[0]= 320;
          dimVector[1]= 240;
          _SFD_SET_DATA_COMPILED_PROPS(2,SF_SINGLE,2,&(dimVector[0]),0,0,0,0.0,
            1.0,0,0,(MexFcnForType)c1_b_sf_marshallOut,(MexInFcnForType)NULL);
        }

        {
          unsigned int dimVector[2];
          dimVector[0]= 320;
          dimVector[1]= 240;
          _SFD_SET_DATA_COMPILED_PROPS(3,SF_SINGLE,2,&(dimVector[0]),0,0,0,0.0,
            1.0,0,0,(MexFcnForType)c1_b_sf_marshallOut,(MexInFcnForType)NULL);
        }

        {
          unsigned int dimVector[2];
          dimVector[0]= 320;
          dimVector[1]= 240;
          _SFD_SET_DATA_COMPILED_PROPS(4,SF_SINGLE,2,&(dimVector[0]),0,0,0,0.0,
            1.0,0,0,(MexFcnForType)c1_b_sf_marshallOut,(MexInFcnForType)NULL);
        }

        _SFD_SET_DATA_VALUE_PTR(0U, *chartInstance->c1_b_redBool);
        _SFD_SET_DATA_VALUE_PTR(1U, *chartInstance->c1_b_blueBool);
        _SFD_SET_DATA_VALUE_PTR(2U, *chartInstance->c1_b_H);
        _SFD_SET_DATA_VALUE_PTR(3U, *chartInstance->c1_b_S);
        _SFD_SET_DATA_VALUE_PTR(4U, *chartInstance->c1_b_V);
      }
    } else {
      sf_debug_reset_current_state_configuration(sfGlobalDebugInstanceStruct,
        _ImageDetMachineNumber_,chartInstance->chartNumber,
        chartInstance->instanceNumber);
    }
  }
}

static const char* sf_get_instance_specialization(void)
{
  return "LiNDKp54yE9FUToGpNz85D";
}

static void sf_opaque_initialize_c1_ImageDet(void *chartInstanceVar)
{
  chart_debug_initialization(((SFc1_ImageDetInstanceStruct*) chartInstanceVar)
    ->S,0);
  initialize_params_c1_ImageDet((SFc1_ImageDetInstanceStruct*) chartInstanceVar);
  initialize_c1_ImageDet((SFc1_ImageDetInstanceStruct*) chartInstanceVar);
}

static void sf_opaque_enable_c1_ImageDet(void *chartInstanceVar)
{
  enable_c1_ImageDet((SFc1_ImageDetInstanceStruct*) chartInstanceVar);
}

static void sf_opaque_disable_c1_ImageDet(void *chartInstanceVar)
{
  disable_c1_ImageDet((SFc1_ImageDetInstanceStruct*) chartInstanceVar);
}

static void sf_opaque_gateway_c1_ImageDet(void *chartInstanceVar)
{
  sf_gateway_c1_ImageDet((SFc1_ImageDetInstanceStruct*) chartInstanceVar);
}

static const mxArray* sf_opaque_get_sim_state_c1_ImageDet(SimStruct* S)
{
  ChartRunTimeInfo * crtInfo = (ChartRunTimeInfo *)(ssGetUserData(S));
  ChartInfoStruct * chartInfo = (ChartInfoStruct *)(crtInfo->instanceInfo);
  return get_sim_state_c1_ImageDet((SFc1_ImageDetInstanceStruct*)
    chartInfo->chartInstance);         /* raw sim ctx */
}

static void sf_opaque_set_sim_state_c1_ImageDet(SimStruct* S, const mxArray *st)
{
  ChartRunTimeInfo * crtInfo = (ChartRunTimeInfo *)(ssGetUserData(S));
  ChartInfoStruct * chartInfo = (ChartInfoStruct *)(crtInfo->instanceInfo);
  set_sim_state_c1_ImageDet((SFc1_ImageDetInstanceStruct*)
    chartInfo->chartInstance, st);
}

static void sf_opaque_terminate_c1_ImageDet(void *chartInstanceVar)
{
  if (chartInstanceVar!=NULL) {
    SimStruct *S = ((SFc1_ImageDetInstanceStruct*) chartInstanceVar)->S;
    ChartRunTimeInfo * crtInfo = (ChartRunTimeInfo *)(ssGetUserData(S));
    if (sim_mode_is_rtw_gen(S) || sim_mode_is_external(S)) {
      sf_clear_rtw_identifier(S);
      unload_ImageDet_optimization_info();
    }

    finalize_c1_ImageDet((SFc1_ImageDetInstanceStruct*) chartInstanceVar);
    utFree(chartInstanceVar);
    if (crtInfo != NULL) {
      utFree(crtInfo);
    }

    ssSetUserData(S,NULL);
  }
}

static void sf_opaque_init_subchart_simstructs(void *chartInstanceVar)
{
  initSimStructsc1_ImageDet((SFc1_ImageDetInstanceStruct*) chartInstanceVar);
}

extern unsigned int sf_machine_global_initializer_called(void);
static void mdlProcessParameters_c1_ImageDet(SimStruct *S)
{
  int i;
  for (i=0;i<ssGetNumRunTimeParams(S);i++) {
    if (ssGetSFcnParamTunable(S,i)) {
      ssUpdateDlgParamAsRunTimeParam(S,i);
    }
  }

  if (sf_machine_global_initializer_called()) {
    ChartRunTimeInfo * crtInfo = (ChartRunTimeInfo *)(ssGetUserData(S));
    ChartInfoStruct * chartInfo = (ChartInfoStruct *)(crtInfo->instanceInfo);
    initialize_params_c1_ImageDet((SFc1_ImageDetInstanceStruct*)
      (chartInfo->chartInstance));
  }
}

static void mdlSetWorkWidths_c1_ImageDet(SimStruct *S)
{
  if (sim_mode_is_rtw_gen(S) || sim_mode_is_external(S)) {
    mxArray *infoStruct = load_ImageDet_optimization_info();
    int_T chartIsInlinable =
      (int_T)sf_is_chart_inlinable(sf_get_instance_specialization(),infoStruct,1);
    ssSetStateflowIsInlinable(S,chartIsInlinable);
    ssSetRTWCG(S,sf_rtw_info_uint_prop(sf_get_instance_specialization(),
                infoStruct,1,"RTWCG"));
    ssSetEnableFcnIsTrivial(S,1);
    ssSetDisableFcnIsTrivial(S,1);
    ssSetNotMultipleInlinable(S,sf_rtw_info_uint_prop
      (sf_get_instance_specialization(),infoStruct,1,
       "gatewayCannotBeInlinedMultipleTimes"));
    sf_update_buildInfo(sf_get_instance_specialization(),infoStruct,1);
    if (chartIsInlinable) {
      ssSetInputPortOptimOpts(S, 0, SS_REUSABLE_AND_LOCAL);
      ssSetInputPortOptimOpts(S, 1, SS_REUSABLE_AND_LOCAL);
      ssSetInputPortOptimOpts(S, 2, SS_REUSABLE_AND_LOCAL);
      sf_mark_chart_expressionable_inputs(S,sf_get_instance_specialization(),
        infoStruct,1,3);
      sf_mark_chart_reusable_outputs(S,sf_get_instance_specialization(),
        infoStruct,1,2);
    }

    {
      unsigned int outPortIdx;
      for (outPortIdx=1; outPortIdx<=2; ++outPortIdx) {
        ssSetOutputPortOptimizeInIR(S, outPortIdx, 1U);
      }
    }

    {
      unsigned int inPortIdx;
      for (inPortIdx=0; inPortIdx < 3; ++inPortIdx) {
        ssSetInputPortOptimizeInIR(S, inPortIdx, 1U);
      }
    }

    sf_set_rtw_dwork_info(S,sf_get_instance_specialization(),infoStruct,1);
    ssSetHasSubFunctions(S,!(chartIsInlinable));
  } else {
  }

  ssSetOptions(S,ssGetOptions(S)|SS_OPTION_WORKS_WITH_CODE_REUSE);
  ssSetChecksum0(S,(1631747466U));
  ssSetChecksum1(S,(457107425U));
  ssSetChecksum2(S,(3337890271U));
  ssSetChecksum3(S,(3420478593U));
  ssSetmdlDerivatives(S, NULL);
  ssSetExplicitFCSSCtrl(S,1);
  ssSupportsMultipleExecInstances(S,1);
}

static void mdlRTW_c1_ImageDet(SimStruct *S)
{
  if (sim_mode_is_rtw_gen(S)) {
    ssWriteRTWStrParam(S, "StateflowChartType", "Embedded MATLAB");
  }
}

static void mdlStart_c1_ImageDet(SimStruct *S)
{
  SFc1_ImageDetInstanceStruct *chartInstance;
  ChartRunTimeInfo * crtInfo = (ChartRunTimeInfo *)utMalloc(sizeof
    (ChartRunTimeInfo));
  chartInstance = (SFc1_ImageDetInstanceStruct *)utMalloc(sizeof
    (SFc1_ImageDetInstanceStruct));
  memset(chartInstance, 0, sizeof(SFc1_ImageDetInstanceStruct));
  if (chartInstance==NULL) {
    sf_mex_error_message("Could not allocate memory for chart instance.");
  }

  chartInstance->chartInfo.chartInstance = chartInstance;
  chartInstance->chartInfo.isEMLChart = 1;
  chartInstance->chartInfo.chartInitialized = 0;
  chartInstance->chartInfo.sFunctionGateway = sf_opaque_gateway_c1_ImageDet;
  chartInstance->chartInfo.initializeChart = sf_opaque_initialize_c1_ImageDet;
  chartInstance->chartInfo.terminateChart = sf_opaque_terminate_c1_ImageDet;
  chartInstance->chartInfo.enableChart = sf_opaque_enable_c1_ImageDet;
  chartInstance->chartInfo.disableChart = sf_opaque_disable_c1_ImageDet;
  chartInstance->chartInfo.getSimState = sf_opaque_get_sim_state_c1_ImageDet;
  chartInstance->chartInfo.setSimState = sf_opaque_set_sim_state_c1_ImageDet;
  chartInstance->chartInfo.getSimStateInfo = sf_get_sim_state_info_c1_ImageDet;
  chartInstance->chartInfo.zeroCrossings = NULL;
  chartInstance->chartInfo.outputs = NULL;
  chartInstance->chartInfo.derivatives = NULL;
  chartInstance->chartInfo.mdlRTW = mdlRTW_c1_ImageDet;
  chartInstance->chartInfo.mdlStart = mdlStart_c1_ImageDet;
  chartInstance->chartInfo.mdlSetWorkWidths = mdlSetWorkWidths_c1_ImageDet;
  chartInstance->chartInfo.extModeExec = NULL;
  chartInstance->chartInfo.restoreLastMajorStepConfiguration = NULL;
  chartInstance->chartInfo.restoreBeforeLastMajorStepConfiguration = NULL;
  chartInstance->chartInfo.storeCurrentConfiguration = NULL;
  chartInstance->chartInfo.callAtomicSubchartUserFcn = NULL;
  chartInstance->chartInfo.callAtomicSubchartAutoFcn = NULL;
  chartInstance->chartInfo.debugInstance = sfGlobalDebugInstanceStruct;
  chartInstance->S = S;
  crtInfo->checksum = SF_RUNTIME_INFO_CHECKSUM;
  crtInfo->instanceInfo = (&(chartInstance->chartInfo));
  crtInfo->isJITEnabled = false;
  crtInfo->compiledInfo = NULL;
  ssSetUserData(S,(void *)(crtInfo));  /* register the chart instance with simstruct */
  init_dsm_address_info(chartInstance);
  init_simulink_io_address(chartInstance);
  if (!sim_mode_is_rtw_gen(S)) {
  }

  sf_opaque_init_subchart_simstructs(chartInstance->chartInfo.chartInstance);
  chart_debug_initialization(S,1);
}

void c1_ImageDet_method_dispatcher(SimStruct *S, int_T method, void *data)
{
  switch (method) {
   case SS_CALL_MDL_START:
    mdlStart_c1_ImageDet(S);
    break;

   case SS_CALL_MDL_SET_WORK_WIDTHS:
    mdlSetWorkWidths_c1_ImageDet(S);
    break;

   case SS_CALL_MDL_PROCESS_PARAMETERS:
    mdlProcessParameters_c1_ImageDet(S);
    break;

   default:
    /* Unhandled method */
    sf_mex_error_message("Stateflow Internal Error:\n"
                         "Error calling c1_ImageDet_method_dispatcher.\n"
                         "Can't handle method %d.\n", method);
    break;
  }
}

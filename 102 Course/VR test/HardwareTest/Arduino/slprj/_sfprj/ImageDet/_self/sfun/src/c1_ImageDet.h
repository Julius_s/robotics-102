#ifndef __c1_ImageDet_h__
#define __c1_ImageDet_h__

/* Include files */
#include "sf_runtime/sfc_sf.h"
#include "sf_runtime/sfc_mex.h"
#include "rtwtypes.h"
#include "multiword_types.h"

/* Type Definitions */
#ifndef typedef_SFc1_ImageDetInstanceStruct
#define typedef_SFc1_ImageDetInstanceStruct

typedef struct {
  SimStruct *S;
  ChartInfoStruct chartInfo;
  uint32_T chartNumber;
  uint32_T instanceNumber;
  int32_T c1_sfEvent;
  boolean_T c1_isStable;
  boolean_T c1_doneDoubleBufferReInit;
  uint8_T c1_is_active_c1_ImageDet;
  real32_T c1_H[76800];
  real32_T c1_S[76800];
  real32_T c1_V[76800];
  boolean_T c1_redBool[76800];
  boolean_T c1_blueBool[76800];
  real32_T c1_inData[76800];
  real32_T c1_u[76800];
  boolean_T c1_bv0[76800];
  boolean_T (*c1_b_redBool)[76800];
  boolean_T (*c1_b_blueBool)[76800];
  real32_T (*c1_b_H)[76800];
  real32_T (*c1_b_S)[76800];
  real32_T (*c1_b_V)[76800];
} SFc1_ImageDetInstanceStruct;

#endif                                 /*typedef_SFc1_ImageDetInstanceStruct*/

/* Named Constants */

/* Variable Declarations */
extern struct SfDebugInstanceStruct *sfGlobalDebugInstanceStruct;

/* Variable Definitions */

/* Function Declarations */
extern const mxArray *sf_c1_ImageDet_get_eml_resolved_functions_info(void);

/* Function Definitions */
extern void sf_c1_ImageDet_get_check_sum(mxArray *plhs[]);
extern void c1_ImageDet_method_dispatcher(SimStruct *S, int_T method, void *data);

#endif

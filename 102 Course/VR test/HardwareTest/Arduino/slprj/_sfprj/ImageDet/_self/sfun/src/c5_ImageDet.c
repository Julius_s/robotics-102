/* Include files */

#include <stddef.h>
#include "blas.h"
#include "ImageDet_sfun.h"
#include "c5_ImageDet.h"
#include "mwmathutil.h"
#define CHARTINSTANCE_CHARTNUMBER      (chartInstance->chartNumber)
#define CHARTINSTANCE_INSTANCENUMBER   (chartInstance->instanceNumber)
#include "ImageDet_sfun_debug_macros.h"
#define _SF_MEX_LISTEN_FOR_CTRL_C(S)   sf_mex_listen_for_ctrl_c(sfGlobalDebugInstanceStruct,S);

/* Type Definitions */

/* Named Constants */
#define CALL_EVENT                     (-1)

/* Variable Declarations */

/* Variable Definitions */
static real_T _sfTime_;
static const char * c5_debug_family_names[14] = { "lenA", "centroidsInt", "ii",
  "cr", "cb", "colorVec", "color", "nargin", "nargout", "centroids", "area",
  "RBW", "BBW", "blobs" };

/* Function Declarations */
static void initialize_c5_ImageDet(SFc5_ImageDetInstanceStruct *chartInstance);
static void initialize_params_c5_ImageDet(SFc5_ImageDetInstanceStruct
  *chartInstance);
static void enable_c5_ImageDet(SFc5_ImageDetInstanceStruct *chartInstance);
static void disable_c5_ImageDet(SFc5_ImageDetInstanceStruct *chartInstance);
static void c5_update_debugger_state_c5_ImageDet(SFc5_ImageDetInstanceStruct
  *chartInstance);
static const mxArray *get_sim_state_c5_ImageDet(SFc5_ImageDetInstanceStruct
  *chartInstance);
static void set_sim_state_c5_ImageDet(SFc5_ImageDetInstanceStruct *chartInstance,
  const mxArray *c5_st);
static void finalize_c5_ImageDet(SFc5_ImageDetInstanceStruct *chartInstance);
static void sf_gateway_c5_ImageDet(SFc5_ImageDetInstanceStruct *chartInstance);
static void mdl_start_c5_ImageDet(SFc5_ImageDetInstanceStruct *chartInstance);
static void c5_chartstep_c5_ImageDet(SFc5_ImageDetInstanceStruct *chartInstance);
static void initSimStructsc5_ImageDet(SFc5_ImageDetInstanceStruct *chartInstance);
static void init_script_number_translation(uint32_T c5_machineNumber, uint32_T
  c5_chartNumber, uint32_T c5_instanceNumber);
static const mxArray *c5_sf_marshallOut(void *chartInstanceVoid, c5_BlobData
  *c5_inData_data, c5_BlobData_size *c5_inData_elems_sizes);
static void c5_emlrt_marshallIn(SFc5_ImageDetInstanceStruct *chartInstance,
  const mxArray *c5_blobs, const char_T *c5_identifier, c5_BlobData *c5_y_data,
  c5_BlobData_size *c5_y_elems_sizes);
static void c5_b_emlrt_marshallIn(SFc5_ImageDetInstanceStruct *chartInstance,
  const mxArray *c5_u, const emlrtMsgIdentifier *c5_parentId, c5_BlobData
  *c5_y_data, c5_BlobData_size *c5_y_elems_sizes);
static void c5_c_emlrt_marshallIn(SFc5_ImageDetInstanceStruct *chartInstance,
  const mxArray *c5_u, const emlrtMsgIdentifier *c5_parentId, real32_T
  c5_y_data[], int32_T c5_y_sizes[2]);
static void c5_d_emlrt_marshallIn(SFc5_ImageDetInstanceStruct *chartInstance,
  const mxArray *c5_u, const emlrtMsgIdentifier *c5_parentId, int32_T c5_y_data[],
  int32_T *c5_y_sizes);
static void c5_e_emlrt_marshallIn(SFc5_ImageDetInstanceStruct *chartInstance,
  const mxArray *c5_u, const emlrtMsgIdentifier *c5_parentId, uint8_T c5_y_data[],
  int32_T *c5_y_sizes);
static void c5_sf_marshallIn(void *chartInstanceVoid, const mxArray
  *c5_mxArrayInData, const char_T *c5_varName, c5_BlobData *c5_outData_data,
  c5_BlobData_size *c5_outData_elems_sizes);
static const mxArray *c5_b_sf_marshallOut(void *chartInstanceVoid, void
  *c5_inData);
static const mxArray *c5_c_sf_marshallOut(void *chartInstanceVoid, int32_T
  c5_inData_data[], int32_T c5_inData_sizes[2]);
static const mxArray *c5_d_sf_marshallOut(void *chartInstanceVoid, real32_T
  c5_inData_data[], int32_T c5_inData_sizes[2]);
static const mxArray *c5_e_sf_marshallOut(void *chartInstanceVoid, void
  *c5_inData);
static real_T c5_f_emlrt_marshallIn(SFc5_ImageDetInstanceStruct *chartInstance,
  const mxArray *c5_u, const emlrtMsgIdentifier *c5_parentId);
static void c5_b_sf_marshallIn(void *chartInstanceVoid, const mxArray
  *c5_mxArrayInData, const char_T *c5_varName, void *c5_outData);
static const mxArray *c5_f_sf_marshallOut(void *chartInstanceVoid, real_T
  c5_inData_data[], int32_T c5_inData_sizes[2]);
static void c5_g_emlrt_marshallIn(SFc5_ImageDetInstanceStruct *chartInstance,
  const mxArray *c5_u, const emlrtMsgIdentifier *c5_parentId, real_T c5_y_data[],
  int32_T c5_y_sizes[2]);
static void c5_c_sf_marshallIn(void *chartInstanceVoid, const mxArray
  *c5_mxArrayInData, const char_T *c5_varName, real_T c5_outData_data[], int32_T
  c5_outData_sizes[2]);
static const mxArray *c5_g_sf_marshallOut(void *chartInstanceVoid, void
  *c5_inData);
static void c5_h_emlrt_marshallIn(SFc5_ImageDetInstanceStruct *chartInstance,
  const mxArray *c5_u, const emlrtMsgIdentifier *c5_parentId, boolean_T c5_y[2]);
static void c5_d_sf_marshallIn(void *chartInstanceVoid, const mxArray
  *c5_mxArrayInData, const char_T *c5_varName, void *c5_outData);
static const mxArray *c5_h_sf_marshallOut(void *chartInstanceVoid, void
  *c5_inData);
static boolean_T c5_i_emlrt_marshallIn(SFc5_ImageDetInstanceStruct
  *chartInstance, const mxArray *c5_u, const emlrtMsgIdentifier *c5_parentId);
static void c5_e_sf_marshallIn(void *chartInstanceVoid, const mxArray
  *c5_mxArrayInData, const char_T *c5_varName, void *c5_outData);
static void c5_f_sf_marshallIn(void *chartInstanceVoid, const mxArray
  *c5_mxArrayInData, const char_T *c5_varName, real32_T c5_outData_data[],
  int32_T c5_outData_sizes[2]);
static void c5_info_helper(const mxArray **c5_info);
static const mxArray *c5_emlrt_marshallOut(const char * c5_u);
static const mxArray *c5_b_emlrt_marshallOut(const uint32_T c5_u);
static void c5_floor(SFc5_ImageDetInstanceStruct *chartInstance, real32_T
                     c5_x_data[], int32_T c5_x_sizes[2], real32_T c5_b_x_data[],
                     int32_T c5_b_x_sizes[2]);
static const mxArray *c5_i_sf_marshallOut(void *chartInstanceVoid, void
  *c5_inData);
static int32_T c5_j_emlrt_marshallIn(SFc5_ImageDetInstanceStruct *chartInstance,
  const mxArray *c5_u, const emlrtMsgIdentifier *c5_parentId);
static void c5_g_sf_marshallIn(void *chartInstanceVoid, const mxArray
  *c5_mxArrayInData, const char_T *c5_varName, void *c5_outData);
static const mxArray *c5_blobs_bus_io(void *chartInstanceVoid, void *c5_pData);
static const mxArray *c5_sf_marshall_unsupported(void *chartInstanceVoid);
static const mxArray *c5_c_emlrt_marshallOut(SFc5_ImageDetInstanceStruct
  *chartInstance, const char * c5_u);
static uint8_T c5_k_emlrt_marshallIn(SFc5_ImageDetInstanceStruct *chartInstance,
  const mxArray *c5_b_is_active_c5_ImageDet, const char_T *c5_identifier);
static uint8_T c5_l_emlrt_marshallIn(SFc5_ImageDetInstanceStruct *chartInstance,
  const mxArray *c5_u, const emlrtMsgIdentifier *c5_parentId);
static void c5_b_floor(SFc5_ImageDetInstanceStruct *chartInstance, real32_T
  c5_x_data[], int32_T c5_x_sizes[2]);
static void init_dsm_address_info(SFc5_ImageDetInstanceStruct *chartInstance);
static void init_simulink_io_address(SFc5_ImageDetInstanceStruct *chartInstance);

/* Function Definitions */
static void initialize_c5_ImageDet(SFc5_ImageDetInstanceStruct *chartInstance)
{
  chartInstance->c5_sfEvent = CALL_EVENT;
  _sfTime_ = sf_get_time(chartInstance->S);
  chartInstance->c5_is_active_c5_ImageDet = 0U;
}

static void initialize_params_c5_ImageDet(SFc5_ImageDetInstanceStruct
  *chartInstance)
{
  (void)chartInstance;
}

static void enable_c5_ImageDet(SFc5_ImageDetInstanceStruct *chartInstance)
{
  _sfTime_ = sf_get_time(chartInstance->S);
}

static void disable_c5_ImageDet(SFc5_ImageDetInstanceStruct *chartInstance)
{
  _sfTime_ = sf_get_time(chartInstance->S);
}

static void c5_update_debugger_state_c5_ImageDet(SFc5_ImageDetInstanceStruct
  *chartInstance)
{
  (void)chartInstance;
}

static const mxArray *get_sim_state_c5_ImageDet(SFc5_ImageDetInstanceStruct
  *chartInstance)
{
  const mxArray *c5_st;
  const mxArray *c5_y = NULL;
  const mxArray *c5_b_y = NULL;
  int32_T c5_u_sizes[2];
  int32_T c5_u;
  int32_T c5_b_u;
  int32_T c5_loop_ub;
  int32_T c5_i0;
  real32_T c5_u_data[32];
  const mxArray *c5_c_y = NULL;
  int32_T c5_b_u_sizes;
  int32_T c5_b_loop_ub;
  int32_T c5_i1;
  int32_T c5_b_u_data[16];
  const mxArray *c5_d_y = NULL;
  int32_T c5_c_u_sizes;
  int32_T c5_c_loop_ub;
  int32_T c5_i2;
  uint8_T c5_c_u_data[16];
  const mxArray *c5_e_y = NULL;
  uint8_T c5_hoistedGlobal;
  uint8_T c5_c_u;
  const mxArray *c5_f_y = NULL;
  c5_st = NULL;
  c5_st = NULL;
  c5_y = NULL;
  sf_mex_assign(&c5_y, sf_mex_createcellmatrix(2, 1), false);
  c5_b_y = NULL;
  sf_mex_assign(&c5_b_y, sf_mex_createstruct("structure", 2, 1, 1), false);
  c5_u_sizes[0] = chartInstance->c5_blobs_elems_sizes->centroid[0];
  c5_u_sizes[1] = chartInstance->c5_blobs_elems_sizes->centroid[1];
  c5_u = c5_u_sizes[0];
  c5_b_u = c5_u_sizes[1];
  c5_loop_ub = chartInstance->c5_blobs_elems_sizes->centroid[0] *
    chartInstance->c5_blobs_elems_sizes->centroid[1] - 1;
  for (c5_i0 = 0; c5_i0 <= c5_loop_ub; c5_i0++) {
    c5_u_data[c5_i0] = ((real32_T *)&((char_T *)chartInstance->c5_blobs_data)[0])
      [c5_i0];
  }

  c5_c_y = NULL;
  sf_mex_assign(&c5_c_y, sf_mex_create("y", c5_u_data, 1, 0U, 1U, 0U, 2,
    c5_u_sizes[0], c5_u_sizes[1]), false);
  sf_mex_addfield(c5_b_y, c5_c_y, "centroid", "centroid", 0);
  c5_b_u_sizes = chartInstance->c5_blobs_elems_sizes->area;
  c5_b_loop_ub = chartInstance->c5_blobs_elems_sizes->area - 1;
  for (c5_i1 = 0; c5_i1 <= c5_b_loop_ub; c5_i1++) {
    c5_b_u_data[c5_i1] = ((int32_T *)&((char_T *)chartInstance->c5_blobs_data)
                          [128])[c5_i1];
  }

  c5_d_y = NULL;
  sf_mex_assign(&c5_d_y, sf_mex_create("y", c5_b_u_data, 6, 0U, 1U, 0U, 1,
    c5_b_u_sizes), false);
  sf_mex_addfield(c5_b_y, c5_d_y, "area", "area", 0);
  c5_c_u_sizes = chartInstance->c5_blobs_elems_sizes->color;
  c5_c_loop_ub = chartInstance->c5_blobs_elems_sizes->color - 1;
  for (c5_i2 = 0; c5_i2 <= c5_c_loop_ub; c5_i2++) {
    c5_c_u_data[c5_i2] = ((uint8_T *)&((char_T *)chartInstance->c5_blobs_data)
                          [192])[c5_i2];
  }

  c5_e_y = NULL;
  sf_mex_assign(&c5_e_y, sf_mex_create("y", c5_c_u_data, 3, 0U, 1U, 0U, 1,
    c5_c_u_sizes), false);
  sf_mex_addfield(c5_b_y, c5_e_y, "color", "color", 0);
  sf_mex_setcell(c5_y, 0, c5_b_y);
  c5_hoistedGlobal = chartInstance->c5_is_active_c5_ImageDet;
  c5_c_u = c5_hoistedGlobal;
  c5_f_y = NULL;
  sf_mex_assign(&c5_f_y, sf_mex_create("y", &c5_c_u, 3, 0U, 0U, 0U, 0), false);
  sf_mex_setcell(c5_y, 1, c5_f_y);
  sf_mex_assign(&c5_st, c5_y, false);
  return c5_st;
}

static void set_sim_state_c5_ImageDet(SFc5_ImageDetInstanceStruct *chartInstance,
  const mxArray *c5_st)
{
  const mxArray *c5_u;
  c5_BlobData_size c5_tmp_elems_sizes;
  c5_BlobData c5_tmp_data;
  int32_T c5_loop_ub;
  int32_T c5_i3;
  int32_T c5_b_loop_ub;
  int32_T c5_i4;
  int32_T c5_c_loop_ub;
  int32_T c5_i5;
  int32_T c5_d_loop_ub;
  int32_T c5_i6;
  chartInstance->c5_doneDoubleBufferReInit = true;
  c5_u = sf_mex_dup(c5_st);
  c5_emlrt_marshallIn(chartInstance, sf_mex_dup(sf_mex_getcell(c5_u, 0)),
                      "blobs", &c5_tmp_data, &c5_tmp_elems_sizes);
  chartInstance->c5_blobs_elems_sizes->centroid[0] =
    c5_tmp_elems_sizes.centroid[0];
  chartInstance->c5_blobs_elems_sizes->centroid[1] =
    c5_tmp_elems_sizes.centroid[1];
  c5_loop_ub = c5_tmp_elems_sizes.centroid[1] - 1;
  for (c5_i3 = 0; c5_i3 <= c5_loop_ub; c5_i3++) {
    c5_b_loop_ub = c5_tmp_elems_sizes.centroid[0] - 1;
    for (c5_i4 = 0; c5_i4 <= c5_b_loop_ub; c5_i4++) {
      ((real32_T *)&((char_T *)chartInstance->c5_blobs_data)[0])[c5_i4 +
        chartInstance->c5_blobs_elems_sizes->centroid[0] * c5_i3] =
        c5_tmp_data.centroid[c5_i4 + c5_tmp_elems_sizes.centroid[0] * c5_i3];
    }
  }

  chartInstance->c5_blobs_elems_sizes->area = c5_tmp_elems_sizes.area;
  c5_c_loop_ub = c5_tmp_elems_sizes.area - 1;
  for (c5_i5 = 0; c5_i5 <= c5_c_loop_ub; c5_i5++) {
    ((int32_T *)&((char_T *)chartInstance->c5_blobs_data)[128])[c5_i5] =
      c5_tmp_data.area[c5_i5];
  }

  chartInstance->c5_blobs_elems_sizes->color = c5_tmp_elems_sizes.color;
  c5_d_loop_ub = c5_tmp_elems_sizes.color - 1;
  for (c5_i6 = 0; c5_i6 <= c5_d_loop_ub; c5_i6++) {
    ((uint8_T *)&((char_T *)chartInstance->c5_blobs_data)[192])[c5_i6] =
      c5_tmp_data.color[c5_i6];
  }

  chartInstance->c5_is_active_c5_ImageDet = c5_k_emlrt_marshallIn(chartInstance,
    sf_mex_dup(sf_mex_getcell(c5_u, 1)), "is_active_c5_ImageDet");
  sf_mex_destroy(&c5_u);
  c5_update_debugger_state_c5_ImageDet(chartInstance);
  sf_mex_destroy(&c5_st);
}

static void finalize_c5_ImageDet(SFc5_ImageDetInstanceStruct *chartInstance)
{
  (void)chartInstance;
}

static void sf_gateway_c5_ImageDet(SFc5_ImageDetInstanceStruct *chartInstance)
{
  int32_T c5_loop_ub;
  int32_T c5_i7;
  int32_T c5_b_loop_ub;
  int32_T c5_i8;
  int32_T c5_i9;
  int32_T c5_i10;
  _SFD_SYMBOL_SCOPE_PUSH(0U, 0U);
  _sfTime_ = sf_get_time(chartInstance->S);
  _SFD_CC_CALL(CHART_ENTER_SFUNCTION_TAG, 3U, chartInstance->c5_sfEvent);
  c5_loop_ub = (*chartInstance->c5_centroids_sizes)[0] *
    (*chartInstance->c5_centroids_sizes)[1] - 1;
  for (c5_i7 = 0; c5_i7 <= c5_loop_ub; c5_i7++) {
    _SFD_DATA_RANGE_CHECK((real_T)(*chartInstance->c5_centroids_data)[c5_i7], 0U);
  }

  c5_b_loop_ub = (*chartInstance->c5_area_sizes)[0] *
    (*chartInstance->c5_area_sizes)[1] - 1;
  for (c5_i8 = 0; c5_i8 <= c5_b_loop_ub; c5_i8++) {
    _SFD_DATA_RANGE_CHECK((real_T)(*chartInstance->c5_area_data)[c5_i8], 1U);
  }

  for (c5_i9 = 0; c5_i9 < 76800; c5_i9++) {
    _SFD_DATA_RANGE_CHECK((real_T)(*chartInstance->c5_RBW)[c5_i9], 2U);
  }

  for (c5_i10 = 0; c5_i10 < 76800; c5_i10++) {
    _SFD_DATA_RANGE_CHECK((real_T)(*chartInstance->c5_BBW)[c5_i10], 3U);
  }

  chartInstance->c5_sfEvent = CALL_EVENT;
  c5_chartstep_c5_ImageDet(chartInstance);
  _SFD_SYMBOL_SCOPE_POP();
  _SFD_CHECK_FOR_STATE_INCONSISTENCY(_ImageDetMachineNumber_,
    chartInstance->chartNumber, chartInstance->instanceNumber);
}

static void mdl_start_c5_ImageDet(SFc5_ImageDetInstanceStruct *chartInstance)
{
  (void)chartInstance;
}

static void c5_chartstep_c5_ImageDet(SFc5_ImageDetInstanceStruct *chartInstance)
{
  int32_T c5_b_centroids_sizes[2];
  int32_T c5_centroids;
  int32_T c5_b_centroids;
  int32_T c5_loop_ub;
  int32_T c5_i11;
  real32_T c5_b_centroids_data[32];
  int32_T c5_b_area_sizes[2];
  int32_T c5_area;
  int32_T c5_b_area;
  int32_T c5_b_loop_ub;
  int32_T c5_i12;
  int32_T c5_b_area_data[16];
  int32_T c5_i13;
  boolean_T c5_b_RBW[76800];
  int32_T c5_i14;
  boolean_T c5_b_BBW[76800];
  uint32_T c5_debug_family_var_map[14];
  real_T c5_lenA;
  int32_T c5_centroidsInt_sizes[2];
  real32_T c5_centroidsInt_data[32];
  real_T c5_ii;
  boolean_T c5_cr;
  boolean_T c5_cb;
  boolean_T c5_colorVec[2];
  int32_T c5_color_sizes[2];
  real_T c5_color_data[2];
  real_T c5_nargin = 4.0;
  real_T c5_nargout = 1.0;
  c5_BlobData_size c5_b_blobs_elems_sizes;
  c5_BlobData c5_b_blobs_data;
  int32_T c5_x_sizes;
  int32_T c5_c_loop_ub;
  int32_T c5_i15;
  int32_T c5_x_data[16];
  int32_T c5_tmp_sizes[2];
  int32_T c5_i16;
  int32_T c5_i17;
  int32_T c5_d_loop_ub;
  int32_T c5_i18;
  real32_T c5_tmp_data[32];
  int32_T c5_centroidsInt;
  int32_T c5_b_centroidsInt;
  int32_T c5_e_loop_ub;
  int32_T c5_i19;
  int32_T c5_blobs;
  int32_T c5_b_blobs;
  real_T c5_b_lenA;
  int32_T c5_i20;
  int32_T c5_b_ii;
  int32_T c5_i21;
  boolean_T c5_x[2];
  int32_T c5_idx;
  int32_T c5_i22;
  int32_T c5_ii_sizes[2];
  int32_T c5_c_ii;
  int32_T c5_d_ii;
  int32_T c5_a;
  int32_T c5_b_a;
  int32_T c5_ii_data[2];
  boolean_T c5_b0;
  boolean_T c5_b1;
  boolean_T c5_b2;
  int32_T c5_i23;
  int32_T c5_color;
  int32_T c5_b_color;
  int32_T c5_f_loop_ub;
  int32_T c5_i24;
  int32_T c5_b_x_sizes[2];
  int32_T c5_b_x;
  int32_T c5_c_x;
  int32_T c5_g_loop_ub;
  int32_T c5_i25;
  real_T c5_b_x_data[2];
  real_T c5_d0;
  int32_T c5_i26;
  int32_T c5_e_ii;
  int32_T c5_b_tmp_sizes[2];
  int32_T c5_h_loop_ub;
  int32_T c5_i27;
  real32_T c5_b_tmp_data[2];
  int32_T c5_c_tmp_sizes[2];
  int32_T c5_i_loop_ub;
  int32_T c5_i28;
  int32_T c5_j_loop_ub;
  int32_T c5_i29;
  real32_T c5_c_tmp_data[34];
  int32_T c5_k_loop_ub;
  int32_T c5_i30;
  int32_T c5_c_blobs;
  int32_T c5_d_blobs;
  int32_T c5_l_loop_ub;
  int32_T c5_i31;
  int32_T c5_m_loop_ub;
  int32_T c5_i32;
  int32_T c5_d_tmp_sizes;
  int32_T c5_n_loop_ub;
  int32_T c5_i33;
  int32_T c5_d_tmp_data[17];
  int32_T c5_o_loop_ub;
  int32_T c5_i34;
  int32_T c5_e_tmp_sizes;
  int32_T c5_p_loop_ub;
  int32_T c5_i35;
  uint8_T c5_e_tmp_data[17];
  real_T c5_d1;
  uint8_T c5_u0;
  int32_T c5_q_loop_ub;
  int32_T c5_i36;
  int32_T c5_r_loop_ub;
  int32_T c5_i37;
  int32_T c5_s_loop_ub;
  int32_T c5_i38;
  int32_T c5_t_loop_ub;
  int32_T c5_i39;
  int32_T c5_u_loop_ub;
  int32_T c5_i40;
  boolean_T exitg1;
  boolean_T guard1 = false;
  _SFD_CC_CALL(CHART_ENTER_DURING_FUNCTION_TAG, 3U, chartInstance->c5_sfEvent);
  c5_b_centroids_sizes[0] = (*chartInstance->c5_centroids_sizes)[0];
  c5_b_centroids_sizes[1] = (*chartInstance->c5_centroids_sizes)[1];
  c5_centroids = c5_b_centroids_sizes[0];
  c5_b_centroids = c5_b_centroids_sizes[1];
  c5_loop_ub = (*chartInstance->c5_centroids_sizes)[0] *
    (*chartInstance->c5_centroids_sizes)[1] - 1;
  for (c5_i11 = 0; c5_i11 <= c5_loop_ub; c5_i11++) {
    c5_b_centroids_data[c5_i11] = (*chartInstance->c5_centroids_data)[c5_i11];
  }

  c5_b_area_sizes[0] = (*chartInstance->c5_area_sizes)[0];
  c5_b_area_sizes[1] = 1;
  c5_area = c5_b_area_sizes[0];
  c5_b_area = c5_b_area_sizes[1];
  c5_b_loop_ub = (*chartInstance->c5_area_sizes)[0] *
    (*chartInstance->c5_area_sizes)[1] - 1;
  for (c5_i12 = 0; c5_i12 <= c5_b_loop_ub; c5_i12++) {
    c5_b_area_data[c5_i12] = (*chartInstance->c5_area_data)[c5_i12];
  }

  for (c5_i13 = 0; c5_i13 < 76800; c5_i13++) {
    c5_b_RBW[c5_i13] = (*chartInstance->c5_RBW)[c5_i13];
  }

  for (c5_i14 = 0; c5_i14 < 76800; c5_i14++) {
    c5_b_BBW[c5_i14] = (*chartInstance->c5_BBW)[c5_i14];
  }

  _SFD_SYMBOL_SCOPE_PUSH_EML(0U, 14U, 14U, c5_debug_family_names,
    c5_debug_family_var_map);
  _SFD_SYMBOL_SCOPE_ADD_EML_IMPORTABLE(&c5_lenA, 0U, c5_e_sf_marshallOut,
    c5_b_sf_marshallIn);
  _SFD_SYMBOL_SCOPE_ADD_EML_DYN_IMPORTABLE(c5_centroidsInt_data, (const int32_T *)
    &c5_centroidsInt_sizes, NULL, 0, 1, (void *)c5_d_sf_marshallOut, (void *)
    c5_f_sf_marshallIn);
  _SFD_SYMBOL_SCOPE_ADD_EML_IMPORTABLE(&c5_ii, 2U, c5_e_sf_marshallOut,
    c5_b_sf_marshallIn);
  _SFD_SYMBOL_SCOPE_ADD_EML_IMPORTABLE(&c5_cr, 3U, c5_h_sf_marshallOut,
    c5_e_sf_marshallIn);
  _SFD_SYMBOL_SCOPE_ADD_EML_IMPORTABLE(&c5_cb, 4U, c5_h_sf_marshallOut,
    c5_e_sf_marshallIn);
  _SFD_SYMBOL_SCOPE_ADD_EML_IMPORTABLE(c5_colorVec, 5U, c5_g_sf_marshallOut,
    c5_d_sf_marshallIn);
  _SFD_SYMBOL_SCOPE_ADD_EML_DYN_IMPORTABLE(c5_color_data, (const int32_T *)
    &c5_color_sizes, NULL, 0, 6, (void *)c5_f_sf_marshallOut, (void *)
    c5_c_sf_marshallIn);
  _SFD_SYMBOL_SCOPE_ADD_EML_IMPORTABLE(&c5_nargin, 7U, c5_e_sf_marshallOut,
    c5_b_sf_marshallIn);
  _SFD_SYMBOL_SCOPE_ADD_EML_IMPORTABLE(&c5_nargout, 8U, c5_e_sf_marshallOut,
    c5_b_sf_marshallIn);
  _SFD_SYMBOL_SCOPE_ADD_EML_DYN(c5_b_centroids_data, (const int32_T *)
    &c5_b_centroids_sizes, NULL, 1, 9, (void *)c5_d_sf_marshallOut);
  _SFD_SYMBOL_SCOPE_ADD_EML_DYN(c5_b_area_data, (const int32_T *)
    &c5_b_area_sizes, NULL, 1, 10, (void *)c5_c_sf_marshallOut);
  _SFD_SYMBOL_SCOPE_ADD_EML(c5_b_RBW, 11U, c5_b_sf_marshallOut);
  _SFD_SYMBOL_SCOPE_ADD_EML(c5_b_BBW, 12U, c5_b_sf_marshallOut);
  _SFD_SYMBOL_SCOPE_ADD_EML_DYN_IMPORTABLE(&c5_b_blobs_data, NULL,
    &c5_b_blobs_elems_sizes, 0, 13, (void *)c5_sf_marshallOut, (void *)
    c5_sf_marshallIn);
  CV_EML_FCN(0, 0);
  _SFD_EML_CALL(0U, chartInstance->c5_sfEvent, 4);
  c5_x_sizes = c5_b_area_sizes[0];
  c5_c_loop_ub = c5_b_area_sizes[0] - 1;
  for (c5_i15 = 0; c5_i15 <= c5_c_loop_ub; c5_i15++) {
    c5_x_data[c5_i15] = c5_b_area_data[c5_i15];
  }

  c5_lenA = (real_T)c5_x_sizes;
  _SFD_EML_CALL(0U, chartInstance->c5_sfEvent, 5);
  c5_tmp_sizes[0] = c5_b_centroids_sizes[0];
  c5_tmp_sizes[1] = c5_b_centroids_sizes[1];
  c5_i16 = c5_tmp_sizes[0];
  c5_i17 = c5_tmp_sizes[1];
  c5_d_loop_ub = c5_b_centroids_sizes[0] * c5_b_centroids_sizes[1] - 1;
  for (c5_i18 = 0; c5_i18 <= c5_d_loop_ub; c5_i18++) {
    c5_tmp_data[c5_i18] = c5_b_centroids_data[c5_i18];
  }

  c5_b_floor(chartInstance, c5_tmp_data, c5_tmp_sizes);
  c5_centroidsInt_sizes[0] = c5_tmp_sizes[0];
  c5_centroidsInt_sizes[1] = c5_tmp_sizes[1];
  c5_centroidsInt = c5_centroidsInt_sizes[0];
  c5_b_centroidsInt = c5_centroidsInt_sizes[1];
  c5_e_loop_ub = c5_tmp_sizes[0] * c5_tmp_sizes[1] - 1;
  for (c5_i19 = 0; c5_i19 <= c5_e_loop_ub; c5_i19++) {
    c5_centroidsInt_data[c5_i19] = c5_tmp_data[c5_i19];
  }

  _SFD_EML_CALL(0U, chartInstance->c5_sfEvent, 7);
  _SFD_DIM_SIZE_GEQ_CHECK(16, 0, 1);
  c5_b_blobs_elems_sizes.centroid[0] = 0;
  c5_b_blobs_elems_sizes.centroid[1] = 2;
  c5_blobs = c5_b_blobs_elems_sizes.centroid[0];
  c5_b_blobs = c5_b_blobs_elems_sizes.centroid[1];
  _SFD_EML_CALL(0U, chartInstance->c5_sfEvent, 8);
  _SFD_DIM_SIZE_GEQ_CHECK(16, 0, 1);
  c5_b_blobs_elems_sizes.area = 0;
  _SFD_EML_CALL(0U, chartInstance->c5_sfEvent, 9);
  _SFD_DIM_SIZE_GEQ_CHECK(16, 0, 1);
  c5_b_blobs_elems_sizes.color = 0;
  _SFD_EML_CALL(0U, chartInstance->c5_sfEvent, 11);
  _SFD_EML_CALL(0U, chartInstance->c5_sfEvent, 12);
  _SFD_EML_CALL(0U, chartInstance->c5_sfEvent, 13);
  _SFD_EML_CALL(0U, chartInstance->c5_sfEvent, 15);
  c5_b_lenA = c5_lenA;
  c5_i20 = (int32_T)c5_b_lenA - 1;
  c5_ii = 1.0;
  c5_b_ii = 0;
  while (c5_b_ii <= c5_i20) {
    c5_ii = 1.0 + (real_T)c5_b_ii;
    CV_EML_FOR(0, 1, 0, 1);
    _SFD_EML_CALL(0U, chartInstance->c5_sfEvent, 16);
    (real_T)_SFD_EML_ARRAY_BOUNDS_CHECK("centroidsInt", 2, 1,
      c5_centroidsInt_sizes[1], 2, 0);
    (real_T)_SFD_EML_ARRAY_BOUNDS_CHECK("centroidsInt", 1, 1,
      c5_centroidsInt_sizes[1], 2, 0);
    c5_cr = c5_b_RBW[((int32_T)(real32_T)_SFD_EML_ARRAY_BOUNDS_CHECK("RBW",
      (int32_T)(real32_T)_SFD_INTEGER_CHECK("centroidsInt(ii,2)", (real_T)
      c5_centroidsInt_data[(_SFD_EML_ARRAY_BOUNDS_CHECK("centroidsInt", (int32_T)
      c5_ii, 1, c5_centroidsInt_sizes[0], 1, 0) + c5_centroidsInt_sizes[0]) - 1]),
      1, 320, 1, 0) + 320 * ((int32_T)(real32_T)_SFD_EML_ARRAY_BOUNDS_CHECK(
      "RBW", (int32_T)(real32_T)_SFD_INTEGER_CHECK("centroidsInt(ii,1)", (real_T)
      c5_centroidsInt_data[_SFD_EML_ARRAY_BOUNDS_CHECK("centroidsInt", (int32_T)
      c5_ii, 1, c5_centroidsInt_sizes[0], 1, 0) - 1]), 1, 240, 2, 0) - 1)) - 1];
    _SFD_EML_CALL(0U, chartInstance->c5_sfEvent, 17);
    (real_T)_SFD_EML_ARRAY_BOUNDS_CHECK("centroidsInt", 2, 1,
      c5_centroidsInt_sizes[1], 2, 0);
    (real_T)_SFD_EML_ARRAY_BOUNDS_CHECK("centroidsInt", 1, 1,
      c5_centroidsInt_sizes[1], 2, 0);
    c5_cb = c5_b_BBW[((int32_T)(real32_T)_SFD_EML_ARRAY_BOUNDS_CHECK("BBW",
      (int32_T)(real32_T)_SFD_INTEGER_CHECK("centroidsInt(ii,2)", (real_T)
      c5_centroidsInt_data[(_SFD_EML_ARRAY_BOUNDS_CHECK("centroidsInt", (int32_T)
      c5_ii, 1, c5_centroidsInt_sizes[0], 1, 0) + c5_centroidsInt_sizes[0]) - 1]),
      1, 320, 1, 0) + 320 * ((int32_T)(real32_T)_SFD_EML_ARRAY_BOUNDS_CHECK(
      "BBW", (int32_T)(real32_T)_SFD_INTEGER_CHECK("centroidsInt(ii,1)", (real_T)
      c5_centroidsInt_data[_SFD_EML_ARRAY_BOUNDS_CHECK("centroidsInt", (int32_T)
      c5_ii, 1, c5_centroidsInt_sizes[0], 1, 0) - 1]), 1, 240, 2, 0) - 1)) - 1];
    _SFD_EML_CALL(0U, chartInstance->c5_sfEvent, 18);
    c5_colorVec[0] = c5_cr;
    c5_colorVec[1] = c5_cb;
    _SFD_EML_CALL(0U, chartInstance->c5_sfEvent, 19);
    for (c5_i21 = 0; c5_i21 < 2; c5_i21++) {
      c5_x[c5_i21] = c5_colorVec[c5_i21];
    }

    c5_idx = 0;
    for (c5_i22 = 0; c5_i22 < 2; c5_i22++) {
      c5_ii_sizes[c5_i22] = 1 + c5_i22;
    }

    c5_c_ii = 1;
    exitg1 = false;
    while ((exitg1 == false) && (c5_c_ii < 3)) {
      c5_d_ii = c5_c_ii;
      guard1 = false;
      if (c5_x[c5_d_ii - 1]) {
        c5_a = c5_idx;
        c5_b_a = c5_a + 1;
        c5_idx = c5_b_a;
        c5_ii_data[c5_idx - 1] = c5_d_ii;
        if (c5_idx >= 2) {
          exitg1 = true;
        } else {
          guard1 = true;
        }
      } else {
        guard1 = true;
      }

      if (guard1 == true) {
        c5_c_ii++;
      }
    }

    c5_b0 = (1 > c5_idx);
    c5_b1 = c5_b0;
    c5_b2 = c5_b1;
    if (c5_b2) {
      c5_i23 = 0;
    } else {
      c5_i23 = _SFD_EML_ARRAY_BOUNDS_CHECK("", c5_idx, 1, 2, 0, 0);
    }

    c5_ii_sizes[1] = c5_i23;
    c5_color_sizes[0] = 1;
    c5_color_sizes[1] = c5_ii_sizes[1];
    c5_color = c5_color_sizes[0];
    c5_b_color = c5_color_sizes[1];
    c5_f_loop_ub = c5_ii_sizes[0] * c5_ii_sizes[1] - 1;
    for (c5_i24 = 0; c5_i24 <= c5_f_loop_ub; c5_i24++) {
      c5_color_data[c5_i24] = (real_T)c5_ii_data[c5_i24];
    }

    _SFD_EML_CALL(0U, chartInstance->c5_sfEvent, 20);
    c5_b_x_sizes[0] = 1;
    c5_b_x_sizes[1] = c5_color_sizes[1];
    c5_b_x = c5_b_x_sizes[0];
    c5_c_x = c5_b_x_sizes[1];
    c5_g_loop_ub = c5_color_sizes[0] * c5_color_sizes[1] - 1;
    for (c5_i25 = 0; c5_i25 <= c5_g_loop_ub; c5_i25++) {
      c5_b_x_data[c5_i25] = c5_color_data[c5_i25];
    }

    c5_d0 = (real_T)c5_b_x_sizes[1];
    if (CV_EML_IF(0, 1, 0, CV_RELATIONAL_EVAL(4U, 0U, 0, c5_d0, 1.0, -1, 0U,
          c5_d0 == 1.0))) {
      _SFD_EML_CALL(0U, chartInstance->c5_sfEvent, 21);
      c5_i26 = c5_b_centroids_sizes[1];
      c5_e_ii = _SFD_EML_ARRAY_BOUNDS_CHECK("centroids", (int32_T)c5_ii, 1,
        c5_b_centroids_sizes[0], 1, 0) - 1;
      c5_b_tmp_sizes[0] = 1;
      c5_b_tmp_sizes[1] = c5_i26;
      c5_h_loop_ub = c5_i26 - 1;
      for (c5_i27 = 0; c5_i27 <= c5_h_loop_ub; c5_i27++) {
        c5_b_tmp_data[c5_b_tmp_sizes[0] * c5_i27] = c5_b_centroids_data[c5_e_ii
          + c5_b_centroids_sizes[0] * c5_i27];
      }

      _SFD_DIM_SIZE_EQ_CHECK(2, c5_b_tmp_sizes[1], 2);
      c5_c_tmp_sizes[0] = c5_b_blobs_elems_sizes.centroid[0] + 1;
      c5_c_tmp_sizes[1] = c5_b_blobs_elems_sizes.centroid[1];
      c5_i_loop_ub = c5_b_blobs_elems_sizes.centroid[1] - 1;
      for (c5_i28 = 0; c5_i28 <= c5_i_loop_ub; c5_i28++) {
        c5_j_loop_ub = c5_b_blobs_elems_sizes.centroid[0] - 1;
        for (c5_i29 = 0; c5_i29 <= c5_j_loop_ub; c5_i29++) {
          c5_c_tmp_data[c5_i29 + c5_c_tmp_sizes[0] * c5_i28] =
            c5_b_blobs_data.centroid[c5_i29 + c5_b_blobs_elems_sizes.centroid[0]
            * c5_i28];
        }
      }

      c5_k_loop_ub = c5_b_tmp_sizes[1] - 1;
      for (c5_i30 = 0; c5_i30 <= c5_k_loop_ub; c5_i30++) {
        c5_c_tmp_data[c5_b_blobs_elems_sizes.centroid[0] + c5_c_tmp_sizes[0] *
          c5_i30] = c5_b_tmp_data[c5_b_tmp_sizes[0] * c5_i30];
      }

      _SFD_DIM_SIZE_GEQ_CHECK(16, c5_c_tmp_sizes[0], 1);
      c5_b_blobs_elems_sizes.centroid[0] = c5_c_tmp_sizes[0];
      c5_b_blobs_elems_sizes.centroid[1] = 2;
      c5_c_blobs = c5_b_blobs_elems_sizes.centroid[0];
      c5_d_blobs = c5_b_blobs_elems_sizes.centroid[1];
      c5_l_loop_ub = c5_c_tmp_sizes[0] * c5_c_tmp_sizes[1] - 1;
      for (c5_i31 = 0; c5_i31 <= c5_l_loop_ub; c5_i31++) {
        c5_b_blobs_data.centroid[c5_i31] = c5_c_tmp_data[c5_i31];
      }

      _SFD_EML_CALL(0U, chartInstance->c5_sfEvent, 22);
      c5_x_sizes = c5_b_area_sizes[0];
      c5_m_loop_ub = c5_b_area_sizes[0] - 1;
      for (c5_i32 = 0; c5_i32 <= c5_m_loop_ub; c5_i32++) {
        c5_x_data[c5_i32] = c5_b_area_data[c5_i32];
      }

      c5_d_tmp_sizes = c5_b_blobs_elems_sizes.area + 1;
      c5_n_loop_ub = c5_b_blobs_elems_sizes.area - 1;
      for (c5_i33 = 0; c5_i33 <= c5_n_loop_ub; c5_i33++) {
        c5_d_tmp_data[c5_i33] = c5_b_blobs_data.area[c5_i33];
      }

      c5_d_tmp_data[c5_b_blobs_elems_sizes.area] =
        c5_x_data[_SFD_EML_ARRAY_BOUNDS_CHECK("area", (int32_T)c5_ii, 1,
        c5_x_sizes, 1, 0) - 1];
      _SFD_DIM_SIZE_GEQ_CHECK(16, c5_d_tmp_sizes, 1);
      c5_b_blobs_elems_sizes.area = c5_d_tmp_sizes;
      c5_o_loop_ub = c5_d_tmp_sizes - 1;
      for (c5_i34 = 0; c5_i34 <= c5_o_loop_ub; c5_i34++) {
        c5_b_blobs_data.area[c5_i34] = c5_d_tmp_data[c5_i34];
      }

      _SFD_EML_CALL(0U, chartInstance->c5_sfEvent, 23);
      (real_T)_SFD_EML_ARRAY_BOUNDS_CHECK("color", 1, 1, c5_color_sizes[1], 2, 0);
      c5_e_tmp_sizes = c5_b_blobs_elems_sizes.color + 1;
      c5_p_loop_ub = c5_b_blobs_elems_sizes.color - 1;
      for (c5_i35 = 0; c5_i35 <= c5_p_loop_ub; c5_i35++) {
        c5_e_tmp_data[c5_i35] = c5_b_blobs_data.color[c5_i35];
      }

      c5_d1 = muDoubleScalarRound(c5_color_data[0]);
      if (c5_d1 < 256.0) {
        if (CV_SATURATION_EVAL(4, 0, 0, 1, c5_d1 >= 0.0)) {
          c5_u0 = (uint8_T)c5_d1;
        } else {
          c5_u0 = 0U;
        }
      } else if (CV_SATURATION_EVAL(4, 0, 0, 0, c5_d1 >= 256.0)) {
        c5_u0 = MAX_uint8_T;
      } else {
        c5_u0 = 0U;
      }

      c5_e_tmp_data[c5_b_blobs_elems_sizes.color] = c5_u0;
      _SFD_DIM_SIZE_GEQ_CHECK(16, c5_e_tmp_sizes, 1);
      c5_b_blobs_elems_sizes.color = c5_e_tmp_sizes;
      c5_q_loop_ub = c5_e_tmp_sizes - 1;
      for (c5_i36 = 0; c5_i36 <= c5_q_loop_ub; c5_i36++) {
        c5_b_blobs_data.color[c5_i36] = c5_e_tmp_data[c5_i36];
      }
    }

    c5_b_ii++;
    _SF_MEX_LISTEN_FOR_CTRL_C(chartInstance->S);
  }

  CV_EML_FOR(0, 1, 0, 0);
  _SFD_EML_CALL(0U, chartInstance->c5_sfEvent, -23);
  _SFD_SYMBOL_SCOPE_POP();
  chartInstance->c5_blobs_elems_sizes->centroid[0] =
    c5_b_blobs_elems_sizes.centroid[0];
  chartInstance->c5_blobs_elems_sizes->centroid[1] =
    c5_b_blobs_elems_sizes.centroid[1];
  c5_r_loop_ub = c5_b_blobs_elems_sizes.centroid[1] - 1;
  for (c5_i37 = 0; c5_i37 <= c5_r_loop_ub; c5_i37++) {
    c5_s_loop_ub = c5_b_blobs_elems_sizes.centroid[0] - 1;
    for (c5_i38 = 0; c5_i38 <= c5_s_loop_ub; c5_i38++) {
      ((real32_T *)&((char_T *)chartInstance->c5_blobs_data)[0])[c5_i38 +
        chartInstance->c5_blobs_elems_sizes->centroid[0] * c5_i37] =
        c5_b_blobs_data.centroid[c5_i38 + c5_b_blobs_elems_sizes.centroid[0] *
        c5_i37];
    }
  }

  chartInstance->c5_blobs_elems_sizes->area = c5_b_blobs_elems_sizes.area;
  c5_t_loop_ub = c5_b_blobs_elems_sizes.area - 1;
  for (c5_i39 = 0; c5_i39 <= c5_t_loop_ub; c5_i39++) {
    ((int32_T *)&((char_T *)chartInstance->c5_blobs_data)[128])[c5_i39] =
      c5_b_blobs_data.area[c5_i39];
  }

  chartInstance->c5_blobs_elems_sizes->color = c5_b_blobs_elems_sizes.color;
  c5_u_loop_ub = c5_b_blobs_elems_sizes.color - 1;
  for (c5_i40 = 0; c5_i40 <= c5_u_loop_ub; c5_i40++) {
    ((uint8_T *)&((char_T *)chartInstance->c5_blobs_data)[192])[c5_i40] =
      c5_b_blobs_data.color[c5_i40];
  }

  _SFD_CC_CALL(EXIT_OUT_OF_FUNCTION_TAG, 3U, chartInstance->c5_sfEvent);
}

static void initSimStructsc5_ImageDet(SFc5_ImageDetInstanceStruct *chartInstance)
{
  (void)chartInstance;
}

static void init_script_number_translation(uint32_T c5_machineNumber, uint32_T
  c5_chartNumber, uint32_T c5_instanceNumber)
{
  (void)c5_machineNumber;
  (void)c5_chartNumber;
  (void)c5_instanceNumber;
}

static const mxArray *c5_sf_marshallOut(void *chartInstanceVoid, c5_BlobData
  *c5_inData_data, c5_BlobData_size *c5_inData_elems_sizes)
{
  const mxArray *c5_mxArrayOutData = NULL;
  c5_BlobData_size c5_u_elems_sizes;
  c5_BlobData c5_u_data;
  const mxArray *c5_y = NULL;
  int32_T c5_u_sizes[2];
  int32_T c5_u;
  int32_T c5_b_u;
  int32_T c5_loop_ub;
  int32_T c5_i41;
  real32_T c5_b_u_data[32];
  const mxArray *c5_b_y = NULL;
  int32_T c5_b_u_sizes;
  int32_T c5_b_loop_ub;
  int32_T c5_i42;
  int32_T c5_c_u_data[16];
  const mxArray *c5_c_y = NULL;
  int32_T c5_c_u_sizes;
  int32_T c5_c_loop_ub;
  int32_T c5_i43;
  uint8_T c5_d_u_data[16];
  const mxArray *c5_d_y = NULL;
  SFc5_ImageDetInstanceStruct *chartInstance;
  chartInstance = (SFc5_ImageDetInstanceStruct *)chartInstanceVoid;
  c5_mxArrayOutData = NULL;
  c5_u_elems_sizes = *c5_inData_elems_sizes;
  c5_u_data = *c5_inData_data;
  c5_y = NULL;
  sf_mex_assign(&c5_y, sf_mex_createstruct("structure", 2, 1, 1), false);
  c5_u_sizes[0] = c5_u_elems_sizes.centroid[0];
  c5_u_sizes[1] = c5_u_elems_sizes.centroid[1];
  c5_u = c5_u_sizes[0];
  c5_b_u = c5_u_sizes[1];
  c5_loop_ub = c5_u_elems_sizes.centroid[0] * c5_u_elems_sizes.centroid[1] - 1;
  for (c5_i41 = 0; c5_i41 <= c5_loop_ub; c5_i41++) {
    c5_b_u_data[c5_i41] = c5_u_data.centroid[c5_i41];
  }

  c5_b_y = NULL;
  sf_mex_assign(&c5_b_y, sf_mex_create("y", c5_b_u_data, 1, 0U, 1U, 0U, 2,
    c5_u_sizes[0], c5_u_sizes[1]), false);
  sf_mex_addfield(c5_y, c5_b_y, "centroid", "centroid", 0);
  c5_b_u_sizes = c5_u_elems_sizes.area;
  c5_b_loop_ub = c5_u_elems_sizes.area - 1;
  for (c5_i42 = 0; c5_i42 <= c5_b_loop_ub; c5_i42++) {
    c5_c_u_data[c5_i42] = c5_u_data.area[c5_i42];
  }

  c5_c_y = NULL;
  sf_mex_assign(&c5_c_y, sf_mex_create("y", c5_c_u_data, 6, 0U, 1U, 0U, 1,
    c5_b_u_sizes), false);
  sf_mex_addfield(c5_y, c5_c_y, "area", "area", 0);
  c5_c_u_sizes = c5_u_elems_sizes.color;
  c5_c_loop_ub = c5_u_elems_sizes.color - 1;
  for (c5_i43 = 0; c5_i43 <= c5_c_loop_ub; c5_i43++) {
    c5_d_u_data[c5_i43] = c5_u_data.color[c5_i43];
  }

  c5_d_y = NULL;
  sf_mex_assign(&c5_d_y, sf_mex_create("y", c5_d_u_data, 3, 0U, 1U, 0U, 1,
    c5_c_u_sizes), false);
  sf_mex_addfield(c5_y, c5_d_y, "color", "color", 0);
  sf_mex_assign(&c5_mxArrayOutData, c5_y, false);
  return c5_mxArrayOutData;
}

static void c5_emlrt_marshallIn(SFc5_ImageDetInstanceStruct *chartInstance,
  const mxArray *c5_blobs, const char_T *c5_identifier, c5_BlobData *c5_y_data,
  c5_BlobData_size *c5_y_elems_sizes)
{
  emlrtMsgIdentifier c5_thisId;
  c5_thisId.fIdentifier = c5_identifier;
  c5_thisId.fParent = NULL;
  c5_b_emlrt_marshallIn(chartInstance, sf_mex_dup(c5_blobs), &c5_thisId,
                        c5_y_data, c5_y_elems_sizes);
  sf_mex_destroy(&c5_blobs);
}

static void c5_b_emlrt_marshallIn(SFc5_ImageDetInstanceStruct *chartInstance,
  const mxArray *c5_u, const emlrtMsgIdentifier *c5_parentId, c5_BlobData
  *c5_y_data, c5_BlobData_size *c5_y_elems_sizes)
{
  emlrtMsgIdentifier c5_thisId;
  static const char * c5_fieldNames[3] = { "centroid", "area", "color" };

  c5_thisId.fParent = c5_parentId;
  sf_mex_check_struct(c5_parentId, c5_u, 3, c5_fieldNames, 0U, NULL);
  c5_thisId.fIdentifier = "centroid";
  c5_c_emlrt_marshallIn(chartInstance, sf_mex_dup(sf_mex_getfield(c5_u,
    "centroid", "centroid", 0)), &c5_thisId, c5_y_data->centroid,
                        c5_y_elems_sizes->centroid);
  c5_thisId.fIdentifier = "area";
  c5_d_emlrt_marshallIn(chartInstance, sf_mex_dup(sf_mex_getfield(c5_u, "area",
    "area", 0)), &c5_thisId, c5_y_data->area, &c5_y_elems_sizes->area);
  c5_thisId.fIdentifier = "color";
  c5_e_emlrt_marshallIn(chartInstance, sf_mex_dup(sf_mex_getfield(c5_u, "color",
    "color", 0)), &c5_thisId, c5_y_data->color, &c5_y_elems_sizes->color);
  sf_mex_destroy(&c5_u);
}

static void c5_c_emlrt_marshallIn(SFc5_ImageDetInstanceStruct *chartInstance,
  const mxArray *c5_u, const emlrtMsgIdentifier *c5_parentId, real32_T
  c5_y_data[], int32_T c5_y_sizes[2])
{
  int32_T c5_i44;
  uint32_T c5_uv0[2];
  int32_T c5_i45;
  boolean_T c5_bv0[2];
  int32_T c5_tmp_sizes[2];
  real32_T c5_tmp_data[32];
  int32_T c5_y;
  int32_T c5_b_y;
  int32_T c5_loop_ub;
  int32_T c5_i46;
  (void)chartInstance;
  for (c5_i44 = 0; c5_i44 < 2; c5_i44++) {
    c5_uv0[c5_i44] = 16U + (uint32_T)(-14 * c5_i44);
  }

  for (c5_i45 = 0; c5_i45 < 2; c5_i45++) {
    c5_bv0[c5_i45] = true;
  }

  sf_mex_import_vs(c5_parentId, sf_mex_dup(c5_u), c5_tmp_data, 1, 1, 0U, 1, 0U,
                   2, c5_bv0, c5_uv0, c5_tmp_sizes);
  c5_y_sizes[0] = c5_tmp_sizes[0];
  c5_y_sizes[1] = c5_tmp_sizes[1];
  c5_y = c5_y_sizes[0];
  c5_b_y = c5_y_sizes[1];
  c5_loop_ub = c5_tmp_sizes[0] * c5_tmp_sizes[1] - 1;
  for (c5_i46 = 0; c5_i46 <= c5_loop_ub; c5_i46++) {
    c5_y_data[c5_i46] = c5_tmp_data[c5_i46];
  }

  sf_mex_destroy(&c5_u);
}

static void c5_d_emlrt_marshallIn(SFc5_ImageDetInstanceStruct *chartInstance,
  const mxArray *c5_u, const emlrtMsgIdentifier *c5_parentId, int32_T c5_y_data[],
  int32_T *c5_y_sizes)
{
  static uint32_T c5_uv1[1] = { 16U };

  uint32_T c5_uv2[1];
  static boolean_T c5_bv1[1] = { true };

  boolean_T c5_bv2[1];
  int32_T c5_tmp_sizes;
  int32_T c5_tmp_data[16];
  int32_T c5_loop_ub;
  int32_T c5_i47;
  (void)chartInstance;
  c5_uv2[0] = c5_uv1[0];
  c5_bv2[0] = c5_bv1[0];
  sf_mex_import_vs(c5_parentId, sf_mex_dup(c5_u), c5_tmp_data, 1, 6, 0U, 1, 0U,
                   1, c5_bv2, c5_uv2, &c5_tmp_sizes);
  *c5_y_sizes = c5_tmp_sizes;
  c5_loop_ub = c5_tmp_sizes - 1;
  for (c5_i47 = 0; c5_i47 <= c5_loop_ub; c5_i47++) {
    c5_y_data[c5_i47] = c5_tmp_data[c5_i47];
  }

  sf_mex_destroy(&c5_u);
}

static void c5_e_emlrt_marshallIn(SFc5_ImageDetInstanceStruct *chartInstance,
  const mxArray *c5_u, const emlrtMsgIdentifier *c5_parentId, uint8_T c5_y_data[],
  int32_T *c5_y_sizes)
{
  static uint32_T c5_uv3[1] = { 16U };

  uint32_T c5_uv4[1];
  static boolean_T c5_bv3[1] = { true };

  boolean_T c5_bv4[1];
  int32_T c5_tmp_sizes;
  uint8_T c5_tmp_data[16];
  int32_T c5_loop_ub;
  int32_T c5_i48;
  (void)chartInstance;
  c5_uv4[0] = c5_uv3[0];
  c5_bv4[0] = c5_bv3[0];
  sf_mex_import_vs(c5_parentId, sf_mex_dup(c5_u), c5_tmp_data, 1, 3, 0U, 1, 0U,
                   1, c5_bv4, c5_uv4, &c5_tmp_sizes);
  *c5_y_sizes = c5_tmp_sizes;
  c5_loop_ub = c5_tmp_sizes - 1;
  for (c5_i48 = 0; c5_i48 <= c5_loop_ub; c5_i48++) {
    c5_y_data[c5_i48] = c5_tmp_data[c5_i48];
  }

  sf_mex_destroy(&c5_u);
}

static void c5_sf_marshallIn(void *chartInstanceVoid, const mxArray
  *c5_mxArrayInData, const char_T *c5_varName, c5_BlobData *c5_outData_data,
  c5_BlobData_size *c5_outData_elems_sizes)
{
  const mxArray *c5_blobs;
  const char_T *c5_identifier;
  emlrtMsgIdentifier c5_thisId;
  c5_BlobData_size c5_y_elems_sizes;
  c5_BlobData c5_y_data;
  SFc5_ImageDetInstanceStruct *chartInstance;
  chartInstance = (SFc5_ImageDetInstanceStruct *)chartInstanceVoid;
  c5_blobs = sf_mex_dup(c5_mxArrayInData);
  c5_identifier = c5_varName;
  c5_thisId.fIdentifier = c5_identifier;
  c5_thisId.fParent = NULL;
  c5_b_emlrt_marshallIn(chartInstance, sf_mex_dup(c5_blobs), &c5_thisId,
                        &c5_y_data, &c5_y_elems_sizes);
  sf_mex_destroy(&c5_blobs);
  *c5_outData_elems_sizes = c5_y_elems_sizes;
  *c5_outData_data = c5_y_data;
  sf_mex_destroy(&c5_mxArrayInData);
}

static const mxArray *c5_b_sf_marshallOut(void *chartInstanceVoid, void
  *c5_inData)
{
  const mxArray *c5_mxArrayOutData = NULL;
  int32_T c5_i49;
  int32_T c5_i50;
  int32_T c5_i51;
  boolean_T c5_b_inData[76800];
  int32_T c5_i52;
  int32_T c5_i53;
  int32_T c5_i54;
  boolean_T c5_u[76800];
  const mxArray *c5_y = NULL;
  SFc5_ImageDetInstanceStruct *chartInstance;
  chartInstance = (SFc5_ImageDetInstanceStruct *)chartInstanceVoid;
  c5_mxArrayOutData = NULL;
  c5_i49 = 0;
  for (c5_i50 = 0; c5_i50 < 240; c5_i50++) {
    for (c5_i51 = 0; c5_i51 < 320; c5_i51++) {
      c5_b_inData[c5_i51 + c5_i49] = (*(boolean_T (*)[76800])c5_inData)[c5_i51 +
        c5_i49];
    }

    c5_i49 += 320;
  }

  c5_i52 = 0;
  for (c5_i53 = 0; c5_i53 < 240; c5_i53++) {
    for (c5_i54 = 0; c5_i54 < 320; c5_i54++) {
      c5_u[c5_i54 + c5_i52] = c5_b_inData[c5_i54 + c5_i52];
    }

    c5_i52 += 320;
  }

  c5_y = NULL;
  sf_mex_assign(&c5_y, sf_mex_create("y", c5_u, 11, 0U, 1U, 0U, 2, 320, 240),
                false);
  sf_mex_assign(&c5_mxArrayOutData, c5_y, false);
  return c5_mxArrayOutData;
}

static const mxArray *c5_c_sf_marshallOut(void *chartInstanceVoid, int32_T
  c5_inData_data[], int32_T c5_inData_sizes[2])
{
  const mxArray *c5_mxArrayOutData = NULL;
  int32_T c5_u_sizes[2];
  int32_T c5_u;
  int32_T c5_b_u;
  int32_T c5_inData;
  int32_T c5_b_inData;
  int32_T c5_b_inData_sizes;
  int32_T c5_loop_ub;
  int32_T c5_i55;
  int32_T c5_b_inData_data[16];
  int32_T c5_b_loop_ub;
  int32_T c5_i56;
  int32_T c5_u_data[16];
  const mxArray *c5_y = NULL;
  SFc5_ImageDetInstanceStruct *chartInstance;
  chartInstance = (SFc5_ImageDetInstanceStruct *)chartInstanceVoid;
  c5_mxArrayOutData = NULL;
  c5_u_sizes[0] = c5_inData_sizes[0];
  c5_u_sizes[1] = 1;
  c5_u = c5_u_sizes[0];
  c5_b_u = c5_u_sizes[1];
  c5_inData = c5_inData_sizes[0];
  c5_b_inData = c5_inData_sizes[1];
  c5_b_inData_sizes = c5_inData * c5_b_inData;
  c5_loop_ub = c5_inData * c5_b_inData - 1;
  for (c5_i55 = 0; c5_i55 <= c5_loop_ub; c5_i55++) {
    c5_b_inData_data[c5_i55] = c5_inData_data[c5_i55];
  }

  c5_b_loop_ub = c5_b_inData_sizes - 1;
  for (c5_i56 = 0; c5_i56 <= c5_b_loop_ub; c5_i56++) {
    c5_u_data[c5_i56] = c5_b_inData_data[c5_i56];
  }

  c5_y = NULL;
  sf_mex_assign(&c5_y, sf_mex_create("y", c5_u_data, 6, 0U, 1U, 0U, 2,
    c5_u_sizes[0], c5_u_sizes[1]), false);
  sf_mex_assign(&c5_mxArrayOutData, c5_y, false);
  return c5_mxArrayOutData;
}

static const mxArray *c5_d_sf_marshallOut(void *chartInstanceVoid, real32_T
  c5_inData_data[], int32_T c5_inData_sizes[2])
{
  const mxArray *c5_mxArrayOutData = NULL;
  int32_T c5_u_sizes[2];
  int32_T c5_u;
  int32_T c5_b_u;
  int32_T c5_inData;
  int32_T c5_b_inData;
  int32_T c5_b_inData_sizes;
  int32_T c5_loop_ub;
  int32_T c5_i57;
  real32_T c5_b_inData_data[32];
  int32_T c5_b_loop_ub;
  int32_T c5_i58;
  real32_T c5_u_data[32];
  const mxArray *c5_y = NULL;
  SFc5_ImageDetInstanceStruct *chartInstance;
  chartInstance = (SFc5_ImageDetInstanceStruct *)chartInstanceVoid;
  c5_mxArrayOutData = NULL;
  c5_u_sizes[0] = c5_inData_sizes[0];
  c5_u_sizes[1] = c5_inData_sizes[1];
  c5_u = c5_u_sizes[0];
  c5_b_u = c5_u_sizes[1];
  c5_inData = c5_inData_sizes[0];
  c5_b_inData = c5_inData_sizes[1];
  c5_b_inData_sizes = c5_inData * c5_b_inData;
  c5_loop_ub = c5_inData * c5_b_inData - 1;
  for (c5_i57 = 0; c5_i57 <= c5_loop_ub; c5_i57++) {
    c5_b_inData_data[c5_i57] = c5_inData_data[c5_i57];
  }

  c5_b_loop_ub = c5_b_inData_sizes - 1;
  for (c5_i58 = 0; c5_i58 <= c5_b_loop_ub; c5_i58++) {
    c5_u_data[c5_i58] = c5_b_inData_data[c5_i58];
  }

  c5_y = NULL;
  sf_mex_assign(&c5_y, sf_mex_create("y", c5_u_data, 1, 0U, 1U, 0U, 2,
    c5_u_sizes[0], c5_u_sizes[1]), false);
  sf_mex_assign(&c5_mxArrayOutData, c5_y, false);
  return c5_mxArrayOutData;
}

static const mxArray *c5_e_sf_marshallOut(void *chartInstanceVoid, void
  *c5_inData)
{
  const mxArray *c5_mxArrayOutData = NULL;
  real_T c5_u;
  const mxArray *c5_y = NULL;
  SFc5_ImageDetInstanceStruct *chartInstance;
  chartInstance = (SFc5_ImageDetInstanceStruct *)chartInstanceVoid;
  c5_mxArrayOutData = NULL;
  c5_u = *(real_T *)c5_inData;
  c5_y = NULL;
  sf_mex_assign(&c5_y, sf_mex_create("y", &c5_u, 0, 0U, 0U, 0U, 0), false);
  sf_mex_assign(&c5_mxArrayOutData, c5_y, false);
  return c5_mxArrayOutData;
}

static real_T c5_f_emlrt_marshallIn(SFc5_ImageDetInstanceStruct *chartInstance,
  const mxArray *c5_u, const emlrtMsgIdentifier *c5_parentId)
{
  real_T c5_y;
  real_T c5_d2;
  (void)chartInstance;
  sf_mex_import(c5_parentId, sf_mex_dup(c5_u), &c5_d2, 1, 0, 0U, 0, 0U, 0);
  c5_y = c5_d2;
  sf_mex_destroy(&c5_u);
  return c5_y;
}

static void c5_b_sf_marshallIn(void *chartInstanceVoid, const mxArray
  *c5_mxArrayInData, const char_T *c5_varName, void *c5_outData)
{
  const mxArray *c5_nargout;
  const char_T *c5_identifier;
  emlrtMsgIdentifier c5_thisId;
  real_T c5_y;
  SFc5_ImageDetInstanceStruct *chartInstance;
  chartInstance = (SFc5_ImageDetInstanceStruct *)chartInstanceVoid;
  c5_nargout = sf_mex_dup(c5_mxArrayInData);
  c5_identifier = c5_varName;
  c5_thisId.fIdentifier = c5_identifier;
  c5_thisId.fParent = NULL;
  c5_y = c5_f_emlrt_marshallIn(chartInstance, sf_mex_dup(c5_nargout), &c5_thisId);
  sf_mex_destroy(&c5_nargout);
  *(real_T *)c5_outData = c5_y;
  sf_mex_destroy(&c5_mxArrayInData);
}

static const mxArray *c5_f_sf_marshallOut(void *chartInstanceVoid, real_T
  c5_inData_data[], int32_T c5_inData_sizes[2])
{
  const mxArray *c5_mxArrayOutData = NULL;
  int32_T c5_u_sizes[2];
  int32_T c5_u;
  int32_T c5_b_u;
  int32_T c5_inData;
  int32_T c5_b_inData;
  int32_T c5_b_inData_sizes;
  int32_T c5_loop_ub;
  int32_T c5_i59;
  real_T c5_b_inData_data[2];
  int32_T c5_b_loop_ub;
  int32_T c5_i60;
  real_T c5_u_data[2];
  const mxArray *c5_y = NULL;
  SFc5_ImageDetInstanceStruct *chartInstance;
  chartInstance = (SFc5_ImageDetInstanceStruct *)chartInstanceVoid;
  c5_mxArrayOutData = NULL;
  c5_u_sizes[0] = 1;
  c5_u_sizes[1] = c5_inData_sizes[1];
  c5_u = c5_u_sizes[0];
  c5_b_u = c5_u_sizes[1];
  c5_inData = c5_inData_sizes[0];
  c5_b_inData = c5_inData_sizes[1];
  c5_b_inData_sizes = c5_inData * c5_b_inData;
  c5_loop_ub = c5_inData * c5_b_inData - 1;
  for (c5_i59 = 0; c5_i59 <= c5_loop_ub; c5_i59++) {
    c5_b_inData_data[c5_i59] = c5_inData_data[c5_i59];
  }

  c5_b_loop_ub = c5_b_inData_sizes - 1;
  for (c5_i60 = 0; c5_i60 <= c5_b_loop_ub; c5_i60++) {
    c5_u_data[c5_i60] = c5_b_inData_data[c5_i60];
  }

  c5_y = NULL;
  sf_mex_assign(&c5_y, sf_mex_create("y", c5_u_data, 0, 0U, 1U, 0U, 2,
    c5_u_sizes[0], c5_u_sizes[1]), false);
  sf_mex_assign(&c5_mxArrayOutData, c5_y, false);
  return c5_mxArrayOutData;
}

static void c5_g_emlrt_marshallIn(SFc5_ImageDetInstanceStruct *chartInstance,
  const mxArray *c5_u, const emlrtMsgIdentifier *c5_parentId, real_T c5_y_data[],
  int32_T c5_y_sizes[2])
{
  int32_T c5_i61;
  uint32_T c5_uv5[2];
  int32_T c5_i62;
  static boolean_T c5_bv5[2] = { false, true };

  boolean_T c5_bv6[2];
  int32_T c5_tmp_sizes[2];
  real_T c5_tmp_data[2];
  int32_T c5_y;
  int32_T c5_b_y;
  int32_T c5_loop_ub;
  int32_T c5_i63;
  (void)chartInstance;
  for (c5_i61 = 0; c5_i61 < 2; c5_i61++) {
    c5_uv5[c5_i61] = 1U + (uint32_T)c5_i61;
  }

  for (c5_i62 = 0; c5_i62 < 2; c5_i62++) {
    c5_bv6[c5_i62] = c5_bv5[c5_i62];
  }

  sf_mex_import_vs(c5_parentId, sf_mex_dup(c5_u), c5_tmp_data, 1, 0, 0U, 1, 0U,
                   2, c5_bv6, c5_uv5, c5_tmp_sizes);
  c5_y_sizes[0] = 1;
  c5_y_sizes[1] = c5_tmp_sizes[1];
  c5_y = c5_y_sizes[0];
  c5_b_y = c5_y_sizes[1];
  c5_loop_ub = c5_tmp_sizes[0] * c5_tmp_sizes[1] - 1;
  for (c5_i63 = 0; c5_i63 <= c5_loop_ub; c5_i63++) {
    c5_y_data[c5_i63] = c5_tmp_data[c5_i63];
  }

  sf_mex_destroy(&c5_u);
}

static void c5_c_sf_marshallIn(void *chartInstanceVoid, const mxArray
  *c5_mxArrayInData, const char_T *c5_varName, real_T c5_outData_data[], int32_T
  c5_outData_sizes[2])
{
  const mxArray *c5_color;
  const char_T *c5_identifier;
  emlrtMsgIdentifier c5_thisId;
  int32_T c5_y_sizes[2];
  real_T c5_y_data[2];
  int32_T c5_loop_ub;
  int32_T c5_i64;
  SFc5_ImageDetInstanceStruct *chartInstance;
  chartInstance = (SFc5_ImageDetInstanceStruct *)chartInstanceVoid;
  c5_color = sf_mex_dup(c5_mxArrayInData);
  c5_identifier = c5_varName;
  c5_thisId.fIdentifier = c5_identifier;
  c5_thisId.fParent = NULL;
  c5_g_emlrt_marshallIn(chartInstance, sf_mex_dup(c5_color), &c5_thisId,
                        c5_y_data, c5_y_sizes);
  sf_mex_destroy(&c5_color);
  c5_outData_sizes[0] = 1;
  c5_outData_sizes[1] = c5_y_sizes[1];
  c5_loop_ub = c5_y_sizes[1] - 1;
  for (c5_i64 = 0; c5_i64 <= c5_loop_ub; c5_i64++) {
    c5_outData_data[c5_outData_sizes[0] * c5_i64] = c5_y_data[c5_y_sizes[0] *
      c5_i64];
  }

  sf_mex_destroy(&c5_mxArrayInData);
}

static const mxArray *c5_g_sf_marshallOut(void *chartInstanceVoid, void
  *c5_inData)
{
  const mxArray *c5_mxArrayOutData = NULL;
  int32_T c5_i65;
  boolean_T c5_b_inData[2];
  int32_T c5_i66;
  boolean_T c5_u[2];
  const mxArray *c5_y = NULL;
  SFc5_ImageDetInstanceStruct *chartInstance;
  chartInstance = (SFc5_ImageDetInstanceStruct *)chartInstanceVoid;
  c5_mxArrayOutData = NULL;
  for (c5_i65 = 0; c5_i65 < 2; c5_i65++) {
    c5_b_inData[c5_i65] = (*(boolean_T (*)[2])c5_inData)[c5_i65];
  }

  for (c5_i66 = 0; c5_i66 < 2; c5_i66++) {
    c5_u[c5_i66] = c5_b_inData[c5_i66];
  }

  c5_y = NULL;
  sf_mex_assign(&c5_y, sf_mex_create("y", c5_u, 11, 0U, 1U, 0U, 2, 1, 2), false);
  sf_mex_assign(&c5_mxArrayOutData, c5_y, false);
  return c5_mxArrayOutData;
}

static void c5_h_emlrt_marshallIn(SFc5_ImageDetInstanceStruct *chartInstance,
  const mxArray *c5_u, const emlrtMsgIdentifier *c5_parentId, boolean_T c5_y[2])
{
  boolean_T c5_bv7[2];
  int32_T c5_i67;
  (void)chartInstance;
  sf_mex_import(c5_parentId, sf_mex_dup(c5_u), c5_bv7, 1, 11, 0U, 1, 0U, 2, 1, 2);
  for (c5_i67 = 0; c5_i67 < 2; c5_i67++) {
    c5_y[c5_i67] = c5_bv7[c5_i67];
  }

  sf_mex_destroy(&c5_u);
}

static void c5_d_sf_marshallIn(void *chartInstanceVoid, const mxArray
  *c5_mxArrayInData, const char_T *c5_varName, void *c5_outData)
{
  const mxArray *c5_colorVec;
  const char_T *c5_identifier;
  emlrtMsgIdentifier c5_thisId;
  boolean_T c5_y[2];
  int32_T c5_i68;
  SFc5_ImageDetInstanceStruct *chartInstance;
  chartInstance = (SFc5_ImageDetInstanceStruct *)chartInstanceVoid;
  c5_colorVec = sf_mex_dup(c5_mxArrayInData);
  c5_identifier = c5_varName;
  c5_thisId.fIdentifier = c5_identifier;
  c5_thisId.fParent = NULL;
  c5_h_emlrt_marshallIn(chartInstance, sf_mex_dup(c5_colorVec), &c5_thisId, c5_y);
  sf_mex_destroy(&c5_colorVec);
  for (c5_i68 = 0; c5_i68 < 2; c5_i68++) {
    (*(boolean_T (*)[2])c5_outData)[c5_i68] = c5_y[c5_i68];
  }

  sf_mex_destroy(&c5_mxArrayInData);
}

static const mxArray *c5_h_sf_marshallOut(void *chartInstanceVoid, void
  *c5_inData)
{
  const mxArray *c5_mxArrayOutData = NULL;
  boolean_T c5_u;
  const mxArray *c5_y = NULL;
  SFc5_ImageDetInstanceStruct *chartInstance;
  chartInstance = (SFc5_ImageDetInstanceStruct *)chartInstanceVoid;
  c5_mxArrayOutData = NULL;
  c5_u = *(boolean_T *)c5_inData;
  c5_y = NULL;
  sf_mex_assign(&c5_y, sf_mex_create("y", &c5_u, 11, 0U, 0U, 0U, 0), false);
  sf_mex_assign(&c5_mxArrayOutData, c5_y, false);
  return c5_mxArrayOutData;
}

static boolean_T c5_i_emlrt_marshallIn(SFc5_ImageDetInstanceStruct
  *chartInstance, const mxArray *c5_u, const emlrtMsgIdentifier *c5_parentId)
{
  boolean_T c5_y;
  boolean_T c5_b3;
  (void)chartInstance;
  sf_mex_import(c5_parentId, sf_mex_dup(c5_u), &c5_b3, 1, 11, 0U, 0, 0U, 0);
  c5_y = c5_b3;
  sf_mex_destroy(&c5_u);
  return c5_y;
}

static void c5_e_sf_marshallIn(void *chartInstanceVoid, const mxArray
  *c5_mxArrayInData, const char_T *c5_varName, void *c5_outData)
{
  const mxArray *c5_cb;
  const char_T *c5_identifier;
  emlrtMsgIdentifier c5_thisId;
  boolean_T c5_y;
  SFc5_ImageDetInstanceStruct *chartInstance;
  chartInstance = (SFc5_ImageDetInstanceStruct *)chartInstanceVoid;
  c5_cb = sf_mex_dup(c5_mxArrayInData);
  c5_identifier = c5_varName;
  c5_thisId.fIdentifier = c5_identifier;
  c5_thisId.fParent = NULL;
  c5_y = c5_i_emlrt_marshallIn(chartInstance, sf_mex_dup(c5_cb), &c5_thisId);
  sf_mex_destroy(&c5_cb);
  *(boolean_T *)c5_outData = c5_y;
  sf_mex_destroy(&c5_mxArrayInData);
}

static void c5_f_sf_marshallIn(void *chartInstanceVoid, const mxArray
  *c5_mxArrayInData, const char_T *c5_varName, real32_T c5_outData_data[],
  int32_T c5_outData_sizes[2])
{
  const mxArray *c5_centroidsInt;
  const char_T *c5_identifier;
  emlrtMsgIdentifier c5_thisId;
  int32_T c5_y_sizes[2];
  real32_T c5_y_data[32];
  int32_T c5_loop_ub;
  int32_T c5_i69;
  int32_T c5_b_loop_ub;
  int32_T c5_i70;
  SFc5_ImageDetInstanceStruct *chartInstance;
  chartInstance = (SFc5_ImageDetInstanceStruct *)chartInstanceVoid;
  c5_centroidsInt = sf_mex_dup(c5_mxArrayInData);
  c5_identifier = c5_varName;
  c5_thisId.fIdentifier = c5_identifier;
  c5_thisId.fParent = NULL;
  c5_c_emlrt_marshallIn(chartInstance, sf_mex_dup(c5_centroidsInt), &c5_thisId,
                        c5_y_data, c5_y_sizes);
  sf_mex_destroy(&c5_centroidsInt);
  c5_outData_sizes[0] = c5_y_sizes[0];
  c5_outData_sizes[1] = c5_y_sizes[1];
  c5_loop_ub = c5_y_sizes[1] - 1;
  for (c5_i69 = 0; c5_i69 <= c5_loop_ub; c5_i69++) {
    c5_b_loop_ub = c5_y_sizes[0] - 1;
    for (c5_i70 = 0; c5_i70 <= c5_b_loop_ub; c5_i70++) {
      c5_outData_data[c5_i70 + c5_outData_sizes[0] * c5_i69] = c5_y_data[c5_i70
        + c5_y_sizes[0] * c5_i69];
    }
  }

  sf_mex_destroy(&c5_mxArrayInData);
}

const mxArray *sf_c5_ImageDet_get_eml_resolved_functions_info(void)
{
  const mxArray *c5_nameCaptureInfo = NULL;
  c5_nameCaptureInfo = NULL;
  sf_mex_assign(&c5_nameCaptureInfo, sf_mex_createstruct("structure", 2, 18, 1),
                false);
  c5_info_helper(&c5_nameCaptureInfo);
  sf_mex_emlrtNameCapturePostProcessR2012a(&c5_nameCaptureInfo);
  return c5_nameCaptureInfo;
}

static void c5_info_helper(const mxArray **c5_info)
{
  const mxArray *c5_rhs0 = NULL;
  const mxArray *c5_lhs0 = NULL;
  const mxArray *c5_rhs1 = NULL;
  const mxArray *c5_lhs1 = NULL;
  const mxArray *c5_rhs2 = NULL;
  const mxArray *c5_lhs2 = NULL;
  const mxArray *c5_rhs3 = NULL;
  const mxArray *c5_lhs3 = NULL;
  const mxArray *c5_rhs4 = NULL;
  const mxArray *c5_lhs4 = NULL;
  const mxArray *c5_rhs5 = NULL;
  const mxArray *c5_lhs5 = NULL;
  const mxArray *c5_rhs6 = NULL;
  const mxArray *c5_lhs6 = NULL;
  const mxArray *c5_rhs7 = NULL;
  const mxArray *c5_lhs7 = NULL;
  const mxArray *c5_rhs8 = NULL;
  const mxArray *c5_lhs8 = NULL;
  const mxArray *c5_rhs9 = NULL;
  const mxArray *c5_lhs9 = NULL;
  const mxArray *c5_rhs10 = NULL;
  const mxArray *c5_lhs10 = NULL;
  const mxArray *c5_rhs11 = NULL;
  const mxArray *c5_lhs11 = NULL;
  const mxArray *c5_rhs12 = NULL;
  const mxArray *c5_lhs12 = NULL;
  const mxArray *c5_rhs13 = NULL;
  const mxArray *c5_lhs13 = NULL;
  const mxArray *c5_rhs14 = NULL;
  const mxArray *c5_lhs14 = NULL;
  const mxArray *c5_rhs15 = NULL;
  const mxArray *c5_lhs15 = NULL;
  const mxArray *c5_rhs16 = NULL;
  const mxArray *c5_lhs16 = NULL;
  const mxArray *c5_rhs17 = NULL;
  const mxArray *c5_lhs17 = NULL;
  sf_mex_addfield(*c5_info, c5_emlrt_marshallOut(""), "context", "context", 0);
  sf_mex_addfield(*c5_info, c5_emlrt_marshallOut("length"), "name", "name", 0);
  sf_mex_addfield(*c5_info, c5_emlrt_marshallOut("int32"), "dominantType",
                  "dominantType", 0);
  sf_mex_addfield(*c5_info, c5_emlrt_marshallOut(
    "[ILXE]$matlabroot$/toolbox/eml/lib/matlab/elmat/length.m"), "resolved",
                  "resolved", 0);
  sf_mex_addfield(*c5_info, c5_b_emlrt_marshallOut(1303149806U), "fileTimeLo",
                  "fileTimeLo", 0);
  sf_mex_addfield(*c5_info, c5_b_emlrt_marshallOut(0U), "fileTimeHi",
                  "fileTimeHi", 0);
  sf_mex_addfield(*c5_info, c5_b_emlrt_marshallOut(0U), "mFileTimeLo",
                  "mFileTimeLo", 0);
  sf_mex_addfield(*c5_info, c5_b_emlrt_marshallOut(0U), "mFileTimeHi",
                  "mFileTimeHi", 0);
  sf_mex_assign(&c5_rhs0, sf_mex_createcellmatrix(0, 1), false);
  sf_mex_assign(&c5_lhs0, sf_mex_createcellmatrix(0, 1), false);
  sf_mex_addfield(*c5_info, sf_mex_duplicatearraysafe(&c5_rhs0), "rhs", "rhs", 0);
  sf_mex_addfield(*c5_info, sf_mex_duplicatearraysafe(&c5_lhs0), "lhs", "lhs", 0);
  sf_mex_addfield(*c5_info, c5_emlrt_marshallOut(""), "context", "context", 1);
  sf_mex_addfield(*c5_info, c5_emlrt_marshallOut("floor"), "name", "name", 1);
  sf_mex_addfield(*c5_info, c5_emlrt_marshallOut("single"), "dominantType",
                  "dominantType", 1);
  sf_mex_addfield(*c5_info, c5_emlrt_marshallOut(
    "[ILXE]$matlabroot$/toolbox/eml/lib/matlab/elfun/floor.m"), "resolved",
                  "resolved", 1);
  sf_mex_addfield(*c5_info, c5_b_emlrt_marshallOut(1363717454U), "fileTimeLo",
                  "fileTimeLo", 1);
  sf_mex_addfield(*c5_info, c5_b_emlrt_marshallOut(0U), "fileTimeHi",
                  "fileTimeHi", 1);
  sf_mex_addfield(*c5_info, c5_b_emlrt_marshallOut(0U), "mFileTimeLo",
                  "mFileTimeLo", 1);
  sf_mex_addfield(*c5_info, c5_b_emlrt_marshallOut(0U), "mFileTimeHi",
                  "mFileTimeHi", 1);
  sf_mex_assign(&c5_rhs1, sf_mex_createcellmatrix(0, 1), false);
  sf_mex_assign(&c5_lhs1, sf_mex_createcellmatrix(0, 1), false);
  sf_mex_addfield(*c5_info, sf_mex_duplicatearraysafe(&c5_rhs1), "rhs", "rhs", 1);
  sf_mex_addfield(*c5_info, sf_mex_duplicatearraysafe(&c5_lhs1), "lhs", "lhs", 1);
  sf_mex_addfield(*c5_info, c5_emlrt_marshallOut(
    "[ILXE]$matlabroot$/toolbox/eml/lib/matlab/elfun/floor.m"), "context",
                  "context", 2);
  sf_mex_addfield(*c5_info, c5_emlrt_marshallOut(
    "coder.internal.isBuiltInNumeric"), "name", "name", 2);
  sf_mex_addfield(*c5_info, c5_emlrt_marshallOut("single"), "dominantType",
                  "dominantType", 2);
  sf_mex_addfield(*c5_info, c5_emlrt_marshallOut(
    "[ILXE]$matlabroot$/toolbox/shared/coder/coder/+coder/+internal/isBuiltInNumeric.m"),
                  "resolved", "resolved", 2);
  sf_mex_addfield(*c5_info, c5_b_emlrt_marshallOut(1395935456U), "fileTimeLo",
                  "fileTimeLo", 2);
  sf_mex_addfield(*c5_info, c5_b_emlrt_marshallOut(0U), "fileTimeHi",
                  "fileTimeHi", 2);
  sf_mex_addfield(*c5_info, c5_b_emlrt_marshallOut(0U), "mFileTimeLo",
                  "mFileTimeLo", 2);
  sf_mex_addfield(*c5_info, c5_b_emlrt_marshallOut(0U), "mFileTimeHi",
                  "mFileTimeHi", 2);
  sf_mex_assign(&c5_rhs2, sf_mex_createcellmatrix(0, 1), false);
  sf_mex_assign(&c5_lhs2, sf_mex_createcellmatrix(0, 1), false);
  sf_mex_addfield(*c5_info, sf_mex_duplicatearraysafe(&c5_rhs2), "rhs", "rhs", 2);
  sf_mex_addfield(*c5_info, sf_mex_duplicatearraysafe(&c5_lhs2), "lhs", "lhs", 2);
  sf_mex_addfield(*c5_info, c5_emlrt_marshallOut(
    "[ILXE]$matlabroot$/toolbox/eml/lib/matlab/elfun/floor.m"), "context",
                  "context", 3);
  sf_mex_addfield(*c5_info, c5_emlrt_marshallOut("eml_scalar_floor"), "name",
                  "name", 3);
  sf_mex_addfield(*c5_info, c5_emlrt_marshallOut("single"), "dominantType",
                  "dominantType", 3);
  sf_mex_addfield(*c5_info, c5_emlrt_marshallOut(
    "[ILXE]$matlabroot$/toolbox/eml/lib/matlab/elfun/eml_scalar_floor.m"),
                  "resolved", "resolved", 3);
  sf_mex_addfield(*c5_info, c5_b_emlrt_marshallOut(1286822326U), "fileTimeLo",
                  "fileTimeLo", 3);
  sf_mex_addfield(*c5_info, c5_b_emlrt_marshallOut(0U), "fileTimeHi",
                  "fileTimeHi", 3);
  sf_mex_addfield(*c5_info, c5_b_emlrt_marshallOut(0U), "mFileTimeLo",
                  "mFileTimeLo", 3);
  sf_mex_addfield(*c5_info, c5_b_emlrt_marshallOut(0U), "mFileTimeHi",
                  "mFileTimeHi", 3);
  sf_mex_assign(&c5_rhs3, sf_mex_createcellmatrix(0, 1), false);
  sf_mex_assign(&c5_lhs3, sf_mex_createcellmatrix(0, 1), false);
  sf_mex_addfield(*c5_info, sf_mex_duplicatearraysafe(&c5_rhs3), "rhs", "rhs", 3);
  sf_mex_addfield(*c5_info, sf_mex_duplicatearraysafe(&c5_lhs3), "lhs", "lhs", 3);
  sf_mex_addfield(*c5_info, c5_emlrt_marshallOut(""), "context", "context", 4);
  sf_mex_addfield(*c5_info, c5_emlrt_marshallOut("find"), "name", "name", 4);
  sf_mex_addfield(*c5_info, c5_emlrt_marshallOut("logical"), "dominantType",
                  "dominantType", 4);
  sf_mex_addfield(*c5_info, c5_emlrt_marshallOut(
    "[ILXE]$matlabroot$/toolbox/eml/lib/matlab/elmat/find.m"), "resolved",
                  "resolved", 4);
  sf_mex_addfield(*c5_info, c5_b_emlrt_marshallOut(1303149806U), "fileTimeLo",
                  "fileTimeLo", 4);
  sf_mex_addfield(*c5_info, c5_b_emlrt_marshallOut(0U), "fileTimeHi",
                  "fileTimeHi", 4);
  sf_mex_addfield(*c5_info, c5_b_emlrt_marshallOut(0U), "mFileTimeLo",
                  "mFileTimeLo", 4);
  sf_mex_addfield(*c5_info, c5_b_emlrt_marshallOut(0U), "mFileTimeHi",
                  "mFileTimeHi", 4);
  sf_mex_assign(&c5_rhs4, sf_mex_createcellmatrix(0, 1), false);
  sf_mex_assign(&c5_lhs4, sf_mex_createcellmatrix(0, 1), false);
  sf_mex_addfield(*c5_info, sf_mex_duplicatearraysafe(&c5_rhs4), "rhs", "rhs", 4);
  sf_mex_addfield(*c5_info, sf_mex_duplicatearraysafe(&c5_lhs4), "lhs", "lhs", 4);
  sf_mex_addfield(*c5_info, c5_emlrt_marshallOut(
    "[ILXE]$matlabroot$/toolbox/eml/lib/matlab/elmat/find.m!eml_find"),
                  "context", "context", 5);
  sf_mex_addfield(*c5_info, c5_emlrt_marshallOut("eml_index_class"), "name",
                  "name", 5);
  sf_mex_addfield(*c5_info, c5_emlrt_marshallOut(""), "dominantType",
                  "dominantType", 5);
  sf_mex_addfield(*c5_info, c5_emlrt_marshallOut(
    "[ILXE]$matlabroot$/toolbox/eml/lib/matlab/eml/eml_index_class.m"),
                  "resolved", "resolved", 5);
  sf_mex_addfield(*c5_info, c5_b_emlrt_marshallOut(1323174178U), "fileTimeLo",
                  "fileTimeLo", 5);
  sf_mex_addfield(*c5_info, c5_b_emlrt_marshallOut(0U), "fileTimeHi",
                  "fileTimeHi", 5);
  sf_mex_addfield(*c5_info, c5_b_emlrt_marshallOut(0U), "mFileTimeLo",
                  "mFileTimeLo", 5);
  sf_mex_addfield(*c5_info, c5_b_emlrt_marshallOut(0U), "mFileTimeHi",
                  "mFileTimeHi", 5);
  sf_mex_assign(&c5_rhs5, sf_mex_createcellmatrix(0, 1), false);
  sf_mex_assign(&c5_lhs5, sf_mex_createcellmatrix(0, 1), false);
  sf_mex_addfield(*c5_info, sf_mex_duplicatearraysafe(&c5_rhs5), "rhs", "rhs", 5);
  sf_mex_addfield(*c5_info, sf_mex_duplicatearraysafe(&c5_lhs5), "lhs", "lhs", 5);
  sf_mex_addfield(*c5_info, c5_emlrt_marshallOut(
    "[ILXE]$matlabroot$/toolbox/eml/lib/matlab/elmat/find.m!eml_find"),
                  "context", "context", 6);
  sf_mex_addfield(*c5_info, c5_emlrt_marshallOut("eml_scalar_eg"), "name",
                  "name", 6);
  sf_mex_addfield(*c5_info, c5_emlrt_marshallOut("logical"), "dominantType",
                  "dominantType", 6);
  sf_mex_addfield(*c5_info, c5_emlrt_marshallOut(
    "[ILXE]$matlabroot$/toolbox/eml/lib/matlab/eml/eml_scalar_eg.m"), "resolved",
                  "resolved", 6);
  sf_mex_addfield(*c5_info, c5_b_emlrt_marshallOut(1375984288U), "fileTimeLo",
                  "fileTimeLo", 6);
  sf_mex_addfield(*c5_info, c5_b_emlrt_marshallOut(0U), "fileTimeHi",
                  "fileTimeHi", 6);
  sf_mex_addfield(*c5_info, c5_b_emlrt_marshallOut(0U), "mFileTimeLo",
                  "mFileTimeLo", 6);
  sf_mex_addfield(*c5_info, c5_b_emlrt_marshallOut(0U), "mFileTimeHi",
                  "mFileTimeHi", 6);
  sf_mex_assign(&c5_rhs6, sf_mex_createcellmatrix(0, 1), false);
  sf_mex_assign(&c5_lhs6, sf_mex_createcellmatrix(0, 1), false);
  sf_mex_addfield(*c5_info, sf_mex_duplicatearraysafe(&c5_rhs6), "rhs", "rhs", 6);
  sf_mex_addfield(*c5_info, sf_mex_duplicatearraysafe(&c5_lhs6), "lhs", "lhs", 6);
  sf_mex_addfield(*c5_info, c5_emlrt_marshallOut(
    "[ILXE]$matlabroot$/toolbox/eml/lib/matlab/eml/eml_scalar_eg.m"), "context",
                  "context", 7);
  sf_mex_addfield(*c5_info, c5_emlrt_marshallOut("coder.internal.scalarEg"),
                  "name", "name", 7);
  sf_mex_addfield(*c5_info, c5_emlrt_marshallOut("logical"), "dominantType",
                  "dominantType", 7);
  sf_mex_addfield(*c5_info, c5_emlrt_marshallOut(
    "[ILXE]$matlabroot$/toolbox/coder/coder/+coder/+internal/scalarEg.p"),
                  "resolved", "resolved", 7);
  sf_mex_addfield(*c5_info, c5_b_emlrt_marshallOut(1410811370U), "fileTimeLo",
                  "fileTimeLo", 7);
  sf_mex_addfield(*c5_info, c5_b_emlrt_marshallOut(0U), "fileTimeHi",
                  "fileTimeHi", 7);
  sf_mex_addfield(*c5_info, c5_b_emlrt_marshallOut(0U), "mFileTimeLo",
                  "mFileTimeLo", 7);
  sf_mex_addfield(*c5_info, c5_b_emlrt_marshallOut(0U), "mFileTimeHi",
                  "mFileTimeHi", 7);
  sf_mex_assign(&c5_rhs7, sf_mex_createcellmatrix(0, 1), false);
  sf_mex_assign(&c5_lhs7, sf_mex_createcellmatrix(0, 1), false);
  sf_mex_addfield(*c5_info, sf_mex_duplicatearraysafe(&c5_rhs7), "rhs", "rhs", 7);
  sf_mex_addfield(*c5_info, sf_mex_duplicatearraysafe(&c5_lhs7), "lhs", "lhs", 7);
  sf_mex_addfield(*c5_info, c5_emlrt_marshallOut(
    "[ILXE]$matlabroot$/toolbox/eml/lib/matlab/elmat/find.m!eml_find"),
                  "context", "context", 8);
  sf_mex_addfield(*c5_info, c5_emlrt_marshallOut(
    "eml_int_forloop_overflow_check"), "name", "name", 8);
  sf_mex_addfield(*c5_info, c5_emlrt_marshallOut(""), "dominantType",
                  "dominantType", 8);
  sf_mex_addfield(*c5_info, c5_emlrt_marshallOut(
    "[ILXE]$matlabroot$/toolbox/eml/lib/matlab/eml/eml_int_forloop_overflow_check.m"),
                  "resolved", "resolved", 8);
  sf_mex_addfield(*c5_info, c5_b_emlrt_marshallOut(1397261022U), "fileTimeLo",
                  "fileTimeLo", 8);
  sf_mex_addfield(*c5_info, c5_b_emlrt_marshallOut(0U), "fileTimeHi",
                  "fileTimeHi", 8);
  sf_mex_addfield(*c5_info, c5_b_emlrt_marshallOut(0U), "mFileTimeLo",
                  "mFileTimeLo", 8);
  sf_mex_addfield(*c5_info, c5_b_emlrt_marshallOut(0U), "mFileTimeHi",
                  "mFileTimeHi", 8);
  sf_mex_assign(&c5_rhs8, sf_mex_createcellmatrix(0, 1), false);
  sf_mex_assign(&c5_lhs8, sf_mex_createcellmatrix(0, 1), false);
  sf_mex_addfield(*c5_info, sf_mex_duplicatearraysafe(&c5_rhs8), "rhs", "rhs", 8);
  sf_mex_addfield(*c5_info, sf_mex_duplicatearraysafe(&c5_lhs8), "lhs", "lhs", 8);
  sf_mex_addfield(*c5_info, c5_emlrt_marshallOut(
    "[ILXE]$matlabroot$/toolbox/eml/lib/matlab/eml/eml_int_forloop_overflow_check.m!eml_int_forloop_overflow_check_helper"),
                  "context", "context", 9);
  sf_mex_addfield(*c5_info, c5_emlrt_marshallOut("isfi"), "name", "name", 9);
  sf_mex_addfield(*c5_info, c5_emlrt_marshallOut("coder.internal.indexInt"),
                  "dominantType", "dominantType", 9);
  sf_mex_addfield(*c5_info, c5_emlrt_marshallOut(
    "[ILXE]$matlabroot$/toolbox/eml/lib/fixedpoint/isfi.m"), "resolved",
                  "resolved", 9);
  sf_mex_addfield(*c5_info, c5_b_emlrt_marshallOut(1346513958U), "fileTimeLo",
                  "fileTimeLo", 9);
  sf_mex_addfield(*c5_info, c5_b_emlrt_marshallOut(0U), "fileTimeHi",
                  "fileTimeHi", 9);
  sf_mex_addfield(*c5_info, c5_b_emlrt_marshallOut(0U), "mFileTimeLo",
                  "mFileTimeLo", 9);
  sf_mex_addfield(*c5_info, c5_b_emlrt_marshallOut(0U), "mFileTimeHi",
                  "mFileTimeHi", 9);
  sf_mex_assign(&c5_rhs9, sf_mex_createcellmatrix(0, 1), false);
  sf_mex_assign(&c5_lhs9, sf_mex_createcellmatrix(0, 1), false);
  sf_mex_addfield(*c5_info, sf_mex_duplicatearraysafe(&c5_rhs9), "rhs", "rhs", 9);
  sf_mex_addfield(*c5_info, sf_mex_duplicatearraysafe(&c5_lhs9), "lhs", "lhs", 9);
  sf_mex_addfield(*c5_info, c5_emlrt_marshallOut(
    "[ILXE]$matlabroot$/toolbox/eml/lib/fixedpoint/isfi.m"), "context",
                  "context", 10);
  sf_mex_addfield(*c5_info, c5_emlrt_marshallOut("isnumerictype"), "name",
                  "name", 10);
  sf_mex_addfield(*c5_info, c5_emlrt_marshallOut("char"), "dominantType",
                  "dominantType", 10);
  sf_mex_addfield(*c5_info, c5_emlrt_marshallOut(
    "[ILXE]$matlabroot$/toolbox/eml/lib/fixedpoint/isnumerictype.m"), "resolved",
                  "resolved", 10);
  sf_mex_addfield(*c5_info, c5_b_emlrt_marshallOut(1398879198U), "fileTimeLo",
                  "fileTimeLo", 10);
  sf_mex_addfield(*c5_info, c5_b_emlrt_marshallOut(0U), "fileTimeHi",
                  "fileTimeHi", 10);
  sf_mex_addfield(*c5_info, c5_b_emlrt_marshallOut(0U), "mFileTimeLo",
                  "mFileTimeLo", 10);
  sf_mex_addfield(*c5_info, c5_b_emlrt_marshallOut(0U), "mFileTimeHi",
                  "mFileTimeHi", 10);
  sf_mex_assign(&c5_rhs10, sf_mex_createcellmatrix(0, 1), false);
  sf_mex_assign(&c5_lhs10, sf_mex_createcellmatrix(0, 1), false);
  sf_mex_addfield(*c5_info, sf_mex_duplicatearraysafe(&c5_rhs10), "rhs", "rhs",
                  10);
  sf_mex_addfield(*c5_info, sf_mex_duplicatearraysafe(&c5_lhs10), "lhs", "lhs",
                  10);
  sf_mex_addfield(*c5_info, c5_emlrt_marshallOut(
    "[ILXE]$matlabroot$/toolbox/eml/lib/matlab/eml/eml_int_forloop_overflow_check.m!eml_int_forloop_overflow_check_helper"),
                  "context", "context", 11);
  sf_mex_addfield(*c5_info, c5_emlrt_marshallOut("intmax"), "name", "name", 11);
  sf_mex_addfield(*c5_info, c5_emlrt_marshallOut("char"), "dominantType",
                  "dominantType", 11);
  sf_mex_addfield(*c5_info, c5_emlrt_marshallOut(
    "[ILXE]$matlabroot$/toolbox/eml/lib/matlab/elmat/intmax.m"), "resolved",
                  "resolved", 11);
  sf_mex_addfield(*c5_info, c5_b_emlrt_marshallOut(1362265482U), "fileTimeLo",
                  "fileTimeLo", 11);
  sf_mex_addfield(*c5_info, c5_b_emlrt_marshallOut(0U), "fileTimeHi",
                  "fileTimeHi", 11);
  sf_mex_addfield(*c5_info, c5_b_emlrt_marshallOut(0U), "mFileTimeLo",
                  "mFileTimeLo", 11);
  sf_mex_addfield(*c5_info, c5_b_emlrt_marshallOut(0U), "mFileTimeHi",
                  "mFileTimeHi", 11);
  sf_mex_assign(&c5_rhs11, sf_mex_createcellmatrix(0, 1), false);
  sf_mex_assign(&c5_lhs11, sf_mex_createcellmatrix(0, 1), false);
  sf_mex_addfield(*c5_info, sf_mex_duplicatearraysafe(&c5_rhs11), "rhs", "rhs",
                  11);
  sf_mex_addfield(*c5_info, sf_mex_duplicatearraysafe(&c5_lhs11), "lhs", "lhs",
                  11);
  sf_mex_addfield(*c5_info, c5_emlrt_marshallOut(
    "[ILXE]$matlabroot$/toolbox/eml/lib/matlab/elmat/intmax.m"), "context",
                  "context", 12);
  sf_mex_addfield(*c5_info, c5_emlrt_marshallOut("eml_switch_helper"), "name",
                  "name", 12);
  sf_mex_addfield(*c5_info, c5_emlrt_marshallOut(""), "dominantType",
                  "dominantType", 12);
  sf_mex_addfield(*c5_info, c5_emlrt_marshallOut(
    "[ILXE]$matlabroot$/toolbox/eml/lib/matlab/eml/eml_switch_helper.m"),
                  "resolved", "resolved", 12);
  sf_mex_addfield(*c5_info, c5_b_emlrt_marshallOut(1393334458U), "fileTimeLo",
                  "fileTimeLo", 12);
  sf_mex_addfield(*c5_info, c5_b_emlrt_marshallOut(0U), "fileTimeHi",
                  "fileTimeHi", 12);
  sf_mex_addfield(*c5_info, c5_b_emlrt_marshallOut(0U), "mFileTimeLo",
                  "mFileTimeLo", 12);
  sf_mex_addfield(*c5_info, c5_b_emlrt_marshallOut(0U), "mFileTimeHi",
                  "mFileTimeHi", 12);
  sf_mex_assign(&c5_rhs12, sf_mex_createcellmatrix(0, 1), false);
  sf_mex_assign(&c5_lhs12, sf_mex_createcellmatrix(0, 1), false);
  sf_mex_addfield(*c5_info, sf_mex_duplicatearraysafe(&c5_rhs12), "rhs", "rhs",
                  12);
  sf_mex_addfield(*c5_info, sf_mex_duplicatearraysafe(&c5_lhs12), "lhs", "lhs",
                  12);
  sf_mex_addfield(*c5_info, c5_emlrt_marshallOut(
    "[ILXE]$matlabroot$/toolbox/eml/lib/matlab/eml/eml_int_forloop_overflow_check.m!eml_int_forloop_overflow_check_helper"),
                  "context", "context", 13);
  sf_mex_addfield(*c5_info, c5_emlrt_marshallOut("intmin"), "name", "name", 13);
  sf_mex_addfield(*c5_info, c5_emlrt_marshallOut("char"), "dominantType",
                  "dominantType", 13);
  sf_mex_addfield(*c5_info, c5_emlrt_marshallOut(
    "[ILXE]$matlabroot$/toolbox/eml/lib/matlab/elmat/intmin.m"), "resolved",
                  "resolved", 13);
  sf_mex_addfield(*c5_info, c5_b_emlrt_marshallOut(1362265482U), "fileTimeLo",
                  "fileTimeLo", 13);
  sf_mex_addfield(*c5_info, c5_b_emlrt_marshallOut(0U), "fileTimeHi",
                  "fileTimeHi", 13);
  sf_mex_addfield(*c5_info, c5_b_emlrt_marshallOut(0U), "mFileTimeLo",
                  "mFileTimeLo", 13);
  sf_mex_addfield(*c5_info, c5_b_emlrt_marshallOut(0U), "mFileTimeHi",
                  "mFileTimeHi", 13);
  sf_mex_assign(&c5_rhs13, sf_mex_createcellmatrix(0, 1), false);
  sf_mex_assign(&c5_lhs13, sf_mex_createcellmatrix(0, 1), false);
  sf_mex_addfield(*c5_info, sf_mex_duplicatearraysafe(&c5_rhs13), "rhs", "rhs",
                  13);
  sf_mex_addfield(*c5_info, sf_mex_duplicatearraysafe(&c5_lhs13), "lhs", "lhs",
                  13);
  sf_mex_addfield(*c5_info, c5_emlrt_marshallOut(
    "[ILXE]$matlabroot$/toolbox/eml/lib/matlab/elmat/intmin.m"), "context",
                  "context", 14);
  sf_mex_addfield(*c5_info, c5_emlrt_marshallOut("eml_switch_helper"), "name",
                  "name", 14);
  sf_mex_addfield(*c5_info, c5_emlrt_marshallOut(""), "dominantType",
                  "dominantType", 14);
  sf_mex_addfield(*c5_info, c5_emlrt_marshallOut(
    "[ILXE]$matlabroot$/toolbox/eml/lib/matlab/eml/eml_switch_helper.m"),
                  "resolved", "resolved", 14);
  sf_mex_addfield(*c5_info, c5_b_emlrt_marshallOut(1393334458U), "fileTimeLo",
                  "fileTimeLo", 14);
  sf_mex_addfield(*c5_info, c5_b_emlrt_marshallOut(0U), "fileTimeHi",
                  "fileTimeHi", 14);
  sf_mex_addfield(*c5_info, c5_b_emlrt_marshallOut(0U), "mFileTimeLo",
                  "mFileTimeLo", 14);
  sf_mex_addfield(*c5_info, c5_b_emlrt_marshallOut(0U), "mFileTimeHi",
                  "mFileTimeHi", 14);
  sf_mex_assign(&c5_rhs14, sf_mex_createcellmatrix(0, 1), false);
  sf_mex_assign(&c5_lhs14, sf_mex_createcellmatrix(0, 1), false);
  sf_mex_addfield(*c5_info, sf_mex_duplicatearraysafe(&c5_rhs14), "rhs", "rhs",
                  14);
  sf_mex_addfield(*c5_info, sf_mex_duplicatearraysafe(&c5_lhs14), "lhs", "lhs",
                  14);
  sf_mex_addfield(*c5_info, c5_emlrt_marshallOut(
    "[ILXE]$matlabroot$/toolbox/eml/lib/matlab/elmat/find.m!eml_find"),
                  "context", "context", 15);
  sf_mex_addfield(*c5_info, c5_emlrt_marshallOut("eml_index_plus"), "name",
                  "name", 15);
  sf_mex_addfield(*c5_info, c5_emlrt_marshallOut("double"), "dominantType",
                  "dominantType", 15);
  sf_mex_addfield(*c5_info, c5_emlrt_marshallOut(
    "[ILXE]$matlabroot$/toolbox/eml/lib/matlab/eml/eml_index_plus.m"),
                  "resolved", "resolved", 15);
  sf_mex_addfield(*c5_info, c5_b_emlrt_marshallOut(1372586016U), "fileTimeLo",
                  "fileTimeLo", 15);
  sf_mex_addfield(*c5_info, c5_b_emlrt_marshallOut(0U), "fileTimeHi",
                  "fileTimeHi", 15);
  sf_mex_addfield(*c5_info, c5_b_emlrt_marshallOut(0U), "mFileTimeLo",
                  "mFileTimeLo", 15);
  sf_mex_addfield(*c5_info, c5_b_emlrt_marshallOut(0U), "mFileTimeHi",
                  "mFileTimeHi", 15);
  sf_mex_assign(&c5_rhs15, sf_mex_createcellmatrix(0, 1), false);
  sf_mex_assign(&c5_lhs15, sf_mex_createcellmatrix(0, 1), false);
  sf_mex_addfield(*c5_info, sf_mex_duplicatearraysafe(&c5_rhs15), "rhs", "rhs",
                  15);
  sf_mex_addfield(*c5_info, sf_mex_duplicatearraysafe(&c5_lhs15), "lhs", "lhs",
                  15);
  sf_mex_addfield(*c5_info, c5_emlrt_marshallOut(
    "[ILXE]$matlabroot$/toolbox/eml/lib/matlab/eml/eml_index_plus.m"), "context",
                  "context", 16);
  sf_mex_addfield(*c5_info, c5_emlrt_marshallOut("coder.internal.indexPlus"),
                  "name", "name", 16);
  sf_mex_addfield(*c5_info, c5_emlrt_marshallOut("double"), "dominantType",
                  "dominantType", 16);
  sf_mex_addfield(*c5_info, c5_emlrt_marshallOut(
    "[ILXE]$matlabroot$/toolbox/shared/coder/coder/+coder/+internal/indexPlus.m"),
                  "resolved", "resolved", 16);
  sf_mex_addfield(*c5_info, c5_b_emlrt_marshallOut(1372586760U), "fileTimeLo",
                  "fileTimeLo", 16);
  sf_mex_addfield(*c5_info, c5_b_emlrt_marshallOut(0U), "fileTimeHi",
                  "fileTimeHi", 16);
  sf_mex_addfield(*c5_info, c5_b_emlrt_marshallOut(0U), "mFileTimeLo",
                  "mFileTimeLo", 16);
  sf_mex_addfield(*c5_info, c5_b_emlrt_marshallOut(0U), "mFileTimeHi",
                  "mFileTimeHi", 16);
  sf_mex_assign(&c5_rhs16, sf_mex_createcellmatrix(0, 1), false);
  sf_mex_assign(&c5_lhs16, sf_mex_createcellmatrix(0, 1), false);
  sf_mex_addfield(*c5_info, sf_mex_duplicatearraysafe(&c5_rhs16), "rhs", "rhs",
                  16);
  sf_mex_addfield(*c5_info, sf_mex_duplicatearraysafe(&c5_lhs16), "lhs", "lhs",
                  16);
  sf_mex_addfield(*c5_info, c5_emlrt_marshallOut(""), "context", "context", 17);
  sf_mex_addfield(*c5_info, c5_emlrt_marshallOut("length"), "name", "name", 17);
  sf_mex_addfield(*c5_info, c5_emlrt_marshallOut("double"), "dominantType",
                  "dominantType", 17);
  sf_mex_addfield(*c5_info, c5_emlrt_marshallOut(
    "[ILXE]$matlabroot$/toolbox/eml/lib/matlab/elmat/length.m"), "resolved",
                  "resolved", 17);
  sf_mex_addfield(*c5_info, c5_b_emlrt_marshallOut(1303149806U), "fileTimeLo",
                  "fileTimeLo", 17);
  sf_mex_addfield(*c5_info, c5_b_emlrt_marshallOut(0U), "fileTimeHi",
                  "fileTimeHi", 17);
  sf_mex_addfield(*c5_info, c5_b_emlrt_marshallOut(0U), "mFileTimeLo",
                  "mFileTimeLo", 17);
  sf_mex_addfield(*c5_info, c5_b_emlrt_marshallOut(0U), "mFileTimeHi",
                  "mFileTimeHi", 17);
  sf_mex_assign(&c5_rhs17, sf_mex_createcellmatrix(0, 1), false);
  sf_mex_assign(&c5_lhs17, sf_mex_createcellmatrix(0, 1), false);
  sf_mex_addfield(*c5_info, sf_mex_duplicatearraysafe(&c5_rhs17), "rhs", "rhs",
                  17);
  sf_mex_addfield(*c5_info, sf_mex_duplicatearraysafe(&c5_lhs17), "lhs", "lhs",
                  17);
  sf_mex_destroy(&c5_rhs0);
  sf_mex_destroy(&c5_lhs0);
  sf_mex_destroy(&c5_rhs1);
  sf_mex_destroy(&c5_lhs1);
  sf_mex_destroy(&c5_rhs2);
  sf_mex_destroy(&c5_lhs2);
  sf_mex_destroy(&c5_rhs3);
  sf_mex_destroy(&c5_lhs3);
  sf_mex_destroy(&c5_rhs4);
  sf_mex_destroy(&c5_lhs4);
  sf_mex_destroy(&c5_rhs5);
  sf_mex_destroy(&c5_lhs5);
  sf_mex_destroy(&c5_rhs6);
  sf_mex_destroy(&c5_lhs6);
  sf_mex_destroy(&c5_rhs7);
  sf_mex_destroy(&c5_lhs7);
  sf_mex_destroy(&c5_rhs8);
  sf_mex_destroy(&c5_lhs8);
  sf_mex_destroy(&c5_rhs9);
  sf_mex_destroy(&c5_lhs9);
  sf_mex_destroy(&c5_rhs10);
  sf_mex_destroy(&c5_lhs10);
  sf_mex_destroy(&c5_rhs11);
  sf_mex_destroy(&c5_lhs11);
  sf_mex_destroy(&c5_rhs12);
  sf_mex_destroy(&c5_lhs12);
  sf_mex_destroy(&c5_rhs13);
  sf_mex_destroy(&c5_lhs13);
  sf_mex_destroy(&c5_rhs14);
  sf_mex_destroy(&c5_lhs14);
  sf_mex_destroy(&c5_rhs15);
  sf_mex_destroy(&c5_lhs15);
  sf_mex_destroy(&c5_rhs16);
  sf_mex_destroy(&c5_lhs16);
  sf_mex_destroy(&c5_rhs17);
  sf_mex_destroy(&c5_lhs17);
}

static const mxArray *c5_emlrt_marshallOut(const char * c5_u)
{
  const mxArray *c5_y = NULL;
  c5_y = NULL;
  sf_mex_assign(&c5_y, sf_mex_create("y", c5_u, 15, 0U, 0U, 0U, 2, 1, strlen
    (c5_u)), false);
  return c5_y;
}

static const mxArray *c5_b_emlrt_marshallOut(const uint32_T c5_u)
{
  const mxArray *c5_y = NULL;
  c5_y = NULL;
  sf_mex_assign(&c5_y, sf_mex_create("y", &c5_u, 7, 0U, 0U, 0U, 0), false);
  return c5_y;
}

static void c5_floor(SFc5_ImageDetInstanceStruct *chartInstance, real32_T
                     c5_x_data[], int32_T c5_x_sizes[2], real32_T c5_b_x_data[],
                     int32_T c5_b_x_sizes[2])
{
  int32_T c5_x;
  int32_T c5_b_x;
  int32_T c5_loop_ub;
  int32_T c5_i71;
  c5_b_x_sizes[0] = c5_x_sizes[0];
  c5_b_x_sizes[1] = c5_x_sizes[1];
  c5_x = c5_b_x_sizes[0];
  c5_b_x = c5_b_x_sizes[1];
  c5_loop_ub = c5_x_sizes[0] * c5_x_sizes[1] - 1;
  for (c5_i71 = 0; c5_i71 <= c5_loop_ub; c5_i71++) {
    c5_b_x_data[c5_i71] = c5_x_data[c5_i71];
  }

  c5_b_floor(chartInstance, c5_b_x_data, c5_b_x_sizes);
}

static const mxArray *c5_i_sf_marshallOut(void *chartInstanceVoid, void
  *c5_inData)
{
  const mxArray *c5_mxArrayOutData = NULL;
  int32_T c5_u;
  const mxArray *c5_y = NULL;
  SFc5_ImageDetInstanceStruct *chartInstance;
  chartInstance = (SFc5_ImageDetInstanceStruct *)chartInstanceVoid;
  c5_mxArrayOutData = NULL;
  c5_u = *(int32_T *)c5_inData;
  c5_y = NULL;
  sf_mex_assign(&c5_y, sf_mex_create("y", &c5_u, 6, 0U, 0U, 0U, 0), false);
  sf_mex_assign(&c5_mxArrayOutData, c5_y, false);
  return c5_mxArrayOutData;
}

static int32_T c5_j_emlrt_marshallIn(SFc5_ImageDetInstanceStruct *chartInstance,
  const mxArray *c5_u, const emlrtMsgIdentifier *c5_parentId)
{
  int32_T c5_y;
  int32_T c5_i72;
  (void)chartInstance;
  sf_mex_import(c5_parentId, sf_mex_dup(c5_u), &c5_i72, 1, 6, 0U, 0, 0U, 0);
  c5_y = c5_i72;
  sf_mex_destroy(&c5_u);
  return c5_y;
}

static void c5_g_sf_marshallIn(void *chartInstanceVoid, const mxArray
  *c5_mxArrayInData, const char_T *c5_varName, void *c5_outData)
{
  const mxArray *c5_b_sfEvent;
  const char_T *c5_identifier;
  emlrtMsgIdentifier c5_thisId;
  int32_T c5_y;
  SFc5_ImageDetInstanceStruct *chartInstance;
  chartInstance = (SFc5_ImageDetInstanceStruct *)chartInstanceVoid;
  c5_b_sfEvent = sf_mex_dup(c5_mxArrayInData);
  c5_identifier = c5_varName;
  c5_thisId.fIdentifier = c5_identifier;
  c5_thisId.fParent = NULL;
  c5_y = c5_j_emlrt_marshallIn(chartInstance, sf_mex_dup(c5_b_sfEvent),
    &c5_thisId);
  sf_mex_destroy(&c5_b_sfEvent);
  *(int32_T *)c5_outData = c5_y;
  sf_mex_destroy(&c5_mxArrayInData);
}

static const mxArray *c5_blobs_bus_io(void *chartInstanceVoid, void *c5_pData)
{
  const mxArray *c5_mxVal = NULL;
  SFc5_ImageDetInstanceStruct *chartInstance;
  (void)c5_pData;
  chartInstance = (SFc5_ImageDetInstanceStruct *)chartInstanceVoid;
  c5_mxVal = NULL;
  sf_mex_assign(&c5_mxVal, c5_sf_marshall_unsupported(chartInstance), false);
  return c5_mxVal;
}

static const mxArray *c5_sf_marshall_unsupported(void *chartInstanceVoid)
{
  const mxArray *c5_y = NULL;
  SFc5_ImageDetInstanceStruct *chartInstance;
  chartInstance = (SFc5_ImageDetInstanceStruct *)chartInstanceVoid;
  c5_y = NULL;
  sf_mex_assign(&c5_y, c5_c_emlrt_marshallOut(chartInstance,
    "Structures with variable-sized fields unsupported for debugging."), false);
  return c5_y;
}

static const mxArray *c5_c_emlrt_marshallOut(SFc5_ImageDetInstanceStruct
  *chartInstance, const char * c5_u)
{
  const mxArray *c5_y = NULL;
  (void)chartInstance;
  c5_y = NULL;
  sf_mex_assign(&c5_y, sf_mex_create("y", c5_u, 15, 0U, 0U, 0U, 2, 1, strlen
    (c5_u)), false);
  return c5_y;
}

static uint8_T c5_k_emlrt_marshallIn(SFc5_ImageDetInstanceStruct *chartInstance,
  const mxArray *c5_b_is_active_c5_ImageDet, const char_T *c5_identifier)
{
  uint8_T c5_y;
  emlrtMsgIdentifier c5_thisId;
  c5_thisId.fIdentifier = c5_identifier;
  c5_thisId.fParent = NULL;
  c5_y = c5_l_emlrt_marshallIn(chartInstance, sf_mex_dup
    (c5_b_is_active_c5_ImageDet), &c5_thisId);
  sf_mex_destroy(&c5_b_is_active_c5_ImageDet);
  return c5_y;
}

static uint8_T c5_l_emlrt_marshallIn(SFc5_ImageDetInstanceStruct *chartInstance,
  const mxArray *c5_u, const emlrtMsgIdentifier *c5_parentId)
{
  uint8_T c5_y;
  uint8_T c5_u1;
  (void)chartInstance;
  sf_mex_import(c5_parentId, sf_mex_dup(c5_u), &c5_u1, 1, 3, 0U, 0, 0U, 0);
  c5_y = c5_u1;
  sf_mex_destroy(&c5_u);
  return c5_y;
}

static void c5_b_floor(SFc5_ImageDetInstanceStruct *chartInstance, real32_T
  c5_x_data[], int32_T c5_x_sizes[2])
{
  real_T c5_d3;
  int32_T c5_i73;
  int32_T c5_k;
  real_T c5_b_k;
  real32_T c5_x;
  real32_T c5_b_x;
  int32_T c5_c_x[1];
  (void)chartInstance;
  c5_d3 = (real_T)(c5_x_sizes[0] * c5_x_sizes[1]);
  c5_i73 = (int32_T)c5_d3 - 1;
  for (c5_k = 0; c5_k <= c5_i73; c5_k++) {
    c5_b_k = 1.0 + (real_T)c5_k;
    c5_x = c5_x_data[_SFD_EML_ARRAY_BOUNDS_CHECK("", (int32_T)c5_b_k, 1,
      c5_x_sizes[0] * c5_x_sizes[1], 1, 0) - 1];
    c5_b_x = c5_x;
    c5_b_x = muSingleScalarFloor(c5_b_x);
    c5_c_x[0] = c5_x_sizes[0] * c5_x_sizes[1];
    c5_x_data[_SFD_EML_ARRAY_BOUNDS_CHECK("", (int32_T)c5_b_k, 1, c5_x_sizes[0] *
      c5_x_sizes[1], 1, 0) - 1] = c5_b_x;
  }
}

static void init_dsm_address_info(SFc5_ImageDetInstanceStruct *chartInstance)
{
  (void)chartInstance;
}

static void init_simulink_io_address(SFc5_ImageDetInstanceStruct *chartInstance)
{
  chartInstance->c5_centroids_data = (real32_T (*)[32])
    ssGetInputPortSignal_wrapper(chartInstance->S, 0);
  chartInstance->c5_centroids_sizes = (int32_T (*)[2])
    ssGetCurrentInputPortDimensions_wrapper(chartInstance->S, 0);
  chartInstance->c5_area_data = (int32_T (*)[16])ssGetInputPortSignal_wrapper
    (chartInstance->S, 1);
  chartInstance->c5_area_sizes = (int32_T (*)[2])
    ssGetCurrentInputPortDimensions_wrapper(chartInstance->S, 1);
  sf_mex_size_one_check(((*chartInstance->c5_area_sizes)[1U] == 0) &&
                        (!((*chartInstance->c5_area_sizes)[0U] == 0)), "area");
  chartInstance->c5_RBW = (boolean_T (*)[76800])ssGetInputPortSignal_wrapper
    (chartInstance->S, 2);
  chartInstance->c5_BBW = (boolean_T (*)[76800])ssGetInputPortSignal_wrapper
    (chartInstance->S, 3);
  chartInstance->c5_blobs_data = (c5_BlobData *)ssGetOutputPortSignal_wrapper
    (chartInstance->S, 1);
  chartInstance->c5_blobs_elems_sizes = (c5_BlobData_size *)
    ssGetCurrentOutputPortDimensions_wrapper(chartInstance->S, 1);
}

/* SFunction Glue Code */
#ifdef utFree
#undef utFree
#endif

#ifdef utMalloc
#undef utMalloc
#endif

#ifdef __cplusplus

extern "C" void *utMalloc(size_t size);
extern "C" void utFree(void*);

#else

extern void *utMalloc(size_t size);
extern void utFree(void*);

#endif

void sf_c5_ImageDet_get_check_sum(mxArray *plhs[])
{
  ((real_T *)mxGetPr((plhs[0])))[0] = (real_T)(1409144398U);
  ((real_T *)mxGetPr((plhs[0])))[1] = (real_T)(3791171080U);
  ((real_T *)mxGetPr((plhs[0])))[2] = (real_T)(127671523U);
  ((real_T *)mxGetPr((plhs[0])))[3] = (real_T)(2966984979U);
}

mxArray* sf_c5_ImageDet_get_post_codegen_info(void);
mxArray *sf_c5_ImageDet_get_autoinheritance_info(void)
{
  const char *autoinheritanceFields[] = { "checksum", "inputs", "parameters",
    "outputs", "locals", "postCodegenInfo" };

  mxArray *mxAutoinheritanceInfo = mxCreateStructMatrix(1, 1, sizeof
    (autoinheritanceFields)/sizeof(autoinheritanceFields[0]),
    autoinheritanceFields);

  {
    mxArray *mxChecksum = mxCreateString("iDJyvjMVCb74ZpvVxe3sSG");
    mxSetField(mxAutoinheritanceInfo,0,"checksum",mxChecksum);
  }

  {
    const char *dataFields[] = { "size", "type", "complexity" };

    mxArray *mxData = mxCreateStructMatrix(1,4,3,dataFields);

    {
      mxArray *mxSize = mxCreateDoubleMatrix(1,2,mxREAL);
      double *pr = mxGetPr(mxSize);
      pr[0] = (double)(16);
      pr[1] = (double)(2);
      mxSetField(mxData,0,"size",mxSize);
    }

    {
      const char *typeFields[] = { "base", "fixpt" };

      mxArray *mxType = mxCreateStructMatrix(1,1,2,typeFields);
      mxSetField(mxType,0,"base",mxCreateDoubleScalar(9));
      mxSetField(mxType,0,"fixpt",mxCreateDoubleMatrix(0,0,mxREAL));
      mxSetField(mxData,0,"type",mxType);
    }

    mxSetField(mxData,0,"complexity",mxCreateDoubleScalar(0));

    {
      mxArray *mxSize = mxCreateDoubleMatrix(1,2,mxREAL);
      double *pr = mxGetPr(mxSize);
      pr[0] = (double)(16);
      pr[1] = (double)(1);
      mxSetField(mxData,1,"size",mxSize);
    }

    {
      const char *typeFields[] = { "base", "fixpt" };

      mxArray *mxType = mxCreateStructMatrix(1,1,2,typeFields);
      mxSetField(mxType,0,"base",mxCreateDoubleScalar(8));
      mxSetField(mxType,0,"fixpt",mxCreateDoubleMatrix(0,0,mxREAL));
      mxSetField(mxData,1,"type",mxType);
    }

    mxSetField(mxData,1,"complexity",mxCreateDoubleScalar(0));

    {
      mxArray *mxSize = mxCreateDoubleMatrix(1,2,mxREAL);
      double *pr = mxGetPr(mxSize);
      pr[0] = (double)(320);
      pr[1] = (double)(240);
      mxSetField(mxData,2,"size",mxSize);
    }

    {
      const char *typeFields[] = { "base", "fixpt" };

      mxArray *mxType = mxCreateStructMatrix(1,1,2,typeFields);
      mxSetField(mxType,0,"base",mxCreateDoubleScalar(1));
      mxSetField(mxType,0,"fixpt",mxCreateDoubleMatrix(0,0,mxREAL));
      mxSetField(mxData,2,"type",mxType);
    }

    mxSetField(mxData,2,"complexity",mxCreateDoubleScalar(0));

    {
      mxArray *mxSize = mxCreateDoubleMatrix(1,2,mxREAL);
      double *pr = mxGetPr(mxSize);
      pr[0] = (double)(320);
      pr[1] = (double)(240);
      mxSetField(mxData,3,"size",mxSize);
    }

    {
      const char *typeFields[] = { "base", "fixpt" };

      mxArray *mxType = mxCreateStructMatrix(1,1,2,typeFields);
      mxSetField(mxType,0,"base",mxCreateDoubleScalar(1));
      mxSetField(mxType,0,"fixpt",mxCreateDoubleMatrix(0,0,mxREAL));
      mxSetField(mxData,3,"type",mxType);
    }

    mxSetField(mxData,3,"complexity",mxCreateDoubleScalar(0));
    mxSetField(mxAutoinheritanceInfo,0,"inputs",mxData);
  }

  {
    mxSetField(mxAutoinheritanceInfo,0,"parameters",mxCreateDoubleMatrix(0,0,
                mxREAL));
  }

  {
    const char *dataFields[] = { "size", "type", "complexity" };

    mxArray *mxData = mxCreateStructMatrix(1,1,3,dataFields);

    {
      mxArray *mxSize = mxCreateDoubleMatrix(1,2,mxREAL);
      double *pr = mxGetPr(mxSize);
      pr[0] = (double)(1);
      pr[1] = (double)(1);
      mxSetField(mxData,0,"size",mxSize);
    }

    {
      const char *typeFields[] = { "base", "fixpt" };

      mxArray *mxType = mxCreateStructMatrix(1,1,2,typeFields);
      mxSetField(mxType,0,"base",mxCreateDoubleScalar(13));
      mxSetField(mxType,0,"fixpt",mxCreateDoubleMatrix(0,0,mxREAL));
      mxSetField(mxData,0,"type",mxType);
    }

    mxSetField(mxData,0,"complexity",mxCreateDoubleScalar(0));
    mxSetField(mxAutoinheritanceInfo,0,"outputs",mxData);
  }

  {
    mxSetField(mxAutoinheritanceInfo,0,"locals",mxCreateDoubleMatrix(0,0,mxREAL));
  }

  {
    mxArray* mxPostCodegenInfo = sf_c5_ImageDet_get_post_codegen_info();
    mxSetField(mxAutoinheritanceInfo,0,"postCodegenInfo",mxPostCodegenInfo);
  }

  return(mxAutoinheritanceInfo);
}

mxArray *sf_c5_ImageDet_third_party_uses_info(void)
{
  mxArray * mxcell3p = mxCreateCellMatrix(1,0);
  return(mxcell3p);
}

mxArray *sf_c5_ImageDet_jit_fallback_info(void)
{
  const char *infoFields[] = { "fallbackType", "fallbackReason",
    "incompatibleSymbol", };

  mxArray *mxInfo = mxCreateStructMatrix(1, 1, 3, infoFields);
  mxArray *fallbackReason = mxCreateString("feature_off");
  mxArray *incompatibleSymbol = mxCreateString("");
  mxArray *fallbackType = mxCreateString("early");
  mxSetField(mxInfo, 0, infoFields[0], fallbackType);
  mxSetField(mxInfo, 0, infoFields[1], fallbackReason);
  mxSetField(mxInfo, 0, infoFields[2], incompatibleSymbol);
  return mxInfo;
}

mxArray *sf_c5_ImageDet_updateBuildInfo_args_info(void)
{
  mxArray *mxBIArgs = mxCreateCellMatrix(1,0);
  return mxBIArgs;
}

mxArray* sf_c5_ImageDet_get_post_codegen_info(void)
{
  const char* fieldNames[] = { "exportedFunctionsUsedByThisChart",
    "exportedFunctionsChecksum" };

  mwSize dims[2] = { 1, 1 };

  mxArray* mxPostCodegenInfo = mxCreateStructArray(2, dims, sizeof(fieldNames)/
    sizeof(fieldNames[0]), fieldNames);

  {
    mxArray* mxExportedFunctionsChecksum = mxCreateString("");
    mwSize exp_dims[2] = { 0, 1 };

    mxArray* mxExportedFunctionsUsedByThisChart = mxCreateCellArray(2, exp_dims);
    mxSetField(mxPostCodegenInfo, 0, "exportedFunctionsUsedByThisChart",
               mxExportedFunctionsUsedByThisChart);
    mxSetField(mxPostCodegenInfo, 0, "exportedFunctionsChecksum",
               mxExportedFunctionsChecksum);
  }

  return mxPostCodegenInfo;
}

static const mxArray *sf_get_sim_state_info_c5_ImageDet(void)
{
  const char *infoFields[] = { "chartChecksum", "varInfo" };

  mxArray *mxInfo = mxCreateStructMatrix(1, 1, 2, infoFields);
  const char *infoEncStr[] = {
    "100 S1x2'type','srcId','name','auxInfo'{{M[1],M[10],T\"blobs\",},{M[8],M[0],T\"is_active_c5_ImageDet\",}}"
  };

  mxArray *mxVarInfo = sf_mex_decode_encoded_mx_struct_array(infoEncStr, 2, 10);
  mxArray *mxChecksum = mxCreateDoubleMatrix(1, 4, mxREAL);
  sf_c5_ImageDet_get_check_sum(&mxChecksum);
  mxSetField(mxInfo, 0, infoFields[0], mxChecksum);
  mxSetField(mxInfo, 0, infoFields[1], mxVarInfo);
  return mxInfo;
}

static void chart_debug_initialization(SimStruct *S, unsigned int
  fullDebuggerInitialization)
{
  if (!sim_mode_is_rtw_gen(S)) {
    SFc5_ImageDetInstanceStruct *chartInstance;
    ChartRunTimeInfo * crtInfo = (ChartRunTimeInfo *)(ssGetUserData(S));
    ChartInfoStruct * chartInfo = (ChartInfoStruct *)(crtInfo->instanceInfo);
    chartInstance = (SFc5_ImageDetInstanceStruct *) chartInfo->chartInstance;
    if (ssIsFirstInitCond(S) && fullDebuggerInitialization==1) {
      /* do this only if simulation is starting */
      {
        unsigned int chartAlreadyPresent;
        chartAlreadyPresent = sf_debug_initialize_chart
          (sfGlobalDebugInstanceStruct,
           _ImageDetMachineNumber_,
           5,
           1,
           1,
           0,
           5,
           0,
           0,
           0,
           0,
           0,
           &(chartInstance->chartNumber),
           &(chartInstance->instanceNumber),
           (void *)S);

        /* Each instance must initialize its own list of scripts */
        init_script_number_translation(_ImageDetMachineNumber_,
          chartInstance->chartNumber,chartInstance->instanceNumber);
        if (chartAlreadyPresent==0) {
          /* this is the first instance */
          sf_debug_set_chart_disable_implicit_casting
            (sfGlobalDebugInstanceStruct,_ImageDetMachineNumber_,
             chartInstance->chartNumber,1);
          sf_debug_set_chart_event_thresholds(sfGlobalDebugInstanceStruct,
            _ImageDetMachineNumber_,
            chartInstance->chartNumber,
            0,
            0,
            0);
          _SFD_SET_DATA_PROPS(0,1,1,0,"centroids");
          _SFD_SET_DATA_PROPS(1,1,1,0,"area");
          _SFD_SET_DATA_PROPS(2,1,1,0,"RBW");
          _SFD_SET_DATA_PROPS(3,1,1,0,"BBW");
          _SFD_SET_DATA_PROPS(4,2,0,1,"blobs");
          _SFD_STATE_INFO(0,0,2);
          _SFD_CH_SUBSTATE_COUNT(0);
          _SFD_CH_SUBSTATE_DECOMP(0);
        }

        _SFD_CV_INIT_CHART(0,0,0,0);

        {
          _SFD_CV_INIT_STATE(0,0,0,0,0,0,NULL,NULL);
        }

        _SFD_CV_INIT_TRANS(0,0,NULL,NULL,0,NULL);

        /* Initialization of MATLAB Function Model Coverage */
        _SFD_CV_INIT_EML(0,1,1,1,0,1,0,1,0,0,0);
        _SFD_CV_INIT_EML_FCN(0,0,"eML_blk_kernel",0,-1,738);
        _SFD_CV_INIT_EML_SATURATION(0,1,0,705,-1,722);
        _SFD_CV_INIT_EML_IF(0,1,0,539,558,-1,733);
        _SFD_CV_INIT_EML_FOR(0,1,0,369,385,737);
        _SFD_CV_INIT_EML_RELATIONAL(0,1,0,542,558,-1,0);

        {
          unsigned int dimVector[2];
          dimVector[0]= 16;
          dimVector[1]= 2;
          _SFD_SET_DATA_COMPILED_PROPS(0,SF_SINGLE,2,&(dimVector[0]),0,0,0,0.0,
            1.0,0,0,(MexFcnForType)c5_d_sf_marshallOut,(MexInFcnForType)NULL);
        }

        {
          unsigned int dimVector[2];
          dimVector[0]= 16;
          dimVector[1]= 1;
          _SFD_SET_DATA_COMPILED_PROPS(1,SF_INT32,2,&(dimVector[0]),0,0,0,0.0,
            1.0,0,0,(MexFcnForType)c5_c_sf_marshallOut,(MexInFcnForType)NULL);
        }

        {
          unsigned int dimVector[2];
          dimVector[0]= 320;
          dimVector[1]= 240;
          _SFD_SET_DATA_COMPILED_PROPS(2,SF_UINT8,2,&(dimVector[0]),0,0,0,0.0,
            1.0,0,0,(MexFcnForType)c5_b_sf_marshallOut,(MexInFcnForType)NULL);
        }

        {
          unsigned int dimVector[2];
          dimVector[0]= 320;
          dimVector[1]= 240;
          _SFD_SET_DATA_COMPILED_PROPS(3,SF_UINT8,2,&(dimVector[0]),0,0,0,0.0,
            1.0,0,0,(MexFcnForType)c5_b_sf_marshallOut,(MexInFcnForType)NULL);
        }

        _SFD_SET_DATA_COMPILED_PROPS(4,SF_STRUCT,0,NULL,0,0,0,0.0,1.0,0,0,
          (MexFcnForType)c5_blobs_bus_io,(MexInFcnForType)NULL);
        _SFD_SET_DATA_VALUE_PTR_VAR_DIM(0U, *chartInstance->c5_centroids_data,
          (void *)chartInstance->c5_centroids_sizes);
        _SFD_SET_DATA_VALUE_PTR_VAR_DIM(1U, *chartInstance->c5_area_data, (void *)
          chartInstance->c5_area_sizes);
        _SFD_SET_DATA_VALUE_PTR(2U, *chartInstance->c5_RBW);
        _SFD_SET_DATA_VALUE_PTR(3U, *chartInstance->c5_BBW);
        _SFD_SET_DATA_VALUE_PTR(4U, chartInstance->c5_blobs_data);
      }
    } else {
      sf_debug_reset_current_state_configuration(sfGlobalDebugInstanceStruct,
        _ImageDetMachineNumber_,chartInstance->chartNumber,
        chartInstance->instanceNumber);
    }
  }
}

static const char* sf_get_instance_specialization(void)
{
  return "UAjSElHQfBjUmqpqKewW3";
}

static void sf_opaque_initialize_c5_ImageDet(void *chartInstanceVar)
{
  chart_debug_initialization(((SFc5_ImageDetInstanceStruct*) chartInstanceVar)
    ->S,0);
  initialize_params_c5_ImageDet((SFc5_ImageDetInstanceStruct*) chartInstanceVar);
  initialize_c5_ImageDet((SFc5_ImageDetInstanceStruct*) chartInstanceVar);
}

static void sf_opaque_enable_c5_ImageDet(void *chartInstanceVar)
{
  enable_c5_ImageDet((SFc5_ImageDetInstanceStruct*) chartInstanceVar);
}

static void sf_opaque_disable_c5_ImageDet(void *chartInstanceVar)
{
  disable_c5_ImageDet((SFc5_ImageDetInstanceStruct*) chartInstanceVar);
}

static void sf_opaque_gateway_c5_ImageDet(void *chartInstanceVar)
{
  sf_gateway_c5_ImageDet((SFc5_ImageDetInstanceStruct*) chartInstanceVar);
}

static const mxArray* sf_opaque_get_sim_state_c5_ImageDet(SimStruct* S)
{
  ChartRunTimeInfo * crtInfo = (ChartRunTimeInfo *)(ssGetUserData(S));
  ChartInfoStruct * chartInfo = (ChartInfoStruct *)(crtInfo->instanceInfo);
  return get_sim_state_c5_ImageDet((SFc5_ImageDetInstanceStruct*)
    chartInfo->chartInstance);         /* raw sim ctx */
}

static void sf_opaque_set_sim_state_c5_ImageDet(SimStruct* S, const mxArray *st)
{
  ChartRunTimeInfo * crtInfo = (ChartRunTimeInfo *)(ssGetUserData(S));
  ChartInfoStruct * chartInfo = (ChartInfoStruct *)(crtInfo->instanceInfo);
  set_sim_state_c5_ImageDet((SFc5_ImageDetInstanceStruct*)
    chartInfo->chartInstance, st);
}

static void sf_opaque_terminate_c5_ImageDet(void *chartInstanceVar)
{
  if (chartInstanceVar!=NULL) {
    SimStruct *S = ((SFc5_ImageDetInstanceStruct*) chartInstanceVar)->S;
    ChartRunTimeInfo * crtInfo = (ChartRunTimeInfo *)(ssGetUserData(S));
    if (sim_mode_is_rtw_gen(S) || sim_mode_is_external(S)) {
      sf_clear_rtw_identifier(S);
      unload_ImageDet_optimization_info();
    }

    finalize_c5_ImageDet((SFc5_ImageDetInstanceStruct*) chartInstanceVar);
    utFree(chartInstanceVar);
    if (crtInfo != NULL) {
      utFree(crtInfo);
    }

    ssSetUserData(S,NULL);
  }
}

static void sf_opaque_init_subchart_simstructs(void *chartInstanceVar)
{
  initSimStructsc5_ImageDet((SFc5_ImageDetInstanceStruct*) chartInstanceVar);
}

extern unsigned int sf_machine_global_initializer_called(void);
static void mdlProcessParameters_c5_ImageDet(SimStruct *S)
{
  int i;
  for (i=0;i<ssGetNumRunTimeParams(S);i++) {
    if (ssGetSFcnParamTunable(S,i)) {
      ssUpdateDlgParamAsRunTimeParam(S,i);
    }
  }

  if (sf_machine_global_initializer_called()) {
    ChartRunTimeInfo * crtInfo = (ChartRunTimeInfo *)(ssGetUserData(S));
    ChartInfoStruct * chartInfo = (ChartInfoStruct *)(crtInfo->instanceInfo);
    initialize_params_c5_ImageDet((SFc5_ImageDetInstanceStruct*)
      (chartInfo->chartInstance));
  }
}

static void mdlSetWorkWidths_c5_ImageDet(SimStruct *S)
{
  if (sim_mode_is_rtw_gen(S) || sim_mode_is_external(S)) {
    mxArray *infoStruct = load_ImageDet_optimization_info();
    int_T chartIsInlinable =
      (int_T)sf_is_chart_inlinable(sf_get_instance_specialization(),infoStruct,5);
    ssSetStateflowIsInlinable(S,chartIsInlinable);
    ssSetRTWCG(S,sf_rtw_info_uint_prop(sf_get_instance_specialization(),
                infoStruct,5,"RTWCG"));
    ssSetEnableFcnIsTrivial(S,1);
    ssSetDisableFcnIsTrivial(S,1);
    ssSetNotMultipleInlinable(S,sf_rtw_info_uint_prop
      (sf_get_instance_specialization(),infoStruct,5,
       "gatewayCannotBeInlinedMultipleTimes"));
    sf_update_buildInfo(sf_get_instance_specialization(),infoStruct,5);
    if (chartIsInlinable) {
      ssSetInputPortOptimOpts(S, 0, SS_REUSABLE_AND_LOCAL);
      ssSetInputPortOptimOpts(S, 1, SS_REUSABLE_AND_LOCAL);
      ssSetInputPortOptimOpts(S, 2, SS_REUSABLE_AND_LOCAL);
      ssSetInputPortOptimOpts(S, 3, SS_REUSABLE_AND_LOCAL);
      sf_mark_chart_expressionable_inputs(S,sf_get_instance_specialization(),
        infoStruct,5,4);
      sf_mark_chart_reusable_outputs(S,sf_get_instance_specialization(),
        infoStruct,5,1);
    }

    {
      unsigned int outPortIdx;
      for (outPortIdx=1; outPortIdx<=1; ++outPortIdx) {
        ssSetOutputPortOptimizeInIR(S, outPortIdx, 1U);
      }
    }

    {
      unsigned int inPortIdx;
      for (inPortIdx=0; inPortIdx < 4; ++inPortIdx) {
        ssSetInputPortOptimizeInIR(S, inPortIdx, 1U);
      }
    }

    sf_set_rtw_dwork_info(S,sf_get_instance_specialization(),infoStruct,5);
    ssSetHasSubFunctions(S,!(chartIsInlinable));
  } else {
  }

  ssSetOptions(S,ssGetOptions(S)|SS_OPTION_WORKS_WITH_CODE_REUSE);
  ssSetChecksum0(S,(167648982U));
  ssSetChecksum1(S,(1452953919U));
  ssSetChecksum2(S,(2237744448U));
  ssSetChecksum3(S,(816595265U));
  ssSetmdlDerivatives(S, NULL);
  ssSetExplicitFCSSCtrl(S,1);
  ssSupportsMultipleExecInstances(S,1);
}

static void mdlRTW_c5_ImageDet(SimStruct *S)
{
  if (sim_mode_is_rtw_gen(S)) {
    ssWriteRTWStrParam(S, "StateflowChartType", "Embedded MATLAB");
  }
}

static void mdlStart_c5_ImageDet(SimStruct *S)
{
  SFc5_ImageDetInstanceStruct *chartInstance;
  ChartRunTimeInfo * crtInfo = (ChartRunTimeInfo *)utMalloc(sizeof
    (ChartRunTimeInfo));
  chartInstance = (SFc5_ImageDetInstanceStruct *)utMalloc(sizeof
    (SFc5_ImageDetInstanceStruct));
  memset(chartInstance, 0, sizeof(SFc5_ImageDetInstanceStruct));
  if (chartInstance==NULL) {
    sf_mex_error_message("Could not allocate memory for chart instance.");
  }

  chartInstance->chartInfo.chartInstance = chartInstance;
  chartInstance->chartInfo.isEMLChart = 1;
  chartInstance->chartInfo.chartInitialized = 0;
  chartInstance->chartInfo.sFunctionGateway = sf_opaque_gateway_c5_ImageDet;
  chartInstance->chartInfo.initializeChart = sf_opaque_initialize_c5_ImageDet;
  chartInstance->chartInfo.terminateChart = sf_opaque_terminate_c5_ImageDet;
  chartInstance->chartInfo.enableChart = sf_opaque_enable_c5_ImageDet;
  chartInstance->chartInfo.disableChart = sf_opaque_disable_c5_ImageDet;
  chartInstance->chartInfo.getSimState = sf_opaque_get_sim_state_c5_ImageDet;
  chartInstance->chartInfo.setSimState = sf_opaque_set_sim_state_c5_ImageDet;
  chartInstance->chartInfo.getSimStateInfo = sf_get_sim_state_info_c5_ImageDet;
  chartInstance->chartInfo.zeroCrossings = NULL;
  chartInstance->chartInfo.outputs = NULL;
  chartInstance->chartInfo.derivatives = NULL;
  chartInstance->chartInfo.mdlRTW = mdlRTW_c5_ImageDet;
  chartInstance->chartInfo.mdlStart = mdlStart_c5_ImageDet;
  chartInstance->chartInfo.mdlSetWorkWidths = mdlSetWorkWidths_c5_ImageDet;
  chartInstance->chartInfo.extModeExec = NULL;
  chartInstance->chartInfo.restoreLastMajorStepConfiguration = NULL;
  chartInstance->chartInfo.restoreBeforeLastMajorStepConfiguration = NULL;
  chartInstance->chartInfo.storeCurrentConfiguration = NULL;
  chartInstance->chartInfo.callAtomicSubchartUserFcn = NULL;
  chartInstance->chartInfo.callAtomicSubchartAutoFcn = NULL;
  chartInstance->chartInfo.debugInstance = sfGlobalDebugInstanceStruct;
  chartInstance->S = S;
  crtInfo->checksum = SF_RUNTIME_INFO_CHECKSUM;
  crtInfo->instanceInfo = (&(chartInstance->chartInfo));
  crtInfo->isJITEnabled = false;
  crtInfo->compiledInfo = NULL;
  ssSetUserData(S,(void *)(crtInfo));  /* register the chart instance with simstruct */
  init_dsm_address_info(chartInstance);
  init_simulink_io_address(chartInstance);
  if (!sim_mode_is_rtw_gen(S)) {
  }

  sf_opaque_init_subchart_simstructs(chartInstance->chartInfo.chartInstance);
  chart_debug_initialization(S,1);
}

void c5_ImageDet_method_dispatcher(SimStruct *S, int_T method, void *data)
{
  switch (method) {
   case SS_CALL_MDL_START:
    mdlStart_c5_ImageDet(S);
    break;

   case SS_CALL_MDL_SET_WORK_WIDTHS:
    mdlSetWorkWidths_c5_ImageDet(S);
    break;

   case SS_CALL_MDL_PROCESS_PARAMETERS:
    mdlProcessParameters_c5_ImageDet(S);
    break;

   default:
    /* Unhandled method */
    sf_mex_error_message("Stateflow Internal Error:\n"
                         "Error calling c5_ImageDet_method_dispatcher.\n"
                         "Can't handle method %d.\n", method);
    break;
  }
}

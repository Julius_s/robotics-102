#ifndef __c53_LessonI_start_h__
#define __c53_LessonI_start_h__

/* Include files */
#include "sf_runtime/sfc_sf.h"
#include "sf_runtime/sfc_mex.h"
#include "rtwtypes.h"
#include "multiword_types.h"

/* Type Definitions */
#ifndef struct_Ball_tag
#define struct_Ball_tag

struct Ball_tag
{
  int8_T x;
  int8_T y;
  uint8_T valid;
};

#endif                                 /*struct_Ball_tag*/

#ifndef typedef_c53_Ball
#define typedef_c53_Ball

typedef struct Ball_tag c53_Ball;

#endif                                 /*typedef_c53_Ball*/

#ifndef struct_Player_tag
#define struct_Player_tag

struct Player_tag
{
  int8_T x;
  int8_T y;
  int16_T orientation;
  uint8_T color;
  uint8_T position;
  uint8_T valid;
};

#endif                                 /*struct_Player_tag*/

#ifndef typedef_c53_Player
#define typedef_c53_Player

typedef struct Player_tag c53_Player;

#endif                                 /*typedef_c53_Player*/

#include <stddef.h>
#ifndef typedef_SFc53_LessonI_startInstanceStruct
#define typedef_SFc53_LessonI_startInstanceStruct

typedef struct {
  SimStruct *S;
  ChartInfoStruct chartInfo;
  uint32_T chartNumber;
  uint32_T instanceNumber;
  int32_T c53_sfEvent;
  boolean_T c53_isStable;
  boolean_T c53_doneDoubleBufferReInit;
  uint8_T c53_is_active_c53_LessonI_start;
  uint8_T (*c53_message)[31];
  c53_Ball *c53_b_Ball;
  c53_Player (*c53_players)[6];
  uint8_T *c53_gameOn;
} SFc53_LessonI_startInstanceStruct;

#endif                                 /*typedef_SFc53_LessonI_startInstanceStruct*/

/* Named Constants */

/* Variable Declarations */
extern struct SfDebugInstanceStruct *sfGlobalDebugInstanceStruct;

/* Variable Definitions */

/* Function Declarations */
extern const mxArray *sf_c53_LessonI_start_get_eml_resolved_functions_info(void);

/* Function Definitions */
extern void sf_c53_LessonI_start_get_check_sum(mxArray *plhs[]);
extern void c53_LessonI_start_method_dispatcher(SimStruct *S, int_T method, void
  *data);

#endif

#ifndef __c42_LessonII_start_h__
#define __c42_LessonII_start_h__

/* Include files */
#include "sf_runtime/sfc_sf.h"
#include "sf_runtime/sfc_mex.h"
#include "rtwtypes.h"
#include "multiword_types.h"

/* Type Definitions */
#ifndef struct_Player_tag
#define struct_Player_tag

struct Player_tag
{
  int8_T x;
  int8_T y;
  int16_T orientation;
  uint8_T color;
  uint8_T position;
  uint8_T valid;
};

#endif                                 /*struct_Player_tag*/

#ifndef typedef_c42_Player
#define typedef_c42_Player

typedef struct Player_tag c42_Player;

#endif                                 /*typedef_c42_Player*/

#ifndef struct_Ball_tag
#define struct_Ball_tag

struct Ball_tag
{
  int8_T x;
  int8_T y;
  uint8_T valid;
};

#endif                                 /*struct_Ball_tag*/

#ifndef typedef_c42_Ball
#define typedef_c42_Ball

typedef struct Ball_tag c42_Ball;

#endif                                 /*typedef_c42_Ball*/

#ifndef struct_Waypoint_tag
#define struct_Waypoint_tag

struct Waypoint_tag
{
  int8_T x;
  int8_T y;
  int16_T orientation;
};

#endif                                 /*struct_Waypoint_tag*/

#ifndef typedef_c42_Waypoint
#define typedef_c42_Waypoint

typedef struct Waypoint_tag c42_Waypoint;

#endif                                 /*typedef_c42_Waypoint*/

#ifndef typedef_SFc42_LessonII_startInstanceStruct
#define typedef_SFc42_LessonII_startInstanceStruct

typedef struct {
  SimStruct *S;
  ChartInfoStruct chartInfo;
  uint32_T chartNumber;
  uint32_T instanceNumber;
  int32_T c42_sfEvent;
  uint8_T c42_tp_GameIsOn_Offence;
  uint8_T c42_tp_GetToTheBall;
  uint8_T c42_tp_Idle;
  uint8_T c42_tp_Aim;
  uint8_T c42_tp_Kick;
  uint8_T c42_tp_waiting;
  uint8_T c42_tp_Idle1;
  uint8_T c42_tp_goToPosition;
  boolean_T c42_isStable;
  uint8_T c42_is_active_c42_LessonII_start;
  uint8_T c42_is_c42_LessonII_start;
  uint8_T c42_is_GameIsOn_Offence;
  uint8_T c42_is_waiting;
  int8_T c42_startingPos[2];
  c42_Player c42_enemy[3];
  c42_Player c42_friends[3];
  c42_Waypoint c42_shootWay;
  c42_Waypoint c42_kickWay;
  uint8_T c42_temporalCounter_i1;
  boolean_T c42_dataWrittenToVector[7];
  uint8_T c42_doSetSimStateSideEffects;
  const mxArray *c42_setSimStateSideEffectsInfo;
  c42_Player *c42_me;
  c42_Player (*c42_players)[6];
  c42_Ball *c42_ball;
  c42_Waypoint *c42_finalWay;
  c42_Waypoint *c42_manualWay;
  uint8_T *c42_GameOn;
} SFc42_LessonII_startInstanceStruct;

#endif                                 /*typedef_SFc42_LessonII_startInstanceStruct*/

/* Named Constants */

/* Variable Declarations */
extern struct SfDebugInstanceStruct *sfGlobalDebugInstanceStruct;

/* Variable Definitions */

/* Function Declarations */
extern const mxArray *sf_c42_LessonII_start_get_eml_resolved_functions_info(void);

/* Function Definitions */
extern void sf_c42_LessonII_start_get_check_sum(mxArray *plhs[]);
extern void c42_LessonII_start_method_dispatcher(SimStruct *S, int_T method,
  void *data);

#endif

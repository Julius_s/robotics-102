#ifndef __c39_LessonII_start_h__
#define __c39_LessonII_start_h__

/* Include files */
#include "sf_runtime/sfc_sf.h"
#include "sf_runtime/sfc_mex.h"
#include "rtwtypes.h"
#include "multiword_types.h"

/* Type Definitions */
#ifndef struct_sMB5GaXc40ozYzvPFeCX3Q
#define struct_sMB5GaXc40ozYzvPFeCX3Q

struct sMB5GaXc40ozYzvPFeCX3Q
{
  int32_T dummy;
};

#endif                                 /*struct_sMB5GaXc40ozYzvPFeCX3Q*/

#ifndef typedef_c39_coder_internal_cell
#define typedef_c39_coder_internal_cell

typedef struct sMB5GaXc40ozYzvPFeCX3Q c39_coder_internal_cell;

#endif                                 /*typedef_c39_coder_internal_cell*/

#ifndef typedef_SFc39_LessonII_startInstanceStruct
#define typedef_SFc39_LessonII_startInstanceStruct

typedef struct {
  SimStruct *S;
  ChartInfoStruct chartInfo;
  uint32_T chartNumber;
  uint32_T instanceNumber;
  int32_T c39_sfEvent;
  boolean_T c39_isStable;
  boolean_T c39_doneDoubleBufferReInit;
  uint8_T c39_is_active_c39_LessonII_start;
  int16_T *c39_dO;
  real_T *c39_limitFwd;
  uint8_T *c39_certainty;
} SFc39_LessonII_startInstanceStruct;

#endif                                 /*typedef_SFc39_LessonII_startInstanceStruct*/

/* Named Constants */

/* Variable Declarations */
extern struct SfDebugInstanceStruct *sfGlobalDebugInstanceStruct;

/* Variable Definitions */

/* Function Declarations */
extern const mxArray *sf_c39_LessonII_start_get_eml_resolved_functions_info(void);

/* Function Definitions */
extern void sf_c39_LessonII_start_get_check_sum(mxArray *plhs[]);
extern void c39_LessonII_start_method_dispatcher(SimStruct *S, int_T method,
  void *data);

#endif

#ifndef __c8_LessonII_start_h__
#define __c8_LessonII_start_h__

/* Include files */
#include "sf_runtime/sfc_sf.h"
#include "sf_runtime/sfc_mex.h"
#include "rtwtypes.h"
#include "multiword_types.h"

/* Type Definitions */
#ifndef struct_Player_tag
#define struct_Player_tag

struct Player_tag
{
  int8_T x;
  int8_T y;
  int16_T orientation;
  uint8_T color;
  uint8_T position;
  uint8_T valid;
};

#endif                                 /*struct_Player_tag*/

#ifndef typedef_c8_Player
#define typedef_c8_Player

typedef struct Player_tag c8_Player;

#endif                                 /*typedef_c8_Player*/

#ifndef struct_Ball_tag
#define struct_Ball_tag

struct Ball_tag
{
  int8_T x;
  int8_T y;
  uint8_T valid;
};

#endif                                 /*struct_Ball_tag*/

#ifndef typedef_c8_Ball
#define typedef_c8_Ball

typedef struct Ball_tag c8_Ball;

#endif                                 /*typedef_c8_Ball*/

#ifndef struct_PlayerData_tag
#define struct_PlayerData_tag

struct PlayerData_tag
{
  int8_T x[6];
  int8_T y[6];
  int16_T orientation[6];
  uint8_T color[6];
};

#endif                                 /*struct_PlayerData_tag*/

#ifndef typedef_c8_PlayerData
#define typedef_c8_PlayerData

typedef struct PlayerData_tag c8_PlayerData;

#endif                                 /*typedef_c8_PlayerData*/

#ifndef struct_PlayerData_tag_size
#define struct_PlayerData_tag_size

struct PlayerData_tag_size
{
  int32_T x[2];
  int32_T y[2];
  int32_T orientation[2];
  int32_T color[2];
};

#endif                                 /*struct_PlayerData_tag_size*/

#ifndef typedef_c8_PlayerData_size
#define typedef_c8_PlayerData_size

typedef struct PlayerData_tag_size c8_PlayerData_size;

#endif                                 /*typedef_c8_PlayerData_size*/

#ifndef struct_BallData_tag
#define struct_BallData_tag

struct BallData_tag
{
  int8_T x[2];
  int8_T y[2];
};

#endif                                 /*struct_BallData_tag*/

#ifndef typedef_c8_BallData
#define typedef_c8_BallData

typedef struct BallData_tag c8_BallData;

#endif                                 /*typedef_c8_BallData*/

#ifndef struct_BallData_tag_size
#define struct_BallData_tag_size

struct BallData_tag_size
{
  int32_T x;
  int32_T y;
};

#endif                                 /*struct_BallData_tag_size*/

#ifndef typedef_c8_BallData_size
#define typedef_c8_BallData_size

typedef struct BallData_tag_size c8_BallData_size;

#endif                                 /*typedef_c8_BallData_size*/

#ifndef struct_stETj7QHScyDJroYDDeQcXH
#define struct_stETj7QHScyDJroYDDeQcXH

struct stETj7QHScyDJroYDDeQcXH
{
  int8_T x[6];
  int8_T y[6];
  int16_T orientation[6];
  uint8_T color[6];
  uint8_T position[6];
  uint8_T valid[6];
};

#endif                                 /*struct_stETj7QHScyDJroYDDeQcXH*/

#ifndef typedef_c8_stETj7QHScyDJroYDDeQcXH
#define typedef_c8_stETj7QHScyDJroYDDeQcXH

typedef struct stETj7QHScyDJroYDDeQcXH c8_stETj7QHScyDJroYDDeQcXH;

#endif                                 /*typedef_c8_stETj7QHScyDJroYDDeQcXH*/

#ifndef typedef_SFc8_LessonII_startInstanceStruct
#define typedef_SFc8_LessonII_startInstanceStruct

typedef struct {
  SimStruct *S;
  ChartInfoStruct chartInfo;
  uint32_T chartNumber;
  uint32_T instanceNumber;
  int32_T c8_sfEvent;
  boolean_T c8_isStable;
  boolean_T c8_doneDoubleBufferReInit;
  uint8_T c8_is_active_c8_LessonII_start;
  c8_stETj7QHScyDJroYDDeQcXH c8_latestPlayers;
  boolean_T c8_latestPlayers_not_empty;
  c8_Ball c8_latestBall;
  boolean_T c8_latestBall_not_empty;
  c8_PlayerData *c8_playerData_data;
  c8_PlayerData_size *c8_playerData_elems_sizes;
  c8_BallData *c8_BallData_data;
  c8_BallData_size *c8_BallData_elems_sizes;
  c8_Player (*c8_players)[6];
  c8_Ball *c8_ball;
} SFc8_LessonII_startInstanceStruct;

#endif                                 /*typedef_SFc8_LessonII_startInstanceStruct*/

/* Named Constants */

/* Variable Declarations */
extern struct SfDebugInstanceStruct *sfGlobalDebugInstanceStruct;

/* Variable Definitions */

/* Function Declarations */
extern const mxArray *sf_c8_LessonII_start_get_eml_resolved_functions_info(void);

/* Function Definitions */
extern void sf_c8_LessonII_start_get_check_sum(mxArray *plhs[]);
extern void c8_LessonII_start_method_dispatcher(SimStruct *S, int_T method, void
  *data);

#endif

#ifndef __c34_LessonII_start_h__
#define __c34_LessonII_start_h__

/* Include files */
#include "sf_runtime/sfc_sf.h"
#include "sf_runtime/sfc_mex.h"
#include "rtwtypes.h"
#include "multiword_types.h"

/* Type Definitions */
#ifndef typedef_SFc34_LessonII_startInstanceStruct
#define typedef_SFc34_LessonII_startInstanceStruct

typedef struct {
  SimStruct *S;
  ChartInfoStruct chartInfo;
  uint32_T chartNumber;
  uint32_T instanceNumber;
  int32_T c34_sfEvent;
  boolean_T c34_isStable;
  boolean_T c34_doneDoubleBufferReInit;
  uint8_T c34_is_active_c34_LessonII_start;
  uint8_T (*c34_messageBuffer_data)[64];
  int32_T (*c34_messageBuffer_sizes)[2];
  uint8_T (*c34_message)[31];
} SFc34_LessonII_startInstanceStruct;

#endif                                 /*typedef_SFc34_LessonII_startInstanceStruct*/

/* Named Constants */

/* Variable Declarations */
extern struct SfDebugInstanceStruct *sfGlobalDebugInstanceStruct;

/* Variable Definitions */

/* Function Declarations */
extern const mxArray *sf_c34_LessonII_start_get_eml_resolved_functions_info(void);

/* Function Definitions */
extern void sf_c34_LessonII_start_get_check_sum(mxArray *plhs[]);
extern void c34_LessonII_start_method_dispatcher(SimStruct *S, int_T method,
  void *data);

#endif

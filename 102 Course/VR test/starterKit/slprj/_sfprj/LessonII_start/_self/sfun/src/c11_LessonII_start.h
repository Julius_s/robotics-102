#ifndef __c11_LessonII_start_h__
#define __c11_LessonII_start_h__

/* Include files */
#include "sf_runtime/sfc_sf.h"
#include "sf_runtime/sfc_mex.h"
#include "rtwtypes.h"
#include "multiword_types.h"

/* Type Definitions */
#ifndef struct_Waypoint_tag
#define struct_Waypoint_tag

struct Waypoint_tag
{
  int8_T x;
  int8_T y;
  int16_T orientation;
};

#endif                                 /*struct_Waypoint_tag*/

#ifndef typedef_c11_Waypoint
#define typedef_c11_Waypoint

typedef struct Waypoint_tag c11_Waypoint;

#endif                                 /*typedef_c11_Waypoint*/

#ifndef typedef_SFc11_LessonII_startInstanceStruct
#define typedef_SFc11_LessonII_startInstanceStruct

typedef struct {
  SimStruct *S;
  ChartInfoStruct chartInfo;
  uint32_T chartNumber;
  uint32_T instanceNumber;
  int32_T c11_sfEvent;
  boolean_T c11_isStable;
  boolean_T c11_doneDoubleBufferReInit;
  uint8_T c11_is_active_c11_LessonII_start;
  real_T (*c11_array)[3];
  c11_Waypoint *c11_way;
} SFc11_LessonII_startInstanceStruct;

#endif                                 /*typedef_SFc11_LessonII_startInstanceStruct*/

/* Named Constants */

/* Variable Declarations */
extern struct SfDebugInstanceStruct *sfGlobalDebugInstanceStruct;

/* Variable Definitions */

/* Function Declarations */
extern const mxArray *sf_c11_LessonII_start_get_eml_resolved_functions_info(void);

/* Function Definitions */
extern void sf_c11_LessonII_start_get_check_sum(mxArray *plhs[]);
extern void c11_LessonII_start_method_dispatcher(SimStruct *S, int_T method,
  void *data);

#endif

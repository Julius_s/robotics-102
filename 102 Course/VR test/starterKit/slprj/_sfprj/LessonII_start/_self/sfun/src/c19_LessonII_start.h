#ifndef __c19_LessonII_start_h__
#define __c19_LessonII_start_h__

/* Include files */
#include "sf_runtime/sfc_sf.h"
#include "sf_runtime/sfc_mex.h"
#include "rtwtypes.h"
#include "multiword_types.h"

/* Type Definitions */
#ifndef struct_Waypoint_tag
#define struct_Waypoint_tag

struct Waypoint_tag
{
  int8_T x;
  int8_T y;
  int16_T orientation;
};

#endif                                 /*struct_Waypoint_tag*/

#ifndef typedef_c19_Waypoint
#define typedef_c19_Waypoint

typedef struct Waypoint_tag c19_Waypoint;

#endif                                 /*typedef_c19_Waypoint*/

#ifndef typedef_SFc19_LessonII_startInstanceStruct
#define typedef_SFc19_LessonII_startInstanceStruct

typedef struct {
  SimStruct *S;
  ChartInfoStruct chartInfo;
  uint32_T chartNumber;
  uint32_T instanceNumber;
  int32_T c19_sfEvent;
  boolean_T c19_isStable;
  boolean_T c19_doneDoubleBufferReInit;
  uint8_T c19_is_active_c19_LessonII_start;
  real32_T (*c19_data)[3];
  c19_Waypoint *c19_way;
} SFc19_LessonII_startInstanceStruct;

#endif                                 /*typedef_SFc19_LessonII_startInstanceStruct*/

/* Named Constants */

/* Variable Declarations */
extern struct SfDebugInstanceStruct *sfGlobalDebugInstanceStruct;

/* Variable Definitions */

/* Function Declarations */
extern const mxArray *sf_c19_LessonII_start_get_eml_resolved_functions_info(void);

/* Function Definitions */
extern void sf_c19_LessonII_start_get_check_sum(mxArray *plhs[]);
extern void c19_LessonII_start_method_dispatcher(SimStruct *S, int_T method,
  void *data);

#endif

/* Include files */

#include <stddef.h>
#include "blas.h"
#include "LessonII_start_sfun.h"
#include "c11_LessonII_start.h"
#include "mwmathutil.h"
#define CHARTINSTANCE_CHARTNUMBER      (chartInstance->chartNumber)
#define CHARTINSTANCE_INSTANCENUMBER   (chartInstance->instanceNumber)
#include "LessonII_start_sfun_debug_macros.h"
#define _SF_MEX_LISTEN_FOR_CTRL_C(S)   sf_mex_listen_for_ctrl_c(sfGlobalDebugInstanceStruct,S);

/* Type Definitions */

/* Named Constants */
#define CALL_EVENT                     (-1)

/* Variable Declarations */

/* Variable Definitions */
static real_T _sfTime_;
static const char * c11_debug_family_names[4] = { "nargin", "nargout", "array",
  "way" };

/* Function Declarations */
static void initialize_c11_LessonII_start(SFc11_LessonII_startInstanceStruct
  *chartInstance);
static void initialize_params_c11_LessonII_start
  (SFc11_LessonII_startInstanceStruct *chartInstance);
static void enable_c11_LessonII_start(SFc11_LessonII_startInstanceStruct
  *chartInstance);
static void disable_c11_LessonII_start(SFc11_LessonII_startInstanceStruct
  *chartInstance);
static void c11_update_debugger_state_c11_LessonII_start
  (SFc11_LessonII_startInstanceStruct *chartInstance);
static const mxArray *get_sim_state_c11_LessonII_start
  (SFc11_LessonII_startInstanceStruct *chartInstance);
static void set_sim_state_c11_LessonII_start(SFc11_LessonII_startInstanceStruct *
  chartInstance, const mxArray *c11_st);
static void finalize_c11_LessonII_start(SFc11_LessonII_startInstanceStruct
  *chartInstance);
static void sf_gateway_c11_LessonII_start(SFc11_LessonII_startInstanceStruct
  *chartInstance);
static void mdl_start_c11_LessonII_start(SFc11_LessonII_startInstanceStruct
  *chartInstance);
static void initSimStructsc11_LessonII_start(SFc11_LessonII_startInstanceStruct *
  chartInstance);
static void init_script_number_translation(uint32_T c11_machineNumber, uint32_T
  c11_chartNumber, uint32_T c11_instanceNumber);
static const mxArray *c11_sf_marshallOut(void *chartInstanceVoid, void
  *c11_inData);
static c11_Waypoint c11_emlrt_marshallIn(SFc11_LessonII_startInstanceStruct
  *chartInstance, const mxArray *c11_b_way, const char_T *c11_identifier);
static c11_Waypoint c11_b_emlrt_marshallIn(SFc11_LessonII_startInstanceStruct
  *chartInstance, const mxArray *c11_u, const emlrtMsgIdentifier *c11_parentId);
static int8_T c11_c_emlrt_marshallIn(SFc11_LessonII_startInstanceStruct
  *chartInstance, const mxArray *c11_u, const emlrtMsgIdentifier *c11_parentId);
static int16_T c11_d_emlrt_marshallIn(SFc11_LessonII_startInstanceStruct
  *chartInstance, const mxArray *c11_u, const emlrtMsgIdentifier *c11_parentId);
static void c11_sf_marshallIn(void *chartInstanceVoid, const mxArray
  *c11_mxArrayInData, const char_T *c11_varName, void *c11_outData);
static const mxArray *c11_b_sf_marshallOut(void *chartInstanceVoid, void
  *c11_inData);
static const mxArray *c11_c_sf_marshallOut(void *chartInstanceVoid, void
  *c11_inData);
static real_T c11_e_emlrt_marshallIn(SFc11_LessonII_startInstanceStruct
  *chartInstance, const mxArray *c11_u, const emlrtMsgIdentifier *c11_parentId);
static void c11_b_sf_marshallIn(void *chartInstanceVoid, const mxArray
  *c11_mxArrayInData, const char_T *c11_varName, void *c11_outData);
static const mxArray *c11_d_sf_marshallOut(void *chartInstanceVoid, void
  *c11_inData);
static int32_T c11_f_emlrt_marshallIn(SFc11_LessonII_startInstanceStruct
  *chartInstance, const mxArray *c11_u, const emlrtMsgIdentifier *c11_parentId);
static void c11_c_sf_marshallIn(void *chartInstanceVoid, const mxArray
  *c11_mxArrayInData, const char_T *c11_varName, void *c11_outData);
static const mxArray *c11_way_bus_io(void *chartInstanceVoid, void *c11_pData);
static uint8_T c11_g_emlrt_marshallIn(SFc11_LessonII_startInstanceStruct
  *chartInstance, const mxArray *c11_b_is_active_c11_LessonII_start, const
  char_T *c11_identifier);
static uint8_T c11_h_emlrt_marshallIn(SFc11_LessonII_startInstanceStruct
  *chartInstance, const mxArray *c11_u, const emlrtMsgIdentifier *c11_parentId);
static void init_dsm_address_info(SFc11_LessonII_startInstanceStruct
  *chartInstance);
static void init_simulink_io_address(SFc11_LessonII_startInstanceStruct
  *chartInstance);

/* Function Definitions */
static void initialize_c11_LessonII_start(SFc11_LessonII_startInstanceStruct
  *chartInstance)
{
  chartInstance->c11_sfEvent = CALL_EVENT;
  _sfTime_ = sf_get_time(chartInstance->S);
  chartInstance->c11_is_active_c11_LessonII_start = 0U;
}

static void initialize_params_c11_LessonII_start
  (SFc11_LessonII_startInstanceStruct *chartInstance)
{
  (void)chartInstance;
}

static void enable_c11_LessonII_start(SFc11_LessonII_startInstanceStruct
  *chartInstance)
{
  _sfTime_ = sf_get_time(chartInstance->S);
}

static void disable_c11_LessonII_start(SFc11_LessonII_startInstanceStruct
  *chartInstance)
{
  _sfTime_ = sf_get_time(chartInstance->S);
}

static void c11_update_debugger_state_c11_LessonII_start
  (SFc11_LessonII_startInstanceStruct *chartInstance)
{
  (void)chartInstance;
}

static const mxArray *get_sim_state_c11_LessonII_start
  (SFc11_LessonII_startInstanceStruct *chartInstance)
{
  const mxArray *c11_st;
  const mxArray *c11_y = NULL;
  const mxArray *c11_b_y = NULL;
  int8_T c11_u;
  const mxArray *c11_c_y = NULL;
  int8_T c11_b_u;
  const mxArray *c11_d_y = NULL;
  int16_T c11_c_u;
  const mxArray *c11_e_y = NULL;
  uint8_T c11_hoistedGlobal;
  uint8_T c11_d_u;
  const mxArray *c11_f_y = NULL;
  c11_st = NULL;
  c11_st = NULL;
  c11_y = NULL;
  sf_mex_assign(&c11_y, sf_mex_createcellmatrix(2, 1), false);
  c11_b_y = NULL;
  sf_mex_assign(&c11_b_y, sf_mex_createstruct("structure", 2, 1, 1), false);
  c11_u = *(int8_T *)&((char_T *)chartInstance->c11_way)[0];
  c11_c_y = NULL;
  sf_mex_assign(&c11_c_y, sf_mex_create("y", &c11_u, 2, 0U, 0U, 0U, 0), false);
  sf_mex_addfield(c11_b_y, c11_c_y, "x", "x", 0);
  c11_b_u = *(int8_T *)&((char_T *)chartInstance->c11_way)[1];
  c11_d_y = NULL;
  sf_mex_assign(&c11_d_y, sf_mex_create("y", &c11_b_u, 2, 0U, 0U, 0U, 0), false);
  sf_mex_addfield(c11_b_y, c11_d_y, "y", "y", 0);
  c11_c_u = *(int16_T *)&((char_T *)chartInstance->c11_way)[2];
  c11_e_y = NULL;
  sf_mex_assign(&c11_e_y, sf_mex_create("y", &c11_c_u, 4, 0U, 0U, 0U, 0), false);
  sf_mex_addfield(c11_b_y, c11_e_y, "orientation", "orientation", 0);
  sf_mex_setcell(c11_y, 0, c11_b_y);
  c11_hoistedGlobal = chartInstance->c11_is_active_c11_LessonII_start;
  c11_d_u = c11_hoistedGlobal;
  c11_f_y = NULL;
  sf_mex_assign(&c11_f_y, sf_mex_create("y", &c11_d_u, 3, 0U, 0U, 0U, 0), false);
  sf_mex_setcell(c11_y, 1, c11_f_y);
  sf_mex_assign(&c11_st, c11_y, false);
  return c11_st;
}

static void set_sim_state_c11_LessonII_start(SFc11_LessonII_startInstanceStruct *
  chartInstance, const mxArray *c11_st)
{
  const mxArray *c11_u;
  c11_Waypoint c11_r0;
  chartInstance->c11_doneDoubleBufferReInit = true;
  c11_u = sf_mex_dup(c11_st);
  c11_r0 = c11_emlrt_marshallIn(chartInstance, sf_mex_dup(sf_mex_getcell(c11_u,
    0)), "way");
  *(int8_T *)&((char_T *)chartInstance->c11_way)[0] = c11_r0.x;
  *(int8_T *)&((char_T *)chartInstance->c11_way)[1] = c11_r0.y;
  *(int16_T *)&((char_T *)chartInstance->c11_way)[2] = c11_r0.orientation;
  chartInstance->c11_is_active_c11_LessonII_start = c11_g_emlrt_marshallIn
    (chartInstance, sf_mex_dup(sf_mex_getcell(c11_u, 1)),
     "is_active_c11_LessonII_start");
  sf_mex_destroy(&c11_u);
  c11_update_debugger_state_c11_LessonII_start(chartInstance);
  sf_mex_destroy(&c11_st);
}

static void finalize_c11_LessonII_start(SFc11_LessonII_startInstanceStruct
  *chartInstance)
{
  (void)chartInstance;
}

static void sf_gateway_c11_LessonII_start(SFc11_LessonII_startInstanceStruct
  *chartInstance)
{
  int32_T c11_i0;
  int32_T c11_i1;
  real_T c11_b_array[3];
  uint32_T c11_debug_family_var_map[4];
  real_T c11_nargin = 1.0;
  real_T c11_nargout = 1.0;
  c11_Waypoint c11_b_way;
  real_T c11_d0;
  int8_T c11_i2;
  real_T c11_d1;
  int8_T c11_i3;
  real_T c11_d2;
  int16_T c11_i4;
  _SFD_SYMBOL_SCOPE_PUSH(0U, 0U);
  _sfTime_ = sf_get_time(chartInstance->S);
  _SFD_CC_CALL(CHART_ENTER_SFUNCTION_TAG, 8U, chartInstance->c11_sfEvent);
  for (c11_i0 = 0; c11_i0 < 3; c11_i0++) {
    _SFD_DATA_RANGE_CHECK((*chartInstance->c11_array)[c11_i0], 0U);
  }

  chartInstance->c11_sfEvent = CALL_EVENT;
  _SFD_CC_CALL(CHART_ENTER_DURING_FUNCTION_TAG, 8U, chartInstance->c11_sfEvent);
  for (c11_i1 = 0; c11_i1 < 3; c11_i1++) {
    c11_b_array[c11_i1] = (*chartInstance->c11_array)[c11_i1];
  }

  _SFD_SYMBOL_SCOPE_PUSH_EML(0U, 4U, 4U, c11_debug_family_names,
    c11_debug_family_var_map);
  _SFD_SYMBOL_SCOPE_ADD_EML_IMPORTABLE(&c11_nargin, 0U, c11_c_sf_marshallOut,
    c11_b_sf_marshallIn);
  _SFD_SYMBOL_SCOPE_ADD_EML_IMPORTABLE(&c11_nargout, 1U, c11_c_sf_marshallOut,
    c11_b_sf_marshallIn);
  _SFD_SYMBOL_SCOPE_ADD_EML(c11_b_array, 2U, c11_b_sf_marshallOut);
  _SFD_SYMBOL_SCOPE_ADD_EML_IMPORTABLE(&c11_b_way, 3U, c11_sf_marshallOut,
    c11_sf_marshallIn);
  CV_EML_FCN(0, 0);
  _SFD_EML_CALL(0U, chartInstance->c11_sfEvent, 2);
  c11_d0 = muDoubleScalarRound(c11_b_array[0]);
  if (c11_d0 < 128.0) {
    if (CV_SATURATION_EVAL(4, 0, 0, 1, c11_d0 >= -128.0)) {
      c11_i2 = (int8_T)c11_d0;
    } else {
      c11_i2 = MIN_int8_T;
    }
  } else if (CV_SATURATION_EVAL(4, 0, 0, 0, c11_d0 >= 128.0)) {
    c11_i2 = MAX_int8_T;
  } else {
    c11_i2 = 0;
  }

  c11_b_way.x = c11_i2;
  _SFD_EML_CALL(0U, chartInstance->c11_sfEvent, 3);
  c11_d1 = muDoubleScalarRound(c11_b_array[1]);
  if (c11_d1 < 128.0) {
    if (CV_SATURATION_EVAL(4, 0, 1, 1, c11_d1 >= -128.0)) {
      c11_i3 = (int8_T)c11_d1;
    } else {
      c11_i3 = MIN_int8_T;
    }
  } else if (CV_SATURATION_EVAL(4, 0, 1, 0, c11_d1 >= 128.0)) {
    c11_i3 = MAX_int8_T;
  } else {
    c11_i3 = 0;
  }

  c11_b_way.y = c11_i3;
  _SFD_EML_CALL(0U, chartInstance->c11_sfEvent, 4);
  c11_d2 = muDoubleScalarRound(c11_b_array[2]);
  if (c11_d2 < 32768.0) {
    if (CV_SATURATION_EVAL(4, 0, 2, 1, c11_d2 >= -32768.0)) {
      c11_i4 = (int16_T)c11_d2;
    } else {
      c11_i4 = MIN_int16_T;
    }
  } else if (CV_SATURATION_EVAL(4, 0, 2, 0, c11_d2 >= 32768.0)) {
    c11_i4 = MAX_int16_T;
  } else {
    c11_i4 = 0;
  }

  c11_b_way.orientation = c11_i4;
  _SFD_EML_CALL(0U, chartInstance->c11_sfEvent, -4);
  _SFD_SYMBOL_SCOPE_POP();
  *(int8_T *)&((char_T *)chartInstance->c11_way)[0] = c11_b_way.x;
  *(int8_T *)&((char_T *)chartInstance->c11_way)[1] = c11_b_way.y;
  *(int16_T *)&((char_T *)chartInstance->c11_way)[2] = c11_b_way.orientation;
  _SFD_CC_CALL(EXIT_OUT_OF_FUNCTION_TAG, 8U, chartInstance->c11_sfEvent);
  _SFD_SYMBOL_SCOPE_POP();
  _SFD_CHECK_FOR_STATE_INCONSISTENCY(_LessonII_startMachineNumber_,
    chartInstance->chartNumber, chartInstance->instanceNumber);
}

static void mdl_start_c11_LessonII_start(SFc11_LessonII_startInstanceStruct
  *chartInstance)
{
  (void)chartInstance;
}

static void initSimStructsc11_LessonII_start(SFc11_LessonII_startInstanceStruct *
  chartInstance)
{
  (void)chartInstance;
}

static void init_script_number_translation(uint32_T c11_machineNumber, uint32_T
  c11_chartNumber, uint32_T c11_instanceNumber)
{
  (void)c11_machineNumber;
  (void)c11_chartNumber;
  (void)c11_instanceNumber;
}

static const mxArray *c11_sf_marshallOut(void *chartInstanceVoid, void
  *c11_inData)
{
  const mxArray *c11_mxArrayOutData = NULL;
  c11_Waypoint c11_u;
  const mxArray *c11_y = NULL;
  int8_T c11_b_u;
  const mxArray *c11_b_y = NULL;
  int8_T c11_c_u;
  const mxArray *c11_c_y = NULL;
  int16_T c11_d_u;
  const mxArray *c11_d_y = NULL;
  SFc11_LessonII_startInstanceStruct *chartInstance;
  chartInstance = (SFc11_LessonII_startInstanceStruct *)chartInstanceVoid;
  c11_mxArrayOutData = NULL;
  c11_u = *(c11_Waypoint *)c11_inData;
  c11_y = NULL;
  sf_mex_assign(&c11_y, sf_mex_createstruct("structure", 2, 1, 1), false);
  c11_b_u = c11_u.x;
  c11_b_y = NULL;
  sf_mex_assign(&c11_b_y, sf_mex_create("y", &c11_b_u, 2, 0U, 0U, 0U, 0), false);
  sf_mex_addfield(c11_y, c11_b_y, "x", "x", 0);
  c11_c_u = c11_u.y;
  c11_c_y = NULL;
  sf_mex_assign(&c11_c_y, sf_mex_create("y", &c11_c_u, 2, 0U, 0U, 0U, 0), false);
  sf_mex_addfield(c11_y, c11_c_y, "y", "y", 0);
  c11_d_u = c11_u.orientation;
  c11_d_y = NULL;
  sf_mex_assign(&c11_d_y, sf_mex_create("y", &c11_d_u, 4, 0U, 0U, 0U, 0), false);
  sf_mex_addfield(c11_y, c11_d_y, "orientation", "orientation", 0);
  sf_mex_assign(&c11_mxArrayOutData, c11_y, false);
  return c11_mxArrayOutData;
}

static c11_Waypoint c11_emlrt_marshallIn(SFc11_LessonII_startInstanceStruct
  *chartInstance, const mxArray *c11_b_way, const char_T *c11_identifier)
{
  c11_Waypoint c11_y;
  emlrtMsgIdentifier c11_thisId;
  c11_thisId.fIdentifier = c11_identifier;
  c11_thisId.fParent = NULL;
  c11_y = c11_b_emlrt_marshallIn(chartInstance, sf_mex_dup(c11_b_way),
    &c11_thisId);
  sf_mex_destroy(&c11_b_way);
  return c11_y;
}

static c11_Waypoint c11_b_emlrt_marshallIn(SFc11_LessonII_startInstanceStruct
  *chartInstance, const mxArray *c11_u, const emlrtMsgIdentifier *c11_parentId)
{
  c11_Waypoint c11_y;
  emlrtMsgIdentifier c11_thisId;
  static const char * c11_fieldNames[3] = { "x", "y", "orientation" };

  c11_thisId.fParent = c11_parentId;
  sf_mex_check_struct(c11_parentId, c11_u, 3, c11_fieldNames, 0U, NULL);
  c11_thisId.fIdentifier = "x";
  c11_y.x = c11_c_emlrt_marshallIn(chartInstance, sf_mex_dup(sf_mex_getfield
    (c11_u, "x", "x", 0)), &c11_thisId);
  c11_thisId.fIdentifier = "y";
  c11_y.y = c11_c_emlrt_marshallIn(chartInstance, sf_mex_dup(sf_mex_getfield
    (c11_u, "y", "y", 0)), &c11_thisId);
  c11_thisId.fIdentifier = "orientation";
  c11_y.orientation = c11_d_emlrt_marshallIn(chartInstance, sf_mex_dup
    (sf_mex_getfield(c11_u, "orientation", "orientation", 0)), &c11_thisId);
  sf_mex_destroy(&c11_u);
  return c11_y;
}

static int8_T c11_c_emlrt_marshallIn(SFc11_LessonII_startInstanceStruct
  *chartInstance, const mxArray *c11_u, const emlrtMsgIdentifier *c11_parentId)
{
  int8_T c11_y;
  int8_T c11_i5;
  (void)chartInstance;
  sf_mex_import(c11_parentId, sf_mex_dup(c11_u), &c11_i5, 1, 2, 0U, 0, 0U, 0);
  c11_y = c11_i5;
  sf_mex_destroy(&c11_u);
  return c11_y;
}

static int16_T c11_d_emlrt_marshallIn(SFc11_LessonII_startInstanceStruct
  *chartInstance, const mxArray *c11_u, const emlrtMsgIdentifier *c11_parentId)
{
  int16_T c11_y;
  int16_T c11_i6;
  (void)chartInstance;
  sf_mex_import(c11_parentId, sf_mex_dup(c11_u), &c11_i6, 1, 4, 0U, 0, 0U, 0);
  c11_y = c11_i6;
  sf_mex_destroy(&c11_u);
  return c11_y;
}

static void c11_sf_marshallIn(void *chartInstanceVoid, const mxArray
  *c11_mxArrayInData, const char_T *c11_varName, void *c11_outData)
{
  const mxArray *c11_b_way;
  const char_T *c11_identifier;
  emlrtMsgIdentifier c11_thisId;
  c11_Waypoint c11_y;
  SFc11_LessonII_startInstanceStruct *chartInstance;
  chartInstance = (SFc11_LessonII_startInstanceStruct *)chartInstanceVoid;
  c11_b_way = sf_mex_dup(c11_mxArrayInData);
  c11_identifier = c11_varName;
  c11_thisId.fIdentifier = c11_identifier;
  c11_thisId.fParent = NULL;
  c11_y = c11_b_emlrt_marshallIn(chartInstance, sf_mex_dup(c11_b_way),
    &c11_thisId);
  sf_mex_destroy(&c11_b_way);
  *(c11_Waypoint *)c11_outData = c11_y;
  sf_mex_destroy(&c11_mxArrayInData);
}

static const mxArray *c11_b_sf_marshallOut(void *chartInstanceVoid, void
  *c11_inData)
{
  const mxArray *c11_mxArrayOutData = NULL;
  int32_T c11_i7;
  real_T c11_b_inData[3];
  int32_T c11_i8;
  real_T c11_u[3];
  const mxArray *c11_y = NULL;
  SFc11_LessonII_startInstanceStruct *chartInstance;
  chartInstance = (SFc11_LessonII_startInstanceStruct *)chartInstanceVoid;
  c11_mxArrayOutData = NULL;
  for (c11_i7 = 0; c11_i7 < 3; c11_i7++) {
    c11_b_inData[c11_i7] = (*(real_T (*)[3])c11_inData)[c11_i7];
  }

  for (c11_i8 = 0; c11_i8 < 3; c11_i8++) {
    c11_u[c11_i8] = c11_b_inData[c11_i8];
  }

  c11_y = NULL;
  sf_mex_assign(&c11_y, sf_mex_create("y", c11_u, 0, 0U, 1U, 0U, 1, 3), false);
  sf_mex_assign(&c11_mxArrayOutData, c11_y, false);
  return c11_mxArrayOutData;
}

static const mxArray *c11_c_sf_marshallOut(void *chartInstanceVoid, void
  *c11_inData)
{
  const mxArray *c11_mxArrayOutData = NULL;
  real_T c11_u;
  const mxArray *c11_y = NULL;
  SFc11_LessonII_startInstanceStruct *chartInstance;
  chartInstance = (SFc11_LessonII_startInstanceStruct *)chartInstanceVoid;
  c11_mxArrayOutData = NULL;
  c11_u = *(real_T *)c11_inData;
  c11_y = NULL;
  sf_mex_assign(&c11_y, sf_mex_create("y", &c11_u, 0, 0U, 0U, 0U, 0), false);
  sf_mex_assign(&c11_mxArrayOutData, c11_y, false);
  return c11_mxArrayOutData;
}

static real_T c11_e_emlrt_marshallIn(SFc11_LessonII_startInstanceStruct
  *chartInstance, const mxArray *c11_u, const emlrtMsgIdentifier *c11_parentId)
{
  real_T c11_y;
  real_T c11_d3;
  (void)chartInstance;
  sf_mex_import(c11_parentId, sf_mex_dup(c11_u), &c11_d3, 1, 0, 0U, 0, 0U, 0);
  c11_y = c11_d3;
  sf_mex_destroy(&c11_u);
  return c11_y;
}

static void c11_b_sf_marshallIn(void *chartInstanceVoid, const mxArray
  *c11_mxArrayInData, const char_T *c11_varName, void *c11_outData)
{
  const mxArray *c11_nargout;
  const char_T *c11_identifier;
  emlrtMsgIdentifier c11_thisId;
  real_T c11_y;
  SFc11_LessonII_startInstanceStruct *chartInstance;
  chartInstance = (SFc11_LessonII_startInstanceStruct *)chartInstanceVoid;
  c11_nargout = sf_mex_dup(c11_mxArrayInData);
  c11_identifier = c11_varName;
  c11_thisId.fIdentifier = c11_identifier;
  c11_thisId.fParent = NULL;
  c11_y = c11_e_emlrt_marshallIn(chartInstance, sf_mex_dup(c11_nargout),
    &c11_thisId);
  sf_mex_destroy(&c11_nargout);
  *(real_T *)c11_outData = c11_y;
  sf_mex_destroy(&c11_mxArrayInData);
}

const mxArray *sf_c11_LessonII_start_get_eml_resolved_functions_info(void)
{
  const mxArray *c11_nameCaptureInfo = NULL;
  c11_nameCaptureInfo = NULL;
  sf_mex_assign(&c11_nameCaptureInfo, sf_mex_create("nameCaptureInfo", NULL, 0,
    0U, 1U, 0U, 2, 0, 1), false);
  return c11_nameCaptureInfo;
}

static const mxArray *c11_d_sf_marshallOut(void *chartInstanceVoid, void
  *c11_inData)
{
  const mxArray *c11_mxArrayOutData = NULL;
  int32_T c11_u;
  const mxArray *c11_y = NULL;
  SFc11_LessonII_startInstanceStruct *chartInstance;
  chartInstance = (SFc11_LessonII_startInstanceStruct *)chartInstanceVoid;
  c11_mxArrayOutData = NULL;
  c11_u = *(int32_T *)c11_inData;
  c11_y = NULL;
  sf_mex_assign(&c11_y, sf_mex_create("y", &c11_u, 6, 0U, 0U, 0U, 0), false);
  sf_mex_assign(&c11_mxArrayOutData, c11_y, false);
  return c11_mxArrayOutData;
}

static int32_T c11_f_emlrt_marshallIn(SFc11_LessonII_startInstanceStruct
  *chartInstance, const mxArray *c11_u, const emlrtMsgIdentifier *c11_parentId)
{
  int32_T c11_y;
  int32_T c11_i9;
  (void)chartInstance;
  sf_mex_import(c11_parentId, sf_mex_dup(c11_u), &c11_i9, 1, 6, 0U, 0, 0U, 0);
  c11_y = c11_i9;
  sf_mex_destroy(&c11_u);
  return c11_y;
}

static void c11_c_sf_marshallIn(void *chartInstanceVoid, const mxArray
  *c11_mxArrayInData, const char_T *c11_varName, void *c11_outData)
{
  const mxArray *c11_b_sfEvent;
  const char_T *c11_identifier;
  emlrtMsgIdentifier c11_thisId;
  int32_T c11_y;
  SFc11_LessonII_startInstanceStruct *chartInstance;
  chartInstance = (SFc11_LessonII_startInstanceStruct *)chartInstanceVoid;
  c11_b_sfEvent = sf_mex_dup(c11_mxArrayInData);
  c11_identifier = c11_varName;
  c11_thisId.fIdentifier = c11_identifier;
  c11_thisId.fParent = NULL;
  c11_y = c11_f_emlrt_marshallIn(chartInstance, sf_mex_dup(c11_b_sfEvent),
    &c11_thisId);
  sf_mex_destroy(&c11_b_sfEvent);
  *(int32_T *)c11_outData = c11_y;
  sf_mex_destroy(&c11_mxArrayInData);
}

static const mxArray *c11_way_bus_io(void *chartInstanceVoid, void *c11_pData)
{
  const mxArray *c11_mxVal = NULL;
  c11_Waypoint c11_tmp;
  SFc11_LessonII_startInstanceStruct *chartInstance;
  chartInstance = (SFc11_LessonII_startInstanceStruct *)chartInstanceVoid;
  c11_mxVal = NULL;
  c11_tmp.x = *(int8_T *)&((char_T *)(c11_Waypoint *)c11_pData)[0];
  c11_tmp.y = *(int8_T *)&((char_T *)(c11_Waypoint *)c11_pData)[1];
  c11_tmp.orientation = *(int16_T *)&((char_T *)(c11_Waypoint *)c11_pData)[2];
  sf_mex_assign(&c11_mxVal, c11_sf_marshallOut(chartInstance, &c11_tmp), false);
  return c11_mxVal;
}

static uint8_T c11_g_emlrt_marshallIn(SFc11_LessonII_startInstanceStruct
  *chartInstance, const mxArray *c11_b_is_active_c11_LessonII_start, const
  char_T *c11_identifier)
{
  uint8_T c11_y;
  emlrtMsgIdentifier c11_thisId;
  c11_thisId.fIdentifier = c11_identifier;
  c11_thisId.fParent = NULL;
  c11_y = c11_h_emlrt_marshallIn(chartInstance, sf_mex_dup
    (c11_b_is_active_c11_LessonII_start), &c11_thisId);
  sf_mex_destroy(&c11_b_is_active_c11_LessonII_start);
  return c11_y;
}

static uint8_T c11_h_emlrt_marshallIn(SFc11_LessonII_startInstanceStruct
  *chartInstance, const mxArray *c11_u, const emlrtMsgIdentifier *c11_parentId)
{
  uint8_T c11_y;
  uint8_T c11_u0;
  (void)chartInstance;
  sf_mex_import(c11_parentId, sf_mex_dup(c11_u), &c11_u0, 1, 3, 0U, 0, 0U, 0);
  c11_y = c11_u0;
  sf_mex_destroy(&c11_u);
  return c11_y;
}

static void init_dsm_address_info(SFc11_LessonII_startInstanceStruct
  *chartInstance)
{
  (void)chartInstance;
}

static void init_simulink_io_address(SFc11_LessonII_startInstanceStruct
  *chartInstance)
{
  chartInstance->c11_array = (real_T (*)[3])ssGetInputPortSignal_wrapper
    (chartInstance->S, 0);
  chartInstance->c11_way = (c11_Waypoint *)ssGetOutputPortSignal_wrapper
    (chartInstance->S, 1);
}

/* SFunction Glue Code */
#ifdef utFree
#undef utFree
#endif

#ifdef utMalloc
#undef utMalloc
#endif

#ifdef __cplusplus

extern "C" void *utMalloc(size_t size);
extern "C" void utFree(void*);

#else

extern void *utMalloc(size_t size);
extern void utFree(void*);

#endif

void sf_c11_LessonII_start_get_check_sum(mxArray *plhs[])
{
  ((real_T *)mxGetPr((plhs[0])))[0] = (real_T)(1433238692U);
  ((real_T *)mxGetPr((plhs[0])))[1] = (real_T)(882519685U);
  ((real_T *)mxGetPr((plhs[0])))[2] = (real_T)(2237019287U);
  ((real_T *)mxGetPr((plhs[0])))[3] = (real_T)(365560715U);
}

mxArray* sf_c11_LessonII_start_get_post_codegen_info(void);
mxArray *sf_c11_LessonII_start_get_autoinheritance_info(void)
{
  const char *autoinheritanceFields[] = { "checksum", "inputs", "parameters",
    "outputs", "locals", "postCodegenInfo" };

  mxArray *mxAutoinheritanceInfo = mxCreateStructMatrix(1, 1, sizeof
    (autoinheritanceFields)/sizeof(autoinheritanceFields[0]),
    autoinheritanceFields);

  {
    mxArray *mxChecksum = mxCreateString("hL4tQKeUdydUfBZ3vvhj3C");
    mxSetField(mxAutoinheritanceInfo,0,"checksum",mxChecksum);
  }

  {
    const char *dataFields[] = { "size", "type", "complexity" };

    mxArray *mxData = mxCreateStructMatrix(1,1,3,dataFields);

    {
      mxArray *mxSize = mxCreateDoubleMatrix(1,2,mxREAL);
      double *pr = mxGetPr(mxSize);
      pr[0] = (double)(3);
      pr[1] = (double)(1);
      mxSetField(mxData,0,"size",mxSize);
    }

    {
      const char *typeFields[] = { "base", "fixpt" };

      mxArray *mxType = mxCreateStructMatrix(1,1,2,typeFields);
      mxSetField(mxType,0,"base",mxCreateDoubleScalar(10));
      mxSetField(mxType,0,"fixpt",mxCreateDoubleMatrix(0,0,mxREAL));
      mxSetField(mxData,0,"type",mxType);
    }

    mxSetField(mxData,0,"complexity",mxCreateDoubleScalar(0));
    mxSetField(mxAutoinheritanceInfo,0,"inputs",mxData);
  }

  {
    mxSetField(mxAutoinheritanceInfo,0,"parameters",mxCreateDoubleMatrix(0,0,
                mxREAL));
  }

  {
    const char *dataFields[] = { "size", "type", "complexity" };

    mxArray *mxData = mxCreateStructMatrix(1,1,3,dataFields);

    {
      mxArray *mxSize = mxCreateDoubleMatrix(1,2,mxREAL);
      double *pr = mxGetPr(mxSize);
      pr[0] = (double)(1);
      pr[1] = (double)(1);
      mxSetField(mxData,0,"size",mxSize);
    }

    {
      const char *typeFields[] = { "base", "fixpt" };

      mxArray *mxType = mxCreateStructMatrix(1,1,2,typeFields);
      mxSetField(mxType,0,"base",mxCreateDoubleScalar(13));
      mxSetField(mxType,0,"fixpt",mxCreateDoubleMatrix(0,0,mxREAL));
      mxSetField(mxData,0,"type",mxType);
    }

    mxSetField(mxData,0,"complexity",mxCreateDoubleScalar(0));
    mxSetField(mxAutoinheritanceInfo,0,"outputs",mxData);
  }

  {
    mxSetField(mxAutoinheritanceInfo,0,"locals",mxCreateDoubleMatrix(0,0,mxREAL));
  }

  {
    mxArray* mxPostCodegenInfo = sf_c11_LessonII_start_get_post_codegen_info();
    mxSetField(mxAutoinheritanceInfo,0,"postCodegenInfo",mxPostCodegenInfo);
  }

  return(mxAutoinheritanceInfo);
}

mxArray *sf_c11_LessonII_start_third_party_uses_info(void)
{
  mxArray * mxcell3p = mxCreateCellMatrix(1,0);
  return(mxcell3p);
}

mxArray *sf_c11_LessonII_start_jit_fallback_info(void)
{
  const char *infoFields[] = { "fallbackType", "fallbackReason",
    "incompatibleSymbol", };

  mxArray *mxInfo = mxCreateStructMatrix(1, 1, 3, infoFields);
  mxArray *fallbackReason = mxCreateString("feature_off");
  mxArray *incompatibleSymbol = mxCreateString("");
  mxArray *fallbackType = mxCreateString("early");
  mxSetField(mxInfo, 0, infoFields[0], fallbackType);
  mxSetField(mxInfo, 0, infoFields[1], fallbackReason);
  mxSetField(mxInfo, 0, infoFields[2], incompatibleSymbol);
  return mxInfo;
}

mxArray *sf_c11_LessonII_start_updateBuildInfo_args_info(void)
{
  mxArray *mxBIArgs = mxCreateCellMatrix(1,0);
  return mxBIArgs;
}

mxArray* sf_c11_LessonII_start_get_post_codegen_info(void)
{
  const char* fieldNames[] = { "exportedFunctionsUsedByThisChart",
    "exportedFunctionsChecksum" };

  mwSize dims[2] = { 1, 1 };

  mxArray* mxPostCodegenInfo = mxCreateStructArray(2, dims, sizeof(fieldNames)/
    sizeof(fieldNames[0]), fieldNames);

  {
    mxArray* mxExportedFunctionsChecksum = mxCreateString("");
    mwSize exp_dims[2] = { 0, 1 };

    mxArray* mxExportedFunctionsUsedByThisChart = mxCreateCellArray(2, exp_dims);
    mxSetField(mxPostCodegenInfo, 0, "exportedFunctionsUsedByThisChart",
               mxExportedFunctionsUsedByThisChart);
    mxSetField(mxPostCodegenInfo, 0, "exportedFunctionsChecksum",
               mxExportedFunctionsChecksum);
  }

  return mxPostCodegenInfo;
}

static const mxArray *sf_get_sim_state_info_c11_LessonII_start(void)
{
  const char *infoFields[] = { "chartChecksum", "varInfo" };

  mxArray *mxInfo = mxCreateStructMatrix(1, 1, 2, infoFields);
  const char *infoEncStr[] = {
    "100 S1x2'type','srcId','name','auxInfo'{{M[1],M[5],T\"way\",},{M[8],M[0],T\"is_active_c11_LessonII_start\",}}"
  };

  mxArray *mxVarInfo = sf_mex_decode_encoded_mx_struct_array(infoEncStr, 2, 10);
  mxArray *mxChecksum = mxCreateDoubleMatrix(1, 4, mxREAL);
  sf_c11_LessonII_start_get_check_sum(&mxChecksum);
  mxSetField(mxInfo, 0, infoFields[0], mxChecksum);
  mxSetField(mxInfo, 0, infoFields[1], mxVarInfo);
  return mxInfo;
}

static void chart_debug_initialization(SimStruct *S, unsigned int
  fullDebuggerInitialization)
{
  if (!sim_mode_is_rtw_gen(S)) {
    SFc11_LessonII_startInstanceStruct *chartInstance;
    ChartRunTimeInfo * crtInfo = (ChartRunTimeInfo *)(ssGetUserData(S));
    ChartInfoStruct * chartInfo = (ChartInfoStruct *)(crtInfo->instanceInfo);
    chartInstance = (SFc11_LessonII_startInstanceStruct *)
      chartInfo->chartInstance;
    if (ssIsFirstInitCond(S) && fullDebuggerInitialization==1) {
      /* do this only if simulation is starting */
      {
        unsigned int chartAlreadyPresent;
        chartAlreadyPresent = sf_debug_initialize_chart
          (sfGlobalDebugInstanceStruct,
           _LessonII_startMachineNumber_,
           11,
           1,
           1,
           0,
           2,
           0,
           0,
           0,
           0,
           0,
           &(chartInstance->chartNumber),
           &(chartInstance->instanceNumber),
           (void *)S);

        /* Each instance must initialize its own list of scripts */
        init_script_number_translation(_LessonII_startMachineNumber_,
          chartInstance->chartNumber,chartInstance->instanceNumber);
        if (chartAlreadyPresent==0) {
          /* this is the first instance */
          sf_debug_set_chart_disable_implicit_casting
            (sfGlobalDebugInstanceStruct,_LessonII_startMachineNumber_,
             chartInstance->chartNumber,1);
          sf_debug_set_chart_event_thresholds(sfGlobalDebugInstanceStruct,
            _LessonII_startMachineNumber_,
            chartInstance->chartNumber,
            0,
            0,
            0);
          _SFD_SET_DATA_PROPS(0,1,1,0,"array");
          _SFD_SET_DATA_PROPS(1,2,0,1,"way");
          _SFD_STATE_INFO(0,0,2);
          _SFD_CH_SUBSTATE_COUNT(0);
          _SFD_CH_SUBSTATE_DECOMP(0);
        }

        _SFD_CV_INIT_CHART(0,0,0,0);

        {
          _SFD_CV_INIT_STATE(0,0,0,0,0,0,NULL,NULL);
        }

        _SFD_CV_INIT_TRANS(0,0,NULL,NULL,0,NULL);

        /* Initialization of MATLAB Function Model Coverage */
        _SFD_CV_INIT_EML(0,1,1,0,0,3,0,0,0,0,0);
        _SFD_CV_INIT_EML_FCN(0,0,"eML_blk_kernel",0,-1,102);
        _SFD_CV_INIT_EML_SATURATION(0,1,0,32,-1,46);
        _SFD_CV_INIT_EML_SATURATION(0,1,1,54,-1,68);
        _SFD_CV_INIT_EML_SATURATION(0,1,2,86,-1,101);

        {
          unsigned int dimVector[1];
          dimVector[0]= 3;
          _SFD_SET_DATA_COMPILED_PROPS(0,SF_DOUBLE,1,&(dimVector[0]),0,0,0,0.0,
            1.0,0,0,(MexFcnForType)c11_b_sf_marshallOut,(MexInFcnForType)NULL);
        }

        _SFD_SET_DATA_COMPILED_PROPS(1,SF_STRUCT,0,NULL,0,0,0,0.0,1.0,0,0,
          (MexFcnForType)c11_way_bus_io,(MexInFcnForType)NULL);
        _SFD_SET_DATA_VALUE_PTR(0U, *chartInstance->c11_array);
        _SFD_SET_DATA_VALUE_PTR(1U, chartInstance->c11_way);
      }
    } else {
      sf_debug_reset_current_state_configuration(sfGlobalDebugInstanceStruct,
        _LessonII_startMachineNumber_,chartInstance->chartNumber,
        chartInstance->instanceNumber);
    }
  }
}

static const char* sf_get_instance_specialization(void)
{
  return "rx5Y5ul0WbKA9RRIw2tpjH";
}

static void sf_opaque_initialize_c11_LessonII_start(void *chartInstanceVar)
{
  chart_debug_initialization(((SFc11_LessonII_startInstanceStruct*)
    chartInstanceVar)->S,0);
  initialize_params_c11_LessonII_start((SFc11_LessonII_startInstanceStruct*)
    chartInstanceVar);
  initialize_c11_LessonII_start((SFc11_LessonII_startInstanceStruct*)
    chartInstanceVar);
}

static void sf_opaque_enable_c11_LessonII_start(void *chartInstanceVar)
{
  enable_c11_LessonII_start((SFc11_LessonII_startInstanceStruct*)
    chartInstanceVar);
}

static void sf_opaque_disable_c11_LessonII_start(void *chartInstanceVar)
{
  disable_c11_LessonII_start((SFc11_LessonII_startInstanceStruct*)
    chartInstanceVar);
}

static void sf_opaque_gateway_c11_LessonII_start(void *chartInstanceVar)
{
  sf_gateway_c11_LessonII_start((SFc11_LessonII_startInstanceStruct*)
    chartInstanceVar);
}

static const mxArray* sf_opaque_get_sim_state_c11_LessonII_start(SimStruct* S)
{
  ChartRunTimeInfo * crtInfo = (ChartRunTimeInfo *)(ssGetUserData(S));
  ChartInfoStruct * chartInfo = (ChartInfoStruct *)(crtInfo->instanceInfo);
  return get_sim_state_c11_LessonII_start((SFc11_LessonII_startInstanceStruct*)
    chartInfo->chartInstance);         /* raw sim ctx */
}

static void sf_opaque_set_sim_state_c11_LessonII_start(SimStruct* S, const
  mxArray *st)
{
  ChartRunTimeInfo * crtInfo = (ChartRunTimeInfo *)(ssGetUserData(S));
  ChartInfoStruct * chartInfo = (ChartInfoStruct *)(crtInfo->instanceInfo);
  set_sim_state_c11_LessonII_start((SFc11_LessonII_startInstanceStruct*)
    chartInfo->chartInstance, st);
}

static void sf_opaque_terminate_c11_LessonII_start(void *chartInstanceVar)
{
  if (chartInstanceVar!=NULL) {
    SimStruct *S = ((SFc11_LessonII_startInstanceStruct*) chartInstanceVar)->S;
    ChartRunTimeInfo * crtInfo = (ChartRunTimeInfo *)(ssGetUserData(S));
    if (sim_mode_is_rtw_gen(S) || sim_mode_is_external(S)) {
      sf_clear_rtw_identifier(S);
      unload_LessonII_start_optimization_info();
    }

    finalize_c11_LessonII_start((SFc11_LessonII_startInstanceStruct*)
      chartInstanceVar);
    utFree(chartInstanceVar);
    if (crtInfo != NULL) {
      utFree(crtInfo);
    }

    ssSetUserData(S,NULL);
  }
}

static void sf_opaque_init_subchart_simstructs(void *chartInstanceVar)
{
  initSimStructsc11_LessonII_start((SFc11_LessonII_startInstanceStruct*)
    chartInstanceVar);
}

extern unsigned int sf_machine_global_initializer_called(void);
static void mdlProcessParameters_c11_LessonII_start(SimStruct *S)
{
  int i;
  for (i=0;i<ssGetNumRunTimeParams(S);i++) {
    if (ssGetSFcnParamTunable(S,i)) {
      ssUpdateDlgParamAsRunTimeParam(S,i);
    }
  }

  if (sf_machine_global_initializer_called()) {
    ChartRunTimeInfo * crtInfo = (ChartRunTimeInfo *)(ssGetUserData(S));
    ChartInfoStruct * chartInfo = (ChartInfoStruct *)(crtInfo->instanceInfo);
    initialize_params_c11_LessonII_start((SFc11_LessonII_startInstanceStruct*)
      (chartInfo->chartInstance));
  }
}

static void mdlSetWorkWidths_c11_LessonII_start(SimStruct *S)
{
  if (sim_mode_is_rtw_gen(S) || sim_mode_is_external(S)) {
    mxArray *infoStruct = load_LessonII_start_optimization_info();
    int_T chartIsInlinable =
      (int_T)sf_is_chart_inlinable(sf_get_instance_specialization(),infoStruct,
      11);
    ssSetStateflowIsInlinable(S,chartIsInlinable);
    ssSetRTWCG(S,sf_rtw_info_uint_prop(sf_get_instance_specialization(),
                infoStruct,11,"RTWCG"));
    ssSetEnableFcnIsTrivial(S,1);
    ssSetDisableFcnIsTrivial(S,1);
    ssSetNotMultipleInlinable(S,sf_rtw_info_uint_prop
      (sf_get_instance_specialization(),infoStruct,11,
       "gatewayCannotBeInlinedMultipleTimes"));
    sf_update_buildInfo(sf_get_instance_specialization(),infoStruct,11);
    if (chartIsInlinable) {
      ssSetInputPortOptimOpts(S, 0, SS_REUSABLE_AND_LOCAL);
      sf_mark_chart_expressionable_inputs(S,sf_get_instance_specialization(),
        infoStruct,11,1);
      sf_mark_chart_reusable_outputs(S,sf_get_instance_specialization(),
        infoStruct,11,1);
    }

    {
      unsigned int outPortIdx;
      for (outPortIdx=1; outPortIdx<=1; ++outPortIdx) {
        ssSetOutputPortOptimizeInIR(S, outPortIdx, 1U);
      }
    }

    {
      unsigned int inPortIdx;
      for (inPortIdx=0; inPortIdx < 1; ++inPortIdx) {
        ssSetInputPortOptimizeInIR(S, inPortIdx, 1U);
      }
    }

    sf_set_rtw_dwork_info(S,sf_get_instance_specialization(),infoStruct,11);
    ssSetHasSubFunctions(S,!(chartIsInlinable));
  } else {
  }

  ssSetOptions(S,ssGetOptions(S)|SS_OPTION_WORKS_WITH_CODE_REUSE);
  ssSetChecksum0(S,(2088416887U));
  ssSetChecksum1(S,(675047122U));
  ssSetChecksum2(S,(3869416246U));
  ssSetChecksum3(S,(2483576889U));
  ssSetmdlDerivatives(S, NULL);
  ssSetExplicitFCSSCtrl(S,1);
  ssSupportsMultipleExecInstances(S,1);
}

static void mdlRTW_c11_LessonII_start(SimStruct *S)
{
  if (sim_mode_is_rtw_gen(S)) {
    ssWriteRTWStrParam(S, "StateflowChartType", "Embedded MATLAB");
  }
}

static void mdlStart_c11_LessonII_start(SimStruct *S)
{
  SFc11_LessonII_startInstanceStruct *chartInstance;
  ChartRunTimeInfo * crtInfo = (ChartRunTimeInfo *)utMalloc(sizeof
    (ChartRunTimeInfo));
  chartInstance = (SFc11_LessonII_startInstanceStruct *)utMalloc(sizeof
    (SFc11_LessonII_startInstanceStruct));
  memset(chartInstance, 0, sizeof(SFc11_LessonII_startInstanceStruct));
  if (chartInstance==NULL) {
    sf_mex_error_message("Could not allocate memory for chart instance.");
  }

  chartInstance->chartInfo.chartInstance = chartInstance;
  chartInstance->chartInfo.isEMLChart = 1;
  chartInstance->chartInfo.chartInitialized = 0;
  chartInstance->chartInfo.sFunctionGateway =
    sf_opaque_gateway_c11_LessonII_start;
  chartInstance->chartInfo.initializeChart =
    sf_opaque_initialize_c11_LessonII_start;
  chartInstance->chartInfo.terminateChart =
    sf_opaque_terminate_c11_LessonII_start;
  chartInstance->chartInfo.enableChart = sf_opaque_enable_c11_LessonII_start;
  chartInstance->chartInfo.disableChart = sf_opaque_disable_c11_LessonII_start;
  chartInstance->chartInfo.getSimState =
    sf_opaque_get_sim_state_c11_LessonII_start;
  chartInstance->chartInfo.setSimState =
    sf_opaque_set_sim_state_c11_LessonII_start;
  chartInstance->chartInfo.getSimStateInfo =
    sf_get_sim_state_info_c11_LessonII_start;
  chartInstance->chartInfo.zeroCrossings = NULL;
  chartInstance->chartInfo.outputs = NULL;
  chartInstance->chartInfo.derivatives = NULL;
  chartInstance->chartInfo.mdlRTW = mdlRTW_c11_LessonII_start;
  chartInstance->chartInfo.mdlStart = mdlStart_c11_LessonII_start;
  chartInstance->chartInfo.mdlSetWorkWidths =
    mdlSetWorkWidths_c11_LessonII_start;
  chartInstance->chartInfo.extModeExec = NULL;
  chartInstance->chartInfo.restoreLastMajorStepConfiguration = NULL;
  chartInstance->chartInfo.restoreBeforeLastMajorStepConfiguration = NULL;
  chartInstance->chartInfo.storeCurrentConfiguration = NULL;
  chartInstance->chartInfo.callAtomicSubchartUserFcn = NULL;
  chartInstance->chartInfo.callAtomicSubchartAutoFcn = NULL;
  chartInstance->chartInfo.debugInstance = sfGlobalDebugInstanceStruct;
  chartInstance->S = S;
  crtInfo->checksum = SF_RUNTIME_INFO_CHECKSUM;
  crtInfo->instanceInfo = (&(chartInstance->chartInfo));
  crtInfo->isJITEnabled = false;
  crtInfo->compiledInfo = NULL;
  ssSetUserData(S,(void *)(crtInfo));  /* register the chart instance with simstruct */
  init_dsm_address_info(chartInstance);
  init_simulink_io_address(chartInstance);
  if (!sim_mode_is_rtw_gen(S)) {
  }

  sf_opaque_init_subchart_simstructs(chartInstance->chartInfo.chartInstance);
  chart_debug_initialization(S,1);
}

void c11_LessonII_start_method_dispatcher(SimStruct *S, int_T method, void *data)
{
  switch (method) {
   case SS_CALL_MDL_START:
    mdlStart_c11_LessonII_start(S);
    break;

   case SS_CALL_MDL_SET_WORK_WIDTHS:
    mdlSetWorkWidths_c11_LessonII_start(S);
    break;

   case SS_CALL_MDL_PROCESS_PARAMETERS:
    mdlProcessParameters_c11_LessonII_start(S);
    break;

   default:
    /* Unhandled method */
    sf_mex_error_message("Stateflow Internal Error:\n"
                         "Error calling c11_LessonII_start_method_dispatcher.\n"
                         "Can't handle method %d.\n", method);
    break;
  }
}

#ifndef __c1_LessonIII_start_h__
#define __c1_LessonIII_start_h__

/* Include files */
#include "sf_runtime/sfc_sf.h"
#include "sf_runtime/sfc_mex.h"
#include "rtwtypes.h"
#include "multiword_types.h"

/* Type Definitions */
#ifndef struct_BlobData_tag
#define struct_BlobData_tag

struct BlobData_tag
{
  real32_T centroid[32];
  int32_T area[16];
  uint8_T color[16];
};

#endif                                 /*struct_BlobData_tag*/

#ifndef typedef_c1_BlobData
#define typedef_c1_BlobData

typedef struct BlobData_tag c1_BlobData;

#endif                                 /*typedef_c1_BlobData*/

#ifndef struct_BlobData_tag_size
#define struct_BlobData_tag_size

struct BlobData_tag_size
{
  int32_T centroid[2];
  int32_T area;
  int32_T color;
};

#endif                                 /*struct_BlobData_tag_size*/

#ifndef typedef_c1_BlobData_size
#define typedef_c1_BlobData_size

typedef struct BlobData_tag_size c1_BlobData_size;

#endif                                 /*typedef_c1_BlobData_size*/

#ifndef typedef_SFc1_LessonIII_startInstanceStruct
#define typedef_SFc1_LessonIII_startInstanceStruct

typedef struct {
  SimStruct *S;
  ChartInfoStruct chartInfo;
  uint32_T chartNumber;
  uint32_T instanceNumber;
  int32_T c1_sfEvent;
  boolean_T c1_isStable;
  boolean_T c1_doneDoubleBufferReInit;
  uint8_T c1_is_active_c1_LessonIII_start;
  boolean_T c1_RBW[76800];
  real32_T (*c1_centroids_data)[32];
  int32_T (*c1_centroids_sizes)[2];
  int32_T (*c1_area_data)[16];
  int32_T (*c1_area_sizes)[2];
  boolean_T (*c1_b_RBW)[76800];
  boolean_T (*c1_GBW)[76800];
  boolean_T (*c1_BBW)[76800];
  c1_BlobData *c1_blobs_data;
  c1_BlobData_size *c1_blobs_elems_sizes;
} SFc1_LessonIII_startInstanceStruct;

#endif                                 /*typedef_SFc1_LessonIII_startInstanceStruct*/

/* Named Constants */

/* Variable Declarations */
extern struct SfDebugInstanceStruct *sfGlobalDebugInstanceStruct;

/* Variable Definitions */

/* Function Declarations */
extern const mxArray *sf_c1_LessonIII_start_get_eml_resolved_functions_info(void);

/* Function Definitions */
extern void sf_c1_LessonIII_start_get_check_sum(mxArray *plhs[]);
extern void c1_LessonIII_start_method_dispatcher(SimStruct *S, int_T method,
  void *data);

#endif

#ifndef __c60_LessonIII_start_h__
#define __c60_LessonIII_start_h__

/* Include files */
#include "sf_runtime/sfc_sf.h"
#include "sf_runtime/sfc_mex.h"
#include "rtwtypes.h"
#include "multiword_types.h"

/* Type Definitions */
#ifndef typedef_SFc60_LessonIII_startInstanceStruct
#define typedef_SFc60_LessonIII_startInstanceStruct

typedef struct {
  SimStruct *S;
  ChartInfoStruct chartInfo;
  uint32_T chartNumber;
  uint32_T instanceNumber;
  int32_T c60_sfEvent;
  boolean_T c60_isStable;
  boolean_T c60_doneDoubleBufferReInit;
  uint8_T c60_is_active_c60_LessonIII_start;
  real32_T c60_position[12];
  boolean_T c60_position_not_empty;
  real32_T c60_rotation[6];
  boolean_T c60_rotation_not_empty;
  real32_T c60_t;
  boolean_T c60_t_not_empty;
  real32_T (*c60_Crotation)[24];
  real_T (*c60_leftArray)[6];
  real32_T (*c60_Ctranspose)[18];
  real_T (*c60_rightArray)[6];
  real_T *c60_robotWidth;
  real32_T (*c60_init_cond)[18];
  real_T *c60_maxVel;
  real_T *c60_timeStep;
  real32_T (*c60_active)[6];
} SFc60_LessonIII_startInstanceStruct;

#endif                                 /*typedef_SFc60_LessonIII_startInstanceStruct*/

/* Named Constants */

/* Variable Declarations */
extern struct SfDebugInstanceStruct *sfGlobalDebugInstanceStruct;

/* Variable Definitions */

/* Function Declarations */
extern const mxArray *sf_c60_LessonIII_start_get_eml_resolved_functions_info
  (void);

/* Function Definitions */
extern void sf_c60_LessonIII_start_get_check_sum(mxArray *plhs[]);
extern void c60_LessonIII_start_method_dispatcher(SimStruct *S, int_T method,
  void *data);

#endif

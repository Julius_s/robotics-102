/* Include files */

#include <stddef.h>
#include "blas.h"
#include "LessonIII_start_sfun.h"
#include "c48_LessonIII_start.h"
#include "mwmathutil.h"
#define CHARTINSTANCE_CHARTNUMBER      (chartInstance->chartNumber)
#define CHARTINSTANCE_INSTANCENUMBER   (chartInstance->instanceNumber)
#include "LessonIII_start_sfun_debug_macros.h"
#define _SF_MEX_LISTEN_FOR_CTRL_C(S)   sf_mex_listen_for_ctrl_c(sfGlobalDebugInstanceStruct,S);

/* Type Definitions */

/* Named Constants */
#define CALL_EVENT                     (-1)

/* Variable Declarations */

/* Variable Definitions */
static real_T _sfTime_;
static const char * c48_debug_family_names[5] = { "nargin", "nargout",
  "targetPos", "myPos", "targetOrientation" };

/* Function Declarations */
static void initialize_c48_LessonIII_start(SFc48_LessonIII_startInstanceStruct
  *chartInstance);
static void initialize_params_c48_LessonIII_start
  (SFc48_LessonIII_startInstanceStruct *chartInstance);
static void enable_c48_LessonIII_start(SFc48_LessonIII_startInstanceStruct
  *chartInstance);
static void disable_c48_LessonIII_start(SFc48_LessonIII_startInstanceStruct
  *chartInstance);
static void c48_update_debugger_state_c48_LessonIII_start
  (SFc48_LessonIII_startInstanceStruct *chartInstance);
static const mxArray *get_sim_state_c48_LessonIII_start
  (SFc48_LessonIII_startInstanceStruct *chartInstance);
static void set_sim_state_c48_LessonIII_start
  (SFc48_LessonIII_startInstanceStruct *chartInstance, const mxArray *c48_st);
static void finalize_c48_LessonIII_start(SFc48_LessonIII_startInstanceStruct
  *chartInstance);
static void sf_gateway_c48_LessonIII_start(SFc48_LessonIII_startInstanceStruct
  *chartInstance);
static void mdl_start_c48_LessonIII_start(SFc48_LessonIII_startInstanceStruct
  *chartInstance);
static void initSimStructsc48_LessonIII_start
  (SFc48_LessonIII_startInstanceStruct *chartInstance);
static void init_script_number_translation(uint32_T c48_machineNumber, uint32_T
  c48_chartNumber, uint32_T c48_instanceNumber);
static const mxArray *c48_sf_marshallOut(void *chartInstanceVoid, void
  *c48_inData);
static int16_T c48_emlrt_marshallIn(SFc48_LessonIII_startInstanceStruct
  *chartInstance, const mxArray *c48_b_targetOrientation, const char_T
  *c48_identifier);
static int16_T c48_b_emlrt_marshallIn(SFc48_LessonIII_startInstanceStruct
  *chartInstance, const mxArray *c48_u, const emlrtMsgIdentifier *c48_parentId);
static void c48_sf_marshallIn(void *chartInstanceVoid, const mxArray
  *c48_mxArrayInData, const char_T *c48_varName, void *c48_outData);
static const mxArray *c48_b_sf_marshallOut(void *chartInstanceVoid, void
  *c48_inData);
static const mxArray *c48_c_sf_marshallOut(void *chartInstanceVoid, void
  *c48_inData);
static real_T c48_c_emlrt_marshallIn(SFc48_LessonIII_startInstanceStruct
  *chartInstance, const mxArray *c48_u, const emlrtMsgIdentifier *c48_parentId);
static void c48_b_sf_marshallIn(void *chartInstanceVoid, const mxArray
  *c48_mxArrayInData, const char_T *c48_varName, void *c48_outData);
static void c48_info_helper(const mxArray **c48_info);
static const mxArray *c48_emlrt_marshallOut(const char * c48_u);
static const mxArray *c48_b_emlrt_marshallOut(const uint32_T c48_u);
static const mxArray *c48_d_sf_marshallOut(void *chartInstanceVoid, void
  *c48_inData);
static int32_T c48_d_emlrt_marshallIn(SFc48_LessonIII_startInstanceStruct
  *chartInstance, const mxArray *c48_u, const emlrtMsgIdentifier *c48_parentId);
static void c48_c_sf_marshallIn(void *chartInstanceVoid, const mxArray
  *c48_mxArrayInData, const char_T *c48_varName, void *c48_outData);
static uint8_T c48_e_emlrt_marshallIn(SFc48_LessonIII_startInstanceStruct
  *chartInstance, const mxArray *c48_b_is_active_c48_LessonIII_start, const
  char_T *c48_identifier);
static uint8_T c48_f_emlrt_marshallIn(SFc48_LessonIII_startInstanceStruct
  *chartInstance, const mxArray *c48_u, const emlrtMsgIdentifier *c48_parentId);
static void init_dsm_address_info(SFc48_LessonIII_startInstanceStruct
  *chartInstance);
static void init_simulink_io_address(SFc48_LessonIII_startInstanceStruct
  *chartInstance);

/* Function Definitions */
static void initialize_c48_LessonIII_start(SFc48_LessonIII_startInstanceStruct
  *chartInstance)
{
  chartInstance->c48_sfEvent = CALL_EVENT;
  _sfTime_ = sf_get_time(chartInstance->S);
  chartInstance->c48_is_active_c48_LessonIII_start = 0U;
}

static void initialize_params_c48_LessonIII_start
  (SFc48_LessonIII_startInstanceStruct *chartInstance)
{
  (void)chartInstance;
}

static void enable_c48_LessonIII_start(SFc48_LessonIII_startInstanceStruct
  *chartInstance)
{
  _sfTime_ = sf_get_time(chartInstance->S);
}

static void disable_c48_LessonIII_start(SFc48_LessonIII_startInstanceStruct
  *chartInstance)
{
  _sfTime_ = sf_get_time(chartInstance->S);
}

static void c48_update_debugger_state_c48_LessonIII_start
  (SFc48_LessonIII_startInstanceStruct *chartInstance)
{
  (void)chartInstance;
}

static const mxArray *get_sim_state_c48_LessonIII_start
  (SFc48_LessonIII_startInstanceStruct *chartInstance)
{
  const mxArray *c48_st;
  const mxArray *c48_y = NULL;
  int16_T c48_hoistedGlobal;
  int16_T c48_u;
  const mxArray *c48_b_y = NULL;
  uint8_T c48_b_hoistedGlobal;
  uint8_T c48_b_u;
  const mxArray *c48_c_y = NULL;
  c48_st = NULL;
  c48_st = NULL;
  c48_y = NULL;
  sf_mex_assign(&c48_y, sf_mex_createcellmatrix(2, 1), false);
  c48_hoistedGlobal = *chartInstance->c48_targetOrientation;
  c48_u = c48_hoistedGlobal;
  c48_b_y = NULL;
  sf_mex_assign(&c48_b_y, sf_mex_create("y", &c48_u, 4, 0U, 0U, 0U, 0), false);
  sf_mex_setcell(c48_y, 0, c48_b_y);
  c48_b_hoistedGlobal = chartInstance->c48_is_active_c48_LessonIII_start;
  c48_b_u = c48_b_hoistedGlobal;
  c48_c_y = NULL;
  sf_mex_assign(&c48_c_y, sf_mex_create("y", &c48_b_u, 3, 0U, 0U, 0U, 0), false);
  sf_mex_setcell(c48_y, 1, c48_c_y);
  sf_mex_assign(&c48_st, c48_y, false);
  return c48_st;
}

static void set_sim_state_c48_LessonIII_start
  (SFc48_LessonIII_startInstanceStruct *chartInstance, const mxArray *c48_st)
{
  const mxArray *c48_u;
  chartInstance->c48_doneDoubleBufferReInit = true;
  c48_u = sf_mex_dup(c48_st);
  *chartInstance->c48_targetOrientation = c48_emlrt_marshallIn(chartInstance,
    sf_mex_dup(sf_mex_getcell(c48_u, 0)), "targetOrientation");
  chartInstance->c48_is_active_c48_LessonIII_start = c48_e_emlrt_marshallIn
    (chartInstance, sf_mex_dup(sf_mex_getcell(c48_u, 1)),
     "is_active_c48_LessonIII_start");
  sf_mex_destroy(&c48_u);
  c48_update_debugger_state_c48_LessonIII_start(chartInstance);
  sf_mex_destroy(&c48_st);
}

static void finalize_c48_LessonIII_start(SFc48_LessonIII_startInstanceStruct
  *chartInstance)
{
  (void)chartInstance;
}

static void sf_gateway_c48_LessonIII_start(SFc48_LessonIII_startInstanceStruct
  *chartInstance)
{
  int32_T c48_i0;
  int32_T c48_i1;
  int8_T c48_b_targetPos[2];
  int32_T c48_i2;
  int8_T c48_b_myPos[2];
  uint32_T c48_debug_family_var_map[5];
  real_T c48_nargin = 2.0;
  real_T c48_nargout = 1.0;
  int16_T c48_b_targetOrientation;
  int32_T c48_i3;
  real_T c48_y;
  int32_T c48_i4;
  real_T c48_x;
  real_T c48_b_y;
  real_T c48_b_x;
  real_T c48_r;
  real_T c48_b_r;
  real_T c48_d0;
  int16_T c48_i5;
  int32_T c48_i6;
  _SFD_SYMBOL_SCOPE_PUSH(0U, 0U);
  _sfTime_ = sf_get_time(chartInstance->S);
  _SFD_CC_CALL(CHART_ENTER_SFUNCTION_TAG, 31U, chartInstance->c48_sfEvent);
  for (c48_i0 = 0; c48_i0 < 2; c48_i0++) {
    _SFD_DATA_RANGE_CHECK((real_T)(*chartInstance->c48_targetPos)[c48_i0], 0U);
  }

  chartInstance->c48_sfEvent = CALL_EVENT;
  _SFD_CC_CALL(CHART_ENTER_DURING_FUNCTION_TAG, 31U, chartInstance->c48_sfEvent);
  for (c48_i1 = 0; c48_i1 < 2; c48_i1++) {
    c48_b_targetPos[c48_i1] = (*chartInstance->c48_targetPos)[c48_i1];
  }

  for (c48_i2 = 0; c48_i2 < 2; c48_i2++) {
    c48_b_myPos[c48_i2] = (*chartInstance->c48_myPos)[c48_i2];
  }

  _SFD_SYMBOL_SCOPE_PUSH_EML(0U, 5U, 5U, c48_debug_family_names,
    c48_debug_family_var_map);
  _SFD_SYMBOL_SCOPE_ADD_EML_IMPORTABLE(&c48_nargin, 0U, c48_c_sf_marshallOut,
    c48_b_sf_marshallIn);
  _SFD_SYMBOL_SCOPE_ADD_EML_IMPORTABLE(&c48_nargout, 1U, c48_c_sf_marshallOut,
    c48_b_sf_marshallIn);
  _SFD_SYMBOL_SCOPE_ADD_EML(c48_b_targetPos, 2U, c48_b_sf_marshallOut);
  _SFD_SYMBOL_SCOPE_ADD_EML(c48_b_myPos, 3U, c48_b_sf_marshallOut);
  _SFD_SYMBOL_SCOPE_ADD_EML_IMPORTABLE(&c48_b_targetOrientation, 4U,
    c48_sf_marshallOut, c48_sf_marshallIn);
  CV_EML_FCN(0, 0);
  _SFD_EML_CALL(0U, chartInstance->c48_sfEvent, 2);
  c48_i3 = c48_b_targetPos[1] - c48_b_myPos[1];
  if (c48_i3 > 127) {
    CV_SATURATION_EVAL(4, 0, 2, 0, 1);
    c48_i3 = 127;
  } else {
    if (CV_SATURATION_EVAL(4, 0, 2, 0, c48_i3 < -128)) {
      c48_i3 = -128;
    }
  }

  c48_y = (real_T)(int8_T)c48_i3;
  c48_i4 = c48_b_targetPos[0] - c48_b_myPos[0];
  if (c48_i4 > 127) {
    CV_SATURATION_EVAL(4, 0, 0, 0, 1);
    c48_i4 = 127;
  } else {
    if (CV_SATURATION_EVAL(4, 0, 0, 0, c48_i4 < -128)) {
      c48_i4 = -128;
    }
  }

  c48_x = (real_T)(int8_T)c48_i4;
  c48_b_y = c48_y;
  c48_b_x = c48_x;
  c48_r = muDoubleScalarAtan2(c48_b_y, c48_b_x);
  c48_b_r = 57.295779513082323 * c48_r;
  c48_d0 = muDoubleScalarRound(c48_b_r);
  if (c48_d0 < 32768.0) {
    if (CV_SATURATION_EVAL(4, 0, 1, 1, c48_d0 >= -32768.0)) {
      c48_i5 = (int16_T)c48_d0;
    } else {
      c48_i5 = MIN_int16_T;
    }
  } else if (CV_SATURATION_EVAL(4, 0, 1, 0, c48_d0 >= 32768.0)) {
    c48_i5 = MAX_int16_T;
  } else {
    c48_i5 = 0;
  }

  c48_b_targetOrientation = c48_i5;
  _SFD_EML_CALL(0U, chartInstance->c48_sfEvent, -2);
  _SFD_SYMBOL_SCOPE_POP();
  *chartInstance->c48_targetOrientation = c48_b_targetOrientation;
  _SFD_CC_CALL(EXIT_OUT_OF_FUNCTION_TAG, 31U, chartInstance->c48_sfEvent);
  _SFD_SYMBOL_SCOPE_POP();
  _SFD_CHECK_FOR_STATE_INCONSISTENCY(_LessonIII_startMachineNumber_,
    chartInstance->chartNumber, chartInstance->instanceNumber);
  _SFD_DATA_RANGE_CHECK((real_T)*chartInstance->c48_targetOrientation, 1U);
  for (c48_i6 = 0; c48_i6 < 2; c48_i6++) {
    _SFD_DATA_RANGE_CHECK((real_T)(*chartInstance->c48_myPos)[c48_i6], 2U);
  }
}

static void mdl_start_c48_LessonIII_start(SFc48_LessonIII_startInstanceStruct
  *chartInstance)
{
  (void)chartInstance;
}

static void initSimStructsc48_LessonIII_start
  (SFc48_LessonIII_startInstanceStruct *chartInstance)
{
  (void)chartInstance;
}

static void init_script_number_translation(uint32_T c48_machineNumber, uint32_T
  c48_chartNumber, uint32_T c48_instanceNumber)
{
  (void)c48_machineNumber;
  (void)c48_chartNumber;
  (void)c48_instanceNumber;
}

static const mxArray *c48_sf_marshallOut(void *chartInstanceVoid, void
  *c48_inData)
{
  const mxArray *c48_mxArrayOutData = NULL;
  int16_T c48_u;
  const mxArray *c48_y = NULL;
  SFc48_LessonIII_startInstanceStruct *chartInstance;
  chartInstance = (SFc48_LessonIII_startInstanceStruct *)chartInstanceVoid;
  c48_mxArrayOutData = NULL;
  c48_u = *(int16_T *)c48_inData;
  c48_y = NULL;
  sf_mex_assign(&c48_y, sf_mex_create("y", &c48_u, 4, 0U, 0U, 0U, 0), false);
  sf_mex_assign(&c48_mxArrayOutData, c48_y, false);
  return c48_mxArrayOutData;
}

static int16_T c48_emlrt_marshallIn(SFc48_LessonIII_startInstanceStruct
  *chartInstance, const mxArray *c48_b_targetOrientation, const char_T
  *c48_identifier)
{
  int16_T c48_y;
  emlrtMsgIdentifier c48_thisId;
  c48_thisId.fIdentifier = c48_identifier;
  c48_thisId.fParent = NULL;
  c48_y = c48_b_emlrt_marshallIn(chartInstance, sf_mex_dup
    (c48_b_targetOrientation), &c48_thisId);
  sf_mex_destroy(&c48_b_targetOrientation);
  return c48_y;
}

static int16_T c48_b_emlrt_marshallIn(SFc48_LessonIII_startInstanceStruct
  *chartInstance, const mxArray *c48_u, const emlrtMsgIdentifier *c48_parentId)
{
  int16_T c48_y;
  int16_T c48_i7;
  (void)chartInstance;
  sf_mex_import(c48_parentId, sf_mex_dup(c48_u), &c48_i7, 1, 4, 0U, 0, 0U, 0);
  c48_y = c48_i7;
  sf_mex_destroy(&c48_u);
  return c48_y;
}

static void c48_sf_marshallIn(void *chartInstanceVoid, const mxArray
  *c48_mxArrayInData, const char_T *c48_varName, void *c48_outData)
{
  const mxArray *c48_b_targetOrientation;
  const char_T *c48_identifier;
  emlrtMsgIdentifier c48_thisId;
  int16_T c48_y;
  SFc48_LessonIII_startInstanceStruct *chartInstance;
  chartInstance = (SFc48_LessonIII_startInstanceStruct *)chartInstanceVoid;
  c48_b_targetOrientation = sf_mex_dup(c48_mxArrayInData);
  c48_identifier = c48_varName;
  c48_thisId.fIdentifier = c48_identifier;
  c48_thisId.fParent = NULL;
  c48_y = c48_b_emlrt_marshallIn(chartInstance, sf_mex_dup
    (c48_b_targetOrientation), &c48_thisId);
  sf_mex_destroy(&c48_b_targetOrientation);
  *(int16_T *)c48_outData = c48_y;
  sf_mex_destroy(&c48_mxArrayInData);
}

static const mxArray *c48_b_sf_marshallOut(void *chartInstanceVoid, void
  *c48_inData)
{
  const mxArray *c48_mxArrayOutData = NULL;
  int32_T c48_i8;
  int8_T c48_b_inData[2];
  int32_T c48_i9;
  int8_T c48_u[2];
  const mxArray *c48_y = NULL;
  SFc48_LessonIII_startInstanceStruct *chartInstance;
  chartInstance = (SFc48_LessonIII_startInstanceStruct *)chartInstanceVoid;
  c48_mxArrayOutData = NULL;
  for (c48_i8 = 0; c48_i8 < 2; c48_i8++) {
    c48_b_inData[c48_i8] = (*(int8_T (*)[2])c48_inData)[c48_i8];
  }

  for (c48_i9 = 0; c48_i9 < 2; c48_i9++) {
    c48_u[c48_i9] = c48_b_inData[c48_i9];
  }

  c48_y = NULL;
  sf_mex_assign(&c48_y, sf_mex_create("y", c48_u, 2, 0U, 1U, 0U, 2, 2, 1), false);
  sf_mex_assign(&c48_mxArrayOutData, c48_y, false);
  return c48_mxArrayOutData;
}

static const mxArray *c48_c_sf_marshallOut(void *chartInstanceVoid, void
  *c48_inData)
{
  const mxArray *c48_mxArrayOutData = NULL;
  real_T c48_u;
  const mxArray *c48_y = NULL;
  SFc48_LessonIII_startInstanceStruct *chartInstance;
  chartInstance = (SFc48_LessonIII_startInstanceStruct *)chartInstanceVoid;
  c48_mxArrayOutData = NULL;
  c48_u = *(real_T *)c48_inData;
  c48_y = NULL;
  sf_mex_assign(&c48_y, sf_mex_create("y", &c48_u, 0, 0U, 0U, 0U, 0), false);
  sf_mex_assign(&c48_mxArrayOutData, c48_y, false);
  return c48_mxArrayOutData;
}

static real_T c48_c_emlrt_marshallIn(SFc48_LessonIII_startInstanceStruct
  *chartInstance, const mxArray *c48_u, const emlrtMsgIdentifier *c48_parentId)
{
  real_T c48_y;
  real_T c48_d1;
  (void)chartInstance;
  sf_mex_import(c48_parentId, sf_mex_dup(c48_u), &c48_d1, 1, 0, 0U, 0, 0U, 0);
  c48_y = c48_d1;
  sf_mex_destroy(&c48_u);
  return c48_y;
}

static void c48_b_sf_marshallIn(void *chartInstanceVoid, const mxArray
  *c48_mxArrayInData, const char_T *c48_varName, void *c48_outData)
{
  const mxArray *c48_nargout;
  const char_T *c48_identifier;
  emlrtMsgIdentifier c48_thisId;
  real_T c48_y;
  SFc48_LessonIII_startInstanceStruct *chartInstance;
  chartInstance = (SFc48_LessonIII_startInstanceStruct *)chartInstanceVoid;
  c48_nargout = sf_mex_dup(c48_mxArrayInData);
  c48_identifier = c48_varName;
  c48_thisId.fIdentifier = c48_identifier;
  c48_thisId.fParent = NULL;
  c48_y = c48_c_emlrt_marshallIn(chartInstance, sf_mex_dup(c48_nargout),
    &c48_thisId);
  sf_mex_destroy(&c48_nargout);
  *(real_T *)c48_outData = c48_y;
  sf_mex_destroy(&c48_mxArrayInData);
}

const mxArray *sf_c48_LessonIII_start_get_eml_resolved_functions_info(void)
{
  const mxArray *c48_nameCaptureInfo = NULL;
  c48_nameCaptureInfo = NULL;
  sf_mex_assign(&c48_nameCaptureInfo, sf_mex_createstruct("structure", 2, 6, 1),
                false);
  c48_info_helper(&c48_nameCaptureInfo);
  sf_mex_emlrtNameCapturePostProcessR2012a(&c48_nameCaptureInfo);
  return c48_nameCaptureInfo;
}

static void c48_info_helper(const mxArray **c48_info)
{
  const mxArray *c48_rhs0 = NULL;
  const mxArray *c48_lhs0 = NULL;
  const mxArray *c48_rhs1 = NULL;
  const mxArray *c48_lhs1 = NULL;
  const mxArray *c48_rhs2 = NULL;
  const mxArray *c48_lhs2 = NULL;
  const mxArray *c48_rhs3 = NULL;
  const mxArray *c48_lhs3 = NULL;
  const mxArray *c48_rhs4 = NULL;
  const mxArray *c48_lhs4 = NULL;
  const mxArray *c48_rhs5 = NULL;
  const mxArray *c48_lhs5 = NULL;
  sf_mex_addfield(*c48_info, c48_emlrt_marshallOut(""), "context", "context", 0);
  sf_mex_addfield(*c48_info, c48_emlrt_marshallOut("atan2d"), "name", "name", 0);
  sf_mex_addfield(*c48_info, c48_emlrt_marshallOut("double"), "dominantType",
                  "dominantType", 0);
  sf_mex_addfield(*c48_info, c48_emlrt_marshallOut(
    "[ILXE]$matlabroot$/toolbox/eml/lib/matlab/elfun/atan2d.m"), "resolved",
                  "resolved", 0);
  sf_mex_addfield(*c48_info, c48_b_emlrt_marshallOut(1395332096U), "fileTimeLo",
                  "fileTimeLo", 0);
  sf_mex_addfield(*c48_info, c48_b_emlrt_marshallOut(0U), "fileTimeHi",
                  "fileTimeHi", 0);
  sf_mex_addfield(*c48_info, c48_b_emlrt_marshallOut(0U), "mFileTimeLo",
                  "mFileTimeLo", 0);
  sf_mex_addfield(*c48_info, c48_b_emlrt_marshallOut(0U), "mFileTimeHi",
                  "mFileTimeHi", 0);
  sf_mex_assign(&c48_rhs0, sf_mex_createcellmatrix(0, 1), false);
  sf_mex_assign(&c48_lhs0, sf_mex_createcellmatrix(0, 1), false);
  sf_mex_addfield(*c48_info, sf_mex_duplicatearraysafe(&c48_rhs0), "rhs", "rhs",
                  0);
  sf_mex_addfield(*c48_info, sf_mex_duplicatearraysafe(&c48_lhs0), "lhs", "lhs",
                  0);
  sf_mex_addfield(*c48_info, c48_emlrt_marshallOut(
    "[ILXE]$matlabroot$/toolbox/eml/lib/matlab/elfun/atan2d.m"), "context",
                  "context", 1);
  sf_mex_addfield(*c48_info, c48_emlrt_marshallOut("eml_scalar_eg"), "name",
                  "name", 1);
  sf_mex_addfield(*c48_info, c48_emlrt_marshallOut("double"), "dominantType",
                  "dominantType", 1);
  sf_mex_addfield(*c48_info, c48_emlrt_marshallOut(
    "[ILXE]$matlabroot$/toolbox/eml/lib/matlab/eml/eml_scalar_eg.m"), "resolved",
                  "resolved", 1);
  sf_mex_addfield(*c48_info, c48_b_emlrt_marshallOut(1375984288U), "fileTimeLo",
                  "fileTimeLo", 1);
  sf_mex_addfield(*c48_info, c48_b_emlrt_marshallOut(0U), "fileTimeHi",
                  "fileTimeHi", 1);
  sf_mex_addfield(*c48_info, c48_b_emlrt_marshallOut(0U), "mFileTimeLo",
                  "mFileTimeLo", 1);
  sf_mex_addfield(*c48_info, c48_b_emlrt_marshallOut(0U), "mFileTimeHi",
                  "mFileTimeHi", 1);
  sf_mex_assign(&c48_rhs1, sf_mex_createcellmatrix(0, 1), false);
  sf_mex_assign(&c48_lhs1, sf_mex_createcellmatrix(0, 1), false);
  sf_mex_addfield(*c48_info, sf_mex_duplicatearraysafe(&c48_rhs1), "rhs", "rhs",
                  1);
  sf_mex_addfield(*c48_info, sf_mex_duplicatearraysafe(&c48_lhs1), "lhs", "lhs",
                  1);
  sf_mex_addfield(*c48_info, c48_emlrt_marshallOut(
    "[ILXE]$matlabroot$/toolbox/eml/lib/matlab/eml/eml_scalar_eg.m"), "context",
                  "context", 2);
  sf_mex_addfield(*c48_info, c48_emlrt_marshallOut("coder.internal.scalarEg"),
                  "name", "name", 2);
  sf_mex_addfield(*c48_info, c48_emlrt_marshallOut("double"), "dominantType",
                  "dominantType", 2);
  sf_mex_addfield(*c48_info, c48_emlrt_marshallOut(
    "[ILXE]$matlabroot$/toolbox/coder/coder/+coder/+internal/scalarEg.p"),
                  "resolved", "resolved", 2);
  sf_mex_addfield(*c48_info, c48_b_emlrt_marshallOut(1410811370U), "fileTimeLo",
                  "fileTimeLo", 2);
  sf_mex_addfield(*c48_info, c48_b_emlrt_marshallOut(0U), "fileTimeHi",
                  "fileTimeHi", 2);
  sf_mex_addfield(*c48_info, c48_b_emlrt_marshallOut(0U), "mFileTimeLo",
                  "mFileTimeLo", 2);
  sf_mex_addfield(*c48_info, c48_b_emlrt_marshallOut(0U), "mFileTimeHi",
                  "mFileTimeHi", 2);
  sf_mex_assign(&c48_rhs2, sf_mex_createcellmatrix(0, 1), false);
  sf_mex_assign(&c48_lhs2, sf_mex_createcellmatrix(0, 1), false);
  sf_mex_addfield(*c48_info, sf_mex_duplicatearraysafe(&c48_rhs2), "rhs", "rhs",
                  2);
  sf_mex_addfield(*c48_info, sf_mex_duplicatearraysafe(&c48_lhs2), "lhs", "lhs",
                  2);
  sf_mex_addfield(*c48_info, c48_emlrt_marshallOut(
    "[ILXE]$matlabroot$/toolbox/eml/lib/matlab/elfun/atan2d.m"), "context",
                  "context", 3);
  sf_mex_addfield(*c48_info, c48_emlrt_marshallOut("eml_scalexp_alloc"), "name",
                  "name", 3);
  sf_mex_addfield(*c48_info, c48_emlrt_marshallOut("double"), "dominantType",
                  "dominantType", 3);
  sf_mex_addfield(*c48_info, c48_emlrt_marshallOut(
    "[ILXE]$matlabroot$/toolbox/eml/lib/matlab/eml/eml_scalexp_alloc.m"),
                  "resolved", "resolved", 3);
  sf_mex_addfield(*c48_info, c48_b_emlrt_marshallOut(1375984288U), "fileTimeLo",
                  "fileTimeLo", 3);
  sf_mex_addfield(*c48_info, c48_b_emlrt_marshallOut(0U), "fileTimeHi",
                  "fileTimeHi", 3);
  sf_mex_addfield(*c48_info, c48_b_emlrt_marshallOut(0U), "mFileTimeLo",
                  "mFileTimeLo", 3);
  sf_mex_addfield(*c48_info, c48_b_emlrt_marshallOut(0U), "mFileTimeHi",
                  "mFileTimeHi", 3);
  sf_mex_assign(&c48_rhs3, sf_mex_createcellmatrix(0, 1), false);
  sf_mex_assign(&c48_lhs3, sf_mex_createcellmatrix(0, 1), false);
  sf_mex_addfield(*c48_info, sf_mex_duplicatearraysafe(&c48_rhs3), "rhs", "rhs",
                  3);
  sf_mex_addfield(*c48_info, sf_mex_duplicatearraysafe(&c48_lhs3), "lhs", "lhs",
                  3);
  sf_mex_addfield(*c48_info, c48_emlrt_marshallOut(
    "[ILXE]$matlabroot$/toolbox/eml/lib/matlab/eml/eml_scalexp_alloc.m"),
                  "context", "context", 4);
  sf_mex_addfield(*c48_info, c48_emlrt_marshallOut("coder.internal.scalexpAlloc"),
                  "name", "name", 4);
  sf_mex_addfield(*c48_info, c48_emlrt_marshallOut("double"), "dominantType",
                  "dominantType", 4);
  sf_mex_addfield(*c48_info, c48_emlrt_marshallOut(
    "[ILXE]$matlabroot$/toolbox/coder/coder/+coder/+internal/scalexpAlloc.p"),
                  "resolved", "resolved", 4);
  sf_mex_addfield(*c48_info, c48_b_emlrt_marshallOut(1410811370U), "fileTimeLo",
                  "fileTimeLo", 4);
  sf_mex_addfield(*c48_info, c48_b_emlrt_marshallOut(0U), "fileTimeHi",
                  "fileTimeHi", 4);
  sf_mex_addfield(*c48_info, c48_b_emlrt_marshallOut(0U), "mFileTimeLo",
                  "mFileTimeLo", 4);
  sf_mex_addfield(*c48_info, c48_b_emlrt_marshallOut(0U), "mFileTimeHi",
                  "mFileTimeHi", 4);
  sf_mex_assign(&c48_rhs4, sf_mex_createcellmatrix(0, 1), false);
  sf_mex_assign(&c48_lhs4, sf_mex_createcellmatrix(0, 1), false);
  sf_mex_addfield(*c48_info, sf_mex_duplicatearraysafe(&c48_rhs4), "rhs", "rhs",
                  4);
  sf_mex_addfield(*c48_info, sf_mex_duplicatearraysafe(&c48_lhs4), "lhs", "lhs",
                  4);
  sf_mex_addfield(*c48_info, c48_emlrt_marshallOut(
    "[ILXE]$matlabroot$/toolbox/eml/lib/matlab/elfun/atan2d.m"), "context",
                  "context", 5);
  sf_mex_addfield(*c48_info, c48_emlrt_marshallOut("eml_scalar_atan2"), "name",
                  "name", 5);
  sf_mex_addfield(*c48_info, c48_emlrt_marshallOut("double"), "dominantType",
                  "dominantType", 5);
  sf_mex_addfield(*c48_info, c48_emlrt_marshallOut(
    "[ILXE]$matlabroot$/toolbox/eml/lib/matlab/elfun/eml_scalar_atan2.m"),
                  "resolved", "resolved", 5);
  sf_mex_addfield(*c48_info, c48_b_emlrt_marshallOut(1286822320U), "fileTimeLo",
                  "fileTimeLo", 5);
  sf_mex_addfield(*c48_info, c48_b_emlrt_marshallOut(0U), "fileTimeHi",
                  "fileTimeHi", 5);
  sf_mex_addfield(*c48_info, c48_b_emlrt_marshallOut(0U), "mFileTimeLo",
                  "mFileTimeLo", 5);
  sf_mex_addfield(*c48_info, c48_b_emlrt_marshallOut(0U), "mFileTimeHi",
                  "mFileTimeHi", 5);
  sf_mex_assign(&c48_rhs5, sf_mex_createcellmatrix(0, 1), false);
  sf_mex_assign(&c48_lhs5, sf_mex_createcellmatrix(0, 1), false);
  sf_mex_addfield(*c48_info, sf_mex_duplicatearraysafe(&c48_rhs5), "rhs", "rhs",
                  5);
  sf_mex_addfield(*c48_info, sf_mex_duplicatearraysafe(&c48_lhs5), "lhs", "lhs",
                  5);
  sf_mex_destroy(&c48_rhs0);
  sf_mex_destroy(&c48_lhs0);
  sf_mex_destroy(&c48_rhs1);
  sf_mex_destroy(&c48_lhs1);
  sf_mex_destroy(&c48_rhs2);
  sf_mex_destroy(&c48_lhs2);
  sf_mex_destroy(&c48_rhs3);
  sf_mex_destroy(&c48_lhs3);
  sf_mex_destroy(&c48_rhs4);
  sf_mex_destroy(&c48_lhs4);
  sf_mex_destroy(&c48_rhs5);
  sf_mex_destroy(&c48_lhs5);
}

static const mxArray *c48_emlrt_marshallOut(const char * c48_u)
{
  const mxArray *c48_y = NULL;
  c48_y = NULL;
  sf_mex_assign(&c48_y, sf_mex_create("y", c48_u, 15, 0U, 0U, 0U, 2, 1, strlen
    (c48_u)), false);
  return c48_y;
}

static const mxArray *c48_b_emlrt_marshallOut(const uint32_T c48_u)
{
  const mxArray *c48_y = NULL;
  c48_y = NULL;
  sf_mex_assign(&c48_y, sf_mex_create("y", &c48_u, 7, 0U, 0U, 0U, 0), false);
  return c48_y;
}

static const mxArray *c48_d_sf_marshallOut(void *chartInstanceVoid, void
  *c48_inData)
{
  const mxArray *c48_mxArrayOutData = NULL;
  int32_T c48_u;
  const mxArray *c48_y = NULL;
  SFc48_LessonIII_startInstanceStruct *chartInstance;
  chartInstance = (SFc48_LessonIII_startInstanceStruct *)chartInstanceVoid;
  c48_mxArrayOutData = NULL;
  c48_u = *(int32_T *)c48_inData;
  c48_y = NULL;
  sf_mex_assign(&c48_y, sf_mex_create("y", &c48_u, 6, 0U, 0U, 0U, 0), false);
  sf_mex_assign(&c48_mxArrayOutData, c48_y, false);
  return c48_mxArrayOutData;
}

static int32_T c48_d_emlrt_marshallIn(SFc48_LessonIII_startInstanceStruct
  *chartInstance, const mxArray *c48_u, const emlrtMsgIdentifier *c48_parentId)
{
  int32_T c48_y;
  int32_T c48_i10;
  (void)chartInstance;
  sf_mex_import(c48_parentId, sf_mex_dup(c48_u), &c48_i10, 1, 6, 0U, 0, 0U, 0);
  c48_y = c48_i10;
  sf_mex_destroy(&c48_u);
  return c48_y;
}

static void c48_c_sf_marshallIn(void *chartInstanceVoid, const mxArray
  *c48_mxArrayInData, const char_T *c48_varName, void *c48_outData)
{
  const mxArray *c48_b_sfEvent;
  const char_T *c48_identifier;
  emlrtMsgIdentifier c48_thisId;
  int32_T c48_y;
  SFc48_LessonIII_startInstanceStruct *chartInstance;
  chartInstance = (SFc48_LessonIII_startInstanceStruct *)chartInstanceVoid;
  c48_b_sfEvent = sf_mex_dup(c48_mxArrayInData);
  c48_identifier = c48_varName;
  c48_thisId.fIdentifier = c48_identifier;
  c48_thisId.fParent = NULL;
  c48_y = c48_d_emlrt_marshallIn(chartInstance, sf_mex_dup(c48_b_sfEvent),
    &c48_thisId);
  sf_mex_destroy(&c48_b_sfEvent);
  *(int32_T *)c48_outData = c48_y;
  sf_mex_destroy(&c48_mxArrayInData);
}

static uint8_T c48_e_emlrt_marshallIn(SFc48_LessonIII_startInstanceStruct
  *chartInstance, const mxArray *c48_b_is_active_c48_LessonIII_start, const
  char_T *c48_identifier)
{
  uint8_T c48_y;
  emlrtMsgIdentifier c48_thisId;
  c48_thisId.fIdentifier = c48_identifier;
  c48_thisId.fParent = NULL;
  c48_y = c48_f_emlrt_marshallIn(chartInstance, sf_mex_dup
    (c48_b_is_active_c48_LessonIII_start), &c48_thisId);
  sf_mex_destroy(&c48_b_is_active_c48_LessonIII_start);
  return c48_y;
}

static uint8_T c48_f_emlrt_marshallIn(SFc48_LessonIII_startInstanceStruct
  *chartInstance, const mxArray *c48_u, const emlrtMsgIdentifier *c48_parentId)
{
  uint8_T c48_y;
  uint8_T c48_u0;
  (void)chartInstance;
  sf_mex_import(c48_parentId, sf_mex_dup(c48_u), &c48_u0, 1, 3, 0U, 0, 0U, 0);
  c48_y = c48_u0;
  sf_mex_destroy(&c48_u);
  return c48_y;
}

static void init_dsm_address_info(SFc48_LessonIII_startInstanceStruct
  *chartInstance)
{
  (void)chartInstance;
}

static void init_simulink_io_address(SFc48_LessonIII_startInstanceStruct
  *chartInstance)
{
  chartInstance->c48_targetPos = (int8_T (*)[2])ssGetInputPortSignal_wrapper
    (chartInstance->S, 0);
  chartInstance->c48_targetOrientation = (int16_T *)
    ssGetOutputPortSignal_wrapper(chartInstance->S, 1);
  chartInstance->c48_myPos = (int8_T (*)[2])ssGetInputPortSignal_wrapper
    (chartInstance->S, 1);
}

/* SFunction Glue Code */
#ifdef utFree
#undef utFree
#endif

#ifdef utMalloc
#undef utMalloc
#endif

#ifdef __cplusplus

extern "C" void *utMalloc(size_t size);
extern "C" void utFree(void*);

#else

extern void *utMalloc(size_t size);
extern void utFree(void*);

#endif

void sf_c48_LessonIII_start_get_check_sum(mxArray *plhs[])
{
  ((real_T *)mxGetPr((plhs[0])))[0] = (real_T)(1745899133U);
  ((real_T *)mxGetPr((plhs[0])))[1] = (real_T)(3390497353U);
  ((real_T *)mxGetPr((plhs[0])))[2] = (real_T)(298628668U);
  ((real_T *)mxGetPr((plhs[0])))[3] = (real_T)(4230334289U);
}

mxArray* sf_c48_LessonIII_start_get_post_codegen_info(void);
mxArray *sf_c48_LessonIII_start_get_autoinheritance_info(void)
{
  const char *autoinheritanceFields[] = { "checksum", "inputs", "parameters",
    "outputs", "locals", "postCodegenInfo" };

  mxArray *mxAutoinheritanceInfo = mxCreateStructMatrix(1, 1, sizeof
    (autoinheritanceFields)/sizeof(autoinheritanceFields[0]),
    autoinheritanceFields);

  {
    mxArray *mxChecksum = mxCreateString("aeIe7Vb6eDndSoISjztQnH");
    mxSetField(mxAutoinheritanceInfo,0,"checksum",mxChecksum);
  }

  {
    const char *dataFields[] = { "size", "type", "complexity" };

    mxArray *mxData = mxCreateStructMatrix(1,2,3,dataFields);

    {
      mxArray *mxSize = mxCreateDoubleMatrix(1,2,mxREAL);
      double *pr = mxGetPr(mxSize);
      pr[0] = (double)(2);
      pr[1] = (double)(1);
      mxSetField(mxData,0,"size",mxSize);
    }

    {
      const char *typeFields[] = { "base", "fixpt" };

      mxArray *mxType = mxCreateStructMatrix(1,1,2,typeFields);
      mxSetField(mxType,0,"base",mxCreateDoubleScalar(4));
      mxSetField(mxType,0,"fixpt",mxCreateDoubleMatrix(0,0,mxREAL));
      mxSetField(mxData,0,"type",mxType);
    }

    mxSetField(mxData,0,"complexity",mxCreateDoubleScalar(0));

    {
      mxArray *mxSize = mxCreateDoubleMatrix(1,2,mxREAL);
      double *pr = mxGetPr(mxSize);
      pr[0] = (double)(2);
      pr[1] = (double)(1);
      mxSetField(mxData,1,"size",mxSize);
    }

    {
      const char *typeFields[] = { "base", "fixpt" };

      mxArray *mxType = mxCreateStructMatrix(1,1,2,typeFields);
      mxSetField(mxType,0,"base",mxCreateDoubleScalar(4));
      mxSetField(mxType,0,"fixpt",mxCreateDoubleMatrix(0,0,mxREAL));
      mxSetField(mxData,1,"type",mxType);
    }

    mxSetField(mxData,1,"complexity",mxCreateDoubleScalar(0));
    mxSetField(mxAutoinheritanceInfo,0,"inputs",mxData);
  }

  {
    mxSetField(mxAutoinheritanceInfo,0,"parameters",mxCreateDoubleMatrix(0,0,
                mxREAL));
  }

  {
    const char *dataFields[] = { "size", "type", "complexity" };

    mxArray *mxData = mxCreateStructMatrix(1,1,3,dataFields);

    {
      mxArray *mxSize = mxCreateDoubleMatrix(1,2,mxREAL);
      double *pr = mxGetPr(mxSize);
      pr[0] = (double)(1);
      pr[1] = (double)(1);
      mxSetField(mxData,0,"size",mxSize);
    }

    {
      const char *typeFields[] = { "base", "fixpt" };

      mxArray *mxType = mxCreateStructMatrix(1,1,2,typeFields);
      mxSetField(mxType,0,"base",mxCreateDoubleScalar(6));
      mxSetField(mxType,0,"fixpt",mxCreateDoubleMatrix(0,0,mxREAL));
      mxSetField(mxData,0,"type",mxType);
    }

    mxSetField(mxData,0,"complexity",mxCreateDoubleScalar(0));
    mxSetField(mxAutoinheritanceInfo,0,"outputs",mxData);
  }

  {
    mxSetField(mxAutoinheritanceInfo,0,"locals",mxCreateDoubleMatrix(0,0,mxREAL));
  }

  {
    mxArray* mxPostCodegenInfo = sf_c48_LessonIII_start_get_post_codegen_info();
    mxSetField(mxAutoinheritanceInfo,0,"postCodegenInfo",mxPostCodegenInfo);
  }

  return(mxAutoinheritanceInfo);
}

mxArray *sf_c48_LessonIII_start_third_party_uses_info(void)
{
  mxArray * mxcell3p = mxCreateCellMatrix(1,0);
  return(mxcell3p);
}

mxArray *sf_c48_LessonIII_start_jit_fallback_info(void)
{
  const char *infoFields[] = { "fallbackType", "fallbackReason",
    "incompatibleSymbol", };

  mxArray *mxInfo = mxCreateStructMatrix(1, 1, 3, infoFields);
  mxArray *fallbackReason = mxCreateString("feature_off");
  mxArray *incompatibleSymbol = mxCreateString("");
  mxArray *fallbackType = mxCreateString("early");
  mxSetField(mxInfo, 0, infoFields[0], fallbackType);
  mxSetField(mxInfo, 0, infoFields[1], fallbackReason);
  mxSetField(mxInfo, 0, infoFields[2], incompatibleSymbol);
  return mxInfo;
}

mxArray *sf_c48_LessonIII_start_updateBuildInfo_args_info(void)
{
  mxArray *mxBIArgs = mxCreateCellMatrix(1,0);
  return mxBIArgs;
}

mxArray* sf_c48_LessonIII_start_get_post_codegen_info(void)
{
  const char* fieldNames[] = { "exportedFunctionsUsedByThisChart",
    "exportedFunctionsChecksum" };

  mwSize dims[2] = { 1, 1 };

  mxArray* mxPostCodegenInfo = mxCreateStructArray(2, dims, sizeof(fieldNames)/
    sizeof(fieldNames[0]), fieldNames);

  {
    mxArray* mxExportedFunctionsChecksum = mxCreateString("");
    mwSize exp_dims[2] = { 0, 1 };

    mxArray* mxExportedFunctionsUsedByThisChart = mxCreateCellArray(2, exp_dims);
    mxSetField(mxPostCodegenInfo, 0, "exportedFunctionsUsedByThisChart",
               mxExportedFunctionsUsedByThisChart);
    mxSetField(mxPostCodegenInfo, 0, "exportedFunctionsChecksum",
               mxExportedFunctionsChecksum);
  }

  return mxPostCodegenInfo;
}

static const mxArray *sf_get_sim_state_info_c48_LessonIII_start(void)
{
  const char *infoFields[] = { "chartChecksum", "varInfo" };

  mxArray *mxInfo = mxCreateStructMatrix(1, 1, 2, infoFields);
  const char *infoEncStr[] = {
    "100 S1x2'type','srcId','name','auxInfo'{{M[1],M[5],T\"targetOrientation\",},{M[8],M[0],T\"is_active_c48_LessonIII_start\",}}"
  };

  mxArray *mxVarInfo = sf_mex_decode_encoded_mx_struct_array(infoEncStr, 2, 10);
  mxArray *mxChecksum = mxCreateDoubleMatrix(1, 4, mxREAL);
  sf_c48_LessonIII_start_get_check_sum(&mxChecksum);
  mxSetField(mxInfo, 0, infoFields[0], mxChecksum);
  mxSetField(mxInfo, 0, infoFields[1], mxVarInfo);
  return mxInfo;
}

static void chart_debug_initialization(SimStruct *S, unsigned int
  fullDebuggerInitialization)
{
  if (!sim_mode_is_rtw_gen(S)) {
    SFc48_LessonIII_startInstanceStruct *chartInstance;
    ChartRunTimeInfo * crtInfo = (ChartRunTimeInfo *)(ssGetUserData(S));
    ChartInfoStruct * chartInfo = (ChartInfoStruct *)(crtInfo->instanceInfo);
    chartInstance = (SFc48_LessonIII_startInstanceStruct *)
      chartInfo->chartInstance;
    if (ssIsFirstInitCond(S) && fullDebuggerInitialization==1) {
      /* do this only if simulation is starting */
      {
        unsigned int chartAlreadyPresent;
        chartAlreadyPresent = sf_debug_initialize_chart
          (sfGlobalDebugInstanceStruct,
           _LessonIII_startMachineNumber_,
           48,
           1,
           1,
           0,
           3,
           0,
           0,
           0,
           0,
           0,
           &(chartInstance->chartNumber),
           &(chartInstance->instanceNumber),
           (void *)S);

        /* Each instance must initialize its own list of scripts */
        init_script_number_translation(_LessonIII_startMachineNumber_,
          chartInstance->chartNumber,chartInstance->instanceNumber);
        if (chartAlreadyPresent==0) {
          /* this is the first instance */
          sf_debug_set_chart_disable_implicit_casting
            (sfGlobalDebugInstanceStruct,_LessonIII_startMachineNumber_,
             chartInstance->chartNumber,1);
          sf_debug_set_chart_event_thresholds(sfGlobalDebugInstanceStruct,
            _LessonIII_startMachineNumber_,
            chartInstance->chartNumber,
            0,
            0,
            0);
          _SFD_SET_DATA_PROPS(0,1,1,0,"targetPos");
          _SFD_SET_DATA_PROPS(1,2,0,1,"targetOrientation");
          _SFD_SET_DATA_PROPS(2,1,1,0,"myPos");
          _SFD_STATE_INFO(0,0,2);
          _SFD_CH_SUBSTATE_COUNT(0);
          _SFD_CH_SUBSTATE_DECOMP(0);
        }

        _SFD_CV_INIT_CHART(0,0,0,0);

        {
          _SFD_CV_INIT_STATE(0,0,0,0,0,0,NULL,NULL);
        }

        _SFD_CV_INIT_TRANS(0,0,NULL,NULL,0,NULL);

        /* Initialization of MATLAB Function Model Coverage */
        _SFD_CV_INIT_EML(0,1,1,0,0,3,0,0,0,0,0);
        _SFD_CV_INIT_EML_FCN(0,0,"eML_blk_kernel",0,-1,159);
        _SFD_CV_INIT_EML_SATURATION(0,1,0,132,-1,153);
        _SFD_CV_INIT_EML_SATURATION(0,1,1,74,-1,156);
        _SFD_CV_INIT_EML_SATURATION(0,1,2,94,-1,115);

        {
          unsigned int dimVector[2];
          dimVector[0]= 2;
          dimVector[1]= 1;
          _SFD_SET_DATA_COMPILED_PROPS(0,SF_INT8,2,&(dimVector[0]),0,0,0,0.0,1.0,
            0,0,(MexFcnForType)c48_b_sf_marshallOut,(MexInFcnForType)NULL);
        }

        _SFD_SET_DATA_COMPILED_PROPS(1,SF_INT16,0,NULL,0,0,0,0.0,1.0,0,0,
          (MexFcnForType)c48_sf_marshallOut,(MexInFcnForType)c48_sf_marshallIn);

        {
          unsigned int dimVector[2];
          dimVector[0]= 2;
          dimVector[1]= 1;
          _SFD_SET_DATA_COMPILED_PROPS(2,SF_INT8,2,&(dimVector[0]),0,0,0,0.0,1.0,
            0,0,(MexFcnForType)c48_b_sf_marshallOut,(MexInFcnForType)NULL);
        }

        _SFD_SET_DATA_VALUE_PTR(0U, *chartInstance->c48_targetPos);
        _SFD_SET_DATA_VALUE_PTR(1U, chartInstance->c48_targetOrientation);
        _SFD_SET_DATA_VALUE_PTR(2U, *chartInstance->c48_myPos);
      }
    } else {
      sf_debug_reset_current_state_configuration(sfGlobalDebugInstanceStruct,
        _LessonIII_startMachineNumber_,chartInstance->chartNumber,
        chartInstance->instanceNumber);
    }
  }
}

static const char* sf_get_instance_specialization(void)
{
  return "mTPa5Pzbe3w9l3J7cRjFdB";
}

static void sf_opaque_initialize_c48_LessonIII_start(void *chartInstanceVar)
{
  chart_debug_initialization(((SFc48_LessonIII_startInstanceStruct*)
    chartInstanceVar)->S,0);
  initialize_params_c48_LessonIII_start((SFc48_LessonIII_startInstanceStruct*)
    chartInstanceVar);
  initialize_c48_LessonIII_start((SFc48_LessonIII_startInstanceStruct*)
    chartInstanceVar);
}

static void sf_opaque_enable_c48_LessonIII_start(void *chartInstanceVar)
{
  enable_c48_LessonIII_start((SFc48_LessonIII_startInstanceStruct*)
    chartInstanceVar);
}

static void sf_opaque_disable_c48_LessonIII_start(void *chartInstanceVar)
{
  disable_c48_LessonIII_start((SFc48_LessonIII_startInstanceStruct*)
    chartInstanceVar);
}

static void sf_opaque_gateway_c48_LessonIII_start(void *chartInstanceVar)
{
  sf_gateway_c48_LessonIII_start((SFc48_LessonIII_startInstanceStruct*)
    chartInstanceVar);
}

static const mxArray* sf_opaque_get_sim_state_c48_LessonIII_start(SimStruct* S)
{
  ChartRunTimeInfo * crtInfo = (ChartRunTimeInfo *)(ssGetUserData(S));
  ChartInfoStruct * chartInfo = (ChartInfoStruct *)(crtInfo->instanceInfo);
  return get_sim_state_c48_LessonIII_start((SFc48_LessonIII_startInstanceStruct*)
    chartInfo->chartInstance);         /* raw sim ctx */
}

static void sf_opaque_set_sim_state_c48_LessonIII_start(SimStruct* S, const
  mxArray *st)
{
  ChartRunTimeInfo * crtInfo = (ChartRunTimeInfo *)(ssGetUserData(S));
  ChartInfoStruct * chartInfo = (ChartInfoStruct *)(crtInfo->instanceInfo);
  set_sim_state_c48_LessonIII_start((SFc48_LessonIII_startInstanceStruct*)
    chartInfo->chartInstance, st);
}

static void sf_opaque_terminate_c48_LessonIII_start(void *chartInstanceVar)
{
  if (chartInstanceVar!=NULL) {
    SimStruct *S = ((SFc48_LessonIII_startInstanceStruct*) chartInstanceVar)->S;
    ChartRunTimeInfo * crtInfo = (ChartRunTimeInfo *)(ssGetUserData(S));
    if (sim_mode_is_rtw_gen(S) || sim_mode_is_external(S)) {
      sf_clear_rtw_identifier(S);
      unload_LessonIII_start_optimization_info();
    }

    finalize_c48_LessonIII_start((SFc48_LessonIII_startInstanceStruct*)
      chartInstanceVar);
    utFree(chartInstanceVar);
    if (crtInfo != NULL) {
      utFree(crtInfo);
    }

    ssSetUserData(S,NULL);
  }
}

static void sf_opaque_init_subchart_simstructs(void *chartInstanceVar)
{
  initSimStructsc48_LessonIII_start((SFc48_LessonIII_startInstanceStruct*)
    chartInstanceVar);
}

extern unsigned int sf_machine_global_initializer_called(void);
static void mdlProcessParameters_c48_LessonIII_start(SimStruct *S)
{
  int i;
  for (i=0;i<ssGetNumRunTimeParams(S);i++) {
    if (ssGetSFcnParamTunable(S,i)) {
      ssUpdateDlgParamAsRunTimeParam(S,i);
    }
  }

  if (sf_machine_global_initializer_called()) {
    ChartRunTimeInfo * crtInfo = (ChartRunTimeInfo *)(ssGetUserData(S));
    ChartInfoStruct * chartInfo = (ChartInfoStruct *)(crtInfo->instanceInfo);
    initialize_params_c48_LessonIII_start((SFc48_LessonIII_startInstanceStruct*)
      (chartInfo->chartInstance));
  }
}

static void mdlSetWorkWidths_c48_LessonIII_start(SimStruct *S)
{
  if (sim_mode_is_rtw_gen(S) || sim_mode_is_external(S)) {
    mxArray *infoStruct = load_LessonIII_start_optimization_info();
    int_T chartIsInlinable =
      (int_T)sf_is_chart_inlinable(sf_get_instance_specialization(),infoStruct,
      48);
    ssSetStateflowIsInlinable(S,chartIsInlinable);
    ssSetRTWCG(S,sf_rtw_info_uint_prop(sf_get_instance_specialization(),
                infoStruct,48,"RTWCG"));
    ssSetEnableFcnIsTrivial(S,1);
    ssSetDisableFcnIsTrivial(S,1);
    ssSetNotMultipleInlinable(S,sf_rtw_info_uint_prop
      (sf_get_instance_specialization(),infoStruct,48,
       "gatewayCannotBeInlinedMultipleTimes"));
    sf_update_buildInfo(sf_get_instance_specialization(),infoStruct,48);
    if (chartIsInlinable) {
      ssSetInputPortOptimOpts(S, 0, SS_REUSABLE_AND_LOCAL);
      ssSetInputPortOptimOpts(S, 1, SS_REUSABLE_AND_LOCAL);
      sf_mark_chart_expressionable_inputs(S,sf_get_instance_specialization(),
        infoStruct,48,2);
      sf_mark_chart_reusable_outputs(S,sf_get_instance_specialization(),
        infoStruct,48,1);
    }

    {
      unsigned int outPortIdx;
      for (outPortIdx=1; outPortIdx<=1; ++outPortIdx) {
        ssSetOutputPortOptimizeInIR(S, outPortIdx, 1U);
      }
    }

    {
      unsigned int inPortIdx;
      for (inPortIdx=0; inPortIdx < 2; ++inPortIdx) {
        ssSetInputPortOptimizeInIR(S, inPortIdx, 1U);
      }
    }

    sf_set_rtw_dwork_info(S,sf_get_instance_specialization(),infoStruct,48);
    ssSetHasSubFunctions(S,!(chartIsInlinable));
  } else {
  }

  ssSetOptions(S,ssGetOptions(S)|SS_OPTION_WORKS_WITH_CODE_REUSE);
  ssSetChecksum0(S,(2930432160U));
  ssSetChecksum1(S,(727866723U));
  ssSetChecksum2(S,(28310003U));
  ssSetChecksum3(S,(1524288311U));
  ssSetmdlDerivatives(S, NULL);
  ssSetExplicitFCSSCtrl(S,1);
  ssSupportsMultipleExecInstances(S,1);
}

static void mdlRTW_c48_LessonIII_start(SimStruct *S)
{
  if (sim_mode_is_rtw_gen(S)) {
    ssWriteRTWStrParam(S, "StateflowChartType", "Embedded MATLAB");
  }
}

static void mdlStart_c48_LessonIII_start(SimStruct *S)
{
  SFc48_LessonIII_startInstanceStruct *chartInstance;
  ChartRunTimeInfo * crtInfo = (ChartRunTimeInfo *)utMalloc(sizeof
    (ChartRunTimeInfo));
  chartInstance = (SFc48_LessonIII_startInstanceStruct *)utMalloc(sizeof
    (SFc48_LessonIII_startInstanceStruct));
  memset(chartInstance, 0, sizeof(SFc48_LessonIII_startInstanceStruct));
  if (chartInstance==NULL) {
    sf_mex_error_message("Could not allocate memory for chart instance.");
  }

  chartInstance->chartInfo.chartInstance = chartInstance;
  chartInstance->chartInfo.isEMLChart = 1;
  chartInstance->chartInfo.chartInitialized = 0;
  chartInstance->chartInfo.sFunctionGateway =
    sf_opaque_gateway_c48_LessonIII_start;
  chartInstance->chartInfo.initializeChart =
    sf_opaque_initialize_c48_LessonIII_start;
  chartInstance->chartInfo.terminateChart =
    sf_opaque_terminate_c48_LessonIII_start;
  chartInstance->chartInfo.enableChart = sf_opaque_enable_c48_LessonIII_start;
  chartInstance->chartInfo.disableChart = sf_opaque_disable_c48_LessonIII_start;
  chartInstance->chartInfo.getSimState =
    sf_opaque_get_sim_state_c48_LessonIII_start;
  chartInstance->chartInfo.setSimState =
    sf_opaque_set_sim_state_c48_LessonIII_start;
  chartInstance->chartInfo.getSimStateInfo =
    sf_get_sim_state_info_c48_LessonIII_start;
  chartInstance->chartInfo.zeroCrossings = NULL;
  chartInstance->chartInfo.outputs = NULL;
  chartInstance->chartInfo.derivatives = NULL;
  chartInstance->chartInfo.mdlRTW = mdlRTW_c48_LessonIII_start;
  chartInstance->chartInfo.mdlStart = mdlStart_c48_LessonIII_start;
  chartInstance->chartInfo.mdlSetWorkWidths =
    mdlSetWorkWidths_c48_LessonIII_start;
  chartInstance->chartInfo.extModeExec = NULL;
  chartInstance->chartInfo.restoreLastMajorStepConfiguration = NULL;
  chartInstance->chartInfo.restoreBeforeLastMajorStepConfiguration = NULL;
  chartInstance->chartInfo.storeCurrentConfiguration = NULL;
  chartInstance->chartInfo.callAtomicSubchartUserFcn = NULL;
  chartInstance->chartInfo.callAtomicSubchartAutoFcn = NULL;
  chartInstance->chartInfo.debugInstance = sfGlobalDebugInstanceStruct;
  chartInstance->S = S;
  crtInfo->checksum = SF_RUNTIME_INFO_CHECKSUM;
  crtInfo->instanceInfo = (&(chartInstance->chartInfo));
  crtInfo->isJITEnabled = false;
  crtInfo->compiledInfo = NULL;
  ssSetUserData(S,(void *)(crtInfo));  /* register the chart instance with simstruct */
  init_dsm_address_info(chartInstance);
  init_simulink_io_address(chartInstance);
  if (!sim_mode_is_rtw_gen(S)) {
  }

  sf_opaque_init_subchart_simstructs(chartInstance->chartInfo.chartInstance);
  chart_debug_initialization(S,1);
}

void c48_LessonIII_start_method_dispatcher(SimStruct *S, int_T method, void
  *data)
{
  switch (method) {
   case SS_CALL_MDL_START:
    mdlStart_c48_LessonIII_start(S);
    break;

   case SS_CALL_MDL_SET_WORK_WIDTHS:
    mdlSetWorkWidths_c48_LessonIII_start(S);
    break;

   case SS_CALL_MDL_PROCESS_PARAMETERS:
    mdlProcessParameters_c48_LessonIII_start(S);
    break;

   default:
    /* Unhandled method */
    sf_mex_error_message("Stateflow Internal Error:\n"
                         "Error calling c48_LessonIII_start_method_dispatcher.\n"
                         "Can't handle method %d.\n", method);
    break;
  }
}

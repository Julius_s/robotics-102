/* Include files */

#include "LessonIII_sfun.h"
#include "LessonIII_sfun_debug_macros.h"
#include "c1_LessonIII.h"
#include "c2_LessonIII.h"
#include "c3_LessonIII.h"
#include "c4_LessonIII.h"
#include "c7_LessonIII.h"
#include "c8_LessonIII.h"
#include "c9_LessonIII.h"
#include "c10_LessonIII.h"
#include "c11_LessonIII.h"
#include "c12_LessonIII.h"
#include "c13_LessonIII.h"
#include "c17_LessonIII.h"
#include "c20_LessonIII.h"
#include "c21_LessonIII.h"
#include "c22_LessonIII.h"
#include "c34_LessonIII.h"

/* Type Definitions */

/* Named Constants */

/* Variable Declarations */

/* Variable Definitions */
uint32_T _LessonIIIMachineNumber_;

/* Function Declarations */

/* Function Definitions */
void LessonIII_initializer(void)
{
}

void LessonIII_terminator(void)
{
}

/* SFunction Glue Code */
unsigned int sf_LessonIII_method_dispatcher(SimStruct *simstructPtr, unsigned
  int chartFileNumber, const char* specsCksum, int_T method, void *data)
{
  if (chartFileNumber==1) {
    c1_LessonIII_method_dispatcher(simstructPtr, method, data);
    return 1;
  }

  if (chartFileNumber==2) {
    c2_LessonIII_method_dispatcher(simstructPtr, method, data);
    return 1;
  }

  if (chartFileNumber==3) {
    c3_LessonIII_method_dispatcher(simstructPtr, method, data);
    return 1;
  }

  if (chartFileNumber==4) {
    c4_LessonIII_method_dispatcher(simstructPtr, method, data);
    return 1;
  }

  if (chartFileNumber==7) {
    c7_LessonIII_method_dispatcher(simstructPtr, method, data);
    return 1;
  }

  if (chartFileNumber==8) {
    c8_LessonIII_method_dispatcher(simstructPtr, method, data);
    return 1;
  }

  if (chartFileNumber==9) {
    c9_LessonIII_method_dispatcher(simstructPtr, method, data);
    return 1;
  }

  if (chartFileNumber==10) {
    c10_LessonIII_method_dispatcher(simstructPtr, method, data);
    return 1;
  }

  if (chartFileNumber==11) {
    c11_LessonIII_method_dispatcher(simstructPtr, method, data);
    return 1;
  }

  if (chartFileNumber==12) {
    c12_LessonIII_method_dispatcher(simstructPtr, method, data);
    return 1;
  }

  if (chartFileNumber==13) {
    c13_LessonIII_method_dispatcher(simstructPtr, method, data);
    return 1;
  }

  if (chartFileNumber==17) {
    c17_LessonIII_method_dispatcher(simstructPtr, method, data);
    return 1;
  }

  if (chartFileNumber==20) {
    c20_LessonIII_method_dispatcher(simstructPtr, method, data);
    return 1;
  }

  if (chartFileNumber==21) {
    c21_LessonIII_method_dispatcher(simstructPtr, method, data);
    return 1;
  }

  if (chartFileNumber==22) {
    c22_LessonIII_method_dispatcher(simstructPtr, method, data);
    return 1;
  }

  if (chartFileNumber==34) {
    c34_LessonIII_method_dispatcher(simstructPtr, method, data);
    return 1;
  }

  return 0;
}

extern void sf_LessonIII_uses_exported_functions(int nlhs, mxArray * plhs[], int
  nrhs, const mxArray * prhs[])
{
  plhs[0] = mxCreateLogicalScalar(0);
}

unsigned int sf_LessonIII_process_check_sum_call( int nlhs, mxArray * plhs[],
  int nrhs, const mxArray * prhs[] )
{

#ifdef MATLAB_MEX_FILE

  char commandName[20];
  if (nrhs<1 || !mxIsChar(prhs[0]) )
    return 0;

  /* Possible call to get the checksum */
  mxGetString(prhs[0], commandName,sizeof(commandName)/sizeof(char));
  commandName[(sizeof(commandName)/sizeof(char)-1)] = '\0';
  if (strcmp(commandName,"sf_get_check_sum"))
    return 0;
  plhs[0] = mxCreateDoubleMatrix( 1,4,mxREAL);
  if (nrhs>1 && mxIsChar(prhs[1])) {
    mxGetString(prhs[1], commandName,sizeof(commandName)/sizeof(char));
    commandName[(sizeof(commandName)/sizeof(char)-1)] = '\0';
    if (!strcmp(commandName,"machine")) {
      ((real_T *)mxGetPr((plhs[0])))[0] = (real_T)(1189521737U);
      ((real_T *)mxGetPr((plhs[0])))[1] = (real_T)(3081108056U);
      ((real_T *)mxGetPr((plhs[0])))[2] = (real_T)(3445577031U);
      ((real_T *)mxGetPr((plhs[0])))[3] = (real_T)(3350862002U);
    } else if (!strcmp(commandName,"exportedFcn")) {
      ((real_T *)mxGetPr((plhs[0])))[0] = (real_T)(0U);
      ((real_T *)mxGetPr((plhs[0])))[1] = (real_T)(0U);
      ((real_T *)mxGetPr((plhs[0])))[2] = (real_T)(0U);
      ((real_T *)mxGetPr((plhs[0])))[3] = (real_T)(0U);
    } else if (!strcmp(commandName,"makefile")) {
      ((real_T *)mxGetPr((plhs[0])))[0] = (real_T)(2221681674U);
      ((real_T *)mxGetPr((plhs[0])))[1] = (real_T)(4021516515U);
      ((real_T *)mxGetPr((plhs[0])))[2] = (real_T)(1381713934U);
      ((real_T *)mxGetPr((plhs[0])))[3] = (real_T)(1696158251U);
    } else if (nrhs==3 && !strcmp(commandName,"chart")) {
      unsigned int chartFileNumber;
      chartFileNumber = (unsigned int)mxGetScalar(prhs[2]);
      switch (chartFileNumber) {
       case 1:
        {
          extern void sf_c1_LessonIII_get_check_sum(mxArray *plhs[]);
          sf_c1_LessonIII_get_check_sum(plhs);
          break;
        }

       case 2:
        {
          extern void sf_c2_LessonIII_get_check_sum(mxArray *plhs[]);
          sf_c2_LessonIII_get_check_sum(plhs);
          break;
        }

       case 3:
        {
          extern void sf_c3_LessonIII_get_check_sum(mxArray *plhs[]);
          sf_c3_LessonIII_get_check_sum(plhs);
          break;
        }

       case 4:
        {
          extern void sf_c4_LessonIII_get_check_sum(mxArray *plhs[]);
          sf_c4_LessonIII_get_check_sum(plhs);
          break;
        }

       case 7:
        {
          extern void sf_c7_LessonIII_get_check_sum(mxArray *plhs[]);
          sf_c7_LessonIII_get_check_sum(plhs);
          break;
        }

       case 8:
        {
          extern void sf_c8_LessonIII_get_check_sum(mxArray *plhs[]);
          sf_c8_LessonIII_get_check_sum(plhs);
          break;
        }

       case 9:
        {
          extern void sf_c9_LessonIII_get_check_sum(mxArray *plhs[]);
          sf_c9_LessonIII_get_check_sum(plhs);
          break;
        }

       case 10:
        {
          extern void sf_c10_LessonIII_get_check_sum(mxArray *plhs[]);
          sf_c10_LessonIII_get_check_sum(plhs);
          break;
        }

       case 11:
        {
          extern void sf_c11_LessonIII_get_check_sum(mxArray *plhs[]);
          sf_c11_LessonIII_get_check_sum(plhs);
          break;
        }

       case 12:
        {
          extern void sf_c12_LessonIII_get_check_sum(mxArray *plhs[]);
          sf_c12_LessonIII_get_check_sum(plhs);
          break;
        }

       case 13:
        {
          extern void sf_c13_LessonIII_get_check_sum(mxArray *plhs[]);
          sf_c13_LessonIII_get_check_sum(plhs);
          break;
        }

       case 17:
        {
          extern void sf_c17_LessonIII_get_check_sum(mxArray *plhs[]);
          sf_c17_LessonIII_get_check_sum(plhs);
          break;
        }

       case 20:
        {
          extern void sf_c20_LessonIII_get_check_sum(mxArray *plhs[]);
          sf_c20_LessonIII_get_check_sum(plhs);
          break;
        }

       case 21:
        {
          extern void sf_c21_LessonIII_get_check_sum(mxArray *plhs[]);
          sf_c21_LessonIII_get_check_sum(plhs);
          break;
        }

       case 22:
        {
          extern void sf_c22_LessonIII_get_check_sum(mxArray *plhs[]);
          sf_c22_LessonIII_get_check_sum(plhs);
          break;
        }

       case 34:
        {
          extern void sf_c34_LessonIII_get_check_sum(mxArray *plhs[]);
          sf_c34_LessonIII_get_check_sum(plhs);
          break;
        }

       default:
        ((real_T *)mxGetPr((plhs[0])))[0] = (real_T)(0.0);
        ((real_T *)mxGetPr((plhs[0])))[1] = (real_T)(0.0);
        ((real_T *)mxGetPr((plhs[0])))[2] = (real_T)(0.0);
        ((real_T *)mxGetPr((plhs[0])))[3] = (real_T)(0.0);
      }
    } else if (!strcmp(commandName,"target")) {
      ((real_T *)mxGetPr((plhs[0])))[0] = (real_T)(3061339410U);
      ((real_T *)mxGetPr((plhs[0])))[1] = (real_T)(1991824845U);
      ((real_T *)mxGetPr((plhs[0])))[2] = (real_T)(3599338742U);
      ((real_T *)mxGetPr((plhs[0])))[3] = (real_T)(2357874978U);
    } else {
      return 0;
    }
  } else {
    ((real_T *)mxGetPr((plhs[0])))[0] = (real_T)(503238284U);
    ((real_T *)mxGetPr((plhs[0])))[1] = (real_T)(1558008427U);
    ((real_T *)mxGetPr((plhs[0])))[2] = (real_T)(4139045807U);
    ((real_T *)mxGetPr((plhs[0])))[3] = (real_T)(1283132379U);
  }

  return 1;

#else

  return 0;

#endif

}

unsigned int sf_LessonIII_autoinheritance_info( int nlhs, mxArray * plhs[], int
  nrhs, const mxArray * prhs[] )
{

#ifdef MATLAB_MEX_FILE

  char commandName[32];
  char aiChksum[64];
  if (nrhs<3 || !mxIsChar(prhs[0]) )
    return 0;

  /* Possible call to get the autoinheritance_info */
  mxGetString(prhs[0], commandName,sizeof(commandName)/sizeof(char));
  commandName[(sizeof(commandName)/sizeof(char)-1)] = '\0';
  if (strcmp(commandName,"get_autoinheritance_info"))
    return 0;
  mxGetString(prhs[2], aiChksum,sizeof(aiChksum)/sizeof(char));
  aiChksum[(sizeof(aiChksum)/sizeof(char)-1)] = '\0';

  {
    unsigned int chartFileNumber;
    chartFileNumber = (unsigned int)mxGetScalar(prhs[1]);
    switch (chartFileNumber) {
     case 1:
      {
        if (strcmp(aiChksum, "y6Xs4WcS8WLRGoiPgg9VIE") == 0) {
          extern mxArray *sf_c1_LessonIII_get_autoinheritance_info(void);
          plhs[0] = sf_c1_LessonIII_get_autoinheritance_info();
          break;
        }

        plhs[0] = mxCreateDoubleMatrix(0,0,mxREAL);
        break;
      }

     case 2:
      {
        if (strcmp(aiChksum, "kHfYduJyIVyU5ctHVH1c5") == 0) {
          extern mxArray *sf_c2_LessonIII_get_autoinheritance_info(void);
          plhs[0] = sf_c2_LessonIII_get_autoinheritance_info();
          break;
        }

        plhs[0] = mxCreateDoubleMatrix(0,0,mxREAL);
        break;
      }

     case 3:
      {
        if (strcmp(aiChksum, "UdXCtx3vKeGTbSGWNIJsGB") == 0) {
          extern mxArray *sf_c3_LessonIII_get_autoinheritance_info(void);
          plhs[0] = sf_c3_LessonIII_get_autoinheritance_info();
          break;
        }

        plhs[0] = mxCreateDoubleMatrix(0,0,mxREAL);
        break;
      }

     case 4:
      {
        if (strcmp(aiChksum, "7xpZWBSL3O26qhrkPeIjhE") == 0) {
          extern mxArray *sf_c4_LessonIII_get_autoinheritance_info(void);
          plhs[0] = sf_c4_LessonIII_get_autoinheritance_info();
          break;
        }

        plhs[0] = mxCreateDoubleMatrix(0,0,mxREAL);
        break;
      }

     case 7:
      {
        if (strcmp(aiChksum, "W9IV4u3yFE0kAKQHwULBKC") == 0) {
          extern mxArray *sf_c7_LessonIII_get_autoinheritance_info(void);
          plhs[0] = sf_c7_LessonIII_get_autoinheritance_info();
          break;
        }

        plhs[0] = mxCreateDoubleMatrix(0,0,mxREAL);
        break;
      }

     case 8:
      {
        if (strcmp(aiChksum, "XEXsRmqFscIqZkSUMcSdQF") == 0) {
          extern mxArray *sf_c8_LessonIII_get_autoinheritance_info(void);
          plhs[0] = sf_c8_LessonIII_get_autoinheritance_info();
          break;
        }

        plhs[0] = mxCreateDoubleMatrix(0,0,mxREAL);
        break;
      }

     case 9:
      {
        if (strcmp(aiChksum, "ArIrdR8KduvZC6FfJiWpp") == 0) {
          extern mxArray *sf_c9_LessonIII_get_autoinheritance_info(void);
          plhs[0] = sf_c9_LessonIII_get_autoinheritance_info();
          break;
        }

        plhs[0] = mxCreateDoubleMatrix(0,0,mxREAL);
        break;
      }

     case 10:
      {
        if (strcmp(aiChksum, "SWDcea26Rp8EjP7oloSyNC") == 0) {
          extern mxArray *sf_c10_LessonIII_get_autoinheritance_info(void);
          plhs[0] = sf_c10_LessonIII_get_autoinheritance_info();
          break;
        }

        plhs[0] = mxCreateDoubleMatrix(0,0,mxREAL);
        break;
      }

     case 11:
      {
        if (strcmp(aiChksum, "w74dGpMhbzSYFg9BBO2f9D") == 0) {
          extern mxArray *sf_c11_LessonIII_get_autoinheritance_info(void);
          plhs[0] = sf_c11_LessonIII_get_autoinheritance_info();
          break;
        }

        plhs[0] = mxCreateDoubleMatrix(0,0,mxREAL);
        break;
      }

     case 12:
      {
        if (strcmp(aiChksum, "d45jyKSJodkQaDmuPJupxG") == 0) {
          extern mxArray *sf_c12_LessonIII_get_autoinheritance_info(void);
          plhs[0] = sf_c12_LessonIII_get_autoinheritance_info();
          break;
        }

        plhs[0] = mxCreateDoubleMatrix(0,0,mxREAL);
        break;
      }

     case 13:
      {
        if (strcmp(aiChksum, "avMqIanoQY6XoECCReQu1D") == 0) {
          extern mxArray *sf_c13_LessonIII_get_autoinheritance_info(void);
          plhs[0] = sf_c13_LessonIII_get_autoinheritance_info();
          break;
        }

        plhs[0] = mxCreateDoubleMatrix(0,0,mxREAL);
        break;
      }

     case 17:
      {
        if (strcmp(aiChksum, "dvW3XiPYLp9ZM0lH4FZx7") == 0) {
          extern mxArray *sf_c17_LessonIII_get_autoinheritance_info(void);
          plhs[0] = sf_c17_LessonIII_get_autoinheritance_info();
          break;
        }

        plhs[0] = mxCreateDoubleMatrix(0,0,mxREAL);
        break;
      }

     case 20:
      {
        if (strcmp(aiChksum, "7ivw4cCNB5MBh41uVb3SPB") == 0) {
          extern mxArray *sf_c20_LessonIII_get_autoinheritance_info(void);
          plhs[0] = sf_c20_LessonIII_get_autoinheritance_info();
          break;
        }

        plhs[0] = mxCreateDoubleMatrix(0,0,mxREAL);
        break;
      }

     case 21:
      {
        if (strcmp(aiChksum, "aeIe7Vb6eDndSoISjztQnH") == 0) {
          extern mxArray *sf_c21_LessonIII_get_autoinheritance_info(void);
          plhs[0] = sf_c21_LessonIII_get_autoinheritance_info();
          break;
        }

        plhs[0] = mxCreateDoubleMatrix(0,0,mxREAL);
        break;
      }

     case 22:
      {
        if (strcmp(aiChksum, "xTDZbH0g8R2TeJ7jn9aNrF") == 0) {
          extern mxArray *sf_c22_LessonIII_get_autoinheritance_info(void);
          plhs[0] = sf_c22_LessonIII_get_autoinheritance_info();
          break;
        }

        plhs[0] = mxCreateDoubleMatrix(0,0,mxREAL);
        break;
      }

     case 34:
      {
        if (strcmp(aiChksum, "fMAGRcG0QbMVqNYw4znGTF") == 0) {
          extern mxArray *sf_c34_LessonIII_get_autoinheritance_info(void);
          plhs[0] = sf_c34_LessonIII_get_autoinheritance_info();
          break;
        }

        plhs[0] = mxCreateDoubleMatrix(0,0,mxREAL);
        break;
      }

     default:
      plhs[0] = mxCreateDoubleMatrix(0,0,mxREAL);
    }
  }

  return 1;

#else

  return 0;

#endif

}

unsigned int sf_LessonIII_get_eml_resolved_functions_info( int nlhs, mxArray *
  plhs[], int nrhs, const mxArray * prhs[] )
{

#ifdef MATLAB_MEX_FILE

  char commandName[64];
  if (nrhs<2 || !mxIsChar(prhs[0]))
    return 0;

  /* Possible call to get the get_eml_resolved_functions_info */
  mxGetString(prhs[0], commandName,sizeof(commandName)/sizeof(char));
  commandName[(sizeof(commandName)/sizeof(char)-1)] = '\0';
  if (strcmp(commandName,"get_eml_resolved_functions_info"))
    return 0;

  {
    unsigned int chartFileNumber;
    chartFileNumber = (unsigned int)mxGetScalar(prhs[1]);
    switch (chartFileNumber) {
     case 1:
      {
        extern const mxArray *sf_c1_LessonIII_get_eml_resolved_functions_info
          (void);
        mxArray *persistentMxArray = (mxArray *)
          sf_c1_LessonIII_get_eml_resolved_functions_info();
        plhs[0] = mxDuplicateArray(persistentMxArray);
        mxDestroyArray(persistentMxArray);
        break;
      }

     case 2:
      {
        extern const mxArray *sf_c2_LessonIII_get_eml_resolved_functions_info
          (void);
        mxArray *persistentMxArray = (mxArray *)
          sf_c2_LessonIII_get_eml_resolved_functions_info();
        plhs[0] = mxDuplicateArray(persistentMxArray);
        mxDestroyArray(persistentMxArray);
        break;
      }

     case 3:
      {
        extern const mxArray *sf_c3_LessonIII_get_eml_resolved_functions_info
          (void);
        mxArray *persistentMxArray = (mxArray *)
          sf_c3_LessonIII_get_eml_resolved_functions_info();
        plhs[0] = mxDuplicateArray(persistentMxArray);
        mxDestroyArray(persistentMxArray);
        break;
      }

     case 4:
      {
        extern const mxArray *sf_c4_LessonIII_get_eml_resolved_functions_info
          (void);
        mxArray *persistentMxArray = (mxArray *)
          sf_c4_LessonIII_get_eml_resolved_functions_info();
        plhs[0] = mxDuplicateArray(persistentMxArray);
        mxDestroyArray(persistentMxArray);
        break;
      }

     case 7:
      {
        extern const mxArray *sf_c7_LessonIII_get_eml_resolved_functions_info
          (void);
        mxArray *persistentMxArray = (mxArray *)
          sf_c7_LessonIII_get_eml_resolved_functions_info();
        plhs[0] = mxDuplicateArray(persistentMxArray);
        mxDestroyArray(persistentMxArray);
        break;
      }

     case 8:
      {
        extern const mxArray *sf_c8_LessonIII_get_eml_resolved_functions_info
          (void);
        mxArray *persistentMxArray = (mxArray *)
          sf_c8_LessonIII_get_eml_resolved_functions_info();
        plhs[0] = mxDuplicateArray(persistentMxArray);
        mxDestroyArray(persistentMxArray);
        break;
      }

     case 9:
      {
        extern const mxArray *sf_c9_LessonIII_get_eml_resolved_functions_info
          (void);
        mxArray *persistentMxArray = (mxArray *)
          sf_c9_LessonIII_get_eml_resolved_functions_info();
        plhs[0] = mxDuplicateArray(persistentMxArray);
        mxDestroyArray(persistentMxArray);
        break;
      }

     case 10:
      {
        extern const mxArray *sf_c10_LessonIII_get_eml_resolved_functions_info
          (void);
        mxArray *persistentMxArray = (mxArray *)
          sf_c10_LessonIII_get_eml_resolved_functions_info();
        plhs[0] = mxDuplicateArray(persistentMxArray);
        mxDestroyArray(persistentMxArray);
        break;
      }

     case 11:
      {
        extern const mxArray *sf_c11_LessonIII_get_eml_resolved_functions_info
          (void);
        mxArray *persistentMxArray = (mxArray *)
          sf_c11_LessonIII_get_eml_resolved_functions_info();
        plhs[0] = mxDuplicateArray(persistentMxArray);
        mxDestroyArray(persistentMxArray);
        break;
      }

     case 12:
      {
        extern const mxArray *sf_c12_LessonIII_get_eml_resolved_functions_info
          (void);
        mxArray *persistentMxArray = (mxArray *)
          sf_c12_LessonIII_get_eml_resolved_functions_info();
        plhs[0] = mxDuplicateArray(persistentMxArray);
        mxDestroyArray(persistentMxArray);
        break;
      }

     case 13:
      {
        extern const mxArray *sf_c13_LessonIII_get_eml_resolved_functions_info
          (void);
        mxArray *persistentMxArray = (mxArray *)
          sf_c13_LessonIII_get_eml_resolved_functions_info();
        plhs[0] = mxDuplicateArray(persistentMxArray);
        mxDestroyArray(persistentMxArray);
        break;
      }

     case 17:
      {
        extern const mxArray *sf_c17_LessonIII_get_eml_resolved_functions_info
          (void);
        mxArray *persistentMxArray = (mxArray *)
          sf_c17_LessonIII_get_eml_resolved_functions_info();
        plhs[0] = mxDuplicateArray(persistentMxArray);
        mxDestroyArray(persistentMxArray);
        break;
      }

     case 20:
      {
        extern const mxArray *sf_c20_LessonIII_get_eml_resolved_functions_info
          (void);
        mxArray *persistentMxArray = (mxArray *)
          sf_c20_LessonIII_get_eml_resolved_functions_info();
        plhs[0] = mxDuplicateArray(persistentMxArray);
        mxDestroyArray(persistentMxArray);
        break;
      }

     case 21:
      {
        extern const mxArray *sf_c21_LessonIII_get_eml_resolved_functions_info
          (void);
        mxArray *persistentMxArray = (mxArray *)
          sf_c21_LessonIII_get_eml_resolved_functions_info();
        plhs[0] = mxDuplicateArray(persistentMxArray);
        mxDestroyArray(persistentMxArray);
        break;
      }

     case 22:
      {
        extern const mxArray *sf_c22_LessonIII_get_eml_resolved_functions_info
          (void);
        mxArray *persistentMxArray = (mxArray *)
          sf_c22_LessonIII_get_eml_resolved_functions_info();
        plhs[0] = mxDuplicateArray(persistentMxArray);
        mxDestroyArray(persistentMxArray);
        break;
      }

     case 34:
      {
        extern const mxArray *sf_c34_LessonIII_get_eml_resolved_functions_info
          (void);
        mxArray *persistentMxArray = (mxArray *)
          sf_c34_LessonIII_get_eml_resolved_functions_info();
        plhs[0] = mxDuplicateArray(persistentMxArray);
        mxDestroyArray(persistentMxArray);
        break;
      }

     default:
      plhs[0] = mxCreateDoubleMatrix(0,0,mxREAL);
    }
  }

  return 1;

#else

  return 0;

#endif

}

unsigned int sf_LessonIII_third_party_uses_info( int nlhs, mxArray * plhs[], int
  nrhs, const mxArray * prhs[] )
{
  char commandName[64];
  char tpChksum[64];
  if (nrhs<3 || !mxIsChar(prhs[0]))
    return 0;

  /* Possible call to get the third_party_uses_info */
  mxGetString(prhs[0], commandName,sizeof(commandName)/sizeof(char));
  commandName[(sizeof(commandName)/sizeof(char)-1)] = '\0';
  mxGetString(prhs[2], tpChksum,sizeof(tpChksum)/sizeof(char));
  tpChksum[(sizeof(tpChksum)/sizeof(char)-1)] = '\0';
  if (strcmp(commandName,"get_third_party_uses_info"))
    return 0;

  {
    unsigned int chartFileNumber;
    chartFileNumber = (unsigned int)mxGetScalar(prhs[1]);
    switch (chartFileNumber) {
     case 1:
      {
        if (strcmp(tpChksum, "77qs0O1aFQP9R5KVWKglD") == 0) {
          extern mxArray *sf_c1_LessonIII_third_party_uses_info(void);
          plhs[0] = sf_c1_LessonIII_third_party_uses_info();
          break;
        }
      }

     case 2:
      {
        if (strcmp(tpChksum, "BPbr6mnccR4iiqbbOejEIC") == 0) {
          extern mxArray *sf_c2_LessonIII_third_party_uses_info(void);
          plhs[0] = sf_c2_LessonIII_third_party_uses_info();
          break;
        }
      }

     case 3:
      {
        if (strcmp(tpChksum, "gblOQFxJdbh8vdZcLHybtE") == 0) {
          extern mxArray *sf_c3_LessonIII_third_party_uses_info(void);
          plhs[0] = sf_c3_LessonIII_third_party_uses_info();
          break;
        }
      }

     case 4:
      {
        if (strcmp(tpChksum, "gyIrbHzC3iCzVLSSw6nCSD") == 0) {
          extern mxArray *sf_c4_LessonIII_third_party_uses_info(void);
          plhs[0] = sf_c4_LessonIII_third_party_uses_info();
          break;
        }
      }

     case 7:
      {
        if (strcmp(tpChksum, "3m7J7bSuKWROPt7dyFpV5E") == 0) {
          extern mxArray *sf_c7_LessonIII_third_party_uses_info(void);
          plhs[0] = sf_c7_LessonIII_third_party_uses_info();
          break;
        }
      }

     case 8:
      {
        if (strcmp(tpChksum, "Jbiqze4DlRPbPmbIL0wS8F") == 0) {
          extern mxArray *sf_c8_LessonIII_third_party_uses_info(void);
          plhs[0] = sf_c8_LessonIII_third_party_uses_info();
          break;
        }
      }

     case 9:
      {
        if (strcmp(tpChksum, "SafIxCjmM9qEPS6BmID8AG") == 0) {
          extern mxArray *sf_c9_LessonIII_third_party_uses_info(void);
          plhs[0] = sf_c9_LessonIII_third_party_uses_info();
          break;
        }
      }

     case 10:
      {
        if (strcmp(tpChksum, "8fzVJxuAcigplLDM8puMmC") == 0) {
          extern mxArray *sf_c10_LessonIII_third_party_uses_info(void);
          plhs[0] = sf_c10_LessonIII_third_party_uses_info();
          break;
        }
      }

     case 11:
      {
        if (strcmp(tpChksum, "lSC5a8XYA6cZdj3x6TQDLH") == 0) {
          extern mxArray *sf_c11_LessonIII_third_party_uses_info(void);
          plhs[0] = sf_c11_LessonIII_third_party_uses_info();
          break;
        }
      }

     case 12:
      {
        if (strcmp(tpChksum, "9sXAisX9DlmicJiI3Thqs") == 0) {
          extern mxArray *sf_c12_LessonIII_third_party_uses_info(void);
          plhs[0] = sf_c12_LessonIII_third_party_uses_info();
          break;
        }
      }

     case 13:
      {
        if (strcmp(tpChksum, "ynY5ttWD0J2n5VW3gZiZPC") == 0) {
          extern mxArray *sf_c13_LessonIII_third_party_uses_info(void);
          plhs[0] = sf_c13_LessonIII_third_party_uses_info();
          break;
        }
      }

     case 17:
      {
        if (strcmp(tpChksum, "17qto9Q0GZg3Br64LuRYvC") == 0) {
          extern mxArray *sf_c17_LessonIII_third_party_uses_info(void);
          plhs[0] = sf_c17_LessonIII_third_party_uses_info();
          break;
        }
      }

     case 20:
      {
        if (strcmp(tpChksum, "iLKkTZfgeUWbCHGkvxHUyF") == 0) {
          extern mxArray *sf_c20_LessonIII_third_party_uses_info(void);
          plhs[0] = sf_c20_LessonIII_third_party_uses_info();
          break;
        }
      }

     case 21:
      {
        if (strcmp(tpChksum, "mTPa5Pzbe3w9l3J7cRjFdB") == 0) {
          extern mxArray *sf_c21_LessonIII_third_party_uses_info(void);
          plhs[0] = sf_c21_LessonIII_third_party_uses_info();
          break;
        }
      }

     case 22:
      {
        if (strcmp(tpChksum, "Rlx5hbPfiijZHfd9WQUKiG") == 0) {
          extern mxArray *sf_c22_LessonIII_third_party_uses_info(void);
          plhs[0] = sf_c22_LessonIII_third_party_uses_info();
          break;
        }
      }

     case 34:
      {
        if (strcmp(tpChksum, "rUtkHx5PMrq2fFWAaHOP0E") == 0) {
          extern mxArray *sf_c34_LessonIII_third_party_uses_info(void);
          plhs[0] = sf_c34_LessonIII_third_party_uses_info();
          break;
        }
      }

     default:
      plhs[0] = mxCreateDoubleMatrix(0,0,mxREAL);
    }
  }

  return 1;
}

unsigned int sf_LessonIII_jit_fallback_info( int nlhs, mxArray * plhs[], int
  nrhs, const mxArray * prhs[] )
{
  char commandName[64];
  char tpChksum[64];
  if (nrhs<3 || !mxIsChar(prhs[0]))
    return 0;

  /* Possible call to get the jit_fallback_info */
  mxGetString(prhs[0], commandName,sizeof(commandName)/sizeof(char));
  commandName[(sizeof(commandName)/sizeof(char)-1)] = '\0';
  mxGetString(prhs[2], tpChksum,sizeof(tpChksum)/sizeof(char));
  tpChksum[(sizeof(tpChksum)/sizeof(char)-1)] = '\0';
  if (strcmp(commandName,"get_jit_fallback_info"))
    return 0;

  {
    unsigned int chartFileNumber;
    chartFileNumber = (unsigned int)mxGetScalar(prhs[1]);
    switch (chartFileNumber) {
     case 1:
      {
        if (strcmp(tpChksum, "77qs0O1aFQP9R5KVWKglD") == 0) {
          extern mxArray *sf_c1_LessonIII_jit_fallback_info(void);
          plhs[0] = sf_c1_LessonIII_jit_fallback_info();
          break;
        }
      }

     case 2:
      {
        if (strcmp(tpChksum, "BPbr6mnccR4iiqbbOejEIC") == 0) {
          extern mxArray *sf_c2_LessonIII_jit_fallback_info(void);
          plhs[0] = sf_c2_LessonIII_jit_fallback_info();
          break;
        }
      }

     case 3:
      {
        if (strcmp(tpChksum, "gblOQFxJdbh8vdZcLHybtE") == 0) {
          extern mxArray *sf_c3_LessonIII_jit_fallback_info(void);
          plhs[0] = sf_c3_LessonIII_jit_fallback_info();
          break;
        }
      }

     case 4:
      {
        if (strcmp(tpChksum, "gyIrbHzC3iCzVLSSw6nCSD") == 0) {
          extern mxArray *sf_c4_LessonIII_jit_fallback_info(void);
          plhs[0] = sf_c4_LessonIII_jit_fallback_info();
          break;
        }
      }

     case 7:
      {
        if (strcmp(tpChksum, "3m7J7bSuKWROPt7dyFpV5E") == 0) {
          extern mxArray *sf_c7_LessonIII_jit_fallback_info(void);
          plhs[0] = sf_c7_LessonIII_jit_fallback_info();
          break;
        }
      }

     case 8:
      {
        if (strcmp(tpChksum, "Jbiqze4DlRPbPmbIL0wS8F") == 0) {
          extern mxArray *sf_c8_LessonIII_jit_fallback_info(void);
          plhs[0] = sf_c8_LessonIII_jit_fallback_info();
          break;
        }
      }

     case 9:
      {
        if (strcmp(tpChksum, "SafIxCjmM9qEPS6BmID8AG") == 0) {
          extern mxArray *sf_c9_LessonIII_jit_fallback_info(void);
          plhs[0] = sf_c9_LessonIII_jit_fallback_info();
          break;
        }
      }

     case 10:
      {
        if (strcmp(tpChksum, "8fzVJxuAcigplLDM8puMmC") == 0) {
          extern mxArray *sf_c10_LessonIII_jit_fallback_info(void);
          plhs[0] = sf_c10_LessonIII_jit_fallback_info();
          break;
        }
      }

     case 11:
      {
        if (strcmp(tpChksum, "lSC5a8XYA6cZdj3x6TQDLH") == 0) {
          extern mxArray *sf_c11_LessonIII_jit_fallback_info(void);
          plhs[0] = sf_c11_LessonIII_jit_fallback_info();
          break;
        }
      }

     case 12:
      {
        if (strcmp(tpChksum, "9sXAisX9DlmicJiI3Thqs") == 0) {
          extern mxArray *sf_c12_LessonIII_jit_fallback_info(void);
          plhs[0] = sf_c12_LessonIII_jit_fallback_info();
          break;
        }
      }

     case 13:
      {
        if (strcmp(tpChksum, "ynY5ttWD0J2n5VW3gZiZPC") == 0) {
          extern mxArray *sf_c13_LessonIII_jit_fallback_info(void);
          plhs[0] = sf_c13_LessonIII_jit_fallback_info();
          break;
        }
      }

     case 17:
      {
        if (strcmp(tpChksum, "17qto9Q0GZg3Br64LuRYvC") == 0) {
          extern mxArray *sf_c17_LessonIII_jit_fallback_info(void);
          plhs[0] = sf_c17_LessonIII_jit_fallback_info();
          break;
        }
      }

     case 20:
      {
        if (strcmp(tpChksum, "iLKkTZfgeUWbCHGkvxHUyF") == 0) {
          extern mxArray *sf_c20_LessonIII_jit_fallback_info(void);
          plhs[0] = sf_c20_LessonIII_jit_fallback_info();
          break;
        }
      }

     case 21:
      {
        if (strcmp(tpChksum, "mTPa5Pzbe3w9l3J7cRjFdB") == 0) {
          extern mxArray *sf_c21_LessonIII_jit_fallback_info(void);
          plhs[0] = sf_c21_LessonIII_jit_fallback_info();
          break;
        }
      }

     case 22:
      {
        if (strcmp(tpChksum, "Rlx5hbPfiijZHfd9WQUKiG") == 0) {
          extern mxArray *sf_c22_LessonIII_jit_fallback_info(void);
          plhs[0] = sf_c22_LessonIII_jit_fallback_info();
          break;
        }
      }

     case 34:
      {
        if (strcmp(tpChksum, "rUtkHx5PMrq2fFWAaHOP0E") == 0) {
          extern mxArray *sf_c34_LessonIII_jit_fallback_info(void);
          plhs[0] = sf_c34_LessonIII_jit_fallback_info();
          break;
        }
      }

     default:
      plhs[0] = mxCreateDoubleMatrix(0,0,mxREAL);
    }
  }

  return 1;
}

unsigned int sf_LessonIII_updateBuildInfo_args_info( int nlhs, mxArray * plhs[],
  int nrhs, const mxArray * prhs[] )
{
  char commandName[64];
  char tpChksum[64];
  if (nrhs<3 || !mxIsChar(prhs[0]))
    return 0;

  /* Possible call to get the updateBuildInfo_args_info */
  mxGetString(prhs[0], commandName,sizeof(commandName)/sizeof(char));
  commandName[(sizeof(commandName)/sizeof(char)-1)] = '\0';
  mxGetString(prhs[2], tpChksum,sizeof(tpChksum)/sizeof(char));
  tpChksum[(sizeof(tpChksum)/sizeof(char)-1)] = '\0';
  if (strcmp(commandName,"get_updateBuildInfo_args_info"))
    return 0;

  {
    unsigned int chartFileNumber;
    chartFileNumber = (unsigned int)mxGetScalar(prhs[1]);
    switch (chartFileNumber) {
     case 1:
      {
        if (strcmp(tpChksum, "77qs0O1aFQP9R5KVWKglD") == 0) {
          extern mxArray *sf_c1_LessonIII_updateBuildInfo_args_info(void);
          plhs[0] = sf_c1_LessonIII_updateBuildInfo_args_info();
          break;
        }
      }

     case 2:
      {
        if (strcmp(tpChksum, "BPbr6mnccR4iiqbbOejEIC") == 0) {
          extern mxArray *sf_c2_LessonIII_updateBuildInfo_args_info(void);
          plhs[0] = sf_c2_LessonIII_updateBuildInfo_args_info();
          break;
        }
      }

     case 3:
      {
        if (strcmp(tpChksum, "gblOQFxJdbh8vdZcLHybtE") == 0) {
          extern mxArray *sf_c3_LessonIII_updateBuildInfo_args_info(void);
          plhs[0] = sf_c3_LessonIII_updateBuildInfo_args_info();
          break;
        }
      }

     case 4:
      {
        if (strcmp(tpChksum, "gyIrbHzC3iCzVLSSw6nCSD") == 0) {
          extern mxArray *sf_c4_LessonIII_updateBuildInfo_args_info(void);
          plhs[0] = sf_c4_LessonIII_updateBuildInfo_args_info();
          break;
        }
      }

     case 7:
      {
        if (strcmp(tpChksum, "3m7J7bSuKWROPt7dyFpV5E") == 0) {
          extern mxArray *sf_c7_LessonIII_updateBuildInfo_args_info(void);
          plhs[0] = sf_c7_LessonIII_updateBuildInfo_args_info();
          break;
        }
      }

     case 8:
      {
        if (strcmp(tpChksum, "Jbiqze4DlRPbPmbIL0wS8F") == 0) {
          extern mxArray *sf_c8_LessonIII_updateBuildInfo_args_info(void);
          plhs[0] = sf_c8_LessonIII_updateBuildInfo_args_info();
          break;
        }
      }

     case 9:
      {
        if (strcmp(tpChksum, "SafIxCjmM9qEPS6BmID8AG") == 0) {
          extern mxArray *sf_c9_LessonIII_updateBuildInfo_args_info(void);
          plhs[0] = sf_c9_LessonIII_updateBuildInfo_args_info();
          break;
        }
      }

     case 10:
      {
        if (strcmp(tpChksum, "8fzVJxuAcigplLDM8puMmC") == 0) {
          extern mxArray *sf_c10_LessonIII_updateBuildInfo_args_info(void);
          plhs[0] = sf_c10_LessonIII_updateBuildInfo_args_info();
          break;
        }
      }

     case 11:
      {
        if (strcmp(tpChksum, "lSC5a8XYA6cZdj3x6TQDLH") == 0) {
          extern mxArray *sf_c11_LessonIII_updateBuildInfo_args_info(void);
          plhs[0] = sf_c11_LessonIII_updateBuildInfo_args_info();
          break;
        }
      }

     case 12:
      {
        if (strcmp(tpChksum, "9sXAisX9DlmicJiI3Thqs") == 0) {
          extern mxArray *sf_c12_LessonIII_updateBuildInfo_args_info(void);
          plhs[0] = sf_c12_LessonIII_updateBuildInfo_args_info();
          break;
        }
      }

     case 13:
      {
        if (strcmp(tpChksum, "ynY5ttWD0J2n5VW3gZiZPC") == 0) {
          extern mxArray *sf_c13_LessonIII_updateBuildInfo_args_info(void);
          plhs[0] = sf_c13_LessonIII_updateBuildInfo_args_info();
          break;
        }
      }

     case 17:
      {
        if (strcmp(tpChksum, "17qto9Q0GZg3Br64LuRYvC") == 0) {
          extern mxArray *sf_c17_LessonIII_updateBuildInfo_args_info(void);
          plhs[0] = sf_c17_LessonIII_updateBuildInfo_args_info();
          break;
        }
      }

     case 20:
      {
        if (strcmp(tpChksum, "iLKkTZfgeUWbCHGkvxHUyF") == 0) {
          extern mxArray *sf_c20_LessonIII_updateBuildInfo_args_info(void);
          plhs[0] = sf_c20_LessonIII_updateBuildInfo_args_info();
          break;
        }
      }

     case 21:
      {
        if (strcmp(tpChksum, "mTPa5Pzbe3w9l3J7cRjFdB") == 0) {
          extern mxArray *sf_c21_LessonIII_updateBuildInfo_args_info(void);
          plhs[0] = sf_c21_LessonIII_updateBuildInfo_args_info();
          break;
        }
      }

     case 22:
      {
        if (strcmp(tpChksum, "Rlx5hbPfiijZHfd9WQUKiG") == 0) {
          extern mxArray *sf_c22_LessonIII_updateBuildInfo_args_info(void);
          plhs[0] = sf_c22_LessonIII_updateBuildInfo_args_info();
          break;
        }
      }

     case 34:
      {
        if (strcmp(tpChksum, "rUtkHx5PMrq2fFWAaHOP0E") == 0) {
          extern mxArray *sf_c34_LessonIII_updateBuildInfo_args_info(void);
          plhs[0] = sf_c34_LessonIII_updateBuildInfo_args_info();
          break;
        }
      }

     default:
      plhs[0] = mxCreateDoubleMatrix(0,0,mxREAL);
    }
  }

  return 1;
}

void sf_LessonIII_get_post_codegen_info( int nlhs, mxArray * plhs[], int nrhs,
  const mxArray * prhs[] )
{
  unsigned int chartFileNumber = (unsigned int) mxGetScalar(prhs[0]);
  char tpChksum[64];
  mxGetString(prhs[1], tpChksum,sizeof(tpChksum)/sizeof(char));
  tpChksum[(sizeof(tpChksum)/sizeof(char)-1)] = '\0';
  switch (chartFileNumber) {
   case 1:
    {
      if (strcmp(tpChksum, "77qs0O1aFQP9R5KVWKglD") == 0) {
        extern mxArray *sf_c1_LessonIII_get_post_codegen_info(void);
        plhs[0] = sf_c1_LessonIII_get_post_codegen_info();
        return;
      }
    }
    break;

   case 2:
    {
      if (strcmp(tpChksum, "BPbr6mnccR4iiqbbOejEIC") == 0) {
        extern mxArray *sf_c2_LessonIII_get_post_codegen_info(void);
        plhs[0] = sf_c2_LessonIII_get_post_codegen_info();
        return;
      }
    }
    break;

   case 3:
    {
      if (strcmp(tpChksum, "gblOQFxJdbh8vdZcLHybtE") == 0) {
        extern mxArray *sf_c3_LessonIII_get_post_codegen_info(void);
        plhs[0] = sf_c3_LessonIII_get_post_codegen_info();
        return;
      }
    }
    break;

   case 4:
    {
      if (strcmp(tpChksum, "gyIrbHzC3iCzVLSSw6nCSD") == 0) {
        extern mxArray *sf_c4_LessonIII_get_post_codegen_info(void);
        plhs[0] = sf_c4_LessonIII_get_post_codegen_info();
        return;
      }
    }
    break;

   case 7:
    {
      if (strcmp(tpChksum, "3m7J7bSuKWROPt7dyFpV5E") == 0) {
        extern mxArray *sf_c7_LessonIII_get_post_codegen_info(void);
        plhs[0] = sf_c7_LessonIII_get_post_codegen_info();
        return;
      }
    }
    break;

   case 8:
    {
      if (strcmp(tpChksum, "Jbiqze4DlRPbPmbIL0wS8F") == 0) {
        extern mxArray *sf_c8_LessonIII_get_post_codegen_info(void);
        plhs[0] = sf_c8_LessonIII_get_post_codegen_info();
        return;
      }
    }
    break;

   case 9:
    {
      if (strcmp(tpChksum, "SafIxCjmM9qEPS6BmID8AG") == 0) {
        extern mxArray *sf_c9_LessonIII_get_post_codegen_info(void);
        plhs[0] = sf_c9_LessonIII_get_post_codegen_info();
        return;
      }
    }
    break;

   case 10:
    {
      if (strcmp(tpChksum, "8fzVJxuAcigplLDM8puMmC") == 0) {
        extern mxArray *sf_c10_LessonIII_get_post_codegen_info(void);
        plhs[0] = sf_c10_LessonIII_get_post_codegen_info();
        return;
      }
    }
    break;

   case 11:
    {
      if (strcmp(tpChksum, "lSC5a8XYA6cZdj3x6TQDLH") == 0) {
        extern mxArray *sf_c11_LessonIII_get_post_codegen_info(void);
        plhs[0] = sf_c11_LessonIII_get_post_codegen_info();
        return;
      }
    }
    break;

   case 12:
    {
      if (strcmp(tpChksum, "9sXAisX9DlmicJiI3Thqs") == 0) {
        extern mxArray *sf_c12_LessonIII_get_post_codegen_info(void);
        plhs[0] = sf_c12_LessonIII_get_post_codegen_info();
        return;
      }
    }
    break;

   case 13:
    {
      if (strcmp(tpChksum, "ynY5ttWD0J2n5VW3gZiZPC") == 0) {
        extern mxArray *sf_c13_LessonIII_get_post_codegen_info(void);
        plhs[0] = sf_c13_LessonIII_get_post_codegen_info();
        return;
      }
    }
    break;

   case 17:
    {
      if (strcmp(tpChksum, "17qto9Q0GZg3Br64LuRYvC") == 0) {
        extern mxArray *sf_c17_LessonIII_get_post_codegen_info(void);
        plhs[0] = sf_c17_LessonIII_get_post_codegen_info();
        return;
      }
    }
    break;

   case 20:
    {
      if (strcmp(tpChksum, "iLKkTZfgeUWbCHGkvxHUyF") == 0) {
        extern mxArray *sf_c20_LessonIII_get_post_codegen_info(void);
        plhs[0] = sf_c20_LessonIII_get_post_codegen_info();
        return;
      }
    }
    break;

   case 21:
    {
      if (strcmp(tpChksum, "mTPa5Pzbe3w9l3J7cRjFdB") == 0) {
        extern mxArray *sf_c21_LessonIII_get_post_codegen_info(void);
        plhs[0] = sf_c21_LessonIII_get_post_codegen_info();
        return;
      }
    }
    break;

   case 22:
    {
      if (strcmp(tpChksum, "Rlx5hbPfiijZHfd9WQUKiG") == 0) {
        extern mxArray *sf_c22_LessonIII_get_post_codegen_info(void);
        plhs[0] = sf_c22_LessonIII_get_post_codegen_info();
        return;
      }
    }
    break;

   case 34:
    {
      if (strcmp(tpChksum, "rUtkHx5PMrq2fFWAaHOP0E") == 0) {
        extern mxArray *sf_c34_LessonIII_get_post_codegen_info(void);
        plhs[0] = sf_c34_LessonIII_get_post_codegen_info();
        return;
      }
    }
    break;

   default:
    break;
  }

  plhs[0] = mxCreateDoubleMatrix(0,0,mxREAL);
}

void LessonIII_debug_initialize(struct SfDebugInstanceStruct* debugInstance)
{
  _LessonIIIMachineNumber_ = sf_debug_initialize_machine(debugInstance,
    "LessonIII","sfun",0,16,0,0,0);
  sf_debug_set_machine_event_thresholds(debugInstance,_LessonIIIMachineNumber_,0,
    0);
  sf_debug_set_machine_data_thresholds(debugInstance,_LessonIIIMachineNumber_,0);
}

void LessonIII_register_exported_symbols(SimStruct* S)
{
}

static mxArray* sRtwOptimizationInfoStruct= NULL;
mxArray* load_LessonIII_optimization_info(void)
{
  if (sRtwOptimizationInfoStruct==NULL) {
    sRtwOptimizationInfoStruct = sf_load_rtw_optimization_info("LessonIII",
      "LessonIII");
    mexMakeArrayPersistent(sRtwOptimizationInfoStruct);
  }

  return(sRtwOptimizationInfoStruct);
}

void unload_LessonIII_optimization_info(void)
{
  if (sRtwOptimizationInfoStruct!=NULL) {
    mxDestroyArray(sRtwOptimizationInfoStruct);
    sRtwOptimizationInfoStruct = NULL;
  }
}

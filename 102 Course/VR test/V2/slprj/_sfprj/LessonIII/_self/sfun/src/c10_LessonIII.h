#ifndef __c10_LessonIII_h__
#define __c10_LessonIII_h__

/* Include files */
#include "sf_runtime/sfc_sf.h"
#include "sf_runtime/sfc_mex.h"
#include "rtwtypes.h"
#include "multiword_types.h"

/* Type Definitions */
#ifndef typedef_SFc10_LessonIIIInstanceStruct
#define typedef_SFc10_LessonIIIInstanceStruct

typedef struct {
  SimStruct *S;
  ChartInfoStruct chartInfo;
  uint32_T chartNumber;
  uint32_T instanceNumber;
  int32_T c10_sfEvent;
  boolean_T c10_isStable;
  boolean_T c10_doneDoubleBufferReInit;
  uint8_T c10_is_active_c10_LessonIII;
  real32_T c10_position[3];
  boolean_T c10_position_not_empty;
  real32_T c10_rotation[4];
  boolean_T c10_rotation_not_empty;
  real32_T (*c10_Crotation)[4];
  real_T *c10_leftW;
  real32_T (*c10_Ctranspose)[3];
  real_T *c10_rightW;
  real_T *c10_robotWidth;
  real_T (*c10_init_cond)[3];
  real_T *c10_maxVel;
  real_T *c10_timeStep;
} SFc10_LessonIIIInstanceStruct;

#endif                                 /*typedef_SFc10_LessonIIIInstanceStruct*/

/* Named Constants */

/* Variable Declarations */
extern struct SfDebugInstanceStruct *sfGlobalDebugInstanceStruct;

/* Variable Definitions */

/* Function Declarations */
extern const mxArray *sf_c10_LessonIII_get_eml_resolved_functions_info(void);

/* Function Definitions */
extern void sf_c10_LessonIII_get_check_sum(mxArray *plhs[]);
extern void c10_LessonIII_method_dispatcher(SimStruct *S, int_T method, void
  *data);

#endif

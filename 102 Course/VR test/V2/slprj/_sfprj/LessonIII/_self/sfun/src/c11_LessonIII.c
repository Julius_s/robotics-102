/* Include files */

#include <stddef.h>
#include "blas.h"
#include "LessonIII_sfun.h"
#include "c11_LessonIII.h"
#include "mwmathutil.h"
#define CHARTINSTANCE_CHARTNUMBER      (chartInstance->chartNumber)
#define CHARTINSTANCE_INSTANCENUMBER   (chartInstance->instanceNumber)
#include "LessonIII_sfun_debug_macros.h"
#define _SF_MEX_LISTEN_FOR_CTRL_C(S)   sf_mex_listen_for_ctrl_c(sfGlobalDebugInstanceStruct,S);

/* Type Definitions */

/* Named Constants */
#define CALL_EVENT                     (-1)
#define c11_IN_NO_ACTIVE_CHILD         ((uint8_T)0U)
#define c11_IN_Idle                    ((uint8_T)1U)
#define c11_IN_goalie                  ((uint8_T)2U)
#define c11_IN_manual                  ((uint8_T)3U)
#define c11_IN_CheckForCollisions      ((uint8_T)1U)
#define c11_IN_WaitingTofreeUp         ((uint8_T)2U)

/* Variable Declarations */

/* Variable Definitions */
static real_T _sfTime_;
static const char * c11_debug_family_names[7] = { "diffVecx", "tempPos", "angle",
  "R", "tempWay", "nargin", "nargout" };

static const char * c11_b_debug_family_names[11] = { "diffVec", "rectangle",
  "angle", "R", "minx", "maxx", "miny", "maxy", "ii", "nargin", "nargout" };

static const char * c11_c_debug_family_names[2] = { "nargin", "nargout" };

static const char * c11_d_debug_family_names[2] = { "nargin", "nargout" };

static const char * c11_e_debug_family_names[2] = { "nargin", "nargout" };

static const char * c11_f_debug_family_names[2] = { "nargin", "nargout" };

static const char * c11_g_debug_family_names[2] = { "nargin", "nargout" };

static const char * c11_h_debug_family_names[2] = { "nargin", "nargout" };

static const char * c11_i_debug_family_names[2] = { "nargin", "nargout" };

static const char * c11_j_debug_family_names[2] = { "nargin", "nargout" };

static const char * c11_k_debug_family_names[3] = { "nargin", "nargout",
  "sf_internal_predicateOutput" };

static const char * c11_l_debug_family_names[3] = { "nargin", "nargout",
  "sf_internal_predicateOutput" };

static const char * c11_m_debug_family_names[3] = { "nargin", "nargout",
  "sf_internal_predicateOutput" };

static const char * c11_n_debug_family_names[3] = { "nargin", "nargout",
  "sf_internal_predicateOutput" };

static const char * c11_o_debug_family_names[3] = { "nargin", "nargout",
  "sf_internal_predicateOutput" };

static const char * c11_p_debug_family_names[3] = { "nargin", "nargout",
  "sf_internal_predicateOutput" };

static const char * c11_q_debug_family_names[3] = { "nargin", "nargout",
  "sf_internal_predicateOutput" };

static const char * c11_r_debug_family_names[3] = { "nargin", "nargout",
  "sf_internal_predicateOutput" };

/* Function Declarations */
static void initialize_c11_LessonIII(SFc11_LessonIIIInstanceStruct
  *chartInstance);
static void initialize_params_c11_LessonIII(SFc11_LessonIIIInstanceStruct
  *chartInstance);
static void enable_c11_LessonIII(SFc11_LessonIIIInstanceStruct *chartInstance);
static void disable_c11_LessonIII(SFc11_LessonIIIInstanceStruct *chartInstance);
static void c11_update_debugger_state_c11_LessonIII
  (SFc11_LessonIIIInstanceStruct *chartInstance);
static const mxArray *get_sim_state_c11_LessonIII(SFc11_LessonIIIInstanceStruct *
  chartInstance);
static void set_sim_state_c11_LessonIII(SFc11_LessonIIIInstanceStruct
  *chartInstance, const mxArray *c11_st);
static void c11_set_sim_state_side_effects_c11_LessonIII
  (SFc11_LessonIIIInstanceStruct *chartInstance);
static void finalize_c11_LessonIII(SFc11_LessonIIIInstanceStruct *chartInstance);
static void sf_gateway_c11_LessonIII(SFc11_LessonIIIInstanceStruct
  *chartInstance);
static void mdl_start_c11_LessonIII(SFc11_LessonIIIInstanceStruct *chartInstance);
static void c11_chartstep_c11_LessonIII(SFc11_LessonIIIInstanceStruct
  *chartInstance);
static void initSimStructsc11_LessonIII(SFc11_LessonIIIInstanceStruct
  *chartInstance);
static void init_script_number_translation(uint32_T c11_machineNumber, uint32_T
  c11_chartNumber, uint32_T c11_instanceNumber);
static const mxArray *c11_sf_marshallOut(void *chartInstanceVoid, void
  *c11_inData);
static real_T c11_emlrt_marshallIn(SFc11_LessonIIIInstanceStruct *chartInstance,
  const mxArray *c11_nargout, const char_T *c11_identifier);
static real_T c11_b_emlrt_marshallIn(SFc11_LessonIIIInstanceStruct
  *chartInstance, const mxArray *c11_u, const emlrtMsgIdentifier *c11_parentId);
static void c11_sf_marshallIn(void *chartInstanceVoid, const mxArray
  *c11_mxArrayInData, const char_T *c11_varName, void *c11_outData);
static const mxArray *c11_b_sf_marshallOut(void *chartInstanceVoid, void
  *c11_inData);
static c11_Waypoint c11_c_emlrt_marshallIn(SFc11_LessonIIIInstanceStruct
  *chartInstance, const mxArray *c11_tempWay, const char_T *c11_identifier);
static c11_Waypoint c11_d_emlrt_marshallIn(SFc11_LessonIIIInstanceStruct
  *chartInstance, const mxArray *c11_u, const emlrtMsgIdentifier *c11_parentId);
static int8_T c11_e_emlrt_marshallIn(SFc11_LessonIIIInstanceStruct
  *chartInstance, const mxArray *c11_u, const emlrtMsgIdentifier *c11_parentId);
static int16_T c11_f_emlrt_marshallIn(SFc11_LessonIIIInstanceStruct
  *chartInstance, const mxArray *c11_u, const emlrtMsgIdentifier *c11_parentId);
static void c11_b_sf_marshallIn(void *chartInstanceVoid, const mxArray
  *c11_mxArrayInData, const char_T *c11_varName, void *c11_outData);
static const mxArray *c11_c_sf_marshallOut(void *chartInstanceVoid, void
  *c11_inData);
static void c11_g_emlrt_marshallIn(SFc11_LessonIIIInstanceStruct *chartInstance,
  const mxArray *c11_u, const emlrtMsgIdentifier *c11_parentId, real32_T c11_y[4]);
static void c11_c_sf_marshallIn(void *chartInstanceVoid, const mxArray
  *c11_mxArrayInData, const char_T *c11_varName, void *c11_outData);
static const mxArray *c11_d_sf_marshallOut(void *chartInstanceVoid, void
  *c11_inData);
static real32_T c11_h_emlrt_marshallIn(SFc11_LessonIIIInstanceStruct
  *chartInstance, const mxArray *c11_u, const emlrtMsgIdentifier *c11_parentId);
static void c11_d_sf_marshallIn(void *chartInstanceVoid, const mxArray
  *c11_mxArrayInData, const char_T *c11_varName, void *c11_outData);
static const mxArray *c11_e_sf_marshallOut(void *chartInstanceVoid, void
  *c11_inData);
static const mxArray *c11_f_sf_marshallOut(void *chartInstanceVoid, void
  *c11_inData);
static void c11_i_emlrt_marshallIn(SFc11_LessonIIIInstanceStruct *chartInstance,
  const mxArray *c11_u, const emlrtMsgIdentifier *c11_parentId, real32_T c11_y[2]);
static void c11_e_sf_marshallIn(void *chartInstanceVoid, const mxArray
  *c11_mxArrayInData, const char_T *c11_varName, void *c11_outData);
static const mxArray *c11_g_sf_marshallOut(void *chartInstanceVoid, void
  *c11_inData);
static void c11_f_sf_marshallIn(void *chartInstanceVoid, const mxArray
  *c11_mxArrayInData, const char_T *c11_varName, void *c11_outData);
static const mxArray *c11_h_sf_marshallOut(void *chartInstanceVoid, void
  *c11_inData);
static const mxArray *c11_i_sf_marshallOut(void *chartInstanceVoid, void
  *c11_inData);
static boolean_T c11_j_emlrt_marshallIn(SFc11_LessonIIIInstanceStruct
  *chartInstance, const mxArray *c11_u, const emlrtMsgIdentifier *c11_parentId);
static void c11_g_sf_marshallIn(void *chartInstanceVoid, const mxArray
  *c11_mxArrayInData, const char_T *c11_varName, void *c11_outData);
static void c11_info_helper(const mxArray **c11_info);
static const mxArray *c11_emlrt_marshallOut(const char * c11_u);
static const mxArray *c11_b_emlrt_marshallOut(const uint32_T c11_u);
static void c11_b_info_helper(const mxArray **c11_info);
static void c11_normCollisionFinder(SFc11_LessonIIIInstanceStruct *chartInstance);
static real32_T c11_atan2d(SFc11_LessonIIIInstanceStruct *chartInstance,
  real32_T c11_y, real32_T c11_x);
static void c11_eml_scalar_eg(SFc11_LessonIIIInstanceStruct *chartInstance);
static real32_T c11_cosd(SFc11_LessonIIIInstanceStruct *chartInstance, real32_T
  c11_x);
static boolean_T c11_isfinite(SFc11_LessonIIIInstanceStruct *chartInstance,
  real32_T c11_x);
static real32_T c11_sind(SFc11_LessonIIIInstanceStruct *chartInstance, real32_T
  c11_x);
static void c11_threshold(SFc11_LessonIIIInstanceStruct *chartInstance);
static real32_T c11_norm(SFc11_LessonIIIInstanceStruct *chartInstance, real32_T
  c11_x[2]);
static void c11_below_threshold(SFc11_LessonIIIInstanceStruct *chartInstance);
static int32_T c11_intmin(SFc11_LessonIIIInstanceStruct *chartInstance);
static void c11_b_eml_scalar_eg(SFc11_LessonIIIInstanceStruct *chartInstance);
static const mxArray *c11_j_sf_marshallOut(void *chartInstanceVoid, void
  *c11_inData);
static int32_T c11_k_emlrt_marshallIn(SFc11_LessonIIIInstanceStruct
  *chartInstance, const mxArray *c11_u, const emlrtMsgIdentifier *c11_parentId);
static void c11_h_sf_marshallIn(void *chartInstanceVoid, const mxArray
  *c11_mxArrayInData, const char_T *c11_varName, void *c11_outData);
static const mxArray *c11_k_sf_marshallOut(void *chartInstanceVoid, void
  *c11_inData);
static uint8_T c11_l_emlrt_marshallIn(SFc11_LessonIIIInstanceStruct
  *chartInstance, const mxArray *c11_b_tp_Idle, const char_T *c11_identifier);
static uint8_T c11_m_emlrt_marshallIn(SFc11_LessonIIIInstanceStruct
  *chartInstance, const mxArray *c11_u, const emlrtMsgIdentifier *c11_parentId);
static void c11_i_sf_marshallIn(void *chartInstanceVoid, const mxArray
  *c11_mxArrayInData, const char_T *c11_varName, void *c11_outData);
static const mxArray *c11_players_bus_io(void *chartInstanceVoid, void
  *c11_pData);
static const mxArray *c11_l_sf_marshallOut(void *chartInstanceVoid, void
  *c11_inData);
static const mxArray *c11_ball_bus_io(void *chartInstanceVoid, void *c11_pData);
static const mxArray *c11_m_sf_marshallOut(void *chartInstanceVoid, void
  *c11_inData);
static const mxArray *c11_finalWay_bus_io(void *chartInstanceVoid, void
  *c11_pData);
static const mxArray *c11_me_bus_io(void *chartInstanceVoid, void *c11_pData);
static const mxArray *c11_n_sf_marshallOut(void *chartInstanceVoid, void
  *c11_inData);
static void c11_n_emlrt_marshallIn(SFc11_LessonIIIInstanceStruct *chartInstance,
  const mxArray *c11_b_dataWrittenToVector, const char_T *c11_identifier,
  boolean_T c11_y[4]);
static void c11_o_emlrt_marshallIn(SFc11_LessonIIIInstanceStruct *chartInstance,
  const mxArray *c11_u, const emlrtMsgIdentifier *c11_parentId, boolean_T c11_y
  [4]);
static const mxArray *c11_p_emlrt_marshallIn(SFc11_LessonIIIInstanceStruct
  *chartInstance, const mxArray *c11_b_setSimStateSideEffectsInfo, const char_T *
  c11_identifier);
static const mxArray *c11_q_emlrt_marshallIn(SFc11_LessonIIIInstanceStruct
  *chartInstance, const mxArray *c11_u, const emlrtMsgIdentifier *c11_parentId);
static void c11_updateDataWrittenToVector(SFc11_LessonIIIInstanceStruct
  *chartInstance, uint32_T c11_vectorIndex);
static void c11_errorIfDataNotWrittenToFcn(SFc11_LessonIIIInstanceStruct
  *chartInstance, uint32_T c11_vectorIndex, uint32_T c11_dataNumber, uint32_T
  c11_ssIdOfSourceObject, int32_T c11_offsetInSourceObject, int32_T
  c11_lengthInSourceObject);
static void c11_b_cosd(SFc11_LessonIIIInstanceStruct *chartInstance, real32_T
  *c11_x);
static void c11_b_sind(SFc11_LessonIIIInstanceStruct *chartInstance, real32_T
  *c11_x);
static void init_dsm_address_info(SFc11_LessonIIIInstanceStruct *chartInstance);
static void init_simulink_io_address(SFc11_LessonIIIInstanceStruct
  *chartInstance);

/* Function Definitions */
static void initialize_c11_LessonIII(SFc11_LessonIIIInstanceStruct
  *chartInstance)
{
  chartInstance->c11_sfEvent = CALL_EVENT;
  _sfTime_ = sf_get_time(chartInstance->S);
  chartInstance->c11_doSetSimStateSideEffects = 0U;
  chartInstance->c11_setSimStateSideEffectsInfo = NULL;
  chartInstance->c11_is_active_Collision = 0U;
  chartInstance->c11_is_Collision = c11_IN_NO_ACTIVE_CHILD;
  chartInstance->c11_tp_Collision = 0U;
  chartInstance->c11_tp_CheckForCollisions = 0U;
  chartInstance->c11_tp_WaitingTofreeUp = 0U;
  chartInstance->c11_is_active_PlayerLogics = 0U;
  chartInstance->c11_is_PlayerLogics = c11_IN_NO_ACTIVE_CHILD;
  chartInstance->c11_tp_PlayerLogics = 0U;
  chartInstance->c11_tp_Idle = 0U;
  chartInstance->c11_temporalCounter_i1 = 0U;
  chartInstance->c11_tp_goalie = 0U;
  chartInstance->c11_temporalCounter_i1 = 0U;
  chartInstance->c11_tp_manual = 0U;
  chartInstance->c11_is_active_c11_LessonIII = 0U;
}

static void initialize_params_c11_LessonIII(SFc11_LessonIIIInstanceStruct
  *chartInstance)
{
  (void)chartInstance;
}

static void enable_c11_LessonIII(SFc11_LessonIIIInstanceStruct *chartInstance)
{
  _sfTime_ = sf_get_time(chartInstance->S);
}

static void disable_c11_LessonIII(SFc11_LessonIIIInstanceStruct *chartInstance)
{
  _sfTime_ = sf_get_time(chartInstance->S);
}

static void c11_update_debugger_state_c11_LessonIII
  (SFc11_LessonIIIInstanceStruct *chartInstance)
{
  uint32_T c11_prevAniVal;
  c11_prevAniVal = _SFD_GET_ANIMATION();
  _SFD_SET_ANIMATION(0U);
  _SFD_SET_HONOR_BREAKPOINTS(0U);
  if (chartInstance->c11_is_active_c11_LessonIII == 1U) {
    _SFD_CC_CALL(CHART_ACTIVE_TAG, 8U, chartInstance->c11_sfEvent);
  }

  if (chartInstance->c11_is_PlayerLogics == c11_IN_Idle) {
    _SFD_CS_CALL(STATE_ACTIVE_TAG, 6U, chartInstance->c11_sfEvent);
  } else {
    _SFD_CS_CALL(STATE_INACTIVE_TAG, 6U, chartInstance->c11_sfEvent);
  }

  if (chartInstance->c11_is_active_PlayerLogics == 1U) {
    _SFD_CS_CALL(STATE_ACTIVE_TAG, 5U, chartInstance->c11_sfEvent);
  } else {
    _SFD_CS_CALL(STATE_INACTIVE_TAG, 5U, chartInstance->c11_sfEvent);
  }

  if (chartInstance->c11_is_active_Collision == 1U) {
    _SFD_CS_CALL(STATE_ACTIVE_TAG, 0U, chartInstance->c11_sfEvent);
  } else {
    _SFD_CS_CALL(STATE_INACTIVE_TAG, 0U, chartInstance->c11_sfEvent);
  }

  if (chartInstance->c11_is_Collision == c11_IN_WaitingTofreeUp) {
    _SFD_CS_CALL(STATE_ACTIVE_TAG, 2U, chartInstance->c11_sfEvent);
  } else {
    _SFD_CS_CALL(STATE_INACTIVE_TAG, 2U, chartInstance->c11_sfEvent);
  }

  if (chartInstance->c11_is_Collision == c11_IN_CheckForCollisions) {
    _SFD_CS_CALL(STATE_ACTIVE_TAG, 1U, chartInstance->c11_sfEvent);
  } else {
    _SFD_CS_CALL(STATE_INACTIVE_TAG, 1U, chartInstance->c11_sfEvent);
  }

  if (chartInstance->c11_is_PlayerLogics == c11_IN_goalie) {
    _SFD_CS_CALL(STATE_ACTIVE_TAG, 7U, chartInstance->c11_sfEvent);
  } else {
    _SFD_CS_CALL(STATE_INACTIVE_TAG, 7U, chartInstance->c11_sfEvent);
  }

  if (chartInstance->c11_is_PlayerLogics == c11_IN_manual) {
    _SFD_CS_CALL(STATE_ACTIVE_TAG, 8U, chartInstance->c11_sfEvent);
  } else {
    _SFD_CS_CALL(STATE_INACTIVE_TAG, 8U, chartInstance->c11_sfEvent);
  }

  _SFD_SET_ANIMATION(c11_prevAniVal);
  _SFD_SET_HONOR_BREAKPOINTS(1U);
  _SFD_ANIMATE();
}

static const mxArray *get_sim_state_c11_LessonIII(SFc11_LessonIIIInstanceStruct *
  chartInstance)
{
  const mxArray *c11_st;
  const mxArray *c11_y = NULL;
  uint8_T c11_hoistedGlobal;
  uint8_T c11_u;
  const mxArray *c11_b_y = NULL;
  const mxArray *c11_c_y = NULL;
  int8_T c11_b_u;
  const mxArray *c11_d_y = NULL;
  int8_T c11_c_u;
  const mxArray *c11_e_y = NULL;
  int16_T c11_d_u;
  const mxArray *c11_f_y = NULL;
  uint8_T c11_b_hoistedGlobal;
  uint8_T c11_e_u;
  const mxArray *c11_g_y = NULL;
  real_T c11_c_hoistedGlobal;
  real_T c11_f_u;
  const mxArray *c11_h_y = NULL;
  uint8_T c11_d_hoistedGlobal;
  uint8_T c11_g_u;
  const mxArray *c11_i_y = NULL;
  uint8_T c11_e_hoistedGlobal;
  uint8_T c11_h_u;
  const mxArray *c11_j_y = NULL;
  uint8_T c11_f_hoistedGlobal;
  uint8_T c11_i_u;
  const mxArray *c11_k_y = NULL;
  uint8_T c11_g_hoistedGlobal;
  uint8_T c11_j_u;
  const mxArray *c11_l_y = NULL;
  uint8_T c11_h_hoistedGlobal;
  uint8_T c11_k_u;
  const mxArray *c11_m_y = NULL;
  uint8_T c11_i_hoistedGlobal;
  uint8_T c11_l_u;
  const mxArray *c11_n_y = NULL;
  int32_T c11_i0;
  boolean_T c11_m_u[4];
  const mxArray *c11_o_y = NULL;
  c11_st = NULL;
  c11_st = NULL;
  c11_y = NULL;
  sf_mex_assign(&c11_y, sf_mex_createcellmatrix(11, 1), false);
  c11_hoistedGlobal = *chartInstance->c11_collision;
  c11_u = c11_hoistedGlobal;
  c11_b_y = NULL;
  sf_mex_assign(&c11_b_y, sf_mex_create("y", &c11_u, 3, 0U, 0U, 0U, 0), false);
  sf_mex_setcell(c11_y, 0, c11_b_y);
  c11_c_y = NULL;
  sf_mex_assign(&c11_c_y, sf_mex_createstruct("structure", 2, 1, 1), false);
  c11_b_u = *(int8_T *)&((char_T *)chartInstance->c11_finalWay)[0];
  c11_d_y = NULL;
  sf_mex_assign(&c11_d_y, sf_mex_create("y", &c11_b_u, 2, 0U, 0U, 0U, 0), false);
  sf_mex_addfield(c11_c_y, c11_d_y, "x", "x", 0);
  c11_c_u = *(int8_T *)&((char_T *)chartInstance->c11_finalWay)[1];
  c11_e_y = NULL;
  sf_mex_assign(&c11_e_y, sf_mex_create("y", &c11_c_u, 2, 0U, 0U, 0U, 0), false);
  sf_mex_addfield(c11_c_y, c11_e_y, "y", "y", 0);
  c11_d_u = *(int16_T *)&((char_T *)chartInstance->c11_finalWay)[2];
  c11_f_y = NULL;
  sf_mex_assign(&c11_f_y, sf_mex_create("y", &c11_d_u, 4, 0U, 0U, 0U, 0), false);
  sf_mex_addfield(c11_c_y, c11_f_y, "orientation", "orientation", 0);
  sf_mex_setcell(c11_y, 1, c11_c_y);
  c11_b_hoistedGlobal = chartInstance->c11_collisionDetected;
  c11_e_u = c11_b_hoistedGlobal;
  c11_g_y = NULL;
  sf_mex_assign(&c11_g_y, sf_mex_create("y", &c11_e_u, 3, 0U, 0U, 0U, 0), false);
  sf_mex_setcell(c11_y, 2, c11_g_y);
  c11_c_hoistedGlobal = chartInstance->c11_lol;
  c11_f_u = c11_c_hoistedGlobal;
  c11_h_y = NULL;
  sf_mex_assign(&c11_h_y, sf_mex_create("y", &c11_f_u, 0, 0U, 0U, 0U, 0), false);
  sf_mex_setcell(c11_y, 3, c11_h_y);
  c11_d_hoistedGlobal = chartInstance->c11_is_active_c11_LessonIII;
  c11_g_u = c11_d_hoistedGlobal;
  c11_i_y = NULL;
  sf_mex_assign(&c11_i_y, sf_mex_create("y", &c11_g_u, 3, 0U, 0U, 0U, 0), false);
  sf_mex_setcell(c11_y, 4, c11_i_y);
  c11_e_hoistedGlobal = chartInstance->c11_is_active_PlayerLogics;
  c11_h_u = c11_e_hoistedGlobal;
  c11_j_y = NULL;
  sf_mex_assign(&c11_j_y, sf_mex_create("y", &c11_h_u, 3, 0U, 0U, 0U, 0), false);
  sf_mex_setcell(c11_y, 5, c11_j_y);
  c11_f_hoistedGlobal = chartInstance->c11_is_active_Collision;
  c11_i_u = c11_f_hoistedGlobal;
  c11_k_y = NULL;
  sf_mex_assign(&c11_k_y, sf_mex_create("y", &c11_i_u, 3, 0U, 0U, 0U, 0), false);
  sf_mex_setcell(c11_y, 6, c11_k_y);
  c11_g_hoistedGlobal = chartInstance->c11_is_PlayerLogics;
  c11_j_u = c11_g_hoistedGlobal;
  c11_l_y = NULL;
  sf_mex_assign(&c11_l_y, sf_mex_create("y", &c11_j_u, 3, 0U, 0U, 0U, 0), false);
  sf_mex_setcell(c11_y, 7, c11_l_y);
  c11_h_hoistedGlobal = chartInstance->c11_is_Collision;
  c11_k_u = c11_h_hoistedGlobal;
  c11_m_y = NULL;
  sf_mex_assign(&c11_m_y, sf_mex_create("y", &c11_k_u, 3, 0U, 0U, 0U, 0), false);
  sf_mex_setcell(c11_y, 8, c11_m_y);
  c11_i_hoistedGlobal = chartInstance->c11_temporalCounter_i1;
  c11_l_u = c11_i_hoistedGlobal;
  c11_n_y = NULL;
  sf_mex_assign(&c11_n_y, sf_mex_create("y", &c11_l_u, 3, 0U, 0U, 0U, 0), false);
  sf_mex_setcell(c11_y, 9, c11_n_y);
  for (c11_i0 = 0; c11_i0 < 4; c11_i0++) {
    c11_m_u[c11_i0] = chartInstance->c11_dataWrittenToVector[c11_i0];
  }

  c11_o_y = NULL;
  sf_mex_assign(&c11_o_y, sf_mex_create("y", c11_m_u, 11, 0U, 1U, 0U, 1, 4),
                false);
  sf_mex_setcell(c11_y, 10, c11_o_y);
  sf_mex_assign(&c11_st, c11_y, false);
  return c11_st;
}

static void set_sim_state_c11_LessonIII(SFc11_LessonIIIInstanceStruct
  *chartInstance, const mxArray *c11_st)
{
  const mxArray *c11_u;
  c11_Waypoint c11_r0;
  boolean_T c11_bv0[4];
  int32_T c11_i1;
  c11_u = sf_mex_dup(c11_st);
  *chartInstance->c11_collision = c11_l_emlrt_marshallIn(chartInstance,
    sf_mex_dup(sf_mex_getcell(c11_u, 0)), "collision");
  c11_r0 = c11_c_emlrt_marshallIn(chartInstance, sf_mex_dup(sf_mex_getcell(c11_u,
    1)), "finalWay");
  *(int8_T *)&((char_T *)chartInstance->c11_finalWay)[0] = c11_r0.x;
  *(int8_T *)&((char_T *)chartInstance->c11_finalWay)[1] = c11_r0.y;
  *(int16_T *)&((char_T *)chartInstance->c11_finalWay)[2] = c11_r0.orientation;
  chartInstance->c11_collisionDetected = c11_l_emlrt_marshallIn(chartInstance,
    sf_mex_dup(sf_mex_getcell(c11_u, 2)), "collisionDetected");
  chartInstance->c11_lol = c11_emlrt_marshallIn(chartInstance, sf_mex_dup
    (sf_mex_getcell(c11_u, 3)), "lol");
  chartInstance->c11_is_active_c11_LessonIII = c11_l_emlrt_marshallIn
    (chartInstance, sf_mex_dup(sf_mex_getcell(c11_u, 4)),
     "is_active_c11_LessonIII");
  chartInstance->c11_is_active_PlayerLogics = c11_l_emlrt_marshallIn
    (chartInstance, sf_mex_dup(sf_mex_getcell(c11_u, 5)),
     "is_active_PlayerLogics");
  chartInstance->c11_is_active_Collision = c11_l_emlrt_marshallIn(chartInstance,
    sf_mex_dup(sf_mex_getcell(c11_u, 6)), "is_active_Collision");
  chartInstance->c11_is_PlayerLogics = c11_l_emlrt_marshallIn(chartInstance,
    sf_mex_dup(sf_mex_getcell(c11_u, 7)), "is_PlayerLogics");
  chartInstance->c11_is_Collision = c11_l_emlrt_marshallIn(chartInstance,
    sf_mex_dup(sf_mex_getcell(c11_u, 8)), "is_Collision");
  chartInstance->c11_temporalCounter_i1 = c11_l_emlrt_marshallIn(chartInstance,
    sf_mex_dup(sf_mex_getcell(c11_u, 9)), "temporalCounter_i1");
  c11_n_emlrt_marshallIn(chartInstance, sf_mex_dup(sf_mex_getcell(c11_u, 10)),
    "dataWrittenToVector", c11_bv0);
  for (c11_i1 = 0; c11_i1 < 4; c11_i1++) {
    chartInstance->c11_dataWrittenToVector[c11_i1] = c11_bv0[c11_i1];
  }

  sf_mex_assign(&chartInstance->c11_setSimStateSideEffectsInfo,
                c11_p_emlrt_marshallIn(chartInstance, sf_mex_dup(sf_mex_getcell
    (c11_u, 11)), "setSimStateSideEffectsInfo"), true);
  sf_mex_destroy(&c11_u);
  chartInstance->c11_doSetSimStateSideEffects = 1U;
  c11_update_debugger_state_c11_LessonIII(chartInstance);
  sf_mex_destroy(&c11_st);
}

static void c11_set_sim_state_side_effects_c11_LessonIII
  (SFc11_LessonIIIInstanceStruct *chartInstance)
{
  if (chartInstance->c11_doSetSimStateSideEffects != 0) {
    if (chartInstance->c11_is_active_PlayerLogics == 1U) {
      chartInstance->c11_tp_PlayerLogics = 1U;
    } else {
      chartInstance->c11_tp_PlayerLogics = 0U;
    }

    if (chartInstance->c11_is_PlayerLogics == c11_IN_Idle) {
      chartInstance->c11_tp_Idle = 1U;
      if (sf_mex_sub(chartInstance->c11_setSimStateSideEffectsInfo,
                     "setSimStateSideEffectsInfo", 1, 3) == 0.0) {
        chartInstance->c11_temporalCounter_i1 = 0U;
      }
    } else {
      chartInstance->c11_tp_Idle = 0U;
    }

    if (chartInstance->c11_is_PlayerLogics == c11_IN_goalie) {
      chartInstance->c11_tp_goalie = 1U;
      if (sf_mex_sub(chartInstance->c11_setSimStateSideEffectsInfo,
                     "setSimStateSideEffectsInfo", 1, 4) == 0.0) {
        chartInstance->c11_temporalCounter_i1 = 0U;
      }
    } else {
      chartInstance->c11_tp_goalie = 0U;
    }

    if (chartInstance->c11_is_PlayerLogics == c11_IN_manual) {
      chartInstance->c11_tp_manual = 1U;
    } else {
      chartInstance->c11_tp_manual = 0U;
    }

    if (chartInstance->c11_is_active_Collision == 1U) {
      chartInstance->c11_tp_Collision = 1U;
    } else {
      chartInstance->c11_tp_Collision = 0U;
    }

    if (chartInstance->c11_is_Collision == c11_IN_CheckForCollisions) {
      chartInstance->c11_tp_CheckForCollisions = 1U;
    } else {
      chartInstance->c11_tp_CheckForCollisions = 0U;
    }

    if (chartInstance->c11_is_Collision == c11_IN_WaitingTofreeUp) {
      chartInstance->c11_tp_WaitingTofreeUp = 1U;
    } else {
      chartInstance->c11_tp_WaitingTofreeUp = 0U;
    }

    chartInstance->c11_doSetSimStateSideEffects = 0U;
  }
}

static void finalize_c11_LessonIII(SFc11_LessonIIIInstanceStruct *chartInstance)
{
  sf_mex_destroy(&chartInstance->c11_setSimStateSideEffectsInfo);
}

static void sf_gateway_c11_LessonIII(SFc11_LessonIIIInstanceStruct
  *chartInstance)
{
  c11_set_sim_state_side_effects_c11_LessonIII(chartInstance);
  _SFD_SYMBOL_SCOPE_PUSH(0U, 0U);
  _sfTime_ = sf_get_time(chartInstance->S);
  _SFD_CC_CALL(CHART_ENTER_SFUNCTION_TAG, 8U, chartInstance->c11_sfEvent);
  _SFD_DATA_RANGE_CHECK((real_T)*chartInstance->c11_collision, 5U);
  _SFD_DATA_RANGE_CHECK((real_T)chartInstance->c11_collisionDetected, 6U);
  _SFD_DATA_RANGE_CHECK(chartInstance->c11_lol, 7U);
  chartInstance->c11_sfEvent = CALL_EVENT;
  if (chartInstance->c11_temporalCounter_i1 < 31U) {
    chartInstance->c11_temporalCounter_i1++;
  }

  c11_chartstep_c11_LessonIII(chartInstance);
  _SFD_SYMBOL_SCOPE_POP();
  _SFD_CHECK_FOR_STATE_INCONSISTENCY(_LessonIIIMachineNumber_,
    chartInstance->chartNumber, chartInstance->instanceNumber);
}

static void mdl_start_c11_LessonIII(SFc11_LessonIIIInstanceStruct *chartInstance)
{
  (void)chartInstance;
}

static void c11_chartstep_c11_LessonIII(SFc11_LessonIIIInstanceStruct
  *chartInstance)
{
  uint32_T c11_debug_family_var_map[2];
  real_T c11_nargin = 0.0;
  real_T c11_nargout = 0.0;
  real_T c11_b_nargin = 0.0;
  real_T c11_b_nargout = 0.0;
  uint32_T c11_b_debug_family_var_map[3];
  real_T c11_c_nargin = 0.0;
  real_T c11_c_nargout = 1.0;
  boolean_T c11_out;
  real_T c11_d_nargin = 0.0;
  real_T c11_d_nargout = 0.0;
  real_T c11_e_nargin = 0.0;
  real_T c11_e_nargout = 1.0;
  boolean_T c11_b_out;
  real_T c11_f_nargin = 0.0;
  real_T c11_f_nargout = 0.0;
  real_T c11_g_nargin = 0.0;
  real_T c11_g_nargout = 0.0;
  real_T c11_h_nargin = 0.0;
  real_T c11_h_nargout = 0.0;
  real_T c11_i_nargin = 0.0;
  real_T c11_i_nargout = 1.0;
  boolean_T c11_c_out;
  real_T c11_j_nargin = 0.0;
  real_T c11_j_nargout = 0.0;
  real_T c11_k_nargin = 0.0;
  real_T c11_k_nargout = 1.0;
  boolean_T c11_d_out;
  real_T c11_l_nargin = 0.0;
  real_T c11_l_nargout = 0.0;
  _SFD_CC_CALL(CHART_ENTER_DURING_FUNCTION_TAG, 8U, chartInstance->c11_sfEvent);
  if (chartInstance->c11_is_active_c11_LessonIII == 0U) {
    _SFD_CC_CALL(CHART_ENTER_ENTRY_FUNCTION_TAG, 8U, chartInstance->c11_sfEvent);
    chartInstance->c11_is_active_c11_LessonIII = 1U;
    _SFD_CC_CALL(EXIT_OUT_OF_FUNCTION_TAG, 8U, chartInstance->c11_sfEvent);
    chartInstance->c11_is_active_PlayerLogics = 1U;
    _SFD_CS_CALL(STATE_ACTIVE_TAG, 5U, chartInstance->c11_sfEvent);
    chartInstance->c11_tp_PlayerLogics = 1U;
    _SFD_SYMBOL_SCOPE_PUSH_EML(0U, 2U, 2U, c11_e_debug_family_names,
      c11_debug_family_var_map);
    _SFD_SYMBOL_SCOPE_ADD_EML_IMPORTABLE(&c11_nargin, 0U, c11_sf_marshallOut,
      c11_sf_marshallIn);
    _SFD_SYMBOL_SCOPE_ADD_EML_IMPORTABLE(&c11_nargout, 1U, c11_sf_marshallOut,
      c11_sf_marshallIn);
    *chartInstance->c11_collision = 0U;
    c11_updateDataWrittenToVector(chartInstance, 1U);
    _SFD_DATA_RANGE_CHECK((real_T)*chartInstance->c11_collision, 5U);
    _SFD_SYMBOL_SCOPE_POP();
    _SFD_CT_CALL(TRANSITION_ACTIVE_TAG, 5U, chartInstance->c11_sfEvent);
    chartInstance->c11_is_PlayerLogics = c11_IN_Idle;
    _SFD_CS_CALL(STATE_ACTIVE_TAG, 6U, chartInstance->c11_sfEvent);
    chartInstance->c11_temporalCounter_i1 = 0U;
    chartInstance->c11_tp_Idle = 1U;
    _SFD_SYMBOL_SCOPE_PUSH_EML(0U, 2U, 2U, c11_c_debug_family_names,
      c11_debug_family_var_map);
    _SFD_SYMBOL_SCOPE_ADD_EML_IMPORTABLE(&c11_b_nargin, 0U, c11_sf_marshallOut,
      c11_sf_marshallIn);
    _SFD_SYMBOL_SCOPE_ADD_EML_IMPORTABLE(&c11_b_nargout, 1U, c11_sf_marshallOut,
      c11_sf_marshallIn);
    *(int8_T *)&((char_T *)chartInstance->c11_finalWay)[0] = *(int8_T *)
      &((char_T *)chartInstance->c11_me)[0];
    c11_updateDataWrittenToVector(chartInstance, 0U);
    *(int8_T *)&((char_T *)chartInstance->c11_finalWay)[1] = *(int8_T *)
      &((char_T *)chartInstance->c11_me)[1];
    c11_updateDataWrittenToVector(chartInstance, 0U);
    *(int16_T *)&((char_T *)chartInstance->c11_finalWay)[2] = *(int16_T *)
      &((char_T *)chartInstance->c11_me)[2];
    c11_updateDataWrittenToVector(chartInstance, 0U);
    _SFD_SYMBOL_SCOPE_POP();
    chartInstance->c11_is_active_Collision = 1U;
    _SFD_CS_CALL(STATE_ACTIVE_TAG, 0U, chartInstance->c11_sfEvent);
    chartInstance->c11_tp_Collision = 1U;
    _SFD_CT_CALL(TRANSITION_ACTIVE_TAG, 2U, chartInstance->c11_sfEvent);
    chartInstance->c11_is_Collision = c11_IN_CheckForCollisions;
    _SFD_CS_CALL(STATE_ACTIVE_TAG, 1U, chartInstance->c11_sfEvent);
    chartInstance->c11_tp_CheckForCollisions = 1U;
  } else {
    _SFD_CS_CALL(STATE_ENTER_DURING_FUNCTION_TAG, 5U, chartInstance->c11_sfEvent);
    switch (chartInstance->c11_is_PlayerLogics) {
     case c11_IN_Idle:
      CV_STATE_EVAL(5, 0, 1);
      _SFD_CT_CALL(TRANSITION_BEFORE_PROCESSING_TAG, 3U,
                   chartInstance->c11_sfEvent);
      _SFD_SYMBOL_SCOPE_PUSH_EML(0U, 3U, 3U, c11_n_debug_family_names,
        c11_b_debug_family_var_map);
      _SFD_SYMBOL_SCOPE_ADD_EML_IMPORTABLE(&c11_c_nargin, 0U, c11_sf_marshallOut,
        c11_sf_marshallIn);
      _SFD_SYMBOL_SCOPE_ADD_EML_IMPORTABLE(&c11_c_nargout, 1U,
        c11_sf_marshallOut, c11_sf_marshallIn);
      _SFD_SYMBOL_SCOPE_ADD_EML_IMPORTABLE(&c11_out, 2U, c11_i_sf_marshallOut,
        c11_g_sf_marshallIn);
      c11_out = CV_EML_IF(3, 0, 0, chartInstance->c11_temporalCounter_i1 >= 20);
      _SFD_SYMBOL_SCOPE_POP();
      if (c11_out) {
        _SFD_CT_CALL(TRANSITION_ACTIVE_TAG, 3U, chartInstance->c11_sfEvent);
        chartInstance->c11_tp_Idle = 0U;
        _SFD_CS_CALL(STATE_INACTIVE_TAG, 6U, chartInstance->c11_sfEvent);
        chartInstance->c11_is_PlayerLogics = c11_IN_goalie;
        _SFD_CS_CALL(STATE_ACTIVE_TAG, 7U, chartInstance->c11_sfEvent);
        chartInstance->c11_temporalCounter_i1 = 0U;
        chartInstance->c11_tp_goalie = 1U;
      } else {
        _SFD_CS_CALL(STATE_ENTER_DURING_FUNCTION_TAG, 6U,
                     chartInstance->c11_sfEvent);
        _SFD_SYMBOL_SCOPE_PUSH_EML(0U, 2U, 2U, c11_d_debug_family_names,
          c11_debug_family_var_map);
        _SFD_SYMBOL_SCOPE_ADD_EML_IMPORTABLE(&c11_d_nargin, 0U,
          c11_sf_marshallOut, c11_sf_marshallIn);
        _SFD_SYMBOL_SCOPE_ADD_EML_IMPORTABLE(&c11_d_nargout, 1U,
          c11_sf_marshallOut, c11_sf_marshallIn);
        *(int8_T *)&((char_T *)chartInstance->c11_finalWay)[0] = *(int8_T *)
          &((char_T *)chartInstance->c11_me)[0];
        c11_updateDataWrittenToVector(chartInstance, 0U);
        *(int8_T *)&((char_T *)chartInstance->c11_finalWay)[1] = *(int8_T *)
          &((char_T *)chartInstance->c11_me)[1];
        c11_updateDataWrittenToVector(chartInstance, 0U);
        *(int16_T *)&((char_T *)chartInstance->c11_finalWay)[2] = *(int16_T *)
          &((char_T *)chartInstance->c11_me)[2];
        c11_updateDataWrittenToVector(chartInstance, 0U);
        _SFD_SYMBOL_SCOPE_POP();
      }

      _SFD_CS_CALL(EXIT_OUT_OF_FUNCTION_TAG, 6U, chartInstance->c11_sfEvent);
      break;

     case c11_IN_goalie:
      CV_STATE_EVAL(5, 0, 2);
      _SFD_CT_CALL(TRANSITION_BEFORE_PROCESSING_TAG, 4U,
                   chartInstance->c11_sfEvent);
      _SFD_SYMBOL_SCOPE_PUSH_EML(0U, 3U, 3U, c11_k_debug_family_names,
        c11_b_debug_family_var_map);
      _SFD_SYMBOL_SCOPE_ADD_EML_IMPORTABLE(&c11_e_nargin, 0U, c11_sf_marshallOut,
        c11_sf_marshallIn);
      _SFD_SYMBOL_SCOPE_ADD_EML_IMPORTABLE(&c11_e_nargout, 1U,
        c11_sf_marshallOut, c11_sf_marshallIn);
      _SFD_SYMBOL_SCOPE_ADD_EML_IMPORTABLE(&c11_b_out, 2U, c11_i_sf_marshallOut,
        c11_g_sf_marshallIn);
      c11_b_out = CV_EML_IF(4, 0, 0, chartInstance->c11_temporalCounter_i1 >= 20);
      _SFD_SYMBOL_SCOPE_POP();
      if (c11_b_out) {
        _SFD_CT_CALL(TRANSITION_ACTIVE_TAG, 4U, chartInstance->c11_sfEvent);
        chartInstance->c11_tp_goalie = 0U;
        _SFD_CS_CALL(STATE_INACTIVE_TAG, 7U, chartInstance->c11_sfEvent);
        chartInstance->c11_is_PlayerLogics = c11_IN_manual;
        _SFD_CS_CALL(STATE_ACTIVE_TAG, 8U, chartInstance->c11_sfEvent);
        chartInstance->c11_tp_manual = 1U;
      } else {
        _SFD_CS_CALL(STATE_ENTER_DURING_FUNCTION_TAG, 7U,
                     chartInstance->c11_sfEvent);
        _SFD_SYMBOL_SCOPE_PUSH_EML(0U, 2U, 2U, c11_i_debug_family_names,
          c11_debug_family_var_map);
        _SFD_SYMBOL_SCOPE_ADD_EML_IMPORTABLE(&c11_f_nargin, 0U,
          c11_sf_marshallOut, c11_sf_marshallIn);
        _SFD_SYMBOL_SCOPE_ADD_EML_IMPORTABLE(&c11_f_nargout, 1U,
          c11_sf_marshallOut, c11_sf_marshallIn);
        *(int8_T *)&((char_T *)chartInstance->c11_finalWay)[0] = -80;
        c11_updateDataWrittenToVector(chartInstance, 0U);
        *(int8_T *)&((char_T *)chartInstance->c11_finalWay)[1] = *(int8_T *)
          &((char_T *)chartInstance->c11_ball)[1];
        c11_updateDataWrittenToVector(chartInstance, 0U);
        *(int16_T *)&((char_T *)chartInstance->c11_finalWay)[2] = MAX_int16_T;
        c11_updateDataWrittenToVector(chartInstance, 0U);
        _SFD_SYMBOL_SCOPE_POP();
      }

      _SFD_CS_CALL(EXIT_OUT_OF_FUNCTION_TAG, 7U, chartInstance->c11_sfEvent);
      break;

     case c11_IN_manual:
      CV_STATE_EVAL(5, 0, 3);
      _SFD_CS_CALL(STATE_ENTER_DURING_FUNCTION_TAG, 8U,
                   chartInstance->c11_sfEvent);
      _SFD_SYMBOL_SCOPE_PUSH_EML(0U, 2U, 2U, c11_j_debug_family_names,
        c11_debug_family_var_map);
      _SFD_SYMBOL_SCOPE_ADD_EML_IMPORTABLE(&c11_g_nargin, 0U, c11_sf_marshallOut,
        c11_sf_marshallIn);
      _SFD_SYMBOL_SCOPE_ADD_EML_IMPORTABLE(&c11_g_nargout, 1U,
        c11_sf_marshallOut, c11_sf_marshallIn);
      *(int8_T *)&((char_T *)chartInstance->c11_finalWay)[0] = *(int8_T *)
        &((char_T *)chartInstance->c11_manualWay)[0];
      *(int8_T *)&((char_T *)chartInstance->c11_finalWay)[1] = *(int8_T *)
        &((char_T *)chartInstance->c11_manualWay)[1];
      *(int16_T *)&((char_T *)chartInstance->c11_finalWay)[2] = *(int16_T *)
        &((char_T *)chartInstance->c11_manualWay)[2];
      c11_updateDataWrittenToVector(chartInstance, 0U);
      _SFD_SYMBOL_SCOPE_POP();
      _SFD_CS_CALL(EXIT_OUT_OF_FUNCTION_TAG, 8U, chartInstance->c11_sfEvent);
      break;

     default:
      CV_STATE_EVAL(5, 0, 0);
      chartInstance->c11_is_PlayerLogics = c11_IN_NO_ACTIVE_CHILD;
      _SFD_CS_CALL(STATE_INACTIVE_TAG, 6U, chartInstance->c11_sfEvent);
      break;
    }

    _SFD_CS_CALL(EXIT_OUT_OF_FUNCTION_TAG, 5U, chartInstance->c11_sfEvent);
    _SFD_CS_CALL(STATE_ENTER_DURING_FUNCTION_TAG, 0U, chartInstance->c11_sfEvent);
    _SFD_SYMBOL_SCOPE_PUSH_EML(0U, 2U, 2U, c11_f_debug_family_names,
      c11_debug_family_var_map);
    _SFD_SYMBOL_SCOPE_ADD_EML_IMPORTABLE(&c11_h_nargin, 0U, c11_sf_marshallOut,
      c11_sf_marshallIn);
    _SFD_SYMBOL_SCOPE_ADD_EML_IMPORTABLE(&c11_h_nargout, 1U, c11_sf_marshallOut,
      c11_sf_marshallIn);
    c11_normCollisionFinder(chartInstance);
    _SFD_SYMBOL_SCOPE_POP();
    switch (chartInstance->c11_is_Collision) {
     case c11_IN_CheckForCollisions:
      CV_STATE_EVAL(0, 0, 1);
      _SFD_CT_CALL(TRANSITION_BEFORE_PROCESSING_TAG, 0U,
                   chartInstance->c11_sfEvent);
      _SFD_SYMBOL_SCOPE_PUSH_EML(0U, 3U, 3U, c11_l_debug_family_names,
        c11_b_debug_family_var_map);
      _SFD_SYMBOL_SCOPE_ADD_EML_IMPORTABLE(&c11_i_nargin, 0U, c11_sf_marshallOut,
        c11_sf_marshallIn);
      _SFD_SYMBOL_SCOPE_ADD_EML_IMPORTABLE(&c11_i_nargout, 1U,
        c11_sf_marshallOut, c11_sf_marshallIn);
      _SFD_SYMBOL_SCOPE_ADD_EML_IMPORTABLE(&c11_c_out, 2U, c11_i_sf_marshallOut,
        c11_g_sf_marshallIn);
      c11_errorIfDataNotWrittenToFcn(chartInstance, 2U, 6U, 52U, 1, 17);
      c11_c_out = CV_EML_IF(0, 0, 0, CV_RELATIONAL_EVAL(5U, 0U, 0, (real_T)
        chartInstance->c11_collisionDetected, 1.0, 0, 0U,
        chartInstance->c11_collisionDetected == 1));
      _SFD_SYMBOL_SCOPE_POP();
      if (c11_c_out) {
        _SFD_CT_CALL(TRANSITION_ACTIVE_TAG, 0U, chartInstance->c11_sfEvent);
        chartInstance->c11_tp_CheckForCollisions = 0U;
        _SFD_CS_CALL(STATE_INACTIVE_TAG, 1U, chartInstance->c11_sfEvent);
        chartInstance->c11_is_Collision = c11_IN_WaitingTofreeUp;
        _SFD_CS_CALL(STATE_ACTIVE_TAG, 2U, chartInstance->c11_sfEvent);
        chartInstance->c11_tp_WaitingTofreeUp = 1U;
        _SFD_SYMBOL_SCOPE_PUSH_EML(0U, 2U, 2U, c11_g_debug_family_names,
          c11_debug_family_var_map);
        _SFD_SYMBOL_SCOPE_ADD_EML_IMPORTABLE(&c11_j_nargin, 0U,
          c11_sf_marshallOut, c11_sf_marshallIn);
        _SFD_SYMBOL_SCOPE_ADD_EML_IMPORTABLE(&c11_j_nargout, 1U,
          c11_sf_marshallOut, c11_sf_marshallIn);
        *chartInstance->c11_collision = 1U;
        c11_updateDataWrittenToVector(chartInstance, 1U);
        _SFD_DATA_RANGE_CHECK((real_T)*chartInstance->c11_collision, 5U);
        _SFD_SYMBOL_SCOPE_POP();
      } else {
        _SFD_CS_CALL(STATE_ENTER_DURING_FUNCTION_TAG, 1U,
                     chartInstance->c11_sfEvent);
      }

      _SFD_CS_CALL(EXIT_OUT_OF_FUNCTION_TAG, 1U, chartInstance->c11_sfEvent);
      break;

     case c11_IN_WaitingTofreeUp:
      CV_STATE_EVAL(0, 0, 2);
      _SFD_CT_CALL(TRANSITION_BEFORE_PROCESSING_TAG, 1U,
                   chartInstance->c11_sfEvent);
      _SFD_SYMBOL_SCOPE_PUSH_EML(0U, 3U, 3U, c11_m_debug_family_names,
        c11_b_debug_family_var_map);
      _SFD_SYMBOL_SCOPE_ADD_EML_IMPORTABLE(&c11_k_nargin, 0U, c11_sf_marshallOut,
        c11_sf_marshallIn);
      _SFD_SYMBOL_SCOPE_ADD_EML_IMPORTABLE(&c11_k_nargout, 1U,
        c11_sf_marshallOut, c11_sf_marshallIn);
      _SFD_SYMBOL_SCOPE_ADD_EML_IMPORTABLE(&c11_d_out, 2U, c11_i_sf_marshallOut,
        c11_g_sf_marshallIn);
      c11_errorIfDataNotWrittenToFcn(chartInstance, 2U, 6U, 57U, 1, 17);
      c11_d_out = CV_EML_IF(1, 0, 0, CV_RELATIONAL_EVAL(5U, 1U, 0, (real_T)
        chartInstance->c11_collisionDetected, 0.0, 0, 0U,
        chartInstance->c11_collisionDetected == 0));
      _SFD_SYMBOL_SCOPE_POP();
      if (c11_d_out) {
        _SFD_CT_CALL(TRANSITION_ACTIVE_TAG, 1U, chartInstance->c11_sfEvent);
        chartInstance->c11_tp_WaitingTofreeUp = 0U;
        _SFD_CS_CALL(STATE_ENTER_EXIT_FUNCTION_TAG, 2U,
                     chartInstance->c11_sfEvent);
        _SFD_SYMBOL_SCOPE_PUSH_EML(0U, 2U, 2U, c11_h_debug_family_names,
          c11_debug_family_var_map);
        _SFD_SYMBOL_SCOPE_ADD_EML_IMPORTABLE(&c11_l_nargin, 0U,
          c11_sf_marshallOut, c11_sf_marshallIn);
        _SFD_SYMBOL_SCOPE_ADD_EML_IMPORTABLE(&c11_l_nargout, 1U,
          c11_sf_marshallOut, c11_sf_marshallIn);
        *chartInstance->c11_collision = 0U;
        c11_updateDataWrittenToVector(chartInstance, 1U);
        _SFD_DATA_RANGE_CHECK((real_T)*chartInstance->c11_collision, 5U);
        _SFD_SYMBOL_SCOPE_POP();
        _SFD_CS_CALL(EXIT_OUT_OF_FUNCTION_TAG, 2U, chartInstance->c11_sfEvent);
        _SFD_CS_CALL(STATE_INACTIVE_TAG, 2U, chartInstance->c11_sfEvent);
        chartInstance->c11_is_Collision = c11_IN_CheckForCollisions;
        _SFD_CS_CALL(STATE_ACTIVE_TAG, 1U, chartInstance->c11_sfEvent);
        chartInstance->c11_tp_CheckForCollisions = 1U;
      } else {
        _SFD_CS_CALL(STATE_ENTER_DURING_FUNCTION_TAG, 2U,
                     chartInstance->c11_sfEvent);
      }

      _SFD_CS_CALL(EXIT_OUT_OF_FUNCTION_TAG, 2U, chartInstance->c11_sfEvent);
      break;

     default:
      CV_STATE_EVAL(0, 0, 0);
      chartInstance->c11_is_Collision = c11_IN_NO_ACTIVE_CHILD;
      _SFD_CS_CALL(STATE_INACTIVE_TAG, 1U, chartInstance->c11_sfEvent);
      break;
    }

    _SFD_CS_CALL(EXIT_OUT_OF_FUNCTION_TAG, 0U, chartInstance->c11_sfEvent);
  }

  _SFD_CC_CALL(EXIT_OUT_OF_FUNCTION_TAG, 8U, chartInstance->c11_sfEvent);
}

static void initSimStructsc11_LessonIII(SFc11_LessonIIIInstanceStruct
  *chartInstance)
{
  (void)chartInstance;
}

static void init_script_number_translation(uint32_T c11_machineNumber, uint32_T
  c11_chartNumber, uint32_T c11_instanceNumber)
{
  (void)c11_machineNumber;
  (void)c11_chartNumber;
  (void)c11_instanceNumber;
}

static const mxArray *c11_sf_marshallOut(void *chartInstanceVoid, void
  *c11_inData)
{
  const mxArray *c11_mxArrayOutData = NULL;
  real_T c11_u;
  const mxArray *c11_y = NULL;
  SFc11_LessonIIIInstanceStruct *chartInstance;
  chartInstance = (SFc11_LessonIIIInstanceStruct *)chartInstanceVoid;
  c11_mxArrayOutData = NULL;
  c11_u = *(real_T *)c11_inData;
  c11_y = NULL;
  sf_mex_assign(&c11_y, sf_mex_create("y", &c11_u, 0, 0U, 0U, 0U, 0), false);
  sf_mex_assign(&c11_mxArrayOutData, c11_y, false);
  return c11_mxArrayOutData;
}

static real_T c11_emlrt_marshallIn(SFc11_LessonIIIInstanceStruct *chartInstance,
  const mxArray *c11_nargout, const char_T *c11_identifier)
{
  real_T c11_y;
  emlrtMsgIdentifier c11_thisId;
  c11_thisId.fIdentifier = c11_identifier;
  c11_thisId.fParent = NULL;
  c11_y = c11_b_emlrt_marshallIn(chartInstance, sf_mex_dup(c11_nargout),
    &c11_thisId);
  sf_mex_destroy(&c11_nargout);
  return c11_y;
}

static real_T c11_b_emlrt_marshallIn(SFc11_LessonIIIInstanceStruct
  *chartInstance, const mxArray *c11_u, const emlrtMsgIdentifier *c11_parentId)
{
  real_T c11_y;
  real_T c11_d0;
  (void)chartInstance;
  sf_mex_import(c11_parentId, sf_mex_dup(c11_u), &c11_d0, 1, 0, 0U, 0, 0U, 0);
  c11_y = c11_d0;
  sf_mex_destroy(&c11_u);
  return c11_y;
}

static void c11_sf_marshallIn(void *chartInstanceVoid, const mxArray
  *c11_mxArrayInData, const char_T *c11_varName, void *c11_outData)
{
  const mxArray *c11_nargout;
  const char_T *c11_identifier;
  emlrtMsgIdentifier c11_thisId;
  real_T c11_y;
  SFc11_LessonIIIInstanceStruct *chartInstance;
  chartInstance = (SFc11_LessonIIIInstanceStruct *)chartInstanceVoid;
  c11_nargout = sf_mex_dup(c11_mxArrayInData);
  c11_identifier = c11_varName;
  c11_thisId.fIdentifier = c11_identifier;
  c11_thisId.fParent = NULL;
  c11_y = c11_b_emlrt_marshallIn(chartInstance, sf_mex_dup(c11_nargout),
    &c11_thisId);
  sf_mex_destroy(&c11_nargout);
  *(real_T *)c11_outData = c11_y;
  sf_mex_destroy(&c11_mxArrayInData);
}

static const mxArray *c11_b_sf_marshallOut(void *chartInstanceVoid, void
  *c11_inData)
{
  const mxArray *c11_mxArrayOutData = NULL;
  c11_Waypoint c11_u;
  const mxArray *c11_y = NULL;
  int8_T c11_b_u;
  const mxArray *c11_b_y = NULL;
  int8_T c11_c_u;
  const mxArray *c11_c_y = NULL;
  int16_T c11_d_u;
  const mxArray *c11_d_y = NULL;
  SFc11_LessonIIIInstanceStruct *chartInstance;
  chartInstance = (SFc11_LessonIIIInstanceStruct *)chartInstanceVoid;
  c11_mxArrayOutData = NULL;
  c11_u = *(c11_Waypoint *)c11_inData;
  c11_y = NULL;
  sf_mex_assign(&c11_y, sf_mex_createstruct("structure", 2, 1, 1), false);
  c11_b_u = c11_u.x;
  c11_b_y = NULL;
  sf_mex_assign(&c11_b_y, sf_mex_create("y", &c11_b_u, 2, 0U, 0U, 0U, 0), false);
  sf_mex_addfield(c11_y, c11_b_y, "x", "x", 0);
  c11_c_u = c11_u.y;
  c11_c_y = NULL;
  sf_mex_assign(&c11_c_y, sf_mex_create("y", &c11_c_u, 2, 0U, 0U, 0U, 0), false);
  sf_mex_addfield(c11_y, c11_c_y, "y", "y", 0);
  c11_d_u = c11_u.orientation;
  c11_d_y = NULL;
  sf_mex_assign(&c11_d_y, sf_mex_create("y", &c11_d_u, 4, 0U, 0U, 0U, 0), false);
  sf_mex_addfield(c11_y, c11_d_y, "orientation", "orientation", 0);
  sf_mex_assign(&c11_mxArrayOutData, c11_y, false);
  return c11_mxArrayOutData;
}

static c11_Waypoint c11_c_emlrt_marshallIn(SFc11_LessonIIIInstanceStruct
  *chartInstance, const mxArray *c11_tempWay, const char_T *c11_identifier)
{
  c11_Waypoint c11_y;
  emlrtMsgIdentifier c11_thisId;
  c11_thisId.fIdentifier = c11_identifier;
  c11_thisId.fParent = NULL;
  c11_y = c11_d_emlrt_marshallIn(chartInstance, sf_mex_dup(c11_tempWay),
    &c11_thisId);
  sf_mex_destroy(&c11_tempWay);
  return c11_y;
}

static c11_Waypoint c11_d_emlrt_marshallIn(SFc11_LessonIIIInstanceStruct
  *chartInstance, const mxArray *c11_u, const emlrtMsgIdentifier *c11_parentId)
{
  c11_Waypoint c11_y;
  emlrtMsgIdentifier c11_thisId;
  static const char * c11_fieldNames[3] = { "x", "y", "orientation" };

  c11_thisId.fParent = c11_parentId;
  sf_mex_check_struct(c11_parentId, c11_u, 3, c11_fieldNames, 0U, NULL);
  c11_thisId.fIdentifier = "x";
  c11_y.x = c11_e_emlrt_marshallIn(chartInstance, sf_mex_dup(sf_mex_getfield
    (c11_u, "x", "x", 0)), &c11_thisId);
  c11_thisId.fIdentifier = "y";
  c11_y.y = c11_e_emlrt_marshallIn(chartInstance, sf_mex_dup(sf_mex_getfield
    (c11_u, "y", "y", 0)), &c11_thisId);
  c11_thisId.fIdentifier = "orientation";
  c11_y.orientation = c11_f_emlrt_marshallIn(chartInstance, sf_mex_dup
    (sf_mex_getfield(c11_u, "orientation", "orientation", 0)), &c11_thisId);
  sf_mex_destroy(&c11_u);
  return c11_y;
}

static int8_T c11_e_emlrt_marshallIn(SFc11_LessonIIIInstanceStruct
  *chartInstance, const mxArray *c11_u, const emlrtMsgIdentifier *c11_parentId)
{
  int8_T c11_y;
  int8_T c11_i2;
  (void)chartInstance;
  sf_mex_import(c11_parentId, sf_mex_dup(c11_u), &c11_i2, 1, 2, 0U, 0, 0U, 0);
  c11_y = c11_i2;
  sf_mex_destroy(&c11_u);
  return c11_y;
}

static int16_T c11_f_emlrt_marshallIn(SFc11_LessonIIIInstanceStruct
  *chartInstance, const mxArray *c11_u, const emlrtMsgIdentifier *c11_parentId)
{
  int16_T c11_y;
  int16_T c11_i3;
  (void)chartInstance;
  sf_mex_import(c11_parentId, sf_mex_dup(c11_u), &c11_i3, 1, 4, 0U, 0, 0U, 0);
  c11_y = c11_i3;
  sf_mex_destroy(&c11_u);
  return c11_y;
}

static void c11_b_sf_marshallIn(void *chartInstanceVoid, const mxArray
  *c11_mxArrayInData, const char_T *c11_varName, void *c11_outData)
{
  const mxArray *c11_tempWay;
  const char_T *c11_identifier;
  emlrtMsgIdentifier c11_thisId;
  c11_Waypoint c11_y;
  SFc11_LessonIIIInstanceStruct *chartInstance;
  chartInstance = (SFc11_LessonIIIInstanceStruct *)chartInstanceVoid;
  c11_tempWay = sf_mex_dup(c11_mxArrayInData);
  c11_identifier = c11_varName;
  c11_thisId.fIdentifier = c11_identifier;
  c11_thisId.fParent = NULL;
  c11_y = c11_d_emlrt_marshallIn(chartInstance, sf_mex_dup(c11_tempWay),
    &c11_thisId);
  sf_mex_destroy(&c11_tempWay);
  *(c11_Waypoint *)c11_outData = c11_y;
  sf_mex_destroy(&c11_mxArrayInData);
}

static const mxArray *c11_c_sf_marshallOut(void *chartInstanceVoid, void
  *c11_inData)
{
  const mxArray *c11_mxArrayOutData = NULL;
  int32_T c11_i4;
  int32_T c11_i5;
  int32_T c11_i6;
  real32_T c11_b_inData[4];
  int32_T c11_i7;
  int32_T c11_i8;
  int32_T c11_i9;
  real32_T c11_u[4];
  const mxArray *c11_y = NULL;
  SFc11_LessonIIIInstanceStruct *chartInstance;
  chartInstance = (SFc11_LessonIIIInstanceStruct *)chartInstanceVoid;
  c11_mxArrayOutData = NULL;
  c11_i4 = 0;
  for (c11_i5 = 0; c11_i5 < 2; c11_i5++) {
    for (c11_i6 = 0; c11_i6 < 2; c11_i6++) {
      c11_b_inData[c11_i6 + c11_i4] = (*(real32_T (*)[4])c11_inData)[c11_i6 +
        c11_i4];
    }

    c11_i4 += 2;
  }

  c11_i7 = 0;
  for (c11_i8 = 0; c11_i8 < 2; c11_i8++) {
    for (c11_i9 = 0; c11_i9 < 2; c11_i9++) {
      c11_u[c11_i9 + c11_i7] = c11_b_inData[c11_i9 + c11_i7];
    }

    c11_i7 += 2;
  }

  c11_y = NULL;
  sf_mex_assign(&c11_y, sf_mex_create("y", c11_u, 1, 0U, 1U, 0U, 2, 2, 2), false);
  sf_mex_assign(&c11_mxArrayOutData, c11_y, false);
  return c11_mxArrayOutData;
}

static void c11_g_emlrt_marshallIn(SFc11_LessonIIIInstanceStruct *chartInstance,
  const mxArray *c11_u, const emlrtMsgIdentifier *c11_parentId, real32_T c11_y[4])
{
  real32_T c11_fv0[4];
  int32_T c11_i10;
  (void)chartInstance;
  sf_mex_import(c11_parentId, sf_mex_dup(c11_u), c11_fv0, 1, 1, 0U, 1, 0U, 2, 2,
                2);
  for (c11_i10 = 0; c11_i10 < 4; c11_i10++) {
    c11_y[c11_i10] = c11_fv0[c11_i10];
  }

  sf_mex_destroy(&c11_u);
}

static void c11_c_sf_marshallIn(void *chartInstanceVoid, const mxArray
  *c11_mxArrayInData, const char_T *c11_varName, void *c11_outData)
{
  const mxArray *c11_R;
  const char_T *c11_identifier;
  emlrtMsgIdentifier c11_thisId;
  real32_T c11_y[4];
  int32_T c11_i11;
  int32_T c11_i12;
  int32_T c11_i13;
  SFc11_LessonIIIInstanceStruct *chartInstance;
  chartInstance = (SFc11_LessonIIIInstanceStruct *)chartInstanceVoid;
  c11_R = sf_mex_dup(c11_mxArrayInData);
  c11_identifier = c11_varName;
  c11_thisId.fIdentifier = c11_identifier;
  c11_thisId.fParent = NULL;
  c11_g_emlrt_marshallIn(chartInstance, sf_mex_dup(c11_R), &c11_thisId, c11_y);
  sf_mex_destroy(&c11_R);
  c11_i11 = 0;
  for (c11_i12 = 0; c11_i12 < 2; c11_i12++) {
    for (c11_i13 = 0; c11_i13 < 2; c11_i13++) {
      (*(real32_T (*)[4])c11_outData)[c11_i13 + c11_i11] = c11_y[c11_i13 +
        c11_i11];
    }

    c11_i11 += 2;
  }

  sf_mex_destroy(&c11_mxArrayInData);
}

static const mxArray *c11_d_sf_marshallOut(void *chartInstanceVoid, void
  *c11_inData)
{
  const mxArray *c11_mxArrayOutData = NULL;
  real32_T c11_u;
  const mxArray *c11_y = NULL;
  SFc11_LessonIIIInstanceStruct *chartInstance;
  chartInstance = (SFc11_LessonIIIInstanceStruct *)chartInstanceVoid;
  c11_mxArrayOutData = NULL;
  c11_u = *(real32_T *)c11_inData;
  c11_y = NULL;
  sf_mex_assign(&c11_y, sf_mex_create("y", &c11_u, 1, 0U, 0U, 0U, 0), false);
  sf_mex_assign(&c11_mxArrayOutData, c11_y, false);
  return c11_mxArrayOutData;
}

static real32_T c11_h_emlrt_marshallIn(SFc11_LessonIIIInstanceStruct
  *chartInstance, const mxArray *c11_u, const emlrtMsgIdentifier *c11_parentId)
{
  real32_T c11_y;
  real32_T c11_f0;
  (void)chartInstance;
  sf_mex_import(c11_parentId, sf_mex_dup(c11_u), &c11_f0, 1, 1, 0U, 0, 0U, 0);
  c11_y = c11_f0;
  sf_mex_destroy(&c11_u);
  return c11_y;
}

static void c11_d_sf_marshallIn(void *chartInstanceVoid, const mxArray
  *c11_mxArrayInData, const char_T *c11_varName, void *c11_outData)
{
  const mxArray *c11_angle;
  const char_T *c11_identifier;
  emlrtMsgIdentifier c11_thisId;
  real32_T c11_y;
  SFc11_LessonIIIInstanceStruct *chartInstance;
  chartInstance = (SFc11_LessonIIIInstanceStruct *)chartInstanceVoid;
  c11_angle = sf_mex_dup(c11_mxArrayInData);
  c11_identifier = c11_varName;
  c11_thisId.fIdentifier = c11_identifier;
  c11_thisId.fParent = NULL;
  c11_y = c11_h_emlrt_marshallIn(chartInstance, sf_mex_dup(c11_angle),
    &c11_thisId);
  sf_mex_destroy(&c11_angle);
  *(real32_T *)c11_outData = c11_y;
  sf_mex_destroy(&c11_mxArrayInData);
}

static const mxArray *c11_e_sf_marshallOut(void *chartInstanceVoid, void
  *c11_inData)
{
  const mxArray *c11_mxArrayOutData = NULL;
  int32_T c11_i14;
  int8_T c11_b_inData[2];
  int32_T c11_i15;
  int8_T c11_u[2];
  const mxArray *c11_y = NULL;
  SFc11_LessonIIIInstanceStruct *chartInstance;
  chartInstance = (SFc11_LessonIIIInstanceStruct *)chartInstanceVoid;
  c11_mxArrayOutData = NULL;
  for (c11_i14 = 0; c11_i14 < 2; c11_i14++) {
    c11_b_inData[c11_i14] = (*(int8_T (*)[2])c11_inData)[c11_i14];
  }

  for (c11_i15 = 0; c11_i15 < 2; c11_i15++) {
    c11_u[c11_i15] = c11_b_inData[c11_i15];
  }

  c11_y = NULL;
  sf_mex_assign(&c11_y, sf_mex_create("y", c11_u, 2, 0U, 1U, 0U, 1, 2), false);
  sf_mex_assign(&c11_mxArrayOutData, c11_y, false);
  return c11_mxArrayOutData;
}

static const mxArray *c11_f_sf_marshallOut(void *chartInstanceVoid, void
  *c11_inData)
{
  const mxArray *c11_mxArrayOutData = NULL;
  int32_T c11_i16;
  real32_T c11_b_inData[2];
  int32_T c11_i17;
  real32_T c11_u[2];
  const mxArray *c11_y = NULL;
  SFc11_LessonIIIInstanceStruct *chartInstance;
  chartInstance = (SFc11_LessonIIIInstanceStruct *)chartInstanceVoid;
  c11_mxArrayOutData = NULL;
  for (c11_i16 = 0; c11_i16 < 2; c11_i16++) {
    c11_b_inData[c11_i16] = (*(real32_T (*)[2])c11_inData)[c11_i16];
  }

  for (c11_i17 = 0; c11_i17 < 2; c11_i17++) {
    c11_u[c11_i17] = c11_b_inData[c11_i17];
  }

  c11_y = NULL;
  sf_mex_assign(&c11_y, sf_mex_create("y", c11_u, 1, 0U, 1U, 0U, 1, 2), false);
  sf_mex_assign(&c11_mxArrayOutData, c11_y, false);
  return c11_mxArrayOutData;
}

static void c11_i_emlrt_marshallIn(SFc11_LessonIIIInstanceStruct *chartInstance,
  const mxArray *c11_u, const emlrtMsgIdentifier *c11_parentId, real32_T c11_y[2])
{
  real32_T c11_fv1[2];
  int32_T c11_i18;
  (void)chartInstance;
  sf_mex_import(c11_parentId, sf_mex_dup(c11_u), c11_fv1, 1, 1, 0U, 1, 0U, 1, 2);
  for (c11_i18 = 0; c11_i18 < 2; c11_i18++) {
    c11_y[c11_i18] = c11_fv1[c11_i18];
  }

  sf_mex_destroy(&c11_u);
}

static void c11_e_sf_marshallIn(void *chartInstanceVoid, const mxArray
  *c11_mxArrayInData, const char_T *c11_varName, void *c11_outData)
{
  const mxArray *c11_diffVecx;
  const char_T *c11_identifier;
  emlrtMsgIdentifier c11_thisId;
  real32_T c11_y[2];
  int32_T c11_i19;
  SFc11_LessonIIIInstanceStruct *chartInstance;
  chartInstance = (SFc11_LessonIIIInstanceStruct *)chartInstanceVoid;
  c11_diffVecx = sf_mex_dup(c11_mxArrayInData);
  c11_identifier = c11_varName;
  c11_thisId.fIdentifier = c11_identifier;
  c11_thisId.fParent = NULL;
  c11_i_emlrt_marshallIn(chartInstance, sf_mex_dup(c11_diffVecx), &c11_thisId,
    c11_y);
  sf_mex_destroy(&c11_diffVecx);
  for (c11_i19 = 0; c11_i19 < 2; c11_i19++) {
    (*(real32_T (*)[2])c11_outData)[c11_i19] = c11_y[c11_i19];
  }

  sf_mex_destroy(&c11_mxArrayInData);
}

static const mxArray *c11_g_sf_marshallOut(void *chartInstanceVoid, void
  *c11_inData)
{
  const mxArray *c11_mxArrayOutData = NULL;
  int8_T c11_u;
  const mxArray *c11_y = NULL;
  SFc11_LessonIIIInstanceStruct *chartInstance;
  chartInstance = (SFc11_LessonIIIInstanceStruct *)chartInstanceVoid;
  c11_mxArrayOutData = NULL;
  c11_u = *(int8_T *)c11_inData;
  c11_y = NULL;
  sf_mex_assign(&c11_y, sf_mex_create("y", &c11_u, 2, 0U, 0U, 0U, 0), false);
  sf_mex_assign(&c11_mxArrayOutData, c11_y, false);
  return c11_mxArrayOutData;
}

static void c11_f_sf_marshallIn(void *chartInstanceVoid, const mxArray
  *c11_mxArrayInData, const char_T *c11_varName, void *c11_outData)
{
  const mxArray *c11_maxy;
  const char_T *c11_identifier;
  emlrtMsgIdentifier c11_thisId;
  int8_T c11_y;
  SFc11_LessonIIIInstanceStruct *chartInstance;
  chartInstance = (SFc11_LessonIIIInstanceStruct *)chartInstanceVoid;
  c11_maxy = sf_mex_dup(c11_mxArrayInData);
  c11_identifier = c11_varName;
  c11_thisId.fIdentifier = c11_identifier;
  c11_thisId.fParent = NULL;
  c11_y = c11_e_emlrt_marshallIn(chartInstance, sf_mex_dup(c11_maxy),
    &c11_thisId);
  sf_mex_destroy(&c11_maxy);
  *(int8_T *)c11_outData = c11_y;
  sf_mex_destroy(&c11_mxArrayInData);
}

static const mxArray *c11_h_sf_marshallOut(void *chartInstanceVoid, void
  *c11_inData)
{
  const mxArray *c11_mxArrayOutData = NULL;
  int32_T c11_i20;
  int32_T c11_i21;
  int32_T c11_i22;
  int8_T c11_b_inData[8];
  int32_T c11_i23;
  int32_T c11_i24;
  int32_T c11_i25;
  int8_T c11_u[8];
  const mxArray *c11_y = NULL;
  SFc11_LessonIIIInstanceStruct *chartInstance;
  chartInstance = (SFc11_LessonIIIInstanceStruct *)chartInstanceVoid;
  c11_mxArrayOutData = NULL;
  c11_i20 = 0;
  for (c11_i21 = 0; c11_i21 < 4; c11_i21++) {
    for (c11_i22 = 0; c11_i22 < 2; c11_i22++) {
      c11_b_inData[c11_i22 + c11_i20] = (*(int8_T (*)[8])c11_inData)[c11_i22 +
        c11_i20];
    }

    c11_i20 += 2;
  }

  c11_i23 = 0;
  for (c11_i24 = 0; c11_i24 < 4; c11_i24++) {
    for (c11_i25 = 0; c11_i25 < 2; c11_i25++) {
      c11_u[c11_i25 + c11_i23] = c11_b_inData[c11_i25 + c11_i23];
    }

    c11_i23 += 2;
  }

  c11_y = NULL;
  sf_mex_assign(&c11_y, sf_mex_create("y", c11_u, 2, 0U, 1U, 0U, 2, 2, 4), false);
  sf_mex_assign(&c11_mxArrayOutData, c11_y, false);
  return c11_mxArrayOutData;
}

static const mxArray *c11_i_sf_marshallOut(void *chartInstanceVoid, void
  *c11_inData)
{
  const mxArray *c11_mxArrayOutData = NULL;
  boolean_T c11_u;
  const mxArray *c11_y = NULL;
  SFc11_LessonIIIInstanceStruct *chartInstance;
  chartInstance = (SFc11_LessonIIIInstanceStruct *)chartInstanceVoid;
  c11_mxArrayOutData = NULL;
  c11_u = *(boolean_T *)c11_inData;
  c11_y = NULL;
  sf_mex_assign(&c11_y, sf_mex_create("y", &c11_u, 11, 0U, 0U, 0U, 0), false);
  sf_mex_assign(&c11_mxArrayOutData, c11_y, false);
  return c11_mxArrayOutData;
}

static boolean_T c11_j_emlrt_marshallIn(SFc11_LessonIIIInstanceStruct
  *chartInstance, const mxArray *c11_u, const emlrtMsgIdentifier *c11_parentId)
{
  boolean_T c11_y;
  boolean_T c11_b0;
  (void)chartInstance;
  sf_mex_import(c11_parentId, sf_mex_dup(c11_u), &c11_b0, 1, 11, 0U, 0, 0U, 0);
  c11_y = c11_b0;
  sf_mex_destroy(&c11_u);
  return c11_y;
}

static void c11_g_sf_marshallIn(void *chartInstanceVoid, const mxArray
  *c11_mxArrayInData, const char_T *c11_varName, void *c11_outData)
{
  const mxArray *c11_sf_internal_predicateOutput;
  const char_T *c11_identifier;
  emlrtMsgIdentifier c11_thisId;
  boolean_T c11_y;
  SFc11_LessonIIIInstanceStruct *chartInstance;
  chartInstance = (SFc11_LessonIIIInstanceStruct *)chartInstanceVoid;
  c11_sf_internal_predicateOutput = sf_mex_dup(c11_mxArrayInData);
  c11_identifier = c11_varName;
  c11_thisId.fIdentifier = c11_identifier;
  c11_thisId.fParent = NULL;
  c11_y = c11_j_emlrt_marshallIn(chartInstance, sf_mex_dup
    (c11_sf_internal_predicateOutput), &c11_thisId);
  sf_mex_destroy(&c11_sf_internal_predicateOutput);
  *(boolean_T *)c11_outData = c11_y;
  sf_mex_destroy(&c11_mxArrayInData);
}

const mxArray *sf_c11_LessonIII_get_eml_resolved_functions_info(void)
{
  const mxArray *c11_nameCaptureInfo = NULL;
  c11_nameCaptureInfo = NULL;
  sf_mex_assign(&c11_nameCaptureInfo, sf_mex_createstruct("structure", 2, 87, 1),
                false);
  c11_info_helper(&c11_nameCaptureInfo);
  c11_b_info_helper(&c11_nameCaptureInfo);
  sf_mex_emlrtNameCapturePostProcessR2012a(&c11_nameCaptureInfo);
  return c11_nameCaptureInfo;
}

static void c11_info_helper(const mxArray **c11_info)
{
  const mxArray *c11_rhs0 = NULL;
  const mxArray *c11_lhs0 = NULL;
  const mxArray *c11_rhs1 = NULL;
  const mxArray *c11_lhs1 = NULL;
  const mxArray *c11_rhs2 = NULL;
  const mxArray *c11_lhs2 = NULL;
  const mxArray *c11_rhs3 = NULL;
  const mxArray *c11_lhs3 = NULL;
  const mxArray *c11_rhs4 = NULL;
  const mxArray *c11_lhs4 = NULL;
  const mxArray *c11_rhs5 = NULL;
  const mxArray *c11_lhs5 = NULL;
  const mxArray *c11_rhs6 = NULL;
  const mxArray *c11_lhs6 = NULL;
  const mxArray *c11_rhs7 = NULL;
  const mxArray *c11_lhs7 = NULL;
  const mxArray *c11_rhs8 = NULL;
  const mxArray *c11_lhs8 = NULL;
  const mxArray *c11_rhs9 = NULL;
  const mxArray *c11_lhs9 = NULL;
  const mxArray *c11_rhs10 = NULL;
  const mxArray *c11_lhs10 = NULL;
  const mxArray *c11_rhs11 = NULL;
  const mxArray *c11_lhs11 = NULL;
  const mxArray *c11_rhs12 = NULL;
  const mxArray *c11_lhs12 = NULL;
  const mxArray *c11_rhs13 = NULL;
  const mxArray *c11_lhs13 = NULL;
  const mxArray *c11_rhs14 = NULL;
  const mxArray *c11_lhs14 = NULL;
  const mxArray *c11_rhs15 = NULL;
  const mxArray *c11_lhs15 = NULL;
  const mxArray *c11_rhs16 = NULL;
  const mxArray *c11_lhs16 = NULL;
  const mxArray *c11_rhs17 = NULL;
  const mxArray *c11_lhs17 = NULL;
  const mxArray *c11_rhs18 = NULL;
  const mxArray *c11_lhs18 = NULL;
  const mxArray *c11_rhs19 = NULL;
  const mxArray *c11_lhs19 = NULL;
  const mxArray *c11_rhs20 = NULL;
  const mxArray *c11_lhs20 = NULL;
  const mxArray *c11_rhs21 = NULL;
  const mxArray *c11_lhs21 = NULL;
  const mxArray *c11_rhs22 = NULL;
  const mxArray *c11_lhs22 = NULL;
  const mxArray *c11_rhs23 = NULL;
  const mxArray *c11_lhs23 = NULL;
  const mxArray *c11_rhs24 = NULL;
  const mxArray *c11_lhs24 = NULL;
  const mxArray *c11_rhs25 = NULL;
  const mxArray *c11_lhs25 = NULL;
  const mxArray *c11_rhs26 = NULL;
  const mxArray *c11_lhs26 = NULL;
  const mxArray *c11_rhs27 = NULL;
  const mxArray *c11_lhs27 = NULL;
  const mxArray *c11_rhs28 = NULL;
  const mxArray *c11_lhs28 = NULL;
  const mxArray *c11_rhs29 = NULL;
  const mxArray *c11_lhs29 = NULL;
  const mxArray *c11_rhs30 = NULL;
  const mxArray *c11_lhs30 = NULL;
  const mxArray *c11_rhs31 = NULL;
  const mxArray *c11_lhs31 = NULL;
  const mxArray *c11_rhs32 = NULL;
  const mxArray *c11_lhs32 = NULL;
  const mxArray *c11_rhs33 = NULL;
  const mxArray *c11_lhs33 = NULL;
  const mxArray *c11_rhs34 = NULL;
  const mxArray *c11_lhs34 = NULL;
  const mxArray *c11_rhs35 = NULL;
  const mxArray *c11_lhs35 = NULL;
  const mxArray *c11_rhs36 = NULL;
  const mxArray *c11_lhs36 = NULL;
  const mxArray *c11_rhs37 = NULL;
  const mxArray *c11_lhs37 = NULL;
  const mxArray *c11_rhs38 = NULL;
  const mxArray *c11_lhs38 = NULL;
  const mxArray *c11_rhs39 = NULL;
  const mxArray *c11_lhs39 = NULL;
  const mxArray *c11_rhs40 = NULL;
  const mxArray *c11_lhs40 = NULL;
  const mxArray *c11_rhs41 = NULL;
  const mxArray *c11_lhs41 = NULL;
  const mxArray *c11_rhs42 = NULL;
  const mxArray *c11_lhs42 = NULL;
  const mxArray *c11_rhs43 = NULL;
  const mxArray *c11_lhs43 = NULL;
  const mxArray *c11_rhs44 = NULL;
  const mxArray *c11_lhs44 = NULL;
  const mxArray *c11_rhs45 = NULL;
  const mxArray *c11_lhs45 = NULL;
  const mxArray *c11_rhs46 = NULL;
  const mxArray *c11_lhs46 = NULL;
  const mxArray *c11_rhs47 = NULL;
  const mxArray *c11_lhs47 = NULL;
  const mxArray *c11_rhs48 = NULL;
  const mxArray *c11_lhs48 = NULL;
  const mxArray *c11_rhs49 = NULL;
  const mxArray *c11_lhs49 = NULL;
  const mxArray *c11_rhs50 = NULL;
  const mxArray *c11_lhs50 = NULL;
  const mxArray *c11_rhs51 = NULL;
  const mxArray *c11_lhs51 = NULL;
  const mxArray *c11_rhs52 = NULL;
  const mxArray *c11_lhs52 = NULL;
  const mxArray *c11_rhs53 = NULL;
  const mxArray *c11_lhs53 = NULL;
  const mxArray *c11_rhs54 = NULL;
  const mxArray *c11_lhs54 = NULL;
  const mxArray *c11_rhs55 = NULL;
  const mxArray *c11_lhs55 = NULL;
  const mxArray *c11_rhs56 = NULL;
  const mxArray *c11_lhs56 = NULL;
  const mxArray *c11_rhs57 = NULL;
  const mxArray *c11_lhs57 = NULL;
  const mxArray *c11_rhs58 = NULL;
  const mxArray *c11_lhs58 = NULL;
  const mxArray *c11_rhs59 = NULL;
  const mxArray *c11_lhs59 = NULL;
  const mxArray *c11_rhs60 = NULL;
  const mxArray *c11_lhs60 = NULL;
  const mxArray *c11_rhs61 = NULL;
  const mxArray *c11_lhs61 = NULL;
  const mxArray *c11_rhs62 = NULL;
  const mxArray *c11_lhs62 = NULL;
  const mxArray *c11_rhs63 = NULL;
  const mxArray *c11_lhs63 = NULL;
  sf_mex_addfield(*c11_info, c11_emlrt_marshallOut(""), "context", "context", 0);
  sf_mex_addfield(*c11_info, c11_emlrt_marshallOut("atan2d"), "name", "name", 0);
  sf_mex_addfield(*c11_info, c11_emlrt_marshallOut("single"), "dominantType",
                  "dominantType", 0);
  sf_mex_addfield(*c11_info, c11_emlrt_marshallOut(
    "[ILXE]$matlabroot$/toolbox/eml/lib/matlab/elfun/atan2d.m"), "resolved",
                  "resolved", 0);
  sf_mex_addfield(*c11_info, c11_b_emlrt_marshallOut(1395332096U), "fileTimeLo",
                  "fileTimeLo", 0);
  sf_mex_addfield(*c11_info, c11_b_emlrt_marshallOut(0U), "fileTimeHi",
                  "fileTimeHi", 0);
  sf_mex_addfield(*c11_info, c11_b_emlrt_marshallOut(0U), "mFileTimeLo",
                  "mFileTimeLo", 0);
  sf_mex_addfield(*c11_info, c11_b_emlrt_marshallOut(0U), "mFileTimeHi",
                  "mFileTimeHi", 0);
  sf_mex_assign(&c11_rhs0, sf_mex_createcellmatrix(0, 1), false);
  sf_mex_assign(&c11_lhs0, sf_mex_createcellmatrix(0, 1), false);
  sf_mex_addfield(*c11_info, sf_mex_duplicatearraysafe(&c11_rhs0), "rhs", "rhs",
                  0);
  sf_mex_addfield(*c11_info, sf_mex_duplicatearraysafe(&c11_lhs0), "lhs", "lhs",
                  0);
  sf_mex_addfield(*c11_info, c11_emlrt_marshallOut(
    "[ILXE]$matlabroot$/toolbox/eml/lib/matlab/elfun/atan2d.m"), "context",
                  "context", 1);
  sf_mex_addfield(*c11_info, c11_emlrt_marshallOut("eml_scalar_eg"), "name",
                  "name", 1);
  sf_mex_addfield(*c11_info, c11_emlrt_marshallOut("single"), "dominantType",
                  "dominantType", 1);
  sf_mex_addfield(*c11_info, c11_emlrt_marshallOut(
    "[ILXE]$matlabroot$/toolbox/eml/lib/matlab/eml/eml_scalar_eg.m"), "resolved",
                  "resolved", 1);
  sf_mex_addfield(*c11_info, c11_b_emlrt_marshallOut(1375984288U), "fileTimeLo",
                  "fileTimeLo", 1);
  sf_mex_addfield(*c11_info, c11_b_emlrt_marshallOut(0U), "fileTimeHi",
                  "fileTimeHi", 1);
  sf_mex_addfield(*c11_info, c11_b_emlrt_marshallOut(0U), "mFileTimeLo",
                  "mFileTimeLo", 1);
  sf_mex_addfield(*c11_info, c11_b_emlrt_marshallOut(0U), "mFileTimeHi",
                  "mFileTimeHi", 1);
  sf_mex_assign(&c11_rhs1, sf_mex_createcellmatrix(0, 1), false);
  sf_mex_assign(&c11_lhs1, sf_mex_createcellmatrix(0, 1), false);
  sf_mex_addfield(*c11_info, sf_mex_duplicatearraysafe(&c11_rhs1), "rhs", "rhs",
                  1);
  sf_mex_addfield(*c11_info, sf_mex_duplicatearraysafe(&c11_lhs1), "lhs", "lhs",
                  1);
  sf_mex_addfield(*c11_info, c11_emlrt_marshallOut(
    "[ILXE]$matlabroot$/toolbox/eml/lib/matlab/eml/eml_scalar_eg.m"), "context",
                  "context", 2);
  sf_mex_addfield(*c11_info, c11_emlrt_marshallOut("coder.internal.scalarEg"),
                  "name", "name", 2);
  sf_mex_addfield(*c11_info, c11_emlrt_marshallOut("single"), "dominantType",
                  "dominantType", 2);
  sf_mex_addfield(*c11_info, c11_emlrt_marshallOut(
    "[ILXE]$matlabroot$/toolbox/coder/coder/+coder/+internal/scalarEg.p"),
                  "resolved", "resolved", 2);
  sf_mex_addfield(*c11_info, c11_b_emlrt_marshallOut(1410811370U), "fileTimeLo",
                  "fileTimeLo", 2);
  sf_mex_addfield(*c11_info, c11_b_emlrt_marshallOut(0U), "fileTimeHi",
                  "fileTimeHi", 2);
  sf_mex_addfield(*c11_info, c11_b_emlrt_marshallOut(0U), "mFileTimeLo",
                  "mFileTimeLo", 2);
  sf_mex_addfield(*c11_info, c11_b_emlrt_marshallOut(0U), "mFileTimeHi",
                  "mFileTimeHi", 2);
  sf_mex_assign(&c11_rhs2, sf_mex_createcellmatrix(0, 1), false);
  sf_mex_assign(&c11_lhs2, sf_mex_createcellmatrix(0, 1), false);
  sf_mex_addfield(*c11_info, sf_mex_duplicatearraysafe(&c11_rhs2), "rhs", "rhs",
                  2);
  sf_mex_addfield(*c11_info, sf_mex_duplicatearraysafe(&c11_lhs2), "lhs", "lhs",
                  2);
  sf_mex_addfield(*c11_info, c11_emlrt_marshallOut(
    "[ILXE]$matlabroot$/toolbox/eml/lib/matlab/elfun/atan2d.m"), "context",
                  "context", 3);
  sf_mex_addfield(*c11_info, c11_emlrt_marshallOut("eml_scalexp_alloc"), "name",
                  "name", 3);
  sf_mex_addfield(*c11_info, c11_emlrt_marshallOut("single"), "dominantType",
                  "dominantType", 3);
  sf_mex_addfield(*c11_info, c11_emlrt_marshallOut(
    "[ILXE]$matlabroot$/toolbox/eml/lib/matlab/eml/eml_scalexp_alloc.m"),
                  "resolved", "resolved", 3);
  sf_mex_addfield(*c11_info, c11_b_emlrt_marshallOut(1375984288U), "fileTimeLo",
                  "fileTimeLo", 3);
  sf_mex_addfield(*c11_info, c11_b_emlrt_marshallOut(0U), "fileTimeHi",
                  "fileTimeHi", 3);
  sf_mex_addfield(*c11_info, c11_b_emlrt_marshallOut(0U), "mFileTimeLo",
                  "mFileTimeLo", 3);
  sf_mex_addfield(*c11_info, c11_b_emlrt_marshallOut(0U), "mFileTimeHi",
                  "mFileTimeHi", 3);
  sf_mex_assign(&c11_rhs3, sf_mex_createcellmatrix(0, 1), false);
  sf_mex_assign(&c11_lhs3, sf_mex_createcellmatrix(0, 1), false);
  sf_mex_addfield(*c11_info, sf_mex_duplicatearraysafe(&c11_rhs3), "rhs", "rhs",
                  3);
  sf_mex_addfield(*c11_info, sf_mex_duplicatearraysafe(&c11_lhs3), "lhs", "lhs",
                  3);
  sf_mex_addfield(*c11_info, c11_emlrt_marshallOut(
    "[ILXE]$matlabroot$/toolbox/eml/lib/matlab/eml/eml_scalexp_alloc.m"),
                  "context", "context", 4);
  sf_mex_addfield(*c11_info, c11_emlrt_marshallOut("coder.internal.scalexpAlloc"),
                  "name", "name", 4);
  sf_mex_addfield(*c11_info, c11_emlrt_marshallOut("single"), "dominantType",
                  "dominantType", 4);
  sf_mex_addfield(*c11_info, c11_emlrt_marshallOut(
    "[ILXE]$matlabroot$/toolbox/coder/coder/+coder/+internal/scalexpAlloc.p"),
                  "resolved", "resolved", 4);
  sf_mex_addfield(*c11_info, c11_b_emlrt_marshallOut(1410811370U), "fileTimeLo",
                  "fileTimeLo", 4);
  sf_mex_addfield(*c11_info, c11_b_emlrt_marshallOut(0U), "fileTimeHi",
                  "fileTimeHi", 4);
  sf_mex_addfield(*c11_info, c11_b_emlrt_marshallOut(0U), "mFileTimeLo",
                  "mFileTimeLo", 4);
  sf_mex_addfield(*c11_info, c11_b_emlrt_marshallOut(0U), "mFileTimeHi",
                  "mFileTimeHi", 4);
  sf_mex_assign(&c11_rhs4, sf_mex_createcellmatrix(0, 1), false);
  sf_mex_assign(&c11_lhs4, sf_mex_createcellmatrix(0, 1), false);
  sf_mex_addfield(*c11_info, sf_mex_duplicatearraysafe(&c11_rhs4), "rhs", "rhs",
                  4);
  sf_mex_addfield(*c11_info, sf_mex_duplicatearraysafe(&c11_lhs4), "lhs", "lhs",
                  4);
  sf_mex_addfield(*c11_info, c11_emlrt_marshallOut(
    "[ILXE]$matlabroot$/toolbox/eml/lib/matlab/elfun/atan2d.m"), "context",
                  "context", 5);
  sf_mex_addfield(*c11_info, c11_emlrt_marshallOut("eml_scalar_atan2"), "name",
                  "name", 5);
  sf_mex_addfield(*c11_info, c11_emlrt_marshallOut("single"), "dominantType",
                  "dominantType", 5);
  sf_mex_addfield(*c11_info, c11_emlrt_marshallOut(
    "[ILXE]$matlabroot$/toolbox/eml/lib/matlab/elfun/eml_scalar_atan2.m"),
                  "resolved", "resolved", 5);
  sf_mex_addfield(*c11_info, c11_b_emlrt_marshallOut(1286822320U), "fileTimeLo",
                  "fileTimeLo", 5);
  sf_mex_addfield(*c11_info, c11_b_emlrt_marshallOut(0U), "fileTimeHi",
                  "fileTimeHi", 5);
  sf_mex_addfield(*c11_info, c11_b_emlrt_marshallOut(0U), "mFileTimeLo",
                  "mFileTimeLo", 5);
  sf_mex_addfield(*c11_info, c11_b_emlrt_marshallOut(0U), "mFileTimeHi",
                  "mFileTimeHi", 5);
  sf_mex_assign(&c11_rhs5, sf_mex_createcellmatrix(0, 1), false);
  sf_mex_assign(&c11_lhs5, sf_mex_createcellmatrix(0, 1), false);
  sf_mex_addfield(*c11_info, sf_mex_duplicatearraysafe(&c11_rhs5), "rhs", "rhs",
                  5);
  sf_mex_addfield(*c11_info, sf_mex_duplicatearraysafe(&c11_lhs5), "lhs", "lhs",
                  5);
  sf_mex_addfield(*c11_info, c11_emlrt_marshallOut(
    "[ILXE]$matlabroot$/toolbox/eml/lib/matlab/elfun/atan2d.m"), "context",
                  "context", 6);
  sf_mex_addfield(*c11_info, c11_emlrt_marshallOut("eml_mtimes_helper"), "name",
                  "name", 6);
  sf_mex_addfield(*c11_info, c11_emlrt_marshallOut(""), "dominantType",
                  "dominantType", 6);
  sf_mex_addfield(*c11_info, c11_emlrt_marshallOut(
    "[ILXE]$matlabroot$/toolbox/eml/lib/matlab/ops/eml_mtimes_helper.m"),
                  "resolved", "resolved", 6);
  sf_mex_addfield(*c11_info, c11_b_emlrt_marshallOut(1383880894U), "fileTimeLo",
                  "fileTimeLo", 6);
  sf_mex_addfield(*c11_info, c11_b_emlrt_marshallOut(0U), "fileTimeHi",
                  "fileTimeHi", 6);
  sf_mex_addfield(*c11_info, c11_b_emlrt_marshallOut(0U), "mFileTimeLo",
                  "mFileTimeLo", 6);
  sf_mex_addfield(*c11_info, c11_b_emlrt_marshallOut(0U), "mFileTimeHi",
                  "mFileTimeHi", 6);
  sf_mex_assign(&c11_rhs6, sf_mex_createcellmatrix(0, 1), false);
  sf_mex_assign(&c11_lhs6, sf_mex_createcellmatrix(0, 1), false);
  sf_mex_addfield(*c11_info, sf_mex_duplicatearraysafe(&c11_rhs6), "rhs", "rhs",
                  6);
  sf_mex_addfield(*c11_info, sf_mex_duplicatearraysafe(&c11_lhs6), "lhs", "lhs",
                  6);
  sf_mex_addfield(*c11_info, c11_emlrt_marshallOut(
    "[ILXE]$matlabroot$/toolbox/eml/lib/matlab/ops/eml_mtimes_helper.m!common_checks"),
                  "context", "context", 7);
  sf_mex_addfield(*c11_info, c11_emlrt_marshallOut(
    "coder.internal.isBuiltInNumeric"), "name", "name", 7);
  sf_mex_addfield(*c11_info, c11_emlrt_marshallOut("double"), "dominantType",
                  "dominantType", 7);
  sf_mex_addfield(*c11_info, c11_emlrt_marshallOut(
    "[ILXE]$matlabroot$/toolbox/shared/coder/coder/+coder/+internal/isBuiltInNumeric.m"),
                  "resolved", "resolved", 7);
  sf_mex_addfield(*c11_info, c11_b_emlrt_marshallOut(1395935456U), "fileTimeLo",
                  "fileTimeLo", 7);
  sf_mex_addfield(*c11_info, c11_b_emlrt_marshallOut(0U), "fileTimeHi",
                  "fileTimeHi", 7);
  sf_mex_addfield(*c11_info, c11_b_emlrt_marshallOut(0U), "mFileTimeLo",
                  "mFileTimeLo", 7);
  sf_mex_addfield(*c11_info, c11_b_emlrt_marshallOut(0U), "mFileTimeHi",
                  "mFileTimeHi", 7);
  sf_mex_assign(&c11_rhs7, sf_mex_createcellmatrix(0, 1), false);
  sf_mex_assign(&c11_lhs7, sf_mex_createcellmatrix(0, 1), false);
  sf_mex_addfield(*c11_info, sf_mex_duplicatearraysafe(&c11_rhs7), "rhs", "rhs",
                  7);
  sf_mex_addfield(*c11_info, sf_mex_duplicatearraysafe(&c11_lhs7), "lhs", "lhs",
                  7);
  sf_mex_addfield(*c11_info, c11_emlrt_marshallOut(
    "[ILXE]$matlabroot$/toolbox/eml/lib/matlab/ops/eml_mtimes_helper.m!common_checks"),
                  "context", "context", 8);
  sf_mex_addfield(*c11_info, c11_emlrt_marshallOut(
    "coder.internal.isBuiltInNumeric"), "name", "name", 8);
  sf_mex_addfield(*c11_info, c11_emlrt_marshallOut("single"), "dominantType",
                  "dominantType", 8);
  sf_mex_addfield(*c11_info, c11_emlrt_marshallOut(
    "[ILXE]$matlabroot$/toolbox/shared/coder/coder/+coder/+internal/isBuiltInNumeric.m"),
                  "resolved", "resolved", 8);
  sf_mex_addfield(*c11_info, c11_b_emlrt_marshallOut(1395935456U), "fileTimeLo",
                  "fileTimeLo", 8);
  sf_mex_addfield(*c11_info, c11_b_emlrt_marshallOut(0U), "fileTimeHi",
                  "fileTimeHi", 8);
  sf_mex_addfield(*c11_info, c11_b_emlrt_marshallOut(0U), "mFileTimeLo",
                  "mFileTimeLo", 8);
  sf_mex_addfield(*c11_info, c11_b_emlrt_marshallOut(0U), "mFileTimeHi",
                  "mFileTimeHi", 8);
  sf_mex_assign(&c11_rhs8, sf_mex_createcellmatrix(0, 1), false);
  sf_mex_assign(&c11_lhs8, sf_mex_createcellmatrix(0, 1), false);
  sf_mex_addfield(*c11_info, sf_mex_duplicatearraysafe(&c11_rhs8), "rhs", "rhs",
                  8);
  sf_mex_addfield(*c11_info, sf_mex_duplicatearraysafe(&c11_lhs8), "lhs", "lhs",
                  8);
  sf_mex_addfield(*c11_info, c11_emlrt_marshallOut(""), "context", "context", 9);
  sf_mex_addfield(*c11_info, c11_emlrt_marshallOut("cosd"), "name", "name", 9);
  sf_mex_addfield(*c11_info, c11_emlrt_marshallOut("single"), "dominantType",
                  "dominantType", 9);
  sf_mex_addfield(*c11_info, c11_emlrt_marshallOut(
    "[ILXE]$matlabroot$/toolbox/eml/lib/matlab/elfun/cosd.m"), "resolved",
                  "resolved", 9);
  sf_mex_addfield(*c11_info, c11_b_emlrt_marshallOut(1395332098U), "fileTimeLo",
                  "fileTimeLo", 9);
  sf_mex_addfield(*c11_info, c11_b_emlrt_marshallOut(0U), "fileTimeHi",
                  "fileTimeHi", 9);
  sf_mex_addfield(*c11_info, c11_b_emlrt_marshallOut(0U), "mFileTimeLo",
                  "mFileTimeLo", 9);
  sf_mex_addfield(*c11_info, c11_b_emlrt_marshallOut(0U), "mFileTimeHi",
                  "mFileTimeHi", 9);
  sf_mex_assign(&c11_rhs9, sf_mex_createcellmatrix(0, 1), false);
  sf_mex_assign(&c11_lhs9, sf_mex_createcellmatrix(0, 1), false);
  sf_mex_addfield(*c11_info, sf_mex_duplicatearraysafe(&c11_rhs9), "rhs", "rhs",
                  9);
  sf_mex_addfield(*c11_info, sf_mex_duplicatearraysafe(&c11_lhs9), "lhs", "lhs",
                  9);
  sf_mex_addfield(*c11_info, c11_emlrt_marshallOut(
    "[ILXE]$matlabroot$/toolbox/eml/lib/matlab/elfun/cosd.m"), "context",
                  "context", 10);
  sf_mex_addfield(*c11_info, c11_emlrt_marshallOut("eml_scalar_cosd_and_sind"),
                  "name", "name", 10);
  sf_mex_addfield(*c11_info, c11_emlrt_marshallOut("single"), "dominantType",
                  "dominantType", 10);
  sf_mex_addfield(*c11_info, c11_emlrt_marshallOut(
    "[ILXE]$matlabroot$/toolbox/eml/lib/matlab/elfun/eml_scalar_cosd_and_sind.m"),
                  "resolved", "resolved", 10);
  sf_mex_addfield(*c11_info, c11_b_emlrt_marshallOut(1343833978U), "fileTimeLo",
                  "fileTimeLo", 10);
  sf_mex_addfield(*c11_info, c11_b_emlrt_marshallOut(0U), "fileTimeHi",
                  "fileTimeHi", 10);
  sf_mex_addfield(*c11_info, c11_b_emlrt_marshallOut(0U), "mFileTimeLo",
                  "mFileTimeLo", 10);
  sf_mex_addfield(*c11_info, c11_b_emlrt_marshallOut(0U), "mFileTimeHi",
                  "mFileTimeHi", 10);
  sf_mex_assign(&c11_rhs10, sf_mex_createcellmatrix(0, 1), false);
  sf_mex_assign(&c11_lhs10, sf_mex_createcellmatrix(0, 1), false);
  sf_mex_addfield(*c11_info, sf_mex_duplicatearraysafe(&c11_rhs10), "rhs", "rhs",
                  10);
  sf_mex_addfield(*c11_info, sf_mex_duplicatearraysafe(&c11_lhs10), "lhs", "lhs",
                  10);
  sf_mex_addfield(*c11_info, c11_emlrt_marshallOut(
    "[ILXE]$matlabroot$/toolbox/eml/lib/matlab/elfun/eml_scalar_cosd_and_sind.m"),
                  "context", "context", 11);
  sf_mex_addfield(*c11_info, c11_emlrt_marshallOut("isfinite"), "name", "name",
                  11);
  sf_mex_addfield(*c11_info, c11_emlrt_marshallOut("single"), "dominantType",
                  "dominantType", 11);
  sf_mex_addfield(*c11_info, c11_emlrt_marshallOut(
    "[ILXE]$matlabroot$/toolbox/eml/lib/matlab/elmat/isfinite.m"), "resolved",
                  "resolved", 11);
  sf_mex_addfield(*c11_info, c11_b_emlrt_marshallOut(1363717456U), "fileTimeLo",
                  "fileTimeLo", 11);
  sf_mex_addfield(*c11_info, c11_b_emlrt_marshallOut(0U), "fileTimeHi",
                  "fileTimeHi", 11);
  sf_mex_addfield(*c11_info, c11_b_emlrt_marshallOut(0U), "mFileTimeLo",
                  "mFileTimeLo", 11);
  sf_mex_addfield(*c11_info, c11_b_emlrt_marshallOut(0U), "mFileTimeHi",
                  "mFileTimeHi", 11);
  sf_mex_assign(&c11_rhs11, sf_mex_createcellmatrix(0, 1), false);
  sf_mex_assign(&c11_lhs11, sf_mex_createcellmatrix(0, 1), false);
  sf_mex_addfield(*c11_info, sf_mex_duplicatearraysafe(&c11_rhs11), "rhs", "rhs",
                  11);
  sf_mex_addfield(*c11_info, sf_mex_duplicatearraysafe(&c11_lhs11), "lhs", "lhs",
                  11);
  sf_mex_addfield(*c11_info, c11_emlrt_marshallOut(
    "[ILXE]$matlabroot$/toolbox/eml/lib/matlab/elmat/isfinite.m"), "context",
                  "context", 12);
  sf_mex_addfield(*c11_info, c11_emlrt_marshallOut(
    "coder.internal.isBuiltInNumeric"), "name", "name", 12);
  sf_mex_addfield(*c11_info, c11_emlrt_marshallOut("single"), "dominantType",
                  "dominantType", 12);
  sf_mex_addfield(*c11_info, c11_emlrt_marshallOut(
    "[ILXE]$matlabroot$/toolbox/shared/coder/coder/+coder/+internal/isBuiltInNumeric.m"),
                  "resolved", "resolved", 12);
  sf_mex_addfield(*c11_info, c11_b_emlrt_marshallOut(1395935456U), "fileTimeLo",
                  "fileTimeLo", 12);
  sf_mex_addfield(*c11_info, c11_b_emlrt_marshallOut(0U), "fileTimeHi",
                  "fileTimeHi", 12);
  sf_mex_addfield(*c11_info, c11_b_emlrt_marshallOut(0U), "mFileTimeLo",
                  "mFileTimeLo", 12);
  sf_mex_addfield(*c11_info, c11_b_emlrt_marshallOut(0U), "mFileTimeHi",
                  "mFileTimeHi", 12);
  sf_mex_assign(&c11_rhs12, sf_mex_createcellmatrix(0, 1), false);
  sf_mex_assign(&c11_lhs12, sf_mex_createcellmatrix(0, 1), false);
  sf_mex_addfield(*c11_info, sf_mex_duplicatearraysafe(&c11_rhs12), "rhs", "rhs",
                  12);
  sf_mex_addfield(*c11_info, sf_mex_duplicatearraysafe(&c11_lhs12), "lhs", "lhs",
                  12);
  sf_mex_addfield(*c11_info, c11_emlrt_marshallOut(
    "[ILXE]$matlabroot$/toolbox/eml/lib/matlab/elmat/isfinite.m"), "context",
                  "context", 13);
  sf_mex_addfield(*c11_info, c11_emlrt_marshallOut("isinf"), "name", "name", 13);
  sf_mex_addfield(*c11_info, c11_emlrt_marshallOut("single"), "dominantType",
                  "dominantType", 13);
  sf_mex_addfield(*c11_info, c11_emlrt_marshallOut(
    "[ILXE]$matlabroot$/toolbox/eml/lib/matlab/elmat/isinf.m"), "resolved",
                  "resolved", 13);
  sf_mex_addfield(*c11_info, c11_b_emlrt_marshallOut(1363717456U), "fileTimeLo",
                  "fileTimeLo", 13);
  sf_mex_addfield(*c11_info, c11_b_emlrt_marshallOut(0U), "fileTimeHi",
                  "fileTimeHi", 13);
  sf_mex_addfield(*c11_info, c11_b_emlrt_marshallOut(0U), "mFileTimeLo",
                  "mFileTimeLo", 13);
  sf_mex_addfield(*c11_info, c11_b_emlrt_marshallOut(0U), "mFileTimeHi",
                  "mFileTimeHi", 13);
  sf_mex_assign(&c11_rhs13, sf_mex_createcellmatrix(0, 1), false);
  sf_mex_assign(&c11_lhs13, sf_mex_createcellmatrix(0, 1), false);
  sf_mex_addfield(*c11_info, sf_mex_duplicatearraysafe(&c11_rhs13), "rhs", "rhs",
                  13);
  sf_mex_addfield(*c11_info, sf_mex_duplicatearraysafe(&c11_lhs13), "lhs", "lhs",
                  13);
  sf_mex_addfield(*c11_info, c11_emlrt_marshallOut(
    "[ILXE]$matlabroot$/toolbox/eml/lib/matlab/elmat/isinf.m"), "context",
                  "context", 14);
  sf_mex_addfield(*c11_info, c11_emlrt_marshallOut(
    "coder.internal.isBuiltInNumeric"), "name", "name", 14);
  sf_mex_addfield(*c11_info, c11_emlrt_marshallOut("single"), "dominantType",
                  "dominantType", 14);
  sf_mex_addfield(*c11_info, c11_emlrt_marshallOut(
    "[ILXE]$matlabroot$/toolbox/shared/coder/coder/+coder/+internal/isBuiltInNumeric.m"),
                  "resolved", "resolved", 14);
  sf_mex_addfield(*c11_info, c11_b_emlrt_marshallOut(1395935456U), "fileTimeLo",
                  "fileTimeLo", 14);
  sf_mex_addfield(*c11_info, c11_b_emlrt_marshallOut(0U), "fileTimeHi",
                  "fileTimeHi", 14);
  sf_mex_addfield(*c11_info, c11_b_emlrt_marshallOut(0U), "mFileTimeLo",
                  "mFileTimeLo", 14);
  sf_mex_addfield(*c11_info, c11_b_emlrt_marshallOut(0U), "mFileTimeHi",
                  "mFileTimeHi", 14);
  sf_mex_assign(&c11_rhs14, sf_mex_createcellmatrix(0, 1), false);
  sf_mex_assign(&c11_lhs14, sf_mex_createcellmatrix(0, 1), false);
  sf_mex_addfield(*c11_info, sf_mex_duplicatearraysafe(&c11_rhs14), "rhs", "rhs",
                  14);
  sf_mex_addfield(*c11_info, sf_mex_duplicatearraysafe(&c11_lhs14), "lhs", "lhs",
                  14);
  sf_mex_addfield(*c11_info, c11_emlrt_marshallOut(
    "[ILXE]$matlabroot$/toolbox/eml/lib/matlab/elmat/isfinite.m"), "context",
                  "context", 15);
  sf_mex_addfield(*c11_info, c11_emlrt_marshallOut("isnan"), "name", "name", 15);
  sf_mex_addfield(*c11_info, c11_emlrt_marshallOut("single"), "dominantType",
                  "dominantType", 15);
  sf_mex_addfield(*c11_info, c11_emlrt_marshallOut(
    "[ILXE]$matlabroot$/toolbox/eml/lib/matlab/elmat/isnan.m"), "resolved",
                  "resolved", 15);
  sf_mex_addfield(*c11_info, c11_b_emlrt_marshallOut(1363717458U), "fileTimeLo",
                  "fileTimeLo", 15);
  sf_mex_addfield(*c11_info, c11_b_emlrt_marshallOut(0U), "fileTimeHi",
                  "fileTimeHi", 15);
  sf_mex_addfield(*c11_info, c11_b_emlrt_marshallOut(0U), "mFileTimeLo",
                  "mFileTimeLo", 15);
  sf_mex_addfield(*c11_info, c11_b_emlrt_marshallOut(0U), "mFileTimeHi",
                  "mFileTimeHi", 15);
  sf_mex_assign(&c11_rhs15, sf_mex_createcellmatrix(0, 1), false);
  sf_mex_assign(&c11_lhs15, sf_mex_createcellmatrix(0, 1), false);
  sf_mex_addfield(*c11_info, sf_mex_duplicatearraysafe(&c11_rhs15), "rhs", "rhs",
                  15);
  sf_mex_addfield(*c11_info, sf_mex_duplicatearraysafe(&c11_lhs15), "lhs", "lhs",
                  15);
  sf_mex_addfield(*c11_info, c11_emlrt_marshallOut(
    "[ILXE]$matlabroot$/toolbox/eml/lib/matlab/elmat/isnan.m"), "context",
                  "context", 16);
  sf_mex_addfield(*c11_info, c11_emlrt_marshallOut(
    "coder.internal.isBuiltInNumeric"), "name", "name", 16);
  sf_mex_addfield(*c11_info, c11_emlrt_marshallOut("single"), "dominantType",
                  "dominantType", 16);
  sf_mex_addfield(*c11_info, c11_emlrt_marshallOut(
    "[ILXE]$matlabroot$/toolbox/shared/coder/coder/+coder/+internal/isBuiltInNumeric.m"),
                  "resolved", "resolved", 16);
  sf_mex_addfield(*c11_info, c11_b_emlrt_marshallOut(1395935456U), "fileTimeLo",
                  "fileTimeLo", 16);
  sf_mex_addfield(*c11_info, c11_b_emlrt_marshallOut(0U), "fileTimeHi",
                  "fileTimeHi", 16);
  sf_mex_addfield(*c11_info, c11_b_emlrt_marshallOut(0U), "mFileTimeLo",
                  "mFileTimeLo", 16);
  sf_mex_addfield(*c11_info, c11_b_emlrt_marshallOut(0U), "mFileTimeHi",
                  "mFileTimeHi", 16);
  sf_mex_assign(&c11_rhs16, sf_mex_createcellmatrix(0, 1), false);
  sf_mex_assign(&c11_lhs16, sf_mex_createcellmatrix(0, 1), false);
  sf_mex_addfield(*c11_info, sf_mex_duplicatearraysafe(&c11_rhs16), "rhs", "rhs",
                  16);
  sf_mex_addfield(*c11_info, sf_mex_duplicatearraysafe(&c11_lhs16), "lhs", "lhs",
                  16);
  sf_mex_addfield(*c11_info, c11_emlrt_marshallOut(
    "[ILXE]$matlabroot$/toolbox/eml/lib/matlab/elfun/eml_scalar_cosd_and_sind.m"),
                  "context", "context", 17);
  sf_mex_addfield(*c11_info, c11_emlrt_marshallOut("eml_guarded_nan"), "name",
                  "name", 17);
  sf_mex_addfield(*c11_info, c11_emlrt_marshallOut("char"), "dominantType",
                  "dominantType", 17);
  sf_mex_addfield(*c11_info, c11_emlrt_marshallOut(
    "[ILXE]$matlabroot$/toolbox/eml/lib/matlab/eml/eml_guarded_nan.m"),
                  "resolved", "resolved", 17);
  sf_mex_addfield(*c11_info, c11_b_emlrt_marshallOut(1286822376U), "fileTimeLo",
                  "fileTimeLo", 17);
  sf_mex_addfield(*c11_info, c11_b_emlrt_marshallOut(0U), "fileTimeHi",
                  "fileTimeHi", 17);
  sf_mex_addfield(*c11_info, c11_b_emlrt_marshallOut(0U), "mFileTimeLo",
                  "mFileTimeLo", 17);
  sf_mex_addfield(*c11_info, c11_b_emlrt_marshallOut(0U), "mFileTimeHi",
                  "mFileTimeHi", 17);
  sf_mex_assign(&c11_rhs17, sf_mex_createcellmatrix(0, 1), false);
  sf_mex_assign(&c11_lhs17, sf_mex_createcellmatrix(0, 1), false);
  sf_mex_addfield(*c11_info, sf_mex_duplicatearraysafe(&c11_rhs17), "rhs", "rhs",
                  17);
  sf_mex_addfield(*c11_info, sf_mex_duplicatearraysafe(&c11_lhs17), "lhs", "lhs",
                  17);
  sf_mex_addfield(*c11_info, c11_emlrt_marshallOut(
    "[ILXE]$matlabroot$/toolbox/eml/lib/matlab/eml/eml_guarded_nan.m"),
                  "context", "context", 18);
  sf_mex_addfield(*c11_info, c11_emlrt_marshallOut("eml_is_float_class"), "name",
                  "name", 18);
  sf_mex_addfield(*c11_info, c11_emlrt_marshallOut("char"), "dominantType",
                  "dominantType", 18);
  sf_mex_addfield(*c11_info, c11_emlrt_marshallOut(
    "[ILXE]$matlabroot$/toolbox/eml/lib/matlab/eml/eml_is_float_class.m"),
                  "resolved", "resolved", 18);
  sf_mex_addfield(*c11_info, c11_b_emlrt_marshallOut(1286822382U), "fileTimeLo",
                  "fileTimeLo", 18);
  sf_mex_addfield(*c11_info, c11_b_emlrt_marshallOut(0U), "fileTimeHi",
                  "fileTimeHi", 18);
  sf_mex_addfield(*c11_info, c11_b_emlrt_marshallOut(0U), "mFileTimeLo",
                  "mFileTimeLo", 18);
  sf_mex_addfield(*c11_info, c11_b_emlrt_marshallOut(0U), "mFileTimeHi",
                  "mFileTimeHi", 18);
  sf_mex_assign(&c11_rhs18, sf_mex_createcellmatrix(0, 1), false);
  sf_mex_assign(&c11_lhs18, sf_mex_createcellmatrix(0, 1), false);
  sf_mex_addfield(*c11_info, sf_mex_duplicatearraysafe(&c11_rhs18), "rhs", "rhs",
                  18);
  sf_mex_addfield(*c11_info, sf_mex_duplicatearraysafe(&c11_lhs18), "lhs", "lhs",
                  18);
  sf_mex_addfield(*c11_info, c11_emlrt_marshallOut(
    "[ILXE]$matlabroot$/toolbox/eml/lib/matlab/elfun/eml_scalar_cosd_and_sind.m"),
                  "context", "context", 19);
  sf_mex_addfield(*c11_info, c11_emlrt_marshallOut("eml_scalar_rem90"), "name",
                  "name", 19);
  sf_mex_addfield(*c11_info, c11_emlrt_marshallOut("single"), "dominantType",
                  "dominantType", 19);
  sf_mex_addfield(*c11_info, c11_emlrt_marshallOut(
    "[ILXE]$matlabroot$/toolbox/eml/lib/matlab/elfun/eml_scalar_rem90.m"),
                  "resolved", "resolved", 19);
  sf_mex_addfield(*c11_info, c11_b_emlrt_marshallOut(1343833978U), "fileTimeLo",
                  "fileTimeLo", 19);
  sf_mex_addfield(*c11_info, c11_b_emlrt_marshallOut(0U), "fileTimeHi",
                  "fileTimeHi", 19);
  sf_mex_addfield(*c11_info, c11_b_emlrt_marshallOut(0U), "mFileTimeLo",
                  "mFileTimeLo", 19);
  sf_mex_addfield(*c11_info, c11_b_emlrt_marshallOut(0U), "mFileTimeHi",
                  "mFileTimeHi", 19);
  sf_mex_assign(&c11_rhs19, sf_mex_createcellmatrix(0, 1), false);
  sf_mex_assign(&c11_lhs19, sf_mex_createcellmatrix(0, 1), false);
  sf_mex_addfield(*c11_info, sf_mex_duplicatearraysafe(&c11_rhs19), "rhs", "rhs",
                  19);
  sf_mex_addfield(*c11_info, sf_mex_duplicatearraysafe(&c11_lhs19), "lhs", "lhs",
                  19);
  sf_mex_addfield(*c11_info, c11_emlrt_marshallOut(
    "[ILXE]$matlabroot$/toolbox/eml/lib/matlab/elfun/eml_scalar_rem90.m"),
                  "context", "context", 20);
  sf_mex_addfield(*c11_info, c11_emlrt_marshallOut("mrdivide"), "name", "name",
                  20);
  sf_mex_addfield(*c11_info, c11_emlrt_marshallOut("double"), "dominantType",
                  "dominantType", 20);
  sf_mex_addfield(*c11_info, c11_emlrt_marshallOut(
    "[ILXE]$matlabroot$/toolbox/eml/lib/matlab/ops/mrdivide.p"), "resolved",
                  "resolved", 20);
  sf_mex_addfield(*c11_info, c11_b_emlrt_marshallOut(1410811248U), "fileTimeLo",
                  "fileTimeLo", 20);
  sf_mex_addfield(*c11_info, c11_b_emlrt_marshallOut(0U), "fileTimeHi",
                  "fileTimeHi", 20);
  sf_mex_addfield(*c11_info, c11_b_emlrt_marshallOut(1370013486U), "mFileTimeLo",
                  "mFileTimeLo", 20);
  sf_mex_addfield(*c11_info, c11_b_emlrt_marshallOut(0U), "mFileTimeHi",
                  "mFileTimeHi", 20);
  sf_mex_assign(&c11_rhs20, sf_mex_createcellmatrix(0, 1), false);
  sf_mex_assign(&c11_lhs20, sf_mex_createcellmatrix(0, 1), false);
  sf_mex_addfield(*c11_info, sf_mex_duplicatearraysafe(&c11_rhs20), "rhs", "rhs",
                  20);
  sf_mex_addfield(*c11_info, sf_mex_duplicatearraysafe(&c11_lhs20), "lhs", "lhs",
                  20);
  sf_mex_addfield(*c11_info, c11_emlrt_marshallOut(
    "[ILXE]$matlabroot$/toolbox/eml/lib/matlab/ops/mrdivide.p"), "context",
                  "context", 21);
  sf_mex_addfield(*c11_info, c11_emlrt_marshallOut("coder.internal.assert"),
                  "name", "name", 21);
  sf_mex_addfield(*c11_info, c11_emlrt_marshallOut("char"), "dominantType",
                  "dominantType", 21);
  sf_mex_addfield(*c11_info, c11_emlrt_marshallOut(
    "[ILXE]$matlabroot$/toolbox/shared/coder/coder/+coder/+internal/assert.m"),
                  "resolved", "resolved", 21);
  sf_mex_addfield(*c11_info, c11_b_emlrt_marshallOut(1389721374U), "fileTimeLo",
                  "fileTimeLo", 21);
  sf_mex_addfield(*c11_info, c11_b_emlrt_marshallOut(0U), "fileTimeHi",
                  "fileTimeHi", 21);
  sf_mex_addfield(*c11_info, c11_b_emlrt_marshallOut(0U), "mFileTimeLo",
                  "mFileTimeLo", 21);
  sf_mex_addfield(*c11_info, c11_b_emlrt_marshallOut(0U), "mFileTimeHi",
                  "mFileTimeHi", 21);
  sf_mex_assign(&c11_rhs21, sf_mex_createcellmatrix(0, 1), false);
  sf_mex_assign(&c11_lhs21, sf_mex_createcellmatrix(0, 1), false);
  sf_mex_addfield(*c11_info, sf_mex_duplicatearraysafe(&c11_rhs21), "rhs", "rhs",
                  21);
  sf_mex_addfield(*c11_info, sf_mex_duplicatearraysafe(&c11_lhs21), "lhs", "lhs",
                  21);
  sf_mex_addfield(*c11_info, c11_emlrt_marshallOut(
    "[ILXE]$matlabroot$/toolbox/eml/lib/matlab/ops/mrdivide.p"), "context",
                  "context", 22);
  sf_mex_addfield(*c11_info, c11_emlrt_marshallOut("rdivide"), "name", "name",
                  22);
  sf_mex_addfield(*c11_info, c11_emlrt_marshallOut("double"), "dominantType",
                  "dominantType", 22);
  sf_mex_addfield(*c11_info, c11_emlrt_marshallOut(
    "[ILXE]$matlabroot$/toolbox/eml/lib/matlab/ops/rdivide.m"), "resolved",
                  "resolved", 22);
  sf_mex_addfield(*c11_info, c11_b_emlrt_marshallOut(1363717480U), "fileTimeLo",
                  "fileTimeLo", 22);
  sf_mex_addfield(*c11_info, c11_b_emlrt_marshallOut(0U), "fileTimeHi",
                  "fileTimeHi", 22);
  sf_mex_addfield(*c11_info, c11_b_emlrt_marshallOut(0U), "mFileTimeLo",
                  "mFileTimeLo", 22);
  sf_mex_addfield(*c11_info, c11_b_emlrt_marshallOut(0U), "mFileTimeHi",
                  "mFileTimeHi", 22);
  sf_mex_assign(&c11_rhs22, sf_mex_createcellmatrix(0, 1), false);
  sf_mex_assign(&c11_lhs22, sf_mex_createcellmatrix(0, 1), false);
  sf_mex_addfield(*c11_info, sf_mex_duplicatearraysafe(&c11_rhs22), "rhs", "rhs",
                  22);
  sf_mex_addfield(*c11_info, sf_mex_duplicatearraysafe(&c11_lhs22), "lhs", "lhs",
                  22);
  sf_mex_addfield(*c11_info, c11_emlrt_marshallOut(
    "[ILXE]$matlabroot$/toolbox/eml/lib/matlab/ops/rdivide.m"), "context",
                  "context", 23);
  sf_mex_addfield(*c11_info, c11_emlrt_marshallOut(
    "coder.internal.isBuiltInNumeric"), "name", "name", 23);
  sf_mex_addfield(*c11_info, c11_emlrt_marshallOut("double"), "dominantType",
                  "dominantType", 23);
  sf_mex_addfield(*c11_info, c11_emlrt_marshallOut(
    "[ILXE]$matlabroot$/toolbox/shared/coder/coder/+coder/+internal/isBuiltInNumeric.m"),
                  "resolved", "resolved", 23);
  sf_mex_addfield(*c11_info, c11_b_emlrt_marshallOut(1395935456U), "fileTimeLo",
                  "fileTimeLo", 23);
  sf_mex_addfield(*c11_info, c11_b_emlrt_marshallOut(0U), "fileTimeHi",
                  "fileTimeHi", 23);
  sf_mex_addfield(*c11_info, c11_b_emlrt_marshallOut(0U), "mFileTimeLo",
                  "mFileTimeLo", 23);
  sf_mex_addfield(*c11_info, c11_b_emlrt_marshallOut(0U), "mFileTimeHi",
                  "mFileTimeHi", 23);
  sf_mex_assign(&c11_rhs23, sf_mex_createcellmatrix(0, 1), false);
  sf_mex_assign(&c11_lhs23, sf_mex_createcellmatrix(0, 1), false);
  sf_mex_addfield(*c11_info, sf_mex_duplicatearraysafe(&c11_rhs23), "rhs", "rhs",
                  23);
  sf_mex_addfield(*c11_info, sf_mex_duplicatearraysafe(&c11_lhs23), "lhs", "lhs",
                  23);
  sf_mex_addfield(*c11_info, c11_emlrt_marshallOut(
    "[ILXE]$matlabroot$/toolbox/eml/lib/matlab/ops/rdivide.m"), "context",
                  "context", 24);
  sf_mex_addfield(*c11_info, c11_emlrt_marshallOut("eml_scalexp_compatible"),
                  "name", "name", 24);
  sf_mex_addfield(*c11_info, c11_emlrt_marshallOut("double"), "dominantType",
                  "dominantType", 24);
  sf_mex_addfield(*c11_info, c11_emlrt_marshallOut(
    "[ILXE]$matlabroot$/toolbox/eml/lib/matlab/eml/eml_scalexp_compatible.m"),
                  "resolved", "resolved", 24);
  sf_mex_addfield(*c11_info, c11_b_emlrt_marshallOut(1286822396U), "fileTimeLo",
                  "fileTimeLo", 24);
  sf_mex_addfield(*c11_info, c11_b_emlrt_marshallOut(0U), "fileTimeHi",
                  "fileTimeHi", 24);
  sf_mex_addfield(*c11_info, c11_b_emlrt_marshallOut(0U), "mFileTimeLo",
                  "mFileTimeLo", 24);
  sf_mex_addfield(*c11_info, c11_b_emlrt_marshallOut(0U), "mFileTimeHi",
                  "mFileTimeHi", 24);
  sf_mex_assign(&c11_rhs24, sf_mex_createcellmatrix(0, 1), false);
  sf_mex_assign(&c11_lhs24, sf_mex_createcellmatrix(0, 1), false);
  sf_mex_addfield(*c11_info, sf_mex_duplicatearraysafe(&c11_rhs24), "rhs", "rhs",
                  24);
  sf_mex_addfield(*c11_info, sf_mex_duplicatearraysafe(&c11_lhs24), "lhs", "lhs",
                  24);
  sf_mex_addfield(*c11_info, c11_emlrt_marshallOut(
    "[ILXE]$matlabroot$/toolbox/eml/lib/matlab/ops/rdivide.m"), "context",
                  "context", 25);
  sf_mex_addfield(*c11_info, c11_emlrt_marshallOut("eml_div"), "name", "name",
                  25);
  sf_mex_addfield(*c11_info, c11_emlrt_marshallOut("double"), "dominantType",
                  "dominantType", 25);
  sf_mex_addfield(*c11_info, c11_emlrt_marshallOut(
    "[ILXE]$matlabroot$/toolbox/eml/lib/matlab/eml/eml_div.m"), "resolved",
                  "resolved", 25);
  sf_mex_addfield(*c11_info, c11_b_emlrt_marshallOut(1386427552U), "fileTimeLo",
                  "fileTimeLo", 25);
  sf_mex_addfield(*c11_info, c11_b_emlrt_marshallOut(0U), "fileTimeHi",
                  "fileTimeHi", 25);
  sf_mex_addfield(*c11_info, c11_b_emlrt_marshallOut(0U), "mFileTimeLo",
                  "mFileTimeLo", 25);
  sf_mex_addfield(*c11_info, c11_b_emlrt_marshallOut(0U), "mFileTimeHi",
                  "mFileTimeHi", 25);
  sf_mex_assign(&c11_rhs25, sf_mex_createcellmatrix(0, 1), false);
  sf_mex_assign(&c11_lhs25, sf_mex_createcellmatrix(0, 1), false);
  sf_mex_addfield(*c11_info, sf_mex_duplicatearraysafe(&c11_rhs25), "rhs", "rhs",
                  25);
  sf_mex_addfield(*c11_info, sf_mex_duplicatearraysafe(&c11_lhs25), "lhs", "lhs",
                  25);
  sf_mex_addfield(*c11_info, c11_emlrt_marshallOut(
    "[ILXE]$matlabroot$/toolbox/eml/lib/matlab/eml/eml_div.m"), "context",
                  "context", 26);
  sf_mex_addfield(*c11_info, c11_emlrt_marshallOut("coder.internal.div"), "name",
                  "name", 26);
  sf_mex_addfield(*c11_info, c11_emlrt_marshallOut("double"), "dominantType",
                  "dominantType", 26);
  sf_mex_addfield(*c11_info, c11_emlrt_marshallOut(
    "[ILXE]$matlabroot$/toolbox/coder/coder/+coder/+internal/div.p"), "resolved",
                  "resolved", 26);
  sf_mex_addfield(*c11_info, c11_b_emlrt_marshallOut(1410811370U), "fileTimeLo",
                  "fileTimeLo", 26);
  sf_mex_addfield(*c11_info, c11_b_emlrt_marshallOut(0U), "fileTimeHi",
                  "fileTimeHi", 26);
  sf_mex_addfield(*c11_info, c11_b_emlrt_marshallOut(0U), "mFileTimeLo",
                  "mFileTimeLo", 26);
  sf_mex_addfield(*c11_info, c11_b_emlrt_marshallOut(0U), "mFileTimeHi",
                  "mFileTimeHi", 26);
  sf_mex_assign(&c11_rhs26, sf_mex_createcellmatrix(0, 1), false);
  sf_mex_assign(&c11_lhs26, sf_mex_createcellmatrix(0, 1), false);
  sf_mex_addfield(*c11_info, sf_mex_duplicatearraysafe(&c11_rhs26), "rhs", "rhs",
                  26);
  sf_mex_addfield(*c11_info, sf_mex_duplicatearraysafe(&c11_lhs26), "lhs", "lhs",
                  26);
  sf_mex_addfield(*c11_info, c11_emlrt_marshallOut(
    "[ILXE]$matlabroot$/toolbox/eml/lib/matlab/elfun/eml_scalar_rem90.m"),
                  "context", "context", 27);
  sf_mex_addfield(*c11_info, c11_emlrt_marshallOut("rem"), "name", "name", 27);
  sf_mex_addfield(*c11_info, c11_emlrt_marshallOut("single"), "dominantType",
                  "dominantType", 27);
  sf_mex_addfield(*c11_info, c11_emlrt_marshallOut(
    "[ILXE]$matlabroot$/toolbox/eml/lib/matlab/elfun/rem.m"), "resolved",
                  "resolved", 27);
  sf_mex_addfield(*c11_info, c11_b_emlrt_marshallOut(1363717454U), "fileTimeLo",
                  "fileTimeLo", 27);
  sf_mex_addfield(*c11_info, c11_b_emlrt_marshallOut(0U), "fileTimeHi",
                  "fileTimeHi", 27);
  sf_mex_addfield(*c11_info, c11_b_emlrt_marshallOut(0U), "mFileTimeLo",
                  "mFileTimeLo", 27);
  sf_mex_addfield(*c11_info, c11_b_emlrt_marshallOut(0U), "mFileTimeHi",
                  "mFileTimeHi", 27);
  sf_mex_assign(&c11_rhs27, sf_mex_createcellmatrix(0, 1), false);
  sf_mex_assign(&c11_lhs27, sf_mex_createcellmatrix(0, 1), false);
  sf_mex_addfield(*c11_info, sf_mex_duplicatearraysafe(&c11_rhs27), "rhs", "rhs",
                  27);
  sf_mex_addfield(*c11_info, sf_mex_duplicatearraysafe(&c11_lhs27), "lhs", "lhs",
                  27);
  sf_mex_addfield(*c11_info, c11_emlrt_marshallOut(
    "[ILXE]$matlabroot$/toolbox/eml/lib/matlab/elfun/rem.m"), "context",
                  "context", 28);
  sf_mex_addfield(*c11_info, c11_emlrt_marshallOut(
    "coder.internal.isBuiltInNumeric"), "name", "name", 28);
  sf_mex_addfield(*c11_info, c11_emlrt_marshallOut("single"), "dominantType",
                  "dominantType", 28);
  sf_mex_addfield(*c11_info, c11_emlrt_marshallOut(
    "[ILXE]$matlabroot$/toolbox/shared/coder/coder/+coder/+internal/isBuiltInNumeric.m"),
                  "resolved", "resolved", 28);
  sf_mex_addfield(*c11_info, c11_b_emlrt_marshallOut(1395935456U), "fileTimeLo",
                  "fileTimeLo", 28);
  sf_mex_addfield(*c11_info, c11_b_emlrt_marshallOut(0U), "fileTimeHi",
                  "fileTimeHi", 28);
  sf_mex_addfield(*c11_info, c11_b_emlrt_marshallOut(0U), "mFileTimeLo",
                  "mFileTimeLo", 28);
  sf_mex_addfield(*c11_info, c11_b_emlrt_marshallOut(0U), "mFileTimeHi",
                  "mFileTimeHi", 28);
  sf_mex_assign(&c11_rhs28, sf_mex_createcellmatrix(0, 1), false);
  sf_mex_assign(&c11_lhs28, sf_mex_createcellmatrix(0, 1), false);
  sf_mex_addfield(*c11_info, sf_mex_duplicatearraysafe(&c11_rhs28), "rhs", "rhs",
                  28);
  sf_mex_addfield(*c11_info, sf_mex_duplicatearraysafe(&c11_lhs28), "lhs", "lhs",
                  28);
  sf_mex_addfield(*c11_info, c11_emlrt_marshallOut(
    "[ILXE]$matlabroot$/toolbox/eml/lib/matlab/elfun/rem.m"), "context",
                  "context", 29);
  sf_mex_addfield(*c11_info, c11_emlrt_marshallOut("eml_scalar_eg"), "name",
                  "name", 29);
  sf_mex_addfield(*c11_info, c11_emlrt_marshallOut("single"), "dominantType",
                  "dominantType", 29);
  sf_mex_addfield(*c11_info, c11_emlrt_marshallOut(
    "[ILXE]$matlabroot$/toolbox/eml/lib/matlab/eml/eml_scalar_eg.m"), "resolved",
                  "resolved", 29);
  sf_mex_addfield(*c11_info, c11_b_emlrt_marshallOut(1375984288U), "fileTimeLo",
                  "fileTimeLo", 29);
  sf_mex_addfield(*c11_info, c11_b_emlrt_marshallOut(0U), "fileTimeHi",
                  "fileTimeHi", 29);
  sf_mex_addfield(*c11_info, c11_b_emlrt_marshallOut(0U), "mFileTimeLo",
                  "mFileTimeLo", 29);
  sf_mex_addfield(*c11_info, c11_b_emlrt_marshallOut(0U), "mFileTimeHi",
                  "mFileTimeHi", 29);
  sf_mex_assign(&c11_rhs29, sf_mex_createcellmatrix(0, 1), false);
  sf_mex_assign(&c11_lhs29, sf_mex_createcellmatrix(0, 1), false);
  sf_mex_addfield(*c11_info, sf_mex_duplicatearraysafe(&c11_rhs29), "rhs", "rhs",
                  29);
  sf_mex_addfield(*c11_info, sf_mex_duplicatearraysafe(&c11_lhs29), "lhs", "lhs",
                  29);
  sf_mex_addfield(*c11_info, c11_emlrt_marshallOut(
    "[ILXE]$matlabroot$/toolbox/eml/lib/matlab/elfun/rem.m"), "context",
                  "context", 30);
  sf_mex_addfield(*c11_info, c11_emlrt_marshallOut("eml_scalexp_alloc"), "name",
                  "name", 30);
  sf_mex_addfield(*c11_info, c11_emlrt_marshallOut("single"), "dominantType",
                  "dominantType", 30);
  sf_mex_addfield(*c11_info, c11_emlrt_marshallOut(
    "[ILXE]$matlabroot$/toolbox/eml/lib/matlab/eml/eml_scalexp_alloc.m"),
                  "resolved", "resolved", 30);
  sf_mex_addfield(*c11_info, c11_b_emlrt_marshallOut(1375984288U), "fileTimeLo",
                  "fileTimeLo", 30);
  sf_mex_addfield(*c11_info, c11_b_emlrt_marshallOut(0U), "fileTimeHi",
                  "fileTimeHi", 30);
  sf_mex_addfield(*c11_info, c11_b_emlrt_marshallOut(0U), "mFileTimeLo",
                  "mFileTimeLo", 30);
  sf_mex_addfield(*c11_info, c11_b_emlrt_marshallOut(0U), "mFileTimeHi",
                  "mFileTimeHi", 30);
  sf_mex_assign(&c11_rhs30, sf_mex_createcellmatrix(0, 1), false);
  sf_mex_assign(&c11_lhs30, sf_mex_createcellmatrix(0, 1), false);
  sf_mex_addfield(*c11_info, sf_mex_duplicatearraysafe(&c11_rhs30), "rhs", "rhs",
                  30);
  sf_mex_addfield(*c11_info, sf_mex_duplicatearraysafe(&c11_lhs30), "lhs", "lhs",
                  30);
  sf_mex_addfield(*c11_info, c11_emlrt_marshallOut(
    "[ILXE]$matlabroot$/toolbox/eml/lib/matlab/elfun/eml_scalar_rem90.m"),
                  "context", "context", 31);
  sf_mex_addfield(*c11_info, c11_emlrt_marshallOut("abs"), "name", "name", 31);
  sf_mex_addfield(*c11_info, c11_emlrt_marshallOut("single"), "dominantType",
                  "dominantType", 31);
  sf_mex_addfield(*c11_info, c11_emlrt_marshallOut(
    "[ILXE]$matlabroot$/toolbox/eml/lib/matlab/elfun/abs.m"), "resolved",
                  "resolved", 31);
  sf_mex_addfield(*c11_info, c11_b_emlrt_marshallOut(1363717452U), "fileTimeLo",
                  "fileTimeLo", 31);
  sf_mex_addfield(*c11_info, c11_b_emlrt_marshallOut(0U), "fileTimeHi",
                  "fileTimeHi", 31);
  sf_mex_addfield(*c11_info, c11_b_emlrt_marshallOut(0U), "mFileTimeLo",
                  "mFileTimeLo", 31);
  sf_mex_addfield(*c11_info, c11_b_emlrt_marshallOut(0U), "mFileTimeHi",
                  "mFileTimeHi", 31);
  sf_mex_assign(&c11_rhs31, sf_mex_createcellmatrix(0, 1), false);
  sf_mex_assign(&c11_lhs31, sf_mex_createcellmatrix(0, 1), false);
  sf_mex_addfield(*c11_info, sf_mex_duplicatearraysafe(&c11_rhs31), "rhs", "rhs",
                  31);
  sf_mex_addfield(*c11_info, sf_mex_duplicatearraysafe(&c11_lhs31), "lhs", "lhs",
                  31);
  sf_mex_addfield(*c11_info, c11_emlrt_marshallOut(
    "[ILXE]$matlabroot$/toolbox/eml/lib/matlab/elfun/abs.m"), "context",
                  "context", 32);
  sf_mex_addfield(*c11_info, c11_emlrt_marshallOut(
    "coder.internal.isBuiltInNumeric"), "name", "name", 32);
  sf_mex_addfield(*c11_info, c11_emlrt_marshallOut("single"), "dominantType",
                  "dominantType", 32);
  sf_mex_addfield(*c11_info, c11_emlrt_marshallOut(
    "[ILXE]$matlabroot$/toolbox/shared/coder/coder/+coder/+internal/isBuiltInNumeric.m"),
                  "resolved", "resolved", 32);
  sf_mex_addfield(*c11_info, c11_b_emlrt_marshallOut(1395935456U), "fileTimeLo",
                  "fileTimeLo", 32);
  sf_mex_addfield(*c11_info, c11_b_emlrt_marshallOut(0U), "fileTimeHi",
                  "fileTimeHi", 32);
  sf_mex_addfield(*c11_info, c11_b_emlrt_marshallOut(0U), "mFileTimeLo",
                  "mFileTimeLo", 32);
  sf_mex_addfield(*c11_info, c11_b_emlrt_marshallOut(0U), "mFileTimeHi",
                  "mFileTimeHi", 32);
  sf_mex_assign(&c11_rhs32, sf_mex_createcellmatrix(0, 1), false);
  sf_mex_assign(&c11_lhs32, sf_mex_createcellmatrix(0, 1), false);
  sf_mex_addfield(*c11_info, sf_mex_duplicatearraysafe(&c11_rhs32), "rhs", "rhs",
                  32);
  sf_mex_addfield(*c11_info, sf_mex_duplicatearraysafe(&c11_lhs32), "lhs", "lhs",
                  32);
  sf_mex_addfield(*c11_info, c11_emlrt_marshallOut(
    "[ILXE]$matlabroot$/toolbox/eml/lib/matlab/elfun/abs.m"), "context",
                  "context", 33);
  sf_mex_addfield(*c11_info, c11_emlrt_marshallOut("eml_scalar_abs"), "name",
                  "name", 33);
  sf_mex_addfield(*c11_info, c11_emlrt_marshallOut("single"), "dominantType",
                  "dominantType", 33);
  sf_mex_addfield(*c11_info, c11_emlrt_marshallOut(
    "[ILXE]$matlabroot$/toolbox/eml/lib/matlab/elfun/eml_scalar_abs.m"),
                  "resolved", "resolved", 33);
  sf_mex_addfield(*c11_info, c11_b_emlrt_marshallOut(1286822312U), "fileTimeLo",
                  "fileTimeLo", 33);
  sf_mex_addfield(*c11_info, c11_b_emlrt_marshallOut(0U), "fileTimeHi",
                  "fileTimeHi", 33);
  sf_mex_addfield(*c11_info, c11_b_emlrt_marshallOut(0U), "mFileTimeLo",
                  "mFileTimeLo", 33);
  sf_mex_addfield(*c11_info, c11_b_emlrt_marshallOut(0U), "mFileTimeHi",
                  "mFileTimeHi", 33);
  sf_mex_assign(&c11_rhs33, sf_mex_createcellmatrix(0, 1), false);
  sf_mex_assign(&c11_lhs33, sf_mex_createcellmatrix(0, 1), false);
  sf_mex_addfield(*c11_info, sf_mex_duplicatearraysafe(&c11_rhs33), "rhs", "rhs",
                  33);
  sf_mex_addfield(*c11_info, sf_mex_duplicatearraysafe(&c11_lhs33), "lhs", "lhs",
                  33);
  sf_mex_addfield(*c11_info, c11_emlrt_marshallOut(
    "[ILXE]$matlabroot$/toolbox/eml/lib/matlab/elfun/eml_scalar_rem90.m"),
                  "context", "context", 34);
  sf_mex_addfield(*c11_info, c11_emlrt_marshallOut("eml_mtimes_helper"), "name",
                  "name", 34);
  sf_mex_addfield(*c11_info, c11_emlrt_marshallOut(""), "dominantType",
                  "dominantType", 34);
  sf_mex_addfield(*c11_info, c11_emlrt_marshallOut(
    "[ILXE]$matlabroot$/toolbox/eml/lib/matlab/ops/eml_mtimes_helper.m"),
                  "resolved", "resolved", 34);
  sf_mex_addfield(*c11_info, c11_b_emlrt_marshallOut(1383880894U), "fileTimeLo",
                  "fileTimeLo", 34);
  sf_mex_addfield(*c11_info, c11_b_emlrt_marshallOut(0U), "fileTimeHi",
                  "fileTimeHi", 34);
  sf_mex_addfield(*c11_info, c11_b_emlrt_marshallOut(0U), "mFileTimeLo",
                  "mFileTimeLo", 34);
  sf_mex_addfield(*c11_info, c11_b_emlrt_marshallOut(0U), "mFileTimeHi",
                  "mFileTimeHi", 34);
  sf_mex_assign(&c11_rhs34, sf_mex_createcellmatrix(0, 1), false);
  sf_mex_assign(&c11_lhs34, sf_mex_createcellmatrix(0, 1), false);
  sf_mex_addfield(*c11_info, sf_mex_duplicatearraysafe(&c11_rhs34), "rhs", "rhs",
                  34);
  sf_mex_addfield(*c11_info, sf_mex_duplicatearraysafe(&c11_lhs34), "lhs", "lhs",
                  34);
  sf_mex_addfield(*c11_info, c11_emlrt_marshallOut(""), "context", "context", 35);
  sf_mex_addfield(*c11_info, c11_emlrt_marshallOut("sind"), "name", "name", 35);
  sf_mex_addfield(*c11_info, c11_emlrt_marshallOut("single"), "dominantType",
                  "dominantType", 35);
  sf_mex_addfield(*c11_info, c11_emlrt_marshallOut(
    "[ILXE]$matlabroot$/toolbox/eml/lib/matlab/elfun/sind.m"), "resolved",
                  "resolved", 35);
  sf_mex_addfield(*c11_info, c11_b_emlrt_marshallOut(1395332104U), "fileTimeLo",
                  "fileTimeLo", 35);
  sf_mex_addfield(*c11_info, c11_b_emlrt_marshallOut(0U), "fileTimeHi",
                  "fileTimeHi", 35);
  sf_mex_addfield(*c11_info, c11_b_emlrt_marshallOut(0U), "mFileTimeLo",
                  "mFileTimeLo", 35);
  sf_mex_addfield(*c11_info, c11_b_emlrt_marshallOut(0U), "mFileTimeHi",
                  "mFileTimeHi", 35);
  sf_mex_assign(&c11_rhs35, sf_mex_createcellmatrix(0, 1), false);
  sf_mex_assign(&c11_lhs35, sf_mex_createcellmatrix(0, 1), false);
  sf_mex_addfield(*c11_info, sf_mex_duplicatearraysafe(&c11_rhs35), "rhs", "rhs",
                  35);
  sf_mex_addfield(*c11_info, sf_mex_duplicatearraysafe(&c11_lhs35), "lhs", "lhs",
                  35);
  sf_mex_addfield(*c11_info, c11_emlrt_marshallOut(
    "[ILXE]$matlabroot$/toolbox/eml/lib/matlab/elfun/sind.m"), "context",
                  "context", 36);
  sf_mex_addfield(*c11_info, c11_emlrt_marshallOut("eml_scalar_cosd_and_sind"),
                  "name", "name", 36);
  sf_mex_addfield(*c11_info, c11_emlrt_marshallOut("single"), "dominantType",
                  "dominantType", 36);
  sf_mex_addfield(*c11_info, c11_emlrt_marshallOut(
    "[ILXE]$matlabroot$/toolbox/eml/lib/matlab/elfun/eml_scalar_cosd_and_sind.m"),
                  "resolved", "resolved", 36);
  sf_mex_addfield(*c11_info, c11_b_emlrt_marshallOut(1343833978U), "fileTimeLo",
                  "fileTimeLo", 36);
  sf_mex_addfield(*c11_info, c11_b_emlrt_marshallOut(0U), "fileTimeHi",
                  "fileTimeHi", 36);
  sf_mex_addfield(*c11_info, c11_b_emlrt_marshallOut(0U), "mFileTimeLo",
                  "mFileTimeLo", 36);
  sf_mex_addfield(*c11_info, c11_b_emlrt_marshallOut(0U), "mFileTimeHi",
                  "mFileTimeHi", 36);
  sf_mex_assign(&c11_rhs36, sf_mex_createcellmatrix(0, 1), false);
  sf_mex_assign(&c11_lhs36, sf_mex_createcellmatrix(0, 1), false);
  sf_mex_addfield(*c11_info, sf_mex_duplicatearraysafe(&c11_rhs36), "rhs", "rhs",
                  36);
  sf_mex_addfield(*c11_info, sf_mex_duplicatearraysafe(&c11_lhs36), "lhs", "lhs",
                  36);
  sf_mex_addfield(*c11_info, c11_emlrt_marshallOut(""), "context", "context", 37);
  sf_mex_addfield(*c11_info, c11_emlrt_marshallOut("eml_mtimes_helper"), "name",
                  "name", 37);
  sf_mex_addfield(*c11_info, c11_emlrt_marshallOut(""), "dominantType",
                  "dominantType", 37);
  sf_mex_addfield(*c11_info, c11_emlrt_marshallOut(
    "[ILXE]$matlabroot$/toolbox/eml/lib/matlab/ops/eml_mtimes_helper.m"),
                  "resolved", "resolved", 37);
  sf_mex_addfield(*c11_info, c11_b_emlrt_marshallOut(1383880894U), "fileTimeLo",
                  "fileTimeLo", 37);
  sf_mex_addfield(*c11_info, c11_b_emlrt_marshallOut(0U), "fileTimeHi",
                  "fileTimeHi", 37);
  sf_mex_addfield(*c11_info, c11_b_emlrt_marshallOut(0U), "mFileTimeLo",
                  "mFileTimeLo", 37);
  sf_mex_addfield(*c11_info, c11_b_emlrt_marshallOut(0U), "mFileTimeHi",
                  "mFileTimeHi", 37);
  sf_mex_assign(&c11_rhs37, sf_mex_createcellmatrix(0, 1), false);
  sf_mex_assign(&c11_lhs37, sf_mex_createcellmatrix(0, 1), false);
  sf_mex_addfield(*c11_info, sf_mex_duplicatearraysafe(&c11_rhs37), "rhs", "rhs",
                  37);
  sf_mex_addfield(*c11_info, sf_mex_duplicatearraysafe(&c11_lhs37), "lhs", "lhs",
                  37);
  sf_mex_addfield(*c11_info, c11_emlrt_marshallOut(
    "[ILXE]$matlabroot$/toolbox/eml/lib/matlab/ops/eml_mtimes_helper.m"),
                  "context", "context", 38);
  sf_mex_addfield(*c11_info, c11_emlrt_marshallOut("eml_index_class"), "name",
                  "name", 38);
  sf_mex_addfield(*c11_info, c11_emlrt_marshallOut(""), "dominantType",
                  "dominantType", 38);
  sf_mex_addfield(*c11_info, c11_emlrt_marshallOut(
    "[ILXE]$matlabroot$/toolbox/eml/lib/matlab/eml/eml_index_class.m"),
                  "resolved", "resolved", 38);
  sf_mex_addfield(*c11_info, c11_b_emlrt_marshallOut(1323174178U), "fileTimeLo",
                  "fileTimeLo", 38);
  sf_mex_addfield(*c11_info, c11_b_emlrt_marshallOut(0U), "fileTimeHi",
                  "fileTimeHi", 38);
  sf_mex_addfield(*c11_info, c11_b_emlrt_marshallOut(0U), "mFileTimeLo",
                  "mFileTimeLo", 38);
  sf_mex_addfield(*c11_info, c11_b_emlrt_marshallOut(0U), "mFileTimeHi",
                  "mFileTimeHi", 38);
  sf_mex_assign(&c11_rhs38, sf_mex_createcellmatrix(0, 1), false);
  sf_mex_assign(&c11_lhs38, sf_mex_createcellmatrix(0, 1), false);
  sf_mex_addfield(*c11_info, sf_mex_duplicatearraysafe(&c11_rhs38), "rhs", "rhs",
                  38);
  sf_mex_addfield(*c11_info, sf_mex_duplicatearraysafe(&c11_lhs38), "lhs", "lhs",
                  38);
  sf_mex_addfield(*c11_info, c11_emlrt_marshallOut(
    "[ILXE]$matlabroot$/toolbox/eml/lib/matlab/ops/eml_mtimes_helper.m"),
                  "context", "context", 39);
  sf_mex_addfield(*c11_info, c11_emlrt_marshallOut("eml_scalar_eg"), "name",
                  "name", 39);
  sf_mex_addfield(*c11_info, c11_emlrt_marshallOut("single"), "dominantType",
                  "dominantType", 39);
  sf_mex_addfield(*c11_info, c11_emlrt_marshallOut(
    "[ILXE]$matlabroot$/toolbox/eml/lib/matlab/eml/eml_scalar_eg.m"), "resolved",
                  "resolved", 39);
  sf_mex_addfield(*c11_info, c11_b_emlrt_marshallOut(1375984288U), "fileTimeLo",
                  "fileTimeLo", 39);
  sf_mex_addfield(*c11_info, c11_b_emlrt_marshallOut(0U), "fileTimeHi",
                  "fileTimeHi", 39);
  sf_mex_addfield(*c11_info, c11_b_emlrt_marshallOut(0U), "mFileTimeLo",
                  "mFileTimeLo", 39);
  sf_mex_addfield(*c11_info, c11_b_emlrt_marshallOut(0U), "mFileTimeHi",
                  "mFileTimeHi", 39);
  sf_mex_assign(&c11_rhs39, sf_mex_createcellmatrix(0, 1), false);
  sf_mex_assign(&c11_lhs39, sf_mex_createcellmatrix(0, 1), false);
  sf_mex_addfield(*c11_info, sf_mex_duplicatearraysafe(&c11_rhs39), "rhs", "rhs",
                  39);
  sf_mex_addfield(*c11_info, sf_mex_duplicatearraysafe(&c11_lhs39), "lhs", "lhs",
                  39);
  sf_mex_addfield(*c11_info, c11_emlrt_marshallOut(
    "[ILXE]$matlabroot$/toolbox/eml/lib/matlab/ops/eml_mtimes_helper.m"),
                  "context", "context", 40);
  sf_mex_addfield(*c11_info, c11_emlrt_marshallOut("eml_xgemm"), "name", "name",
                  40);
  sf_mex_addfield(*c11_info, c11_emlrt_marshallOut("single"), "dominantType",
                  "dominantType", 40);
  sf_mex_addfield(*c11_info, c11_emlrt_marshallOut(
    "[ILXE]$matlabroot$/toolbox/eml/lib/matlab/eml/blas/eml_xgemm.m"),
                  "resolved", "resolved", 40);
  sf_mex_addfield(*c11_info, c11_b_emlrt_marshallOut(1375984290U), "fileTimeLo",
                  "fileTimeLo", 40);
  sf_mex_addfield(*c11_info, c11_b_emlrt_marshallOut(0U), "fileTimeHi",
                  "fileTimeHi", 40);
  sf_mex_addfield(*c11_info, c11_b_emlrt_marshallOut(0U), "mFileTimeLo",
                  "mFileTimeLo", 40);
  sf_mex_addfield(*c11_info, c11_b_emlrt_marshallOut(0U), "mFileTimeHi",
                  "mFileTimeHi", 40);
  sf_mex_assign(&c11_rhs40, sf_mex_createcellmatrix(0, 1), false);
  sf_mex_assign(&c11_lhs40, sf_mex_createcellmatrix(0, 1), false);
  sf_mex_addfield(*c11_info, sf_mex_duplicatearraysafe(&c11_rhs40), "rhs", "rhs",
                  40);
  sf_mex_addfield(*c11_info, sf_mex_duplicatearraysafe(&c11_lhs40), "lhs", "lhs",
                  40);
  sf_mex_addfield(*c11_info, c11_emlrt_marshallOut(
    "[ILXE]$matlabroot$/toolbox/eml/lib/matlab/eml/blas/eml_xgemm.m"), "context",
                  "context", 41);
  sf_mex_addfield(*c11_info, c11_emlrt_marshallOut("coder.internal.blas.inline"),
                  "name", "name", 41);
  sf_mex_addfield(*c11_info, c11_emlrt_marshallOut(""), "dominantType",
                  "dominantType", 41);
  sf_mex_addfield(*c11_info, c11_emlrt_marshallOut(
    "[ILXE]$matlabroot$/toolbox/coder/coder/+coder/+internal/+blas/inline.p"),
                  "resolved", "resolved", 41);
  sf_mex_addfield(*c11_info, c11_b_emlrt_marshallOut(1410811372U), "fileTimeLo",
                  "fileTimeLo", 41);
  sf_mex_addfield(*c11_info, c11_b_emlrt_marshallOut(0U), "fileTimeHi",
                  "fileTimeHi", 41);
  sf_mex_addfield(*c11_info, c11_b_emlrt_marshallOut(0U), "mFileTimeLo",
                  "mFileTimeLo", 41);
  sf_mex_addfield(*c11_info, c11_b_emlrt_marshallOut(0U), "mFileTimeHi",
                  "mFileTimeHi", 41);
  sf_mex_assign(&c11_rhs41, sf_mex_createcellmatrix(0, 1), false);
  sf_mex_assign(&c11_lhs41, sf_mex_createcellmatrix(0, 1), false);
  sf_mex_addfield(*c11_info, sf_mex_duplicatearraysafe(&c11_rhs41), "rhs", "rhs",
                  41);
  sf_mex_addfield(*c11_info, sf_mex_duplicatearraysafe(&c11_lhs41), "lhs", "lhs",
                  41);
  sf_mex_addfield(*c11_info, c11_emlrt_marshallOut(
    "[ILXE]$matlabroot$/toolbox/eml/lib/matlab/eml/blas/eml_xgemm.m"), "context",
                  "context", 42);
  sf_mex_addfield(*c11_info, c11_emlrt_marshallOut("coder.internal.blas.xgemm"),
                  "name", "name", 42);
  sf_mex_addfield(*c11_info, c11_emlrt_marshallOut("single"), "dominantType",
                  "dominantType", 42);
  sf_mex_addfield(*c11_info, c11_emlrt_marshallOut(
    "[ILXE]$matlabroot$/toolbox/coder/coder/+coder/+internal/+blas/xgemm.p"),
                  "resolved", "resolved", 42);
  sf_mex_addfield(*c11_info, c11_b_emlrt_marshallOut(1410811370U), "fileTimeLo",
                  "fileTimeLo", 42);
  sf_mex_addfield(*c11_info, c11_b_emlrt_marshallOut(0U), "fileTimeHi",
                  "fileTimeHi", 42);
  sf_mex_addfield(*c11_info, c11_b_emlrt_marshallOut(0U), "mFileTimeLo",
                  "mFileTimeLo", 42);
  sf_mex_addfield(*c11_info, c11_b_emlrt_marshallOut(0U), "mFileTimeHi",
                  "mFileTimeHi", 42);
  sf_mex_assign(&c11_rhs42, sf_mex_createcellmatrix(0, 1), false);
  sf_mex_assign(&c11_lhs42, sf_mex_createcellmatrix(0, 1), false);
  sf_mex_addfield(*c11_info, sf_mex_duplicatearraysafe(&c11_rhs42), "rhs", "rhs",
                  42);
  sf_mex_addfield(*c11_info, sf_mex_duplicatearraysafe(&c11_lhs42), "lhs", "lhs",
                  42);
  sf_mex_addfield(*c11_info, c11_emlrt_marshallOut(
    "[ILXE]$matlabroot$/toolbox/coder/coder/+coder/+internal/+blas/xgemm.p"),
                  "context", "context", 43);
  sf_mex_addfield(*c11_info, c11_emlrt_marshallOut(
    "coder.internal.blas.use_refblas"), "name", "name", 43);
  sf_mex_addfield(*c11_info, c11_emlrt_marshallOut(""), "dominantType",
                  "dominantType", 43);
  sf_mex_addfield(*c11_info, c11_emlrt_marshallOut(
    "[ILXE]$matlabroot$/toolbox/coder/coder/+coder/+internal/+blas/use_refblas.p"),
                  "resolved", "resolved", 43);
  sf_mex_addfield(*c11_info, c11_b_emlrt_marshallOut(1410811370U), "fileTimeLo",
                  "fileTimeLo", 43);
  sf_mex_addfield(*c11_info, c11_b_emlrt_marshallOut(0U), "fileTimeHi",
                  "fileTimeHi", 43);
  sf_mex_addfield(*c11_info, c11_b_emlrt_marshallOut(0U), "mFileTimeLo",
                  "mFileTimeLo", 43);
  sf_mex_addfield(*c11_info, c11_b_emlrt_marshallOut(0U), "mFileTimeHi",
                  "mFileTimeHi", 43);
  sf_mex_assign(&c11_rhs43, sf_mex_createcellmatrix(0, 1), false);
  sf_mex_assign(&c11_lhs43, sf_mex_createcellmatrix(0, 1), false);
  sf_mex_addfield(*c11_info, sf_mex_duplicatearraysafe(&c11_rhs43), "rhs", "rhs",
                  43);
  sf_mex_addfield(*c11_info, sf_mex_duplicatearraysafe(&c11_lhs43), "lhs", "lhs",
                  43);
  sf_mex_addfield(*c11_info, c11_emlrt_marshallOut(
    "[ILXE]$matlabroot$/toolbox/coder/coder/+coder/+internal/+blas/xgemm.p!below_threshold"),
                  "context", "context", 44);
  sf_mex_addfield(*c11_info, c11_emlrt_marshallOut(
    "coder.internal.blas.threshold"), "name", "name", 44);
  sf_mex_addfield(*c11_info, c11_emlrt_marshallOut("char"), "dominantType",
                  "dominantType", 44);
  sf_mex_addfield(*c11_info, c11_emlrt_marshallOut(
    "[ILXE]$matlabroot$/toolbox/coder/coder/+coder/+internal/+blas/threshold.p"),
                  "resolved", "resolved", 44);
  sf_mex_addfield(*c11_info, c11_b_emlrt_marshallOut(1410811372U), "fileTimeLo",
                  "fileTimeLo", 44);
  sf_mex_addfield(*c11_info, c11_b_emlrt_marshallOut(0U), "fileTimeHi",
                  "fileTimeHi", 44);
  sf_mex_addfield(*c11_info, c11_b_emlrt_marshallOut(0U), "mFileTimeLo",
                  "mFileTimeLo", 44);
  sf_mex_addfield(*c11_info, c11_b_emlrt_marshallOut(0U), "mFileTimeHi",
                  "mFileTimeHi", 44);
  sf_mex_assign(&c11_rhs44, sf_mex_createcellmatrix(0, 1), false);
  sf_mex_assign(&c11_lhs44, sf_mex_createcellmatrix(0, 1), false);
  sf_mex_addfield(*c11_info, sf_mex_duplicatearraysafe(&c11_rhs44), "rhs", "rhs",
                  44);
  sf_mex_addfield(*c11_info, sf_mex_duplicatearraysafe(&c11_lhs44), "lhs", "lhs",
                  44);
  sf_mex_addfield(*c11_info, c11_emlrt_marshallOut(
    "[ILXE]$matlabroot$/toolbox/coder/coder/+coder/+internal/+blas/threshold.p"),
                  "context", "context", 45);
  sf_mex_addfield(*c11_info, c11_emlrt_marshallOut("eml_switch_helper"), "name",
                  "name", 45);
  sf_mex_addfield(*c11_info, c11_emlrt_marshallOut(""), "dominantType",
                  "dominantType", 45);
  sf_mex_addfield(*c11_info, c11_emlrt_marshallOut(
    "[ILXE]$matlabroot$/toolbox/eml/lib/matlab/eml/eml_switch_helper.m"),
                  "resolved", "resolved", 45);
  sf_mex_addfield(*c11_info, c11_b_emlrt_marshallOut(1393334458U), "fileTimeLo",
                  "fileTimeLo", 45);
  sf_mex_addfield(*c11_info, c11_b_emlrt_marshallOut(0U), "fileTimeHi",
                  "fileTimeHi", 45);
  sf_mex_addfield(*c11_info, c11_b_emlrt_marshallOut(0U), "mFileTimeLo",
                  "mFileTimeLo", 45);
  sf_mex_addfield(*c11_info, c11_b_emlrt_marshallOut(0U), "mFileTimeHi",
                  "mFileTimeHi", 45);
  sf_mex_assign(&c11_rhs45, sf_mex_createcellmatrix(0, 1), false);
  sf_mex_assign(&c11_lhs45, sf_mex_createcellmatrix(0, 1), false);
  sf_mex_addfield(*c11_info, sf_mex_duplicatearraysafe(&c11_rhs45), "rhs", "rhs",
                  45);
  sf_mex_addfield(*c11_info, sf_mex_duplicatearraysafe(&c11_lhs45), "lhs", "lhs",
                  45);
  sf_mex_addfield(*c11_info, c11_emlrt_marshallOut(
    "[ILXE]$matlabroot$/toolbox/coder/coder/+coder/+internal/+blas/xgemm.p"),
                  "context", "context", 46);
  sf_mex_addfield(*c11_info, c11_emlrt_marshallOut("coder.internal.scalarEg"),
                  "name", "name", 46);
  sf_mex_addfield(*c11_info, c11_emlrt_marshallOut("single"), "dominantType",
                  "dominantType", 46);
  sf_mex_addfield(*c11_info, c11_emlrt_marshallOut(
    "[ILXE]$matlabroot$/toolbox/coder/coder/+coder/+internal/scalarEg.p"),
                  "resolved", "resolved", 46);
  sf_mex_addfield(*c11_info, c11_b_emlrt_marshallOut(1410811370U), "fileTimeLo",
                  "fileTimeLo", 46);
  sf_mex_addfield(*c11_info, c11_b_emlrt_marshallOut(0U), "fileTimeHi",
                  "fileTimeHi", 46);
  sf_mex_addfield(*c11_info, c11_b_emlrt_marshallOut(0U), "mFileTimeLo",
                  "mFileTimeLo", 46);
  sf_mex_addfield(*c11_info, c11_b_emlrt_marshallOut(0U), "mFileTimeHi",
                  "mFileTimeHi", 46);
  sf_mex_assign(&c11_rhs46, sf_mex_createcellmatrix(0, 1), false);
  sf_mex_assign(&c11_lhs46, sf_mex_createcellmatrix(0, 1), false);
  sf_mex_addfield(*c11_info, sf_mex_duplicatearraysafe(&c11_rhs46), "rhs", "rhs",
                  46);
  sf_mex_addfield(*c11_info, sf_mex_duplicatearraysafe(&c11_lhs46), "lhs", "lhs",
                  46);
  sf_mex_addfield(*c11_info, c11_emlrt_marshallOut(
    "[ILXE]$matlabroot$/toolbox/coder/coder/+coder/+internal/+blas/xgemm.p"),
                  "context", "context", 47);
  sf_mex_addfield(*c11_info, c11_emlrt_marshallOut(
    "coder.internal.refblas.xgemm"), "name", "name", 47);
  sf_mex_addfield(*c11_info, c11_emlrt_marshallOut("single"), "dominantType",
                  "dominantType", 47);
  sf_mex_addfield(*c11_info, c11_emlrt_marshallOut(
    "[ILXE]$matlabroot$/toolbox/coder/coder/+coder/+internal/+refblas/xgemm.p"),
                  "resolved", "resolved", 47);
  sf_mex_addfield(*c11_info, c11_b_emlrt_marshallOut(1410811372U), "fileTimeLo",
                  "fileTimeLo", 47);
  sf_mex_addfield(*c11_info, c11_b_emlrt_marshallOut(0U), "fileTimeHi",
                  "fileTimeHi", 47);
  sf_mex_addfield(*c11_info, c11_b_emlrt_marshallOut(0U), "mFileTimeLo",
                  "mFileTimeLo", 47);
  sf_mex_addfield(*c11_info, c11_b_emlrt_marshallOut(0U), "mFileTimeHi",
                  "mFileTimeHi", 47);
  sf_mex_assign(&c11_rhs47, sf_mex_createcellmatrix(0, 1), false);
  sf_mex_assign(&c11_lhs47, sf_mex_createcellmatrix(0, 1), false);
  sf_mex_addfield(*c11_info, sf_mex_duplicatearraysafe(&c11_rhs47), "rhs", "rhs",
                  47);
  sf_mex_addfield(*c11_info, sf_mex_duplicatearraysafe(&c11_lhs47), "lhs", "lhs",
                  47);
  sf_mex_addfield(*c11_info, c11_emlrt_marshallOut(""), "context", "context", 48);
  sf_mex_addfield(*c11_info, c11_emlrt_marshallOut("norm"), "name", "name", 48);
  sf_mex_addfield(*c11_info, c11_emlrt_marshallOut("single"), "dominantType",
                  "dominantType", 48);
  sf_mex_addfield(*c11_info, c11_emlrt_marshallOut(
    "[ILXE]$matlabroot$/toolbox/eml/lib/matlab/matfun/norm.m"), "resolved",
                  "resolved", 48);
  sf_mex_addfield(*c11_info, c11_b_emlrt_marshallOut(1363717468U), "fileTimeLo",
                  "fileTimeLo", 48);
  sf_mex_addfield(*c11_info, c11_b_emlrt_marshallOut(0U), "fileTimeHi",
                  "fileTimeHi", 48);
  sf_mex_addfield(*c11_info, c11_b_emlrt_marshallOut(0U), "mFileTimeLo",
                  "mFileTimeLo", 48);
  sf_mex_addfield(*c11_info, c11_b_emlrt_marshallOut(0U), "mFileTimeHi",
                  "mFileTimeHi", 48);
  sf_mex_assign(&c11_rhs48, sf_mex_createcellmatrix(0, 1), false);
  sf_mex_assign(&c11_lhs48, sf_mex_createcellmatrix(0, 1), false);
  sf_mex_addfield(*c11_info, sf_mex_duplicatearraysafe(&c11_rhs48), "rhs", "rhs",
                  48);
  sf_mex_addfield(*c11_info, sf_mex_duplicatearraysafe(&c11_lhs48), "lhs", "lhs",
                  48);
  sf_mex_addfield(*c11_info, c11_emlrt_marshallOut(
    "[ILXE]$matlabroot$/toolbox/eml/lib/matlab/matfun/norm.m!genpnorm"),
                  "context", "context", 49);
  sf_mex_addfield(*c11_info, c11_emlrt_marshallOut("eml_index_class"), "name",
                  "name", 49);
  sf_mex_addfield(*c11_info, c11_emlrt_marshallOut(""), "dominantType",
                  "dominantType", 49);
  sf_mex_addfield(*c11_info, c11_emlrt_marshallOut(
    "[ILXE]$matlabroot$/toolbox/eml/lib/matlab/eml/eml_index_class.m"),
                  "resolved", "resolved", 49);
  sf_mex_addfield(*c11_info, c11_b_emlrt_marshallOut(1323174178U), "fileTimeLo",
                  "fileTimeLo", 49);
  sf_mex_addfield(*c11_info, c11_b_emlrt_marshallOut(0U), "fileTimeHi",
                  "fileTimeHi", 49);
  sf_mex_addfield(*c11_info, c11_b_emlrt_marshallOut(0U), "mFileTimeLo",
                  "mFileTimeLo", 49);
  sf_mex_addfield(*c11_info, c11_b_emlrt_marshallOut(0U), "mFileTimeHi",
                  "mFileTimeHi", 49);
  sf_mex_assign(&c11_rhs49, sf_mex_createcellmatrix(0, 1), false);
  sf_mex_assign(&c11_lhs49, sf_mex_createcellmatrix(0, 1), false);
  sf_mex_addfield(*c11_info, sf_mex_duplicatearraysafe(&c11_rhs49), "rhs", "rhs",
                  49);
  sf_mex_addfield(*c11_info, sf_mex_duplicatearraysafe(&c11_lhs49), "lhs", "lhs",
                  49);
  sf_mex_addfield(*c11_info, c11_emlrt_marshallOut(
    "[ILXE]$matlabroot$/toolbox/eml/lib/matlab/matfun/norm.m!genpnorm"),
                  "context", "context", 50);
  sf_mex_addfield(*c11_info, c11_emlrt_marshallOut(
    "coder.internal.isBuiltInNumeric"), "name", "name", 50);
  sf_mex_addfield(*c11_info, c11_emlrt_marshallOut("double"), "dominantType",
                  "dominantType", 50);
  sf_mex_addfield(*c11_info, c11_emlrt_marshallOut(
    "[ILXE]$matlabroot$/toolbox/shared/coder/coder/+coder/+internal/isBuiltInNumeric.m"),
                  "resolved", "resolved", 50);
  sf_mex_addfield(*c11_info, c11_b_emlrt_marshallOut(1395935456U), "fileTimeLo",
                  "fileTimeLo", 50);
  sf_mex_addfield(*c11_info, c11_b_emlrt_marshallOut(0U), "fileTimeHi",
                  "fileTimeHi", 50);
  sf_mex_addfield(*c11_info, c11_b_emlrt_marshallOut(0U), "mFileTimeLo",
                  "mFileTimeLo", 50);
  sf_mex_addfield(*c11_info, c11_b_emlrt_marshallOut(0U), "mFileTimeHi",
                  "mFileTimeHi", 50);
  sf_mex_assign(&c11_rhs50, sf_mex_createcellmatrix(0, 1), false);
  sf_mex_assign(&c11_lhs50, sf_mex_createcellmatrix(0, 1), false);
  sf_mex_addfield(*c11_info, sf_mex_duplicatearraysafe(&c11_rhs50), "rhs", "rhs",
                  50);
  sf_mex_addfield(*c11_info, sf_mex_duplicatearraysafe(&c11_lhs50), "lhs", "lhs",
                  50);
  sf_mex_addfield(*c11_info, c11_emlrt_marshallOut(
    "[ILXE]$matlabroot$/toolbox/eml/lib/matlab/matfun/norm.m!genpnorm"),
                  "context", "context", 51);
  sf_mex_addfield(*c11_info, c11_emlrt_marshallOut("eml_xnrm2"), "name", "name",
                  51);
  sf_mex_addfield(*c11_info, c11_emlrt_marshallOut("single"), "dominantType",
                  "dominantType", 51);
  sf_mex_addfield(*c11_info, c11_emlrt_marshallOut(
    "[ILXE]$matlabroot$/toolbox/eml/lib/matlab/eml/blas/eml_xnrm2.m"),
                  "resolved", "resolved", 51);
  sf_mex_addfield(*c11_info, c11_b_emlrt_marshallOut(1375984292U), "fileTimeLo",
                  "fileTimeLo", 51);
  sf_mex_addfield(*c11_info, c11_b_emlrt_marshallOut(0U), "fileTimeHi",
                  "fileTimeHi", 51);
  sf_mex_addfield(*c11_info, c11_b_emlrt_marshallOut(0U), "mFileTimeLo",
                  "mFileTimeLo", 51);
  sf_mex_addfield(*c11_info, c11_b_emlrt_marshallOut(0U), "mFileTimeHi",
                  "mFileTimeHi", 51);
  sf_mex_assign(&c11_rhs51, sf_mex_createcellmatrix(0, 1), false);
  sf_mex_assign(&c11_lhs51, sf_mex_createcellmatrix(0, 1), false);
  sf_mex_addfield(*c11_info, sf_mex_duplicatearraysafe(&c11_rhs51), "rhs", "rhs",
                  51);
  sf_mex_addfield(*c11_info, sf_mex_duplicatearraysafe(&c11_lhs51), "lhs", "lhs",
                  51);
  sf_mex_addfield(*c11_info, c11_emlrt_marshallOut(
    "[ILXE]$matlabroot$/toolbox/eml/lib/matlab/eml/blas/eml_xnrm2.m"), "context",
                  "context", 52);
  sf_mex_addfield(*c11_info, c11_emlrt_marshallOut("coder.internal.blas.inline"),
                  "name", "name", 52);
  sf_mex_addfield(*c11_info, c11_emlrt_marshallOut(""), "dominantType",
                  "dominantType", 52);
  sf_mex_addfield(*c11_info, c11_emlrt_marshallOut(
    "[ILXE]$matlabroot$/toolbox/coder/coder/+coder/+internal/+blas/inline.p"),
                  "resolved", "resolved", 52);
  sf_mex_addfield(*c11_info, c11_b_emlrt_marshallOut(1410811372U), "fileTimeLo",
                  "fileTimeLo", 52);
  sf_mex_addfield(*c11_info, c11_b_emlrt_marshallOut(0U), "fileTimeHi",
                  "fileTimeHi", 52);
  sf_mex_addfield(*c11_info, c11_b_emlrt_marshallOut(0U), "mFileTimeLo",
                  "mFileTimeLo", 52);
  sf_mex_addfield(*c11_info, c11_b_emlrt_marshallOut(0U), "mFileTimeHi",
                  "mFileTimeHi", 52);
  sf_mex_assign(&c11_rhs52, sf_mex_createcellmatrix(0, 1), false);
  sf_mex_assign(&c11_lhs52, sf_mex_createcellmatrix(0, 1), false);
  sf_mex_addfield(*c11_info, sf_mex_duplicatearraysafe(&c11_rhs52), "rhs", "rhs",
                  52);
  sf_mex_addfield(*c11_info, sf_mex_duplicatearraysafe(&c11_lhs52), "lhs", "lhs",
                  52);
  sf_mex_addfield(*c11_info, c11_emlrt_marshallOut(
    "[ILXE]$matlabroot$/toolbox/eml/lib/matlab/eml/blas/eml_xnrm2.m"), "context",
                  "context", 53);
  sf_mex_addfield(*c11_info, c11_emlrt_marshallOut("coder.internal.blas.xnrm2"),
                  "name", "name", 53);
  sf_mex_addfield(*c11_info, c11_emlrt_marshallOut("single"), "dominantType",
                  "dominantType", 53);
  sf_mex_addfield(*c11_info, c11_emlrt_marshallOut(
    "[ILXE]$matlabroot$/toolbox/coder/coder/+coder/+internal/+blas/xnrm2.p"),
                  "resolved", "resolved", 53);
  sf_mex_addfield(*c11_info, c11_b_emlrt_marshallOut(1410811370U), "fileTimeLo",
                  "fileTimeLo", 53);
  sf_mex_addfield(*c11_info, c11_b_emlrt_marshallOut(0U), "fileTimeHi",
                  "fileTimeHi", 53);
  sf_mex_addfield(*c11_info, c11_b_emlrt_marshallOut(0U), "mFileTimeLo",
                  "mFileTimeLo", 53);
  sf_mex_addfield(*c11_info, c11_b_emlrt_marshallOut(0U), "mFileTimeHi",
                  "mFileTimeHi", 53);
  sf_mex_assign(&c11_rhs53, sf_mex_createcellmatrix(0, 1), false);
  sf_mex_assign(&c11_lhs53, sf_mex_createcellmatrix(0, 1), false);
  sf_mex_addfield(*c11_info, sf_mex_duplicatearraysafe(&c11_rhs53), "rhs", "rhs",
                  53);
  sf_mex_addfield(*c11_info, sf_mex_duplicatearraysafe(&c11_lhs53), "lhs", "lhs",
                  53);
  sf_mex_addfield(*c11_info, c11_emlrt_marshallOut(
    "[ILXE]$matlabroot$/toolbox/coder/coder/+coder/+internal/+blas/xnrm2.p"),
                  "context", "context", 54);
  sf_mex_addfield(*c11_info, c11_emlrt_marshallOut(
    "coder.internal.blas.use_refblas"), "name", "name", 54);
  sf_mex_addfield(*c11_info, c11_emlrt_marshallOut(""), "dominantType",
                  "dominantType", 54);
  sf_mex_addfield(*c11_info, c11_emlrt_marshallOut(
    "[ILXE]$matlabroot$/toolbox/coder/coder/+coder/+internal/+blas/use_refblas.p"),
                  "resolved", "resolved", 54);
  sf_mex_addfield(*c11_info, c11_b_emlrt_marshallOut(1410811370U), "fileTimeLo",
                  "fileTimeLo", 54);
  sf_mex_addfield(*c11_info, c11_b_emlrt_marshallOut(0U), "fileTimeHi",
                  "fileTimeHi", 54);
  sf_mex_addfield(*c11_info, c11_b_emlrt_marshallOut(0U), "mFileTimeLo",
                  "mFileTimeLo", 54);
  sf_mex_addfield(*c11_info, c11_b_emlrt_marshallOut(0U), "mFileTimeHi",
                  "mFileTimeHi", 54);
  sf_mex_assign(&c11_rhs54, sf_mex_createcellmatrix(0, 1), false);
  sf_mex_assign(&c11_lhs54, sf_mex_createcellmatrix(0, 1), false);
  sf_mex_addfield(*c11_info, sf_mex_duplicatearraysafe(&c11_rhs54), "rhs", "rhs",
                  54);
  sf_mex_addfield(*c11_info, sf_mex_duplicatearraysafe(&c11_lhs54), "lhs", "lhs",
                  54);
  sf_mex_addfield(*c11_info, c11_emlrt_marshallOut(
    "[ILXE]$matlabroot$/toolbox/coder/coder/+coder/+internal/+blas/xnrm2.p!below_threshold"),
                  "context", "context", 55);
  sf_mex_addfield(*c11_info, c11_emlrt_marshallOut(
    "coder.internal.blas.threshold"), "name", "name", 55);
  sf_mex_addfield(*c11_info, c11_emlrt_marshallOut("char"), "dominantType",
                  "dominantType", 55);
  sf_mex_addfield(*c11_info, c11_emlrt_marshallOut(
    "[ILXE]$matlabroot$/toolbox/coder/coder/+coder/+internal/+blas/threshold.p"),
                  "resolved", "resolved", 55);
  sf_mex_addfield(*c11_info, c11_b_emlrt_marshallOut(1410811372U), "fileTimeLo",
                  "fileTimeLo", 55);
  sf_mex_addfield(*c11_info, c11_b_emlrt_marshallOut(0U), "fileTimeHi",
                  "fileTimeHi", 55);
  sf_mex_addfield(*c11_info, c11_b_emlrt_marshallOut(0U), "mFileTimeLo",
                  "mFileTimeLo", 55);
  sf_mex_addfield(*c11_info, c11_b_emlrt_marshallOut(0U), "mFileTimeHi",
                  "mFileTimeHi", 55);
  sf_mex_assign(&c11_rhs55, sf_mex_createcellmatrix(0, 1), false);
  sf_mex_assign(&c11_lhs55, sf_mex_createcellmatrix(0, 1), false);
  sf_mex_addfield(*c11_info, sf_mex_duplicatearraysafe(&c11_rhs55), "rhs", "rhs",
                  55);
  sf_mex_addfield(*c11_info, sf_mex_duplicatearraysafe(&c11_lhs55), "lhs", "lhs",
                  55);
  sf_mex_addfield(*c11_info, c11_emlrt_marshallOut(
    "[ILXE]$matlabroot$/toolbox/coder/coder/+coder/+internal/+blas/xnrm2.p"),
                  "context", "context", 56);
  sf_mex_addfield(*c11_info, c11_emlrt_marshallOut(
    "coder.internal.refblas.xnrm2"), "name", "name", 56);
  sf_mex_addfield(*c11_info, c11_emlrt_marshallOut("single"), "dominantType",
                  "dominantType", 56);
  sf_mex_addfield(*c11_info, c11_emlrt_marshallOut(
    "[ILXE]$matlabroot$/toolbox/coder/coder/+coder/+internal/+refblas/xnrm2.p"),
                  "resolved", "resolved", 56);
  sf_mex_addfield(*c11_info, c11_b_emlrt_marshallOut(1410811372U), "fileTimeLo",
                  "fileTimeLo", 56);
  sf_mex_addfield(*c11_info, c11_b_emlrt_marshallOut(0U), "fileTimeHi",
                  "fileTimeHi", 56);
  sf_mex_addfield(*c11_info, c11_b_emlrt_marshallOut(0U), "mFileTimeLo",
                  "mFileTimeLo", 56);
  sf_mex_addfield(*c11_info, c11_b_emlrt_marshallOut(0U), "mFileTimeHi",
                  "mFileTimeHi", 56);
  sf_mex_assign(&c11_rhs56, sf_mex_createcellmatrix(0, 1), false);
  sf_mex_assign(&c11_lhs56, sf_mex_createcellmatrix(0, 1), false);
  sf_mex_addfield(*c11_info, sf_mex_duplicatearraysafe(&c11_rhs56), "rhs", "rhs",
                  56);
  sf_mex_addfield(*c11_info, sf_mex_duplicatearraysafe(&c11_lhs56), "lhs", "lhs",
                  56);
  sf_mex_addfield(*c11_info, c11_emlrt_marshallOut(
    "[ILXE]$matlabroot$/toolbox/coder/coder/+coder/+internal/+refblas/xnrm2.p"),
                  "context", "context", 57);
  sf_mex_addfield(*c11_info, c11_emlrt_marshallOut("realmin"), "name", "name",
                  57);
  sf_mex_addfield(*c11_info, c11_emlrt_marshallOut("char"), "dominantType",
                  "dominantType", 57);
  sf_mex_addfield(*c11_info, c11_emlrt_marshallOut(
    "[ILXE]$matlabroot$/toolbox/eml/lib/matlab/elmat/realmin.m"), "resolved",
                  "resolved", 57);
  sf_mex_addfield(*c11_info, c11_b_emlrt_marshallOut(1307654842U), "fileTimeLo",
                  "fileTimeLo", 57);
  sf_mex_addfield(*c11_info, c11_b_emlrt_marshallOut(0U), "fileTimeHi",
                  "fileTimeHi", 57);
  sf_mex_addfield(*c11_info, c11_b_emlrt_marshallOut(0U), "mFileTimeLo",
                  "mFileTimeLo", 57);
  sf_mex_addfield(*c11_info, c11_b_emlrt_marshallOut(0U), "mFileTimeHi",
                  "mFileTimeHi", 57);
  sf_mex_assign(&c11_rhs57, sf_mex_createcellmatrix(0, 1), false);
  sf_mex_assign(&c11_lhs57, sf_mex_createcellmatrix(0, 1), false);
  sf_mex_addfield(*c11_info, sf_mex_duplicatearraysafe(&c11_rhs57), "rhs", "rhs",
                  57);
  sf_mex_addfield(*c11_info, sf_mex_duplicatearraysafe(&c11_lhs57), "lhs", "lhs",
                  57);
  sf_mex_addfield(*c11_info, c11_emlrt_marshallOut(
    "[ILXE]$matlabroot$/toolbox/eml/lib/matlab/elmat/realmin.m"), "context",
                  "context", 58);
  sf_mex_addfield(*c11_info, c11_emlrt_marshallOut("eml_realmin"), "name",
                  "name", 58);
  sf_mex_addfield(*c11_info, c11_emlrt_marshallOut("char"), "dominantType",
                  "dominantType", 58);
  sf_mex_addfield(*c11_info, c11_emlrt_marshallOut(
    "[ILXE]$matlabroot$/toolbox/eml/lib/matlab/eml/eml_realmin.m"), "resolved",
                  "resolved", 58);
  sf_mex_addfield(*c11_info, c11_b_emlrt_marshallOut(1307654844U), "fileTimeLo",
                  "fileTimeLo", 58);
  sf_mex_addfield(*c11_info, c11_b_emlrt_marshallOut(0U), "fileTimeHi",
                  "fileTimeHi", 58);
  sf_mex_addfield(*c11_info, c11_b_emlrt_marshallOut(0U), "mFileTimeLo",
                  "mFileTimeLo", 58);
  sf_mex_addfield(*c11_info, c11_b_emlrt_marshallOut(0U), "mFileTimeHi",
                  "mFileTimeHi", 58);
  sf_mex_assign(&c11_rhs58, sf_mex_createcellmatrix(0, 1), false);
  sf_mex_assign(&c11_lhs58, sf_mex_createcellmatrix(0, 1), false);
  sf_mex_addfield(*c11_info, sf_mex_duplicatearraysafe(&c11_rhs58), "rhs", "rhs",
                  58);
  sf_mex_addfield(*c11_info, sf_mex_duplicatearraysafe(&c11_lhs58), "lhs", "lhs",
                  58);
  sf_mex_addfield(*c11_info, c11_emlrt_marshallOut(
    "[ILXE]$matlabroot$/toolbox/eml/lib/matlab/eml/eml_realmin.m"), "context",
                  "context", 59);
  sf_mex_addfield(*c11_info, c11_emlrt_marshallOut("eml_float_model"), "name",
                  "name", 59);
  sf_mex_addfield(*c11_info, c11_emlrt_marshallOut("char"), "dominantType",
                  "dominantType", 59);
  sf_mex_addfield(*c11_info, c11_emlrt_marshallOut(
    "[ILXE]$matlabroot$/toolbox/eml/lib/matlab/eml/eml_float_model.m"),
                  "resolved", "resolved", 59);
  sf_mex_addfield(*c11_info, c11_b_emlrt_marshallOut(1326731596U), "fileTimeLo",
                  "fileTimeLo", 59);
  sf_mex_addfield(*c11_info, c11_b_emlrt_marshallOut(0U), "fileTimeHi",
                  "fileTimeHi", 59);
  sf_mex_addfield(*c11_info, c11_b_emlrt_marshallOut(0U), "mFileTimeLo",
                  "mFileTimeLo", 59);
  sf_mex_addfield(*c11_info, c11_b_emlrt_marshallOut(0U), "mFileTimeHi",
                  "mFileTimeHi", 59);
  sf_mex_assign(&c11_rhs59, sf_mex_createcellmatrix(0, 1), false);
  sf_mex_assign(&c11_lhs59, sf_mex_createcellmatrix(0, 1), false);
  sf_mex_addfield(*c11_info, sf_mex_duplicatearraysafe(&c11_rhs59), "rhs", "rhs",
                  59);
  sf_mex_addfield(*c11_info, sf_mex_duplicatearraysafe(&c11_lhs59), "lhs", "lhs",
                  59);
  sf_mex_addfield(*c11_info, c11_emlrt_marshallOut(
    "[ILXE]$matlabroot$/toolbox/coder/coder/+coder/+internal/+refblas/xnrm2.p"),
                  "context", "context", 60);
  sf_mex_addfield(*c11_info, c11_emlrt_marshallOut("coder.internal.indexMinus"),
                  "name", "name", 60);
  sf_mex_addfield(*c11_info, c11_emlrt_marshallOut("double"), "dominantType",
                  "dominantType", 60);
  sf_mex_addfield(*c11_info, c11_emlrt_marshallOut(
    "[ILXE]$matlabroot$/toolbox/shared/coder/coder/+coder/+internal/indexMinus.m"),
                  "resolved", "resolved", 60);
  sf_mex_addfield(*c11_info, c11_b_emlrt_marshallOut(1372586760U), "fileTimeLo",
                  "fileTimeLo", 60);
  sf_mex_addfield(*c11_info, c11_b_emlrt_marshallOut(0U), "fileTimeHi",
                  "fileTimeHi", 60);
  sf_mex_addfield(*c11_info, c11_b_emlrt_marshallOut(0U), "mFileTimeLo",
                  "mFileTimeLo", 60);
  sf_mex_addfield(*c11_info, c11_b_emlrt_marshallOut(0U), "mFileTimeHi",
                  "mFileTimeHi", 60);
  sf_mex_assign(&c11_rhs60, sf_mex_createcellmatrix(0, 1), false);
  sf_mex_assign(&c11_lhs60, sf_mex_createcellmatrix(0, 1), false);
  sf_mex_addfield(*c11_info, sf_mex_duplicatearraysafe(&c11_rhs60), "rhs", "rhs",
                  60);
  sf_mex_addfield(*c11_info, sf_mex_duplicatearraysafe(&c11_lhs60), "lhs", "lhs",
                  60);
  sf_mex_addfield(*c11_info, c11_emlrt_marshallOut(
    "[ILXE]$matlabroot$/toolbox/coder/coder/+coder/+internal/+refblas/xnrm2.p"),
                  "context", "context", 61);
  sf_mex_addfield(*c11_info, c11_emlrt_marshallOut("coder.internal.indexTimes"),
                  "name", "name", 61);
  sf_mex_addfield(*c11_info, c11_emlrt_marshallOut("coder.internal.indexInt"),
                  "dominantType", "dominantType", 61);
  sf_mex_addfield(*c11_info, c11_emlrt_marshallOut(
    "[ILXE]$matlabroot$/toolbox/shared/coder/coder/+coder/+internal/indexTimes.m"),
                  "resolved", "resolved", 61);
  sf_mex_addfield(*c11_info, c11_b_emlrt_marshallOut(1372586760U), "fileTimeLo",
                  "fileTimeLo", 61);
  sf_mex_addfield(*c11_info, c11_b_emlrt_marshallOut(0U), "fileTimeHi",
                  "fileTimeHi", 61);
  sf_mex_addfield(*c11_info, c11_b_emlrt_marshallOut(0U), "mFileTimeLo",
                  "mFileTimeLo", 61);
  sf_mex_addfield(*c11_info, c11_b_emlrt_marshallOut(0U), "mFileTimeHi",
                  "mFileTimeHi", 61);
  sf_mex_assign(&c11_rhs61, sf_mex_createcellmatrix(0, 1), false);
  sf_mex_assign(&c11_lhs61, sf_mex_createcellmatrix(0, 1), false);
  sf_mex_addfield(*c11_info, sf_mex_duplicatearraysafe(&c11_rhs61), "rhs", "rhs",
                  61);
  sf_mex_addfield(*c11_info, sf_mex_duplicatearraysafe(&c11_lhs61), "lhs", "lhs",
                  61);
  sf_mex_addfield(*c11_info, c11_emlrt_marshallOut(
    "[ILXE]$matlabroot$/toolbox/coder/coder/+coder/+internal/+refblas/xnrm2.p"),
                  "context", "context", 62);
  sf_mex_addfield(*c11_info, c11_emlrt_marshallOut("coder.internal.indexPlus"),
                  "name", "name", 62);
  sf_mex_addfield(*c11_info, c11_emlrt_marshallOut("coder.internal.indexInt"),
                  "dominantType", "dominantType", 62);
  sf_mex_addfield(*c11_info, c11_emlrt_marshallOut(
    "[ILXE]$matlabroot$/toolbox/shared/coder/coder/+coder/+internal/indexPlus.m"),
                  "resolved", "resolved", 62);
  sf_mex_addfield(*c11_info, c11_b_emlrt_marshallOut(1372586760U), "fileTimeLo",
                  "fileTimeLo", 62);
  sf_mex_addfield(*c11_info, c11_b_emlrt_marshallOut(0U), "fileTimeHi",
                  "fileTimeHi", 62);
  sf_mex_addfield(*c11_info, c11_b_emlrt_marshallOut(0U), "mFileTimeLo",
                  "mFileTimeLo", 62);
  sf_mex_addfield(*c11_info, c11_b_emlrt_marshallOut(0U), "mFileTimeHi",
                  "mFileTimeHi", 62);
  sf_mex_assign(&c11_rhs62, sf_mex_createcellmatrix(0, 1), false);
  sf_mex_assign(&c11_lhs62, sf_mex_createcellmatrix(0, 1), false);
  sf_mex_addfield(*c11_info, sf_mex_duplicatearraysafe(&c11_rhs62), "rhs", "rhs",
                  62);
  sf_mex_addfield(*c11_info, sf_mex_duplicatearraysafe(&c11_lhs62), "lhs", "lhs",
                  62);
  sf_mex_addfield(*c11_info, c11_emlrt_marshallOut(
    "[ILXE]$matlabroot$/toolbox/coder/coder/+coder/+internal/+refblas/xnrm2.p"),
                  "context", "context", 63);
  sf_mex_addfield(*c11_info, c11_emlrt_marshallOut(
    "eml_int_forloop_overflow_check"), "name", "name", 63);
  sf_mex_addfield(*c11_info, c11_emlrt_marshallOut(""), "dominantType",
                  "dominantType", 63);
  sf_mex_addfield(*c11_info, c11_emlrt_marshallOut(
    "[ILXE]$matlabroot$/toolbox/eml/lib/matlab/eml/eml_int_forloop_overflow_check.m"),
                  "resolved", "resolved", 63);
  sf_mex_addfield(*c11_info, c11_b_emlrt_marshallOut(1397261022U), "fileTimeLo",
                  "fileTimeLo", 63);
  sf_mex_addfield(*c11_info, c11_b_emlrt_marshallOut(0U), "fileTimeHi",
                  "fileTimeHi", 63);
  sf_mex_addfield(*c11_info, c11_b_emlrt_marshallOut(0U), "mFileTimeLo",
                  "mFileTimeLo", 63);
  sf_mex_addfield(*c11_info, c11_b_emlrt_marshallOut(0U), "mFileTimeHi",
                  "mFileTimeHi", 63);
  sf_mex_assign(&c11_rhs63, sf_mex_createcellmatrix(0, 1), false);
  sf_mex_assign(&c11_lhs63, sf_mex_createcellmatrix(0, 1), false);
  sf_mex_addfield(*c11_info, sf_mex_duplicatearraysafe(&c11_rhs63), "rhs", "rhs",
                  63);
  sf_mex_addfield(*c11_info, sf_mex_duplicatearraysafe(&c11_lhs63), "lhs", "lhs",
                  63);
  sf_mex_destroy(&c11_rhs0);
  sf_mex_destroy(&c11_lhs0);
  sf_mex_destroy(&c11_rhs1);
  sf_mex_destroy(&c11_lhs1);
  sf_mex_destroy(&c11_rhs2);
  sf_mex_destroy(&c11_lhs2);
  sf_mex_destroy(&c11_rhs3);
  sf_mex_destroy(&c11_lhs3);
  sf_mex_destroy(&c11_rhs4);
  sf_mex_destroy(&c11_lhs4);
  sf_mex_destroy(&c11_rhs5);
  sf_mex_destroy(&c11_lhs5);
  sf_mex_destroy(&c11_rhs6);
  sf_mex_destroy(&c11_lhs6);
  sf_mex_destroy(&c11_rhs7);
  sf_mex_destroy(&c11_lhs7);
  sf_mex_destroy(&c11_rhs8);
  sf_mex_destroy(&c11_lhs8);
  sf_mex_destroy(&c11_rhs9);
  sf_mex_destroy(&c11_lhs9);
  sf_mex_destroy(&c11_rhs10);
  sf_mex_destroy(&c11_lhs10);
  sf_mex_destroy(&c11_rhs11);
  sf_mex_destroy(&c11_lhs11);
  sf_mex_destroy(&c11_rhs12);
  sf_mex_destroy(&c11_lhs12);
  sf_mex_destroy(&c11_rhs13);
  sf_mex_destroy(&c11_lhs13);
  sf_mex_destroy(&c11_rhs14);
  sf_mex_destroy(&c11_lhs14);
  sf_mex_destroy(&c11_rhs15);
  sf_mex_destroy(&c11_lhs15);
  sf_mex_destroy(&c11_rhs16);
  sf_mex_destroy(&c11_lhs16);
  sf_mex_destroy(&c11_rhs17);
  sf_mex_destroy(&c11_lhs17);
  sf_mex_destroy(&c11_rhs18);
  sf_mex_destroy(&c11_lhs18);
  sf_mex_destroy(&c11_rhs19);
  sf_mex_destroy(&c11_lhs19);
  sf_mex_destroy(&c11_rhs20);
  sf_mex_destroy(&c11_lhs20);
  sf_mex_destroy(&c11_rhs21);
  sf_mex_destroy(&c11_lhs21);
  sf_mex_destroy(&c11_rhs22);
  sf_mex_destroy(&c11_lhs22);
  sf_mex_destroy(&c11_rhs23);
  sf_mex_destroy(&c11_lhs23);
  sf_mex_destroy(&c11_rhs24);
  sf_mex_destroy(&c11_lhs24);
  sf_mex_destroy(&c11_rhs25);
  sf_mex_destroy(&c11_lhs25);
  sf_mex_destroy(&c11_rhs26);
  sf_mex_destroy(&c11_lhs26);
  sf_mex_destroy(&c11_rhs27);
  sf_mex_destroy(&c11_lhs27);
  sf_mex_destroy(&c11_rhs28);
  sf_mex_destroy(&c11_lhs28);
  sf_mex_destroy(&c11_rhs29);
  sf_mex_destroy(&c11_lhs29);
  sf_mex_destroy(&c11_rhs30);
  sf_mex_destroy(&c11_lhs30);
  sf_mex_destroy(&c11_rhs31);
  sf_mex_destroy(&c11_lhs31);
  sf_mex_destroy(&c11_rhs32);
  sf_mex_destroy(&c11_lhs32);
  sf_mex_destroy(&c11_rhs33);
  sf_mex_destroy(&c11_lhs33);
  sf_mex_destroy(&c11_rhs34);
  sf_mex_destroy(&c11_lhs34);
  sf_mex_destroy(&c11_rhs35);
  sf_mex_destroy(&c11_lhs35);
  sf_mex_destroy(&c11_rhs36);
  sf_mex_destroy(&c11_lhs36);
  sf_mex_destroy(&c11_rhs37);
  sf_mex_destroy(&c11_lhs37);
  sf_mex_destroy(&c11_rhs38);
  sf_mex_destroy(&c11_lhs38);
  sf_mex_destroy(&c11_rhs39);
  sf_mex_destroy(&c11_lhs39);
  sf_mex_destroy(&c11_rhs40);
  sf_mex_destroy(&c11_lhs40);
  sf_mex_destroy(&c11_rhs41);
  sf_mex_destroy(&c11_lhs41);
  sf_mex_destroy(&c11_rhs42);
  sf_mex_destroy(&c11_lhs42);
  sf_mex_destroy(&c11_rhs43);
  sf_mex_destroy(&c11_lhs43);
  sf_mex_destroy(&c11_rhs44);
  sf_mex_destroy(&c11_lhs44);
  sf_mex_destroy(&c11_rhs45);
  sf_mex_destroy(&c11_lhs45);
  sf_mex_destroy(&c11_rhs46);
  sf_mex_destroy(&c11_lhs46);
  sf_mex_destroy(&c11_rhs47);
  sf_mex_destroy(&c11_lhs47);
  sf_mex_destroy(&c11_rhs48);
  sf_mex_destroy(&c11_lhs48);
  sf_mex_destroy(&c11_rhs49);
  sf_mex_destroy(&c11_lhs49);
  sf_mex_destroy(&c11_rhs50);
  sf_mex_destroy(&c11_lhs50);
  sf_mex_destroy(&c11_rhs51);
  sf_mex_destroy(&c11_lhs51);
  sf_mex_destroy(&c11_rhs52);
  sf_mex_destroy(&c11_lhs52);
  sf_mex_destroy(&c11_rhs53);
  sf_mex_destroy(&c11_lhs53);
  sf_mex_destroy(&c11_rhs54);
  sf_mex_destroy(&c11_lhs54);
  sf_mex_destroy(&c11_rhs55);
  sf_mex_destroy(&c11_lhs55);
  sf_mex_destroy(&c11_rhs56);
  sf_mex_destroy(&c11_lhs56);
  sf_mex_destroy(&c11_rhs57);
  sf_mex_destroy(&c11_lhs57);
  sf_mex_destroy(&c11_rhs58);
  sf_mex_destroy(&c11_lhs58);
  sf_mex_destroy(&c11_rhs59);
  sf_mex_destroy(&c11_lhs59);
  sf_mex_destroy(&c11_rhs60);
  sf_mex_destroy(&c11_lhs60);
  sf_mex_destroy(&c11_rhs61);
  sf_mex_destroy(&c11_lhs61);
  sf_mex_destroy(&c11_rhs62);
  sf_mex_destroy(&c11_lhs62);
  sf_mex_destroy(&c11_rhs63);
  sf_mex_destroy(&c11_lhs63);
}

static const mxArray *c11_emlrt_marshallOut(const char * c11_u)
{
  const mxArray *c11_y = NULL;
  c11_y = NULL;
  sf_mex_assign(&c11_y, sf_mex_create("y", c11_u, 15, 0U, 0U, 0U, 2, 1, strlen
    (c11_u)), false);
  return c11_y;
}

static const mxArray *c11_b_emlrt_marshallOut(const uint32_T c11_u)
{
  const mxArray *c11_y = NULL;
  c11_y = NULL;
  sf_mex_assign(&c11_y, sf_mex_create("y", &c11_u, 7, 0U, 0U, 0U, 0), false);
  return c11_y;
}

static void c11_b_info_helper(const mxArray **c11_info)
{
  const mxArray *c11_rhs64 = NULL;
  const mxArray *c11_lhs64 = NULL;
  const mxArray *c11_rhs65 = NULL;
  const mxArray *c11_lhs65 = NULL;
  const mxArray *c11_rhs66 = NULL;
  const mxArray *c11_lhs66 = NULL;
  const mxArray *c11_rhs67 = NULL;
  const mxArray *c11_lhs67 = NULL;
  const mxArray *c11_rhs68 = NULL;
  const mxArray *c11_lhs68 = NULL;
  const mxArray *c11_rhs69 = NULL;
  const mxArray *c11_lhs69 = NULL;
  const mxArray *c11_rhs70 = NULL;
  const mxArray *c11_lhs70 = NULL;
  const mxArray *c11_rhs71 = NULL;
  const mxArray *c11_lhs71 = NULL;
  const mxArray *c11_rhs72 = NULL;
  const mxArray *c11_lhs72 = NULL;
  const mxArray *c11_rhs73 = NULL;
  const mxArray *c11_lhs73 = NULL;
  const mxArray *c11_rhs74 = NULL;
  const mxArray *c11_lhs74 = NULL;
  const mxArray *c11_rhs75 = NULL;
  const mxArray *c11_lhs75 = NULL;
  const mxArray *c11_rhs76 = NULL;
  const mxArray *c11_lhs76 = NULL;
  const mxArray *c11_rhs77 = NULL;
  const mxArray *c11_lhs77 = NULL;
  const mxArray *c11_rhs78 = NULL;
  const mxArray *c11_lhs78 = NULL;
  const mxArray *c11_rhs79 = NULL;
  const mxArray *c11_lhs79 = NULL;
  const mxArray *c11_rhs80 = NULL;
  const mxArray *c11_lhs80 = NULL;
  const mxArray *c11_rhs81 = NULL;
  const mxArray *c11_lhs81 = NULL;
  const mxArray *c11_rhs82 = NULL;
  const mxArray *c11_lhs82 = NULL;
  const mxArray *c11_rhs83 = NULL;
  const mxArray *c11_lhs83 = NULL;
  const mxArray *c11_rhs84 = NULL;
  const mxArray *c11_lhs84 = NULL;
  const mxArray *c11_rhs85 = NULL;
  const mxArray *c11_lhs85 = NULL;
  const mxArray *c11_rhs86 = NULL;
  const mxArray *c11_lhs86 = NULL;
  sf_mex_addfield(*c11_info, c11_emlrt_marshallOut(
    "[ILXE]$matlabroot$/toolbox/eml/lib/matlab/eml/eml_int_forloop_overflow_check.m!eml_int_forloop_overflow_check_helper"),
                  "context", "context", 64);
  sf_mex_addfield(*c11_info, c11_emlrt_marshallOut("isfi"), "name", "name", 64);
  sf_mex_addfield(*c11_info, c11_emlrt_marshallOut("coder.internal.indexInt"),
                  "dominantType", "dominantType", 64);
  sf_mex_addfield(*c11_info, c11_emlrt_marshallOut(
    "[ILXE]$matlabroot$/toolbox/eml/lib/fixedpoint/isfi.m"), "resolved",
                  "resolved", 64);
  sf_mex_addfield(*c11_info, c11_b_emlrt_marshallOut(1346513958U), "fileTimeLo",
                  "fileTimeLo", 64);
  sf_mex_addfield(*c11_info, c11_b_emlrt_marshallOut(0U), "fileTimeHi",
                  "fileTimeHi", 64);
  sf_mex_addfield(*c11_info, c11_b_emlrt_marshallOut(0U), "mFileTimeLo",
                  "mFileTimeLo", 64);
  sf_mex_addfield(*c11_info, c11_b_emlrt_marshallOut(0U), "mFileTimeHi",
                  "mFileTimeHi", 64);
  sf_mex_assign(&c11_rhs64, sf_mex_createcellmatrix(0, 1), false);
  sf_mex_assign(&c11_lhs64, sf_mex_createcellmatrix(0, 1), false);
  sf_mex_addfield(*c11_info, sf_mex_duplicatearraysafe(&c11_rhs64), "rhs", "rhs",
                  64);
  sf_mex_addfield(*c11_info, sf_mex_duplicatearraysafe(&c11_lhs64), "lhs", "lhs",
                  64);
  sf_mex_addfield(*c11_info, c11_emlrt_marshallOut(
    "[ILXE]$matlabroot$/toolbox/eml/lib/fixedpoint/isfi.m"), "context",
                  "context", 65);
  sf_mex_addfield(*c11_info, c11_emlrt_marshallOut("isnumerictype"), "name",
                  "name", 65);
  sf_mex_addfield(*c11_info, c11_emlrt_marshallOut("char"), "dominantType",
                  "dominantType", 65);
  sf_mex_addfield(*c11_info, c11_emlrt_marshallOut(
    "[ILXE]$matlabroot$/toolbox/eml/lib/fixedpoint/isnumerictype.m"), "resolved",
                  "resolved", 65);
  sf_mex_addfield(*c11_info, c11_b_emlrt_marshallOut(1398879198U), "fileTimeLo",
                  "fileTimeLo", 65);
  sf_mex_addfield(*c11_info, c11_b_emlrt_marshallOut(0U), "fileTimeHi",
                  "fileTimeHi", 65);
  sf_mex_addfield(*c11_info, c11_b_emlrt_marshallOut(0U), "mFileTimeLo",
                  "mFileTimeLo", 65);
  sf_mex_addfield(*c11_info, c11_b_emlrt_marshallOut(0U), "mFileTimeHi",
                  "mFileTimeHi", 65);
  sf_mex_assign(&c11_rhs65, sf_mex_createcellmatrix(0, 1), false);
  sf_mex_assign(&c11_lhs65, sf_mex_createcellmatrix(0, 1), false);
  sf_mex_addfield(*c11_info, sf_mex_duplicatearraysafe(&c11_rhs65), "rhs", "rhs",
                  65);
  sf_mex_addfield(*c11_info, sf_mex_duplicatearraysafe(&c11_lhs65), "lhs", "lhs",
                  65);
  sf_mex_addfield(*c11_info, c11_emlrt_marshallOut(
    "[ILXE]$matlabroot$/toolbox/eml/lib/matlab/eml/eml_int_forloop_overflow_check.m!eml_int_forloop_overflow_check_helper"),
                  "context", "context", 66);
  sf_mex_addfield(*c11_info, c11_emlrt_marshallOut("intmax"), "name", "name", 66);
  sf_mex_addfield(*c11_info, c11_emlrt_marshallOut("char"), "dominantType",
                  "dominantType", 66);
  sf_mex_addfield(*c11_info, c11_emlrt_marshallOut(
    "[ILXE]$matlabroot$/toolbox/eml/lib/matlab/elmat/intmax.m"), "resolved",
                  "resolved", 66);
  sf_mex_addfield(*c11_info, c11_b_emlrt_marshallOut(1362265482U), "fileTimeLo",
                  "fileTimeLo", 66);
  sf_mex_addfield(*c11_info, c11_b_emlrt_marshallOut(0U), "fileTimeHi",
                  "fileTimeHi", 66);
  sf_mex_addfield(*c11_info, c11_b_emlrt_marshallOut(0U), "mFileTimeLo",
                  "mFileTimeLo", 66);
  sf_mex_addfield(*c11_info, c11_b_emlrt_marshallOut(0U), "mFileTimeHi",
                  "mFileTimeHi", 66);
  sf_mex_assign(&c11_rhs66, sf_mex_createcellmatrix(0, 1), false);
  sf_mex_assign(&c11_lhs66, sf_mex_createcellmatrix(0, 1), false);
  sf_mex_addfield(*c11_info, sf_mex_duplicatearraysafe(&c11_rhs66), "rhs", "rhs",
                  66);
  sf_mex_addfield(*c11_info, sf_mex_duplicatearraysafe(&c11_lhs66), "lhs", "lhs",
                  66);
  sf_mex_addfield(*c11_info, c11_emlrt_marshallOut(
    "[ILXE]$matlabroot$/toolbox/eml/lib/matlab/elmat/intmax.m"), "context",
                  "context", 67);
  sf_mex_addfield(*c11_info, c11_emlrt_marshallOut("eml_switch_helper"), "name",
                  "name", 67);
  sf_mex_addfield(*c11_info, c11_emlrt_marshallOut(""), "dominantType",
                  "dominantType", 67);
  sf_mex_addfield(*c11_info, c11_emlrt_marshallOut(
    "[ILXE]$matlabroot$/toolbox/eml/lib/matlab/eml/eml_switch_helper.m"),
                  "resolved", "resolved", 67);
  sf_mex_addfield(*c11_info, c11_b_emlrt_marshallOut(1393334458U), "fileTimeLo",
                  "fileTimeLo", 67);
  sf_mex_addfield(*c11_info, c11_b_emlrt_marshallOut(0U), "fileTimeHi",
                  "fileTimeHi", 67);
  sf_mex_addfield(*c11_info, c11_b_emlrt_marshallOut(0U), "mFileTimeLo",
                  "mFileTimeLo", 67);
  sf_mex_addfield(*c11_info, c11_b_emlrt_marshallOut(0U), "mFileTimeHi",
                  "mFileTimeHi", 67);
  sf_mex_assign(&c11_rhs67, sf_mex_createcellmatrix(0, 1), false);
  sf_mex_assign(&c11_lhs67, sf_mex_createcellmatrix(0, 1), false);
  sf_mex_addfield(*c11_info, sf_mex_duplicatearraysafe(&c11_rhs67), "rhs", "rhs",
                  67);
  sf_mex_addfield(*c11_info, sf_mex_duplicatearraysafe(&c11_lhs67), "lhs", "lhs",
                  67);
  sf_mex_addfield(*c11_info, c11_emlrt_marshallOut(
    "[ILXE]$matlabroot$/toolbox/eml/lib/matlab/eml/eml_int_forloop_overflow_check.m!eml_int_forloop_overflow_check_helper"),
                  "context", "context", 68);
  sf_mex_addfield(*c11_info, c11_emlrt_marshallOut("intmin"), "name", "name", 68);
  sf_mex_addfield(*c11_info, c11_emlrt_marshallOut("char"), "dominantType",
                  "dominantType", 68);
  sf_mex_addfield(*c11_info, c11_emlrt_marshallOut(
    "[ILXE]$matlabroot$/toolbox/eml/lib/matlab/elmat/intmin.m"), "resolved",
                  "resolved", 68);
  sf_mex_addfield(*c11_info, c11_b_emlrt_marshallOut(1362265482U), "fileTimeLo",
                  "fileTimeLo", 68);
  sf_mex_addfield(*c11_info, c11_b_emlrt_marshallOut(0U), "fileTimeHi",
                  "fileTimeHi", 68);
  sf_mex_addfield(*c11_info, c11_b_emlrt_marshallOut(0U), "mFileTimeLo",
                  "mFileTimeLo", 68);
  sf_mex_addfield(*c11_info, c11_b_emlrt_marshallOut(0U), "mFileTimeHi",
                  "mFileTimeHi", 68);
  sf_mex_assign(&c11_rhs68, sf_mex_createcellmatrix(0, 1), false);
  sf_mex_assign(&c11_lhs68, sf_mex_createcellmatrix(0, 1), false);
  sf_mex_addfield(*c11_info, sf_mex_duplicatearraysafe(&c11_rhs68), "rhs", "rhs",
                  68);
  sf_mex_addfield(*c11_info, sf_mex_duplicatearraysafe(&c11_lhs68), "lhs", "lhs",
                  68);
  sf_mex_addfield(*c11_info, c11_emlrt_marshallOut(
    "[ILXE]$matlabroot$/toolbox/eml/lib/matlab/elmat/intmin.m"), "context",
                  "context", 69);
  sf_mex_addfield(*c11_info, c11_emlrt_marshallOut("eml_switch_helper"), "name",
                  "name", 69);
  sf_mex_addfield(*c11_info, c11_emlrt_marshallOut(""), "dominantType",
                  "dominantType", 69);
  sf_mex_addfield(*c11_info, c11_emlrt_marshallOut(
    "[ILXE]$matlabroot$/toolbox/eml/lib/matlab/eml/eml_switch_helper.m"),
                  "resolved", "resolved", 69);
  sf_mex_addfield(*c11_info, c11_b_emlrt_marshallOut(1393334458U), "fileTimeLo",
                  "fileTimeLo", 69);
  sf_mex_addfield(*c11_info, c11_b_emlrt_marshallOut(0U), "fileTimeHi",
                  "fileTimeHi", 69);
  sf_mex_addfield(*c11_info, c11_b_emlrt_marshallOut(0U), "mFileTimeLo",
                  "mFileTimeLo", 69);
  sf_mex_addfield(*c11_info, c11_b_emlrt_marshallOut(0U), "mFileTimeHi",
                  "mFileTimeHi", 69);
  sf_mex_assign(&c11_rhs69, sf_mex_createcellmatrix(0, 1), false);
  sf_mex_assign(&c11_lhs69, sf_mex_createcellmatrix(0, 1), false);
  sf_mex_addfield(*c11_info, sf_mex_duplicatearraysafe(&c11_rhs69), "rhs", "rhs",
                  69);
  sf_mex_addfield(*c11_info, sf_mex_duplicatearraysafe(&c11_lhs69), "lhs", "lhs",
                  69);
  sf_mex_addfield(*c11_info, c11_emlrt_marshallOut(
    "[ILXE]$matlabroot$/toolbox/coder/coder/+coder/+internal/+refblas/xnrm2.p"),
                  "context", "context", 70);
  sf_mex_addfield(*c11_info, c11_emlrt_marshallOut("abs"), "name", "name", 70);
  sf_mex_addfield(*c11_info, c11_emlrt_marshallOut("single"), "dominantType",
                  "dominantType", 70);
  sf_mex_addfield(*c11_info, c11_emlrt_marshallOut(
    "[ILXE]$matlabroot$/toolbox/eml/lib/matlab/elfun/abs.m"), "resolved",
                  "resolved", 70);
  sf_mex_addfield(*c11_info, c11_b_emlrt_marshallOut(1363717452U), "fileTimeLo",
                  "fileTimeLo", 70);
  sf_mex_addfield(*c11_info, c11_b_emlrt_marshallOut(0U), "fileTimeHi",
                  "fileTimeHi", 70);
  sf_mex_addfield(*c11_info, c11_b_emlrt_marshallOut(0U), "mFileTimeLo",
                  "mFileTimeLo", 70);
  sf_mex_addfield(*c11_info, c11_b_emlrt_marshallOut(0U), "mFileTimeHi",
                  "mFileTimeHi", 70);
  sf_mex_assign(&c11_rhs70, sf_mex_createcellmatrix(0, 1), false);
  sf_mex_assign(&c11_lhs70, sf_mex_createcellmatrix(0, 1), false);
  sf_mex_addfield(*c11_info, sf_mex_duplicatearraysafe(&c11_rhs70), "rhs", "rhs",
                  70);
  sf_mex_addfield(*c11_info, sf_mex_duplicatearraysafe(&c11_lhs70), "lhs", "lhs",
                  70);
  sf_mex_addfield(*c11_info, c11_emlrt_marshallOut(""), "context", "context", 71);
  sf_mex_addfield(*c11_info, c11_emlrt_marshallOut("min"), "name", "name", 71);
  sf_mex_addfield(*c11_info, c11_emlrt_marshallOut("int8"), "dominantType",
                  "dominantType", 71);
  sf_mex_addfield(*c11_info, c11_emlrt_marshallOut(
    "[ILXE]$matlabroot$/toolbox/eml/lib/matlab/datafun/min.m"), "resolved",
                  "resolved", 71);
  sf_mex_addfield(*c11_info, c11_b_emlrt_marshallOut(1311258918U), "fileTimeLo",
                  "fileTimeLo", 71);
  sf_mex_addfield(*c11_info, c11_b_emlrt_marshallOut(0U), "fileTimeHi",
                  "fileTimeHi", 71);
  sf_mex_addfield(*c11_info, c11_b_emlrt_marshallOut(0U), "mFileTimeLo",
                  "mFileTimeLo", 71);
  sf_mex_addfield(*c11_info, c11_b_emlrt_marshallOut(0U), "mFileTimeHi",
                  "mFileTimeHi", 71);
  sf_mex_assign(&c11_rhs71, sf_mex_createcellmatrix(0, 1), false);
  sf_mex_assign(&c11_lhs71, sf_mex_createcellmatrix(0, 1), false);
  sf_mex_addfield(*c11_info, sf_mex_duplicatearraysafe(&c11_rhs71), "rhs", "rhs",
                  71);
  sf_mex_addfield(*c11_info, sf_mex_duplicatearraysafe(&c11_lhs71), "lhs", "lhs",
                  71);
  sf_mex_addfield(*c11_info, c11_emlrt_marshallOut(
    "[ILXE]$matlabroot$/toolbox/eml/lib/matlab/datafun/min.m"), "context",
                  "context", 72);
  sf_mex_addfield(*c11_info, c11_emlrt_marshallOut("eml_min_or_max"), "name",
                  "name", 72);
  sf_mex_addfield(*c11_info, c11_emlrt_marshallOut("int8"), "dominantType",
                  "dominantType", 72);
  sf_mex_addfield(*c11_info, c11_emlrt_marshallOut(
    "[ILXE]$matlabroot$/toolbox/eml/lib/matlab/eml/eml_min_or_max.m"),
                  "resolved", "resolved", 72);
  sf_mex_addfield(*c11_info, c11_b_emlrt_marshallOut(1378299584U), "fileTimeLo",
                  "fileTimeLo", 72);
  sf_mex_addfield(*c11_info, c11_b_emlrt_marshallOut(0U), "fileTimeHi",
                  "fileTimeHi", 72);
  sf_mex_addfield(*c11_info, c11_b_emlrt_marshallOut(0U), "mFileTimeLo",
                  "mFileTimeLo", 72);
  sf_mex_addfield(*c11_info, c11_b_emlrt_marshallOut(0U), "mFileTimeHi",
                  "mFileTimeHi", 72);
  sf_mex_assign(&c11_rhs72, sf_mex_createcellmatrix(0, 1), false);
  sf_mex_assign(&c11_lhs72, sf_mex_createcellmatrix(0, 1), false);
  sf_mex_addfield(*c11_info, sf_mex_duplicatearraysafe(&c11_rhs72), "rhs", "rhs",
                  72);
  sf_mex_addfield(*c11_info, sf_mex_duplicatearraysafe(&c11_lhs72), "lhs", "lhs",
                  72);
  sf_mex_addfield(*c11_info, c11_emlrt_marshallOut(
    "[ILXE]$matlabroot$/toolbox/eml/lib/matlab/eml/eml_min_or_max.m!eml_extremum"),
                  "context", "context", 73);
  sf_mex_addfield(*c11_info, c11_emlrt_marshallOut("eml_const_nonsingleton_dim"),
                  "name", "name", 73);
  sf_mex_addfield(*c11_info, c11_emlrt_marshallOut("int8"), "dominantType",
                  "dominantType", 73);
  sf_mex_addfield(*c11_info, c11_emlrt_marshallOut(
    "[ILXE]$matlabroot$/toolbox/eml/lib/matlab/eml/eml_const_nonsingleton_dim.m"),
                  "resolved", "resolved", 73);
  sf_mex_addfield(*c11_info, c11_b_emlrt_marshallOut(1372586016U), "fileTimeLo",
                  "fileTimeLo", 73);
  sf_mex_addfield(*c11_info, c11_b_emlrt_marshallOut(0U), "fileTimeHi",
                  "fileTimeHi", 73);
  sf_mex_addfield(*c11_info, c11_b_emlrt_marshallOut(0U), "mFileTimeLo",
                  "mFileTimeLo", 73);
  sf_mex_addfield(*c11_info, c11_b_emlrt_marshallOut(0U), "mFileTimeHi",
                  "mFileTimeHi", 73);
  sf_mex_assign(&c11_rhs73, sf_mex_createcellmatrix(0, 1), false);
  sf_mex_assign(&c11_lhs73, sf_mex_createcellmatrix(0, 1), false);
  sf_mex_addfield(*c11_info, sf_mex_duplicatearraysafe(&c11_rhs73), "rhs", "rhs",
                  73);
  sf_mex_addfield(*c11_info, sf_mex_duplicatearraysafe(&c11_lhs73), "lhs", "lhs",
                  73);
  sf_mex_addfield(*c11_info, c11_emlrt_marshallOut(
    "[ILXE]$matlabroot$/toolbox/eml/lib/matlab/eml/eml_const_nonsingleton_dim.m"),
                  "context", "context", 74);
  sf_mex_addfield(*c11_info, c11_emlrt_marshallOut(
    "coder.internal.constNonSingletonDim"), "name", "name", 74);
  sf_mex_addfield(*c11_info, c11_emlrt_marshallOut("int8"), "dominantType",
                  "dominantType", 74);
  sf_mex_addfield(*c11_info, c11_emlrt_marshallOut(
    "[ILXE]$matlabroot$/toolbox/shared/coder/coder/+coder/+internal/constNonSingletonDim.m"),
                  "resolved", "resolved", 74);
  sf_mex_addfield(*c11_info, c11_b_emlrt_marshallOut(1372586760U), "fileTimeLo",
                  "fileTimeLo", 74);
  sf_mex_addfield(*c11_info, c11_b_emlrt_marshallOut(0U), "fileTimeHi",
                  "fileTimeHi", 74);
  sf_mex_addfield(*c11_info, c11_b_emlrt_marshallOut(0U), "mFileTimeLo",
                  "mFileTimeLo", 74);
  sf_mex_addfield(*c11_info, c11_b_emlrt_marshallOut(0U), "mFileTimeHi",
                  "mFileTimeHi", 74);
  sf_mex_assign(&c11_rhs74, sf_mex_createcellmatrix(0, 1), false);
  sf_mex_assign(&c11_lhs74, sf_mex_createcellmatrix(0, 1), false);
  sf_mex_addfield(*c11_info, sf_mex_duplicatearraysafe(&c11_rhs74), "rhs", "rhs",
                  74);
  sf_mex_addfield(*c11_info, sf_mex_duplicatearraysafe(&c11_lhs74), "lhs", "lhs",
                  74);
  sf_mex_addfield(*c11_info, c11_emlrt_marshallOut(
    "[ILXE]$matlabroot$/toolbox/eml/lib/matlab/eml/eml_min_or_max.m!eml_extremum"),
                  "context", "context", 75);
  sf_mex_addfield(*c11_info, c11_emlrt_marshallOut("eml_scalar_eg"), "name",
                  "name", 75);
  sf_mex_addfield(*c11_info, c11_emlrt_marshallOut("int8"), "dominantType",
                  "dominantType", 75);
  sf_mex_addfield(*c11_info, c11_emlrt_marshallOut(
    "[ILXE]$matlabroot$/toolbox/eml/lib/matlab/eml/eml_scalar_eg.m"), "resolved",
                  "resolved", 75);
  sf_mex_addfield(*c11_info, c11_b_emlrt_marshallOut(1375984288U), "fileTimeLo",
                  "fileTimeLo", 75);
  sf_mex_addfield(*c11_info, c11_b_emlrt_marshallOut(0U), "fileTimeHi",
                  "fileTimeHi", 75);
  sf_mex_addfield(*c11_info, c11_b_emlrt_marshallOut(0U), "mFileTimeLo",
                  "mFileTimeLo", 75);
  sf_mex_addfield(*c11_info, c11_b_emlrt_marshallOut(0U), "mFileTimeHi",
                  "mFileTimeHi", 75);
  sf_mex_assign(&c11_rhs75, sf_mex_createcellmatrix(0, 1), false);
  sf_mex_assign(&c11_lhs75, sf_mex_createcellmatrix(0, 1), false);
  sf_mex_addfield(*c11_info, sf_mex_duplicatearraysafe(&c11_rhs75), "rhs", "rhs",
                  75);
  sf_mex_addfield(*c11_info, sf_mex_duplicatearraysafe(&c11_lhs75), "lhs", "lhs",
                  75);
  sf_mex_addfield(*c11_info, c11_emlrt_marshallOut(
    "[ILXE]$matlabroot$/toolbox/eml/lib/matlab/eml/eml_scalar_eg.m"), "context",
                  "context", 76);
  sf_mex_addfield(*c11_info, c11_emlrt_marshallOut("coder.internal.scalarEg"),
                  "name", "name", 76);
  sf_mex_addfield(*c11_info, c11_emlrt_marshallOut("int8"), "dominantType",
                  "dominantType", 76);
  sf_mex_addfield(*c11_info, c11_emlrt_marshallOut(
    "[ILXE]$matlabroot$/toolbox/coder/coder/+coder/+internal/scalarEg.p"),
                  "resolved", "resolved", 76);
  sf_mex_addfield(*c11_info, c11_b_emlrt_marshallOut(1410811370U), "fileTimeLo",
                  "fileTimeLo", 76);
  sf_mex_addfield(*c11_info, c11_b_emlrt_marshallOut(0U), "fileTimeHi",
                  "fileTimeHi", 76);
  sf_mex_addfield(*c11_info, c11_b_emlrt_marshallOut(0U), "mFileTimeLo",
                  "mFileTimeLo", 76);
  sf_mex_addfield(*c11_info, c11_b_emlrt_marshallOut(0U), "mFileTimeHi",
                  "mFileTimeHi", 76);
  sf_mex_assign(&c11_rhs76, sf_mex_createcellmatrix(0, 1), false);
  sf_mex_assign(&c11_lhs76, sf_mex_createcellmatrix(0, 1), false);
  sf_mex_addfield(*c11_info, sf_mex_duplicatearraysafe(&c11_rhs76), "rhs", "rhs",
                  76);
  sf_mex_addfield(*c11_info, sf_mex_duplicatearraysafe(&c11_lhs76), "lhs", "lhs",
                  76);
  sf_mex_addfield(*c11_info, c11_emlrt_marshallOut(
    "[ILXE]$matlabroot$/toolbox/eml/lib/matlab/eml/eml_min_or_max.m!eml_extremum"),
                  "context", "context", 77);
  sf_mex_addfield(*c11_info, c11_emlrt_marshallOut("eml_index_class"), "name",
                  "name", 77);
  sf_mex_addfield(*c11_info, c11_emlrt_marshallOut(""), "dominantType",
                  "dominantType", 77);
  sf_mex_addfield(*c11_info, c11_emlrt_marshallOut(
    "[ILXE]$matlabroot$/toolbox/eml/lib/matlab/eml/eml_index_class.m"),
                  "resolved", "resolved", 77);
  sf_mex_addfield(*c11_info, c11_b_emlrt_marshallOut(1323174178U), "fileTimeLo",
                  "fileTimeLo", 77);
  sf_mex_addfield(*c11_info, c11_b_emlrt_marshallOut(0U), "fileTimeHi",
                  "fileTimeHi", 77);
  sf_mex_addfield(*c11_info, c11_b_emlrt_marshallOut(0U), "mFileTimeLo",
                  "mFileTimeLo", 77);
  sf_mex_addfield(*c11_info, c11_b_emlrt_marshallOut(0U), "mFileTimeHi",
                  "mFileTimeHi", 77);
  sf_mex_assign(&c11_rhs77, sf_mex_createcellmatrix(0, 1), false);
  sf_mex_assign(&c11_lhs77, sf_mex_createcellmatrix(0, 1), false);
  sf_mex_addfield(*c11_info, sf_mex_duplicatearraysafe(&c11_rhs77), "rhs", "rhs",
                  77);
  sf_mex_addfield(*c11_info, sf_mex_duplicatearraysafe(&c11_lhs77), "lhs", "lhs",
                  77);
  sf_mex_addfield(*c11_info, c11_emlrt_marshallOut(
    "[ILXE]$matlabroot$/toolbox/eml/lib/matlab/eml/eml_min_or_max.m!eml_extremum_sub"),
                  "context", "context", 78);
  sf_mex_addfield(*c11_info, c11_emlrt_marshallOut("eml_index_class"), "name",
                  "name", 78);
  sf_mex_addfield(*c11_info, c11_emlrt_marshallOut(""), "dominantType",
                  "dominantType", 78);
  sf_mex_addfield(*c11_info, c11_emlrt_marshallOut(
    "[ILXE]$matlabroot$/toolbox/eml/lib/matlab/eml/eml_index_class.m"),
                  "resolved", "resolved", 78);
  sf_mex_addfield(*c11_info, c11_b_emlrt_marshallOut(1323174178U), "fileTimeLo",
                  "fileTimeLo", 78);
  sf_mex_addfield(*c11_info, c11_b_emlrt_marshallOut(0U), "fileTimeHi",
                  "fileTimeHi", 78);
  sf_mex_addfield(*c11_info, c11_b_emlrt_marshallOut(0U), "mFileTimeLo",
                  "mFileTimeLo", 78);
  sf_mex_addfield(*c11_info, c11_b_emlrt_marshallOut(0U), "mFileTimeHi",
                  "mFileTimeHi", 78);
  sf_mex_assign(&c11_rhs78, sf_mex_createcellmatrix(0, 1), false);
  sf_mex_assign(&c11_lhs78, sf_mex_createcellmatrix(0, 1), false);
  sf_mex_addfield(*c11_info, sf_mex_duplicatearraysafe(&c11_rhs78), "rhs", "rhs",
                  78);
  sf_mex_addfield(*c11_info, sf_mex_duplicatearraysafe(&c11_lhs78), "lhs", "lhs",
                  78);
  sf_mex_addfield(*c11_info, c11_emlrt_marshallOut(
    "[ILXE]$matlabroot$/toolbox/eml/lib/matlab/eml/eml_min_or_max.m!eml_extremum_sub"),
                  "context", "context", 79);
  sf_mex_addfield(*c11_info, c11_emlrt_marshallOut("isnan"), "name", "name", 79);
  sf_mex_addfield(*c11_info, c11_emlrt_marshallOut("int8"), "dominantType",
                  "dominantType", 79);
  sf_mex_addfield(*c11_info, c11_emlrt_marshallOut(
    "[ILXE]$matlabroot$/toolbox/eml/lib/matlab/elmat/isnan.m"), "resolved",
                  "resolved", 79);
  sf_mex_addfield(*c11_info, c11_b_emlrt_marshallOut(1363717458U), "fileTimeLo",
                  "fileTimeLo", 79);
  sf_mex_addfield(*c11_info, c11_b_emlrt_marshallOut(0U), "fileTimeHi",
                  "fileTimeHi", 79);
  sf_mex_addfield(*c11_info, c11_b_emlrt_marshallOut(0U), "mFileTimeLo",
                  "mFileTimeLo", 79);
  sf_mex_addfield(*c11_info, c11_b_emlrt_marshallOut(0U), "mFileTimeHi",
                  "mFileTimeHi", 79);
  sf_mex_assign(&c11_rhs79, sf_mex_createcellmatrix(0, 1), false);
  sf_mex_assign(&c11_lhs79, sf_mex_createcellmatrix(0, 1), false);
  sf_mex_addfield(*c11_info, sf_mex_duplicatearraysafe(&c11_rhs79), "rhs", "rhs",
                  79);
  sf_mex_addfield(*c11_info, sf_mex_duplicatearraysafe(&c11_lhs79), "lhs", "lhs",
                  79);
  sf_mex_addfield(*c11_info, c11_emlrt_marshallOut(
    "[ILXE]$matlabroot$/toolbox/eml/lib/matlab/elmat/isnan.m"), "context",
                  "context", 80);
  sf_mex_addfield(*c11_info, c11_emlrt_marshallOut(
    "coder.internal.isBuiltInNumeric"), "name", "name", 80);
  sf_mex_addfield(*c11_info, c11_emlrt_marshallOut("int8"), "dominantType",
                  "dominantType", 80);
  sf_mex_addfield(*c11_info, c11_emlrt_marshallOut(
    "[ILXE]$matlabroot$/toolbox/shared/coder/coder/+coder/+internal/isBuiltInNumeric.m"),
                  "resolved", "resolved", 80);
  sf_mex_addfield(*c11_info, c11_b_emlrt_marshallOut(1395935456U), "fileTimeLo",
                  "fileTimeLo", 80);
  sf_mex_addfield(*c11_info, c11_b_emlrt_marshallOut(0U), "fileTimeHi",
                  "fileTimeHi", 80);
  sf_mex_addfield(*c11_info, c11_b_emlrt_marshallOut(0U), "mFileTimeLo",
                  "mFileTimeLo", 80);
  sf_mex_addfield(*c11_info, c11_b_emlrt_marshallOut(0U), "mFileTimeHi",
                  "mFileTimeHi", 80);
  sf_mex_assign(&c11_rhs80, sf_mex_createcellmatrix(0, 1), false);
  sf_mex_assign(&c11_lhs80, sf_mex_createcellmatrix(0, 1), false);
  sf_mex_addfield(*c11_info, sf_mex_duplicatearraysafe(&c11_rhs80), "rhs", "rhs",
                  80);
  sf_mex_addfield(*c11_info, sf_mex_duplicatearraysafe(&c11_lhs80), "lhs", "lhs",
                  80);
  sf_mex_addfield(*c11_info, c11_emlrt_marshallOut(
    "[ILXE]$matlabroot$/toolbox/eml/lib/matlab/eml/eml_min_or_max.m!eml_extremum_sub"),
                  "context", "context", 81);
  sf_mex_addfield(*c11_info, c11_emlrt_marshallOut("eml_index_plus"), "name",
                  "name", 81);
  sf_mex_addfield(*c11_info, c11_emlrt_marshallOut("coder.internal.indexInt"),
                  "dominantType", "dominantType", 81);
  sf_mex_addfield(*c11_info, c11_emlrt_marshallOut(
    "[ILXE]$matlabroot$/toolbox/eml/lib/matlab/eml/eml_index_plus.m"),
                  "resolved", "resolved", 81);
  sf_mex_addfield(*c11_info, c11_b_emlrt_marshallOut(1372586016U), "fileTimeLo",
                  "fileTimeLo", 81);
  sf_mex_addfield(*c11_info, c11_b_emlrt_marshallOut(0U), "fileTimeHi",
                  "fileTimeHi", 81);
  sf_mex_addfield(*c11_info, c11_b_emlrt_marshallOut(0U), "mFileTimeLo",
                  "mFileTimeLo", 81);
  sf_mex_addfield(*c11_info, c11_b_emlrt_marshallOut(0U), "mFileTimeHi",
                  "mFileTimeHi", 81);
  sf_mex_assign(&c11_rhs81, sf_mex_createcellmatrix(0, 1), false);
  sf_mex_assign(&c11_lhs81, sf_mex_createcellmatrix(0, 1), false);
  sf_mex_addfield(*c11_info, sf_mex_duplicatearraysafe(&c11_rhs81), "rhs", "rhs",
                  81);
  sf_mex_addfield(*c11_info, sf_mex_duplicatearraysafe(&c11_lhs81), "lhs", "lhs",
                  81);
  sf_mex_addfield(*c11_info, c11_emlrt_marshallOut(
    "[ILXE]$matlabroot$/toolbox/eml/lib/matlab/eml/eml_index_plus.m"), "context",
                  "context", 82);
  sf_mex_addfield(*c11_info, c11_emlrt_marshallOut("coder.internal.indexPlus"),
                  "name", "name", 82);
  sf_mex_addfield(*c11_info, c11_emlrt_marshallOut("coder.internal.indexInt"),
                  "dominantType", "dominantType", 82);
  sf_mex_addfield(*c11_info, c11_emlrt_marshallOut(
    "[ILXE]$matlabroot$/toolbox/shared/coder/coder/+coder/+internal/indexPlus.m"),
                  "resolved", "resolved", 82);
  sf_mex_addfield(*c11_info, c11_b_emlrt_marshallOut(1372586760U), "fileTimeLo",
                  "fileTimeLo", 82);
  sf_mex_addfield(*c11_info, c11_b_emlrt_marshallOut(0U), "fileTimeHi",
                  "fileTimeHi", 82);
  sf_mex_addfield(*c11_info, c11_b_emlrt_marshallOut(0U), "mFileTimeLo",
                  "mFileTimeLo", 82);
  sf_mex_addfield(*c11_info, c11_b_emlrt_marshallOut(0U), "mFileTimeHi",
                  "mFileTimeHi", 82);
  sf_mex_assign(&c11_rhs82, sf_mex_createcellmatrix(0, 1), false);
  sf_mex_assign(&c11_lhs82, sf_mex_createcellmatrix(0, 1), false);
  sf_mex_addfield(*c11_info, sf_mex_duplicatearraysafe(&c11_rhs82), "rhs", "rhs",
                  82);
  sf_mex_addfield(*c11_info, sf_mex_duplicatearraysafe(&c11_lhs82), "lhs", "lhs",
                  82);
  sf_mex_addfield(*c11_info, c11_emlrt_marshallOut(
    "[ILXE]$matlabroot$/toolbox/eml/lib/matlab/eml/eml_min_or_max.m!eml_extremum_sub"),
                  "context", "context", 83);
  sf_mex_addfield(*c11_info, c11_emlrt_marshallOut(
    "eml_int_forloop_overflow_check"), "name", "name", 83);
  sf_mex_addfield(*c11_info, c11_emlrt_marshallOut(""), "dominantType",
                  "dominantType", 83);
  sf_mex_addfield(*c11_info, c11_emlrt_marshallOut(
    "[ILXE]$matlabroot$/toolbox/eml/lib/matlab/eml/eml_int_forloop_overflow_check.m"),
                  "resolved", "resolved", 83);
  sf_mex_addfield(*c11_info, c11_b_emlrt_marshallOut(1397261022U), "fileTimeLo",
                  "fileTimeLo", 83);
  sf_mex_addfield(*c11_info, c11_b_emlrt_marshallOut(0U), "fileTimeHi",
                  "fileTimeHi", 83);
  sf_mex_addfield(*c11_info, c11_b_emlrt_marshallOut(0U), "mFileTimeLo",
                  "mFileTimeLo", 83);
  sf_mex_addfield(*c11_info, c11_b_emlrt_marshallOut(0U), "mFileTimeHi",
                  "mFileTimeHi", 83);
  sf_mex_assign(&c11_rhs83, sf_mex_createcellmatrix(0, 1), false);
  sf_mex_assign(&c11_lhs83, sf_mex_createcellmatrix(0, 1), false);
  sf_mex_addfield(*c11_info, sf_mex_duplicatearraysafe(&c11_rhs83), "rhs", "rhs",
                  83);
  sf_mex_addfield(*c11_info, sf_mex_duplicatearraysafe(&c11_lhs83), "lhs", "lhs",
                  83);
  sf_mex_addfield(*c11_info, c11_emlrt_marshallOut(
    "[ILXE]$matlabroot$/toolbox/eml/lib/matlab/eml/eml_min_or_max.m!eml_extremum_sub"),
                  "context", "context", 84);
  sf_mex_addfield(*c11_info, c11_emlrt_marshallOut("eml_relop"), "name", "name",
                  84);
  sf_mex_addfield(*c11_info, c11_emlrt_marshallOut("function_handle"),
                  "dominantType", "dominantType", 84);
  sf_mex_addfield(*c11_info, c11_emlrt_marshallOut(
    "[ILXE]$matlabroot$/toolbox/eml/lib/matlab/eml/eml_relop.m"), "resolved",
                  "resolved", 84);
  sf_mex_addfield(*c11_info, c11_b_emlrt_marshallOut(1342454782U), "fileTimeLo",
                  "fileTimeLo", 84);
  sf_mex_addfield(*c11_info, c11_b_emlrt_marshallOut(0U), "fileTimeHi",
                  "fileTimeHi", 84);
  sf_mex_addfield(*c11_info, c11_b_emlrt_marshallOut(0U), "mFileTimeLo",
                  "mFileTimeLo", 84);
  sf_mex_addfield(*c11_info, c11_b_emlrt_marshallOut(0U), "mFileTimeHi",
                  "mFileTimeHi", 84);
  sf_mex_assign(&c11_rhs84, sf_mex_createcellmatrix(0, 1), false);
  sf_mex_assign(&c11_lhs84, sf_mex_createcellmatrix(0, 1), false);
  sf_mex_addfield(*c11_info, sf_mex_duplicatearraysafe(&c11_rhs84), "rhs", "rhs",
                  84);
  sf_mex_addfield(*c11_info, sf_mex_duplicatearraysafe(&c11_lhs84), "lhs", "lhs",
                  84);
  sf_mex_addfield(*c11_info, c11_emlrt_marshallOut(""), "context", "context", 85);
  sf_mex_addfield(*c11_info, c11_emlrt_marshallOut("max"), "name", "name", 85);
  sf_mex_addfield(*c11_info, c11_emlrt_marshallOut("int8"), "dominantType",
                  "dominantType", 85);
  sf_mex_addfield(*c11_info, c11_emlrt_marshallOut(
    "[ILXE]$matlabroot$/toolbox/eml/lib/matlab/datafun/max.m"), "resolved",
                  "resolved", 85);
  sf_mex_addfield(*c11_info, c11_b_emlrt_marshallOut(1311258916U), "fileTimeLo",
                  "fileTimeLo", 85);
  sf_mex_addfield(*c11_info, c11_b_emlrt_marshallOut(0U), "fileTimeHi",
                  "fileTimeHi", 85);
  sf_mex_addfield(*c11_info, c11_b_emlrt_marshallOut(0U), "mFileTimeLo",
                  "mFileTimeLo", 85);
  sf_mex_addfield(*c11_info, c11_b_emlrt_marshallOut(0U), "mFileTimeHi",
                  "mFileTimeHi", 85);
  sf_mex_assign(&c11_rhs85, sf_mex_createcellmatrix(0, 1), false);
  sf_mex_assign(&c11_lhs85, sf_mex_createcellmatrix(0, 1), false);
  sf_mex_addfield(*c11_info, sf_mex_duplicatearraysafe(&c11_rhs85), "rhs", "rhs",
                  85);
  sf_mex_addfield(*c11_info, sf_mex_duplicatearraysafe(&c11_lhs85), "lhs", "lhs",
                  85);
  sf_mex_addfield(*c11_info, c11_emlrt_marshallOut(
    "[ILXE]$matlabroot$/toolbox/eml/lib/matlab/datafun/max.m"), "context",
                  "context", 86);
  sf_mex_addfield(*c11_info, c11_emlrt_marshallOut("eml_min_or_max"), "name",
                  "name", 86);
  sf_mex_addfield(*c11_info, c11_emlrt_marshallOut("int8"), "dominantType",
                  "dominantType", 86);
  sf_mex_addfield(*c11_info, c11_emlrt_marshallOut(
    "[ILXE]$matlabroot$/toolbox/eml/lib/matlab/eml/eml_min_or_max.m"),
                  "resolved", "resolved", 86);
  sf_mex_addfield(*c11_info, c11_b_emlrt_marshallOut(1378299584U), "fileTimeLo",
                  "fileTimeLo", 86);
  sf_mex_addfield(*c11_info, c11_b_emlrt_marshallOut(0U), "fileTimeHi",
                  "fileTimeHi", 86);
  sf_mex_addfield(*c11_info, c11_b_emlrt_marshallOut(0U), "mFileTimeLo",
                  "mFileTimeLo", 86);
  sf_mex_addfield(*c11_info, c11_b_emlrt_marshallOut(0U), "mFileTimeHi",
                  "mFileTimeHi", 86);
  sf_mex_assign(&c11_rhs86, sf_mex_createcellmatrix(0, 1), false);
  sf_mex_assign(&c11_lhs86, sf_mex_createcellmatrix(0, 1), false);
  sf_mex_addfield(*c11_info, sf_mex_duplicatearraysafe(&c11_rhs86), "rhs", "rhs",
                  86);
  sf_mex_addfield(*c11_info, sf_mex_duplicatearraysafe(&c11_lhs86), "lhs", "lhs",
                  86);
  sf_mex_destroy(&c11_rhs64);
  sf_mex_destroy(&c11_lhs64);
  sf_mex_destroy(&c11_rhs65);
  sf_mex_destroy(&c11_lhs65);
  sf_mex_destroy(&c11_rhs66);
  sf_mex_destroy(&c11_lhs66);
  sf_mex_destroy(&c11_rhs67);
  sf_mex_destroy(&c11_lhs67);
  sf_mex_destroy(&c11_rhs68);
  sf_mex_destroy(&c11_lhs68);
  sf_mex_destroy(&c11_rhs69);
  sf_mex_destroy(&c11_lhs69);
  sf_mex_destroy(&c11_rhs70);
  sf_mex_destroy(&c11_lhs70);
  sf_mex_destroy(&c11_rhs71);
  sf_mex_destroy(&c11_lhs71);
  sf_mex_destroy(&c11_rhs72);
  sf_mex_destroy(&c11_lhs72);
  sf_mex_destroy(&c11_rhs73);
  sf_mex_destroy(&c11_lhs73);
  sf_mex_destroy(&c11_rhs74);
  sf_mex_destroy(&c11_lhs74);
  sf_mex_destroy(&c11_rhs75);
  sf_mex_destroy(&c11_lhs75);
  sf_mex_destroy(&c11_rhs76);
  sf_mex_destroy(&c11_lhs76);
  sf_mex_destroy(&c11_rhs77);
  sf_mex_destroy(&c11_lhs77);
  sf_mex_destroy(&c11_rhs78);
  sf_mex_destroy(&c11_lhs78);
  sf_mex_destroy(&c11_rhs79);
  sf_mex_destroy(&c11_lhs79);
  sf_mex_destroy(&c11_rhs80);
  sf_mex_destroy(&c11_lhs80);
  sf_mex_destroy(&c11_rhs81);
  sf_mex_destroy(&c11_lhs81);
  sf_mex_destroy(&c11_rhs82);
  sf_mex_destroy(&c11_lhs82);
  sf_mex_destroy(&c11_rhs83);
  sf_mex_destroy(&c11_lhs83);
  sf_mex_destroy(&c11_rhs84);
  sf_mex_destroy(&c11_lhs84);
  sf_mex_destroy(&c11_rhs85);
  sf_mex_destroy(&c11_lhs85);
  sf_mex_destroy(&c11_rhs86);
  sf_mex_destroy(&c11_lhs86);
}

static void c11_normCollisionFinder(SFc11_LessonIIIInstanceStruct *chartInstance)
{
  uint32_T c11_debug_family_var_map[11];
  real32_T c11_diffVec[2];
  int8_T c11_rectangle[8];
  real32_T c11_angle;
  real32_T c11_R[4];
  int8_T c11_minx;
  int8_T c11_maxx;
  int8_T c11_miny;
  int8_T c11_maxy;
  real_T c11_ii;
  real_T c11_nargin = 0.0;
  real_T c11_nargout = 0.0;
  int8_T c11_iv0[2];
  int8_T c11_iv1[2];
  int32_T c11_i26;
  int32_T c11_i27;
  int32_T c11_i28;
  real32_T c11_b_diffVec[2];
  real_T c11_d1;
  int32_T c11_i29;
  static int8_T c11_iv2[8] = { 4, -3, 13, -3, 13, 3, 4, 3 };

  real32_T c11_f1;
  real32_T c11_f2;
  real32_T c11_f3;
  real32_T c11_f4;
  int32_T c11_i30;
  real32_T c11_a[4];
  int32_T c11_i31;
  int32_T c11_i32;
  int32_T c11_i33;
  real32_T c11_y[8];
  int32_T c11_i34;
  int32_T c11_i35;
  static real32_T c11_b[8] = { 4.0F, -3.0F, 13.0F, -3.0F, 13.0F, 3.0F, 4.0F,
    3.0F };

  int8_T c11_iv3[8];
  int32_T c11_i36;
  int32_T c11_i37;
  int32_T c11_i38;
  real32_T c11_f5;
  int8_T c11_i39;
  int32_T c11_i40;
  int32_T c11_i41;
  int32_T c11_i42;
  int8_T c11_varargin_1[4];
  int8_T c11_mtmp;
  int32_T c11_ix;
  int32_T c11_b_ix;
  int8_T c11_b_a;
  int8_T c11_b_b;
  boolean_T c11_p;
  int8_T c11_b_mtmp;
  int32_T c11_i43;
  int32_T c11_i44;
  int8_T c11_c_mtmp;
  int32_T c11_c_ix;
  int32_T c11_d_ix;
  int8_T c11_c_a;
  int8_T c11_c_b;
  boolean_T c11_b_p;
  int8_T c11_d_mtmp;
  int32_T c11_i45;
  int32_T c11_i46;
  int8_T c11_e_mtmp;
  int32_T c11_e_ix;
  int32_T c11_f_ix;
  int8_T c11_d_a;
  int8_T c11_d_b;
  boolean_T c11_c_p;
  int8_T c11_f_mtmp;
  int32_T c11_i47;
  int32_T c11_i48;
  int8_T c11_g_mtmp;
  int32_T c11_g_ix;
  int32_T c11_h_ix;
  int8_T c11_e_a;
  int8_T c11_e_b;
  boolean_T c11_d_p;
  int8_T c11_h_mtmp;
  int32_T c11_b_ii;
  int8_T c11_i49;
  int8_T c11_i50;
  int8_T c11_i51;
  int8_T c11_i52;
  uint8_T c11_hoistedGlobal;
  uint8_T c11_u;
  const mxArray *c11_b_y = NULL;
  uint8_T c11_b_hoistedGlobal;
  uint8_T c11_b_u;
  const mxArray *c11_c_y = NULL;
  boolean_T guard1 = false;
  int32_T exitg1;
  boolean_T guard11 = false;
  boolean_T guard2 = false;
  boolean_T guard3 = false;
  _SFD_SYMBOL_SCOPE_PUSH_EML(0U, 11U, 11U, c11_b_debug_family_names,
    c11_debug_family_var_map);
  _SFD_SYMBOL_SCOPE_ADD_EML_IMPORTABLE(c11_diffVec, 0U, c11_f_sf_marshallOut,
    c11_e_sf_marshallIn);
  _SFD_SYMBOL_SCOPE_ADD_EML(c11_rectangle, 1U, c11_h_sf_marshallOut);
  _SFD_SYMBOL_SCOPE_ADD_EML_IMPORTABLE(&c11_angle, 2U, c11_d_sf_marshallOut,
    c11_d_sf_marshallIn);
  _SFD_SYMBOL_SCOPE_ADD_EML_IMPORTABLE(c11_R, 3U, c11_c_sf_marshallOut,
    c11_c_sf_marshallIn);
  _SFD_SYMBOL_SCOPE_ADD_EML_IMPORTABLE(&c11_minx, 4U, c11_g_sf_marshallOut,
    c11_f_sf_marshallIn);
  _SFD_SYMBOL_SCOPE_ADD_EML_IMPORTABLE(&c11_maxx, 5U, c11_g_sf_marshallOut,
    c11_f_sf_marshallIn);
  _SFD_SYMBOL_SCOPE_ADD_EML_IMPORTABLE(&c11_miny, 6U, c11_g_sf_marshallOut,
    c11_f_sf_marshallIn);
  _SFD_SYMBOL_SCOPE_ADD_EML_IMPORTABLE(&c11_maxy, 7U, c11_g_sf_marshallOut,
    c11_f_sf_marshallIn);
  _SFD_SYMBOL_SCOPE_ADD_EML_IMPORTABLE(&c11_ii, 8U, c11_sf_marshallOut,
    c11_sf_marshallIn);
  _SFD_SYMBOL_SCOPE_ADD_EML_IMPORTABLE(&c11_nargin, 9U, c11_sf_marshallOut,
    c11_sf_marshallIn);
  _SFD_SYMBOL_SCOPE_ADD_EML_IMPORTABLE(&c11_nargout, 10U, c11_sf_marshallOut,
    c11_sf_marshallIn);
  CV_EML_FCN(4, 0);
  _SFD_EML_CALL(4U, chartInstance->c11_sfEvent, 2);
  chartInstance->c11_collisionDetected = 0U;
  c11_updateDataWrittenToVector(chartInstance, 2U);
  _SFD_EML_CALL(4U, chartInstance->c11_sfEvent, 4);
  c11_errorIfDataNotWrittenToFcn(chartInstance, 0U, 2U, 66U, 74, 10);
  c11_iv0[0] = *(int8_T *)&((char_T *)chartInstance->c11_finalWay)[0];
  c11_iv0[1] = *(int8_T *)&((char_T *)chartInstance->c11_finalWay)[1];
  c11_iv1[0] = *(int8_T *)&((char_T *)chartInstance->c11_me)[0];
  c11_iv1[1] = *(int8_T *)&((char_T *)chartInstance->c11_me)[1];
  for (c11_i26 = 0; c11_i26 < 2; c11_i26++) {
    c11_i27 = c11_iv0[c11_i26] - c11_iv1[c11_i26];
    if (c11_i27 > 127) {
      CV_SATURATION_EVAL(4, 4, 2, 0, 1);
      c11_i27 = 127;
    } else {
      if (CV_SATURATION_EVAL(4, 4, 2, 0, c11_i27 < -128)) {
        c11_i27 = -128;
      }
    }

    c11_diffVec[c11_i26] = (real32_T)(int8_T)c11_i27;
  }

  c11_updateDataWrittenToVector(chartInstance, 0U);
  _SFD_EML_CALL(4U, chartInstance->c11_sfEvent, 5);
  for (c11_i28 = 0; c11_i28 < 2; c11_i28++) {
    c11_b_diffVec[c11_i28] = c11_diffVec[c11_i28];
  }

  c11_d1 = c11_norm(chartInstance, c11_b_diffVec);
  guard1 = false;
  if (CV_EML_IF(4, 1, 0, CV_RELATIONAL_EVAL(4U, 4U, 0, c11_d1, 4.0, -1, 4U,
        c11_d1 > 4.0))) {
    _SFD_EML_CALL(4U, chartInstance->c11_sfEvent, 6);
    for (c11_i29 = 0; c11_i29 < 8; c11_i29++) {
      c11_rectangle[c11_i29] = c11_iv2[c11_i29];
    }

    _SFD_EML_CALL(4U, chartInstance->c11_sfEvent, 7);
    c11_angle = c11_atan2d(chartInstance, c11_diffVec[1], c11_diffVec[0]);
    _SFD_EML_CALL(4U, chartInstance->c11_sfEvent, 8);
    c11_f1 = c11_angle;
    c11_b_cosd(chartInstance, &c11_f1);
    c11_f2 = c11_angle;
    c11_b_sind(chartInstance, &c11_f2);
    c11_f3 = c11_angle;
    c11_b_cosd(chartInstance, &c11_f3);
    c11_f4 = c11_angle;
    c11_b_sind(chartInstance, &c11_f4);
    c11_R[0] = c11_f1;
    c11_R[2] = -c11_f2;
    c11_R[1] = c11_f3;
    c11_R[3] = c11_f4;
    _SFD_EML_CALL(4U, chartInstance->c11_sfEvent, 9);
    for (c11_i30 = 0; c11_i30 < 4; c11_i30++) {
      c11_a[c11_i30] = c11_R[c11_i30];
    }

    c11_b_eml_scalar_eg(chartInstance);
    c11_b_eml_scalar_eg(chartInstance);
    c11_threshold(chartInstance);
    for (c11_i31 = 0; c11_i31 < 2; c11_i31++) {
      c11_i32 = 0;
      for (c11_i33 = 0; c11_i33 < 4; c11_i33++) {
        c11_y[c11_i32 + c11_i31] = 0.0F;
        c11_i34 = 0;
        for (c11_i35 = 0; c11_i35 < 2; c11_i35++) {
          c11_y[c11_i32 + c11_i31] += c11_a[c11_i34 + c11_i31] * c11_b[c11_i35 +
            c11_i32];
          c11_i34 += 2;
        }

        c11_i32 += 2;
      }
    }

    c11_iv3[0] = *(int8_T *)&((char_T *)chartInstance->c11_me)[0];
    c11_iv3[2] = *(int8_T *)&((char_T *)chartInstance->c11_me)[0];
    c11_iv3[4] = *(int8_T *)&((char_T *)chartInstance->c11_me)[0];
    c11_iv3[6] = *(int8_T *)&((char_T *)chartInstance->c11_me)[0];
    c11_iv3[1] = *(int8_T *)&((char_T *)chartInstance->c11_me)[1];
    c11_iv3[3] = *(int8_T *)&((char_T *)chartInstance->c11_me)[1];
    c11_iv3[5] = *(int8_T *)&((char_T *)chartInstance->c11_me)[1];
    c11_iv3[7] = *(int8_T *)&((char_T *)chartInstance->c11_me)[1];
    c11_i36 = 0;
    for (c11_i37 = 0; c11_i37 < 4; c11_i37++) {
      for (c11_i38 = 0; c11_i38 < 2; c11_i38++) {
        c11_f5 = muSingleScalarRound(c11_y[c11_i38 + c11_i36]);
        if (c11_f5 < 128.0F) {
          if (CV_SATURATION_EVAL(4, 4, 1, 1, c11_f5 >= -128.0F)) {
            c11_i39 = (int8_T)c11_f5;
          } else {
            c11_i39 = MIN_int8_T;
          }
        } else if (CV_SATURATION_EVAL(4, 4, 1, 0, c11_f5 >= 128.0F)) {
          c11_i39 = MAX_int8_T;
        } else {
          c11_i39 = 0;
        }

        c11_i40 = c11_iv3[c11_i38 + c11_i36] + c11_i39;
        if (c11_i40 > 127) {
          CV_SATURATION_EVAL(4, 4, 0, 0, 1);
          c11_i40 = 127;
        } else {
          if (CV_SATURATION_EVAL(4, 4, 0, 0, c11_i40 < -128)) {
            c11_i40 = -128;
          }
        }

        c11_rectangle[c11_i38 + c11_i36] = (int8_T)c11_i40;
      }

      c11_i36 += 2;
    }

    _SFD_EML_CALL(4U, chartInstance->c11_sfEvent, 10);
    c11_i41 = 0;
    for (c11_i42 = 0; c11_i42 < 4; c11_i42++) {
      c11_varargin_1[c11_i42] = c11_rectangle[c11_i41];
      c11_i41 += 2;
    }

    c11_mtmp = c11_varargin_1[0];
    c11_intmin(chartInstance);
    for (c11_ix = 2; c11_ix < 5; c11_ix++) {
      c11_b_ix = c11_ix;
      c11_b_a = c11_varargin_1[_SFD_EML_ARRAY_BOUNDS_CHECK("", (int32_T)
        _SFD_INTEGER_CHECK("", (real_T)c11_b_ix), 1, 4, 1, 0) - 1];
      c11_b_b = c11_mtmp;
      c11_p = (c11_b_a < c11_b_b);
      if (c11_p) {
        c11_mtmp = c11_varargin_1[_SFD_EML_ARRAY_BOUNDS_CHECK("", (int32_T)
          _SFD_INTEGER_CHECK("", (real_T)c11_b_ix), 1, 4, 1, 0) - 1];
      }
    }

    c11_b_mtmp = c11_mtmp;
    c11_minx = c11_b_mtmp;
    _SFD_EML_CALL(4U, chartInstance->c11_sfEvent, 11);
    c11_i43 = 0;
    for (c11_i44 = 0; c11_i44 < 4; c11_i44++) {
      c11_varargin_1[c11_i44] = c11_rectangle[c11_i43];
      c11_i43 += 2;
    }

    c11_c_mtmp = c11_varargin_1[0];
    c11_intmin(chartInstance);
    for (c11_c_ix = 2; c11_c_ix < 5; c11_c_ix++) {
      c11_d_ix = c11_c_ix;
      c11_c_a = c11_varargin_1[_SFD_EML_ARRAY_BOUNDS_CHECK("", (int32_T)
        _SFD_INTEGER_CHECK("", (real_T)c11_d_ix), 1, 4, 1, 0) - 1];
      c11_c_b = c11_c_mtmp;
      c11_b_p = (c11_c_a > c11_c_b);
      if (c11_b_p) {
        c11_c_mtmp = c11_varargin_1[_SFD_EML_ARRAY_BOUNDS_CHECK("", (int32_T)
          _SFD_INTEGER_CHECK("", (real_T)c11_d_ix), 1, 4, 1, 0) - 1];
      }
    }

    c11_d_mtmp = c11_c_mtmp;
    c11_maxx = c11_d_mtmp;
    _SFD_EML_CALL(4U, chartInstance->c11_sfEvent, 12);
    c11_i45 = 0;
    for (c11_i46 = 0; c11_i46 < 4; c11_i46++) {
      c11_varargin_1[c11_i46] = c11_rectangle[c11_i45 + 1];
      c11_i45 += 2;
    }

    c11_e_mtmp = c11_varargin_1[0];
    c11_intmin(chartInstance);
    for (c11_e_ix = 2; c11_e_ix < 5; c11_e_ix++) {
      c11_f_ix = c11_e_ix;
      c11_d_a = c11_varargin_1[_SFD_EML_ARRAY_BOUNDS_CHECK("", (int32_T)
        _SFD_INTEGER_CHECK("", (real_T)c11_f_ix), 1, 4, 1, 0) - 1];
      c11_d_b = c11_e_mtmp;
      c11_c_p = (c11_d_a < c11_d_b);
      if (c11_c_p) {
        c11_e_mtmp = c11_varargin_1[_SFD_EML_ARRAY_BOUNDS_CHECK("", (int32_T)
          _SFD_INTEGER_CHECK("", (real_T)c11_f_ix), 1, 4, 1, 0) - 1];
      }
    }

    c11_f_mtmp = c11_e_mtmp;
    c11_miny = c11_f_mtmp;
    _SFD_EML_CALL(4U, chartInstance->c11_sfEvent, 13);
    c11_i47 = 0;
    for (c11_i48 = 0; c11_i48 < 4; c11_i48++) {
      c11_varargin_1[c11_i48] = c11_rectangle[c11_i47 + 1];
      c11_i47 += 2;
    }

    c11_g_mtmp = c11_varargin_1[0];
    c11_intmin(chartInstance);
    for (c11_g_ix = 2; c11_g_ix < 5; c11_g_ix++) {
      c11_h_ix = c11_g_ix;
      c11_e_a = c11_varargin_1[_SFD_EML_ARRAY_BOUNDS_CHECK("", (int32_T)
        _SFD_INTEGER_CHECK("", (real_T)c11_h_ix), 1, 4, 1, 0) - 1];
      c11_e_b = c11_g_mtmp;
      c11_d_p = (c11_e_a > c11_e_b);
      if (c11_d_p) {
        c11_g_mtmp = c11_varargin_1[_SFD_EML_ARRAY_BOUNDS_CHECK("", (int32_T)
          _SFD_INTEGER_CHECK("", (real_T)c11_h_ix), 1, 4, 1, 0) - 1];
      }
    }

    c11_h_mtmp = c11_g_mtmp;
    c11_maxy = c11_h_mtmp;
    _SFD_EML_CALL(4U, chartInstance->c11_sfEvent, 14);
    c11_ii = 2.0;
    c11_b_ii = 0;
    do {
      exitg1 = 0;
      if (c11_b_ii < 5) {
        c11_ii = 2.0 + (real_T)c11_b_ii;
        CV_EML_FOR(4, 1, 0, 1);
        _SFD_EML_CALL(4U, chartInstance->c11_sfEvent, 15);
        c11_i49 = *(int8_T *)&((char_T *)(c11_Player *)&((char_T *)
          chartInstance->c11_players)[8 * (_SFD_EML_ARRAY_BOUNDS_CHECK("players",
                                 (int32_T)_SFD_INTEGER_CHECK("ii", c11_ii), 1, 6,
          1, 0) - 1)])[0];
        guard11 = false;
        guard2 = false;
        guard3 = false;
        if (CV_EML_COND(4, 1, 0, CV_RELATIONAL_EVAL(4U, 4U, 1, (real_T)c11_i49,
              (real_T)c11_minx, 0, 4U, c11_i49 > c11_minx))) {
          c11_i50 = *(int8_T *)&((char_T *)(c11_Player *)&((char_T *)
            chartInstance->c11_players)[8 * (_SFD_EML_ARRAY_BOUNDS_CHECK(
            "players", (int32_T)_SFD_INTEGER_CHECK("ii", c11_ii), 1, 6, 1, 0) -
            1)])[0];
          if (CV_EML_COND(4, 1, 1, CV_RELATIONAL_EVAL(4U, 4U, 2, (real_T)c11_i50,
                (real_T)c11_maxx, 0, 2U, c11_i50 < c11_maxx))) {
            c11_i51 = *(int8_T *)&((char_T *)(c11_Player *)&((char_T *)
              chartInstance->c11_players)[8 * (_SFD_EML_ARRAY_BOUNDS_CHECK(
              "players", (int32_T)_SFD_INTEGER_CHECK("ii", c11_ii), 1, 6, 1, 0)
              - 1)])[1];
            if (CV_EML_COND(4, 1, 2, CV_RELATIONAL_EVAL(4U, 4U, 3, (real_T)
                  c11_i51, (real_T)c11_miny, 0, 4U, c11_i51 > c11_miny))) {
              c11_i52 = *(int8_T *)&((char_T *)(c11_Player *)&((char_T *)
                chartInstance->c11_players)[8 * (_SFD_EML_ARRAY_BOUNDS_CHECK(
                "players", (int32_T)_SFD_INTEGER_CHECK("ii", c11_ii), 1, 6, 1, 0)
                - 1)])[1];
              if (CV_EML_COND(4, 1, 3, CV_RELATIONAL_EVAL(4U, 4U, 4, (real_T)
                    c11_i52, (real_T)c11_maxy, 0, 2U, c11_i52 < c11_maxy))) {
                CV_EML_MCDC(4, 1, 0, true);
                CV_EML_IF(4, 1, 1, true);
                _SFD_EML_CALL(4U, chartInstance->c11_sfEvent, 16);
                chartInstance->c11_collisionDetected = 1U;
                c11_updateDataWrittenToVector(chartInstance, 2U);
                c11_errorIfDataNotWrittenToFcn(chartInstance, 2U, 6U, 66U, 564,
                  17);
                sf_mex_printf("%s =\\n", "collisionDetected");
                c11_hoistedGlobal = chartInstance->c11_collisionDetected;
                c11_u = c11_hoistedGlobal;
                c11_b_y = NULL;
                sf_mex_assign(&c11_b_y, sf_mex_create("y", &c11_u, 3, 0U, 0U, 0U,
                  0), false);
                sf_mex_call_debug(sfGlobalDebugInstanceStruct, "disp", 0U, 1U,
                                  14, c11_b_y);
                _SFD_EML_CALL(4U, chartInstance->c11_sfEvent, 17);
                exitg1 = 1;
              } else {
                guard11 = true;
              }
            } else {
              guard2 = true;
            }
          } else {
            guard3 = true;
          }
        } else {
          guard3 = true;
        }

        if (guard3 == true) {
          guard2 = true;
        }

        if (guard2 == true) {
          guard11 = true;
        }

        if (guard11 == true) {
          CV_EML_MCDC(4, 1, 0, false);
          CV_EML_IF(4, 1, 1, false);
          c11_b_ii++;
          _SF_MEX_LISTEN_FOR_CTRL_C(chartInstance->S);
        }
      } else {
        CV_EML_FOR(4, 1, 0, 0);
        guard1 = true;
        exitg1 = 1;
      }
    } while (exitg1 == 0);
  } else {
    guard1 = true;
  }

  if (guard1 == true) {
    _SFD_EML_CALL(4U, chartInstance->c11_sfEvent, 21);
    c11_errorIfDataNotWrittenToFcn(chartInstance, 2U, 6U, 66U, 632, 17);
    sf_mex_printf("%s =\\n", "collisionDetected");
    c11_b_hoistedGlobal = chartInstance->c11_collisionDetected;
    c11_b_u = c11_b_hoistedGlobal;
    c11_c_y = NULL;
    sf_mex_assign(&c11_c_y, sf_mex_create("y", &c11_b_u, 3, 0U, 0U, 0U, 0),
                  false);
    sf_mex_call_debug(sfGlobalDebugInstanceStruct, "disp", 0U, 1U, 14, c11_c_y);
  }

  _SFD_EML_CALL(4U, chartInstance->c11_sfEvent, -21);
  _SFD_SYMBOL_SCOPE_POP();
}

static real32_T c11_atan2d(SFc11_LessonIIIInstanceStruct *chartInstance,
  real32_T c11_y, real32_T c11_x)
{
  real32_T c11_b_y;
  real32_T c11_b_x;
  real32_T c11_b_r;
  real32_T c11_b;
  c11_eml_scalar_eg(chartInstance);
  c11_b_y = c11_y;
  c11_b_x = c11_x;
  c11_b_r = muSingleScalarAtan2(c11_b_y, c11_b_x);
  c11_b = c11_b_r;
  return 57.2957802F * c11_b;
}

static void c11_eml_scalar_eg(SFc11_LessonIIIInstanceStruct *chartInstance)
{
  (void)chartInstance;
}

static real32_T c11_cosd(SFc11_LessonIIIInstanceStruct *chartInstance, real32_T
  c11_x)
{
  real32_T c11_b_x;
  c11_b_x = c11_x;
  c11_b_cosd(chartInstance, &c11_b_x);
  return c11_b_x;
}

static boolean_T c11_isfinite(SFc11_LessonIIIInstanceStruct *chartInstance,
  real32_T c11_x)
{
  real32_T c11_b_x;
  boolean_T c11_b_b;
  boolean_T c11_b1;
  real32_T c11_c_x;
  boolean_T c11_c_b;
  boolean_T c11_b2;
  (void)chartInstance;
  c11_b_x = c11_x;
  c11_b_b = muSingleScalarIsInf(c11_b_x);
  c11_b1 = !c11_b_b;
  c11_c_x = c11_x;
  c11_c_b = muSingleScalarIsNaN(c11_c_x);
  c11_b2 = !c11_c_b;
  return c11_b1 && c11_b2;
}

static real32_T c11_sind(SFc11_LessonIIIInstanceStruct *chartInstance, real32_T
  c11_x)
{
  real32_T c11_b_x;
  c11_b_x = c11_x;
  c11_b_sind(chartInstance, &c11_b_x);
  return c11_b_x;
}

static void c11_threshold(SFc11_LessonIIIInstanceStruct *chartInstance)
{
  (void)chartInstance;
}

static real32_T c11_norm(SFc11_LessonIIIInstanceStruct *chartInstance, real32_T
  c11_x[2])
{
  real32_T c11_y;
  real32_T c11_scale;
  int32_T c11_k;
  int32_T c11_b_k;
  real32_T c11_b_x;
  real32_T c11_c_x;
  real32_T c11_absxk;
  real32_T c11_t;
  c11_below_threshold(chartInstance);
  c11_y = 0.0F;
  c11_scale = 1.17549435E-38F;
  c11_intmin(chartInstance);
  for (c11_k = 1; c11_k < 3; c11_k++) {
    c11_b_k = c11_k;
    c11_b_x = c11_x[_SFD_EML_ARRAY_BOUNDS_CHECK("", (int32_T)_SFD_INTEGER_CHECK(
      "", (real_T)c11_b_k), 1, 2, 1, 0) - 1];
    c11_c_x = c11_b_x;
    c11_absxk = muSingleScalarAbs(c11_c_x);
    if (c11_absxk > c11_scale) {
      c11_t = c11_scale / c11_absxk;
      c11_y = 1.0F + c11_y * c11_t * c11_t;
      c11_scale = c11_absxk;
    } else {
      c11_t = c11_absxk / c11_scale;
      c11_y += c11_t * c11_t;
    }
  }

  return c11_scale * muSingleScalarSqrt(c11_y);
}

static void c11_below_threshold(SFc11_LessonIIIInstanceStruct *chartInstance)
{
  (void)chartInstance;
}

static int32_T c11_intmin(SFc11_LessonIIIInstanceStruct *chartInstance)
{
  (void)chartInstance;
  return MIN_int32_T;
}

static void c11_b_eml_scalar_eg(SFc11_LessonIIIInstanceStruct *chartInstance)
{
  (void)chartInstance;
}

static const mxArray *c11_j_sf_marshallOut(void *chartInstanceVoid, void
  *c11_inData)
{
  const mxArray *c11_mxArrayOutData = NULL;
  int32_T c11_u;
  const mxArray *c11_y = NULL;
  SFc11_LessonIIIInstanceStruct *chartInstance;
  chartInstance = (SFc11_LessonIIIInstanceStruct *)chartInstanceVoid;
  c11_mxArrayOutData = NULL;
  c11_u = *(int32_T *)c11_inData;
  c11_y = NULL;
  sf_mex_assign(&c11_y, sf_mex_create("y", &c11_u, 6, 0U, 0U, 0U, 0), false);
  sf_mex_assign(&c11_mxArrayOutData, c11_y, false);
  return c11_mxArrayOutData;
}

static int32_T c11_k_emlrt_marshallIn(SFc11_LessonIIIInstanceStruct
  *chartInstance, const mxArray *c11_u, const emlrtMsgIdentifier *c11_parentId)
{
  int32_T c11_y;
  int32_T c11_i53;
  (void)chartInstance;
  sf_mex_import(c11_parentId, sf_mex_dup(c11_u), &c11_i53, 1, 6, 0U, 0, 0U, 0);
  c11_y = c11_i53;
  sf_mex_destroy(&c11_u);
  return c11_y;
}

static void c11_h_sf_marshallIn(void *chartInstanceVoid, const mxArray
  *c11_mxArrayInData, const char_T *c11_varName, void *c11_outData)
{
  const mxArray *c11_b_sfEvent;
  const char_T *c11_identifier;
  emlrtMsgIdentifier c11_thisId;
  int32_T c11_y;
  SFc11_LessonIIIInstanceStruct *chartInstance;
  chartInstance = (SFc11_LessonIIIInstanceStruct *)chartInstanceVoid;
  c11_b_sfEvent = sf_mex_dup(c11_mxArrayInData);
  c11_identifier = c11_varName;
  c11_thisId.fIdentifier = c11_identifier;
  c11_thisId.fParent = NULL;
  c11_y = c11_k_emlrt_marshallIn(chartInstance, sf_mex_dup(c11_b_sfEvent),
    &c11_thisId);
  sf_mex_destroy(&c11_b_sfEvent);
  *(int32_T *)c11_outData = c11_y;
  sf_mex_destroy(&c11_mxArrayInData);
}

static const mxArray *c11_k_sf_marshallOut(void *chartInstanceVoid, void
  *c11_inData)
{
  const mxArray *c11_mxArrayOutData = NULL;
  uint8_T c11_u;
  const mxArray *c11_y = NULL;
  SFc11_LessonIIIInstanceStruct *chartInstance;
  chartInstance = (SFc11_LessonIIIInstanceStruct *)chartInstanceVoid;
  c11_mxArrayOutData = NULL;
  c11_u = *(uint8_T *)c11_inData;
  c11_y = NULL;
  sf_mex_assign(&c11_y, sf_mex_create("y", &c11_u, 3, 0U, 0U, 0U, 0), false);
  sf_mex_assign(&c11_mxArrayOutData, c11_y, false);
  return c11_mxArrayOutData;
}

static uint8_T c11_l_emlrt_marshallIn(SFc11_LessonIIIInstanceStruct
  *chartInstance, const mxArray *c11_b_tp_Idle, const char_T *c11_identifier)
{
  uint8_T c11_y;
  emlrtMsgIdentifier c11_thisId;
  c11_thisId.fIdentifier = c11_identifier;
  c11_thisId.fParent = NULL;
  c11_y = c11_m_emlrt_marshallIn(chartInstance, sf_mex_dup(c11_b_tp_Idle),
    &c11_thisId);
  sf_mex_destroy(&c11_b_tp_Idle);
  return c11_y;
}

static uint8_T c11_m_emlrt_marshallIn(SFc11_LessonIIIInstanceStruct
  *chartInstance, const mxArray *c11_u, const emlrtMsgIdentifier *c11_parentId)
{
  uint8_T c11_y;
  uint8_T c11_u0;
  (void)chartInstance;
  sf_mex_import(c11_parentId, sf_mex_dup(c11_u), &c11_u0, 1, 3, 0U, 0, 0U, 0);
  c11_y = c11_u0;
  sf_mex_destroy(&c11_u);
  return c11_y;
}

static void c11_i_sf_marshallIn(void *chartInstanceVoid, const mxArray
  *c11_mxArrayInData, const char_T *c11_varName, void *c11_outData)
{
  const mxArray *c11_b_tp_Idle;
  const char_T *c11_identifier;
  emlrtMsgIdentifier c11_thisId;
  uint8_T c11_y;
  SFc11_LessonIIIInstanceStruct *chartInstance;
  chartInstance = (SFc11_LessonIIIInstanceStruct *)chartInstanceVoid;
  c11_b_tp_Idle = sf_mex_dup(c11_mxArrayInData);
  c11_identifier = c11_varName;
  c11_thisId.fIdentifier = c11_identifier;
  c11_thisId.fParent = NULL;
  c11_y = c11_m_emlrt_marshallIn(chartInstance, sf_mex_dup(c11_b_tp_Idle),
    &c11_thisId);
  sf_mex_destroy(&c11_b_tp_Idle);
  *(uint8_T *)c11_outData = c11_y;
  sf_mex_destroy(&c11_mxArrayInData);
}

static const mxArray *c11_players_bus_io(void *chartInstanceVoid, void
  *c11_pData)
{
  const mxArray *c11_mxVal = NULL;
  int32_T c11_i54;
  int32_T c11_i55;
  c11_Player c11_tmp[6];
  SFc11_LessonIIIInstanceStruct *chartInstance;
  chartInstance = (SFc11_LessonIIIInstanceStruct *)chartInstanceVoid;
  c11_mxVal = NULL;
  for (c11_i54 = 0; c11_i54 < 6; c11_i54++) {
    for (c11_i55 = 0; c11_i55 < 1; c11_i55++) {
      c11_tmp[c11_i55 + c11_i54].x = *(int8_T *)&((char_T *)(c11_Player *)
        &((char_T *)(c11_Player (*)[6])c11_pData)[8 * (c11_i55 + c11_i54)])[0];
      c11_tmp[c11_i55 + c11_i54].y = *(int8_T *)&((char_T *)(c11_Player *)
        &((char_T *)(c11_Player (*)[6])c11_pData)[8 * (c11_i55 + c11_i54)])[1];
      c11_tmp[c11_i55 + c11_i54].orientation = *(int16_T *)&((char_T *)
        (c11_Player *)&((char_T *)(c11_Player (*)[6])c11_pData)[8 * (c11_i55 +
        c11_i54)])[2];
      c11_tmp[c11_i55 + c11_i54].color = *(uint8_T *)&((char_T *)(c11_Player *)
        &((char_T *)(c11_Player (*)[6])c11_pData)[8 * (c11_i55 + c11_i54)])[4];
      c11_tmp[c11_i55 + c11_i54].position = *(uint8_T *)&((char_T *)(c11_Player *)
        &((char_T *)(c11_Player (*)[6])c11_pData)[8 * (c11_i55 + c11_i54)])[5];
      c11_tmp[c11_i55 + c11_i54].valid = *(uint8_T *)&((char_T *)(c11_Player *)
        &((char_T *)(c11_Player (*)[6])c11_pData)[8 * (c11_i55 + c11_i54)])[6];
    }
  }

  sf_mex_assign(&c11_mxVal, c11_l_sf_marshallOut(chartInstance, c11_tmp), false);
  return c11_mxVal;
}

static const mxArray *c11_l_sf_marshallOut(void *chartInstanceVoid, void
  *c11_inData)
{
  const mxArray *c11_mxArrayOutData;
  int32_T c11_i56;
  c11_Player c11_b_inData[6];
  int32_T c11_i57;
  c11_Player c11_u[6];
  const mxArray *c11_y = NULL;
  int32_T c11_i58;
  int32_T c11_iv4[2];
  int32_T c11_i59;
  const c11_Player *c11_r1;
  int8_T c11_b_u;
  const mxArray *c11_b_y = NULL;
  int8_T c11_c_u;
  const mxArray *c11_c_y = NULL;
  int16_T c11_d_u;
  const mxArray *c11_d_y = NULL;
  uint8_T c11_e_u;
  const mxArray *c11_e_y = NULL;
  uint8_T c11_f_u;
  const mxArray *c11_f_y = NULL;
  uint8_T c11_g_u;
  const mxArray *c11_g_y = NULL;
  SFc11_LessonIIIInstanceStruct *chartInstance;
  chartInstance = (SFc11_LessonIIIInstanceStruct *)chartInstanceVoid;
  c11_mxArrayOutData = NULL;
  c11_mxArrayOutData = NULL;
  for (c11_i56 = 0; c11_i56 < 6; c11_i56++) {
    c11_b_inData[c11_i56] = (*(c11_Player (*)[6])c11_inData)[c11_i56];
  }

  for (c11_i57 = 0; c11_i57 < 6; c11_i57++) {
    c11_u[c11_i57] = c11_b_inData[c11_i57];
  }

  c11_y = NULL;
  for (c11_i58 = 0; c11_i58 < 2; c11_i58++) {
    c11_iv4[c11_i58] = 1 + 5 * c11_i58;
  }

  sf_mex_assign(&c11_y, sf_mex_createstructarray("structure", 2, c11_iv4), false);
  for (c11_i59 = 0; c11_i59 < 6; c11_i59++) {
    c11_r1 = &c11_u[c11_i59];
    c11_b_u = c11_r1->x;
    c11_b_y = NULL;
    sf_mex_assign(&c11_b_y, sf_mex_create("y", &c11_b_u, 2, 0U, 0U, 0U, 0),
                  false);
    sf_mex_addfield(c11_y, c11_b_y, "x", "x", c11_i59);
    c11_c_u = c11_r1->y;
    c11_c_y = NULL;
    sf_mex_assign(&c11_c_y, sf_mex_create("y", &c11_c_u, 2, 0U, 0U, 0U, 0),
                  false);
    sf_mex_addfield(c11_y, c11_c_y, "y", "y", c11_i59);
    c11_d_u = c11_r1->orientation;
    c11_d_y = NULL;
    sf_mex_assign(&c11_d_y, sf_mex_create("y", &c11_d_u, 4, 0U, 0U, 0U, 0),
                  false);
    sf_mex_addfield(c11_y, c11_d_y, "orientation", "orientation", c11_i59);
    c11_e_u = c11_r1->color;
    c11_e_y = NULL;
    sf_mex_assign(&c11_e_y, sf_mex_create("y", &c11_e_u, 3, 0U, 0U, 0U, 0),
                  false);
    sf_mex_addfield(c11_y, c11_e_y, "color", "color", c11_i59);
    c11_f_u = c11_r1->position;
    c11_f_y = NULL;
    sf_mex_assign(&c11_f_y, sf_mex_create("y", &c11_f_u, 3, 0U, 0U, 0U, 0),
                  false);
    sf_mex_addfield(c11_y, c11_f_y, "position", "position", c11_i59);
    c11_g_u = c11_r1->valid;
    c11_g_y = NULL;
    sf_mex_assign(&c11_g_y, sf_mex_create("y", &c11_g_u, 3, 0U, 0U, 0U, 0),
                  false);
    sf_mex_addfield(c11_y, c11_g_y, "valid", "valid", c11_i59);
  }

  sf_mex_assign(&c11_mxArrayOutData, c11_y, false);
  return c11_mxArrayOutData;
}

static const mxArray *c11_ball_bus_io(void *chartInstanceVoid, void *c11_pData)
{
  const mxArray *c11_mxVal = NULL;
  c11_Ball c11_tmp;
  SFc11_LessonIIIInstanceStruct *chartInstance;
  chartInstance = (SFc11_LessonIIIInstanceStruct *)chartInstanceVoid;
  c11_mxVal = NULL;
  c11_tmp.x = *(int8_T *)&((char_T *)(c11_Ball *)c11_pData)[0];
  c11_tmp.y = *(int8_T *)&((char_T *)(c11_Ball *)c11_pData)[1];
  c11_tmp.valid = *(uint8_T *)&((char_T *)(c11_Ball *)c11_pData)[2];
  sf_mex_assign(&c11_mxVal, c11_m_sf_marshallOut(chartInstance, &c11_tmp), false);
  return c11_mxVal;
}

static const mxArray *c11_m_sf_marshallOut(void *chartInstanceVoid, void
  *c11_inData)
{
  const mxArray *c11_mxArrayOutData = NULL;
  c11_Ball c11_u;
  const mxArray *c11_y = NULL;
  int8_T c11_b_u;
  const mxArray *c11_b_y = NULL;
  int8_T c11_c_u;
  const mxArray *c11_c_y = NULL;
  uint8_T c11_d_u;
  const mxArray *c11_d_y = NULL;
  SFc11_LessonIIIInstanceStruct *chartInstance;
  chartInstance = (SFc11_LessonIIIInstanceStruct *)chartInstanceVoid;
  c11_mxArrayOutData = NULL;
  c11_u = *(c11_Ball *)c11_inData;
  c11_y = NULL;
  sf_mex_assign(&c11_y, sf_mex_createstruct("structure", 2, 1, 1), false);
  c11_b_u = c11_u.x;
  c11_b_y = NULL;
  sf_mex_assign(&c11_b_y, sf_mex_create("y", &c11_b_u, 2, 0U, 0U, 0U, 0), false);
  sf_mex_addfield(c11_y, c11_b_y, "x", "x", 0);
  c11_c_u = c11_u.y;
  c11_c_y = NULL;
  sf_mex_assign(&c11_c_y, sf_mex_create("y", &c11_c_u, 2, 0U, 0U, 0U, 0), false);
  sf_mex_addfield(c11_y, c11_c_y, "y", "y", 0);
  c11_d_u = c11_u.valid;
  c11_d_y = NULL;
  sf_mex_assign(&c11_d_y, sf_mex_create("y", &c11_d_u, 3, 0U, 0U, 0U, 0), false);
  sf_mex_addfield(c11_y, c11_d_y, "valid", "valid", 0);
  sf_mex_assign(&c11_mxArrayOutData, c11_y, false);
  return c11_mxArrayOutData;
}

static const mxArray *c11_finalWay_bus_io(void *chartInstanceVoid, void
  *c11_pData)
{
  const mxArray *c11_mxVal = NULL;
  c11_Waypoint c11_tmp;
  SFc11_LessonIIIInstanceStruct *chartInstance;
  chartInstance = (SFc11_LessonIIIInstanceStruct *)chartInstanceVoid;
  c11_mxVal = NULL;
  c11_tmp.x = *(int8_T *)&((char_T *)(c11_Waypoint *)c11_pData)[0];
  c11_tmp.y = *(int8_T *)&((char_T *)(c11_Waypoint *)c11_pData)[1];
  c11_tmp.orientation = *(int16_T *)&((char_T *)(c11_Waypoint *)c11_pData)[2];
  sf_mex_assign(&c11_mxVal, c11_b_sf_marshallOut(chartInstance, &c11_tmp), false);
  return c11_mxVal;
}

static const mxArray *c11_me_bus_io(void *chartInstanceVoid, void *c11_pData)
{
  const mxArray *c11_mxVal = NULL;
  c11_Player c11_tmp;
  SFc11_LessonIIIInstanceStruct *chartInstance;
  chartInstance = (SFc11_LessonIIIInstanceStruct *)chartInstanceVoid;
  c11_mxVal = NULL;
  c11_tmp.x = *(int8_T *)&((char_T *)(c11_Player *)c11_pData)[0];
  c11_tmp.y = *(int8_T *)&((char_T *)(c11_Player *)c11_pData)[1];
  c11_tmp.orientation = *(int16_T *)&((char_T *)(c11_Player *)c11_pData)[2];
  c11_tmp.color = *(uint8_T *)&((char_T *)(c11_Player *)c11_pData)[4];
  c11_tmp.position = *(uint8_T *)&((char_T *)(c11_Player *)c11_pData)[5];
  c11_tmp.valid = *(uint8_T *)&((char_T *)(c11_Player *)c11_pData)[6];
  sf_mex_assign(&c11_mxVal, c11_n_sf_marshallOut(chartInstance, &c11_tmp), false);
  return c11_mxVal;
}

static const mxArray *c11_n_sf_marshallOut(void *chartInstanceVoid, void
  *c11_inData)
{
  const mxArray *c11_mxArrayOutData = NULL;
  c11_Player c11_u;
  const mxArray *c11_y = NULL;
  int8_T c11_b_u;
  const mxArray *c11_b_y = NULL;
  int8_T c11_c_u;
  const mxArray *c11_c_y = NULL;
  int16_T c11_d_u;
  const mxArray *c11_d_y = NULL;
  uint8_T c11_e_u;
  const mxArray *c11_e_y = NULL;
  uint8_T c11_f_u;
  const mxArray *c11_f_y = NULL;
  uint8_T c11_g_u;
  const mxArray *c11_g_y = NULL;
  SFc11_LessonIIIInstanceStruct *chartInstance;
  chartInstance = (SFc11_LessonIIIInstanceStruct *)chartInstanceVoid;
  c11_mxArrayOutData = NULL;
  c11_u = *(c11_Player *)c11_inData;
  c11_y = NULL;
  sf_mex_assign(&c11_y, sf_mex_createstruct("structure", 2, 1, 1), false);
  c11_b_u = c11_u.x;
  c11_b_y = NULL;
  sf_mex_assign(&c11_b_y, sf_mex_create("y", &c11_b_u, 2, 0U, 0U, 0U, 0), false);
  sf_mex_addfield(c11_y, c11_b_y, "x", "x", 0);
  c11_c_u = c11_u.y;
  c11_c_y = NULL;
  sf_mex_assign(&c11_c_y, sf_mex_create("y", &c11_c_u, 2, 0U, 0U, 0U, 0), false);
  sf_mex_addfield(c11_y, c11_c_y, "y", "y", 0);
  c11_d_u = c11_u.orientation;
  c11_d_y = NULL;
  sf_mex_assign(&c11_d_y, sf_mex_create("y", &c11_d_u, 4, 0U, 0U, 0U, 0), false);
  sf_mex_addfield(c11_y, c11_d_y, "orientation", "orientation", 0);
  c11_e_u = c11_u.color;
  c11_e_y = NULL;
  sf_mex_assign(&c11_e_y, sf_mex_create("y", &c11_e_u, 3, 0U, 0U, 0U, 0), false);
  sf_mex_addfield(c11_y, c11_e_y, "color", "color", 0);
  c11_f_u = c11_u.position;
  c11_f_y = NULL;
  sf_mex_assign(&c11_f_y, sf_mex_create("y", &c11_f_u, 3, 0U, 0U, 0U, 0), false);
  sf_mex_addfield(c11_y, c11_f_y, "position", "position", 0);
  c11_g_u = c11_u.valid;
  c11_g_y = NULL;
  sf_mex_assign(&c11_g_y, sf_mex_create("y", &c11_g_u, 3, 0U, 0U, 0U, 0), false);
  sf_mex_addfield(c11_y, c11_g_y, "valid", "valid", 0);
  sf_mex_assign(&c11_mxArrayOutData, c11_y, false);
  return c11_mxArrayOutData;
}

static void c11_n_emlrt_marshallIn(SFc11_LessonIIIInstanceStruct *chartInstance,
  const mxArray *c11_b_dataWrittenToVector, const char_T *c11_identifier,
  boolean_T c11_y[4])
{
  emlrtMsgIdentifier c11_thisId;
  c11_thisId.fIdentifier = c11_identifier;
  c11_thisId.fParent = NULL;
  c11_o_emlrt_marshallIn(chartInstance, sf_mex_dup(c11_b_dataWrittenToVector),
    &c11_thisId, c11_y);
  sf_mex_destroy(&c11_b_dataWrittenToVector);
}

static void c11_o_emlrt_marshallIn(SFc11_LessonIIIInstanceStruct *chartInstance,
  const mxArray *c11_u, const emlrtMsgIdentifier *c11_parentId, boolean_T c11_y
  [4])
{
  boolean_T c11_bv1[4];
  int32_T c11_i60;
  (void)chartInstance;
  sf_mex_import(c11_parentId, sf_mex_dup(c11_u), c11_bv1, 1, 11, 0U, 1, 0U, 1, 4);
  for (c11_i60 = 0; c11_i60 < 4; c11_i60++) {
    c11_y[c11_i60] = c11_bv1[c11_i60];
  }

  sf_mex_destroy(&c11_u);
}

static const mxArray *c11_p_emlrt_marshallIn(SFc11_LessonIIIInstanceStruct
  *chartInstance, const mxArray *c11_b_setSimStateSideEffectsInfo, const char_T *
  c11_identifier)
{
  const mxArray *c11_y = NULL;
  emlrtMsgIdentifier c11_thisId;
  c11_y = NULL;
  c11_thisId.fIdentifier = c11_identifier;
  c11_thisId.fParent = NULL;
  sf_mex_assign(&c11_y, c11_q_emlrt_marshallIn(chartInstance, sf_mex_dup
    (c11_b_setSimStateSideEffectsInfo), &c11_thisId), false);
  sf_mex_destroy(&c11_b_setSimStateSideEffectsInfo);
  return c11_y;
}

static const mxArray *c11_q_emlrt_marshallIn(SFc11_LessonIIIInstanceStruct
  *chartInstance, const mxArray *c11_u, const emlrtMsgIdentifier *c11_parentId)
{
  const mxArray *c11_y = NULL;
  (void)chartInstance;
  (void)c11_parentId;
  c11_y = NULL;
  sf_mex_assign(&c11_y, sf_mex_duplicatearraysafe(&c11_u), false);
  sf_mex_destroy(&c11_u);
  return c11_y;
}

static void c11_updateDataWrittenToVector(SFc11_LessonIIIInstanceStruct
  *chartInstance, uint32_T c11_vectorIndex)
{
  chartInstance->c11_dataWrittenToVector[(uint32_T)_SFD_EML_ARRAY_BOUNDS_CHECK
    (0U, (int32_T)c11_vectorIndex, 0, 3, 1, 0)] = true;
}

static void c11_errorIfDataNotWrittenToFcn(SFc11_LessonIIIInstanceStruct
  *chartInstance, uint32_T c11_vectorIndex, uint32_T c11_dataNumber, uint32_T
  c11_ssIdOfSourceObject, int32_T c11_offsetInSourceObject, int32_T
  c11_lengthInSourceObject)
{
  (void)c11_ssIdOfSourceObject;
  (void)c11_offsetInSourceObject;
  (void)c11_lengthInSourceObject;
  if (!chartInstance->c11_dataWrittenToVector[(uint32_T)
      _SFD_EML_ARRAY_BOUNDS_CHECK(0U, (int32_T)c11_vectorIndex, 0, 3, 1, 0)]) {
    _SFD_DATA_READ_BEFORE_WRITE_ERROR(c11_dataNumber);
  }
}

static void c11_b_cosd(SFc11_LessonIIIInstanceStruct *chartInstance, real32_T
  *c11_x)
{
  real32_T c11_b_x;
  real32_T c11_c_x;
  real32_T c11_d_x;
  real32_T c11_e_x;
  real32_T c11_xk;
  real32_T c11_f_x;
  real32_T c11_g_x;
  real32_T c11_absx;
  real32_T c11_h_x;
  real32_T c11_i_x;
  real32_T c11_b;
  int8_T c11_n;
  real32_T c11_b_b;
  real32_T c11_c_b;
  real32_T c11_d_b;
  real32_T c11_e_b;
  int8_T c11_b_n;
  c11_b_x = *c11_x;
  if (!c11_isfinite(chartInstance, c11_b_x)) {
    *c11_x = ((real32_T)rtNaN);
  } else {
    c11_c_x = c11_b_x;
    c11_d_x = c11_c_x;
    c11_e_x = c11_d_x;
    c11_eml_scalar_eg(chartInstance);
    c11_xk = c11_e_x;
    c11_d_x = muSingleScalarRem(c11_xk, 360.0F);
    c11_f_x = c11_d_x;
    c11_g_x = c11_f_x;
    c11_absx = muSingleScalarAbs(c11_g_x);
    if (c11_absx > 180.0F) {
      if (c11_d_x > 0.0F) {
        c11_d_x -= 360.0F;
      } else {
        c11_d_x += 360.0F;
      }

      c11_h_x = c11_d_x;
      c11_i_x = c11_h_x;
      c11_absx = muSingleScalarAbs(c11_i_x);
    }

    if (c11_absx <= 45.0F) {
      c11_b = c11_d_x;
      c11_d_x = 0.0174532924F * c11_b;
      c11_n = 0;
    } else if (c11_absx <= 135.0F) {
      if (c11_d_x > 0.0F) {
        c11_b_b = c11_d_x - 90.0F;
        c11_d_x = 0.0174532924F * c11_b_b;
        c11_n = 1;
      } else {
        c11_c_b = c11_d_x + 90.0F;
        c11_d_x = 0.0174532924F * c11_c_b;
        c11_n = -1;
      }
    } else if (c11_d_x > 0.0F) {
      c11_d_b = c11_d_x - 180.0F;
      c11_d_x = 0.0174532924F * c11_d_b;
      c11_n = 2;
    } else {
      c11_e_b = c11_d_x + 180.0F;
      c11_d_x = 0.0174532924F * c11_e_b;
      c11_n = -2;
    }

    c11_b_n = c11_n;
    c11_b_x = c11_d_x;
    if ((real_T)c11_b_n == 0.0) {
      *c11_x = muSingleScalarCos(c11_b_x);
    } else if ((real_T)c11_b_n == 1.0) {
      *c11_x = -muSingleScalarSin(c11_b_x);
    } else if ((real_T)c11_b_n == -1.0) {
      *c11_x = muSingleScalarSin(c11_b_x);
    } else {
      *c11_x = -muSingleScalarCos(c11_b_x);
    }
  }
}

static void c11_b_sind(SFc11_LessonIIIInstanceStruct *chartInstance, real32_T
  *c11_x)
{
  real32_T c11_b_x;
  real32_T c11_f6;
  real32_T c11_c_x;
  real32_T c11_d_x;
  real32_T c11_e_x;
  real32_T c11_xk;
  real32_T c11_f_x;
  real32_T c11_g_x;
  real32_T c11_absx;
  real32_T c11_h_x;
  real32_T c11_i_x;
  real32_T c11_b;
  int8_T c11_n;
  real32_T c11_b_b;
  real32_T c11_c_b;
  real32_T c11_d_b;
  real32_T c11_e_b;
  int8_T c11_b_n;
  c11_b_x = *c11_x;
  if (!c11_isfinite(chartInstance, c11_b_x)) {
    c11_f6 = ((real32_T)rtNaN);
  } else {
    c11_c_x = c11_b_x;
    c11_d_x = c11_c_x;
    c11_e_x = c11_d_x;
    c11_eml_scalar_eg(chartInstance);
    c11_xk = c11_e_x;
    c11_d_x = muSingleScalarRem(c11_xk, 360.0F);
    c11_f_x = c11_d_x;
    c11_g_x = c11_f_x;
    c11_absx = muSingleScalarAbs(c11_g_x);
    if (c11_absx > 180.0F) {
      if (c11_d_x > 0.0F) {
        c11_d_x -= 360.0F;
      } else {
        c11_d_x += 360.0F;
      }

      c11_h_x = c11_d_x;
      c11_i_x = c11_h_x;
      c11_absx = muSingleScalarAbs(c11_i_x);
    }

    if (c11_absx <= 45.0F) {
      c11_b = c11_d_x;
      c11_d_x = 0.0174532924F * c11_b;
      c11_n = 0;
    } else if (c11_absx <= 135.0F) {
      if (c11_d_x > 0.0F) {
        c11_b_b = c11_d_x - 90.0F;
        c11_d_x = 0.0174532924F * c11_b_b;
        c11_n = 1;
      } else {
        c11_c_b = c11_d_x + 90.0F;
        c11_d_x = 0.0174532924F * c11_c_b;
        c11_n = -1;
      }
    } else if (c11_d_x > 0.0F) {
      c11_d_b = c11_d_x - 180.0F;
      c11_d_x = 0.0174532924F * c11_d_b;
      c11_n = 2;
    } else {
      c11_e_b = c11_d_x + 180.0F;
      c11_d_x = 0.0174532924F * c11_e_b;
      c11_n = -2;
    }

    c11_b_n = c11_n;
    c11_b_x = c11_d_x;
    if ((real_T)c11_b_n == 0.0) {
      c11_f6 = muSingleScalarSin(c11_b_x);
    } else if ((real_T)c11_b_n == 1.0) {
      c11_f6 = muSingleScalarCos(c11_b_x);
    } else if ((real_T)c11_b_n == -1.0) {
      c11_f6 = -muSingleScalarCos(c11_b_x);
    } else {
      c11_f6 = -muSingleScalarSin(c11_b_x);
    }
  }

  *c11_x = c11_f6;
}

static void init_dsm_address_info(SFc11_LessonIIIInstanceStruct *chartInstance)
{
  (void)chartInstance;
}

static void init_simulink_io_address(SFc11_LessonIIIInstanceStruct
  *chartInstance)
{
  chartInstance->c11_players = (c11_Player (*)[6])ssGetInputPortSignal_wrapper
    (chartInstance->S, 0);
  chartInstance->c11_ball = (c11_Ball *)ssGetInputPortSignal_wrapper
    (chartInstance->S, 1);
  chartInstance->c11_finalWay = (c11_Waypoint *)ssGetOutputPortSignal_wrapper
    (chartInstance->S, 1);
  chartInstance->c11_me = (c11_Player *)ssGetInputPortSignal_wrapper
    (chartInstance->S, 2);
  chartInstance->c11_manualWay = (c11_Waypoint *)ssGetInputPortSignal_wrapper
    (chartInstance->S, 3);
  chartInstance->c11_collision = (uint8_T *)ssGetOutputPortSignal_wrapper
    (chartInstance->S, 2);
}

/* SFunction Glue Code */
#ifdef utFree
#undef utFree
#endif

#ifdef utMalloc
#undef utMalloc
#endif

#ifdef __cplusplus

extern "C" void *utMalloc(size_t size);
extern "C" void utFree(void*);

#else

extern void *utMalloc(size_t size);
extern void utFree(void*);

#endif

void sf_c11_LessonIII_get_check_sum(mxArray *plhs[])
{
  ((real_T *)mxGetPr((plhs[0])))[0] = (real_T)(3229527491U);
  ((real_T *)mxGetPr((plhs[0])))[1] = (real_T)(1321859305U);
  ((real_T *)mxGetPr((plhs[0])))[2] = (real_T)(1294810081U);
  ((real_T *)mxGetPr((plhs[0])))[3] = (real_T)(118718787U);
}

mxArray* sf_c11_LessonIII_get_post_codegen_info(void);
mxArray *sf_c11_LessonIII_get_autoinheritance_info(void)
{
  const char *autoinheritanceFields[] = { "checksum", "inputs", "parameters",
    "outputs", "locals", "postCodegenInfo" };

  mxArray *mxAutoinheritanceInfo = mxCreateStructMatrix(1, 1, sizeof
    (autoinheritanceFields)/sizeof(autoinheritanceFields[0]),
    autoinheritanceFields);

  {
    mxArray *mxChecksum = mxCreateString("w74dGpMhbzSYFg9BBO2f9D");
    mxSetField(mxAutoinheritanceInfo,0,"checksum",mxChecksum);
  }

  {
    const char *dataFields[] = { "size", "type", "complexity" };

    mxArray *mxData = mxCreateStructMatrix(1,4,3,dataFields);

    {
      mxArray *mxSize = mxCreateDoubleMatrix(1,2,mxREAL);
      double *pr = mxGetPr(mxSize);
      pr[0] = (double)(1);
      pr[1] = (double)(6);
      mxSetField(mxData,0,"size",mxSize);
    }

    {
      const char *typeFields[] = { "base", "fixpt" };

      mxArray *mxType = mxCreateStructMatrix(1,1,2,typeFields);
      mxSetField(mxType,0,"base",mxCreateDoubleScalar(13));
      mxSetField(mxType,0,"fixpt",mxCreateDoubleMatrix(0,0,mxREAL));
      mxSetField(mxData,0,"type",mxType);
    }

    mxSetField(mxData,0,"complexity",mxCreateDoubleScalar(0));

    {
      mxArray *mxSize = mxCreateDoubleMatrix(1,2,mxREAL);
      double *pr = mxGetPr(mxSize);
      pr[0] = (double)(1);
      pr[1] = (double)(1);
      mxSetField(mxData,1,"size",mxSize);
    }

    {
      const char *typeFields[] = { "base", "fixpt" };

      mxArray *mxType = mxCreateStructMatrix(1,1,2,typeFields);
      mxSetField(mxType,0,"base",mxCreateDoubleScalar(13));
      mxSetField(mxType,0,"fixpt",mxCreateDoubleMatrix(0,0,mxREAL));
      mxSetField(mxData,1,"type",mxType);
    }

    mxSetField(mxData,1,"complexity",mxCreateDoubleScalar(0));

    {
      mxArray *mxSize = mxCreateDoubleMatrix(1,2,mxREAL);
      double *pr = mxGetPr(mxSize);
      pr[0] = (double)(1);
      pr[1] = (double)(1);
      mxSetField(mxData,2,"size",mxSize);
    }

    {
      const char *typeFields[] = { "base", "fixpt" };

      mxArray *mxType = mxCreateStructMatrix(1,1,2,typeFields);
      mxSetField(mxType,0,"base",mxCreateDoubleScalar(13));
      mxSetField(mxType,0,"fixpt",mxCreateDoubleMatrix(0,0,mxREAL));
      mxSetField(mxData,2,"type",mxType);
    }

    mxSetField(mxData,2,"complexity",mxCreateDoubleScalar(0));

    {
      mxArray *mxSize = mxCreateDoubleMatrix(1,2,mxREAL);
      double *pr = mxGetPr(mxSize);
      pr[0] = (double)(1);
      pr[1] = (double)(1);
      mxSetField(mxData,3,"size",mxSize);
    }

    {
      const char *typeFields[] = { "base", "fixpt" };

      mxArray *mxType = mxCreateStructMatrix(1,1,2,typeFields);
      mxSetField(mxType,0,"base",mxCreateDoubleScalar(13));
      mxSetField(mxType,0,"fixpt",mxCreateDoubleMatrix(0,0,mxREAL));
      mxSetField(mxData,3,"type",mxType);
    }

    mxSetField(mxData,3,"complexity",mxCreateDoubleScalar(0));
    mxSetField(mxAutoinheritanceInfo,0,"inputs",mxData);
  }

  {
    mxSetField(mxAutoinheritanceInfo,0,"parameters",mxCreateDoubleMatrix(0,0,
                mxREAL));
  }

  {
    const char *dataFields[] = { "size", "type", "complexity" };

    mxArray *mxData = mxCreateStructMatrix(1,2,3,dataFields);

    {
      mxArray *mxSize = mxCreateDoubleMatrix(1,2,mxREAL);
      double *pr = mxGetPr(mxSize);
      pr[0] = (double)(1);
      pr[1] = (double)(1);
      mxSetField(mxData,0,"size",mxSize);
    }

    {
      const char *typeFields[] = { "base", "fixpt" };

      mxArray *mxType = mxCreateStructMatrix(1,1,2,typeFields);
      mxSetField(mxType,0,"base",mxCreateDoubleScalar(13));
      mxSetField(mxType,0,"fixpt",mxCreateDoubleMatrix(0,0,mxREAL));
      mxSetField(mxData,0,"type",mxType);
    }

    mxSetField(mxData,0,"complexity",mxCreateDoubleScalar(0));

    {
      mxArray *mxSize = mxCreateDoubleMatrix(1,2,mxREAL);
      double *pr = mxGetPr(mxSize);
      pr[0] = (double)(1);
      pr[1] = (double)(1);
      mxSetField(mxData,1,"size",mxSize);
    }

    {
      const char *typeFields[] = { "base", "fixpt" };

      mxArray *mxType = mxCreateStructMatrix(1,1,2,typeFields);
      mxSetField(mxType,0,"base",mxCreateDoubleScalar(3));
      mxSetField(mxType,0,"fixpt",mxCreateDoubleMatrix(0,0,mxREAL));
      mxSetField(mxData,1,"type",mxType);
    }

    mxSetField(mxData,1,"complexity",mxCreateDoubleScalar(0));
    mxSetField(mxAutoinheritanceInfo,0,"outputs",mxData);
  }

  {
    const char *dataFields[] = { "size", "type", "complexity" };

    mxArray *mxData = mxCreateStructMatrix(1,2,3,dataFields);

    {
      mxArray *mxSize = mxCreateDoubleMatrix(1,2,mxREAL);
      double *pr = mxGetPr(mxSize);
      pr[0] = (double)(1);
      pr[1] = (double)(1);
      mxSetField(mxData,0,"size",mxSize);
    }

    {
      const char *typeFields[] = { "base", "fixpt" };

      mxArray *mxType = mxCreateStructMatrix(1,1,2,typeFields);
      mxSetField(mxType,0,"base",mxCreateDoubleScalar(3));
      mxSetField(mxType,0,"fixpt",mxCreateDoubleMatrix(0,0,mxREAL));
      mxSetField(mxData,0,"type",mxType);
    }

    mxSetField(mxData,0,"complexity",mxCreateDoubleScalar(0));

    {
      mxArray *mxSize = mxCreateDoubleMatrix(1,2,mxREAL);
      double *pr = mxGetPr(mxSize);
      pr[0] = (double)(1);
      pr[1] = (double)(1);
      mxSetField(mxData,1,"size",mxSize);
    }

    {
      const char *typeFields[] = { "base", "fixpt" };

      mxArray *mxType = mxCreateStructMatrix(1,1,2,typeFields);
      mxSetField(mxType,0,"base",mxCreateDoubleScalar(10));
      mxSetField(mxType,0,"fixpt",mxCreateDoubleMatrix(0,0,mxREAL));
      mxSetField(mxData,1,"type",mxType);
    }

    mxSetField(mxData,1,"complexity",mxCreateDoubleScalar(0));
    mxSetField(mxAutoinheritanceInfo,0,"locals",mxData);
  }

  {
    mxArray* mxPostCodegenInfo = sf_c11_LessonIII_get_post_codegen_info();
    mxSetField(mxAutoinheritanceInfo,0,"postCodegenInfo",mxPostCodegenInfo);
  }

  return(mxAutoinheritanceInfo);
}

mxArray *sf_c11_LessonIII_third_party_uses_info(void)
{
  mxArray * mxcell3p = mxCreateCellMatrix(1,0);
  return(mxcell3p);
}

mxArray *sf_c11_LessonIII_jit_fallback_info(void)
{
  const char *infoFields[] = { "fallbackType", "fallbackReason",
    "incompatibleSymbol", };

  mxArray *mxInfo = mxCreateStructMatrix(1, 1, 3, infoFields);
  mxArray *fallbackReason = mxCreateString("feature_off");
  mxArray *incompatibleSymbol = mxCreateString("");
  mxArray *fallbackType = mxCreateString("early");
  mxSetField(mxInfo, 0, infoFields[0], fallbackType);
  mxSetField(mxInfo, 0, infoFields[1], fallbackReason);
  mxSetField(mxInfo, 0, infoFields[2], incompatibleSymbol);
  return mxInfo;
}

mxArray *sf_c11_LessonIII_updateBuildInfo_args_info(void)
{
  mxArray *mxBIArgs = mxCreateCellMatrix(1,0);
  return mxBIArgs;
}

mxArray* sf_c11_LessonIII_get_post_codegen_info(void)
{
  const char* fieldNames[] = { "exportedFunctionsUsedByThisChart",
    "exportedFunctionsChecksum" };

  mwSize dims[2] = { 1, 1 };

  mxArray* mxPostCodegenInfo = mxCreateStructArray(2, dims, sizeof(fieldNames)/
    sizeof(fieldNames[0]), fieldNames);

  {
    mxArray* mxExportedFunctionsChecksum = mxCreateString("");
    mwSize exp_dims[2] = { 0, 1 };

    mxArray* mxExportedFunctionsUsedByThisChart = mxCreateCellArray(2, exp_dims);
    mxSetField(mxPostCodegenInfo, 0, "exportedFunctionsUsedByThisChart",
               mxExportedFunctionsUsedByThisChart);
    mxSetField(mxPostCodegenInfo, 0, "exportedFunctionsChecksum",
               mxExportedFunctionsChecksum);
  }

  return mxPostCodegenInfo;
}

static const mxArray *sf_get_sim_state_info_c11_LessonIII(void)
{
  const char *infoFields[] = { "chartChecksum", "varInfo" };

  mxArray *mxInfo = mxCreateStructMatrix(1, 1, 2, infoFields);
  const char *infoEncStr[] = {
    "100 S1x10'type','srcId','name','auxInfo'{{M[1],M[68],T\"collision\",},{M[1],M[27],T\"finalWay\",},{M[3],M[78],T\"collisionDetected\",},{M[3],M[79],T\"lol\",},{M[8],M[0],T\"is_active_c11_LessonIII\",},{M[8],M[17],T\"is_active_PlayerLogics\",},{M[8],M[53],T\"is_active_Collision\",},{M[9],M[17],T\"is_PlayerLogics\",},{M[9],M[53],T\"is_Collision\",},{M[11],M[0],T\"temporalCounter_i1\",S'et','os','ct'{{T\"wu\",M1x2[60 47],M[1]}}}}",
    "100 S'type','srcId','name','auxInfo'{{M[15],M[0],T\"dataWrittenToVector\",}}"
  };

  mxArray *mxVarInfo = sf_mex_decode_encoded_mx_struct_array(infoEncStr, 11, 10);
  mxArray *mxChecksum = mxCreateDoubleMatrix(1, 4, mxREAL);
  sf_c11_LessonIII_get_check_sum(&mxChecksum);
  mxSetField(mxInfo, 0, infoFields[0], mxChecksum);
  mxSetField(mxInfo, 0, infoFields[1], mxVarInfo);
  return mxInfo;
}

static void chart_debug_initialization(SimStruct *S, unsigned int
  fullDebuggerInitialization)
{
  if (!sim_mode_is_rtw_gen(S)) {
    SFc11_LessonIIIInstanceStruct *chartInstance;
    ChartRunTimeInfo * crtInfo = (ChartRunTimeInfo *)(ssGetUserData(S));
    ChartInfoStruct * chartInfo = (ChartInfoStruct *)(crtInfo->instanceInfo);
    chartInstance = (SFc11_LessonIIIInstanceStruct *) chartInfo->chartInstance;
    if (ssIsFirstInitCond(S) && fullDebuggerInitialization==1) {
      /* do this only if simulation is starting */
      {
        unsigned int chartAlreadyPresent;
        chartAlreadyPresent = sf_debug_initialize_chart
          (sfGlobalDebugInstanceStruct,
           _LessonIIIMachineNumber_,
           11,
           9,
           6,
           0,
           8,
           0,
           0,
           0,
           0,
           0,
           &(chartInstance->chartNumber),
           &(chartInstance->instanceNumber),
           (void *)S);

        /* Each instance must initialize its own list of scripts */
        init_script_number_translation(_LessonIIIMachineNumber_,
          chartInstance->chartNumber,chartInstance->instanceNumber);
        if (chartAlreadyPresent==0) {
          /* this is the first instance */
          sf_debug_set_chart_disable_implicit_casting
            (sfGlobalDebugInstanceStruct,_LessonIIIMachineNumber_,
             chartInstance->chartNumber,1);
          sf_debug_set_chart_event_thresholds(sfGlobalDebugInstanceStruct,
            _LessonIIIMachineNumber_,
            chartInstance->chartNumber,
            0,
            0,
            0);
          _SFD_SET_DATA_PROPS(0,1,1,0,"players");
          _SFD_SET_DATA_PROPS(1,1,1,0,"ball");
          _SFD_SET_DATA_PROPS(2,2,0,1,"finalWay");
          _SFD_SET_DATA_PROPS(3,1,1,0,"me");
          _SFD_SET_DATA_PROPS(4,1,1,0,"manualWay");
          _SFD_SET_DATA_PROPS(5,2,0,1,"collision");
          _SFD_SET_DATA_PROPS(6,0,0,0,"collisionDetected");
          _SFD_SET_DATA_PROPS(7,0,0,0,"lol");
          _SFD_STATE_INFO(0,0,1);
          _SFD_STATE_INFO(1,0,0);
          _SFD_STATE_INFO(2,0,0);
          _SFD_STATE_INFO(5,0,1);
          _SFD_STATE_INFO(6,0,0);
          _SFD_STATE_INFO(7,0,0);
          _SFD_STATE_INFO(8,0,0);
          _SFD_STATE_INFO(3,0,2);
          _SFD_STATE_INFO(4,0,2);
          _SFD_CH_SUBSTATE_COUNT(2);
          _SFD_CH_SUBSTATE_DECOMP(1);
          _SFD_CH_SUBSTATE_INDEX(0,5);
          _SFD_CH_SUBSTATE_INDEX(1,0);
          _SFD_ST_SUBSTATE_COUNT(5,3);
          _SFD_ST_SUBSTATE_INDEX(5,0,6);
          _SFD_ST_SUBSTATE_INDEX(5,1,7);
          _SFD_ST_SUBSTATE_INDEX(5,2,8);
          _SFD_ST_SUBSTATE_COUNT(6,0);
          _SFD_ST_SUBSTATE_COUNT(7,0);
          _SFD_ST_SUBSTATE_COUNT(8,0);
          _SFD_ST_SUBSTATE_COUNT(0,2);
          _SFD_ST_SUBSTATE_INDEX(0,0,1);
          _SFD_ST_SUBSTATE_INDEX(0,1,2);
          _SFD_ST_SUBSTATE_COUNT(1,0);
          _SFD_ST_SUBSTATE_COUNT(2,0);
        }

        _SFD_CV_INIT_CHART(2,0,0,0);

        {
          _SFD_CV_INIT_STATE(0,2,1,0,0,0,NULL,NULL);
        }

        {
          _SFD_CV_INIT_STATE(1,0,0,0,0,0,NULL,NULL);
        }

        {
          _SFD_CV_INIT_STATE(2,0,0,0,0,0,NULL,NULL);
        }

        {
          _SFD_CV_INIT_STATE(5,3,1,0,0,0,NULL,NULL);
        }

        {
          _SFD_CV_INIT_STATE(6,0,0,0,0,0,NULL,NULL);
        }

        {
          _SFD_CV_INIT_STATE(7,0,0,0,0,0,NULL,NULL);
        }

        {
          _SFD_CV_INIT_STATE(8,0,0,0,0,0,NULL,NULL);
        }

        {
          _SFD_CV_INIT_STATE(3,0,0,0,0,0,NULL,NULL);
        }

        {
          _SFD_CV_INIT_STATE(4,0,0,0,0,0,NULL,NULL);
        }

        _SFD_CV_INIT_TRANS(5,0,NULL,NULL,0,NULL);
        _SFD_CV_INIT_TRANS(3,0,NULL,NULL,0,NULL);
        _SFD_CV_INIT_TRANS(4,0,NULL,NULL,0,NULL);
        _SFD_CV_INIT_TRANS(2,0,NULL,NULL,0,NULL);
        _SFD_CV_INIT_TRANS(0,0,NULL,NULL,0,NULL);
        _SFD_CV_INIT_TRANS(1,0,NULL,NULL,0,NULL);

        /* Initialization of MATLAB Function Model Coverage */
        _SFD_CV_INIT_EML(3,1,1,0,0,0,0,0,0,0,0);
        _SFD_CV_INIT_EML_FCN(3,0,"alternateWay",0,-1,319);
        _SFD_CV_INIT_EML(4,1,1,2,0,3,0,1,0,4,1);
        _SFD_CV_INIT_EML_FCN(4,0,"normCollisionFinder",0,-1,650);
        _SFD_CV_INIT_EML_SATURATION(4,1,0,278,-1,345);
        _SFD_CV_INIT_EML_SATURATION(4,1,1,320,-1,345);
        _SFD_CV_INIT_EML_SATURATION(4,1,2,73,-1,109);
        _SFD_CV_INIT_EML_IF(4,1,0,112,130,-1,629);
        _SFD_CV_INIT_EML_IF(4,1,1,466,555,-1,-2);
        _SFD_CV_INIT_EML_FOR(4,1,0,451,462,625);

        {
          static int condStart[] = { 469, 492, 514, 537 };

          static int condEnd[] = { 488, 510, 533, 555 };

          static int pfixExpr[] = { 0, 1, -3, 2, -3, 3, -3 };

          _SFD_CV_INIT_EML_MCDC(4,1,0,469,555,4,0,&(condStart[0]),&(condEnd[0]),
                                7,&(pfixExpr[0]));
        }

        _SFD_CV_INIT_EML_RELATIONAL(4,1,0,115,130,-1,4);
        _SFD_CV_INIT_EML_RELATIONAL(4,1,1,469,488,0,4);
        _SFD_CV_INIT_EML_RELATIONAL(4,1,2,492,510,0,2);
        _SFD_CV_INIT_EML_RELATIONAL(4,1,3,514,533,0,4);
        _SFD_CV_INIT_EML_RELATIONAL(4,1,4,537,555,0,2);
        _SFD_CV_INIT_EML(6,1,0,0,0,0,0,0,0,0,0);
        _SFD_CV_INIT_EML(5,1,0,0,0,0,0,0,0,0,0);
        _SFD_CV_INIT_EML(0,1,0,0,0,0,0,0,0,0,0);
        _SFD_CV_INIT_EML(2,1,0,0,0,0,0,0,0,0,0);
        _SFD_CV_INIT_EML(7,1,0,0,0,0,0,0,0,0,0);
        _SFD_CV_INIT_EML(8,1,0,0,0,0,0,0,0,0,0);
        _SFD_CV_INIT_EML(4,0,0,1,0,0,0,0,0,0,0);
        _SFD_CV_INIT_EML_IF(4,0,0,0,14,0,14);
        _SFD_CV_INIT_EML(0,0,0,1,0,0,0,0,0,0,0);
        _SFD_CV_INIT_EML_IF(0,0,0,1,28,1,28);
        _SFD_CV_INIT_EML_RELATIONAL(0,0,0,1,28,0,0);
        _SFD_CV_INIT_EML(1,0,0,1,0,0,0,0,0,0,0);
        _SFD_CV_INIT_EML_IF(1,0,0,1,28,1,28);
        _SFD_CV_INIT_EML_RELATIONAL(1,0,0,1,28,0,0);
        _SFD_CV_INIT_EML(3,0,0,1,0,0,0,0,0,0,0);
        _SFD_CV_INIT_EML_IF(3,0,0,0,14,0,14);

        {
          unsigned int dimVector[2];
          dimVector[0]= 1;
          dimVector[1]= 6;
          _SFD_SET_DATA_COMPILED_PROPS(0,SF_STRUCT,2,&(dimVector[0]),0,0,0,0.0,
            1.0,0,0,(MexFcnForType)c11_players_bus_io,(MexInFcnForType)NULL);
        }

        _SFD_SET_DATA_COMPILED_PROPS(1,SF_STRUCT,0,NULL,0,0,0,0.0,1.0,0,0,
          (MexFcnForType)c11_ball_bus_io,(MexInFcnForType)NULL);
        _SFD_SET_DATA_COMPILED_PROPS(2,SF_STRUCT,0,NULL,0,0,0,0.0,1.0,0,0,
          (MexFcnForType)c11_finalWay_bus_io,(MexInFcnForType)NULL);
        _SFD_SET_DATA_COMPILED_PROPS(3,SF_STRUCT,0,NULL,0,0,0,0.0,1.0,0,0,
          (MexFcnForType)c11_me_bus_io,(MexInFcnForType)NULL);
        _SFD_SET_DATA_COMPILED_PROPS(4,SF_STRUCT,0,NULL,0,0,0,0.0,1.0,0,0,
          (MexFcnForType)c11_finalWay_bus_io,(MexInFcnForType)NULL);
        _SFD_SET_DATA_COMPILED_PROPS(5,SF_UINT8,0,NULL,0,0,0,0.0,1.0,0,0,
          (MexFcnForType)c11_k_sf_marshallOut,(MexInFcnForType)
          c11_i_sf_marshallIn);
        _SFD_SET_DATA_COMPILED_PROPS(6,SF_UINT8,0,NULL,0,0,0,0.0,1.0,0,0,
          (MexFcnForType)c11_k_sf_marshallOut,(MexInFcnForType)
          c11_i_sf_marshallIn);
        _SFD_SET_DATA_COMPILED_PROPS(7,SF_DOUBLE,0,NULL,0,0,0,0.0,1.0,0,0,
          (MexFcnForType)c11_sf_marshallOut,(MexInFcnForType)c11_sf_marshallIn);
        _SFD_SET_DATA_VALUE_PTR(0U, *chartInstance->c11_players);
        _SFD_SET_DATA_VALUE_PTR(1U, chartInstance->c11_ball);
        _SFD_SET_DATA_VALUE_PTR(2U, chartInstance->c11_finalWay);
        _SFD_SET_DATA_VALUE_PTR(3U, chartInstance->c11_me);
        _SFD_SET_DATA_VALUE_PTR(4U, chartInstance->c11_manualWay);
        _SFD_SET_DATA_VALUE_PTR(5U, chartInstance->c11_collision);
        _SFD_SET_DATA_VALUE_PTR(6U, &chartInstance->c11_collisionDetected);
        _SFD_SET_DATA_VALUE_PTR(7U, &chartInstance->c11_lol);
      }
    } else {
      sf_debug_reset_current_state_configuration(sfGlobalDebugInstanceStruct,
        _LessonIIIMachineNumber_,chartInstance->chartNumber,
        chartInstance->instanceNumber);
    }
  }
}

static const char* sf_get_instance_specialization(void)
{
  return "lSC5a8XYA6cZdj3x6TQDLH";
}

static void sf_opaque_initialize_c11_LessonIII(void *chartInstanceVar)
{
  chart_debug_initialization(((SFc11_LessonIIIInstanceStruct*) chartInstanceVar
    )->S,0);
  initialize_params_c11_LessonIII((SFc11_LessonIIIInstanceStruct*)
    chartInstanceVar);
  initialize_c11_LessonIII((SFc11_LessonIIIInstanceStruct*) chartInstanceVar);
}

static void sf_opaque_enable_c11_LessonIII(void *chartInstanceVar)
{
  enable_c11_LessonIII((SFc11_LessonIIIInstanceStruct*) chartInstanceVar);
}

static void sf_opaque_disable_c11_LessonIII(void *chartInstanceVar)
{
  disable_c11_LessonIII((SFc11_LessonIIIInstanceStruct*) chartInstanceVar);
}

static void sf_opaque_gateway_c11_LessonIII(void *chartInstanceVar)
{
  sf_gateway_c11_LessonIII((SFc11_LessonIIIInstanceStruct*) chartInstanceVar);
}

static const mxArray* sf_opaque_get_sim_state_c11_LessonIII(SimStruct* S)
{
  ChartRunTimeInfo * crtInfo = (ChartRunTimeInfo *)(ssGetUserData(S));
  ChartInfoStruct * chartInfo = (ChartInfoStruct *)(crtInfo->instanceInfo);
  return get_sim_state_c11_LessonIII((SFc11_LessonIIIInstanceStruct*)
    chartInfo->chartInstance);         /* raw sim ctx */
}

static void sf_opaque_set_sim_state_c11_LessonIII(SimStruct* S, const mxArray
  *st)
{
  ChartRunTimeInfo * crtInfo = (ChartRunTimeInfo *)(ssGetUserData(S));
  ChartInfoStruct * chartInfo = (ChartInfoStruct *)(crtInfo->instanceInfo);
  set_sim_state_c11_LessonIII((SFc11_LessonIIIInstanceStruct*)
    chartInfo->chartInstance, st);
}

static void sf_opaque_terminate_c11_LessonIII(void *chartInstanceVar)
{
  if (chartInstanceVar!=NULL) {
    SimStruct *S = ((SFc11_LessonIIIInstanceStruct*) chartInstanceVar)->S;
    ChartRunTimeInfo * crtInfo = (ChartRunTimeInfo *)(ssGetUserData(S));
    if (sim_mode_is_rtw_gen(S) || sim_mode_is_external(S)) {
      sf_clear_rtw_identifier(S);
      unload_LessonIII_optimization_info();
    }

    finalize_c11_LessonIII((SFc11_LessonIIIInstanceStruct*) chartInstanceVar);
    utFree(chartInstanceVar);
    if (crtInfo != NULL) {
      utFree(crtInfo);
    }

    ssSetUserData(S,NULL);
  }
}

static void sf_opaque_init_subchart_simstructs(void *chartInstanceVar)
{
  initSimStructsc11_LessonIII((SFc11_LessonIIIInstanceStruct*) chartInstanceVar);
}

extern unsigned int sf_machine_global_initializer_called(void);
static void mdlProcessParameters_c11_LessonIII(SimStruct *S)
{
  int i;
  for (i=0;i<ssGetNumRunTimeParams(S);i++) {
    if (ssGetSFcnParamTunable(S,i)) {
      ssUpdateDlgParamAsRunTimeParam(S,i);
    }
  }

  if (sf_machine_global_initializer_called()) {
    ChartRunTimeInfo * crtInfo = (ChartRunTimeInfo *)(ssGetUserData(S));
    ChartInfoStruct * chartInfo = (ChartInfoStruct *)(crtInfo->instanceInfo);
    initialize_params_c11_LessonIII((SFc11_LessonIIIInstanceStruct*)
      (chartInfo->chartInstance));
  }
}

static void mdlSetWorkWidths_c11_LessonIII(SimStruct *S)
{
  if (sim_mode_is_rtw_gen(S) || sim_mode_is_external(S)) {
    mxArray *infoStruct = load_LessonIII_optimization_info();
    int_T chartIsInlinable =
      (int_T)sf_is_chart_inlinable(sf_get_instance_specialization(),infoStruct,
      11);
    ssSetStateflowIsInlinable(S,chartIsInlinable);
    ssSetRTWCG(S,sf_rtw_info_uint_prop(sf_get_instance_specialization(),
                infoStruct,11,"RTWCG"));
    ssSetEnableFcnIsTrivial(S,1);
    ssSetDisableFcnIsTrivial(S,1);
    ssSetNotMultipleInlinable(S,sf_rtw_info_uint_prop
      (sf_get_instance_specialization(),infoStruct,11,
       "gatewayCannotBeInlinedMultipleTimes"));
    sf_update_buildInfo(sf_get_instance_specialization(),infoStruct,11);
    if (chartIsInlinable) {
      ssSetInputPortOptimOpts(S, 0, SS_REUSABLE_AND_LOCAL);
      ssSetInputPortOptimOpts(S, 1, SS_REUSABLE_AND_LOCAL);
      ssSetInputPortOptimOpts(S, 2, SS_REUSABLE_AND_LOCAL);
      ssSetInputPortOptimOpts(S, 3, SS_REUSABLE_AND_LOCAL);
      sf_mark_chart_expressionable_inputs(S,sf_get_instance_specialization(),
        infoStruct,11,4);
      sf_mark_chart_reusable_outputs(S,sf_get_instance_specialization(),
        infoStruct,11,2);
    }

    {
      unsigned int outPortIdx;
      for (outPortIdx=1; outPortIdx<=2; ++outPortIdx) {
        ssSetOutputPortOptimizeInIR(S, outPortIdx, 1U);
      }
    }

    {
      unsigned int inPortIdx;
      for (inPortIdx=0; inPortIdx < 4; ++inPortIdx) {
        ssSetInputPortOptimizeInIR(S, inPortIdx, 1U);
      }
    }

    sf_set_rtw_dwork_info(S,sf_get_instance_specialization(),infoStruct,11);
    ssSetHasSubFunctions(S,!(chartIsInlinable));
  } else {
  }

  ssSetOptions(S,ssGetOptions(S)|SS_OPTION_WORKS_WITH_CODE_REUSE);
  ssSetChecksum0(S,(4147013537U));
  ssSetChecksum1(S,(3286355014U));
  ssSetChecksum2(S,(694858397U));
  ssSetChecksum3(S,(3306034651U));
  ssSetmdlDerivatives(S, NULL);
  ssSetExplicitFCSSCtrl(S,1);
  ssSupportsMultipleExecInstances(S,1);
}

static void mdlRTW_c11_LessonIII(SimStruct *S)
{
  if (sim_mode_is_rtw_gen(S)) {
    ssWriteRTWStrParam(S, "StateflowChartType", "Stateflow");
  }
}

static void mdlStart_c11_LessonIII(SimStruct *S)
{
  SFc11_LessonIIIInstanceStruct *chartInstance;
  ChartRunTimeInfo * crtInfo = (ChartRunTimeInfo *)utMalloc(sizeof
    (ChartRunTimeInfo));
  chartInstance = (SFc11_LessonIIIInstanceStruct *)utMalloc(sizeof
    (SFc11_LessonIIIInstanceStruct));
  memset(chartInstance, 0, sizeof(SFc11_LessonIIIInstanceStruct));
  if (chartInstance==NULL) {
    sf_mex_error_message("Could not allocate memory for chart instance.");
  }

  chartInstance->chartInfo.chartInstance = chartInstance;
  chartInstance->chartInfo.isEMLChart = 0;
  chartInstance->chartInfo.chartInitialized = 0;
  chartInstance->chartInfo.sFunctionGateway = sf_opaque_gateway_c11_LessonIII;
  chartInstance->chartInfo.initializeChart = sf_opaque_initialize_c11_LessonIII;
  chartInstance->chartInfo.terminateChart = sf_opaque_terminate_c11_LessonIII;
  chartInstance->chartInfo.enableChart = sf_opaque_enable_c11_LessonIII;
  chartInstance->chartInfo.disableChart = sf_opaque_disable_c11_LessonIII;
  chartInstance->chartInfo.getSimState = sf_opaque_get_sim_state_c11_LessonIII;
  chartInstance->chartInfo.setSimState = sf_opaque_set_sim_state_c11_LessonIII;
  chartInstance->chartInfo.getSimStateInfo = sf_get_sim_state_info_c11_LessonIII;
  chartInstance->chartInfo.zeroCrossings = NULL;
  chartInstance->chartInfo.outputs = NULL;
  chartInstance->chartInfo.derivatives = NULL;
  chartInstance->chartInfo.mdlRTW = mdlRTW_c11_LessonIII;
  chartInstance->chartInfo.mdlStart = mdlStart_c11_LessonIII;
  chartInstance->chartInfo.mdlSetWorkWidths = mdlSetWorkWidths_c11_LessonIII;
  chartInstance->chartInfo.extModeExec = NULL;
  chartInstance->chartInfo.restoreLastMajorStepConfiguration = NULL;
  chartInstance->chartInfo.restoreBeforeLastMajorStepConfiguration = NULL;
  chartInstance->chartInfo.storeCurrentConfiguration = NULL;
  chartInstance->chartInfo.callAtomicSubchartUserFcn = NULL;
  chartInstance->chartInfo.callAtomicSubchartAutoFcn = NULL;
  chartInstance->chartInfo.debugInstance = sfGlobalDebugInstanceStruct;
  chartInstance->S = S;
  crtInfo->checksum = SF_RUNTIME_INFO_CHECKSUM;
  crtInfo->instanceInfo = (&(chartInstance->chartInfo));
  crtInfo->isJITEnabled = false;
  crtInfo->compiledInfo = NULL;
  ssSetUserData(S,(void *)(crtInfo));  /* register the chart instance with simstruct */
  init_dsm_address_info(chartInstance);
  init_simulink_io_address(chartInstance);
  if (!sim_mode_is_rtw_gen(S)) {
  }

  sf_opaque_init_subchart_simstructs(chartInstance->chartInfo.chartInstance);
  chart_debug_initialization(S,1);
}

void c11_LessonIII_method_dispatcher(SimStruct *S, int_T method, void *data)
{
  switch (method) {
   case SS_CALL_MDL_START:
    mdlStart_c11_LessonIII(S);
    break;

   case SS_CALL_MDL_SET_WORK_WIDTHS:
    mdlSetWorkWidths_c11_LessonIII(S);
    break;

   case SS_CALL_MDL_PROCESS_PARAMETERS:
    mdlProcessParameters_c11_LessonIII(S);
    break;

   default:
    /* Unhandled method */
    sf_mex_error_message("Stateflow Internal Error:\n"
                         "Error calling c11_LessonIII_method_dispatcher.\n"
                         "Can't handle method %d.\n", method);
    break;
  }
}

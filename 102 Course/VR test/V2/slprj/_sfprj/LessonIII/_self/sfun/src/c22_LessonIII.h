#ifndef __c22_LessonIII_h__
#define __c22_LessonIII_h__

/* Include files */
#include "sf_runtime/sfc_sf.h"
#include "sf_runtime/sfc_mex.h"
#include "rtwtypes.h"
#include "multiword_types.h"

/* Type Definitions */
#ifndef struct_sMB5GaXc40ozYzvPFeCX3Q
#define struct_sMB5GaXc40ozYzvPFeCX3Q

struct sMB5GaXc40ozYzvPFeCX3Q
{
  int32_T dummy;
};

#endif                                 /*struct_sMB5GaXc40ozYzvPFeCX3Q*/

#ifndef typedef_c22_coder_internal_cell
#define typedef_c22_coder_internal_cell

typedef struct sMB5GaXc40ozYzvPFeCX3Q c22_coder_internal_cell;

#endif                                 /*typedef_c22_coder_internal_cell*/

#ifndef typedef_SFc22_LessonIIIInstanceStruct
#define typedef_SFc22_LessonIIIInstanceStruct

typedef struct {
  SimStruct *S;
  ChartInfoStruct chartInfo;
  uint32_T chartNumber;
  uint32_T instanceNumber;
  int32_T c22_sfEvent;
  boolean_T c22_isStable;
  boolean_T c22_doneDoubleBufferReInit;
  uint8_T c22_is_active_c22_LessonIII;
  int16_T *c22_dO;
  real_T *c22_limitFwd;
  uint8_T *c22_certainty;
} SFc22_LessonIIIInstanceStruct;

#endif                                 /*typedef_SFc22_LessonIIIInstanceStruct*/

/* Named Constants */

/* Variable Declarations */
extern struct SfDebugInstanceStruct *sfGlobalDebugInstanceStruct;

/* Variable Definitions */

/* Function Declarations */
extern const mxArray *sf_c22_LessonIII_get_eml_resolved_functions_info(void);

/* Function Definitions */
extern void sf_c22_LessonIII_get_check_sum(mxArray *plhs[]);
extern void c22_LessonIII_method_dispatcher(SimStruct *S, int_T method, void
  *data);

#endif

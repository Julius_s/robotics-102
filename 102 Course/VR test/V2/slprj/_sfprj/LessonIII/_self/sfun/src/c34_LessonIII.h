#ifndef __c34_LessonIII_h__
#define __c34_LessonIII_h__

/* Include files */
#include "sf_runtime/sfc_sf.h"
#include "sf_runtime/sfc_mex.h"
#include "rtwtypes.h"
#include "multiword_types.h"

/* Type Definitions */
#ifndef typedef_SFc34_LessonIIIInstanceStruct
#define typedef_SFc34_LessonIIIInstanceStruct

typedef struct {
  SimStruct *S;
  ChartInfoStruct chartInfo;
  uint32_T chartNumber;
  uint32_T instanceNumber;
  int32_T c34_sfEvent;
  boolean_T c34_isStable;
  boolean_T c34_doneDoubleBufferReInit;
  uint8_T c34_is_active_c34_LessonIII;
  uint8_T (*c34_messageBuffer_data)[64];
  int32_T (*c34_messageBuffer_sizes)[2];
  uint8_T (*c34_message)[30];
} SFc34_LessonIIIInstanceStruct;

#endif                                 /*typedef_SFc34_LessonIIIInstanceStruct*/

/* Named Constants */

/* Variable Declarations */
extern struct SfDebugInstanceStruct *sfGlobalDebugInstanceStruct;

/* Variable Definitions */

/* Function Declarations */
extern const mxArray *sf_c34_LessonIII_get_eml_resolved_functions_info(void);

/* Function Definitions */
extern void sf_c34_LessonIII_get_check_sum(mxArray *plhs[]);
extern void c34_LessonIII_method_dispatcher(SimStruct *S, int_T method, void
  *data);

#endif

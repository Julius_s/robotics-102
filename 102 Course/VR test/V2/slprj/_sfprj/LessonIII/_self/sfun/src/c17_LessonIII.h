#ifndef __c17_LessonIII_h__
#define __c17_LessonIII_h__

/* Include files */
#include "sf_runtime/sfc_sf.h"
#include "sf_runtime/sfc_mex.h"
#include "rtwtypes.h"
#include "multiword_types.h"

/* Type Definitions */
#ifndef typedef_SFc17_LessonIIIInstanceStruct
#define typedef_SFc17_LessonIIIInstanceStruct

typedef struct {
  SimStruct *S;
  ChartInfoStruct chartInfo;
  uint32_T chartNumber;
  uint32_T instanceNumber;
  int32_T c17_sfEvent;
  boolean_T c17_isStable;
  boolean_T c17_doneDoubleBufferReInit;
  uint8_T c17_is_active_c17_LessonIII;
  real32_T c17_t;
  boolean_T c17_t_not_empty;
  real32_T (*c17_AI1)[3];
  real32_T (*c17_AI2)[3];
  real32_T (*c17_AI3)[3];
  real32_T (*c17_AI4)[3];
  real32_T (*c17_AI5)[3];
  real32_T (*c17_AI6)[3];
} SFc17_LessonIIIInstanceStruct;

#endif                                 /*typedef_SFc17_LessonIIIInstanceStruct*/

/* Named Constants */

/* Variable Declarations */
extern struct SfDebugInstanceStruct *sfGlobalDebugInstanceStruct;

/* Variable Definitions */

/* Function Declarations */
extern const mxArray *sf_c17_LessonIII_get_eml_resolved_functions_info(void);

/* Function Definitions */
extern void sf_c17_LessonIII_get_check_sum(mxArray *plhs[]);
extern void c17_LessonIII_method_dispatcher(SimStruct *S, int_T method, void
  *data);

#endif
